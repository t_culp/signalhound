# Creates .tar.xz compressed archives from daily SignalHound data. Intended to
# be run with a crontab job daily. Currently looks for these daily directories
# in the same folder it is run from unless a directory is specified with the -p
# flag. The compressed archives are similarly saved to the same directory it is
# run from unless one is specified.

import subprocess
import datetime
import os
import sys

date = datetime.datetime.now().strftime('%Y%m%d')

option_p = False
option_d = False
path = ''
i = 1
while i < len(sys.argv):
  if sys.argv[i] == '-p':
    option_p = True
    sys.argv.pop(i)
    path = sys.argv.pop(i)
  elif sys.argv[i] == '--delete':
    option_d = True
    sys.argv.pop(i)
  else:
    i += 1

d = os.path.join(path, date)
if os.path.isdir(d):
  subprocess.call(['tar', 'cf', '%s.tar.xz' % d, '--xz', d])
  if option_d:
    subprocess.call(['rm', '-rf', d])

