// signalhound.js
// --------------
// Pager javascript for signalhound browser: by_tag.html, by_date.html
// Uses URI.js: http://medialize.github.io/URI.js/
//
// 2014-07-24 CAB

// Sets the scroll bar for div data-select back to its position before the new
// page was loaded.
window.onload = function() {
    document.getElementById("data-select").scrollTop = 
        localStorage.getItem("scroll");
}

// Generate plot url and update src for <img> element with id="plot_img"
function plupdate() {
    // Parse url.
    var url = URI(window.location.href);
    var key_value = url.search(true);
    
    // Extract variables, use defaults in necessary.
    var data = key_value["data"];
    if (typeof data === "undefined") data = default_data;
    var plot = key_value["plot"];
    if (typeof plot === "undefined") plot = default_plot;

    // Generate plot url.
    plot_url = url_base + "signalhound1/" + data + "_plot" + plot + ".png";
    // Update url for img tag.
    document.getElementById("plot_img1").src = plot_url;

    // Generate plot url.
    plot_url = url_base + "signalhound2/" + data + "_plot" + plot + ".png";
    // Update url for img tag.
    document.getElementById("plot_img2").src = plot_url;
}

// Update a (key, value) pair in the url search string and then 
// reload the page.
function set_value(key, value) {
    // Store div data-select scroll bar position
    localStorage.setItem("scroll",
        document.getElementById("data-select").scrollTop);

    // Update url with new (key, value) pair.
    var url = URI(window.location.href);
    url.removeSearch(key);
    url.addSearch(key, value);

    // Take us to the new url.
    window.location.href = url;
}
