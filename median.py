import math
import numpy
import datetime
import os
import re
import sys

def extractFreqs(filename):
  f = open(filename)
  # discard first line of file
  f.readline()
  freqs = []
  for line in f:
    freqs.append(float(line.split(',')[0]))
  f.close()
  return freqs

def extractAmps(filename):
  f = open(filename) 
  # discard first line of file
  f.readline()
  amps = [] 
  for line in f: 
    amps.append(float(line.split(',')[1]))
  f.close()
  return amps 

# calculates a median trace over the course of the dates (datetime objects)
# given in start and end. Looks in base for the date subdirectories
def calculateMedian(start, end, base):
  day = datetime.timedelta(days=1)
  # this will be a list of lists... data[i][:] gives an entire trace, 
  data = []
  # all of the files we are looking at should share the same frequencies, so
  # getting an arbitrary set of frequencies will work.
  freqs = []
  subdir = os.path.join(base, start.strftime('%Y%m%d'))
  if not os.path.isdir(subdir):
    return None, None
  for trace in os.listdir(subdir):
    if re.match('^[0-9]{8}_[0-9]{6}_trace.csv$', trace) is not None:
      freqs = extractFreqs(os.path.join(subdir, trace))
      break
  # loop through the days and build our list of lists of amplitudes
  while start <= end:
    subdir = os.path.join(base, start.strftime('%Y%m%d'))
    if not os.path.isdir(subdir):
      return None, None
    for trace in os.listdir(subdir):
      if re.match('^[0-9]{8}_[0-9]{6}_trace.csv$', trace) is not None:
        data.append(extractAmps(os.path.join(subdir, trace)))
    start = start + day
  medians = [None] * len(freqs)
  for i in xrange(len(freqs)):
    fixedFrequency = [None] * len(data)
    for j in xrange(len(data)):
      fixedFrequency[j] = data[j][i]
    medians[i] = numpy.median(fixedFrequency)
  return freqs, medians

option_p = False
path = ''
i = 1
while i < len(sys.argv):
  if sys.argv[i] == '-p':
    option_p = True
    sys.argv.pop(i)
    path = sys.argv.pop(i)
  else:
    i += 1

if len(sys.argv) == 1:
  print 'Too few arguments (at least one date in yyyy-mmm-dd format required)'
elif len(sys.argv) > 3:
  print ('Too many arguments supplied (at most one start date and one end' +
    'date may be supplied in yyyy-mmm-dd format)')
else:
  start = sys.argv[1]
  end = ''
  if len(sys.argv) == 2:
    end = start
  else:
    end = sys.argv[2]
  startDate = None
  endDate = None
  try:
    startDate = datetime.datetime.strptime(start, '%Y-%b-%d')
    endDate = datetime.datetime.strptime(end, '%Y-%b-%d')
  except ValueError:
    print 'Invalid date format. yyyy-mmm-dd format required.'
  if startDate is not None and endDate is not None:
    f, m = calculateMedian(startDate, endDate, path)
    ret = 'frequency (MHz), amplitude (dBm)\n'
    for i in xrange(len(f)):
      ret = ret + str(f[i]) + ',' + str(m[i]) + '\n'
    path = os.path.join(path, 'medians')
    if not os.path.isdir(path):
      os.makedirs(path)
    out = open(os.path.join(path, '%s_%s_median.csv' %
      (startDate.strftime('%Y%m%d'), endDate.strftime('%Y%m%d'))), 'w')
    out.write(ret)
    out.close()

