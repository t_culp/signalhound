function reduc_ebmap_pager(mapn,uk,c)
% reduc_ebmap_pager(mapn,ukpervolt,c);
%
% c is a two element array for color axis. E is plotted with +/- c(1). B is plotted
% with +/- c(2). (default [3,3])
% Generate plots for an E/B map pager. Plots are saved to ebmap_pager/.
%
% ex. reduc_ebmap_pager('1205/real_blah_blah'); 


if ~exist('uk','var')
  uk=[];
end
if isempty(uk)
  uk=get_ukpervolt;
end

if ~exist('c','var')
  c=[];
end
if isempty(c)
  c=[3,3];
end

figure(1)
setwinsize(1,800,600)

if ~exist('ebmap_pager','dir')
  system('mkdir ebmap_pager');
end

uk

% make row vector
mapn=rvec(mapn);

% find jacktype
jackn=findstr(mapn,'jack');
jackn=str2num(mapn(jackn+4));

% non-jackknifed plots
[m,map]=reduc_plotebmap(mapn,uk,0);
make_ebplot(m,map,c,mapn);

% jackknifed plots
if jackn>0
  [m,map]=reduc_plotebmap(mapn,uk,1);
  make_ebplot(m,map,c,mapn);
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_ebplot(m,map,c,fn)

Nmaps=numel(map);
jackt={'a','b'};

for i=1:Nmaps

  clf
  
  imagesc(m.x_tic,m.y_tic,map(i).E);caxis([-c(1),c(1)]);colorbar;
  title('E map');
  gtitle(strrep(fn,'_','\_'),.98);

  if Nmaps>1
    fname=sprintf('ebmap_pager/%s_E_jack%s.gif',fn,jackt{i});
  else
    fname=sprintf('ebmap_pager/%s_E_jack.gif',fn);
  end
  
  fdir=fileparts(fname);
  if ~exist(fdir,'dir')
    system(sprintf('mkdir %s',fdir));
  end
  fname=strrep(fname,'.mat','');
  printfig(1,fname,'pp');
  
  clf
  
  imagesc(m.x_tic,m.y_tic,map(i).B);caxis([-c(2),c(2)]);colorbar;
  title('B map');
  gtitle(strrep(fn,'_','\_'),.98);
  
  if Nmaps>1
    fname=sprintf('ebmap_pager/%s_B_jack%s.gif',fn,jackt{i});
  else
    fname=sprintf('ebmap_pager/%s_B_jack.gif',fn);
  end
  fname=strrep(fname,'.mat','');  
  printfig(1,fname,'pp');
  
end

  
return
