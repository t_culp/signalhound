function tuning_browser_html(t1,t2,html_dir,tuningdir_url)
% function tuning_browser_html(t1,t2,html_dir,tuningdir_url)
%
% t1 is the first yymm to make html for
% t2 is the last yymm
%    default is the keck dates: '1103' to '2000'
%
% html_dir is where to save the html.
%    default is 'tuning_plots/browser'
% tuningdir_url is where the .png files are 
%    default is '../.' i.e. 'tuning_plots'
%

%Tuning browser, made in the form of reduc_plot_browser
if nargin<1 || isempty(t1)
  t1='110303';
end
if nargin<2 || isempty(t2)
  t2='200000';
end
if nargin<3 || isempty(html_dir)
  html_dir=fullfile('tuning_plots','browser');
end
if ~exist(html_dir,'dir')
  mkdir(html_dir);
end
if nargin<4 || isempty(tuningdir_url)
  tuningdir_url=fullfile('..','.');
end

% Are we BICEP2 or keck?
if(~exist('experiment','var'))
  experiment=get_experiment_name;
  if(strcmp(experiment,'bicep2'))
    experiment='BICEP2';
  end
end

% Index the tunings
pt.tag={};
if(strmatch(experiment,'keck'))
  %files have been moved to subdirs since there are so freaking many...
  subdirs=dir([html_dir '/' tuningdir_url]);
  subdirs=subdirs(cellfun(@(x) x==1,{subdirs(:).isdir}));
  files=[]; %initialize files
  for ii=1:length(subdirs)
    %make sure the subdir exists in the browser folder
    if ~exist([html_dir '/' subdirs(ii).name])
      mkdir([html_dir '/' subdirs(ii).name])
      system(['chmod g+w ' html_dir '/' subdirs(ii).name]);
    end
    files_tmp=dir([html_dir '/' tuningdir_url '/' subdirs(ii).name '/*rx0_ssa.png']);
    files_tmp={files_tmp.name};
    %if subdir is not the root folder...
    if ~strcmp(subdirs(ii).name,'.')
      files_tmp=cellfun(@(x) [subdirs(ii).name '/' x],files_tmp,'UniformOutput',false);
    end
  files=[files files_tmp];
  end

else
  %BICEP2 is not subdivided since it has fewer tunings
  files=dir('tuning/');
  ind=[files.isdir];
  ind(1:2)=0;
  files={files(ind).name};  %find the directories
end
%filter out images not in the dates
for i=1:length(files)  
  if str2num(files{i}(1:4)) > str2num(t1(1:4)) & str2num(files{i}(1:4)) < str2num(t2(1:4))
    pt.tag{end+1}=files{i}(1:10);     % keep only files between t1 and t2.  structure is tag= yearmonthdate_hour.  don't keep min/sec. 
    if strcmp(pt.tag{end}(end),'_')
      pt.tag{end}=pt.tag{end}(1:end-1);
    end
  end
end

% Top-level index page
h=fopen(fullfile(html_dir,'index.html'),'wt');
fprintf(h,'<html>\n\n');
fprintf(h,'<SCRIPT LANGUAGE="JavaScript">\n');
fprintf(h,'<!--\n');
fprintf(h,'scanset_tag=window.location.search.substring(1);\n');
fprintf(h,'tuning_plot_type=''ssa'';\n');
if(strmatch(experiment,'keck'))
  fprintf(h,'tuning_plot_rx=''_rx0'';\n');
elseif(strmatch(experiment,'BICEP2'))
  fprintf(h,'tuning_plot_rx='''';\n');
end
fprintf(h,'tuning_plot_url='''';\n');
fprintf(h,'tuning_plot_row=''0'';\n');
fprintf(h,'tag=''%s'';\n',pt.tag{end});
if (strmatch(experiment,'keck'))
  fprintf(h,'tuning_plot_prefix=''../%s'';\n\n',tuningdir_url);
elseif(strmatch(experiment,'BICEP2'))
  fprintf(h,'tuning_plot_prefix=''%s'';\n\n',tuningdir_url);
end

fprintf(h,'function set_tuning_plot_url(){\n');
fprintf(h,' switch (tuning_plot_type)\n');
fprintf(h,' {\n');
fprintf(h,' case "ssa":\n');
fprintf(h,'   tuning_plot_url=tuning_plot_prefix+"/"+tag+tuning_plot_rx+"_ssa.png";\n');
fprintf(h,'   break;\n');
fprintf(h,' case "sq2":\n');
fprintf(h,'   tuning_plot_url=tuning_plot_prefix+"/"+tag+tuning_plot_rx+"_sq2servo.png"; \n');
fprintf(h,'   break;\n');
fprintf(h,' case "sq1servo":\n');
fprintf(h,'   tuning_plot_url=tuning_plot_prefix+"/"+tag+tuning_plot_rx+"_sq1servo_r"+tuning_plot_row+".png";\n');
fprintf(h,'   break;\n');
fprintf(h,' case "sq1ramp":\n');
fprintf(h,'   tuning_plot_url=tuning_plot_prefix+"/"+tag+tuning_plot_rx+"_sq1ramp_r"+tuning_plot_row+".png";\n');
fprintf(h,'   break;\n');
fprintf(h,' }\n');
fprintf(h,' tagpage.document["tuning"].src=tuning_plot_url;\n');
fprintf(h,'}\n');

fprintf(h,'function set_rx(newrx){\n');
fprintf(h,' tuning_plot_rx=newrx;\n');
fprintf(h,' set_tuning_plot_url();\n');
fprintf(h,'}\n');
fprintf(h,'function set_row(newrow){\n');
fprintf(h,' tuning_plot_row=newrow;\n');
fprintf(h,' set_tuning_plot_url();\n');
fprintf(h,'}\n');
fprintf(h,'function set_plot_type(newtype){\n');
fprintf(h,' tuning_plot_type=newtype;\n');
fprintf(h,' set_tuning_plot_url();\n');
fprintf(h,'}\n');
fprintf(h,'function set_plot_tag(newtag){\n');
fprintf(h,' tag=newtag;\n');
fprintf(h,' set_tuning_plot_url();\n');
fprintf(h,'}\n');

fprintf(h,'//-->\n</SCRIPT>\n\n');
fprintf(h,'<frameset noresize="noresize" cols="200,*">\n\n');
fprintf(h,'<frame src="tag_index.html">\n');
fprintf(h,'<frame src="%s.html" name="tagpage">\n\n',pt.tag{end});
fprintf(h,'</frameset>\n\n</html>\n\n');
fclose(h);
setpermissions(fullfile(html_dir,'index.html'));

% Left pane with list of tags
h=fopen(fullfile(html_dir,'tag_index.html'),'wt');
fprintf(h,'<html>\n\n');
fprintf(h,'<body bgcolor="#d0d0d0">\n\n');
fprintf(h,'<SCRIPT LANGUAGE="JavaScript">\n');
fprintf(h,'<!--\n');
fprintf(h,'if(parent.scanset_tag!=""){\n');
fprintf(h,'  parent.tagpage.location=parent.scanset_tag+''.html'';\n');
fprintf(h,'}\n//-->\n</SCRIPT>\n\n');
fprintf(h,'Go to:\n');
fprintf(h,'<pre>\n');
fprintf(h,'<b><font color="blue">Tuning name</font></b>\n\n');
for ii=length(pt.tag):-1:1
  fprintf(h,'<a href="%s.html" target="tagpage"><font color="blue">%s</font></a>\n',...
    pt.tag{ii},pt.tag{ii});
end
fprintf(h,'</pre>\n\n');
fprintf(h,'</body>\n\n');
fprintf(h,'</html>\n\n');
fclose(h);
setpermissions(fullfile(html_dir,'tag_index.html'));


% Individual HTML files for each tag
for ii=1:length(pt.tag)

  h=fopen(fullfile(html_dir,[pt.tag{ii} '.html']),'wt');
  fprintf(h,'<html>\n\n');
  fprintf(h,'<head>\n\n');
  fprintf(h,'</head>\n<body>\n\n');
  fprintf(h,'<center><h2>%s squid tuning browser</h2></center>\n',experiment);
  fprintf(h,'<center>\n');
  if ii>1
    fprintf(h,'[<a href="../%s.html">prev_tag</a>]',pt.tag{ii-1});
  else
    fprintf(h,'[prev_tuning]');
  end
  fprintf(h,'<b>[%s]</b>',pt.tag{ii});
  if ii<length(pt.tag)
    fprintf(h,'[<a href="../%s.html">next_tag</a>]',pt.tag{ii+1});
  else
    fprintf(h,'[next_tuning]');
  end
  fprintf(h,'</center>\n');
  
  
 % Choosing which plots to view 
  fprintf(h,'<center>\n');
  fprintf(h,'<p>\n');
  fprintf(h,'<table border="0" cellspacing="1" cellpadding="1">\n');

  fprintf(h,'<tr>\n');
  if(strmatch(experiment,'keck'))
    fprintf(h,'<td><b>Receiver</b>\n');
  end
  fprintf(h,'<td><b>Squid Stage &nbsp</b></td>\n');
  fprintf(h,'<td><b>Row</b></td>\n');

  fprintf(h,'<tr>\n');
  if(strmatch(experiment,'keck'))
    fprintf(h,'<td><a href="javascript:parent.set_rx(''_rx0'');">Rx0</a></td>\n');
  end
  fprintf(h,'<td><a href="javascript:parent.set_plot_type(''ssa'');">SSA</a></td>\n');
  fprintf(h,'<td><a href="javascript:parent.set_row(''0'');">0</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''1'');">1</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''2'');">2</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''3'');">3</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''4'');">4</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''5'');">5</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''6'');">6</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''7'');">7</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''8'');">8</a>\n');
  fprintf(h,'</td>\n');

  fprintf(h,'<tr>\n');
  if(strmatch(experiment,'keck'))
    fprintf(h,'<td><a href="javascript:parent.set_rx(''_rx1'');">Rx1</a></td>\n');
  end
  fprintf(h,'<td><a href="javascript:parent.set_plot_type(''sq2'');">SQ2</a></td>\n');
  fprintf(h,'<td><a href="javascript:parent.set_row(''9'');">9</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''10'');">10</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''11'');">11</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''12'');">12</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''13'');">13</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''14'');">14</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''15'');">15</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''16'');">16</a>\n');
  fprintf(h,'</td>\n');

  fprintf(h,'<tr>\n');
  if(strmatch(experiment,'keck'))
    fprintf(h,'<td><a href="javascript:parent.set_rx(''_rx2'');">Rx2</a></td>\n');
  end
  fprintf(h,'<td><a href="javascript:parent.set_plot_type(''sq1servo'');">SQ1 servo</a></td>\n');
  fprintf(h,'<td><a href="javascript:parent.set_row(''17'');">17</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''18'');">18</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''19'');">19</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''20'');">20</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''21'');">21</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''22'');">22</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''23'');">23</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''24'');">24</a>\n');
  fprintf(h,'</td>\n');

  fprintf(h,'<tr>\n');
  if(strmatch(experiment,'keck') & str2num(pt.tag{ii}(1:4))>1200)
    fprintf(h,'<td><a href="javascript:parent.set_rx(''_rx3'');">Rx3</a></td>\n');
  elseif(strmatch(experiment,'keck') & str2num(pt.tag{ii}(1:6))<120000)
    fprintf(h,'<td></td>\n');
  end
  fprintf(h,'<td><a href="javascript:parent.set_plot_type(''sq1ramp'');">SQ1 ramp</a></td>\n');
  fprintf(h,'<td><a href="javascript:parent.set_row(''25'');">25</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''26'');">26</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''27'');">27</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''28'');">28</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''29'');">29</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''30'');">30</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''31'');">31</a>\n');
  fprintf(h,'<a href="javascript:parent.set_row(''32'');">32</a>\n');
  fprintf(h,'</td>\n');

  if(strmatch(experiment,'keck') & str2num(pt.tag{ii}(1:4))>1200)
    fprintf(h,'<tr>\n');
    fprintf(h,'<td><a href="javascript:parent.set_rx(''_rx4'');">Rx4</a></td>\n');
  end
  
  fprintf(h,'</table>\n');
  if(strmatch(experiment,'keck'))
    fprintf(h,'<a href="%s"><img src="%s" name="tuning" height="600"></a>\n', ...
      'javascript:location.href=parent.tuning_plot_url;', ...
      fullfile(tuningdir_url,[pt.tag{ii} '_rx0_ssa.png']));
  elseif(strmatch(experiment,'BICEP2'))
    fprintf(h,'<a href="%s"><img src="%s" name="tuning" height="600"></a>\n', ...
      fullfile(tuningdir_url,[pt.tag{ii} '_ssa.png']), ...
      fullfile(tuningdir_url,[pt.tag{ii} '_ssa.png']));
  end
  fprintf(h,'<SCRIPT LANGUAGE="JavaScript">\n');
  fprintf(h,'<!--\n');
  fprintf(h,'parent.set_plot_tag(''%s'');\n',pt.tag{ii});
  fprintf(h,'parent.set_tuning_plot_url();\n');
  fprintf(h,'//-->\n</SCRIPT>\n\n');
  fprintf(h,'</center>\n');

  fprintf(h,'</body>\n\n');
  fprintf(h,'</html>');
  fclose(h);
  setpermissions(fullfile(html_dir,[pt.tag{ii} '.html']));
end
