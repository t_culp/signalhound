function plot_tuning(dates,rx,default_tuning)
% out=plot_tuning(dates, rx, default_tuning)
% 
% dates = which dates to analyze.  in gcp format
%         i.e. {'110524','110303}
% rx = which rx to look at.  defaults to all 
%      number of off rx in all different for experiments
% default_tuning = which tuning to compare it to.
%      default is a link in the tuning dir to default.
%
% All of this assumes you have a symbolic link to the tuning directory
% and a tuning_plots folder to save them in.
% 
% JAB 20100902 
% SAS 20110518
%
% 4x4 per row poster plots for sq1rampc & sq1servo
% 4x4 plots for ssa and sq2servo
%

if(~exist('rx','var'))
  rx=[0,1,2,3,4];
end

if(~exist('default_tuning','var'))
  default_tuning = 'default_tuning';
end


%don't plot to screen since we are saving.
set(0,'DefaultFigureVisible','off');

index=zeros(length(rx),1);

for dat=1:length(dates)

  % get the array information
  tmp=['20' dates{dat}];
  [p ind]=get_array_info(tmp(1:8));

  % strip down rx array to contain only receivers that are actually present
  keepind=ismember(rx,p.rx);
  rx=rx(keepind);

  for i=1:length(rx);
    % don't save an _rx0 extension if only 1 rx exists (i.e. bicep2)
    if(numel(unique(p.rx))==1)
      mceext='';
      mceext_save='';
    else
      mceext=sprintf('mce%d',rx(i));
      mceext_save=sprintf('_rx%d',rx(i));
    end

    %read in the default data first
    tuning_dir=dir(['tuning/' mceext '/' default_tuning '*']);
    if dat==1
      index(i)=index(i)+1;
      tmp=analyze_tuning(['tuning/' mceext '/' char(tuning_dir(1).name)]); 
      tmp.mceext=mceext_save;
      out(index(i),rx(i)+1)=tmp;
    end
    tuning_dir=dir(['tuning/' mceext '/' char(dates{dat}) '*']);
    ind=find([tuning_dir.isdir]);
    %read in the tuning data for all of the dates
    for j=1:length(ind)
      index(i)=index(i)+1;
      tmp=analyze_tuning(['tuning/' mceext '/' char(tuning_dir(ind(j)).name)]);
      tmp.mceext=mceext_save;
      out(index(i),rx(i)+1)=tmp;
    end
  end
end

%cyan=default, current =blue.  only plot one tuning + default at a time
colors={'c','b'};

%try to make the figure in the background
h1=make_fig;
set(0,'currentfigure',h1)
set(0,'DefaultFigureVisible','off');

%plot each day individually against the default.

for rrx=rx %loops over rx's 
  
for dd=2:index(rx==rrx) %loops over days (can be different number for each rx)
  
  gcp_offset=528*rrx;
  plots_vec=[1 dd]; %vector to pass to plotting routines
  %make the filename and check that the folder exists
  savefolder=['tuning_plots/' char(out(dd,rrx+1).tune_name(1:4))];
  if ~exist(savefolder,'dir')
    system(['mkdir ' savefolder]);
    system(['chmod g+w ' savefolder]);
  end
  savedir=[savefolder '/' char(out(dd,rrx+1).tune_name(5:9)) char(out(dd,rrx+1).mceext)];
  
  % SQ1 rampc poster plot by row
  ylims=-50:25:50;
  for irow=1:33  
    for ii=1:2
      kk=plots_vec(ii);
      for icol=1:16;
        igcp=mce2gcp(irow-1,icol-1)+1;  %gcp index
        subplot(4,4,icol); hold on; box on; 
        %plot the sq1ramp for igcp
        plot(out(kk,rrx+1).fb1ramp./1000,out(kk,rrx+1).sq1ramp(igcp,:)./1000,'Color',colors{ii});
        title(['COL' int2str(icol-1) ' (GCP' int2str(igcp-1+gcp_offset) ')'],'FontSize',8);
        %plot the chosen adc offset
        %the adc_offset_cr=(adc_offset_c+y(of sq1ramp plot))/sample_num
        adcoffset=-out(kk,rrx+1).adcoffsetsc(icol)+out(kk,rrx+1).adcoffsets_cr(igcp)*10;
        plot([out(kk,rrx+1).fb1ramp(1)./1000 out(kk,rrx+1).fb1ramp(end)./1000],[adcoffset./1000 adcoffset./1000],'--','Color',colors{ii});
        %axis tight
        if out(kk,rrx+1).dead_pix(igcp)==1; deadtext(out,colors{ii},ii); end
        set(gca,'FontSize',8,'XLim',[ceil(out(kk,rrx+1).fb1ramp(1)./1000) ceil(out(kk,rrx+1).fb1ramp(end)./1000)],'YLim',[ylims(1) ylims(end)],'XTick',[],'YTick',[]);
        %Setting labels:
        switch icol
          case {1,5,9}
            set(gca,'YTick',ylims)
          case {14,15,16}
            set(gca,'XTick',ceil(out(kk,rrx+1).fb1ramp(1)./1000):4:ceil(out(kk,rrx+1).fb1ramp(end)./1000));
          case 13
            set(gca,'XTick',ceil(out(kk,rrx+1).fb1ramp(1)./1000):4:ceil(out(kk,rrx+1).fb1ramp(end)./1000),'YTick',ylims);
            xlabel('sq1 fb (adu/1000)','FontSize',8); ylabel('error (adu/1000)','FontSize',8);
        end
      end
    end

    margin_legend(out,colors,kk,rrx+1);
    stamp(out,savedir);
    suptitle(['SQ1RAMPC ROW' int2str(irow-1)]);
    saveplot([savedir '_sq1ramp_r' int2str(irow-1) '.png']);
  end

  % SQ1 servo poster plot by row
  ydiv=1000;
  inc=1;
  tick_inc=1;
  len=length(out(dd,rrx+1).sq1servo(1,:));
  xlims=[out(kk,rrx+1).sq1fbramp(1)./1000 out(kk,rrx+1).sq1fbramp(end)./1000];
  for irow=1:33
    for icol=1:16;
      ymin=[]; ymax=[];
      for ii=1:2
        kk=plots_vec(ii);
        igcp=mce2gcp(irow-1,icol-1)+1; %gcp index
        ymin=min([(out(kk,rrx+1).sq1servo(igcp,25:len)./1000)./inc ymin]);
        ymax=max([(out(kk,rrx+1).sq1servo(igcp,25:len)./1000)./inc ymax]);
        subplot(4,4,icol); hold on; box on; set(gca,'XTick',[],'YTick',[]);
        %plot the sq1servo vs sq1fb
        plot(out(kk,rrx+1).sq1fbramp./1000,out(kk,rrx+1).sq1servo(igcp,:)./1000,'Color',colors{ii});
        title(['COL' int2str(icol-1) ' (GCP' int2str(igcp-1+gcp_offset) ')'],'FontSize',8);

        %Find the sq2 fb set point.  Tuning code minimizes by flux quantas, so may have to add a flux quanta to get it in range.
        sq2fbset=out(kk,rrx+1).sq2fbset(igcp)./1000;
        if sq2fbset<ymin                        
          sq2fbset=sq2fbset+out(kk,rrx+1).sq2_flux_quanta(icol)./1000;
        end
        %Plot the sq2 fb set point.  
        plot([xlims(1) xlims(2)],[sq2fbset sq2fbset],'--','Color',colors{ii});

        %plotting stuff:
        yticks=(inc*floor(ymin)):tick_inc:(inc*ceil(ymax));
        ylims=(inc*floor(ymin)):inc:(inc*ceil(ymax));
        set(gca,'FontSize',8,'XLim',[xlims(1) xlims(2)],'XTick',[],'YTick',yticks,'YLim',[ylims(1) ylims(end)]);
        if out(kk,rrx+1).dead_pix(igcp)==1; deadtext(out,colors{ii},ii); end
        switch icol
          %case {1,5,9}
          % set(gca,'YTick',ylims)
          case {14,15,16}
            set(gca,'XTick',ceil(xlims(1)):4:ceil(xlims(2)));
          case 13
            set(gca,'XTick',ceil(xlims(1)):4:ceil(xlims(2)));
            xlabel('sq1 fb (adu/1000)','FontSize',8); ylabel('sq2 fb (adu/1000)','FontSize',8);
        end
      end
    end
    margin_legend(out,colors,kk,rrx+1);
    stamp(out,savedir);
    suptitle(['SQ1SERVO ROW' int2str(irow-1)]);
    saveplot([savedir '_sq1servo_r' int2str(irow-1) '.png']);
  end

  % SQ2 servo poster plot
  ydiv=1000;
  inc=5;
  tick_inc=10;
  len=length(out(dd,rrx+1).sq2servo(1,:));
  xlims=[out(kk,rrx+1).sq2fbramp(1)./1000 out(kk,rrx+1).sq2fbramp(end)./1000];
  for icol=1:16;
    ymin=[]; ymax=[];
    for ii=1:2
      kk=plots_vec(ii);
      ymin=min([(out(kk,rrx+1).sq2servo(icol,25:len)./1000)./inc ymin]);
      ymax=max([(out(kk,rrx+1).sq2servo(icol,25:len)./1000)./inc ymax]);
      subplot(4,4,icol); hold on; box on;
      %plot the sq2servo vs sq2fb
      plot(out(kk,rrx+1).sq2fbramp./1000,out(kk,rrx+1).sq2servo(icol,:)./1000,'Color',colors{ii});
      title(['COL' int2str(icol-1)],'FontSize',8);
      %plot the safb set point
      plot([xlims(1) xlims(2)],[out(kk,rrx+1).ssafb(icol)./1000 out(kk,rrx+1).ssafb(icol)./1000],'--','Color',colors{ii});

      %plotting stuff:
      yticks=(inc*floor(ymin)):tick_inc:(inc*ceil(ymax));
      ylims=(inc*floor(ymin)):inc:(inc*ceil(ymax));
      set(gca,'FontSize',8,'XLim',[xlims(1) xlims(2)],'XTick',[],'YTick',yticks,'YLim',[ylims(1) ylims(end)]);
      switch icol
        %case {1,5,9}
        % set(gca,'YTick',ylims)
        case {14,15,16}
          set(gca,'XTick',ceil(xlims(1)):5:ceil(xlims(2)));
        case 13
          set(gca,'XTick',ceil(xlims(1)):5:ceil(xlims(2)));
          xlabel('sq2 fb (adu/1000)','FontSize',8); ylabel('ssa fb (adu/1000)','FontSize',8);
      end
    end
  end
  margin_legend(out,colors,kk,rrx+1);
  stamp(out,savedir);
  suptitle(['SQ2SERVO']);
  saveplot([savedir '_sq2servo.png']);

  % SSA servo poster plot
  ydiv=10000;
  inc=1;
  tick_inc=2;
  xlims=[out(kk,rrx+1).ssafbramp(1)./ydiv out(kk,rrx+1).ssafbramp(end)./ydiv];
  for icol=1:16;
    ymin=[]; ymax=[];
    for ii=1:2
      kk=plots_vec(ii);
      ymin=min([(out(kk,rrx+1).ssa(icol,:)./ydiv)./inc ymin]);
      ymax=max([(out(kk,rrx+1).ssa(icol,:)./ydiv)./inc ymax]);
      subplot(4,4,icol); hold on; box on;
      % plot the sa output vs safb
      plot(out(kk,rrx+1).ssafbramp./ydiv,out(kk,rrx+1).ssa(icol,:)./ydiv,'Color',colors{ii});
      title(['COL' int2str(icol-1)],'FontSize',8);
      % plot the chosen adcoffsets
      plot([xlims(1) xlims(2)],[out(kk,rrx+1).adcoffsetsc(icol)./ydiv out(kk,rrx+1).adcoffsetsc(icol)./ydiv],'--','Color',colors{ii});

      %plotting stuff
      yticks=(inc*floor(ymin)):tick_inc:(inc*ceil(ymax));
      ylims=(inc*floor(ymin)):inc:(inc*ceil(ymax));
      set(gca,'FontSize',8,'XLim',[xlims(1) xlims(2)],'XTick',[],'YTick',yticks,'YLim',[ylims(1) ylims(end)]);
      switch icol
        %case {1,5,9}
        %  set(gca,'YTick',ylims)
        case {14,15,16}
          set(gca,'XTick',ceil(xlims(1)):2:ceil(xlims(2)));
        case 13
          set(gca,'XTick',ceil(xlims(1)):2:ceil(xlims(2)));
          xlabel(['ssa fb (adu/' int2str(ydiv) ')'],'FontSize',8); ylabel(['error (adu/' int2str(ydiv) ')'],'FontSize',8);
      end
    end
  end
  margin_legend(out,colors,kk,rrx+1);
  stamp(out,savedir);
  suptitle(['SSA']);
  saveplot([savedir '_ssa.png']);

end
end
end

function stamp(out,savedir)
xpos=.8;
ypos=-.075;
yoff=-.025;
ax=axes('Visible','off');
text(xpos,ypos,savedir,'FontSize',8,'Units','Normalized','Interpreter','none');
if length(out)==1
    text(xpos,ypos+yoff,out.tune_name,'FontSize',8,'Units','Normalized','Interpreter','none');
end
end

function margin_legend(out,colors,kk,rrx)
xpos=-.14;
ypos=.95;
yoff=-.025;
ax=axes('Visible','off');
plot_vec=[1,kk];
if length(out)>1
    text(xpos,ypos,'tune legend','Units','Normalized','FontSize',8)
    for jj=1:2;
        ii=plot_vec(jj);
        text(xpos,ypos+yoff*jj,out(ii,rrx).tune_name,'FontSize',8,'Color',colors{jj},'Units','Normalized','Interpreter','none');
    end
end
end

function deadtext(out,colors,kk)
xpos=.7;
ypos=.9;
ysep=-.125;
if length(out)==1; colors=[1 0 0]; end
if kk<8; text(xpos,ypos+ysep*(kk-1),'DEAD','Color',colors,'FontSize',10,'Units','Normalized'); end
end

function h=make_fig
font='times';
fontsize=14;
titlesize=14;
textsize=14;
h=figure('Position',[1 1 1000 700],'DefaultAxesFontName',font,'DefaultAxesFontSize',...
    fontsize,'DefaultTextFontSize',textsize,'DefaultTextFontName',font,'Visible','off');
box on
end

function saveplot(name)
setwinsize(gcf,1000,700);
mkpng(name,1);
setpermissions(name);
end

