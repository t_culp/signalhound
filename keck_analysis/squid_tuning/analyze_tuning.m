function out=analyze_tuning(tuningdir,savedir)
% analyze squid tunings
% out=analyze_tuning(tuningdir,savedir)

if strcmp(tuningdir(end),'/'); tuningdir=tuningdir(1:end-1); end
indx=strfind(tuningdir,'/');

out.tune_name=tuningdir(indx(end)+1:end);
out.gains=getgains(tuningdir);

% read in experiment.cfg and pull out the parameters
file=[tuningdir '/experiment.cfg'];

disp([tuningdir '/experiment.cfg']);
data=importdata(file,',');
chardata=char(data);
sizechar=size(chardata);
for ii=1:sizechar(1);
    test=strfind(chardata(ii,:),'adc_offset_c ');
    if test, rowadc=ii;end
    test=strfind(chardata(ii,:),'adc_offset_cr');
    if test, rowadccr=ii;end
    test=strfind(chardata(ii,:),'sq2_fb =');
    if test, rowsq2fb=ii;end
    test=strfind(chardata(ii,:),'sq2_fb_set');
    if test, rowsq2fbset=ii;end
    test=strfind(chardata(ii,:),'sa_fb');
    if test, rowsafb=ii;end
    test=strfind(chardata(ii,:),'sa_offset');
    if test, rowsaoff=ii;end
    test=strfind(chardata(ii,:),'dead');
    if test, rowdead=ii;end
    test=strfind(chardata(ii,:),'flux_quanta_all = [');
    if test, rowflux=ii;end
    test=strfind(chardata(ii,:),'sq2_flux_quanta');
    if test, rowsq2flux=ii;end
    test=strfind(chardata(ii,:),'sa_flux_quanta');
    if test, rowsaflux=ii;end

end
%reform adcoff_c
adcoff=chardata(rowadc,:);
indx=regexp(adcoff,'\s-?[0-9\]]');
adcoffsets_c=[];
for ii=1:length(indx)-1
    adcoffsets_c=[adcoffsets_c str2num(adcoff(indx(ii):indx(ii+1)-2))];
end
adcoffsets_c=adcoffsets_c(1:16);
out.adcoffsetsc=adcoffsets_c;

%reform adcoff_cr
adcoff=chardata(rowadccr,:);
indx=regexp(adcoff,'\s-?[0-9\]]');
adcoffsets_cr=[];
for ii=1:length(indx)-1
    adcoffsets_cr=[adcoffsets_cr str2num(adcoff(indx(ii):indx(ii+1)-2))];
end
adcoffsets_cr(1312)=NaN;
adcoffsets_cr=(reshape(adcoffsets_cr,41,32))';
adcoffsets_cr=(adcoffsets_cr(1:16,1:33))';
out.adcoffsets_cr=adcoffsets_cr;

%reform sq2fb
sq2fbtmp=chardata(rowsq2fb,:);
indx=regexp(sq2fbtmp,'\s[0-9\]]');
sq2fb=[];
for ii=1:length(indx)-1
    sq2fb=[sq2fb str2num(sq2fbtmp(indx(ii):indx(ii+1)-2))];
end
sq2fb=sq2fb(1:16);
out.sq2fb=sq2fb;

%reform sq2_fb_set
sq2fbtmp=chardata(rowsq2fbset,:);
indx=regexp(sq2fbtmp,'\s[0-9\]]');
sq2fbset=[];
for ii=1:length(indx)-1
    sq2fbset=[sq2fbset str2num(sq2fbtmp(indx(ii):indx(ii+1)-2))];
end
sq2fbset(1312)=NaN;
sq2fbset=(reshape(sq2fbset,41,32))';
sq2fbset=(sq2fbset(1:16,1:33))';
out.sq2fbset=sq2fbset;

%refrom ssafb
safb=chardata(rowsafb,:);
indx=regexp(safb,'\s[0-9\]]');
ssafb=[];
for ii=1:length(indx)-1
    ssafb=[ssafb str2num(safb(indx(ii):indx(ii+1)-2))];
end
ssafb=(ssafb(1:16));
out.ssafb=ssafb;

%reform sa_offset
saoff=chardata(rowsaoff,:);
indx=regexp(saoff,'\s[0-9\]]');
ssaoff=[];
for ii=1:length(indx)-1
    ssaoff=[ssaoff str2num(saoff(indx(ii):indx(ii+1)-2))];
end
ssaoff=(ssaoff(1:16));
out.ssaoff=ssaoff;

%reform the dead pixel matrix
deadchar=chardata(rowdead,:);
indx=regexp(deadchar,'\s[0-9\]]');
dead_pix=[];
for ii=1:length(indx)
    dead_pix=[dead_pix str2num(deadchar(indx(ii)+1))];
end
dead_pix=(reshape(dead_pix,41,32))';
dead_pix=(dead_pix(1:16,1:33))';
out.dead_pix(1:528)=dead_pix;

%reform the sq1 flux quanta matrix
fluxchar=chardata(rowflux,:);
indx=regexp(fluxchar,'\s-?[0-9\]]');
flux_quanta=[];
for ii=1:length(indx)-1
    flux_quanta=[flux_quanta str2num(fluxchar(indx(ii):indx(ii+1)-2))];
end
flux_quanta(1312)=NaN;
flux_quanta=(reshape(flux_quanta,41,32))';
flux_quanta=(flux_quanta(1:16,1:33))';
out.flux_quanta(1:528)=flux_quanta;

%reform sq2_flux quanta
sq2flux=chardata(rowsq2flux,:);
indx=regexp(sq2flux,'\s[0-9\]]');
sq2_flux_quanta=[];
for ii=1:length(indx)-1
    sq2_flux_quanta=[sq2_flux_quanta str2num(sq2flux(indx(ii):indx(ii+1)-2))];
end
sq2_flux_quanta=(sq2_flux_quanta(1:16));
out.sq2_flux_quanta=sq2_flux_quanta;

%reform sa_flux quanta
saflux=chardata(rowsaflux,:);
indx=regexp(saflux,'\s[0-9\]]');
ssa_flux_quanta=[];
for ii=1:length(indx)-1
    ssa_flux_quanta=[ssa_flux_quanta str2num(saflux(indx(ii):indx(ii+1)-2))];
end
ssa_flux_quanta=(ssa_flux_quanta(1:16));
out.ssa_flux_quanta=ssa_flux_quanta;



% read in squid ramps & servoes

% sq1rampc
files=dir([tuningdir '/*sq1ramp']);
out.sq1ramp=[];
for i=1:length(files)
  [r h]=read_mce([tuningdir '/' files(i).name]);
  out.sq1ramp=[out.sq1ramp' r.err]';
end
param=h.par_ramp.par_step.loop1.par1;
out.fb1ramp=param(1):param(2):(param(1)+param(2)*(param(3)-1));

% sq1servo
files=dir([tuningdir '/*sq1servo']);
rf=read_mce_runfile([tuningdir '/' files(1).name '.run']);
param=rf.par_ramp.par_step.loop2.par1;
out.sq1fbramp=param(1):param(2):(param(1)+param(2)*(param(3)-1));
temp=NaN*ones(33,16,param(3));
for ii=0:32
  if length(files)==2  %idl style
    r1=importdata([tuningdir '/' files(1).name '.r' sprintf('%.2d',ii) '.bias'],' ');
    r2=importdata([tuningdir '/' files(2).name '.r' sprintf('%.2d',ii) '.bias'],' ');
    temp(ii+1,:,:)=[r1.data(:,9:end) r2.data(:,9:end)]';
  end
  if length(files)==1 %python style
    r=importdata([tuningdir '/' files(1).name '.r' sprintf('%.2d',ii) '.bias'],' ');
    temp(ii+1,:,:)=[r.data(:,17:end)]';
  end
end
out.sq1servo=reshape(temp,528,param(3));

% sq2servo
files=dir([tuningdir '/*sq2servo']);
rf=read_mce_runfile([tuningdir '/' files(1).name '.run']);
if length(files)==2 %idl style
  r1=importdata([tuningdir '/' files(1).name '.bias'],' ');
  r2=importdata([tuningdir '/' files(2).name '.bias'],' ');
  out.sq2servo=[r1.data(:,9:end) r2.data(:,9:end)]';
  rf2=read_mce_runfile([tuningdir '/' files(2).name '.run']);
  out.ssafb_init=[rf.servo_init.safb_init rf2.servo_init.safb_init(9:16)];
end
if length(files)==1 %python style
  r=importdata([tuningdir '/' files.name '.bias'],' ');
  out.sq2servo=[r.data(:,17:end)]';
  out.ssafb_init=rf.servo_init.safb_init;
end
param=rf.par_ramp.par_step.loop2.par1;
out.sq2fbramp=param(1):param(2):(param(1)+param(2)*(param(3)-1));

% ssa
files=dir([tuningdir '/*ssa']);
ssa=[];
for i=1:length(files);
  [r h]=read_mce([tuningdir '/' files(i).name]);
  ssa=[ssa' r.err]';
end
for ii=1:16; out.ssa(ii,:)=mean(ssa((1+33*(ii-1)):(33+33*(ii-1)),:)); end
param=h.par_ramp.par_step.loop1.par1;
out.ssafbramp=param(1):param(2):(param(1)+param(2)*(param(3)-1));

% save it
if nargin>1
    if ~exist(savedir); mkdir(savedir); end
    savename=[savedir '/' out.tune_name '_tuning.mat'];
    save(savename,'out');
end

end

function gainmatrix=getgains(tuning_dir)
%
% gainmatrix=getgains(tuning_dir)
%
% This function gets the ERR to FB1_DAC gains at the lockpoints from
% sq1rampc.  These are saved in the tuning directory in the file .sqtune.
% Specify the appropriate lockpoint (+1,-1) of the SQ1s.
%
% output: a 33row x 16col matrix of ERR(nsamp coadded) / FB1_DAC gains.
%
% JAB 20090420

lockpoint=-1;

sqtune=dir([tuning_dir '/*sqtune']);
fullpath=[tuning_dir '/' sqtune(end).name];
read=importdata(fullpath,' ',4);
if lockpoint == -1
    indx=find(strcmp(read.textdata,'<Col0_squid_lockslope_down>'));
    gainmatrix=read.data(indx-4:indx+11,:)';        %33cols x 16rows
else if lockpoint == +1
    indx=find(strcmp(read.textdata,'<Col0_squid_lockslope_up>'));
    gainmatrix=read.data(indx-4:indx+11,:)';
    end
end
gainmatrix=abs(gainmatrix);

end


