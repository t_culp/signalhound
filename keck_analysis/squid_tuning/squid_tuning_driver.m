function squid_tuning_driver(experiment)
%squid_tuning_driver Processes all new squid tunings
%   make sure you have the folder squid_tuning in bicep2_analysis.  If not,
%   run a cvs update -d
% 
%   squid_tuning_driver looks for tunings in the tuning/ directory and
%   compares them to what's been processed in the tuning_plots/ directory.
%   It finds the most recent tuning plot and then processes the un-reduced
%   tunings after this.  This code does not generate the squid tuning
%   browser.  That step is done along with the tag browser in
%   reduc_processnewtags_driver
%
%   20120523 - JPK - Now with 100% all-grain farming of jobs
%   20120628 - SASK - added alfalfa to the farm
%   
%   e.g. squid_tuning_driver('BICEP2');

if(~exist('experiment','var'))
  experiment=get_experiment_name;
  if(strcmp(experiment,'bicep2'))
    experiment='BICEP2';
  end
end

% Create symlinks if necessary
if(~exist('tuning','dir'))
    if(strcmp(experiment,'BICEP2')) % check if it's BICEP2 or Keck
        unix('ln -s /n/bicepfs1/bicep2/bicep2daq/tuning/ tuning');
    else
        unix('ln -s /n/bicepfs2/keck/keckdaq/tuning/ tuning');
    end
end

if(~exist('tuning_plots','dir'))
    if(strcmp(experiment,'BICEP2')) % check if it's BICEP2 or Keck
        unix('ln -s /n/bicepfs1/www/bicep2/tuning_plots/ tuning_plots');
    else
        unix('ln -s /n/bicepfs2/keck/pipeline/tuning_plots/ tuning_plots');
    end
end


% get the newest un-processed tunings
if(strcmp(experiment,'BICEP2'))
    newtune=get_newtuning; % actually get the tunings.  The rest just displays which tunings to process
    if(~isempty(newtune))
    disp('processing tunings for the following date(s):');
        for i=1:length(newtune)
            disp(newtune{i});
        end
    else
        disp('tunings are all up to date.  Nothing to see here, move along.');
    return
    end
else % for Keck
    newtune=get_newtuning_keck; % gets the tunings. code below displays which tunings to process
    if(~isempty(newtune))
    disp('processing tunings for the following date(s):');
        for jj=1:size(newtune,1)
            rxtune=newtune{jj};
            disp(['rx' int2str(jj-1) ':']);
            if(~isempty(rxtune))
                for i=1:length(rxtune)
                    disp(rxtune{i});
                end
            else
                disp('no new tunings');
            end
        end
    else
        disp('tunings are all up to date.  Nothing to see here, move along.');
    return
    end
end

% run squid tuning plotting code
farmatune(newtune,experiment);

end


function tune=get_newtuning
% this one is for BICEP2

% find processed tunings
donetune=dir('tuning_plots');
% only interested in the last tuning ran
lasttune=datenum(donetune(end-1).name(1:6),'yymmdd');
% get all tunings
alltune=dir('tuning');
% find only tunings, not folders, by looking for the year (there's probably a
% better way to do this--maybe arrayfun)
y09=cellfun(@(x) strfind(x(1:min([2 end])),'09'),{alltune(:).name},'UniformOutput',false);
i09=find(~cellfun(@isempty,y09));
y10=cellfun(@(x) strfind(x(1:min([2 end])),'10'),{alltune(:).name},'UniformOutput',false);
i10=find(~cellfun(@isempty,y10));
y11=cellfun(@(x) strfind(x(1:min([2 end])),'11'),{alltune(:).name},'UniformOutput',false);
i11=find(~cellfun(@isempty,y11));
y12=cellfun(@(x) strfind(x(1:min([2 end])),'12'),{alltune(:).name},'UniformOutput',false);
i12=find(~cellfun(@isempty,y12));

idx=unique([i09 i10 i11 i12]);

% get time of all tunings
alltime=cellfun(@(x) datenum(x(1:6),'yymmdd'),{alltune(idx).name},'UniformOutput',false);
% find tunings more recent than lasttune
newtime=alltime(cellfun(@(x) x>lasttune,alltime));

% convert date back to a string, strip repeats
tune=unique(cellfun(@(x) datestr(x,'yymmdd'),newtime,'UniformOutput',false));
if(~iscell(tune))
    tune={tune};
end
end



function tune=get_newtuning_keck

% do for each receiver
for ii=1:5
  %find processed tunings for this Rx
  tunepath=dir('tuning_plots');
  tunepath=tunepath(find(cellfun(@(x) x==1,{tunepath(:).isdir})));
  donetime_all=[];
  for i=1:length(tunepath)
    donetune=dir(['tuning_plots/' tunepath(i).name '/*rx' int2str(ii-1) '_ssa.png']);
    if strcmp(tunepath(i).name,'.')
      donetime=cellfun(@(x) datenum(x(1:9),'yymmdd_HH'),{donetune.name},'UniformOutput',false);
    else
      donetime=cellfun(@(x) datenum([tunepath(i).name x(1:5)],'yymmdd_HH'),{donetune.name},'UniformOutput',false);
    end
    donetime_all=[donetime_all cell2mat(donetime)]; %change to array from cell
  end
  % only interested in the last tuning ran
  %lasttune=get_lasttune_keck(donetune,ii-1);
  % get all tunings
  alltune=dir(['tuning/mce' num2str(ii-1) '/*.experiment.cfg']);
  % then filter out the ones which aren't complete (i.e. will fail during
  % analyze_tuning) by filtering out directories that don't have an
  % experiment.cfg file within them.
  tunemask=arrayfun(@(x) ~isempty(dir(['tuning/mce' num2str(ii-1) '/' strrep(x.name,'.ex','/ex')])), alltune);
  % find only tunings, not folders, by looking for the year (there's probably a
  % better way to do this--maybe arrayfun)
  y11=cellfun(@(x) strfind(x(1:min([2 end])),'11'),{alltune(:).name},'UniformOutput',false);
  i11=find(~cellfun(@isempty,y11));
  y12=cellfun(@(x) strfind(x(1:min([2 end])),'12'),{alltune(:).name},'UniformOutput',false);
  i12=find(~cellfun(@isempty,y12));
  y13=cellfun(@(x) strfind(x(1:min([2 end])),'13'),{alltune(:).name},'UniformOutput',false);
  i13=find(~cellfun(@isempty,y13));
  y14=cellfun(@(x) strfind(x(1:min([2 end])),'14'),{alltune(:).name},'UniformOutput',false);
  i14=find(~cellfun(@isempty,y14));
  y15=cellfun(@(x) strfind(x(1:min([2 end])),'15'),{alltune(:).name},'UniformOutput',false);
  i15=find(~cellfun(@isempty,y15));
  y16=cellfun(@(x) strfind(x(1:min([2 end])),'16'),{alltune(:).name},'UniformOutput',false);
  i16=find(~cellfun(@isempty,y16));

  idx=unique([i16]);

  % get time of all tunings
  alltime=cellfun(@(x) datenum(x(1:9),'yymmdd_HH'),{alltune(idx).name},'UniformOutput',false);
  alltime=cell2mat(alltime);
  % find tunings more recent than lasttune
  newtime=alltime(~ismember(alltime,donetime_all));
  newtime=num2cell(newtime); %convert back to cell

  % convert date back to a string, strip repeats
  tune{ii,:}=unique(cellfun(@(x) datestr(x,'yymmdd_HH'),newtime,'UniformOutput',false));
end

end

function lasttunedat=get_lasttune_keck(donetune,rx)
% find all plots for given rx
irx=strfind({donetune(:).name},['rx' num2str(rx)]);
ind=find(~cellfun(@isempty,irx));

% strip away the 'all' plots for just the dated plots
rxtune={donetune(ind).name};

alldone=rxtune(cellfun(@isempty,strfind(rxtune,'all')));

lasttune=alldone{end};

lasttunedat=datenum(lasttune(1:6),'yymmdd');
end


function farmatune(newtune,experiment)
QUEUE = 'serial_requeue';
MEM = 2000; % Increased after farmit() restricts VM size with ulimit
LICENSE = 'bicepfs1:17';
MAXTIME = 1440; % Specify this in case the maintenacne day is nearby

% for each receiver, farms out each tuning
if(strcmp(experiment,'BICEP2')) 
  newtune={newtune};
end
for xx=1:size(newtune,1)
  rxtune=newtune{xx};
  for ii=1:length(rxtune)
    tune=rxtune{ii};
    farmit('farmfiles',['plot_tuning({''' tune '''},[' int2str(xx-1) ']);'],...
        'queue',QUEUE, 'jobname','tuning', 'mem',MEM, 'maxtime',MAXTIME, ...
        'license',LICENSE, 'account','bicepdata_group');
  end
end

end

