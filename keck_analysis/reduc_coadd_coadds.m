function reduc_coadd_coadds(input_acs,sernum,daughter,clean_inputs,ukpv,coaddrx)
%  function reduc_coadd_coadds(input_acs,sernum,daughter,clean_inputs,ukpv,coaddrx)
%  
%  Take many input_acs and combine them into an overall ac.
%  and saves a file in maps/ following the usual naming rules
%
%  input_acs = cell array of the names of files containing coadded acs
%              or a single string which may contain wildcards
%
%  sernum = output serial number
%
%  daughter = output daughter name
%
%  clean_inputs = (true), remove the input_acs files
%                 upon successfully saving the data product
%                 default = false
%           
%  ukpv = if given, applies a calibration before adding
%
%  i.e. reduc_coadd_coadds('maps/1450/real_as20*jack?.mat','1450real','a',0);


if ~exist('clean_inputs','var')
  clean_inputs=false;
end

if ~exist('sernum','var') || ~exist('daughter','var')
  error('You must input ''sernum'' and ''daughter'' to give the ouput file a valid name.')
end

if ischar(input_acs)
  % There's only one filename, but we can still push it through.
  input_acs={input_acs};
  
  % dir function only knows * wildcard
  [s,d]=system_safe(sprintf('ls -1 %s',input_acs{1}));
  % for some bizarre reason d now contains non-printing nonsense
  d=strip_nonsense(d);
  input_acs=strread(d,'%s');
end

if exist('ukpv','var') && numel(ukpv)~=numel(input_acs)
  error('ukpv is not the same size as the input maps')
end

for jj=1:numel(input_acs)
  if jj==1
    % Load the first file to use as baseline.
    disp(['Loading ',input_acs{1},'...']);
    load(input_acs{1})
    if ~exist('ac','var')
      error('Files must be in ac format.')
    end
    if exist('ukpv','var')
      ac=cal_coadd_ac(ac,ukpv(jj));
     if exist('matrixc', 'var')
       matrixc=cal_coadd_ac(matrixc,ukpv(jj));
     end
    end
    if isfield(coaddopt,'rx')
      % Temporarily convert to cell for processing. The field will either be
      % restored (for pair-subset coadds) or removed (coadding over receivers)
      % before being saved out.
      if ~iscell(coaddopt.rx)
        coaddopt.rx = {coaddopt.rx};
      end
    end
  else
    % Load the next file.
    disp(['Loading ',input_acs{jj},'...']);
    a=load(input_acs{jj});
    if ~isfield(a,'ac')
      error('Files must be in ac format.')
    end
    % Make sure they have exactly the same m, no exceptions.
    if ~isequal(m,a.m)
      error('Input acs have incompatible ''m'' structures.')
    end
    % Make sure the important coaddopt are the same.
    if compare_coaddopt(coaddopt,a.coaddopt)
      error('Input acs have incompatible coaddopt.') 
    end

    % Overlapping tag lists are OK in certain circumstances:
    % 1) In matrix pipeline may need to coadd over overlapping tags for:
    %    a) coadding pair-subsets together
    %    b) receivers stored in separate files and need to be
    %       combined into single structure (i.e. per-frequency)
    % 2) Coadding BICEP2 and Keck
    if (isfield(coaddopt, 'pair_subset') && any(coaddopt.pair_subset)) ...
    || (isfield(coaddopt, 'rx') && isfield(a.coaddopt, 'rx') ...
        && ~isempty(coaddopt.rx) && ~isempty(a.coaddopt.rx) ...
        && iscell(coaddopt.rx) && coaddopt.rx{1}~=a.coaddopt.rx) ...
    || numel(unique(coaddopt.p.rx))~=numel(unique(a.coaddopt.p.rx))

      % Clear new tags so we don't keep concatenating the list.
      a.coaddopt.tags = [];

    % Otherwise, make sure the tags are non-overlapping
    else
      if numel([coaddopt.tags,a.coaddopt.tags])~=numel(unique([coaddopt.tags,a.coaddopt.tags])) 
      	error('Tag lists are overlapping.  Combining these acs will result in double counting.')
      end

      if(any(coaddopt.deproj))
        % Make sure the deprojection occured over the entire timescales.
        deproj_error=check_deproj_timescale(coaddopt,a.coaddopt);
        if deproj_error
          error(deproj_error)
        end
      end
    end

    % calibrate before adding
    if exist('ukpv','var')
      a.ac=cal_coadd_ac(a.ac,ukpv(jj));
     if isfield(a, 'matrixc')
       a.matrixc=cal_coadd_ac(a.matrixc,ukpv(jj));
     end
    end
        
    disp('Everything seems OK.  Coadding...')
    
    % Do the coadd for each jacktype and coaddtype.
    for jt=1:size(ac,2)
      for ct=1:size(ac,1)
        ac(ct,jt)=addac(ac(ct,jt),a.ac(ct,jt));
      end
    end
    
    %if we are coadding a matrix...
    if(exist('matrixc','var'))
      for jt=1:size(ac,2)
        for ct=1:size(ac,1)
          matrixc(ct,jt)=addac(matrixc(ct,jt),a.matrixc(ct,jt));
        end
      end
    end
    %combine coaddopt if not a pair subset
    if ~(isfield(coaddopt, 'pair_subset') && any(coaddopt.pair_subset))
      % Combine coaddopt.
      coaddopt=combine_coaddopt(coaddopt,a.coaddopt);
    end
    % Keep track of coadded receivers. Pair-subsets will all be same receiver
    % and needs to be maintained during naming while coadding over receivers
    % should remove the designation.
    if isfield(coaddopt,'rx') && coaddopt.rx{1}~=a.coaddopt.rx
      coaddopt.rx{end+1} = a.coaddopt.rx;
    end

    clear a;
  end
end

% allow to combine rxs
if exist('coaddrx','var')
  switch coaddrx
   case 2
    ac = coadd_ac_overfreq(ac,coaddopt);
   case 1
    ac = coadd_ac_overrx(ac);
    coaddopt.coaddtype=0;
   case 0
    ac = coadd_ac_intorx(ac,coaddopt);    
  end
end    

% Update the sernum and daughter.
coaddopt.sernum=sernum;
coaddopt.daughter=daughter;
if exist('ukpv','var')
  coaddopt.ukpv_applied=ukpv;
end

fname=get_map_filename(coaddopt);
if(exist('matrixc','var'))
  % Fix naming for pair subsets
  if isfield(coaddopt, 'pair_subset') && any(coaddopt.pair_subset)
    coaddopt.pair_subset=[];
  end
  if isfield(coaddopt, 'rx')
    % For single receiver, restore to non-cell value
    if length(coaddopt.rx) == 1
      coaddopt.rx = coaddopt.rx{1};
    % For multiple receivers, remove the designation.
    else
      coaddopt = rmfield(coaddopt,'rx');
    end
  end
  fname=get_matrix_filename(coaddopt);
end
disp(['Saving output file: ',fname])

% Save the file according to the naming convention.
coadddir=fileparts(fname);
if ~isdir(coadddir)
  mkdir(coadddir)
end
if(exist('matrixc','var'))
  saveandtest(fname,'matrixc', 'ac','m','coaddopt','-v7.3');
else
  saveandtest(fname,'ac','m','coaddopt','-v7.3');
end
setpermissions(fname);
% saveandtest will result in an error if the saving 
% has failed, and the next line will never be executed
% in this case:
if clean_inputs
  display('Removing input_acs...')
  for jj=1:numel(input_acs)
    system(['rm ',input_acs{jj}])
  end
end


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function compare_coaddopt_out=compare_coaddopt(co1,co2)

% Compare the important fields of co1 to the important fields of co2.
% Output true for any disagreement.

compare_coaddopt_out=...
  ~strcmp(co1.filt,co2.filt) ...
  || ...
  co1.gs~=co2.gs ...
  || ...
  co1.weight~=co2.weight ...
  || ...
  co1.jacktype~=co2.jacktype ...
  || ...
  co1.coaddtype~=co2.coaddtype ...
  || ...
  ~strcmp(co1.proj,co2.proj) ...
  || ...
  ~isequal(co1.cut,co2.cut) ...
  || ...
  ~isequal(co1.deproj,co2.deproj) ...
  || ...
  ~strcmp(co1.deproj_timescale,co2.deproj_timescale) ...
  ;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function check_deproj_timescale_out=check_deproj_timescale(co1,co2)

% We have already checked that the values of deproj_timescale are
% consistent.  It is possible, however, for only part of the relevant tags
% to have been deprojected given that timescale.

switch co1.deproj_timescale
  case 'by_scanset'
    % We are safe.
    check_deproj_timescale_out=false;
  case 'by_phase'
    ph1=taglist_to_phaselist(co1.tags);
    ph2=taglist_to_phaselist(co2.tags);
    % Make sure the phases are non-overlapping.
    if numel([ph1.name,ph2.name])~=numel(unique([ph1.name,ph2.name]))
      check_deproj_timescale_out='The deprojection timescale is by_phase, but the phase lists are overlapping.';
    else
      % We are safe.
      check_deproj_timescale_out=false;
    end
  case 'by_raster'
    % Not really necessary to convert to phases, but the for loops are
    % shorter this way.
    ph1=taglist_to_phaselist(co1.tags);
    ph2=taglist_to_phaselist(co2.tags);
    [day1,phase1,dummy,dk1]=parse_tag(ph1);
    [day2,phase2,dummy,dk2]=parse_tag(ph2);
    counter=0;
    for jj=1:length(day1)
      for kk=1:length(day2)
        if ...
          day(jj)==day(kk) && dk1(jj)==dk2(kk) ...
            && ...
            ((any(phase1{jj}=='BC') && any(phase2{kk}=='BC')) ...
            || (any(phase1{jj}=='EFGHI') && any(phase2{kk}=='EFGHI')))
          counter=counter+1;
        end
      end
    end
    if counter>0
      check_deproj_timescale_out='The deprojection timescale is by_raster, but the raster lists are overlapping.';
    else
      check_deproj_timescale_out=false;
    end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function coaddopt_out=combine_coaddopt(co1,co2)

% Combine fields c, hsmax, whist, devhist, and tags of different coaddopt.
% Notation for coaddopts1/2 vs. cuts1/2 is a little awkward.
if isfield(co1,'c')
  if isfield(co2,'c')
    if isfield(co1.c,'cp')
      try
        co1.c.cp=structcat(1,[co1.c.cp,co2.c.cp]);
      catch
        co1.c=rmfield(co1.c,'cp');
        warning('Unable to combine c.cp.  Some cut information is missing from a coaddopt.')
      end
    end
    if isfield(co1.c,'c1')
      try
        co1.c.c1=structcat(1,[co1.c.c1,co2.c.c1]);
      catch
        co1.c=rmfield(co1.c,'c1');
        warning('Unable to combine c.c1.  Some cut information is missing from a coaddopt.')
      end
    end
    if isfield(co1.c,'c2')
      try
        co1.c.c2=structcat(1,[co1.c.c2,co2.c.c2]);
      catch
        co1.c=rmfield(co1.c,'c2');
        warning('Unable to combine c.c2.  Some cut information is missing from a coaddopt.')
      end
    end
  else
    co1=rmfield(co1,'c');
    warning('Unable to combine any cuts.  Cut information is missing from a coaddopt.')
  end
end

% Histograms
if isfield(co1,'hsmax')
  if isfield(co2,'hsmax')
    co1.hsmax=[co1.hsmax,co2.hsmax];
  else
    co1=rmfield(co1,'hsmax');
    warning('Unable to combine hsmax.  Some histogram information is missing from a coaddopt.')
  end
end
if isfield(co1,'whist')
  if isfield(co2,'whist')
    co1.whist.sum.n=co1.whist.sum.n+co2.whist.sum.n;
    co1.whist.dif.n=co1.whist.dif.n+co2.whist.dif.n;
  else
    co1=rmfield(co1,'whist');
    warning('Unable to combine whist.  Some histogram information is missing from a coaddopt.')
  end
end
if isfield(co1,'devhist')
  if isfield(co2,'devhist')
    co1.devhist.sum.n=co1.devhist.sum.n+co2.devhist.sum.n;
    co1.devhist.dif.n=co1.devhist.dif.n+co2.devhist.dif.n;
  else
    co1=rmfield(co1,'devhist');
    warning('Unable to combine devhist.  Some histogram information is missing from a coaddopt.')
  end
end

% Traj
if isfield(co1,'traj')
  if isfield(co2,'traj')
    co1.traj=structcat(1,[co1.traj,co2.traj]);
  else
    co1=rmfield(co1,'traj');
    warning('Unable to combine traj.  Some traj information is missing from a coaddopt.')
  end
end

% Jackmask
if isfield(co1,'jackmask')
  if isfield(co2,'jackmask')
    co1.jackmask{1}=[co1.jackmask{1};co2.jackmask{1}];
    co1.jackmask{2}=[co1.jackmask{2};co2.jackmask{2}];
  else
    co1=rmfield(co1,'jackmask');
    warning('Unable to combine jackmask.  Some jackmask information is missing from a coaddopt.')
  end
end

% Deprojection coefficients and weights
if isfield(co1,'b')
  if isfield(co2,'b')
    co1.bi=cat(1,co1.bi,co2.bi);
    co1.b=cat(4,co1.b,co2.b);
    co1.bw=cat(3,co1.bw,co2.bw);
  else
    co1=rmfield(co1,'bi');
    co1=rmfield(co1,'b');
    co1=rmfield(co1,'bw');
    warning('Unable to combine b and bw. Some deprojection coefficients will be missing.');
  end
end

% Tags
co1.tags=[co1.tags,co2.tags];

coaddopt_out=co1;

return
