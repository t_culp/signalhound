function reduc_plotperphasemaps(phase_tag)
% reduc_makeperphasemaps(phase_tag)
% 
% generates scanset browser per-phase plots and save hard copies to reducplots/
%
% i.e. tags=get_tags('all','new')
%      ph=taglist_to_phaselist(tags);
%      reduc_makeperphasemaps(ph.tags{1})
%      reduc_plotperphasemaps(ph.name{1})
%
% CDS 20110420

% get run information
[p,ind]=get_array_info(phase_tag);
rx=unique(p.rx);
freqs = unique(p.band(ind.la));

% plot all receivers coadded together
plot_perphasemaps(phase_tag, 1, [], false);

% if there are multiple frequencies, group by frequency for plotting as well
if numel(freqs) > 1
  for ff=1:numel(freqs)
    % Choose rx's which are this frequency
    rgla = sprintf('rgl%03da', freqs(ff));
    rxfreq = unique(p.rx(ind.(rgla)));
    % Plot for a single frequency
    plot_perphasemaps(phase_tag, 1, rxfreq, true);
  end
end

% Also plot each receiver individually.
if numel(rx) > 1
  for j=1:numel(rx)
    plot_perphasemaps(phase_tag, 1, rx(j), false);
  end
end

% Plot all dark maps
plot_perphasedarkmaps(phase_tag,1, [], false);

% if there are multiple frequencies, group by frequency to plot dark maps
if numel(freqs) > 1
  for ff=1:numel(freqs)
    % Choose rx's which are this frequency
    rgla = sprintf('rgl%03da', freqs(ff));
    rxfreq = unique(p.rx(ind.(rgla)));
    % Plot for a single frequency
    plot_perphasedarkmaps(phase_tag, 1, rxfreq, true);
  end
end
% and again, plot per-receiver dark maps.
if numel(rx) > 1
  for j=1:numel(rx)
    plot_perphasedarkmaps(phase_tag, 1, rx(j), false);
  end
end
  
return
