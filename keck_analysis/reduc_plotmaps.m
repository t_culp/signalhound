function reduc_plotmaps(tag,filttype)
% reduc_plotmaps(tag,filttype)
%
% Plot all per channel maps for one or more days
%
% reduc_plotmaps({'050517','050518'},'p0')
% reduc_plotmaps('050517','p0')

if(~exist('filttype','var'))
  filttype=[];
end

% get stack of days
if(iscell(tag))
  % get stack
  [mapu,mapd,tag]=maps_read_stack(tag);
  % combine days
  mapu=maps_combine_stack(mapu);
  mapd=maps_combine_stack(mapd);
  
  tag=tag{1};
else
  load(sprintf('data/%s_maps_filt%s',tag,filttype));
end

close all
%for i=[1,3]
for i=1:numel(mapu)
  figure(i);
  maps_plot(mapu{i},p,ind,tag,filttype)
end

for i=1:numel(mapd)
  figure(i+numel(mapu));
  maps_plot(mapd{i},p,ind,tag,filttype)
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function maps_plot(map,p,ind,tag,filttype)

%map=mapa{1};
%map.rdoff.map=[mapa{2}.rdoff.map,mapa{1}.rdoff.map];
%map.rdoff.x_tic=[mapa{2}.rdoff.x_tic,mapa{1}.rdoff.x_tic];

q=rotarray(p,map.dk);

limd=[-6e-3,6e-3];
lims=limd*2.5;

%limd=lims;

if(0)
% regress a map against b
for i=ind.rgla
  ma=map.rdoff.map(:,:,i);
  mb=map.rdoff.map(:,:,i+1);
  da=ma(:); db=mb(:);
  X=[ones(size(db)),db];
  b=regress(da,X)
  db=X*(b);
  mb=reshape(db,size(mb));
  
  map.rdoff.map(:,:,i)=ma+mb;
  map.rdoff.map(:,:,i+1)=ma-mb;
  
end
for i=1:2:length(p.channel_name)
  p.channel_name{i}=[p.channel_name{i}(1:end-1),'sum'];
  p.channel_name{i+1}=[p.channel_name{i+1}(1:end-1),'diff'];
end
end

setwinsize(gcf,1400,1000); clf

% plot the light channels
for i=ind.rgla
  x=(i-1)/2;
    
  axes('position',[-q.ra_off_dos(i)/1.9+0.47-0.034,q.dec_off(i)/2.3+0.5,0.065,0.075]);
  m=map.rdoff.map(:,:,i);
  imagesc(map.rdoff.x_tic,map.rdoff.y_tic,m);
  caxis(lims);
  axis xy; axis tight; title(sprintf('%s %.4f',p.channel_name{i},nanstd(m(:))));
  set(gca,'XDir','reverse');

  axes('position',[-q.ra_off_dos(i)/1.9+0.47+0.034,q.dec_off(i)/2.3+0.5,0.065,0.075]);
  m=map.rdoff.map(:,:,i+1);
  imagesc(map.rdoff.x_tic,map.rdoff.y_tic,m);
  caxis(limd);
  axis xy; axis tight;
  set(gca,'XDir','reverse');
  set(gca,'YTick',[]);
  
  if(p.grid_phi(i)==-59)
    set(gca,'XColor','red'); set(gca,'YColor','red');
    title(sprintf('%.4f',nanstd(m(:))),'Color','red');
  else
    set(gca,'XColor','blue'); set(gca,'YColor','blue');
    title(sprintf('%.4f',nanstd(m(:))),'Color','blue');
  end
end

% plot the dark channels
for i=1:length(ind.gd)
  ch=ind.gd(i);
  axes('position',[i*0.1-0.07,0.02,0.065,0.075]);
  m=map.rdoff.map(:,:,ch);
  imagesc(map.rdoff.x_tic,map.rdoff.y_tic,m);
  caxis(lims);
  axis xy; axis tight; title(sprintf('%s %.4f',p.channel_name{ch},nanstd(m(:))));
  set(gca,'XDir','reverse');  
end

map.src=strrep(map.src,' ','\_');

gtitle(sprintf('%s src=%s dk=%.0f',strrep(tag,'_','\_'),map.src,map.dk));

text(0.8,0.2,sprintf('color range \\pm%.4f sum and \\pm%.4f diff',lims(2),limd(2)));

if(numel(strfind(tag,'through'))>0)
  mkgif(sprintf('reducplots/all/maps_%s_dk%.0f.gif',map.src,map.dk));
end
if(numel(strfind(tag,'+'))==0)
  %mkgif(sprintf('reducplots/%s/maps_filt%s_%s_dk%.0f.gif',tag,filttype,map.src,map.dk));
  mkgif(sprintf('reducplots/%s/maps_%s_dk%.0f.gif',tag,map.src,map.dk));
end

return
