function bpwf=reduc_bpwf(mapn,bins,boost,mixrange,nreal,pl,howtomask)
% bpwf=reduc_bpwf(mapn,bins,boost,mixrange,nreal,pl,howtomask)
%
% Calculate the bandpower window functions corresponding to
% the 1/var masks in mapn
%
% NB 1: for B2xKeck sims the combined maps are never stored on disk -
% so this prog is now called from reduc_makeaps (upon request) with
% the input map structure in mapn. In this mode only the default
% options are possible - but this is OK as we should use Challinor
% and Chong always anyway.
% NB 2: This prog knows nothing about Kendrick pureB algorithm used
% in reduc_makeaps - but it should as this presumably modifies the
% bpwf.
%
%
% Before we FT we mult by 1/var mask. Mult in real space is
% convolution in Fourier space. So we are convolving the FT by the FT
% of the mask.
%
% Here we take each ps annuli in turn,
% convolve with the mask FT, and take the power spectrum as normal.
% This traces out the band power window functions which measure how
% much theory spectrum power couples to each experimental bandpower.
% Instead of doing it for each ell we use annuli of approx delta ell
% 20 width extending out to ell of 2750 to trace out the shape of the
% last bandpower.
%
% When working with the same f plane resolution which we use normally
% the bpwf come out looking "rattey". It turns out that this makes
% no practical difference when they are used in reduc_final but it
% doesn't look nice on plots. So the code below can increase the
% resolution by padding out the mask by a "boost" factor.
%
% The size of the convolving kernel determines the maximum ell range
% over which the bpwf can come out non-zero. Since these BPWF get
% "published" as part of the data release we want to calculate the
% mixing over the widest possible range - but it makes the
% convolutions take ages...
%
% It turns out that the predicted mixing from E->B is larger when
% using an annulus of ones than when using an annulus of random
% numbers and averaging the result over several iterations. Therefore
% the code below now does the later by default (taking longer).
% nreal = 0; Computes using the Challinor & Chon approximation which is mathematically
%            equivalent to an infinite number of random realizations
%       = 1; uses old annulus of ones
%       > 1; uses specified number of realizations of annulus of random numbers
%
% The BPWF calc here do not know about the filter/beam supression -
% that is accounted for in reduc_final by an iterated calculation. 
%
% This code has been "upgraded" to allow for non-overlap of the
% various map areas as in reduc_makeaps.
% 
% e.g. 
%
% To start with run something reasonable like
% reduc_bpwf('sim34_filtp3_weight2_jack0_fd',[],2,500)
% and set a sym link like
% ln -s sim34_filtp3_weight2_jack0_fd_bpwf_quad_norm_2_500.mat sim34_filtp3_weight2_jack0_fd_bpwf_bicep_norm.mat
%
% Then increase boost and mixrange - ultimately want
% reduc_bpwf('sim32_filtp3_weight2_jack0_fd',[],4,2000)
% but that will take forever

if(~exist('bins','var'))
  bins=[];
end
if(~exist('boost','var'))
  boost=[];
end
if(~exist('mixrange','var'))
  mixrange=[];
end
if(~exist('nreal','var'))
  nreal=[];
end
if(~exist('pl','var'))
  pl=[];
end
if(~exist('howtomask','var'))
  howtomask='';
end

if(isempty(bins))
  bins='bicep_norm';
end
if(isempty(boost))
  boost=1;
end
if(isempty(mixrange))
  mixrange=500;
end
if(isempty(nreal))
  nreal=0;
end
if(isempty(pl))
  pl=0;
end

if(ischar(mapn))
  % if mapn is character var load the map of this name
  load(sprintf('maps/%s',mapn));
  if ~exist('map','var') && exist('ac','var')
    map=make_map(ac,m,coaddopt);
  end
else
  % unpack map and m from input struct
  map=mapn.map;
  m=mapn.m;
end

% smooth the var maps to reduce mode mixing
if(1)
  disp('smooth var maps')
  tic
  map=smooth_varmaps(m,map);
  toc
end

if(0)
% Inject gaussian shaped lumps into variance map to mask point
% sources
% - these will contribute to non-ideality of bpwf
map=mask_pntsrc(m,map,get_src([],mapopt));
end

% pad the maps (making them square)
disp('pad')
tic
p=2^nextpow2(max([m.nx,m.ny]));
% Increase the span to get higher resolution in F space so that
% convolution kernel is well resolved.
% I am not sure this is really kosher as it is deviating from "what we
% actually do" when measuring power spectrum from map so although the
% bpwf may come out prettier they may not actually be the functions we
% want...
p=p*boost;
[m,map]=pad_map(m,map,p);
toc

% Calculate the weight maps at each freq
for i=1:numel(map)
  % inverse variance map for T and P
  map(i).Tw=1./map(i).Tvar;
  if(isfield(map,'Q'))
    map(i).Pw=1./((map(i).Qvar+map(i).Uvar)/2);
  end
end

% calc axis data
ad=calc_ad2([m.xdos,m.ydos],[m.nx,m.ny]);

% plot the image space masks
if(pl==1)
  figure(1); clf
  setwinsize(gcf,700,700)
  %subplot(2,2,1); plot_map(ad,map(1).Tw); title('100GHz T mask')
  subplot(2,1,1); plot_map(ad,map.Tw); title('150GHz T mask')
  %subplot(2,2,3); plot_map(ad,map(1).Pw); title('100GHz P mask');
  subplot(2,1,2); plot_map(ad,map.Pw); title('150GHz P mask');
  colormap jet
  drawnow
end

% pre calc the fts of the masks to speed up - these are cut down to
% to mixrange radius to speed up the convolve
disp('Calc mask fts')
tic
for j=1:size(map,1)
  % TT
  % get the weight mask
  w=map(j).Tw;
  % take its ft
  mft(j).TT=get_ft(ad,w,mixrange);
  % PP
  w=map(j).Pw;
  mft(j).PP=get_ft(ad,w,mixrange);
  % TP
  w=gmean(map(j).Tw,map(j).Pw); 
  mft(j).TP=get_ft(ad,w,mixrange);
  % PT
  w=gmean(map(j).Pw,map(j).Tw); 
  mft(j).PT=get_ft(ad,w,mixrange);
end
% take cross spectra if 
% multiple bands or maps exist
offset = length(mft);
if size(map,1)>1
  n = 1;
  for j=1:size(map,1)-1
    for c=j+1:size(map,1)
      % TT
      w=gmean(map(j).Tw,map(c).Tw);
      mft(offset+n).TT=get_ft(ad,w,mixrange);
      % PP
      w=gmean(map(j).Pw,map(c).Pw);
      mft(offset+n).PP=get_ft(ad,w,mixrange);    
      % TP
      w=gmean(map(j).Tw,map(c).Pw); 
      mft(offset+n).TP=get_ft(ad,w,mixrange);
      % PT
      w=gmean(map(j).Pw,map(c).Tw); 
      mft(offset+n).PT=get_ft(ad,w,mixrange);
      n=n+1;
    end
  end
end
toc

clear map

% plot the mask ft conv kernels
if(pl==1)
  figure(2); clf
  setwinsize(gcf,800,800)
  for j=1:length(mft)
    subplot(4,3,0+j); imagesc(abs(mft(j).TT)); ylabel('TT')
    subplot(4,3,3+j); imagesc(abs(mft(j).PP)); ylabel('PP')
    subplot(4,3,6+j); imagesc(abs(mft(j).TP)); ylabel('TP')
    if(j==3)
      subplot(4,3,9+j); imagesc(abs(mft(j).PT)); ylabel('PT')
    end
  end
  drawnow
end

% for each fine band of sky power we will calc the contribution to
% each of the experimental bins
be=get_bins(bins);
be=1.5:7:(max(be)+200);
bc=mean([be(1:end-1);be(2:end)]);

% Re-calc ad dropping the image space resolution - and hence the f
% space span to avoid wasting time.
% The highest ell at which we will inject power is 2750 so we don't
% need to extend beyond that.
n=2*round((580/(2*pi))./ad.del_u(1))
ad2=calc_ad2(ad.Field_size_deg,[n,n]);
ad2.u_val{1}(end)*2*pi;

% setup the l vals and u,v vals for QU<->EB below
ad2.l_r=ad2.u_r*2*pi;
[u,v]=meshgrid(ad2.u_val{1},ad2.u_val{2});

% get the f plane mode mask
%fpmask=prepare_fplane_mask(ad2,howtomask);

% the mixing occurs in C_l space even though we plot in l(l+1)C_l
% space - setup the fourier modes corresponding to flat in l(l+1)C_l -
% we will go back to l(l+1)C_l space in calcspec below
z=ones(ad2.N_pix(2),ad2.N_pix(1));
z=z.*sqrt(1./(ad2.l_r.*(ad2.l_r+1)));

% kludge to allow to not keep recalc index pointers in
% calcspec below - make sure it's cleared here from any
% previous call to calcspec
clear global calcspec_ind

% allow multiple realizations
for r=1:max([nreal,1])

  switch nreal
   case 0
    rnd=ones(ad2.N_pix);
    disp('using Challinor & Chon')
   case 1
    rnd=ones(ad2.N_pix);
    disp('using annulus of ones')
   otherwise
    % generate sets of random complex numbers with pure real FT
    % (this func does not know how to do non square arrays but code
    % above ensures ad is square)
    rnd=randn_ft_of_real(ad2.N_pix(2));
    disp('using random annulus')
  end
  
  % for each bin
  for i=1:length(be)-1
  %for i=30
  
    [r i]
  
    tic
  
    % construct the annulus
    annulus=z;
    annulus(ad2.l_r<be(i)|ad2.l_r>be(i+1))=0;
    
    % fold in random numbers - use same for T/E/B so will be 100%
    % cross-correlated
    annulus=annulus.*rnd;
    
    % construct the Q/U annuli which correspond to pure E
    [annulusEQ,annulusEU]=eb2qu(u,v,annulus,0,'iau');
    % construct the Q/U annuli which correspond to pure B
    [annulusBQ,annulusBU]=eb2qu(u,v,0,annulus,'iau');
    
    % calc 100/150GHz spectra
    for j=1:length(mft)
      
      switch nreal
       case 0  
        % compute using Challinor & Chon
        Tft=sqrt(conv2(annulus.^2,mft(j).TT.*conj(mft(j).TT),'same'));
        [l,aps(j,1).Cs_l(i,:,1,r)]=calcspec(ad2,Tft,Tft,[],bins);
        
        sz=size(annulus);
        add=calc_ad2(sz*m.pixsize,sz); 
        [uu,vv]=meshgrid(add.u_val{1},add.u_val{2});
        phi_l=atan2(vv,uu)-pi/2;
        
        %EE / BB
        costerm=conv2(annulus.^2.*cos(4*phi_l),mft(j).PP.*conj(mft(j).PP),'same');
        sinterm=conv2(annulus.^2.*sin(4*phi_l),mft(j).PP.*conj(mft(j).PP),'same');
        noterm=conv2(annulus.^2,mft(j).PP.*conj(mft(j).PP),'same');
        
        EEft=sqrt(cos(4*phi_l).*costerm + sin(4*phi_l).*sinterm + noterm);
        EBft=sqrt(noterm - cos(4*phi_l).*costerm - sin(4*phi_l).*sinterm);
        BEft=EBft;
        BBft=EEft;
        % EE from pure EE sky
        [l,aps(j,1).Cs_l(i,:,3,r)]=calcspec(ad2,EEft,EEft,[],bins);
        % BB from pure EE sky
        [l,aps(j,1).Cs_l(i,:,5,r)]=calcspec(ad2,EBft,EBft,[],bins);

        [l,aps(j,1).Cs_l(i,:,4,r)]=calcspec(ad2,BBft,BBft,[],bins);
        [l,aps(j,1).Cs_l(i,:,6,r)]=calcspec(ad2,BEft,BEft,[],bins);

        %TP 
        term1=conv2(annulus.^2.*cos(2*phi_l),mft(j).TP.*conj(mft(j).TP),'same');
        term2=conv2(annulus.^2.*sin(2*phi_l),mft(j).TP.*conj(mft(j).TP),'same');    
        TEft=sqrt(cos(2*phi_l).*term1 + sin(2*phi_l).*term2);      
        [l,aps(j,1).Cs_l(i,:,2,r)]=calcspec(ad2,TEft,TEft,[],bins);

        % PT for cross spectra
        if(j>offset)
          term1=conv2(annulus.^2.*cos(2*phi_l),mft(j).PT.*conj(mft(j).PT),'same');
          term2=conv2(annulus.^2.*sin(2*phi_l),mft(j).PT.*conj(mft(j).PT),'same');    
          ETft=sqrt(cos(2*phi_l).*term1 + sin(2*phi_l).*term2);      
          [l,aps(j,1).Cs_l(i,:,7,r)]=calcspec(ad2,ETft,ETft,[],bins);
        end

        % record the ell we have just calc at
        aps(j).l(i,1)=bc(i);

       otherwise        
        % TT
        % convolve onto the annulus
        Tft=conv2(annulus,mft(j).TT,'same');    
        [l,aps(j,1).Cs_l(i,:,1,r)]=calcspec(ad2,Tft,Tft,[],bins);
        
        % EE
        EQft=conv2(annulusEQ,mft(j).PP,'same');
        EUft=conv2(annulusEU,mft(j).PP,'same');
        [EEft,EBft]=qu2eb(u,v,EQft,EUft,[],[],'iau');
        % EE from pure EE sky
        [l,aps(j,1).Cs_l(i,:,3,r)]=calcspec(ad2,EEft,EEft,[],bins);
        % BB from pure EE sky
        [l,aps(j,1).Cs_l(i,:,5,r)]=calcspec(ad2,EBft,EBft,[],bins);
        
        % BB
        BQft=conv2(annulusBQ,mft(j).PP,'same');
        BUft=conv2(annulusBU,mft(j).PP,'same');
        [BEft,BBft]=qu2eb(u,v,BQft,BUft,[],[],'iau');
        [l,aps(j,1).Cs_l(i,:,4,r)]=calcspec(ad2,BBft,BBft,[],bins);
        [l,aps(j,1).Cs_l(i,:,6,r)]=calcspec(ad2,BEft,BEft,[],bins);
        
        % TP
        Tft=conv2(annulus,mft(j).TP,'same');
        EQft=conv2(annulusEQ,mft(j).TP,'same');
        EUft=conv2(annulusEU,mft(j).TP,'same');
        [EEft,EBft]=qu2eb(u,v,EQft,EUft,[],[],'iau');
        [l,aps(j,1).Cs_l(i,:,2,r)]=calcspec(ad2,Tft,EEft,[],bins);
        
        % PT
        if(j>offset)
          Tft=conv2(annulus,mft(j).PT,'same');
          EQft=conv2(annulusEQ,mft(j).PT,'same');
          EUft=conv2(annulusEU,mft(j).PT,'same');
          [EEft,EBft]=qu2eb(u,v,EQft,EUft,[],[],'iau');
          [l,aps(j,1).Cs_l(i,:,7,r)]=calcspec(ad2,EEft,Tft,[],bins);
        end
        
        % record the ell we have just calc at
        aps(j).l(i,1)=bc(i);

      end
      
    end
    
    % illustrate process
    if(i==30&pl==1&nreal>0)
      figure(3); clf
      setwinsize(gcf,700,900)
      
      l=2*pi*ad2.u_val{1};

      xl=be(i)*2; xl=[-xl,xl];
      
      subplot(4,2,1);
      imagesc(l,l,real(annulus)); axis image; xlim(xl); ylim(xl);
      ca=caxis; ca=max(abs(ca)); ca=[-ca,ca]; caxis(ca);
      title('E'); colorbar; xlabel('ell')
      subplot(4,2,2);
      imagesc(l,l,zeros(size(annulus))); caxis(ca); axis image; xlim(xl); ylim(xl);
      title('B'); colorbar; xlabel('ell')
    
      subplot(4,2,3);
      imagesc(l,l,real(annulusEQ)); caxis(ca); axis image; xlim(xl); ylim(xl);
      title('Q'); colorbar; xlabel('ell')
      subplot(4,2,4);
      imagesc(l,l,real(annulusEU)); caxis(ca); axis image; xlim(xl); ylim(xl);
      title('U'); colorbar; xlabel('ell')
      
      subplot(4,2,5);
      imagesc(l,l,real(EQft)); axis image; xlim(xl); ylim(xl);
      ca=caxis; ca=max(abs(ca)); ca=[-ca,ca]; caxis(ca);
      title('convolved Q'); colorbar; xlabel('ell')
      subplot(4,2,6);
      imagesc(l,l,real(EUft)); caxis(ca); axis image; xlim(xl); ylim(xl);
      title('convolved U'); colorbar; xlabel('ell')
      
      subplot(4,2,7);
      imagesc(l,l,real(EEft)); caxis(ca); axis image; xlim(xl); ylim(xl);
      title('resulting E'); colorbar; xlabel('ell')
      subplot(4,2,8);
      imagesc(l,l,real(EBft)); caxis(ca/10); axis image; xlim(xl); ylim(xl);
      title('resulting B'); colorbar; xlabel('ell')
      
      colormap jet
      
      drawnow    
    end
   
    toc
  end
end

% take mean over realizations
for i=1:length(aps)
  aps(i).Cs_l=mean(aps(i).Cs_l,4);
end

% clear ind array
clear global calcspec_ind

if(pl==1)
  figure(4); clf
  setwinsize(gcf,1200,1000)
  plot_bpwf(1,aps(1),'100GHz')
  plot_bpwf(2,aps(2),'150GHz')
  plot_bpwf(3,aps(3),'cross')
end

%save(sprintf('final/%s_bpwf_%s_%1d_%d_%d_raw',mapn,bins,boost,mixrange,nreal));

% spline interpolate to every ell
xx=2:581;
for j=1:length(aps)
  apss(j).l=xx';
  for i=1:size(aps(j).Cs_l,3)
    % get the x and y (spline works over last dimension
    x=aps(j).l';
    y=aps(j).Cs_l(:,:,i)';
    % prepend an imaginary zero point
    x=[0,x];
    y=[zeros(size(y,1),1),y];
    % force zero slope at end points
    y=[zeros(size(y,1),1),y,zeros(size(y,1),1)];
    % do the spline
    yy=spline(x,y,xx);
    % put the data back in the array
    apss(j).Cs_l(:,:,i)=yy';
  end
  % force (small) negative values to zero
  %apss(j).Cs_l(apss(j).Cs_l<0)=0;
end
aps=apss;

% normalize bpwf to unity sum
aps=norm_bpwf(aps);

if(pl==1)
  figure(5); clf
  setwinsize(gcf,1200,1000)
  plot_bpwf(1,aps(1),'100GHz')
  plot_bpwf(2,aps(2),'150GHz')
  plot_bpwf(3,aps(3),'cross')
end

bpwf=aps;

if(ischar(mapn))
  % old style is to save out here - new style passes back the bpwf
  % to save from reduc_makeaps
  
  % Make folder if it doesn't exist
  mapdir=fileparts(mapn);
  if ~exist(['final/' mapdir],'dir')
    system(['mkdir final/' mapdir]);
  end

  if(isempty(howtomask))
    save(sprintf('final/%s_bpwf_%s_%1d_%d_%d',mapn,bins,boost,mixrange,nreal),'bpwf');
  else
    save(sprintf('final/%s_e_bpwf_%s_%1d_%d_%d',mapn,bins,boost,mixrange,nreal),'bpwf');
  end
  
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_bpwf(n,aps,freq)

lab={'TT','TE','EE->EE','BB->BB','EE->BB','BB->EE'};
for i=1:6
  subplot_stack(6,3,(i-1)*3+n)
  
  plot(aps.l,aps.Cs_l(:,:,i));
  
  axis tight
  grid
  ylabel(lab{i})
  if(i==1)
    title(freq);
  end
  subplot_stack2(6,3,(i-1)*3+n)
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ft=get_ft(ad,map,mixrange)

% convert unmeasured region to zero
map(isnan(map))=0;

% take the ft
ft=i2f(ad,map);

% impose a circular non-zero extent so we have included mixing out to
% mixrange and no further
ind=ad.u_r*2*pi>mixrange;
ft(ind)=0;

% cut down to central region to speed up convolution
% this decides maximum extent of mode mixing
c=ad.N_pix/2+1;
ind=find(mixrange-abs(ad.u_val{1}*2*pi)>0);
ft=ft(ind(1):ind(end),ind(1):ind(end));

% applying normalization to the convolution kernel causes all sorts of
% problems and we re-normalize the bpwf later anyway

% This isn't right because it assumes random field
% Apply scale factor as in calc_map_fts
%sf=prod(ad.N_pix)/nansum(rvec(map.^2));
%ft=ft*sqrt(sf);

% Think this is correct normalization - for narrow bandpowers it
% produces bpwf which go above 1 - but for uniform plane of power
% would produce 1.
% Doesn't matter anyway as we re-normalize to bpwf sum of 1 as last
% step after spline interp to every ell.
%
% Doing this normalization actually plays havoc - due to small diff in
% Q/U masks the sidelobes differ and there is lots of area in the
% sidelobes. Thus normalizing the sum to one causes the central peak
% to differ strongly in amplitude and causes massive leakage between E
% and B.
%ft=ft./sum(ft(:));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ca=plot_map(ad,map,ca)
imagesc(ad.t_val_deg{1},ad.t_val_deg{2},map);
axis xy; set(gca,'XDir','reverse');
axis image
if(exist('ca','var'))
  caxis(ca);
else
  ca=caxis;
end
colorbar
xlabel('deg')
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ca=plot_ft(ad,ft,ca)
c=ad.N_pix(1)/2+1;
n=(length(ft)-1)/2;
xtic=2*pi*ad.u_val{1}(c-n:c+n);
imagesc(xtic,xtic,ft);
axis xy; set(gca,'XDir','reverse');
axis image
if(exist('ca','var'))
  caxis(ca);
else
  ca=caxis;
end
colorbar
xlabel('ell')
return
