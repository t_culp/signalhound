function reduc_sidelobemaps(t1,t2,filename,bs,skip_demod)
% function reduc_sidelobemaps(t1,t2,filename)
% t1: start time
% t2: end time
% filename: desired filename
% bs: optional argument - make boresight centered maps rather than zenith
% centered maps
% skip_demod: optional argument - skip the demodulation step

% condition input arguments:
if ~exist('bs','var')
  bs = [];
end
if isempty(bs)
  bs = 0;
end
if ~exist('skip_demod','var')
  skip_demod = [];
end
if isempty('skip_demod')
  skip_demod = 0;
end

% Demod:
if ~skip_demod
  beammap_demod_par(t1,t2,'r81',[],1,'fisheye')
end

type='sin';

% Get the files:
files=list_bm_files('scratch/beammaps/',t1,t2);

n_files=length(files);
tic
% Concatenate files from beammap_demod_par
for fl=1:n_files
  disp(files{fl}(1,1:15))
  e=load(strcat('scratch/beammaps/bm_',files{fl}(1,1:15)));
  switch type % Throwout unnecessary fields to save memory
    case 'sin'
      e=rmfield(e.d,{'cos','fb'});
    case 'cos'
      e=rmfield(e.d,{'sin','fb'});      
    case 'quad'
      e=rmfield(e.d,'fb'); 
    case 'raw'
      e=rmfield(e.d,{'sin','cos'});
    case 'moon'
      e=e.d;
  end
  d(fl)=e;
end
toc

clear e
d=structcat(1,d);

nsamp=length(d.pos(:,1));

[p ind]=get_array_info('20110101','obs');
% from source.cat:
source_lat = repmat(61.2,nsamp,1);
source_lon = repmat(336,nsamp,1);

if bs
  x_bin = -40:.5:40;
  y_bin = -40:.5:40;
else
  x_bin=0:1:360;
  y_bin=50:1:90;
end
[xg yg]=meshgrid(x_bin,y_bin);
[X Y Z]=sph2cart(xg*pi/180,yg*pi/180,ones(size(xg)));

for ii=1:length(p.r)
  if ~isnan(p.r(ii))
    pp=structcut(p,ii);
    inddd=structcut(ind,ii);
    [az_ap el_ap]=sidelobe_parallax(d.pos(:,1),d.pos(:,2),d.pos(:,3),d.com(:,1),d.com(:,2),d.com(:,3),pp,inddd,bs);
    [rng bear]=distance(el_ap',az_ap',source_lat,source_lon);
    if bs
      [el_ap az_ap]=reckon(0,0,rng',bear');
    end
    map(:,:,ii)=double(grid_map(az_ap,el_ap,d.sin(:,ii),x_bin,y_bin));
    [pvr(:,ii) S r]=binned_mean(rng,d.sin(:,ii),200);
  end
end

if isempty(strmatch('sidelobemaps/',filename))
  if bs
    filename=['sidelobemaps/bs_' filename];
  else
    filename=['sidelobemaps/' filename];
  end
end

save(filename,'pvr','map','r','X','Y','Z')