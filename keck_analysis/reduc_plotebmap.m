function [m,map]=reduc_plotebmap(mapf,ukpervolt,pureB,purifmatname,jackmaps,nsim,ssim)
% [m,map]=reduc_plotebmap(map,ukpervolt,pureB,purifmatname,jackmaps,nsim,ssim)
%
% go to f plane, go to eb, apply filter to downweight noisy modes,
% go back to image plane -> eb maps
%
% e.g.
% purifmat=load('matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj','pb','obs_pixels','reob','m');
% reduc_plotebmap('1450/real_a_filtp3_weight3_gs_dp1100_jack0',[],'normal',purifmat)

if(~exist('ukpervolt','var'))
  ukpervolt=[];
end
if(~exist('pureB','var'))
  pureB=[];
end
if(~exist('purifmatname','var'))
  purifmatname=[];
end

if(isempty(ukpervolt))
  ukpervolt=get_ukpervolt;
end
if(isempty(pureB))
  pureB='normal';
end
if ~exist('jackmaps','var')
  jackmaps=1;
end

% get the purification matrix
if(ischar(purifmatname))
  disp('loading purification matrix');
  purifmat=load(purifmatname, 'pb','obs_pixels','reob','m');
else
  if(~isempty(purifmatname))
    disp('using purification matrix passed in as arg');
  end
  purifmat=purifmatname;
end

% get the maps
load(sprintf('maps/%s',mapf))
if ~exist('map','var') && exist('ac','var')
  map=make_map(ac,m,coaddopt);
end
mapf=strrep(mapf,'_','\_');

% cal the maps
map=cal_coadd_maps(map,ukpervolt);

% derotate Q/U if alternate projection
if ~strcmp(m.proj,'radec')
  for i=1:numel(map)
    [map(i).Q,map(i).U]=derotate_qu(map(i).Q,map(i).U,m);
  end
end

% jackknife the maps
if jackmaps
  map=jackknife_map(map);
end

% smooth the var maps to reduce mode mixing
if(1)
  disp('smooth var maps')
  map=smooth_varmaps(m,map);
end

% pad the maps
if(0)
  disp('pad')
  tic
  p=2^nextpow2(max([m.nx,m.ny]));
  [m,map]=pad_map(m,map,p);
  toc
end

% calc axis data
ad=calc_ad2([m.xdos,m.ydos],[m.nx,m.ny]);
l=ad.u_r*2*pi;

% make the eb maps
disp('fft')
tic
map=make_ebmap(m,map,pureB,[],purifmat);
toc

% weiner filter
% - this code currently defunct...
if(0)
  % get the noise only sim set
  load(sprintf('aps/%s',nsim));
  ns=process_simset(aps);
  
  % if using signal sim get it
  if(exist('ssim','var'))
    load(sprintf('aps/%s',ssim));
    ss=process_simset(aps);
  end
  
  % prepare the filters - use ee for each freq
  for i=1:2
    if(~exist('ssim','var'))
      f(i).f=1./(1+ns(i).mean(:,3));
    else
      f(i).f=ss(i).mean(:,3)./(ss(i).mean(:,3)+ns(i).mean(:,3));
    end
    f(i).l=ns(i).l;
  end
  
  if(1)
    figure(1); clf
    plot(f(1).l,f(1).f);
    hold on
    plot(f(2).l,f(2).f,'r');
    hold off
  end
  
  r=1;
  n=81/r; be=1.5:n:2351; be=[be(1:end-1),linspace(be(end),2500.5,r+1)];
  
  for j=1:2
    map(j).Eftf=zeros(size(map(j).Eft));
    map(j).Bftf=zeros(size(map(j).Bft));
  end
  
  % for each aps bin
  for i=1:length(be)-1
    % find modes in annulus
    ind=l>be(i)&l<=be(i+1);    
    
    for j=1:2
      % multiply modes in bin by filter
      map(j).Eftf(ind)=map(j).Eft(ind)*f(j).f(i);
      map(j).Bftf(ind)=map(j).Bft(ind)*f(j).f(i);
    end
  end
end

if(nargout==0)
  %figure(3);
  clf; colormap jet
  subplot(1,2,1); plot_map(m,map(1).E); title('E'); caxis([-2,2]);
  subplot(1,2,2); plot_map(m,map(1).B); title('B'); caxis([-0.35,0.35]);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%
function plot_map(m,map)
imagescnan(m.x_tic,m.y_tic,map);
axis xy; set(gca,'YDir','reverse');
set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1]);
%caxis([-8,8]);
colorbar
return
