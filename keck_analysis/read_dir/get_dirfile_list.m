function [dirfiles,dirll] = get_dirfile_list(dir_norm)
% [dirfiles, dirll] = get_dirfile_list(dir)
% reads and returns the whole list of dirfile in a given directory.
% Inputs:
%          - dir directory name '/data/bicepdaq/dirfile/')
% Outputs
% dirfiles is the list fo dirfiles in that specific directory
% dirll is the 14 char filename.
% Written dB April27 2007
disp('getting dirfile list into dirfiles and dirll variables ...');

if nargin == 0 
dir_norm='/raid/bicep5/bicepdaq/dirfile/' ;
end

if dir_norm(end:end) ~= '/' ; dir_norm=strcat(dir_norm,'/'); end;

[s,t] = system(sprintf('''ls'' -1 %s',dir_norm));
t=strip_nonsense(t);
t=strread(t, '%s');

%strip the non *.dat data.
for i=1:size(t,1)
   if(~isempty(strfind(t{i}, 'dat')))
     q(i)=i;
   end
end
t=t(q);

dirfiles=char(t);
[a,b]=strtok(t,'.');
[first,second]=strtok(a,'_');
first=char(first);
second=char(second);
dirll=cat(2,first(:,:),second(:,2:end));
dirll=uint64(str2num(dirll));
