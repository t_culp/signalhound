%+
% NAME:
%    readRegs
%
% PURPOSE:
%    Generalized function to read in data into a stardard d. data structure.
%
% CALLING SEQUENCE:
%    d =  readRegs(reglist, input2, stop, shift32,str24, sim, dir)
%
% INPUTS:
%    reglist - a cell array of registers. ie regs={'antenna0.frame.utc',
%             'antenna0.tracker.horiz_topo'};
%   input2  - special input which determines what kind of data is being
%             read. input 2 can be of three types:
%                - a start time (ie 20060408000000) in which case the function
%                  will read in the reglist for the start/stop time
%                  interval. If this is the chosen option, the keywords,
%                  shift32, dir, and sim must be 
%                  specified (0 or 1)
%
%                - a dirfile name ( ie
%                '/data/bicepdaq/dirfile/20070424_185841.dat/') to read
%                in a single dirfile. or a tod filename
%                ('/data/tods/20070503_cmb13-1D/B_180/'). 
%
% DEPENDENCIES:
%   
%
% MODIFICATION HISTORY
%    2007-05-05 Written. (dB)


function d = readRegs(reglist, input2, stop, shift32,str24, sim, dir)

  
%--------read data if interval is requested. Most likely only reading one register.
  
  if isa(input2,'double') 
    
    if isempty(shift32) | isempty(sim) | isempty(dir)| isempty(str24)
      disp(['ERROR: You must specify the keywords shift32,str24, sim, and dir in the format'])
      disp('d =  readRegs(reglist, start, stop, shift32,str24, sim, dir)')
      d=0;
      return;
    end

    if ~iscell(reglist)
      reglist={reglist}; % or reg=num2cell(reg,[2])
    end
    
    start=input2;
    n_regs=length(reglist);
    cal=readCalfile();
        
    for reg_ind = 1:n_regs
      yy=find(strcmp(cal.name, reglist{reg_ind}) == 1);
      if isempty(yy)
        calfactor=1;
      else 
        calfactor =cal.factor{yy};
      end
      eval(['d.' reglist{reg_ind} '= readInterval(reglist{reg_ind},start,stop,shift32, str24, sim, dir) .* calfactor;']);
      end
       
  end
  
 %---------read data from a single dirfile (either from raw dirfiles or
 %from Cynthia's intermediate TODS)
  
%if  ~isempty(findstr(input2,'tods')) || ~isempty(findstr(input2,'dirfile')) || ~isempty(findstr(input2,'hcc_test'))

if isa(input2, 'char')
  
if ~iscell(reglist)
  reglist={reglist}; % or reg=num2cell(reg,[2])
end
   
   dirtods=input2;
   n_regs=length(reglist);
   r=readFormat(reglist,dirtods);
   no_reform = (r(2,:) ==10);
   no_reform([find(strcmp(reglist,'relgains') ==1),...
              find(strcmp(reglist,'relgains_err') ==1)])=0; 
   str24=0;   % no str24 in Cynthia's tod
   cal=readCalfile();
   
   for reg_ind = 1:n_regs
     yy=find(strcmp(cal.name, reglist{reg_ind}) == 1);
     if isempty(yy)
       calfactor=1;
     else 
       calfactor =cal.factor{yy};
     end
     eval(['d.' reglist{reg_ind} '= readdir(reglist{reg_ind},dirtods,no_reform(reg_ind),str24) .* calfactor;'])
    
   end
   
end


