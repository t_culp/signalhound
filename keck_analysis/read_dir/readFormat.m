%+
% NAME:   readFormat
% PURPOSE:
%    Given a single dirfile directory, reads in the data type and
%    dimension information for the specified register into an (1 x 2) integer
%    array.
%
% CALLING SEQUENCE:
%    [result, info] = read_format(reg, dir);
% INPUTS:
%    reg - register name, e.g. 'antenna0.pmac.axis_stat' or a cell array
%    of register names
%    dir - a dirfile name (ie '
%    /data/bicepdaq/dirfile/20070422_115342.dat/' or  '/data/tods/20070503_cmb13-1D/B_180/'
%
%OUTPUTS:
%    An integer array containing data type and dimension:
%        data_type = 'uint16' ; unsigned int (16-bit)
%        data_type = 'uint32' ; unsigned long
%        data_type = 'double'  ; double
%        data_type = 'float32' ; float
%        data_type = 'int32'  ; signed long
%    result(1) -- data_type string
%    result(2) -- dimension as recorded in the format file
%    The value -1 signifies the register does not exist in a dirfile.
%
% MODIFICATION HISTORY
%    2007-04-29 Written. (dB)
%-

function [result, info] = readFormat(reglist, dirf)

if ~iscell(reglist)
  reglist={reglist}; % or reg=num2cell(reg,[2])
end

  
n_regs=length(reglist);
result=zeros(2, n_regs);
if ~(strcmp(dirf(end:end),'/'))
  dirf=strcat(dirf,'/');
end;

  formatfile=strcat(dirf,'format');
  fid=fopen(formatfile);
  info=textscan(fid,'%s %s %s %u','commentStyle','#');
  fclose(fid);

  for i=1:n_regs
  ind=find(strcmp(info{1},reglist{i}));
  n_columns=info{4}(ind);
  type=char(info{3}(ind));
  
  switch type
   case {'u'} ; data_type = 1;     % uint16
   case {'U'} ; data_type = 2;     % uint32
   case {'d','D'} ; data_type = 3; % double
   case {'f','F'} ; data_type = 4; % float32
   case {'s','S'} ; data_type = 5; % int32
  end
  
  result(1,i)=data_type;
  result(2,i)=n_columns;
  
end

