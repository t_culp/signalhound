%+
% NAME:
%    readBolos
%
% PURPOSE:
%    Reads in all RTC channels into an (n_sample, 144) array.
%
% CALLING SEQUENCE:
%    [result, utcslow, utcfast, ffbits, biasdc] = readBolos(start, stop)
% INPUTS:
%    start   - start date/time integer (e.g. 20060701123456)
%    stop    - stop date/time integer
%    sim     - optionnaly read in bolo data from simulated dirfiles
%
%OUTPUTS
%    (n_sample, 144) array
%    utcslow - optionally read in the utcslow register
%    utcfast - optionally read in the utcfast register
%    ffbits  - optionally read in feature bits integer and upsample to fast
%              rate
%    biasdc  - optionally read in DC bias
%

function  [bolos, utcslow, utcfast, biasdc] = readBolos(start,stop, sim)
  
if(~exist('sim','var'))
  sim = [];
end

if(isempty(sim))
  sim=0;
end

%; time and features
  
[utcfast, utcslow] = readInterval('antenna0.time.utcfast', start, stop,0, 0,sim,0);
n = size(utcfast,1);

features = readInterval('array.frame.features', start, stop,0, 0,sim,0);
n_slow= size(features,1);
ff=permute(reshape(permute(repmat(features,1,n/n_slow),[2 1]),1,n),[2 1]);

% read in all 144 channels

shift32=1;
bolos = zeros(n, 144);
for i = 1:144
  reg = strcat('antenna0.bolo.mag',strtrim(num2str(i-1)));
  bolos(:, i) = readInterval(reg, start, stop, shift32,0,sim, 0);
end

%biasdc = readInterval('antenna0.bolo.voltage0', start, stop, shift32,0,sim);
