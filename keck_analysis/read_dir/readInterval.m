%+
% NAME:
%    readInterval
%
% PURPOSE:
%    Reads in a register for time interval specified by the start/stop UT
%    keywords.
%
% CALLING SEQUENCE:
%    [result,utcslow] = readInterval(reg, start, stop, shift32, sim, dir)
%
% INPUTS:
%    reg - a string specifying the register to be read (e.g. 'antenna0.bolo.mag0')
%    start   - UT start date/time 64-bit signed integer (e.g. 20060101235959)
%    stop    - UT stop date/time 64-bit signed integer
%    shift32 - Seamlessly reads ahead and shifts fast lock-in registers by 32
%              samples.
%    string24 -if set, read data as 24-character-long strings (e.g. antenna0.tracker.scan)
%    sim     - if set, read from simulated dirfiles (does not work now)
%    dir     - If set, read in dirfiles from alternate location (e.g. dir='/data/pointing/')
%
% OUTPUTS:
%    data stored in register within start/stop window
%    utcslow - Returns utcslow register
%
% DEPENDENCIES:
%    Need to have get_dirfile_list in your path
%
% MODIFICATION HISTORY
%    2007-04-27 Written. (dB)


function [data,utc] = readInterval(reg, start, stop, shift32,str24, sim, dir)
persistent dirll dirfiles 
 

if(~exist('str24'))
    str24=[];
end
if(isempty(str24))
  str24=0;
end

if(~exist('shift32'))
  shift32=[];
end
if(isempty(shift32))
  shift32=0;
end

if(exist('sim')~=1)
  sim = [];
end
if(isempty(sim))
  sim=0;
end

if(exist('dir')~=1)
  dir=[];
end
if(isempty(dir))
  dir=0;
end

% switch back and forth from real data, simulated data, and user-set dirfiles home

if isempty(dirfiles)
  switch 1 
   case sim
    dirll = dirll_sim;
    dirfiles = dirfiles_sim;
    dir_base='/data/dirfile_sim/';
   case dir
    [dirfiles, dirll]= get_dirfile_list(dir)  ;
   otherwise
    dir_base='/data/bicepdaq/dirfile/';
    %dir_base='/raid/bicep5/bicepdaq/dirfile/';    
    [dirfiles, dirll]= get_dirfile_list(dir_base);
  end
else
  switch 1 
   case sim
    dir_base='/data/dirfile_sim/';
   case dir
    dir_base=dir;
   otherwise
    dir_base='/data/bicepdaq/dirfile/';
    %dir_base='/raid/bicep5/bicepdaq/dirfile/'; 
  end   
end

% find dirfiles in the range of specified time interval and read data

n_dirll = size(dirll,1);
if start > dirll(end,:) 
  df_index = n_dirll;
  n = 1;
else
  ge_start = ge(dirll, start);
  ge_start0 = find((ge_start == 1),1,'first');
  if isempty(ge_start0)
    disp  'There are no dirfiles within specified range!';
  end
  ge_start(ge_start0-1) = 1;
  lt_stop  = lt(dirll,mjd2int(int2mjd(stop) + 1.0 / 86400.0));
  df_index = find(ge_start & lt_stop);
  n=size(df_index,1);
  if isempty(df_index)
    disp 'There are no dirfiles within specified range!';
  end
end


% initialize variables ahead of time using init_var
dirf=cat(2,repmat(dir_base,n_dirll,1),dirfiles,repmat('/',n_dirll,1));
dirnames=cat(2,repmat(dir_base,n_dirll,1),dirfiles,repmat('/',n_dirll,1));


% read in data and utcslow

utc = readdir('antenna0.time.utcslow', dirf(df_index(1),:));
data = readdir(reg, dirnames(df_index(1),:),shift32,str24);

for i = 2:n
  utc_i = readdir('antenna0.time.utcslow', dirf(df_index(i),:),0);
  data_i = readdir(reg, dirnames(df_index(i),:),shift32,str24);
  utc=cat(1,utc, utc_i);
  data=cat(1,data, data_i);
end

% convert start/stop times to UTC (MJD)

start_mjd = int2mjd(start);
stop_mjd = int2mjd(stop);

% cut data to start/stop, taking care to deal with fast registers appropriately

if (shift32 == 1 )
  data = circshift(data, -32);
end
foo= find(utc >= start_mjd  & utc < stop_mjd);

if size(foo,1) == 0
  utc = 0;
  data= 0;
end

start_index = foo(1);
stop_index = foo(end);

if size(utc,1) < size(data,1)
    data = data((start_index-1)*50 +1:stop_index*50, :);
else
    data = data(start_index:stop_index, :);
end
utc = utc(start_index:stop_index, :);
  