%+
% NAME:
%    readCalfile
%
% PURPOSE:
%    Reads in the cal file from ~bicep/gcp/control/conf/bicep/cal and put
%    the output in a structure
%
% CALLING SEQUENCE:
%    d = readCalfile()
%
% INPUTS:
%   none
%
% DEPENDENCIES:
%   
%
% MODIFICATION HISTORY
%    2007-05-09 Written. (dB)


function cal = readCalfile() 
 
 calfile='/home/bicep0/bicep/gcp/control/conf/bicep/cal';                
    fid=fopen(calfile);
    formatstring='%s %n %s';
    c=textscan(fid, formatstring , 'commentStyle','#');
    fclose(fid);
    for i= 1:length(c{3})
      [t,t1]=strtok(c{3}{i}, '/');
      if ~isempty(strfind(c{3}{i}, '/'))
        c{3}{i}=str2double(t)/str2double(t1(2:end));
      else
        c{3}{i}=str2double(t);
      end
    
    fields={'name','offset','factor'};
    cal=cell2struct(c,fields,2);
    cal.name=regexprep(cal.name, '*', '0');
    end