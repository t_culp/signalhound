regs={'antenna0.frame.utc', 'antenna0.tracker.horiz_topo',...
      'antenna0.tracker.horiz_mount',...
      'antenna0.tracker.flexure',...
      'antenna0.tracker.tilts',...
      'antenna0.tracker.fixedCollimation',...
      'antenna0.tracker.polarCollimation',...
      'antenna0.tracker.encoder_off',...
      'antenna0.tracker.horiz_off',...  % 'antenna0.tracker.source',...
      'antenna0.tracker.sky_xy_off',...
      'antenna0.weather.windSpeed',...
      'array.frame.features',...
      'antenna0.pmac.mtr_pos',...
      'antenna0.pmac.x_tilt',...
      'antenna0.pmac.y_tilt'};



start=20070427114824 
stop=20070427142740
d = readRegs(regs, start, stop)

%------------------------------

dirtods={'/data/tods/20070503_cmb13-1D/B_180'};
[result, info] = readFormat('array.frame.features', dirtods{1});

reglist={'array.frame.features','antenna0.time.utcfast', ...
         'antenna0.tracker.encoder_off', 'antenna0.bolo.mag40', ...
         'antenna0.bolo.mag41'};
reglist=info{1};

d = readRegs(reglist,dirtods);

% read in  time & features bits

n         = size(d.antenna0.time.utcfast,1);
indexfast = 1:n;
n_slow= size(d.array.frame.features,1);
ff=permute(reshape(permute(repmat(features,1,n/n_slow),[2 1]),1,n),[2 1]);

% make sure I have the start/stop time-integers consistent with Cynthia's cuts

start     = mjd2int(utcfast(1) - 2.0 / 3600.0 / 24.0) ; read larger chunk of data than needed
stop      = mjd2int(utcfast(end) + 2.0 / 3600.0 / 24.0);
%utc_orig  = readInterval('antenna0.time.utcfast', start, stop);
%n_orig    = size(utc_orig,1);
%dt_start  = round((utc_orig[0] - utcfast[0]) * 24.0 * 3600.0)
%dt_stop   = round((utc_orig[n_orig - (n_per_frame*down_ratio)] - utcfast[n - n_per_frame]) *   24.0 * 3600.0)

% set fp_data

if start < 20070101000000
  fp=ParameterRead('/data/bicep_aux_data/fp_data/fp_data_20061116.csv');
else
  fp=ParameterRead('/data/bicep_aux_data/fp_data/fp_data_20070221.csv');
end


% read in scanlist.txt
fid=fopen(strcat(dirtods{1},'scanlist.txt'));
result=textscan(fid,'%n %n %n %n %n','commentStyle','#');
fclose(fid);
 
scanlist.set  = result{1};
scanlist.num  = result{2};
scanlist.start= result{3};
scanlist.stop = result{4};
scanlist.inc  = result{5};

%n_scans = n_elements(scanlist_start)
%n_sets  = n_elements(uniq(scanlist_set, sort(scanlist_set)))
%scanset_start = scanlist_start[where(scanlist_num eq 0)]
%scanset_stop  = scanlist_stop[uniq(scanlist_set)]

% read in scancheck
scancheck_flag = readdir('scancheck', dirtods{1});

% read in relgains.txt <-- these are really absolute gains! (V/K)

result = readdir('relagains', dirtods{1});

%#######
[p,ind]=get_array_info(tag);

for i=1:86 
  plot(d.antenna0.bolo.mag(:,i))
  hold on
end
