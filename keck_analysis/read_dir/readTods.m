
%+
% NAME:
%    readTods
%
% PURPOSE:
%    function to read in Cynthia's tods and package them in a manner to
%    be as similar as quad's xxxxxx_tod.mat and thus be usable by further
%    quad pipline code.
%
% CALLING SEQUENCE:
%    [d,fs] =  readtods(tags, sav, dirtods)
%
% INPUTS:
%    - tags (ie  'B20070501_cmb13_1C_B_135') tag which designates which tod
%    phase to read in.
%    - sav: option  to save this as matlab.mat file.
%    - dir_tods: option to read in dir_tods from elsewhere than
%    /data/tods default
%
% DEPENDENCIES:
%   
%
% MODIFICATION HISTORY
%    2007-05-10 Written. (dB)
%    2007-10-24 added d.ra and d.dec for quad matlab consistency. (dB)

function [d, fs] = readTods(tags, sav, dirtods)
%disp("reading in Cynthia's tods")
      
  if(~exist('tags'))
    tags=[];
  end
   
  if(isempty(tags))
    tags='B20070501_cmb13_1C_B_135';
  end
 
    if(~exist('sav'))
    sav=[];
  end
   
  if(isempty(sav))
    sav=0;
  end
  
        
  if(~exist('dirtods'))
    dirtods=[];
  end
   
  if(isempty(dirtods))
    dirtods='/data/tods/';
  end

  exp=tags(1);
  [dat,r]=strtok(tags(2:end),'_');
  [sch,r]=strtok(r,'_');
  [fam,r]=strtok(r,'_');
  fas=r(2:end);
  dirname=strcat(dirtods,dat,'_',sch,'-',fam,'/',fas,'/')
  
[result, info] = readFormat('array.frame.features', dirname);
reglist=info{1};
d = readRegs(reglist,dirname);
d.t=d.antenna0.time.utcfast;
n_samples=size(d.t,1);

if (isfield(d,'pointing'))
  d.ra=d.pointing.cel.ra;
  d.dec=d.pointing.cel.dec;
else
  disp('Warning: Using raw pointing values because corrected pointing does not yet exist')
  d.ra=d.rawpoint.cel.ra;
  d.dec=d.rawpoint.cel.dec;
  d.pointing=d.rawpoint;
end


% change the structure of d.antenna0.bolo into a multi-dim array.
out=reformMulti(d.antenna0.bolo, n_samples); 
d.antenna0.bolo = rmfield(d.antenna0.bolo,fieldnames(d.antenna0.bolo));
d.antenna0.bolo.mag=out;

% read in scanlist.txt 
fid=fopen(strcat(dirname,'scanlist.txt'));
result=textscan(fid,'%n %n %n %n %n','commentStyle','#');
fclose(fid);
fields={'set', 'num', 'start', 'stop', 'inc'};
fs=cell2struct(result, fields, 2);


% read in scanlist_cut.txt  
fid=fopen(strcat(dirname,'scanlist_cut.txt'));
result=textscan(fid,'%n %n %n %n %n','commentStyle','#');
fclose(fid);
fs.sf=result{3};
fs.ef=result{4};

%fix fs array to now be 1 indexed instead of being 0 indexed
%moved to readTods on Nov 25 2009, by dB
fs.set=fs.set+1;
fs.num=fs.num+1;
fs.start=fs.start+1;
fs.stop=fs.stop+1;
fs.sf=fs.sf+1;
fs.ef=fs.ef+1;

% read in source.txt 
fid=fopen(strcat(dirname,'source.txt'));
source=textscan(fid,'%s','commentStyle','#');
fclose(fid);
d.scan.src=source{1};

%read in blackmarket scan profile info
fid=fopen('/data/bicep_aux_data/scansource.txt');
scans=textscan(fid,'%s %n %n',5,'commentStyle','#');
fields={'name', 'throw', 'rate'};
scans=cell2struct(scans, fields, 2);
sources=textscan(fid,'%s %s %n','commentStyle','#');
fields={'type', 'name', 'midpoint'};
sources=cell2struct(sources, fields, 2);
fclose(fid);

%create d.azoff as well as other needed structures in d.
% this part will soon be replaced by just reading in these values
% from Cynthia
samprate = 10;           % Hz, downsampled fast rate (HARDWIRE)
n_el_steps = length(unique(fs.set));  % number of el steps in each phase

azoff=zeros(length(d.rawpoint.hor.az),1);
throw=zeros(n_el_steps,1);
rate=zeros(n_el_steps,1);
npoints=zeros(n_el_steps,1);


% this may all be incorrect way of calculating d.azoff.
% what about d.eloff
% dB oct 19 2007
if(1)
for k=1:fs.set(end)
  
  q=find(fs.set == k);
  first=fs.start(q(1));
  last=fs.stop(q(end));
  start=uint64(mjd2int(d.t(first)));
  stop=uint64(mjd2int(d.t(last)));
  scan_prof=readInterval('antenna0.tracker.scan',start,stop,0,1,0,0);
  scan_prof=deblank(scan_prof(2,:));
  
  % if source is tracked (ra-fixed)
  if ~isempty(strfind(d.scan.src{k}, 'galpl')) | ~ ...
        isempty(strfind(d.scan.src{k}, 'bicepCMB'))
    tmp=d.ra(first:last);
    tmp = tmp-360*(tmp >285);  %change RA range to -5 - 19 hrs
    hours2deg=15;
 % if source is az-fixed
  else % if CMBX-XXXX, or galp-XXXX
    tmp=d.rawpoint.hor.az(first:last);
    hours2deg=1.;
  end
   
  midpoint=sources.midpoint(strmatch(d.scan.src{k}, sources.name))* hours2deg;
  throw(k)=scans.throw(strmatch(scan_prof, scans.name,'exact'));
  rate(k)=scans.rate(strmatch(scan_prof, scans.name, 'exact'));
  npoints(k)=floor(2*samprate*throw(k)/rate(k));
  tmp=tmp-midpoint;
  
  azoff(first:last)=tmp;
  
  fs.src(q)=d.scan.src(k);
  fs.throw(q)=throw(k);
  fs.rate(q)=rate(k);
end

d.azoff=azoff;
fs.src=fs.src';
fs.throw=fs.throw';
fs.rate=fs.rate';
d=rmfield(d,'scan')


end

if(sav)
  save(strcat('data/',tags,'_tod_uncal'),'d', 'fs');
end


if(0)
  plot(d.azoff)
  hold on
  plot(fss.start, d.azoff(fss.start), 'r.')
  plot(fss.stop, d.azoff(fss.stop), 'g.')
end


if(0)
  n         = size(d.antenna0.time.utcfast,1);
  indexfast = 1:n;
  n_slow= size(d.array.frame.features,1);
  ff=permute(reshape(permute(repmat(features,1,n/n_slow),[2 1]),1,n),[2 1]);
end


