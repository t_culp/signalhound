function [result, n_samples_per_file, n_columns] = init_var(reg, dirf)

  if dirf(end:end) ~= '/' ; dirf=strcat(dirf,'/'); end;
 
  n_sec=2000;     % max number of seconds per file
  n_dirf=size(dirf,1);  % number of individual dirfiles
  
% strcat(dirf,reg,'*');
% file_info=dir(strcat(dirf(1,:),reg,'*')); % get info from first file
%   [a,b,c,d,e,f,g]=strread(x, '%s %s %s %s %s %s %s');
%   [s, x]=system('ls -l /data/bicepdaq/dirfile/*/antenna0.time.utcslow');
% bytes = file_info.bytes;
 
 format=readFormat(reg, dirf(1,:));
  
  if format(1) == 3 
    bytes_per_sample = 8;
  else
    bytes_per_sample = 4;
  end
  
  if format(2) == 50 
    n_columns = 1;
    n_samples_per_sec=50;
  else
    n_columns=format(2);
    n_samples_per_sec= 1;
  end
 
  switch format(1)
   case 1 ; type = 'uint16';
   case 2;  type = 'uint32';
   case 3;  type = 'double';
   case 4;  type = 'float32'; % used to be float32
   case 5;  type = 'int32';
  end
  
  n_samples_per_file= n_sec *  n_columns * n_samples_per_sec;
  n_samples=n_dirf *n_samples_per_file;
  result=zeros(n_dirf, n_samples_per_file, n_columns, type);
  