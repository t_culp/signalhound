function write_dir(var,reg,dir,update_format,skip_format,verbose)
%
% Writes out dirfiles and creates a format file in the specified directory.
% transposed from write_dir.pro from bicep analysis
%
% Example: write_dir(scancheck,'scancheck_v1','data/tods/20070706_cmb16-2C/B_135/')
%
% Inputs
%   var - the variable to be written
%   reg - the register name.  i.e. 'scancheck' or 'antenna0.bolo.mag41'
%   dir - the directory name: '/data/tods/20070706....'
%   update_format - set to update the formate entry (default)
%   skip_format - if set, do not modify format file
%   verbose - print info
%
% Recommended to run fix_format as well.
%
%
% sas 12/14/2009

%preliminaries			       
data = var;   %protect input

if ~strcmp(dir(end:end),'/') 
  dir=strcat(dirf,'/');
end
			       
file = strcat(dir,reg);

if (~exist('update_format'))
  update_format=1;
end

if(~exist('skip_format'))
  skip_format=0;
end

if(~exist('verbose'))
  verbose=0;
end

%input variable information

size_info=size(data);
data_type=class(data);


switch data_type
 case 'uint32';  type = 'U'; 
 case 'double';  type = 'd';
 case 'float32'; type = 'f';
 case 'int32';   type = 's';
 case 'uint16';  type = 'u';
 case '*char';   type = 'u';
 otherwise, sprintf('Unsupported input variable type %s %s',reg,data_type)
end


% impose strict 24-character width if input is string array
if strcmp(data_type,'*char')
  data = unit8(data(0:24));
  data = data + (data == 0) * (unit8(' '));
  data = char(data) ; %no need to conver back to string, but do it just because...
end


% write variable to disk as unformatted binary
				  
fid=fopen(file,'w');
fwrite(fid,data);
fclose(fid);

if skip_format 
  return
end

% update/create format file

  n_columns= size_info(2);

if update_format
   permission='a';
 else
   permission = 'w';
end

whitespace=repmat(' ',1,35-length(reg));

fid=fopen(strcat(dir,'format'),permission);
fprintf(fid,'%s%sRAW %s %d\n',reg,whitespace,type,n_columns);
  fclose(fid);

end
