function data = readdir(reg, dirf, no_reform, str24)
%READDIR reads dir data.
%  Reads in dirfile data  given a register name and a directory name.
%  It automatically checks for
%  proper data type and columns by looking at the "format" file.
%  Inputs:
%       - reg ( register name, ie 'antenna0.bolo.mag41')
%       - dirf directory name
%       '/data/bicepdaq/dirfile/20070424_185841.dat/')
%       - no_reform - if set, don't reform/transpose data into
%       multi-dimensional array (default is no_reform=0)
%       -string24  - if set, read data as 24-character-long strings (e.g. antenna0.tracker.scan)
% Written dB April26 2007
% Mod     dB May7 2007. Added no_reform keyword
% Mod     dB Jun 10 2007. added the string24 keyword
 
 if(~exist('str24'))
    str24=[];
  end
   
  if(isempty(str24))
    str24=0;
  end
  
  
  if(~exist('no_reform'))
    no_reform=[];
  end
   
  if(isempty(no_reform))
    no_reform=0;
  end
  
  
if ~strcmp(dirf(end:end),'/') 
  dirf=strcat(dirf,'/');
end

[result, info] = readFormat(reg, dirf);
n_columns=result(2);
type=result(1);
if (str24)
  n_columns = n_columns/12;
end

switch type
 case 1 ; 
  if (str24)
    data_type = '*char';
  else
    data_type = 'uint16';
  end
 case 2 ; data_type = 'uint32';
 case 3 ; data_type = 'double';
 case 4 ; data_type = 'float32';
 case 5 ; data_type = 'int32';
end


% read data

filename=strcat(dirf,reg);
fid=fopen(filename);
data=fread(fid, inf, data_type);
fclose(fid);

if (str24) 
    data = reshape(data, 24, size(data,1)/24);
    data=permute(data,[2 1]);
end

if (n_columns ~= 1 & n_columns ~= 50 &  ~(no_reform))
  data=reshape(data,n_columns,size(data,1)/n_columns);
  data=permute(data,[2 1]);
end