function reduc_final_pager_080416()
%
% make .html files to provide pager between reduc_final 's plots

system('rm -Rf reduc_final_pager')
system('mkdir reduc_final_pager')

make1(200);
make1(500);

return

% make pages
function make1(lmax)

  pl=1;
  tt={'start','next'};
  realspec=sprintf('sim003_filtp3_weight2_jack0')
  nsimset=sprintf('sim003xxx1_filtn_weight2_jack0');
  ssimset=sprintf('sim003xxx2_filtp3_weight2_jack0');
  snsimset=sprintf('sim003xxx3_filtp3_weight2_jack0');
  
  reduc_final(realspec,nsimset,ssimset,snsimset,pl,lmax)

%write out gifs
for pp=1:6
  printname=sprintf('reduc_final_pager/fig_%s_final_%i_%i.gif',realspec,lmax,pp);
  printfig(pp,printname,'pp')
   
fh=fopen(sprintf('reduc_final_pager/fig_%s_final_%i_%i.html',realspec,lmax,pp),'w');

fprintf(fh,'<a href="../">up</a> ------ ');

if(pp == 1)
  fprintf(fh,'<a href="fig_%s_final_%i_1.html">start</a>-- ',realspec,lmax);
  fprintf(fh,'back ');
  fprintf(fh,'<a href="fig_%s_final_%i_%i.html">next</a>--  ',realspec,lmax,pp+1);
elseif(pp == 6)
  fprintf(fh,'<a href="fig_%s_final_%i_1.html">start</a>-- ',realspec,lmax);
  fprintf(fh,'<a href="fig_%s_final_%i_%i.html">back</a>-- ',realspec,lmax,pp-1);
  fprintf(fh,'next ');
else
  fprintf(fh,'<a href="fig_%s_final_%i_1.html">start</a>-- ',realspec,lmax);
  fprintf(fh,'<a href="fig_%s_final_%i_%i.html">back</a>-- ',realspec,lmax,pp-1);
  fprintf(fh,'<a href="fig_%s_final_%i_%i.html">next</a>--  ',realspec,lmax,pp+1);
end

fprintf(fh,'\n');


% Choose set
fprintf(fh,'------ ');
switch lmax
 case 200
  fprintf(fh,'<a href="fig_%s_final_500_%i.html">lmax=500</a>',realspec,pp);
 case 500
  fprintf(fh,'<a href="fig_%s_final_200_%i.html">lmax=200</a>',realspec,pp);
end

% include the figure
fprintf(fh,'<p><img src="fig_%s_final_%i_%i.gif">%s</a>\n',realspec,lmax,pp);

fclose(fh);
end

return
