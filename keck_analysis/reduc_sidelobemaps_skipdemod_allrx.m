function reduc_sidelobemaps_skipdemod_allrx(t1,t2,filename_base, skip)
  for iRx = 0:4
    reduc_sidelobemaps_skipdemod(t1,t2,[filename_base num2str(iRx) '.mat'], ...
      iRx, skip)
  end
end


