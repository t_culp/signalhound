function paper_plots_b2p(pl,dopr)
% paper_plots_b2_b2p(pl,dopr)
%
% This file should know how to make all of the plots and quoted
% numbers

global dopres;
if exist('dopr','var') & dopr
  set(0,'defaultlinelinewidth',1.0);
  set(0,'DefaultAxesFontSize',14);
  %set(0,'defaulttextfontsize',14);
  dopres = 1;
else
  dopres = 0;
end

if(any(pl==1))
  make_mapplot;
end

if(any(pl==2))
  make_specplot1;
end

if(any(pl==3))
  make_specplot2;
end

if(any(pl==4))
  make_specplot3;
end

if(any(pl==5))
  make_specplot4;
end

if(any(pl==6))
  make_likebase;
end

if(any(pl==7))
  make_likevar;
end

if(any(pl==8))
  make_sync;
end

if(any(pl==9))
  make_al;
end

if(any(pl==10))
  make_simcons;
end

if(any(pl==11))
  make_psm;
end

if(any(pl==12))
  make_noilev;
end

if(any(pl==13))
  make_speccomp;
end

if any(pl==100)
  make_powspecrestable
end

if any(pl==101)
  make_rlikelihood_file
end

if any(pl==102)
  make_cosmomc_tarball
end

if any(pl==103)
  make_cosmomc_EB_tarball
end

%%%%%%%%%%%%%%%%%%%%%
% Functions for getting final data
%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%
function [r, im, ml, bpwf, filename] = get_pxp_DSsplit
% Get PxP spectra from DS split
%  filename = 'final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack4_real_a_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_nonej_directbpwf.mat';
filename = 'final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf.mat'
load(filename)
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); 
r = r(end);
r.lc(1,:) = NaN;
im=inpmod;

% get the lensing model to compare to lensing sims
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');
return


%%%%%%%%%%%%%%%%%%%%%
function [r, im, ml, bpwf, filename,mt,rscaling,jackname,projfile] = get_bkxp
% Fetches official BKxP spectra
filename = 'final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat';  
jackname = '';
load(filename)
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); 
for i = 1:numel(r)
  r(i).lc(1,:)=NaN;
end
im=inpmod;

% get the lensing model to compare to lensing sims
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');

rscaling = '';

return

%%%%%%%%%%%%%%%%%%%%%
function make_mapplot

% get observed map
%load maps/1613/real_a_filtp3_weight3_gs_dp1100_jack0.mat
load maps/1457/real_aab_filtp3_weight3_gs_dp1100_jack0.mat
map=make_map(ac,m,coaddopt);
%map=cal_coadd_maps(map,get_ukpervolt);
% use 353GHz map
mapo=map(end);

% make the apodization masks
maskt=1./mapo.Tvar; maskt=maskt./prctile(maskt(:),97);
maskp=1./((mapo.Qvar+mapo.Uvar)/2);
maskp=maskp./prctile(maskp(:),97);

% apply mask
mapo.T=mapo.T.*maskt; mapo.Q=mapo.Q.*maskp; mapo.U=mapo.U.*maskp; 

% extract Planck map in our field
map=healpix_to_longlat(coaddopt.reob_map_fname{7},m.x_tic,m.y_tic);
mapi.T=map(:,:,1); mapi.Q=map(:,:,2); mapi.U=map(:,:,3);

% mask to non nan region
mapi.T(isnan(mapo.T))=NaN; mapi.Q(isnan(mapo.T))=NaN; mapi.U(isnan(mapo.T))=NaN;

% remove the weighted mean in the obs region
mapi.T=mapi.T-nansum(rvec(mapi.T.*maskt))/nansum(maskt(:));
mapi.Q=mapi.Q-nansum(rvec(mapi.Q.*maskp))/nansum(maskp(:));
mapi.U=mapi.U-nansum(rvec(mapi.U.*maskp))/nansum(maskp(:));

% apply mask
mapi.T=mapi.T.*maskt; mapi.Q=mapi.Q.*maskp; mapi.U=mapi.U.*maskp; 

% qu caxis range
qur=[-40,40];

fig=figure;
clf;
dim=get_sixpaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% T

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapi.T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
title('T unfiltered');

ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapo.T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax2,'FontSize',7,'FontWeight','normal');
set(ax2,'Xticklabel',[],'Yticklabel',[]);
title('T filtered');

% Q

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapi.Q,'iau');
caxis(qur);
colormap gray
freezeColors
set(ax3,'FontSize',7,'FontWeight','normal');
set(ax3,'Xticklabel',[]);
title('Q unfiltered');

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapo.Q,'iau');
caxis(qur);
colormap gray
freezeColors
set(ax4,'FontSize',7,'FontWeight','normal');
set(ax4,'Xticklabel',[],'Yticklabel',[]);
title('Q filtered');

% U

ax5=axes('Position',[dim.x1,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,mapi.U,'iau');
caxis(qur);
colormap gray
freezeColors
set(ax5,'FontSize',7,'FontWeight','normal');
title('U unfiltered');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

ax6=axes('Position',[dim.x2,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,mapo.U,'iau');
caxis(qur);
colormap gray
freezeColors
set(ax6,'FontSize',7,'FontWeight','normal');
set(ax6,'Yticklabel',[]);
title('U filtered');

% colorbars

ax7 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax7,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-150,150,1000),repmat(linspace(-150,150,1000)',1,10));
colormap hot;
freezeColors;
set(ax7,'Xtick',[]);
set(ax7,'YDir','normal','YAxisLocation','right','YTick',[-100 0 100]);

ax8 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax8,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(qur(1),qur(2),1000),repmat(linspace(qur(1),qur(2),1000)',1,10));
colormap gray;
freezeColors;
set(ax8,'Xtick',[]);
set(ax8,'YDir','normal','YAxisLocation','right','YTick',[qur(1) 0 qur(2)]);

ax9 = axes('Position',[dim.x3,dim.y3,dim.cbar/dim.W,dim.ymap]);
set(ax9,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(qur(1),qur(2),1000),repmat(linspace(qur(1),qur(2),1000)',1,10));
colormap gray;
freezeColors;
set(ax9,'Xtick',[]);
set(ax9,'YDir','normal','YAxisLocation','right','YTick',[qur(1) 0 qur(2)]);

% colorbar units

ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y3 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

print -depsc2 b2p_plots/tqu_maps.eps
fix_lines('b2p_plots/tqu_maps.eps')
% the version of epstopdf on Odyssey now compresses the images
% causing artifacts (even when using --nocompress switch) - do the
% epstopsdf step on focus to avoid this
%!epstopdf --nocompress b2p_plots/tqu_maps.eps
disp('WARNING: remember to transfer eps file to another computer to run epstopdf');

return

%%%%%%%%%%%%%%%%%%%%%
% used by make_mapplot above
function dim=get_sixpaneldims(m)
% Parameters for six-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 3 * dim.maph + dim.wide + 3 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 2 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 2 * dim.maph + 2 * dim.med) / dim.H; % Bottom edge of row 1.
dim.y2 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y3 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return

%%%%%%%%%%%%%%%%%%%%%
function make_specplot1

for i=1:9
  chibins{i}=[2:10]; % use all plotted bins
end

% For TT get errorbars from legacy unconstrained sims - we will
% also use these for B2xP and PxP errorbars since noise is negligible
load final/1450x1350/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_overrx
r=r(1); r=get_bpcov(r);
rt=r;

% get the B2xP full points
% NB: the B2xB2 ones in here lack the residual beam correction and
% hence are not quite the same as the PRL ones in the highest 3
% bandpower plotted
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rf=r;
im=inpmod;

% So get the B2xB2 exactly as plotted in the PRL
load final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf_rbc.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rb2prl=r;

% get the KxP full points
load final/1351x1614/real_ab_filtp3_weight3_gs_dp1102_jack0_real_ab_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rk=r;

% get the CxP full points
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rc=r;

ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');

% the weighted bin centers
lc=r(1).lc; lc(1,:)=NaN;

% get the PxP splitxsplit points
%  load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack3_real_a_filtp3_weight3_gs_dp1100_jack3_pureB_matrix_nonej_directbpwf.mat
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack3_pureB_matrix_directbpwf.mat
r=get_bpcov(r);
rs1=r;
%  load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack4_real_a_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_nonej_directbpwf.mat
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf.mat
r=get_bpcov(r);
rs2=r;
%  load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack5_real_a_filtp3_weight3_gs_dp1100_jack5_pureB_matrix_nonej_directbpwf.mat
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack5_pureB_matrix_directbpwf.mat
r=get_bpcov(r);
rs3=r;

% strip down to the elements we need
i=aps_getxind(length(rf),1,8);
rf=rf([1,i]);
rk=rk([1,i]);
rc=rc([1,i]);
rs1=rs1(end);
rs2=rs2(end);
rs3=rs3(end);

% calc the chi2 for 9 bandpowers
for i=1:9
  chibins{i}=[2:10]; % use all plotted bins
end
rc=calc_chi(rc,chibins,[],1); rc=calc_devs(rc,chibins,1);
rs2=calc_chi(rs2,chibins,[],1); rs2=calc_devs(rs2,chibins,1);

clf; setwinsize(gcf,1000,750);
ms=5; % marker size
ms2=10;
f=0.5;

% TT
subplot_grid(4,3,1)
% plot the least important things first
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,1),'r');
h1=errorbar2(lc(:,1)-5,rf(1).real(:,1),rt.derr(:,1),'b*');
h2=errorbar2(lc(:,1)+5,rk(1).real(:,1),rt.derr(:,1),'rx');
h3=errorbar2(lc(:,1)+0,rc(1).real(:,1),rt.derr(:,1),'k.');
set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
set(h3(2),'MarkerSize',ms2);
set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
xlim([0,330]); ylim([-100,8000]);
title('150x150');
ylabel('TT l(l+1)C_l/2\pi [\muK^2]')
legend([h3(2),h1(2),h2(2)],{'BKxBK','BxB','KxK'},'Location','NW');
legend boxoff
subplot_grid2(4,3,1)

subplot_grid(4,3,2)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,1),'r');
h1=errorbar2(lc(:,1)-5,rf(2).real(:,1),rt.derr(:,1),'b*');
h2=errorbar2(lc(:,1)+5,rk(2).real(:,1),rt.derr(:,1),'rx');
h3=errorbar2(lc(:,1)+0,rc(2).real(:,1),rt.derr(:,1),'k.');
set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
set(h3(2),'MarkerSize',ms2);
set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
xlim([0,330]); ylim([-100,8000]);
title('150x353');
legend([h3(2),h1(2),h2(2)],{'BKxP353','BxP353','KxP353'},'Location','NW');
legend boxoff
subplot_grid2(4,3,2)

subplot_grid(4,3,3)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,1),'r');
h1=errorbar2(lc(:,1)-5,rs1.real(:,1),rt.derr(:,1),'b.');
h2=errorbar2(lc(:,1)+5,rs3.real(:,1),rt.derr(:,1),'r.');
h3=errorbar2(lc(:,1)-0,rs2.real(:,1),rt.derr(:,1),'k.');
set(h1(2),'MarkerSize',ms2); set(h2(2),'MarkerSize',ms2);
set(h3(2),'MarkerSize',ms2);
%set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
%set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
xlim([0,330]); ylim([-100,8000]);
title('353half1x353half2');
legend([h3(2),h1(2),h2(2)],{'DS1xDS2','Y1xY2','HR1xHR2'},'Location','NW');
legend boxoff
subplot_grid2(4,3,3)

% TE
subplot_grid(4,3,4)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,2),'r');
h1=errorbar2(lc(:,2)-5,rf(1).real(:,2),rf(1).derr(:,2),'b*');
h2=errorbar2(lc(:,2)+5,rk(1).real(:,2),rk(1).derr(:,2),'rx');
h3=errorbar2(lc(:,2)+0,rc(1).real(:,2),rc(1).derr(:,2),'k.');
set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
set(h3(2),'MarkerSize',ms2);
set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
xlim([0,330]); ylim([-150,250]);
ylabel('TE l(l+1)C_l/2\pi [\muK^2]')
subplot_grid2(4,3,4)

subplot_grid(4,3,5)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,2),'r');
h1=errorbar2(lc(:,2)+2.5,rc(2).real(:,2),rc(2).derr(:,2),'r.');
set(h1(2),'MarkerSize',ms2);
set(h1(1),'Color',[f,0,0]); set(h1(2),'Color',[f,0,0]);
h2=errorbar2(lc(:,2)-2.5,rc(2).real(:,7),rc(2).derr(:,7),'b.');
set(h2(2),'MarkerSize',ms2);
set(h2(1),'Color',[0,f,0]); set(h2(2),'Color',[0,f,0]);
hold off
xlim([0,330]); ylim([-150,250]);
legend([h2(2),h1(2)],{'BK_ExP353_T','BK_TxP353_E'},'Location','NW');
legend boxoff
subplot_grid2(4,3,5)

subplot_grid(4,3,6)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,2),'r');
h1=errorbar2(lc(:,2)-5,rs1.real(:,2),rs1.derr(:,2),'b.');
h2=errorbar2(lc(:,2)+5,rs3.real(:,2),rs3.derr(:,2),'r.');
h3=errorbar2(lc(:,2)-0,rs2.real(:,2),rs2.derr(:,2),'k.');
set(h1(2),'MarkerSize',ms2); set(h2(2),'MarkerSize',ms2);
set(h3(2),'MarkerSize',ms2);
%set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
%set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
xlim([0,330]); ylim([-150,250]);
subplot_grid2(4,3,6)

% EE
subplot_grid(4,3,7)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,3),'r');
h1=errorbar2(lc(:,3)-5,rf(1).real(:,3),rf(1).derr(:,3),'b*');
h2=errorbar2(lc(:,3)+5,rk(1).real(:,3),rk(1).derr(:,3),'rx');
h3=errorbar2(lc(:,3)+0,rc(1).real(:,3),rc(1).derr(:,3),'k.');
set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
set(h3(2),'MarkerSize',ms2);
set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
xlim([0,330]); ylim([-1,7]);
ylabel('EE l(l+1)C_l/2\pi [\muK^2]')
%ptitle(sprintf('%.1f\\sigma',-norminv(rc(1).ptes(3))));
ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rc(1).rchisq(3),rc(1).rsumdev(3)))
subplot_grid2(4,3,7)

subplot_grid(4,3,8)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,3),'r');
h1=errorbar2(lc(:,3)-5,rf(2).real(:,3),rf(2).derr(:,3),'b*');
h2=errorbar2(lc(:,3)+5,rk(2).real(:,3),rk(2).derr(:,3),'rx');
h3=errorbar2(lc(:,3)+0,rc(2).real(:,3),rc(2).derr(:,3),'k.');
set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
set(h3(2),'MarkerSize',ms2);
set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
xlim([0,330]); ylim([-1,7]);
%ptitle(sprintf('%.1f\\sigma',-norminv(rc(2).ptet(3))));
ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rc(2).rchisq(3),rc(2).rsumdev(3)))
subplot_grid2(4,3,8)

subplot_grid(4,3,9)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,3),'r');
h1=errorbar2(lc(:,3)-5,rs1.real(:,3),rs1.derr(:,3),'b.');
h2=errorbar2(lc(:,3)+5,rs3.real(:,3),rs3.derr(:,3),'r.');
h3=errorbar2(lc(:,3)+0,rs2.real(:,3),rs2.derr(:,3),'k.');
set(h1(2),'MarkerSize',ms2); set(h2(2),'MarkerSize',ms2);
set(h3(2),'MarkerSize',ms2);
%set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
%set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
xlim([0,330]); ylim([-36.4,48.5]); % twice the BB range
%ptitle(sprintf('%.1f\\sigma',-norminv(rs2.ptet(3))));
ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rs2(1).rchisq(3),rs2(1).rsumdev(3)))
subplot_grid2(4,3,9)

% BB
subplot_grid(4,3,10)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,ml.Cs_l(:,4),'r');
%h1=errorbar2(lc(:,4)-5,rf(1).real(:,4),rf(1).derr(:,4),'b*');
% plot points exactly as in PRL - for ell<200 nearly identical
h1=errorbar2(lc(:,4)-5,rb2prl.real(:,4),rb2prl.derr(:,4),'b*');
h2=errorbar2(lc(:,4)+5,rk(1).real(:,4),rk(1).derr(:,4),'rx');
h3=errorbar2(lc(:,4)+0,rc(1).real(:,4),rc(1).derr(:,4),'k.');
set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
set(h3(2),'MarkerSize',ms2);
set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
xlim([0,330]); ylim([-0.03,0.04]);
ylabel('BB l(l+1)C_l/2\pi [\muK^2]')
yl=ylim;
%ptitle(sprintf('%.1f\\sigma',-norminv(rc(1).ptet(4))));
ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rc(1).rchisq(4),rc(1).rsumdev(4)),0.03,0.1)
subplot_grid2(4,3,10)

sf=24.63; % from Ganga post 8/14

subplot_grid(4,3,11)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,ml.Cs_l(:,4),'r--');
h1=errorbar2(lc(:,4)-5,rf(2).real(:,4),rf(2).derr(:,4),'b*');
h2=errorbar2(lc(:,4)+5,rk(2).real(:,4),rk(2).derr(:,4),'rx');
h3=errorbar2(lc(:,4)+0,rc(2).real(:,4),rc(2).derr(:,4),'k.');
set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
set(h3(2),'MarkerSize',ms2);
set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
xlim([0,330]); ylim(yl*sf); box off
axes('Position',get(gca,'Position'),'XAxisLocation','top','YAxisLocation','right','Color','none');
ylim(yl);
y=ylabel('scaled'); set(y,'position',get(y,'position')-[0.07,0,0]);
set(gca,'XTick',[])
%ptitle(sprintf('%.1f\\sigma',-norminv(rc(2).ptet(4))));
ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rc(2).rchisq(4),rc(2).rsumdev(4)),0.03,0.1)
subplot_grid2(4,3,11)

subplot_grid(4,3,12)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,ml.Cs_l(:,4),'r--');
h1=errorbar2(lc(:,4)-5,rs1.real(:,4),rs1.derr(:,4),'b.');
h2=errorbar2(lc(:,4)+5,rs3.real(:,4),rs3.derr(:,4),'r.');
h3=errorbar2(lc(:,4)-0,rs2.real(:,4),rs2.derr(:,4),'k.');
set(h1(2),'MarkerSize',ms2); set(h2(2),'MarkerSize',ms2);
set(h3(2),'MarkerSize',ms2);
%set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
%set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
xlim([0,330]); ylim(yl*sf^2); box off
xlabel('Multipole')
axes('Position',get(gca,'Position'),'XAxisLocation','top','YAxisLocation','right','Color','none');
ylim(yl);
y=ylabel('scaled'); set(y,'position',get(y,'position')-[0.07,0,0]);
set(gca,'XTick',[])
%ptitle(sprintf('%.1f\\sigma',-norminv(rs2.ptet(4))));
ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rs2(1).rchisq(4),rs2(1).rsumdev(4)),0.03,0.1)
subplot_grid2(4,3,12)

print -depsc2 b2p_plots/spectra.eps
fix_lines('b2p_plots/spectra.eps')
!epstopdf b2p_plots/spectra.eps
!convert -density 300 b2p_plots/spectra.eps b2p_plots/spectra.png

return

%%%%%%%%%%%%%%%%%%%%%
function make_specplot2

% get the B2xP points
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rf=r;
im=inpmod;

% Get KxP points
load final/1351x1614/real_ab_filtp3_weight3_gs_dp1102_jack0_real_ab_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rk=r;

% get the CxP points
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rc=r;

ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');

% strip down to used spectra
n=[9,10,11,12,13,1,14,15];
rf=rf(n); rk=rk(n); rc=rc(n);

% calc the chi2 for 5 bandpowers
for i=1:9
  chibins{i}=[2:6]; % use the plotted bins
end
rc=calc_chi(rc,chibins,[],1);
rc=calc_devs(rc,chibins,1);

% the weighted bin centers
lc=r(1).lc; lc(1,:)=NaN;

clf; setwinsize(gcf,400,1000);
ms=5; % marker size
ms2=10;
f=0.5;

f1={'P30','P44','P70','P100','P143','BK150','P217','P353'};
byl=[-0.1,0.2;-0.1,0.2;-0.1,0.2;-0.01,0.05;-0.01,0.05;-0.01,0.05;-0.02,0.3;-0.02,0.3];
for i=1:8
  subplot_grid(8,2,1+(i-1)*2);
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  plot(im.l,im.Cs_l(:,3),'r');
  h1=errorbar2(lc(:,3)-5,rf(i).real(:,3),rf(i).derr(:,3),'b*');
  h2=errorbar2(lc(:,3)+5,rk(i).real(:,3),rk(i).derr(:,3),'rx');
  h3=errorbar2(lc(:,3)+0,rc(i).real(:,3),rc(i).derr(:,3),'k.');
  set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
  set(h3(2),'MarkerSize',ms2);
  set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
  set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
  hold off
  xlim([0,200]); ylim([-0.1,2]);
  ylabel(sprintf('BK150x%s',f1{i}));
  %ptitle(sprintf('%.1f\\sigma',-norminv(rc(i).ptet(3))));
  ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rc(i).rchisq(3),rc(i).rsumdev(3)))
  if(i==1)
    title('EE');
    [l,h]=legend([h3(2),h1(2),h2(2)],{'BKxP','BxP','KxP'},'Location','W');
    legend boxoff
    set(l,'Position',[0.0943,0.8415,0.1819,0.0611]);
    p=get(h(1),'position'); p(1)=p(1)-.2; set(h(1),'position',p);
    p=get(h(2),'position'); p(1)=p(1)-.2; set(h(2),'position',p);
    p=get(h(3),'position'); p(1)=p(1)-.2; set(h(3),'position',p);   
  end
  subplot_grid2(8,2,1+(i-1)*2);
  subplot_grid(8,2,2+(i-1)*2);
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  plot(im.l,ml.Cs_l(:,4),'r');
  h1=errorbar2(lc(:,4)-5,rf(i).real(:,4),rf(i).derr(:,4),'b*');
  h2=errorbar2(lc(:,4)+5,rk(i).real(:,4),rk(i).derr(:,4),'rx');
  h3=errorbar2(lc(:,4)+0,rc(i).real(:,4),rc(i).derr(:,4),'k.');
  set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
  set(h3(2),'MarkerSize',ms2);
  set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
  set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
  hold off
  xlim([0,200]);
  ylim(byl(i,:));
  %ptitle(sprintf('%.1f\\sigma',-norminv(rc(i).ptet(4))));
  ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rc(i).rchisq(4),rc(i).rsumdev(4)))
  if(i==1)
    title('BB');
  end
  subplot_grid2(8,2,2+(i-1)*2);
end
xlabel('Multipole')

print -depsc2 b2p_plots/spectra2.eps
fix_lines('b2p_plots/spectra2.eps')
!epstopdf b2p_plots/spectra2.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_specplot3
% plot the differences of B2 and K spectra
% - use the type7 dust only A_d=3.6 model

% get the B2xP points
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1100_jack0_real_a_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf_fgm7.mat
r=weighted_ellbins(r,bpwf);
rb=r;
im=inpmod;

% Get KxP points
load final/1351x1614/real_ab_filtp3_weight3_gs_dp1102_jack0_real_ab_filtp3_weight3_gs_dp1100_jack0_real_ab_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf_fgm7.mat
r=weighted_ellbins(r,bpwf);
rk=r;

% take the pairwise differences between the two files - we feed the
% simr into the sim so that when we take the chi2 below we are
% looking at the differences versus the differences in model which
% contains power comparable to the real data
for i=1:length(rb)
  rd(i).real=(rb(i).real-rk(i).real);
  rd(i).sim=(rb(i).simr-rk(i).simr);
  rd(i).noi=(rb(i).noi-rk(i).noi);
end

% drop all the useless elements of r
rd=rd([1,aps_getxind(length(rb),1,8)]);

% the weighted bin centers
lc=r(1).lc; lc(1,:)=NaN;

% calculate the chi2
chibins1=get_chibins;
for i=1:9
  chibins2{i}=2:10;
end
rd1=calc_chi(rd,chibins1,[],1); % 1=set expv to mean of sims
rd1=calc_devs(rd1,chibins1,1); % 1=set expv to mean of sims

rd2=calc_chi(rd,chibins2,[],1); % 1=set expv to mean of sims
rd2=calc_devs(rd2,chibins2,1); % 1=set expv to mean of sims

% try to reproduce relevant line of tab5 in K paper
disp(sprintf('BxB-KxK pte %.3f %.3f %.3f %.3f',...
             rd1(1).ptes(4),rd2(1).ptes(4),...
             rd1(1).ptes_sumdev(4),rd2(1).ptes_sumdev(4)));

% make analogous stats for diff of cross spec
disp(sprintf('BxP-KxP pte %.3f %.3f %.3f %.3f',...
             rd1(2).ptes(4),rd2(2).ptes(4),...
             rd1(2).ptes_sumdev(4),rd2(2).ptes_sumdev(4)));

clf; setwinsize(gcf,400,250);
ms=10; % marker size

%ns=size(rd(1).sim,3);
%ncdf=normcdf([-2:2],0,1);
%t=[floor(ns*ncdf(ncdf<0.5)),ceil(ns*ncdf(ncdf>=0.5))];

%subplot(1,2,1);
%plot(lc(:,4),squeeze(rd(1).sim(:,4,:)),'color',[0.7,0.7,0.7],'LineWidth',0.5);
%hold on
%sims=sort(rd(1).sim,3);
%plot(lc(:,4),squeeze(sims(:,4,t(3))),'b','LineWidth',1);
%plot(lc(:,4),squeeze(sims(:,4,t([2,4]))),'r','LineWidth',1);
%plot(lc(:,4),squeeze(sims(:,4,t([1,5]))),'g','LineWidth',1);
%h=errorbar2(lc(:,4),rd(1).real(:,4),std(rd(1).sim(:,4,:),[],3),'k+');
%set(h,'MarkerSize',10);
%hold off
%xlim([30,330]); ylim([-0.01,0.01]);
%title('BxB-KxK');

%subplot(1,2,2);
%plot(lc(:,4),squeeze(rd(2).sim(:,4,:)),'color',[0.7,0.7,0.7],'LineWidth',0.5);
%hold on
%sims=sort(rd(2).sim,3);
%plot(lc(:,4),squeeze(sims(:,4,t(3))),'b','LineWidth',1);
%plot(lc(:,4),squeeze(sims(:,4,t([2,4]))),'r','LineWidth',1);
%plot(lc(:,4),squeeze(sims(:,4,t([1,5]))),'g','LineWidth',1);
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(lc(:,4),rd(2).real(:,4),std(rd(2).sim(:,4,:),[],3),'k.');
set(h,'MarkerSize',10);
hold off
xlim([0,330]); ylim([-0.6,0.6]);
legend(h(2),{'B150xP353-K150xP353'},'Location','NW');
legend boxoff
xlabel('Multipole'); ylabel('BB l(l+1)C_l/2\pi [\muK^2]');
text(20,-0.5,sprintf('PTEs: 1-5 \\chi^2 %.3f, 1-9 \\chi^2 %.3f, 1-5 \\chi %.3f, 1-9 \\chi %.3f',...
                     rd1(2).ptes(4),rd2(2).ptes(4),...
                     rd1(2).ptes_sumdev(4),rd2(2).ptes_sumdev(4)));

print -depsc2 b2p_plots/spectra3.eps
fix_lines('b2p_plots/spectra3.eps')
!epstopdf b2p_plots/spectra3.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_specplot4

% make "dust cleaned" spectra

% get the B2xP points - use model which has A_d=2.5 for error bars
%load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1100_jack0_real_a_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf_fgm7.mat
%r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
%rb=r;
%im=inpmod;

% Get KxP points
%load final/1351x1614/real_ab_filtp3_weight3_gs_dp1102_jack0_real_ab_filtp3_weight3_gs_dp1100_jack0_real_ab_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf_fgm7.mat
%r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
%rk=r;

% get the CxP points
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf_fgm7.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rc=r;

ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');

% the weighted bin centers
ellc=r(1).lc; ellc(1,:)=NaN;

%sf=0.04;

%sf=1/24.63; % from Ganga post 8/14
sf= 0.0394589677 % from Colin email 12/17

% make scaled subtracted spectra
i=aps_getxind(length(rc),1,8);
%reab=(rb(1).real(:,4)-sf*rb(i).real(:,4))/(1-sf);
%simb=(rb(1).simr(:,4,:)-sf*rb(i).simr(:,4,:))/(1-sf);
%reak=(rk(1).real(:,4)-sf*rk(i).real(:,4))/(1-sf);
%simk=(rk(1).simr(:,4,:)-sf*rk(i).simr(:,4,:))/(1-sf);
reac=(rc(1).real(:,4)-sf*rc(i).real(:,4))/(1-sf);
simrc=(rc(1).simr(:,4,:)-sf*rc(i).simr(:,4,:))/(1-sf);
simc=(rc(1).sim(:,4,:)-sf*rc(i).sim(:,4,:))/(1-sf);

% get the fiducial r constraint
[loc,lc]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,0,1);

% get Colin HL based r constraint
%load /n/home05/cbischoff/work/2014_12/20141217_cleaning/mapclean_hl_fgm7_v4.mat

% get Colin final tight prior version - see email 1/21/15
load /n/home05/cbischoff/work/2015_01/20150121_cleaning/lik1d_cleaning_HL.mat

% get Challinor versions
%clp=load('B2K_rlike_cleanedGaussDet_noPxP_withdustprior.txt');
%ctp=load('B2K_rlike_cleanedGaussDet_noPxP_withdustprior_Ad2to7.txt');

figure(1); clf; setwinsize(gcf,400,600);
ms=10; % marker size

subplot(2,1,1)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
%plot(ellc(:,4),squeeze(simrc),'color',[0,0.7,0.7],'LineWidth',0.5);
%plot(ellc(:,4),squeeze(simc),'color',[0.7,0.7,0.7],'LineWidth',0.5);
plot(ml.l,ml.Cs_l(:,4),'r');
%h1=errorbar2(lc(:,4)-2,reab,std(simb,[],3),'b.');
%h2=errorbar2(lc(:,4)+2,reak,std(simk,[],3),'r.');
%set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
h1=errorbar2(ellc(:,4)-4,rc(1).real(:,4),std(rc(1).simr(:,4,:),[],3),'k.');
set(h1(1),'color',[0.5,0.5,0.5]);
errorbar(ellc(:,4)-4,rc(1).real(:,4),std(rc(1).sim(:,4,:),[],3),'k.');

h2=errorbar2(ellc(:,4)+4,reac,std(simrc,[],3),'b.');
set(h2(1),'color',[0.5,0.5,1]);
errorbar(ellc(:,4)+4,reac,std(simc,[],3),'b.');

set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-0.01,0.055]);
%legend([h1(2),h2(2)],{'(BxB-\alphaBxP)/(1-\alpha)','(KxK-\alphaKxP)/(1-\alpha)'},'Location','NW');
legend([h1(2),h2(2)],{'BKxBK','(BKxBK-\alphaBKxP)/(1-\alpha)'},'Location','NW');
legend boxoff
xlabel('Multipole'); ylabel('BB l(l+1)C_l/2\pi [\muK^2]')

subplot(2,1,2)
y=sum(sum(lc.l,1),3); h1=plot(lc.r,y./max(y),'k','LineWidth',2);
hold on
%h2=plot(lc.r-0.02,y./max(y),'b','LineWidth',2);
%h2=plot(r_range_comb_real,lik1d_comb_real./max(lik1d_comb_real),'b');
h3=plot(r,lik1./max(lik1),'b');
%h3=plot(clp(:,2),clp(:,3),'b');
%h4=plot(ctp(:,2),ctp(:,3),'m');
hold off
xlabel('r'); ylabel('L/L_{peak}');
xlim([0,0.3]); ylim([0,1.05]);
legend([h1,h3],{'Fiducial analysis','Cleaning analysis'});
legend boxoff
axis square

print -depsc2 b2p_plots/spectra4.eps
fix_lines('b2p_plots/spectra4.eps')
!epstopdf b2p_plots/spectra4.eps

keyboard

return

%%%%%%%%%%%%%%%%%%%%%
function make_likebase

[lob,lb]=dynchi2_grid_analysis(3,2:6,1,[],1,4,1,0,1);
[lok,lk]=dynchi2_grid_analysis(3,2:6,1,[],1,14,1,0,1);
[loc,lc]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,0,1);

figure(2)
clf; setwinsize(gcf,1000,250);

subplot(1,3,1);
y=sum(sum(lb.l,1),3); h1=plot(lb.r,y./max(y),'color',[0.5,0.5,1]);
hold on
y=sum(sum(lk.l,1),3); h2=plot(lb.r,y./max(y),'color',[1,0.5,0.5]);
y=sum(sum(lc.l,1),3); h3=plot(lb.r,y./max(y),'k','LineWidth',2);
hold off
xlabel('r'); ylabel('L/L_{peak}');
axis tight; ylim([0,1.05]);
legend([h3,h1,h2],{'BK+P','B+P','K+P'});
legend boxoff

% get the stats for r
y=sum(sum(lc.l,1),3); cons1d_stats(lc.r,y);
% and read the global ML model, chi2 and PTE off the plot

% find r=0 ML model
%[v,i]=maxmd(lc.l(:,1,:));
%mod=loc.mlmod;
%mod(1)=lc.r(i(2));
%mod(4)=lc.A_d(i(1));
%mod(7)=lc.beta(i(3));
%[ell,cmp]=model_rms(mod,loc);
%loc.bpcm=scale_bpcm(cmp,loc);
%loc.expv=like_getexpvals(mod,loc);

%d=cvec(loc.real');
%m=cvec(loc.expv');
%chi2=(d-m)'*inv(loc.bpcm)*(d-m);
%dof=sum(~isnan(d))-1;
%pte=1-chi2cdf(chi2,dof);
%disp(sprintf('chi^2=%.1f, pte=%.3f for %d dof',chi2,pte,dof));

subplot(1,3,2);
y=sum(sum(lb.l,2),3); plot(lb.A_d,y./max(y),'color',[0.5,0.5,1]);
hold on
y=sum(sum(lk.l,2),3); plot(lb.A_d,y./max(y),'color',[1,0.5,0.5]);
y=sum(sum(lc.l,2),3); plot(lb.A_d,y./max(y),'k','LineWidth',2);
hold off
xlabel('A_d @ l=80 & 353 GHz [\muK^2]'); ylabel('L/L_{peak}');
axis tight; ylim([0,1.05]);

% get the stats for A_d
y=sum(sum(lc.l,2),3); cons1d_stats(lc.A_d,y);

subplot(1,3,3);
if(0)
  y=sum(lb.l,3); mv=maxmd(y); contour(lb.r,lb.A_d,y,mv*exp(-([1,2]).^2/2),'color',[0.5,0.5,1]);
  hold on
  y=sum(lk.l,3); mv=maxmd(y); contour(lb.r,lb.A_d,y,mv*exp(-([1,2]).^2/2),'color',[1,0.5,0.5]);
  y=sum(lc.l,3); mv=maxmd(y); contour(lb.r,lb.A_d,y,mv*exp(-([1,2]).^2/2),'k','LineWidth',2);
else
  y=sum(lb.l,3); contourfrac(lb.r,lb.A_d,y,[0.68,0.95],'color',[0.5,0.5,1]);
  hold on
  y=sum(lk.l,3); contourfrac(lb.r,lb.A_d,y,[0.68,0.95],'color',[1,0.5,0.5]);
  y=sum(lc.l,3); contourfrac(lb.r,lb.A_d,y,[0.68,0.95],'k','LineWidth',2);
end
hold off
axis xy
xlabel('r'); ylabel('A_d @ l=80 & 353 GHz [\muK^2]');

print -depsc2 b2p_plots/likebase.eps
fix_lines('b2p_plots/likebase.eps')
!epstopdf b2p_plots/likebase.eps

keyboard

return

%%%%%%%%%%%%%%%%%%%%%
function make_likevar
% make plot comparing r-cons when varying model

% fiducial
[loc,lc1]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,0,1);

% 33=Y1xY2
[loc,lc2]=dynchi2_grid_analysis(3,2:6,1,[],1,33,1,0,1);

% drop 217
[loc,lc3]=dynchi2_grid_analysis(2,2:6,1,[],1,34,1,0,1);

% CxC and CxP353 only
[loc,lc4]=dynchi2_grid_analysis(12,2:6,1,[],1,34,1,0,1);

% base + 9 bins
[loc,lc5]=dynchi2_grid_analysis(3,2:10,1,[],1,34,1,0,1);

% include EE
%[loc,lc6]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,1,1);
% to use Planck halfxhalf splits noise bias kludge is required
% for paper v1 the recipe used required also to take the real part
% of the likelihood when including EE
% Now shown we can change the recipe such that the real val kludge
% is never required. This in principle changes all the results but
% only significantly when including EE. So for v2 of the paper just
% replace this one curve.
[loc,lc6]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,2,1);

% relax beta_d prior
[loc,lc7]=dynchi2_grid_analysis(3,2:6,1,[],2,34,1,0,1);
% shift assumed beta down/up - as quoted in paper
%[loc,lc7d]=dynchi2_grid_analysis(3,2:6,1,[],3,34,1,0,1);
%[loc,lc7u]=dynchi2_grid_analysis(3,2:6,1,[],4,34,1,0,1);
%y=sum(sum(lc7d.l,1),3); cons1d_stats(lc7d.r,y);
%y=sum(sum(lc7u.l,1),3); cons1d_stats(lc7u.r,y);

% use dynchi2
[loc,lc8]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,0,0);

% use lcdm+r=0.2 as HL fiducial
[loc,lc9]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,0,1,0,0,0,1);

save b2p_plots/likevar_dust.mat

figure(1)
clf; setwinsize(gcf,300,300);
y=sum(sum(lc1.l,1),3); h1=plot(lc1.r,y./max(y),'k','LineWidth',2);
hold on
y=sum(sum(lc2.l,1),3); h2=plot(lc2.r,y./max(y),'b');
y=sum(sum(lc3.l,1),3); h3=plot(lc3.r,y./max(y),'r');
y=sum(sum(lc4.l,1),3); h4=plot(lc4.r,y./max(y),'y');
y=sum(sum(lc5.l,1),3); h5=plot(lc5.r,y./max(y),'m');
y=sum(sum(lc6.l,1),3); h6=plot(lc6.r,y./max(y),'k--');
y=sum(sum(lc7.l,1),3); h7=plot(lc7.r,y./max(y),'c');
y=sum(sum(lc8.l,1),3); h8=plot(lc8.r,y./max(y),'g');
y=sum(sum(lc9.l,1),3); h9=plot(lc9.r,y./max(y),'r--');
hold off
xlabel('r'); ylabel('L/L_{peak}');
xlim([0,0.3]); ylim([0,1.05]);
legend([h1,h2,h3,h4,h5,h6,h7,h8,h9],{'Fiducial analysis','Y1xY2','no 217GHz',...
                   'Only BKxBK&BKxP353',...
                   '9 bandpowers','Inc. EE (EE/BB=2)',...
                   'relax \beta_d prior','Gauss det','alt. HL fid. model'});
legend boxoff
axis square

print -depsc2 b2p_plots/likevar.eps
fix_lines('b2p_plots/likevar.eps')
!epstopdf b2p_plots/likevar.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_sync
% compare to Y1xY2
[loc,lc]=dynchi2_grid_analysis(3,2:6,1,[],1,33,1,0,1);

% 7 means use all the freqs
[loc,lc1]=dynchi2_grid_analysis(7,2:6,1,[],1,33,1,0,1,0,1,0); % 0%
[loc,lc2]=dynchi2_grid_analysis(7,2:6,1,[],1,33,1,0,1,0,2,0); % 100%
[loc,lc3]=dynchi2_grid_analysis(7,2:6,1,[],1,33,1,0,1,0,3,0); % 50%
save b2p_plots/sync_dust.mat

figure(1)
clf; setwinsize(gcf,500,220);

subplot(1,2,1);
y=summd(lc.l,2); h1=plot(lc.r,y./max(y),'k');
hold on
y=summd(lc1.l,2); h2=plot(lc1.r,y./max(y),'b');
y=summd(lc2.l,2); h3=plot(lc2.r,y./max(y),'r');
y=summd(lc3.l,2); h4=plot(lc3.r,y./max(y),'g');
xlabel('r'); %ylabel('L/L_{peak}');
axis tight; ylim([0,1.05]);
%legend([h1,h2,h3],{'No Sync','+Sync uncorr.',...
%                   '+Sync 100% corr'});
legend([h1,h2,h3,h4],{'No Sync','+Sync uncorr.',...
                   '+Sync 100% corr','+Sync 50% corr'});
legend boxoff

subplot(1,2,2)
y=summd(lc1.l,4); plot(lc1.A_s,y,'b.-');
hold on
y=summd(lc2.l,4); plot(lc2.A_s,y,'r.-');
y=summd(lc3.l,4); plot(lc3.A_s,y,'g.-');
hold off


% look up 95% upper limit on A_s
y=summd(lc1.l,4); cons1d_stats(lc1.A_s,y);
y=summd(lc2.l,4); cons1d_stats(lc2.A_s,y);
y=summd(lc3.l,4); cons1d_stats(lc3.A_s,y);


subplot(1,2,2);
if(0)
  y=summd(lc1.l,[1,4]); mv=maxmd(y);
  contour(lc1.A_s,lc1.A_d,y,mv*exp(-([1,2]).^2/2),'b','LineWidth',2);
  hold on
  y=summd(lc2.l,[1,4]); mv=maxmd(y);
  contour(lc2.A_s,lc2.A_d,y,mv*exp(-([1,2]).^2/2),'r','LineWidth',2);
else
  y=summd(lc1.l,[1,4]);
  contourfrac(lc1.A_s,lc1.A_d,y,[0.68,0.95],'b','LineWidth',2);
  hold on
  y=summd(lc2.l,[1,4]);
  contourfrac(lc2.A_s,lc2.A_d,y,[0.68,0.95],'r','LineWidth',2);
  y=summd(lc3.l,[1,4]);
  contourfrac(lc3.A_s,lc3.A_d,y,[0.68,0.95],'g','LineWidth',2);
end
hold off
xlabel('A_{sync} @ l=80 & 150GHz [\muK^2]');
ylabel('A_d @ l=80 & 353GHz [\muK^2]');

print -depsc2 b2p_plots/sync.eps
fix_lines('b2p_plots/sync.eps')
!epstopdf b2p_plots/sync.eps

keyboard

return

%%%%%%%%%%%%%%%%%%%%%
function make_al

[loc,lc2]=dynchi2_grid_analysis(3,2:10,1,[],1,34,1,0,1,0,0,1);
save b2p_plots/alens_dust
%[loc,lc2]=dynchi2_grid_analysis(5,2:10,1,[],1,33,1,0,1,0,0,1);

figure(1)
clf; setwinsize(gcf,500,220);

subplot(1,2,1);
line([0,0.3],[0.94,0.94],'LineStyle',':','color','k'); box on
line([0,0.3],[1.04,1.04],'LineStyle',':','color','k');
y=summd(lc2.l,[2,5]);
hold on
%mv=maxmd(y);
%contour(lc2.r,lc2.A_L,y',mv*exp(-([1,2]).^2/2),'r','LineWidth',2);
contourfrac(lc2.r,lc2.A_L,y',[0.68,0.95],'r','LineWidth',2);
hold off
xlabel('r'); ylabel('A_L');
xlim([0,0.3]); ylim([0,1.75]);

subplot(1,2,2);
line([0,7],[0.94,0.94],'LineStyle',':','color','k'); box on
line([0,7],[1.04,1.04],'LineStyle',':','color','k');
y=summd(lc2.l,[1,5]);
hold on
%mv=maxmd(y);
%contour(lc2.A_d,lc2.A_L,y',mv*exp(-([1,2]).^2/2),'r','LineWidth',2);
contourfrac(lc2.A_d,lc2.A_L,y',[0.68,0.95],'r','LineWidth',2);
hold off
xlabel('A_d @ l=80 & 353GHz [\muK^2]'); ylabel('A_L');
xlim([0,7]); ylim([0,1.75]);

print -depsc2 b2p_plots/al.eps
fix_lines('b2p_plots/al.eps')
!epstopdf b2p_plots/al.eps

% marginalize over r and A_d and print stats
y=summd(lc2.l,5);
cons1d_stats(lc2.A_L,y);

keyboard

return

%%%%%%%%%%%%%%%%%%%%%
function make_simcons
% make a plot of many sim 1d r constraints with real overplotted

% due to bandpass change need to recompute real data case
[loc,lc]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,0,1);

% use same set of rlz as used for psm plot below -
% get the dust spectra
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf_fgm12.mat
% select on 353x353 ell=70 power level
d353=squeeze(r(8).simr(3,4,1:352));
c353=find(d353<20);

% thin down the plot
c353=c353(1:3:end);

figure(1)
clf; setwinsize(gcf,400,180);

%load /n/bicepfs2/users/vbuza/2014_11/20/farmfiles/b2_keck_comb_p217p353_r1d_fgm7.mat
load /n/bicepfs2/users/vbuza/2015_01/10/farmfiles/b2_keck_comb_p217p353_r1d_hl_fgm7_flv2.mat

%load /n/home05/cbischoff/work/2014_12/20141208_multicomp/mapclean_hl_fgm7_v1.mat
%lik1d_comb=lik1d_comb./repmat(max(lik1d_comb,[],2),[1,size(lik1d_comb,2)]);
%lik1d_comb_real=lik1d_comb_real./max(lik1d_comb_real);

subplot(1,2,1)
h1=plot(r_range,lik1d_comb(c353,:));
set(h1,'Color',[0.7,0.7,0.7]);
keyboard
hold on
%h2=plot(r_range,lik1d_comb_real,'r');
y=sum(sum(lc.l,1),3); h2=plot(lc.r,y./max(y),'k','LineWidth',2);
hold off
xlabel('r'); %ylabel('L/L_{peak}');
axis tight; ylim([0,1.05]);

% find fraction which peak above zero
[mv,ind]=max(lik1d_comb');
n=length(ind); m=sum(ind>1);
[m n]
f=sum(m/n)
pte=binocdf(m,n,0.5)

% find fraction which have L0/Lpeak less than real data
% (where Lpeak is already norm to 1)
%sum(lik1d_comb(:,1)<lik1d_comb_real(1))./size(lik1d_comb,1)
% bandpass change pushes L0/Lpeak for real data down a bit so hard
% code this
sum(lik1d_comb(:,1)<0.38)./size(lik1d_comb,1)

%load /n/bicepfs2/users/vbuza/2014_11/20/farmfiles/b2_keck_comb_p217p353_Ad1d_fgm7.mat
load /n/bicepfs2/users/vbuza/2015_01/10/farmfiles/b2_keck_comb_p217p353_Ad1d_hl_fgm7_flv2.mat

subplot(1,2,2)
%h1=plot(dust_range_comb,squeeze(lik1d_comb(140:280,:)),'k');
h1=plot(dust_range_comb,squeeze(lik1d_comb(c353,:)),'k');
set(h1,'Color',[0.7,0.7,0.7]);
hold on
%h2=plot(dust_range_comb_real,lik1d_comb_real,'r');
y=sum(sum(lc.l,2),3); h2=plot(lc.A_d,y./max(y),'k','LineWidth',2);
hold off
xlabel('A_d @ l=80 & 353GHz [\muK^2]'); %ylabel('L/L_{peak}');
axis tight; xlim([0,15]); ylim([0,1.05]);

print -depsc2 b2p_plots/simcons.eps
fix_lines('b2p_plots/simcons.eps')
!epstopdf b2p_plots/simcons.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_psm
% make a plot of many sim 1d r constraints for psm

%load /n/bicepfs2/users/vbuza/2014_12/11/farmfiles/b2_keck_comb_p217p353_Ad1d_hl_fgm10.mat
%load /n/bicepfs2/users/vbuza/2014_12/14/farmfiles/b2_keck_comb_p217p353_Ad1d_hl_fgm10_beta13.mat
load /n/bicepfs2/users/vbuza/2014_12/14/farmfiles/b2_keck_comb_p217p353_Ad1d_hl_fgm12.mat
% this version has HL fid mod update - not in PRL version - come
% back to this
%load /n/bicepfs2/users/vbuza/2015_01/21/farmfiles/b2_keck_comb_p217p353_Ad1d_hl_fgm12_flv2.mat
ld=squeeze(lik1d_comb)';
ldr=lik1d_comb_real;

%load /n/bicepfs2/users/vbuza/2014_12/11/farmfiles/b2_keck_comb_p217p353_r1d_hl_fgm10.mat
%load /n/bicepfs2/users/vbuza/2014_12/14/farmfiles/b2_keck_comb_p217p353_r1d_hl_fgm10_beta13.mat
load /n/bicepfs2/users/vbuza/2014_12/14/farmfiles/b2_keck_comb_p217p353_r1d_hl_fgm12.mat
%load /n/bicepfs2/users/vbuza/2015_01/21/farmfiles/b2_keck_comb_p217p353_r1d_hl_fgm12_flv2.mat
lr=lik1d_comb';
lrr=lik1d_comb_real;

[mv,ind]=max(ld); dp=dust_range_comb(ind);
[mv,ind]=max(lr); rp=r_range(ind);

% get the dust spectra
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf_fgm12.mat
% select on 353x353 ell=70 power level - would be better to use the
% actual dust contribution - this has noise in it
d353=squeeze(r(8).simr(3,4,1:352));
c353=find(d353<20);

% thin down the plot
c353=c353(1:3:end);

figure(1)
setwinsize(gcf,400,180);
clf;

subplot(1,2,1)
h1=plot(r_range,lr(:,c353),'k');
set(h1,'Color',[0.7,0.7,0.7]);
hold on
%h2=plot(r_range,lrr,'k','LineWidth',2);
hold off
xlabel('r'); %ylabel('L/L_{peak}');
axis tight; ylim([0,1.05]);

subplot(1,2,2)
h1=plot(dust_range_comb,ld(:,c353),'k');
set(h1,'Color',[0.7,0.7,0.7]);
hold on
%h2=plot(dust_range_comb_real,ldr,'k','LineWidth',2);
% highlight the field closest to BK field
%plot(dust_range_comb,ld(:,48),'r');
hold off
xlabel('A_d @ l=80 & 353GHz [\muK^2]'); %ylabel('L/L_{peak}');
axis tight; ylim([0,1.05]);
xlim([0,15]);

[mv,ind]=max(lr(:,c353));
[mv,indr]=max(lrr);
n=length(ind); m=sum(ind>1);
[m n]
f=sum(m/n)
pte=binocdf(m,n,0.5)

% find fraction which have L0/Lpeak less than real data
% (where Lpeak is already norm to 1)
% bandpass change pushes L0/Lpeak for real data down a bit so hard
% code this
sum(lr(1,c353)<0.38)./sum(c353)

print -depsc2 b2p_plots/psm.eps
fix_lines('b2p_plots/psm.eps')
!epstopdf b2p_plots/psm.eps

keyboard

return

%%%%%%%%%%%%%%%%%%%%%
function make_noilev
% make a plot summarizing noise levels

% run the standard analysis to get the expd vals for dust
% (this is unncessary - could strip out the needed calls)
%[loc,lc]=dynchi2_grid_analysis(4,2:6,1,[],1,34,1,0,1);

% get the file which contains a lot of what we need
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf.mat
rc=r;
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack3_pureB_matrix_directbpwf.mat
rc3=r;

f=[150,30,44,70,100,143,217,353];

figure(1); clf;
setwinsize(gcf,400,300);
%setwinsize(gcf,1000,800);

hold on

% plot dust/sync as fine spaced curves
ff=linspace(f(2),f(end),100);
for i=1:numel(ff)
  sc(i)=0.0003*freq_scaling([ff(i),1],-3.3,[],150)^2;
  %scu(i)=0.0004*freq_scaling([ff(i),1],-3.0,[],150)^2;
  dcu(i)=(3.3+0.9)*freq_scaling([ff(i),1],1.59,19.6,353)^2;
  dcl(i)=(3.3-0.8)*freq_scaling([ff(i),1],1.59,19.6,353)^2;
  dc(i)=(3.3+0.0)*freq_scaling([ff(i),1],1.59,19.6,353)^2;
end
% plot sync shaded region
patch([ff,ff(end),ff(1)],[sc,1e-9,1e-9],[0.7,0.7,1],'EdgeColor','none');
text(42,2e0,'Sync upper limit','Rotation',-72);

% plot alt sync limit for bu^-3.0
%plot(ff,scu,'b--');

% plot dust shaded region
patch([ff,fliplr(ff)],[dcu,fliplr(dcl)],[0.7,0.7,0.7],'EdgeColor','none');
text(250,0.135,'Dust','Rotation',35);

% plot smooth center dust curve
%plot(ff,dc,'k');

set(gca,'yscale','log'); box on

% find nominal gmean freqs
for i=1:7
  fgm(i)=gmean(f(1),f(i+1));
end
% manually tune to push "points onto curves"
fgmp=fgm+[-4,-3.5,-3,+1,0,+2.5,+20];

% plot gmean dust/sync cross spectral points
% find coarse dust/sync points
%for i=1:numel(f)
%  sc(i)=0.0002*freq_scaling([f(i),1],-3.3,[],150)^2;
%  dc(i)=2.9*freq_scaling([f(i),1],1.59,19.6,353)^2;
%end
%for i=1:7
%  plot(fgm(i),gmean(dc(1),dc(i+1)),'kx');
%  plot(fgmp(i),gmean(dc(1),dc(i+1)),'k.');
%  plot(fgm(i),gmean(sc(1),sc(i+1)),'bx');
%  plot(fgmp(i),gmean(sc(1),sc(i+1)),'b.');
%end

% plot the noise bandpower uncertainties in BB ell=70 bin
bkn=std(rc(1).noi(3,4,:),[],3);
h1=semilogy(f(1),bkn,'k*');
text(160,4.7e-4,'BK noise uncer.')
for i=1:7
  % get Planck autos
  j=aps_getxind(length(rc),i+1,i+1);
  p(i)=std(rc(j).noi(3,4,:),[],3);
  % get halfxhalf cross spectra
  % LFI from Y1xY2 and HFI from DS1xDS2...
  j=aps_getxind(length(rc),i*2+7,i*2+8);
  pc4(i)=std(rc(j).noi(3,4,:),[],3);
  pc3(i)=std(rc3(j).noi(3,4,:),[],3);
  % get BKxP
  j=aps_getxind(length(rc),1,i+1);
  c(i)=std(rc(j).noi(3,4,:),[],3);
end
% plot single freq noise uncer curve
%plot(f(2:8),p,'r.-'); % auto
%plot(f(2:8),pc4,'c.-'); % DS1xDS2
%plot(f(2:8),pc3,'m.-'); % Y1xY2
plot(f(2:8),[pc3(1:3),pc4(4:7)],'r.-'); % hybrid
text(100,0.2,{'Planck noise uncer.','     in BK field'},'Rotation',0);

% plot regular cross spectral noise curve
%plot(fgm,c,'c.-');
% plot stretched cross spectral noise curve
plot(fgmp,c,'b.-');
text(255,2e-2,'BKxP noise uncer.');

% llcdm and r are same values at all ell
xl=xlim;
llcdm=mean(rc(1).sim(3,4,:),3);
rp2=mean(rc(1).simr(3,4,:),3)-llcdm;
h3=line(xl,[llcdm,llcdm],'color','g');
text(260,1e-3,'lensed-LCDM');
h4=line(xl,[rp2,rp2]/4,'color','m');
text(260,4e-3,'r=0.05');

if(1)
  % plot the K14_95 noise level
  % switch to matrix when available but will make no difference
  load final/1351/real_d_filtp3_weight3_gs_dp1102_jack01_pureB_overfreq.mat
  k1495n=std(r(1).noi(3,4,:),[],3);
  plot(95,k1495n,'r*');
  plot(gmean(95,f(1)),gmean(k1495n,bkn),'m*')
end

hold off
xlim([0,370]); ylim([1e-4,1e1]);

xlabel('Nominal band center [GHz]');
ylabel('BB in l\sim80 bandpower l(l+1)C_l/2\pi [\muK^2]')

print -depsc2 b2p_plots/noilev.eps
fix_lines('b2p_plots/noilev.eps')
!epstopdf b2p_plots/noilev.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_speccomp

% plot comparison of spectra
p=load('Consistency_B2xP353_PureCl_499.txt');
x=load('Consistency_B2xP353_XPOL_499.txt');

% they applied dust scaling - undo
% (see Ludo email 1/21/15)
p(:,2:3)=p(:,2:3)*24.63;
x(:,2:3)=x(:,2:3)*24.63;

% get the sim sets
ps=load('MC_B2xP353_PureCl-B2.txt');
xs=load('MC_B2xP353_XPOL-B2.txt');
ps=ps'*24.63;
xs=xs'*24.63;

% get the real data as template (and for ell values)
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf.mat
r=weighted_ellbins(r,bpwf);
rc=r;
lc=r(1).lc; lc(1,:)=NaN;

% feed real and sims into r struct
r=r(1:2);
r(1).real(2:10,4)=p(:,2);
r(2).real(2:10,4)=x(:,2);
r(1).sim(2:10,4,:)=ps;
r(2).sim(2:10,4,:)=xs;

% calculate the chi2 and chi values
chibins1=get_chibins;
for i=1:9
  chibins2{i}=2:10;
end
r1=calc_chi(r,chibins1,[],1); % 1=set expv to mean of sims
r1=calc_devs(r1,chibins1,1); % 1=set expv to mean of sims
r2=calc_chi(r,chibins2,[],1); % 1=set expv to mean of sims
r2=calc_devs(r2,chibins2,1); % 1=set expv to mean of sims

figure(1);
clf; setwinsize(gcf,400,250);

line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h1=errorbar2(lc(:,4)-3,r1(1).real(:,4),r1(1).derr(:,4),'b.');
set(h1,'MarkerSize',10);
h2=errorbar2(lc(:,4)+3,r1(2).real(:,4),r1(2).derr(:,4),'r.');
set(h2,'MarkerSize',10);
hold off
xlim([0,330]); ylim([-0.6,0.6]);
xlabel('Multipole'); ylabel('BB l(l+1)C_l/2\pi [\muK^2]');
legend([h1(2),h2(2)],{'Std-PureCl','Std-XPOL'},'Location','NW');
legend boxoff
text(20,-0.4,sprintf('PTEs: 1-5 \\chi^2 %.3f, 1-9 \\chi^2 %.3f, 1-5 \\chi %.3f, 1-9 \\chi %.3f',...
                     r1(1).ptes(4),r2(1).ptes(4),...
                     r1(1).ptes_sumdev(4),r2(1).ptes_sumdev(4)));
text(20,-0.5,sprintf('PTEs: 1-5 \\chi^2 %.3f, 1-9 \\chi^2 %.3f, 1-5 \\chi %.3f, 1-9 \\chi %.3f',...
                     r1(2).ptes(4),r2(2).ptes(4),...
                     r1(2).ptes_sumdev(4),r2(2).ptes_sumdev(4)));

% note that there is strong anticorrelation of adjacent bins in the
% std-XPOL set - this means that sumdevs varies much less than for
% uncorrelated numbers and the chi PTE are smaller than they look
% like they should be
%imagesc(corr(squeeze(r2(2).sim(2:6,4,:))')); caxis([-1,1]); colorbar

print -depsc2 b2p_plots/powspeccheck.eps
fix_lines('b2p_plots/powspeccheck.eps')
!epstopdf b2p_plots/powspeccheck.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fid = open_data_file_and_print_header(filename, day)
  %filename should contain '%s' where date should go
  filename = sprintf(filename, datestr(day, 'yyyymmdd'));
  fid = fopen(filename, 'w');
  fprintf(fid, '# BICEP2/Keck Array and Planck Joint Analysis January 2015 Data Products\n');
  fprintf(fid, '# The BICEP2/Keck and Planck Collaborations, A Joint Analysis of BICEP2/Keck Array and Planck Data\n');
  fprintf(fid, '# http://bicepkeck.org/\n#\n');
  fprintf(fid,'# File: ');
  fprintf(fid, filename);
  fprintf(fid, ' \n');
  fprintf(fid, '# Date: ');
  fprintf(fid, datestr(day, 'yyyy-mm-dd'));
  fprintf(fid, '\n#\n');
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_powspecrestable
  for i=1:9
    chibins{i}=[2:10]; % use all plotted bins
  end
  [r, im, ml] = get_bkxp();

  %Get the cross with P353
  i=aps_getxind(length(r),1,8);
  r=r([1,i]);


  filename = 'BKP_bandpowers_150x353_%s.txt';
  day = '30-Jan-2015';
  fh = open_data_file_and_print_header(filename, day);

  fprintf(fh,'# BICEP2/Keck/Planck combined bandpowers \n');
  fprintf(fh,'# This text file contains BICEP2/Keck Array/Planck TT, TE, EE, and BB bandpowers [ell*(ell+1)*C_{ell}/(2*pi)] and uncertainties. \n');
  fprintf(fh,'# This file contains cross-frequency spectra between BICEP2/Keck maps at 150 GHz and Planck maps at 353 GHz, corresponding to Figure 2 (middle column) of A Joint Analysis of BICEP2/Keck Array and Planck Data.\n');
  fprintf(fh,'# Uncertainties are statistical only, the standard deviation of the constrained lensed-ΛCDM+noise simulations. \n');
  fprintf(fh,'# Uncertainties are calculated as the square root of diagonal elements of the bandpower covariance matrix. \n');
  fprintf(fh,'# The nature of the simulations constrains T to match the observed sky. Thus TT and TE uncertainties do not include appropriate sample variance.\n');
  fprintf(fh, '# Sample variance for any additional BB signal is not included either.\n');
  fprintf(fh, '# For TE we give only the result with T modes from Planck and E modes from BICEP2/Keck Array.\n');
  fprintf(fh,'# The units are uK^2. \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, dTT, dTE, dEE, dBB  \n');

  for i = chibins{1}
    lmin = r(2).l(i) - 35/2 +1;
    lmax = r(2).l(i) + 35/2;
    fprintf(fh,['%3d  %3.1f  %3d   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e\n'],lmin, r(2).lc(i), lmax, ...
      r(2).real(i,1), r(2).real(i,7), r(2).real(i,3:4), ...
      r(2).derr(i,1), r(2).derr(i,7), r(2).derr(i,3:4) ); % Use BK_ExP_T for TE
  end
  fclose(fh);

  %PxP 353 cross spectra
  [r, im, ml] = get_pxp_DSsplit();
  filename = 'BKP_bandpowers_353x353_%s.txt';
  day = '30-Jan-2015';
  fh = open_data_file_and_print_header(filename, day);
  fprintf(fh,'# BICEP2/Keck/Planck combined bandpowers \n');
  fprintf(fh,'# This text file contains BICEP2/Keck Array/Planck TT, TE, EE, and BB bandpowers [ell*(ell+1)*C_{ell}/(2*pi)] and uncertainties. \n');
  fprintf(fh,'# This file contains Planck 353 GHz cross-spectra from the detector set data split, corresponding to Figure 2 (right column) of A Joint Analysis of BICEP2/Keck Array and Planck Data.\n');
  fprintf(fh,'# Uncertainties are statistical only, the standard deviation of the constrained lensed-ΛCDM+noise simulations. \n');
  fprintf(fh,'# Uncertainties are calculated as the square root of diagonal elements of the bandpower covariance matrix. \n');
  fprintf(fh,'# The nature of the simulations constrains T to match the observed sky. Thus TT and TE uncertainties do not include appropriate sample variance.\n');
  fprintf(fh, '# Sample variance for any additional BB signal is not included either.\n');
  fprintf(fh,'# The units are uK^2. \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, dTT, dTE, dEE, dBB  \n');
  for i = chibins{1}
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,['%3d  %3.1f  %3d   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e\n'],lmin, r.lc(i), lmax, ...
      r.real(i,1:4), r.derr(i,1:4) ); 
  end
  fclose(fh);

  [r, im, ml] = get_bkxp();
  r = r(9:15); % Get only BKxP cross spectra
  filename = 'BKP_bandpowers_EEBB_%s.txt';
  day = '30-Jan-2015';
  fh = open_data_file_and_print_header(filename, day);
  fprintf(fh,'# BICEP2/Keck/Planck combined bandpowers \n');
  fprintf(fh,'# This text file contains BICEP2/Keck Array/Planck EE and BB bandpowers [ell*(ell+1)*C_{ell}/(2*pi)] and uncertainties. \n');
  fprintf(fh,'# This file contains cross-spectra between BICEP2/Keck maps and all of the polarized frequencies of Planck, corresponding to Figure 3 of A Joint Analysis of BICEP2/Keck Array and Planck Data.\n');
  fprintf(fh,'# Uncertainties are statistical only, the standard deviation of the constrained lensed-ΛCDM+noise simulations. \n');
  fprintf(fh,'# Uncertainties are calculated as the square root of diagonal elements of the bandpower covariance matrix. \n');
  fprintf(fh, '# Sample variance for any additional BB signal is not included.\n');
  fprintf(fh,'# The units are uK^2. \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, EE, BB, dEE, dBB\n');
  freq_names = {'30','44','70','100','143','217','353'};  
  for iFreq = 1:numel(freq_names)
    fprintf(fh, '\n# BKxPlanck%s\n', freq_names{iFreq});
    for i = chibins{1}
      lmin = r(iFreq).l(i) - 35/2 +1;
      lmax = r(iFreq).l(i) + 35/2;
      fprintf(fh,['%3d  %3.1f  %3d   %1.2e   %1.2e   %1.2e   %1.2e\n'],lmin, r(iFreq).lc(i), lmax, ...
        r(iFreq).real(i,3:4), r(iFreq).derr(i,3:4) ); 
    end
  end
  fclose(fh);
    

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function make_cosmomc_tarball
  make_cosmomc_dataset('detset', 'comb', 'dust');
  make_cosmomc_dataset('year', 'comb', 'dust');
  system_safe([sprintf('tar --exclude-vcs -czf BKP_likelihood_cosmomc_%s.tgz BKPlanck_detset*', ...
    datestr(date, 'yyyymmdd')) ' BKPlanck_year* windows/BKPlanck* bandpass_*' ...
    ' -C ~/cosmomcb2keck/batch2 BKPlanck/' ...
    ' BKPlanck.ini BKPlanckonly.ini BKPlanck_README.txt' ...
    ' -C ~/cosmomcb2keck/source CMB_BK_Planck.f90'
    ]);
return 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function make_cosmomc_EB_tarball
  make_cosmomc_dataset('detset', 'comb', 'dust', {'E', 'B'});
  make_cosmomc_dataset('year', 'comb', 'dust', {'E', 'B'});
  system_safe([sprintf('tar --exclude-vcs -czf BKP_likelihood_cosmomc_%s.tgz BKPlanck_detset*', ...
    datestr(date, 'yyyymmdd')) ' BKPlanck_year* windows/BKPlanck* bandpass_*' ...
    ' -C ~/cosmomcb2keck/batch2 BKPlanck/' ...
    ' BKPlanck.ini BKPlanckonly.ini BKPlanck_README.txt' ...
    ' -C ~/cosmomcb2keck/source CMB_BK_Planck.f90'
    ]);
return 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_rlikelihood_file
  [loc,lc]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,0,1);
  y=sum(sum(lc.l,1),3);

  filename = 'BKP_r_likelihood_%s.txt';
  day = '23-Jan-2015';
  fh = open_data_file_and_print_header(filename, day);
  format shortE
  fprintf(fh, '# BICEP2/Keck Array/Planck combined likelihood for r\n');
  fprintf(fh, '# This text file contains the tabulated likelihood for the tensor-to-scalar ratio, r, derived from the BICEP2/Keck and Planck maps.\n');
  fprintf(fh, '# Calculated via the "multi-component likelihood" method described in Section III B of A Joint Analysis of BICEP2/Keck Array and Planck Data.\n');
  fprintf(fh, '# This likelihood curve corresponds to the black curve from the left panel of Figure 6 from A Joint Analysis of BICEP2/Keck Array and Planck Data.\n');
  fprintf(fh, '#\n# Columns: r, Likelihood(r)\n');
  fprintf(fh, '  %5.3f\t%6.4e\n', [lc.r; y./max(y)]);

  fclose(fh);
return



