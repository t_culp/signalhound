function chi2_pager(fname,labelname)
% chi2_pager(fnames,labelnames)
% make pager plots and html in directory ./chi2_pager
% specify jack0 filename
%
% i.e. chi2_pager('0304real_filtp3_weight3_gs_jack0_debias','0304')
%      chi2_pager({'0304real_filtp3_weight3_gs_jack0_debias','1204real_filtp3_weight3_gs_jack0_debias0304'},{'0304','1204_debias0304'})
%      chi2_pager({'0304real_filtp3_weight3_gs_jack0_debias','1204real_filtp3_weight3_gs_jack0_debias0304',1204real_filtp3_weight3_gs_jack0_debias0'},{'A','B','C'})
%

if(~iscell(fname))
  fname={fname};
  labelname={labelname};
end

close all
set(0,'defaultfigurevisible','off')

unix('rm -rf chi2_pager');
unix('mkdir chi2_pager');

% figures to print
figs=[1,2,3,4];

makeN(figs,500,fname,labelname); % lmax=500
makeN(figs,200,fname,labelname); % lmax=200

set(0,'defaultfigurevisible','on')

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% make plots and pages
function makeN(n,lmax,fname,labelname)

% Plot link names
pl={'spectra','bp devs','chisq','mean bp devs'};

% Jackkinfe link names
jkn={'sig','dk jack','scan jack','tag-split jack','tile jack','phase jack (b/c/e vs f/h/i)','mux col jack','alt dk jack','mux row jack'};

% Jack numbers
jks=[0,1,2,3,4,5,6,7,8];

% We specified jack0 in the filename but we want to include all jacks
for i=1:numel(fname)
  fname{i}=strrep(fname{i},'jack0','jack%1d');
end

% for each reduc_final file
for k=1:numel(fname)

  % for each jack
  for jk=jks
    
    ff=fname{k};
    
    % shave off any .mat
    ff=strrep(ff,'.mat','');

    % get rid of any debias filename extensions like '_debias' or '_debias0304' if
    % requesting a jackknife
    if jk~=0
      % where are the underscores
      ind1=strfind(ff,'_');
      
      % find the debias flag
      ind2=strfind(ff,'debias');

      % get rid of any strings like 'debias' or 'debias_' or 'debias0304_'
      if ~isempty(ind2)
        % next underscore after 'debias'
        ind3=min(ind1(ind1>ind2));
        if ~isempty(ind3)
          ff=[ff(1:ind2-2) ff(ind3:end)];
        else
          ff=[ff(1:ind2-2)];
        end
      end
    end
    
    % make plots
    reduc_final_chi2(sprintf(ff,jk),0,lmax);

    % for each figure print to file and write HTML
    for fig=n
      
      % write figure 
      figure(fig);
      mkpng(sprintf('chi2_pager/%s_fig%1d_l%03d_jk%1d.png',labelname{k},fig,lmax,jk));
      % printfig(fig, sprintf('chi2_pager/%s_fig%1d_l%03d_jk%1d.gif',labelname{k},fig,lmax,jk),'pp');
      
      % open HTML file for this figure
      fh=fopen(sprintf('chi2_pager/%s_fig%1d_l%03d_jk%1d.html',labelname{k},fig,lmax,jk),'w');
      
      fprintf(fh,'<a href="../">up</a> ------ ');
      
      % write figure types html links
      for m=n
        if(m~=fig)
          fprintf(fh,'<a href="%s_fig%1d_l%03d_jk%1d.html">%s</a>, ',labelname{k},m,lmax,jk,pl{m});
        else
          fprintf(fh,'%s, ',pl{m});
        end
      end
      fprintf(fh,'\n');
      
      % write jack types html links
      fprintf(fh,'------ ');
      for m=1:numel(jks)
        if(jks(m)~=jk)
          fprintf(fh,'<a href="%s_fig%1d_l%03d_jk%1d.html">%s</a>, ',labelname{k},fig,lmax,jks(m),jkn{1+jks(m)});
        else
          fprintf(fh,'%s, ',jkn{1+jks(m)});
        end
      end
      fprintf(fh,'\n');
      
      % Choose lmax 
      fprintf(fh,'------ ');
      switch lmax
       case 500
        fprintf(fh,'<a href="%s_fig%1d_l%03d_jk%1d.html">lmax=200</a>',labelname{k},fig,200,jk);
       case 200
        fprintf(fh,'<a href="%s_fig%1d_l%03d_jk%1d.html">lmax=500</a>',labelname{k},fig,500,jk);        
      end
      
      % Choose set
      fprintf(fh,'------ <br>');
      for m=1:length(fname)
        if m~=k
          fprintf(fh,'<a href="%s_fig%1d_l%03d_jk%1d.html">%s</a>, ',labelname{m},fig,lmax,jk,labelname{m});
        else
          fprintf(fh,'%s, ',labelname{m});
        end
      end
      fprintf(fh,'\n');
      
      % include the figure
      fprintf(fh,'<p><a href="%s_fig%1d_l%03d_jk%1d.png">',labelname{k},fig,lmax,jk);
      fprintf(fh,'<img src="%s_fig%1d_l%03d_jk%1d.png" height="750"></a>\n',labelname{k},fig,lmax,jk);
      
      fclose(fh);
    end
  end
end

return
