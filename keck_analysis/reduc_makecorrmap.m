function map=reduc_makecorrmap(runnum,ad,apsfile,pl)
% map=reduc_makecorrmap(runnum,ad,apsfile,pl)
%
% Make simulated T&E maps which have proper correlations
% Translate to T,Q,U maps and save
%
% e.g.
% reduc_makecorrmap(200,'quad05')
% map=reduc_makecorrmap([],'bicep','aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits')

if(~exist('usr'))
  usr=[];
end
if(~exist('apsfile'))
  apsfile=[];
end
if(~exist('pl'))
  pl=0;
end

if(isempty(usr))
  usr='';
end
if(isempty(apsfile))
  apsfile='cmbfast_output.txt';
end

% try to make random numbers truly random
% Had some weird problems with identical maps - make 1e6 rather than
% 100...
randn('state',sum(1e6*clock));
randnstate=randn('state');

% make I/F plane info structure
if(ischar(ad))
  switch type
    % 20 deg field suitable for quad sims
    case {'quad05','quad0506'}
      ad=calc_ad(1024*0.02,1024);
      
      % big field for spud sims
    case 'spud'
      ad=calc_ad(1024*0.07,1024);
      
      % bicep1
    case 'bicep'
      ad=calc_ad(512*.25,512)
  end
end

% for convenience
ad.l_r=ad.u_r*2*pi;
ad.l_val=ad.u_val*2*pi;

% get cmbfast model
if isstr(apsfile)
  mod=load_cmbfast(apsfile);
else
  mod=apsfile
end

% avoid NAN in first element
if isnan(mod.C_l(1,:))
  mod.l=mod.l(2:end); mod.C_l=mod.C_l(2:end,:);
end

% scale for F plane spacing
C_u=mod.C_l/ad.del_u^2;

% make gaussian rand numbers with real ft
r1=randn_ft_of_real(ad.N_pix);
r2=randn_ft_of_real(ad.N_pix);
r3=randn_ft_of_real(ad.N_pix);

% preallocate T, E and B fourier plane arrays
Tf=zeros(ad.N_pix);
Ef=zeros(ad.N_pix);
Bf=zeros(ad.N_pix);

% Make T and E fourier plane arrays
for i=1:length(mod.l)
  
  % Setup desired covariance matrix
  % Note that off-diag elements can be complex. This represents
  % covariance between the real and imaginary parts. Presumably for
  % the present case this should be zero.
  X=[C_u(i,1),C_u(i,2);C_u(i,2),C_u(i,3)];
  if(~all(X(:)==0))
    CD=chol(X);
  else
    CD=zeros(size(X));
  end
  
  % find the relevant fourier modes
  ind=find(abs(ad.l_r-mod.l(i))<0.5);
  
  % mix them
  if(length(ind>0))
    x1=r1(ind);
    x2=r2(ind);
    x3=r3(ind);
    
    y=[x1,x2]*CD;
    yb=x3*sqrt(C_u(i,4)); %B doesn't correlate to T or E
    
    Tf(ind)=y(:,1);
    Ef(ind)=y(:,2);
    Bf(ind)=yb;
  end
end

if(pl==1)
  figure(1)
  subplot(2,2,1);
  imagesc(ad.l_val,ad.l_val,real(Tf)); axis square
  subplot(2,2,2);
  imagesc(ad.l_val,ad.l_val,real(Ef)); axis square
  subplot(2,2,3);
  imagesc(ad.l_val,ad.l_val,real(Tf).*real(Ef)); axis square
  subplot(2,2,4);
  imagesc(ad.l_val,ad.l_val,real(Bf)); axis square  
  
  % Re-bin to check it works
  figure(2)
  [bc,mu]=hprof(ad.l_r,Tf.*conj(Tf)*ad.del_u^2,50,-0.5,1500.5);
  subplot(2,2,1); semilogy(bc,mu); hold on; plot(mod.l,mod.C_l(:,1),'r'); hold off;
  [bc,mu]=hprof(ad.l_r,Ef.*conj(Ef)*ad.del_u^2,50,-0.5,1500.5);
  subplot(2,2,2); semilogy(bc,mu); hold on; plot(mod.l,mod.C_l(:,3),'r'); hold off;
  
  % hprof is throwing away the imaginary part of the complex data being
  % fed to it here, but in fact the imaginary part is real/imag cross
  % terms which sum to zero for the present case so the result is OK.
  [bc,mu]=hprof(ad.l_r,Tf.*conj(Ef)*ad.del_u^2,50,-0.5,1500.5);
  subplot(2,2,3); plot(bc,mu); hold on; plot(mod.l,mod.C_l(:,2),'r');

  [bc,mu]=hprof(ad.l_r,Bf.*conj(Bf)*ad.del_u^2,50,-0.5,1500.5);
  subplot(2,2,4); loglog(bc,mu); hold on; plot(mod.l,mod.C_l(:,4),'r'); hold off;

end

% Make maps
map.T=f2i(ad,Tf);
map.E=f2i(ad,Ef);
map.B=f2i(ad,Bf);

% Convert E/B to Q,U in F plane
[u,v]=meshgrid(ad.u_val,ad.u_val);
[Qf,Uf]=eb2qu(u,v,Ef,Bf,'iau'); 

% Make Q,U maps
map.Q=f2i(ad,Qf);
map.U=f2i(ad,Uf);

map.ad=ad;
map.randnstate=randnstate;
map.mod=mod;
map.apsfile=apsfile;

if(nargout==0)
  save(sprintf('corrmaps/corrmap%06d',runnum),'map');
end

if(pl==1)
  figure(3)
  setwinsize(gcf, 800, 500);
  subplot(2,3,1);
  imagesc(ad.t_val_deg,ad.t_val_deg,map.T); colorbar; axis square; axis xy
  title('T aka I')
  subplot(2,3,2);
  imagesc(ad.t_val_deg,ad.t_val_deg,map.E); colorbar; axis square; axis xy
  title('E')
  subplot(2,3,3);
  imagesc(ad.t_val_deg,ad.t_val_deg,map.B); colorbar; axis square; axis xy
  title('B')
  subplot(2,3,4);
  imagesc(ad.t_val_deg,ad.t_val_deg,map.Q); colorbar; axis square; axis xy
  title('Q')
  subplot(2,3,5);
  imagesc(ad.t_val_deg,ad.t_val_deg,map.U); colorbar; axis square; axis xy
  title('U')
end

return
