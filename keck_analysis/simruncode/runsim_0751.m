function runsim_0751(nbase,doreal,donoisesim,dorealcoadd,donoise,dosim,docoadd,rlz,docombmaps,queue,farmdps,usecompiled,simtypes,submit,daughter)
% runsim_0751(nbase,doreal,donoisesim,dorealcoadd,donoise,dosim,docoadd,rlz,docombmaps,queue,farmdps,usecompiled,simtypes,submit,daughter)
%  
% Real and sim maps with Planck deprojection map and planck noise, planck cosmo parameters, constrained sim rlz
% this should become the B2 major sim set
% the runsim_ function makes
%  - real pairmaps/maps
%  - sim pairmaps/maps
%  - the sim map combinations
%  - the traditional noise sim
%  - the signflip noise sim
% all the code for post-processing of these maps (aps, simsets and final etc) is found down in the bottom after this function
% there are also examples how to runsim_0751 in practice
%  
%  doreal       : (0) make real pairmaps
%  donoisesim   : (0) make traditional noise sim (just few rlz)
%  dorealcoadd  : (0) coadd real pair maps
%  donoise      : (0) make signflip noise sim
%  dosim        : (0) make flavour subset sims
%  docoadd      : (0) coadd flavour subsetsims (and traditional noise sims) 
%  rlz          : (1 default), i.e. 1:499
%  docombmaps   : (0) combine to the types 7,8,9
%  queue        : take this out - no more effect
%  farmdps      : (0) farm deprojections seperately 
%  usecompiled  : (1) - if usecompiled=1 the compiled exectuables of
%                 reduc_makesim and reduc_coaddpairmaps are going to be used
%                 to make those, run 
%                 compile_code('reduc_coaddpairmaps'); 
%                 compile_code('reduc_makesim');
%                 Note that if the .m files change and you want those changes
%                 to apply to your current computation you need to re-compiled.
%  simtypes     : ([2,3,4,5]) select the simtypes for which pairmaps are created
%                 and that are coadded
%  submit       : (1), if 0 just create farmfiles without submission
%  
%  funny order? the donoise and dosim options need the real coadded maps!
%  
%  run the coadd command twice as it is using a split coadd in the first round
%  then a coadd_coadds in the second
%  
%  before running on many realizations, copy the input_maps to panlfs2
%  
%  NOTE on usage example:
%   after the return statement there are actual examples where runsim_0751 is used
%  
%  NOTE on simbuddies:
%   - look at /n/bicepfs2/users/sfliescher/bicep2_analysis/*Stefan*
%   - Simbuddy Stefan is feed with farmfiles over the feedStefan script that slews
%     through the many jobs it takes to make a simset.
%   - Simbuddy Stefan just keeps the runStefan script running. It picks up farmfiles
%     over the wildcard provided in stringStefan.txt and submits them to the queue.
%   - It worked pretty good in the past to have one simbuddy for each simtype and
%     one for the noise
%   - Also the pairmaps can be created in this fashion
%   - The person who manages the simbuddies should not be part of the feed script.
%     This because there will be plenty of unforseen jobs to re-submit to the queue,
%     combmaps to be created, pagers to be done etc.
%

% check to not mess it up:
if nbase~=1351 & nbase~=0751
  error('This script is not meant for this base number.')
end
if nbase==0751 & ~strcmp(get_experiment_name(),'bicep2')
  error('Are you in the right folder for Bicep2 sims?')
end
if nbase==1351 & ~strcmp(get_experiment_name(),'keck')
  error('Are you in the right folder for Keck sims?')
end

% defaults: do nothing at all
if(~exist('doreal','var'))
  doreal=0;
end
if(~exist('donoisesim','var'))
  donoisesim=0;
end
if(~exist('dorealcoadd','var'))
  dorealcoadd=0;
end
if(~exist('donoise','var'))
  donoise=0;
end
if(~exist('dosim','var'))
  dosim=0;
end
if(~exist('docoadd','var'))
  docoadd=0;
end
if(~exist('rlz','var'))
  rlz=1;
end
if(~exist('docombmaps','var'))
  docombmaps=0;
end
if(~exist('queue','var'))
  queue=1;
end
if(~exist('farmdps','var'))
  farmdps=0;
end
if(~exist('usecompiled','var'))
  % choose whether to use normal (.m) or compiled code
  usecompiled=1;
end
if(~exist('simtypes','var'))
  simtypes=[2,3,4,5];
end
if(~exist('submit','var'))
  submit=true;
end
if(~exist('daughter','var'))
  daughter='a';
end

%memory usage differences between B2 and keck
if nbase==1351
  memreal=30000; % increased from 15000
  memcoadd = 30000;
  memcoaddcoadd = 12000;
  memcoaddcoadddp = 12000;
elseif nbase==0751
  memreal=10000;
  memcoadd = 4000;
  memcoaddcoadd = 4000;
  memcoaddcoadddp = 20000;
end
maxtime=400
maxtime_real=400;
if docoadd | dorealcoadd
  maxtime=12*60;
%    if farmdps
%      maxtime=240;
%    end
end
if dosim
  if strcmp(get_experiment_name, 'keck')
    maxtime=190;
  else
    maxtime=240*2;
  end
end


% pick appropiate queues:
display('we use slurm')
slurm_queue='serial_requeue,itc_cluster';
queue_real=slurm_queue;
queue_sim=slurm_queue;
queue_coadd=slurm_queue;
queue_coadd_coadd=slurm_queue;
queue_other=slurm_queue;

% global options
om=1;  % supplies 'OnlyMissing' option for both runsim and coadds

ntpg=4; % NTagsPerJob real data

% simulation options
if strcmp(get_experiment_name(),'bicep2')
  rlzc=8; % rlzchunksize for runsim
end
if strcmp(get_experiment_name(),'keck')
  rlzc=3; % rlzchunksize for runsim
end
tagc=1; % tagchunksize for runsim

% coadd options
js=0 % option for FarmJacksSeparately for coadd
disp(['farming dp seperately=' num2str(farmdps)])

% which deprojection we want to do:
%  deprojs=[0,0,0,0;  % No deprojection
%           1,0,0,0;  % relgain
%           0,1,0,0;  % A/B
%           1,1,0,0;  % relgain + A/B offsets   
%           1,1,1,0;  % beamwidth + A/B offsets + relgain
%           1,1,0,1;  % ellipticity + A/B offsets + relgain
%           1,1,1,1]; % all
         
% which deprojection we want to do:
deprojs=[1,1,0,0];  % relgain + A/B offsets

% add real map specials:
if dorealcoadd
  deprojs=[deprojs;
          1,1,0,2;  % measured ellipticity + A/B offsets + relgain
          0,0,0,0]; % for signal subtraction from the signflip noise
end


if dorealcoadd
  rlz=[];
end
rlz

% do want to wait for creating the tag list if it is not needed
dotags = doreal|donoisesim|dorealcoadd|donoise|dosim|docoadd;
if dotags
  if strcmp(get_experiment_name(),'bicep2')
    realfile = ['maps/',num2str(nbase, '%.4d'),'/real_' daughter '_filtp3_weight3_gs_dp1100_jack0.mat']
  else
    realfile = ['maps/',num2str(nbase, '%.4d'),'/real_' daughter '_filtp3_weight3_gs_dp1100_jack01.mat']
  end
  if exist(realfile)
    x = load(realfile);
    tags = x.coaddopt.tags;
    if daughter=='c'
      % use first occurance for keck2014 since taglist is still flowing
      [tagsublist,mtl]=get_tag_sublist(x.coaddopt,[],'first');
    else
      [tagsublist,mtl]=get_tag_sublist(x.coaddopt);
    end
    clear x; 
  else
    % do the tags - this should only happen once when the real map is made:
    if nbase==0751 tags=get_tags('allcmb','has_tod'); end % 17097, B2
    if nbase==1351 % Keck
      switch daughter
       case 'a'
        tags=get_tags('cmb2012','has_tod');
       case 'b'
        tags=get_tags('cmb2013','has_tod');
       case 'c'
        tags=get_tags('cmb2014','has_tod');
       case 'd'
        tags=get_tags('cmb2014','has_tod');
       case 'e'
        tags=get_tags('cmb2015','has_tod');
      end
    end
  end
end

clear simopt
simopt.siginterp='taylor';
%this is 'aux_data/beams/beams_cmb_3yr.csv' not 'aux_data/beams/beams_cmb_3yr_planck.csv'
simopt.beamcen='obs'; 
simopt.diffpoint='obs';
simopt.chi='obs';
simopt.epsilon='obs';

%3 yrs of B2 vs planck: http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130612_abscal_4/
% 3150 is the round number recommendation arising from this posting
% simopts are not foreseen to change by daughter, so changing the simopt.ukpervolt
% was ignored in the simrunfile mechanism. To start with all sims use the same ukpervolt - 
% that of 2012 which is the default:
ukpervolt=get_ukpervolt;
switch daughter
 case 'a'
  ukpervolt=get_ukpervolt('2012');
 case 'b'
  ukpervolt=get_ukpervolt('2013');
 case 'c'
  ukpervolt=get_ukpervolt('2014');
 case 'd'
  ukpervolt=get_ukpervolt('2014');
 case 'e'
  ukpervolt=get_ukpervolt('2015');
end
simopt.ukpervolt=ukpervolt;
simopt.coord='C';
simopt.force_ab_int_from_common_pix=false;
simopt.update=1;

clear mapopt
mapopt.beamcen='obs';
mapopt.chi='obs';
mapopt.epsilon='obs';
mapopt.acpack=0;
mapopt.gs=1;
mapopt.filt='p3';
mapopt.deproj=true;
mapopt.update=true;

clear coaddopt
%  http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130628_channel_cuts_3/
switch daughter
 case 'a'
  chflags=get_default_chflags();
 case 'b'
  chflags=get_default_chflags([], '2013');
 case 'c'
  chflags=get_default_chflags([], '2014');
 case 'd'
  chflags=get_default_chflags([], '2014');
 case 'e'
  chflags=get_default_chflags([], '2015');
end

coaddopt.chflags=chflags;
coaddopt.daughter=daughter;
coaddopt.deproj_timescale='by_phase';
coaddopt.filt='p3';
coaddopt.gs=1;
coaddopt.jacktype='0123456789abcde';

%change to coaddtype=1 for keck
if nbase==1351
  coaddopt.coaddtype=1;
end

% for a special sim/real map:
%  mapopt.proj='arc';
%  coaddopt.proj='arc';

% we are going do coadding over subsets, hence specifiy
% the 1st/2nd half split explicity with the logic from
% reduc_coaddpairmaps. The spilt is at the point of even
% accumulated pair diff weight in the first and second half
% found by additional analysis
if nbase==0751 coaddopt.temporaljack_splitdate='20111101'; end
if nbase==1351 
  switch daughter
   case 'a'
    coaddopt.temporaljack_splitdate='20120719'; 
   case 'b'
    coaddopt.temporaljack_splitdate='20130710'; %this is just 1/2 the tags... 
   case 'c'
    coaddopt.temporaljack_splitdate='20140518'; %this is even weights in diff
   case 'd'
    coaddopt.temporaljack_splitdate='20140717'; %this is even weights in diff
   case 'e'
    coaddopt.temporaljack_splitdate='20150708'; %this is even weights in diff
  end
end

%%%%%%%%%%%%%%%%%%%%%%% let's work
if doreal
  mapopt.sernum=sprintf('%04dreal',nbase);
 % it is important that the deproj_map cell is size=(2,1) and not (1,2)
  switch daughter
   case 'e'
    mapopt.deproj_map=...
    {'input_maps/planck/planck_derivs_nopix/HFI_SkyMap_100_2048_R1.10_nominal_nside0512_nlmax1280_bKuber100.fits';...
    'input_maps/planck/planck_derivs_nopix/synfast_deproj_143_nominal_B2.fits';...
    'input_maps/planck/planck_derivs_nopix/HFI_SkyMap_217_2048_R1.10_nominal_nside0512_nlmax1280_bKuber220.fits';}
   case 'd'
    % look at the examples in prep_fullsky_map how to prepare deprojection maps
    mapopt.deproj_map=...
    {'input_maps/planck/planck_derivs_nopix/HFI_SkyMap_100_2048_R1.10_nominal_nside0512_nlmax1280_bKuber100.fits';...
    'input_maps/planck/planck_derivs_nopix/synfast_deproj_143_nominal_B2.fits'};
   case 'c'
    mapopt.deproj_map=...
    {'input_maps/planck/planck_derivs_nopix/HFI_SkyMap_100_2048_R1.10_nominal_43p1000.fits';...
     'input_maps/planck/planck_derivs_nopix/synfast_deproj_143_nominal_B2.fits'};
   otherwise
    mapopt.deproj_map=...
    {'input_maps/planck/planck_derivs_nopix/synfast_deproj_143_nominal_B2.fits'};
  end
  
  farm_makepairmaps(tags,mapopt,'JobLimit',600,'NTagsPerJob',ntpg,'FarmJobs',1, ...
                    'OnlyMissing',1,'Queue',queue_real,'MemRequire',memreal,...
                    'maxtime',maxtime_real,'submit',submit);

end

%  types
%  xxx1 - reserved for traditional noise sims
%  xxx2 - LCDM unlensed scalars with diff point on
%  xxx3 - dust
%  xxx4 - r=0.1 tensors with E forced to zero (and T switched off at makesim stage)
%  xxx5 - LCDM lensed scalars with diff point on
%  xxx6 - sign flip noise
%  xxx8 - reserved for alternate artificial B-mode input spectrum

%  combinations:
%  xxx7 - the map sum 5+6 
%  xxx8 - the sum of 3+5+6 = 7+3
%  xxx9 - the sum of 4+5+6 = 7+4
if dosim
  try
    par_make_simrunfiles(tags,simopt,nbase,daughter);
  catch
    display('simrunfile exists, not remaking it...')
  end
  
% Some re-writing needs to be done here.  For B2, K2012, and K2013, the maps now should
% include only the 150 maps

  mapopt.deproj_map={...
  'input_maps/camb_planck2013_r0/map_unlens_n0512_rxxxx_sKuber100_cPl100_dPl100.fits';...
  'input_maps/camb_planck2013_r0/map_unlens_n0512_rxxxx_sB2bbns_cPl143_dPl143.fits';...
  'input_maps/camb_planck2013_r0/map_unlens_n0512_rxxxx_sKuber220_cPl217_dPl217.fits'};
  if ismember(2,simtypes)
    type = 2;
    sigmapfilename={...
        'input_maps/camb_planck2013_r0/map_unlens_n2048_rxxxx_sKuber100_cPl100_dNoNoi.fits',...
        'input_maps/camb_planck2013_r0/map_unlens_n2048_rxxxx_sB2bbns_cPl143_dNoNoi.fits',...
        'input_maps/camb_planck2013_r0/map_unlens_n2048_rxxxx_sKuber220_cPl217_dNoNoi.fits'};
    runsim(nbase,daughter,mapopt,'normal','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime,submit);
  end
  if ismember(3,simtypes)
    type = 3;
    sigmapfilename={...
        'input_maps/dust_95_3p75/map_unlens_n2048_rxxxx_sKuber100_dNoNoi.fits',...
        'input_maps/dust_150_3p75/map_unlens_n2048_rxxxx_sB2bbns_dNoNoi.fits',...
        'input_maps/dust_220_3p75/map_unlens_n2048_rxxxx_sKuber220_dNoNoi.fits'};
    runsim(nbase,daughter,mapopt,'onlypol','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime,submit);
  end
  if ismember(4,simtypes)
    type = 4;
    sigmapfilename={...
        'input_maps/camb_planck2013_r0p1_noE/map_unlens_n2048_rxxxx_sKuber100_cPl100_dNoNoi.fits',...
        'input_maps/camb_planck2013_r0p1_noE/map_unlens_n2048_rxxxx_sB2bbns_cPl143_dNoNoi.fits',...
        'input_maps/camb_planck2013_r0p1_noE/map_unlens_n2048_rxxxx_sKuber220_cPl217_dNoNoi.fits'};
    runsim(nbase,daughter,mapopt,'onlypol','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime,submit);
  end
  if ismember(5,simtypes)
    type = 5;
    % deproj template should be lensed for type5
    % camb_planck2013_r0 indicates the lensing spectrum that is off by ~10%
    % camb_planck2013_r0_rev1 would be the updated lensing spectrum
    mapopt.deproj_map={...
        'input_maps/camb_planck2013_r0/map_lensed_n0512_rxxxx_sKuber100_cPl100_dPl100.fits';...
        'input_maps/camb_planck2013_r0/map_lensed_n0512_rxxxx_sB2bbns_cPl143_dPl143.fits';...
        'input_maps/camb_planck2013_r0/map_lensed_n0512_rxxxx_sKuber220_cPl217_dPl217.fits'};
    sigmapfilename = {...
        'input_maps/camb_planck2013_r0/map_lensed_n2048_rxxxx_sKuber100_cPl100_dNoNoi.fits',...
        'input_maps/camb_planck2013_r0/map_lensed_n2048_rxxxx_sB2bbns_cPl143_dNoNoi.fits',...
        'input_maps/camb_planck2013_r0/map_lensed_n2048_rxxxx_sKuber220_cPl217_dNoNoi.fits'};
    runsim(nbase,daughter,mapopt,'normal','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime,submit);
  end
end

if donoisesim
  try
    par_make_simrunfiles(tags,simopt,nbase,daughter);
  catch
    display('simrunfile exists, not remaking it...')
  end

  mapopt.deproj_map= 'input_maps/camb_planck2013_r0/map_unlens_n0512_rxxxx_sB2bbns_dPl143.fits'; % should this be lensed?
  
  type = 1;
  sigmapfilename='input_maps/camb_planck2013_r0/map_unlens_n2048_rxxxx_sB2bbns_dNoNoi.fits';
  runsim(nbase,mapopt,'none','data',type,sigmapfilename,rlz,tags,false,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime);
end

if docoadd | dorealcoadd
  %loop over sim types, will jump out for real
  for k=simtypes
    k

    coaddopt.sernum=sprintf('%04dxxx%01d',nbase,k);
    if dorealcoadd
      coaddopt.sernum(5:8)='real';
      coaddopt.save_cuts=true;
    else
      coaddopt.save_cuts=false;
    end
    % no tag substitution for real or traditional noise sim:
    if ~dorealcoadd & k~=1
      coaddopt.tagsublist=tagsublist;
    end
    if farmdps
      %loop over the deprojs
      for jj = 1:size(deprojs,1)
        coaddopt.deproj=deprojs(jj,:);
        if dorealcoadd && all(coaddopt.deproj==[1 1 0 2]) && daughter~='d'
          % put in the make extra dp1102 maps for k2012,13,bicep2 with fixed esub
          % for 2014_d is simply fixed without notice
          coaddopt.daughter=[coaddopt.daughter '_fixesub'];
        end
        farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',20,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
      end
    else
      coaddopt.deproj=deprojs;
      farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                              'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                              'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadddp,...
                              'SplitSubmission',20,'UseCompiled',usecompiled,...
                              'maxtime',maxtime,'submit',submit);
    end
    %no loop for real data
    if (dorealcoadd) break; end 
  end
end

% can be done after real data coadd
if donoise
  % sign_flip noise, see: http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130614_sign_flip_seq/
  % and for the signal subtraction: http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20140709_SignFlip_SignalSub/
  % and links therein.
  % Keck 2014 signal subtraction:
  if nbase==1351 && (daughter=='c' || daughter=='d' || daughter=='e')
    disp('use dp0000 to subtract')
    x=load(['maps/',num2str(nbase,'%.4d'),'/real_',coaddopt.daughter,'_filtp3_weight3_gs_dp0000_jack01'],'ac','m');
    coaddopt.ac=x.ac;
    coaddopt.m=x.m;
    clear x;
  end
  
  % that is the coaddtype
  ct = '';
  if nbase==1351
    ct = '1';
  end

  % first load in real data to fetch the jackmasks
  % we do this before the loop over the realizations, to avoid the io
  % all the non-temporal jacks get the same sign_flip sequence:
  x= containers.Map();
  % if the load fails, you probably have not created a special version of the real
  % maps that has the _s appended to the filename. You can just run it with the straight
  % real map instead (w/o the _s in the filename). However, the real maps are pretty big
  % so this will take a while to loop over the different jacks. The _s versions are
  % stripped down files that only contain the portion that are neccessary for the
  % signflip noise. To create them, run the corrsponding piece of code underneath
  % the end of this function at the end of this file.
  x('024689abce')=load(['maps/' num2str(nbase, '%.4d') '/real_' coaddopt.daughter '_filtp3_weight3_gs_dp1100_jack2',ct,'_s']);
  % all the temporal jacks fetch the individual jackmasks:
  for xx=['1','3','5','7','d']
    disp(['loading jack' xx]);
    x(xx)=load(['maps/' num2str(nbase, '%.4d') '/real_' coaddopt.daughter '_filtp3_weight3_gs_dp1100_jack',xx,ct,'_s']);
  end

  % the real data is coming with a large cut stucture, avoid saving this over and over
  % for the noise realizations:
  coaddopt.save_cuts=false;
  coaddopt.sernum=[num2str(nbase, '%.4d') 'real'];

  coaddopt.sign_flip_type='by_scanset';
  for ii=rlz
    % fix the random numbers
    stream0 = RandStream('mt19937ar','Seed',ii)
    RandStream.setDefaultStream(stream0);

    % loop over the different jacks
    for xx={'024689abce','1','3','5','7','d'}

      % fetch the sign_flip sequence...
      coaddopt.sign_flip_seq=get_sign_flip_seq(x(char(xx)).coaddopt);
      % ...specific for each jack
      coaddopt.jacktype=char(xx);
      % assign a realization number:
      coaddopt.sign_flip_rlz=ii;
      if farmdps
        for jj = 1:size(deprojs,1)
          coaddopt.deproj=deprojs(jj,:);
          farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',[],...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',20,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
        end
      else
        coaddopt.deproj=deprojs;
        farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',[],...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',20,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
      end
    end
  end
end

% Combine comaps
%  xxx3 - reserved for the map sum of 1+2 
%  xxx7 - the map sum 1+5 or 6+5 
%  xxx9 - the sum of 6+5+4 = 7+4
if nbase==1351; ct='1'; end; 
if nbase==0751; ct=''; end;
if docombmaps

  for crlz = rlz
    cmd  = ['reduc_combcomap(''',num2str(nbase, '%.4d'),'/xxx6_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',5,7,1)'];
    cmd  = strrep(cmd,'xxx',sprintf('%03d',crlz));
    cmd2 = ['reduc_combcomap(''',num2str(nbase, '%.4d'),'/xxx7_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',4,9,1)'];
    cmd2 = strrep(cmd2,'xxx',sprintf('%03d',crlz));
    cmd3 = ['reduc_combcomap(''',num2str(nbase, '%.4d'),'/xxx7_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',3,8,1)'];
    cmd3 = strrep(cmd3,'xxx',sprintf('%03d',crlz));
    cmd = [cmd,';',cmd2,';',cmd3];
    farmit('farmfiles/comb/',cmd,'queue',queue_other,'mem',16000,'maxtime',60,'submit',0);
  end

end

return

% below some code to handle the runsim function above:
% 1. run run_sim
% 2. make a maps pager
% 3. make the _s real file helpful for signflip noise

% run run_sim, just drag the protion you want into your matlab session
if(0)

  % which simset to run, select one combination
  nbase=1351 
  daughter='a'
  
  nbase=1351 
  daughter='b'
  
  nbase=1351 
  daughter='d'
  
  nbase=0751 
  daughter='a'
  
  % general decisions, you probably don't want to change these
  queue=1
  farmdps=1
  usecompiled=1
  submit=0
  docorrnoise=0
  
  % nullify all
  doreal=0
  donoisesim=0
  dorealcoadd=0
  donoise=0
  dosim=0
  docombmaps=0
  docoadd=0

  % run the full thing, all rlz and simtypes:
  rlz=[1:499]
  simtypes=[2,3,4,5]

  % pick what should be run, this is the order in which it is needed - so wait
  % until 1. is finished before running 2. and so on:
  
  % 1. allways need real pairmaps...
  doreal=1
  
  % 2. ...and the real map, needs to be run twice due to split submission
  dorealcoadd=1
  
  % 3 a and b run indpendently from one another
  % 3a. do signflip noise
  % needs to be run twice due to split submission
  donoise=1 
  
  % 3b. sim pairmaps
  dosim=1
  
  % 4. sim coadd, does not depend on 3a
  % needs to be run twice due to split submission
  docoadd=1
  
  % 5. combine the different types  
  docombmaps=1
  
  runsim_0751(nbase,doreal,donoisesim,dorealcoadd,donoise,dosim,docoadd,rlz,docombmaps,queue,farmdps,usecompiled,simtypes,submit,daughter)

end

% make a maps pager
if(0)
  jacks = get_default_coaddopt();
  jacks = jacks.jacktype; 
  
  % BK14
  nbase=1459;
  daughter='aabd';
  apsopt.mapname={'BK14_95','BK14_150'}
  ct=''; 
  polrot=[-0.5,-0.6];
  apsopt.ukpervolt={1,1};
  crx = 0;
  jacks='0'
  
  % BK13
  nbase=1456;
  daughter='aab';
  apsopt.mapname={'BK13_150'}
  ct=''; 
  polrot=[-0.8];
  apsopt.ukpervolt={1,1};
  apsopt.purifmatname = '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat';
  crx = 0;
  jacks='0'

  % Keck15
  nbase=1351;
  daughter='e';
  apsopt.mapname={'K2015_95','K2015_150','K2015_220'}
  ct='1'; 
  polrot=[0,0,0];
  apsopt.ukpervolt=get_ukpervolt('2015');
  crx = 2;

  % Keck2014
  nbase=1351;
  daughter='d';
  apsopt.mapname={'K2014_95','K2014_150'}
  ct='1'; 
  polrot=[-0.5,0];
  apsopt.ukpervolt=get_ukpervolt('2014');
  crx = 2;
  
  % Keck2012+2013
  nbase=1351;
  daughter='ab';
  apsopt.mapname={'K13'}
  ct=''; 
  apsopt.ukpervolt=1;
  polrot=[-0.5];
  apsopt.purifmatname = '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat';
  crx = 0;

  % Bicep2
  nbase=0751;
  daughter='a';
  apsopt.mapname={'B12'}
  ct=''; 
  apsopt.ukpervolt=3150;
  polrot=[-1.1];
  apsopt.purifmatname = '/n/bicepfs2/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat';  
  crx = 0;
  
  % reduc_plotcomap_pager auto pager
  types = {'real','0012','0013','0014','0015','0016','0017','0018','0019'}
 
  for type = types
    if strcmp(type{:},'real')
      deprojs={'1102','1100','0000'};
      subs = {'','psub','gsub','dsub'}
      subs = {''}
      apsopt.polrot=polrot;
    else
      deprojs={'1100'};
      subs={''};
      apsopt.polrot=[0];
    end
    for deproj = deprojs
      for jack=jacks
        for coaddrx=crx
          for fcs=[0,1]
            for sub = subs

              lastp = ['_B_noi_',apsopt.mapname{end},'.png'];
              if fcs lastp = strrep(lastp,'.png','_fcs.png'); end
              
              apsopt.coaddrx=coaddrx;
              map=[num2str(nbase,'%04i'),'/',type{:},'_',daughter,'_filtp3_weight3_gs_dpDDDD_jackJJ.mat'];
              map=strrep(map,'JJ',[jack,ct]);
              map=strrep(map,'DDDD',deproj{:});
              cmd=['reduc_plotcomap_pager(map,apsopt,',num2str(fcs),',1,1,0,0,0,0,0,''',sub{:},''',1);'];
              if ~exist(['reduc_plotcomap_pager/',num2str(nbase,'%04i'),sub{:},'/' map(6:end-4) lastp],'file')
                disp('do it')
                farmit('farmfiles/rpp/',cmd,'var',{'map','apsopt'},'queue','serial_requeue,itc_cluster','mem',18000,'maxtime',60,'submit',0);
              else
                disp('skip')
              end
            end
          end
        end
      end
    end
  end
end

% create the _s real map files:
if (0)
  % first load in real data to fetch the jackmasks
  % we do this before the loop over the realizations, to avoid the io
  % all the non-temporal jacks get the same sign_flip sequence:
  ct='1'
  nbase=1351;
  % adjust to your current daughter:
  daughter='c';
  for xx=['1','2','3','5','7','d']
    disp(['loading jack' xx]);
    load(['maps/' num2str(nbase, '%.4d') '/real_' daughter '_filtp3_weight3_gs_dp1100_jack',xx,ct]);
    coaddopt = rmfield(coaddopt,'c');
    save(['maps/' num2str(nbase, '%.4d') '/real_' daughter '_filtp3_weight3_gs_dp1100_jack',xx,ct,'_s'],'ac','coaddopt','m');
  end
end
