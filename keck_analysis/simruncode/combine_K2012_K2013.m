function combine_K2012_K2013()
%  combine_K2012_K2013()
%  this makes the 1351/????_ab_ maps (K13) for the mainline deprojection options
%  and jacks 0345 which were used in the Planck Joint analsis.
%  
% The code below was not used for the 1351/????_ab_ maps created till Jul 2015.
% Previously there was some private code making those, that, however, had in issue
% with the format of the coaddopt. The code below is tested to produce identical maps as before
% but fixes the coaddopt issue. This code below should be used for the next
% regeneration of the maps.

% the real maps 1351 dp1102
for j='0345'
  mapK2012 = (['1351/real_a_filtp3_weight3_gs_dp1102_jack',j,'1.mat']);
  mapK2013 = (['1351/real_b_filtp3_weight3_gs_dp1102_jack',j,'1.mat']);
  mapK13   = (['1351/real_ab_filtp3_weight3_gs_dp1102_jack',j,'.mat']);
  % load_and_combine(mapK2012,mapK2013,mapK13)
  farmit('farmfiles/coaddcoadd/','load_and_combine(mapK2012,mapK2013,mapK13)','func',{@load_and_combine,@minimize_coaddopt},'var',{'mapK2012','mapK2013','mapK13'},'queue',queue,'mem',10000,'maxtime',15,'submit',0)    
end

% the sims
for j='0345'
  for t = '23456'
    for rlz = 1:499
      mapK13 = sprintf(['1351/%03d',t,'_ab_filtp3_weight3_gs_dp1100_jack',j,'.mat'],rlz);
      if ~exist(['maps/',mapK13],'file')
        mapK2012   = sprintf(['1351/%03d',t,'_a_filtp3_weight3_gs_dp1100_jack',j,'1.mat'],rlz);        
        mapK2013   = sprintf(['1351/%03d',t,'_b_filtp3_weight3_gs_dp1100_jack',j,'1.mat'],rlz);
        % load_and_combine(mapK2012,mapK2013,mapK13)
        farmit('farmfiles/coaddcoadd/','load_and_combine(mapK2012,mapK2013,mapK13)','func',{@load_and_combine,@minimize_coaddopt},'var',{'mapK2012','mapK2013','mapK13'},'queue',queue,'mem',10000,'maxtime',15,'submit',0)    
      end      
    end
  end
end
babysitjobs('coaddcoadd/*.mat','wait2',0,0,50)

for rlz = rlzs
  cmd1 = ['reduc_combcomap(''1351/xxx6_ab_filtp3_weight3_gs_dp1100_jack[0345].mat'',5,7,1)'];
  cmd1 = strrep(cmd,'xxx',sprintf('%03d',rlz));
  
  cmd2 = ['reduc_combcomap(''1351/xxx7_ab_filtp3_weight3_gs_dp1100_jack[0345].mat'',4,9,1)'];
  cmd2 = strrep(cmd2,'xxx',sprintf('%03d',rlz)); cmd  = [cmd,';',cmd2];
  
  cmd3 = ['reduc_combcomap(''1351/xxx7_ab_filtp3_weight3_gs_dp1100_jack[0345].mat'',3,8,1)'];
  cmd3 = strrep(cmd3,'xxx',sprintf('%03d',rlz)); 
  
  cmd  = [cmd1,';',cmd2,';',cmd3];
  
  farmit('farmfiles/comb/',cmd,'queue',queue,'mem',2000,'maxtime',20,'submit',0);
end
babysitjobs('farmfiles/comb/*','wait5')
      
return


function load_and_combine(mapK2012,mapK2013,mapK13)
%  this needs to be adjusted year to year
  mapK2012
  mapK2013
  mapK13
  
  mK2012  = load(['maps/',mapK2012]);
  mK2013  = load(['maps/',mapK2013]);
  
  % make sure you got the right ukpervolt for that year here:
  mK2012.coaddopt.ukpv_applied = get_ukpervolt('2012');
  mK2012.ac=cal_coadd_ac(mK2012.ac,mK2012.coaddopt.ukpv_applied);
  mK2012.ac=coadd_ac_overfreq(mK2012.ac,mK2012.coaddopt);
  
  mK2013.coaddopt.ukpv_applied = get_ukpervolt('2013');
  mK2013.ac=cal_coadd_ac(mK2013.ac,mK2013.coaddopt.ukpv_applied);
  mK2013.ac=coadd_ac_overfreq(mK2013.ac,mK2013.coaddopt);
  
  ac = struct_merge(mK2012.ac,mK2013.ac);
  ac = coadd_ac_overrx(ac);
  
  mK2012.coaddopt = minimize_coaddopt(mK2012.coaddopt);
  mK2013.coaddopt = minimize_coaddopt(mK2013.coaddopt);  
  
  mK2012.coaddopt.mapname = mapK2012;
  mK2013.coaddopt.mapname = mapK2013;
  
  coaddopt={mK2012.coaddopt;mK2013.coaddopt};
  m = mK2013.m;
  
  saveandtest(['maps/',mapK13],'ac','coaddopt','m','-v7.3');
return

function coaddopt = minimize_coaddopt(coaddopt)
  try coaddopt = rmfield(coaddopt,'b'); end
  try coaddopt = rmfield(coaddopt,'bi'); end
  try coaddopt = rmfield(coaddopt,'bw'); end
  try coaddopt = rmfield(coaddopt,'hsmax'); end
  try coaddopt = rmfield(coaddopt,'whist'); end
  try coaddopt = rmfield(coaddopt,'devhist'); end
  try coaddopt = rmfield(coaddopt,'traj'); end
  try coaddopt = rmfield(coaddopt,'c'); end
  try coaddopt = rmfield(coaddopt,'jackmask'); end
return
