function runsim_2352(nbase,doreal,dorealcoadd,donoisecov,donoise,dosim,docoadd,rlz,docombmaps,queue,farmdps,usecompiled,simtypes,submit,daughter)
% function runsim_2352(nbase,doreal,dorealcoadd,donoisecov,donoise,dosim,docoadd,rlz,docombmaps,queue,farmdps,usecompiled,simtypes,submit,daughter)
%  
% Real and sim maps with Planck deprojection map and planck noise, planck cosmo parameters, constrained sim rlz
% this is in bicepcoarse to test out the full likelihood estimation with a smaller matrix
%
% the runsim_ function makes
%  - real pairmaps/maps
%  - sim pairmaps/maps
%  - the sim map combinations
%  - the traditional noise sim
%  - the signflip noise sim
% all the code for post-processing of these maps (aps, simsets and final etc) is found down in the bottom after this function
% there are also examples how to runsim_2352 in practice
%  
%  doreal -> make real pairmaps
%  dorealcoadd-> coadd real pair maps
%  donoisecov-> calculates the noise covariance for the real
%  donoise-> make signflip noise sim
%  dosim-> make flavour subset sims
%  docoadd-> coadd traditional noise sims and flavour subsetsims
%  rlz -> i.e. 1:10
%  docombmaps: runsim_2352(2352,0,0,0,0,0,0,rlz,1,0) 
%  queue: 0 LSF, 1 serial_requeue (default), 2 general 
%  farmdps: farm deprojections seperately (0 default)
%  usecompiled: 1 default
%  simtypes: [2,4,5] default
%  submit: 1 default, if 0 just create farmfiles without submission
%  
%  funny order? the donoise and dosim options need the real coadded maps!
%  
%  run the coadd command twice as it is using a split coadd in the first round
%  then a coadd_coadds in the second
%  
%  before running on many realizations, copy the input_maps to panlfs
%  

% check to not mess it up:
if nbase~=1352 & nbase~=2352
  error('This script is not meant for this base number.')
end
if nbase==2352 & ~strcmp(get_experiment_name(),'bicep2')
  error('Are you in the right folder for Bicep2 sims?')
end
if nbase==1352 & ~strcmp(get_experiment_name(),'keck')
  error('Are you in the right folder for Keck sims?')
end

% defaults: do nothing at all
if(~exist('doreal','var'))
  doreal=0;
end
if(~exist('donoisesim','var'))
  donoisesim=0;
end
if(~exist('dorealcoadd','var'))
  dorealcoadd=0;
end
if(~exist('donoise','var'))
  donoise=0;
end
if(~exist('dosim','var'))
  dosim=0;
end
if(~exist('docoadd','var'))
  docoadd=0;
end
if(~exist('rlz','var'))
  rlz=1;
end
if(~exist('docombmaps','var'))
  docombmaps=0;
end
if(~exist('queue','var'))
  queue=1;
end
if(~exist('farmdps','var'))
  farmdps=0;
end
if(~exist('usecompiled','var'))
  % choose whether to use normal (.m) or compiled code
  usecompiled=1;
end
if(~exist('simtypes','var'))
  simtypes=[2,4,5];
end
if(~exist('submit','var'))
  submit=true;
end
if(~exist('daughter','var'))
  daughter='a';
end

% setup options
dotagsfrommap=1;

%memory usage differences between B2 and keck
if nbase==1352
  memreal=20000;
  memsim=25000;
  memcoadd = 12000;
  memcoaddcoadd = 12000;
  memcoaddcoadddp = 12000;
elseif nbase==2352
  memsim=6000;
  memreal=10000;
  memcoadd = 16000;
  memcoaddcoadd = 12000;
  memcoaddcoadddp = 20000;
  memnoisecov = 30000;
end
maxtime=[]
if docoadd | dorealcoadd
  maxtime=8*60;
  if farmdps
    maxtime=60;
  end
end
if dosim
  if strcmp(get_experiment_name, 'keck')
    maxtime=60*4;
  else
    maxtime=60;
  end
end


% pick appropiate queues:
use_serial_requeue=0;
use_general=0;
if (queue==1) use_serial_requeue=1; end
if (queue==2) use_general=1; end

queue_real='normal_serial'; % real pairmaps queue
queue_sim='short_serial'; % queue for runsim jobs
queue_coadd='normal_serial'; % queue for coadd jobs
queue_coadd_coadd='normal_serial'; % queue for coadd jobs
if farmdps
  queue_coadd='short_serial'; % queue for coadd jobs
  queue_coadd_coadd='short_serial'; % queue for coadd jobs
end
queue_other='short_serial';
if use_serial_requeue
  display('we use serial_requeue')
  slurm_queue='serial_requeue';
  queue_real=slurm_queue;
  queue_sim=slurm_queue;
  queue_coadd=slurm_queue;
  queue_coadd_coadd=slurm_queue;
  queue_other=slurm_queue;
elseif use_general
  display('we use general')
  slurm_queue='general';
  queue_real=slurm_queue;
  queue_sim=slurm_queue;
  queue_coadd=slurm_queue;
  queue_coadd_coadd=slurm_queue;
  queue_other=slurm_queue;
end

% global options
om=1;  % supplies 'OnlyMissing' option for both runsim and coadds

ntpg=7; % NTagsPerJob real data

% simulation options
if strcmp(get_experiment_name(),'bicep2')
  rlzc=8; % rlzchunksize for runsim
end
if strcmp(get_experiment_name(),'keck')
  rlzc=5; % rlzchunksize for runsim
end
tagc=1; % tagchunksize for runsim

% coadd options
js=0 % option for FarmJacksSeparately for coadd
disp(['farming dp seperately=' num2str(farmdps)])

% which deprojection we want to do:
deprojs=[1,1,0,0];  % relgain + A/B offsets   

% add real map specials:
if dorealcoadd
  deprojs=[deprojs;
           1,1,0,2];  % measured ellipticity + A/B offsets + relgain
end

if dorealcoadd
  rlz=[];
end
rlz

% do want to wait for creating the tag list if it is not needed
dotags = doreal|donoisecov|dorealcoadd|donoise|dosim|docoadd;
if dotags
  % do the tags
  % switch this to grabbing it from the real data asa available
% this does not work in the Tier2 test since it uses data/real/...
  if nbase==2352 tags=get_tags('allcmb','has_tod'); end % 17097, B2
  if nbase==1352 tags=get_tags('cmb2012','has_tod'); end % Keck
  if nbase==1352 && daughter=='b'; tags=get_tags('cmb2013','has_tod'); end % Keck 2013

  if dotagsfrommap
    x = load(['maps/',num2str(nbase, '%.4d'),'/real_' daughter '_filtp3_weight3_gs_dp1100_jack0.mat']);
    tags = x.coaddopt.tags;
    [tagsublist,mtl]=get_tag_sublist(x.coaddopt);
    clear x; 
  end
end

clear simopt
simopt.siginterp='taylor';
%this is 'aux_data/beams/beams_cmb_3yr.csv' not 'aux_data/beams/beams_cmb_3yr_planck.csv'
simopt.beamcen='obs'; 
simopt.diffpoint='obs';
simopt.chi='obs';
simopt.epsilon='obs';
%3 yrs of B2 vs planck: http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130612_abscal_4/
% 3150 is the round number recommendation arising from this posting
ukpervolt=get_ukpervolt;
if daughter=='b'
  ukpervolt=get_ukpervolt('2013');
end
simopt.ukpervolt=ukpervolt;
simopt.coord='C';
simopt.force_ab_int_from_common_pix=false;
simopt.update=1;

clear mapopt
mapopt.beamcen='obs';
mapopt.chi='obs';
mapopt.epsilon='obs';
mapopt.acpack=0;
mapopt.gs=1;
mapopt.filt='p3';
mapopt.deproj=true;
% main difference between this and others is the map type
mapopt.type='bicepcoarse';

clear coaddopt
%  http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130628_channel_cuts_3/
if daughter=='b'
  chflags=get_default_chflags([], '2013');
else
  chflags=get_default_chflags();
end
coaddopt.chflags=chflags;
coaddopt.daughter=daughter;
coaddopt.deproj_timescale='by_phase';
coaddopt.filt='p3';
coaddopt.gs=1;
coaddopt.jacktype='0123456789abcde';
%change to coaddtype=1 for keck
if nbase==1352
  coaddopt.coaddtype=1;
end

% for a special sim/real map:
%  mapopt.proj='arc';
%  coaddopt.proj='arc';

% we are going do coadding over subsets, hence specifiy
% the 1st/2nd half split explicity with the logic from
% reduc_coaddpairmaps. The spilt is at the point of even
% accumulated pair diff weight in the first and second half
% found by additional analysis
if nbase==2352 coaddopt.temporaljack_splitdate='20111101'; end
if nbase==1352 
  if daughter=='b'
    coaddopt.temporaljack_splitdate='20130710'; %this is just 1/2 the tags... 
  else
    coaddopt.temporaljack_splitdate='20120719'; 
  end
end

%%%%%%%%%%%%%%%%%%%%%%% let's work
if doreal
  mapopt.sernum=sprintf('%04dreal',nbase);
  mapopt.deproj_map='input_maps/planck/planck_derivs_nopix/synfast_deproj_143_nominal_B2.fits';
  farm_makepairmaps(tags,mapopt,'JobLimit',600,'NTagsPerJob',ntpg,'FarmJobs',1, ...
                    'OnlyMissing',1,'Queue',queue_real,'MemRequire',memreal);
end

%  types
%  xxx1 - reserved for traditional noise sims
%  xxx2 - LCDM unlensed scalars with diff point on
%  xxx4 - r=0.1 tensors with E forced to zero (and T switched off at makesim stage)
%  xxx5 - LCDM lensed scalars with diff point on
%  xxx6 - sign flip noise
%  xxx8 - reserved for alternate artificial B-mode input spectrum

%  combinations:
%  xxx3 - reserved for the map sum of 1+2 
%  xxx7 - the map sum 5+1 or 5+6 
%  xxx9 - the sum of 4+5+6 
if dosim
  try
    par_make_simrunfiles(tags,simopt,nbase);
  catch
    display('simrunfile exists, not remaking it...')
  end
  
  mapopt.deproj_map='input_maps/camb_planck2013_r0/map_unlens_n0512_rxxxx_sB2bbns_constrained_dPl143.fits';
 
  type = 2;
  sigmapfilename='input_maps/camb_planck2013_r0/map_unlens_n2048_rxxxx_sB2bbns_constrained_dNoNoi.fits';
  runsim(nbase,mapopt,'normal','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,memsim,tagc,usecompiled,maxtime,submit);

  type = 4;
  sigmapfilename='input_maps/camb_planck2013_r0p1_noE/map_unlens_n2048_rxxxx_sB2bbns_constrained_dNoNoi.fits';
  runsim(nbase,mapopt,'onlypol','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,memsim,tagc,usecompiled,maxtime,submit);
  
  type = 5;
  %deproj template should be lensed for type5
  mapopt.deproj_map= 'input_maps/camb_planck2013_r0/map_lensed_n0512_rxxxx_sB2bbns_constrained_dPl143.fits';
  sigmapfilename = 'input_maps/camb_planck2013_r0/map_lensed_n2048_rxxxx_sB2bbns_constrained_dNoNoi.fits';
  runsim(nbase,mapopt,'normal','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,memsim,tagc,usecompiled,maxtime,submit);

end

if docoadd | dorealcoadd | donoisecov
  %loop over sim types, will jump out for real
  for k=simtypes
    k

    coaddopt.sernum=sprintf('%04dxxx%01d',nbase,k);
    if dorealcoadd
      coaddopt.sernum(5:8)='real';
      coaddopt.save_cuts=true;
    else
      coaddopt.save_cuts=false;
    end
    % no tag substitution for real or traditional noise sim:
    if ~dorealcoadd & k~=1
      coaddopt.tagsublist=tagsublist;
    end
    if donoisecov
      %set the noise covariance to polarization only
      coaddopt.do_covariance = 3;
      % also pass back the real map for signal subtraction
      x = load(['maps/',num2str(nbase, '%.4d'),'/real_' daughter '_filtp3_weight3_gs_dp1100_jack0.mat']);
      coaddopt.ac = x.ac;
      coaddopt.m = x.m;
      coaddopt.sernum(5:8)='real';
      % change the daughter name to not overwrite the real map
      coaddopt.daughter = [daughter '_cov'];
      % change the memory usage as well
      memcoaddcoadd = memnoisecov; memcoaddcoadddp = memnoisecov;
      % do only jack=0
      coaddopt.jacktype='0';
    end
    if farmdps
      %loop over the deprojs
      for jj = 1:size(deprojs,1)
        coaddopt.deproj=deprojs(jj,:);
        farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',20,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
      end
    else
      coaddopt.deproj=deprojs;
      farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                              'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                              'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadddp,...
                              'SplitSubmission',20,'UseCompiled',usecompiled,...
                              'maxtime',maxtime,'submit',submit);
    end
    %no loop for real data
    if (dorealcoadd) break; end
    if (donoisecov) break; end 
  end
end

% can be done after real data coadd
if donoise
  % sign_flip noise, see: http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130614_sign_flip_seq/
  
  % first load in real data to fetch the jackmasks
  % we do this in before the loop over the realizations, to avoid the io
  % all the non-temporal jacks get the same sign_flip sequence:
  x= containers.Map();
  x('024689abce')=load(['maps/' num2str(nbase, '%.4d') '/real_' coaddopt.daughter '_filtp3_weight3_gs_dp1100_jack2_s']);
  % all the temporal jacks fetch the individual jackmasks:
  for xx=['1','3','5','7','d']
    disp(['loading jack' xx]);
    x(xx)=load(['maps/' num2str(nbase, '%.4d') '/real_' coaddopt.daughter '_filtp3_weight3_gs_dp1100_jack',xx,'_s']);
  end
  
  % the real data is coming with a large cut stucture, avoid saving this over and over
  % for the noise realizations:
  coaddopt.save_cuts=false;
  coaddopt.sernum=[num2str(nbase, '%.4d') 'real'];
  
  coaddopt.sign_flip_type='by_scanset';
  for ii=rlz
    % fix the random numbers
    stream0 = RandStream('mt19937ar','Seed',ii)
    RandStream.setDefaultStream(stream0);
    
    % loop over the different jacks
    for xx={'024689abce','1','3','5','7','d'}

      % fetch the sign_flip sequence...
      coaddopt.sign_flip_seq=get_sign_flip_seq(x(char(xx)).coaddopt);
      % ...specific for each jack
      coaddopt.jacktype=char(xx);
      % assign a realization number:
      coaddopt.sign_flip_rlz=ii;
      if farmdps
        for jj = 1:size(deprojs,1)
          coaddopt.deproj=deprojs(jj,:);
          farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',[],...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',20,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
        end
      else
        coaddopt.deproj=deprojs;
        farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',[],...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',20,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
      end
    end
  end
end

% Combine comaps
%  xxx3 - reserved for the map sum of 1+2 
%  xxx7 - the map sum 1+5 or 6+5 
%  xxx9 - the sum of 6+5+4 = 7+4
if nbase==1352; ct='1'; end; 
if nbase==2352; ct=''; end;
if docombmaps

  % split in two loops so that the maptype7 is present when 9 is done
  % carfull here, the map that contains the noise must be first!
  for crlz = rlz
  
    % this is not really part of 1352/2352 since type1 is not created...
    % cmd = ['reduc_combcomap(''',num2str(nbase, '%.4d'),'/xxx1_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',2,3,1)'];
    % if nbase==1352  %no normal sim realizations....
    %   cmd = ['reduc_combcomap(''',num2str(nbase, '%.4d'),'/xxx6_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',2,3,1)'];
    % end
    % cmd = strrep(cmd,'xxx',sprintf('%03d',crlz));
    % farmit('farmfiles/',cmd,'queue',queue_other,'mem',16000);
    
    cmd = ['reduc_combcomap(''',num2str(nbase, '%.4d'),'/xxx6_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',5,7,1)'];
    cmd = strrep(cmd,'xxx',sprintf('%03d',crlz));
    farmit('farmfiles/comb/',cmd,'queue',queue_other,'mem',16000,'maxtime',60);
  end

  for crlz = rlz
    cmd = ['reduc_combcomap(''',num2str(nbase, '%.4d'),'/xxx7_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',4,9,1)'];
    cmd = strrep(cmd,'xxx',sprintf('%03d',crlz));
    farmit('farmfiles/comb/',cmd,'queue',queue_other,'mem',16000,'maxtime',60);   
  end

end

return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
