function adjust_lensing(final_file,bsn)
%
% function written to adjust the simulation and the bsn to reflect the lensing 
% power spectrum adjustments suggested by Anthony Lewis
%
% Not fully general - can only handle a single spectrum
%
% inputs:
%     - final file (output from reduc_final.m)
%     - bsn (the simulation cross spectrum of types 4,5, and 6.  Should have 6 columns)
%

% Load in the final
load(final_file);

% load in the bxsxn file
load(bsn);

% load in the lensing models
ml_fix=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing_lensfix.fits');
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing.fits');

% calc the expvals - these are the corrections
for ii=1:length(r)
  rtmp=calc_expvals(r(ii),ml,bpwf);
  lens=rtmp.expv;
  rtmp=calc_expvals(r(ii),ml_fix,bpwf);
  lens_fix=rtmp.expv;
  scale(:,ii)=lens_fix(:,4)./lens(:,4);
end

% first fix the bsn file
% 6 cols: B,S,N,BxS,BxN,SxN
% need to fix the signal sims
aps_fix=aps;
aps_fix(2).Cs_l(:,4,:)=aps(2).Cs_l(:,4,:).*repmat(scale,[1,1,size(aps(2).Cs_l,3)]);
aps_fix(4).Cs_l(:,4,:)=aps(4).Cs_l(:,4,:).*sqrt(repmat(scale,[1,1,size(aps(2).Cs_l,3)]));
aps_fix(6).Cs_l(:,4,:)=aps(6).Cs_l(:,4,:).*sqrt(repmat(scale,[1,1,size(aps(2).Cs_l,3)]));

% now propogate this to the final
r_fix=r;
% expand the bsn file to usable names..
fiducialr=0.2; r_0=0.1;
BB = squeeze(aps_fix(1).Cs_l(:,4,:))*fiducialr/r_0;;
SS = squeeze(aps_fix(2).Cs_l(:,4,:));
NN = squeeze(aps_fix(3).Cs_l(:,4,:));
BS = squeeze(aps_fix(4).Cs_l(:,4,:))*sqrt(fiducialr/r_0);
BN = squeeze(aps_fix(5).Cs_l(:,4,:))*sqrt(fiducialr/r_0);
SN = squeeze(aps_fix(6).Cs_l(:,4,:));

% prepare suppression factor correction:
supfacB = repmat(supfac.rwf(:,4),1,size(SN,2));
% prepare the debias:
db=repmat(r.db(:,4),[1,size(SN,2)]);

% sim and simr change
% sim = P(S+N) = P(S)+P(N)+2*P(SN);
% simr = P(S+N+
r_fix.sim(:,4,:)=(SS+NN+2*SN).*supfacB-db;
r_fix.simr(:,4,:)=(SS+NN+BB+2*(SN+BS+BN)).*supfacB-db;

%save the bsn and final file
bsn=strrep(bsn,'.mat','');
bsn=[bsn '_lensfix'];
aps=aps_fix;
%save(bsn,'aps','apsopt','coaddopt');

%save the final file
final_file=strrep(final_file,'.mat','');
final_file=[final_file '_lensfix'];
r=r_fix;
saveandtest(final_file,'r','bpwf','opt','supfac','inpmod');

