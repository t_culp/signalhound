function make_aps_final(nbase1,nbase2,daughter1,daughter2,make_aps_real,make_aps,make_simset,make_final,coaddtype,queue)
% function make_aps_final(nbase1,nbase2,daughter1,daughter2,make_aps_real,make_aps,make_simset,make_final,coaddtype,queue)
%
% A script that makes the aps and final plots for Keck/BICEP2
% modified from Sarah's make_aps_final_1450.m (JPK)
%
% cross spectra are made via so nbase1_daughter1xnbase2_daughter2
% auto spectra are made when nbase2=[] - set up for B2 only up to now
% 
% Options:
% nbase1, nbase2 (defaults 0751, 1351) 
%          The sernums for the relevant files
% daughter1, daughter1 (defaults 'a','a') 
%          For the respective sernums, 'b'=2013 Keck
% make_aps_real (default 0)
%          Makes the real aps.  farms bpwf for jack0
% make_aps (default 0)
%          Makes sims cross aps.
% make_simset (default 0)
%          turns sims into simset
% make_final (default 0)
%          makes the reduc_final and reduc_final_chi2 pager plots
% coaddtype (default 1)
%          The coaddtype.  if 1, only Keck is modified.
%          If 5, only Keck2012 is modified.
% queue (default 1)
%          The queue to use.  0 for LSF.  1 for serial_requeue
%

if ~exist('nbase1','var')
  nbase1=0751;
end
if ~exist('nbase2','var')
  nbase2=1351;
end
if ~exist('daughter1','var')
  daughter1='a';
end
if ~exist('daughter2','var')
  daughter2='a';
end
if ~exist('make_aps_real','var')
  make_aps_real=0;
end
if ~exist('make_aps','var')
  make_aps=0;
end
if ~exist('make_final','var')
  make_final=0;
end
if ~exist('make_simset','var')
  make_simset=0;
end
if ~exist('coaddtype','var')
  coaddtype=1;
end
if ~exist('queue','var')
  queue=1;
end

% these are used all over the following loops
% just drag them into the workspace:
types= '245679';
deprojs={'1100'};
deprojsreal={'11022','11020','11000'};
jacks = '0';
estimators_jack0 = {'_overall_rxjack','_pureB_overall_rxjack'};
estimators = {'_overall_rxjack','_pureB_overall_rxjack'};
purebs = {'normal','kendrick'};

%Choose the correct queue
if queue
  queue_small='serial_requeue';
  queue_large='serial_requeue';
  license='bicepfs1:1';
else
  queue_small='short_serial';
  queue_large='normal_serial';
  license='';
end

%initialize apsopt
apsopt.update = 1;
apsopt.save_coaddopts=0;
apsopt.overall={[1,2,3],[4,5]};
apsopt.daughter='rxjack';
maxtime=[];
ct1='1'; ct1s='1';
scalefac={1*ones(1,5),1*ones(1,5),sqrt(2)*ones(1,5)};
mem=10000;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cross spectra for real, includes bpwf and experiment jack
% the jack0 run for many hours since it will calculate the bpwf as well.


if make_aps_real

  %finish initializing apsopt
  apsopt.makebpwf=0;
  coaddrxs=[0];
  domats = [0];
  doauto=1;
  apsopt.polrot=[-0.5];
  apsopt.ukpervolt={1};
  for deproj = deprojsreal
    for coaddrx=coaddrxs
      for pure_b = purebs;
        for jack=jacks
          if strcmp(jack,'0')
            apsopt.makebpwf=1;
            maxtime=[];
            queue_use=queue_large; 
          else
            apsopt.makebpwf=0;
            maxtime=30;
            queue_use=queue_small;
          end
          
          apsopt.pure_b = pure_b{:};
          apsopt.coaddrx=coaddrx;
          apsopt.howtojack='dim2';

          % compile the base filename, the replacement string and the cmd 
          fname1=[num2str(nbase1,'%04d'),'/real_' daughter1 '_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1,'.mat'];
          if ~doauto
            replacementstr=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase2,'%04d') ''',''',ct1,'.mat'',''',ct2,'.mat'',''_',daughter1,'_'',''_',daughter2,'_'''];
            cmd = ['reduc_makeaps({''',fname1,''',',replacementstr,'},apsopt)'];
          else
            cmd = ['reduc_makeaps({''',fname1,'''},apsopt)'];
          end

          farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_use,'mem',mem,'maxtime',maxtime,'license',license);  

          
          if strcmp(jack,'0') 
            % Also make experiment jack for jack0
            apsopt.makebpwf=0; apsopt.update=1;
            apsopt.howtojack='dim1'; % experiment jack
            farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_use,'mem',mem,'maxtime',maxtime,'license',license);
          end

        end
      end
    end
  end
end

% cross spectra for the sims
if make_aps
  apsopt.update=1;
  apsopt.makebpwf=0;
  apsopt.polrot=[];
  coaddrxs=[0];
  domats = [0];
  maxtime= 120;
  %setup the abscals
  apsopt.ukpervolt={1}; coaddrxs=0; doauto=1;
  for coaddrx=coaddrxs
    for type = types
      %make the type 9 have r=0.02
      if type=='9'
        if coaddrx
          apsopt.scalefac=scalefac_overrx;
        else
          apsopt.scalefac=scalefac;
        end
      else
        % otherwise make sure scalec is not a field
        if isfield(apsopt,'scalefac');
          apsopt=rmfield(apsopt,'scalefac');
        end
        apsopt.update=1;
      end
      for pure_b = purebs;
        for jack=jacks
          apsopt.coaddrx=coaddrx;
          apsopt.pure_b=pure_b{:};
          apsopt.howtojack='dim1';

          fname1=[num2str(nbase1,'%04d'),'/[01234]??',type,'_' daughter1 '_filtp3_weight3_gs_dp1100_jack',jack,ct1s,'.mat'];
          if ~doauto
            replacementstr=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase2,'%04d') ''',''',ct1s,'.mat'',''',ct2s,'.mat'',''_',daughter1,'_'',''_',daughter2,'_'''];
            cmd = ['reduc_makeaps({''',fname1,''',',replacementstr,'},apsopt)'];
          else
            cmd = ['reduc_makeaps({''',fname1,'''},apsopt)'];
          end
        
          farmit('farmfiles/aps/',cmd,'var','apsopt','queue',queue_small,'mem',mem,'maxtime',maxtime,'license',license);
          
          if strcmp(jack,'0') && nbase1==1351 && any(strcmp(type,{'2','4'}))
            % also do jack0 for xxx2 and xxx4
            apsopt.howtojack='dim2'; % jack0
            farmit('farmfiles/aps/',cmd,'var','apsopt','queue',queue_small,'mem',mem,'maxtime',maxtime,'license',license);
          end

        end
      end   
    end
  end
end

% simsets cross:
if make_simset
  doauto=1;
  jacks='f';
  folder='1351'; 
  for jack=jacks
    for type = types
      for deproj = deprojs
        estimators_loop=estimators;
        if strcmp(jack,'0'); estimators_loop=estimators_jack0; end;
        for estimator=estimators_loop
          if doauto
            fname=[folder '/[01234]??',type,'_',daughter1,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1s,estimator{:},'.mat'];
          else
            fname=[folder '/[01]??',type,'_',daughter1,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1s,'_[01]??',type,'_',daughter2,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct2s,estimator{:},'.mat'];
          end
          cmd = ['reduc_makesimset(''' fname ''')'];
          farmit('farmfiles/simset/',cmd,'queue',queue_small,'mem',10000,'maxtime',60,'license',license);
        end
      end
    end
  end
end

% reduc_final and reduc_final_chi2 cross:
if make_final
  disp('make_final')
  directbpwfs=0;
  jacks='f';
  %  pl_cross selects the rx combination to plot
  folder='1353'; doauto=1; pl_cross=[1,2]; 
  folders='1351';
  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  for jack=jacks
    for deproj = deprojsreal
      estimators_loop=estimators;
      if strcmp(jack,'0'); estimators_loop=estimators_jack0; end;
      for estimator=estimators_loop
        for directbpwf=directbpwfs
          rbcs=[0]; % residual beam correction in few cases
          for rbc=rbcs
            % if jack0 then also do the spectral jack:
            diffspecs=0;
            for diffspec = diffspecs
              rots=[1];
              for rot = rots              
                clear finalopt;
                finalopt.doblind = 0;
                rfcopt.diffspec = diffspec;            
                
                deprojS=deproj{:};
                % when subtraction instead of regression in real data, 
                % use the undeprojected sim but just in the case that the deprojected
                % systematic was not simulated (diff ell yes but not diff pointing):
                if strcmp(deproj{:},'1202') continue; end
                if strcmp(deproj{:},'1102') deprojS='1100'; end
                if strcmp(deproj{:},'2102') deprojS='0100'; end
                  if length(deproj{:})==5; deprojS='1100'; end

                if doauto
                  % no over rx for bicep2 auto
                  r = sprintf([folder '/real_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1  'BB']); %5 for the per-tile abscal
                  n = sprintf([folders '/xxx6_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s 'BB']); %signflip
                  s = sprintf([folders '/xxx2_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s 'BB']); %yes, always jack0, unlensed
                  sn= sprintf([folders '/xxx7_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s 'BB']); %lensed
                  b = sprintf([folders '/xxx4_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s 'BB']); %yes, always jack0
                  snr=sprintf([folders '/xxx9_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s 'BB']); %signal+noise+r
                else
                  r = sprintf([folder '/real_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1  '_real_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2  'BB']); %5 for the per-tile abscal
                  n = sprintf([folder '/xxx6_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx6_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2s 'BB']); %signflip
                  s = sprintf([folder '/xxx2_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s '_xxx2_' daughter2 '_filtp3_weight3_gs_dpDDDD_jack0' ct2s 'BB']); %yes, always jack0, unlensed
                  sn= sprintf([folder '/xxx7_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx7_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2s 'BB']); %lensed
                  b = sprintf([folder '/xxx4_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s '_xxx4_' daughter2 '_filtp3_weight3_gs_dpDDDD_jack0' ct2s 'BB']); %yes, always jack0
                  snr=sprintf([folder '/xxx9_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx9_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2s 'BB']); %lensed
                end
                
                r = strrep(r,'DDDD',deproj{:}); r = strrep(r,'BB',estimator{:}); r = strrep(r,'J',jack);
                n = strrep(n,'DDDD',deprojS); n = strrep(n,'BB',estimator{:}); n = strrep(n,'J',jack);
                s = strrep(s,'DDDD',deprojS); s = strrep(s,'BB',estimator{:}); s = strrep(s,'J',jack);
                sn= strrep(sn,'DDDD',deprojS); sn= strrep(sn,'BB',estimator{:}); sn= strrep(sn,'J',jack);
                b = strrep(b,'DDDD',deprojS); b = strrep(b,'BB',estimator{:}); b = strrep(b,'J',jack);
                snr= strrep(snr,'DDDD',deprojS); snr= strrep(snr,'BB',estimator{:}); snr= strrep(snr,'J',jack);

                if strcmp(deproj{:},'1102')
                  r = strrep(r,'filt','fixesub_filt');  %change to the fixed maps
                end

                % now all stored in finalopt
                finalopt.nsimset=n; finalopt.ssimset=s; finalopt.snsimset=sn; finalopt.bosimset=b; finalopt.snrsimset=snr;
              
                % handle no rotation case
                if ~rot
                  r = [r,'_norot'];
                end
                
                % for b2 auto also use the direct bpwfs
                rc = r;
                if directbpwf
                  finalopt.supfacstyle='direct_bpwf';
                  if nbase1==0751
                    finalopt.bpwfname=['0704/xxxx_allcmb_filtp3_weight3_gs_dp',deprojS,'_jack0',estimator{:},'_directbpwf.mat'];
                  elseif nbase1==1351 && nbase2==1351
                    finalopt.bpwfname=['1332/xxxx_cmb_subset2012_2013_allrx_filtp3_weight3_gs_dp',deprojS,'_jack0_matrix_directbpwf.mat'];
                  end
                  % append _directbpwf to the final file name for reduc_final_chi2
                  rc = [r,'_directbpwf'];
                end
                if rbc
                  finalopt.residbeamcorr='aps/0751/beammap_leakage_spectra_1000rlz_usewithdp1102';
                  finalopt.daughter='_rbc';
                  rc = [rc,'_rbc'];
                end

                % define the farm jobs.  does the final plots for max ell=200,500,
                % saves the final file, and does chi2 plot for max ell=200,500
                cmd=[...
                'reduc_final(r,finalopt,2,200);close all;',...
                'reduc_final(r,finalopt,2,500);close all;',...
                'reduc_final(r,finalopt,0,500);close all;',...
                'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
                'reduc_final_chi2(rc,rfcopt,500,2);close all;'];

                % when not coadded over Keck revceiver also do the other plot combinations:
                % B2xRx2, B2xRx3 etc
                if isempty(strfind(estimator{:},'overrx'))
                  for jj = 1:size(pl_cross,1)
                    rfcopt.pl_cross=pl_cross(jj,:);
                    finalopt.pl_cross=rfcopt.pl_cross;
                    
                    % only make the final file once - rm from cmd otherwise
                    if jj~=1
                      cmd=[...
                      'reduc_final(r,finalopt,2,200);close all;',...
                      'reduc_final(r,finalopt,2,500);close all;',...
                      'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
                      'reduc_final_chi2(rc,rfcopt,500,2);close all;'];
                    end
                    farmit('farmfiles/final/',cmd,'var',{'r','rc','n','s','sn','b','rfcopt','finalopt'},'queue',queue_small,'mem',16000,'maxtime',60,'license',license);
                  end
                else
                  rfcopt.pl_cross=[1,2]; finalopt.pl_cross=rfcopt.pl_cross;
                  farmit('farmfiles/final/',cmd,'var',{'r','rc','n','s','sn','b','rfcopt','finalopt'},'queue',queue_small,'mem',16000,'maxtime',60,'license',license);
                end
                
              end  %loops
            end
          end
        end
      end
    end
  end
end

