function make_aps_final_BK14(do_single,make_aps_real,make_aps,make_simset,make_final)
% make_aps_final_BK14(do_single,do_comb,make_aps_real,make_aps,make_simset,make_final)
%
% makes:
%  do_single      = 1: the K2014 spectra including jacks
%  do_single      = 0: the BK14 spectra, run combine_BK13_K2014 first to make
%                      the coadded maps of BK13 + K2014 first
%  
% for the two do_single options, one has do go through make_aps_real, make_aps
%  make_simset and make_final one by one to produce all data products
% 

if ~exist('make_aps_real','var')
  make_aps_real=0;
end
if ~exist('make_aps','var')
  make_aps=0;
end
if ~exist('make_final','var')
  make_final=0;
end
if ~exist('make_simset','var')
  make_simset=0;
end

% this is K2014
nbase1 = 1351
daughter1 = 'd'
ct1='1'; % the per year coadd are usually per rx

% this is the BK14
nbase2 = 1459
daughter2 = 'aabd'
ct2=''; % the combined maps are coadded over rx
  
% these are used all over the following loops
% just drag them into the workspace:
types= '245679';
deprojs={'1100'};
deprojsreal={'1100','1102'};

jacks = get_default_coaddopt();
jacks = jacks.jacktype;

% auto spectrum
purebs = {'normal','kendrick'};

%Choose the correct queue
queue='serial_requeue,general,itc_cluster';

%initialize apsopt
apsopt.update = 1;
apsopt.save_coaddopts=0;
maxtime=120;

% also initialize scale factors...
scalefac={1*ones(1,3),1*ones(1,3),sqrt(2)*ones(1,3)};

% here be carefull to pick the right options for the single year map...:
if do_single
  % http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20150220_birefringence/
  apsopt.polrot=[-0.5,0];
  apsopt.ukpervolt={get_ukpervolt('2014')};
  purifmatname={...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/1351/healpix_red_spectrum_lmax700_beamKuber100rev1_reob1351_d_100GHz_QQQUUU_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/1459/healpix_red_spectrum_lmax700_beamB2bbns_reob1459_aabd_150GHz_QQQUUU_proj.mat'};
  estimators_j0 = {'_overfreq','_pureB_overfreq','_matrix_overfreq'};
  estimators={'_overfreq','_pureB_overfreq'};
  nbase = nbase1;
  daughter = daughter1;
  ct = ct1;
  mapname = {'K2014_95','K2014_150'}
% ... and the combined map. The inputs above and below need additional analysis to be filled:
else
  % till now no jacks for the combined data set
  jacks = ['0'];
  % http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20150220_birefringence/
  apsopt.polrot=[-0.5,-0.6];
  % the combined maps have the per-year abscal applied
  apsopt.ukpervolt={1,1};
  purifmatname={...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/1351/healpix_red_spectrum_lmax700_beamKuber100rev1_reob1351_d_100GHz_QQQUUU_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/1459/healpix_red_spectrum_lmax700_beamB2bbns_reob1459_aabd_150GHz_QQQUUU_proj.mat'};
  estimators_j0 = {'_overfreq_overall','_pureB_overfreq_overall','_matrix_overfreq_overall'};
  estimators={'_overfreq_overall','_pureB_overfreq_overall'};
  nbase = nbase2;
  daughter = daughter2;
  ct = ct2;
  mapname = {'BK14_95','BK14_150'}
end

% cross spectra for real, includes bpwf and experiment jack
% the jack0 run for many hours since it will calculate the bpwf as well.
if make_aps_real

  %finish initializing apsopt
  apsopt.makebpwf=0;
  coaddrx=2;
  
  for deproj = deprojsreal
    deproj1=deproj{1}(1:4);
    
    for jack=jacks       
      
      if strcmp(jack,'0')
        apsopt.makebpwf=1;
        apsopt.update=0;
        maxtime=60*8;
      else
        apsopt.makebpwf=0;
        apsopt.update=1;
        maxtime=60;
      end
      
      % Loop through with and without the matrix for jack0 only
      domat=0;
      if strcmp(jack,'0')
        domat=[0 1];
      end

      for mat=domat
        pb=purebs;
        if mat
          pb={'normal'};
        end
        
        for pure_b = pb;

          % Setup below for matrix purification
          if mat
            % add the matrix names
            apsopt.purifmatname=purifmatname;
            mem=60000; queue=queue;
          else
            % make sure purifmatname is not a field
            if isfield(apsopt,'purifmatname')
              apsopt=rmfield(apsopt, 'purifmatname');
            end
            mem=15000;
          end

          apsopt.pure_b = pure_b{:};
          apsopt.coaddrx=coaddrx;
          apsopt.howtojack='dim2';

          % compile the base filename, the replacement string and the cmd 
          fname=[num2str(nbase,'%04d'),'/real_' daughter '_filtp3_weight3_gs_dp',deproj1,'_jack',jack,ct,'.mat'];
          cmd = ['reduc_makeaps(fname,apsopt)'];

          % keyboard
          
          farmit('farmfiles/realaps/',cmd,'var',{'apsopt','fname'},'queue',queue,'mem',mem,'maxtime',maxtime);
          % eval(cmd)
          
          % Now do again but without rotation (only for jack0)
          if strcmp(jack,'0') & ~isempty(apsopt.polrot)
            % remove the rotation
            oldpolrot=apsopt.polrot; 
            apsopt.polrot = [0,0];
          
            % Add a _norot at the end of the output filename
            apsopt.daughter='norot';
            farmit('farmfiles/realaps/',cmd,'var',{'apsopt','fname'},'queue',queue,'mem',mem,'maxtime',maxtime);

            % revert back to normal setting
            apsopt.polrot=oldpolrot; apsopt=rmfield(apsopt,'daughter');
          end

          if strcmp(jack,'0') && ~do_single
            % Also make experiment jack for jack0
            apsopt.makebpwf=0; apsopt.update=1;
            apsopt.howtojack='dim1'; % experiment jack
            farmit('farmfiles/realaps/',cmd,'var',{'apsopt','fname'},'queue',queue,'mem',mem,'maxtime',maxtime);
            % eval(cmd)
            apsopt.makebpwf=1; %return to old settings
          end

        end
      end
    end
  
  end
end

% cross spectra for the sims
if make_aps
  apsopt.update=1;
  apsopt.makebpwf=0;
  apsopt.polrot=[];
  coaddrx=2;

  
  for type = types
    %make the type 9 have r=0.02
    if type=='9'
      apsopt.scalefac=scalefac;
    else
      % otherwise make sure scalec is not a field
      if isfield(apsopt,'scalefac');
        apsopt=rmfield(apsopt,'scalefac');
      end
      apsopt.update=1;
    end
    for pure_b = purebs;
      for jack=jacks
        apsopt.coaddrx=coaddrx;
        apsopt.pure_b=pure_b{:};
        
        % Loop through with and without the matrix for jack0 only
        domat=0;
        if jack=='0' | jack=='f'
          domat=[0 1];
        end

        for mat=domat

          % Setup below for matrix purification
          if mat
            % add the matrix names
            apsopt.purifmatname=purifmatname;
            mem=30000;
          else
            % make sure purifmatname is not a field
            if isfield(apsopt,'purifmatname')
              apsopt=rmfield(apsopt, 'purifmatname');
            end
            mem=15000;
          end

          fname=[num2str(nbase,'%04d'),'/[01234]??',type,'_' daughter '_filtp3_weight3_gs_dp1100_jack',jack,ct,'.mat'];
          cmd = ['reduc_makeaps(fname,apsopt)'];
                      
          apsopt.howtojack='dim2';
          farmit('farmfiles/aps/',cmd,'var',{'apsopt','fname'},'queue',queue,'mem',mem,'maxtime',maxtime,'submit',0);

          if strcmp(jack,'0')
            apsopt.howtojack='dim1'; % experiment jack
            farmit('farmfiles/aps/',cmd,'var',{'apsopt','fname'},'queue',queue,'mem',mem,'maxtime',maxtime,'submit',0);
          end
          
        end
      end
    end   
  end

end

% simsets cross:
if make_simset
  
  jacks=[jacks,'f'];
  
  for jack=jacks
    for type = types
      for deproj = deprojs
        if jack=='0'
          estimators_loop=estimators_j0;
        else
          estimators_loop=estimators;
        end
        for estimator=estimators_loop
          fname=[num2str(nbase),'/[01234]??',type,'_',daughter,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct,estimator{:},'.mat'];
          cmd = ['reduc_makesimset(fname)'];
          farmit('farmfiles/simset/',cmd,'var','fname','queue',queue,'mem',10000,'maxtime',60,'submit',0);
        end
      end
    end
  end
  
end

% reduc_final and reduc_final_chi2 cross:
if make_final
  
  jacks=[jacks,'f'];
  

  directbpwfs=[0];
  %  pl_cross selects the rx combination to plot
  pl_cross = [1,2]; %that's ok
  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  
  for jack=jacks
    for deproj = deprojsreal
      deproj1=deproj{1}(1:4);
      if jack=='f'; 
        directbpwfs=[0]; %don't do the directbpwf for jackf
      end
      for directbpwf=directbpwfs
        
        if jack=='0' | jack=='f'
          estimators_loop=estimators_j0;
        else
          estimators_loop=estimators;
        end

        for estimator=estimators_loop
          % if jack0 then also do the spectral jack:
          diffspecs=0;
          if (jack=='0')
            diffspecs=[0,1];
          end
          
          for diffspec = diffspecs
            rots = 1;
            if (jack=='0' && ~diffspec)  rots=[0,1]; end;
              
            for rot = rots
              clear finalopt;
              finalopt.mapname=mapname;
              
              rfcopt.mapname=finalopt.mapname;
              rfcopt.diffspec = diffspec;            
              
              deprojS=deproj{:};
              % when subtraction instead of regression in real data, 
              % use the undeprojected sim but just in the case that the deprojected
              % systematic was not simulated (diff ell yes but not diff pointing):
              if strcmp(deproj{:},'1202') continue; end
              if strcmp(deproj{:},'1102') deprojS='1100'; end
              if strcmp(deproj{:},'2102') deprojS='0100'; end
              if length(deproj{:})==5; deprojS='1100'; end
              
              r = sprintf([num2str(nbase),'/real_',daughter,'_filtp3_weight3_gs_dpDDDD_jackJ',ct ,'BB']); %5 for the per-tile abscal
              n = sprintf([num2str(nbase),'/xxx6_',daughter,'_filtp3_weight3_gs_dpDDDD_jackJ',ct,'BB']); %signflip
              s = sprintf([num2str(nbase),'/xxx2_',daughter,'_filtp3_weight3_gs_dpDDDD_jack0',ct,'BB']); %yes, always jack0, unlensed
              sn= sprintf([num2str(nbase),'/xxx7_',daughter,'_filtp3_weight3_gs_dpDDDD_jackJ',ct,'BB']); %lensed
              b = sprintf([num2str(nbase),'/xxx4_',daughter,'_filtp3_weight3_gs_dpDDDD_jack0',ct,'BB']); %yes, always jack0
              snr=sprintf([num2str(nbase),'/xxx9_',daughter,'_filtp3_weight3_gs_dpDDDD_jackJ',ct,'BB']); %lensed
              
              r=regexprep(r,'DDDD',deproj1,'once'); 
              r = strrep(r,'DDDD',deproj{:}); r = strrep(r,'BB',estimator{:}); r = strrep(r,'J',jack);
              n = strrep(n,'DDDD',deprojS); n = strrep(n,'BB',estimator{:}); n = strrep(n,'J',jack);
              s = strrep(s,'DDDD',deprojS); s = strrep(s,'BB',estimator{:}); s = strrep(s,'J',jack);
              sn= strrep(sn,'DDDD',deprojS); sn= strrep(sn,'BB',estimator{:}); sn= strrep(sn,'J',jack);
              b = strrep(b,'DDDD',deprojS); b = strrep(b,'BB',estimator{:}); b = strrep(b,'J',jack);
              snr= strrep(snr,'DDDD',deprojS); snr= strrep(snr,'BB',estimator{:}); snr= strrep(snr,'J',jack);

              % now all stored in finalopt
              finalopt.nsimset=n; finalopt.ssimset=s; finalopt.snsimset=sn; finalopt.bosimset=b; finalopt.snrsimset=snr;
              
              if ~rot
                r = [r,'_norot'];
              end
              
              % for b2 auto also use the direct bpwfs
              rc = r;
              if directbpwf
                finalopt.supfacstyle='direct_bpwf';
                rc = [r,'_directbpwf'];
                finalopt.bpwfname=['0706x1332/xxxx_allcmb_filtp3_weight3_gs_dp1100_jack0_xxxx_cmb_subset2012_2013_rxall_filtp3_weight3_gs_dp1100_jack0_matrix_directbpwf.mat'];
              end

              % define the farm jobs.  does the final plots for max ell=200,500,
              % saves the final file, and does chi2 plot for max ell=200,500
              cmd=[...
                'reduc_final(r,finalopt,0,500);close all;',...
                'reduc_final(r,finalopt,2,500);close all;',...
                'reduc_final(r,finalopt,2,200);close all;',...
                'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
                'reduc_final_chi2(rc,rfcopt2,500,2);close all;'];

              rfcopt.pl_cross=pl_cross; finalopt.pl_cross=rfcopt.pl_cross;
              
              rfcopt2 = rfcopt;
              rfcopt2.chibins=get_chibins(10);
              farmit('farmfiles/final/',cmd,'var',{'r','rc','n','s','sn','b','rfcopt','rfcopt2','finalopt'},'queue',queue,'mem',10000,'maxtime',30,'submit',0);
              %eval(cmd)
            end
          end
        end
      end
    end
  end
end
