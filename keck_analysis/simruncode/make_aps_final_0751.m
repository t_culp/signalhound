function make_aps_final(nbase1,nbase2,daughter1,daughter2,make_aps_real,make_aps,make_simset,make_final,coaddtype,queue)
% function make_aps_final(nbase1,nbase2,daughter1,daughter2,make_aps_real,make_aps,make_simset,make_final,coaddtype,queue)
%
% A script that makes the aps and final plots for Keck/BICEP2
% modified from Sarah's make_aps_final_1450.m (JPK)
%
% cross spectra are made via so nbase1_daughter1xnbase2_daughter2
% auto spectra are made when nbase2=[] - set up for B2 only up to now
% 
% Options:
% nbase1, nbase2 (defaults 0751, 1351) 
%          The sernums for the relevant files
% daughter1, daughter1 (defaults 'a','a') 
%          For the respective sernums, 'b'=2013 Keck
% make_aps_real (default 0)
%          Makes the real aps.  farms bpwf for jack0
% make_aps (default 0)
%          Makes sims cross aps.
% make_simset (default 0)
%          turns sims into simset
% make_final (default 0)
%          makes the reduc_final and reduc_final_chi2 pager plots
% coaddtype (default 1)
%          The coaddtype.  if 1, only Keck is modified.
%          If 5, only Keck2012 is modified.
% queue (default 1)
%          The queue to use.  0 for LSF.  1 for serial_requeue
%

if ~exist('nbase1','var')
  nbase1=0751;
end
if ~exist('nbase2','var')
  nbase2=1351;
end
if ~exist('daughter1','var')
  daughter1='a';
end
if ~exist('daughter2','var')
  daughter2='a';
end
if ~exist('make_aps_real','var')
  make_aps_real=0;
end
if ~exist('make_aps','var')
  make_aps=0;
end
if ~exist('make_final','var')
  make_final=0;
end
if ~exist('make_simset','var')
  make_simset=0;
end
if ~exist('coaddtype','var')
  coaddtype=1;
end
if ~exist('queue','var')
  queue=1;
end

% these are used all over the following loops
% just drag them into the workspace:
types= '23456789';
deprojs={'1100'};
deprojsreal={'1102','1100'};
jacks = get_default_coaddopt();
jacks = jacks.jacktype;
% auto spectrum
if isempty(nbase2) && nbase1==0751
  estimators_jack0 = {'','_pureB','_matrix'};
  estimators = {'','_pureB'};
elseif nbase1==1351 && isempty(nbase2) && daughter1=='c'
  estimators_jack0 = {'_overfreq','_pureB_overfreq'};
  estimators = estimators_jack0;
elseif nbase1==1351 && isempty(nbase2) && daughter1=='d'
  estimators_jack0 = {'_overfreq','_pureB_overfreq','_matrix_overfreq','_pureB_matrix_overfreq'};
  estimators = {'_overfreq','_pureB_overfreq'};
elseif nbase1==0751 && nbase2==1351 && daughter2=='c'
  % estimators_jack0 = {'_matrix_overfreq','_pureB_matrix_overfreq'};
  estimators_jack0 = {'_overfreq','_pureB_overfreq','_matrix_overfreq','_pureB_matrix_overfreq'};
  estimators = {'_overfreq','_pureB_overfreq'};
elseif nbase1==1351 && isempty(nbase2)
  estimators_jack0 = {'_overrx','_pureB_overrx','_matrix_overrx'};
  estimators = {'_overrx','_pureB_overrx'};
elseif nbase1==1351 && nbase2==1351 %also do keck addition
  estimators_jack0 = {'','_pureB','_overrx','_pureB_overrx', '_matrix', '_matrix_overrx','_pureB_matrix','_pureB_matrix_overrx',...
  '_overrx_overall','_pureB_overrx_overall','_matrix_overrx_overall'};
  estimators = {'','_pureB','_overrx','_pureB_overrx','_overrx_overall','_pureB_overrx_overall'};
else
  estimators_jack0 = {'','_pureB','_overrx','_pureB_overrx', '_matrix', '_matrix_overrx','_pureB_matrix','_pureB_matrix_overrx'};
  estimators = {'','_pureB','_overrx','_pureB_overrx'};
end
purebs = {'normal','kendrick'};

%Choose the correct queue
if queue
  queue_small='serial_requeue,itc_cluster';
  queue_large='serial_requeue,itc_cluster';
  license='';
else
  queue_small='short_serial';
  queue_large='normal_serial';
  license='';
end

%initialize apsopt
apsopt.update = 1;
apsopt.save_coaddopts=0;
maxtime=[];

%initialize coaddtype
if nbase1==0751 && isempty(nbase2)
  ct1='';
  ct1s='';
  if coaddtype==2
    ct1='2';
  end
elseif nbase1==1351 && isempty(nbase2)
  ct1='1'; ct1s='1';
elseif nbase1==0751 && nbase2==1351 
  ct1=''; ct2='1'; 
  ct1s=''; ct2s='1';
  if coaddtype==5 && daughter2=='a'
    ct2='5';
  elseif coaddtype==2
    ct1='2'; ct2='2';
  end
elseif nbase1==1351 && nbase2==1351
  ct1='1'; ct2='1';
  ct1s='1'; ct2s='1';
  if coaddtype==5 && daughter1=='a'
    ct1='5';
  elseif coaddtype==2
    ct1='2'; ct2='2';
  end
end

%initialize purification matrix names for the different cases
% also initialize scale factors...
if nbase1==0751 && isempty(nbase2) && daughter1=='a'
  % Bicep2 auto, hand in two for the SN x B case.
  purifmatname={'/n/panlfs2/bicep/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat',...
  '/n/panlfs2/bicep/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat',...
  '/n/panlfs2/bicep/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat'};
  scalefac={1*ones(1,2),1*ones(1,2),sqrt(2)*ones(1,2)};
elseif nbase1==1351 && isempty(nbase2) && strcmp(daughter1,'ab')
  purifmatname_overrx={'/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat'};
  scalefac={1*ones(1,2),1*ones(1,2),sqrt(2)*ones(1,2)};
elseif nbase1==1351 && isempty(nbase2) && strcmp(daughter1,'c')
  % Keck2014 mid-season
  purifmatname={};
  % is here for now used for _overfreq
  scalefac_overrx={1*ones(1,2),1*ones(1,2),sqrt(2)*ones(1,2)};
elseif nbase1==1351 && isempty(nbase2) && strcmp(daughter1,'d')
  purifmatname_overfreq = {...
    '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/1351/healpix_red_spectrum_lmax700_beamKuber100rev1_reob1351_d_100GHz_QQQUUU_proj.mat',...
    '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/1459/healpix_red_spectrum_lmax700_beamB2bbns_reob1459_aabd_150GHz_QQQUUU_proj.mat'};
  scalefac_overfreq = {1*ones(1,2), 1*ones(1,2), sqrt(2)*ones(1,2)};
elseif nbase1==0751 && nbase2==1351 && daughter2=='a'
  % Bicep2 x Keck2012
  purifmatname_overrx={'/n/panlfs2/bicep/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rxa_proj.mat'};
  purifmatname={'/n/panlfs2/bicep/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rx0_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rx1_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rx2_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rx3_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rx4_proj.mat'};
  scalefac_overrx={1*ones(1,2),1*ones(1,2),sqrt(2)*ones(1,2)};
  scalefac={1*ones(1,6),1*ones(1,6),sqrt(2)*ones(1,6)};
elseif nbase1==0751 && nbase2==1351 && daughter2=='b'
  % Bicep2 x Keck2013
  purifmatname_overrx={'/n/panlfs2/bicep/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0705/healpix_red_spectrum_lmax700_beamB2bbns_reob0705_rxa_proj.mat'};
  purifmatname={'/n/panlfs2/bicep/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat',[],[],[],[],[]};
  scalefac_overrx={1*ones(1,2),1*ones(1,2),sqrt(2)*ones(1,2)};
  scalefac={1*ones(1,6),1*ones(1,6),sqrt(2)*ones(1,6)};
elseif nbase1==0751 && nbase2==1351 && daughter2=='c'
  % Bicep2 x Keck2014
  purifmatname_overfreq={'/n/panlfs2/bicep/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat',[],[]};
  scalefac_overfreq={1*ones(1,3),1*ones(1,3),sqrt(2)*ones(1,3)};
elseif nbase1==1351 && nbase2==1351 && daughter1=='a' && daughter2=='b'
  purifmatname_overrx={'/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rxa_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0705/healpix_red_spectrum_lmax700_beamB2bbns_reob0705_rxa_proj.mat'};
  purifmatname={'/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rx0_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rx1_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rx2_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rx3_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rx4_proj.mat',[],[],[],[],[]};
  scalefac_overrx={1*ones(1,2),1*ones(1,2),sqrt(2)*ones(1,2)};
  scalefac={1*ones(1,10),1*ones(1,10),sqrt(2)*ones(1,10)};
  purifmatname_comb={'/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat'};
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cross spectra for real, includes bpwf and experiment jack
% the jack0 run for many hours since it will calculate the bpwf as well.

%setup the abscal per pair stuff
currentdir=pwd;
cd ../bicep2_analysis/
[p1 ind1]=get_array_info('20120101',[],[],[],[],[],[],'obs');
ukpv_b2=nanmean([p1.ukpv(ind1.a) p1.ukpv(ind1.b)],2);
cd ../keck_analysis/
[p1 ind1]=get_array_info('20120101',[],[],[],[],[],[],'obs');
ukpv_k12=nanmean([p1.ukpv(ind1.a) p1.ukpv(ind1.b)],2);
[p1 ind1]=get_array_info('20130101',[],[],[],[],[],[],'obs');                              
ukpv_k13=nanmean([p1.ukpv(ind1.a) p1.ukpv(ind1.b)],2);
cd(currentdir);
pwd

if make_aps_real

  %finish initializing apsopt
  apsopt.makebpwf=0;
  coaddrxs=[0,1];
  domats = [0 1];
  doauto=0;
  if nbase1==0751 && isempty(nbase2) && daughter1=='a'
    % auto spectra only!
    apsopt.polrot=[-1.1];
    apsopt.ukpervolt={3150};
    coaddrxs=0;
    doauto=1;
  elseif nbase1==1351 && isempty(nbase2) && strcmp(daughter1,'ab')
    apsopt.polrot=[-0.5];
    apsopt.ukpervolt={1};
    coaddrxs=1; doauto=1;
  elseif nbase1==1351 && isempty(nbase2) && strcmp(daughter1,'c')
    apsopt.polrot=[];
    apsopt.ukpervolt={get_ukpervolt('2014')};
    coaddrxs=2; doauto=1;
    domats = [0];
  elseif nbase1==1351 && isempty(nbase2) && strcmp(daughter1,'d')
    % http://bicep.rc.fas.harvard.edu/bkcmb/analysis_logbook/analysis/20150220_birefringence/
    apsopt.polrot=[-0.5, 0];
    apsopt.ukpervolt = {get_ukpervolt('2014')};
    coaddrxs = 2;
    doauto = 1;
    domats = [0 1];
  elseif nbase1==0751 && nbase2==1351 && daughter2=='a'
    apsopt.polrot=[-1.1,-0.5];
    apsopt.ukpervolt={3150,3400};
    if coaddtype==5
      %setup the per-tile abscal for Keck.  
      [pp kk]=ParameterRead('aux_data/abscal/abscal_tile_20120101.csv');  % aux_data pointed to keck's here
      apsopt.ukpervolt={3150,pp.ukpv};
    elseif coaddtype==2
      %setup the bicep2 abscal per pair
      apsopt.ukpervolt={ukpv_b2,ukpv_k12};
    end
  elseif nbase1==0751 && nbase2==1351 && daughter2=='b'
    apsopt.polrot=[-1.1,-0.7];
    apsopt.ukpervolt={3150,2900};
    if coaddtype==2
      apsopt.ukpervolt={ukpv_b2,ukpv_k13};
    end
  elseif nbase1==0751 && nbase2==1351 && daughter2=='c'
    apsopt.polrot=[-1.1,0];
    apsopt.ukpervolt={3150,get_ukpervolt('2014')};
    coaddrxs=2;
  elseif nbase1==1351 && nbase2==1351 && daughter2=='b' && daughter1=='a'
    apsopt.polrot=[-0.5,-0.7];
    apsopt.ukpervolt={3400,2900};
    if coaddtype==5
      [pp kk]=ParameterRead('aux_data/abscal/abscal_tile_20120101.csv');  % aux_data pointed to keck's here
      apsopt.ukpervolt={pp.ukpv,2900};
    elseif coaddtype==2
      apsopt.ukpervolt={ukpv_k12,ukpv_k13};
    end
  end
  for deproj = deprojsreal
    daughter1c = daughter1; daughter2c=daughter2;
    if strcmp(deproj{:},'1102') && ~strcmp(daughter1,'d')
      daughter1c=[daughter1 '_fixesub']; daughter2c=[daughter2 '_fixesub'];
    end
    for coaddrx=coaddrxs
      for pure_b = purebs;
        for jack=jacks
          if strcmp(jack,'0')
            apsopt.makebpwf=1;
            maxtime=180;
            queue_use=queue_large; 
          else
            apsopt.makebpwf=0;
            maxtime=30;
            queue_use=queue_small;
          end
          
          % Loop through with and without the matrix for jack0 only
          domat=0;
          if strcmp(jack,'0')
            domat=domats;
          end

          for mat=domat

            % Setup below for matrix purification
            if mat
              % add the matrix names
              switch coaddrx
               case 0
                apsopt.purifmatname=purifmatname;
               case 1
                apsopt.purifmatname=purifmatname_overrx;
               case 2
                apsopt.purifmatname=purifmatname_overfreq;
              end
              mem=50000; queue_use=queue_large;
            else
              % make sure purifmatname is not a field
              if isfield(apsopt,'purifmatname')
                apsopt=rmfield(apsopt, 'purifmatname');
              end
              mem=15000;
            end

            apsopt.pure_b = pure_b{:};
            apsopt.coaddrx=coaddrx;
            apsopt.howtojack='dim2';

            % compile the base filename, the replacement string and the cmd 
            fname1=[num2str(nbase1,'%04d'),'/real_' daughter1c '_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1,'.mat'];
            if ~doauto
              replacementstr=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase2,'%04d') ''',''',ct1,'.mat'',''',ct2,'.mat'',''_',daughter1c,'_'',''_',daughter2c,'_'''];
              cmd = ['reduc_makeaps({''',fname1,''',',replacementstr,'},apsopt)'];
            else
              cmd = ['reduc_makeaps({''',fname1,'''},apsopt)'];
            end
            
            farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_use,'mem',mem,'maxtime',maxtime,'submit',0);

            % Now do again but without rotation (only for jack0)
            if strcmp(jack,'0') & ~isempty(apsopt.polrot)
              % remove the rotation
              oldpolrot=apsopt.polrot; 
              apsopt.polrot = [0,0];
            
              % Add a _norot at the end of the output filename
              apsopt.daughter='norot';
              farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_use,'mem',mem,'maxtime',maxtime,'submit',0);

              % revert back to normal setting
              apsopt.polrot=oldpolrot; apsopt=rmfield(apsopt,'daughter');
            end

            % also do keck2012+2013
            if nbase1==1351 && coaddrx==1 && ~isempty(nbase2) && nbase2==1351 
              apsopt.overall=1;
              if mat
                apsopt.purifmatname=purifmatname_comb;
              end
                
              farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_use,'mem',mem,'maxtime',maxtime);
              % remove the overall
              apsopt.overall=0;
            end
          
            if strcmp(jack,'0') && (~doauto | coaddrx==2)
              % Also make experiment jack for jack0
              apsopt.makebpwf=0; apsopt.update=1; maxtime=30;
              apsopt.howtojack='dim1'; % experiment jack
              farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_use,'mem',mem,'maxtime',maxtime);
            end
          end
        end
      end
    end
  end
end

% cross spectra for the sims
if make_aps
  apsopt.update=1;
  apsopt.makebpwf=0;
  apsopt.polrot=[];
  coaddrxs=[0,1];
  doauto=0;
  domats = [0,1];
  maxtime= 120;
  %setup the abscals
  if nbase1==0751 && isempty(nbase2) && daughter1=='a'
    apsopt.ukpervolt={3150};
    coaddrxs=0;
    doauto=1;
  elseif nbase1==1351 && isempty(nbase2) && strcmp(daughter1,'ab')
    apsopt.ukpervolt={1}; coaddrxs=1; doauto=1;
  elseif nbase1==1351 && isempty(nbase2) && daughter1=='c'
    apsopt.ukpervolt={get_ukpervolt('2014')};
    coaddrxs=2; doauto=1;
    domats = [0];  
  elseif nbase1==1351 && isempty(nbase2) && daughter1=='d'
    apsopt.ukpervolt={get_ukpervolt('2014')};
    coaddrxs=2; doauto=1;
    domats = [0 1];
  elseif nbase1==0751 && nbase2==1351 && daughter2=='a'
    apsopt.ukpervolt={3150,3400};
  elseif nbase1==0751 && nbase2==1351 && daughter2=='b'
    apsopt.ukpervolt={3150,2900};
  elseif nbase1==0751 && nbase2==1351 && (daughter2=='c' || daughter1=='d')
    apsopt.ukpervolt={3150,get_ukpervolt('2014')};
    coaddrxs=2;
  elseif nbase1==1351 && nbase2==1351 && daughter1=='a' && daughter2=='b'
    apsopt.ukpervolt={3400,2900};
  end
  for coaddrx=coaddrxs
    for type = types
      %make the type 9 have r=0.02
      if type=='9'
        switch coaddrx
         case 0
          apsopt.scalefac=scalefac;
         case 1
          apsopt.scalefac=scalefac_overrx;
         case 2
          apsopt.scalefac=scalefac_overfreq;
        end
      else
        % otherwise make sure scalec is not a field
        if isfield(apsopt,'scalefac');
          apsopt=rmfield(apsopt,'scalefac');
        end
        apsopt.update=1;
      end
      for pure_b = purebs;
        for jack=jacks
          for deproj=deprojs
            apsopt.coaddrx=coaddrx;
            apsopt.pure_b=pure_b{:};
            
            % Loop through with and without the matrix for jack0 only
            domat=0;
            if strcmp(jack,'0')
              domat=domats;
            end

            for mat=domat

              % Setup below for matrix purification
              if mat
                % add the matrix names
                switch coaddrx
                 case 0
                  apsopt.purifmatname=purifmatname;
                 case 1
                  apsopt.purifmatname=purifmatname_overrx;
                 case 2
                  apsopt.purifmatname=purifmatname_overfreq;
                end
                mem=50000;
              else
                % make sure purifmatname is not a field
                if isfield(apsopt,'purifmatname')
                  apsopt=rmfield(apsopt, 'purifmatname');
                end
                mem=15000;
              end

              fname1=[num2str(nbase1,'%04d'),'/???',type,'_' daughter1 '_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1s,'.mat'];
              if ~doauto
                replacementstr=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase2,'%04d') ''',''',ct1s,'.mat'',''',ct2s,'.mat'',''_',daughter1,'_'',''_',daughter2,'_'''];
                cmd = ['reduc_makeaps({''',fname1,''',',replacementstr,'},apsopt)'];
              else
                cmd = ['reduc_makeaps({''',fname1,'''},apsopt)'];
              end
          
              apsopt.howtojack='dim2';
  %              keyboard
              farmit('farmfiles/aps/',cmd, ...
                  'var','apsopt', ...
                  'queue',queue_small, ...
                  'mem',mem, ...
                  'maxtime',maxtime, ...
                  'submit', false);

              % for keck2012+2013, also do the addition
              if nbase1==1351 && ~isempty(nbase2) && nbase2==1351 && coaddrx && jack=='0';
                apsopt.overall=1;
                if mat
                  apsopt.purifmatname=purifmatname_comb;
                end
                farmit('farmfiles/aps/',cmd, ...
                    'var','apsopt', ...
                    'queue',queue_small, ...
                    'mem',mem, ...
                    'maxtime',maxtime, ...
                    'submit',false);
                apsopt.overall=0; % return overall to 0
              end

              % for auto spectra and type 4 run also the B x S x N cross spectrum
              if strcmp(jack,'0') && type=='4' && doauto && mat
                cmd = ['reduc_makeaps({''',fname1,''',{''4_',daughter1,''',''5_',daughter1,'''},{''4_',daughter1,''',''6_',daughter1,'''}},apsopt)'];
                farmit('farmfiles/aps/',cmd, ...
                    'var','apsopt', ...
                    'queue',queue_small, ...
                    'mem',mem, ...
                    'maxtime',maxtime, ...
                    'submit',false);
              end
              
              if strcmp(jack,'0') && (~doauto | coaddrx==2)
                apsopt.howtojack='dim1'; % experiment jack
                farmit('farmfiles/aps/',cmd, ...
                    'var','apsopt', ...
                    'queue',queue_small, ...
                    'mem',mem, ...
                    'maxtime',maxtime, ...
                    'submit',false);
              end

            end % domat
          end % deprojs
        end % jacks
      end % purebs
    end % types
  end % coaddrxs

end % make_aps

% simsets cross:
if make_simset
  disp('make_simset')
  doauto=0;
  jacks=[jacks,'f'];
  if nbase1==1351 && isempty(nbase2) && (daughter1=='c' || daughter1=='d')
    folder='1351'; doauto=1;
  elseif nbase1==0751 && isempty(nbase2)
    folder='0751';
    doauto=1;
    % no experiment jack
    jacks=strrep(jacks,'f','');
  elseif nbase1==1351 && isempty(nbase2)
    folder='1351'; doauto=1; jacks=strrep(jacks,'f','');
  elseif nbase1==0751 && nbase2==1351
    folder='0751x1351';
  elseif nbase1==1351 && nbase2==1351
    folder='1351';
  end
  for jack=jacks
    for type = types
      for deproj = deprojs
        estimators_loop=estimators;
        if strcmp(jack,'0')|strcmp(jack,'f'); estimators_loop=estimators_jack0; end;
        for estimator=estimators_loop
          if doauto
            fname=[folder '/[0-4]??',type,'_',daughter1,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1s,estimator{:},'.mat'];
          else
            fname=[folder '/[0-4]??',type,'_',daughter1,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1s,'_[0-4]??',type,'_',daughter2,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct2s,estimator{:},'.mat'];
          end
          cmd = ['reduc_makesimset(''' fname ''')'];
          farmit('farmfiles/simset/',cmd, ...
              'queue',queue_small, ...
              'mem',10000, ...
              'maxtime',120, ...
              'submit',false);
        end % estimators
      end % deprojs
    end % types
  end % jacks

  %cmd = 'reduc_makesimset(''0751/[01234]??4_a_filtp3_weight3_gs_dp1100_jack0_[01234]??5_a_filtp3_weight3_gs_dp1100_jack0_[01234]??6_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat'')';
  %farmit('farmfiles/simset/',cmd,'queue',queue_small,'mem',10000,'maxtime',60,'license',license);
end

% reduc_final and reduc_final_chi2 cross:
if make_final
  disp('make_final')
  doauto=0;
  directbpwfs=0;
  jacks=[jacks,'f'];
  %  pl_cross selects the rx combination to plot
  if nbase1==1351 && isempty(nbase2) && daughter1=='c'
    folder='1351';
    doauto=1;
    pl_cross = [1,2]; %that's ok
    directbpwfs=[0]; 
  elseif nbase1==1351 && isempty(nbase2) && daughter1=='d'
    folder='1351';
    doauto=1;
    pl_cross = [1,2]; %that's ok
    directbpwfs=[0,1];
    rfcopt.mapname={'K2014_100','K2014_150'};
  elseif nbase1==0751 && isempty(nbase2)
    folder='0751';
    doauto=1;
    pl_cross = [1,2]; %that's ok
    % for b2 auto also use the direct bpwfs
    directbpwfs=[0,1];
    % no experiment jack
    jacks=strrep(jacks,'f','');
  elseif nbase1==1351 && isempty(nbase2)
    folder='1351'; doauto=1; pl_cross=[1,2]; 
    jacks=strrep(jacks,'f','');
  elseif nbase1==0751 && nbase2==1351 && daughter2=='c'
    pl_cross = [1,2;1,3;2,3];
    folder='0751x1351';
    rfcopt.mapname={'Bicep2','Keck100','Keck150'};  
  elseif nbase1==0751 && nbase2==1351
    pl_cross = [1,2;1,3;1,4;1,5;1,6];
    folder='0751x1351';
  elseif nbase1==1351 && nbase2==1351
    pl_cross = [1,6;2,7;3,8;4,9;5,10]; %keck 2012 vs 2013
    folder='1351';
  end
  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  for jack=jacks
    for deproj = deprojsreal
      estimators_loop=estimators;
      if strcmp(jack,'0')|strcmp(jack,'f'); estimators_loop=estimators_jack0; end;
      for estimator=estimators_loop
          if nbase1==1351 && ~isempty(nbase2) && nbase2==1351 && ~isempty(strfind(estimator{:},'overall'))
            % also do direct bpwf for the final 1351 
            directbpwfs=[0,1];
          end
        for directbpwf=directbpwfs
          rbcs=[0]; % residual beam correction in few cases
          if doauto && strcmp(jack,'0') && strcmp(deproj{:},'1102') ...
          && daughter1~='c' && daughter1~='d'
            rbcs=[1,0];
          end
          for rbc=rbcs
            % if jack0 then also do the spectral jack:
            diffspecs=0;
            if (jack=='0') & ~doauto && (~isempty(strfind(estimator{:},'overall')) | ~isempty(strfind(estimator{:},'overfreq')) )
%                diffspecs=[0,1];
              diffspecs=[0];
            end
            for diffspec = diffspecs
              rots=[1];
              if strcmp(jack,'0') && diffspec==0 && ~isempty(strfind(estimator{:},'overall'))
                rots=[0,1];
              end
              for rot = rots              
                clear finalopt;
                finalopt.doblind = 0;
                finalopt.mapname=rfcopt.mapname;
                rfcopt.diffspec = diffspec;            
                
                deprojS=deproj{:};
                % when subtraction instead of regression in real data, 
                % use the undeprojected sim but just in the case that the deprojected
                % systematic was not simulated (diff ell yes but not diff pointing):
                if strcmp(deproj{:},'1202') continue; end
                if strcmp(deproj{:},'1102') deprojS='1100'; end
                if strcmp(deproj{:},'2102') deprojS='0100'; end
                
                if doauto
                  % no over rx for bicep2 auto
                  r = sprintf([folder '/real_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1  'BB']); %5 for the per-tile abscal
                  n = sprintf([folder '/xxx6_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s 'BB']); %signflip
                  s = sprintf([folder '/xxx2_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s 'BB']); %yes, always jack0, unlensed
                  sn= sprintf([folder '/xxx7_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s 'BB']); %lensed
                  b = sprintf([folder '/xxx4_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s 'BB']); %yes, always jack0
                  snr=sprintf([folder '/xxx9_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s 'BB']); %signal+noise+r
                else
                  r = sprintf([folder '/real_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1  '_real_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2  'BB']); %5 for the per-tile abscal
                  n = sprintf([folder '/xxx6_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx6_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2s 'BB']); %signflip
                  s = sprintf([folder '/xxx2_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s '_xxx2_' daughter2 '_filtp3_weight3_gs_dpDDDD_jack0' ct2s 'BB']); %yes, always jack0, unlensed
                  sn= sprintf([folder '/xxx7_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx7_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2s 'BB']); %lensed
                  b = sprintf([folder '/xxx4_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s '_xxx4_' daughter2 '_filtp3_weight3_gs_dpDDDD_jack0' ct2s 'BB']); %yes, always jack0
                  snr=sprintf([folder '/xxx9_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx9_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2s 'BB']); %lensed
                end
                
                r = strrep(r,'DDDD',deproj{:}); r = strrep(r,'BB',estimator{:}); r = strrep(r,'J',jack);
                n = strrep(n,'DDDD',deprojS); n = strrep(n,'BB',estimator{:}); n = strrep(n,'J',jack);
                s = strrep(s,'DDDD',deprojS); s = strrep(s,'BB',estimator{:}); s = strrep(s,'J',jack);
                sn= strrep(sn,'DDDD',deprojS); sn= strrep(sn,'BB',estimator{:}); sn= strrep(sn,'J',jack);
                b = strrep(b,'DDDD',deprojS); b = strrep(b,'BB',estimator{:}); b = strrep(b,'J',jack);
                snr= strrep(snr,'DDDD',deprojS); snr= strrep(snr,'BB',estimator{:}); snr= strrep(snr,'J',jack);

                if strcmp(deproj{:},'1102') && daughter1~='d'
                  r = strrep(r,'filt','fixesub_filt');  %change to the fixed maps
                end

                % now all stored in finalopt
                finalopt.nsimset=n; finalopt.ssimset=s; finalopt.snsimset=sn; finalopt.bosimset=b; finalopt.snrsimset=snr;
              
                % handle no rotation case
                if ~rot
                  r = [r,'_norot'];
                end
                
                % for b2 auto also use the direct bpwfs
                rc = r;
                if directbpwf
                  finalopt.supfacstyle='direct_bpwf';
                  if nbase1==0751
                    finalopt.bpwfname=['0704/xxxx_allcmb_filtp3_weight3_gs_dp',deprojS,'_jack0',estimator{:},'_directbpwf.mat'];
                  elseif nbase1==1351 && isempty(nbase2) && daughter1=='d'
                    finalopt.bpwfname=['1407/xxxx_d_100GHz_filtp3_weight3_gs_dp1100_jack0_xxxx_aabd_150GHz_filtp3_weight3_gs_dp1100_jack0_matrix_cm_directbpwf.mat'];
                  elseif nbase1==1351 && nbase2==1351
                    finalopt.bpwfname=['1332/xxxx_cmb_subset2012_2013_allrx_filtp3_weight3_gs_dp',deprojS,'_jack0_matrix_directbpwf.mat'];
                  end
                  % append _directbpwf to the final file name for reduc_final_chi2
                  rc = [r,'_directbpwf'];
                end
                if rbc
                  finalopt.residbeamcorr='aps/0751/beammap_leakage_spectra_1000rlz_usewithdp1102';
                  finalopt.daughter='_rbc';
                  rc = [rc,'_rbc'];
                end

                % define the farm jobs.  does the final plots for max ell=200,500,
                % saves the final file, and does chi2 plot for max ell=200,500
                cmd=[...
                'reduc_final(r,finalopt,0,500);close all;',...
                'reduc_final(r,finalopt,2,200);close all;',...
                'reduc_final(r,finalopt,2,500);close all;',...
                'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
                'reduc_final_chi2(rc,rfcopt,500,2);close all;'];

                % when not coadded over Keck revceiver also do the other plot combinations:
                % B2xRx2, B2xRx3 etc
                if isempty(strfind(estimator{:},'overrx'))
                  for jj = 1:size(pl_cross,1)
                    rfcopt.pl_cross=pl_cross(jj,:);
                    finalopt.pl_cross=rfcopt.pl_cross;
                    
                    % only make the final file once - rm from cmd otherwise
                    if jj~=1
                      cmd=[...
                      'reduc_final(r,finalopt,2,200);close all;',...
                      'reduc_final(r,finalopt,2,500);close all;',...
                      'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
                      'reduc_final_chi2(rc,rfcopt,500,2);close all;'];
%                        cmd = 'reduc_final_chi2(rc,rfcopt,200,2);close all;';
                    end
%                      keyboard
                    farmit('farmfiles/final/',cmd, ...
                        'var',{'r','rc','n','s','sn','b','rfcopt','finalopt'}, ...
                        'queue',queue_small, ...
                        'mem',16000, ...
                        'maxtime',60, ...
                        'submit',false);
                  end
                else
                  rfcopt.pl_cross=[1,2]; finalopt.pl_cross=rfcopt.pl_cross;
                  farmit('farmfiles/final/',cmd, ...
                      'var',{'r','rc','n','s','sn','b','rfcopt','finalopt'}, ...
                      'queue',queue_small, ...
                      'mem',16000, ...
                      'maxtime',60, ...
                      'submit',false);
                end
                
              end  %loops
            end
          end
        end
      end
    end
  end
end

