function make_aps_final(nbase1,nbase2,daughter1,daughter2,make_aps_real,make_aps,make_simset,make_final,coaddtype)
% function make_aps_final(nbase1,nbase2,daughter1,daughter2,make_aps_real,make_aps,make_simset,make_final,coaddtype)
%
% A script that makes the aps and final plots for Keck/BICEP2
% broken off from runsim_1450.m
%
% Everything is made as cross spectra, so nbase1_daughter1xnbase2_daughter2
% 
% Options:
% nbase1, nbase2 (defaults 1450, 1350) 
%          The sernums for the relevant files
% daughter1, daughter1 (defaults 'a','a') 
%          For the respective sernums, 'b'=2013 Keck
% make_aps_real (default 0)
%          Makes the real aps.  farms bpwf for jack0
% make_aps (default 0)
%          Makes sims cross aps.
% make_simset (default 0)
%          turns sims into simset
% make_final (default 0)
%          makes the reduc_final and reduc_final_chi2 pager plots
% coaddtype (default 1)
%          The coaddtype.  if 1, only Keck is modified.
%          If 5, only Keck2012 is modified.
%

if ~exist('nbase1','var')
  nbase1=1450;
end
if ~exist('nbase2','var')
  nbase2=1350;
end
if ~exist('daughter1','var')
  daughter1='a';
end
if ~exist('daughter2','var')
  daughter2='a';
end
if ~exist('make_aps_real','var')
  make_aps_real=0;
end
if ~exist('make_aps','var')
  make_aps=0;
end
if ~exist('make_final','var')
  make_final=0;
end
if ~exist('make_simset','var')
  make_simset=0;
end
if ~exist('coaddtype','var')
  coaddtype=1;
end

% these are used all over the following loops
% just drag them into the workspace:
types= '245679';
deprojs={'0000','1000','0100','1100','1110','1101','1111'};
deprojsreal={'2102','1102','1202','0000','1000','0100','1100','1110','1101','1111'};
%deprojs={'1100'};
%deprojsreal={'1102','1100'};
if daughter2=='b' || daughter1=='b'
  deprojs={'1100'};
  deprojsreal={'1100','1102'};
end
jacks = get_default_coaddopt();
jacks = jacks.jacktype;
estimators = {'','_pureB'};
estimators = {'','_pureB','_overrx','_pureB_overrx'};
purebs = {'normal','kendrick'};
queue_small='short_serial';
queue_other='short_serial';

%initialize apsopt
apsopt.update = 1;
apsopt.save_coaddopts=0;
maxtime=[];

%initialize coaddtype
if nbase1==1450 && nbase2==1350 
  ct1=''; ct2='1'; 
  ct1s=''; ct2s='1';
  if coaddtype==5 && daughter2=='a'
    ct2='5';
  elseif coaddtype==2
    ct1='2'; ct2='2';
  end
elseif nbase1==1350 && nbase2==1350
  ct1='1'; ct2='1';
  ct1s='1'; ct2s='1';
  if coaddtype==5 && daughter1=='a'
    ct1='5';
  elseif coaddtype==2
    ct1='2'; ct2='2';
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cross spectra for real, includes bpwf and experiment jack
% the jack0 run for many hours since it will calculate the bpwf as well.

%setup the abscal per pair stuff
currentdir=pwd;
cd ../bicep2_analysis/
[p1 ind1]=get_array_info('20120101',[],[],[],[],[],[],'obs');
ukpv_b2=nanmean([p1.ukpv(ind1.a) p1.ukpv(ind1.b)],2);
cd ../keck_analysis/
[p1 ind1]=get_array_info('20120101',[],[],[],[],[],[],'obs');
ukpv_k12=nanmean([p1.ukpv(ind1.a) p1.ukpv(ind1.b)],2);
[p1 ind1]=get_array_info('20130101',[],[],[],[],[],[],'obs');                              
ukpv_k13=nanmean([p1.ukpv(ind1.a) p1.ukpv(ind1.b)],2);
cd(currentdir);
pwd

if make_aps_real

  %finish initializing apsopt
  apsopt.makebpwf=0;
  if nbase1==1450 && nbase2==1350 && daughter2=='a'
    apsopt.polrot=[-1.1,-0.5];
    apsopt.ukpervolt={3150,3400};
    if coaddtype==5
      %setup the per-tile abscal for Keck.  
      [pp kk]=ParameterRead('aux_data/abscal/abscal_tile_20120101.csv');  % aux_data pointed to keck's here
      apsopt.ukpervolt={3150,pp.ukpv};
    elseif coaddtype==2
      %setup the bicep2 abscal per pair
      apsopt.ukpervolt={ukpv_b2,ukpv_k12};
    end
  elseif nbase1==1450 && nbase2==1350 && daughter2=='b'
    apsopt.polrot=[-1.1,0];
    apsopt.ukpervolt={3150,2900};
    if coaddtype==2
      apsopt.ukpervolt={ukpv_b2,ukpv_k13};
    end
  elseif nbase1==1350 && nbase2==1350 && daughter2=='b' && daughter1=='a'
    apsopt.polrot=[-0.5,0];
    apsopt.ukpervolt={3400,2900};
    if coaddtype==5
      [pp kk]=ParameterRead('aux_data/abscal/abscal_tile_20120101.csv');  % aux_data pointed to keck's here
      apsopt.ukpervolt={pp.ukpv,2900};
    elseif coaddtype==2
      apsopt.ukpervolt={ukpv_k12,ukpv_k13};
    end
  end
  for deproj = deprojsreal
    for coaddrx=[0,1]
      for pure_b = purebs;
        for jack=jacks
          if strcmp(jack,'0')
            apsopt.makebpwf=1;
            apsopt.update=1;
            maxtime=[];
            queue_other='normal_serial'; %can you commit here!?
          else
            apsopt.makebpwf=0;
            apsopt.update=1;
            maxtime=[];
            queue_other='short_serial';
          end
          
          apsopt.pure_b = pure_b{:};
          apsopt.coaddrx=coaddrx;
          apsopt.howtojack='dim2';

          fname1=[num2str(nbase1),'/real_' daughter1 '_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1,'.mat'];
          replacementstr=['''',num2str(nbase1),''',''' num2str(nbase2) ''',''',ct1,'.mat'',''',ct2,'.mat'',''_',daughter1,'_'',''_',daughter2,'_'''];
          cmd = ['reduc_makeaps({''',fname1,''',',replacementstr,'},apsopt)'];
          farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_other,'mem',15000,'maxtime',maxtime);
        
          if strcmp(jack,'0')
            apsopt.makebpwf=0; apsopt.update=0;
            apsopt.howtojack='dim1'; % experiment jack
            farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_other,'mem',15000,'maxtime',maxtime);
          end
        end
      end
    end
  end
end

% cross spectra for the sims
if make_aps
  apsopt.update=0;
  apsopt.makebpwf=0;
  apsopt.polrot=[];
  if nbase1==1450 && nbase2==1350 && daughter2=='a'
    apsopt.ukpervolt={3150,3400};
  elseif nbase1==1450 && nbase2==1350 && daughter2=='b'
    apsopt.ukpervolt={3150,2900};
  elseif nbase1==1350 && nbase2==1350 && daughter1=='a' && daughter2=='b'
    apsopt.ukpervolt={3400,2900};
  end
  for type = types
    for coaddrx=[0,1]
      for pure_b = purebs;
        for jack=jacks
          apsopt.coaddrx=coaddrx;
          apsopt.pure_b = pure_b{:};     

          fname1=[num2str(nbase1),'/0??',type,'_' daughter1 '_filtp3_weight3_gs_dp????_jack',jack,ct1s,'.mat'];
          replacementstr=['''',num2str(nbase1),''',''' num2str(nbase2) ''',''',ct1s,'.mat'',''',ct2s,'.mat'',''_',daughter1,'_'',''_',daughter2,'_'''];
          cmd = ['reduc_makeaps({''',fname1,''',',replacementstr,'},apsopt)'];
        
          apsopt.howtojack='dim2';
          while 1
            try
              farmit('farmfiles/aps/',cmd,'var','apsopt','queue',queue_other,'mem',10000,'maxtime',maxtime);
              break
            catch
              display('oh no')
            end
          end
        
          if strcmp(jack,'0')
            apsopt.howtojack='dim1'; % experiment jack
            while 1
              try
                farmit('farmfiles/aps/',cmd,'var','apsopt','queue',queue_other,'mem',10000,'maxtime',maxtime);
                break
              catch
                display('oh no')
              end
            end
          end
        end
      end    
    end
  end
end

% simsets cross:
if make_simset
  if nbase1==1450 && nbase2==1350
    folder='1450x1350';
  elseif nbase1==1350 && nbase2==1350
    folder='1350';
  end
  jacks=[jacks,'f'];
  for jack=jacks
    for type = types
      for deproj = deprojs
        for estimator=estimators       
          fname=[folder '/[012]??',type,'_',daughter1,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1s,'_[012]??',type,'_',daughter2,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct2s,estimator{:},'.mat'];
          cmd = ['reduc_makesimset(''' fname ''')'];
          farmit('farmfiles/simset/',cmd,'queue',queue_other,'mem',10000,'maxtime',60);
        end
      end
    end
  end
end

% reduc_final and reduc_final_chi2 cross:
if make_final
  %  pl_cross selects the rx combination to plot
  if nbase1==1450 && nbase2==1350
    pl_cross = [1,2;1,3;1,4;1,5;1,6];
    folder='1450x1350';
  elseif nbase1==1350 && nbase2==1350
    pl_cross = [1,6;2,7;3,8;4,9;5,10]; %keck 2012 vs 2013
    folder='1350';
  end
  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  jacks=[jacks,'f'];
  for jack=jacks
    for deproj = deprojsreal
      for estimator=estimators
        deprojS=deproj{:};
        % when subtraction instead of regression in real data, 
        % use the undeprojected sim but just in the case that the deprojected
        % systematic was not simulated (diff ell yes but not diff pointing):
        if strcmp(deproj{:},'1202') continue; end
        if strcmp(deproj{:},'1102') deprojS='1100'; end
        if strcmp(deproj{:},'2102') deprojS='0100'; end
       
        r = sprintf([folder '/real_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1 '_real_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2 'BB']); %5 for the per-tile abscal
        n = sprintf([folder '/xxx6_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_0016_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2s 'BB']); %signflip
        s = sprintf([folder '/xxx2_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s '_0012_' daughter2 '_filtp3_weight3_gs_dpDDDD_jack0' ct2s 'BB']); %yes, always jack0, unlensed
        sn= sprintf([folder '/xxx7_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_0017_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2s 'BB']); %lensed
        b = sprintf([folder '/xxx4_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s '_0014_' daughter2 '_filtp3_weight3_gs_dpDDDD_jack0' ct2s 'BB']); %yes, always jack0

        r = strrep(r,'DDDD',deproj{:}); r = strrep(r,'BB',estimator{:}); r = strrep(r,'J',jack);
        n = strrep(n,'DDDD',deprojS); n = strrep(n,'BB',estimator{:}); n = strrep(n,'J',jack);
        s = strrep(s,'DDDD',deprojS); s = strrep(s,'BB',estimator{:}); s = strrep(s,'J',jack);
        sn= strrep(sn,'DDDD',deprojS); sn= strrep(sn,'BB',estimator{:}); sn= strrep(sn,'J',jack);
        b = strrep(b,'DDDD',deprojS); b = strrep(b,'BB',estimator{:}); b = strrep(b,'J',jack);
      
        cmd=['reduc_final(r,n,s,sn,b,finalopt,2,200);close all;',...
         'reduc_final(r,n,s,sn,b,finalopt,2,500);close all;',...
         'reduc_final(r,n,s,sn,b,finalopt,0,500);close all;',...
         'reduc_final_chi2(r,rfcopt,200,2);close all;',...
         'reduc_final_chi2(r,rfcopt,500,2);close all;'];

        % if jack0 then also do the spectral jack:
        diffspecs=0;
        if (jack=='0') diffspecs=[0,1]; end
        
        for diffspec = diffspecs
          rfcopt.diffspec = diffspec;
        
          % when not coadded over Keck revceiver also do the other plot combinations:
          % B2xRx2, B2xRx3 etc
          if isempty(strfind(estimator{:},'overrx'))
            for jj = 1:size(pl_cross,1)
              rfcopt.pl_cross=pl_cross(jj,:);
              finalopt.pl_cross=rfcopt.pl_cross;
              farmit('farmfiles/final/',cmd,'var',{'r','n','s','sn','b','rfcopt','finalopt'},'queue',queue_other,'mem',16000,'maxtime',60);
            end
          else
            rfcopt.pl_cross=[1,2]; finalopt.pl_cross=rfcopt.pl_cross;
            farmit('farmfiles/final/',cmd,'var',{'r','n','s','sn','b','rfcopt','finalopt'},'queue',queue_other,'mem',16000,'maxtime',60);
          end
        
        end
      end
    end
  end
end

