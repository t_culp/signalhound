function combine_bsn(filename,w,supfacin,supfacout)
% function combine_bsn(filename,w)
% 
% Written to spectrally combine the bxsxn aps to be used 
%   for the spectrally combined final for r scaling
%
% Input: filename - file of the simset
%        w - weights used in the spectrally combined case
%
% Example:
%   filename='aps/0751x1351/xxx4_a_filtp3_weight3_gs_dp1100_jack0_xxx5_a_filtp3_weight3_gs_dp1100_jack0_xxx6_a_filtp3_weight3_gs_dp1100_jack0_xxx4_ab_filtp3_weight3_gs_dp1100_jack01_xxx5_ab_filtp3_weight3_gs_dp1100_jack01_xxx6_ab_filtp3_weight3_gs_dp1100_jack01_matrix_overrx.mat'
%   load('final/0751x1351/real_a_fixesub_filtp3_weight3_gs_dp1102_jack0_real_a_fixesub_filtp3_weight3_gs_dp1102_jack01_real_b_fixesub_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall_directbpwf_debias_comb.mat')
%   supfacout=supfac; w=r.ws;
%   load('final/0751x1351/real_a_fixesub_filtp3_weight3_gs_dp1102_jack0_real_a_fixesub_filtp3_weight3_gs_dp1102_jack01_real_b_fixesub_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall_directbpwf.mat')
%   supfacin=supfac;
%   combine_bsn(filename,w,supfacin,supfacout);
%

if ~exist('filename','var')
  error('Must input filename')
end
if ~exist('w','var')
  error('Must input weight')
end

% load the aps
load(filename);

% check the size
N=size(aps,1);
na = -0.5+sqrt(2*N+0.25);
nc = na/2;
% since reduc_final_comb.m only knows how to deal with 2 maps, assume it must be an even vector
if mod(na,2)
  error('code cannot handle odd cases')
end


% loop through the spectra.  TE, TB, EB have 4 cases
for jj=1:size(w,2)

  % loop through the easy cases
  for ii=1:nc

    pl=[ii,ii+nc];
    % follow the loop to get the cross 
    nc_i=get_cross_ind(na,pl);
    pl=[pl nc_i];

    % apply the suppression factor
    aps_temp=apply_supfac(aps(pl),supfacin);
    % add the spectra together
    aps_comb(ii,1).Cs_l(:,jj,:)=comb_r({aps_temp.Cs_l},jj,w{jj});
    % remove the suppression factor
    aps_comb(ii,1).Cs_l(:,jj,:)=aps_comb(ii).Cs_l(:,jj,:)./repmat(supfacout.rwf(:,jj),[1,1,size(aps(ii).Cs_l,3)]);

  end

  % now for the less easy cases
  % bxs = b1xs1*w1+b2xs2*w2+(b1xs2+b2xs1)*w3/2 and so forth
  % define 4 crosses for each case:
  % each is base, base+nc,[base(1),base(2)+nc],and [base(2),base(1)+nc] 
  % define this generally for any size
  nc_c=nc+1;
  for j=1:nc-1
    for c=j+1:nc
      pl(1)=get_cross_ind(na,[j,c]);
      pl(2)=get_cross_ind(na,[j,c]+nc);
      pl(3)=get_cross_ind(na,[j,c+nc]);
      pl(4)=get_cross_ind(na,[c,j+nc]);

      % add the spectra together
      aps_temp=apply_supfac_cross(aps(pl),supfacin,jj);
      aps_comb(nc_c,1).Cs_l(:,jj,:)=comb_r_cross({aps_temp.Cs_l},jj,w{jj});
      aps_comb(nc_c,1).Cs_l(:,jj,:)=aps_comb(nc_c).Cs_l(:,jj,:)./repmat(supfacout.rwf(:,jj),[1,1,size(aps(ii).Cs_l,3)]);


      %increment to next set
      nc_c=nc_c+1;
    end
  end
end

% substitute back
aps=aps_comb;

% and save
filename=strrep(filename,'.mat','');
% filenames are tooo long... remove filtp3_weight3_gs?
filename=strrep(filename,'filtp3_weight3_gs_','');
filename=[filename '_comb']
saveandtest(filename,'aps','apsopt','coaddopt');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cross= get_cross_ind(na,pl)

nc_c=na+1;
for j=1:na-1
  for c=j+1:na
    if j==pl(1) & c==pl(2)
      cross=nc_c;
    end
    nc_c=nc_c+1;
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xc=comb_r(x,i,ws)

% for each realization
for j=1:size(x{1},3)
  switch i
    case {1,3,4} % TT,EE,BB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j)].*ws,2);
    case 2 % TE
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,7,j)].*ws,2);
    case 5 % TB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,8,j)].*ws,2);
    case 6 % EB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,9,j)].*ws,2);

  end
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xc=comb_r_cross(x,i,ws)

% expand ws.  still sums to 1
if size(ws,2)==3
  ws=[ws,ws(:,3)];
  ws(:,3:4)=ws(:,3:4)./2;
else
  ws=[ws,ws(:,3:4)];
  ws(:,3:6)=ws(:,3:6)./2;
end

% for each realization
for j=1:size(x{1},3)
  switch i
    case {1,3,4} % TT,EE,BB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{4}(:,i,j)].*ws,2);
    case 2 % TE
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,7,j),x{4}(:,i,j),x{4}(:,7,j)].*ws,2);
    case 5 % TB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,8,j),x{4}(:,i,j),x{4}(:,8,j)].*ws,2);
    case 6 % EB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,9,j),x{4}(:,i,j),x{4}(:,9,j)].*ws,2);

  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%
function aps=apply_supfac_cross(aps,supfac,i)

% expand supfac
supfac(4)=supfac(3);

n=size(aps(1).Cs_l,3);

for ii=1:length(aps)
  switch i
    case {1,3,4} %TT,EE,BB
      aps(ii).Cs_l(:,i,:)=aps(ii).Cs_l(:,i,:).*repmat(supfac(ii).rwf(:,i,:),[1,1,n]);
    case 2 %TE
      aps(ii).Cs_l(:,i,:)=aps(ii).Cs_l(:,i,:).*repmat(supfac(ii).rwf(:,i,:),[1,1,n]);
      if size(supfac(ii).rwf,2)>6
        aps(ii).Cs_l(:,7,:)=aps(ii).Cs_l(:,7,:).*repmat(supfac(ii).rwf(:,7,:),[1,1,n]);
      else
        aps(ii).Cs_l(:,7,:)=aps(ii).Cs_l(:,7,:).*repmat(supfac(ii).rwf(:,i,:),[1,1,n]);
      end
    case 5 % TB
      aps(ii).Cs_l(:,i,:)=aps(ii).Cs_l(:,i,:).*repmat(supfac(ii).rwf(:,i,:),[1,1,n]);
      if size(supfac(ii).rwf,2)>6
        aps(ii).Cs_l(:,8,:)=aps(ii).Cs_l(:,8,:).*repmat(supfac(ii).rwf(:,8,:),[1,1,n]);
      else
        aps(ii).Cs_l(:,8,:)=aps(ii).Cs_l(:,8,:).*repmat(supfac(ii).rwf(:,i,:),[1,1,n]);
      end
    case 6 % EB
      aps(ii).Cs_l(:,i,:)=aps(ii).Cs_l(:,i,:).*repmat(supfac(ii).rwf(:,i,:),[1,1,n]);
      if size(supfac(ii).rwf,2)>6
        aps(ii).Cs_l(:,9,:)=aps(ii).Cs_l(:,9,:).*repmat(supfac(ii).rwf(:,9,:),[1,1,n]);
      else
        aps(ii).Cs_l(:,9,:)=aps(ii).Cs_l(:,9,:).*repmat(supfac(ii).rwf(:,i,:),[1,1,n]);
      end
  end
end




