function make_aps_final(nbase1,nbase2,nbase3,daughter1,daughter2,daughter3,make_aps_real,make_aps,make_simset,make_final,coaddtype,queue)
% function make_aps_final(nbase1,nbase2,daughter1,daughter2,make_aps_real,make_aps,make_simset,make_final,coaddtype,queue)
%
% A script that makes the aps and final plots for Keck/BICEP2
% modified from Sarah's make_aps_final_1450.m (JPK)
%
% cross spectra are made via so nbase1_daughter1xnbase2_daughter2
% auto spectra are made when nbase2=[] - set up for B2 only up to now
% 
% Options:
% nbase1, nbase2 (defaults 0751, 1351) 
%          The sernums for the relevant files
% daughter1, daughter1 (defaults 'a','a') 
%          For the respective sernums, 'b'=2013 Keck
% make_aps_real (default 0)
%          Makes the real aps.  farms bpwf for jack0
% make_aps (default 0)
%          Makes sims cross aps.
% make_simset (default 0)
%          turns sims into simset
% make_final (default 0)
%          makes the reduc_final and reduc_final_chi2 pager plots
% coaddtype (default 1)
%          The coaddtype.  if 1, only Keck is modified.
%          If 5, only Keck2012 is modified.
% queue (default 1)
%          The queue to use.  0 for LSF.  1 for serial_requeue
%

if ~exist('nbase1','var')
  nbase1=0751;
end
if ~exist('nbase2','var')
  nbase2=1351;
end
if ~exist('nbase3','var')
  nbase3=1351;
end
if ~exist('daughter1','var')
  daughter1='a';
end
if ~exist('daughter2','var')
  daughter2='a';
end
if ~exist('daughter3','var')
  daughter3='b';
end
if ~exist('make_aps_real','var')
  make_aps_real=0;
end
if ~exist('make_aps','var')
  make_aps=0;
end
if ~exist('make_final','var')
  make_final=0;
end
if ~exist('make_simset','var')
  make_simset=0;
end
if ~exist('coaddtype','var')
  coaddtype=1;
end
if ~exist('queue','var')
  queue=1;
end

% these are used all over the following loops
% just drag them into the workspace:
types= '245679';
deprojs={'1100'};
if nbase2==1353
  deprojsreal={'11022','11020','11000'};
else
  deprojsreal={'1102','1100'};
end
jacks = get_default_coaddopt();
jacks = jacks.jacktype;
%  jacks='0';
% auto spectrum
%  estimators = {'_overrx_overall','_pureB_overrx_overall','_matrix_overrx_overall'};
%  estimators_j={'_overrx_overall','_pureB_overrx_overall'};
%  purebs = {'normal','kendrick'};
estimators   = {'_matrix_overrx_overall','_pureB_overrx'};
estimators_f = {'_matrix_overrx_overall','_pureB_overrx'};
estimators_7 = {'_pureB_overrx_overall' ,'_pureB_overrx'};
estimators_j = {'_pureB_overrx_overall'};
purebs = {'kendrick'};

%Choose the correct queue
queue = 'general,serial_requeue,itc_cluster';
queue_small=queue;
queue_large=queue;

%initialize apsopt
apsopt.update = 1;
apsopt.save_coaddopts=0;
maxtime=120;

%initialize coaddtype
  ct1=''; ct2='1'; ct3='1';
  ct1s=''; ct2s='1'; ct3s='1';

%initialize purification matrix names for the different cases
% also initialize scale factors...
purifmatname_overrx={'/n/panlfs2/bicep/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat',...
  '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat'};
scalefac={1*ones(1,3),1*ones(1,3),sqrt(2)*ones(1,3)};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cross spectra for real, includes bpwf and experiment jack
% the jack0 run for many hours since it will calculate the bpwf as well.

%setup the abscal per pair stuff
if make_aps_real

  %finish initializing apsopt
  apsopt.makebpwf=0;
  coaddrxs=[1];
  doauto=0;
  apsopt.ukpervolt={3150,3400,2900};
  for deproj = deprojsreal
    deproj1=deproj{1}(1:4);
    for coaddrx=coaddrxs
      for pure_b = purebs;
        for jack=jacks
          % For jack f and 7 we want Keck12 and 13 separately
          if jack=='0' | jack=='7'
            overalls=[0,1];
          else
            overalls=[1];
          end
          for oa = overalls
            if isfield(apsopt,'overall') apsopt=rmfield(apsopt,'overall'); end
            if oa 
              apsopt.overall={1,[2,3]};
              apsopt.polrot=[-1.1,-0.5];
            else
              apsopt.polrot=[-1.1,-0.5,-0.7];
            end
        
            if strcmp(jack,'0')
              apsopt.makebpwf=1;
              apsopt.update=0;
              maxtime=60*12;
              queue_use=queue_large; 
            else
              apsopt.makebpwf=0;
              apsopt.update=1;
              maxtime=60;
              queue_use=queue_small;
            end
            
            % Loop through with and without the matrix for jack0 only
            % for jackf only use matrix if the keck years are coadded
            domat=0;
            if strcmp(jack,'0') & oa
              domat=[0 1];
            end

            for mat=domat

              % Setup below for matrix purification
              if mat
                % add the matrix names
                if ~coaddrx
                  apsopt.purifmatname=purifmatname;
                else
                  apsopt.purifmatname=purifmatname_overrx;
                end
                mem=60000; queue_use=queue_large;
              else
                % make sure purifmatname is not a field
                if isfield(apsopt,'purifmatname')
                  apsopt=rmfield(apsopt, 'purifmatname');
                end
                mem=15000;
              end

              apsopt.pure_b = pure_b{:};
              apsopt.coaddrx=coaddrx;
              apsopt.howtojack='dim2';

              % compile the base filename, the replacement string and the cmd 
              fname1=[num2str(nbase1,'%04d'),'/real_' daughter1 '_filtp3_weight3_gs_dp',deproj1,'_jack',jack,ct1,'.mat'];
              replacementstr=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase2,'%04d') ''',''',ct1,'.mat'',''',ct2,'.mat'',''_',daughter1,...
                '_'',''_',daughter2,'_'',''',deproj1,''',''',deproj{:},''''];
              replacementstr2=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase3,'%04d') ''',''',ct1,'.mat'',''',ct3,'.mat'',''_',daughter1,...
                '_'',''_',daughter3,'_'',''',deproj1,''',''',deproj{:},''''];
              cmd = ['reduc_makeaps({''',fname1,''',{',replacementstr,'},{',replacementstr2,'}},apsopt)'];
              farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_use,'mem',mem,'maxtime',maxtime,'submit',0);  
              %eval(cmd)

              if strcmp(jack,'0') && ~doauto
                % Also make experiment jack for jack0
                apsopt.makebpwf=0; apsopt.update=1;
                apsopt.howtojack='dim1'; % experiment jack
                farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_use,'mem',mem,'maxtime',maxtime,'submit',0);
                %eval(cmd)
                apsopt.makebpwf=1; %return to old settings
              end

            end
          end
        end
      end
    end
  end
end

% cross spectra for the sims
if make_aps
  apsopt.update=1;
  apsopt.makebpwf=0;
  apsopt.polrot=[];
  coaddrxs=[1];
  doauto=0;
  apsopt.ukpervolt={3150,3400,2900};
  for coaddrx=coaddrxs
    for type = types
      %make the type 9 have r=0.02
      if type=='9'
        apsopt.scalefac=scalefac;
      else
        % otherwise make sure scalec is not a field
        if isfield(apsopt,'scalefac');
          apsopt=rmfield(apsopt,'scalefac');
        end
        apsopt.update=1;
      end
      for pure_b = purebs;
        for jack=jacks
          % For jack f and 7 we want Keck12 and 13 separately
          if jack=='0' | jack=='7'
            overalls=[0,1];
          else
            overalls=[1];
          end
          for oa = overalls
            if isfield(apsopt,'overall') apsopt=rmfield(apsopt,'overall'); end
            if oa apsopt.overall={1,[2,3]}; end
           
            apsopt.coaddrx=coaddrx;
            apsopt.pure_b=pure_b{:};
            
            % Loop through with and without the matrix for jack0 only
            % for jackf only use matrix if the keck years are coadded
            domat=0;
            if strcmp(jack,'0') & oa
              domat=[0 1];
            end

            for mat=domat

              % Setup below for matrix purification
              if mat
                % add the matrix names
                if ~coaddrx
                  apsopt.purifmatname=purifmatname;
                else
                  apsopt.purifmatname=purifmatname_overrx;
                end
                mem=30000;
              else
                % make sure purifmatname is not a field
                if isfield(apsopt,'purifmatname')
                  apsopt=rmfield(apsopt, 'purifmatname');
                end
                mem=15000;
              end

              fname1=[num2str(nbase1,'%04d'),'/[01234]??',type,'_' daughter1 '_filtp3_weight3_gs_dp1100_jack',jack,ct1s,'.mat'];
              replacementstr=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase2,'%04d') ''',''',ct1s,'.mat'',''',ct2s,'.mat'',''_',daughter1,'_'',''_',daughter2,'_'''];
              replacementstr2=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase3,'%04d') ''',''',ct1s,'.mat'',''',ct3s,'.mat'',''_',daughter1,'_'',''_',daughter3,'_'''];
              cmd = ['reduc_makeaps({''',fname1,''',{',replacementstr,'},{',replacementstr2,'}},apsopt)'];
          
              apsopt.howtojack='dim2';
              farmit('farmfiles/aps/',cmd,'var','apsopt','queue',queue_small,'mem',mem,'maxtime',maxtime,'submit',0);

              % for auto spectra and type 4 run also the B x S x N cross spectrum
              if strcmp(jack,'0') && type=='4' && mat && 0
                % too long of filename if concatenating over 9 spectra... use the daughter ab already saved to disk
                apsopt=rmfield(apsopt,'overall');
                apsopt.ukpervolt={3150,3150,3150,1,1,1};
                apsopt.purifmatname={purifmatname_overrx{1},purifmatname_overrx{1},purifmatname_overrx{1},...
                                    purifmatname_overrx{2},purifmatname_overrx{2},purifmatname_overrx{2}};
                r1str0=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase2,'%04d') ''',''',ct1s,'.mat'',''',ct2s,'.mat'',''4_',daughter1,''',''4_ab'''];
                r1str1=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase2,'%04d') ''',''',ct1s,'.mat'',''',ct2s,'.mat'',''4_',daughter1,''',''5_ab'''];
                r1str2=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase2,'%04d') ''',''',ct1s,'.mat'',''',ct2s,'.mat'',''4_',daughter1,''',''6_ab'''];
                cmd=['reduc_makeaps({''',fname1,''',{',rstr1,'},{',rstr2,'},{',r1str0,'},{',r1str1,'},{',r1str2,'}},apsopt)']; 

                farmit('farmfiles/aps/',cmd,'var','apsopt','queue',queue_small,'mem',mem,'maxtime',maxtime,'submit',0);

                % change overall back
                apsopt.overall={1,[2,3]};   apsopt.ukpervolt={3150,3400,2900}; apsopt.purifmatname=purifmatname_overrx;
              end        
              if strcmp(jack,'0') && ~doauto
                apsopt.howtojack='dim1'; % experiment jack
                farmit('farmfiles/aps/',cmd,'var','apsopt','queue',queue_small,'mem',mem,'maxtime',maxtime,'submit',0);
              end
            end
          end %overalls
        end
      end   
    end
  end
end

% simsets cross:
if make_simset
  jacks=[jacks,'f'];
  folder='0751x1351';
  %folder='1450x1350';
  for jack=jacks
    for type = types
      for deproj = deprojs
        switch jack
          case '0'
            estimators_loop=estimators;
          case 'f'
           estimators_loop=estimators_f;
          case '7'
            estimators_loop=estimators_7;
          otherwise        
            estimators_loop=estimators_j;
        end
        for estimator=estimators_loop
          fname=[folder '/[01234]??',type,'_',daughter1,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1s,...
          '_[01234]??',type,'_',daughter2,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct2s,...
          '_[01234]??',type,'_',daughter3,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct3s,estimator{:},'.mat'];
          cmd = ['reduc_makesimset(''' fname ''')'];
          farmit('farmfiles/simset/',cmd,'queue',queue_small,'mem',10000,'maxtime',120,'submit',0);
        end
      end
    end
  end
  if (0)
    cmd = 'reduc_makesimset(''0751x1351/[01234]??4_a_filtp3_weight3_gs_dp1100_jack0_[01234]??5_a_filtp3_weight3_gs_dp1100_jack0_[01234]??6_a_filtp3_weight3_gs_dp1100_jack0_[01234]??4_ab_filtp3_weight3_gs_dp1100_jack01_[01234]??5_ab_filtp3_weight3_gs_dp1100_jack01_[01234]??6_ab_filtp3_weight3_gs_dp1100_jack01_matrix_overrx.mat'')';
    farmit('farmfiles/simset/',cmd,'queue',queue_small,'mem',10000,'maxtime',120);
  end
end

% reduc_final and reduc_final_chi2 cross:
if make_final
  doauto=0;
  directbpwfs=[0,1];
  jacks=[jacks,'f'];
  %  pl_cross selects the rx combination to plot
  pl_cross = [1,2]; %that's ok
  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  folder = sprintf('%04dx%04d',nbase1,nbase2);
  folders = folder;
  %folders= strrep(folder,'1353','1351');
  %folder='1450x1350';
  keyboard
  for jack=jacks
    for deproj = deprojsreal
      deproj1=deproj{1}(1:4);
      for directbpwf=directbpwfs       
        switch jack
          case '0'
            estimators_loop=estimators;
          case 'f'
           estimators_loop=estimators_f;
          case '7'
            estimators_loop=estimators_7;
          otherwise        
            estimators_loop=estimators_j;
        end      

        for estimator=estimators_loop
          rbcs=[0]; % residual beam correction in few cases
          if doauto & strcmp(jack,'0') && strcmp(deproj{:},'1102')
            rbcs=[1,0];
          end
          for rbc=rbcs
            % if jack0 then also do the spectral jack:
            diffspecs=0;
            if (jack=='0') & ~doauto && ~isempty(strfind(estimator{:},'overall'))
              diffspecs=[0,1];
            end
            for diffspec = diffspecs
              rots=[1];
              if strcmp(jack,'0') && diffspec==0 && isempty(strfind(estimator{:},'overall'))
                rots=[0,1];
              end
              for rot = rots              
                clear finalopt;
                finalopt.mapname={'B2','K'};
                if isempty(strfind(estimator{:},'overall'))
                  finalopt.mapname={'B2','K12','K13'};
                end
                finalopt.pl_cross=[1,2];
                if isempty(strfind(estimator{:},'overall')) & jack=='7'
                  % for alt deck plot b2,K12,k13
                  finalopt.pl_cross=[1,2,3];
                end
                if isempty(strfind(estimator{:},'overall')) & jack=='f'
                  % for alt deck plot b2,K12,k13
                  finalopt.pl_cross=[4,5,6];
                end
                rfcopt.diffspec = diffspec;            
                rfcopt.mapname=finalopt.mapname;
                rfcopt.pl_cross=finalopt.pl_cross;
                % for the Keck year split no simr
                if jack=='f' & ~isempty(strfind(estimator{:},'overall'))
                  rfcopt.simr=true;
                else
                  rfcopt.simr=false;
                end
                
                deprojS=deproj{:};
                % when subtraction instead of regression in real data, 
                % use the undeprojected sim but just in the case that the deprojected
                % systematic was not simulated (diff ell yes but not diff pointing):
                if strcmp(deproj{:},'1202') continue; end
                if strcmp(deproj{:},'1102') deprojS='1100'; end
                if strcmp(deproj{:},'2102') deprojS='0100'; end
                if length(deproj{:})==5; deprojS='1100'; end
                
                r = sprintf([folder '/real_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1  '_real_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2,...
                '_real_' daughter3 '_filtp3_weight3_gs_dpDDDD_jackJ' ct3 'BB' ]); %5 for the per-tile abscal
                n = sprintf([folders '/xxx6_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx6_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2s,...
                '_xxx6_' daughter3 '_filtp3_weight3_gs_dpDDDD_jackJ' ct3s 'BB']); %signflip
                s = sprintf([folders '/xxx2_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s '_xxx2_' daughter2 '_filtp3_weight3_gs_dpDDDD_jack0' ct2s,...
                '_xxx2_' daughter3 '_filtp3_weight3_gs_dpDDDD_jack0' ct3s 'BB']); %yes, always jack0, unlensed
                sn= sprintf([folders '/xxx7_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx7_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2s,...
                '_xxx7_' daughter3 '_filtp3_weight3_gs_dpDDDD_jackJ' ct3s 'BB']); %lensed
                b = sprintf([folders '/xxx4_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s '_xxx4_' daughter2 '_filtp3_weight3_gs_dpDDDD_jack0' ct2s,...
                '_xxx4_' daughter3 '_filtp3_weight3_gs_dpDDDD_jack0' ct3s 'BB']); %yes, always jack0
                snr=sprintf([folders '/xxx9_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx9_' daughter2 '_filtp3_weight3_gs_dpDDDD_jackJ' ct2s,...
                '_xxx9_' daughter3 '_filtp3_weight3_gs_dpDDDD_jackJ' ct3s 'BB']); %lensed
                
                r=regexprep(r,'DDDD',deproj1,'once'); 
                r = strrep(r,'DDDD',deproj{:}); r = strrep(r,'BB',estimator{:}); r = strrep(r,'J',jack);
                n = strrep(n,'DDDD',deprojS); n = strrep(n,'BB',estimator{:}); n = strrep(n,'J',jack);
                s = strrep(s,'DDDD',deprojS); s = strrep(s,'BB',estimator{:}); s = strrep(s,'J',jack);
                sn= strrep(sn,'DDDD',deprojS); sn= strrep(sn,'BB',estimator{:}); sn= strrep(sn,'J',jack);
                b = strrep(b,'DDDD',deprojS); b = strrep(b,'BB',estimator{:}); b = strrep(b,'J',jack);
                snr= strrep(snr,'DDDD',deprojS); snr= strrep(snr,'BB',estimator{:}); snr= strrep(snr,'J',jack);

                % now all stored in finalopt
                finalopt.nsimset=n; finalopt.ssimset=s; finalopt.snsimset=sn; finalopt.bosimset=b; finalopt.snrsimset=snr;
              
                % for b2 auto also use the direct bpwfs
                rc = r;
                if directbpwf
                  finalopt.supfacstyle='direct_bpwf';
                  rc = [r,'_directbpwf'];
                  finalopt.bpwfname=['0706x1332/xxxx_allcmb_filtp3_weight3_gs_dp1100_jack0_xxxx_cmb_subset2012_2013_rxall_filtp3_weight3_gs_dp1100_jack0_matrix_directbpwf.mat'];
                end
                if rbc
                  finalopt.residbeamcorr='aps/0751/beammap_leakage_spectra_1000rlz_usewithdp1102';
                  finalopt.daughter='_rbc';
                  rc = [rc,'_rbc'];
                end

                % define the farm jobs.  does the final plots for max ell=200,500,
                % saves the final file, and does chi2 plot for max ell=200,500
                cmd=[...
                'reduc_final(r,finalopt,0,500);close all;',...
                'reduc_final(r,finalopt,2,500);close all;',...
                'reduc_final(r,finalopt,2,200);close all;',...
                'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
                'reduc_final_chi2(rc,rfcopt2,500,2);close all;'];

                rfcopt2 = rfcopt;
                rfcopt2.chibins=get_chibins(10);
                farmit('farmfiles/final/',cmd,'var',{'r','rc','n','s','sn','b','rfcopt','rfcopt2','finalopt'},'queue',queue_small,'mem',2000,'maxtime',60);
%                  eval(cmd)

              end  %loops
            end
          end
        end
      end
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% After running the final, combine bicep2 and keck using:

%{
rf='0751x1353/real_a_fixesub_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp11022_jack01_real_b_filtp3_weight3_gs_dp11022_jack01_matrix_overrx_overall_directbpwf.mat'
reduc_final_comb(rf);
%}
