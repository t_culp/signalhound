function runsim_1353(nbase,doreal,donoisesim,dorealcoadd,donoise,dosim,docoadd,rlz,docombmaps,queue,farmdps,usecompiled,simtypes,submit,daughter)
% function runsim_1353(nbase,doreal,donoisesim,dorealcoadd,donoise,dosim,docoadd,rlz,docombmaps,queue,farmdps,usecompiled,simtypes,submit,daughter)
%  
% Real and sim maps with Planck deprojection map and planck noise, planck cosmo parameters, constrained sim rlz
% this should become the B2 major sim set
% the runsim_ function makes
%  - real pairmaps/maps
%  - sim pairmaps/maps
%  - the sim map combinations
%  - the traditional noise sim
%  - the signflip noise sim
% all the code for post-processing of these maps (aps, simsets and final etc) is found down in the bottom after this function
% there are also examples how to runsim_0752 in practice
%  
%  doreal -> make real pairmaps
%  donoisesim-> make traditional noise sim (just few rlz)
%  dorealcoadd-> coadd real pair maps
%  donoise-> make signflip noise sim
%  dosim-> make flavour subset sims
%  docoadd-> coadd traditional noise sims and flavour subsetsims
%  rlz -> i.e. 1:10
%  docombmaps: runsim_0752(0752,0,0,0,0,0,0,rlz,1,0) 
%  queue: 0 LSF, 1 serial_requeue (default), 2 general 
%  farmdps: farm deprojections seperately (0 default)
%  usecompiled: 1 default
%  simtypes: [2,4,5] default
%  submit: 1 default, if 0 just create farmfiles without submission
%  
%  funny order? the donoise and dosim options need the real coadded maps!
%  
%  run the coadd command twice as it is using a split coadd in the first round
%  then a coadd_coadds in the second
%  
%  before running on many realizations, copy the input_maps to panlfs
%  

% check to not mess it up:
if nbase~=1353 & nbase~=0752
  error('This script is not meant for this base number.')
end
if nbase==0752 & ~strcmp(get_experiment_name(),'bicep2')
  error('Are you in the right folder for Bicep2 sims?')
end
if nbase==1353 & ~strcmp(get_experiment_name(),'keck')
  error('Are you in the right folder for Keck sims?')
end

% defaults: do nothing at all
if(~exist('doreal','var'))
  doreal=0;
end
if(~exist('donoisesim','var'))
  donoisesim=0;
end
if(~exist('dorealcoadd','var'))
  dorealcoadd=0;
end
if(~exist('donoise','var'))
  donoise=0;
end
if(~exist('dosim','var'))
  dosim=0;
end
if(~exist('docoadd','var'))
  docoadd=0;
end
if(~exist('rlz','var'))
  rlz=1;
end
if(~exist('docombmaps','var'))
  docombmaps=0;
end
if(~exist('queue','var'))
  queue=1;
end
if(~exist('farmdps','var'))
  farmdps=0;
end
if(~exist('usecompiled','var'))
  % choose whether to use normal (.m) or compiled code
  usecompiled=1;
end
if(~exist('simtypes','var'))
  simtypes=[2,4,5];
end
if(~exist('submit','var'))
  submit=true;
end
if(~exist('daughter','var'))
  daughter='a';
end

%memory usage differences between B2 and keck
if nbase==1353
  memreal=30000;
  memcoadd = 30000;
  memcoaddcoadd = 12000;
  memcoaddcoadddp = 12000;
elseif nbase==0752
  memreal=10000;
  memcoadd = 16000;
  memcoaddcoadd = 12000;
  memcoaddcoadddp = 20000;
end
maxtime=240
maxtime_real=240;
if docoadd | dorealcoadd
  maxtime=8*60;
  if farmdps
    maxtime=240;
  end
end
if dosim
  if strcmp(get_experiment_name, 'keck')
    maxtime=150;
  else
    maxtime=240;
  end
end


% pick appropiate queues:
use_serial_requeue=0;
use_general=0;
if (queue==1) use_serial_requeue=1; end
if (queue==2) use_general=1; end

queue_real='normal_serial'; % real pairmaps queue
queue_sim='short_serial'; % queue for runsim jobs
queue_coadd='normal_serial'; % queue for coadd jobs
queue_coadd_coadd='normal_serial'; % queue for coadd jobs
if farmdps
  queue_coadd='short_serial'; % queue for coadd jobs
  queue_coadd_coadd='short_serial'; % queue for coadd jobs
end
queue_other='short_serial';
if use_serial_requeue | use_general
  display('we use slurm')
  slurm_queue='general,serial_requeue';
  queue_real=slurm_queue;
  queue_sim=slurm_queue;
  queue_coadd=slurm_queue;
  queue_coadd_coadd=slurm_queue;
  queue_other=slurm_queue;
end

% global options
om=1;  % supplies 'OnlyMissing' option for both runsim and coadds

ntpg=3; % NTagsPerJob real data

% simulation options
if strcmp(get_experiment_name(),'bicep2')
  rlzc=8; % rlzchunksize for runsim
end
if strcmp(get_experiment_name(),'keck')
  rlzc=3; % rlzchunksize for runsim
end
tagc=1; % tagchunksize for runsim

% coadd options
js=0 % option for FarmJacksSeparately for coadd
disp(['farming dp seperately=' num2str(farmdps)])

% which deprojection we want to do:
%  deprojs=[0,0,0,0;  % No deprojection
%           1,0,0,0;  % relgain
%           0,1,0,0;  % A/B
%           1,1,0,0;  % relgain + A/B offsets   
%           1,1,1,0;  % beamwidth + A/B offsets + relgain
%           1,1,0,1;  % ellipticity + A/B offsets + relgain
%           1,1,1,1]; % all
         
% which deprojection we want to do:
deprojs=[1,1,0,0];  % relgain + A/B offsets

% add real map specials:
if dorealcoadd
  deprojs=[1,1,0,0,0;
           1,1,0,2,0;  % measured ellipticity + A/B offsets + relgain
           1,1,0,2,2];  % measured ellipticity + A/B offsets + relgain + beam residual
end

if dorealcoadd
  rlz=[];
end
rlz

% do want to wait for creating the tag list if it is not needed
dotags = doreal|donoisesim|dorealcoadd|donoise|dosim|docoadd;
if dotags
  if strcmp(get_experiment_name(),'bicep2')
    realfile = ['maps/',num2str(nbase, '%.4d'),'/real_' daughter '_filtp3_weight3_gs_dp1100_jack0.mat']
  else
    realfile = ['maps/',num2str(nbase, '%.4d'),'/real_' daughter '_filtp3_weight3_gs_dp1100_jack01.mat']
  end
  if exist(realfile)
    x = load(realfile);
    tags = x.coaddopt.tags;
    if daughter=='c'
      % use first occurance for keck2014 since taglist is still flowing
      [tagsublist,mtl]=get_tag_sublist(x.coaddopt,[],'first');
    else
      [tagsublist,mtl]=get_tag_sublist(x.coaddopt);
    end
    clear x; 
  else
    % do the tags - this should only happen once when the real map is made:
    if nbase==0752 tags=get_tags('allcmb','has_tod'); end % 17097, B2
    if nbase==1353 % Keck
      switch daughter
       case 'a'
        tags=get_tags('cmb2012','has_tod');
       case 'b'
        tags=get_tags('cmb2013','has_tod');
       case 'c'
        tags=get_tags('cmb2014','has_tod');
      end
    end
  end
end

clear simopt
simopt.siginterp='taylor';
%this is 'aux_data/beams/beams_cmb_3yr.csv' not 'aux_data/beams/beams_cmb_3yr_planck.csv'
simopt.beamcen='obs'; 
simopt.diffpoint='obs';
simopt.chi='obs';
simopt.epsilon='obs';

%3 yrs of B2 vs planck: http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130612_abscal_4/
% 3150 is the round number recommendation arising from this posting
% simopts are not foreseen to change by daughter, so changing the simopt.ukpervolt
% was ignored in the simrunfile mechanism. To start with all sims use the same ukpervolt - 
% that of 2012 which is the default:
ukpervolt=get_ukpervolt;
%  if daughter=='b'
%    ukpervolt=get_ukpervolt('2013');
%  end
%  if daughter=='c'
%    ukpervolt=get_ukpervolt('2014');
%  end
simopt.ukpervolt=ukpervolt;
simopt.coord='C';
simopt.force_ab_int_from_common_pix=false;
simopt.update=1;

clear mapopt
mapopt.beamcen='perazdk';
mapopt.chi='obs';
mapopt.epsilon='obs';
mapopt.acpack=0;
mapopt.gs=1;
mapopt.filt='p3';
mapopt.deproj=true;
mapopt.update=true;

clear coaddopt
%  http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130628_channel_cuts_3/
switch daughter
 case 'a'
  chflags=get_default_chflags();
 case 'b'
  chflags=get_default_chflags([], '2013');
 case 'c'
  chflags=get_default_chflags([], '2014');
end

coaddopt.chflags=chflags;
coaddopt.daughter=daughter;
coaddopt.deproj_timescale='by_phase';
coaddopt.filt='p3';
coaddopt.gs=1;
coaddopt.jacktype='0123456789abcde';

%change to coaddtype=1 for keck
if nbase==1353
  coaddopt.coaddtype=1;
end

% for a special sim/real map:
%  mapopt.proj='arc';
%  coaddopt.proj='arc';

% we are going do coadding over subsets, hence specifiy
% the 1st/2nd half split explicity with the logic from
% reduc_coaddpairmaps. The spilt is at the point of even
% accumulated pair diff weight in the first and second half
% found by additional analysis
if nbase==0752 coaddopt.temporaljack_splitdate='20111101'; end
if nbase==1353 
  switch daughter
   case 'a'
    coaddopt.temporaljack_splitdate='20120719'; 
   case 'b'
    coaddopt.temporaljack_splitdate='20130710'; %this is just 1/2 the tags... 
   case 'c'
    coaddopt.temporaljack_splitdate='20140518'; %this is even weights in diff
  end
end

%%%%%%%%%%%%%%%%%%%%%%% let's work
if doreal
  mapopt.sernum=sprintf('%04dreal',nbase);
  % idendify the deprojections
  mapopt.deproj=[1:6,9];
  mapopt.deproj_residmap.unsmoothed='input_maps/planck/planck_maps/HFI_SkyMap_143_2048_R1.10_nominal_l600_00p00_cel_uk.fits';
  switch daughter
   case 'a'
    mapopt.deproj_residmap.beammap='~clwong/20140729_beamresidual/depjmap/stackedbminner_keck2012_dp1102';
   case 'b'
    mapopt.deproj_residmap.beammap='~clwong/20140729_beamresidual/depjmap/stackedbminner_keck2013_dp1102';
   case 'c'
    mapopt.deproj_residmap.beammap='~clwong/20140729_beamresidual/depjmap/stackedbminner_keck2014_dp1102';
  end

  % it is important that the deproj_map cell is size=(2,1) and not (1,2)
  mapopt.deproj_map={'input_maps/planck/planck_derivs_nopix/HFI_SkyMap_100_2048_R1.10_nominal_43p1000.fits';...
  'input_maps/planck/planck_derivs_nopix/synfast_deproj_143_nominal_B2.fits'};
  farm_makepairmaps(tags,mapopt,'JobLimit',600,'NTagsPerJob',ntpg,'FarmJobs',1, ...
                    'OnlyMissing',1,'Queue',queue_real,'MemRequire',memreal,...
                    'maxtime',maxtime_real,'submit',submit);
end

%  types
%  xxx1 - reserved for traditional noise sims
%  xxx2 - LCDM unlensed scalars with diff point on
%  xxx4 - r=0.1 tensors with E forced to zero (and T switched off at makesim stage)
%  xxx5 - LCDM lensed scalars with diff point on
%  xxx6 - sign flip noise
%  xxx8 - reserved for alternate artificial B-mode input spectrum

%  combinations:
%  xxx3 - reserved for the map sum of 1+2 
%  xxx7 - the map sum 5+1 or 5+6 
%  xxx9 - the sum of 4+5+6 
if dosim
  try
    par_make_simrunfiles(tags,simopt,nbase);
  catch
    display('simrunfile exists, not remaking it...')
  end
  
  mapopt.deproj_map={'input_maps/camb_planck2013_r0/map_unlens_n0512_rxxxx_s43p10_cPl100_dPl100.fits';...
  'input_maps/camb_planck2013_r0/map_unlens_n0512_rxxxx_sB2bbns_constrained_dPl143.fits'};
 
  type = 2;
  sigmapfilename={
  'input_maps/camb_planck2013_r0/map_unlens_n2048_rxxxx_s43p10_cPl100_dNoNoi.fits',...
  'input_maps/camb_planck2013_r0/map_unlens_n2048_rxxxx_sB2bbns_constrained_dNoNoi.fits'};
  runsim(nbase,mapopt,'normal','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime,submit);

  type = 4;
  sigmapfilename={...
  'input_maps/camb_planck2013_r0p1_noE/map_unlens_n2048_rxxxx_s43p10_cPl100_dNoNoi.fits',...
  'input_maps/camb_planck2013_r0p1_noE/map_unlens_n2048_rxxxx_sB2bbns_constrained_dNoNoi.fits'};
  runsim(nbase,mapopt,'onlypol','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime,submit);
  
  type = 5;
  %deproj template should be lensed for type5
  mapopt.deproj_map= {'input_maps/camb_planck2013_r0/map_lensed_n0512_rxxxx_s43p10_cPl100_dPl100.fits';...
  'input_maps/camb_planck2013_r0/map_lensed_n0512_rxxxx_sB2bbns_constrained_dPl143.fits'};
  sigmapfilename = {...
  'input_maps/camb_planck2013_r0/map_lensed_n2048_rxxxx_s43p10_cPl100_dNoNoi.fits',...
  'input_maps/camb_planck2013_r0/map_lensed_n2048_rxxxx_sB2bbns_constrained_dNoNoi.fits'};
  runsim(nbase,mapopt,'normal','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime,submit);

end

if donoisesim
  try
    par_make_simrunfiles(tags,simopt,nbase);
  catch
    display('simrunfile exists, not remaking it...')
  end

  mapopt.deproj_map= 'input_maps/camb_planck2013_r0/map_unlens_n0512_rxxxx_sB2bbns_dPl143.fits'; % should this be lensed?
  
  type = 1;
  sigmapfilename='input_maps/camb_planck2013_r0/map_unlens_n2048_rxxxx_sB2bbns_dNoNoi.fits';
  runsim(nbase,mapopt,'none','data',type,sigmapfilename,rlz,tags,false,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime);
end

if docoadd | dorealcoadd
  %loop over sim types, will jump out for real
  for k=simtypes
    k

    coaddopt.sernum=sprintf('%04dxxx%01d',nbase,k);
    if dorealcoadd
      coaddopt.sernum(5:8)='real';
      coaddopt.save_cuts=true;
    else
      coaddopt.save_cuts=false;
    end
    % no tag substitution for real or traditional noise sim:
    if ~dorealcoadd & k~=1
      coaddopt.tagsublist=tagsublist;
    end
    if farmdps
      %loop over the deprojs
      for jj = 1:size(deprojs,1)
        coaddopt.deproj=deprojs(jj,:);
        if dorealcoadd && all(coaddopt.deproj==[1 1 0 2]);  
          % put in the make extra dp1102 maps for k2012,13,bicep2 with fixed esub
          coaddopt.daughter=[coaddopt.daughter '_fixesub'];
        end
        farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',20,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
      end
    else
      coaddopt.deproj=deprojs;
      farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                              'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                              'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadddp,...
                              'SplitSubmission',20,'UseCompiled',usecompiled,...
                              'maxtime',maxtime,'submit',submit);
    end
    %no loop for real data
    if (dorealcoadd) break; end 
  end
end

% can be done after real data coadd
if donoise
  % sign_flip noise, see: http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130614_sign_flip_seq/
  % and for the signal subtraction: http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20140709_SignFlip_SignalSub/
  % and links therein.
  % Keck 2014 signal subtraction:
  if nbase==1353 && daughter=='c'
    disp('use dp0000 to subtract')
    x=load(['maps/',num2str(nbase,'%.4d'),'/real_',coaddopt.daughter,'_filtp3_weight3_gs_dp0000_jack01'],'ac','m');
    coaddopt.ac=x.ac;
    coaddopt.m=x.m;
    clear x;
  end
  % that is the coaddtype
  ct = '';
  if nbase==1353
    ct = '1';
  end

  % first load in real data to fetch the jackmasks
  % we do this before the loop over the realizations, to avoid the io
  % all the non-temporal jacks get the same sign_flip sequence:
  x= containers.Map();
  x('024689abce')=load(['maps/' num2str(nbase, '%.4d') '/real_' coaddopt.daughter '_filtp3_weight3_gs_dp1100_jack2',ct,'_s']);
  % all the temporal jacks fetch the individual jackmasks:
  for xx=['1','3','5','7','d']
    disp(['loading jack' xx]);
    x(xx)=load(['maps/' num2str(nbase, '%.4d') '/real_' coaddopt.daughter '_filtp3_weight3_gs_dp1100_jack',xx,ct,'_s']);
  end

  % the real data is coming with a large cut stucture, avoid saving this over and over
  % for the noise realizations:
  coaddopt.save_cuts=false;
  coaddopt.sernum=[num2str(nbase, '%.4d') 'real'];

  coaddopt.sign_flip_type='by_scanset';
  for ii=rlz
    % fix the random numbers
    stream0 = RandStream('mt19937ar','Seed',ii)
    RandStream.setDefaultStream(stream0);

    % loop over the different jacks
    for xx={'024689abce','1','3','5','7','d'}

      % fetch the sign_flip sequence...
      coaddopt.sign_flip_seq=get_sign_flip_seq(x(char(xx)).coaddopt);
      % ...specific for each jack
      coaddopt.jacktype=char(xx);
      % assign a realization number:
      coaddopt.sign_flip_rlz=ii;
      if farmdps
        for jj = 1:size(deprojs,1)
          coaddopt.deproj=deprojs(jj,:);
          farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',[],...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',20,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
        end
      else
        coaddopt.deproj=deprojs;
        farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',[],...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',20,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
      end
    end
  end
end

% Combine comaps
%  xxx3 - reserved for the map sum of 1+2 
%  xxx7 - the map sum 1+5 or 6+5 
%  xxx9 - the sum of 6+5+4 = 7+4
if nbase==1353; ct='1'; end; 
if nbase==0752; ct=''; end;
if docombmaps

  % first make sure all of the signal-only sims have been fixed
  s = input(['Have you fixed the ukpv for the signal-only sims?'],'s');
  if strcmp(s,'no') || strcmp(s,'n')
    disp('Fixing the ukpv for the signal-only sims.  Rerun when complete');

    % define the ukpv
    ukpervolt=get_ukpervolt;
    if daughter=='b'
      ukpervolt=get_ukpervolt('2013');
    end
    if daughter=='c'
      ukpervolt=get_ukpervolt('2014');
    end

    % farm for each rlz
    for crlz = rlz

      cmd = ['fix_ukpervolt(''',num2str(nbase, '%.4d'),'/xxx[245]_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',ukpervolt)'];
      cmd=strrep(cmd,'xxx',sprintf('%03d',crlz));
      farmit('farmfiles/comb/',cmd,'queue',queue_other,'mem',16000,'maxtime',3*60,'var','ukpervolt');
    end

    return
  end

  % split in two loops so that the maptype7 is present when 9 is done
  % carfull here, the map that contains the noise must be first!
  for crlz = rlz
  
    % this is not really part of 1353/0752 since type1 is not created...
    % cmd = ['reduc_combcomap(''',num2str(nbase, '%.4d'),'/xxx1_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',2,3,1)'];
    % if nbase==1353  %no normal sim realizations....
    %   cmd = ['reduc_combcomap(''',num2str(nbase, '%.4d'),'/xxx6_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',2,3,1)'];
    % end
    % cmd = strrep(cmd,'xxx',sprintf('%03d',crlz));
    % farmit('farmfiles/',cmd,'queue',queue_other,'mem',16000);
    
    cmd = ['reduc_combcomap(''',num2str(nbase, '%.4d'),'/xxx6_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',5,7,1)'];
    cmd = strrep(cmd,'xxx',sprintf('%03d',crlz));
    farmit('farmfiles/comb/',cmd,'queue',queue_other,'mem',16000,'maxtime',60);
  end

  for crlz = rlz
    cmd = ['reduc_combcomap(''',num2str(nbase, '%.4d'),'/xxx7_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',4,9,1)'];
    cmd = strrep(cmd,'xxx',sprintf('%03d',crlz));
    farmit('farmfiles/comb/',cmd,'queue',queue_other,'mem',16000,'maxtime',60);   
  end

end

return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bunch of code for stuff that combines all the realiations created above, usually run interactively on legacy:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%make links to 1450 for noise and real maps
if(0)
  for i=1:199
      target=['/n/bicepfs1/bicep2/pipeline/maps/1450/' num2str(i, '%.3d') ...
              '6_a_filtp3_weight3_gs_dp1100_jack0.mat'];
      name=['maps/0752/' num2str(i, '%.3d') ...
            '6_a_filtp3_weight3_gs_dp1100_jack0.mat'];
      system(['ln -s ' target ' ' name]);
  end
end

if(0)
  system('ln -s /n/bicepfs1/bicep2/pipeline/maps/1450/real_a_filtp3_weight3_gs_dp1102_jack0.mat maps/0752/real_a_filtp3_weight3_gs_dp1102_jack0.mat')
  system('ln -s /n/bicepfs1/bicep2/pipeline/maps/1450/real_a_filtp3_weight3_gs_dp1100_jack0.mat maps/0752/real_a_filtp3_weight3_gs_dp1100_jack0.mat')
  system('ln -s /n/bicepfs1/bicep2/pipeline/maps/1450/real_a_filtp3_weight3_gs_dp0000_jack0.mat maps/0752/real_a_filtp3_weight3_gs_dp0000_jack0.mat')
end

if(0)
  system('ln -s /n/panlfs/bicep/bicep2/pipeline/pairmaps/1450/real pairmaps/0752/real') 
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%first, run some sims
%%%%%%%%%%%%%
%then, try some sims:
if(0)

  nbase=1353 %or 1353
  doreal=0
  donoisesim=0
  dorealcoadd=0
  donoise=0
  dosim=1
  docoadd=0
  docombmaps=0
  rlz=[137:139]
  % 1:350 ukpervolt fixed; 137-139 have to be remade
  

  queue=1
  farmdps=1
  usecompiled=1
  simtypes=[2]
  submit=1
  docorrnoise=0
  daughter='c'

  runsim_0752(nbase,doreal,donoisesim,dorealcoadd,donoise,dosim,docoadd,rlz,docombmaps,queue,farmdps,usecompiled,simtypes,submit,daughter)

end

%link pureB_matrix bpwf...this assumes the estimator doesn't affect
%the bpwf, which I think is true

if(0)
  ll=dir('aps/0752x1353/real*jack01_matrix*');
  for i=1:length(ll)
    sname=strrep(ll(i).name, '.mat', '');
    kname=strrep(sname, '_matrix', '_pureB');
    system(['ln -s /n/bicepfs1/bicep2/pipeline/aps/0750x1350/' kname '_bpwf.mat aps/0752x1353/' sname '_bpwf.mat']);
  end
end

%link bpwf
if(0)
  ll=dir('/n/bicepfs1/bicep2/pipeline/aps/0750x1350/*bpwf*');
  for i=1:length(ll)
    system(['ln -s /n/bicepfs1/bicep2/pipeline/aps/0750x1350/' ll(i).name ' aps/0752x1353/' ll(i).name]);
  end
end

if(0)

  jacks = get_default_coaddopt();
  jacks = jacks.jacktype;
  nbase=1353;

  % reduc_plotcomap_pager auto pager
  % function reduc_plotcomap_pager(mapname,apsopt,fixc,pad,realimag,dotemplates,doTQ,doHJ,doSL,doCross,doSub,om)
  % system_safe('rm reduc_plotcomap_pager/1350?sub/*Rx[1234]*.png')
%    types = {'real','0012','0014','0015','0016','0017','0019'}
  types = {'0016','0012','0014','0015','0017','0019'}
  % types = {'0012','0014','0015'}
  ct='1'; 
  apsopt.ukpervolt=get_ukpervolt('2014');
  crx = [2];
  for type = types
    if strcmp(type{:},'real')
      deprojs={'1100','1102'};
      % subs = {'','psub','gsub','dsub'}
      subs = {''}
    else
      deprojs={'1100'};
      subs={''};
    end
    for deproj = deprojs
      for jack=jacks
        for coaddrx=crx
          for fcs=[0,1]
            for sub = subs

              lastp = '_B_noi.png';
              if nbase==1353 %Keck
                if coaddrx==0
                  lastp = '_B_noi_Rx4.png';
                else
                  lastp = '_B_noi_Keck.png'; 
                end
              end
              if fcs lastp = strrep(lastp,'.png','_fcs.png'); end
              
              apsopt.coaddrx=coaddrx;
              map=[num2str(nbase),'/',type{:},'_c_filtp3_weight3_gs_dpDDDD_jackJJ.mat'];
              % map=[num2str(nbase),'/2015_a_filtp3_weight3_gs_dpDDDD_jackJJ.mat'];
              % map=[num2str(nbase),'/real_a_filtp3_weight3_gs_arc_dpDDDD_jackJJ.mat'];
              % map=[num2str(nbase),'/2015_a_filtp3_weight3_gs_arc_dpDDDD_jackJJ.mat'];
              map=strrep(map,'JJ',[jack,ct]);
              map=strrep(map,'DDDD',deproj{:});
              if strcmp(deproj,'1102')
                map = strrep(map,'_c_','_c_fixesub_');
              end
              cmd=['reduc_plotcomap_pager(map,apsopt,',num2str(fcs),',1,1,0,0,0,0,0,''',sub{:},''',1);'];
              if ~exist(['reduc_plotcomap_pager/1353',sub{:},'/' map(6:end-4) lastp],'file')
                disp('do it')
                farmit('farmfiles/rpp/',cmd,'var',{'map','apsopt'},'queue','general,serial_requeue','mem',15000,'maxtime',30);
              else
                disp('skip')
              end
            end
          end
        end
      end
    end  
  end
end


if (0)
  % first load in real data to fetch the jackmasks
  % we do this before the loop over the realizations, to avoid the io
  % all the non-temporal jacks get the same sign_flip sequence:
  ct='1'
  nbase=1353;
  daughter='c';
  for xx=['1','2','3','5','7','d']
    disp(['loading jack' xx]);
    load(['maps/' num2str(nbase, '%.4d') '/real_' daughter '_filtp3_weight3_gs_dp1100_jack',xx,ct]);
    coaddopt = rmfield(coaddopt,'c');
    save(['maps/' num2str(nbase, '%.4d') '/real_' daughter '_filtp3_weight3_gs_dp1100_jack',xx,ct,'_s'],'ac','coaddopt','m');
  end
end























