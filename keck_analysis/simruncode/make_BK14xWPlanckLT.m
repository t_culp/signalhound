function make_BK14xWPlanckLT(snLT)
% make_BK14xWPlanckLT()
%
% hacked from make_BK14xWPlanck.m - needs cleanup!!!
%
% goal is to add lensing templates as 12th band in BK14 analysis
%
% run the different do_ function one by one in the order listed below
%
% make_BK14xWPlanckLT('1631') - ideal lensing template w. no noise
% make_BK14xWPlanckLT('1632') - ideal lensing template w. noise same as BK14_150
% make_BK14xWPlanckLT('3005') - Chris provided CMB=BK, Phi=ideal

  switch snLT
   case {'1631','1632'}
    n=499;
    stmapname='_aabd_filtp3_weight3_gs_dp1100_jack0';
    ltmapname='_aabd_filtp3_weight3_gs_dp1100_jack0';
   case '3005'
    n=499;
    stmapname='_aabd_filtp3_weight3_gs_dp1100_jack0';
    ltmapname='_lt_CMBbk_PHIideal';
   case '3006'
    n=499;
    stmapname='_aabd_filtp3_weight3_gs_dp1100_jack0';
    ltmapname='_lt_CMBideal_PHIideal';
   case '3007'
    n=499;
    stmapname='_aabd_filtp3_weight3_gs_dp1100_jack0';
    ltmapname='_lt_CMBideal_PHIpl545';
   case '3008'
    n=499;
    stmapname='_aabd_filtp3_weight3_gs_dp1100_jack0';
    ltmapname='_lt_CMBbk_PHIpl545';
   case '3009'
    n=50;
    stmapname='_aabd_filtp3_weight3_gs_dp1100_jack0';
    ltmapname='_lt_CMBsptpol_PHIideal';
   case '3010'
    n=50;
    stmapname='_aabd_filtp3_weight3_gs_dp1100_jack0';
    ltmapname='_lt_CMBsptpol_PHIpl545';
  end
    
  keyboard
  
  switch snLT
   case {'1631','1632'}
    % make fake lensing templates as 150GHz lensed-unlensed sims
    % - put them in type5
    
    % this should work now
    if(0)
      for i=1:n
        i
        % get the unlen and len maps
        load(sprintf('maps/1459/%03d2%s',i,stmapname));
        ul=make_map(ac,m,coaddopt);
        load(sprintf('maps/1459/%03d5%s',i,stmapname));
        le=make_map(ac,m,coaddopt);
        % take the difference
        map=le;
        for j=1:numel(map)
          map(j).T=le(j).T-ul(j).T;
          map(j).Q=le(j).Q-ul(j).Q;
          map(j).U=le(j).U-ul(j).U;
        end
        % take the diff of 150's as the "lensing template"
        map=map(2);
        % store back to file
        save(sprintf('maps/%s/%03d5%s',snLT,i,ltmapname),'map','coaddopt','m');
      end
    end
   
    % but at the moment we are doing the below...
    
    % instead get the healpix maps, difference and stuff them through
    % the observing matrix to get ac's - note that this is known to be
    % imperfect (20150617_lensing) and may piss me off later
    rm=get_reobs_matrix();
    matrix=load(rm);
    m=matrix.m;
    coaddopt=matrix.coaddopt;
    % get rid of the stuff that blows up the file size
    % (taken from do_reobs in make_BK14xWPlanck)
    %coaddopt = rmfield(coaddopt,'c');
    coaddopt = rmfield(coaddopt,'hsmax');
    coaddopt = rmfield(coaddopt,'whist');
    coaddopt = rmfield(coaddopt,'devhist');
    mapopt = coaddopt.mapopt{1};
    mapopt = rmfield(mapopt,'c');
    mapopt = rmfield(mapopt,'hs');
    mapopt = rmfield(mapopt,'devhist');
    coaddopt = rmfield(coaddopt,'mapopt');
    coaddopt.mapopt{1} = mapopt;
    for i=1:n
      i
      hmu=read_fits_map(sprintf('input_maps/camb_planck2013_r0/map_unlens_n0512_r0%03d_sB2bbns_cPl143_dNoNoi.fits',i));
      hml=read_fits_map(sprintf('input_maps/camb_planck2013_r0/map_lensed_n0512_r0%03d_sB2bbns_cPl143_dNoNoi.fits',i));
      hmd=hml; hmd.map=hml.map-hmu.map;
      ac=reduc_observemap(matrix,hmd,8,0,0,[],[],1);
      save(sprintf('maps/%s/%03d5%s',snLT,i,ltmapname),'ac','coaddopt','m');
    end
    
   case {'3005','3006','3007','3008','3009','3010'}
    
    % Lensing templates
    cmbtype = {'ideal','ideal', 'bk',   'sptpol', 'sptpol'};
    phitype = {'ideal','pl545', 'pl545','ideal',  'pl545'};
    sernum = [3006,3007,3008,3009,3010];
    
    dosernum=str2num(snLT);
    
    doind = find(sernum==dosernum);
    
    clear rlz
    rlz=splitvec(1:n,50);
    
    for j=1:numel(rlz)
      for type=[5,6,7]
        xx=rlz{j};
        cmd=sprintf('for rlzk=xx;gen_lt(rlzk,type,%d,''%s'',''%s'',1);end',sernum(doind),cmbtype{doind},phitype{doind});
        farmit('farmfiles/',cmd,'mem',5000,'maxtime',240,'queue','serial_requeue,itc_cluster','var',{'xx','type'});
      end
    end
    
    
  end
  
  % make single blank map
  load(sprintf('maps/1459/%03d2%s',1,stmapname));
  ac=cal_coadd_ac(ac(2),1e-99); coaddopt=coaddopt(2);
  save(sprintf('maps/%s/blankmap',snLT),'ac','coaddopt','m');
  
  % symlink real to blank - perhaps should link to a noise rlz
  system(sprintf('cd maps/%s; ln -s blankmap.mat real%s.mat',snLT,ltmapname));
  system(sprintf('cd maps/%s; ln -s blankmap.mat real%s.mat',snLT,ltmapname));

  % make noise realizations if necessary
  switch snLT
   case '1631'
    % link noise rlz to blank
    for i=1:n
      system(sprintf('cd maps/%s; ln -s blankmap.mat %03d6%s.mat',snLT,i,ltmapname));
    end 
    % combine 5 and 6 to make type7
    reduc_combcomap(sprintf('%s/???6%s.mat',snLT,ltmapname),5,7);
   
   case '1632'
    % copy 150GHz noise rlz to type6's here but with offset of one
    % so don't correlate with the noise in 150x150
    
    % running on holybicep01 this loop weirdly slowed down
    % iteration by iteration...
    for i=466:n
      i
      if(i~=499)
        j=i+1;
      else
        j=1;
      end
      % get the noise maps
      load(sprintf('maps/1459/%03d6%s',j,stmapname));
      % store back to file
      ac=ac(2);
      save(sprintf('maps/%s/%03d6%s',snLT,i,ltmapname),'ac','coaddopt','m');
    end
    % combine 5 and 6 to make type7
    reduc_combcomap(sprintf('%s/???6%s.mat',snLT,ltmapname),5,7);

  end
  
  % symlink all the type 2,3,4's to blank
  for i=1:n
    i
    system(sprintf('cd maps/%s; ln -s blankmap.mat %03d2%s.mat',snLT,i,ltmapname));
    system(sprintf('cd maps/%s; ln -s blankmap.mat %03d3%s.mat',snLT,i,ltmapname));
    system(sprintf('cd maps/%s; ln -s blankmap.mat %03d4%s.mat',snLT,i,ltmapname));
  end

  % combine 7 and 4 to make 9 and 7 and 3 to make 8
  %reduc_combcomap(sprintf('%s/???7%s.mat',snLT,ltmapname),4,9);
  %reduc_combcomap(sprintf('%s/???7%s.mat',snLT,ltmapname),3,8);

  % symlink all the 8&9's to 7
  for i=1:n
    i
    system(sprintf('cd maps/%s; ln -s %03d7%s.mat %03d8%s.mat',snLT,i,ltmapname,i,ltmapname));
    system(sprintf('cd maps/%s; ln -s %03d7%s.mat %03d9%s.mat',snLT,i,ltmapname,i,ltmapname));
  end
  
  return

  % do the maps pager
  do_mapspager('1459','1615',snLT,stmapname,ltmapname,[-0.5,-0.6,zeros(1,10)])
  
  % aps through final file
  % [-0.5,-0.6] are the best fit global pol rots for BK14
  % from here: http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20150220_birefringence/
  do_aps('1615','1459',snLT,stmapname,ltmapname,[-0.5,-0.6, zeros(1,10)],1)
  do_simset('1615','1459',snLT,stmapname,ltmapname)
  do_final('1615','1459',snLT,stmapname,ltmapname,{'BK14_95','BK14_150'})

  % the aps for the multicomponent analysis
  % * "hyper-idealized" lensing templates (sn 1631,1632) use a 5666 file
  %   (because lensing template contains the same signal modes as 150
  %   GHz CMB map)
  % * Other lensing templates (sn 30xx) use a 56656 file because we
  %   need lensing template signal sims to estimate their covariance
  %   with signals in the CMB maps.
  switch snLT
    case {'1631', '1632'}    
      do_5666('1615','1459',snLT,stmapname,ltmapname)
    case {'3005', '3006', '3007', '3008', '3009', '3010'}
      do_56656('1615', '1459', snLT, stmapname, ltmapname)
    otherwise
      % Unknown lensing template sim number -- do nothing
  end

return

function do_mapspager(snI,snF,snLT,stmapname,ltmapname,polrot)
% does the maps pager:

  if ~exist('farmfiles/rpp/','dir')
    mkdir('farmfiles/rpp/')
    setpermissions('farmfiles/rpp/', '770')
  end

  % these aps opt should be the same as used for reduc_makeaps. Perhaps they should be
  % generated by a function for both the maps pager and the aps making.
  % it is super useful for debugging to check the maps pager first before running the aps.
  apsopt=get_default_apsopt();
  apsopt.commonmask='gmean';
  apsopt.commonmasksel=[1,2];
  apsopt.ukpervolt=get_local_ukpervolt();
  apsopt.purifmatname=get_purification_matrix(12);
  apsopt.mapname={'BK14_95','BK14_150','W023','P030','W033','P044', ...
                  'P070','P100','P143','P217','P353','LT'};

  % the real data:
  apsopt.polrot=polrot;
  cmd = {};
  for fcs=0:1
    % dp1100 for both BK and external data
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/real',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    % dp1102 for BK, dp1100 for external
    spmapname=strrep(stmapname,'dp1100','dp1102');
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/real',spmapname,'.mat'',{''',snI,''',''',snF,''',''dp1102'',''dp1100''},{''',snI,''',''',snLT,''',''',spmapname,''',''',ltmapname,'''}},apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
  end
  
  for ccmd = cmd
    farmit('farmfiles/rpp/', ccmd, ...
        'var',{'apsopt'}, ...
        'queue','itc_cluster', ...
        'mem',27000, ...
        'maxtime',60, ...
        'submit',0);
  end
  
  % the sims:
  rlz = '001';
  apsopt.polrot=[0,0, zeros(1,10)];
  apsopte = apsopt;
  % For types 3/4/5, the external sim maps are the same as for BK. The
  % same 95+150 map is loaded twice (since the second is a sim link), hence
  % there are naturally 5 maps: 95, 150, 95, 150, LT. The external maps are
  % reobserved with the 150 observing matrix, so reuse the 4th sim map for
  % all 9 external maps.
  %apsopte.expand_ac = [1,2, 4*ones(1,9),5];
  apsopte.expand_ac = {[],2*ones(1,9),[]};
  % for type 3 (dust) scale according to the band width.
  % get_dust_scaling() --> [1.0000    1.0000    0.0344    0.0498    0.1005    0.2251   25.5711]
  apsopt3 = apsopte;
  d=get_dust_scaling();
  apsopt3.scalefac = {{d(1:2)},{d(3:11)},{}};
  % the combined types for the external maps are already in the proper 5x1 format,
  % no need to expand them on the fly:
  apsopt8 = apsopt; apsopt9 = apsopt;
  % still we must scale the dust and r portions of the ac cells:
  %apsopt8.scalefac = {1*ones(1,12),1*ones(1,12),apsopt3.scalefac{1}};
  apsopt8.scalefac = {{[1,1],[1,1],d(1:2)},{ones(1,9),ones(1,9),d(3:11)},{}};
  % type9 has r=0.2:
  %apsopt9.scalefac = {1*ones(1,12),1*ones(1,12),sqrt(2)*ones(1,12)};
  apsopt9.scalefac = {{[1,1],[1,1],sqrt(2)*[1,1]},{ones(1,9),ones(1,9),sqrt(2)*ones(1,9)},{}};
  cmd = {};
  for fcs=0:1
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'2',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopte,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'3',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt3,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'4',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopte,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'5',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopte,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'6',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt,' ,num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'7',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt,' ,num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'8',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt8,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'9',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt9,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
  end
  for ccmd = cmd
    farmit('farmfiles/rpp/', ccmd, ...
        'var',{'apsopt','apsopte','apsopt3','apsopt8','apsopt9'}, ...
        'queue','itc_cluster', ...
        'mem',27000, ...
        'maxtime',60, ...
        'submit',0);
  end

  babysitjobs('rpp/*.mat','wait2',0,0)

return

function do_aps(snF,snI,snLT,stmapname,ltmapname,polrot,do_real)
% make the aps that go into the final pagers

  if ~exist('farmfiles/aps/','dir')
    mkdir('farmfiles/aps/')
    setpermissions('farmfiles/aps/', '770')
  end

  % select the realizations, 0-499
  sel = '[01234]??'

  % now run reduc_makeaps in cross spectrum mode
  apsopt=get_default_apsopt();
  apsopt.random_order = 1;
  apsopt.commonmask='gmean';
  apsopt.commonmasksel=[1,2];
  apsopt.update=1;
  apsopt.ukpervolt=get_local_ukpervolt();
  % this is more legacy, could set it to 'normal' without effect
  apsopt.pure_b = 'kendrick';
  apsopt.purifmatname=get_purification_matrix(12);

  % the real data
  apsopt.polrot=polrot;
  cmd = {};
  cmd{end+1} = ['reduc_makeaps({''',snI,'/real',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt)'];
  spmapname=strrep(stmapname,'dp1100','dp1102');
  cmd{end+1} = ['reduc_makeaps({''',snI,'/real',spmapname,'.mat'',{''',snI,''',''',snF,''',''dp1102'',''dp1100''},{''',snI,''',''',snLT,''',''',spmapname,''',''',ltmapname,'''}},apsopt)'];
  if do_real
    for ccmd=cmd
      farmit('farmfiles/aps/',ccmd, ...
        'var',{'apsopt'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',60, ...
        'submit',0);
    end
  end
  if(0)
  % the real experiment jack
  apsopt.howtojack='dim1';
  if do_real
    for ccmd=cmd
      farmit('farmfiles/aps/',ccmd, ...
        'var',{'apsopt'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',60, ...
        'submit',0);
    end
  end
  end
  
  % the sims
  % regular jack
  apsopt.howtojack='dim2';
  % no polrot for the sims
  apsopt.polrot=[0,0, zeros(1,10)];
  apsopte = apsopt;
  % For types 3/4/5, the external sim maps are the same as for BK. The
  % same 95+150 map is loaded twice (since the second is a sim link), hence
  % there are naturally 4 maps: 95, 150, 95, 150. The external maps are
  % reobserved with the 150 observing matrix, so reuse the 4th sim map for
  % all 9 external maps.
  apsopte.expand_ac = {[],2*ones(1,9),[]};
  % For type 3 (dust) scale according to the band width. See
  % get_dust_scaling() below.
  apsopt3 = apsopte;
  d=get_dust_scaling();
  apsopt3.scalefac = {{d(1:2)},{d(3:11)},{}};
  % The combined types for the external maps are already in the proper
  % 9x1 format, so no need to expand them on the fly:
  apsopt8 = apsopt; apsopt9 = apsopt;
  % Still we must scale the dust and r portions of the ac cells. By order
  % listed during combination in do_comb() above, these are
  %   { type6 (noise), type5 (lensed lcdm), type 3/4 (dust/r) }
  apsopt8.scalefac = {{[1,1],[1,1],d(1:2)},{ones(1,9),ones(1,9),d(3:11)},{}};
  apsopt9.scalefac = {{[1,1],[1,1],sqrt(2)*[1,1]},{ones(1,9),ones(1,9),sqrt(2)*ones(1,9)},{}};

  cmd={};
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'2',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopte)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'3',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt3)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'4',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopte)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'5',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopte)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'6',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'7',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'8',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt8)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'9',stmapname,'.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt9)'];
  % this will farm 7*8 jobs to slew through the 499*8 aps files to make
  % usually works in a night
  for ii=1:7
    for ccmd = cmd
      farmit('farmfiles/aps/',ccmd, ...
        'var',{'apsopt','apsopte','apsopt3','apsopt8','apsopt9'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',240, ...
        'submit',0);
    end
  end

  if(0)
  % the sim experiment jack
  apsopt.howtojack='dim1'; apsopte.howtojack='dim1'; apsopt3.howtojack='dim1'; apsopt8.howtojack='dim1'; apsopt9.howtojack='dim1';
  for ii=1:7
    for ccmd = cmd
      farmit('farmfiles/aps/',ccmd, ...
        'var',{'apsopt','apsopte','apsopt3','apsopt8','apsopt9'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',240, ...
        'submit',0);
    end
  end
  end

  babysitjobs('aps/*.mat','wait5',0,0)

return

function do_simset(snF,snI,snLT,stmapname,ltmapname)
% combines the aps from above into the xxx files

  if ~exist('farmfiles/simset/','dir')
    mkdir('farmfiles/simset/')
    setpermissions('farmfiles/simset/', '770')
  end

  sel = '[01234]??'
  estim = {'pureB_matrix'}
  for t='23456789'
    cmd = ['reduc_makesimset(''',snI,'x',snF,'x',snLT,'/',sel,t,stmapname,'_',sel,t,stmapname,'_',sel,t,ltmapname,'_',estim{:},'_cm.mat'')']
    farmit('farmfiles/simset/',cmd, ...
      'queue','serial_requeue,itc_cluster', ...
      'mem',4000, ...
      'maxtime',30, ...
      'submit',0)
    if(0)
    % experiment jack
    strrep(cmd,['jack',t],'jackf');
    farmit('farmfiles/simset/',cmd, ...
      'queue','serial_requeue,itc_cluster', ...
      'mem',4000, ...
      'maxtime',30, ...
      'submit',0)
    end
  end

  babysitjobs('simset/*.mat','wait5',0,0)

return

function do_final(snF,snI,snLT,stmapname,ltmapname,mapname)
% does the final file and pager

  if ~exist('farmfiles/final/','dir')
    mkdir('farmfiles/final/')
    setpermissions('farmfiles/final/', '770')
  end

  % set the final and final_chi2 options:
  finalopt.doblind = 0;
  finalopt.mapname={mapname{:},'W023','P030','W033','P044','P070', ...
                    'P100','P143','P217','P353','LT'};
  finalopt.supfacstyle='direct_bpwf';
  switch snLT
    case {'3005', '3006', '3007', '3008', '3009', '3010'}
     % this is necessary when LT does not obey usual supfac
     finalopt.forceLTsupfacBB=true;
     finalopt.bpwfname=get_bpwf();
  end

  % The bpwfs comes as [95x95, 150x150, 95x150]. However, the 12 maps used
  % here (95,150,23,30,33,40,70,100,143,217,353,LT) result in 12 auto + 66
  % cross-spectra = 78 spectra. All external maps are reobserved with the
  % BK14_150 matrix, so everything uses BPWF #2 except the 95-auto and
  % crosses with 95. In order:
  %  -  1x  95-auto
  %  - 11x 150-auto
  %  - 11x  95x150
  %  - 55x 150x150
  finalopt.expand_bpwf = zeros(1,78);
  finalopt.expand_bpwf(1)     = 1;
  finalopt.expand_bpwf(2:12)  = 2;
  finalopt.expand_bpwf(13:23) = 3;
  finalopt.expand_bpwf(24:78) = 2;

  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  rfcopt.mapname=finalopt.mapname;
  % use the simr and simd fields to display instead of the regular sim field
  rfcoptr = rfcopt; rfcoptd = rfcopt;
  rfcoptr.simtype='simr'; rfcoptd.simtype='simd';
  % the spectral jacks:
  rfcopt_diff = rfcopt; rfcoptr_diff=rfcoptr; rfcoptd_diff=rfcoptd;
  rfcopt_diff.diffspec=1;rfcoptr_diff.diffspec=1;rfcoptd_diff.diffspec=1;

  rs = {};
  % regular map, both dp1100 and dp1102
  rs{1} = [snI,'x',snF,'x',snLT,'/real',stmapname,'_real',stmapname,'_real',ltmapname,'_pureB_matrix_cm'];
  x=strfind(rs{1},'dp1100'); rs{2} = rs{1}; rs{2}(x(1)+5)='2';
  if(0)
  % the experiment jack, again both dp1100 and dp1102
  rs{3}=strrep(rs{1},'jack0','jackf');
  rs{4}=strrep(rs{2},'jack0','jackf');
  end
  
  % select the combination of first and second column to plot in the final pagers.
  % the column for the cross spectrum is chosen automatically
  pl_cross = [...
    1,2; 1,3; 1,4; 1,5; 1,6; 1,7; 1,8; 1,9; 1,10;  1,11;  1,12; ...
         2,3; 2,4; 2,5; 2,6; 2,7; 2,8; 2,9; 2,10;  2,11;  2,12; ...
              3,4; 3,5; 3,6; 3,7; 3,8; 3,9; 3,10;  3,11;  3,12; ...
                   4,5; 4,6; 4,7; 4,8; 4,9; 4,10;  4,11;  4,12; ...
                        5,6; 5,7; 5,8; 5,9; 5,10;  5,11;  5,12; ...
                             6,7; 6,8; 6,9; 6,10;  6,11;  6,12; ...
                                  7,8; 7,9; 7,10;  7,11;  7,12; ...
                                       8,9; 8,10;  8,11;  8,12; ...
                                            9,10;  9,11;  9,12; ...
                                                  10,11; 10,12; ...
                                                         11,12; ...
    ];

  for ii=1:length(rs)
    r = rs{ii};
    rc = [r,'_directbpwf'];
    
    % run the one which saves the file by hand:
    reduc_final(r,finalopt,0,500)

    cmd=[...
      'reduc_final(r,finalopt,2,200);close all;',...
      'reduc_final(r,finalopt,2,500);close all;',...
      'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
      'reduc_final_chi2(rc,rfcopt,500,2);close all;',...
      'reduc_final_chi2(rc,rfcoptr,200,2);close all;',...
      'reduc_final_chi2(rc,rfcoptr,500,2);close all;',...
      'reduc_final_chi2(rc,rfcoptd,200,2);close all;',...
      'reduc_final_chi2(rc,rfcoptd,500,2);close all;'];
    cmd2=[...
      'reduc_final_chi2(rc,rfcopt_diff,200,2);close all;',...
      'reduc_final_chi2(rc,rfcopt_diff,500,2);close all;',...
      'reduc_final_chi2(rc,rfcoptr_diff,200,2);close all;',...
      'reduc_final_chi2(rc,rfcoptr_diff,500,2);close all;',...
      'reduc_final_chi2(rc,rfcoptd_diff,200,2);close all;',...
      'reduc_final_chi2(rc,rfcoptd_diff,500,2);close all;'];
    for jj = 1:size(pl_cross,1)
      rfcopt.pl_cross=pl_cross(jj,:);
      rfcoptr.pl_cross=pl_cross(jj,:);
      rfcoptd.pl_cross=pl_cross(jj,:);
      rfcopt_diff.pl_cross=pl_cross(jj,:);
      rfcoptr_diff.pl_cross=pl_cross(jj,:);
      rfcoptd_diff.pl_cross=pl_cross(jj,:);
      finalopt.pl_cross=rfcopt.pl_cross;
      farmit('farmfiles/final/',cmd, ...
        'var',{'r','rc','rfcopt','rfcoptr','rfcoptd','finalopt'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',6000, ...
        'maxtime',100, ...
        'submit',0);
      if ~isempty(strfind(r,'jack0'))
        farmit('farmfiles/final/',cmd2, ...
          'var',{'rc','rfcopt_diff','rfcoptr_diff','rfcoptd_diff'}, ...
          'queue','serial_requeue,itc_cluster', ...
          'mem',6000, ...
          'maxtime',100, ...
          'submit',0);
      end
    end
  end

  babysitjobs('final/*.mat','wait5',0,0)

return

% This function is for "hyper-idealized" lensing templates (don't
% need to include signal sim for the lensing template because
% signal modes are identical to the 150 GHz CMB map).
function do_5666(snF,snI,snLT,stmapname,ltmapname)
%  do the aps files that feed into the multicomponent analysis:
% Ordering of maps is:
% 1. BK_95 signal sim (type 5)
% 2. BK_150 signal sim
% 3. BK_95 noise sim
% 4. BK_150 noise sim
% 5-13. external (WMAP/Planck) map noise sims
% 14. lensing template noise sim

  apsopt = get_default_apsopt()
  apsopt.random_order = 1;
  % Hyper-idealized lensing template includes coaddopt, can leave
  % ukpervolt unspecified.
  apsopt.ukpervolt={[0]};
  apsopt.update=1;
  apsopt.commonmask='gmean';
  apsopt.commonmasksel=[1,2];
  apsopt.purifmatname=get_purification_matrix(14);
  sel = '[01234]??'

  % 5666 file aps
  cmd = ['reduc_makeaps({''',snI,'/',sel,'5',stmapname,'.mat'',{''5_'',''6_''},{''5_'',''6_'',''',snI,''',''',snF,'''},{''5_'',''6_'',''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt)'];

  % each aps takes pretty long cause of the many cross spectra, with 25 jobs you
  % it through in a night:
  %njobs = 25
  njobs = 50
  for ii=1:njobs
    farmit('farmfiles/aps/',cmd, ...
        'var',{'apsopt'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',240, ...
        'submit',0);
  end

  babysitjobs('aps/*.mat','wait5',0,0)

  % directly make the simset - fine on a login node:
  cmd = ['reduc_makesimset(''',snI,'x',snF,'x',snLT,'/',sel,'5',stmapname,'_',sel,'6',stmapname,'_',sel,'6',stmapname,'_',sel,'6',ltmapname,'_matrix_cm.mat'')'];
  eval(cmd)

return

% This function is for the other lensing templates, which do need
% to include signal sims to correctly measure their signal
% covariance with CMB maps.
function do_56656(snF,snI,snLT,stmapname,ltmapname)
%  do the aps files that feed into the multicomponent analysis:
% Ordering of maps is:
% 1. BK_95 signal sim (type 5)
% 2. BK_150 signal sim
% 3. BK_95 noise sim
% 4. BK_150 noise sim
% 5-13. external (WMAP/Planck) map noise sims
% 14. lensing template signal sim
% 15. lensing template noise sim

  apsopt = get_default_apsopt()
  apsopt.random_order = 1;
  % sim 300x lensing templates don't contain coaddopt, need to
  % specify ukpervolt = 1
  apsopt.ukpervolt={[0,0], [0,0], [0,0,0,0,0,0,0,0,0], [1], [1]};
  apsopt.update=1;
  apsopt.commonmask='gmean';
  apsopt.commonmasksel=[1,2];
  apsopt.purifmatname=get_purification_matrix(15);
  sel = '[01234]??'

  % 56656 file aps
  cmd = ['reduc_makeaps({''',snI,'/',sel,'5',stmapname,'.mat'',{''5_'',''6_''},{''5_'',''6_'',''',snI,''',''',snF,'''},{''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''},{''5_'',''6_'',''',snI,''',''',snLT,''',''',stmapname,''',''',ltmapname,'''}},apsopt)'];

  % each aps takes pretty long cause of the many cross spectra, with 25 jobs you
  % it through in a night:
  %njobs = 25
  njobs = 50
  for ii=1:njobs
    farmit('farmfiles/aps/',cmd, ...
        'var',{'apsopt'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',240, ...
        'submit',0);
  end

  babysitjobs('aps/*.mat','wait5',0,0)

  % directly make the simset - fine on a login node:
  cmd = ['reduc_makesimset(''',snI,'x',snF,'x',snLT,'/',sel,'5',stmapname,'_',sel,'6',stmapname,'_',sel,'6',stmapname,'_',sel,'5',ltmapname,'_',sel,'6',ltmapname,'_matrix_cm.mat'')'];
  eval(cmd)

return

function scalefac = get_dust_scaling()
  % get the dust scaling between the different frequency bands.
  % Dust model is according to this post:
  % http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20150326_DustInputMap/DustInputMap.html
  beta_dust = 1.6;
  T_greybody = 19.6;

  % Assign experiments in order.
  likedata.opt.expt{1}  = 'K95';
  likedata.opt.expt{2}  = 'B2_150';
  likedata.opt.expt{3}  = 'WMAP9_K';
  likedata.opt.expt{4}  = 'P030';
  likedata.opt.expt{5}  = 'WMAP9_Ka';
  likedata.opt.expt{6}  = 'P044';
  likedata.opt.expt{7}  = 'P070';
  likedata.opt.expt{8}  = 'P100';
  likedata.opt.expt{9}  = 'P143';
  likedata.opt.expt{10} = 'P217';
  likedata.opt.expt{11} = 'P353';
  likedata.opt.expt{12} = 'B2_150';
  % Get instrument bandpasses.
  likedata = like_read_bandpass(likedata);

  % Calculate dust scaling between each frequency and BK 150 GHz (expt 2).
  for ii=1:numel(likedata.bandpass)
    scalefac(ii) = freq_scaling(likedata.bandpass{ii}, beta_dust, T_greybody) / ...
        freq_scaling(likedata.bandpass{2}, beta_dust, T_greybody);
  end
  % Should give result:
  % >>  0.3852    1.0000    0.0344    0.0498    0.0628    0.1005    0.2251    0.4509    0.9355    3.2507   25.5711

  % the first entry is for Keck 95 - these maps have already the proper dust
  % amplitude as per construction of the sims and don't need to get scaled:
  scalefac(1)=1;
return

function uk = get_local_ukpervolt()
  uk = {1,1,1};
return

function pm = get_purification_matrix(nmap)
  % that is the 150 one:
  pm = {'/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/1459/healpix_red_spectrum_lmax700_beamB2bbns_reob1459_aabd_150GHz_QQQUUU_proj.mat'};
  % we need nmap matrices overall:
  pm=repmat(pm,1,nmap);
  if nmap == 12
      % the first one is for K2014_95:
      pm{1} = '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/1351/healpix_red_spectrum_lmax700_beamKuber100rev1_reob1351_d_100GHz_QQQUUU_proj.mat';
  else
      % If nmap = 14 or 15, we are calculating a 5666 or 56656
      % file. In this case, map 1 is 95 GHz signal sims and map 3
      % is 95 GHz noise sim -- both use K2014_95 purification
      % matrix.
      pm{1} = '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/1351/healpix_red_spectrum_lmax700_beamKuber100rev1_reob1351_d_100GHz_QQQUUU_proj.mat';
      pm{3}=pm{1};
  end
return

function bp = get_bpwf()
  % these are those
  % http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20150617_DirectBPWF/
  bp = '1407/xxxx_d_100GHz_filtp3_weight3_gs_dp1100_jack0_xxxx_aabd_150GHz_filtp3_weight3_gs_dp1100_jack0_matrix_cm_directbpwf.mat';
return

function rm = get_reobs_matrix()
  % only need the BK14_150 reobs matrix:
  rm = '/n/panlfs2/bicep/keck/pipeline/matrixdata/matrices/1459/real_aabd_150GHz_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat';
return
