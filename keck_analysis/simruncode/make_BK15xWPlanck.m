function make_BK14xWPlanck()
% make_BK14xWPlanck()
%
% this function makes the BK14 x External maps (WMAP, Planck)
% analysis up to the interface with the multicomponent analysis (do_566)
%
% run the different do_ function one by one in the order listed below

  keyboard

  % the cooking of the Planck maps is done already,
  % mostly prepare the WMAPk map:
  do_cook('B2bbns')

  % reobservation - this creates the 5fold maps structures in 1615
  % just use the released Planck Freqs + wmap:
  do_reobs('1615','aabde','B2bbns', ...
      {'023','030','033','044','070','100','143','217','353'})

  % reused the bare signal types 2,3,4,5
  do_links('1615','1459','aabde')

  % combine into types 7,8,9
  do_comb('1615','aabd');

  % do the maps pager
  do_mapspager('1615','1459','aabde',[0.0, 0.0, 0.0, zeros(1,9)])

  % aps through final file
  % [-0.5,-0.6] are the best fit global pols for BK14
  % from here: http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20150220_birefringence/
  do_aps('1615','1459','aabde',[0.0, 0.0, 0.0, zeros(1,9)],1)
  do_simset('1615','1459','aabde')
  do_final('1615','1459','aabde',{'BK15_95','BK15_150','BK15_220'})

  % the aps for the multicomponent analysis
  do_566('1615','1459','aabd')

return

function do_cook(out_beam)
%  prepares the maps for reobservation
%  outbeam is the beam profile to smooth to

  % if you run it on an interactive node
  run_local=false;

  % To use the maps shared under the MOU:
  %
  %   The ones shared under the MOU are already available ahead of this
  %   updated analysis, see make_BK13xPlanck.m.

  % To use the officially released maps:
  %
  %     The official released maps have been manually masked to contain data
  %     in only the same region as under the MOU (lat -70 to -45, lon -55 to
  %     55 deg). This is indicated by appending "_maskbicep" to the FITS file
  %     name.
  %     
  %     Furthermore, some of the frequencies (particularly in T) contain a
  %     large DC offset that shouldn't exist when anafast generates the alms.
  %     A further manual manipulation has removed the DC offset as determined
  %     by the pixels from only within the BICEP region. This is indicated by
  %     further appending "_meanzero" to the FITS file name.
  %     
  %     We continue to use the MOU beam files for the released data maps
  %     since the noise simulations were prepared with these for the BKP
  %     analysis (see make_BK13xPlanck.m).
  %
  % Details in
  %   http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20150831_mou_vs_release_planck/
  prep_opt.out_beams = out_beam;

  prep_opt.in_file = 'input_maps/planck/planck_maps/LFI_SkyMap_030_1024_R2.01_full_maskbicep_meanzero.fits';
  prep_opt.in_beam = 'input_maps/b2planck_140811/beams/Planck_B_ell_030GHz_DX11d_FULL.fits';
  prep_fullsky_map_farm(prep_opt, run_local);

  prep_opt.in_file = 'input_maps/planck/planck_maps/LFI_SkyMap_044_1024_R2.01_full_maskbicep_meanzero.fits';
  prep_opt.in_beam = 'input_maps/b2planck_140811/beams/Planck_B_ell_044GHz_DX11d_FULL.fits';
  prep_fullsky_map_farm(prep_opt, run_local);

  prep_opt.in_file = 'input_maps/planck/planck_maps/LFI_SkyMap_070_1024_R2.01_full_maskbicep_meanzero.fits';
  prep_opt.in_beam = 'input_maps/b2planck_140811/beams/Planck_B_ell_070GHz_DX11d_FULL.fits';
  prep_fullsky_map_farm(prep_opt, run_local);

  prep_opt.in_file = 'input_maps/planck/planck_maps/HFI_SkyMap_100_2048_R2.02_full_maskbicep_meanzero.fits';
  prep_opt.in_beam = 'input_maps/b2planck_140806/beams/Planck_B_ell_100GHz_DX11d_FULL.fits';
  prep_fullsky_map_farm(prep_opt, run_local);

  prep_opt.in_file = 'input_maps/planck/planck_maps/HFI_SkyMap_143_2048_R2.02_full_maskbicep_meanzero.fits';
  prep_opt.in_beam = 'input_maps/b2planck_140806/beams/Planck_B_ell_143GHz_DX11d_FULL.fits';
  prep_fullsky_map_farm(prep_opt, run_local);

  prep_opt.in_file = 'input_maps/planck/planck_maps/HFI_SkyMap_217_2048_R2.02_full_maskbicep_meanzero.fits';
  prep_opt.in_beam = 'input_maps/b2planck_140806/beams/Planck_B_ell_217GHz_DX11d_FULL.fits';
  prep_fullsky_map_farm(prep_opt, run_local);

  prep_opt.in_file = 'input_maps/planck/planck_maps/HFI_SkyMap_353_2048_R2.02_full_maskbicep_meanzero.fits';
  prep_opt.in_beam = 'input_maps/b2planck_140806/beams/Planck_B_ell_353GHz_DX11d_FULL.fits';
  prep_fullsky_map_farm(prep_opt, run_local);

  % WMAP k-band real map
  % http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20150512_WMAPkNoise/WMAPkNoise.html
  clear prep_opt;
  prep_opt.in_file = 'input_maps/wmap9/wmap_band_iqumap_r9_9yr_K_v5.fits';
  prep_opt.in_beam = 'input_maps/wmap9/bl/bl_k_wmap_9yr.fits';
  prep_opt.in_mask = 'input_maps/wmap9/masks/wmap_point_source_catalog_mask_r9_9yr_v5.fits';
  prep_opt.out_beams = out_beam;
  prep_opt.out_roll = [375,425];
  prep_opt.out_nlmax = 450;
  prep_fullsky_map(prep_opt)
  % WMAP k-band noise sims:
  in_files = 'input_maps/wmap9/noisemaps/wmap_band_noimap_r9_9yr_K_v5_r%04d.fits';
  for rlz=1:499
    prep_opt.in_file = sprintf(in_files,rlz);
    prep_fullsky_map_farm(prep_opt,0)
  end

  % WMAP Ka-band real map, similar to K-band above
  % http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20150831_BK14_wmapKa/
  clear prep_opt;
  prep_opt.in_file = 'input_maps/wmap9/wmap_band_iqumap_r9_9yr_Ka_v5.fits';
  prep_opt.in_beam = 'input_maps/wmap9/bl/bl_ka_wmap_9yr.fits';
  prep_opt.in_mask = 'input_maps/wmap9/masks/wmap_point_source_catalog_mask_r9_9yr_v5.fits';
  prep_opt.out_beams = out_beam;
  prep_opt.out_roll = [375,425];
  prep_opt.out_nlmax = 450;
  prep_fullsky_map(prep_opt)
  % WMAP Ka-band noise sims:
  in_files = 'input_maps/wmap9/noisemaps/wmap_band_noimap_r9_9yr_Ka_v5_r%04d.fits';
  for rlz=1:499
    prep_opt.in_file = sprintf(in_files,rlz);
    prep_fullsky_map_farm(prep_opt,0)
  end

  babysitjobs('prep_fullsky_map_*.mat','wait5',0,0)

return

function do_reobs(snF,daughter,beam,fs)
% run the reobservation of the maps prepared do_cook

  % fetch the matrix
  rm = get_reobs_matrix()
  matrix = load(rm);
  coaddopt = rmfield(matrix.coaddopt,'c');

  % get rid of the stuff that blows up the file size
  mapopt = coaddopt.mapopt{1};
  mapopt = rmfield(mapopt,'c');
  mapopt = rmfield(mapopt,'hs');
  mapopt = rmfield(mapopt,'devhist');

  coaddopt = rmfield(coaddopt,'mapopt');
  coaddopt.mapopt{1} = mapopt;

  coaddopt = rmfield(coaddopt,'hsmax');
  coaddopt = rmfield(coaddopt,'whist');
  coaddopt = rmfield(coaddopt,'devhist');

  m = matrix.m;

  % create a proxy for the ac structure with nans.
  ac_proc.wsum  = nan(100,236);
  ac_proc.wwv   = nan(100,236);
  ac_proc.sitime= nan(100,236);
  ac_proc.ditime= nan(100,236);
  ac_proc.w     = nan(100,236);
  ac_proc.wcc   = nan(100,236);
  ac_proc.wcs   = nan(100,236);
  ac_proc.wss   = nan(100,236);
  ac_proc.wwccv = nan(100,236);
  ac_proc.wwcsv = nan(100,236);
  ac_proc.wwssv = nan(100,236);
  ac_proc.wz    = nan(100,236);
  ac_proc.wcz   = nan(100,236);
  ac_proc.wsz   = nan(100,236);


  % Reobseve the real map
  %
  % To use those shared under the MOU:
  %pfn=['input_maps/b2planck_DDDDDD/maps/Planck_B2Field_XXXGHz_DX11d_FULL_nside0512_nlmax1280_b',beam,'.fits'];

  % Alternatively, use the officially release real maps. See do_cook() above
  % or the README in the file directory for more details on the map
  % preparations.
  pfn=['input_maps/planck/planck_maps/III_SkyMap_XXX_NNNN_RVERS_full_maskbicep_meanzero_nside0512_nlmax1280_b',beam,'.fits']

  % These are the WMAP maps.
  wfn = {
    ['input_maps/wmap9/wmap_band_iqumap_r9_9yr_K_v5_mskd_nside0512_nlmax0450_b',beam,'_r375t425.fits'];
    ['input_maps/wmap9/wmap_band_iqumap_r9_9yr_Ka_v5_mskd_nside0512_nlmax0450_b',beam,'_r375t425.fits'];
  };
  ac = repmat(ac_proc,length(fs),1);
  for ii=1:length(fs)
    try
      switch fs{ii}
        % WMAP
        case '023'
          cfn = wfn{1};
        case '033'
          cfn = wfn{2};
        otherwise
          % the planck maps
          if str2num(fs{ii})<100
            cfn = strrep(pfn,'III','LFI');
            cfn = strrep(cfn,'NNNN','1024');
            cfn = strrep(cfn,'RVERS','R2.01');
          else
            cfn = strrep(pfn,'III','HFI');
            cfn = strrep(cfn,'NNNN','2048');
            cfn = strrep(cfn,'RVERS','R2.02');
          end
          cfn = get_planck_filename(cfn,fs{ii});
      end
      ac(ii,1)=reduc_observemap(matrix,cfn,8,0,0,[]);
      coaddopt.reob_map_fname{ii,1}=cfn;
    catch; continue; end;
  end
  save(['maps/',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack0'],'ac','coaddopt','m','-v7.3');
  clear ac;
  clear coaddopt.reob_map_fname;


  % Same game for the noise realizations
  pfn=['input_maps/b2planck_DDDDDD/sims/Planck_B2Field_XXXGHz_FFP8_FULL_noise_MC_YYYYY_nside0512_nlmax1280_b',beam,'.fits'];
  wfn={
    ['input_maps/wmap9/noisemaps/wmap_band_noimap_r9_9yr_K_v5_rYYYYY_mskd_nside0512_nlmax0450_b',beam,'_r375t425.fits'];
    ['input_maps/wmap9/noisemaps/wmap_band_noimap_r9_9yr_Ka_v5_rYYYYY_mskd_nside0512_nlmax0450_b',beam,'_r375t425.fits'];
  };
  for rlz=1:499
    rlz
    fn = sprintf(['maps/',snF,'/%03d6_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'],rlz);
   if exist(fn,'file'); continue; end
    ac = repmat(ac_proc,length(fs),1);
    for ii=1:length(fs)
     try
        switch fs{ii}
          case '023'
            cfn = strrep(wfn{1},'YYYYY',sprintf('%04d',rlz));
          case '033'
            cfn = strrep(wfn{2},'YYYYY',sprintf('%04d',rlz));
          otherwise
            cfn = strrep(pfn,'YYYYY',sprintf('%05d',rlz));
            cfn = get_planck_filename(cfn,fs{ii});
        end
        ac(ii,1)=reduc_observemap(matrix,cfn,8,0,0,[]);
        coaddopt.reob_map_fname{ii,1}=cfn;
     catch; continue; end;
    end
    save(fn,'ac','coaddopt','m','-v7.3');
    clear ac;
    clear coaddopt.reob_map_fname;
  end

return

function do_links(snF,snI,daughter)
% the bare signal sims (no noise added) types 2,3,4,5 are just the
% the same as for BK14, link them. The format of those is slightly off (2 maps
% instead of 5) - this will be fixed during aps making. Also during the aps
% stage the scaling of dust to the different freqs will be done.

  wd=pwd;
  cd(['maps/',snF]);
  fname = 'do_links.sh'
  fileID = fopen(fname, 'w');
  for i=[1:499]
    fprintf(fileID,sprintf(['ln -s ../',snI,'/%03d2_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat\n'],i));
    fprintf(fileID,sprintf(['ln -s ../',snI,'/%03d3_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat\n'],i));
    fprintf(fileID,sprintf(['ln -s ../',snI,'/%03d4_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat\n'],i));
    fprintf(fileID,sprintf(['ln -s ../',snI,'/%03d5_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat\n'],i));
  end
  fclose(fileID)
  % this is way faster that looping it in ML
  system_safe('bash do_links.sh')
  cd(wd)
return

function do_comb(snF,daughter)
%  combine signal and noise and the higher types

  if ~exist('farmfiles/comb/','dir')
    mkdir('farmfiles/comb/')
    setpermissions('farmfiles/comb/', '770')
  end

  % 2*ones(1,9) indicates that the second entry of the signal map (5,4,3)
  % which is the BK14 150 GHz observation should be combined with all the
  % external map noise realizations. This is because the external maps have
  % been reobseved with the BK14_150 reobs matrix. It also fixes the
  % dimensional issue discussed in do_links.
  cmd1=['reduc_combcomap(''',snF,'/xx?6_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',5,7,1,2*ones(1,9))'];
  cmd2=['reduc_combcomap(''',snF,'/xx?7_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',4,9,1,2*ones(1,9))'];
  cmd3=['reduc_combcomap(''',snF,'/xx?7_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',3,8,1,2*ones(1,9))'];
  % run 50 jobs in blocks of 10 rlz:
  for rlz=0:49
    cmd = [cmd1,';',cmd2,';',cmd3];
    cmd = strrep(cmd,'xx',sprintf('%02d',rlz))
    farmit('farmfiles/comb/',cmd, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',4000, ...
        'maxtime',120, ...
        'submit',0);
  end

  babysitjobs('comb/*.mat','wait2',0,0)

return

function do_mapspager(snF,snI,daughter,polrot)
% does the maps pager:
% http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20150831_BK14_mapspager/

  if ~exist('farmfiles/rpp/','dir')
    mkdir('farmfiles/rpp/')
    setpermissions('farmfiles/rpp/', '770')
  end

  % these aps opt should be the same as used for reduc_makeaps. Perhaps they should be
  % generated by a function for both the maps pager and the aps making.
  % it is super usefull for debugging to check the maps pager first before running the aps.
  apsopt=get_default_apsopt();
  apsopt.commonmask='gmean';
  apsopt.commonmasksel=[1,2];
  apsopt.ukpervolt=get_local_ukpervolt();
  apsopt.purifmatname=get_purification_matrix();
  apsopt.mapname={'BK15_95','BK15_150','BK15_220',...
                  'W023','P030','W033','P044', 'P070', ...
                  'P100','P143','P217','P353'};

  % the real data:
  apsopt.polrot=polrot;
  cmd = {}
  for fcs=0:1
    % dp1100 for both BK and external data
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    % dp1102 for BK, dp1100 for external
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0.mat'',''',snI,''',''',snF,''',''dp1102'',''dp1100''},apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
  end
  for ccmd = cmd
    farmit('farmfiles/rpp/', ccmd, ...
        'var',{'apsopt'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',60, ...
        'submit',0);
  end

  % the sims:
  rlz = '001';
  apsopt.polrot=[0,0,0, zeros(1,9)];
  apsopte = apsopt;
  % For types 3/4/5, the external sim maps are the same as for BK14. The
  % BK14 95+150 map is loaded for external (since the second is a sim link),
  % hence there are naturally 5 maps: 95, 150, 220, 95, 150. The external maps
  % are reobserved with the 150 observing matrix, so reuse the 5th sim map for
  % all 9 external maps.
  apsopte.expand_ac = [1,2,3, 5*ones(1,9)];
  % for type 3 (dust) scale according to the band width.
  % get_dust_scaling() --> [1.0000    1.0000    1.000    0.0344    0.0498    0.1005    0.2251   25.5711]
  apsopt3 = apsopte;
  apsopt3.scalefac = {get_dust_scaling()};
  % the combined types for the external maps are already in the proper 5x1 format,
  % no need to expand them on the fly:
  apsopt8 = apsopt; apsopt9 = apsopt;
  % still we must scale the dust and r portions of the ac cells:
  apsopt8.scalefac = {1*ones(1,12),1*ones(1,12),apsopt3.scalefac{1}};
  % type9 has r=0.2:
  apsopt9.scalefac = {1*ones(1,12),1*ones(1,12),sqrt(2)*ones(1,12)};
  cmd = {}
  for fcs=0:1
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'2_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopte,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'3_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt3,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'4_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopte,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'5_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopte,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt,' ,num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'7_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt,' ,num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'8_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt8,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager({''',snI,'/',rlz,'9_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt9,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
  end
  for ccmd = cmd
    farmit('farmfiles/rpp/', ccmd, ...
        'var',{'apsopt','apsopte','apsopt3','apsopt8','apsopt9'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',60, ...
        'submit',0);
  end

  babysitjobs('rpp/*.mat','wait2',0,0)

return

function do_aps(snF,snI,daughter,polrot,do_real)
% make the aps that go into the final pagers

  if ~exist('farmfiles/aps/','dir')
    mkdir('farmfiles/aps/')
    setpermissions('farmfiles/aps/', '770')
  end

  % select the realizations, 0-499
  sel = '[01234]??'

  % now run reduc_makeaps in cross spectrum mode
  apsopt=get_default_apsopt();
  apsopt.random_order = 1;
  apsopt.commonmask='gmean';
  apsopt.commonmasksel=[1,2];
  apsopt.update=1;
  apsopt.ukpervolt=get_local_ukpervolt();
  % this is more legacy, could set it to 'normal' without effect
  apsopt.pure_b = 'kendrick';
  apsopt.purifmatname=get_purification_matrix();

  % the real data
  apsopt.polrot=polrot;
  cmd = {};
  cmd{end+1} = ['reduc_makeaps({''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt)'];
  cmd{end+1} = ['reduc_makeaps({''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0.mat'',''',snI,''',''',snF,''',''dp1102'',''dp1100''},apsopt)'];
  if do_real
    for ccmd=cmd
      farmit('farmfiles/aps/',ccmd, ...
        'var',{'apsopt'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',60, ...
        'submit',0);
    end
  end
  % the real experiment jack
  apsopt.howtojack='dim1';
  if do_real
    for ccmd=cmd
      farmit('farmfiles/aps/',ccmd, ...
        'var',{'apsopt'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',60, ...
        'submit',0);
    end
  end

  % the sims
  % regular jack
  apsopt.howtojack='dim2';
  % no polrot for the sims
  apsopt.polrot=[0,0,0, zeros(1,9)];
  apsopte = apsopt;
  % For types 3/4/5, the external sim maps are the same as for BK14. The
  % BK14 95+150 map is loaded for external (since the second is a sim link),
  % hence there are naturally 5 maps: 95, 150, 220, 95, 150. The external maps
  % are reobserved with the 150 observing matrix, so reuse the 5th sim map for
  % all 9 external maps.
  apsopte.expand_ac = [1,2,3, 5*ones(1,9)];
  % For type 3 (dust) scale according to the band width. See
  % get_dust_scaling() below.
  apsopt3 = apsopte;
  apsopt3.scalefac = {get_dust_scaling()};
  % The combined types for the external maps are already in the proper
  % 9x1 format, so no need to expand them on the fly:
  apsopt8 = apsopt; apsopt9 = apsopt;
  % Still we must scale the dust and r portions of the ac cells. By order
  % listed during combination in do_comb() above, these are
  %   { type6 (noise), type5 (lensed lcdm), type 3/4 (dust/r) }
  apsopt8.scalefac = {1*ones(1,12),1*ones(1,12),apsopt3.scalefac{1}};
  apsopt9.scalefac = {1*ones(1,12),1*ones(1,12),sqrt(2)*ones(1,12)};

  cmd={};
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'2_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopte)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'3_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt3)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'4_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopte)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'5_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopte)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'7_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'8_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt8)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'9_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt9)'];
  % this will farm 7*8 jobs to slew through the 499*8 aps files to make
  % usually works in a night
  for ii=1:7
    for ccmd = cmd
      farmit('farmfiles/aps/',ccmd, ...
        'var',{'apsopt','apsopte','apsopt3','apsopt8','apsopt9'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',240, ...
        'submit',0);
    end
  end

  % the sim experiment jack
  apsopt.howtojack='dim1'; apsopte.howtojack='dim1'; apsopt3.howtojack='dim1'; apsopt8.howtojack='dim1'; apsopt9.howtojack='dim1';
  for ii=1:7
    for ccmd = cmd
      farmit('farmfiles/aps/',ccmd, ...
        'var',{'apsopt','apsopte','apsopt3','apsopt8','apsopt9'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',240, ...
        'submit',0);
    end
  end

  babysitjobs('aps/*.mat','wait5',0,0)

return

function do_simset(snF,snI,daughter)
% combines the aps from above into the xxx files

  if ~exist('farmfiles/simset/','dir')
    mkdir('farmfiles/simset/')
    setpermissions('farmfiles/simset/', '770')
  end

  sel = '[01234]??'
  estim = {'_pureB_matrix'}
  for t='23456789'
    cmd = ['reduc_makesimset(''',snI,'x',snF,'/',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack0_???',t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack0',estim{:},'_cm.mat'')']
    farmit('farmfiles/simset/',cmd, ...
      'queue','serial_requeue,itc_cluster', ...
      'mem',4000, ...
      'maxtime',30, ...
      'submit',0)
    % experiment jack
    cmd = ['reduc_makesimset(''',snI,'x',snF,'/',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jackf_???',t,'_',daughter,'_filtp3_weight3_gs_dp1100_jackf',estim{:},'_cm.mat'')']
    farmit('farmfiles/simset/',cmd, ...
      'queue','serial_requeue,itc_cluster', ...
      'mem',4000, ...
      'maxtime',30, ...
      'submit',0)
  end

  babysitjobs('simset/*.mat','wait5',0,0)

return

function do_final(snF,snI,daughter,mapname)
% does the final files and pagers:
% http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20150831_BK14_final/

  if ~exist('farmfiles/final/','dir')
    mkdir('farmfiles/final/')
    setpermissions('farmfiles/final/', '770')
  end

  % set the final and final_chi2 options:
  finalopt.doblind = 0;
  finalopt.mapname={mapname{:},'W023','P030','W033','P044','P070', ...
                    'P100','P143','P217','P353'};
  finalopt.supfacstyle='direct_bpwf';
  finalopt.bpwfname=get_bpwf();

  % The bpwfs comes as [95x95, 150x150, 95x150]. However, the 12 maps used
  % here (95,150,220,23,30,33,40,70,100,143,217,353) result in 12 auto + 66
  % cross-spectra = 78 spectra. All external maps are reobserved with the
  % BK14_150 matrix, so everything uses BPWF #2 except the 95/220-autos and
  % any crosses with 95 or 220. In order:
  %  -  1x  95-auto (BK15 x BK15)
  %  -  1x 150-auto (BK15 x BK15)
  %  -  1x 220-auto (BK15 x BK15)
  %  -  9x 150-auto (ext x ext)
  %  -  1x  95x150  (BK15 x BK15)
  %  -  1x  95x220  (BK15 x BK15)
  %  -  9x  95x150  (BK15 x ext)
  %  -  1x 150x220  (BK15 x BK15)
  %  -  9x 150x150  (BK15 x ext)
  %  -  9x 220x150  (BK15 x ext)
  %  - 36x 150x150  (ext x ext)
  % **NB** While the 220 analysis is incomplete, we reuse 95x150 for 95x220
  %        and reuse 150x150 for 150x220.
  finalopt.expand_bpwf = zeros(1,78);
  finalopt.expand_bpwf(1)     = 1;
  finalopt.expand_bpwf(2)     = 2;
  finalopt.expand_bpwf(3)     = 2; % Reuse 150-auto
  finalopt.expand_bpwf( 4:12) = 2;
  finalopt.expand_bpwf(13)    = 3;
  finalopt.expand_bpwf(14)    = 3; % Reuse 95x150
  finalopt.expand_bpwf(15:23) = 3;
  finalopt.expand_bpwf(24)    = 2; % Reuse 150-auto
  finalopt.expand_bpwf(25:33) = 2;
  finalopt.expand_bpwf(34:42) = 2; % Reuse 150x150
  finalopt.expand_bpwf(43:78) = 2;

  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  rfcopt.mapname=finalopt.mapname;
  % use the simr and simd fields to display instead of the regular sim field
  rfcoptr = rfcopt; rfcoptd = rfcopt;
  rfcoptr.simtype='simr'; rfcoptd.simtype='simd';
  % the spectral jacks:
  rfcopt_diff = rfcopt; rfcoptr_diff=rfcoptr; rfcoptd_diff=rfcoptd;
  rfcopt_diff.diffspec=1;rfcoptr_diff.diffspec=1;rfcoptd_diff.diffspec=1;

  rs = {};
  % regular map, both dp1100 and dp1102
  rs{end+1} = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack0_real_',daughter,'_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm'];
  rs{end+1} = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0_real_',daughter,'_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm'];
  % the experiment jack, again both dp1100 and dp1102
  rs{end+1} = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jackf_real_',daughter,'_filtp3_weight3_gs_dp1100_jackf_pureB_matrix_cm'];
  rs{end+1} = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jackf_real_',daughter,'_filtp3_weight3_gs_dp1100_jackf_pureB_matrix_cm'];
  % select the combination of first and second column to plot in the final pagers.
  % the column for the cross spectrum is chosen automatically
  pl_cross = [...
    1,2; 1,3; 1,4; 1,5; 1,6; 1,7; 1,8; 1,9; 1,10;  1,11; 1,12; ...
         2,3; 2,4; 2,5; 2,6; 2,7; 2,8; 2,9; 2,10;  2,11; 1,12; ...
              3,4; 3,5; 3,6; 3,7; 3,8; 3,9; 3,10;  3,11; 1,12; ...
                   4,5; 4,6; 4,7; 4,8; 4,9; 4,10;  4,11; 1,12; ...
                        5,6; 5,7; 5,8; 5,9; 5,10;  5,11; 1,12; ...
                             6,7; 6,8; 6,9; 6,10;  6,11; 1,12; ...
                                  7,8; 7,9; 7,10;  7,11; 1,12; ...
                                       8,9; 8,10;  8,11; 1,12; ...
                                            9,10;  9,11; 1,12; ...
                                                  10,11; 1,12; ...
                                                         1,12; ...
    ];

  for ii=1:length(rs)
    r = rs{ii};
    rc = [r,'_directbpwf'];

    % run the one which saves the file by hand:
    reduc_final(r,finalopt,0,500)
    cmd=[...
      'reduc_final(r,finalopt,2,200);close all;',...
      'reduc_final(r,finalopt,2,500);close all;',...
      'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
      'reduc_final_chi2(rc,rfcopt,500,2);close all;',...
      'reduc_final_chi2(rc,rfcoptr,200,2);close all;',...
      'reduc_final_chi2(rc,rfcoptr,500,2);close all;',...
      'reduc_final_chi2(rc,rfcoptd,200,2);close all;',...
      'reduc_final_chi2(rc,rfcoptd,500,2);close all;'];
    cmd2=[...
      'reduc_final_chi2(rc,rfcopt_diff,200,2);close all;',...
      'reduc_final_chi2(rc,rfcopt_diff,500,2);close all;',...
      'reduc_final_chi2(rc,rfcoptr_diff,200,2);close all;',...
      'reduc_final_chi2(rc,rfcoptr_diff,500,2);close all;',...
      'reduc_final_chi2(rc,rfcoptd_diff,200,2);close all;',...
      'reduc_final_chi2(rc,rfcoptd_diff,500,2);close all;'];
    for jj = 1:size(pl_cross,1)
      rfcopt.pl_cross=pl_cross(jj,:);
      rfcoptr.pl_cross=pl_cross(jj,:);
      rfcoptd.pl_cross=pl_cross(jj,:);
      rfcopt_diff.pl_cross=pl_cross(jj,:);
      rfcoptr_diff.pl_cross=pl_cross(jj,:);
      rfcoptd_diff.pl_cross=pl_cross(jj,:);
      finalopt.pl_cross=rfcopt.pl_cross;
      farmit('farmfiles/final/',cmd, ...
        'var',{'r','rc','rfcopt','rfcoptr','rfcoptd','finalopt'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',6000, ...
        'maxtime',50, ...
        'submit',0);
      if ~isempty(strfind(r,'jack0'))
        farmit('farmfiles/final/',cmd2, ...
          'var',{'rc','rfcopt_diff','rfcoptr_diff','rfcoptd_diff'}, ...
          'queue','serial_requeue,itc_cluster', ...
          'mem',6000, ...
          'maxtime',50, ...
          'submit',0);
      end
    end
  end

  babysitjobs('final/*.mat','wait5',0,0)

return

function do_566(snF,snI,daughter)
%  do the aps files that feed into the multicomponent analysis:

  apsopt = get_default_apsopt()
  apsopt.random_order = 1;
  apsopt.ukpervolt={[0]};
  apsopt.update=1;
  apsopt.commonmask='gmean';
  apsopt.commonmasksel=[1,2];
  apsopt.purifmatname=get_purification_matrix();
  sel = '[01234]??'

  % 566 file aps
  cmd = ['reduc_makeaps({''',snI,'/',sel,'5_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',{''5_'',''6_''},{''5_'',''6_'',''',snI,''',''',snF,'''}},apsopt)'];

  % each aps takes pretty long cause of the many cross spectra, with 25 jobs you
  % it through in a night:
  njobs = 25
  for ii=1:njobs
    farmit('farmfiles/aps/',cmd, ...
        'var',{'apsopt'}, ...
        'queue','serial_requeue,itc_cluster', ...
        'mem',27000, ...
        'maxtime',240, ...
        'submit',0);
  end

  babysitjobs('aps/*.mat','wait5',0,0)

  % directly make the simset - fine on a login node:
  cmd = ['reduc_makesimset(''',snI,'x',snF,'/???5_',daughter,'_filtp3_weight3_gs_dp1100_jack0_???6_',daughter,'_filtp3_weight3_gs_dp1100_jack0_???6_',daughter,'_filtp3_weight3_gs_dp1100_jack0_matrix_cm.mat'')'];
  eval(cmd)

return


function scalefac = get_dust_scaling()
  % get the dust scaling between the different frequency bands.
  % Dust model is according to this post:
  % http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20150326_DustInputMap/DustInputMap.html
  beta_dust = 1.6;
  T_greybody = 19.6;

  % Assign experiments in order.
  likedata.opt.expt{1}  = 'K95';
  likedata.opt.expt{2}  = 'B2_150';
  likedata.opt.expt{3}  = 'K220';
  likedata.opt.expt{4}  = 'WMAP9_K';
  likedata.opt.expt{5}  = 'P030';
  likedata.opt.expt{6}  = 'WMAP9_Ka';
  likedata.opt.expt{7}  = 'P044';
  likedata.opt.expt{8}  = 'P070';
  likedata.opt.expt{9}  = 'P100';
  likedata.opt.expt{10} = 'P143';
  likedata.opt.expt{11} = 'P217';
  likedata.opt.expt{12} = 'P353';
  % Get instrument bandpasses.
  likedata = like_read_bandpass(likedata);

  % Calculate dust scaling between each frequency and BK 150 GHz (expt 2).
  for ii=1:numel(likedata.bandpass)
    scalefac(ii) = freq_scaling(likedata.bandpass{ii}, beta_dust, T_greybody) / ...
        freq_scaling(likedata.bandpass{2}, beta_dust, T_greybody);
  end
  % Should give result:
  % >>  0.3852    1.0000    X.XXXX    0.0344    0.0498    0.0628    0.1005    0.2251    0.4509    0.9355    3.2507   25.5711

  % The Keck sims have the scale factor already baked in, so don't use the
  % scale factor returned by the multicomponent framework. Instead, reset to
  % unity.
  scalefac(1) = 1; % 95GHz
  scalefac(3) = 1; % 220GHz
return

function uk = get_local_ukpervolt()
  uk = {1,1,1};
return

function pm = get_purification_matrix()
  % Default to using 150GHz matrix
  pm = {'/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/1459/healpix_red_spectrum_lmax700_beamB2bbns_reob1459_aabd_150GHz_QQQUUU_proj.mat'};
  % We need 12 matices overall
  pm = repmat(pm,1,12);
  % The first one is for K2014_95:
  pm{1} = '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/1351/healpix_red_spectrum_lmax700_beamKuber100rev1_reob1351_d_100GHz_QQQUUU_proj.mat';
  % Third one is for K2015_220:
  % Use 150GHz until a new purification matrix is generated.
  pm{3} = pm{2};
return

function bp = get_bpwf()
  % these are those
  % http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20150617_DirectBPWF/
  bp = '1407/xxxx_d_100GHz_filtp3_weight3_gs_dp1100_jack0_xxxx_aabd_150GHz_filtp3_weight3_gs_dp1100_jack0_matrix_cm_directbpwf.mat';
return

function rm = get_reobs_matrix()
  % Only need the BK14_150 reobs matrix:
  rm = '/n/panlfs2/bicep/keck/pipeline/matrixdata/matrices/1459/real_aabd_150GHz_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat';
return

function prep_fullsky_map_farm(prep_opt,run_local)
  % the nlmax field can either be [] or if we know the final lmax set to that (1280)
  % if nlmax is set this speeds up the look up for missing files.
  prep_opt.dry_run=1;
  fn = prep_fullsky_map(prep_opt);
  prep_opt.dry_run=0;

  if ~exist(fn,'file')
    [dum,fnf,dum] = fileparts(fn);
    [farmfile,jobname] = farmfilejobname('BICEP', 'prep_fullsky_map', fnf);
    cmd = 'prep_fullsky_map(prep_opt)';
    if ~run_local
      farmit(farmfile,cmd,...
        'jobname', jobname, ...
        'var',{'prep_opt'},...
        'queue','serial_requeue,itc_cluster', ...
        'maxtime',65,'mem',4000,'submit',0)
    else
      eval(cmd)
    end
  else
    disp(['Skip over existing ',fn])
  end
return

function cmfn = get_planck_filename(mfn,fs)
% this is a magic function to give a filename where to find a planck input map
% yeah yeah, I know...
%
%  FRQ  FUL     DS1     DS2     YR1     YR2     YR3     YR4     H24     H65     H83     H92     H01     NFU     NS1     NS2     NY1     NY2     NY3     NY4     N24     N65     N83     N92     N01
%  030 140811                  140811  140811  140811  140811                                           140811                  140819  140819  140819  140819
%  044 140811                  140811  140811  140811  140811  140811  140811                           140811                  140819  140819  140819  140819
%  070 140811                  140811  140811  140811  140811                   140811  140811  140811  140811                  140819  140819  140819  140819                  140819  140819  140819
%  100 140806  140806  140806  140806  140806                                                           140806  140819  140819  140819  140819
%  143 140806  140806  140806  140806  140806                                                           140806  140819  140819  140819  140819
%  217 140806  140806  140806  140806  140806                                                           140806  140819  140819  140819  140819
%  353 140806  140806  140806  140806  140806                                                           140901  140901  140906  140901  140901
%                                                                                                       -- 101-499 in 140917

  cmfn = strrep(mfn,'XXX',fs);
  if ~isempty(strfind(cmfn,'noise'))
    rlz = str2num(mfn(strfind(mfn,'noise_MC_')+9:strfind(mfn,'noise_MC_')+13));
  end

  if ~isempty(strfind(cmfn,'noise')) & isempty(strfind(cmfn,'HR'))
    if rlz>99
      cmfn = strrep(cmfn,'DDDDDD','140917');
      return
    end
  end

  if str2num(fs)<100
    cmfn = strrep(cmfn,'DX11d','DX11D');
    cmfn = strrep(cmfn,'III','LFI');
    cmfn = strrep(cmfn,'NNNN','1024');
  else
    cmfn = strrep(cmfn,'III','HFI');
    cmfn = strrep(cmfn,'NNNN','2048');
  end

  if ~isempty(strfind(cmfn,'HR'))
    if isempty(strfind(cmfn,'noise')) | rlz<100
      cmfn = strrep(cmfn,'DDDDDD','140912');
    else
      cmfn = strrep(cmfn,'DDDDDD','141024');
    end
  end

  % the LFI
  if str2num(fs)<100 && isempty(strfind(cmfn,'noise'))
    cmfn = strrep(cmfn,'DDDDDD','140811');
  elseif str2num(fs)<100 && ~isempty(strfind(cmfn,'noise')) && ~isempty(strfind(cmfn,'FULL'))
    cmfn = strrep(cmfn,'DDDDDD','140811');
  elseif str2num(fs)<100 && ~isempty(strfind(cmfn,'noise')) && isempty(strfind(cmfn,'FULL'))
    cmfn = strrep(cmfn,'DDDDDD','140819');

  % the HFI besides 353 noise
  elseif str2num(fs)>=100 && isempty(strfind(cmfn,'noise'))
    cmfn = strrep(cmfn,'DDDDDD','140806');
  elseif str2num(fs)>=100 && str2num(fs)~=353 && ~isempty(strfind(cmfn,'noise')) && ~isempty(strfind(cmfn,'FULL'))
    cmfn = strrep(cmfn,'DDDDDD','140806');
  elseif str2num(fs)>=100 && str2num(fs)~=353 && ~isempty(strfind(cmfn,'noise')) && isempty(strfind(cmfn,'FULL'))
    cmfn = strrep(cmfn,'DDDDDD','140819');

  % 353 noise - might need adjustment if DS2 is fixed
  elseif str2num(fs)==353 && ~isempty(strfind(cmfn,'noise')) && isempty(strfind(cmfn,'DS2'))
    cmfn = strrep(cmfn,'DDDDDD','140901');
  % change this portion to where the fixed DS2 353 noise is:
  elseif str2num(fs)==353 && ~isempty(strfind(cmfn,'noise')) && ~isempty(strfind(cmfn,'DS2'))
    cmfn = strrep(cmfn,'DDDDDD','140906');
  end

return
