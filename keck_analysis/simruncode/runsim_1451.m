%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Real and sim maps with Planck deprojection map and planck noise, planck cosmo parameters, constrained sim rlz
% this should become the B2 major sim set
% the runsim_ function makes
%  - real pairmaps/maps
%  - sim pairmaps/maps
%  - the sim map combinations
%  - the traditional noise sim
%  - the signflip noise sim
% all the code for post-processing of these maps (aps, simsets and final etc) is found down in the bottom after this function
% there are also examples how to runsim_1451 in practice
function runsim_1451(nbase,doreal,donoisesim,dorealcoadd,donoise,dosim,docoadd,rlz,docombmaps,queue,farmdps,usecompiled,simtypes,submit)
%  function runsim_1451(nbase,doreal,donoisesim,dorealcoadd,donoise,dosim,docoadd,rlz,docombmaps,queue,farmdps)
%  
%  doreal -> make real pairmaps
%  donoisesim-> make traditional noise sim (just few rlz)
%  dorealcoadd-> coadd real pair maps
%  donoise-> make signflip noise sim
%  dosim-> make flavour subset sims
%  docoadd-> coadd traditional noise sims and flavour subsetsims
%  rlz -> i.e. 1:10
%  docombmaps: runsim_1451(1451,0,0,0,0,0,0,rlz,1,0) 
%  queue: 0 LSF, 1 serial_requeue (default), 2 general 
%  farmdps: farm deprojections seperately (0 default)
%  usecompiled: 1 default
%  simtypes: [2,4,5] default
%  submit: 1 default, if 0 just create farmfiles without submission
%  
%  funny order? the donoise and dosim options need the real coadded maps!
%  
%  run the coadd command twice as it is using a split coadd in the first round
%  then a coadd_coadds in the second
%  
%  before running on many realizations, copy the input_maps to panelfs
%  

% check to not mess it up:
if nbase~=1351 & nbase~=1451
  error('This script is not meant for this base number.')
end
if nbase==1451 & ~strcmp(get_experiment_name(),'bicep2')
  error('Are you in the right folder for Bicep2 sims?')
end
if nbase==1351 & ~strcmp(get_experiment_name(),'keck')
  error('Are you in the right folder for Keck sims?')
end

% defaults: do nothing at all
if(~exist('doreal','var'))
  doreal=0;
end
if(~exist('donoisesim','var'))
  donoisesim=0;
end
if(~exist('dorealcoadd','var'))
  dorealcoadd=0;
end
if(~exist('donoise','var'))
  donoise=0;
end
if(~exist('dosim','var'))
  dosim=0;
end
if(~exist('docoadd','var'))
  docoadd=0;
end
if(~exist('rlz','var'))
  rlz=1;
end
if(~exist('docombmaps','var'))
  docombmaps=0;
end
if(~exist('queue','var'))
  queue=1;
end
if(~exist('farmdps','var'))
  farmdps=0;
end
if(~exist('usecompiled','var'))
  % choose whether to use normal (.m) or compiled code
  usecompiled=0;
end
if(~exist('simtypes','var'))
  simtypes=[2,4,5];
end
if(~exist('submit','var'))
  submit=true;
end

% setup options
dotagsfrommap=1;

%memory usage differences between B2 and keck
if nbase==1351
  memreal=20000;
  memcoadd = 20000;
  memcoaddcoadd = 24000;
  memcoaddcoadddp = 30000;
elseif nbase==1451
  memreal=10000;
  memcoadd = 16000;
  memcoaddcoadd = 12000;
  memcoaddcoadddp = 20000;
end
maxtime=[]
if docoadd | dorealcoadd
  maxtime=8*60;
  if farmdps
    maxtime=60;
  end
end
if dosim
  maxtime=60;
end


% pick appropiate queues:
use_serial_requeue=0;
use_general=0;
if (queue==1) use_serial_requeue=1; end
if (queue==2) use_general=1; end

queue_real='normal_serial'; % real pairmaps queue
queue_sim='short_serial'; % queue for runsim jobs
queue_coadd='normal_serial'; % queue for coadd jobs
queue_coadd_coadd='normal_serial'; % queue for coadd jobs
if farmdps
  queue_coadd='short_serial'; % queue for coadd jobs
  queue_coadd_coadd='short_serial'; % queue for coadd jobs
end
queue_other='short_serial';
if use_serial_requeue
  display('we use serial_requeue')
  slurm_queue='serial_requeue';
  queue_real=slurm_queue;
  queue_sim=slurm_queue;
  queue_coadd=slurm_queue;
  queue_coadd_coadd=slurm_queue;
  queue_other=slurm_queue;
elseif use_general
  display('we use general')
  slurm_queue='general';
  queue_real=slurm_queue;
  queue_sim=slurm_queue;
  queue_coadd=slurm_queue;
  queue_coadd_coadd=slurm_queue;
  queue_other=slurm_queue;
end

% global options
om=1;  % supplies 'OnlyMissing' option for both runsim and coadds

ntpg=7; % NTagsPerJob real data

% simulation options
rlzc=10; % rlzchunksize for runsim
tagc=1; % tagchunksize for runsim

% coadd options
js=0 % option for FarmJacksSeparately for coadd
disp(['farming dp seperately=' num2str(farmdps)])

% which deprojection we want to do:
deprojs=[0,0,0,0;  % No deprojection
         1,0,0,0;  % relgain
         0,1,0,0;  % A/B
         1,1,0,0;  % relgain + A/B offsets   
         1,1,1,0;  % beamwidth + A/B offsets + relgain
         1,1,0,1;  % ellipticity + A/B offsets + relgain
         1,1,1,1]; % all

% add real map specials:
if dorealcoadd
  deprojs=[deprojs;
           1,1,0,2;  % measured ellipticity + A/B offsets + relgain
           1,2,0,2; % measured ellipticity + measured A/B offsets + relgain
           2,1,0,2];  % measured ellipticity + A/B offsets + measured relgain
%    deprojs=[1,1,0,2;  % measured ellipticity + A/B offsets + relgain
%             1,2,0,2; % measured ellipticity + measured A/B offsets + relgain
%             2,1,0,2];
end

if dorealcoadd
  rlz=[];
end
rlz

% do want to wait for creating the tag list if it is not needed
dotags = doreal|donoisesim|dorealcoadd|donoise|dosim|docoadd;
if dotags
  % do the tags
  % switch this to grabbing it from the real data asa available
% this does not work in the Tier2 test since it uses data/real/...
%    if nbase==1451 tags=get_tags('allcmb','has_tod'); end % 17097, B2
%    if nbase==1351 tags=get_tags('cmb2012','has_tod'); end % Keck

  if dotagsfrommap
    x = load(['maps/',num2str(nbase),'/real_a_filtp3_weight3_gs_dp0000_jack0.mat']);
    tags = x.coaddopt.tags;
    [tagsublist,mtl]=get_tag_sublist(x.coaddopt);
    clear x; 
  end
end

clear simopt
simopt.siginterp='taylor';
%this is 'aux_data/beams/beams_cmb_3yr.csv' not 'aux_data/beams/beams_cmb_3yr_planck.csv'
simopt.beamcen='obs'; 
simopt.diffpoint='obs';
simopt.chi='obs';
simopt.epsilon='obs';
%3 yrs of B2 vs planck: http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130612_abscal_4/
% 3150 is the round number recommendation arising from this posting
ukpervolt=get_ukpervolt();
simopt.ukpervolt=ukpervolt;
simopt.coord='C';
simopt.force_ab_int_from_common_pix=false;
simopt.update=1;

clear mapopt
mapopt.beamcen='obs';
mapopt.chi='obs';
mapopt.epsilon='obs';
mapopt.acpack=0;
mapopt.gs=1;
mapopt.filt='p3';
mapopt.deproj=true;

clear coaddopt
%  http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130628_channel_cuts_3/
chflags=get_default_chflags();
coaddopt.chflags=chflags;
coaddopt.daughter='a';
coaddopt.deproj_timescale='by_phase';
coaddopt.filt='p3';
coaddopt.gs=1;
coaddopt.jacktype='0123456789abcde';
%change to coaddtype=1 for keck
if nbase==1351
  coaddopt.coaddtype=1;
end

% for a special sim/real map:
%  mapopt.proj='arc';
%  coaddopt.proj='arc';

% we are going do coadding over subsets, hence specifiy
% the 1st/2nd half split explicity with the logic from
% reduc_coaddpairmaps. The spilt is at the point of even
% accumulated pair diff weight in the first and second half
% found by additional analysis
if nbase==1451 coaddopt.temporaljack_splitdate='20111101'; end
if nbase==1351 coaddopt.temporaljack_splitdate='20120719'; end

%%%%%%%%%%%%%%%%%%%%%%% let's work
if doreal
  mapopt.sernum=sprintf('%04dreal',nbase);
  mapopt.deproj_map='input_maps/planck/planck_derivs_nopix/synfast_deproj_143_nominal_B2.fits';
  farm_makepairmaps(tags,mapopt,'JobLimit',600,'NTagsPerJob',ntpg,'FarmJobs',1, ...
                    'OnlyMissing',1,'Queue',queue_real,'MemRequire',memreal);
end

%  types
%  xxx1 - reserved for traditional noise sims
%  xxx2 - LCDM unlensed scalars with diff point on
%  xxx4 - r=0.1 tensors with E forced to zero (and T switched off at makesim stage)
%  xxx5 - LCDM lensed scalars with diff point on
%  xxx6 - sign flip noise
%  xxx8 - reserved for alternate artificial B-mode input spectrum

%  combinations:
%  xxx3 - reserved for the map sum of 1+2 
%  xxx7 - the map sum 5+1 or 5+6 
%  xxx9 - the sum of 4+5+6 
if dosim
  try
    par_make_simrunfiles(tags,simopt,nbase);
  catch
    display('simrunfile exists, not remaking it...')
  end
  
  mapopt.deproj_map='input_maps/camb_planck2013_r0/map_unlens_n0512_rxxxx_sB2bbns_constrained.fits';
 
  type = 2;
  sigmapfilename='input_maps/camb_planck2013_r0/map_unlens_n2048_rxxxx_sB2bbns_constrained.fits';
  runsim(nbase,mapopt,'normal','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime);

  type = 4;
  sigmapfilename='input_maps/camb_planck2013_r0p1_noE/map_unlens_n2048_rxxxx_sB2bbns_constrained.fits';
  runsim(nbase,mapopt,'onlypol','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime);
  
  type = 5;
  %deproj template should be lensed for type5
  mapopt.deproj_map= 'input_maps/camb_planck2013_r0/map_lensed_n0512_rxxxx_sB2bbns_constrained.fits';
  sigmapfilename = 'input_maps/camb_planck2013_r0/map_lensed_n2048_rxxxx_sB2bbns_constrained.fits';
  runsim(nbase,mapopt,'normal','none',type,sigmapfilename,rlz,mtl,true,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime);

end

if donoisesim
  try
    par_make_simrunfiles(tags,simopt,nbase);
  catch
    display('simrunfile exists, not remaking it...')
  end

  mapopt.deproj_map= 'input_maps/camb_planck2013_r0/map_unlens_n0512_rxxxx_sB2bbns_dPl143.fits'; % should this be lensed?
  
  type = 1;
  sigmapfilename='input_maps/camb_planck2013_r0/map_unlens_n2048_rxxxx_sB2bbns_dNoNoi.fits';
  runsim(nbase,mapopt,'none','data',type,sigmapfilename,rlz,tags,false,rlzc,om,800,0,[],1,queue_sim,0,[],tagc,usecompiled,maxtime);
end

if docoadd | dorealcoadd
  %loop over sim types, will jump out for real
  for k=simtypes
    k

    coaddopt.sernum=sprintf('%04dxxx%01d',nbase,k);
    if dorealcoadd
      coaddopt.sernum(5:8)='real';
      coaddopt.save_cuts=true;
    else
      coaddopt.save_cuts=false;
    end
    % no tag substitution for real or traditional noise sim:
    if ~dorealcoadd & k~=1
      coaddopt.tagsublist=tagsublist;
    end
    if farmdps
      %loop over the deprojs
      for jj = 1:size(deprojs,1)
        coaddopt.deproj=deprojs(jj,:);
        farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',70,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
      end
    else
      coaddopt.deproj=deprojs;
      farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                              'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                              'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadddp,...
                              'SplitSubmission',70,'UseCompiled',usecompiled,...
                              'maxtime',maxtime,'submit',submit);
    end
    %no loop for real data
    if (dorealcoadd) break; end 
  end
end

% can be done after real data coadd
if donoise
  % sign_flip noise, see: http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20130614_sign_flip_seq/
  
  % first load in real data to fetch the jackmasks
  % we do this in before the loop over the realizations, to avoid the io
  % all the non-temporal jacks get the same sign_flip sequence:
  x= containers.Map();
  x('024689abce')=load(['maps/' num2str(nbase) '/real_' coaddopt.daughter '_filtp3_weight3_gs_dp1100_jack2_s']);
  % all the temporal jacks fetch the individual jackmasks:
  for xx=['1','3','5','7','d']
    disp(['loading jack' xx]);
    x(xx)=load(['maps/' num2str(nbase) '/real_' coaddopt.daughter '_filtp3_weight3_gs_dp1100_jack',xx,'_s']);
  end
  
  % the real data is coming with a large cut stucture, avoid saving this over and over
  % for the noise realizations:
  coaddopt.save_cuts=false;
  coaddopt.sernum=[num2str(nbase) 'real'];
  
  coaddopt.sign_flip_type='by_scanset';
  for ii=rlz
    % fix the random numbers
    stream0 = RandStream('mt19937ar','Seed',ii)
    RandStream.setDefaultStream(stream0);
    
    % loop over the different jacks
    for xx={'024689abce','1','3','5','7','d'}

      % fetch the sign_flip sequence...
      coaddopt.sign_flip_seq=get_sign_flip_seq(x(char(xx)).coaddopt);
      % ...specific for each jack
      coaddopt.jacktype=char(xx);
      % assign a realization number:
      coaddopt.sign_flip_rlz=ii;
      if farmdps
        for jj = 1:size(deprojs,1)
          coaddopt.deproj=deprojs(jj,:);
          farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',[],...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',36,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
        end
      else
        coaddopt.deproj=deprojs;
        farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',[],...
                          'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
                          'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadd,...
                          'SplitSubmission',36,'UseCompiled',usecompiled,...
                          'maxtime',maxtime,'submit',submit);
      end
    end
  end
end

% Combine comaps
%  xxx3 - reserved for the map sum of 1+2 
%  xxx7 - the map sum 1+5 or 6+5 
%  xxx9 - the sum of 6+5+4 = 7+4
if nbase==1351; ct='1'; end; 
if nbase==1451; ct=''; end;
if docombmaps

  % split in two loops so that the maptype7 is present when 9 is done
  % carfull here, the map that contains the noise must be first!
  for crlz = rlz
  
    % this is not really part of 1351/1451 since type1 is not created...
    % cmd = ['reduc_combcomap(''',num2str(nbase),'/xxx1_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',2,3,1)'];
    % if nbase==1351  %no normal sim realizations....
    %   cmd = ['reduc_combcomap(''',num2str(nbase),'/xxx6_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',2,3,1)'];
    % end
    % cmd = strrep(cmd,'xxx',sprintf('%03d',crlz));
    % farmit('farmfiles/',cmd,'queue',queue_other,'mem',16000);
    
    cmd = ['reduc_combcomap(''',num2str(nbase),'/xxx6_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',5,7,1)'];
    cmd = strrep(cmd,'xxx',sprintf('%03d',crlz));
    farmit('farmfiles/comb/',cmd,'queue',queue_other,'mem',16000,'maxtime',60);
  end
  
  for crlz = rlz
    cmd = ['reduc_combcomap(''',num2str(nbase),'/xxx7_',coaddopt.daughter,'_filtp3_weight3_gs_dp????_jack?' ct '.mat'',4,9,1)'];
    cmd = strrep(cmd,'xxx',sprintf('%03d',crlz));
    farmit('farmfiles/comb/',cmd,'queue',queue_other,'mem',16000,'maxtime',60);   
  end

end

return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bunch of code for stuff that combines all the realiations created above, usually run interactively on legacy:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% these are used all over the following loops
% just drag them into the workspace:
types= '245679';
deprojs={'0000','1000','0100','1100','1110','1101','1111'};
deprojsreal={'2102','1102','1202','0000','1000','0100','1100','1110','1101','1111'};
jacks = get_default_coaddopt();
jacks = jacks.jacktype;
estimators = {'','_pureB'};
estimators = {'','_pureB','_overrx','_pureB_overrx'};
purebs = {'normal','kendrick'};
daughter = 'a';
nbase =1451; % 1451 B2, 1351 Keck
queue_small='short_serial';
queue_other='short_serial';
apsopt.update = 1;
apsopt.ukpervolt = [3150,3400];
apsopt.save_coaddopts=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% using the runsim script, non-compiled
rlz=1:10;
% sim pairmaps:
runsim_1451(num2str(nbase),0,0,0,0,1,0,rlz,0,1,0,0)
% sim coadds:
runsim_1451(num2str(nbase),0,0,0,0,0,1,rlz,0,1,0,0)
% signflip noise:
runsim_1451(num2str(nbase),0,0,0,1,0,0,rlz,0,1,0,0)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cross spectra for real, includes bpwf and experiment jack
% the jack0 run for many hours since it will calculate the bpwf as well.
apsopt.polrot=[-1.1,-0.5];
apsopt.makebpwf=0;
for deproj = deprojsreal
  for coaddrx=[0,1]
    for pure_b = purebs;
      for jack=jacks
        if strcmp(jack,'0')
          apsopt.makebpwf=1;
          maxtime=[];
          queue_other='itc_normal'; %can you commit here!?
        else
          apsopt.makebpwf=0;
          maxtime=[];
          queue_other='short_serial';
        end
        
        apsopt.pure_b = pure_b{:};
        apsopt.coaddrx=coaddrx;
        apsopt.howtojack='dim2';

        cmd = ['reduc_makeaps({''1451/real_a_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,'.mat'',''1451'',''1351'',''.mat'',''1.mat''},apsopt)'];
        farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_other,'mem',15000,'maxtime',maxtime);
        
        if strcmp(jack,'0')
          apsopt.makebpwf=0;
          apsopt.howtojack='dim1'; % experiment jack
          farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_other,'mem',15000,'maxtime',maxtime);
        end
        
      end
    end
  end
end

% cross spectra for the sims
apsopt.makebpwf=0;
apsopt.polrot=[];
for type = types
  for coaddrx=[0,1]
    for pure_b = purebs;
      for jack=jacks
        apsopt.coaddrx=coaddrx;
        apsopt.pure_b = pure_b{:};
        
        cmd = ['reduc_makeaps({''1451/[12]??',type,'_a_filtp3','*jack',jack,'.mat'',''1451'',''1351'',''.mat'',''1.mat''},apsopt)'];
        apsopt.howtojack='dim2';
        while 1
          try
            farmit('farmfiles/aps/',cmd,'var','apsopt','queue',queue_other,'mem',10000,'maxtime',[]);
            break
          catch
            display('oh no')
          end
        end
        
        if strcmp(jack,'0')
          apsopt.howtojack='dim1'; % experiment jack
          while 1
            try
              farmit('farmfiles/aps/',cmd,'var','apsopt','queue',queue_other,'mem',10000,'maxtime',[]);
              break
            catch
              display('oh no')
            end
          end
        end
        
      end    
    end
  end
end

% simsets cross:
jacks=[jacks,'f'];
for jack=jacks
  for type = types
    for deproj = deprojs
      for estimator=estimators       
        cmd = ['reduc_makesimset(''1451x1351/0??',type,'_',daughter,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,'_0??',type,'_',daughter,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,'1',estimator{:},'.mat'')'];
        farmit('farmfiles/simset/',cmd,'queue',queue_other,'mem',10000,'maxtime',60);
      end
    end
  end
end

% reduc_final and reduc_final_chi2 cross:
%  pl_cross selects the rx combination to plot
pl_cross = [1,2;1,3;1,4;1,5;1,6];
rfcopt.dia = 0;
rfcopt.doblind = 0;
jacks=[jacks,'f'];
system('bgadd /b2_final')
system('bgmod -L 150 /b2_final')
for jack=jacks
  for deproj = deprojsreal
    for estimator=estimators
      deprojS=deproj{:};
      % when subtraction instead of regression in real data, 
      % use the undeprojected sim but just in the case that the deprojected
      % systematic was not simulated (diff ell yes but not diff pointing):
      if strcmp(deproj{:},'1202') continue; end
      if strcmp(deproj{:},'1102') deprojS='1100'; end
      if strcmp(deproj{:},'2102') deprojS='0100'; end
     
      r = sprintf(['1451x1351/real_a_filtp3_weight3_gs_dpDDDD_jackJ_real_a_filtp3_weight3_gs_dpDDDD_jackJ1BB']);
      n = sprintf(['1451x1351/xxx6_a_filtp3_weight3_gs_dpDDDD_jackJ_0016_a_filtp3_weight3_gs_dpDDDD_jackJ1BB']); %signflip
      s = sprintf(['1451x1351/xxx2_a_filtp3_weight3_gs_dpDDDD_jack0_0012_a_filtp3_weight3_gs_dpDDDD_jack01BB']); %yes, always jack0, unlensed
      sn= sprintf(['1451x1351/xxx7_a_filtp3_weight3_gs_dpDDDD_jackJ_0017_a_filtp3_weight3_gs_dpDDDD_jackJ1BB']); %lensed
      b = sprintf(['1451x1351/xxx4_a_filtp3_weight3_gs_dpDDDD_jack0_0014_a_filtp3_weight3_gs_dpDDDD_jack01BB']); %yes, always jack0

      r = strrep(r,'DDDD',deproj{:}); r = strrep(r,'BB',estimator{:}); r = strrep(r,'J',jack);
      n = strrep(n,'DDDD',deprojS); n = strrep(n,'BB',estimator{:}); n = strrep(n,'J',jack);
      s = strrep(s,'DDDD',deprojS); s = strrep(s,'BB',estimator{:}); s = strrep(s,'J',jack);
      sn= strrep(sn,'DDDD',deprojS); sn= strrep(sn,'BB',estimator{:}); sn= strrep(sn,'J',jack);
      b = strrep(b,'DDDD',deprojS); b = strrep(b,'BB',estimator{:}); b = strrep(b,'J',jack);
           
      cmd=['reduc_final(r,n,s,sn,b,2,200,pl_c);close all;',...
       'reduc_final(r,n,s,sn,b,2,500,pl_c);close all;',...
       'reduc_final(r,n,s,sn,b,0,500,pl_c);close all;',...
       'reduc_final_chi2(r,rfcopt,200,2);close all;',...
       'reduc_final_chi2(r,rfcopt,500,2);close all;'];

      rfcopt.pl_cross=pl_cross(1,:);
      pl_c=rfcopt.pl_cross;

      % if jack0 then also do the spectral jack:
      diffspecs=0;
      if (jack=='0') diffspecs=[0,1]; end
        
      for diffspec = diffspecs
        rfcopt.diffspec = diffspec;
        farmit('farmfiles/final/',cmd,'var',{'r','n','s','sn','b','rfcopt','pl_c'},'queue',queue_other,'mem',16000,'maxtime',60,'group','/b2_final');
        
        % when not coadded over Keck revceiver also do the other plot combinations:
        % B2xRx2, B2xRx3 etc
        if isempty(strfind(estimator{:},'overrx'))
          for jj = 2:size(pl_cross,1)
            rfcopt.pl_cross=pl_cross(jj,:);
            pl_c=rfcopt.pl_cross;
            farmit('farmfiles/final/',cmd,'var',{'r','n','s','sn','b','rfcopt','pl_c'},'queue',queue_other,'mem',16000,'maxtime',60,'group','/b2_final');
          end
        end
        
      end
    end
  end
end

% reduc_plotcomap_pager xspectrum pager
% 500000+ plots
for deproj = deprojs
  for jack=jacks
    for coaddrx=[0,1]
      for fcs=[0,1]
        
        if coaddrx==0 
          lastp = '_B_noi_Rx4.png';
        else
          lastp = '_B_noi_Keck.png'; 
        end
        if fcs lastp = strrep(lastp,'.png','_fcs.png'); end
        
        apsopt.coaddrx=coaddrx;
        map='1451/real_a_filtp3_weight3_gs_dpDDDD_jackJJ.mat';
        map=strrep(map,'JJ',jack);
        map=strrep(map,'DDDD',deproj{:});
        
        map2=strrep(map,'1351/','');
        map2=strrep(map2,'.mat','1.mat');
        
        cmd=['reduc_plotcomap_pager({map,''1451'',''1351'',''.mat'',''1.mat''},apsopt,',num2str(fcs),',1,1,0,1,0,0,1,'',1);'];
        
        % for the real data
        if ~exist(['reduc_plotcomap_pager/1451x1351/' map(6:end-4) '_' map2(6:end-4) lastp],'file')       
          farmit('farmfiles/rpp/',cmd,'var',{'map','apsopt'},'queue',queue_other,'mem',15000);
        end
        
        % loop over the sim types
        for type = types
          cmap=strrep(map,'real',['001',type]);         
          cmap2=strrep(map2,'real',['001',type]);
          cmd=['reduc_plotcomap_pager({cmap,''1451'',''1351'',''.mat'',''1.mat''},apsopt,',num2str(fcs),',1,1,0,1,0,0,1,'',1);'];
          if ~exist(['reduc_plotcomap_pager/1451x1351/' cmap(6:end-4) '_' cmap2(6:end-4) lastp])       
            farmit('farmfiles/rpp/',cmd,'var',{'cmap','apsopt'},'queue',queue_other,'mem',15000);
          end
        end
        
      end
    end
  end
end  

% reduc_plotcomap_pager auto pager
%  function reduc_plotcomap_pager(mapname,apsopt,fixc,pad,realimag,dotemplates,doTQ,doHJ,doSL,doCross,doSub,om)
%  system_safe('rm reduc_plotcomap_pager/1351?sub/*Rx[1234]*.png')
subs = {'psub','gsub','dsub',''}
ct='';
crx = 0;
if nbase==1351 ct='1'; apsopt.ukpervolt = [3400]; crx = [1,0]; end
for deproj = deprojsreal
  for jack=jacks
    for coaddrx=crx
      for fcs=[0,1]
        for sub = subs

          lastp = '_B_noi.png';
          if nbase==1351 %Keck
            if coaddrx==0
              lastp = '_B_noi_Rx4.png';
            else
              lastp = '_B_noi_Keck.png'; 
            end
          end
          if fcs lastp = strrep(lastp,'.png','_fcs.png'); end
          
          apsopt.coaddrx=coaddrx;
          map=[num2str(nbase),'/real_a_filtp3_weight3_gs_dpDDDD_jackJJ.mat'];
          % map=[num2str(nbase),'/2015_a_filtp3_weight3_gs_dpDDDD_jackJJ.mat'];
          % map=[num2str(nbase),'/real_a_filtp3_weight3_gs_arc_dpDDDD_jackJJ.mat'];
          % map=[num2str(nbase),'/2015_a_filtp3_weight3_gs_arc_dpDDDD_jackJJ.mat'];
          map=strrep(map,'JJ',[jack,ct]);
          map=strrep(map,'DDDD',deproj{:});
          
          cmd=['reduc_plotcomap_pager(map,apsopt,',num2str(fcs),',1,1,0,1,0,0,0,''',sub{:},''',1);'];

          if ~exist(['reduc_plotcomap_pager/1451',sub{:},'/' map(6:end-4) lastp],'file')
            farmit('farmfiles/rpp/',cmd,'var',{'map','apsopt'},'queue',queue_other,'mem',15000);
          end
        end
      end
    end
  end
end  


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helpfull to set up the sims:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% fetch all the tag brower plots for the flavour sublist
% (then fetch for instance with wget...)
x = load(['maps/',num2str(nbase),'/real_a_filtp3_weight3_gs_dp0000_jack0.mat']);
tags = x.coaddopt.tags;
[tagsublist,mtl]=get_tag_sublist(x.coaddopt);
for ii = 1:length(mtl)
  if (nbase==1451) display(['http://bicep.rc.fas.harvard.edu/bicep2/reducplots/',mtl{ii}(1:6),'//',mtl{ii},'_001_round2.png']); end
  if (nbase==1351) display(['http://bicep.rc.fas.harvard.edu/keck/reducplots/',mtl{ii}(1:6),'//',mtl{ii},'_001_round2.png']); end
end

% fetch all the tods for the flavour tag subset sim
x = load(['maps/',num2str(nbase),'/real_a_filtp3_weight3_gs_dp0000_jack0.mat']);
tags = x.coaddopt.tags;
[tagsublist,mtl]=get_tag_sublist(x.coaddopt);
extensions={'_caldat.mat','_cutparams.mat','_tod_uncal.mat','_calval.mat','_tod.mat'};
for ii = 1:length(mtl)
  for extension = extensions
    display([mtl{ii}(1:6),'/',mtl{ii},extension{:}])
  end
end




