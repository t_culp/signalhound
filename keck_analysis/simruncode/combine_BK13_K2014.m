function combine_BK13_K2014()
%  combine_BK13_K2014()
%  this makes the 1459/????_aabd_ maps (BK14)
%  those are made into aps and final files in make_aps_final_BK14

queue='serial_requeue,itc_cluster';

deprojsreal={'1100','1102'};
jacks = '0345';

% this the latest addition to the set: K2014
nbase1 = 1351
daughter1 = 'd'

% this is the BK13, the previous map deepest combination
nbaseP = 1456
daughterP = 'aab'

% this is the BK14 = BK13 + K2014
nbase2 = 1459
daughter2 = 'aabd'

%  real
for deproj = deprojsreal
  for j=jacks
    
    mBK = [num2str(nbaseP),'/real_',daughterP,'_filtp3_weight3_gs_dp',deproj{:},'_jack',j,'.mat']
    mK  = [num2str(nbase1),'/real_',daughter1,'_filtp3_weight3_gs_dp',deproj{:},'_jack',j,'1.mat']
    mS  = [num2str(nbase2),'/real_',daughter2,'_filtp3_weight3_gs_dp',deproj{:},'_jack',j,'.mat']
    if ~exist(['maps/',mS],'file')
      if exist(['maps/',mBK]) & exist(['maps/',mK])
      farmit('farmfiles/coaddcoadd/','load_and_combine(mBK,mK,mS)','func',{@load_and_combine,@minimize_coaddopt},'var',{'mBK','mK','mS'},'queue',queue,'mem',10000,'maxtime',15,'submit',0)    
      end
    end
  end
end
    
%  sims    
rlzs = 1:499;
for j=jacks
  for rlz = rlzs
    for t = '23456'
      mBK  = sprintf([num2str(nbaseP),'/%03d',t,'_',daughterP,'_filtp3_weight3_gs_dp1100_jack',j,'.mat'],rlz);
      mK   = sprintf([num2str(nbase1),'/%03d',t,'_',daughter1,'_filtp3_weight3_gs_dp1100_jack',j,'1.mat'],rlz);
      mS   = sprintf([num2str(nbase2),'/%03d',t,'_',daughter2,'_filtp3_weight3_gs_dp1100_jack',j,'.mat'],rlz);
      if ~exist(['maps/',mS],'file')
        if exist(['maps/',mBK]) & exist(['maps/',mK])
        farmit('farmfiles/coaddcoadd/','load_and_combine(mBK,mK,mS)','func',{@load_and_combine,@minimize_coaddopt},'var',{'mBK','mK','mS'},'queue',queue,'mem',2000,'maxtime',15,'submit',0)
        end
      end
    end
  end
end
%babysitjobs('farmfiles/coaddcoadd/*','wait5')

keyboard

% combine signal and noise
for rlz = rlzs
  for j = jacks
  cmd1 = ['reduc_combcomap(''',num2str(nbase2, '%.4d'),'/xxx6_',daughter2,'_filtp3_weight3_gs_dp????_jack' j '.mat'',5,7,1)'];
  cmd1 = strrep(cmd1,'xxx',sprintf('%03d',rlz));
  
  cmd2 = ['reduc_combcomap(''',num2str(nbase2, '%.4d'),'/xxx7_',daughter2,'_filtp3_weight3_gs_dp????_jack' j '.mat'',4,9,1)'];
  cmd2 = strrep(cmd2,'xxx',sprintf('%03d',rlz));
  
  cmd3 = ['reduc_combcomap(''',num2str(nbase2, '%.4d'),'/xxx7_',daughter2,'_filtp3_weight3_gs_dp????_jack' j '.mat'',3,8,1)'];
  cmd3 = strrep(cmd3,'xxx',sprintf('%03d',rlz));
  
  cmd = [cmd1,';',cmd2,';',cmd3];
  
  farmit('farmfiles/comb/',cmd,'queue',queue,'mem',2000,'maxtime',20,'submit',0);
  end
end
babysitjobs('farmfiles/comb/*','wait5')

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function load_and_combine(mBK,mK,mS)
%  this needs to be adjusted year to year
  mBK
  mK
  mS

  mapBK  = load(['maps/',mBK]);
  mapK   = load(['maps/',mK]);

  % make sure you got the right ukpervolt for that year and apply it:
  mapK.coaddopt.ukpv_applied = get_ukpervolt('2014');
  mapK.ac=cal_coadd_ac(mapK.ac,mapK.coaddopt.ukpv_applied);
  mapK.ac=coadd_ac_overfreq(mapK.ac,mapK.coaddopt);
  
  mapK.ac = rmfield(mapK.ac,'wsd');  
  mapK.ac = rmfield(mapK.ac,'wcd');
  actemp=struct_merge(mapK.ac,mapBK.ac);
  ac(1,:) = actemp(1,:);
  % this is handpicking the right freqs to make the deepest map
  % at each frequency:
  ac(2,:)=coadd_ac_overrx(actemp([2,3],:));

  mapBK.coaddopt = minimize_coaddopt(mapBK.coaddopt);
  mapK.coaddopt  = minimize_coaddopt(mapK.coaddopt);
  
  mapK.coaddopt.mapname = mK;
  mapBK.coaddopt.mapname = mBK;
  
  coaddopt={mapBK.coaddopt;mapK.coaddopt};
  m = mapBK.m;
  
  saveandtest(['maps/',mS],'ac','coaddopt','m','-v7.3');
return

function coaddopt = minimize_coaddopt(coaddopt)
  try coaddopt = rmfield(coaddopt,'b'); end
  try coaddopt = rmfield(coaddopt,'bi'); end
  try coaddopt = rmfield(coaddopt,'bw'); end
  try coaddopt = rmfield(coaddopt,'hsmax'); end
  try coaddopt = rmfield(coaddopt,'whist'); end
  try coaddopt = rmfield(coaddopt,'devhist'); end
  try coaddopt = rmfield(coaddopt,'traj'); end
  try coaddopt = rmfield(coaddopt,'c'); end
  try coaddopt = rmfield(coaddopt,'jackmask'); end
return
