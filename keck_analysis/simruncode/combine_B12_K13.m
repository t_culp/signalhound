function combine_B12_K13()
%  combine_B12_K13()
%  this makes the 1456/????_aab_ maps (BK13) for the mainline deprojection options
%  and jacks 0345 which were used in the Planck Joint analsis.
%  
% The code below was not used for the 1456/????_aab_ maps created till Jul 2015.
% Previously there was some private code making those, that, however, had in issue
% with the format of the coaddopt. The code below is tested to produce identical maps as before
% but fixes the coaddopt issue. This code below should be used for the next
% regeneration of the maps.
%  
% see make_aps_final_BK14 to combine to the BK14 map

% the real maps 1351 dp1102
for j='0345'
  mapB12 = (['0751/real_a_filtp3_weight3_gs_dp1102_jack',j,'.mat']);
  mapK13 = (['1351/real_ab_filtp3_weight3_gs_dp1102_jack',j,'.mat']);
  mapBK13= (['1456/real_aab_filtp3_weight3_gs_dp1102_jack',j,'.mat']);
  % load_and_combine(mapB12,mapK13,mapBK13)
  farmit('farmfiles/coaddcoadd/','load_and_combine(mapK13,mapB12,mapK13)','func',{@load_and_combine,@minimize_coaddopt},'var',{'mapK13','mapB12','mapK13'},'queue',queue,'mem',10000,'maxtime',15,'submit',0)    
end

% the sims
for j='0345'
  for t = '23456'
    for rlz = 1:499
      mapBK13 = sprintf(['1456/%03d',t,'_aab_filtp3_weight3_gs_dp1100_jack',j,'.mat'],rlz);
      if ~exist(['maps/',mapBK13],'file')
        mapB12   = sprintf(['0751/%03d',t,'_a_filtp3_weight3_gs_dp1100_jack',j,'.mat'],rlz);
        mapK13   = sprintf(['1351/%03d',t,'_ab_filtp3_weight3_gs_dp1100_jack',j,'.mat'],rlz);        
        % load_and_combine(mapB12,mapK13,mapBK13)
        farmit('farmfiles/coaddcoadd/','load_and_combine(mapK13,mapB12,mapK13)','func',{@load_and_combine,@minimize_coaddopt},'var',{'mapK13','mapB12','mapK13'},'queue',queue,'mem',10000,'maxtime',15,'submit',0)    
      end      
    end
  end
end
babysitjobs('coaddcoadd/*.mat','wait2',0,0,50)

for rlz = rlzs
  cmd1 = ['reduc_combcomap(''1456/xxx6_ab_filtp3_weight3_gs_dp1100_jack[0345].mat'',5,7,1)'];
  cmd1 = strrep(cmd,'xxx',sprintf('%03d',rlz));
  
  cmd2 = ['reduc_combcomap(''1456/xxx7_ab_filtp3_weight3_gs_dp1100_jack[0345].mat'',4,9,1)'];
  cmd2 = strrep(cmd2,'xxx',sprintf('%03d',rlz)); cmd  = [cmd,';',cmd2];
  
  cmd3 = ['reduc_combcomap(''1456/xxx7_ab_filtp3_weight3_gs_dp1100_jack[0345].mat'',3,8,1)'];
  cmd3 = strrep(cmd3,'xxx',sprintf('%03d',rlz)); 
  
  cmd  = [cmd1,';',cmd2,';',cmd3];
  
  farmit('farmfiles/comb/',cmd,'queue',queue,'mem',2000,'maxtime',20,'submit',0);
end
babysitjobs('farmfiles/comb/*','wait5')
      
return


function load_and_combine(mapB12,mapK13,mapBK13)
%  this needs to be adjusted year to year
  mapB12
  mapK13
  mapBK13
  
  mB12  = load(['maps/',mapB12]);
  mK13  = load(['maps/',mapK13]);
    
  % make sure you got the right ukpervolt for that year here:
  mB12.coaddopt.ukpv_applied = 3150;
  mB12.ac=cal_coadd_ac(mB12.ac,mB12.coaddopt.ukpv_applied);
  
  % for K13 nothing needs to be done...
  %
  
  ac = struct_merge(mB12.ac,mK13.ac);
  ac = coadd_ac_overrx(ac);
  
  mB12.coaddopt = minimize_coaddopt(mB12.coaddopt);
  
  mB12.coaddopt.mapname = mapB12;
  
  coaddopt={mB12.coaddopt,mK13.coaddopt{:}};
  m = mK13.m;
  
  saveandtest(['maps/',mapBK13],'ac','coaddopt','m','-v7.3');
return

function coaddopt = minimize_coaddopt(coaddopt)
  try coaddopt = rmfield(coaddopt,'b'); end
  try coaddopt = rmfield(coaddopt,'bi'); end
  try coaddopt = rmfield(coaddopt,'bw'); end
  try coaddopt = rmfield(coaddopt,'hsmax'); end
  try coaddopt = rmfield(coaddopt,'whist'); end
  try coaddopt = rmfield(coaddopt,'devhist'); end
  try coaddopt = rmfield(coaddopt,'traj'); end
  try coaddopt = rmfield(coaddopt,'c'); end
  try coaddopt = rmfield(coaddopt,'jackmask'); end
return
