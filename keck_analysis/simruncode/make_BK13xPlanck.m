function make_BK13xPlanck()
% function make_BK13xPlanck()
%
% moved from make_BKPlanck to make_BK13xPlanck for more consistent naming
%
% Of the BKPlanck joint analysis this script makes:
% * all the reobserved maps
% * all aps
% * all final files
% the only thing it does not make are the fgm map files which are coming
% from util/construct_multicomponent_maps.m
% there are three portions:
% B2, Keck, B2K
% the right choice of purifcation, reobservation matrix and abscal is done on basis of the 
% analysis folder the script is started in. Bicep2/Planck requires the bicep2 folder
% Keck/Planck and BK/Planck requires the keck analysis folder.
  global expt;
  keyboard
  
  % cooking is common to all portions since all use Bicep2's bbns beam profile:
  if(0)
   do_cook('B2bbns')
   do_cook('Kuber100')   
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % the Keck only portion run from the keck folder
  if(0)
    cd ~/keck_analysis
    expt = 'K13'
    % all freqs
    do_reobs('1614','ab','B2bbns',{'030','044','070','100','143','217','353'})
    do_jack3('1614','1353','ab')
    do_links('1614','1353','ab')
    do_fixukpervolt('1614')
    do_comb('1614','ab')
    do_mapspager('1614','1351','ab','K') 

    % regular aps
    do_aps('1614','1351','ab',-0.5,1)
    do_simset('1614','1351','ab')
    do_final('1614','1351','ab','K')
    
    % 45666 files
    do_bns('1614','1351','ab',-0.5,1)
    do_bns_simset('1614','1351','ab')
    do_bns_final('1614','1351','ab','K') 
    
    % all the variations of the fgms. Those got a pain because they started off
    % with straight match of fgm number and folder where to find them,
    % at fgm10 it got more involved
    do_foregrounds('1614','1351','ab','261','260','a','012356789','K')
    do_foregrounds('1614','1351','ab','265','264','a','0123','K',{'10','11','12','13'})
    % a special is this on which is a dust, no noise variation:
    do_foregrounds('1614','1351','ab','265','264','a','2','K',{'13'})
    % and this was part of the rogue sim investigation:
    do_foregrounds('1614','1351','ab','265','264','cmb338','2','K',{'12'})
    do_foregrounds('1614','1351','ab','265','264','cmbn338','2','K',{'12'})
    do_foregrounds('1614','1351','ab','265','264','psm338','2','K',{'12'})
    
    do_foregrounds('1614','1351','ab','265','264','a','5','K',{'15'})
  
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % the bicep2 portion from the bicep folder
  if(0)
    cd ~/bicep2_analysis
    expt = 'B12'
    do_reobs('1613','a','B2bbns',{'030','044','070','100','143','217','353'})
    do_jack3('1613','0751','a')
    do_links('1613','0751','a')
    do_comb('1613','a')
    do_mapspager('1613','0751','a','B2') 
    
    do_aps('1613','0751','a',-1.1,1)
    do_simset('1613','0751','a')
    do_final('1613','0751','a','B2')
    
    do_bns('1613','0751','a',-1.1,1)
    do_bns_simset('1613','0751','a')
    do_bns_final('1613','0751','a','B2') 

    do_foregrounds('1613','0751','a','261','260','a','012356789','B2')
    do_foregrounds('1613','0751','a','265','264','a','0123','B2',{'10','11','12','13'})
    do_foregrounds('1613','0751','a','265','264','a','2','B2',{'13'})
    
    do_foregrounds('1613','0751','a','265','264','cmb338','2','B2',{'12'})
    do_foregrounds('1613','0751','a','265','264','cmbn338','2','B2',{'12'})
    do_foregrounds('1613','0751','a','265','264','psm338','2','B2',{'12'})
    
    do_foregrounds('1613','0751','a','265','264','a','5','B2',{'15'})
    
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % the b2k portion from the Keck folder
  if(0)
    cd ~/keck_analysis
    expt = 'BK13'
    % BKPlanck does not need the reobs portion since those
    % maps are coadded from the B2 and Keck only maps.
    do_jack3('1457','1456','aab')
    do_links('1457','1456','aab'),
    do_comb('1457','aab'),
    do_mapspager('1457','1456','aab','B2K')
  
    do_aps('1457','1456','aab',-0.8,1)
    do_simset('1457','1456','aab')
    do_final('1457','1456','aab','B2K')
    
    do_bns('1457','1456','aab',-0.8,1)
    do_bns_simset('1457','1456','aab')
    do_bns_final('1457','1456','aab','B2K')
  
    do_foregrounds('1457','1456','aab','148','147','aab','012356789','B2K')        
    do_foregrounds('1457','1456','aab','146','149','aab','0123','B2K',{'10','11','12','13'})
    do_foregrounds('1457','1456','aab','146','149','aab','2','B2K',{'13'})
    
    % scaled P100 noise:
    do_bns('1457','1456','aab',-0.8,0,1)
    do_bns_simset('1457','1456','aab',1)
    do_bns_final('1457','1456','aab','B2K',1)
    do_foregrounds('1457','1456','aab','148','147','aab','7','B2K',{'7'},10,1)
    do_foregrounds('1457','1456','aab','148','147','aab','1','B2K',{'1'},10,1,sqrt(3.6/2.5))
    do_foregrounds('1457','1456','aab','148','147','aab','1','B2K',{'1'},10,0,sqrt(3.6/2.5))
    
    do_foregrounds('1457','1456','aab','146','149','aab','5','B2K',{'15'})
  
    
  end

    
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % K14 95
  if(0)
    cd ~/keck_analysis
    expt = 'BK14_150'
    % just use the released Planck Freqs:
    do_reobs('1615','d','B2bbns',{'023','030','044','070','353'})
  end
  
return

function do_foregrounds(snF,snI,daughter,snP,snK,daughtern,ms,mapname,fgms,njobs,scale_noise,scale_dust)
%    keyboard
  if ~exist('scale_noise','var')
    scale_noise=0;
  end
  if ~exist('scale_dust','var')
    scale_dust=-1;
  end
  if ~exist('fgms','var')
    fgms=ms;
  end
  if ~exist('njobs','var')
    njobs=10;
  end
  apsopt=get_default_apsopt();
  apsopt.update=1;

  sel = '[01234]??'

  % 8 times because b2 and planck at once
  apsopt.purifmatname={get_purification_matrix};
  apsopt.purifmatname=repmat(apsopt.purifmatname,1,22);
  apsopt.polrot=[0,0,0];
  % all foregounds in CMB units
  apsopt.ukpervolt=1;
  apsopt.howtojack='none';
  
  if(0)
  for ii=1:njobs
    for jj=1:length(ms)
      m = ms(jj);
      if iscell(fgms)
        fgm=fgms{jj};
      else
        fgm=fgms(jj);
      end

      cmd = ['reduc_makeaps({''',snK,m,'/',sel,'8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0.mat'',{''',snK,m,''',''',snP,m,'''},{''',snK,m,''',''',snP,m,''',''jack0'',''jack4''}},apsopt)']
      farmit('farmfiles/foregrounds/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',25000,'maxtime',120,'submit',0);
      if m=='7'
        apsopt.daughter='dust0p9';
        apsopt.scalefac={1*ones(1,22),1*ones(1,22),1*ones(1,22),sqrt(0.9)*ones(1,22)}; 
        farmit('farmfiles/foregrounds/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',25000,'maxtime',120,'submit',0);
        apsopt=rmfield(apsopt,'daughter');apsopt=rmfield(apsopt,'scalefac');
      end
      if fgm=='13' & m=='2'
        apsopt.daughter='dustonly';
        apsopt.select_ac=4;
        farmit('farmfiles/foregrounds/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',25000,'maxtime',120,'submit',0);
        apsopt=rmfield(apsopt,'daughter');apsopt=rmfield(apsopt,'select_ac');
      end
      if scale_noise | scale_dust>=0
        apsopt.daughter = '';
        apsopt.scalefac = {1*ones(1,22),1*ones(1,22),1*ones(1,22),1*ones(1,22)};
        if scale_noise
          nsc = 1/5.27;
          apsopt.scalefac{1}([5,15,16]) = nsc;
          apsopt.daughter = 'scnoi';
        end
        if scale_dust>=0
          apsopt.daughter = [apsopt.daughter,'_dust',num2str(scale_dust)];
          apsopt.daughter = strrep(apsopt.daughter,'.','p');
          apsopt.scalefac{4} = apsopt.scalefac{4}*scale_dust;
        end
        farmit('farmfiles/foregrounds/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',25000,'maxtime',120,'submit',0);
        apsopt=rmfield(apsopt,'daughter');apsopt=rmfield(apsopt,'scalefac');
      end
      
    end
  end
  
  
  babysitsims('foregrounds/*.mat','wait5',0,0)

  % simsets
  for ii=1:length(ms)
    m=ms(ii);
    cmd = ['reduc_makesimset(''',snK,m,'x',snP,m,'/???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack4_matrix.mat'')'];
    farmit('farmfiles/simset/',cmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',120)
    if m=='7'
      cmd = ['reduc_makesimset(''',snK,m,'x',snP,m,'/???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack4_matrix_dust0p9.mat'')'];
      farmit('farmfiles/simset/',cmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',120)
    end
    if iscell(fgms)
      fgm=fgms{ii};
    else
      fgm=fgms(ii);
    end
    if fgm=='13' & m=='2'
      cmd = ['reduc_makesimset(''',snK,m,'x',snP,m,'/???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack4_matrix_dustonly.mat'')'];
      farmit('farmfiles/simset/',cmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',120)
    end
    if scale_noise | scale_dust>=0
      cmd = ['reduc_makesimset(''',snK,m,'x',snP,m,'/???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_???8_',daughtern,'_filtp3_weight3_gs_dp1100_jack4_matrix.mat'')'];
      if scale_noise
        cmd = strrep(cmd,'.mat','_scnoi.mat');
      end
      if scale_dust>=0
        cmd = strrep(cmd,'.mat',[strrep(['_dust',num2str(scale_dust)],'.','p'),'.mat']);
      end
      keyboard
      farmit('farmfiles/simset/',cmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',120)
    end
  end
  
  babysitsims('simset/*.mat','wait5',0,0)
  end
  
  aps_daughter='';
  if scale_noise
    aps_daughter='_scnoi';
  end
  aps_daughter2=aps_daughter;
  if scale_dust>=0
    aps_daughter2 = [aps_daughter2,strrep(['_dust',num2str(scale_dust)],'.','p')];
  end
  
  % final
  finalopt.doblind = 0;
  finalopt.mapname={mapname,'P030','P044','P070','P100','P143','P217','P353','P030H1','P030H2','P044H1','P044H2','P070H1','P070H2','P100H1','P100H2','P143H1','P143H2','P217H1','P217H2','P353H1','P353H2'}
  finalopt.supfacstyle='direct_bpwf';
  finalopt.bpwfname=get_bpwf();
  finalopt.force_size=[repmat(1,1,22),repmat(17,1,253-22)];

  r = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0_real_',daughter,'_filtp3_weight3_gs_dp1100_jack0_real_',daughter,'_filtp3_weight3_gs_dp1100_jack4_pureB_matrix']; 
  finalopt.ssimset  = [snI,'x',snF,'/xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack4_pureB_matrix'];
  finalopt.bosimset = [snI,'x',snF,'/xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack4_pureB_matrix'];
  finalopt.nsimset  = [snI,'x',snF,'/xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack4_pureB_matrix',aps_daughter];
  finalopt.snsimset = [snI,'x',snF,'/xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack4_pureB_matrix',aps_daughter];  


  % hand in the foreground + r + lensed lcdm + noise instead of type 9
  % this will pop up as simr field in the final file
  fix='';
  switch daughtern,
   case 'cmb338'
    fix='_cmb338'; 
   case 'cmbn338'
    fix='_cmbn338'; 
   case 'psm338'
    fix='_psm338';  
  end
  if scale_noise
    fix=[fix,'_scnoi'];
  end
  if scale_dust>=0
    fix = [fix,strrep(['_dust',num2str(scale_dust)],'.','p')];
  end

  for ii=1:length(ms)
    m=ms(ii);
    if iscell(fgms)
      fgm=fgms{ii};
    else
      fgm=fgms(ii);
    end
    finalopt.snrsimset= [snK,m,'x',snP,m,'/xxx8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_xxx8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_xxx8_',daughtern,'_filtp3_weight3_gs_dp1100_jack4_matrix',aps_daughter2];
    finalopt.daughter=['_fgm',num2str(fgm),fix]
    reduc_final(r,finalopt,0,500)
%      if m=='7'
%        finalopt.snrsimset= [snK,m,'x',snP,m,'/xxx8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_xxx8_',daughtern,'_filtp3_weight3_gs_dp1100_jack0_xxx8_',daughtern,'_filtp3_weight3_gs_dp1100_jack4_matrix_dust0p9'];
%        finalopt.daughter=[finalopt.daughter,'_dust0p9'];
%        reduc_final(r,finalopt,0,500)
%      end
  end
 
return
 
  
function do_reobs(snF,daughter,beam,fs)
  % fetch the matrix
%    keyboard
  rm = get_reobs_matrix()
  matrix=load(rm);
  coaddopt=rmfield(matrix.coaddopt,'c');
  m=matrix.m;
  
  % create a proxy for the ac structure with nans.
  ac_proc.wsum  = nan(100,236);
  ac_proc.wwv   = nan(100,236);
  ac_proc.sitime= nan(100,236);
  ac_proc.ditime= nan(100,236);
  ac_proc.w     = nan(100,236);
  ac_proc.wcc   = nan(100,236);
  ac_proc.wcs   = nan(100,236);
  ac_proc.wss   = nan(100,236);
  ac_proc.wwccv = nan(100,236);
  ac_proc.wwcsv = nan(100,236);
  ac_proc.wwssv = nan(100,236);
  ac_proc.wz    = nan(100,236);
  ac_proc.wcz   = nan(100,236);
  ac_proc.wsz   = nan(100,236);
  

  % full map
  mfn=['input_maps/b2planck_DDDDDD/maps/Planck_B2Field_XXXGHz_DX11d_FULL_nside0512_nlmax1280_b',beam,'.fits'];
  ac = repmat(ac_proc,7,1);
  for ii=1:length(fs)
    try
      cmfn = get_planck_filename(mfn,fs{ii});
      ac(ii,1)=reduc_observemap(matrix,cmfn,8,0,0,[]);
      coaddopt.reob_map_fname{ii,1}=cmfn;
    catch; continue; end;
  end
  save(['maps/',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack0'],'ac','coaddopt','m');
  clear ac;
  clear coaddopt.reob_map_fname;
  
  % year splits
  mfn1=['input_maps/b2planck_DDDDDD/maps/Planck_B2Field_XXXGHz_DX11d_Y1_nside0512_nlmax1280_b',beam,'.fits'];
  mfn2=['input_maps/b2planck_DDDDDD/maps/Planck_B2Field_XXXGHz_DX11d_Y2_nside0512_nlmax1280_b',beam,'.fits'];
  ac = repmat(ac_proc,7,2);
  for ii=1:length(fs)    
    try
      % first half of the split
      cmfn = get_planck_filename(mfn1,fs{ii});
      ac(ii,1)=reduc_observemap(matrix,cmfn,8,0,0,[]);
      coaddopt.reob_map_fname{ii,1}=cmfn;
      % second half of the split
      cmfn = get_planck_filename(mfn2,fs{ii});
      ac(ii,2)=reduc_observemap(matrix,cmfn,8,0,0,[]);
      coaddopt.reob_map_fname{ii,2}=cmfn;
    catch; continue; end;  
  end
  save(['maps/',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack3'],'ac','coaddopt','m');
  clear ac;
  clear coaddopt.reob_map_fname;

  % detector set splits
  mfn1=['input_maps/b2planck_DDDDDD/maps/Planck_B2Field_XXXGHz_DX11d_DS1_nside0512_nlmax1280_b',beam,'.fits'];
  mfn2=['input_maps/b2planck_DDDDDD/maps/Planck_B2Field_XXXGHz_DX11d_DS2_nside0512_nlmax1280_b',beam,'.fits'];
  ac = repmat(ac_proc,7,2);
  for ii=1:length(fs)
    try
      % first half of the split
      cmfn = get_planck_filename(mfn1,fs{ii});
      ac(ii,1)=reduc_observemap(matrix,cmfn,8,0,0,[]);
      coaddopt.reob_map_fname{ii,1}=cmfn;
      % second half of the split
      cmfn = get_planck_filename(mfn2,fs{ii});
      ac(ii,2)=reduc_observemap(matrix,cmfn,8,0,0,[]);
      coaddopt.reob_map_fname{ii,2}=cmfn;
    catch; continue; end;
  end
  save(['maps/',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack4'],'ac','coaddopt','m');
  clear ac;
  clear coaddopt.reob_map_fname;
  
  % halfring splits
  mfn1=['input_maps/b2planck_DDDDDD/maps/Planck_B2Field_XXXGHz_DX11d_FULL_HR1_nside0512_nlmax1280_b',beam,'.fits'];
  mfn2=['input_maps/b2planck_DDDDDD/maps/Planck_B2Field_XXXGHz_DX11d_FULL_HR2_nside0512_nlmax1280_b',beam,'.fits'];
  ac = repmat(ac_proc,7,2);
  for ii=1:length(fs)
    try
      % first half of the split
      cmfn = get_planck_filename(mfn1,fs{ii});
      ac(ii,1)=reduc_observemap(matrix,cmfn,8,0,0,[]);
      coaddopt.reob_map_fname{ii,1}=cmfn;
      % second half of the split
      cmfn = get_planck_filename(mfn2,fs{ii});
      ac(ii,2)=reduc_observemap(matrix,cmfn,8,0,0,[]);
      coaddopt.reob_map_fname{ii,2}=cmfn;
    catch; continue; end;
  end
  save(['maps/',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack5'],'ac','coaddopt','m');
  clear ac;
  clear coaddopt.reob_map_fname;

  % noise rlz FULL
  mfn=['input_maps/b2planck_DDDDDD/sims/Planck_B2Field_XXXGHz_FFP8_FULL_noise_MC_YYYYY_nside0512_nlmax1280_b',beam,'.fits'];
  /n/bicepfs1/bicep2/pipeline/input_maps/wmap9/noisemaps/kb/map_l350_sbbns_r0152.fits
  for rlz=1:499
    rlz
    fn = sprintf(['maps/',snF,'/%03d6_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'],rlz);
    if exist(fn,'file'); continue; end
    ac = repmat(ac_proc,7,1);
    for ii=1:length(fs)
      try
        cmfn = strrep(mfn,'YYYYY',sprintf('%05d',rlz));
        cmfn = get_planck_filename(cmfn,fs{ii});
        ac(ii,1)=reduc_observemap(matrix,cmfn,8,0,0,[]);
        coaddopt.reob_map_fname{ii,1}=cmfn;
      catch; continue; end;
    end
    save(fn,'ac','coaddopt','m');
    clear ac;
    clear coaddopt.reob_map_fname;
  end

  % noise rlz Yrs
  mfn1=['input_maps/b2planck_DDDDDD/sims/Planck_B2Field_XXXGHz_FFP8_Y1_noise_MC_YYYYY_nside0512_nlmax1280_b',beam,'.fits']
  mfn2=['input_maps/b2planck_DDDDDD/sims/Planck_B2Field_XXXGHz_FFP8_Y2_noise_MC_YYYYY_nside0512_nlmax1280_b',beam,'.fits']
  for rlz=1:499
    rlz
    fn = sprintf(['maps/',snF,'/%03d6_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat'],rlz);
    if exist(fn,'file'); continue; end
    ac = repmat(ac_proc,7,2);
    for ii=1:length(fs)
      try
        % first half of the split
        cmfn = strrep(mfn1,'YYYYY',sprintf('%05d',rlz));
        cmfn = get_planck_filename(cmfn,fs{ii});
        ac(ii,1)=reduc_observemap(matrix,cmfn,8,0,0,[]);
        coaddopt.reob_map_fname{ii,1}=cmfn;
        % second half of the split
        cmfn = strrep(mfn2,'YYYYY',sprintf('%05d',rlz));
        cmfn = get_planck_filename(cmfn,fs{ii});
        ac(ii,2)=reduc_observemap(matrix,cmfn,8,0,0,[]);
        coaddopt.reob_map_fname{ii,2}=cmfn;
      catch; continue; end;
    end
    save(fn,'ac','coaddopt','m');
    clear ac;
    clear coaddopt.reob_map_fname;
  end
  clear ac;
  clear coaddopt.reob_map_fname;
  
  % noise rlz DS
  mfn1=['input_maps/b2planck_DDDDDD/sims/Planck_B2Field_XXXGHz_FFP8_DS1_noise_MC_YYYYY_nside0512_nlmax1280_b',beam,'.fits']
  mfn2=['input_maps/b2planck_DDDDDD/sims/Planck_B2Field_XXXGHz_FFP8_DS2_noise_MC_YYYYY_nside0512_nlmax1280_b',beam,'.fits']
  for rlz=1:499
    rlz
    fn = sprintf(['maps/',snF,'/%03d6_',daughter,'_filtp3_weight3_gs_dp1100_jack4.mat'],rlz);
    if exist(fn,'file'); continue; end
    ac = repmat(ac_proc,7,2);
    for ii=1:length(fs)
      try
        % first half of the split
        cmfn = strrep(mfn1,'YYYYY',sprintf('%05d',rlz));
        cmfn = get_planck_filename(cmfn,fs{ii});
        ac(ii,1)=reduc_observemap(matrix,cmfn,8,0,0,[]);
        coaddopt.reob_map_fname{ii,1}=cmfn;
        % second half of the split
        cmfn = strrep(mfn2,'YYYYY',sprintf('%05d',rlz));
        cmfn = get_planck_filename(cmfn,fs{ii});
        ac(ii,2)=reduc_observemap(matrix,cmfn,8,0,0,[]);
        coaddopt.reob_map_fname{ii,2}=cmfn;
      catch; continue; end;
    end
    save(fn,'ac','coaddopt','m');
    clear ac;
    clear coaddopt.reob_map_fname;
  end
  clear ac;
  clear coaddopt.reob_map_fname;
  
  % noise rlz half rings
  mfn1=['input_maps/b2planck_DDDDDD/sims/Planck_B2Field_XXXGHz_FFP8_FULL_noise_MC_YYYYY_HR1_nside0512_nlmax1280_b',beam,'.fits'];
  mfn2=['input_maps/b2planck_DDDDDD/sims/Planck_B2Field_XXXGHz_FFP8_FULL_noise_MC_YYYYY_HR2_nside0512_nlmax1280_b',beam,'.fits'];
  for rlz=1:499
    rlz
    fn = sprintf(['maps/',snF,'/%03d6_',daughter,'_filtp3_weight3_gs_dp1100_jack5.mat'],rlz);
    if exist(fn,'file'); continue; end
    ac = repmat(ac_proc,7,2);
    for ii=1:length(fs)
      try
        % first half of the split
        cmfn = strrep(mfn1,'YYYYY',sprintf('%05d',rlz));
        cmfn = get_planck_filename(cmfn,fs{ii});
        ac(ii,1)=reduc_observemap(matrix,cmfn,8,0,0,[]);
        coaddopt.reob_map_fname{ii,1}=cmfn;
        % second half of the split
        cmfn = strrep(mfn2,'YYYYY',sprintf('%05d',rlz));
        cmfn = get_planck_filename(cmfn,fs{ii});
        ac(ii,2)=reduc_observemap(matrix,cmfn,8,0,0,[]);
        coaddopt.reob_map_fname{ii,2}=cmfn;
      catch; continue; end;
    end
    save(fn,'ac','coaddopt','m');
    clear ac;
    clear coaddopt.reob_map_fname;
  end
  clear ac;
  clear coaddopt.reob_map_fname;
return

function do_jack3(snF,snI,daughter)
  % signal realizations for Yrs and detector splits are the same as jack0 of Keck
  for t='245'
    for rlz=1:499
      rlz
      fn = sprintf(['maps/',snF,'/%03d',t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat'],rlz);
      if exist(fn,'file'); continue; end
      % ac = repmat(ac_proc,7,2);
      keck = load(sprintf(['maps/',snI,'/%03d',t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'],rlz));
      for ii=1:7
        ac(ii,1)=keck.ac;
        ac(ii,2)=keck.ac;
      end
      coaddopt=keck.coaddopt;
      m = keck.m;
      save(fn,'ac','coaddopt','m');
      clear ac;
      clear coaddopt;
      clear m;
      clear keck;
    end
  end
return

function do_links(snF,snI,daughter)      
  wd=pwd;
  cd(['maps/',snF]);
  fname = 'do_links.sh'
  fileID = fopen(fname, 'w');
  for i=1:499
    fprintf(fileID,sprintf(['ln -s ../',snI,'/%03d2_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat\n'],i));
    fprintf(fileID,sprintf(['ln -s ../',snI,'/%03d4_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat\n'],i));
    fprintf(fileID,sprintf(['ln -s ../',snI,'/%03d5_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat\n'],i));
    
    fprintf(fileID,sprintf(['ln -s %03d2_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat %03d2_',daughter,'_filtp3_weight3_gs_dp1100_jack4.mat\n'],i,i));
    fprintf(fileID,sprintf(['ln -s %03d4_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat %03d4_',daughter,'_filtp3_weight3_gs_dp1100_jack4.mat\n'],i,i));
    fprintf(fileID,sprintf(['ln -s %03d5_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat %03d5_',daughter,'_filtp3_weight3_gs_dp1100_jack4.mat\n'],i,i));
    
    fprintf(fileID,sprintf(['ln -s %03d2_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat %03d2_',daughter,'_filtp3_weight3_gs_dp1100_jack5.mat\n'],i,i));
    fprintf(fileID,sprintf(['ln -s %03d4_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat %03d4_',daughter,'_filtp3_weight3_gs_dp1100_jack5.mat\n'],i,i));
    fprintf(fileID,sprintf(['ln -s %03d5_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat %03d5_',daughter,'_filtp3_weight3_gs_dp1100_jack5.mat\n'],i,i));
  end
  fclose(fileID)
  cd(wd)
return

function do_fixukpervolt(snF)
  ukpervolt = get_ukpervolt();
  cmd = ['fix_ukpervolt(''',snF,'/real_ab_filtp3_weight3_gs_dp1100_jack?.mat'',1,ukpervolt)'];
  eval(cmd)

  for rlz = 1:499     
    cmd = ['fix_ukpervolt(''',snF,'/%03d6_ab_filtp3_weight3_gs_dp1100_jack[0345].mat'',1,ukpervolt)'];
    cmd = sprintf(cmd,rlz);
    farmit('farmfiles/fixuk/',cmd,'queue','general,serial_requeue,itc_cluster','mem',3000,'maxtime',15,'var','ukpervolt','submit',0);
  end
    
return
  
function do_comb(snF,daughter)
  % now add the lensed-LCDM to the noise to make type 7
  cmd={}
  cmd{end+1}=['reduc_combcomap(''',snF,'/[01234]??6_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',5,7,1,1)'];
  cmd{end+1}=['reduc_combcomap(''',snF,'/[01234]??6_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat'',5,7,1,1)'];
  cmd{end+1}=['reduc_combcomap(''',snF,'/[01234]??6_',daughter,'_filtp3_weight3_gs_dp1100_jack4.mat'',5,7,1,1)'];
  cmd{end+1}=['reduc_combcomap(''',snF,'/[01234]??6_',daughter,'_filtp3_weight3_gs_dp1100_jack5.mat'',5,7,1,1)'];
  for ccmd = cmd
    farmit('farmfiles/comb/',ccmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',120,'submit',0);
  end
  babysitsims('comb/*.mat','wait2',0,0)
  
  cmd={}
  cmd{end+1}=['reduc_combcomap(''',snF,'/[01234]??7_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',4,9,1,1)'];
  cmd{end+1}=['reduc_combcomap(''',snF,'/[01234]??7_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat'',4,9,1,1)'];
  cmd{end+1}=['reduc_combcomap(''',snF,'/[01234]??7_',daughter,'_filtp3_weight3_gs_dp1100_jack4.mat'',4,9,1,1)'];
  cmd{end+1}=['reduc_combcomap(''',snF,'/[01234]??7_',daughter,'_filtp3_weight3_gs_dp1100_jack5.mat'',4,9,1,1)'];
  for ccmd = cmd
    farmit('farmfiles/comb/',ccmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',120,'submit',0);
  end
  babysitsims('comb/*.mat','wait2',0,0)  
return
  
function do_mapspager(snF,snI,daughter,mapname) 
  % make the maps pager
  apsopt=get_default_apsopt();
  apsopt.purifmatname={get_purification_matrix()};
  % use the same purifmatrix for all planck freqs:
  apsopt.purifmatname=repmat(apsopt.purifmatname,1,7);
  apsopt.ukpervolt=get_local_ukpervolt();
  rlz = '499'
  apsopt.mapname={mapname};
  cmd = {}
  for fcs=0:1
%      cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
%      cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack3.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
%      cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack4.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
%      cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack5.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/',rlz,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/',rlz,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/',rlz,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack4.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/',rlz,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack5.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    
    cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/',rlz,'2_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/',rlz,'4_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/',rlz,'5_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/',rlz,'7_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snI,'/',rlz,'9_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
  end
  for ccmd = cmd
    farmit('farmfiles/rpp/',ccmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);
  end
  
  apsopt.mapname={'Pl030','Pl044','Pl070','Pl100','Pl143','Pl217','Pl353'};
  cmd = {}
  for fcs=0:1
%      cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
%      cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
%      cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack4.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
%      cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1100_jack5.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/',rlz,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/',rlz,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack3.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/',rlz,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack4.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/',rlz,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack5.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/',rlz,'2_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/',rlz,'4_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/',rlz,'5_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/',rlz,'7_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
    cmd{end+1}=['reduc_plotcomap_pager(''',snF,'/',rlz,'9_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',apsopt,',num2str(fcs),',[],[],[],[],[],[],0,[],1);'];
  end
  for ccmd = cmd
    farmit('farmfiles/rpp/',ccmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);
  end

  babysitsims('rpp/*.mat','wait2',0,0)

return
 
function do_aps(snF,snI,daughter,polrot,do_real)
  keyboard
  sel = '[01234]??'

  % now run reduc_makeaps in cross spectrum mode
  apsopt=get_default_apsopt();
  apsopt.update=1;
  scalefac={1*ones(1,8),1*ones(1,8),sqrt(2)*ones(1,8)};
  apsopt.ukpervolt=get_local_ukpervolt();
  % we need the kendrick estimator for the jacks
  apsopt.pure_b = 'kendrick';       
  % 8x the Keck matrix
  apsopt.purifmatname={get_purification_matrix()};  
  apsopt.purifmatname=repmat(apsopt.purifmatname,1,8);

  % the real data
  % rotate keck but only for real data, don't do it for planck
  apsopt.polrot=[polrot,0]; 
  cmd = ['reduc_makeaps({''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0.mat'',''',snI,''',''',snF,''',''dp1102'',''dp1100''},apsopt)'];
  % cmd = ['reduc_makeaps({''',snI,'/real_',daughter,'_fixesub_filtp3_weight3_gs_dp1102_jack0.mat'',''',snI,''',''',snF,''',''dp1102'',''dp1100'',''_fixesub'',''''},apsopt)'];
  if do_real
    farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);
  end
  % the real experiment jack
  apsopt.howtojack='dim1';
  if do_real
    farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);
  end
      
  % the real data jacks
  % no matrix for the jacks
  apsopt.howtojack='dim2';
  apsopt.purifmatname={};
  cmd = ['reduc_makeaps({''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack[345].mat'',''',snI,''',''',snF,''',''dp1102'',''dp1100''},apsopt)'];
  if do_real
    farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);
  end
  
  % but for Planck why not use the matrix. We probably have to: there is no EtoB leakage prediction
  % for the planck Jacks so we have to avoid this in the jack. Since the planck splits are reobserved
  % with B2's full observing pattern that's good to go:
  apsopt.purifmatname={get_purification_matrix()};
  apsopt.purifmatname=repmat(apsopt.purifmatname,1,8);
  apsopt.purifmatname{1}=[]; % no matrix for the b2 part.
  cmd = ['reduc_makeaps({''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack[345].mat'',''',snI,''',''',snF,''',''dp1102'',''dp1100''},apsopt)'];
  if do_real
    farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);
  end

  % the sims
  apsopt.howtojack='dim2';
  apsopt.purifmatname={get_purification_matrix()};
  apsopt.purifmatname=repmat(apsopt.purifmatname,1,8);
  apsopt.polrot=[0,0];
  cmd={};
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'2_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'4_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'5_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'7_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt)'];
  cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,'9_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt)'];
  for ccmd = cmd
    % apply scaling to r=0.2 in the case of type 9
    if ~isempty(strfind(ccmd{:},'9_a'))
      apsopt.scalefac=scalefac
    elseif isfield(apsopt,'scalefac')
      apsopt = rmfield(apsopt,'scalefac');
    end        
    farmit('farmfiles/aps/',ccmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);
  end
        
  % to back up the jacks 3,4,5 we also need the sims without matrix purification....
  apsopt.purifmatname={};
  for ccmd = cmd
    if ~isempty(strfind(ccmd{:},'9_a'))
      apsopt.scalefac=scalefac
    elseif isfield(apsopt,'scalefac')
      apsopt = rmfield(apsopt,'scalefac');
    end        
    farmit('farmfiles/aps/',ccmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',10000,'maxtime',60,'submit',0);
  end
  if isfield(apsopt,'scalefac')
    apsopt = rmfield(apsopt,'scalefac');
  end

  % the sim experiment jack
  apsopt.purifmatname={get_purification_matrix()};
  apsopt.purifmatname=repmat(apsopt.purifmatname,1,8);
  apsopt.howtojack='dim1'; 
  cmd=['reduc_makeaps({''',snI,'/',sel,'6_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',''',snI,''',''',snF,'''},apsopt)'];
  farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);

  % the sim jacks
  apsopt.howtojack='dim2';
  apsopt.purifmatname={};
  cmd={};
  for j='345'
    for t='245679'
      cmd{end+1}=['reduc_makeaps({''',snI,'/',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'.mat'',''',snI,''',''',snF,'''},apsopt)'];
    end
  end
  for ccmd = cmd
    farmit('farmfiles/aps/',ccmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);
  end
  
  babysitsims('aps/*.mat','wait5',0,0)
  
return

  
function do_bns(snF,snI,daughter,polrot,do_real,scale_noise)
  if ~exist('scale_noise','var')
    scale_noise=0;
  end
  keyboard
  apsopt = get_default_apsopt()
  apsopt.ukpervolt=get_local_ukpervolt();
  apsopt.howtojack='none';
  apsopt.update=1;
  apsopt.pure_b = 'kendrick';
  apsopt.purifmatname={get_purification_matrix()};
  apsopt.purifmatname=repmat(apsopt.purifmatname,1,22);
  scalefac={1*ones(1,22),1*ones(1,22),sqrt(2)*ones(1,22)};
  sel = '[01234]??'
    
  %% aps
  % make all crosses of B2 full, Planck full and Planck split
  % real
  apsopt.polrot=[polrot,0,0];
  if do_real
    cmd = ['reduc_makeaps({''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0.mat'',{''',snI,''',''',snF,''',''dp1102'',''dp1100''},{''',snI,''',''',snF,''',''jack0'',''jack5'',''dp1102'',''dp1100''}},apsopt)'];
    farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);
    cmd = ['reduc_makeaps({''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0.mat'',{''',snI,''',''',snF,''',''dp1102'',''dp1100''},{''',snI,''',''',snF,''',''jack0'',''jack4'',''dp1102'',''dp1100''}},apsopt)'];
    farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);
    cmd = ['reduc_makeaps({''',snI,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0.mat'',{''',snI,''',''',snF,''',''dp1102'',''dp1100''},{''',snI,''',''',snF,''',''jack0'',''jack3'',''dp1102'',''dp1100''}},apsopt)'];
    farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',60,'submit',0);
  end
    
  % sims
  ts = '245679';
  apsopt.polrot=[0,0,0];
  if scale_noise
    apsopt.daughter='scnoi'
    ts = '679';
    nsc = 1/5.27;
  end
  
  for t = ts
    if t=='9'
      apsopt.scalefac=scalefac;
    elseif isfield(apsopt,'scalefac')
      apsopt.scalefac = {1*ones(1,22),1*ones(1,22),1*ones(1,22)};
    end
    if scale_noise
      % scale down the noise of Planck 100. Noise is scalefac{1}, Planck 100 full map is index 5, the two split halves are 15,16:
      apsopt.scalefac{1}([5,15,16]) = nsc;
    end
    % if scaled noise just do the DS split
    cmd = ['reduc_makeaps({''',snI,'/',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snF,''',''jack0'',''jack4''}},apsopt)'];  
    farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',120,'submit',0);    
    if ~scale_noise
      cmd = ['reduc_makeaps({''',snI,'/',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snF,''',''jack0'',''jack5''}},apsopt)'];  
      farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',120,'submit',0);
      cmd = ['reduc_makeaps({''',snI,'/',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',{''',snI,''',''',snF,'''},{''',snI,''',''',snF,''',''jack0'',''jack3''}},apsopt)'];  
      farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',27000,'maxtime',120,'submit',0);
    end
  end
  apsopt = rmfield(apsopt,'scalefac');
  
  % 45666 file aps
  apsopt.purifmatname=get_purification_matrix()
  apsopt.polrot=[0];
  apsopt.pure_b='normal'
  js = '345';
  if scale_noise
    js='4';
    apsopt.scalefac = {1*ones(1,24)};
    apsopt.scalefac{1}([7,17,18]) = nsc;
  end
  for j=js
    cmd = ['reduc_makeaps({''',snI,'/',sel,'4_',daughter,'_filtp3_weight3_gs_dp1100_jack0.mat'',{''4_'',''5_''},{''4_'',''6_''},{''4_'',''6_'',''',snI,''',''',snF,'''}, {''4_'',''6_'',''',snI,''',''',snF,''',''jack0'',''jack',j,'''}},apsopt)'];
    farmit('farmfiles/aps/',cmd,'var',{'apsopt'},'queue','general,serial_requeue,itc_cluster','mem',18000,'maxtime',240,'submit',0);
  end
  
  babysitsims('aps/*.mat','wait5',0,0)
  
return
  
function do_bns_simset(snF,snI,daughter,scale_noise)
  if ~exist('scale_noise','var')
    scale_noise=0;
  end
  if scale_noise
    ts = '679';
    js = '4'
    aps_daughter= '_scnoi';
  else
    ts = '245679';
    js = '345'
    aps_daughter= '';
  end

  %% simset
  for t = ts
    for j=js
      cmd = ['reduc_makesimset(''',snI,'x',snF,'/[01234]??',t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack0_???',t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack0_???',t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix',aps_daughter,'.mat'')']
      farmit('farmfiles/simset/',cmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',30)
    end
  end

  % 45666 file simset
  for j=js
    cmd = ['reduc_makesimset(''',snI,'x',snF,'/???4_',daughter,'_filtp3_weight3_gs_dp1100_jack0_???5_',daughter,'_filtp3_weight3_gs_dp1100_jack0_???6_',daughter,'_filtp3_weight3_gs_dp1100_jack0_???6_',daughter,'_filtp3_weight3_gs_dp1100_jack0_???6_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_matrix',aps_daughter,'.mat'')'];
    farmit('farmfiles/simset/',cmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',100)
  end
  
  babysitsims('simset/*.mat','wait5',0,0)

return

function do_bns_final(snF,snI,daughter,mapname,scale_noise)
  if ~exist('scale_noise','var')
    scale_noise=0;
  end
  if scale_noise
    js = '4'
    aps_daughter= '_scnoi';
    finalopt.daughter=aps_daughter;
  else
    js = '345'
    aps_daughter= '';
  end

  keyboard
  %% final
  finalopt.doblind = 0;
  finalopt.mapname={mapname,'P030','P044','P070','P100','P143','P217','P353','P030H1','P030H2','P044H1','P044H2','P070H1','P070H2','P100H1','P100H2','P143H1','P143H2','P217H1','P217H2','P353H1','P353H2'}
  finalopt.supfacstyle='direct_bpwf';
  finalopt.bpwfname=get_bpwf();
  % type 2 and 4 have too few elements. but those are trivial since all is just the B2 sim.
  finalopt.force_size=[repmat(1,1,22),repmat(17,1,253-22)];
  
  for j=js
    finalopt.ssimset  = [snI,'x',snF,'/xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix'];
    finalopt.bosimset = [snI,'x',snF,'/xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix'];
    finalopt.nsimset  = [snI,'x',snF,'/xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix',aps_daughter];
    finalopt.snsimset = [snI,'x',snF,'/xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix',aps_daughter];
    finalopt.snrsimset= [snI,'x',snF,'/xxx9_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx9_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx9_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix',aps_daughter];   
    
    r = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0_real_',daughter,'_filtp3_weight3_gs_dp1100_jack0_real_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix'];
    reduc_final(r,finalopt,0,500)
  end
  
return
  
  
function do_simset(snF,snI,daughter)  
  sel = '[01234]??'
  % jack0
  for estim = {'_pureB_matrix','_pureB'}
    for t='245679'
      cmd = ['reduc_makesimset(''',snI,'x',snF,'/',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack0_???',t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack0',estim{:},'.mat'')']
      farmit('farmfiles/simset/',cmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',30)
    end
  end
  
  % regular jacks  
  for estim = {'_pureB'}
    for t='245679'
      for j = '345'
        cmd = ['reduc_makesimset(''',snI,'x',snF,'/',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_???',t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,estim{:},'.mat'')']
        farmit('farmfiles/simset/',cmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',30)
      end
    end
  end

  % experiment jack
  farmit('farmfiles/simset/',['reduc_makesimset(''',snI,'x',snF,'/',sel,'6_',daughter,'_filtp3_weight3_gs_dp1100_jackf_???6_',daughter,'_filtp3_weight3_gs_dp1100_jackf_pureB_matrix.mat'')'],'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',30)
  

%    % half jacks
%    for ap = {'','_nonej'}
%      if strcmp(ap,'_nonej')
%        ts = '245679'
%      else
%        ts = '679'
%      end   
%      for t=ts
%        for j='345'
%          cmd = ['reduc_makesimset(''',snI,'x',snF,'/',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack0_',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix',ap{:},'.mat'')']
%          farmit('farmfiles/simset/',cmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',60,'submit',0);
%        end
%      end
%    end
%    
%    % jack halfs
%    for t='245679'
%      for j='345'
%        cmd = ['reduc_makesimset(''',snI,'x',snF,'/',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_',sel,t,'_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej.mat'')']
%        farmit('farmfiles/simset/',cmd,'queue','general,serial_requeue,itc_cluster','mem',4000,'maxtime',30);
%      end
%    end
    
  babysitsims('simset/*.mat','wait5',0,0)
  
  pure_bs = {'_pureB'};
  for pure_b=pure_bs
    pure_b = pure_b{:}
    
    % the experiment jack, here is nothing to copy from Bicep2/Keck -- all zeros
    clear aps coaddopt apsopt;
    load (['aps/',snI,'x',snF,'/xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jackf_xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jackf',pure_b,'_matrix.mat'])
    for ii=1:numel(aps)
      aps(ii).Cs_l=aps(ii).Cs_l*0;
    end
    saveandtest(['aps/',snI,'x',snF,'/xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jackf_xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jackf',pure_b,'_matrix.mat'],'aps','coaddopt','apsopt');
    saveandtest(['aps/',snI,'x',snF,'/xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jackf_xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jackf',pure_b,'_matrix.mat'],'aps','coaddopt','apsopt');
    saveandtest(['aps/',snI,'x',snF,'/xxx5_',daughter,'_filtp3_weight3_gs_dp1100_jackf_xxx5_',daughter,'_filtp3_weight3_gs_dp1100_jackf',pure_b,'_matrix.mat'],'aps','coaddopt','apsopt');
    % type 7 for the experiment jack is then a copy of type 6, type 9 too
    system_safe(['cp aps/',snI,'x',snF,'/xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jackf_xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jackf',pure_b,'_matrix.mat aps/',snI,'x',snF,'/xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jackf_xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jackf',pure_b,'_matrix.mat']) 
    system_safe(['cp aps/',snI,'x',snF,'/xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jackf_xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jackf',pure_b,'_matrix.mat aps/',snI,'x',snF,'/xxx9_',daughter,'_filtp3_weight3_gs_dp1100_jackf_xxx9_',daughter,'_filtp3_weight3_gs_dp1100_jackf',pure_b,'_matrix.mat']) 
     
  end % pureBs
           
return
  
  
function do_final(snF,snI,daughter,mapname)
  
  finalopt.doblind = 0;
  finalopt.mapname={mapname,'P030','P044','P070','P100','P143','P217','P353'}
  finalopt.supfacstyle='direct_bpwf';
  finalopt.bpwfname=get_bpwf();
  finalopt.force_size=[1,repmat(2,1,7),repmat(3,1,28)];
  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  rfcopt.mapname=finalopt.mapname;

  rs{1} = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack4_real_',daughter,'_filtp3_weight3_gs_dp1100_jack4_pureB'];
  rs{2} = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack3_real_',daughter,'_filtp3_weight3_gs_dp1100_jack3_pureB'];
  rs{3} = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0_real_',daughter,'_filtp3_weight3_gs_dp1100_jack0_pureB_matrix'];
  rs{4} = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jackf_real_',daughter,'_filtp3_weight3_gs_dp1100_jackf_pureB_matrix'];
  rs{5} = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack5_real_',daughter,'_filtp3_weight3_gs_dp1100_jack5_pureB'];   
  pl_cross = [1,2;1,3;1,4;1,5;1,6;1,7;1,8;2,3;2,4;2,5;2,6;2,7;2,8;3,4;3,5;3,6;3,7;3,8;4,5;4,6;4,7;4,8;5,6;5,7;5,8;6,7;6,8;7,8];
  
  % this the dust scaling (for HFI) to the B2 passband according to
  % http://b2p.planck.fr/index.php/AnalysisLogbook/2014-08-14DustConversion
  % for the auto spectra.
  for kk=[1,2]
    if kk==2
      finalopt.spec_scale = 1./[1,1,1,1,0.424885357233,0.900287214698,3.12985190361,25.1198188496];
    else
        finalopt.spec_scale = [];
    end      
    
    for jj=1:length(rs)
      % no scaling for jackf
      if kk==2 && ~isempty(strfind(rs{jj},'jackf'))
        continue
      end
      r = rs{jj};
      for noxdb = [0]
        finalopt.noxdb=noxdb
      
        rc = [r,'_directbpwf'];
        if ~isempty(finalopt.spec_scale)
          rc = strrep(rc,'directbpwf','scaled_directbpwf');
        end
        if finalopt.noxdb
          rc = strrep(rc,'directbpwf','noxdb_directbpwf');
        end
        
        % run the one which saves the file by hand:
        reduc_final(r,finalopt,0,500)
        cmd=[...
          'reduc_final(r,finalopt,2,200);close all;',...
          'reduc_final(r,finalopt,2,500);close all;',...
          'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
          'reduc_final_chi2(rc,rfcopt,500,2);close all;'];         
        for jj = 1:size(pl_cross,1)
          rfcopt.pl_cross=pl_cross(jj,:);
          finalopt.pl_cross=rfcopt.pl_cross;
          farmit('farmfiles/final/',cmd,'var',{'r','rc','rfcopt','finalopt'},'queue','serial_requeue,general','mem',6000,'maxtime',30,'submit',0);
        end
      end
    end
    
    % the spectral jack
    r = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0_real_',daughter,'_filtp3_weight3_gs_dp1100_jack0_pureB_matrix'];
    rfcopt.diffspec = 1;
    for noxdb = [0]
      rc = [r,'_directbpwf'];
      if ~isempty(finalopt.spec_scale)
        rc = strrep(rc,'directbpwf','scaled_directbpwf');
      end
      if noxdb
        rc = strrep(rc,'directbpwf','noxdb_directbpwf');
      end  
      cmd=[...
        'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
        'reduc_final_chi2(rc,rfcopt,500,2);close all;'];         
      for jj = 1:size(pl_cross,1)
        rfcopt.pl_cross=pl_cross(jj,:);
        finalopt.pl_cross=rfcopt.pl_cross;
        farmit('farmfiles/final/',cmd,'var',{'r','rc','rfcopt','finalopt'},'queue','serial_requeue,general','mem',6000,'maxtime',30,'submit',0);
      end
    end
    rfcopt.diffspec = 0;
  end
  
  % the cross spectra of the split halfs
  clear finalopt rfcopt;
  finalopt.doblind = 0;
  finalopt.mapname={[mapname,'H1'],[mapname,'H2'],'P030H1','P030H2','P044H1','P044H2','P070H1','P070H2','P100H1','P100H2','P143H1','P143H2','P217H1','P217H2','P353H1','P353H2'};
  finalopt.supfacstyle='direct_bpwf';
  finalopt.bpwfname=get_bpwf();

  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  rfcopt.mapname=finalopt.mapname;
  finalopt.force_size=[];
      
  pl_cross = [1,2;3,4;5,6;7,8;9,10;11,12;13,14;15,16];
  % here it says jack3,4 but it is actually all signal spectra. We
  % have to tell reduc_final explictly not to look for jack0:
  
  for j='345'
    r = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack',j,'_real_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    finalopt.ssimset  = [snI,'x',snF,'/xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    finalopt.bosimset = [snI,'x',snF,'/xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    finalopt.nsimset  = [snI,'x',snF,'/xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    finalopt.snsimset = [snI,'x',snF,'/xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    finalopt.snrsimset= [snI,'x',snF,'/xxx9_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_xxx9_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    rc = [r,'_directbpwf'];
    
    reduc_final(r,finalopt,0,500)
    cmd=[...
        'reduc_final(r,finalopt,2,200);close all;',...
        'reduc_final(r,finalopt,2,500);close all;',...
        'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
        'reduc_final_chi2(rc,rfcopt,500,2);close all;'];

    for jj = 1:size(pl_cross,1)
      rfcopt.pl_cross=pl_cross(jj,:);
      finalopt.pl_cross=rfcopt.pl_cross;       
      farmit('farmfiles/final/',cmd,'var',{'r','rc','rfcopt','finalopt'},'queue','serial_requeue,general','mem',6000,'maxtime',30);
    end  
  end %j

  % the cross half jack (jamie's stuff)
  clear finalopt, rfcopt;
  finalopt.doblind = 0;
  finalopt.mapname={mapname,'P030Jack','P044Jack','P070Jack','P100Jack','P143Jack','P217Jack','P353Jack'}
  finalopt.supfacstyle='direct_bpwf';
  finalopt.bpwfname=get_bpwf();

  finalopt.force_size=[1,repmat(2,1,7),repmat(3,1,28)];
  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  rfcopt.mapname=finalopt.mapname;
  pl_cross = [1,2;1,3;1,4;1,5;1,6;1,7;1,8];
  
  for kk=1:2
    if kk==1
      rfcopt.simr=true;
    else
      rfcopt.simr=false;
    end
    for j='34'
    
      r = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0_real_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix'];
      finalopt.ssimset  = [snI,'x',snF,'/xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack0_pureB_matrix'];
      finalopt.bosimset = [snI,'x',snF,'/xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack0_pureB_matrix'];
      finalopt.nsimset  = [snI,'x',snF,'/xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix'];
      finalopt.snsimset = [snI,'x',snF,'/xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix'];
      finalopt.snrsimset= [snI,'x',snF,'/xxx9_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx9_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix'];
      rc = [r,'_directbpwf'];
      
      reduc_final(r,finalopt,0,500)
      cmd=[...
          'reduc_final(r,finalopt,2,200);close all;',...
          'reduc_final(r,finalopt,2,500);close all;',...
          'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
          'reduc_final_chi2(rc,rfcopt,500,2);close all;'];

      for jj = 1:size(pl_cross,1)
        rfcopt.pl_cross=pl_cross(jj,:);
        finalopt.pl_cross=rfcopt.pl_cross;       
        farmit('farmfiles/final/',cmd,'var',{'r','rc','rfcopt','finalopt'},'queue','serial_requeue,general','mem',6000,'maxtime',30);
      end
    end

  end
  
  %do KeckxS1
  clear finalopt, rfcopt;
  finalopt.doblind = 0;
  finalopt.mapname={mapname,'P030H1','P030H2','P044H1','P044H2','P070H1','P070H2','P100H1','P100H2','P143H1','P143H2','P217H1','P217H2','P353H1','P353H2'};
  finalopt.supfacstyle='direct_bpwf';
  finalopt.bpwfname=get_bpwf();

  finalopt.force_size=[];
  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  rfcopt.mapname=finalopt.mapname;
  % only do the spectral jack
  rfcopt.diffspec = 0; %1
  % finalopt.spec_scale = 1./[1,1,1,1,1,1,1,1,0.424885357233,0.424885357233,0.900287214698,0.900287214698,3.12985190361,3.12985190361,25.1198188496,25.1198188496];
  finalopt.spec_scale = []
  
  pl_cross = [1,17,18;1,19,20;1,21,22;1,23,24;1,25,26;1,27,28;1,29,30];
  % here it says jack3,4 but it is actually all signal spectra. We
  % have to tell reduc_final explictly not to look for jack0:    
  for j='345'
  
    r = [snI,'x',snF,'/real_',daughter,'_filtp3_weight3_gs_dp1102_jack0_real_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    finalopt.ssimset  = [snI,'x',snF,'/xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx2_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    finalopt.bosimset = [snI,'x',snF,'/xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx4_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    finalopt.nsimset  = [snI,'x',snF,'/xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx6_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    finalopt.snsimset = [snI,'x',snF,'/xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx7_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    finalopt.snrsimset= [snI,'x',snF,'/xxx9_',daughter,'_filtp3_weight3_gs_dp1100_jack0_xxx9_',daughter,'_filtp3_weight3_gs_dp1100_jack',j,'_pureB_matrix_nonej'];
    rc = [r,'_directbpwf'];
    if ~isempty(finalopt.spec_scale)
      rc = strrep(rc,'directbpwf','scaled_directbpwf');
    end
    finalopt.pl_cross=[1,2];
    
    reduc_final(r,finalopt,0,500)
    cmd=[...
        'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
        'reduc_final_chi2(rc,rfcopt,500,2);close all;'];

    for jj = 1:size(pl_cross,1)
      rfcopt.pl_cross=pl_cross(jj,:);
      finalopt.pl_cross=rfcopt.pl_cross;
      farmit('farmfiles/final/',cmd,'var',{'r','rc','rfcopt','finalopt'},'queue','serial_requeue,general','mem',6000,'maxtime',30);
    end
  
  end %j 

  babysitsims('final/*.mat','wait5',0,0)
  
return

function do_cook(outbeam)
%  function do_cook(outbeam)
%  outbeam is the beam profile to smooth to

    keyboard
    run_local=0;
    
    % full map
    ff = listfiles('input_maps/b2planck_140806/maps/*_[123]??GHz*FULL.fits.gz');
    ff = [ff;listfiles('input_maps/b2planck_140811/maps/*FULL.fits.gz')]
    prep_fullsky_map_farm_loop(run_local,ff,outbeam)
    
    % year splits - assume full beam
    ff = listfiles('input_maps/b2planck_140806/maps/*_[123]??GHz*Y?.fits.gz');
    ff = [ff;listfiles('input_maps/b2planck_140811/maps/*Y?.fits.gz')]
    prep_fullsky_map_farm_loop(run_local,ff,outbeam)
    
    % detector splits
    ff = listfiles('input_maps/b2planck_140806/maps/*_DX11d_DS*.fits.gz')
    prep_fullsky_map_farm_loop(run_local,ff,outbeam)
    
    % horn splits
    ff = listfiles('input_maps/b2planck_140811/maps/*DX11D_*2[1-6].fits.gz')
    prep_fullsky_map_farm_loop(run_local,ff,outbeam)
    
    % half ring splits
    ff = listfiles('input_maps/b2planck_140912/maps/*DX11?_FULL_HR*.fits.gz')
    prep_fullsky_map_farm_loop(run_local,ff,outbeam)
          
    % noise FFP8 full, 30, 44 and 70 broken due to wrong fit header
    ff = listfiles('input_maps/b2planck_140806/sims/*[123]??GHz_FFP8_FULL_noise_MC_00???.fits.gz')
    prep_fullsky_map_farm_loop(run_local,ff,outbeam)
        
    % finally FFP8 noise for 353, all types
    ff = listfiles('input_maps/b2planck_140901/sims/*353GHz_FFP8_*_MC_00???.fits.gz')
    prep_fullsky_map_farm_loop(run_local,ff,outbeam)
    
    % redo FFP8 noise for 353 DS2
    ff = listfiles('input_maps/b2planck_140906/sims/*353GHz_FFP8_*_MC_00???.fits.gz')
    prep_fullsky_map_farm_loop(run_local,ff,outbeam)
    
    % extend noise to 500 sims
    ff = listfiles('input_maps/b2planck_140917/sims/*_FFP8_*_noise_MC_00???.fits.gz')
    prep_fullsky_map_farm_loop(run_local,ff,outbeam) 
        
    % old FFP7 for 353 full
    % ff = listfiles('input_maps/b2planck_140724/*353GHz_FFP7_FULL_noise_MC_000??.fits')
    % prep_fullsky_map_farm_loop(run_local,ff,outbeam)
    % this is dangerous as it only must be done once.
    % ff = listfiles('input_maps/b2planck_140724/*MC*1280_bB2bbns.fits');
    % for ii=1:length(ff)
    %   lash_norm(ff{ii});
    % end
    
    % noise FF8 full for various splits
    ff = listfiles('input_maps/b2planck_140819/sims/*.fits.gz')
    prep_fullsky_map_farm_loop(run_local,ff,outbeam)
    
    % noise for half ring splits <100
    ff = listfiles('input_maps/b2planck_140912/sims/*FULL_noise*HR?.fits.gz')
    prep_fullsky_map_farm_loop(run_local,ff,outbeam)
    % 100 +
    ff = listfiles('input_maps/b2planck_141024/sims/*FULL_noise*HR?.fits.gz')
    prep_fullsky_map_farm_loop(run_local,ff,outbeam)
return

function uk = get_local_ukpervolt()
  global expt;
  switch expt
   case 'K13'
    uk = 1;
   case 'BK13'
    uk = 1;
   case 'B12'
    uk = get_ukpervolt();
   otherwise
    error('Don''t know which uk to use');
  end
return

function pm = get_purification_matrix()
  global expt;
  switch expt
   case 'K13'
    pm = '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat';
   case 'BK13'
    pm = '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat';
   case 'B12'
    pm = 'matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat';
   otherwise
    error('Don''t know which PM to use');
  end
return

function bp = get_bpwf()
  global expt;
  switch expt
   case 'K13'
    bp = '1332/xxxx_cmb_subset2012_2013_rxall_filtp3_weight3_gs_dp1100_jack0_matrix_directbpwf.mat';
   case 'BK13'
    bp = '1332/xxxx_cmb_subset2012_2013_rxall_filtp3_weight3_gs_dp1100_jack0_matrix_directbpwf.mat';
   case 'B12'
    bp = '0704/xxxx_allcmb_filtp3_weight3_gs_dp1100_jack0_matrix_directbpwf.mat';
   otherwise
    error('Don''t know which BPWF to use');
  end
return

function rm = get_reobs_matrix() 
  global expt;
  switch expt
   case 'K13'
    rm = '/n/bicepfs2/keck/pipeline/matrixdata/matrices/0706/real_cmb_subset2012_2013_rxall_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat';
   case 'BK13'
    rm = '/n/bicepfs2/keck/pipeline/matrixdata/matrices/0706/real_cmb_subset2012_2013_rxall_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat';
   case 'B12'
    rm = 'matrixdata/matrices/0704/real_allcmb_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat';
   case 'K2014_95'
    rm = '/n/panlfs2/bicep/keck/pipeline/matrixdata/matrices/1351/real_d_100GHz_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat';
   case 'BK14_150'
    rm = '/n/panlfs2/bicep/keck/pipeline/matrixdata/matrices/1459/real_aabd_150GHz_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat';
   otherwise
    error('Don''t know which RM to use');
  end
return


function in_beam = find_beams_file(in_file)
  % a jig for the beams file:
  in_beam  = 'input_maps/b2planck_DDD/beams/Planck_B_ell_FFFGHz_DX11d_SSS.fits';
  ind = strfind(in_file,'GHz'); FFF = in_file(ind-3:ind-1);
  if isempty(strfind(in_file,'_DS'))
    SSS = 'FULL';
  else
    SSS = ['DS',in_file(strfind(in_file,'_DS')+3)];
  end  
  if str2num(FFF)<100
    DDD = '140811';
  else
    DDD = '140806';
  end
  
  in_beam = strrep(in_beam,'DDD',DDD);
  in_beam = strrep(in_beam,'FFF',FFF);
  in_beam = strrep(in_beam,'SSS',SSS);
  
  if ~exist(in_beam,'file')
    error(['File ',in_beam,' does not exist.'])
  end
  
return

function prep_fullsky_map_farm_loop(run_local,ff,outbeam)
  for ii=1:length(ff)
    in_file = ff{ii};
    try
      in_beam = find_beams_file(in_file);
    catch
      warning('no existing beam file')
      continue
    end
    prep_fullsky_map_farm(in_file,in_beam,outbeam,run_local)
  end
return

function prep_fullsky_map_farm(in_file,in_beam,out_beam,run_local) 
  % use defaults
  nside = []; nlmax = []; cut = []; coord = [];
  
  % the nlmax field can either be [] or if we know the final lmax set to that (1280)
  % if nlmax is set this speeds up the look up for missing files.
  fn = prep_fullsky_map(in_file,nside,1280,out_beam,cut,coord,in_beam,1);
  if ~exist(fn,'file')
    cmd = 'prep_fullsky_map(in_file,nside,nlmax,out_beam,cut,coord,in_beam)';
    if ~run_local
      farmit('farmfiles/planck/',cmd,...
      'var',{'in_file','nside','nlmax','out_beam','cut','coord','in_beam'},...
      'queue','general,serial_requeue,itc_cluster','maxtime',65,'mem',4000,'submit',0)
      disp(['Submited ',fn])
    else
      eval(cmd)
    end
  else
    disp(['Skip over existing ',fn])
  end
return

function cmfn = get_planck_filename(mfn,fs)
%  FRQ  FUL     DS1     DS2     YR1     YR2     YR3     YR4     H24     H65     H83     H92     H01     NFU     NS1     NS2     NY1     NY2     NY3     NY4     N24     N65     N83     N92     N01 
%  030 140811                  140811  140811  140811  140811                                           140811                  140819  140819  140819  140819                      
%  044 140811                  140811  140811  140811  140811  140811  140811                           140811                  140819  140819  140819  140819                      
%  070 140811                  140811  140811  140811  140811                   140811  140811  140811  140811                  140819  140819  140819  140819                  140819  140819  140819  
%  100 140806  140806  140806  140806  140806                                                           140806  140819  140819  140819  140819                              
%  143 140806  140806  140806  140806  140806                                                           140806  140819  140819  140819  140819                              
%  217 140806  140806  140806  140806  140806                                                           140806  140819  140819  140819  140819                              
%  353 140806  140806  140806  140806  140806                                                           140901  140901  140906  140901  140901                              
%                                                                                                       -- 101-499 in 140917

  cmfn = strrep(mfn,'XXX',fs);
  if ~isempty(strfind(cmfn,'noise'))
    rlz = str2num(mfn(strfind(mfn,'noise_MC_')+9:strfind(mfn,'noise_MC_')+13));
  end

  if ~isempty(strfind(cmfn,'noise')) & isempty(strfind(cmfn,'HR'))
    if rlz>99
      cmfn = strrep(cmfn,'DDDDDD','140917');
      return
    end
  end
  
  if str2num(fs)<100
    cmfn = strrep(cmfn,'DX11d','DX11D');
  end
  
  if ~isempty(strfind(cmfn,'HR')) 
    if isempty(strfind(cmfn,'noise')) | rlz<100
      cmfn = strrep(cmfn,'DDDDDD','140912');
    else
      cmfn = strrep(cmfn,'DDDDDD','141024');
    end
  end
  
  % the LFI
  if str2num(fs)<100 && isempty(strfind(cmfn,'noise'))
    cmfn = strrep(cmfn,'DDDDDD','140811');
  elseif str2num(fs)<100 && ~isempty(strfind(cmfn,'noise')) && ~isempty(strfind(cmfn,'FULL'))  
    cmfn = strrep(cmfn,'DDDDDD','140811');
  elseif str2num(fs)<100 && ~isempty(strfind(cmfn,'noise')) && isempty(strfind(cmfn,'FULL'))  
    cmfn = strrep(cmfn,'DDDDDD','140819');
  
  % the HFI besides 353 noise
  elseif str2num(fs)>=100 && isempty(strfind(cmfn,'noise'))
    cmfn = strrep(cmfn,'DDDDDD','140806');
  elseif str2num(fs)>=100 && str2num(fs)~=353 && ~isempty(strfind(cmfn,'noise')) && ~isempty(strfind(cmfn,'FULL'))  
    cmfn = strrep(cmfn,'DDDDDD','140806');
  elseif str2num(fs)>=100 && str2num(fs)~=353 && ~isempty(strfind(cmfn,'noise')) && isempty(strfind(cmfn,'FULL'))  
    cmfn = strrep(cmfn,'DDDDDD','140819');
  
  % 353 noise - might need adjustment if DS2 is fixed
  elseif str2num(fs)==353 && ~isempty(strfind(cmfn,'noise')) && isempty(strfind(cmfn,'DS2'))
    cmfn = strrep(cmfn,'DDDDDD','140901');
  % change this portion to where the fixed DS2 353 noise is:
  elseif str2num(fs)==353 && ~isempty(strfind(cmfn,'noise')) && ~isempty(strfind(cmfn,'DS2'))
    cmfn = strrep(cmfn,'DDDDDD','140906');  
  end
  
return
