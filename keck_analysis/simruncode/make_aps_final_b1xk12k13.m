function make_aps_final(nbase1,nbase2,daughter1,daughter2,make_aps_real,make_aps,make_simset,make_final,coaddtype,queue)
% function make_aps_final(nbase1,nbase2,daughter1,daughter2,make_aps_real,make_aps,make_simset,make_final,coaddtype,queue)
%
% A script that makes the aps and final plots for Keck/BICEP2
% modified from Sarah's make_aps_final_1450.m (JPK)
%
% cross spectra are made via so nbase1_daughter1xnbase2_daughter2
% auto spectra are made when nbase2=[] - set up for B2 only up to now
% 
% Options:
% nbase1, nbase2 (defaults 0751, 1351) 
%          The sernums for the relevant files
% daughter1, daughter1 (defaults 'a','a') 
%          For the respective sernums, 'b'=2013 Keck
% make_aps_real (default 0)
%          Makes the real aps.  farms bpwf for jack0
% make_aps (default 0)
%          Makes sims cross aps.
% make_simset (default 0)
%          turns sims into simset
% make_final (default 0)
%          makes the reduc_final and reduc_final_chi2 pager plots
% coaddtype (default 1)
%          The coaddtype.  if 1, only Keck is modified.
%          If 5, only Keck2012 is modified.
% queue (default 1)
%          The queue to use.  0 for LSF.  1 for serial_requeue
%

if ~exist('nbase1','var')
  nbase1=1351;
end
if ~exist('nbase2','var')
  nbase2=1651;
end
if ~exist('daughter1','var')
  daughter1='ab';
end
if ~exist('daughter2','var')
  daughter2='a';
end
if ~exist('make_aps_real','var')
  make_aps_real=0;
end
if ~exist('make_aps','var')
  make_aps=0;
end
if ~exist('make_final','var')
  make_final=0;
end
if ~exist('make_simset','var')
  make_simset=0;
end
if ~exist('coaddtype','var')
  coaddtype=1;
end
if ~exist('queue','var')
  queue=1;
end

% these are used all over the following loops
% just drag them into the workspace:
types= '245679';
deprojs={'1100'};
deprojsreal={'1102'};
if nbase1==1353
  deprojsreal={'11022'};
end
jacks='0';
% auto spectrum
estimators = {'_overrx','_pureB_overrx','_pureB_matrix_overrx','_matrix_overrx'};
purebs = {'normal','kendrick'};

%Choose the correct queue
if queue
  queue_small='serial_requeue';
  queue_large='serial_requeue';
  license='--licenses=bicepfs1:1';
else
  queue_small='short_serial';
  queue_large='normal_serial';
  license='';
end

%initialize apsopt
apsopt.update = 1;
apsopt.save_coaddopts=0;
maxtime=[];

%initialize coaddtype
  ct1='1'; ct2='';
  ct1s='1'; ct2s=''; 

%initialize purification matrix names for the different cases
% also initialize scale factors...
purifmatname_overrx={'/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat',''};
%scalefac={1*ones(1,3),1*ones(1,3),sqrt(2)*ones(1,3)};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cross spectra for real, includes bpwf and experiment jack
% the jack0 run for many hours since it will calculate the bpwf as well.

%setup the abscal per pair stuff
if make_aps_real

  %finish initializing apsopt
  apsopt.makebpwf=0;
  coaddrxs=[1];
  doauto=0;
  apsopt.polrot=[-0.5,-2.27,-2.91];
  apsopt.ukpervolt={1,[-286700000,-228100000]};
  for deproj = deprojsreal
    for coaddrx=coaddrxs
      for pure_b = purebs;
        for jack=jacks
          if strcmp(jack,'0')
            apsopt.makebpwf=0;
            apsopt.update=0;
            maxtime=[];
            queue_use=queue_large; 
          else
            apsopt.makebpwf=0;
            apsopt.update=1;
            maxtime=[];
            queue_use=queue_small;
          end
          
          % Loop through with and without the matrix for jack0 only
          domat=0;
          if strcmp(jack,'0')
            domat=[0 1];
          end

          for mat=domat

            % Setup below for matrix purification
            if mat
              % add the matrix names
              if ~coaddrx
                apsopt.purifmatname=purifmatname;
              else
                apsopt.purifmatname=purifmatname_overrx;
              end
              mem=30000; queue_use=queue_large;
            else
              % make sure purifmatname is not a field
              if isfield(apsopt,'purifmatname')
                apsopt=rmfield(apsopt, 'purifmatname');
              end
              mem=10000;
            end

            apsopt.pure_b = pure_b{:};
            apsopt.coaddrx=coaddrx;
            apsopt.howtojack='dim2';

            % compile the base filename, the replacement string and the cmd 
            fname1=[num2str(nbase1,'%04d'),'/real_' daughter1 '_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1,'.mat'];
            replacementstr=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase2,'%04d') ''',''',ct1,'.mat'',''',ct2,'.mat'',''_',daughter1,'_'',''_',daughter2,'_'',''', deproj{:},''',''1000'''];
            cmd = ['reduc_makeaps({''',fname1,''',{',replacementstr,'}},apsopt)'];
            farmit('farmfiles/realaps/',cmd,'var','apsopt','queue',queue_use,'mem',mem,'maxtime',maxtime,'args',license);  

          end
        end
      end
    end
  end
end

% cross spectra for the sims
if make_aps
  apsopt.update=1;
  apsopt.makebpwf=0;
  apsopt.polrot=[];
  coaddrxs=[1];
  doauto=0;
  apsopt.ukpervolt={1,[-286700000,-228100000]};
  for coaddrx=coaddrxs
    for type = types
      for pure_b = purebs;
        for jack=jacks
          apsopt.coaddrx=coaddrx;
          apsopt.pure_b=pure_b{:};
          
          % Loop through with and without the matrix for jack0 only
          domat=0;
          if strcmp(jack,'0')
            domat=[0 1];
          end

          for mat=domat

            % Setup below for matrix purification
            if mat
              % add the matrix names
              if ~coaddrx
                apsopt.purifmatname=purifmatname;
              else
                apsopt.purifmatname=purifmatname_overrx;
              end
              mem=30000;
            else
              % make sure purifmatname is not a field
              if isfield(apsopt,'purifmatname')
                apsopt=rmfield(apsopt, 'purifmatname');
              end
              mem=10000;
            end

            fname1=[num2str(nbase1,'%04d'),'/[01]??',type,'_' daughter1 '_filtp3_weight3_gs_dp1100_jack',jack,ct1s,'.mat'];
            replacementstr=['''',num2str(nbase1,'%04d'),''',''' num2str(nbase2,'%04d') ''',''',ct1s,'.mat'',''',ct2s,'.mat'',''_',daughter1,'_'',''_',daughter2,'_'',''dp1100'',''dp1000'''];
            cmd = ['reduc_makeaps({''',fname1,''',{',replacementstr,'}},apsopt)'];
        
            apsopt.howtojack='dim2';
            farmit('farmfiles/aps/',cmd,'var','apsopt','queue',queue_small,'mem',mem,'maxtime',maxtime,'args',license);

          end
        end
      end   
    end
  end
end

% simsets cross:
if make_simset
  doauto=0;
  folder=[num2str(nbase1,'%04d') 'x' num2str(nbase2,'%04d')];
  for jack=jacks
    for type = types
      for deproj = deprojs
        estimators_loop=estimators;
        for estimator=estimators_loop
          fname=[folder '/[01]??',type,'_',daughter1,'_filtp3_weight3_gs_dp',deproj{:},'_jack',jack,ct1s,...
          '_[01]??',type,'_',daughter2,'_filtp3_weight3_gs_dp1000_jack',jack,ct2s,estimator{:},'.mat'];
          cmd = ['reduc_makesimset(''' fname ''')'];
          farmit('farmfiles/simset/',cmd,'queue',queue_small,'mem',10000,'maxtime',60,'args',license);
        end
      end
    end
  end
end

% reduc_final and reduc_final_chi2 cross:
if make_final
  doauto=0;
  directbpwfs=0;
  %  pl_cross selects the rx combination to plot
  pl_crosses = [1,2; 1,3]; %that's ok
  rfcopt.dia = 0;
  rfcopt.doblind = 0;
  folder=[num2str(nbase1,'%04d') 'x' num2str(nbase2,'%04d')];
  folders=strrep(folder,'1353','1351');
  for jack=jacks
    for deproj = deprojsreal
      for directbpwf=directbpwfs
        estimators_loop=estimators;
        for estimator=estimators_loop
          rbcs=[0]; % residual beam correction in few cases
          for rbc=rbcs
            % if jack0 then also do the spectral jack:
            diffspec=0;
            for pl_cross=pl_crosses'
              rots=[1];
              if strcmp(jack,'0') && diffspec==0 && ~isempty(strfind(estimator{:},'overall'))
                rots=[0,1];
              end
              for rot = rots              
                clear finalopt;
                rfcopt.diffspec = diffspec;            
                
                deprojS=deproj{:};
                % when subtraction instead of regression in real data, 
                % use the undeprojected sim but just in the case that the deprojected
                % systematic was not simulated (diff ell yes but not diff pointing):
                if strcmp(deproj{:},'1202') continue; end
                if strcmp(deproj{:},'1102') deprojS='1100'; end
                if strcmp(deproj{:},'2102') deprojS='0100'; end
                if length(deproj{:})==5; deprojS='1100'; end
                
                r = sprintf([folder '/real_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1  '_real_' daughter2 '_filtp3_weight3_gs_dp1000_jackJ' ct2,'BB' ]); %5 for the per-tile abscal
                n = sprintf([folders '/xxx6_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx6_' daughter2 '_filtp3_weight3_gs_dp1000_jackJ' ct2s, 'BB']); %signflip
                s = sprintf([folders '/xxx2_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s '_xxx2_' daughter2 '_filtp3_weight3_gs_dp1000_jack0' ct2s,'BB']); %yes, always jack0, unlensed
                sn= sprintf([folders '/xxx7_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx7_' daughter2 '_filtp3_weight3_gs_dp1000_jackJ' ct2s,'BB']); %lensed
                b = sprintf([folders '/xxx4_' daughter1 '_filtp3_weight3_gs_dpDDDD_jack0' ct1s '_xxx4_' daughter2 '_filtp3_weight3_gs_dp1000_jack0' ct2s,'BB']); %yes, always jack0
                snr=sprintf([folders '/xxx9_' daughter1 '_filtp3_weight3_gs_dpDDDD_jackJ' ct1s '_xxx9_' daughter2 '_filtp3_weight3_gs_dp1000_jackJ' ct2s,'BB']); %lensed
                
                r = strrep(r,'DDDD',deproj{:}); r = strrep(r,'BB',estimator{:}); r = strrep(r,'J',jack);
                n = strrep(n,'DDDD',deprojS); n = strrep(n,'BB',estimator{:}); n = strrep(n,'J',jack);
                s = strrep(s,'DDDD',deprojS); s = strrep(s,'BB',estimator{:}); s = strrep(s,'J',jack);
                sn= strrep(sn,'DDDD',deprojS); sn= strrep(sn,'BB',estimator{:}); sn= strrep(sn,'J',jack);
                b = strrep(b,'DDDD',deprojS); b = strrep(b,'BB',estimator{:}); b = strrep(b,'J',jack);
                snr= strrep(snr,'DDDD',deprojS); snr= strrep(snr,'BB',estimator{:}); snr= strrep(snr,'J',jack);

                % now all stored in finalopt
                finalopt.nsimset=n; finalopt.ssimset=s; finalopt.snsimset=sn; finalopt.bosimset=b; finalopt.snrsimset=snr;
              
                % for b2 auto also use the direct bpwfs
                rc = r;

                % define the farm jobs.  does the final plots for max ell=200,500,
                % saves the final file, and does chi2 plot for max ell=200,500
                cmd=[...
                'reduc_final(r,finalopt,2,200);close all;',...
                'reduc_final(r,finalopt,2,500);close all;',...
                'reduc_final(r,finalopt,0,500);close all;',...
                'reduc_final_chi2(rc,rfcopt,200,2);close all;',...
                'reduc_final_chi2(rc,rfcopt,500,2);close all;'];

                rfcopt.pl_cross=pl_cross'; finalopt.pl_cross=rfcopt.pl_cross;
                farmit('farmfiles/final/',cmd,'var',{'r','rc','n','s','sn','b','rfcopt','finalopt'},'queue',queue_small,'mem',16000,'maxtime',60,'args',license);
                
              end  %loops
            end
          end
        end
      end
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%
% After running, combine the b2xb1 and b1xk spectra via:

%{
load('final/0751x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix.mat');
rb2=r;
load('final/1353x1651/real_ab_filtp3_weight3_gs_dp11022_jack01_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix_overrx.mat');
rbk=r;
ro=comb_spec(rb2(4),rbk(4));
r=ro;
save('final/0751x1353x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_ab_filtp3_weight3_gs_dp11022_jack01_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix_overrx_comb.mat');
%}

