<html><head><title>BICEP2 data cut mechanism</title></head><body>

<BODY TEXT="#000000" BGCOLOR="#F0F0F0">

<center>
<h2>BICEP2 data cut proposal</h2>
</center>

<h3>Goals</h3>
<p>
BICEP2 data cuts should be implemented in a way that is intended to meet the
following requirements:

<ul><li>easy to add, modify, or remove cuts</li>
    <li>cuts can be easily used in reducplots, livetime calculations,
	etc.</li>
    <li>cuts always consistent with data</li>
    <li>cuts calculated at mapmaking time, not saved as bitmasks</li>
    <li>cuts may apply per-channel, per-half-scan, both, or neither</li>
    <li>easy to tweak cut thresholds</li>
    <li>as far as possible, cuts can be imposed or modified
        at the coadd stage.</li>
</ul>
</p>
<hr>

<h3>Overview of proposed scheme</h3>
<p>
Each cut is calculated in three stages:
<ul><li>Calculating a figure of merit or cut parameter --
    <a href="../calc_cutparams.html">calc_cutparams.m</a></li>
    <li>Comparing the cut parameters with a specified threshold or tolerance --
    <a href="../eval_cuts.html">eval_cuts.m</a></li>
    <li>Applying the cut mask to the data --
    <a href="../apply_cuts.html">apply_cuts.m</a></li>
</ul>
These three steps are all performed when
<a href="../reduc_makepairmaps.html">reduc_makepairmaps.m</a>
is run.  The parameters and cut masks are not stored as masks or
external files that need to be updated when the data is reprocessed.
</p>
<p>
Each cut parameter or figure of merit is defined by a script
in the <code>cutparams/</code> directory.  Once the script exists,
it is immediately available for use in mapmaking.  Threshold
values or tolerances are specified as fields in the
<code><a href="../struct_ref.html">mapopt</a></code> structure.
</p>

<hr>
<h3>Setting a threshold</h3>
<p>
Each parameter can be cut with a maximum and/or minimum value.
Some examples:
<pre>
   mapopt.cut_nancount=[0 1];    % a single NaN is OK
   mapopt.cut_resp=[3000 7000];  % el nod responsivity 3000-7000
</pre>
If you want to set only a min or a max but not both, you can
give the other as NaN.  The name of the mapopt entry for a given
cut is cut_NAME, matching the name defined in the script
(see next section).
</p>
<p><em>
<b>Question:</b> Should it be possible to specify
different cut thresholds per-channel or per-FPU?
</em></p>
<p>
Any cut may be specified in the <code>mapopt</code> structure
at the time that <code>reduc_makepairmaps</code> is run.  Some
cuts may also be specified when <code>reduc_coaddpairmaps</code>
is run.  However, there are two restrictions:
<ol><li>A cut that's been applied in makepairmaps can only be
        tightened, not loosened, in coaddpairmaps.</li>
    <li>A cut that's per-half-scan cannot be applied in
	coaddpairmaps, since the half-scans have already been
	coadded.</li>
</ol>
Restriction #1 only applies when a cut on the same parameter
is specified in both places.  It's possible to specify a cut
in coaddpairmaps on a parameter that hasn't been used in makepairmaps.
</p>

<hr>
<h3>Creating a new cut parameter</h3>
<p>
The cut parameters are defined by simple scripts in the <code>cutparams/</code>
directory.  These need to specify the name and dimensions of the cut,
and calculate the value of the quality parameter.  They are allowed to
use a few variables that are available inside the script:
<ul><li>tag -- string with the tag name</li>
    <li>nhs -- number of half-scans in the data</li>
    <li>nfpu -- number of FPUs in the data</li>
    <li>nch -- number of detector channels in the data</li>
    <li>d -- structure as loaded from a TOD file, with fields
        <ul><li>d.d -- registers</li>
            <li>d.ind -- channel indices from get_array_info</li>
            <li>d.fs -- field-scan locator indices</li>
            <li>d.en -- el nod indices, gain values</li>
        </ul></li>
    <li>tod_date -- Matlab datenum corresponding to the time stamp in the tag name.</li>
</ul>
Each script should set the following values:
<ul><li>name -- name of the parameter.  File name must be cutparam_NAME.m.</li>
    <li>perch -- 1 if cut is to be per-channel, 0 otherwise.</li>
    <li>perfpu -- 1 if cut is to be per-fpu, 0 otherwise.  (Ignored if perch==1.)</li>
    <li>perhs -- 1 if cut is to be per-half-scan, 0 otherwise.</li>
    <li>val -- matrix of quality parameter values.  Size should be
	nhs &times; nch for a fine-grained cut (perhs=perch=1).</li>
</ul>
</p>
<p>
An example of a simple script to calculate as a cut parameter the number
of NaNs present in the data for each channel in each half-scan:
<pre>
% nancount.m
%
% This function calculates a BICEP2 cut parameter.
% It is called by calc_cut_params.m.

% Count of NaNs (e.g. from deglitching)
%   - half-scan x channel

% name of cut parameter
name='nancount';

% values per channel?  Yes
perch=1;

% Values per half-scan?  Yes
perhs=1;

% Calculate value from TOD file structure
val = zeros(nhs,nch);
for ihs=1:nhs
  if (d.fs.sf(ihs)&lt;=d.fs.ef(ihs)) &amp;&amp; (d.fs.sf(ihs)&gt;0) &amp;&amp; (d.fs.ef(ihs)&lt;length(d.d.t))
    val(ihs,:) = sum(isnan(d.d.mce0.data.fb(d.fs.sf(ihs):d.fs.ef(ihs),:)),1);
  end
end
</pre>
</p>
<p>
For by-hand cuts on known bad periods of data, the script could just calculate a one for tags
that fall within the period of time (using tod_date) and a zero otherwise.
</p>


<hr>
<h3>Questions about per-phase cuts</h3>
<p>
Since our plan is to coadd all the scansets in a phase during <code>reduc_makepairmaps</code>,
any cuts (or data selection) applied in <code>reduc_coaddpairmaps</code> must be per-phase.
Cuts applied at this stage may be per-channel, per-FPU, or all-channel.  However, it is somewhat
awkward to calculate data quality figures across an entire phase; the mapmaking code never
loads the data for an entire phase into memory at once.  This means that per-phase quality
figures will be limited to things that can be calculated per-scanset, and then combined in a
simple way such as taking a mean, median, or standard deviation.
</p>
<p>
<em><b>Question:</b> Is this enough?  How much freedom do we need in defining per-phase
quality figures?
</em></p>

<hr>
<h3>Record-keeping in mapopt</h3>
<p>
The mapopt file serves both as an input to mapmaking and as a record of what has been
done.  Some records about the cuts are stored in mapopt:
<ul><li>thresholds -- stored as specified, in cut_*.</li>
    <li>cutmask{} -- nhs &times; nch, 1==accept, 0==reject.</li>
    <li>cutsparams{} -- contains the values and other information returned by each cut parameter script, as a record.</li>
</p>

</body>
</html>
