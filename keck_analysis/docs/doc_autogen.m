% script to auto generate matlab documentation from  the help at the top of the matlab functions
% from bicep_analysis, in matlab run doc_autogen. That will
% generate in docs an html file which documents each of the routines in the matlab distribution.
%%this assumes the format of the files is :
%%%%FUNCTION definition without the leading %%%%
%%%%COMMENT
%%%%COMMENT
%%%%....
%%%%COMMENT


function doc_autogen

% identify the main pipeline directory.
pipeline_dir=which('reduc_initial');
if isempty(pipeline_dir) || strcmp(pipeline_dir,'variable')
  error(['Failed to identify main pipeline directory.']);
end
pipeline_dir=fileparts(pipeline_dir);

%move into the matlab directory and remove all file copies
cd(pipeline_dir);
unix('rm -Rf *~');

% List of subdirectories to search for functions.
SUBDIRS = {'.', 'read_dir', 'healpix', 'kendrick', 'util', ...
           'external_lib', 'examiner', ...
           'tes_analysis/file_io/arcfile', 'tes_analysis/util'};

% Settings to include a link to CVSWEB for each function
CVSSERVER = 'bicep0.caltech.edu';
CVSPATH = '/cgi-bin/cvsweb/cvsweb.cgi/bicep_analysis';
CVSARGS = 'f=u&only_with_tag=bicep2&logsort=date&cvsroot=find_mirror&hideattic=0';
cvsfmt = assemble_cvs_string(CVSSERVER,CVSPATH,CVSARGS);

% List files in specified subdirectories
disp(['Listing all files to document.']);
filelist={};
for i=1:length(SUBDIRS)
  tmpflist = dir(fullfile(SUBDIRS{i},'*.m'));
  for j=1:length(tmpflist)
    filelist = {filelist{:}, fullfile(SUBDIRS{i}, tmpflist(j).name)};
  end
end

% List all mutual dependencies of the functions we've found.
disp(['Searching for all mutual dependencies.']);
deps=search_all_deps(filelist);

for j=1:numel(filelist)
  file=filelist{j};
  [tmp filename_simple]=fileparts(file);
  if isempty(filename_simple) || sum(filename_simple=='.')>0
    continue;
  end

  htmlname=strcat('docs/',filename_simple,'.html');
  disp(htmlname);
  comment=help(filename_simple);
  comment=strread(comment,'%s','delimiter','\n','whitespace','');
  
  %create specific file with comments from that function.
  fid =  fopen(htmlname, 'w');
  write_htmlheader(fid,file);
  write_comment(fid, comment);
  calledby=deps.(filename_simple).calledby;
  write_calledby(fid,calledby);
  calls=deps.(filename_simple).calls;
  write_calls(fid,calls);
  if ~isempty (cvsfmt)
    write_cvslink(fid,file,cvsfmt);
  end
  write_htmlfooter(fid); 
  fclose(fid);
end

return

function cvsfmt=assemble_cvs_string(CVSSERVER,CVSPATH,CVSARGS)
% Assembles cvsweb settings into a format string
% for use with fprintf.
if ~isempty(CVSSERVER) & ~isempty(CVSPATH)
  if CVSSERVER(end)=='/'
    CVSSERVER=CVSSERVER(1:(end-1));
  end
  if CVSPATH(1) ~= '/'
    CVSPATH=['/' CVSPATH];
  end
  if CVSPATH(end) ~= '/'
    CVSPATH=[CVSPATH '/'];
  end
  if ~isempty(CVSARGS) && CVSARGS(1) ~= '?'
    CVSARGS = ['?' CVSARGS];
  end
  cvsfmt = ['http://' CVSSERVER CVSPATH '%s' CVSARGS];
else
  cvsfmt = '';
end

return

function deps=search_all_deps(filelist)
% Finds all mutual dependencies of the functions in filelist
% and assembles into a structure with calls and called_by fields.

% initialize empty structure
deps=[];
emptystruct.calls={};
emptystruct.calledby={};
for j=1:numel(filelist)
  [tmp bn]=fileparts(filelist{j});
  if sum(bn=='.')>0
    filelist{j}='';
    continue
  end
  deps.(bn)=emptystruct;
end

% spin through functions, noting what it calls in both the
% calls and calledby lists.
for j=1:numel(filelist)
  if isempty(filelist{j})
    continue
  end
  [tmp bn]=fileparts(filelist{j});
  tmplist=depfun(bn,'-toponly','-quiet');
  for k=1:length(tmplist)
    [tmp bn2]=fileparts(tmplist{k});
    if strcmp(bn,bn2)
      continue
    end
    if isfield(deps,bn2)
      deps.(bn2).calledby={deps.(bn2).calledby{:}, bn};
      deps.(bn).calls={deps.(bn).calls{:}, bn2};
    end
  end
end
 
return

function write_calledby(fid, calledby)
fprintf(fid,'<table border="0" width="100%%">');
fprintf(fid,'<tr valign="top">');
fprintf(fid,'<td>');
fprintf(fid,'<h3>Called&nbsp;by</h3>');
for i=1:numel(calledby)
  fprintf(fid,'<a href="./%s.html">%s</a> <br> ',calledby{i}, calledby{i});
end
fprintf(fid,'</td>\n');
% fprintf(fid,'<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>\n');

return

function write_calls(fid,calls)
fprintf(fid,'<td>');
fprintf(fid,'<h3>Calls</h3>');
for i=1:numel(calls)
  fprintf(fid,'<a href="./%s.html">%s</a> <br> ',calls{i}, calls{i});
end
fprintf(fid,'</td>');
fprintf(fid,'</tr>');
fprintf(fid,'</table>');
fprintf(fid,'<hr><hr>');

return

function write_cvslink(fid, fullfile, cvsfmt)
[dir fname fext] = fileparts(fullfile);
fprintf(fid,'<p>\n');
fprintf(fid,'<b>Browse CVS history:</b>\n');
fprintf(fid,['<a href="' cvsfmt '">%s history</a>\n'],fullfile,[fname fext]);
fprintf(fid,'</p>\n');

return

function write_htmlheader(fid, file)
%assuming the fid is alredy open

fprintf(fid,'<html><head><title>%s</title></head>\n',file);
fprintf(fid,'<BODY>\n');
fprintf(fid,'<center>\n');
fprintf(fid,'<h2>Autogen documentation for %s</h2>\n',file);
fprintf(fid,'%s\n',date);
fprintf(fid,'</center> <p><p>\n');

return

function write_htmlfooter(fid)
%assuming the fid is alredy open
  fprintf(fid,'<h3><a href="documentation.html">Back</a> </h3> ');
fprintf(fid,'</body></html>');

return

function write_comment(fid,comment)
%assuming fid is already open

fprintf(fid,'<hr><hr>\n');
fprintf(fid,'<h3>Help comments</h3>\n');
fprintf(fid,'<pre>\n');
for k=1:numel(comment)
  safe_cmt=comment{k};
  safe_cmt=strrep(safe_cmt,'&','&amp;');
  safe_cmt=strrep(safe_cmt,'<','&lt;');
  safe_cmt=strrep(safe_cmt,'>','&gt;');
  fprintf(fid,'%s \n',safe_cmt);
end
fprintf(fid,'</pre>\n');
fprintf(fid,'<hr><hr>');

return

function comment=read_comment(filename)
%this assumes the format of the files is :
%FUNCTION definition without the trailing %
%COMMENT
%COMMENT
%....
%COMMENT
%CODE without the trailing %

f=fopen(filename);
c=textscan(f,'%s','delimiter','\n');

i=2;
while  numel(c{1}{i}) ~=0
  if strcmp(c{1}{i}(1),'%') 
    i=i+1;
  else break
  end
end
comment=c{1}(1:i);

return
