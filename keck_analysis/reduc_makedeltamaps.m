function reduc_makedeltamaps(specs, mpls, rlzs, beam, matrix_name, sernum)
% reduc_makedeltamaps(specs, mpls, rlzs, beam, matrixname, sernum)
% creates an observed  map which has a delta peak at the given multipole mpls.
% specs     = 'E' - delta function in TT,TE and EE
%           = 'B' - delta function in TT and BB
%           = 'EB' - (default) loop through both
% mpls      = [10,20,30] (default)
% rlzs      = [0:100]; is passed to run_synfast where it will use the random seed list
% beam      = {get_bl_filename('B2bbns'),'B2bbns'} (default), 
%             can also be numeric to specify fwhm
% matrixname = (string) filename of reob matrix to use
% sernum     = (string) serial number for maps to live in, "maps/YYZZ/XXXE_blah.mat"
%              YY = usr code
%              ZZ = sim set number
%              XXX = ell
%              E/B = whether power is in TT,TB,BB or TT, TE,EE
%              if sernum is not specified, will take it from reob matrix
%
% eg:
% matrix_name='matrixdata/matrices/0704/real_allcmb_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat'
% reduc_makedeltamaps('EB',[0:10:500],[0:50],[],matrix_name)
% reduc_makedeltamaps('EB',[10:10:30],[0:10],[],matrix_name)
% reduc_makedeltamaps('EB',70,[0:3],[],matrix_name, '0704')
%
%  % keck 2012:
%  matrix_name = '/n/panlfs/bicep/keck/pipeline/matrixdata/matrices/0704/real_cmb201208-201209_rxall_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat'
%  reduc_makedeltamaps('EB',[0:600],[0:500],[],matrix_name,'1453')
%  % keck 2013:
%  matrix_name = '/n/panlfs/bicep/keck/pipeline/matrixdata/matrices/0705/real_cmb201308-201309_rxall_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat'
%  reduc_makedeltamaps('EB',[0:600],[0:500],[],matrix_name,'1454')
%  % keck 2012+2013: 
%  matrix_name = '/n/panlfs/bicep/keck/pipeline/matrixdata/matrices/0706/real_cmb_subset2012_2013_rxall_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat'
%  reduc_makedeltamaps('EB',[0:600],[0:100],[],matrix_name,'1332')


  %defaults
  if ~exist('specs','var')
    specs = 'E';
  end
  if ~exist('mpls','var')
    mpls = 80;
  end
  if ~exist('rlzs','var') | isempty(rlzs)
    rlzs = [0:50];
  end
  if ~exist('beam','var') | isempty(beam)
    beam = {get_bl_filename('B2bbns'),'B2bbns'}; 
  end
  if(~exist('matrix_name', 'var') | isempty(matrix_name))
    matrix_name='matrixdata/matrices/0704/real_allcmb_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat';
  end
  
  %just set nlmax=1499
  nlmax = 1499;
  
  %farm?
  do_farm=true;
  
  %-------------------------%

  if do_farm
   %choose a queue
    QUEUE='serial_requeue,general,itc_cluster';
    maxtime=5*60; %minutes
  end
  
%load coaddopt from matrix file
load(matrix_name, 'coaddopt')

if ~exist('sernum', 'var') | isempty(sernum)
 sernum=coaddopt.sernum(1:4);
end
  
for spec=specs
  for l=mpls
    
    deltaopt.spec=spec;
    deltaopt.l=l;
    deltaopt.rlzs=rlzs;
    deltaopt.nlmax=nlmax;
    deltaopt.beam=beam;
    deltaopt.matrix_name=matrix_name;
    deltaopt.sernum=sernum;
    
    cmd='observe_delta_map(deltaopt)';
    
    %check if this map stack exists
    stackname=gen_deltastack_name(coaddopt, l, spec, sernum);
    if exist(stackname, 'file')
      disp([stackname ' exists, skipping'])
      continue
    else
    
      if do_farm
        if ~exist('farmfiles/delta', 'dir')
          system_safe('mkdir farmfiles/delta')
        end
        farmit('farmfiles/delta',cmd,'var',{'deltaopt'},'queue',QUEUE,'mem',25000,'maxtime',maxtime,'submit',0);
      else
        eval(cmd)
      end %if farm
      
     end %exist stackname
      
  end %l
end %spec

  
return
