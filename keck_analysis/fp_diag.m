function fp_diag(dk,tag,rx,beamcen,chi,beamwid,diffpoint,chflags)
% fp_diag(dk,tag,rx)
%
% Draw diag of array layout on sky for given deck angle
%
% e.g. fp_diag(0,'20140601',0)

clf

if(~exist('dk','var'))
  dk=0;
end
if(~exist('tag','var'))
  tag='B20070501_cm13_1C_B_135';
end
if(~exist('rx','var'))
  rx=0;
end
if(~exist('beamcen','var'))
  beamcen='ideal';
end
if(~exist('chi','var'))
  chi='ideal';
end
if(~exist('beamwid','var'))
  beamwid='ideal';
end
if(~exist('diffpoint','var'))
  diffpoint='ideal';
end
if(~exist('chflags','var'))
  chflags=[];
end

% get array info
[p,ind]=get_array_info(tag,beamcen,chi,beamwid,diffpoint,chflags);
if ~isfield(p,'fwhm_maj')
  [p.fwhm_maj,p.fwhm_min,p.alpha] =  egauss2_scp2mmt(p.sigma,p.c,p.p);
end

% strip to single rx
p.r(p.rx~=rx)=NaN;

% rotate array to required angle
p=rotarray(p,dk);

% convert polar array defn to x,y for plotting

% this is how it's done in reduc_makecomap
%[y,x]=reckon(0,0,p.r,p.theta+90);

% this does the same thing
[x,y]=pol2cart(-p.theta*pi/180,p.r);

r=p.fwhm_maj/2;

% draw round beams
%  [xc,yc]=circle(x,y,p.fwhm_maj/2,[],1);
%  plot(xc(:,ind.l),yc(:,ind.l),'b');

% draw elliptical beams % here field that are not coming with p are used ?? (STF), below a new version
%  [xc,yc]=ellipse(p.ra_off_dos,p.dec_off,p.beam_sig_a,p.beam_sig_b,p.beam_phi*pi/180,[],1);
%  plot(xc(:,ind.gla),yc(:,ind.gla),'b');
%  hold on
%  plot(xc(:,ind.glb),yc(:,ind.glb),'r');
%  hold off

% draw elliptical beams
[xc,yc]=ellipse(x,y,p.fwhm_maj/2,p.fwhm_min/2,(p.alpha+p.theta)*pi/180,[],1);
plot(xc(:,ind.gla),yc(:,ind.gla),'b');
hold on
plot(xc(:,ind.lb),yc(:,ind.lb),'r');
hold off

% write the index numbers
%text(x(ind.la),y(ind.la)+r(ind.la)*1,num2str(ind.la'),'HorizontalAlignment','center','Color','b');
%text(x(ind.lb),y(ind.lb)+r(ind.lb)*1,num2str(ind.lb'),'HorizontalAlignment','center','Color','r');

% draw the bolo orientation
t=chi2alpha(0,0,p.r,p.theta,p.chi,p.chi_thetaref);

[xo,yo]=pol2cart(-t*pi/180,r);

xl=[x+xo,x-xo]';
yl=[y+yo,y-yo]';

%line(xl(:,ind.rgla),yl(:,ind.rgla),'Color','b');
%line(xl(:,ind.rglb),yl(:,ind.rglb),'Color','r');

line(xl(:,ind.gla),yl(:,ind.gla),'Color','b');
line(xl(:,ind.glb),yl(:,ind.glb),'Color','r');

x=abs(xlim); y=abs(ylim); x=max([x,y]);
xlim([-x,x]); ylim([-x,x]);
axis square

set(gca,'Xdir','reverse')

title(sprintf('Focal plane layout on %s for %s rx%d at dk=%d (A=blue, B=red)',tag,get_experiment_name,rx,dk));

xlabel('deg on sky in direction of RA');
ylabel('deg on sjy in direction of Dec');

return

% draw beam centroid offsets
xo=x; yo=y;
for i=1:2:length(p.r)
  [p.r(i:i+1),p.theta(i:i+1)]=average_pairs(p.r(i:i+1),p.theta(i:i+1));
end
[x,y]=pol2cart(p.theta*pi/180,p.r);
dx=200*(xo-x);
dy=200*(yo-y);
xl=[x,x+dx]';
yl=[y,y+dy]';

line(xl(:,ind.la),yl(:,ind.la),'Color','b','LineStyle','--');
line(xl(:,ind.lb),yl(:,ind.lb),'Color','r','LineStyle','--');

%set(gca,'XDir','reverse');

axis square;
grid

title(sprintf('%s deck angle %d',strrep(tag,'_','\_'),dk));
xlabel('Azimuth (deg)'); ylabel('Elevation (deg)')
xlim([-10,10]);ylim([-10,10]);
%xlim([-.5,.5]);ylim([3.5,4.6]);

return
