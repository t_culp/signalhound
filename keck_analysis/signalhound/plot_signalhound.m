function shopt = plot_signalhound(tag_date, shopt)
% plot_signalhound(tag_date, plot_type)
%
% Plot signalhound data from reduced files.
%
% [Inputs]
%   tag_date   String specifying either a particular tag, like 
%              "20140308B05_dk113", or a date, like "2014-Mar-08".
%   shopt      Signalhound analysis option structure, with fields:
%     mat_dir    Directory to save matlab files containing signalhound data. 
%                Defaults to /n/bicepfs2/users/cbischoff/signalhound/
%     pager_dir  Pager base directory. Defaults to 
%                /n/bicepfs1/www/keck/signalhound/
%     sub_dir    Directory inside (by_date/by_tag) for each signalhound
%     verbose    Set verbose = 1 for detailed feedback.
%
% [Output]
%   Writes one or more plots (in .png format) to plot_dir.

% Define frequencies for signals of interest, in MHz.
GOES_FREQ = 2029;
TDRS_FREQ_1 = 2215; % TDRS shows up as two narrow lines
TDRS_FREQ_2 = 6585;
WIFI_FREQ_1 = 2400; % For wifi, integrate total power in this frequency
WIFI_FREQ_2 = 2500; % range.
LMR_FREQ_1 = 449;   % Same for LMR
LMR_FREQ_2 = 457;

if nargin < 2
  shopt = [];
end
% Assign default values for empty fields in shopt.
if ~isfield(shopt, 'mat_dir')
  shopt.mat_dir = '/n/bicepfs2/users/cbischoff/signalhound';
end
if ~isfield(shopt, 'pager_dir')
  shopt.pager_dir = '/n/bicepfs1/www/keck/signalhound';
end
if ~isfield(shopt, 'verbose')
  shopt.verbose = 0;
end
if ~isfield(shopt, 'sub_dir')
  shopt.sub_dir = 'signalhound1';
end

% Is the tag_date input a tag or a date?
if regexp(tag_date, '\d{8}[BCDEF]\d{2}_dk\d{3}')
  is_date = false;
  plot_dir = fullfile(shopt.pager_dir, 'by_tag', shopt.sub_dir);
elseif regexp(tag_date, '\d{4}-\w{3}-\d{2}')
  is_date = true;
  plot_dir = fullfile(shopt.pager_dir, 'by_date', shopt.sub_dir);
else
  disp('ERROR: Argument 1 not recognized as tag or date');
  disp(tag_date);
  return
end

if ~exist(plot_dir, 'dir')
  mkdir(plot_dir)
end

% Read data.
if is_date
  % Just read a single day of signalhound data.
  sh_file = ['sh_' datestr(datenum(tag_date, 'yyyy-mmm-dd'), 'yyyymmdd') '.mat'];
  sh_file = fullfile(shopt.mat_dir, sh_file);
  if exist(sh_file, 'file')
    sh = load(sh_file);
  else
    if shopt.verbose
      disp(['File does not exist: ' sh_file]);
    end
    return
  end
else
  % Get tag information.
  taginfo = get_run_info(tag_date);
  d1num = datenum(taginfo.tstart, 'dd-mmm-yyyy:HH:MM:SS');
  d2num = datenum(taginfo.tend, 'dd-mmm-yyyy:HH:MM:SS');

  % Read one or more days, concatenating as necessary.
  sh_file = ['sh_' datestr(d1num, 'yyyymmdd') '.mat'];
  sh_file = fullfile(shopt.mat_dir, sh_file);
  if exist(sh_file, 'file')
    sh = load(sh_file);
  else
    if shopt.verbose
      disp(['File does not exist: ' sh_file]);
    end
    return
  end
  if floor(d2num) > floor(d1num)
    for dnum=floor(d1num)+1:floor(d2num)
      sh_file = ['sh_' datestr(dnum, 'yyyymmdd') '.mat'];
      sh_file = fullfile(shopt.mat_dir, sh_file);
      if exist(sh_file, 'file')
        sh_temp = load(sh_file);
        sh.t = cat(2, sh.t, sh_temp.t);
        sh.data = cat(1, sh.data, sh_temp.data);
      end
    end
  end

  % Cut down data to the correct time interval.
  [y,m,d,h,mi,s] = datevec(d1num);
  mjd1 = date2mjd(y, m, d, h, mi, s);
  [y,m,d,h,mi,s] = datevec(d2num);
  mjd2 = date2mjd(y, m, d, h, mi, s);
  select = (sh.time >= mjd1) & (sh.time < mjd2);
  if sum(select) <= 1
    % No data after selection!
    if shopt.verbose
      disp('No data in the time range for this tag');
    end
    return;
  end
  sh.t = sh.t(select);
  sh.data = sh.data(select,:,:);
end

% Make plots.

% 1. Waterfall plot.
% Interpolate to regular one minute time intervals.
if is_date
  [t, data] = sh_resample(sh);
  t = (t - floor(t(1))) * 24;
else
  [t, data] = sh_resample(sh, mjd1, mjd2);
  t = (t - mjd1) * 24;
end
% Set up plot.
fig = figure('Visible', 'off');
setwinsize(fig, 1000, 400);
% Plot data.
imagesc(t, sh.freq, 10*log10(data(:,:,1)'),[-100 -20]);
cb = colorbar();
% Axes.
set(gca, 'YDir', 'normal');
xlabel('time [hours]');
ylabel('frequency [MHz]');
ylabel(cb, 'dBm');
title(tag_date);
% Print figure.
if is_date
  % Convert date string format from yyyy-mmm-dd to yyyymmdd.
  figname = [datestr(datenum(tag_date, 'yyyy-mmm-dd'), 'yyyymmdd') ...
             '_plot1'];
else
  figname = [tag_date '_plot1'];
end
printfig(fig, fullfile(plot_dir, figname), 'png');
close(fig);

% 2. Waterfall plot, median-subtracted.
% Subtract per-frequency median.
med = nanmedian(data, 1);
data = data - repmat(med, size(data, 1), 1);
% Set up plot.
fig = figure('Visible', 'off');
setwinsize(gcf, 1000, 400);
% Plot data.
imagesc(t, sh.freq, real(10*log10(data(:,:,1)')),[-100 -20]);
cb = colorbar();
% Axes.
set(gca, 'YDir', 'normal');
xlabel('time [hours]');
ylabel('frequency [MHz]');
ylabel(cb, 'dBm');
title([tag_date ', median subtracted']);
% Print figure.
if is_date
  % Convert date string format from yyyy-mmm-dd to yyyymmdd.
  figname = [datestr(datenum(tag_date, 'yyyy-mmm-dd'), 'yyyymmdd') ...
             '_plot2'];
else
  figname = [tag_date '_plot2'];
end
printfig(fig, fullfile(plot_dir, figname), 'png');
close(fig);

% 3. GOES/TDRS/wifi plot.
% Select frequency lines.
goes = squeeze(sh.data(:,(sh.freq == GOES_FREQ),:));
tdrs1 = squeeze(sh.data(:,(sh.freq == TDRS_FREQ_1),:));
tdrs2 = squeeze(sh.data(:,(sh.freq == TDRS_FREQ_2),:));
wifi = squeeze(sh.data(:,((sh.freq >= WIFI_FREQ_1) & ...
                          (sh.freq < WIFI_FREQ_2)),:));
wifi = squeeze(sum(wifi,2)); % Integrate within band
shtotal = squeeze(sum(sh.data, 2));
% X-axis is time, in hours. For per-phase plot, time since the
% start of the phase. For per-day plot, time since start of the day.
if is_date
  t = 24 * (sh.t - floor(sh.t(1)));
else
  t = 24 * (sh.t - mjd1);
end
% Set up plot.
fig = figure('Visible', 'off');
setwinsize(gcf, 1000, 400);
% Plot traces.
if ~isempty(goes)
  plot(t, 10*log10(goes(:,1)), 'b');
  hold all;
end
if ~isempty(tdrs1)
  plot(t, 10*log10(tdrs1(:,1)), 'Color', [0 0.5 0]);
end
if ~isempty(tdrs2)
  plot(t, 10*log10(tdrs2(:,1)), 'Color', [0 1 0]);
end
if ~isempty(wifi)
  plot(t, 10*log10(wifi(:,1)), 'r');
end
if ~isempty(shtotal)
  plot(t, 10*log10(shtotal(:,1)), 'k');
end
% Axes.
if is_date
  xlim([0 24]);
end
xlabel('time [hours]');
ylim([-100 -20]); % Hard-code this for now, see how it works out.
ylabel('dBm');
title([tag_date '; GOES = blue, TDRS = green, wifi = red, total = black']);
legend('GOES (2029 MHz)', 'TDRS1 (2215 MHz)', 'TDRS2 (6585 MHz)', ...
  'WiFi (2400-2500 MHz)', 'Total', 'Location', 'southeast');
% Print figure.
if is_date
  % Convert date string format from yyyy-mmm-dd to yyyymmdd.
  figname = [datestr(datenum(tag_date, 'yyyy-mmm-dd'), 'yyyymmdd') ...
             '_plot3'];
else
  figname = [tag_date '_plot3'];
end
printfig(fig, fullfile(plot_dir, figname), 'png');
close(fig);

% 4. Single LMR plot.
% Find total power and trunk channel for color selection
lmr = sh.data(:,((sh.freq >= LMR_FREQ_1) & (sh.freq < LMR_FREQ_2)),:);
lmr_freq = sh.freq(sh.freq >= LMR_FREQ_1 & ...
                   sh.freq < LMR_FREQ_2);
lmrtrunk = lmr_select(lmr,lmr_freq);

% Set up plot.
fig = figure('Visible', 'off');
setwinsize(gcf, 1000, 400);
% Plot traces.
if ~isempty(lmrtrunk)
  plot(t, 10*log10(lmrtrunk(:,1)), 'k');
  hold all;
  plot(t, 10*log10(lmrtrunk(:,2)), 'b');
  plot(t, 10*log10(lmrtrunk(:,3)), 'g');
  plot(t, 10*log10(lmrtrunk(:,4)), 'm');
  plot(t, 10*log10(lmrtrunk(:,5)), 'r');
  hold all;
end
% Axes.
if is_date
  xlim([0 24]);
end
xlabel('time [hours]');
ylim([-100 -20]); % Hard-code this for now, see how it works out.
ylabel('dBm');
title([tag_date '; 1 = black, 2 = blue, 3 = green, 4 = magenta, 5 = red']);
legend('Chan1 (450.2 MHz)', 'Chan2 (450.6 MHz)', 'Chan3 (451 MHz)', ...
  'Chan4 (451.6 MHz)', 'Chan5 (452 MHz)', 'Location', 'southeast');
% Print figure.
if is_date
  % Convert date string format from yyyy-mmm-dd to yyyymmdd.
  figname = [datestr(datenum(tag_date, 'yyyy-mmm-dd'), 'yyyymmdd') ...
             '_plot4'];
else
  figname = [tag_date '_plot4'];
end
printfig(fig, fullfile(plot_dir, figname), 'png');
close(fig);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Subfunction: sh_resample
% ------------------------
% Signalhound data is irregularly sampled (because of the dumb
% Windows-based data acquisition), so we interpolate it to regular
% one-minute intervals for waterfall plots.
% If only the first argument is specified, then the output time
% samples will cover one entire UTC day (1441 samples). If the
% second and third arguments are specified, then the output time
% samples will range from mjd1 to mjd2.
function [t, data] = sh_resample(sh, mjd1, mjd2)

if nargin == 1
  t = floor(sh.t(1)) + [0:1/(24*60):1];
else
  t = [mjd1:1/(24*60):mjd2];
end

data = interp1(sh.t, sh.data, t);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Subfunction: lmr_select
% ------------------------
% The LMR system switches between 5 channels
% In a super-janky move, find the max channel and if it matches the known
% closest-frequency to a trunk channel, call it the channel at that point
function lmrtrunk = lmr_select(lmr,lmr_freq)

% lmrtrunk has one row for each trunk channel, and contains total LMR
% power if that channel is the dominant one (NaN otherwise)
lmrtrunk = NaN(size(lmr,1),5);
lmrtotal = squeeze(sum(lmr,2));

for ii = 1:size(lmr,1)
  thistime = lmr(ii,:,1);
  maxind = find(thistime == nanmax(thistime));
  if numel(maxind) ~= 0 && ~isnan(maxind(1))
    switch lmr_freq(maxind(1))
      case 450.2
        lmrtrunk(ii,1) = lmrtotal(ii,1);
      case 450.6
        lmrtrunk(ii,2) = lmrtotal(ii,1);
      case 451
        lmrtrunk(ii,3) = lmrtotal(ii,1);
      case 451.6
        lmrtrunk(ii,4) = lmrtotal(ii,1);
      case 452
        lmrtrunk(ii,5) = lmrtotal(ii,1);
    end
  end
end


return
