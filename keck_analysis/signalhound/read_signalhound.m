function shopt = read_signalhound(date, shopt)
% read_signalhound(date)
%
% Reads signalhound data for a given day from csv files and saves
% it to a signal matlab file.
%
% [Input]
%   date     Date to read in, specified with format like "2014-Jul-01".
%   shopt  Signalhound analysis option structure, with following fields:
%     csv_dir    Directory containing signalhound raw csv files. Defaults to 
%                /n/bicepfs2/keck/keckdaq/signalhound/
%     mat_dir    Directory to save matlab files containing signalhound data. 
%                Defaults to /n/bicepfs2/users/cbischoff/signalhound/
%     verbose    Set verbose = 1 for detailed feedback.
%     freqlen    Length of frequency vector to look for.  If data
%                (i.e. frequency axis for the first file on this day) 
%                don't match this, will skip.  Default 2014/2015 val
%
% [Output]
%   Writes .mat file containing one day of signalhound data to mat_dir.

if nargin < 2
  shopt = [];
end
% Assign default values for empty fields in shopt.
if ~isfield(shopt, 'csv_dir')
  shopt.csv_dir = 'C:/Users/keck/Documents/signalhound';
end
if ~isfield(shopt, 'mat_dir')
  shopt.mat_dir = 'C:/Users/keck/Documents/signalhound';
end
if ~isfield(shopt, 'verbose')
  shopt.verbose = 1;
end
if ~isfield(shopt, 'freqlen')
  shopt.freqlen = 61999; % For 2014/2015 (62001 for 2016)
end

% Get list of files for this date.
datef = datestr(datenum(date, 'yyyy-mmm-dd'), 'yyyymmdd');

% csv_subdir takes the form {$shopt.csv_dir}/yyyymmdd
csv_subdir = fullfile(shopt.csv_dir, datef);
filelist = dir(csv_subdir);
filelist = {filelist.name};
% remove files which aren't trace files
notTrace = zeros(length(filelist), 1);
for i = 1:length(filelist)
  notTrace(i) = isempty(regexp(filelist{i}, '^\d{8}_\d{6}_trace.csv$', 'once'));
end
filelist = filelist(~notTrace);

if isempty(filelist)
  % Exit if there are no files from this date.
  if shopt.verbose
    disp('No csv files for this date. Skipping.');
  end
  return
else
  % Convert string list of files to cell array.
  if shopt.verbose
    disp(sprintf('Found %i signalhound files.\n', numel(filelist)));
  end
end

% Loop over files.
skipped = zeros(1, numel(filelist));
t = zeros(1, numel(filelist));
data = zeros(numel(filelist), shopt.freqlen, 2);
for i=1:numel(filelist)
  % Read data from csv file.
  rawdata = csvread(fullfile(csv_subdir, filelist{i}), 1, 0);

  % Only read files with 'freqlen' rows
  % There are some files with other numbers of rows (finer
  % frequency binning, but not many).
  if size(rawdata, 1) ~= shopt.freqlen
    if shopt.verbose
      disp(['Skipped file ' filelist{i}]);
    end
    skipped(i) = 1;
  else
    % Get timestamp for this file.
    timestamp = filelist{i}(1:15);
    [y,m,d,h,mi,s] = datevec(timestamp, 'yyyymmdd_HHMMSS');
    t(i) = date2mjd(y,m,d,h,mi,s);

    % Record data.
    % Only have to record the frequency values for the first file.
    if ~exist('freq', 'var')
      freq = rawdata(:,1);
    end
    
    % all rows, 2nd and 3rd columns (min and max amplitude)
    data(i,:,:) = rawdata(:,2:end);

    % Read column labels from first row.
    % But only do this once, for the first file read. Assume all
    % subsequent files are the same.
    if ~exist('columns', 'var')
      fid = fopen(fullfile(csv_subdir, filelist{i}));
      % grab first line from file and split it at the commas.
      columns = strsplit(fgetl(fid), ',');
      fclose(fid);
    end
  end
end

% Dropped rows corresponding to skipped files.
filelist = filelist(~skipped);
t = t(~skipped);
data = data(~skipped,:,:);

% Save results.
outfile = ['sh_' datef '.mat'];
outfile = fullfile(shopt.mat_dir, outfile);
save(outfile, 'filelist', 't', 'freq', 'data', 'columns');

return
