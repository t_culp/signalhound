function shopt = daterange_signalhound(shopt)
%
% [Inputs]
%   t1, t2    beginning and end of week, inclusive.
%   mat_dir   Directory to find .mat files containing signalhound data
%   pager_dir Directory where the plots are saved
%   sub_dir   Directory inside (by_date/by_tag) for each signalhound

GOES_FREQ = 2029;
TDRS_FREQ_1 = 2215;
TDRS_FREQ_2 = 6585;
WIFI_FREQ_1 = 2400;
WIFI_FREQ_2 = 2500;
LMR_FREQ_1 = 449;
LMR_FREQ_2 = 457;

if nargin < 1
  shopt = [];
end

if ~isfield(shopt, 't1') || ~isfield(shopt, 't2')
  disp('ERROR: begin and end times are required');
  return
end
if ~isfield(shopt, 'mat_dir')
  shopt.mat_dir = '/n/bicepfs2/keck/signalhound/signalhound1';
end
if ~isfield(shopt, 'pager_dir')
  shopt.pager_dir = '/n/home03/tculp/pagers';
end
if ~isfield(shopt, 'sub_dir')
  shopt.sub_dir = 'signalhound1/dateranges';
end

sh_file = ['sh_' datestr(datenum(shopt.t1, 'yyyy-mmm-dd'), 'yyyymmdd') '.mat'];
disp(sh_file);
sh_file = fullfile(shopt.mat_dir, sh_file);
if exist(sh_file, 'file')
  sh = load(sh_file);
else
  disp(['File does not exist: ' sh_file]);
  return
end

plot_dir = fullfile(shopt.pager_dir, 'by_date', shopt.sub_dir);

t1num = datenum(shopt.t1, 'yyyy-mmm-dd');
t2num = datenum(shopt.t2, 'yyyy-mmm-dd');
for d = (t1num + 1):t2num
  sh_file = ['sh_' datestr(d, 'yyyymmdd') '.mat'];
  disp(sh_file);
  sh_file = fullfile(shopt.mat_dir, sh_file);
  if exist(sh_file, 'file')
    sh_temp = load(sh_file);
    sh.t = cat(2, sh.t, sh_temp.t);
    sh.data = cat(1, sh.data, sh_temp.data);
  else
    disp(['File does not exist: ' sh_file]);
  end
end

[y, m, d, h, mi, s] = datevec(t1num);
mjd1 = date2mjd(y, m, d, h, mi, s);
[y, m, d, h, mi, s] = datevec(t2num);
mjd2 = date2mjd(y, m, d, h, mi, s);

disp('Begin interpolation');
[t, data] = sh_resample(sh, mjd1, mjd2);
disp('End interpolation');
t = (t - mjd1) * 24;

daterangestr = [datestr(t1num, 'yyyymmdd') '-' datestr(t2num, 'yyyymmdd')];

% 1. Waterfall plot.
% Interpolate to regular one minute time intervals.
% Set up plot.
fig = figure('Visible', 'off');
setwinsize(fig, 1000, 400);
% Plot data.
imagesc(t, sh.freq, 10*log10(data(:,:,1)'),[-100 -20]);
cb = colorbar();
% Axes.
set(gca, 'YDir', 'normal');
xlabel('time [hours]');
ylabel('frequency [MHz]');
ylabel(cb, 'dBm');
title(daterangestr);
% Print figure.
figname = [daterangestr '_plot1'];
printfig(fig, fullfile(plot_dir, figname), 'png');
close(fig);

% 2. Waterfall plot, median-subtracted.
% Subtract per-frequency median.
med = nanmedian(data, 1);
data = data - repmat(med, size(data, 1), 1);
% Set up plot.
fig = figure('Visible', 'off');
setwinsize(gcf, 1000, 400);
% Plot data.
imagesc(t, sh.freq, real(10*log10(data(:,:,1)')),[-100 -20]);
cb = colorbar();
% Axes.
set(gca, 'YDir', 'normal');
xlabel('time [hours]');
ylabel('frequency [MHz]');
ylabel(cb, 'dBm');
title([daterangestr ', median subtracted']);
% Print figure.
figname = [daterangestr '_plot2'];
printfig(fig, fullfile(plot_dir, figname), 'png');
close(fig);

% 3. GOES/TDRS/wifi plot.
% Select frequency lines.
goes = squeeze(sh.data(:,(sh.freq == GOES_FREQ),:));
tdrs1 = squeeze(sh.data(:,(sh.freq == TDRS_FREQ_1),:));
tdrs2 = squeeze(sh.data(:,(sh.freq == TDRS_FREQ_2),:));
wifi = squeeze(sh.data(:,((sh.freq >= WIFI_FREQ_1) & ...
                          (sh.freq < WIFI_FREQ_2)),:));
wifi = squeeze(sum(wifi,2)); % Integrate within band
shtotal = squeeze(sum(sh.data, 2));
% X-axis is time, in hours. For per-phase plot, time since the
% start of the phase. For per-day plot, time since start of the day.
t = 24 * (sh.t - mjd1);
% Set up plot.
fig = figure('Visible', 'off');
setwinsize(gcf, 1000, 400);
% Plot traces.
if ~isempty(goes)
  plot(t, 10*log10(goes(:,1)), 'b');
  hold all;
end
if ~isempty(tdrs1)
  plot(t, 10*log10(tdrs1(:,1)), 'Color', [0 0.5 0]);
end
if ~isempty(tdrs2)
  plot(t, 10*log10(tdrs2(:,1)), 'Color', [0 1 0]);
end
if ~isempty(wifi)
  plot(t, 10*log10(wifi(:,1)), 'r');
end
if ~isempty(shtotal)
  plot(t, 10*log10(shtotal(:,1)), 'k');
end
% Axes.
%xlim([0 24*(t2num - t1num + 1)]);
xlabel('time [hours]');
ylim([-100 -20]); % Hard-code this for now, see how it works out.
ylabel('dBm');
title([daterangestr '; GOES = blue, TDRS = green, wifi = red, total = black']);
% Print figure.
figname = [daterangestr '_plot3'];
printfig(fig, fullfile(plot_dir, figname), 'png');
close(fig);

% 4. Single LMR plot.
% Find total power and trunk channel for color selection
lmr = sh.data(:,((sh.freq >= LMR_FREQ_1) & (sh.freq < LMR_FREQ_2)),:);
lmr_freq = sh.freq(sh.freq >= LMR_FREQ_1 & ...
                   sh.freq < LMR_FREQ_2);
lmrtrunk = lmr_select(lmr,lmr_freq);

% Set up plot.
fig = figure('Visible', 'off');
setwinsize(gcf, 1000, 400);
% Plot traces.
if ~isempty(lmrtrunk)
  plot(t, 10*log10(lmrtrunk(:,1)), 'k');
  hold all;
  plot(t, 10*log10(lmrtrunk(:,2)), 'b');
  plot(t, 10*log10(lmrtrunk(:,3)), 'g');
  plot(t, 10*log10(lmrtrunk(:,4)), 'm');
  plot(t, 10*log10(lmrtrunk(:,5)), 'r');
  hold all;
end
% Axes.
%xlim([0 24*(t2num - t1num + 1)]);
xlabel('time [hours]');
ylim([-100 -20]); % Hard-code this for now, see how it works out.
ylabel('dBm');
title([daterangestr '; 1 = black, 2 = blue, 3 = green, 4 = magenta, 5 = red']);
% Print figure.
figname = [daterangestr '_plot4'];
printfig(fig, fullfile(plot_dir, figname), 'png');
close(fig);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Subfunction: sh_resample
% ------------------------
% Signalhound data is irregularly sampled (because of the dumb
% Windows-based data acquisition), so we interpolate it to regular
% one-minute intervals for waterfall plots.
% If only the first argument is specified, then the output time
% samples will cover one entire UTC day (1441 samples). If the
% second and third arguments are specified, then the output time
% samples will range from mjd1 to mjd2.
function [t, data] = sh_resample(sh, mjd1, mjd2)

if nargin == 1
  t = floor(sh.t(1)) + [0:1/(24*60):1];
else
  t = [mjd1:(mjd2 - mjd1)/(24*60):mjd2];
end

data = interp1(sh.t, sh.data, t);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Subfunction: lmr_select
% ------------------------
% The LMR system switches between 5 channels
% In a super-janky move, find the max channel and if it matches the known
% closest-frequency to a trunk channel, call it the channel at that point
function lmrtrunk = lmr_select(lmr,lmr_freq)

% lmrtrunk has one row for each trunk channel, and contains total LMR
% power if that channel is the dominant one (NaN otherwise)
lmrtrunk = NaN(size(lmr,1),5);
lmrtotal = squeeze(sum(lmr,2));

for ii = 1:size(lmr,1)
  thistime = lmr(ii,:,1);
  maxind = find(thistime == nanmax(thistime));
  if numel(maxind) ~= 0 && ~isnan(maxind(1))
    switch lmr_freq(maxind(1))
      case 450.2
        lmrtrunk(ii,1) = lmrtotal(ii,1);
      case 450.6
        lmrtrunk(ii,2) = lmrtotal(ii,1);
      case 451
        lmrtrunk(ii,3) = lmrtotal(ii,1);
      case 451.6
        lmrtrunk(ii,4) = lmrtotal(ii,1);
      case 452
        lmrtrunk(ii,5) = lmrtotal(ii,1);
    end
  end
end

return
