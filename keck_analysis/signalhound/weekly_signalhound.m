function shopt = weekly_signalhound(date_list, shopt)
%
% [Inputs]
%  date_list      vector of datenums
%  shopt
%   mat_dir       Directory to find .mat files containing signalhound data
%   pager_dir     Directory where the plots are saved
%   sub_dir       Directory inside (by_date/by_tag) for each signalhound
%                 (defaults to 'weeks/signalhound1')
%   update_weekly Boolean designating whether to update existing weekly plots

% so that the originals don't accidentally get overwritten
t1 = shopt.t1;
t2 = shopt.t2;

if nargin < 2
  shopt = [];
end

if ~isfield(shopt, 'mat_dir')
  shopt.mat_dir = '/n/bicepfs2/keck/signalhound/signalhound1';
end
if ~isfield(shopt, 'pager_dir')
  shopt.pager_dir = '/n/home03/tculp/pagers';
end
if ~isfield(shopt, 'sub_dir')
  shopt.sub_dir = 'signalhound1';
end
if ~isfield(shopt, 'update_weekly')
  shopt.update_weekly = 0;
end

% prepending whatever is in shopt.sub_dir with 'weeks/'
shopt.sub_dir = fullfile('weeks', shopt.sub_dir);

% adjusting the date_list so that a Sunday is the first day
wd = weekday(date_list(1));
if wd ~= 1 % if the first day is not Sunday...
  date_list = date_list((8 - wd):end);
end

for i = 1:7:numel(date_list)
  if i + 6 > numel(date_list)
    break;
  end
  shopt.t1 = datestr(date_list(i), 'yyyy-mmm-dd');
  shopt.t2 = datestr(date_list(i + 6), 'yyyy-mmm-dd');
  if ~exist(fullfile(shopt.pager_dir, 'by_date', shopt.sub_dir, [shopt.t1 '-' shopt.t2 ...
    '_plot1.png']), 'file') || update_weekly
    daterange_signalhound(shopt);
  end
end

% restoring the original values of t1 and t2
shopt.t1 = t1;
shopt.t2 = t2;

return
