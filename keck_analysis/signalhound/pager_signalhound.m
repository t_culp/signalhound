function shopt = pager_signalhound(by_date, shopt)
% shopt = pager_signalhound(by_date, shopt)
%
% Generate html for signalhound pager.
%
% [Inputs]
%   data_list  Cell array containing a list of tags or a list of
%              dates, formatted like '20140105', to include in the pager.
%   by_date    If true, generate the per-date pager. If false, generate 
%              the per-tag pager.
%   shopt  Signalhound analysis option structure, with following fields:
%     pager_dir  Pager base directory. Defaults to /n/bicepfs1/www/keck/signalhound/
%     verbose    Set verbose = 1 for detailed feedback.
%
% [Output]
%   Writes html file to the pager directory.

if nargin < 2
  shopt = [];
end
% Assign default values for empty fields in shopt.
if ~isfield(shopt, 'pager_dir')
  shopt.pager_dir = '/n/bicepfs1/www/keck/signalhound';
end
if ~isfield(shopt, 'verbose')
  shopt.verbose = 0;
end

% Assign paths.
if by_date
  url_base = 'by_date/';
  plot_dir = fullfile(shopt.pager_dir, 'by_date');
  html_file = fullfile(shopt.pager_dir, 'by_date.html');
else
  url_base = 'by_tag/';
  plot_dir = fullfile(shopt.pager_dir, 'by_tag');
  html_file = fullfile(shopt.pager_dir, 'by_tag.html');
end

% Get data list.  Do for both SH1/SH2 and find union
%data_list = ls(fullfile(plot_dir, '*_plot1.png'));
[x, data_list1]=system(sprintf('/bin/ls -1 %s/signalhound1/*_plot1.png', ...
                               plot_dir));
data_list1 = textscan(data_list1, '%s');
data_list1 = data_list1{1};
for i=1:numel(data_list1)
  x1 = [0, strfind(data_list1{i}, '/')];
  x2 = strfind(data_list1{i}, '_plot1.png');
  data_list1{i} = data_list1{i}(x1(end)+1:x2-1);
end
[x, data_list2]=system(sprintf('/bin/ls -1 %s/signalhound2/*_plot1.png', ...
                               plot_dir));
data_list2 = textscan(data_list2, '%s');
data_list2 = data_list2{1};
for i=1:numel(data_list2)
  x1 = [0, strfind(data_list2{i}, '/')];
  x2 = strfind(data_list2{i}, '_plot1.png');
  data_list2{i} = data_list2{i}(x1(end)+1:x2-1);
end
data_list = union(data_list1,data_list2);

% Open file to write html.
html = fopen(html_file, 'wt');

% Opening lines.
fprintf(html, '<!DOCTYPE html>\n');
fprintf(html, '<html lang="en">\n');

% Header.
fprintf(html, '  <head>\n');
if by_date
  fprintf(html, '    <title>SignalHound by-date pager</title>\n');
else
  fprintf(html, '    <title>SignalHound by-tag pager</title>\n');
end
fprintf(html, '    <meta charset="utf-8" />\n');
fprintf(html, '    <link rel="stylesheet" href="signalhound.css" />\n');
fprintf(html, '    <script src="URI.js"></script>\n');
fprintf(html, '    <script src="signalhound.js"></script>\n');
fprintf(html, '    <script language="JavaScript">\n');
fprintf(html, '      var url_base = "%s/";\n', url_base);
fprintf(html, '      var default_data = "%s";\n', data_list{end});
fprintf(html, '      var default_plot = "4";\n');
fprintf(html, '    </script>\n');
fprintf(html, '  </head>\n');

% Top section of body.
fprintf(html, '  <body>\n');
fprintf(html, '    <header>\n');
if by_date
  fprintf(html, '      <h1>SignalHound by-date pager</h1>\n');
  fprintf(html, ['      <h2>Switch to ' ...
                 '<a href="by_tag.html">by-tag pager</a></h2>\n']);
else
  fprintf(html, '      <h1>SignalHound by-tag pager</h1>\n');
  fprintf(html, ['      <h2>Switch to ' ...
                 '<a href="by_date.html">by-date pager</a></h2>\n']);
end
fprintf(html, '      <h2>Notes</h2>\n');
fprintf(html, '      <ul>\n');
fprintf(html, ['        <li>Data from 2014-02-10 through 2014-07-15 ' ...
               'are in &ldquo;max hold&rdquo; mode and probably not ' ...
               'useable.</li>\n']);
fprintf(html, ['        <li>Top panel is the MAPO SignalHound, '...
               'bottom panel is DSL.  The DSL SignalHound did not '...
               'exist before Dec 2015.  The MAPO configuration was '...
               'also changed at this time, so data before/after are '...
               'not directly comparable.</li>\n']);
fprintf(html, ['        <li>Channel definitions: GOES = 2029 MHz, '...
               'TDRS = 2215/6585 MHz, WiFi = 2400-2500 MHz, LMR total '...
               ' = 449-457 MHz, Chan1 = 450.2, Chan2 = 450.6, Chan3 = '...
               '451, Chan4 = 451.6, Chan5 = 452</li>\n']);
fprintf(html, '      </ul>\n');
fprintf(html, '    </header>\n');

% Sidebar.
fprintf(html, '    <div id="main">\n');
fprintf(html, '      <div id="data-select">\n');
if by_date
  fprintf(html, '      <p><strong>SELECT DATE</strong></p>\n');
else
  fprintf(html, '      <p><strong>SELECT TAG</strong></p>\n');
end
fprintf(html, '      <ul>\n');
% Now, add links for every element in the data list, in reverse order.
for i=[numel(data_list):-1:1]
  % Display dates as yyyy-mm-dd, instead of yyyymmdd.
  if by_date
    display_item = datestr(datenum(data_list{i}, 'yyyymmdd'), 'yyyy-mm-dd');
  else
    display_item = data_list{i};
  end
  fprintf(html, ['        <li><a href="javascript:set_value(''data''' ...
                 ', ''%s'');">%s</a></li>\n'], data_list{i}, display_item);
end
fprintf(html, '      </ul>\n');
fprintf(html, '    </div>\n');

% Plot type select.
fprintf(html, '      <div id="plot-type">\n');
fprintf(html, '        <p>\n');
fprintf(html, '          Plot type: ');
fprintf(html, ['<a href="javascript:set_value(''plot'', ''1'');">' ...
               'waterfall</a> | <a href="javascript:set_value(''plot''' ...
               ', ''2'');">waterfall, median-subtracted</a> | ' ...
               '<a href="javascript:set_value(''plot'', ''3'');">' ...
               'satcom</a> | <a href="javascript:set_value(''plot'', ''4'');">'...
               'LMR</a>\n']);
fprintf(html, '        </p>\n');
fprintf(html, '      </div>\n');

% Plot region.
fprintf(html, '      <div id="plot">\n');
fprintf(html, '        <img src="" id="plot_img1" />\n');
fprintf(html, '        <br>\n');
fprintf(html, '        <img src="" id="plot_img2" />\n');
fprintf(html, '      </div>\n');

% Finish up.
fprintf(html, '    </div>\n');
fprintf(html, '    <script language="JavaScript">\n');
fprintf(html, '      <!--\n');
fprintf(html, '        plupdate();\n');
fprintf(html, '        -->\n');
fprintf(html, '    </script>');
fprintf(html, '  </body>\n');
fprintf(html, '</html>\n');
fclose(html);


return
