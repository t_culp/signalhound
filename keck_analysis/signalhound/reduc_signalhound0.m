function shopt = reduc_signalhound(shopt)
% reduc_signalhound(shopt)
%
% Top level function to reduce signalhound data, make plots, and
% regenerate pager html.
%
% [Input]
%   shopt  Signalhound analysis option structure, with following fields:
%    t1, t2      Start and stop dates for analysis, formatted like '2014-Jan-02'
%                If not specified, t1 = '2014-Jan-02' and t2 is the current day.
%    csv_dir[2]  Directory containing signalhound raw csv files. Defaults to 
%                /n/bicepfs2/keck/keckdaq/signalhound/ [for data newer than
%                2015-Dec-06 when there become 2 signalhounds]
%    freqlen     Number of frequencies in .csv files. Only files with
%                freqlen rows will be read in read_signalhound.m.
%    mat_dir[2]  Directory to save matlab files containing signalhound data. 
%                Defaults to /n/bicepfs2/users/cbischoff/signalhound/ [for
%                data newer than 2015-Dec-06 when there become 2
%                signalhounds]
%    pager_dir   Pager base directory. Defaults to /n/bicepfs1/www/keck/signalhound/
%    sub_dir[2]  In which subdirectory of the pager_dir directory the
%                plots will be saved.
%    update_mat  If update = 0 (default), then only make .mat files that 
%                don't already exist. If update = 1, then remake .mat files. 
%                The pager html files will always get updated.
%    update_plot If update = 0 (default), the only make plots that don't
%                already exist. If update = 1, then remake plot files.
%    use_bicepfs Use the bicepfs directories and all the messy defaults.
%    verbose     Set verbose = 1 for detailed feedback.
%
% [Output]
%   Updates .mat files in mat_dir, html and png files in pager_dir.

if nargin == 0
  shopt = [];
end
% Assign default values for empty fields in shopt.
if ~isfield(shopt, 't1')
  shopt.t1 = '2014-Jan-02';
end
if ~isfield(shopt, 't2')
  shopt.t2 = datestr(now, 'yyyy-mmm-dd');
end
if ~isfield(shopt, 'csv_dir')
  shopt.csv_dir = '/n/bicepfs2/keck/keckdaq/signalhound';
end
if ~isfield(shopt, 'csv_dir2')
  shopt.csv_dir2 = '/n/bicepfs2/keck/keckdaq/signalhound2/';
end
if ~isfield(shopt, 'freqlen')
  shopt.freqlen = 61999;
end
if ~isfield(shopt, 'mat_dir')
  shopt.mat_dir = '/n/bicepfs2/users/cbischoff/signalhound';
end
if ~isfield(shopt, 'mat_dir2')
  shopt.mat_dir2 = '/n/bicepfs2/keck/signalhound/signalhound2/';
end
if ~isfield(shopt, 'pager_dir')
  shopt.pager_dir = '/n/bicepfs1/www/keck/signalhound';
end
if ~isfield(shopt, 'sub_dir')
  shopt.sub_dir = 'signalhound1';
end
if ~isfield(shopt, 'sub_dir2')
  shopt.sub_dir2 = 'signalhound2';
end
if ~isfield(shopt, 'update_mat')
  shopt.update_mat = 0;
end
if ~isfield(shopt, 'update_plot')
  shopt.update_plot = 0;
end
if ~isfield(shopt, 'use_bicepfs')
  shopt.use_bicepfs = 0;
end
if ~isfield(shopt, 'verbose')
  shopt.verbose = 0;
end

% Check for existence of directories.
if ~exist(shopt.csv_dir, 'dir')
  disp('ERROR: csv_dir does not exist');
  return;
end
if ~exist(shopt.mat_dir, 'dir')
  disp('ERROR: mat_dir does not exist');
  return
end
if ~exist(shopt.pager_dir, 'dir')
  disp('ERROR: pager_dir does not exist');
  return
end
% Pager subdirectories to save images.
if ~exist(fullfile(shopt.pager_dir, 'by_date'), 'dir')
  mkdir(fullfile(shopt.pager_dir, 'by_date'));
end
if ~exist(fullfile(shopt.pager_dir, 'by_tag'), 'dir')
  mkdir(fullfile(shopt.pager_dir, 'by_tag'));
end

% Reduce data from many csv files to daily .mat files
% and update per-date pager.
d1num = datenum(shopt.t1, 'yyyy-mmm-dd');
d2num = datenum(shopt.t2, 'yyyy-mmm-dd');
date_list = d1num:d2num;
date_str = cellstr(datestr(date_list, 'yyyy-mmm-dd'));
for i=1:numel(date_str)
  if shopt.verbose
    disp(date_str{i});
  end

  % this keeps track of the current date we are on just in case the job gets
  % cancelled and rescheduled on Odyssey
  f = fopen('/n/home03/tculp/signalhound/date0.txt', 'w');
  fprintf(f, date_str{i});
  fclose(f);

  if datenum(date_str{i},'yyyy-mmm-dd') >= datenum('2015-Dec-06','yyyy-mmm-dd')
    if shopt.use_bicepfs
      shopt.freqlen = 62001;
      shopt.csv_dir = '/n/bicepfs2/keck/keckdaq/signalhound1/';
      shopt.mat_dir = '/n/bicepfs2/keck/signalhound/signalhound1/';
      shopt.sub_dir = 'signalhound1';
      shopt.csv_dir2 = '/n/bicepfs2/keck/keckdaq/signalhound2/';
      shopt.mat_dir2 = '/n/bicepfs2/keck/signalhound/signalhound2/';
      shopt.sub_dir2 = 'signalhound2';
    end
    shopt.freqlen = 62001;
    nplottype = 4;
    % here we make all the stuff for signalhound2 (if it's needed, of
    % course). 
    reduc_and_plot(date_list(i), date_str{i}, shopt.csv_dir2, shopt.mat_dir2, shopt.sub_dir2, nplottype, shopt)
  else
    if shopt.use_bicepfs
      shopt.freqlen = 10001;
      shopt.csv_dir = '/n/bicepfs2/keck/keckdaq/signalhound/';
      shopt.mat_dir = '/n/bicepfs2/keck/signalhound/signalhound0/';
      shopt.sub_dir = 'signalhound1';
    end
    shopt.freqlen = 10001;
    nplottype = 3;
  end
  
  reduc_and_plot(date_list(i), date_str{i}, shopt.csv_dir, shopt.mat_dir, shopt.sub_dir, nplottype, shopt)
   
end
% Generate by-date pager html.
by_date = true;
pager_signalhound(by_date, shopt);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Subfunction: reduc_and_plot
% ------------------------
% creates the .mat file and plots for the given date and data set, if this is
% deemed necessary.
function reduc_and_plot(date, date_str, csv_dir, mat_dir, sub_dir, nplottype, shopt)
  filelist = dir(fullfile(shopt.pager_dir, 'by_date', sub_dir));
  filelist = {filelist.name};
  % these 3 lines make sure the correct stuff gets passed to the other
  % scripts.
  shopt.sub_dir = sub_dir;
  shopt.csv_dir = csv_dir;
  shopt.mat_dir = mat_dir;
    
  % generating .mat file
  % Only make the .mat file if it doesn't already exist, 
  % or shopt.update_mat = 1.
  fname = ['sh_' datestr(date, 'yyyymmdd') '.mat'];
  if shopt.update_mat || ~exist(fullfile(mat_dir, fname), 'file')
    read_signalhound(date_str, shopt);
  end
  
  % generating plot
  plotnameregex = ['^' datestr(date, 'yyyymmdd') '_plot(\d{1}).png$'];
  plot_exists = 0;
  for j = 1:length(filelist)
    tokens = regexp(filelist{j}, plotnameregex, 'tokens');
    if length(tokens) == 1 && tokens{1}{1} == nplottype
      plot_exists = 1;
      break;
    end
  end
  
  if shopt.update_plot || ~plot_exists
    plot_signalhound(date_str, shopt);
  end
  
return
