function shopt = monthly_signalhound(date_list, shopt)
%
% [Inputs]
%  date_list      vector of datenums
%  shopt
%   mat_dir       Directory to find .mat files containing signalhound data
%   pager_dir     Directory where the plots are saved
%   sub_dir       Directory inside (by_date/by_tag) for each signalhound
%                 (defaults to 'weeks/signalhound1')
%   update_monthly Boolean designating whether to update existing monthly plots

% so that the originals don't accidentally get overwritten
t1 = shopt.t1;
t2 = shopt.t2;

if nargin < 2
  shopt = [];
end

if ~isfield(shopt, 'mat_dir')
  shopt.mat_dir = '/n/bicepfs2/keck/signalhound/signalhound1';
end
if ~isfield(shopt, 'pager_dir')
  shopt.pager_dir = '/n/home03/tculp/pagers';
end
if ~isfield(shopt, 'sub_dir')
  shopt.sub_dir = 'signalhound1';
end
if ~isfield(shopt, 'update_monthly')
  shopt.update_monthly = 0;
end

% prepending whatever is in shopt.sub_dir with 'weeks/'
shopt.sub_dir = fullfile('months', shopt.sub_dir);

for i = 1:numel(date_list)
  d = day(date_list(i));
  if d == 1 % if the first day is not Sunday...
     date_list = date_list(i:end); 
     break;
  end
end

while 1 
  daysinmonth = eomday(year(date_list(i)), month(date_list(i)));
  % if there aren't enough entries in the date_list to make an entire month...
  if i + daysinmonth > numel(date_list)
    break;
  end
  shopt.t1 = datestr(date_list(i), 'yyyy-mmm-dd');
  shopt.t2 = datestr(date_list(i + (daysinmonth - 1)), 'yyyy-mmm-dd');
  if ~exist(fullfile(pager_dir, 'by_date', sub_dir, [shopt.t1 '-' shopt.t2 ...
    '_plot1.png']), 'file') || update_monthly
    daterange_signalhound(shopt);
  end
  i = i + daysinmonth
end

% restoring the original values of t1 and t2
shopt.t1 = t1;
shopt.t2 = t2;

return
