function reduc_calc_stats(tags,csvfile)
% reduc_calc_stats(tags,csvfile)
%
% Calculate per-tag statistics that can be used to
% select good data and cut out bad data.  The first
% argument can be a single tag name or a cell array
% of tags.  The second argument is the CSV file in
% which the results are recorded.  If the CSV file
% already exists, the specified tags will be added
% or updated as appropriate.
%
% To add a new stat to the file, add code to calculate
% it below the comment "ADD CALCULATION OF NEW
% QUANTITIES HERE" and an error value below the comment
% "ADD ERROR VALUES FOR NEW QUANTITIES HERE."


% create temporary tag_stats file in case reduc_calc_stats crashes before
% it can write the new csv file.

if nargin<2 || isempty(csvfile)
  csvfile=fullfile('aux_data','tag_stats.csv');
end

if nargin<1 || isempty(tags)
  [p k]=ParameterRead(fullfile('aux_data','tag_list.csv'));
  tags=p.tag;
end

lt_all=[];
% May have been passed scanset structure from reduc_make_tag_list
if isstruct(tags)
  C=tags;
  tags={};
  lt_all=zeros(length(C),length(C(1).livetime));
  for i=1:length(C)
    tags{i}=C(i).tag;
    lt_all(i,:)=C(i).livetime / 24 / 60;
  end
elseif ischar(tags)
  tags={tags};
end

if exist(csvfile,'file')
  % create temporary tag_stats file in case reduc_calc_stats crashes before
  % it can write the new csv file.
  unix(['cp ' csvfile ' ' csvfile(1:end-3) 'tmp.csv']);
  setpermissions([csvfile(1:end-3) 'tmp.csv']);
  [p k]=ParameterRead(csvfile);
else
  p=[];
  k.comments={};
  k.fields={};
  k.units={};
  k.formats={};
end


% k=set_stat_props(k,prop);

% Which experiment are we?
expt=get_experiment_name();
switch expt
  case 'bicep3'
    experiment='BICEP3';
  case 'keck'
    experiment='Keck';
  case 'bicep2'
    experiment='BICEP2';
end

% update stat CSV file header
if isempty(k) || isempty(k.comments)
  k.comments={['# ' experiment ' tag stats list'], ['# Updated ' datestr(now)]};
else
  for j=1:length(k.comments)
    % if strcmpi(k.comments{j},'# Updated')
    if ~isempty(strfind(lower(k.comments{j}),'updated'))
      k.comments{j}=['# Updated ' datestr(now)];
    end
  end
end

% Save stats file at least every N tags
% This is obsolete -- since we're no longer doing anything slow, don't need to save
% incomplete results.
num_before_save=Inf;
num_since_save=0;

% Parse through GCP log files to get the live times.
% Do this once and for all at the start, for a little faster
% run time.
if isempty(lt_all)
  lt_all=calc_tag_livetime(tags);
end

for i=1:length(tags)
  disp(['Calculating stats for ' tags{i}]);

  % Calculate stats in temp structure
  tmp.tag=tags{i};

  % Load TODs and calvals if they exist
  todfile=fullfile('data','real',[tmp.tag '_tod.mat']);
  calvalfile=fullfile('data','real',[tmp.tag '_calval.mat']);
  caldatfile=fullfile('data','real',[tmp.tag '_caldat.mat']);
  dr=dir(todfile);
  tmp.has_tod=~isempty(dr)&&dr.bytes>0;
  dr=dir(calvalfile);
  tmp.has_calval=~isempty(dr)&&dr.bytes>0;
  dr=dir(caldatfile);
  tmp.has_caldat=~isempty(dr)&&dr.bytes>0;

  % Calculate livetime info from logs
  % lt=calc_tag_livetime(tmp.tag);
  lt=lt_all(i,:);
  if isempty(lt)
    tmp.t_tot=0;
    tmp.t_elnod=0;
    tmp.t_plc=0;
    tmp.t_onsrc=0;
    tmp.t_cyc=0;
    tmp.t_dip=0;
    tmp.t_flc=0;
  else
    lt=nansum(lt,1) * 24 * 60;
    if length(lt)<5
      lt(5)=0;
    end
    tmp.t_tot=lt(1);
    tmp.t_elnod=lt(3);
    tmp.t_plc=lt(4);
    tmp.t_onsrc=lt(2);
    tmp.t_cyc=lt(5);
    tmp.t_dip=lt(6);
    tmp.t_flc=lt(7);
  end

  % ADD CALCULATION OF NEW QUANTITIES HERE

  % Add / update this tag in the stats structure.
  [p,k]=add_to_stats(tmp,p,k);

  num_since_save=num_since_save+1;
  if num_since_save>=num_before_save
    p=sort_stats_by_tag(p);
    try
    ParameterWrite(csvfile,p,k);
    setpermissions(csvfile);
    catch
    %keyboard
    end
    num_since_save=0;
  end
end

% Explicitly set the units for quantities where needed
props.units.t_tot='min';
props.units.t_plc='min';
props.units.t_elnod='min';
props.units.t_cyc='min';
props.units.t_onsrc='min';
props.units.t_dip='min';
props.units.t_flc='min';


% ADD UNITS, ETC. FOR ANY NEW QUANTITIES HERE (OPTIONAL)

for j=1:length(k.fields)
  if exist('props','var')&&isfield(props,'units')
    if isfield(props.units,k.fields{j})
      k.units{j}=props.units.(k.fields{j});
    end
  end
end

p=sort_stats_by_tag(p);

% Ensure all fields are column vectors
for i=1:numel(k.fields)
  x=getfield(p,k.fields{i});
  p=setfield(p,k.fields{i},cvec(x));
end

ParameterWrite(csvfile,p,k);
setpermissions(csvfile);
return


function p_out=sort_stats_by_tag(p)
if isempty(p)
  p_out=p;
  return
end
[tags,idx]=sort(p.tag);
% if issorted(idx)
%  p_out=p;
%  return
% end
fn=fieldnames(p);
for j=1:length(fn)
  if iscell(p.(fn{j}))
    p_out.(fn{j})={p.(fn{j}){idx}};
  else
    if length(p.(fn{j}))<length(idx)
      tmp=NaN*ones(size(idx));
      tmp(1:length(p.(fn{j})))=p.(fn{j});
      p.(fn{j})=tmp;
    end
    p_out.(fn{j})=[p.(fn{j})(idx)];
  end
end
return

function [p_out,k_out]=add_to_stats(stats,p,k)
p_out=p;
k_out=k;
if isempty(p_out)
  idx=1;
else
  idx=strmatch(stats.tag,p_out.tag,'exact');
  if length(idx)>1
    idx=idx(1);
  end
  if isempty(idx)
    idx=1+length(p_out.tag);
  end
end
fn=fieldnames(stats);
for j=1:length(fn)
  jdx=strmatch(fn{j},k_out.fields,'exact');
  if isempty(jdx)
    k_out.fields={k_out.fields{:},fn{j}};
    k_out.units={k_out.units{:},'(null)'};
    switch (class(stats.(fn{j})))
      case 'char', typ='string';
      case {'int8','int16','int32','int64'}, ...
                   typ='integer';
      case 'double', ...
                   typ='float';
      otherwise,   typ='float';
    end
    k_out.formats={k_out.formats{:},typ};
    jdx=length(k_out.fields);
  end
  if strcmp(k_out.formats{jdx},'string')
    p_out.(fn{j}){idx}=stats.(fn{j});
  else
    p_out.(fn{j})(idx,1)=stats.(fn{j});
  end
end
return
