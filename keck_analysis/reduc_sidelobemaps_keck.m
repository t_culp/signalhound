function reduc_sidelobemaps_keck(t1,t2,filename, rxNum)
% function reduc_sidelobemaps(t1,t2,filename)

if ~exist('rxNum','var')
  rxNum=0;
end


% Demod:
beammap_demod_par(t1,t2,'k1001',[],1,'fisheye', [], rxNum);

type='sin';
%type = 'quad'; %IDB test

% Get the files:
files=list_arc_files('arc/',t1,t2);
n_files=length(files);
tic
% Concatenate files from beammap_demod_par
for fl=1:n_files
  disp(files{fl}(1,1:15))
  loadfile = strcat('scratch/beammaps/bm_',files{fl}(1,1:15));
  loadfile = [loadfile '_rx' num2str(rxNum)]; 
  e=load(loadfile);
  switch type % Throwout unnecessary fields to save memory
    case 'sin'
      e=rmfield(e.d,{'cos','fb'});
    case 'cos'
      e=rmfield(e.d,{'sin','fb'});      
    case 'quad'
      e=rmfield(e.d,'fb'); 
    case 'raw'
      e=rmfield(e.d,{'sin','cos'});
    case 'moon'
      e=e.d;
  end
  d(fl)=e;
end
toc

clear e

d=structcat(1,d);

nsamp=length(d.pos(:,1));

[p ind]=get_array_info('20121213','obs');

source_lat = repmat(61.2,nsamp,1);
source_lon = repmat(336,nsamp,1);
%Height of MAPO mast FROM base of groundshield = 470 in.
%Extra height of 12.5 + 96 in. from extension rod
%Total height = 578.5 in.
%horizontal Distance from azimuth axis to mast = 525 in.
%distance = 781.2 in. = 19.8
source.distance = 19.8;

x_bin=-180:1:180;
y_bin=35:1:90;
[xg yg]=meshgrid(x_bin,y_bin);
[X Y Z]=sph2cart(xg*pi/180,yg*pi/180,ones(size(xg)));

%keyboard

for ii=1:528
  ip = ii + 528 * rxNum; %Index into p
  if ~isnan(p.r(ip))
    pp=structcut(p,ip);
    inddd=structcut(ind,ip);
%    [az_ap el_ap]=sidelobe_parallax(d.pos(:,1),d.pos(:,2),d.pos(:,3),d.com(:,1),d.com(:,2),d.com(:,3),pp,inddd);
    [az_ap el_ap pa_ap] =  keck_beam_map_pointing(d.pos(:,1), d.pos(:,2), ...
      d.pos(:,3), [], [], source, pp,  'NoMirror' );
%    keyboard
    [rng bear]=distance(el_ap,az_ap,source_lat,source_lon);
    map(:,:,ii)=double(grid_map(az_ap',el_ap',d.sin(:,ii),x_bin,y_bin));
    [pvr(:,ii) S r]=binned_mean(rng,d.sin(:,ii),200);
  end
end

if isempty(strmatch('sidelobemaps/',filename))
  filename=['sidelobemaps/' filename];
end

save(filename,'pvr','map','r','X','Y','Z', 'x_bin', 'y_bin')
