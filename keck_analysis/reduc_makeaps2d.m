function reduc_makeaps2d(mapname,apsopt)
% reduc_fplane(mapname,apsopt)
%
% Stack set of 2daps and write out
%
% e.g.
%
% Accumulate set of sims
% reduc_makeaps2d('1459/???6_aabd_filtp3_weight3_gs_dp1100_jack0.mat')
% 

if(~exist('apsopt','var'))
  apsopt=[];
end
apsopt = get_default_apsopt(apsopt);

% get the set of maps to stack
[s,d]=system(sprintf('/bin/ls -1 maps/%s',mapname));
% for some bizarre reason d now contains non-printing nonsense
d=strip_nonsense(d);
maps=strread(d,'%s')
nmaps=length(maps);


for i=1:nmaps
  
  mapname=maps{i}
  
  % prepare the maps:
  [mapstruct,apsopt,err] = aps_prepare_maps(mapname,apsopt);

  % extract the co-added maps
  map = mapstruct.map;
  m = mapstruct.m;
  coaddopt=mapstruct.coaddopt;

  % make the 2d aps
  pad=true; realimag=true;
  [ad,aps2d]=make_aps2d(m,map,'normal',pad,realimag);
  
  if(i==1)
    % first time initialize the accumulation arrays
    aps2ds=aps2d;
  else
    % subsequent times accumulate
    s=fieldnames(aps2d);
    for j=1:numel(aps2d)
      for k=1:length(s)
        aps2ds(j).(s{k})=aps2ds(j).(s{k})+aps2d(j).(s{k});
      end
    end
  end

  if(0)
    xtic=ad.u_val{1}*2*pi;
    ytic=ad.u_val{2}*2*pi;
    colormap gray
    subplot(1,2,1);
    imagesc(xtic,ytic,aps2d(1).Q./i); axis image; colorbar
    subplot(1,2,2);
    imagesc(xtic,ytic,aps2ds(1).Q./i); axis image; colorbar
    title(i)
    pause
  end
end

% normalize
s=fieldnames(aps2d);
for j=1:numel(aps2d)
  for k=1:length(s)
    aps2ds(j).(s{k})=aps2ds(j).(s{k})/nmaps;
  end
end
aps2d=aps2ds;

% insert xxx in name and save
setname=maps{1}(6:end);

% create output dir if needed
fdir=['aps2d/',fileparts(setname)];
if ~exist(fdir,'dir')
  mkdir(fdir);
end

setname(6:8)='xxx';
save(sprintf('aps2d/%s',setname),'aps2d','apsopt','ad');

return
