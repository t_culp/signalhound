function reduc_plotmaps_forback(tag)
% reduc_plotmaps_forback(tag)
%
% special function to make/plot for/back jack maps from bright source
% runs to see which channels are contributing most to scan dir jack
% failures
%
% e.g.
% reduc_plotmaps_forback('050713_tod_olddecon')
% reduc_plotmaps_forback('060313_tod')
  
% read in tod
load(['data/',tag]);

% Get array info
[p,ind]=get_array_info(tag); % nominal feed positions

% At the moment we want to make maps where each scan row
% falls perfectly into a single row of the map
% to make sure this happens use ideal d.tracker.horiz_off instead
% of actual eloff
d.tracker.horiz_off(:,2)=round(d.tracker.horiz_off(:,2)*100)/100;
samprate=length(d.tf)/length(d.t);
d.eloff=cvec(repmat(d.tracker.horiz_off(:,2)',samprate,1));

% filter half scans
d=filter_scans(d,fs,'m',ind.a);

% get scan info
scan=get_scan_info(d.tracker.source(fs.s(1),:));

% make for maps
fsc=structcut(fs,fs.d==0);
mapu1=make_imaps(d,p,ind,fsc,scan);

% make back maps
fsc=structcut(fs,fs.d==1);
mapu2=make_imaps(d,p,ind,fsc,scan);

% take diff
mapu1{1}.rdoff.map=mapu1{1}.rdoff.map-mapu2{1}.rdoff.map;

% plot the result
maps_plot(mapu1{1},p,ind,tag)

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function maps_plot(map,p,ind,tagf)

q=rotarray(p,map.dk);

limd=[-6e-3,6e-3];
lims=limd*10;

setwinsize(gcf,1400,1000); clf

if(0)
  % get the full timeconst data
  if(str2num(tagf(1:6))<060101)
    load('timeconst/050714.txt')
    disp('2005')
  else
    load('timeconst/060909.txt')
    disp('2006')
  end

  % re-arrange to scan speed order
  n=size(tc);
  i=[1:5:n(1),2:5:n(1),3:5:n(1),4:5:n(1),5:5:n(1)];
  tc=tc(i,:);
  
  % take the mean over obs at each speed
  n=size(tc);
  tc=reshape(tc,[4,n(1)/4,n(2)]);
  tc=squeeze(mean(tc));
  
  tc=tc*1e3;
end

% plot the channels
clf
for i=ind.gla
  x=(i-1)/2;
    
  axes('position',[-q.ra_off_dos(i)/1.9+0.47-0.034,q.dec_off(i)/2.3+0.5,0.065,0.075]);
  m=map.rdoff.map(:,:,i);
  imagesc(map.rdoff.x_tic,map.rdoff.y_tic,m);
  caxis(lims);
  axis xy; axis tight;
  %title(sprintf('%s %.0f, %.0f',p.channel_name{i},tc(5,i),tc(1,i)-tc(5,i)));
  title(sprintf('%s',p.channel_name{i}));
  set(gca,'XDir','reverse');

  axes('position',[-q.ra_off_dos(i)/1.9+0.47+0.034,q.dec_off(i)/2.3+0.5,0.065,0.075]);
  m=map.rdoff.map(:,:,i+1);
  imagesc(map.rdoff.x_tic,map.rdoff.y_tic,m);
  caxis(lims);
  axis xy; axis tight;
  set(gca,'XDir','reverse');
  set(gca,'YTick',[]);
  title(sprintf('%s',p.channel_name{i+1}));
end

map.src=strrep(map.src,' ','\_');

gtitle(sprintf('%s src=%s dk=%.0f',strrep(tagf,'_','\_'),map.src,map.dk));

text(0.8,0.2,sprintf('color range \\pm%.4f',lims(2)));

return
