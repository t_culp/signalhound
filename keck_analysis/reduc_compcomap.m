function reduc_compcomap(file1,file2,ukpervolt,c,pltype,pl)
% reduc_compcomap(file1,file2,ukpervolt,c,pltype,pl)
%
% compare 2 sets of maps and their spectra
% (real vs sim etc)
%
% pltype is a vector specifying which plot types to display
%
% 1 - big multi panel map/histo/powspec plots
% 2 - 2 columns of T,Q,U maps
% 3 - columns of T,Q,U from map 1 and T,Q,U difference
% 4 - 2 columns of T,Q,U noise maps
% 5 - 2 columns of T,Q,U signal/noise maps
% 6 - if non-jack map plot freq jackknife
% 7 - plot 2d hist of pixel-pixel values
% 8 - Plot ratio of noise maps
%
% pl=1 plots to png
%
% c is two element vector giving color stretch for temperature and
% polarization data
%
% reduc_compcomap('sim12_filtp3_weight2_jack0_fd','sim51_filtp3_weight2_jack0_fd',[[519871,441298],[200,2],1])
% reduc_compcomap('b1_2yr_filtp3_weight2_jack0','b1_3yr_filtp3_weight2_jack0_gs',[-2.867e8,-2.281e8],[150,20],1)
% reduc_compcomap('b1_3yr_filtp3_weight2_jack0_gs','b2_cmb2010_filtp3_weight2_jack0_gs',[-2.867e8,-2.281e8;3605,3605*.9],[150,20],1)

if(~exist('ukpervolt','var'))
  ukpervolt=[];
end
if(~exist('c','var'))
  c=[];
end
if(~exist('pltype','var'))
  pltype=[1];
end
if(~exist('pl','var'))
  pl=0;
end

if(isempty(ukpervolt))
  ukpervolt=get_ukpervolt;
end

if(isempty(c))
  c=[250,20];
end

if  any(ukpervolt == 1)
  units= '(Volts)';
else
  units= '(\muK)';
end


nplots=length(pltype);
pp=1;

% get the co-added maps
if isstr(file1)
  load(sprintf('maps/%s',file1))
  if ~exist('map','var') && exist('ac','var')
      map=make_map(ac,m,coaddopt);
      clear ac
  end
  map1=map;
  clear map
  if(exist('coaddopt','var'))
    mapopt1=coaddopt.mapopt{1};
  else
    mapopt1=mapopt;
  end
  m1=m;
else
 map1=file1;
 file1 = 'mapA'
end

if isstr(file2)
  load(sprintf('maps/%s',file2))
  if ~exist('map','var') && exist('ac','var')
      map=make_map(ac,m,coaddopt);
      clear ac
  end
  map2=map;
  if(exist('coaddopt','var'))
    mapopt2=coaddopt.mapopt{1};
  else
    mapopt2=mapopt;
  end
  m2=m;
else
  map2=file2;
  file2 = 'mapB'
end

% if the filenames are the same assume we actually want to compare the
% two halves of a jackknife
if(strcmp(file1,file2))
  map1=map1(:,1);
  map2=map2(:,2);
end

% kludge lines to deal with newer maps which don't have 100/150 split
%map2=map1(:,2);
%map1=map1(:,1);t
%file2=file1;
%map2(2,1)=map2(1,1); % copy single map to old 150GHz slot

% jackknife the maps
map1=jackknife_map(map1);
map2=jackknife_map(map2);

ukpervolt

% cal the maps
if(numel(ukpervolt)==1)
  % same cals
  map1=cal_coadd_maps(map1,ukpervolt);
  map2=cal_coadd_maps(map2,ukpervolt);
else
  % different cals
  map1=cal_coadd_maps(map1,ukpervolt(1));
  map2=cal_coadd_maps(map2,ukpervolt(2));
end

% smooth the  maps
%map1=smooth_maps(m1,map1,0.5);
%map2=smooth_maps(m2,map2,0.5);

% Inject gaussian shaped lumps into variance map to mask point
% sources
if(0)
  map1=mask_pntsrc(m,map1,get_src([],mapopt1));
  map2=mask_pntsrc(m,map2,get_src([],mapopt1));
end

ad=calc_ad2([m.xdos,m.ydos],[m.nx,m.ny]);

% big multi panel map/histo/powspec plots
if(~isempty(intersect(pltype,1)))
  
  if(pl) figure('visible','off');
  else figure(pp);
  end
  clf;
  setwinsize(gcf,1200,600)
  colormap jet
  i=1;
  plot_map(m,map1(i),1,c);
  plot_map(m,map2(i),2,c);
  plot_maphist(map1(i),map2(i))
  plot_varhist(map1(i),map2(i))
  plot_aps(ad,map1(i),map2(i),5,5)
  gtitle(sprintf('150GHz %s (blue) vs %s (red)',file1,file2),[],'none');

  printname2=strcat('maps/',file1,'_comp_multifig.png')
  if(pl) mkpng(printname2); end
  pp=pp+1;
end


 % 2 columns of T,Q,U maps
if(~isempty(intersect(pltype,2)))
  figure(pp),clf;
  pp=pp+1;
  setwinsize(gcf,1000,1000);% colormap jet
  plot_map2(m,map1(1),1,c);
  plot_map2(m,map2(1),2,c);
  gtitle(sprintf('100GHz %s (left) vs %s (right)',file1,file2),[],'none');
  printname1=strcat('maps/',file1,'_100_multifig.png');
  
  %figure(pp),clf;
  %setwinsize(gcf,1000,1000);% colormap jet
  %plot_map2(m,map1(2),1,c);
  %plot_map2(m,map2(2),2,c);
  %gtitle(sprintf('150GHz %s (left) vs %s (right)',file1,file2),[],'none');
  %printname2=strcat('maps/',file1,'_150_multifig.png');
  if(pl) 
    set(0,'CurrentFigure',pp-1);
    mkpng(printname1);
    set(0,'CurrentFigure',pp);
    mkpng(printname2); 
  end
  pp=pp+1;
end
  

% columns of T,Q,U map1-map2
if(~isempty(intersect(pltype,3)))
  figure(pp),clf;
  setwinsize(gcf,1000,1000);% colormap jet
  plot_map2(m,map1(1),1,c/1);
  mapd=jackknife_map([map1,map2]);
  plot_map2(m,mapd(1),2,c/10);
  gtitle(sprintf('%s minus %s, map1 left, difference right',file1,file2),[],'none');

  printname1=strcat('maps/',file1,'_diff.png');
  if(pl) mkpng(printname1); end
  pp=pp+1;
end


% 2 columns of T,Q,U noise maps
if(~isempty(intersect(pltype,4)))
  figure(pp),clf;
  pp=pp+1;
  setwinsize(gcf,1000,1000);% colormap jet
  plot_map3(m,map1(1),1,c);
  plot_map3(m,map2(1),2,c);
  gtitle(sprintf('100GHz %s (left) vs %s (right)',file1,file2),[],'none');
  
  figure(pp),clf;
  pp=pp+1;
  setwinsize(gcf,1000,1000);% colormap jet
  plot_map3(m,map1(2),1,c);
  plot_map3(m,map2(2),2,c);
  gtitle(sprintf('150GHz %s (left) vs %s (right)',file1,file2),[],'none');
end


% 2 columns of T,Q,U signal/noise maps
if(~isempty(intersect(pltype,5)))
  figure(pp),clf;
  pp=pp+1;
  colormap jet
  setwinsize(gcf,1000,1000);% colormap jet
  plot_map5(m,map1(1),1,c);
  plot_map5(m,map2(1),2,c);
  
  figure(pp),clf;
  pp=pp+1;
  colormap jet
  setwinsize(gcf,1000,1000);% colormap jet
  plot_map5(m,map1(2),1,c);
  plot_map5(m,map2(2),2,c);
end


% if non-jack map plot freq jackknife
if(~isempty(intersect(pltype,6)))
  figure(pp),clf;
  pp=pp+1;
  if(mapopt.jacktype==0)
    map1fj=jackknife_map(map1');
    map2fj=jackknife_map(map2');
    setwinsize(gcf,1000,1000); colormap jet
    plot_map2(m,map1fj(1),1,c);
    plot_map2(m,map2fj(1),2,c);
    gtitle(sprintf('Freq jack %s (left) vs %s (right)',file1,file2),[],'none');
  end
end

% plot 2d hist of pixel-pixel values
if(~isempty(intersect(pltype,7)))
  figure(pp),clf;

  setwinsize(gcf,750,1000)
  colormap jet
  plot_scatter(map1(1),map2(1));
  gtitle(sprintf('150GHz %s vs %s',file2,file1),[],'none');

  printname=strcat('maps/',file1,'_comp_2dhist.png');
  if(pl) mkpng(printname); end
  pp=pp+1;

  %figure(pp),clf;
  %setwinsize(gcf,750,1000)
  %colormap jet
  %plot_scatter(map1(2),map2(2));
  %gtitle(sprintf('150GHz %s vs %s',file2,file1),[],'none');
  %pp=pp+1;  
end


% Plot ratio of noise maps
if(~isempty(intersect(pltype,8)))
  figure(pp),clf;
  pp=pp+1;
  setwinsize(gcf,1000,1000); colormap jet
  plot_map4(m,map1(1),map2(1),1,[0.2,1.5]);
  %plot_map4(m,map1(2),map2(2),2,[0.2,1.5]);
  gtitle(sprintf('Noise map ratio %s divided by %s',file2,file1),[],'none');
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_scatter(map1,map2)

plot_scatter_sub(map1.T,map2.T,map1.Tvar,map2.Tvar,0,'T')
plot_scatter_sub(map1.Q,map2.Q,map1.Qvar,map2.Qvar,1,'Q')
plot_scatter_sub(map1.U,map2.U,map1.Uvar,map2.Uvar,2,'U')

subplot(4,2,8)
%mcv=max(abs(map1.QUcovar(:)))/5000;
mcv=1000;
hfill2(map1.QUcovar,map2.QUcovar,30,-mcv,mcv,30,-mcv,mcv);
%plot(map1.QUcovar,map2.QUcovar,'r.','MarkerSize',0.1); axis image
%xlim([-mcv,mcv]); ylim([-mcv,mcv]);
line([-1e9,1e9],[-1e9,1e9]); grid
axis square
title('QU covar');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_scatter_sub(m1,m2,v1,v2,n,t)
subplot(4,2,1+n*2)
no=sqrt(v1); mn=min(no(:)); ind=no<2*mn;
x=m1(ind); y=m2(ind);
m=max([x;y]);
hfill2(x,y,30,-m,m,30,-m,m);
line([-1e9,1e9],[-1e9,1e9]);
axis square
title(sprintf('%s signal',t));

subplot(4,2,2+n*2)
hfill2(sqrt(v1),sqrt(v2),30,0,5*mn,30,0,5*mn);
line([-1e9,1e9],[-1e9,1e9]);
axis square
title(sprintf('%s noise',t));
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_aps(ad,map1,map2,ncol,col)

% calculate the fourier components of maps
map1=calc_map_fouriercomp(map1,ad);
map2=calc_map_fouriercomp(map2,ad);

subplot(3,ncol,col)
max1=plotspec2(ad,map1.Tft,map1.Tft,map1.Tvar,map1.Tvar,'b');
hold on
max2=plotspec2(ad,map2.Tft,map2.Tft,map2.Tvar,map2.Tvar,'r');
ylim([0,max(max1,max2)*1.05])
hold off
title('        T map spectra');

subplot(3,ncol,col+ncol)
max1=plotspec2(ad,map1.Qft,map1.Qft,map1.Qvar,map1.Qvar,'b');
hold on
max2=plotspec2(ad,map2.Qft,map2.Qft,map2.Qvar,map2.Qvar,'r');
%lim([0,150]); ylim([0,0.1])
ylim([0,max(max1,max2)*1.05])
hold off
title('        Q map spectra');

subplot(3,ncol,col+2*ncol)
max1=plotspec2(ad,map1.Uft,map1.Uft,map1.Uvar,map1.Uvar,'b');
hold on
max2=plotspec2(ad,map2.Uft,map2.Uft,map2.Uvar,map2.Uvar,'r');
%lim([0,150]); ylim([0,0.1])
ylim([0,max(max1,max2)*1.05])
hold off
title('        U map spectra');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function maxC=plotspec2(ad,ft1,ft2,var1,var2,str)

% take the (cross) power spectrum
[C_u,u]=powspec(ad,ft1,ft2);

% factor by which power spectrum is supressed
%sf=prod(ad.N_pix)/nansum(rvec((1./var1).*(1./var2)));
%C_u=C_u*sf;

% convert to CMB form
[lo,Cs_lo]=ps2cmbspec(u,ad.del_u,C_u);

plot(lo,Cs_lo,str);
xlim([0,500]); yl=ylim; ylim([0,yl(2)]);
xlabel('multipole l'); ylabel('uK^2')
maxC=max(Cs_lo);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ft=getft(ad,im,va)

% calc the weight mask
w=1./va;

% take product of map and mask
im=im.*w;
im(isnan(im))=0;

% take the ft
ft=i2f(ad,im);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_varhist(map1,map2)
subplot(3,5,4)
c=5*min(sqrt(map1.Tvar(:)));
hfill(sqrt(map1.Tvar),100,0,c);
hold on
hfill(sqrt(map2.Tvar),100,0,c,[],'r');
hold off
xlabel('uK');
title('T noi pix dist')

subplot(3,5,4+5)
c=5*min(sqrt(map1.Qvar(:)));
hfill(sqrt(map1.Qvar),100,0,c);
hold on
hfill(sqrt(map2.Qvar),100,0,c,[],'r');
hold off
xlabel('uK');
title('Q noi pix dist')

subplot(3,5,4+10)
c=5*min(sqrt(map1.Uvar(:)));
hfill(sqrt(map1.Uvar),100,0,c);
hold on
hfill(sqrt(map2.Uvar),100,0,c,[],'r');
hold off
xlabel('uK');
title('U noi pix dist')

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_maphist(map1,map2)
subplot(3,5,3)
c=4*nanstd(map1.T(:));
[dum,ns1]=hfill(map1.T,100,-c,c);
hfill(map1.T,100,-c,c);
hold on
[dum,ns2]=hfill(map2.T,100,-c,c,[],'r');
hfill(map2.T,100,-c,c,[],'r');
ylim([0,max([ns1,ns2])*1.05])
hold off
xlabel('uK');
title('T sig pix dist')

subplot(3,5,3+5)
c=4*nanstd(map1.Q(:));
[dum,ns1]=hfill(map1.Q,100,-c,c);
hfill(map1.Q,100,-c,c);
hold on
[dum,ns2]=hfill(map2.Q,100,-c,c,[],'r');
hfill(map2.Q,100,-c,c,[],'r');
ylim([0,max([ns1,ns2])*1.05])
hold off
xlabel('uK');
title('Q sig pix dist')

subplot(3,5,3+10)
c=4*nanstd(map1.U(:));
[dum,ns1]=hfill(map1.U,100,-c,c);
hfill(map1.U,100,-c,c);
hold on
[dum,ns2]=hfill(map2.U,100,-c,c,[],'r');
hfill(map2.U,100,-c,c,[],'r');
ylim([0,max([ns1,ns2])*1.05])
hold off
xlabel('uK');
title('U sig pix dist')

return

%%%%%%%%%%%%%%%%%%%%%%%%
function plot_map(m,map,col,c)
subplot(3,5,0+col)
imagesc(m.x_tic,m.y_tic,map.T);
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Tvar(:)));
caxis([-c(1),c(1)]);
xlabel('RA'); ylabel('Dec')
title('T map')

subplot(3,5,5+col)
imagesc(m.x_tic,m.y_tic,map.Q);
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Qvar(:))); caxis([-c,c]);
caxis([-c(2),c(2)]);
xlabel('RA'); ylabel('Dec')
title('Q map')

subplot(3,5,10+col)
imagesc(m.x_tic,m.y_tic,map.U);
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Uvar(:)));
caxis([-c(2),c(2)]);
xlabel('RA'); ylabel('Dec')
title('U map')

return

%%%%%%%%%%%%%%%%%%%%%%%%
function plot_map2(m,map,col,c)

subplot(3,2,0+col)
imagesc(m.x_tic,m.y_tic,map.T);
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Tvar(:)));
caxis([-c(1),c(1)]);
colorbar
xlabel('RA'); ylabel('Dec')
title('T map')
if(exist('src'))
  i=src.r>0; hold on; plot(src.ra(i),src.dec(i),'ko','MarkerSize',15); hold off
end

subplot(3,2,2+col)
imagesc(m.x_tic,m.y_tic,map.Q);
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Qvar(:)));
caxis([-c(2),c(2)]);
colorbar
xlabel('RA'); ylabel('Dec')
title('Q map')
if(exist('src'))
  i=src.r>0; hold on; plot(src.ra(i),src.dec(i),'ko','MarkerSize',15); hold off
end

subplot(3,2,4+col)
imagesc(m.x_tic,m.y_tic,map.U);
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Qvar(:)));
caxis([-c(2),c(2)]);
colorbar
xlabel('RA'); ylabel('Dec')
title('U map')
if(exist('src'))
  i=src.r>0; hold on; plot(src.ra(i),src.dec(i),'ko','MarkerSize',15); hold off
end

return

%%%%%%%%%%%%%%%%%%%%%%%%
function plot_map3(m,map,col,c)

subplot(3,2,0+col)
imagesc(m.x_tic,m.y_tic,log10(sqrt(map.Tvar)));
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Tvar(:)));
%caxis([0,c(1)]);
colorbar
xlabel('RA'); ylabel('Dec')
title('log10(T noise map)')

subplot(3,2,2+col)
imagesc(m.x_tic,m.y_tic,log10(sqrt(map.Qvar)));
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Qvar(:)));
%caxis([0,c(1)]);
colorbar
xlabel('RA'); ylabel('Dec')
title('log10(Q noise map)')

subplot(3,2,4+col)
imagesc(m.x_tic,m.y_tic,log10(sqrt(map.Uvar)));
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Qvar(:)));
%caxis([0,c(1)]);
colorbar
xlabel('RA'); ylabel('Dec')
title('log10(U noise map)')

return

%%%%%%%%%%%%%%%%%%%%%%%%
function plot_map4(m,map1,map2,col,c)

subplot(3,2,0+col)
imagesc(m.x_tic,m.y_tic,sqrt(map2.Tvar./map1.Tvar));
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
caxis(c);
colorbar
xlabel('RA'); ylabel('Dec')
title('T noise ratio map')

subplot(3,2,2+col)
imagesc(m.x_tic,m.y_tic,sqrt(map2.Qvar./map1.Qvar));
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
caxis(c);
colorbar
xlabel('RA'); ylabel('Dec')
title('Q noise ratio map')

subplot(3,2,4+col)
imagesc(m.x_tic,m.y_tic,sqrt(map2.Uvar./map1.Uvar));
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
caxis(c);
colorbar
xlabel('RA'); ylabel('Dec')
title('U noise ratio map')

return

%%%%%%%%%%%%%%%%%%%%%%%%
function plot_map5(m,map,col,c)

subplot(3,2,0+col)
imagesc(m.x_tic,m.y_tic,map.T./sqrt(map.Tvar));
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Tvar(:)));
%caxis(c);
colorbar
xlabel('RA'); ylabel('Dec')
title('T/Tnoi')

subplot(3,2,2+col)
mm=map.Q./sqrt(map.Qvar);
imagesc(m.x_tic,m.y_tic,mm);
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Qvar(:)))
%caxis(c);
colorbar
xlabel('RA'); ylabel('Dec')
title('Q/Qnoi')

[mv,ind]=maxmd(abs(mm));
hold on
plot(m.x_tic(ind(2)),m.y_tic(ind(1)),'ko');
hold off

subplot(3,2,4+col)
mm=map.U./sqrt(map.Uvar);
imagesc(m.x_tic,m.y_tic,mm);
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%c=8*min(sqrt(map.Qvar(:)));
%caxis(c);
colorbar
xlabel('RA'); ylabel('Dec')
title('U/Unoi')

[mv,ind]=maxmd(abs(mm));
hold on
plot(m.x_tic(ind(2)),m.y_tic(ind(1)),'ko');
hold off

return

%%%%%%%%%%%%%%%%%%%%%%%%
function plot_map6(m,map,col,c)

subplot(1,2,0+col)
imagesc(m.x_tic,m.y_tic,map.T);
axis xy; set(gca,'YDir','reverse');
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
caxis([-c,c]);
colorbar
xlabel('RA'); ylabel('Dec')
title('T')

return
