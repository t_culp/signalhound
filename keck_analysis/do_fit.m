function [fm,sa,gof,fme]=do_fit(pars,freepars,obs,ide)
% do_fit(pars,freepars,obs,ide)
%
% Fit a set of points to the model
% Provide the space angle errors as extra out arg

[fm,fme,gof,stat]=matmin('gof_spaceangle',pars,freepars,'pointing_model',obs,ide);

% force collim dir to be in range 0-180
if fm(9)>180
  fm(8) = -fm(8);
  fm(9) = fm(9)-180;
end

% Find the model values
mva=pointing_model(fm,ide);

% Find the space angles between observed and model values
%sa=spaceangle(obs.az,obs.el,mva.az,mva.el,'deg');
%
% JMK: the above is WRONG when the encoder offsets are not small
%  Fixed this by subtracting encoder offsets from both obs and mva coords
%  coordinates, to make them close to ide coords.
sa=spaceangle(obs.az-fm(10),obs.el-fm(11),mva.az-fm(10),mva.el-fm(11),'deg');

return
