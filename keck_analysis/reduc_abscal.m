function ukpervolt=reduc_abscal(qmapn,rmapn,cmapn,name,plots,titles)
% ukpervolt=reduc_abscal(qmapn,rmapn,cmapn,name,plots,titles)
%
% Compare amplitude of Fourier modes to derive absolute calibration factor.
% If maps are jack split, then the cal factors for each half will be
% determined separately.
%
%INPUTS
%     qmapn    Real data map. If a string, will load the path, otherwise
%              should be a structure of map file's contents.
%
%     rmapn    WMAP/Planck reference map. If a string, will load the path,
%              otherwise should be a structure of map file's contents.
%
%     cmapn    WMAP/Planck calibration map. If a string, will load the path,
%              otherwise should be a structure of map file's contents.
%
%     name     Output name prefix of saved plot. rx, tile, gcp, etc suffixes
%              will be added depending on provided maps.
%
%     plots    0 skips plots, 1 plots all maps, otherwise an array will plot
%              a particular subset of maps.
%
%     titles   3-element cell array of plot titles for qmap, rmap, and cmap,
%              respectively. Defaults to the filename (without the .mat extension)
%              if not specified. Only used if plots is true.
%
%RETURNS
%     ukpervolt    The abscal factor
%


if ~exist('plots','var')
  plots=[];
end
if isempty(plots)
  plots = 0;
end

if ~exist('titles','var')
  titles=[];
end
if plots
  if isempty(titles)
    [dum,qmapns,dum2] = fileparts(qmapn);
    [dum,rmapns,dum2] = fileparts(rmapn);
    [dum,cmapns,dum2] = fileparts(cmapn);
  else
    qmapns = titles{1};
    rmapns = titles{2};
    cmapns = titles{3};
  end
end

% load in maps
if ischar(qmapn)
  qmap=load(['maps/',qmapn]);
else
  qmap=qmapn;
end
if isfield(qmap,'ac')
    qmap.map=make_map(qmap.ac,qmap.m,qmap.coaddopt);
end

if ischar(rmapn)
  rmap=load(['maps/',rmapn]);
else
  rmap=rmapn;
end
if isfield(rmap,'ac')
    rmap.map=make_map(rmap.ac,rmap.m,rmap.coaddopt);
end

if ischar(cmapn)
  cmap=load(['maps/',cmapn]);
else
  cmap=cmapn;
end
if isfield(cmap,'ac')
    cmap.map=make_map(cmap.ac,cmap.m,cmap.coaddopt);
end

% Inject gaussian shaped lumps into variance map to mask point
% sources
if(0)
  disp('mask pnt src')
  qmap.map=mask_pntsrc(qmap.m,qmap.map,get_src([],qmap.mapopt));
  rmap.map=mask_pntsrc(rmap.m,rmap.map,get_src([],rmap.mapopt));
  cmap.map=mask_pntsrc(cmap.m,cmap.map,get_src([],cmap.mapopt));
end


% pad the maps
m = qmap.m;
disp('padding the maps')
p=2^(nextpow2(max([qmap.m.nx,qmap.m.ny])));
[qmap.m,qmap.map]=pad_map(qmap.m,qmap.map,p);
[rmap.m,rmap.map]=pad_map(rmap.m,rmap.map,p);
[cmap.m,cmap.map]=pad_map(cmap.m,cmap.map,p);

% calc axis data
ad=calc_ad2([qmap.m.xdos,qmap.m.ydos],[qmap.m.nx,qmap.m.ny]);

% calculate the fourier components of maps
disp('taking fft of maps')
qmap.map=calc_map_fouriercomp(qmap.map,ad);
rmap.map=calc_map_fouriercomp(rmap.map,ad);
cmap.map=calc_map_fouriercomp(cmap.map,ad);

ellbin='bicep_norm';
w=ones(size(qmap.map(1).Tft));

% for each freq (and jack split) take required cross spectra
disp('take xspec')
for j=1:numel(qmap.map)
  [aps(j).l,aps(j).rxc,n,be]=calcspec(ad,rmap.map(j).Tft,cmap.map(j).Tft,w,ellbin);
  [aps(j).l,aps(j).rxq,n,be]=calcspec(ad,rmap.map(j).Tft,qmap.map(j).Tft,w,ellbin);
end
aps=reshape(aps,size(qmap.map));

% calc the cal factors
for j=1:numel(aps)
  aps(j).cal=aps(j).rxc./aps(j).rxq;
end

% apply beam correction
for i=1:numel(aps)
  apsc(i).cal=aps(i).cal;
  apsc(i).l=aps(i).l;
end
apsc=reshape(apsc, size(aps));

% Use cal factors in the science bin range (TT range = 1st element)
chibins = get_chibins;
chibins = chibins{1};

% Find the abscal factor for every aps
ukpervolt=zeros(size(aps));
for i=1:numel(aps)
  ukpervolt(i)=mean(apsc(i).cal(chibins));
end

if plots
  % Get the ell range matching the science bins
  ellr = aps(1).l(chibins([1,end]));
  ellr = repmat(ellr, 1, size(ukpervolt,2));

  % Get the nominal ukpervolt and the ratio between the new value and nominal
  ukpvnom = get_ukpervolt(qmap.coaddopt.tags{1});
  ukpvrel = bsxfun(@rdivide, ukpervolt, ukpvnom);

  % p,ind from real map to get particular charactistic sizes
  p = qmap.coaddopt.p;
  ind = qmap.coaddopt.ind;
  nrx = numel(unique(p.rx(ind.la)));
  % List of A then B for per-det maps
  indab = [ind.a, ind.b];
  % mapping from tile to rx
  tiles = unique(p.tile(ind.la));
  seqtiles = unique(p.sequential_tile(ind.la));
  rxtile = arrayfun(@(t) unique(p.rx(p.sequential_tile==t)), seqtiles);

  figure(1); setwinsize(gcf,1200,600);

  if plots==1
    plots = 1:size(qmap.map,1);
  end
  for ii=rvec(plots)
    clf()

    % Get mappings from map index to useful quantities like rx, tile, etc.
    % This is mainly for titles/annotations and automatic file naming.
    rx = NaN;
    tile = NaN;
    seqtile = NaN;
    gcp = NaN;
    switch qmap.coaddopt.coaddtype
      case 1 % per-rx
        rx = ii;
      case {2,3} % per-pair
        rx = p.rx(ind.a(ii)) + 1;
        tile = p.tile(ind.a(ii));
        seqtile = p.sequential_tile(ind.a(ii));
        gcp = [ind.a(ii), ind.b(ii)] - 1;
      case 4 % per-detector
        rx = p.rx(indab(ii)) + 1;
        tile = p.tile(indab(ii));
        seqtile = p.sequential_tile(indab(ii));
        gcp = indab(ii) - 1;
      case 5
        rx = rxtile(ii) + 1;
        tile = unique(p.tile(p.sequential_tile == ii));
        seqtile = ii;
    end

    % The nominal ukpervolt values may be a single value, per-rx, or maybe
    % something else in the future. Make some heuristics on choosing the
    % correct value given the type of input map given.
    switch numel(ukpvnom)
      case 1;   ukpvind = 1;  % shared ukpervolt
      case nrx; ukpvind = rx; % per-rx ukpervolt
    end

    subplot(2,3,1); plot_apmap(m,rmap.m,rmap.map(ii,1),sprintf('ref, %s',rmapns));
    subplot(2,3,2); plot_apmap(m,cmap.m,cmap.map(ii,1),sprintf('cal, %s',cmapns));
    subplot(2,3,3); plot_apmap(m,qmap.m,qmap.map(ii,1),sprintf('data, %s',qmapns));

    % plot the x spec
    subplot(2,3,4)
    plot([aps(ii,:).l],[aps(ii,:).rxc],'.-')
    title('ref x cal');
    xlabel('ell');ylabel('xspec');

    subplot(2,3,5)
    plot([aps(ii,:).l],[aps(ii,:).rxq],'.-')
    title('ref x data');
    xlabel('ell');ylabel('xspec');

    % plot the cal factors
    subplot(2,3,6)
    plot([aps(ii,:).l],[apsc(ii,:).cal],'.-');
    title('cal factors');
    line(ellr,ukpervolt(ii).*[1;1]);
    line(ellr,ukpvnom(ukpvind).*[1;1],'color','g');
    xlabel('ell'); ylabel('uK/fbu');
    xlim(aps(1).l([1,end]));
    if ~isnan(ukpervolt(ii))
      ylim(ukpervolt(ii) + 0.2*abs(ukpervolt(ii)).*[-1,1]);
    end

    axes('Position',get(gca,'Position'),'Xlim',[0,10],'Ylim',[0,10]);
    set(gca,'Visible','off');
    text(1,1,sprintf('cal/nom='));
    text(4,1,sprintf('%5.3f ',ukpvrel(ii,ukpvind)));
    text(1,2,sprintf('ukpervolt='));
    text(4,2,sprintf('%6.0f ',ukpervolt(ii)));

    rxstr = '';
    tilestr = '';
    gcpstr = '';
    freqstr = '';
    pngsuff = '';
    if nrx > 1 && ~isnan(rx);
      rxstr = sprintf(' rx%1d', rx-1);
      pngsuff = rxstr(2:end);
    end
    if ~isnan(tile)
      % BICEP3-style with many tiles with one receiver.
      if numel(tiles) == numel(seqtiles)
        tilestr = sprintf(', tile %02d', seqtile);
      % Keck style with repeated tile indices per receiver
      else
        tilestr = sprintf(', tile %1d', tile);
      end
      pngsuff = sprintf('tile%02d', seqtile);
      tmp = intersect(ind.l,find(p.sequential_tile==seqtile));
      freqstr = sprintf(' (%03i GHz)', p.band(tmp(1)));
    end
    if ~isnan(gcp)
      gcpstr = sprintf('%03d/', gcp);
      gcpstr = [', gcp ' gcpstr(1:end-1)];
      pngsuff = sprintf('gcp%04d', gcp(1));
    end

    plname = ['abscal' rxstr tilestr gcpstr freqstr];
    gtitle(plname, 0.98, 'none');

    pngname = [name '_' pngsuff '.png'];
    mkpng(pngname);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_apmap(um,pm,map,t)
  % The maps by the time of plotting have been padded, so use the unpadded m
  % (um) to select a subset of the padded map to plot (based on pm).
  dx = floor((pm.nx-um.nx)/2);
  dy = floor((pm.ny-um.ny)/2);
  x = dx + (1:um.nx);
  y = dy + (1:um.ny);

  plot_map(um, map.T(y,x) ./ map.Tvar(y,x));
  title(t, 'interpreter','none');

  return

