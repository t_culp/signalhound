function reduc_combcomap(mapname,type_in,type_out,update,force_size)
% reduc_combcomap(mapname,type_in,type_out,update)
%
% Add noise only sim maps to signal only sim maps. Mapname specifies noise only map. 
%
% e.g.  reduc_combcomap('1450/0016_a_filtp3_weight3_gs_dp1000_jack?.mat',5,7,1)
%  to do   0016_a_filtp3_weight3_gs_dp1000_jack0.mat 
%        + 0015_a_filtp3_weight3_gs_dp1000_jack0.mat
%        = 0017_a_filtp3_weight3_gs_dp1000_jack0.mat
%  or
%  reduc_combcomap('1450/0011_a_filtp3_weight3_gs_dp1000_jack?.mat')
%  to do   0011_a_filtp3_weight3_gs_dp1000_jack0.mat 
%        + 0012_a_filtp3_weight3_gs_dp1000_jack0.mat
%        = 0013_a_filtp3_weight3_gs_dp1000_jack0.mat
%
%  force_size ([] default - do nothing), allows to select and expand the signal
%  realization to be used for multiple noise realizations 
%  (used for instance for the planck analysis)
%  force_size = 1 : use the first signal map for all the s+n combinations
%  force_size = [1,2,2,2,2] : mix and match

% default is to regen all
if(~exist('update','var'))
  update=0;
end

if(~exist('force_size','var'))
  force_size=[];
end

% dir function only knows * wildcard
[s,d]=system_safe(sprintf('/bin/ls -1 maps/%s',mapname));
% for some bizarre reason d now contains non-printing nonsense
d=strip_nonsense(d);
maps=strread(d,'%s')
length(maps)

% process the maps in random order. Usefull to farm out the same
% job serveral times to work through the input maps more rapidly.
% only makes sense with update=1 though
if(update==1)
  nmaps=length(maps);
  RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));
  maps = maps(randperm(nmaps));
end

%find index of z independent of length of mapname
z=strfind(maps{1},'_');z=z(1)-1;

for i=1:length(maps)
  
  mapname1=maps{i};
  mapname2=mapname1;
  mapname3=mapname1;

  if isempty(type_in)
    % change to sig only sim -
    % i.e. in xxxxyyyz, z goes from 1->2 or 4->5
    mapname2(z)=mapname2(z)+1;
    
    % output map name
    % change to sig+noi sim -
    % i.e. in xxxxyyyz, z goes from 1->3 or 4->6
    mapname3(z)=mapname3(z)+2;
  else
    mapname2(z)=num2str(type_in);
    mapname3(z)=num2str(type_out);
  end
  
  %what is this for?
  mapname2=strrep(mapname2,'n','p3');
  mapname3=strrep(mapname3,'n','p3');

  % if the corresponding signal only sim exists
  if(exist(mapname2,'file'))
  
    % if output map does not yet exist or we are re-generating
    if(~exist(mapname3,'file') || update==0)
      disp(['Going to make: ',mapname3])
   
      % get the noise maps
      disp(['Loading map 1: ',mapname1])
      load(mapname1);
      % get the signal maps
      disp(['Loading map 2: ',mapname2])
      m2=load(mapname2);
      % 20130723 GPT: If these are ac format instead of map format, then
      % just concatenate them and let make_map deal with it later.
      if exist('ac','var') && isfield(m2,'ac')
        % handle the a special case for planck maps where we want
        % the same signal rlz for all 7 frequencies.
        if ~isempty(force_size)
          % handle two case, either ac is already cell (type7) or not (type6)
          % if force_size is a single number, just use the same part of the ac
          % everywhere.
          if length(force_size)==1
            if ~iscell(ac) 
              force_size = repmat(force_size,size(ac));
            else
              force_size = repmat(force_size,size(ac{end}));
            end
          end        
          % expand it out:
          m2.ac = m2.ac(force_size);
        end
        % if ac is a cell, append in the back...:
        if iscell(ac)
          ac{end+1}=m2.ac;
        else
        % ...otherwise make it a cell:
          ac={ac;m2.ac};
        end
        % sometimes for deproj0000 b is present an filled with nan
        if isfield(coaddopt,'b') && isfield(m2.coaddopt,'b') 
          if size(coaddopt.b)==size(m2.coaddopt.b)
            m2.coaddopt.b=coaddopt.b+m2.coaddopt.b;
          else
            coaddopt = rmfield(coaddopt,'b');
          end
        end
        coaddopt=m2.coaddopt;
        saveandtest(mapname3,'ac','m','coaddopt');
      else
        % If one is a map but the other is ac, we should convert to map.
        if ~exist('map','var')
          map=make_map(ac,m,coaddopt);
        end
        if ~isfield(m2,'map')
          m2.map=make_map(m2.ac,m,coaddopt);
        end
        
        % sum the maps - we just sum signal maps and keep var maps as the
        % noise only sim ones
        for j=1:numel(map)
          map(j).T=map(j).T+m2.map(j).T;
          if(isfield(map,'Q'))
            map(j).Q=map(j).Q+m2.map(j).Q;
            map(j).U=map(j).U+m2.map(j).U;
          end
          if(isfield(map,'Qd'))
            for k=1:numel(map(j).Qd)
              map(j).Qd{k}=map(j).Qd{k}+m2.map(j).Qd{k};
              map(j).Ud{k}=map(j).Ud{k}+m2.map(j).Ud{k};
            end
          end
        end
        
        % deprojection coefficients add. coefficients weights are the
        % same for signal and for noise
        if isfield(coaddopt,'b')
          m2.coaddopt.b=coaddopt.b+m2.coaddopt.b;
        end
        
        % keep the simopt of the signal sims
        coaddopt=m2.coaddopt;
        
        % store back to file
        saveandtest(mapname3,'map','m','coaddopt');
      end
    else
      disp(['Skipping ',mapname3])
    end
  end
end  

return
