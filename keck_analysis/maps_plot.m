function maps_plot(tag,porder,cflag)
% maps_plot(tag,porder,cflag)
%
% Simple plot of total intensity and int time for each field/deck
% at each freq
% Intended for regular update/posting during obs
%
% maps_plot('all')

if(~exist('porder','var'))
  porder=3;
end

if(~exist('cflag','var'))
  cflag=1;
end

% either get a custom stack of days or a pre-saved file of all
if(iscell(tag))
  [mapu,mapd,tag]=maps_read_stack(tag);
else
  load(['data/cmb_',tag]);
end

% fetch p and ind structures
[p,ind]=make_ind;

% combine days
mapu=maps_combine_stack(mapu);

% remove poly from each row of maps - i.e. from each binned scan
mapu=maps_clean(mapu,porder);

% coadd each freq applying nominal feed offsets
mapu100=maps_coadd(mapu,ind.rgl100,p.x_position,p.y_position);
mapu150=maps_coadd(mapu,ind.rgl150,p.x_position,p.y_position);

figure(1)
plot_coadd_maps(mapu100,tag,'100GHz',cflag)
mkpng('all/coadd_100ghz.png');

figure(2)
plot_coadd_maps(mapu150,tag,'150GHz',cflag)
mkpng('all/coadd_150ghz.png');

%figure(10)
%plot_coadd_maps2(mapd100,tag,'100GHz',cflag)
%figure(11)
%plot_coadd_maps2(mapd150,tag,'150GHz',cflag)

keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_coadd_maps(mapu,tag,com,cflag)

setwinsize(gcf,800,800); clf

for j=1:2
  
  % get the maps
  map1=mapu{1,j};
  map2=mapu{2,j};
  
  subplot(4,2,j)
  imagesc(map1.aeoff.x_tic,map1.aeoff.y_tic,maps_conv_map(map1.aeoff,cflag));
  axis xy; axis tight
  title(sprintf('src=%s dk=%.0f',map1.src,map1.dk));
  xlabel('az offset (deg)'); ylabel('el offset (deg)'); 
  colorbar
  hold on;
  plot(79.98-78.75,45.82-50,'ro','MarkerSize',15);
  hold off
  
  subplot(4,2,j+2)
  imagesc(map2.aeoff.x_tic,map2.aeoff.y_tic,maps_conv_map(map2.aeoff,cflag));
  axis xy; axis tight
  title(sprintf('src=%s dk=%.0f',map2.src,map2.dk));
  colorbar
  
  ind=map1.aeoff.itime==0; map1.aeoff.itime(ind)=NaN;

  subplot(4,2,j+4)
  imagesc(map1.aeoff.x_tic,map1.aeoff.y_tic,map1.aeoff.itime);
  axis xy; axis tight
  title(sprintf('itime src=%s dk=%.0f',map1.src,map1.dk));
  colorbar
  hold on;
  plot(79.98-78.75,45.82-50,'ro','MarkerSize',15);
  hold off
  
  subplot(4,2,j+6)
  [x,n]=hfill(map1.aeoff.itime,1000,0);
  plot(x,fliplr(cumsum(fliplr(n)))*0.02^2);
  xlabel('integration time per 0.02 deg^2 pixel (s)');
  ylabel('area (deg^2)');
  title(sprintf('src=%s dk=%.0f',map1.src,map1.dk));  
end
  
gtitle([tag,' coadd maps ',com]);
  
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_coadd_maps2(mapd,tag,com,cflag)
setwinsize(gcf,1200,500); clf

for j=1:2
  
  % difference map only
  map3=mapd{1,j};
  
  subplot(2,2,j)
  imagesc(map3.aeoff.x_tic*cos(50*pi/180),map3.aeoff.y_tic,make_map(map3.aeoff,1,cflag));
  axis xy; axis equal; axis tight
  xlabel('az offset sky (deg)'); ylabel('el offset (deg)'); 
  title(sprintf('src=%s dk=%.0f',map3.src,map3.dk));
  axis xy; axis tight
  if(~exist('c','var'))
    c=caxis;
  else
    caxis(c);
  end
  colorbar
    
  subplot(2,2,j+2)
  imagesc(map3.aeoff.x_tic*cos(50*pi/180),map3.aeoff.y_tic,map3.aeoff.nhit/100);
  axis xy; axis equal; axis tight
  title('int time per pixel')
  axis xy; axis tight
  colorbar
  
end

gtitle([tag,' coadd maps ',com]);
  
return
