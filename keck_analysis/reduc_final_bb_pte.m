function [pte,pte_sumdev]=reduc_final_bb_pte(rf,rfpopt,pl)
% [pte,pte_sumdev]=reduc_final_bb_pte(rf,rfpopt,pl)
%
% "Amplify" the sim statistics to calculate the probability to
% exceed the observed BB chi2 value when it is way out on the tail
%
% The bandpowers are (at least close to) chi2 distributed. So when
% we take a "chi2" from them we are really taking the sum of
% squares of mean subtracted chi2 numbers. This quantity has much
% higher tail than a proper chi2 formed from the mean of squares
% of Gaussian deviates.
%
% e.g.
% to make numbers for B2 paper:
% pte=reduc_final_bb_pte('0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf_rbc',[],1)
%
% load final/0751x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix.mat
% rc=comb_spec(r(4),r(5));
% rfpopt.crossspec=1
% reduc_final_bb_pte(rc,rfpopt,1)

if(~exist('rfpopt','var'))
  rfpopt=[];
end
if(~exist('pl','var'))
  pl=0;
end

if(~isfield(rfpopt,'nspec'))
  rfpopt.nspec=1;
end
if(~isfield(rfpopt,'nsim'))
  rfpopt.nsim=1e7;
end
if(~isfield(rfpopt,'chibins'))
  rfpopt.chibins=get_chibins;
end
if(~isfield(rfpopt,'noffdiag'))
  rfpopt.noffdiag=1;
end
if(~isfield(rfpopt,'mix'))
  rfpopt.mix=1;
end
if(~isfield(rfpopt,'addsysuncer'))
  rfpopt.addsysuncer=1;
end
if(~isfield(rfpopt,'crossspec'))
  rfpopt.crossspec=0;
end

% get the data
if(ischar(rf))
  load(sprintf('final/%s',rf));
else
  r=rf;
  rf='input r struct';
end

% if r multi spec use the desired one
r=r(rfpopt.nspec);

% examine the distribution of each bandpower and then
% regen high stats pseudo sims using these
for i=1:length(rfpopt.chibins{4})
  b=rfpopt.chibins{4}(i);
  d=r.sim(b,4,:)+r.db(b,4);

  if(~rfpopt.crossspec)
    % estimate ndof and x-axis scale factor from mean and sigma of
    % base sims
    nu(i)=2*mean(d)^2/var(d); s(i)=2*mean(d)/var(d);
    % generate chi2 numbers with those values
    sim(i,:)=chi2rnd(nu(i),1,rfpopt.nsim)/s(i);
  else
    % get mean and sigma of base sims
    m(i)=mean(d); s(i)=std(d); sk(i)=skewness(d); ku(i)=kurtosis(d);
    if ~jbtest(d(:),0.15,0.01)
      % generate normal numbers with those values
      sim(i,:)=normrnd(m(i),s(i),1,rfpopt.nsim);
    else
      % use variance gamma if non-Gaussian
      a=m(i)*sk(i)/s(i);
      if 2>=a && a>0
        % correlation conditions are self-consistent
        nu=(m(i)/(s(i)*sk(i)))*(3+sqrt(9-4*a));
        ss=(s(i)^2/(4*abs(m(i))))*sqrt(-6+4*a+2*sqrt(9-4*a));
        cc=sign(m(i))*((sqrt(9-4*a)-1)/(8-4*a))*sqrt(-6+4*a+2*sqrt(9-4*a));
        sim(i,:)=sumprodxyrnd(sqrt(ss),cc,nu,[1,rfpopt.nsim]);
      elseif ku(i)>3
        % leptokurtic, correlation consistent with zero
        nu=6/(ku(i)-3);
        ss=sqrt((ku(i)-3)/6)*s(i);
        cc=0;
        sim(i,:)=sumprodxyrnd(sqrt(ss),cc,nu,[1,rfpopt.nsim]);
      else
        % normal is the best we can do
        sim(i,:)=normrnd(m(i),s(i),1,rfpopt.nsim);
      end
    end
  end
  
end

if(pl)
  figure(1)
  clf; setwinsize(gcf,1400,600);
  nbin=100;
  for i=1:length(rfpopt.chibins{4})
    b=rfpopt.chibins{4}(i);
    d=r.sim(b,4,:)+r.db(b,4);
    ul=mean(d)+10*std(d);
    ll=mean(d)-10*std(d);
    
    [bc,n]=hfill(d,nbin,ll,ul);
    
    subplot(2,5,b)
    % plot the input distribution
    hplot(bc,n,'L');
    % plot the obs value
    u=r.real(b,4)+r.db(b,4);
    hold on; line([u,u],[1e-9,1e9],'color','r'); hold off    
    % plot the resamp distribution
    hold on; hfill(sim(i,:),nbin,ll,ul,[],'gL'); hold off
    
    if(~rfpopt.crossspec)
      % plot dead rekoning chi2
      sf=length(d)*(bc(2)-bc(1))*s(i);
      hold on; plot(bc,sf*chi2pdf(bc*s(i),nu(i)),'r'); hold off
      % plot refit chi2
      [p,pe]=matmin('logl',[80,nu(i),s(i)],[],'chisq_fitfunc',n,bc);
      hold on; plot(bc,chisq_fitfunc(p,bc),'c'); hold off
      title(sprintf('Bandpower %d, %.1f dof',b,nu(i)));
    else
      % plot Gaussian pdf
      sf=length(d)*(bc(2)-bc(1));
      hold on; plot(bc,sf*normpdf(bc,m(i),s(i)),'r'); hold off
      title(sprintf('Bandpower %d',b));
    end
    axis square

  end
  
  % plot ndof
  %figure(2); clf
  %plot(nu);
end
  
% if wanted mix the numbers to introduce correlation
% - this is not strictly correct as the mixed numbers will not have
% the same ndof as initially and hence will not match the sims
% which we are trying to replicate. But in practice they do seem to
% match pretty well - presumably meaning that mixing of this level
% makes no relevant difference.
if(rfpopt.mix)
  s=squeeze(r.sim(rfpopt.chibins{4},4,:));
  c=corr(s');

  % if wanted strip to diag region
  n=rfpopt.noffdiag;
  if(n>=0)
    c=triu(tril(c,n),-n);
  end
  %c
  
  % remove mean and scale to unit std
  m=mean(sim,2);
  s=std(sim,[],2);
  sim=bsxfun(@minus,sim,m);
  sim=bsxfun(@rdivide,sim,s);
  
  % mix the numbers to introduce the desired correlation
  sim=chol(c)'*sim;
  
  % remove scaling and add back mean
  sim=bsxfun(@times,sim,s);
  sim=bsxfun(@plus,sim,m);
  
  %corr(sim')
  
  % overplot the new distributions
  if(pl)
    figure(1)
    for i=1:length(rfpopt.chibins{4})
      b=rfpopt.chibins{4}(i);
      subplot(2,5,b)
      d=r.sim(b,4,:)+r.db(b,4);
      ul=mean(d)+10*std(d); ll=mean(d)-10*std(d);
      hfill(sim(i,:),nbin,ll,ul,[],'rS');
    end
  end
end

% reapply debias to pseudo sims
r.resampsim=sim-repmat(r.db(rfpopt.chibins{4},4),[1,rfpopt.nsim]);

% calc cov and derr (used in calc_devs - recalc'd in add_sys_uncer if
% that is called
r=get_bpcov(r);

% add systematic uncertainty (abscal/beam)
% NB: this will apply to real chi2/sumdev only hence increasing the pte
% as compared to sims
if(rfpopt.addsysuncer)
  r=add_sys_uncer(r,rfpopt.noffdiag);
end

% calc chi2 the official way for real
r=calc_chi(r,rfpopt.chibins,rfpopt.noffdiag,1);
% calc sum devs also
r=calc_devs(r,rfpopt.chibins);

% calc_chi is the official method but it is too slow for >1000
% realizations - it excludes each sim in turn and recalcs the cov
% mat - this is necessary for low stats sims but shouldn't matter
% the way we do it here
% The real chi2 and devs calc'd below should be the same as
% calc_chi and calc_devs (unless syserr is turned on)
%tic
r=calc_chidev_simp(r,rfpopt.chibins,rfpopt.noffdiag);
%toc

if(pl)
  figure(3); clf
  % plot the chi2 distribution
  [bc,n]=hfill(r.schisq,100,0,100,[]);
  subplot(2,2,1)
  hplot(bc,n,'L');
  line([r.rchisq(4),r.rchisq(4)],ylim,'color','r');
  % overplot the chi2 expectation
  sf=length(r.schisq)*(bc(2)-bc(1));
  nu=length(rfpopt.chibins{4});
  hold on; plot(bc,sf*chi2pdf(bc,nu),'r'); hold off

  % plot the chi2 cdf curve
  subplot(2,2,2)
  d=sort(r.schisq);
  pte=(1:length(d))/length(d);
  semilogy(d,1-pte,'g');
  d(end)=NaN; % on a log plot the last point doesn't show
  hold on; plot(0:0.1:100,1-chi2cdf(0:0.1:100,nu),'m');
  line([r.rchisq(4),r.rchisq(4)],[1e-15,1e0],'color','r');
  xlim([0,100]); ylim([1e-15,1]);
  
  % plot sumdev distribution
  subplot(2,2,3)
  [bc,n]=hfill(r.ssumdev,100,-20,20,[]);
  hplot(bc,n,'L');
  line([r.rsumdev(4),r.rsumdev(4)],ylim,'color','r');
end

ng=sum(r.schisq>r.rchisq(4));
n=length(r.schisq);
pte=ng/n;
disp(sprintf('Chi2: %d of %.2e sims greater than real - pte=%.2e, sigma=%.2f',ng,n,pte,-norminv(pte/2)));

ng=sum(r.ssumdev>r.rsumdev(4));
n=length(r.ssumdev);
pte_sumdev=ng/n;
disp(sprintf('Chi1: %d of %.2e sims greater than real - pte=%.2e, sigma=%.2f',ng,n,pte_sumdev,-norminv(pte_sumdev)));
disp(' ');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function r=calc_chidev_simp(r,chibins,n)

% real data
d=r.real(chibins{4},4);

% expectation values from mean of regular sims
m=mean(r.sim(chibins{4},4,:),3);

% get the cov mat from regular sims
s=squeeze(r.sim(chibins{4},4,:));
c=cov(s');
e=std(s,[],2);

% if wanted strip to diag region
if(n>=0)
  c=triu(tril(c,n),-n);
end

%[s,co]=cov2corr(c); co

% take inverse of cov mat
c=inv(c);
 
% take the chi2 and sum devs
r.rchisq_simp=rvec((d-m))*c*cvec((d-m));
r.rsumdev_simp=sum((d-m)./e);

% now the resamp sims

% model subtract
resampsim=bsxfun(@minus,r.resampsim,m);

r.schisq=ones(1,size(resampsim,2));

% loop is slow
%for i=1:size(resampsim,2)
%  r.schisq(i)=resampsim(:,i)'*c*resampsim(:,i);
%end

% speed up trick courtesy of Grant
if(size(resampsim,2)<1e7)
  r.schisq=sum(resampsim.*(c*resampsim),1);
else
  % memory needed by above may be limiting factor when going to very
  % large nsim - do it in chunks
  n=size(resampsim,2);
  l=1:1e7:n;
  u=l+1e7-1; u(end)=n;
  for i=1:length(l)
    r.schisq(l(i):u(i))=sum(resampsim(:,l(i):u(i)).*(c*resampsim(:,l(i):u(i))),1);
  end  
end
  
% scale the model by std
resampsim=bsxfun(@rdivide,resampsim,e);

% take sum devs
r.ssumdev=sum(resampsim);

return
