function reduc_final_chi2tab(rf,f,lab,chbins,doblind)
% reduc_final_chi2tab(rf,f,lab,chbins)
%
% Make table of chi2 values comparing several analyses and jacknifes
%
% If reduc_final file has multiple elements for, i.e. map1, map2, and cross, f
% specifies which spectrum to do chi2 table for. (default=1)
%
% e.g.
%
% reduc_final_chi2tab('1450x1350/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_overrx.mat',1,{'B2'});
% reduc_final_chi2tab('1450x1350/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_overrx.mat',2,{'Keck'});
% reduc_final_chi2tab('1450x1350/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_overrx.mat',[1,2,3],{'B2xKeck'});
%  
% to do the tables and figures listed in this posting for the K2014 analysis: 
% http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20150218_finalpager_K14/
% reduc_final_chi2tab('1351/real_d_filtp3_weight3_gs_dp1102_jack01_pureB_overfreq',[1,2,3],{'K95','K150','cross'},[],1);
%  

if ~exist('f','var') || isempty(f)
  f=1;
end
if ~exist('lab','var') || isempty(lab)
  lab=[];
end
if ~exist('chbins','var') || isempty(chbins)
  chbins=[];
end
if ~exist('doblind','var') || isempty(doblind)
  doblind=false;
end

chibins1=get_chibins(chbins);
chibins2=get_chibins(10);

ra=[];

jackstr='0123456789abcde';

if numel(f)>1
  jackstr=[jackstr,'s'];
end
 
for j=jackstr

  if strcmp(j,'s')
    j0='0';
  else
    j0=j;
  end
  
  n=findstr(rf,'jack');
  rf(n+4)=sprintf('%s',j0);
  load(sprintf('final/%s',rf));

  % spectrum 1, spectrum 2, or cross 
  r=r(f);

  % do spectral jack
  if strcmp(j,'s')
    r=do_spectral_jack(r);
  end
  
  %set expectation value to mean of s+n sims
  for k=1:length(r)
    r(k).expv=mean(r(k).sim,3);
  end
  
  r=get_bpcov(r);
  
  k=strfind(jackstr,j);
  
  ra{k,1}=calc_chi(r,chibins1);
  ra{k,1}=calc_devs(ra{k,1},chibins1);
  
  ra{k,1}(1).jacktype=j;
  
  ra{k,2}=calc_chi(r,chibins2);
  ra{k,2}=calc_devs(ra{k,2},chibins2);
  
  ra{k,2}(1).jacktype=j;
end

if doblind
  k=strfind(jackstr,'0');
  ra(k,:)=[];
end

disp('<H3>chi-sq PTEs</H3>');
htmlstr = print_tab(ra,lab);
fid=fopen('pte_dist_chi2.html','w');
fprintf(fid,htmlstr);
fclose(fid);

% trick do plot the chi statistic instead
ra2 = ra;
for ii=1:numel(ra2)
  for jj=1:size(ra{ii},1)
    ra2{ii}(jj).ptet = ra2{ii}(jj).ptet_sumdev;
    ra2{ii}(jj).ptes = ra2{ii}(jj).ptes_sumdev;
  end
end

disp('<H3>chi PTEs</H3>');
htmlstr = print_tab(ra2,lab);
fid=fopen('pte_dist_chi.html','w');
fprintf(fid,htmlstr);
fclose(fid);

ptes1=nan(size(ra,1),4,size(ra{1},1));
ptes2=ptes1; ptes1_sumdev=ptes1; ptes2_sumdev=ptes1;
for k=1:size(ra,1)-1 % loop over jacks
  if (jackstr(k)=='s') continue; end; % don't include spectral jack.
  for jj=1:size(ra{1},1) % this many different maps like, B2,Keck,cross
    s=[3,4,6]; % include EE,BB,EB only as B13
    % this is how you recognize the cross spectra:
    if length(ra{k,1}(jj).ptet)==9
      s=[3,4,6,9]; % include EE,BB,EB only as B13
    end
    
    for i=s
      % replace zero values with PTEt
      if(ra{k,1}(jj).ptes(i)==0)
        ra{k,1}(jj).ptes(i)=ra{k,1}(jj).ptet(i); disp(sprintf('chi2 1 rep zero j=%d, i=%d',j,i));
      end
      if(ra{k,2}(jj).ptes(i)==0)
        ra{k,2}(jj).ptes(i)=ra{k,2}(jj).ptet(i); disp(sprintf('chi2 2 rep zero j=%d, i=%d',j,i));
      end
      if(ra{k,1}(jj).ptes_sumdev(i)==0)
        ra{k,1}(jj).ptes_sumdev(i)=ra{k,1}(jj).ptet_sumdev(i); disp(sprintf('chi1 1 rep zero j=%d, i=%d',j,i));
      end
      if(ra{k,2}(jj).ptes_sumdev(i)==0)
        ra{k,2}(jj).ptes_sumdev(i)=ra{k,2}(jj).ptet_sumdev(i); disp(sprintf('chi1 2 rep zero j=%d, i=%d',j,i));
      end
      if(ra{k,1}(jj).ptes_sumdev(i)==1)
        ra{k,1}(jj).ptes_sumdev(i)=ra{k,1}(jj).ptet_sumdev(i); disp(sprintf('chi1 1 rep one j=%d, i=%d',j,i));
      end
      if(ra{k,2}(jj).ptes_sumdev(i)==1)
        ra{k,2}(jj).ptes_sumdev(i)=ra{k,2}(jj).ptet_sumdev(i); disp(sprintf('chi1 2 rep one j=%d, i=%d',j,i));
      end
    end
    
    ptes1(k,1:length(s),jj)=ra{k,1}(jj).ptes(s);
    ptes2(k,1:length(s),jj)=ra{k,2}(jj).ptes(s);
    ptes1_sumdev(k,1:length(s),jj)=ra{k,1}(jj).ptes_sumdev(s);
    ptes2_sumdev(k,1:length(s),jj)=ra{k,2}(jj).ptes_sumdev(s);
  end
end


% make pte dist figure
eps = 1e-10;
for jj=1:size(ra{1},1)
  figure; setwinsize(gcf,400,400);
  yl=9;
  subplot(2,2,1); hfill(ptes1(:,:,jj),20,-eps,1+eps); title('Bandpowers 1-5 \chi^2'); ylim([0,yl]);xlim([0,1]);
  subplot(2,2,2); hfill(ptes2(:,:,jj),20,-eps,1+eps); title('Bandpowers 1-9 \chi^2'); ylim([0,yl]);xlim([0,1]);
  subplot(2,2,3); hfill(ptes1_sumdev(:,:,jj),20,-eps,1+eps); title('Bandpowers 1-5 \chi'); ylim([0,yl]);xlim([0,1]); 
  subplot(2,2,4); hfill(ptes2_sumdev(:,:,jj),20,-eps,1+eps); title('Bandpowers 1-9 \chi'); ylim([0,yl]);xlim([0,1]);
  gtitle(lab{jj})
  mkpng(['pte_dist_',lab{jj}])
end

return



%%%%%%%%%%%%%%%%
function r=do_spectral_jack(r)

for k=1:numel(r)
  r(k).real=r(k).real(:,1:6,:);
  % use r=0.2 for spectral jack
  r(k).sim=r(k).simr(:,1:6,:);
  r(k).noi=r(k).noi(:,1:6,:);
end

r0=r;

% Right now this is hard coded for three spectra. 

r0(1).real=r(1).real-r(3).real;
r0(1).sim=r(1).sim-r(3).sim;
r0(1).noi=r(1).noi-r(3).noi;

r0(2).real=r(1).real-r(2).real;
r0(2).sim=r(1).sim-r(2).sim;
r0(2).noi=r(1).noi-r(2).noi;

r0(3).real=r(3).real-r(2).real;
r0(3).sim=r(3).sim-r(2).sim;
r0(3).noi=r(3).noi-r(2).noi;

r=r0;

return

%%%%%%%%%%%%%%%%%%%%%%
function htmlstr_full = print_tab(ra,lab)

for k=1:length(ra)
  js{k}=get_jackdef(ra{k}(1).jacktype);
end
ss={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};

labind=1;
sp=[1,3;1,2;3,2];

htmlstr_full = '';

for f=1:size(ra{1},1)% loop over the map combinations
  
  htmlstr='<p><table><tr><td>Spectrum bps 2-6/2-10</td><td>';

  for j=1:size(ra,1) %loop over jacks

    if any(strfind(js{j},'Spectral'))
      jt=sprintf('%s minus %s',lab{sp(f,1)},lab{sp(f,2)});
    else
      jt=js{j};
    end
    
    if j~=size(ra,1)
      htmlstr=sprintf('%s %s</td><td>',htmlstr,jt);
    else
      htmlstr=sprintf('%s %s</td></tr>',htmlstr,jt);
    end
  end
  
  % loop over the spectra
  for s=1:length(ra{1,1}(f).ptet)
    if ~isempty(strfind(ss{s},'T'))
      htmlstr=sprintf('%s <tr><td> %s</td><td>',htmlstr,ss{s});
    else
      htmlstr=sprintf('%s <tr bgcolor="#EDD1EC"><td> %s</td><td>',htmlstr,ss{s});
    end

    for j=1:size(ra,1) %loop over jacks
      if length(ra{j,1}(f).ptet)>=s
        for i=[1,2] %loop over chibins
          ptet=ra{j,i}(f).ptet(s);
          ptes=ra{j,i}(f).ptes(s);

          if ptes>0 & ptes<1
            pte=ptes;
            tsub=false;
          else
            pte=ptet;
            tsub=true;
          end
          
          if(pte>0.99|pte<0.01)

            if pte <= 0.001
              if pte == 0
                htmlstr=sprintf('%s   <b>0</b>',htmlstr);
              else
                htmlstr=sprintf('%s  <b>%1.0d</b>',htmlstr,pte);
              end
            elseif pte==1
              htmlstr=sprintf('%s   <b>1</b>',htmlstr);
            else
              htmlstr=sprintf('%s  <b>%5.3f</b>',htmlstr,pte);
            end
          
          else
            htmlstr=sprintf('%s  %5.3f',htmlstr,pte);
          end
          
          if tsub
            htmlstr=sprintf('%s*',htmlstr);
          end
            
          if i==1
            htmlstr=sprintf('%s/',htmlstr);
          end
        end % loop over chi bins
      end
      htmlstr=sprintf('%s</td>',htmlstr);
      
      if j==size(ra,1)
        htmlstr=sprintf('%s</tr>',htmlstr);
      else
        htmlstr=sprintf('%s<td>',htmlstr);
      end
      
    end
    
  end
  
  htmlstr=sprintf('<H4>%s</H4>%s',lab{labind},htmlstr);
  disp(sprintf('<H4>%s</H4>',lab{labind}));labind=labind+1;
  htmlstr=sprintf('%s </table><p>',htmlstr);
  disp(htmlstr)
  htmlstr_full = [htmlstr_full,htmlstr];
end

return

