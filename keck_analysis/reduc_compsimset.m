function reduc_compsimset(ssn1,ssn2,ssn3,lmax)
% reduc_compsimset(ssn1,ssn2,ssn3,lmax)
%
% Compare 2 sim sets
%
% If 3rd sim set provided will be subtracted from 2nd
%
% reduc_compsimset('sim12xxx4_filtn_weight2_jack0','sim51xxx4_filtn_weight2_jack0')
% reduc_compsimset('sim12_filtp3_weight2_jack2_fd','sim13_filtp3_weight2_jack2_fd')

if(~exist('ssn3'))
  ssn3=[];
end

if(~exist('lmax'))
  lmax=150;
end

s1=load(sprintf('aps/%s',ssn1)); coaddopt=s1.coaddopt; s1=s1.aps;
s2=load(sprintf('aps/%s',ssn2)); s2=s2.aps;

ssn1=strrep(ssn1,'_','\_');
ssn2=strrep(ssn2,'_','\_');

% merge aps bins
%s1=aps_bin_merge(s1,4);
%s2=aps_bin_merge(s2,4);

% if 3rd provided sub from 2nd
if(~isempty(ssn3))
  s3=load(sprintf('aps/%s',ssn3)); s3=s3.aps;
  %s3=aps_bin_merge(s3,4);
    
  % force 2nd and 3rd to have same size
  n=min([size(s2(1).Cs_l,3),size(s3(1).Cs_l,3)])
  for i=1:numel(s2)
    s2(i).Cs_l=s2(i).Cs_l(:,:,1:n);
    s3(i).Cs_l=s3(i).Cs_l(:,:,1:n);
    % subtract 3 from 2
    s2(i).Cs_l=s2(i).Cs_l-s3(i).Cs_l;
  end
end

% strip down so both sets have same number of realizations
[size(s1(1).Cs_l,3),size(s2(1).Cs_l,3)]
n=min([size(s1(1).Cs_l,3),size(s2(1).Cs_l,3)])
for i=1:size(s1);
   s1(i).Cs_l=s1(i).Cs_l(:,:,1:n);
end
for i=1:size(s2);
   s2(i).Cs_l=s2(i).Cs_l(:,:,1:n);
end

% gen mean and std
s1=process_simset(s1);
s2=process_simset(s2);

% get input model from cmbfast
inpmod=load_cmbfast([],coaddopt);


% compare mean
figure(1); clf
setwinsize(gcf,1400,1000)
if(length(s1)>1)
  plot_mean(1,s1(1),s2(1),inpmod,'100GHz',lmax);
  plot_mean(2,s1(2),s2(2),inpmod,'150GHz',lmax);
  plot_mean(3,s1(3),s2(3),inpmod,'cross',lmax);
else
  plot_mean(1,s1(1),s2(1),inpmod,'150GHz',lmax);
  %plot_mean(1,s1(1),s2(1),inpmod,'freq jack',lmax);
end
gtitle(sprintf('mean values: %s (magenta) and %s (blue)',ssn2,ssn1));

% show ratio of means
figure(2); clf
setwinsize(gcf,1400,1000)
if(length(s1)>1)
  plot_rmean(1,s1(1),s2(1),inpmod,'100GHz',lmax);
  plot_rmean(2,s1(2),s2(2),inpmod,'150GHz',lmax);
  plot_rmean(3,s1(3),s2(3),inpmod,'cross',lmax);
else
  plot_rmean(1,s1(1),s2(1),inpmod,'150GHz',lmax);
  %plot_rmean(1,s1(1),s2(1),inpmod,'freq jack',lmax);
end
gtitle(sprintf('ratio of means: %s divided by %s',ssn2,ssn1));

if(n==1)
  return
end

% plot all realizations
figure(5); clf
setwinsize(gcf,1400,1000)
if(length(s1)>1)
  plot_real(1,s1(1),s2(1),'100GHz',lmax);
  plot_real(2,s1(2),s2(2),'150GHz',lmax);
  plot_real(3,s1(3),s2(3),'cross',lmax);
else
  plot_real(1,s1(1),s2(1),'150GHz',lmax);
  %plot_real(1,s1(1),s2(1),'freq jack',lmax);
end
gtitle(sprintf('all realizations: %s (magenta), %s (blue)',ssn2,ssn1));


% doesn't make sense to plot ratio std and cov if only 1 realization
if(n==1)
  return
end

% compare std
figure(3); clf
setwinsize(gcf,1400,1000)
if(length(s1)>1)
  plot_std(1,s1(1),s2(1),'100GHz',lmax);
  plot_std(2,s1(2),s2(2),'150GHz',lmax);
  plot_std(3,s1(3),s2(3),'cross',lmax);
else
  plot_std(1,s1(1),s2(1),'150GHz',lmax);
  %plot_std(1,s1(1),s2(1),'freq jack',lmax);
end
gtitle(sprintf('spread of values: %s (magenta) and %s (blue)',ssn2,ssn1));

% compare std
figure(4); clf
setwinsize(gcf,1200,1000)
if(length(s1)>1)
  plot_rstd(1,s1(1),s2(1),'100GHz',lmax);
  plot_rstd(2,s1(2),s2(2),'150GHz',lmax);
  plot_rstd(3,s1(3),s2(3),'cross',lmax);
else
  plot_rstd(1,s1(1),s2(1),'150GHz',lmax);
  %plot_rstd(1,s1(1),s2(1),'freq jack',lmax);
end
gtitle(sprintf('ratio of spreads: %s divided by %s',ssn2,ssn1));

% strip down so both sets have same number of realizations
n=min([size(s1(1).Cs_l,3),size(s2(1).Cs_l,3)])
for i=1:size(s1);
   s1(i).Cs_l=s1(i).Cs_l(:,:,1:n);
end
for i=1:size(s2);
   s2(i).Cs_l=s2(i).Cs_l(:,:,1:n);
end

% look at cov mat
figure(6); clf
setwinsize(gcf,1400,1000)
if(length(s1)>1)
  plot_covmat(1,s1(1),s2(1),'100GHz')
  plot_covmat(2,s1(2),s2(2),'150GHz')
  plot_covmat(3,s1(3),s2(3),'cross')
else
  plot_covmat(1,s1(1),s2(1),'150GHz')
  %plot_covmat(1,s1(1),s2(1),'freq jack')
end
gtitle(sprintf('sqrt(abs(cov)) : 1=%s, 2=%s',ssn1,ssn2));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_mean(n,s1,s2,inpmod,freq,lmax)

% offset in l so can see both bars
s2.l=s2.l+5;

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];
for i=1:size(s1.Cs_l,2)
  z=(j(i)-1)*4+n+k(i);
  subplot_grid(6,4,z)

  errorbar2(s1.l,s1.mean(:,i),s1.eom(:,i),'b.');
  hold on;
  errorbar2(s2.l,s2.mean(:,i),s2.eom(:,i),'m.');
  plot(inpmod.l,inpmod.Cs_l(:,i),'r');
  hold off;
  ylabel(lab{i});
  axis tight
  xlim([0,lmax]);
  %ylim([0,0.01]);
  if(i==1)
    title(freq);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_rmean(n,s1,s2,inpmod,freq,lmax)

%yrange=[0,1.5;0.5,1.5;0.5,1.5;0.5,1.5;-2,2;-2,2];
%yrange=[0,1.5;0,1.5;0,1.5;0,1.5;-2,2;-2,2;-2,2;-2,2;-2,2];
yrange=[0,0.02;0,0.02;0,0.02;0,0.5;-2,2;-2,2;-2,2;-2,2;-2,2];

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};

j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];
for i=1:size(s1.Cs_l,2)
  z=(j(i)-1)*4+n+k(i);
  subplot_grid(6,4,z)

  ferr1=s1.eom(:,i)./s1.mean(:,i);
  ferr2=s2.eom(:,i)./s2.mean(:,i);
  ferr=pyth(ferr1,ferr2);
  rmean=s2.mean(:,i)./s1.mean(:,i);
  err=rmean.*ferr;
  errorbar2(s1.l,rmean,err,'b.');
  %plot(s1.l,s2.mean(:,i)./s1.mean(:,i),'b.-');
  ylabel(lab{i});
  ylim(yrange(i,:));
  xlim([0,lmax]);
  line(xlim,[1,1],'color','r')
  if(i==1)
    title(freq);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_std(n,s1,s2,freq,lmax)

% offset in l so can see both bars
s2.l=s2.l+5;

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};

j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];
for i=1:size(s1.Cs_l,2)
  z=(j(i)-1)*4+n+k(i);
  subplot_grid(6,4,z)

  errorbar2(s1.l,s1.std(:,i),s1.eos(:,i),'b.');
  hold on;
  errorbar2(s2.l,s2.std(:,i),s2.eos(:,i),'m.');
  hold off;
  ylabel(lab{i});
  axis tight
  xlim([0,lmax]);
  if(i==1)
    title(freq);
  end

end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_rstd(n,s1,s2,freq,lmax)


lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];
for i=1:size(s1.Cs_l,2)
  z=(j(i)-1)*4+n+k(i);
  subplot_grid(6,4,z)
 
  ferr1=s1.eos(:,i)./s1.std(:,i);
  ferr2=s2.eos(:,i)./s2.std(:,i);
  ferr=pyth(ferr1,ferr2);
  rstd=s2.std(:,i)./s1.std(:,i);
  err=rstd.*ferr;
  errorbar2(s1.l,rstd,err,'b.');
  
  %plot(s1.l,s2.std(:,i)./s1.std(:,i),'b.-');
  ylabel(lab{i});
  ylim([0,1.5]);
  xlim([0,lmax]);
  line(xlim,[1,1],'color','r')
  if(i==1)
    title(freq);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_covmat(n,s1,s2,freq,lmax)

bl=2;
bu=10;

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};

j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];
for i=1:size(s1.Cs_l,2)
  
  co1=corrcoef(squeeze(s1.Cs_l(:,i,:))');
  co2=corrcoef(squeeze(s2.Cs_l(:,i,:))');

  cv1=cov(squeeze(s1.Cs_l(:,i,:))');
  cv2=cov(squeeze(s2.Cs_l(:,i,:))');
  
  z=(j(i)-1)*8+(n-1+k(i))*2+1;
  subplot(6,8,z)

  %imagesc(co1(bl:bu,bl:bu));
  imagesc(sqrt(abs(cv1(bl:bu,bl:bu))));
  ca=caxis;
  xlabel(sprintf('%.0e to %.0e',ca));
  ylabel(lab{i})
  axis xy; axis image

  if(i==1)
    title(sprintf('case 1 %s',freq));
  end
  
  subplot(6,8,z+1)
  %imagesc(co2(bl:bu,bl:bu));
  imagesc(sqrt(abs(cv2(bl:bu,bl:bu))));
  caxis(ca);
  xlabel(sprintf('%.0e to %.0e',ca));
  ylabel(lab{i})
  axis xy; axis image
  
  if(i==1)
    title(sprintf('case 2 %s',freq));
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_real(n,s1,s2,freq,lmax)

% offset in l so can see both bars
s2.l=s2.l+5;

yl1=[0,15e-3;-2e-3,2e-3;0,3e-3;0,3e-3;-3e-3,3e-3;-4e-4,4e-4;-2e-3,2e-3;-3e-3,3e-3;-4e-4,4e-4];
yl2=[-7.5e-3,7.5e-3;-2e-3,2e-3;-1.5e-3,1.5e-3;-1.5e-3,1.5e-3;-3e-3,3e-3;-4e-4,4e-4;-2e-3,...
     2e-3;-3e-3,3e-3;-4e-4,4e-4];



lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];
for i=1:size(s1.Cs_l,2)
  z=(j(i)-1)*4+n+k(i);
  subplot_grid(6,4,z)

  plot(s1.l,squeeze(s1.Cs_l(:,i,:)),'b');
  hold on;
  plot(s2.l,squeeze(s2.Cs_l(:,i,:)),'m');
  hold off;
  ylabel(lab{i});
  axis tight
  xlim([0,lmax]);
  if(i==1)
    title(freq);
  end
end

return
