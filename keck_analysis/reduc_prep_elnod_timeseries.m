function [en,enm]=reduc_prep_elnod_timeseries(tagz,readfiles,grpname)
% [en,enm]=reduc_prep_elnod_timeseries(tagz,readfiles,grpname)
%
% In reduc_applycal we need to screen out data taken when the weather
% is so bad the elnod values are crazy.
% Here we investigate the long term stability of the elnod values and
% save out the elmod timeseries
%
% reduc_prep_elnod_timeseries(get_tags('cmb2010'),1,'2010')

if(~exist('readfiles','var'))
  readfiles=0;
end
if(~exist('grpname','var'))
  grpname='';
end

% use set of channels valid for start day
[p,ind]=get_array_info(tagz{1});

if(readfiles)

  % k is scanset index
  k=0;
  
  i=1;
  for z=1:length(tagz)
    tag=tagz{z}
    
    % get the cal info
    load(['scratch/',tag,'_calval.mat']);

    en.g=en.g(:,:,2);

    % save in structure
    ena(i)=en;
    i=i+1;
  end
  
  en=structcat(1,ena);

  % calc gain ratios etc
  en=calc_quant(en,ind);

  % save data
  save(sprintf('elnod_timeseries_%s',grpname),'en','tagz');
  
else
  load(sprintf('elnod_timeseries_%s',grpname));
end

% convert from mjd to matlab datenum
do=datenum(2006,5,15)-53871;

% plot elnod timeseries
if(nargout==0)
  figure(1); clf; setwinsize(gcf,1000,800);
  set(gcf,'DefaultAxesColorOrder',jet(length(ind.rgl150)));

  % plot the elnod timeseries
  subplot(3,1,1)
  plot(en.t+do,en.g(:,ind.rgl150));
  axis tight; xaxis_time(gca);
  ylim([-0.5,2]*1e4);
  ylabel('raw elnods')

  % plot the ratios versus median which are used to cal
  subplot(3,1,2)
  plot(en.t+do,en.gr(:,ind.rgl150));
  axis tight; xaxis_time(gca);
  ylim([-0.5,2]);
  ylabel('each channel / median(all) = relgain')  

  subplot(3,1,3)
  set(gcf,'DefaultAxesColorOrder',jet(length(ind.rgl150a)));
  plot(en.t+do,en.grab(:,ind.rgl150a));
  axis tight; xaxis_time(gca);
  ylim([-0.5,2]);
  ylabel('a/b elnod pair ratios')
end

if(nargout==0)
  % visualize norm elnod timeseries
  figure(2); clf; setwinsize(gcf,1000,700);
  imagesc(en.grn(:,ind.rgl150)')
  caxis([0.5,1.5]);
  %set(gca,'YTick',1:length(ind.rgl150))
  %set(gca,'YTickLabel',p.channel_name(ind.rgl150));
  colorbar
  title('elnod relgain timeseries normalized to their long term medians')
  xlabel(sprintf('elnod number (2 elnod per tag times %d tags)',length(tagz)));

  % visualize norm elnod pair timeseries
  figure(3); clf; setwinsize(gcf,1000,700);
  imagesc(en.grabn(:,ind.rgl150a)')
  caxis([0.5,1.5]);
  colorbar
  title('elnod a/b pair ratio timeseries normalized to their long term medians')
  xlabel(sprintf('elnod number (2 elnod per tag times %d tags)',length(tagz)));
end

if(nargout==0)
  % plot norm elnod timeseries distributions
  figure(4)
  hfill(en.grn(:,ind.rgl150),1000,0.6,1.4,[],'L');
  hfill(en.grabn(:,ind.rgl150a),1000,0.6,1.4,[],'LSr');
  legend({'ind ch','pair ratio'})
  xlabel('fractional deviation')
  title('distributions of elnod timeseries normalized to their long term medians')
  grid
end

% develop possible cuts

% look for elnod values which deviate by more than some percentage
% from their long term median
ltsct=0.1;
ltsc=abs(1-en.grn)>ltsct|isnan(en.grn);
% when we apply cal we use mean (or interp) of before/after elnod
% gains - therefore if we make a cut on the above quantity we are
% actually demanding that both of a pair pass cut
ltsc=ltsc(1:2:end,:)|ltsc(2:2:end,:);
ltsc100=sum(ltsc(:,ind.rgl100),2)>=1;
ltsc150=sum(ltsc(:,ind.rgl150),2)>=1;

% look for before/after elnod pairs which differ by more than some
% percentage
bagr=en.gr(1:2:end,:)./en.gr(2:2:end,:);
bagrct=0.1;
bagrc=abs(bagr-1)>bagrct|isnan(bagr);
bagrc100=sum(bagrc(:,ind.rgl100),2)>=1;
bagrc150=sum(bagrc(:,ind.rgl150),2)>=1;

% look for a/b gain ratio which shifts by more than some fraction
% before/after
abgr=en.grab(1:2:end,:)./en.grab(2:2:end,:);
abgrct=0.03;
abgrc=abs(abgr-1)>abgrct|isnan(abgr);
abgrc100=sum(abgrc(:,ind.rgl100a),2)>=1;
abgrc150=sum(abgrc(:,ind.rgl150a),2)>=1;

if(nargout==0)
  % visualize cut quantities
  figure(5); clf; setwinsize(gcf,1000,1000);
  subplot(3,1,1)
  c=ltsc(:,ind.rgl150)';
  imagesc(c);
  title(sprintf('elnod long term stability cut with threshold %.3f giving %.3f cut frac',...
      ltsct,sum(c(:)./prod(size(c)))));

  subplot(3,1,2)
  c=bagrc(:,ind.rgl150)';
  imagesc(c);
  title(sprintf('elnod before/after stability cut with threshold %.3f giving %.3f cut frac',...
      bagrct,sum(c(:)./prod(size(c)))));
  
  subplot(3,1,3)
  c=abgrc(:,[ind.rgl100a,ind.rgl150a])';
  imagesc(c);
  title(sprintf('elnod before/after pair ratio stability cut with threshold %.3f giving %.3f cut frac',...
      abgrct,sum(c(:)./prod(size(c)))));
end

return

if(nargout==0)
  % plot possible cuts
  figure(6); clf; setwinsize(gcf,1000,600);
  subplot(3,1,1)
  plot(en.t(1:2:end)+do,ltsc100,'r')
  hold on
  plot(en.t(1:2:end)+do,ltsc150,'b')
  hold off
  axis tight; xaxis_time(gca); ylim([-0.1,1.1]);
  title(sprintf('fraction %5.3f/%5.3f elnod pairs fail long term stability cut at 100/150GHz (threshold %.2f)',...
      sum(ltsc100)./length(ltsc100),sum(ltsc150)./length(ltsc150),ltsct));
  
  subplot(3,1,2)
  plot(en.t(1:2:end)+do,bagrc100,'r')
  hold on
  plot(en.t(1:2:end)+do,bagrc150,'b')
  hold off
  axis tight; xaxis_time(gca); ylim([-0.1,1.1]);
  title(sprintf('fraction %5.3f/%5.3f elnod pairs fail before/after gain ratio cut at 100/150GHz (threshold %.2f)',...
      sum(bagrc100)./length(bagrc100),sum(bagrc150)./length(bagrc150),bagrct));
  
  subplot(3,1,3)
  plot(en.t(1:2:end)+do,abgrc100,'r')
  hold on
  plot(en.t(1:2:end)+do,abgrc150,'b')
  hold off
  axis tight; xaxis_time(gca); ylim([-0.1,1.1]);
  title(sprintf('fraction %5.3f/%5.3f elnod pairs fail a/b ratio before/after ratio cut at 100/150GHz (theshold %.2f)',...
      sum(abgrc100)./length(abgrc100),sum(abgrc150)./length(abgrc150),abgrct));  
end

return

% take a copy to try masking out on
enm=en;

% mask the periods when any channel bad
x=abs(1-en.grn(:,ind.rgl150));
enm.g(find(sum(x>f,2)>0),ind.rgl150)=NaN;
x=abs(1-en.grn(:,ind.rgl100));
enm.g(find(sum(x>f,2)>0),ind.rgl100)=NaN;
x=abs(1-en.grn(:,ind.l220));
enm.g(find(sum(x>f,2)>0),ind.l220)=NaN;

% recalc the ratios etc
enm=calc_quant(enm,ind);

if(nargout==0)
  % visualize masked norm elnod timeseries
  figure(5); clf; setwinsize(gcf,1000,700);
  imagesc(enm.grn(:,ind.rgl)')
  %set(gca,'YTick',1:length(ind.rgl))
  %set(gca,'YTickLabel',p.channel_name(ind.rgl));
  colorbar
  title('cleaned normalized elnod timeseries')
  xlabel(sprintf('elnod number (20 elnod per phase times %d phases)',length(tagz)));
end

if(nargout==0)
  % same for A/B ratio
  figure(6); clf; setwinsize(gcf,1000,700);
  imagesc(enm.grabn(:,ind.rgla)')
  set(gca,'YTick',1:length(ind.rgla))
  %for i=ind.l
  %  p.channel_name_pair{i}=p.channel_name{i}(1:3);
  %end
  %set(gca,'YTickLabel',p.channel_name_pair(ind.rgla));
  %caxis([0.6,1.4]);
  colorbar
  title('cleaned normalized elnod A/B timeseries')
  xlabel(sprintf('elnod number (20 elnod per phase times %d phases)',length(tagz)));
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function x=calc_quant(x,ind)

% take mean gain for good paired channels at each time step
x.m100=nanmedian(x.g(:,ind.rgl100),2);
x.m150=nanmedian(x.g(:,ind.rgl150),2);

% fill m array with median for each channels group
m=ones(size(x.g));
m(:,ind.gl100)=repmat(x.m100,[1,length(ind.gl100)]);
m(:,ind.gl150)=repmat(x.m150,[1,length(ind.gl150)]);

% take ratio of each versus mean - this is what we do in cal_scans
x.gr=x.g./m;

% take A/B ratio within each pair
x.grab=zeros(size(x.g));
x.grab(:,ind.a)=x.g(:,ind.a)./x.g(:,ind.b);

% normalize each relative gain to its long
% term median to see fractional fluctuation
x.grn=bsxfun(@rdivide,x.gr,nanmedian(x.gr));

% do the same thing for the A/B ratios
x.grabn=bsxfun(@rdivide,x.grab,nanmedian(x.grab));

return
