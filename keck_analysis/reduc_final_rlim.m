function [r,rfropt]=reduc_final_rlim(rf,rfropt,pl)
% r=reduc_final_rlim(rf,rfropt,pl)
%
% calculate r 95% conf limit and max likelihood for real/sims from r structure
%
% input:
% r structure = loaded from final/XXX.mat file
%
% rfropt structure of options:
%   calc_type='r1d'  - hold lensing at LCDM expectation and vary r
%           ='rvsl' - scale lensing amp and vary r
%           ='rvsnt'- tensor spectral index vs r
%           ='cvsi' - fit simple power law (holding lensing at expectation)
%   nspec = if r is multi spectrum structure use this one
%   rvals = range of r values at which to compute likelihood
%   lvals = range of lensing scale factors
%   chibins = bandpowers to use (defaults to standard get_chibins)
%   method: 'hl'   Hamimeche Lewis style (default)
%           'chi2' Standard chi2 calc with option:
%               addxfac - use offset lognormal (default=1)
%   showreal = show the r-limit for the real data (default=0)
%   debias_lensing = add lensing B modes to the model (default=1)
%   addsysuncer = add systematic uncertainty (default=1)
%   noffdiag = number of off diags to use in bpcm (default=1)
%   fiducialr = rescale the error bars to this value of r - only
%               valid for auto spectrum!
%   fgmod = filename or aps of foreground model to debias
%
% output: r with fields rvals, rlike, slike, rconf95, sconf95 
%	Note - r.expv is modified by this function! 
%
% e.g.
% rfropt.fgmod='0704/xxx8_allcmb_dust01_filtp3_weight3_gs_dp1100_jack0_final_matrix'
% reduc_final_rlim('0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf',rfropt)
%
% rfropt.calc_type='cvsi'
% reduc_final_rlim('0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_matrix_overrx',rfropt)
% reduc_final_rlim('0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf')

if(~exist('rfropt','var'))
  rfropt=[];
end
if(~exist('pl','var'))
  pl=1;
end

if(~isfield(rfropt,'calc_type'))
  rfropt.calc_type='r1d';
end
if(~isfield(rfropt,'nspec'))
  rfropt.nspec=1;
end
if(~isfield(rfropt,'method'))
  rfropt.method='hl';
end
if(~isfield(rfropt,'chibins'))
  rfropt.chibins=get_chibins;
end
if(~isfield(rfropt,'addsysuncer'))
  rfropt.addsysuncer=1;
end
if(~isfield(rfropt,'addxfac'))
  rfropt.addxfac=1;
end
if(~isfield(rfropt,'showreal'))
  rfropt.showreal=1;
end
if(~isfield(rfropt,'noffdiag'))
  rfropt.noffdiag=1;
end
if(~isfield(rfropt,'debias_lensing'))
  rfropt.debias_lensing=1;
end
% default parameter ranges depend on calc type
switch(rfropt.calc_type)
 case 'r1d'
  if(~isfield(rfropt,'rvals'))
    %rfropt.rvals=-1:.01:1.5;
    rfropt.rvals=-.25:.01:0.75;
  end
  rfropt.lvals=1;
 case 'rvsl'
  if(~isfield(rfropt,'rvals'))
    %rfropt.rvals=0:.05:1;
    rfropt.rvals=0:0.025:0.5;
  end  
  if(~isfield(rfropt,'lvals'))
    %rfropt.lvals=0.5:.2:2;
    rfropt.lvals=-0.5:.25:3.5;
  end
 case 'rvsnt'
  if(~isfield(rfropt,'rvals'))
    rfropt.rvals=0:0.0125*2:0.5;
  end  
  if(~isfield(rfropt,'lvals'))
    rfropt.lvals=-2.5:.125*2:2.5;
  end
 case 'cvsi'
  % for this fit we re-purpose r and lensing scale factor variables
  % as normalization and slope of power law
  if(~isfield(rfropt,'rvals'))
    rfropt.rvals=0:0.001:0.02;
  end
  if(~isfield(rfropt,'lvals'))
    rfropt.lvals=-1.5:0.1:1.5;
  end
end

% if r is string load it
if(ischar(rf))
  load(sprintf('final/%s',rf));
else
  r=rf;
  rf='input r struct';
end
% if r multi spec use the desired one
r=r(rfropt.nspec); bpwf=bpwf(rfropt.nspec);

% if desired debias foreground model
if(isfield(rfropt,'fgmod'))
  if(ischar(rfropt.fgmod))
    load(sprintf('aps/%s',rfropt.fgmod));
    aps=apply_supfac(aps,r);
  else
    aps=rfropt.fgmod;
  end
  r.real(:,4)=r.real(:,4)-aps.Cs_l(:,4);
end

% add systematic uncertainty (abscal/beam)
if(rfropt.addsysuncer)
  r=add_sys_uncer(r,rfropt.noffdiag);
end

% if blinding substitute first sim for real
if(~rfropt.showreal)
  r.real=r.sim(:,:,1);
end
  
% Get expected r bandpowers from signal only sims and scale to r=1
r=calc_expvals(r,inpmod,bpwf);
expv_r1 = r.expv; 
expv_r1(:,4) = mean(r.sig(:,4,:),3)*10;

if(rfropt.debias_lensing)
  % Use mean of signal+noise sims as expected lensing baseline
  expv_lensing = mean(r.sim, 3);
else
  expv_lensing = expv_r1;
  expv_lensing(:) = 0;
end

if(isfield(rfropt,'fiducialr'))
  % add r bandpowers to signal+noise sims - increase the
  % "errorbars" to reflect presence of real signal

  % take the s+n sims and add back in the debias
  db=repmat(r.db(:,4),[1,1,size(r.sim,3)]);
  sn=r.sim(:,4,:)+db;
  % find the means of the s+n and r=0.1 sets
  msn=mean(sn,3);
  mr=mean(r(1).sig(:,4,:),3);
  % scale the s+n to the new desired mean
  sn=sn.*repmat((msn+mr*10*rfropt.fiducialr)./msn,[1,1,size(r(1).sim,3)]);
  % subtract out the debias again and store back in r struct
  r.sim(:,4,:)=sn-db;
end

% build the grid of models to scan over
switch(rfropt.calc_type)
 case {'r1d','rvsl'}
  for j=1:length(rfropt.rvals)
    for k=1:length(rfropt.lvals)
      r.mod(j,k,:)=expv_r1(:,4)*rfropt.rvals(j)+rfropt.lvals(k)*expv_lensing(:,4);
    end
  end

 case 'rvsnt'
  % changing the fiducial n_t requires more than a rescaling 
  % of the existing input models coming with
  % the sim. This either requires computation of the spectra on the fly or handing
  % them in seperately and then interpolating to the different n_t values. For now we hand
  % them in here explicitly; note that we are also using a pivot scale of 0.009/Mpc  
  %
  % For the interpolation we construct a matrix that holds support points...:
  Bnt=[];
  % ... which are coming from a bunch of files that have been simulated at this n_t grid:
  n_t=linspace(-2.5,2.5,51);
  % create some copy to play with without changing the actual data:
  r2 = r;
  % read them in...
  for ii = 1:length(n_t)
    % missing these files? Did you cvs update your aux_data?
    % these spectra are being created with make_spectra.m
    ulen=dlmread(['aux_data/official_cl/nt_scan/camb_planck2013_r0p1_nt',num2str(n_t(ii)),'_pt0.009_totcls.dat']);
    % ... and put their B-modes into an input model structure.
    inpmod.Cs_l(3:end,4)=ulen(:,4);
    % the expectation values are saved for the scan to come:
    r2=calc_expvals(r2,inpmod,bpwf);
    Bnt = [Bnt,r2(1).expv(:,4)];
  end
  Bnt=Bnt';
  
  for j=1:length(rfropt.rvals)
    for k=1:length(rfropt.lvals)
      % interpolate to the desired n_t value, rescale to desired r and add lensing
      r.expv(:,4)=interp1(n_t,Bnt,rfropt.lvals(k),'spline')'*10*rfropt.rvals(j)+expv_lensing(:,4);
      % store in model grid
      r.mod(j,k,:)=r.expv(:,4);     
    end
  end  
  
 case 'cvsi'
  for j=1:length(rfropt.rvals)
    for k=1:length(rfropt.lvals)
      % calc per ell values of simple power law
      inpmod.Cs_l(:,4)=rfropt.rvals(j)*(inpmod.l/80).^(rfropt.lvals(k));
      % calc exp vals of these
      r=calc_expvals(r,inpmod,bpwf);
      % add vanilla lensing
      r.expv(:,4)=r.expv(:,4)+expv_lensing(:,4);
      % store in model grid
      r.mod(j,k,:)=r.expv(:,4);
    end
  end
end

% Hamimeche Lewis style
switch(rfropt.method)

 case 'hl'
  
  % real data (or first sim if blinded)
  % Cache part of the H-L calculation that's independent of r
  products = hl_create_data_products(r,rfropt.chibins{4},'B',rfropt.noffdiag);
  prep = hamimeche_lewis_prepare(products.C_fl, products.M_f);
  r.likeBB = zeros(length(rfropt.rvals), 1);

  for j=1:length(rfropt.rvals)
    for k=1:length(rfropt.lvals)
      % fetch model spectrum
      r.expv(:,4)=r.mod(j,k,:);
      % compute its likelihood
      r.likeBB(j,k)=hl_like_from_final(r,rfropt.chibins{4},'B',rfropt.noffdiag,0,products,prep);
    end
  end

  % sims
  for i=1:size(r.sim, 3)
    disp(['Process sim ',int2str(i)]);
    rsim = r; % Make rsim as if sim were real data
    rsim.real(:,:) = r.sim(:,:,i);
    products = hl_create_data_products(rsim, rfropt.chibins{4}, 'B', rfropt.noffdiag);
    prep = hamimeche_lewis_prepare(products.C_fl, products.M_f);
    for j=1:length(rfropt.rvals)
      for k=1:length(rfropt.lvals)
        rsim.expv(:,4)=r.mod(j,k,:);
        r.slikeBB(j,k,i)=hl_like_from_final(rsim,rfropt.chibins{4},'B',rfropt.noffdiag,0,products,prep);
      end
    end
  end

 case 'chi2'
  % Traditional chisq style (with optional offset-lognormal xfacs)

  % add xoffsets
  if(rfropt.addxfac)
    r=add_xfac(r);
  end
  
  % calc chi2 and map out likelihood function
  for j=1:length(rfropt.rvals)
    for k=1:length(rfropt.lvals)
      [j k]
      r.expv(:,4)=r.mod(j,k,:);
      % likelihoods are automatically scaled per Chiang et al. by calc_chi
      % (Does the normalization matter? - the covmat is being held fixed)
      r=calc_chi(r,rfropt.chibins,rfropt.noffdiag);

      r.likeBB(j,k)=r.rlike(4);
      r.slikeBB(j,k,:)=r.slike(4,:);
    end
  end
  
 otherwise
  error('unknown method')
  
end

% copy into output structure
r.expv_r1=expv_r1;
r.expv_lensing=expv_lensing;
r.rvals=rfropt.rvals;
r.lvals=rfropt.lvals;

switch rfropt.calc_type
 case 'r1d'
  % calculate 95%CL, max likelihood and +/-sigma
  r=calc_stats_1d(r,rfropt.chibins{4});
  r.lmaxlike=1;
  
  % make plots of likelihood functions, and hist of max like and 95%CL
  if pl>0
    make_plots_1d(r,rfropt);
  end
  
 case 'rvsl'
  r=calc_stats_2d(r,rfropt.chibins{4});
  
  if pl>0
    xl='r';
    yl='lensing scale factor';

    setwinsize(gcf,1100,500); clf
    subplot(2,2,1); plot_raw(r,rfropt);
    subplot(2,2,2); plot_fit(r,rfropt);
    
    subplot(2,4,5);
    imagesc(r.rvals,r.lvals,r.likeBB'); axis xy
    xlabel(xl); ylabel(yl);
    
    subplot(2,4,6);
    mv=maxmd(r.likeBB);
    contour(r.rvals,r.lvals,r.likeBB',mv*exp(-([0.1,1,2]).^2/2));
    grid
    hold on; plot(r.rmaxliker,r.rmaxlikel,'+'); hold off
    xlabel(xl); ylabel(yl);
    title(sprintf('maxlike=%.3g -> \\chi^2=%.1f -> PTE=%.3g',r.rmaxlikev,r.rbfchi2,r.rbfchi2pte));
    
    subplot(2,4,7);
    rlike=sum(r.likeBB,2);
    rlike=rlike./sum(rlike);
    plot(r.rvals,rlike,'k');
    xlim([min(r.rvals),max(r.rvals)])
    hold on
    plot([r.rlo68 r.rlo68],[0 r.rcont68],'b--');
    plot([r.rhi68 r.rhi68],[0 r.rcont68],'b--');
    hold off
    xlabel(xl); ylabel('likelihood');
    
    subplot(2,4,8);
    llike=sum(r.likeBB,1);
    llike=llike./sum(llike);
    plot(r.lvals,llike,'k');
    xlim([min(r.lvals),max(r.lvals)])
    hold on
    plot([r.llo68 r.llo68],[0 r.lcont68],'b--');
    plot([r.lhi68 r.lhi68],[0 r.lcont68],'b--');
    hold off
    xlabel(yl); ylabel('likelihood');
  end

 case 'rvsnt'
  r=calc_stats_2d(r,rfropt.chibins{4});
  
  if pl>0
    xl='r_{0.009}';
    yl='n_T';

    setwinsize(gcf,1100,500); clf
    subplot(2,2,1); plot_raw(r,rfropt);
    subplot(2,2,2);
    eb=std(r.sim(:,4,:),[],3);
    errorbar2(r.l,mean(r.real(:,4,:),3),eb,eb,'b.');
    ellh=r.l(max(rfropt.chibins{4})+3)+10;
    xlim([0,ellh]); ylim([-0.01,0.07])
    hold on;
    lb=rfropt.chibins{4};
    plot(r.l(lb),r.real(lb,4),'ro','MarkerFaceColor','r','MarkerSize',4);
    % plot the scaled r-template
    plot(r.l,interp1(n_t,Bnt,r.rmaxlikel,'spline')'*10*r.rmaxliker,'r--');
    % plot the lensing template
    plot(r.l,r.expv_lensing(:,4),'r--');
    % plot the sum (total fit)
    plot(r.l,interp1(n_t,Bnt,r.rmaxlikel,'spline')'*10*r.rmaxliker+r.expv_lensing(:,4),'r--');
    % plot x-axis
    line(xlim,[0,0],'Color','k','LineStyle',':');
    title('fit')
    
    subplot(2,4,5);
    imagesc(r.rvals,r.lvals,r.likeBB'); axis xy
    xlabel(xl); ylabel(yl);
    
    subplot(2,4,6);
    mv=maxmd(r.likeBB);
    contour(r.rvals,r.lvals,r.likeBB',mv*exp(-([0.1,1,2]).^2/2));
    grid
    hold on; plot(r.rmaxliker,r.rmaxlikel,'+'); hold off
    xlabel(xl); ylabel(yl);
    title(sprintf('maxlike=%.3g -> \\chi^2=%.1f -> PTE=%.3g',r.rmaxlikev,r.rbfchi2,r.rbfchi2pte));
    
    subplot(2,4,7);
    rlike=sum(r.likeBB,2);
    rlike=rlike./sum(rlike);
    plot(r.rvals,rlike,'k');
    xlim([min(r.rvals),max(r.rvals)])
    hold on
    plot([r.rlo68 r.rlo68],[0 r.rcont68],'b--');
    plot([r.rhi68 r.rhi68],[0 r.rcont68],'b--');
    hold off
    xlabel(xl); ylabel('likelihood');
    
    subplot(2,4,8);
    llike=sum(r.likeBB,1);
    llike=llike./sum(llike);
    plot(r.lvals,llike,'k');
    xlim([min(r.lvals),max(r.lvals)])
    hold on
    plot([r.llo68 r.llo68],[0 r.lcont68],'b--');
    plot([r.lhi68 r.lhi68],[0 r.lcont68],'b--');
    hold off
    xlabel(yl); ylabel('likelihood');
  end

  
 case 'cvsi'
  r=calc_stats_2d(r,rfropt.chibins{4});
 
  if pl>0
    setwinsize(gcf,1100,500); clf
    subplot(2,2,1); plot_raw(r,rfropt);
    
    subplot(2,2,2);
    eb=std(r.sim(:,4,:),[],3);
    errorbar2(r.l,mean(r.real(:,4,:),3),eb,eb,'b.');
    ellh=r.l(max(rfropt.chibins{4})+3)+10;
    xlim([0,ellh]); ylim([-0.01,0.07])
    hold on;
    lb=rfropt.chibins{4};
    plot(r.l(lb),r.real(lb,4),'ro','MarkerFaceColor','r','MarkerSize',4);
    % plot the best fit power law
    plot(r.l,r.rmaxliker*(r.l/80).^(r.rmaxlikel),'r--');
    % plot the lensing template
    plot(r.l,r.expv_lensing(:,4),'r--');
    % plot the sum (total fit)
    plot(r.l,r.rmaxliker*(r.l/80).^(r.rmaxlikel)+expv_lensing(:,4),'r');
    % plot x-axis
    line(xlim,[0,0],'Color','k','LineStyle',':');
    title('fit - note: error bars do not contain pow law samp var')
    
    subplot(2,4,5);
    imagesc(r.rvals,r.lvals,r.likeBB'); axis xy
    xlabel('power law amp at l=80'); ylabel('power law slope');
    
    subplot(2,4,6);
    mv=maxmd(r.likeBB);
    if(length(rfropt.lvals)>1)
      contour(r.rvals,r.lvals,r.likeBB',mv*exp(-([0.1,1,2]).^2/2));
    end
    grid
    hold on; plot(r.rmaxliker,r.rmaxlikel,'+'); hold off
    xlabel('power law amp at l=80'); ylabel('power law slope');
    title(sprintf('maxlike=%.3g -> \\chi^2=%.1f -> PTE=%.3g',r.rmaxlikev,r.rbfchi2,r.rbfchi2pte));

    subplot(2,4,7);
    rlike=sum(r.likeBB,2);
    rlike=rlike./sum(rlike);
    plot(r.rvals,rlike,'k');
    xlim([min(r.rvals),max(r.rvals)])
    hold on
    plot([r.rlo68 r.rlo68],[0 r.rcont68],'b--');
    plot([r.rhi68 r.rhi68],[0 r.rcont68],'b--');
    hold off
    xlabel('power law amp at l=80'); ylabel('likelihood');
    
    subplot(2,4,8);
    if(length(rfropt.lvals)>1)
      llike=sum(r.likeBB,1);
      llike=llike./sum(llike);
      plot(r.lvals,llike,'k');
      xlim([min(r.lvals),max(r.lvals)])
      hold on
      plot([r.llo68 r.llo68],[0 r.lcont68],'b--');
      plot([r.lhi68 r.lhi68],[0 r.lcont68],'b--');
      hold off
      xlabel('power law slope'); ylabel('likelihood');
    end
  
  end
end

if pl>0
  gtitle(sprintf('%s spectrum %d using method %s',strrep(rf,'_','\_'),rfropt.nspec,rfropt.method));
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function r=calc_stats_1d(r,ellbins)
% integrate positive portion of likelihood function up to 95% for CL
% find +/- sigma contours
% find max likelihood

% integrate positive portion of likelihood function
indx=find(r.rvals>=0);
rr=r.rvals(indx);

% integrate to 95% for upper limit
% select distinct sum values for interp1, ugh.
% real data:
rnorm=cumsum(r.likeBB(indx))./sum(r.likeBB(indx));
[dum un]=unique(rnorm);
r.rconf95=interp1(rnorm(un),rr(un),0.95);
% sims:
for ii=1:size(r.slikeBB,3)
  snorm=(cumsum(r.slikeBB(indx,ii))./sum(r.slikeBB(indx,ii)));
  [dum un]=unique(snorm);
  r.sconf95(ii)=interp1(snorm(un),rr(un),0.95);
end

% find max likelihood
% real data
[r.rmaxlikev rindx]=max(r.likeBB);
r.rmaxliker=r.rvals(rindx);
% convert real max like to chi2 and then pte
r.rbfchi2=-2*log(r.rmaxlikev);
r.rbfchi2pte=1-chi2cdf(r.rbfchi2,length(ellbins)-1);
% sims
for ii=1:size(r.slikeBB,3)
  [r.smaxlikev(ii) indx]=max(r.slikeBB(:,ii));
  r.smaxliker(ii)=r.rvals(indx);
end
r.sbfchi2=-2*log(r.smaxlikev);
r.sbfchi2pte=1-chi2cdf(r.sbfchi2,length(ellbins)-1);

[r.rcont68,r.rlo68,r.rhi68]=find_68pnts(sum(r.likeBB,2),r.rvals,'interp');

r.rmaxlikel=1;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function r=calc_stats_2d(r,ellbins)
% find global maximum real
[r.rmaxlikev,mi]=maxmd(r.likeBB);
r.rmaxliker=r.rvals(mi(1));
r.rmaxlikel=r.lvals(mi(2));
r.rbfchi2=-2*log(r.rmaxlikev);
r.rbfchi2pte=1-chi2cdf(r.rbfchi2,length(ellbins)-2);
% sims
for i=1:size(r.slikeBB,3)
  [r.smaxlikev(i),mi]=maxmd(r.slikeBB(:,:,i));
  r.smaxliker(i)=r.rvals(mi(1));
  r.smaxlikel(i)=r.lvals(mi(2));
end
r.sbfchi2=-2*log(r.smaxlikev);
r.sbfchi2pte=1-chi2cdf(r.sbfchi2,length(ellbins)-2);  

% integrate over vals and find 68% limits
[r.rcont68,r.rlo68,r.rhi68]=find_68pnts(sum(r.likeBB,2),r.rvals);
if(length(r.lvals)~=1)
  [r.lcont68,r.llo68,r.lhi68]=find_68pnts(sum(r.likeBB,1),r.lvals);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_plots_1d(r,rfropt)

%if(ishandle(1))
%  set(0,'CurrentFigure',1);
%else
%  figure(1);
%end
setwinsize(gcf,1100,500)

clf
% plot the raw information
subplot(2,2,1)
plot_raw(r,rfropt);

% plot the fit
subplot(2,2,2)
plot_fit(r,rfropt);

% make plots like in Chiang et al fig 14
% show the real max likelihood and 95%CL
% plot the max likelihood and 95%CL for the sims and show the median

%  binsml=-.5:.05:1;
%  binscl=0:.02:1;
binsml=-.525:.05:1.025
binscl=0.01:.02:1.01;
fs=9;

legpos='NorthEast';

% plot likelihood function of real data
subplot(2,3,4)
box on; hold on
xticsiz=.25;
rlike=abs(r.likeBB./sum(r.likeBB));
plot(r.rvals,rlike,'k'); 
[a ind]=min(abs(r.rvals-r.rmaxliker));
plot([r.rmaxliker r.rmaxliker],[0 rlike(ind)],'b');
text(r.rmaxliker*1.05,rlike(ind)*1.01,'ML','color','b');
[a ind]=min(abs(r.rvals-r.rconf95));
plot([r.rconf95 r.rconf95],[0 rlike(ind)],'r');
text(r.rconf95*1.05,rlike(ind)*1.01,'95%CL','color','r');
plot([r.rlo68 r.rlo68],[0 r.rcont68],'b--');
plot([r.rhi68 r.rhi68],[0 r.rcont68],'b--');
xlabel('r'); ylabel('likelihood');
ll=legend(['data'],['r = ' sprintf('%1.3f',r.rmaxliker) ' +' sprintf('%1.3f',r.rhi68-r.rmaxliker)...
                    ' ' sprintf('%1.3f',r.rlo68-r.rmaxliker)],['r < ' sprintf('%1.3f',r.rconf95)]);
set(ll,'box','off','location',legpos,'fontsize',fs)
[a ind]=min(abs(r.rvals-r.rmaxliker));
lm=rlike(ind);
[a ind]=min(abs(r.rvals-0));
lz=rlike(ind);
title(sprintf('L(zero)/L(max)=%.e, maxlike=%.3g -> \\chi^2=%.1f -> PTE=%.3g',lz/lm,r.rmaxliker,r.rbfchi2,r.rbfchi2pte));
set(gca,'YTick',[],'XTick',-10:xticsiz:10)
xlim([-.25 .75])
ylim(get(gca,'YLim').*1.10)

% histogram of max likelihood
subplot(2,3,5);
box on; hold on
n=histc([r.smaxliker],binsml);
medsim=median(r.smaxliker);
stairs(binsml,n,'color','k')
ylims=get(gca,'YLim');
xlim([min(binsml) max(binsml)])
set(gca,'YLim',[0 ylims(2)+1]); %,'YTick',0:100);
xlabel('maximum likelihood r'); ylabel('counts');
if(isfield(r,'likeBB'))
  plot([medsim medsim],[0 ylims(2)+1],'k--');
  plot([r.rmaxliker r.rmaxliker],[0 ylims(2)+1],'b');
  ll=legend(sprintf('%i sig+noi sims',size(r.sim,3)),...
    sprintf('med sims: %1.3f',medsim),sprintf('data: %1.3f',r.rmaxliker));
else
  plot([medsim medsim],[0 ylims(2)+1],'b-');
  ll=legend(sprintf('%i sig+noi sims',size(r.sim,3)),sprintf('med sims: %1.3f',medsim));
end
set(ll,'box','off','location',legpos,'fontsize',fs)

% histogram of 95%CL
subplot(2,3,6);
box on; hold on
n=histc([r.sconf95],binscl);
medsim=median(r.sconf95);
stairs(binscl,n,'color','k')
ylims=get(gca,'YLim');
xlim([min(binscl) max(binscl)])
set(gca,'YLim',[0 ylims(2)+1]); %,'YTick',0:100);
xlabel('r upper limit, 95% confidence');ylabel('counts');
if(isfield(r,'likeBB'))
  plot([medsim medsim],[0 ylims(2)+1],'k--');
  plot([r.rconf95 r.rconf95],[0 ylims(2)+1],'r');
  ll=legend(sprintf('%i sig+noi sims',size(r.sim,3)),...
    sprintf('med sims: %1.3f',medsim),sprintf('data: %1.3f',r.rconf95));
else
  plot([medsim medsim],[0 ylims(2)+1],'r');
  ll=legend(sprintf('%i sig+noi sims',size(r.sim,3)),sprintf('med sims: %1.3f',medsim));
end
set(ll,'box','off','location',legpos,'fontsize',fs)

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_raw(r,rfropt)
% plot s+n sims in light gray
plot(r.l,squeeze(r.sim(:,4,:)),'color',[0.7,0.7,0.7]);
ellh=r.l(max(rfropt.chibins{4})+3)+10;
xlim([0,ellh]); ylim([-0.01,0.07])
hold on;
% plot their spread as errorbars
eb=std(r.sim(:,4,:),[],3);
errorbar2(r.l,mean(r.sim(:,4,:),3),eb,eb,'b');
% plot templates
plot(r.l,r.expv_r1(:,4)/10,'r--');
plot(r.l,r.expv_lensing(:,4),'r--');
% plot debias
plot(r.l,r.db(:,4),'m');
% plot real points
plot(r.l,r.real(:,4),'b.');
lb=rfropt.chibins{4};
plot(r.l(lb),r.real(lb,4),'ro','MarkerFaceColor','r','MarkerSize',4);
% plot x-axis
line(xlim,[0,0],'Color','k','LineStyle',':');
title('raw information');
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_fit(r,rfropt)
% plot s+n sims in light gray
% plot the real points with used errorbar
eb=std(r.sim(:,4,:),[],3);
errorbar2(r.l,mean(r.real(:,4,:),3),eb,eb,'b.');
ellh=r.l(max(rfropt.chibins{4})+3)+10;
xlim([0,ellh]); ylim([-0.01,0.07])
hold on;
lb=rfropt.chibins{4};
plot(r.l(lb),r.real(lb,4),'ro','MarkerFaceColor','r','MarkerSize',4);
% plot the scaled r-template
plot(r.l,r.expv_r1(:,4)*r.rmaxliker,'r--');
% plot the lensing template
plot(r.l,r.expv_lensing(:,4)*r.rmaxlikel,'r--');
% plot the sum (total fit)
plot(r.l,r.expv_r1(:,4)*r.rmaxliker+r.expv_lensing(:,4)*r.rmaxlikel,'r');
% plot x-axis
line(xlim,[0,0],'Color','k','LineStyle',':');
title('fit - note: error bars may not contain full sample variance (see left)')

return

