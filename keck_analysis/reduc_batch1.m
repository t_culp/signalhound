function reduc_batch1(tags,doplots,onlyreducplots,onlypairmaps,cpfilestobicepfs1)
% reduc_batch1(tags,doplots,onlyreducplots,onlypairmaps)
%
% Run first to do initial reduction of cmb runs
% if doplots is set to 1, generate hard copy redcuplots
%
% % e.g.
% reduc_batch1(get_tags('cmb2011'))

if(~exist('doplots','var'))
  doplots=0;
end

if(~exist('onlyreducplots','var'))
  onlyreducplots=0;
end

if(~exist('onlypairmaps','var'))
  onlypairmaps=0;
end

if(~exist('cpfilestobicepfs1','var'))
  cpfilestobicepfs1=[];
end

for i=1:length(tags)
  tags{i}

  if(~onlyreducplots && ~onlypairmaps)
    reduc_initial(tags{i});
    reduc_applycal(tags{i});
    reduc_plotcuts(tags(i),'calc');
  end
  
  if(doplots && ~onlypairmaps)
    reduc_plotscans(tags{i},1,[],{'noise','none','round1','round2'});

    %copy files to bicepfs1 for Keck
    if(~isempty(cpfilestobicepfs1))
      copyfile(['reducplots/' tags{i} '*'],cpfilestobicepfs1);
      copyfile(['reducplots/cutmessages/' tags{i} '*'],cpfilestobicepfs1);
    end
  end
  
  if(~onlyreducplots)
    % generate maps on a per-scanset basis
    mapopt.filt='p3';
    mapopt.gs=1;
    mapopt.sernum='0000real';
    mapopt.weight=3;
    mapopt.deproj=false;

    % get ideal parameters for BICEP3 before we have it, change to default when we do
    exptn = get_experiment_name();
    if strcmp(exptn,'bicep3')
      mapopt.beamcen = 'ideal';
      mapopt.chi = 'ideal';
      mapopt.epsilon = 'ideal';
    end

    tmp=get_run_info(tags{i});
    if strncmp(tmp.type{1},'cmb',3)
      if strcmp(exptn,'bicep3')
        mapopt.type='bicepext';
      else
        mapopt.type='bicep';
      end
    elseif strncmp(tmp.type{1},'gal',3)
      mapopt.type='bicepgalb';
    else
      continue
    end
    reduc_makepairmaps(tags(i),mapopt);

  end
  
end
    
return
