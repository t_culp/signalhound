function reduc_plotgscans(tag,out)
% reduc_plotscans(tag)
%
% Make ground scan plots for web post
%
% e.g. reduc_plotgscans('060418')

if(~exist('out','var'))
  out=0;
end

if(out==1)
  unix(sprintf('mkdir reducplots/%s',tag));
end

% get full day's data!
d=read_run(tag);

[d.azoff,d.eloff,d.az,d.el]=arcvar_to_azel(d);

% get nominal array info
[p,ind]=get_array_info(tag);

% deglitch
ap=find_blk(bitand(d.frame.features,2^0+2^8+2^9));
d.lockin.adcData=deglitch(d.lockin.adcData,ap,ind.gl);

% get elnod info
load(['data/',tag,'_calval']);

% average for/back elnod gains
if(rem(length(en.s),2)~=0)
  i=true(size(en.s)); i(end)=0;
  en=structcut(en,i);
end
g=reshape(en.g,[2,size(en.g,1)/2,size(en.g,2)]);
t=reshape(en.t,[2,length(en.t)/2]);
en.g=squeeze(mean(g)); en.t=mean(t)';
en.s=en.s(1:2:end); en.e=en.e(2:2:end);
en.sf=en.sf(1:2:end); en.ef=en.ef(2:2:end);

% up to here identcial to reduc_plotscans.m

% find start/end of ground scan block
gs=find_blk(bitand(d.frame.features,2^10));
gs.t=d.td(gs.s);

% ground scans only present 060414 and later
if(isempty(gs.s))
  return
end

% remove means and apply cal
%d=filter_scans(d,gs,'p0',ind.a);
d.lockin.adcData=cal_scans(d.lockin.adcData,gs,en,ind.gl100,ind.rgl100);
d.lockin.adcData=cal_scans(d.lockin.adcData,gs,en,ind.gl150,ind.rgl150);

% make copy of data pair differenced
dd=sumdiff_pairs(d,p,gs,ind.gla);

% take mean of each 100 points to beat down white noise
% in adcData only - leave pointing etc at full 100Hz rate
% (so don't use meanofn_fastreg function) 
v=d.lockin.adcData; n=size(v);
v=reshape(v,[100,n(1)/100,n(2)]);
d.lockin.adcData=squeeze(mean(v,1));

v=dd.lockin.adcData; n=size(v);
v=reshape(v,[100,n(1)/100,n(2)]);
dd.lockin.adcData=squeeze(mean(v,1));

% doing above means first sample of each block is uncal/filter and
% corrupted, so kludge to fix:
gs.s=gs.s+1; en.s=en.s+1; fs.s=fs.s+1;

% force A/B pairs to have same color but colors to vary rapidly
close all
setwinsize(gcf,700,900);

cm=get(gcf,'DefaultAxesColorOrder');
x=1:size(cm,1); x=[x;x]; x=x(:);
cmd=cm(x,:);
set(gcf,'DefaultAxesColorOrder',cmd);

% plot ground scans
plot_ground_scans(d,gs,ind,p,tag,out);

% back to original colormap
set(gcf,'DefaultAxesColorOrder',cm);

% plot pair differenced
plot_ground_scansd(dd,gs,ind,p,tag,out);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_ground_scans(d,gs,ind,p,tag,out);

% panel junction angles
jstr={'-156:26','-126:44','-96:42','-67:06','-37:03','-7:05','22:58','53:01','82:56','113:09','143:10','173:44'};
for i=1:length(jstr)
  z=sscanf(jstr{i},'%d:%d');
  if(z(1)>0)
    junc(i)=z(1)+z(2)/60;
  else
    junc(i)=z(1)-z(2)/60;
  end
end

% cmb field az ranges
azrx=[60.38,202.83-360;179.47,321.97-360];
azry=-3.8*ones(size(azrx));
azmx=-21.8; azmy=-3.8; % MAPO
 
% for each scan
for i=1:length(gs.s)

  dk=d.tracker.horiz_off(gs.s(i),3);
  
  % rotate array to deck angle
  q=rotarray(p,dk);

  % set of nominal el offsets
  x=[0,1.1,0.9,-0.2,-1.1,-0.9,0.2,3.1,2.9,1.9,0.7,-1.3,-2.1,-3.1,-2.9,-1.9,-0.7,1.3,2.1,...
	2.2,2.0,1.8,0.9,-0.1,-1.1,-2.2,-2,-1.8,-0.9,0.1,1.1];
  x=[x;x]; q.dec_off=rvec(x);
  % this would be broken for deck not close to zero
  
  s=gs.s(i); e=gs.e(i);
  
  y=double(d.lockin.adcData(s:e,:));

  it=round((s+e)/2);
  
  % set turnaround point to zero for each trace and then
  % add offset to each channel based on vertical position in array
  for j=1:length(ind.gl)
    ch=ind.gl(j);
    y(:,ch)=y(:,ch)-y(it-s,ch)-q.dec_off(ch)*1;
  end

  % strip off -A/B from name
  for j=1:length(p.channel_name)
    chnames{j}=p.channel_name{j}(1:end-2);
  end
  
  subplot(1,2,1)
  [dum,n]=sort(q.dec_off(ind.rgl100));
  plot(d.tracker.actual(s:e,1),y(:,ind.rgl100(n)));
  axis tight; ylim([-4,4]);
  set(gca,'YTickLabel',[]);
  xlabel('az (deg)');
  yt=y(it-s,ind.rgl100a); xt=d.tracker.actual(it)*ones(size(yt));
  text(xt,yt,chnames(ind.rgl100a),'HorizontalAlignment','right','color','b');
  for j=1:length(junc)
    line([junc(j),junc(j)],[-4,4],'Color','k','LineStyle',':');
  end
  line(azrx,azry,'color','k');
  text(mean(azrx),mean(azry),{'-3','+57'},'VerticalAlignment','bottom','HorizontalAlignment','center');
  text(azmx,azmy,'MAPO','Rotation',90)
  title('100GHz');
  
  subplot(1,2,2)
  [dum,n]=sort(q.dec_off(ind.rgl150));
  plot(d.tracker.actual(s:e,1),y(:,ind.rgl150(n)));
  axis tight; ylim([-4,4]);
  set(gca,'YTickLabel',[]);
  xlabel('az (deg)');
  yt=y(it-s,ind.rgl150a); xt=d.tracker.actual(it)*ones(size(yt));
  text(xt,yt,chnames(ind.rgl150a),'HorizontalAlignment','right','color','b');
  for j=1:length(junc)
    line([junc(j),junc(j)],[-4,4],'Color','k','LineStyle',':');
  end
  line(azrx,azry,'color','k');
  text(mean(azrx),mean(azry),{'-3','+57'},'VerticalAlignment','bottom','HorizontalAlignment','center');
  text(azmx,azmy,'MAPO','Rotation',90)
  title('150GHz');

  gtitle(sprintf('undiff, %s, el=%.1f, dk=%.0f',tag,mean(d.tracker.actual(s:e,2)),dk));
  
  drawnow
  if(out==1)
    mkpng(sprintf('reducplots/%s/groundscan%03d.png',tag,i));
  else
    pause
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_ground_scansd(d,gs,ind,p,tag,out);

% panel junction angles
jstr={'-156:26','-126:44','-96:42','-67:06','-37:03','-7:05','22:58','53:01','82:56','113:09','143:10','173:44'};
for i=1:length(jstr)
  z=sscanf(jstr{i},'%d:%d');
  if(z(1)>0)
    junc(i)=z(1)+z(2)/60;
  else
    junc(i)=z(1)-z(2)/60;
  end
end

% cmb field az ranges
azrx=[60.38,202.83-360;179.47,321.97-360];
azry=-3.8*ones(size(azrx));
azmx=-21.8; azmy=-3.8; % MAPO

% for each scan
for i=1:length(gs.s)

  dk=d.tracker.horiz_off(gs.s(i),3);
  
  % rotate array to deck angle
  q=rotarray(p,dk);

  % set of nominal el offsets
  x=[0,1.1,0.9,-0.2,-1.1,-0.9,0.2,3.1,2.9,1.9,0.7,-1.3,-2.1,-3.1,-2.9,-1.9,-0.7,1.3,2.1,...
	2.2,2.0,1.8,0.9,-0.1,-1.1,-2.2,-2,-1.8,-0.9,0.1,1.1];
  x=[x;x]; q.dec_off=rvec(x);
  % this would be broken for deck not close to zero
  
  s=gs.s(i); e=gs.e(i);
  
  y=double(d.lockin.adcData(s:e,:));
  x=d.tracker.actual(s:e,1);
  
  it=round((s+e)/2);

  % set turnaround point to zero for each trace and then
  % add offset to each channel based on vertical position in array
  for j=1:length(ind.gl)
    ch=ind.gl(j);
    y(:,ch)=3*(y(:,ch)-y(it-s,ch))-q.dec_off(ch)*1;
  end

  % strip off -A/B from name
  for j=1:length(p.channel_name)
    chnames{j}=p.channel_name{j}(1:end-2);
  end
  
  subplot(1,2,1)
  [dum,n]=sort(q.dec_off(ind.rgl100a));
  plot(x,y(:,ind.rgl100a(n)+1));
  hold on
  plot(x(1:35:end),y(1:35:end,ind.rgl100q+1),'k.');
  hold off
  axis tight; ylim([-4,4]);
  set(gca,'YTickLabel',[]);
  xlabel('az (deg)');
  yt=y(it-s,ind.rgl100a+1); xt=d.tracker.actual(it)*ones(size(yt));
  text(xt,yt,chnames(ind.rgl100a),'HorizontalAlignment','right','color','b');
  for j=1:length(junc)
    line([junc(j),junc(j)],[-4,4],'Color','k','LineStyle',':');
  end
  line(azrx,azry,'color','k');
  text(mean(azrx),mean(azry),{'-3','+57'},'VerticalAlignment','bottom','HorizontalAlignment','center');
  text(azmx,azmy,'MAPO','Rotation',90)
  title('100GHz');
  
  subplot(1,2,2)
  [dum,n]=sort(q.dec_off(ind.rgl150a));
  plot(x,y(:,ind.rgl150a(n)+1)); 
  hold on
  plot(x(1:35:end),y(1:35:end,ind.rgl150q+1),'k.');
  hold off
  axis tight; ylim([-4,4]);
  set(gca,'YTickLabel',[]);
  xlabel('az (deg)');
  yt=y(it-s,ind.rgl150a+1); xt=d.tracker.actual(it)*ones(size(yt));
  text(xt,yt,chnames(ind.rgl150a),'HorizontalAlignment','right','color','b');
  for j=1:length(junc)
    line([junc(j),junc(j)],[-4,4],'Color','k','LineStyle',':');
  end
  line(azrx,azry,'color','k');
  text(mean(azrx),mean(azry),{'-3','+57'},'VerticalAlignment','bottom','HorizontalAlignment','center');
  text(azmx,azmy,'MAPO','Rotation',90)
  title('150GHz');

  gtitle(sprintf('pair diff times 3, %s, el=%.1f, dk=%.0f',tag,mean(d.tracker.actual(s:e,2)),dk));
  
  drawnow
  if(out==1)
    mkpng(sprintf('reducplots/%s/groundscand%03d.png',tag,i));
  else
    pause
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function v=cal_scans(v,fs,en,indg,indrg)
% indg =  may include some suspect channels and non-pairs
% indrg = "gold" channels and only pairs
%
% Private copy of this func here as will change in reduc_applycal

disp('cal_scans...')

% take mean gain for gold channels at each time step
g=en.g;
m=mean(g(:,indrg),2);

% For each detector scale to match mean
% We assume that the absolute gain of the instrument is pretty stable -
% as it seems to be from calsrc, but want to correct for
% relative gain, and relative gain drift
for i=indg

  % for each field scan
  for j=1:length(fs.t)
    
    % find the preceding el nod
    x=fs.t(j)-en.t; x(x<0)=+inf; [dummy,k]=min(x);
      
    % Scale to mean
    s=fs.sf(j); e=fs.ef(j);
    v(s:e,i)=v(s:e,i)*m(k)/g(k,i);
  end
end

return

