function reduc_comptodspec(tags,sim1,sim2,fil,pairmapcut,pl)
% reduc_comptodspec(tod1f,tod2f)
%
% Check that the real and sim tod look the same under a battery of tests
%
% e.g.
% To compare sim and real:
% reduc_comptodspec({'20110617B05_dk068'},'real','00000001','p3',1)

if(~exist('fil'))
  fil=[];
end
if(~exist('pairmapcut'))
  pairmapcut=[];
end
if(~exist('pl','var'))
  pl=0;
end

if(isempty('fil'))
  fil='p3';
end
if(isempty('pairmapcut'))
  pairmapcut=0;
end


% for title
sim1t=strrep(sim1,'_','');
sim2t=strrep(sim2,'_','');
  
if(strcmp(sim1t,''))
  sim1t='real';
end

for t=1:length(tags)
  
  try
  
  tag=tags{t}

  % get ind lists
  [p,ind]=get_array_info(tag);

  tod1f=sprintf('data/%s/%s_tod',sim1,tag);
  tod2f=sprintf('data/%s/%s_tod',sim2,tag);
  
  % load the 1st set of tod
  tod1=load(tod1f);
  
  % load the 2nd set of tod
  tod2=load(tod2f);
  
  % kludge due to varying number in Keck
  tod1.fs.ef=tod1.fs.sf+412;
  tod2.fs.ef=tod2.fs.sf+412;
  
  % if wanted plot real/sim tod
  figure(1); setwinsize(gcf,850,900); clf
  plot_tod(tod1,tod2,ind.rgl);
  gtitle(strrep(tag,'_','\_')); drawnow; pause(1);
  if(pl==1)
    % causes crash
    %mkpng(sprintf('comptodspec/%s_fig1_%s.png',tag,fil));
  end
  
  if(pairmapcut)
    
    if(1)
      % calculate the cutparams from data
      %cp=calc_cutparams(tod1.d,tod1.fs,tod1.en,p,ind);
      
      % load the cutparams from file
      load(sprintf('data/%s/%s_cutparams',sim1,tag));

      % get "default cuts"
      cut=get_default_cuts;
      % evaluate the cuts
      c=eval_cuts(cp,cut,p,ind);
      % expand the cuts to "full size"
      ce=expand_cuts(c,p);
      % add the cut-on-cuts
      ce=add_cut_on_cut(ce,cut,p,ind);
      % combine to final mask array (with stat printout)
      cm=combine_cuts(ce,ind);
    else
      % get cut masks from pairmaps files
      % assume "sim1" is "real"
      % make assumption about weight3 and gs - although cuts don't
      % really depend on these(?)
      load(sprintf('pairmaps/%s%s/%s_filt%s_weight3_gs_vw',sim2(1:4),sim1,tag,fil));
    end
    
    % mask both real and sim the same
    tod1.d=apply_cuts(tod1.d,tod1.fs,cm);
    tod2.d=apply_cuts(tod2.d,tod2.fs,cm);
  end
  
  % apply requested filter
  tod1.d=filter_scans(tod1.d,tod1.fs,fil);
  tod2.d=filter_scans(tod2.d,tod2.fs,fil);

  % sum/diff the pairs
  [tod1.d,p]=sumdiff_pairs(tod1.d,p,tod1.fs,ind.a,ind.b);
  [tod2.d]=sumdiff_pairs(tod2.d,p,tod2.fs,ind.a,ind.b);

  % take the spectra for each half scan
  [spec1,f]=make_spec(tod1.d,tod1.fs,ind,1);
  [spec2,f]=make_spec(tod2.d,tod2.fs,ind,1);
  
  % select which pair to plot on fig2&3
  ns=ind.rgl150a(3); nd=ind.rgl150b(3);
  
  % plot half scan rms versus time
  tod1.fs.std=scan_std(tod1.d,tod1.fs);
  tod2.fs.std=scan_std(tod2.d,tod2.fs);
  figure(2); setwinsize(gcf,850,900); clf
  set(gcf,'DefaultAxesColorOrder',jet(length(ind.rgla)));
  subplot(2,2,1)
  to=tod1.d.t(tod1.fs.sf(1))
  imagesc(tod1.fs.std(:,ind.rgla)');
  caxis([0,3]); title(sprintf('%s pair sum half-scan std',sim1t));
  subplot(2,2,2)
  imagesc(tod1.fs.std(:,ind.rglb)');
  caxis([0,1]); title(sprintf('%s pair diff half-scan std',sim1t));
  subplot(2,2,3)
  imagesc(tod2.fs.std(:,ind.rgla)');
  caxis([0,3]); title(sprintf('%s pair sum half-scan std',sim2t));
  subplot(2,2,4)
  imagesc(tod2.fs.std(:,ind.rglb)');
  caxis([0,1]); title(sprintf('%s pair diff half-scan std',sim2t));
  gtitle(strrep(tag,'_','\_')); drawnow; pause(1);
  if(pl==1)
    mkpng(sprintf('comptodspec/%s_fig2_%s.png',tag,fil));
  end
  
  % plot half scan percentiles of single channel
  for i=1:length(tod1.fs.sf)
    % get the half-scan
    s=tod1.fs.sf(i); e=tod1.fs.ef(i);
    hs1(i,:,:)=tod1.d.mce0.data.fb(s:e,[ns,nd]);
    hs2(i,:,:)=tod2.d.mce0.data.fb(s:e,[ns,nd]);
  end
  figure(3); setwinsize(gcf,850,400); clf
  subplot(1,2,1)
  plot(prctile(hs1(:,:,1),[5,20,50,80,95])','b')
  hold on
  plot(prctile(hs2(:,:,1),[5,20,50,80,95])','r')
  hold off
  axis tight
  title(sprintf('channel %d half scan percentiles',ns))
  xlabel('sample in scan'); ylabel('ADU')
  subplot(1,2,2)
  plot(prctile(hs1(:,:,2),[5,20,50,80,95])','b')
  hold on
  plot(prctile(hs2(:,:,2),[5,20,50,80,95])','r')
  hold off
  axis tight
  title(sprintf('channel %d half scan percentiles',nd))
  xlabel('sample in scan'); ylabel('ADU')
  gtitle(strrep(tag,'_','\_'));
  text(.5,.01,sprintf('blue = %s, red = %s',sim1t,sim2t),'HorizontalAlignment','center');
  drawnow; pause(1);
  if(pl==1)
    mkpng(sprintf('comptodspec/%s_fig3_%s.png',tag,fil));
  end
  
  % plot spectra percentiles of single channel
  figure(4); setwinsize(gcf,850,400); clf
  subplot(1,2,1)
  loglog(f,squeeze(prctile(spec1(:,ns,:),[5,20,50,80,95],3)),'b.-')
  hold on
  loglog(f,squeeze(prctile(spec2(:,ns,:),[5,20,50,80,95],3)),'r.-')
  hold off
  ylim([1e-4,1e3]);
  title(sprintf('channel %d spectrum percentiles',ns))
  subplot(1,2,2)
  loglog(f,squeeze(prctile(spec1(:,nd,:),[5,20,50,80,95],3)),'b.-')
  hold on
  loglog(f,squeeze(prctile(spec2(:,nd,:),[5,20,50,80,95],3)),'r.-')
  hold off
  ylim([1e-4,1e3]);
  title(sprintf('channel %d spectrum percentiles',nd))
  gtitle(strrep(tag,'_','\_'));
  text(.5,.01,sprintf('blue = %s, red = %s',sim1t,sim2t),'HorizontalAlignment','center');
  drawnow; pause(1);
  if(pl==1)
    mkpng(sprintf('comptodspec/%s_fig4_%s.png',tag,fil));  
  end
  
  % take the mean spectrum over the tag for each channel
  mspec1(:,:,t)=nanmean(spec1,3);
  mspec2(:,:,t)=nanmean(spec2,3);

  % plot all mean spectra
  figure(5); setwinsize(gcf,850,900); clf
  set(gcf,'DefaultAxesColorOrder',jet(length(ind.rgla)));
  subplot(2,2,1)
  loglog(f,mspec1(:,ind.rgla,t)); ylim([1e-4,1e3]);
  title(sprintf('%s mean pair sum spectra',sim1t));
  subplot(2,2,3)
  loglog(f,mspec2(:,ind.rgla,t)); ylim([1e-4,1e3]);
  title(sprintf('%s mean pair sum spectra',sim2t));
  subplot(2,2,2)
  loglog(f,mspec1(:,ind.rglb,t)); ylim([1e-4,1e3]);
  title(sprintf('%s mean pair diff spectra',sim1t))
  subplot(2,2,4)
  loglog(f,mspec2(:,ind.rglb,t)); ylim([1e-4,1e3]);
  title(sprintf('%s mean pair diff spectra',sim2t));
  gtitle(strrep(tag,'_','\_')); drawnow; pause(1);
  if(pl==1)
    mkpng(sprintf('comptodspec/%s_fig5_%s.png',tag,fil));
  end
  
  % take the mean over channels for each channel group and each day
  mmspec1{1}=squeeze(nanmean(mspec1(:,ind.rgl150a,:),2));
  mmspec1{2}=squeeze(nanmean(mspec1(:,ind.rgl150b,:),2));
  mmspec2{1}=squeeze(nanmean(mspec2(:,ind.rgl150a,:),2));
  mmspec2{2}=squeeze(nanmean(mspec2(:,ind.rgl150b,:),2));
  
  % plot per channel ratios and ratio of mean spectra within each group
  figure(6); setwinsize(gcf,850,400); clf
  set(gcf,'DefaultAxesColorOrder',jet(length(ind.rgla)));
  subplot(1,2,1)
  semilogx(f,mspec2(:,ind.rgla,t)./mspec1(:,ind.rgla,t))
  hold on; plot(f,mmspec2{1}(:,t)./mmspec1{1}(:,t),'k','LineWidth',2); hold off
  ylim([0.7,1.3]); xlim([1e-2,1e1]); grid; ylabel('ratio');
  title('pair sum')
  subplot(1,2,2)
  semilogx(f,mspec2(:,ind.rglb,t)./mspec1(:,ind.rglb,t))
  hold on; plot(f,mmspec2{2}(:,t)./mmspec1{2}(:,t),'k','LineWidth',2); hold off
  ylim([0.7,1.3]); xlim([1e-2,1e1]); grid; ylabel('ratio');
  title('pair diff')
  set(gcf,'DefaultAxesColorOrder',jet(length(ind.rgla)));
  gtitle(sprintf('%s, ratio of spectra %s over %s',strrep(tag,'_','\_'),sim2t,sim1t));
  text(.5,.05,'Per channel ratios in color, ratio of mean spectra in black','HorizontalAlignment','center')
  drawnow; pause(1);
  if(pl==1)
    mkpng(sprintf('comptodspec/%s_fig6_%s.png',tag,fil));
  end
    
  % plot per day mean ratio over channel group (black lines from
  % above) and mean of these ratios over all days
  figure(7); setwinsize(gcf,850,400); clf
  set(gcf,'DefaultAxesColorOrder',jet(length(tags)));
  subplot(1,2,1)
  semilogx(f,mmspec2{1}./mmspec1{1})
  hold on; plot(f,nanmean(mmspec2{1}./mmspec1{1},2),'k','LineWidth',2); hold off
  ylim([0.7,1.3]); xlim([1e-2,1e1]); grid; ylabel('ratio');
  title('pair sum')
  subplot(1,2,2)
  semilogx(f,mmspec2{2}./mmspec1{2})
  hold on; plot(f,nanmean(mmspec2{2}./mmspec1{2},2),'k','LineWidth',2); hold off
  ylim([0.7,1.3]); xlim([1e-2,1e1]); grid; ylabel('ratio');
  title('pair diff')
  gtitle(sprintf('mean of %d days, ratio of spectra %s over %s',t,sim2t,sim1t));
  text(.5,.05,'Per day ratios in color, mean of these in black','HorizontalAlignment','center')
  drawnow; pause(1);
  if(pl==1)
    mkpng(sprintf('comptodspec/%s_fig7_%s.png',tag,fil));
  end
  
  catch
  % just proceed to next tag
  end
  
end

save('comptodspec','mspec1','mspec2');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [dat,f]=make_spec2(d,fs,ind,dum)

l=length(fs.sf);
l=64;

for i=1:l
  i
  
  % get the half-scan
  s=fs.sf(i); e=fs.ef(i);
  v=double(d.mce0.data.fb(s:e,ind.l));
  n=size(v,1);
  
  % for each channel
  for j=1:size(v,2)

    % calc using Matlab canned overlap-add function
    [hs,f]=pwelch(v(:,j),[],[],1024,20);
    
    % on first loop pre-allocate the (very large) output array
    if(i==1&j==1)
      dat=zeros([size(hs),l]);
      size(dat)
    end

    % enter into output array
    dat(:,j,i)=hs;
  end
  
end

% throw out zero freq comp
%dat=dat(2:end,:,:);
%f=f(2:end);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_tod(tod1,tod2,ind)

% set non used periods to NaN to prevent display
if(0)
  mapind=make_mapind(tod1.d,tod1.fs);
  tod1.d.mce0.data.fb(~mapind,:)=NaN;
  tod2.d.mce0.data.fb(~mapind,:)=NaN;
end

% find complete scan blocks
sb=find_blk(bitand(tod1.d.array.frame.features,2^1),length(tod1.d.t)/length(tod1.d.ts));

% remove median from scan blocks
tod1.d=filter_scans(tod1.d,sb,'m');
tod2.d=filter_scans(tod2.d,sb,'m');
  
% time since run start in sec
tr=tod1.d.t;
tr=(tr-tr(1))*86400;

% plot each plot
for i=1:length(sb.sf)
  % get scan pointers
  s=sb.sf(i); e=sb.ef(i);
  t=tr(s:e)-tr(s);
  
  set(gcf,'DefaultAxesColorOrder',jet(length(ind)));
  subplot(2,1,1)
  imagesc(tod1.d.mce0.data.fb(s:e,ind)'); axis tight; caxis([-50,50]);
  ylabel('Real (V)'); xlabel('time (s)')
  
  subplot(2,1,2)
  imagesc(tod2.d.mce0.data.fb(s:e,ind)'); axis tight; caxis([-50,50]);
  ylabel('Sim (V)'); xlabel('time (s)')
  
  gtitle(sprintf('Real and sim tod section %d (no cuts)',i));
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [dat,f]=make_spec(d,fs,ind,win)

for i=1:length(fs.sf)
  
  % get the half-scan
  s=fs.sf(i); e=fs.ef(i);
  v=double(d.mce0.data.fb(s:e,ind.l));
  n=size(v,1);
  
  % windowing is very tricky point here - it's useful to supress
  % leakage but it can change the result because the data and maybe
  % sim do not have equal noise across the half scan - have to try
  % comparison both with and without to be sure
  if(win==1)
    w=tukeywin(n,0.2); % slight end taper
  else
    w=rectwin(n);
  end
  
  % apply window
  v=v.*repmat(w,[1,size(v,2)]);
  
  % since the purpose of this prog is comparison it shouldn't matter
  % if we pad or not - may as well as it's seems like it can only
  % increase the chance to find real/sim mismatch
  n=2^nextpow2(n);
  
  % take fft
  ft=fft(v,n);
  
  % undo normalization change due to window application (and padding)
  ft=ft*sqrt(n/sum(w.^2));
  
  % throw away symmetric part
  nup=ceil((n+1)/2);
  ft=ft(1:nup,:);
    
  % scale so result not a function of input length
  ft=ft/sqrt(n);
  
  % generate auto spectra
  hs=ft.*conj(ft);
    
  % force result real instead of fold-over-and-add which does
  % the same thing
  hs=real(hs);
  
  % on first loop pre-allocate the (very large) output array
  if(i==1)
    dat=zeros([size(hs),length(fs.sf)]);
  end

  % enter into output array
  dat(:,ind.l,i)=hs;
end

% calc the half freq axis
samprate=10;
switch rem(n,2)
  case 0
    f=[0:n/2]'*samprate/n;
  case 1
    f=[0:n/2-0.5]'*samprate/n;
end

% throw out zero freq comp
%dat=dat(2:end,:,:);
%f=f(2:end);

return
