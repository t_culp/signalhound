function paper_plots_k_respap14(pl, dopr)
%  keyboard
% paper_plots_k_respap14(pl)
%
% Make plots for Keck 2014 results paper
%
% Immitated after paper_plots_k_respap14
% 
% This file should know how to make all of the plots and quoted numbers
% for the upcoming paper allowing us to:
% 1) maintain a common "look and feel"
% 2) anyone can tweak any plot rather then having to refer to its
% "keeper"
% 
% Some notes:
%
% - It's probably going to be necessary to always run this script
% interactively as batch running seems to produce mysteriously
% different plots.
% eps->pdf is by far the best for a paper being infinitely rescalable
% - if it really won't work then pdflatex also eats png - jpg is
% *crap*

global dopres;
if exist('dopr','var') & dopr
  set(0,'defaultlinelinewidth',1.0);
  set(0,'DefaultAxesFontSize',14);
%    set(0,'defaulttextfontsize',14);
  dopres = 1;
else
  dopres = 0;
end

% make T/Q/U map plot
if(any(pl==1))
  make_mapplot;
end

% make 6 panel power spectrum plot
if(any(pl==2))
  make_powspecres;
end

% jackknife table and pte histograms
if(any(pl==11))
  make_jacktab;
end

% spectral jackknife pte table and figure
if(any(pl==12))
  make_spectraljacktab;
end

% make E/B map plot
if(any(pl==3))
  make_ebplot;
end

% make BB devs plots
if(any(pl==4))
  make_bbdevs;
end

% make b2xk spectrum
if(any(pl==5))
  make_b2xk;
end
if(any(pl==6))
  make_b2pk;
end
if(any(pl==7))
  make_b1xb2pk;
end

% generate the quotes statistics
if(any(pl==13))
  make_quotedstats;
end

% make suppression factor plot
if(any(pl==19))
  make_supfac;
end

% r constraint
if(any(pl==20))
  make_rconstraint;
end

% r vs l
if(any(pl==21))
  make_rvsl;
end
% c vs i power law
if(any(pl==22))
  make_cvsi;
end
% r vs d
if (any(pl==23))
  make_rvsd;
end
% beammap sims plot
if(any(pl==24))
  make_beammapsimsplot;
end
% bicep2+keck vs world
if(any(pl==25))
  make_b2k_vs_world
end
% livetime,sensitivity, map depth
if(any(pl==27))
  make_sens
end
% the quoted map depths and areas
if(any(pl==28))
  make_mapdepth
end
% make the weights statistics 
if(any(pl==29))
  calc_corr
end
if (any(pl==101))
  make_powspecrestable
end

if any(pl==102)
  make_bpwftable
end

if any(pl==103)
  make_hlproductfiles
end

if any(pl==104)
  make_inputspectrafiles
end

return

%%%%%%%%%%%%%%%%%%%%%%%%
% five functions: get_b2xb2 get_kxk, get_b2xk, get_b2k_comb, get_b2xkxb1
% all filenames and data must be loaded calling those functions!
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
function [r,im,ml, bpwf,filename,mt,rscaling,jackname,mapname,projfile]=get_b2xb2
% Single function fetches official B2xB2 spectra and models for
% plotting etc - one point switch!

%  filename = 'final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf_rbc_lensfix.mat';
filename = 'final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf_rbc.mat';
jackname = '';
mapname  = 'maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0.mat';
projfile = '';
load(filename)
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN;
im=inpmod;

% get the lensing model to compare to lensing sims
%  ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing_lensfix.fits');
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');
%  ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing.fits');

% get the tensor only model, mainly to plot the EE tensor only
% this is currently not a fits file so be care full about the data columns
% 
mt=load('aux_data/official_cl/camb_planck2013_r0p1_tenscls.dat');
% extend it to the same l range as the other inputs. Note quite sure
% why it is not to start with. However, checked that tensor B is the same as in im
mt = interp1(mt(:,1),mt,ml.l);

%  rscaling = 'aps/0751/xxx4_a_filtp3_weight3_gs_dp1100_jack0_xxx5_a_filtp3_weight3_gs_dp1100_jack0_xxx6_a_filtp3_weight3_gs_dp1100_jack0_matrix_lensfix.mat'
rscaling = 'aps/0751/xxx4_a_filtp3_weight3_gs_dp1100_jack0_xxx5_a_filtp3_weight3_gs_dp1100_jack0_xxx6_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat';

return

%%%%%%%%%%%%%%%%%%%%%%%%
function [r,im,ml, bpwf,filename,mt,rscaling,jackname,mapname,projfile]=get_kxk()
% Single function fetches official KeckxKeck (2012+2013) spectra and models for
% plotting etc - one point switch!
filename = 'final/1351/real_a_filtp3_weight3_gs_dp1102_jack01_real_b_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall_directbpwf.mat';
% serves as proxy to get to the different jacks by doing a string replace on jackX
jackname = 'final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jackX_real_a_filtp3_weight3_gs_dp1102_jackX1_real_b_filtp3_weight3_gs_dp1102_jackX1_pureB_overrx_overall_directbpwf.mat';
%mapname  = ' maps/1353/real_ab_filtp3_weight3_gs_dp11022_jack01.mat';
mapname  = 'maps/1351/real_ab_filtp3_weight3_gs_dp1102_jack0.mat';
% the matrix deprojection file
projfile='/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat';

%  filename = '1351/real_a_fixesub_filtp3_weight3_gs_dp1102_jack01_real_b_fixesub_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall_directbpwf.mat';
%  filename = '1353/real_a_filtp3_weight3_gs_dp11022_jack01_real_b_filtp3_weight3_gs_dp11022_jack01_matrix_overrx_overall_directbpwf.mat';
load(filename);
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN;
im=inpmod;

% get the lensing model to compare to lensing sims
%  ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing.fits');
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');
%  ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing_lensfix.fits');

% get the tensor only model, mainly to plot the EE tensor only
% this is currently not a fits file so be care full about the data columns
% 
mt=load('aux_data/official_cl/camb_planck2013_r0p1_tenscls.dat');
% extend it to the same l range as the other inputs. Note quite sure
% why it is not to start with. However, checked that tensor B is the same as in im
mt = interp1(mt(:,1),mt,ml.l);

% rscaling for reduc_final_rlim_direct
rscaling='aps/1351/xxx4_ab_filtp3_weight3_gs_dp1100_jack01_xxx5_ab_filtp3_weight3_gs_dp1100_jack01_xxx6_ab_filtp3_weight3_gs_dp1100_jack01_matrix_overrx.mat';

return

%%%%%%%%%%%%%%%%%%%%%%%%
function [r,im,filename,jackname]=get_b2xk()
% Single function fetches official KeckxKeck (2012+2013) spectra and models for
% plotting etc - one point switch!

% don't know if the right version here exists
% filename =  final/0751x1353/real_a_fixesub_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp11022_jack01_real_b_filtp3_weight3_gs_dp11022_jack01_matrix_overrx_overall_directbpwf.mat
filename = 'final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_real_b_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall_directbpwf.mat';

% serves as proxy to get to the spectral and experiment jacks
%  jackname = 'final/0751x1353/real_a_fixesub_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp11022_jack01_real_b_filtp3_weight3_gs_dp11022_jack01_matrix_overrx_overall.mat';
jackname = 'final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_real_b_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall.mat';

load(filename);
r=r(3);
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN;
im=inpmod;

return

%%%%%%%%%%%%%%%%%%%%%%%%
function [r,im,ml, bpwf,filename,mt,rscaling,jackname,mapname,projfile]=get_b2k_comb
% Single function fetches official B2xB2 spectra and models for
% plotting etc - one point switch!
%  filename = 'final/0751x1353/real_a_fixesub_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp11022_jack01_real_b_filtp3_weight3_gs_dp11022_jack01_matrix_overrx_overall_directbpwf_debias_comb_lensfix.mat';
%  filename = 'final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_real_b_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall_directbpwf_debias_comb.mat';
% switch to map combine:
filename = 'final/1456/real_aab_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat';
jackname = '';
mapname  = 'maps/1456/real_aab_filtp3_weight3_gs_dp1102_jack0.mat';
projfile = '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat';
load(filename)
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN;
im=inpmod;

% get the lensing model to compare to lensing sims
%  ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing_lensfix.fits');
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');
%  ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing.fits');

% get the tensor only model, mainly to plot the EE tensor only
% this is currently not a fits file so be care full about the data columns
% 
mt=load('aux_data/official_cl/camb_planck2013_r0p1_tenscls.dat');
% extend it to the same l range as the other inputs. Note quite sure
% why it is not to start with. However, checked that tensor B is the same as in im
mt = interp1(mt(:,1),mt,ml.l);

%  rscaling = 'aps/0751x1351/xxx4_a_dp1100_jack0_xxx5_a_dp1100_jack0_xxx6_a_dp1100_jack0_xxx4_ab_dp1100_jack01_xxx5_ab_dp1100_jack01_xxx6_ab_dp1100_jack01_matrix_overrx_comb_lensfix.mat';
%  rscaling = 'aps/0751x1351/xxx4_a_dp1100_jack0_xxx5_a_dp1100_jack0_xxx6_a_dp1100_jack0_xxx4_ab_dp1100_jack01_xxx5_ab_dp1100_jack01_xxx6_ab_dp1100_jack01_matrix_overrx_comb.mat';
rscaling = '';

return

function [filename] = get_b2xkxb1()
  %    filename = 'final/0751x1353x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_ab_filtp3_weight3_gs_dp11022_jack01_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix_overrx_comb.mat';
  filename = 'final/0751x1351x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_ab_filtp3_weight3_gs_dp1102_jack01_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix_overrx_comb.mat';
  load(filename)
return

%%%%%%%%%%%%%%%%%%%%%%%%
% five functions to define the data set now done.
%%%%%%%%%%%%%%%%%%%%%%%%


function make_mapdepth()
  keyboard
  jackname={};
  [dum,dum,dum,dum,dum,dum,dum,dum,mapname,dum]=get_b2xb2;
  jackname{1} = strrep(mapname,'jack0','jack3');
  [dum,dum,dum,dum,dum,dum,dum,dum,mapname,dum]=get_kxk;
  jackname{2} = strrep(mapname,'jack0','jack3');
  [dum,dum,dum,dum,dum,dum,dum,dum,mapname,dum]=get_b2k_comb;
  jackname{3} = strrep(mapname,'jack0','jack3');

  exp = {'B2  ','K   ','B2+K'};
  disp('Map depths and areas as from  ''Max depth, Pvar smoothed adjusted'' recipe, here:');
  disp('http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20140311_map_depth_3/');
  disp('');
  for ii=1:3
    load(jackname{ii})
    % bicep2 is uncalibrated
    % since this is run from the keck_analysis side
    % there is no access to the abscal, hard code it here:
    if (ii==1)
      ac=cal_coadd_ac(ac,3150,coaddopt);
    end
    map = make_map(ac,m,coaddopt);
    map=jackknife_map(map);
    Pvar_ns=(map.Qvar+map.Uvar)/2;
    map=smooth_varmaps(m,map);
    Pvar=(map.Qvar+map.Uvar)/2;
    [Qd Qa]=calc_map_deptharea(m,map.Q,1./Pvar,Pvar_ns);
    [Ud Ua]=calc_map_deptharea(m,map.U,1./Pvar,Pvar_ns);
    % combined depth
    depth=sqrt(2)*(Qd.^(-2)+Ud.^(-2)).^-.5;
    % area is the average of Q and U
    area=(Qa+Ua)./2;
    % total map depth is depth x area
    sens=depth./sqrt(area)/sqrt(2);
    disp(sprintf('%s : depth %d nk-deg, area %d deg^2, total sensitivity %d nK', exp{ii}, depth*1000, area, sens*1000));
  end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate how correlated the b2 and keck maps are

function calc_corr()

  jackname={};
  [rb2,dum,dum,dum,dum,dum,dum,dum,mapname,dum]=get_b2xb2;
  jackname{1} = mapname;
  [rk,dum,dum,dum,dum,dum,dum,dum,mapname,dum]=get_kxk;
  jackname{2} = mapname;
  [rbk,dum,dum,dum,dum,dum,dum,dum,mapname,dum]=get_b2k_comb;
  
  for ii=1:2
    load(jackname{ii})
    if ii==1
      ac=cal_coadd_ac(ac,3150,coaddopt);
    end
    map=make_map(ac,m,coaddopt);
    weight{ii}= 1./(map.Qvar+map.Uvar);
  end

  % correlation as sum(w_b2xw_k)/sqrt(sum(w_b2^2)*sum(w_k^2))
  corr=nansum(nansum(weight{1}.*weight{2}))/sqrt(nansum(nansum(weight{1}.^2))*nansum(nansum(weight{2}.^2)))
  disp(['Expected correlation between Bicep2 and Keck is ' num2str(corr)]);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_noise_ratio()

  [rb2,im,ml,dum,dum,dum,dum,dum,mapname,dum]=get_b2xb2;
  [rk,dum,dum,dum,dum,dum,dum,dum,mapname,dum]=get_kxk;
  [rbk,dum,dum,dum,dum,dum,dum,dum,mapname,dum]=get_b2k_comb;
  % get the spec combined spectrum
  filename = 'final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_real_b_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall_directbpwf_debias_comb.mat';
  load(filename);
  rbcs = r(1);

  noise_ratio = std(rb2(1).noi(:,4,:),[],3)./std(rbk(1).noi(:,4,:),[],3);
  sim_ratio = std(rb2(1).sim(:,4,:),[],3)./std(rbk(1).sim(:,4,:),[],3); 

  noise_spec_ratio = std(rbcs(1).noi(:,4,:),[],3)./std(rbk(1).noi(:,4,:),[],3);
  noise_bcs_ratio = std(rb2(1).noi(:,4,:),[],3)./std(rbcs(1).noi(:,4,:),[],3); 

  noistd=std(rbk(1).noi(:,4,:),[],3);
  simstd=std(rbk(1).sim(:,4,:),[],3);
  simrstd=std(rbk(1).simr(:,4,:),[],3);

  mean(noise_ratio(2:10))

  plot(rbk(1).lc(:,4),std(rb2(1).noi(:,4,:),[],3)); hold on;
  plot(rbk(1).lc(:,4),std(rk(1).noi(:,4,:),[],3),'r')
  plot(rbk(1).lc(:,4),std(rbk(1).noi(:,4,:),[],3),'g')

return


function make_allBB()

clf; setwinsize(gcf,400,300)
subplot(1,1,1);

% First panel - compare the spectra

% get the real points
[rk,im,ml]=get_kxk;

% get the bicep2 point
rb2=get_b2xb2;

% get the cross points
rbk=get_b2xk()

% get the cross points
rbc=get_b2k_comb()

% get the spec combined spectrum
filename = 'final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_real_b_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall_directbpwf_debias_comb.mat';
load(filename);
rbcs = r(1);

line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
% commenting out r=0.20 curve
%plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
plot(im.l,ml.Cs_l(:,4),'r');
% plot B2xB2
h1=errorbar2(rb2(1).lc(:,4)-2,rb2(1).real(:,4),std(rb2(1).sim(:,4,:),[],3),'k.');
set(h1(2),'MarkerSize',12);
% plot kxk
h2=errorbar2(rk(1).lc(:,4)+4,rk(1).real(:,4),std(rk(1).sim(:,4,:),[],3),'kx');
set(h2(2),'Color',[0,0,.7]); set(h2(1),'Color',[.0,.0,.7]);
% plot B2xK
h3=errorbar2(rk(1).lc(:,4)-4,rbk(1).real(:,4),std(rbk(1).sim(:,4,:),[],3),'k*');
set(h3(2),'Color',[.0,.7,.0]); set(h3(1),'Color',[.0,.7,.0]);
% plot B2+K
h4=errorbar2(rk(1).lc(:,4)+2,rbc(1).real(:,4),std(rbc(1).sim(:,4,:),[],3),'k^');
set(h4(2),'Color',[.7,.0,.0]); set(h4(1),'Color',[.7,.0,.0]);
% plot B2+K derived from cross and auto:
%  h5=errorbar2(rk(1).lc(:,4),(rk(1).real(:,4)+rb2(1).real(:,4)+2*rbk(1).real(:,4)),std(rk(1).sim(:,4,:)+rb2(1).sim(:,4,:)+2*rbk(1).sim(:,4,:),[],3),'kv');
%  set(h5(2),'Color',[.7,.7,.0]); set(h5(1),'Color',[.7,.7,.0]);
% plot B2+K derived spectral combination
h6=errorbar2(rk(1).lc(:,4),rbcs(1).real(:,4),std(rbcs(1).sim(:,4,:),[],3),'kv');
set(h6(2),'Color',[.0,.0,.7]); set(h6(1),'Color',[.0,.0,.7]);

h=legend([h1(2),h2(2),h3(2),h4(2),h6(2)],{'B2xB2','KxK','B2xK','(B2+K)x(B2+K)','B2+K spec'},'Location','nw');
legend boxoff
hold off
xlim([0,330]); ylim([-.01,.055]);
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')

print -depsc2 paper_plots/b2kall.eps
fix_lines('paper_plots/b2kall.eps')
!epstopdf paper_plots/b2kall.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_quotedstats()
[dum,dum,dum, dum, filename]=get_kxk
filename = strrep(filename,'final/','');

% kxk detection significances as quoted
rfpopt.chibins=get_chibins;

disp('PTE of kxk spectrum given lensed-LCDM model');
nsims = 1e9;
rfpopt.nsim=nsims; % 1e8 is required here to get good stats on chi2 - chi1 seems impossible to compute.
                 % 1e8 may not be runnable on odyssey login nodes -works on spud
rfpopt.chibins{4}=2:10;
disp(sprintf('Bins %d through %d:',rfpopt.chibins{4}([1,end])));
reduc_final_bb_pte(filename,rfpopt,0);
rfpopt.chibins{4}=2:6;
disp(sprintf('Bins %d through %d:',rfpopt.chibins{4}([1,end])));
reduc_final_bb_pte(filename,rfpopt,0);
rfpopt.nsim=nsims;
disp(' ')

% and for the combined data
[dum,dum,dum,dum,filename]=get_b2k_comb
filename = strrep(filename,'final/','');
disp('PTE of b2+k x b2+k spectrum given lensed-LCDM model');
rfpopt.chibins{4}=2:10;
disp(sprintf('Bins %d through %d:',rfpopt.chibins{4}([1,end])));
reduc_final_bb_pte(filename,rfpopt,0);
rfpopt.chibins{4}=2:6;
disp(sprintf('Bins %d through %d:',rfpopt.chibins{4}([1,end])));
reduc_final_bb_pte(filename,rfpopt,0);
rfpopt.nsim=nsims;
rfpopt.chibins{4}=7:10;
disp(sprintf('Bins %d through %d:',rfpopt.chibins{4}([1,end])));
reduc_final_bb_pte(filename,rfpopt,0);
disp(' ')

%  with nsim 1e9:
%  PTE of kxk spectrum given lensed-LCDM model
%  Bins 2 through 10:
%  Chi2: 707 of 1.00e+09 sims greater than real - pte=7.07e-07, sigma=4.96
%  Chi1: 74 of 1.00e+09 sims greater than real - pte=7.40e-08, sigma=5.26
%  Bins 2 through 6:
%  Chi2: 394 of 1.00e+09 sims greater than real - pte=3.94e-07, sigma=5.07
%  Chi1: 0 of 1.00e+09 sims greater than real - pte=0.00e+00, sigma=Inf
%  
%  PTE of b2+k x b2+k spectrum given lensed-LCDM model
%  Bins 2 through 10:
%  Chi2: 0 of 1.00e+09 sims greater than real - pte=0.00e+00, sigma=Inf
%  Chi1: 0 of 1.00e+09 sims greater than real - pte=0.00e+00, sigma=Inf
%  Bins 2 through 6:
%  Chi2: 0 of 1.00e+09 sims greater than real - pte=0.00e+00, sigma=Inf
%  Chi1: 0 of 1.00e+09 sims greater than real - pte=0.00e+00, sigma=Inf
%  Bins 7 through 10:
%  Chi2: 201259029 of 1.00e+09 sims greater than real - pte=2.01e-01, sigma=1.28
%  Chi1: 25969719 of 1.00e+09 sims greater than real - pte=2.60e-02, sigma=1.94
%   

return


%%%%%%%%%%%%%%%%%%%%%
function make_mapplot

[dum,dum,dum,dum,dum,dum,dum,dum,mapname]=get_kxk();
load(mapname,'ac','m');
ac=coadd_ac_overrx(ac);
maps=make_map(ac,m);

mapname = strrep(mapname,'jack0','jack3');
load(mapname,'ac','m');
ac=coadd_ac_overrx(ac);
map=make_map(ac,m);
mapj=jackknife_map(map);

fig=figure(1);
clf;

dim=get_sixpaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% T

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,maps.T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
title('T signal');

ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapj.T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax2,'FontSize',7,'FontWeight','normal');
set(ax2,'Xticklabel',[],'Yticklabel',[]);
title('T jackknife');

% Q

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,maps.Q,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax3,'FontSize',7,'FontWeight','normal');
set(ax3,'Xticklabel',[]);
title('Q signal');

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapj.Q,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax4,'FontSize',7,'FontWeight','normal');
set(ax4,'Xticklabel',[],'Yticklabel',[]);
title('Q jackknife');

% U

ax5=axes('Position',[dim.x1,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,maps.U,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax5,'FontSize',7,'FontWeight','normal');
title('U signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

ax6=axes('Position',[dim.x2,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,mapj.U,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax6,'FontSize',7,'FontWeight','normal');
set(ax6,'Yticklabel',[]);
title('U jackknife');

% colorbars

ax7 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax7,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-150,150,1000),repmat(linspace(-150,150,1000)',1,10));
colormap hot;
freezeColors;
set(ax7,'Xtick',[]);
set(ax7,'YDir','normal','YAxisLocation','right','YTick',[-100 0 100]);

ax8 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax8,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax8,'Xtick',[]);
set(ax8,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

ax9 = axes('Position',[dim.x3,dim.y3,dim.cbar/dim.W,dim.ymap]);
set(ax9,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax9,'Xtick',[]);
set(ax9,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

% colorbar units

ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y3 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% this works way better than direct print to pdf
print -depsc2 paper_plots/tqu_maps.eps
fix_lines('paper_plots/tqu_maps.eps')
!epstopdf paper_plots/tqu_maps.eps

return

%%%%%%%%%%%%%%%%%%%%%
% used by make_mapplot above
function dim=get_sixpaneldims(m)
% Parameters for six-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 3 * dim.maph + dim.wide + 3 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 2 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 2 * dim.maph + 2 * dim.med) / dim.H; % Bottom edge of row 1.
dim.y2 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y3 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return

%%%%%%%%%%%%%%%%%%%%%%%%
function make_jacktab()

chibins1=get_chibins;
for i=1:6
  chibins2{i}=2:10;
end
for i=1:6
  chibins3{i}=7:10;
end

%  jackstr='123f456789abcdegr';
% take out the pt (g) and rx jack(r)
jackstr='123f456789abcde';

% 500 rlz constrained
[dum,dum,dum,dum,dum,dum,dum,rf]=get_kxk();
% initiate a running index since we're now allowing for a sub-division
k=0;

% read in all of the jacks and calc the PTEs
for j=jackstr

  n=findstr(rf,'jack');
  rf_j=rf; rf_j(n+4)=sprintf('%s',j);
  inds=[2]; jackappend={''};
  % adjust for alt dk and 1st half/2nd
  if strcmp(j,'f');
    rf_j=strrep(rf_j,'_overall','');
    inds=[6];
  end
  if strcmp(j,'7');
    rf_j=strrep(rf_j,'_overall','');
    inds=[2,3]; jackappend={' 2012',' 2013'};
  end
  if strcmp(j,'g');
    rf_j=strrep(rf_j,'1353','1331');
    rf_j=strrep(rf_j,'11022','1102');
  end
  % don't know how the rx jack works, take it out
  % if strcmp(j,'r')
  %   rf_j='final/1353/real_ab_filtp3_weight3_gs_dp11022_jackf1_pureB_overall_rxjack.mat';
  % end

  for ii=inds
    disp(sprintf('%s',rf_j));
    load(sprintf('%s',rf_j));
   
    % don't know how the rx jack works, take it out
    % rxjack uses column 3, no idea why this is a for loop over inds=1
    % if strcmp(j,'r')
    %  r=r(3);
    % else
    %  r=r(ii);
    % end
    r=r(ii);
    k=k+1;  % step the index

    % definte jacknames here
    js=get_jackdef(j);
    
    % rename jack3f
    if strcmp(j,'3')
      js='Early/Late Season jackknife';
    elseif strcmp(j,'f')
      js='Year Split jackknife';
    end
    
    % set expectation value to mean of s+n sims
    r.expv=mean(r.sim,3);
    r=get_bpcov(r);

    ra{k,1}=calc_chi(r,chibins1);
    ra{k,1}=calc_devs(ra{k,1},chibins1);
    ra{k,1}(1).jacktype=j;
    ra{k,1}(1).jackstring=[js jackappend{inds==ii}];

    ra{k,2}=calc_chi(r,chibins2);
    ra{k,2}=calc_devs(ra{k,2},chibins2);
    ra{k,2}(1).jacktype=j;
    ra{k,2}(1).jackstring=[js jackappend{inds==ii}];

    ra{k,3}=calc_chi(r,chibins3);
    ra{k,3}=calc_devs(ra{k,3},chibins3);
    ra{k,3}(1).jacktype=j;
    ra{k,3}(1).jackstring=[js jackappend{inds==ii}]; 
  end
end

fp=fopen('paper_plots/ptetab.tex','w');

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
fprintf(fp,'\\\\ \n');
%s=2:6; % include TE,EE,BB,TB,EB
s=[3,4,6]; % include EE,BB,EB only as B13
for j=1:size(ra,1)
  js=ra{j}.jackstring;
  fprintf(fp,'\\multicolumn{5}{l}{%s} \\\\ \n',js);
  for i=s
    % replace zero values with PTEt
    if(ra{j,1}.ptes(i)==0)
      ra{j,1}.ptes(i)=ra{j,1}.ptet(i); disp(sprintf('chi2 1 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,2}.ptes(i)==0)
      ra{j,2}.ptes(i)=ra{j,2}.ptet(i); disp(sprintf('chi2 2 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,1}.ptes_sumdev(i)==0)
      ra{j,1}.ptes_sumdev(i)=ra{j,1}.ptet_sumdev(i); disp(sprintf('chi1 1 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,2}.ptes_sumdev(i)==0)
      ra{j,2}.ptes_sumdev(i)=ra{j,2}.ptet_sumdev(i); disp(sprintf('chi1 2 rep zero j=%d, i=%d',j,i));
    end

    fprintf(fp,'%s & %5.3f & %5.3f & %5.3f & %5.3f & %5.3f & %5.3f\\\\ \n',lab{i},...
            ra{j,1}.ptes(i),ra{j,2}.ptes(i),ra{j,3}.ptes(i),...
            ra{j,1}.ptes_sumdev(i),ra{j,2}.ptes_sumdev(i),ra{j,3}.ptes_sumdev(i));
  end
  ptes1(j,:)=ra{j,1}.ptes(s);
  ptes2(j,:)=ra{j,2}.ptes(s);
  ptes3(j,:)=ra{j,3}.ptes(s);
  ptes1_sumdev(j,:)=ra{j,1}.ptes_sumdev(s);
  ptes2_sumdev(j,:)=ra{j,2}.ptes_sumdev(s);
  ptes3_sumdev(j,:)=ra{j,3}.ptes_sumdev(s);
end

fclose(fp);
keyboard

% make pte dist figure
clf; setwinsize(gcf,400,600);
yl=10.5;
subplot(3,2,1); hfill(ptes1,20,0,1); title('Band powers 1-5 \chi^2'); ylim([0,yl]);
subplot(3,2,3); hfill(ptes2,20,0,1); title('Band powers 1-9 \chi^2'); ylim([0,yl]);
subplot(3,2,5); hfill(ptes3,20,0,1); title('Band powers 6-9 \chi^2'); ylim([0,yl]);
subplot(3,2,2); hfill(ptes1_sumdev,20,0,1); title('Band powers 1-5 \chi'); ylim([0,yl]);
subplot(3,2,4); hfill(ptes2_sumdev,20,0,1); title('Band powers 1-9 \chi'); ylim([0,yl]);
subplot(3,2,6); hfill(ptes3_sumdev,20,0,1); title('Band powers 6-9 \chi'); ylim([0,yl]);

print -depsc2 paper_plots/ptedist.eps
fix_lines('paper_plots/ptedist.eps')
!epstopdf paper_plots/ptedist.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function h=small_errorbar(x,y,e,c)

h=errorbar(x,y,e,c);
hb=get(h,'children');
Xdata=get(hb(2),'Xdata');
temp=4:3:length(Xdata);
temp(3:3:end)=[];
% xleft and xright contain the indices of the left and right
%  endpoints of the horizontal lines
xleft = temp; xright = temp+1;
% Decrease line length by 0.2 unites
Xdata(xleft) = Xdata(xleft) + 2.5;
Xdata(xright) = Xdata(xright) - 2.5;
set(hb(2),'Xdata',Xdata)

%%%%%%%%%%%%%%%%%%%%%%%%
function make_spectraljacktab

clf; setwinsize(gcf,400,600)
hs=subplot_grid(2,1,1);

% First panel - compare the spectra
% get the real points
[rk,im,ml]=get_kxk;
% get the bicep2 point
rb2=get_b2xb2;
% get the cross points
rbk=get_b2xk;

line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
% commenting out r=0.20 curve
%plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
plot(im.l,ml.Cs_l(:,4),'r');
% plot B2xB2
h1=errorbar2(rb2(1).lc(:,4),rb2(1).real(:,4),std(rb2(1).simr(:,4,:),[],3),'k.');
set(h1(2),'MarkerSize',12);
small_errorbar(rb2(1).lc(:,4),rb2(1).real(:,4),std(rb2(1).sim(:,4,:),[],3),'k.');
% plot kxk
h2=errorbar2(rk(1).lc(:,4)+5,rk(1).real(:,4),std(rk(1).simr(:,4,:),[],3),'kx');
set(h2(2),'Color',[.3,.3,.3]); set(h2(1),'Color',[.3,.3,.3]);
h4=small_errorbar(rk(1).lc(:,4)+5,rk(1).real(:,4),std(rk(1).sim(:,4,:),[],3),'kx');
set(h4(1),'Color',[.3,.3,.3]);
% plot B2xK
h3=errorbar2(rk(1).lc(:,4)-5,rbk(1).real(:,4),std(rbk(1).simr(:,4,:),[],3),'k*');
set(h3(2),'Color',[.7,.7,.7]); set(h3(1),'Color',[.7,.7,.7]);
h3=small_errorbar(rk(1).lc(:,4)-5,rbk(1).real(:,4),std(rbk(1).sim(:,4,:),[],3),'k*');
set(h3(1),'Color',[.7,.7,.7]);
h=legend([h1(2),h2(2),h3(1)],{'B2xB2','KxK','B2xK'},'Location','nw');
text(15,0.038,'Neither errorbar predicts')
text(15,0.0345,'differences among datasets.')
text(15,0.031,'For this, see bottom panel')
legend boxoff
hold off
xlim([0,330]); ylim([-.01,.055]);
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')

% fix the size
pos=get(hs,'Position');
pos(3)=pos(1)+pos(3)-0.16; pos(1)=0.16;
set(hs,'Position',pos);
subplot_grid2(2,1,1,1,1);


chibins1=get_chibins;
for i=1:6
  chibins2{i}=2:10;
end

jackstr='fs';
% 500 rlz constrained
[dum,dum,dum,rf]=get_b2xk;

% initiate a running index since we're now allowing for a sub-division
k=1;

% read in all of the jacks and calc the PTEs
for j=jackstr

  n=findstr(rf,'jack');
  rf_j=rf; rf_j(n+4)=sprintf('%s',j);
  % keep matrix for jackf
  % if strcmp(j,'f')
  %   rf_j=strrep(rf_j,'matrix','pureB');
  % end
  inds=[1]; jackappend={''};
  if strcmp(j,'s')
    rf_j(n+4)=sprintf('%s','0');
    inds=[1,2,3]; jackappend={' B2-cross',' B2-Keck',' cross-Keck'};
  end

  load(sprintf('%s',rf_j));
  disp(sprintf('%s',rf_j));
  if strcmp(j,'s')
    % For B2,Keck,B2xKeck a=1,b=2 and c=3, c is always the cross spectrum:
    r=specjack(r,1,2,3,1);

    % make comparable to "experiment" jack and others which divide
    % map by 2 - divide power by 4
    for i=1:length(r)
      r(i).real=r(i).real/4;
      r(i).sim=r(i).sim/4;
      r(i).noi=r(i).noi/4;
    end
  end

  % want to replace the sim with simr for experiment jack (done for spectral already)
  if strcmp(j,'f')
    for i=1:length(r)
      r(i).sim=r(i).simr;
    end
  end
  
  %set the expectation value to the mean of s+n sims
  for i=1:length(r)
    r(i).expv=mean(r(i).sim,3);
  end
  r=get_bpcov(r);
  
  % definte jacknames here
  js=get_jackdef(j);

  % save for plotting
  r_j=r;
  
  for ii=inds
    r=r_j(ii);
    %save for plotting
    r_jall(k).l=r.l;
    r_jall(k).real=r.real;
    r_jall(k).sim=r.sim;
    ra{k,1}=calc_chi(r,chibins1);
    ra{k,1}=calc_devs(ra{k,1},chibins1);
    ra{k,1}(1).jacktype=j;
    ra{k,1}(1).jackstring=[js jackappend{inds==ii}];

    ra{k,2}=calc_chi(r,chibins2);
    ra{k,2}=calc_devs(ra{k,2},chibins2);
    ra{k,2}(1).jacktype=j;
    ra{k,2}(1).jackstring=[js jackappend{inds==ii}];

    % do for all 
    for jj=2:10
      for kk=1:6
        chibinstmp{kk}=jj;
      end
      ratmp{k}=calc_chi(r,chibinstmp);
      ptesb(k,jj)=ratmp{k}.ptes(4);
    end 
    k=k+1; % step the index
  end
end


fp=fopen('paper_plots/ptespectab.tex','w');

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
fprintf(fp,'\\\\ \n');
s=[3,4,6]; % include EE,BB,EB only as B13
for j=1:size(ra,1)
  js=ra{j}.jackstring;
  fprintf(fp,'\\multicolumn{5}{l}{%s} \\\\ \n',js);
  for i=s
    % replace zero values with PTEt
    if(ra{j,1}.ptes(i)==0)
      ra{j,1}.ptes(i)=ra{j,1}.ptet(i); disp(sprintf('chi2 1 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,2}.ptes(i)==0)
      ra{j,2}.ptes(i)=ra{j,2}.ptet(i); disp(sprintf('chi2 2 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,1}.ptes_sumdev(i)==0)
      ra{j,1}.ptes_sumdev(i)=ra{j,1}.ptet_sumdev(i); disp(sprintf('chi1 1 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,2}.ptes_sumdev(i)==0)
      ra{j,2}.ptes_sumdev(i)=ra{j,2}.ptet_sumdev(i); disp(sprintf('chi1 2 rep zero j=%d, i=%d',j,i));
    end

    fprintf(fp,'%s & %5.3f & %5.3f & %5.3f & %5.3f \\\\ \n',lab{i},...
            ra{j,1}.ptes(i),ra{j,2}.ptes(i),ra{j,1}.ptes_sumdev(i),ra{j,2}.ptes_sumdev(i));
  end
  ptes1(j,:)=ra{j,1}.ptes(s);
  ptes2(j,:)=ra{j,2}.ptes(s);
  ptes1_sumdev(j,:)=ra{j,1}.ptes_sumdev(s);
  ptes2_sumdev(j,:)=ra{j,2}.ptes_sumdev(s);
end

fclose(fp);

% Second panel - spectral differences
hs=subplot_grid(2,1,2);

% also make a comparison plot of the BB jackknife
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
%remove the first point by naning ell?
r_jall(1).l(1)=NaN;
rb2(1).lc(1,4)=NaN;
h1=errorbar2(rb2(1).lc(:,4)-6,r_jall(1).real(:,4),std(r_jall(1).sim(:,4,:),[],3),'k.');
set(h1(2),'MarkerSize',12);
h2=errorbar2(rb2(1).lc(:,4)-2,r_jall(2).real(:,4),std(r_jall(2).sim(:,4,:),[],3),'kx');
xx=1*.25;  set(h2(2),'Color',[xx,xx,xx]); set(h2(1),'Color',[xx,xx,xx]);
h3=errorbar2(rb2(1).lc(:,4)+2,r_jall(3).real(:,4),std(r_jall(3).sim(:,4,:),[],3),'k+');
xx=2*.25;  set(h3(2),'Color',[xx,xx,xx]); set(h3(1),'Color',[xx,xx,xx]);
h4=errorbar2(rb2(1).lc(:,4)+6,r_jall(4).real(:,4),std(r_jall(4).sim(:,4,:),[],3),'k*');
xx=3*.25;  set(h4(2),'Color',[xx,xx,xx]); set(h4(1),'Color',[xx,xx,xx]);

h=legend([h1(2) h2(2) h3(2) h4(2)],{'Map Jack','B2xB2-B2xK','B2xB2-KxK','B2xK-KxK'},'Location','NW');
legend boxoff
hold off
xlim([0 330]); ylim([-0.01, 0.01]);
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
% fix the size
pos=get(hs,'Position');
pos(3)=pos(1)+pos(3)-0.16; pos(1)=0.16;
set(hs,'Position',pos);
subplot_grid2(2,1,2,1,1);

print -depsc2 paper_plots/specjack.eps
fix_lines('paper_plots/specjack.eps')
!epstopdf paper_plots/specjack.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_powspecres

for i=1:6
  chibins{i}=[2:10]; % use all plotted bins
end

% For TT we use legancy 1350 points... this is a blatant kludge but
% who cares about TT?
load final/1450x1350/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_real_b_filtp3_weight3_gs_dp1102_jack01_overrx_overall
r=r(2); r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN; r=calc_chi(r,chibins,[],1);
rt=r;

% get the real points
[rs,im,ml,dum,dum,mt,dum,jackname]=get_kxk;
rs=calc_chi(rs,chibins,[],1);

% the jackname comes with jackX as jig, replace to jack3
jackname = strrep(jackname,'jackX','jack3')
% get the jackknife spectra 
load(jackname)
r=r(2);
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN; r=calc_chi(r,chibins,[],1);
rj=r;

clf; setwinsize(gcf,1000,600)

ms=10; % marker size

subplot(4,3,1); a1=gca;
% plot the TT
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,1),'r');
% commenting out r=0.20 curve
%plot(im.l,im.Cs_l(:,1)+2*mt(:,2),'r--');
%plot(im.l,2*mt(:,2),'r--');
h=errorbar2(rs.lc(:,1),rs.real(:,1),rt.derr(:,1),'k.');
set(h(2),'MarkerSize',ms); set(h(1),'Color',[0.7,0.7,0.7]);
hold off
xlim([0,330]); ylim([-1000,7000]);
%ptitle(sprintf('TT - \\chi^2 PTE = %.2f',rt.ptes(1)));
ptitle(sprintf('TT'));
ylabel('l(l+1)C_l/2\pi [\muK^2]')
subplot(4,3,4); a2=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rj.lc(:,1),rj.real(:,1),rj.derr(:,1),'b.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-2,5]);
ptitle(sprintf('TT jack - \\chi^2 PTE = %.2f',rj.ptes(1)),0.03,0.1);
make_axis_pair(a1,a2);

subplot(4,3,2); a1=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,2),'r');
% commenting out r=0.20 curve
%plot(im.l,im.Cs_l(:,2)+2*mt(:,5),'r--');
%plot(im.l,2*mt(:,5),'r--');
h=errorbar2(rs.lc(:,2),rs.real(:,2),rt.derr(:,2),'k.');
set(h(2),'MarkerSize',ms); set(h(1),'Color',[0.7,0.7,0.7]);
hold off
xlim([0,330]); ylim([-70,160]);
%ptitle(sprintf('TE - \\chi^2 PTE = %.2f',rs.ptes(2)));
ptitle(sprintf('TE'));
subplot(4,3,5); a2=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rj.lc(:,2),rj.real(:,2),rj.derr(:,2),'b.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); %ylim([-70,160]);
ptitle(sprintf('TE jack - \\chi^2 PTE = %.2f',rj.ptes(2)));
make_axis_pair(a1,a2);

subplot(4,3,3); a1=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,3),'r');
% commenting out r=0.20 curve
%plot(im.l,im.Cs_l(:,3)+2*mt(:,3),'r--');
%plot(im.l,2*mt(:,3),'r--');
h=errorbar2(rs.lc(:,3),rs.real(:,3),rs.derr(:,3),'k.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-1,16]);
ptitle(sprintf('EE - \\chi^2 PTE = %.2f',rs.ptes(3)));
subplot(4,3,6); a2=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rj.lc(:,3),rj.real(:,3),rj.derr(:,3),'b.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-.02,.01]);
ptitle(sprintf('EE jack - \\chi^2 PTE = %.2f',rj.ptes(3)));
make_axis_pair(a1,a2);

subplot(4,3,[7,10]); a1=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
% commenting out r=0.20 curve
%plot(im.l,im.Cs_l(:,4)*2,'r--');
%plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
%plot(rs.lc(:,4),mean(rs.sim(:,4,:),3),'r');
plot(ml.l,ml.Cs_l(:,4),'r');
h=errorbar2(rs.lc(:,4),rs.real(:,4),rs.derr(:,4),'k.');
set(h(2),'MarkerSize',ms);
%h=errorbar2(r.l+5,rk(3).real(:,4),rk(3).derr(:,4),'k.')
%set(h(2),'MarkerSize',ms,'color',[1,1,1]*0.5);
%hold off
xlim([0,330]); ylim([-.015,.055]);
ptitle(sprintf('BB - \\chi^2 PTE = 2.7\\times10^{-7}',rs.ptes(4)));
%subplot(4,3,10); a2=gca;
%line([0,330],[0,0],'LineStyle',':','color','k'); box on
%hold on
h=errorbar2(rj.lc(:,4)+4,rj.real(:,4),rj.derr(:,4),'b.');
set(h(2),'MarkerSize',ms);
%hold off
%xlim([0,330]); ylim([-.01,.01]);
ptitle(sprintf('BB jack - \\chi^2 PTE = %.2f',rj.ptes(4)),0.03,0.05);
%make_axis_pair(a1,a2);

subplot(4,3,8); a1=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,5),'r');
h=errorbar2(rs.lc(:,2),rs.real(:,5),rs.derr(:,5),'k.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-5,5]);
%ptitle(sprintf('TB - \\chi^2 PTE = %.2f',rs.ptes(5)));
ptitle(sprintf('TB'));
subplot(4,3,11); a2=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rj.lc(:,2),rj.real(:,5),rj.derr(:,5),'b.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-0.3,.1]);
ptitle(sprintf('TB jack - \\chi^2 PTE = %.2f',rj.ptes(5)));
make_axis_pair(a1,a2);

subplot(4,3,9); a1=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,6),'r');
h=errorbar2(rs.lc(:,3),rs.real(:,6),rs.derr(:,6),'k.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-.1,.1]);
ptitle(sprintf('EB - \\chi^2 PTE = %.2f',rs.ptes(6)));
subplot(4,3,12); a2=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rj.lc(:,3),rj.real(:,6),rj.derr(:,6),'b.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-.025,.015]);
ptitle(sprintf('EB jack - \\chi^2 PTE = %.2f',rj.ptes(6)));
make_axis_pair(a1,a2);

xlabel('Multipole')

print -depsc2 paper_plots/powspecres.eps
fix_lines('paper_plots/powspecres.eps')
!epstopdf paper_plots/powspecres.eps

return

%%%%%%%%

function make_axis_pair(a1,a2)
p1=get(a1,'Position');
p2=get(a2,'Position');
g=p1(2)-(p2(2)+p2(4));
g=g*0.7;
set(a1,'XtickLabel',[]);
set(a1,'Position',[p1(1),p1(2)-g,p1(3),p1(4)+g]);
return

%%%%%%%%%%%%%%%%%%%%%
function make_bbdevs()

% get the data
[r,im,ml]=get_kxk;
l=r.lc(:,4);

for i=1:6
  chibins{i}=2:10;
end

clf; setwinsize(gcf,300,200)
r=calc_devs(r,chibins,1);

% taken from reduc_final_chi2
devs=sort(r.sdevs,3);
ns=size(r.sdevs,3);
% the levels in sorted bandpowers closest to -2,1,0,1,2 sigma
ncdf = normcdf([-2:2],0,1);
t=[floor(ns*ncdf(ncdf<0.5)),ceil(ns*ncdf(ncdf>=0.5))];

plot(l,squeeze(devs(:,4,t(3))),'b');
hold on;
plot(l,squeeze(devs(:,4,t([2,4]))),'r');
plot(l,squeeze(devs(:,4,t([1,5]))),'g');
h1=plot(l,r.rdevs(:,4),'k.');
set(h1,'MarkerSize',12);
hold off
xlim([40,330]); ylim([-3,7]);
xlabel('Multipole'); ylabel('BB deviation from lensed-\LambdaCDM (\sigma)');
grid

print -depsc2 paper_plots/bbdevs.eps
fix_lines('paper_plots/bbdevs.eps')
!epstopdf paper_plots/bbdevs.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_ebplot

ellrng=[50,120];
[dum,dum,dum,dum,dum,dum,dum,dum,mapname,projfile]=get_kxk()
load(mapname);
% coadd the rxs into one map:
ac=coadd_ac_overrx(ac);
maps=make_map(ac,m);

maps=make_ebmap(m,maps,[],ellrng,projfile);

% use sim instead of jackknife
simmap = strrep(mapname,'real','0017');
% sims always come as dp1100
dp = mapname(strfind(mapname,'dp'):strfind(mapname,'jack')-2);
simmap = strrep(simmap ,dp,'dp1100');
load(simmap);
ac=coadd_ac_overrx(ac);
mapj=make_map(ac,m);
mapj=make_ebmap(m,mapj,[],ellrng,projfile);

fig=figure(1);
clf;
dim=get_fourpaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% color range
ce=2.4;
cb=2.4/6.4;

% vector length scale factor
ve=0.6;
vb=0.6*6.4;

% E

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,maps.E,'iau');
caxis([-ce,ce]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,maps.EQ,maps.EU,maps.Qvar,maps.Uvar,ve,[],3,1)
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
title('Keck Array : E signal');

ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapj.E,'iau');
caxis([-ce,ce]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,mapj.EQ,mapj.EU,mapj.Qvar,mapj.Uvar,ve,[],3,1)
set(ax2,'FontSize',7,'FontWeight','normal');
set(ax2,'Xticklabel',[],'Yticklabel',[]);
% title('E jackknife');
title('Simulation: E from lensed-{\Lambda}CDM+noise')

% B

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,maps.B,'iau');
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,maps.BQ,maps.BU,maps.Qvar,maps.Uvar,vb,[],3,1)
set(ax3,'FontSize',7,'FontWeight','normal');
title('Keck Array : B signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapj.B,'iau');
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,mapj.BQ,mapj.BU,mapj.Qvar,mapj.Uvar,vb,[],3,1)
set(ax4,'FontSize',7,'FontWeight','normal');
set(ax4,'Yticklabel',[]);
% title('B jackknife');
title('Simulation: B from lensed-{\Lambda}CDM+noise')

% colorbars

ax5 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax5,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-ce,ce,1000),repmat(linspace(-ce,ce,1000)',1,10));
colormap(colormap_lint)
freezeColors;
set(ax5,'Xtick',[]);
set(ax5,'YDir','normal','YAxisLocation','right','YTick',[-1.8 0 1.8]);

ax6 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax6,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-cb,cb,1000),repmat(linspace(-cb,cb,1000)',1,10));
colormap(colormap_lint)
freezeColors;
set(ax6,'Xtick',[]);
set(ax6,'YDir','normal','YAxisLocation','right','YTick',[-0.3 0 0.3]);

% colorbar units

ax7 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax7, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% this works way better than direct print to pdf
print -depsc2 paper_plots/eb_maps.eps
%fix_lines('paper_plots/eb_maps.eps')
!epstopdf paper_plots/eb_maps.eps

return


%%%%%%%%%%%%%%%%%%%%%
% used by make_ebplot above
function dim=get_fourpaneldims(m)
% Parameters for six-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 2 * dim.maph + dim.wide + 2 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 2 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y2 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_supfac()

% get supfac and measured bins
[r,im,ml,bpwf]=get_kxk;

% get the pixel window
pw=load('/n/bicepfs2/bicep2/pipeline/matrixdata/pixel_window_bicep_lmax500.mat')

% get bin edges
[be,nn]=get_bins('bicep_norm');

% get beam window function
bf= fitsread('aux_data/beams/beamfile_20130222_sum.fits', 'bintable');
beamf=bf{1}.^2;

if(0)  % can check against this
  beamsize=31.22/60;
  fwhm=beamsize*pi/180;
  sig=fwhm/sqrt(8*log(2));
  sigl=1./sig;
  beamcor=exp(-.5*(r(1).l.*(r(1).l+1))/(sigl.^2)).^2;
end

beamvals=interp1(1:length(beamf),beamf, r.lc(2:end,4));
pwvals=interp1(1:length(pw.wl.w_avg),pw.wl.w_avg.^2, r.lc(2:end,4));
filt=1./(beamvals.*r.rwf(2:end,4).*pwvals);

if(1)
  %colors={'r', 'g', 'm',[.6,.4,.6], [0,.3,0],[.5,0,.6],[.9,.3,0],[0,.4,.6],'c', 'k',[.8 .8 .8],[.3,.3,.9],[.8 .2 .2],[.2,.3,.3]};
  colors={'k','b','r','g','b','r','g','b','r','g'};

  % make plot
  clf
  setwinsize(gcf,400,250)
  hold on
  %plot(pw.wl.w_avg.^2,'--k')
  %plot(beamf,':k')
  %plot(r.lc(2:end,4),filt,'-.k')
  plot(r.lc(2:end,4),1./r.rwf(2:end,4),'k')
  %plot(r.l,beamcor)
  for i=2:10
    plot(r.lc(i,4),1./r.rwf(i,4), '.','Color',colors{i},'MarkerSize',12)
    plot(bpwf.l,10*bpwf.Cs_l(:,i,4), 'color', colors{i})
  end
  hold off
  ylim([0,1]);xlim([0,330])
  ylabel('Suppression Factor');
  xlabel('Multipole')
end

box off

haxes1 = get(gca,'Position');
haxes2 = axes('Position',haxes1,...
              'XAxisLocation','top',...
              'YAxisLocation','right',...
              'Color','none');
set(haxes2,'xtick',[],'ytick',[.2,.4,.6,.8,1],'yticklabel',[.2,.4,.6,.8,1]/10)

ylabel('BPWF')

print -depsc2 paper_plots/supfac.eps
fix_lines('paper_plots/supfac.eps')
!epstopdf paper_plots/supfac.eps

return


%%%%%%%%%%%%
function make_b2pk

% get the real points
[rk,im,ml]=get_kxk;

% get the bicep2 point
rb2=get_b2xb2;

% get the cross points
rbk=get_b2k_comb;

clf; setwinsize(gcf,400,300)

line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
% commenting out r=0.20 curve
%plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
plot(im.l,ml.Cs_l(:,4),'r');
% plot B2+K
h1=errorbar2(rk(1).lc(:,4),rbk(1).real(:,4),std(rbk(1).simr(:,4,:),[],3),'k.');
errorbar(rk(1).lc(:,4),rbk(1).real(:,4),std(rbk(1).sim(:,4,:),[],3),'k.');
set(h1(2),'MarkerSize',12);
h=legend([h1(2)],{'(B2+K)x(B2+K)'},'Location','nw');
legend boxoff
hold off
xlim([0,330]); ylim([-.01,.055]);
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')

print -depsc2 paper_plots/b2kcomb.eps
fix_lines('paper_plots/b2kcomb.eps')
!epstopdf paper_plots/b2kcomb.eps

return

%%%%%%%%%%%%
function make_b1xb2pk

% get the real points
[rk,im,ml]=get_kxk;

% get the bicep2 point
rb2=get_b2xb2;

% get the comb points
rbk=get_b2k_comb;

% get the b1cross points
filename = get_b2xkxb1();
load(filename)
rb1bk=r;

clf; setwinsize(gcf,400,300)

line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
% commenting out r=0.20 curve
%plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
plot(im.l,ml.Cs_l(:,4),'r');
% plot B2+K
h1=errorbar2(rk(1).lc(:,4),rbk(1).real(:,4),std(rbk(1).sim(:,4,:),[],3),'k.');
set(h1(2),'MarkerSize',12);
% plot b1x(b2+k)
h2=errorbar2(rk(1).lc(:,4)+4,rb1bk(1).real(:,4),std(rb1bk.sim(:,4,:),[],3),'kx');
set(h2(2),'Color',[.3,.3,.3]); set(h2(1),'Color',[.3,.3,.3]);
h=legend([h1(2),h2(2)],{'B2xB2+KxK+B2xK','B2xB1_{100}+KxB1_{100}'},'Location','nw');
legend boxoff
hold off
xlim([0,330]); ylim([-.01,.055]);
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')

print -depsc2 paper_plots/b1xb2k.eps
fix_lines('paper_plots/b1xb2k.eps')
!epstopdf paper_plots/b1xb2k.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_rconstraint()

%%%%%%%%%%%%%%%%%%%%
% first panel the bandpowers

% get the real points
[rbk,im,ml,dum,rf,dum,rscaling]=get_b2k_comb;
rf = strrep(rf,'final/','');
rscaling = strrep(rscaling,'aps/','');

% compute the r limits using mostly default options

% fine scaling on r:
rfropt.rvals=0:.001:0.75;
rfropt.rscaling=rscaling;
[r,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

% with s+n+r=0.2
rfropt.fiducialr=0.16;
[rp2,rfroptp2] = reduc_final_rlim_direct(rf,rfropt,0);

clf; setwinsize(gcf,900,250);

subplot_grid(1,3,1)

line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,4)*1.6+ml.Cs_l(:,4),'r--');
plot(im.l,ml.Cs_l(:,4),'r');
% plot combined
h1=errorbar2(rbk(1).lc(:,4),rbk(1).real(:,4),rp2.derr(:,4),'k.');
set(h1(2),'MarkerSize',12);
hold off
xlim([0,200]); ylim([-.005,.03]);
%title('BB');
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
axis square


%%%%%%%%%%%%%%%%%%%%
% second panel the likelihood function
subplot_grid(1,3,2)
box on; hold on

% plot a vertical line at the ML:
plot([r.rmaxlike r.rmaxlike],[0 interp1(r.rvals,r.likeBB,r.rmaxlike)],'k');
%text(r.rmaxlike*1.05,interp1(r.rvals,r.likeBB,r.rmaxlike)*1.01,'ML','color','k');

% plot the 95% confidence level:
%  c(2)=plot([r.rconf95 r.rconf95],[0 interp1(r.rvals,rlike,r.rconf95)],'r','LineWidth',2);
%  text(r.rconf95*1.05,interp1(r.rvals,rlike,r.rconf95)*1.01,'95%CL','color','r');

% plot the 68% confidence interval:
plot([r.rlo68 r.rlo68],[0 r.rcont68],'k--');
plot([r.rhi68 r.rhi68],[0 r.rcont68],'k--');

% plot the likelihood curve on top:
c(1)=plot(r.rvals,r.likeBB,'k');

set(gca,'YTick',[],'XTick',-10:0.1:10)
xlim([0 0.6])
ylim([0 r.maxlike*1.1])
xlabel('Tensor-to-scalar ratio r');
ylabel('Likelihood');
axis square
yl=ylim;
text(0.3,yl(2)*0.8,sprintf('r = %.2f^{+%.2f}_{-%.2f}',r.rmaxlike,r.rhi68-r.rmaxlike,r.rmaxlike-r.rlo68));

% numbers to quote
lz = interp1(r.rvals,r.likeBB,0)
lr=lz/r.maxlike;
[chi2,pte,sig]=likeratio2sigmas(lr);
disp(sprintf('L(zero)/L(max)=%.1e -> chi2=%.1f -> PTE=%.1e -> %.1fsigma',lr,chi2,pte,sig'));

%%%%%%%%%%%%%%%%%%%%
% third panel the maxlike hood distribution from sims
subplot_grid(1,3,3)
box on; hold on
% binning here is tricky since the real value of 0.2
% wants to lie on a bin boundary which looks odd
% we could center the first bin on zero. The direct
% method does not give values r<0, which makes this
% a bit funny. Set xlim to start from 0 - effectively
% this makes the first bin half as wide.
binsml=-0.0125:.025:0.5125;

% do two hists
n=histc([r.smaxlike],binsml);
np2=histc([rp2.smaxlike],binsml);
stairs(binsml,n,'Color',[0,.45,.7])
stairs(binsml,np2,'Color',[1,.32,0])
set(gca,'YLim',[0 max(n)*1.1]);
xlabel('Maximum likelihood r');
ylabel('Entries');
xlim([0,0.5])
% xlim([min(binsml) max(binsml)])

% line for the real data
plot([r.rmaxlike r.rmaxlike],ylim,'k');

% decoration
tx=0.45; ty=0.9; tsx=0.37; tsy=-0.07
text(tx,ty,['Data r = ' sprintf('%1.2f',r.rmaxlike)],'Units','normalized','HorizontalAlignment','left','VerticalAlignment','middle','color','k')
text(tx,ty+tsy,['S+N Sims r=0.0'],'Units','normalized','HorizontalAlignment','left','VerticalAlignment','middle','color',[0,.45,.7])
text(tx,ty+2*tsy,['S+N Sims r=0.16'],'Units','normalized','HorizontalAlignment','left','VerticalAlignment','middle','color',[1,.32,.0])
axis square

% print

print -depsc2 paper_plots/rconstraint.eps
fix_lines('paper_plots/rconstraint.eps')
!epstopdf paper_plots/rconstraint.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%
function make_rvsl()

% call external prog to prepare the data
[dum,dum,dum, dum, filename]=get_b2k_comb;
filename = strrep(filename,'final/','');

rfropt.calc_type='rvsl';
%rfropt.rvals=0:0.0025:0.4; rfropt.lvals=-0.5:0.025:3.5;
rfropt.rvals=0:0.025:0.4; rfropt.lvals=-0.5:0.01:2.5;
%rfropt.rvals=0:0.025:0.4; rfropt.lvals=-0.5:0.25:3.5;
rfropt.chibins{4}=2:10;
[r,rfropt]=reduc_final_rlim(filename,rfropt,1);

% make the plot
clf; setwinsize(gcf,400,300)

% find the max index
llike=sum(r.likeBB,1);
[mv,j]=max(llike);
lr=llike/mv;
llo=interp1(lr(1:j),r.lvals(1:j),exp(-.5));
lhi=interp1(lr(j:end),r.lvals(j:end),exp(-.5));
plot([r.lvals(j) r.lvals(j)],[0 1],'k'); hold on;
% plot the 68% confidence interval:
plot([llo llo],[0 exp(-.5)],'k--');
plot([lhi lhi],[0 exp(-.5)],'k--');
plot(r.lvals,lr,'k')

%also plot the planck limits
line([0.95 .95],[0 1.5],'LineStyle',':','color','r'); 
line([1.05 1.05],[0 1.5],'LineStyle',':','color','r');

xlim([0 2.5]); ylim([0 1.2]);
xlabel('Lensing scale factor A_L');
ylabel('Likelihood')

% find the zero index
i=find(r.lvals==0);
% find the likelihood ratio
lr=llike(i)/llike(j);
[chi2,pte,sig]=likeratio2sigmas(lr);
disp(sprintf('L(zero)/L(max)=%.1e -> chi2=%.1f -> PTE=%.1e -> %.1fsigma',lr,chi2,pte,sig'));

% print the 1 sigma error bars
disp(['A_L = ' num2str(r.lvals(j)) ' + ' num2str(lhi) ' - ' num2str(llo)]);

print -depsc2 paper_plots/lconstraint.eps
fix_lines('paper_plots/lconstraint.eps')
!epstopdf paper_plots/lconstraint.eps

return

%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
function make_cvsi()

% call external prog to prepare the data
[dum,dum,dum, dum, filename]=get_b2k_comb;
filename = strrep(filename,'final/','');

rfropt.calc_type='cvsi';
rfropt.chibins{4}=2:6;
rfropt.rvals=0:0.0004:0.02; rfropt.lvals=-1.5:0.06:1.5;

[r,rfropt]=reduc_final_rlim(filename,rfropt,0);

% make the plot
clf; setwinsize(gcf,400,300)

xl='Power law amplitude'; yl='Power law slope';

%subplot(1,3,1);
mv=maxmd(r.likeBB);
[c,h]=contourf(r.rvals,r.lvals,r.likeBB',mv*exp(-([1,2]).^2/2));
xlabel(xl); ylabel(yl);
axis square
xlim([0,0.02]);
colormap(cool);
caxis([0 1.5]);
ch = get(h,'children');
set(ch(1),'FaceColor',[0 0 1]);
set(ch(1),'EdgeColor',[0 0 1]);
set(ch(2),'FaceColor',[0 1 1]);
set(ch(2),'EdgeColor',[0 0 1]);
set(ch(2),'LineWidth',0.45);

print -depsc2 paper_plots/cvsi.eps
fix_lines('paper_plots/cvsi.eps')
!epstopdf paper_plots/cvsi.eps

return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function make_b2k_vs_world

includeSampleVariance = 1;

% get real data points
[r,im,ml]=get_b2k_comb;

% Use new/old lensing curve
if(0)
  d1 = load('aux_data/official_cl/camb_planck2013_r0p1_ext_lensedcls.dat');
  d2 = load('aux_data/official_cl/camb_planck2013_r0p1_ext_tenscls.dat');
  lensing_curve=d1(1:2899,4);
  tensor_curve=d2(1:2899,4)*1.6;
  l=d1(:,1);
else
  lensing_curve=ml.Cs_l(:,4);
  tensor_curve=2*im.Cs_l(:,4);
  l = ml.l;
end

% add lensing+tensors, making sure they are evaluated at the same ell
total_curve=lensing_curve+tensor_curve;

clf;
setwinsize(gcf,400,300)
set(0,'defaultlinelinewidth',1.0);
set(0,'DefaultAxesFontSize',14);


loglog(l,lensing_curve,'r');
hold on
loglog(l,tensor_curve,'r--');
loglog(l,total_curve,'r--');

% Legend location
text_x=[12,35];
text_y=logspace(log10(5e0),log10(5e1),5);

if includeSampleVariance
  h=errorbar2(r.lc(2:10,4),r.real(2:10,4),std(squeeze(r(1).simr(2:10,4,:))')','k.');
else
  h=errorbar2(r.lc(2:10,4),r.real(2:10,4),r.derr(2:10,4),'k.');
end

h=herrorbar2(r.lc(2:10,4),r.real(2:10,4),r.lc(2:10,4)-r.ll(2:10,4),r.lh(2:10,4)-r.lc(2:10,4),'k.');
h=plot(r.lc(2:10,4),r.real(2:10,4),'k.','MarkerSize',12);
text(text_x(1),text_y(5),'BICEP2+Keck','Color','k')
%set(gca,'FontSize',10,'FontWeight','normal');

ms=4;
other_data=load('other_experiments/bicep1.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(4),'BICEP1','Color',[0.5,0.5,0.5])
other_data=load('other_experiments/boomerang.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(4),'Boomerang','Color',[1,0,1])
other_data=load('other_experiments/capmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(1),'CAPMAP','Color',[0.5,0,0.5])
other_data=load('other_experiments/cbi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,0]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(5),'CBI','Color',[0,1,0])
other_data=load('other_experiments/dasi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(3),'DASI','Color',[0,1,1])
other_data=load('other_experiments/quad.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,0]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(3),'QUAD','Color',[1,0,0])
other_data=load('other_experiments/quiet40.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,1]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(2),'QUIET-Q','Color',[0.5,0.5,1])
other_data=load('other_experiments/quiet90.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(1),'QUIET-W','Color',[0,0,1])
other_data=load('other_experiments/wmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,1/3,0]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(2),'WMAP','Color',[1,1/3,0])

% include the lensing measurements
if(1)
  % polarbear measurment of lensing
  other_data=load('other_experiments/polarbear.txt');
  ul=3;
  meas=[1,2,4];
  halfbw = (other_data(2,1)-other_data(1,1))/2;
  halfbw = repmat(halfbw,4,1);
  h=errorbar2(other_data(meas,1),other_data(meas,2),other_data(meas,3),'bs');
  set(h(2),'MarkerSize',ms);
  h=herrorbar2(other_data(meas,1),other_data(meas,2),halfbw(meas),halfbw(meas),'bs');
  set(h(2),'MarkerSize',ms);

  h=plot(other_data(meas,1),other_data(meas,2),'bs','MarkerSize',ms,'MarkerFaceColor','b');
  h=herrorbar2(other_data(ul,1),other_data(ul,4),halfbw(ul),halfbw(ul),'bv');
  set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');

  % SPT lensing in cross correlation with the CIB
  other_data=load('other_experiments/SptBBX.txt');
  p = other_data(:,2)/1e4.*(other_data(:,1)+1)/2/pi
  pu= other_data(:,3)/1e4.*(other_data(:,1)+1)/2/pi
  pl= other_data(:,4)/1e4.*(other_data(:,1)+1)/2/pi

  le = p-pl;
  ue = pu-p;
  le(p-le<0)=p(p-le<0)-1e-10;
  h=errorbar2(other_data(:,1),p,le,ue,'^')
  set(h(2),'MarkerSize',0.01);
  set(h(1),'Color',[0,0.7,0]);
  plot(other_data(:,1),p,'^','color',[0,0.7,0],'MarkerFaceColor',[0,0.7,0],'MarkerSize',ms)
  plot(other_data(:,1),pl,'^','color',[0,0.7,0],'MarkerFaceColor',[0,0.7,0],'MarkerSize',ms/2)
  plot(other_data(:,1),pu,'^','color',[0,0.7,0],'MarkerFaceColor',[0,0.7,0],'MarkerSize',ms/2)
  %% done
  xlim([10,3000]);
else
  xlim([10,2000]);
end

% zoomed in version
if(1)
  text_y = linspace(0.95,0.8,3)
  text_x = linspace(0.05,0.25,2)
  text(text_x(1),text_y(1),'BICEP1','Color',[0.5,0.5,0.5],'Units','normalized')
  text(text_x(2),text_y(1),'QUIET-Q','Color',[0.5,0.5,1],'Units','normalized')
  text(text_x(1),text_y(2),'WMAP','Color',[1,1/3,0],'Units','normalized')
  text(text_x(2),text_y(2),'Polarbear','Color',[0,0,1],'Units','normalized')
  text(text_x(1),text_y(3),'SPT x-corr','Color',[0,0.7,0],'Units','normalized')
  text(text_x(2),text_y(3),'BICEP2+Keck','Color','k','Units','normalized')
  set(gca,'yscale','lin')
  ylim([-0.02,0.16]);
  text(6e1,5e-3,'r=0.16','Rotation',10)
  text(380,0.032,'lensing','Rotation',55)
else
  ylim([1e-3,1e2]);
  text(2e1,3e-3,'r=0.16','Rotation',32)
  text(6e1,2e-3,'lensing','Rotation',35)
end


hold off
xlabel('Multipole')
ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')

print -depsc2 paper_plots/b2k_and_previous_limits.eps
fix_lines('paper_plots/b2k_and_previous_limits.eps')
!epstopdf paper_plots/b2k_and_previous_limits.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function make_rvsd

%Set up likeopt, mostly default values.
likeopt = get_default_likeopt;
% Choose experiments
likeopt.expt(1).name='B2_150';    likeopt.expt(1).freq = 149.8;
likeopt.expt(2).name = 'B1_100';  likeopt.expt(2).freq = 96;
likeopt.expt(3).name = 'K_150';   likeopt.expt(3).freq = 149.8;
% Get data from final .mat files.
likeopt = like_read_data(likeopt);
% Read theory spectra from CAMB files.
likeopt = like_read_theory(likeopt);
% Precalculate values for H-L likelihood.
hlpre = hamimeche_lewis_prepare(ivecp((likeopt.C_fl + likeopt.N_l)'), likeopt.bpcm);
C_l_hat = ivecp((likeopt.real + likeopt.N_l)');
% Default parameters include lensing, but no foregrounds.
p = [0, 1, 0, 0, -3, -0.6, 1.5, -0.6, 1, 1, 0];

% how about r vs dust?
p(2)=1;
rvals = [0:0.001:0.75];
dvals=0:0.0002:0.04;
for i=1:numel(rvals)
  for j=1:numel(dvals)
    p(1)=rvals(i); p(4)=dvals(j);
    expv = like_getexpvals(p, likeopt);
    logl(i,j) = hamimeche_lewis_likelihood(hlpre, C_l_hat, ivecp((expv + likeopt.N_l)'), 1);
  end
end

%and plot!
% Normalize likelihoods.
lik = exp(logl);
lik = lik / sum(lik(:));

% Make 3-panel plot
clf; setwinsize(gcf,800,250)

subplot_grid(1,3,1)
yl='tensor-to-scalar ratio r'; xl='dust amplitude [\muK^2]';
mv=maxmd(lik);
[c,h]=contourf(dvals,rvals,lik,mv*exp(-([1,2]).^2/2));
xlabel(xl); ylabel(yl);
xlim([0,0.02]); ylim([0 0.4])
colormap(cool);
caxis([0 1.5]);
ch = get(h,'children');
set(ch(1),'FaceColor',[0 0 1]); set(ch(1),'EdgeColor',[0 0 1]);
set(ch(2),'FaceColor',[0 1 1]); set(ch(2),'EdgeColor',[0 0 1]);
set(ch(2),'LineWidth',0.45);
axis square

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% second panel
% marginalize over d
rlike=sum(lik,2);
[mv,j]=max(rlike);
lr=rlike/mv;

% find the zero index
i=find(rvals==0);
% find the likelihood ratio
[chi2,pte,sig]=likeratio2sigmas(lr(i));
disp(sprintf('L(zero)/L(max)=%.1e -> chi2=%.1f -> PTE=%.1e -> %.1fsigma',lr(i),chi2,pte,sig'));

% plot the curve
subplot_grid(1,3,2)
box on; hold on;
plot(rvals,lr,'k');
llo=interp1(lr(1:j),rvals(1:j),exp(-.5));
lhi=interp1(lr(j:end),rvals(j:end),exp(-.5));
plot([rvals(j) rvals(j)],[0 1],'k'); hold on;
plot([llo llo],[0 exp(-.5)],'k--');
plot([lhi lhi],[0 exp(-.5)],'k--');
axis square
xlabel(yl); ylabel('Likelihood')
xlim([0 .4]); ylim([0 1.2]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% third panel
% marginalize over r
dlike=sum(lik,1);
[mv,j]=max(dlike);
lr=dlike/mv;

% find the zero index
i=find(dvals==0);
% find the likelihood ratio
[chi2,pte,sig]=likeratio2sigmas(lr(i));
disp(sprintf('L(zero)/L(max)=%.1e -> chi2=%.1f -> PTE=%.1e -> %.1fsigma',lr(i),chi2,pte,sig'));

% plot the curve
subplot_grid(1,3,3)
box on; hold on;
plot(dvals,lr,'k');
llo=interp1(lr(1:j),dvals(1:j),exp(-.5));
lhi=interp1(lr(j:end),dvals(j:end),exp(-.5));
plot([dvals(j) dvals(j)],[0 1],'k'); hold on;
plot([llo llo],[0 exp(-.5)],'k--');
plot([lhi lhi],[0 exp(-.5)],'k--');
axis square
xlabel(xl); ylabel('Likelihood')
xlim([0 .02]); ylim([0 1.2]);

% print
print -depsc2 paper_plots/rvsd.eps
fix_lines('paper_plots/rvsd.eps')
!epstopdf paper_plots/rvsd.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_sens
% plots the 3-panel livetime, instantaneous sens, and cum map depth

% Load the observing fraction
load(['/n/bicepfs2/keck/pipeline/data/real/livetime_1213.mat']);
% modify the last number to go to 0
source_time{2}(end)=0;

% load the instantaneous sensitivity
a=load(['/n/bicepfs2/keck/pipeline/data/real/k12_net_perscanset.mat']);
b=load(['/n/bicepfs2/keck/pipeline/data/real/k13_net_perscanset.mat']);
%concatenate
cmbtags={a.cmbtags{:} b.cmbtags{:}};
net=[a.net' b.net'];
%a little more complicated...
[p ind]=get_array_info('20120505');
[p ind2013]=get_array_info('20130505');
npp(:,ind.rglb)=a.net_per_pair;  npp2013(:,ind2013.rglb)=b.net_per_pair; npp2013(:,2640)=NaN;
net_per_pair=[npp' npp2013']';
%remove 0s
net_per_pair(net_per_pair==0)=NaN;
%find union of ind.rglb...
rglb=union(ind.rglb,ind2013.rglb);
%par down to that...
net_per_pair=net_per_pair(:,rglb);

%find the time
for ii=1:length(cmbtags)
  ttime(ii)=datenum(cmbtags{ii}(1:8),'yyyymmdd');
end

%average the NET over ~10
net(isinf(net))=NaN;
for ii=1:floor(length(net)*.1)
  indx=(ii-1)*10+1:ii*10;
  net_pp(ii)=nanmean(net(indx).^(-2))^(-.5);
  ptime(ii)=ttime(indx(1));
end

% make years
for yr=2012:2014
  yeartime(yr-2011)=datenum(yr,0,0);
end
xticks=[datenum(2012,7,0),datenum(2013,7,0)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load the cumulative map depth
load(['/n/panlfs2/bicep/keck/pipeline/maps/1325/k1213_cum_depth.mat']);
% convert from phase to time
for ii=1:length(ph.name)
  phtime(ii)=datenum(ph.name{ii}(1:8),'yyyymmdd');
end
% total depth is inverse sum of the Q and U
depth=sqrt(2)*(Qd.^(-2)+Ud.^(-2)).^-.5;
depth100=sqrt(2)*(Qd100.^(-2)+Ud100.^(-2)).^-.5;
% area is the average of Q and U
area=(Qa+Ua)./2;
area100=(Qa100+Ua100)./2;
% total map depth is depth x area
sens=depth./sqrt(area)/sqrt(2);
sens100=depth100./sqrt(area100)/sqrt(2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up some axes now so we can do fiddly things with them later
clf;
h=gcf;
setwinsize(gcf,900,600);
set(gcf,'DefaultAxesFontSize',11)
set(0,'defaultlinelinewidth',1.0);
dax=0.07;
sax=0.1;
for i=1:3
  subplot(3,1,i);
  ax(i)=gca;
  pp=get(ax(i),'position');
  set(ax(i),'position',[pp(1)+dax pp(2) pp(3)-dax pp(4)]);
end
pp=get(ax(3),'position');
ax(4)=axes('position',[sax pp(2) 0.01 pp(4)]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55555
% plot the live time
axes(ax(1));
plot(tt,source_time{2}./source_time{1}*100); hold on
plot(tt,source_time{7}./source_time{1}*100,'r');
title('Livetime per calendar time')
% mark the years
plot([yeartime(2),yeartime(2)],[0 100],'--k');
xlim([yeartime(1) yeartime(3)]);
set(gca,'XTick',xticks,'TickLength',[0,0]);
datetick('x','YYYY','keeplimits','keepticks'); ylabel('Fraction [%]')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%inst sens
axes(ax(2));
plot(ptime,net_pp,'.'); hold on;
% mark the years
plot([yeartime(2),yeartime(2)],[0 100],'--k');
xlim([yeartime(1) yeartime(3)]);
set(gca,'XTick',xticks,'TickLength',[0,0]);
title('Instantaneous sensitivity')
datetick('x','YYYY','keeplimits','keepticks');
ylabel('NET [\muK sqrt s]');
ylim([0 30]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%cum depth
axes(ax(3));
semilogy(phtime,depth*1e3,'.'); hold on
title('Cumulative map depth')
% mark the years
plot([yeartime(2),yeartime(2)],[10 1000],'--k');
xlim([yeartime(1) yeartime(3)]);
set(gca,'XTick',xticks,'TickLength',[0,0]);
datetick('x','YYYY','keeplimits','keepticks');
ylim([30 5e2]);
ylabel('Map Depth [nK deg]');
xlabel('Time')

% Extra left ticks for bottom panel
yl=ylim;
ts=get(gca,'ticklength');
uselog=true;
axes(ax(4));
set(ax(4),'yscale','log');
ylim(yl*1e0/sqrt(area(end))/sqrt(2));
ylabel('Total sensitivity [nK]');
box off;
set(ax(4),'xtick',[]);
if uselog
  yl=ylim;
  % yt=get(ax(4),'ytick');
  yt=[2 10 50];
  ytl={};
  for i=1:length(yt)
    ytl{i}=num2str(yt(i));
  end
  set(ax(4),'ytick',yt);
  set(ax(4),'yticklabel',ytl);
  set(ax(4),'yminortick','off');
  set(ax(4),'ticklength',ts*0);
  bigt=[10];
  xl=xlim;
  for onyt=[1:10 20:10:100]
    if onyt<yl(1) || onyt>yl(2)
      continue
    end
    if ismember(onyt,bigt)
      line(xl,onyt*[1 1],'color','k');
    else
      line(xl/2,onyt*[1 1],'color','k');
    end
  end
end
% set(ax(4),'ticklength',ts*10);
yl=ylim;
line(xlim,yl(2)*[1 1],'color','k','linewidth',1); % get(ax(4),'linewidth'));


% print
print -depsc2 paper_plots/keck1213_depth_sens_frac.eps
fix_lines('paper_plots/keck1213_depth_sens_frac.eps')
!epstopdf paper_plots/keck1213_depth_sens_frac.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function make_beammapsimsplot()
% Load beammap sim spectra and appropriate supfac
%  dp={'0000','0100','1100','1110','1102','11022','1111'};
dp={'0000','0100','1100','1110','1102','1111'};

% Get the beam map sim algorithmic floor from noiseless constructed Planck sky dp1111
% beam map sim
floor=load('/n/bicepfs1/users/clwong/keck_aps/2052/0002_both_filtp3_weight3_gs_dp1111_jack0_pureB_overrx.mat');

% Get the supfac from dp1100 as the others don't exist for keck
load('final/1351/real_a_filtp3_weight3_gs_dp1100_jack01_real_b_filtp3_weight3_gs_dp1100_jack01_pureB_overrx_overall.mat');
rwf1100=r(1).rwf;

% This is the algorithmic floor to remove
floor.aps.Cs_l=floor.aps.Cs_l.*rwf1100;

for k=1:numel(dp)

  % Beam map sim spectrum
  if ~strcmp(dp{k},'1102')
    x=load(sprintf('/n/bicepfs1/users/clwong/keck_aps/2092/0002_both_filtp3_weight3_gs_dp%s_jack0_pureB_overrx.mat',dp{k}));
  else
    x=load(sprintf('/n/bicepfs2/keck/pipeline/aps/2505/0002_cd_filtp3_weight3_gs_dp11020_jack0_pureB_overrx.mat'));
  end
  aps(k).Cs_l=x.aps.Cs_l.*r.rwf;
  aps(k).l=x.aps.l;
  
  % Remove noise bias
  %% (bias not yet calculated for Keck)
  %dpb=dp{k};
  %if strcmp(dpb,'0100') | strcmp(dpb,'1110');
  %  dpb='1100'; % This is dumb, but it makes no difference.
  %end
  %%dpb = '1101'; % always just remove the same dp1101 bias.
  %load(sprintf('~csheehy/bicep2_analysis/bias_for_chinlin/beammap_bias_and_sigma_dp%s',dpb));
  %aps(k).Cs_l(:,4)=aps(k).Cs_l(:,4)-bias;

  % Remove beam map floor
  aps(k).Cs_l(:,4)=aps(k).Cs_l(:,4)-floor.aps.Cs_l(:,4);
  
end

[rs,im,ml,dum,rf,mt,rscaling]=get_kxk;
rf = strrep(rf,'final/','');
rscaling = strrep(rscaling,'aps/','');

clf; setwinsize(gcf,400,600/5*3)

subplot(3,1,[1:3])
a1=gca;
% Plot input model
h1=semilogy(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
hold on
%plot(ml.l,ml.Cs_l(:,4),'r');

xlim([0,330]); 
ylim([1e-4,1]);
l=aps(1).l; l(1)=NaN;

lw=1;
h2=semilogy(l,aps(1).Cs_l(:,4),'g','LineWidth',lw);
h3=semilogy(l,aps(2).Cs_l(:,4),'c','LineWidth',lw);
h4=semilogy(l,aps(3).Cs_l(:,4),'m','LineWidth',lw);
h5=semilogy(l,aps(4).Cs_l(:,4),'b--','LineWidth',lw);
h6=semilogy(l,aps(5).Cs_l(:,4),'kv-','LineWidth',lw,'MarkerSize',4,'MarkerFaceColor','k');
%  h7=semilogy(l,aps(6).Cs_l(:,4),'color',[0.13,0.54,0.13],'LineWidth',lw);

hold off

ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]');

%  h=legend([h1,h2,h3,h4,h5,h6,h7],'lensed-\LambdaCDM+r=0.2',...
%      'no deprojection',...
%      'dp','dp+dg','dp+dg+bw','dp+dg+ellip.',...
%      'dp+dg+ellip+beamres','Location','NorthWest');
h=legend([h1,h2,h3,h4,h5,h6],'lensed-\LambdaCDM+r=0.2',...
         'no deprojection',...
         '\deltax + \deltay',...
         '\deltax + \deltay + \deltag',...
         '\deltax + \deltay + \deltag + \delta\sigma',...
         '\deltax + \deltay + \deltag + \deltap + \deltac',...
         'Location','NorthWest');
legend boxoff

%  subplot(5,1,4:5); a2=gca;
%  xlim([0,330]); %ylim([1e-7,1]);
%  ylim([-12,5]);
%  lw=1;
%  hold on
%  h1=semilogy(im.l,im.Cs_l(:,5),'r--');
%  h2=plot(l,aps(1).Cs_l(:,5),'g','LineWidth',lw);
%  h3=plot(l,aps(2).Cs_l(:,5),'c','LineWidth',lw);
%  h4=plot(l,aps(3).Cs_l(:,5),'m','LineWidth',lw);
%  h5=plot(l,aps(4).Cs_l(:,5),'b--','LineWidth',lw);
%  h6=plot(l,aps(5).Cs_l(:,5),'k','LineWidth',lw);
%  %  h7=plot(l,aps(6).Cs_l(:,5),'color',[0.13,0.54,0.13],'LineWidth',lw);

xlabel('Multipole');
%  ylabel('l(l+1)C_l^{TB}/2\pi [\muK^2]');

%  make_axis_pair(a1,a2);

% fix the ylabel going off the side...
p1=get(a1,'Position'); 
%  p2=get(a2,'Position');
p1(1)=.2; p1(3)=0.905-p1(1); 
%  p2(1)=p1(1); p2(3)=p1(3);
set(a1,'Position',p1); 
%  set(a2,'Position',p2);
box on

set(gcf,'PaperPositionMode','auto')
print -depsc2 paper_plots/beamsim.eps
fix_lines('paper_plots/beamsim.eps')
!epstopdf paper_plots/beamsim.eps

keyboard

% get the standard r likelihood
rfropt.rscaling=rscaling;
rfropt.rvals=0.1:.0002:0.3;
[r1,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

% put in undeproj resid debias and do it again
rfropt.fgmod=aps(6);
[r2,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

% print the delta
disp(sprintf('change in max like r value when debias undeproj resid=%f',r1.rmaxlike-r2.rmaxlike));

return


%%%%%%%%%%%%
function paper_print_generic_header(fileID)
  %Use this function in every data release file generating script at the top
  fprintf(fileID, '# BICEP2/Keck Array January 2015 Data Products\n');
  fprintf(fileID, '# The Keck Array and BICEP2 Collaborations, BK-V: Measurements of B-mode Polarization at Degree Angular Scales and 150 GHz by Keck Array\n');
  fprintf(fileID, '# http://bicepkeck.org/\n#\n');
return



%%%%%%%%%%%%%%%%%%%%%
function make_powspecrestable

  for i=1:6
    chibins{i}=[2:10]; % use all plotted bins
  end
  [r,im,ml]=get_kxk;
  r=calc_chi(r,chibins,[],1);

  filename = 'K_bandpowers_20141223.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);

  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2014-12-23 \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Keck Array bandpowers \n');
  fprintf(fh,'# This text file contains the Keck Array TT, TE, EE, BB, TB, and EB bandpowers [ell*(ell+1)*C_{ell}/(2*pi)] and uncertainties. \n');
  fprintf(fh,'# They correspond to Figure 4 of Keck Array/BICEP2 2015 V.\n');
  fprintf(fh,'# Uncertainties are statistical only, the standard deviation of the constrained lensed-ΛCDM+noise simulations. \n');
  fprintf(fh,'# Uncertainties are calculated as the square root of diagonal elements of the bandpower covariance matrix. \n');
  fprintf(fh,'# The nature of the simulations constrains T to match the observed sky. Thus TT, TE, and TB uncertainties do not include appropriate sample variance.\n');
  fprintf(fh, '# Sample variance for a tensor BB signal is not included either.\n');
  fprintf(fh,'# The calibration procedure uses TB and EB to constrain the polarization angle. Thus TB and EB cannot be used to measure astrophysical polarization rotation.\n');
  fprintf(fh,'# The units are uK^2. \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, TB, EB, dTT, dTE, dEE, dBB, dTB, dEB \n');

  for i = chibins{1}
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,['%3d  %3.1f  %3d   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   ' ...
    '%1.2e \n'],lmin, r.lc(i), lmax,r.real(i,:),r.derr(i,:));
  end

  fclose(fh);

  filename = 'K_bandpowers_withr_20141230.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);

  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2014-12-30 \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Keck Array bandpowers \n');
  fprintf(fh,'# This text file contains the Keck Array TT, TE, EE, BB, TB, and EB bandpowers [ell*(ell+1)*C_{ell}/(2*pi)] and uncertainties. \n');
  fprintf(fh,'# Uncertainties are statistical only, the standard deviation of the constrained lensed-ΛCDM+noise+r=0.2 simulations. \n');
  fprintf(fh,'# Uncertainties are calculated as the square root of diagonal elements of the bandpower covariance matrix. \n');
  fprintf(fh,'# The nature of the simulations constrains T to match the observed sky. Thus TT, TE, and TB uncertainties do not include appropriate sample variance.\n');
  fprintf(fh, '# For BB (and EB) the sample variance of an r=0.2 tensor signal is included.\n');
  fprintf(fh,'# The calibration procedure uses TB and EB to constrain the polarization angle. Thus TB and EB cannot be used to measure astrophysical polarization rotation.\n');
  fprintf(fh,'# These values are for plotting purposes only. For cosmological parameter analysis we provide a bandpower likelihood at http://bicepkeck.org/. The likelihood is also available in cosmological parameter fitting software (e.g. CosmoMC).\n');
  fprintf(fh,'# The units are uK^2. \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, TB, EB, dTT, dTE, dEE, dBB, dTB, dEB \n');

  for i = chibins{1}
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,['%3d  %3.1f  %3d   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   ' ...
    '%1.2e \n'],lmin, r.lc(i), lmax,r.real(i,:), std(r.simr(i,:,:), [], 3)  );
  end
  fclose(fh);

  %B2+Keck combined results
  [r,im,ml]=get_b2k_comb();
  r=calc_chi(r,chibins,[],1);

  filename = 'BK_bandpowers_20150130.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2015-01-30\n');
  fprintf(fh,'# \n');
  fprintf(fh,'# BICEP2/Keck Array combined bandpowers \n');
  fprintf(fh,'# This text file contains the BICEP2/Keck Array combined TT, TE, EE, BB, TB, and EB bandpowers [ell*(ell+1)*C_{ell}/(2*pi)] and uncertainties. \n');
  fprintf(fh,'# They correspond to Figure 9 of BK-V and the left column of A Joint Analysis of BICEP2/Keck Array and Planck Data.\n');
  fprintf(fh,'# Uncertainties are statistical only, the standard deviation of the constrained lensed-ΛCDM+noise simulations. \n');
  fprintf(fh,'# Uncertainties are calculated as the square root of diagonal elements of the bandpower covariance matrix. \n');
  fprintf(fh,'# The nature of the simulations constrains T to match the observed sky. Thus TT, TE, and TB uncertainties do not include appropriate sample variance.\n');
  fprintf(fh, '# Sample variance for a tensor BB signal is not included either.\n');
  fprintf(fh,'# The calibration procedure uses TB and EB to constrain the polarization angle. Thus TB and EB cannot be used to measure astrophysical polarization rotation.\n');
  fprintf(fh,'# The units are uK^2. \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, TB, EB, dTT, dTE, dEE, dBB, dTB, dEB \n');
  for i = chibins{1}
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,['%3d  %3.1f  %3d   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   ' ...
    '%1.2e \n'],lmin, r.lc(i), lmax,r.real(i,:),r.derr(i,:));
  end
  fclose(fh);

  filename = 'BK_bandpowers_sample_20150129.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2015-01-29\n');
  fprintf(fh,'# \n');
  fprintf(fh,'# BICEP2/Keck Array combined bandpowers \n');
  fprintf(fh,'# This text file contains the BICEP2/Keck Array combined TT, TE, EE, BB, TB, and EB bandpowers [ell*(ell+1)*C_{ell}/(2*pi)] and uncertainties. \n');
  fprintf(fh,'# Uncertainties are statistical only, including sample variance consistent with the signal we observe. \n');
  fprintf(fh,'# Uncertainties are calculated as the square root of diagonal elements of the bandpower covariance matrix. \n');
  fprintf(fh,'# The nature of the simulations constrains T to match the observed sky. Thus TT, TE, and TB uncertainties do not include appropriate sample variance.\n');
  fprintf(fh,'# The calibration procedure uses TB and EB to constrain the polarization angle. Thus TB and EB cannot be used to measure astrophysical polarization rotation.\n');
  fprintf(fh,'# These values are for plotting purposes only. For cosmological parameter analysis we provide a bandpower likelihood at http://bicepkeck.org/. The likelihood is also available in cosmological parameter fitting software (e.g. CosmoMC).\n');
  fprintf(fh,'# The units are uK^2. \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, TB, EB, dTT, dTE, dEE, dBB, dTB, dEB \n');
  for i = chibins{1}
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,['%3d  %3.1f  %3d   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   ' ...
    '%1.2e \n'],lmin, r.lc(i), lmax,r.real(i,:), std(r.simr(i,:,:), [], 3)  );
  end
  fclose(fh);



return

%%%%%%%%%%%%%%%%%%%%%
function make_bpwftable
  [r,im,ml, bpwf]=get_b2k_comb();

  mkdir('windows');
  for i=2:10
    filename = sprintf('BK_bpwf_bin%d_20150129.txt',i-1);
    fh = fopen(['windows/' filename],'w') ;
    paper_print_generic_header(fh);
    fprintf(fh,'# File: ');
    fprintf(fh, filename);
    fprintf(fh, ' \n');
    fprintf(fh,'# Date: 2015-01-29\n');
    fprintf(fh,'# \n');
    fprintf(fh,'# BICEP2/Keck Array combined bandpower window functions for bin #%d (ell~%5.1f)\n',i-1, r.lc(i)) ;
    fprintf(fh,'# See Section 6.3 of BK-I.\n');
    fprintf(fh,'# \n');
    fprintf(fh,'# Columns: ');
    fprintf(fh,'ell   TT->TT     TE->TE     EE->EE     BB->BB \n');
    for j =  1:size(bpwf.l,2)
      fprintf(fh,'%3d   %1.2e   %1.2e   %1.2e   %1.2e\n',bpwf.l(j),bpwf.Cs_l(j,i,1:4));
    end
    fclose(fh);
  end
return

%%%%%%%%%%%%%%%%%%%%%
function make_hlproductfiles
  [r,im,ml, bpwf]=get_b2k_comb();
  ellbins = 2:10;
  %Default offdiagonal # in bpcm for B2 is 1 -- IDB 2014-02-26
  p = hl_create_data_products(r, ellbins, 'TEB', 1);

  %nan out T information
  p.C_fl(1,:,:) = nan;
  p.C_fl(:,1,:) = nan;
  p.C_l_hat(1,:,:) = nan;
  p.C_l_hat(:,1,:) = nan;
  p.N_l(1,:,:) = nan;
  p.N_l(:,1,:) = nan;

  format shortE
  filename = 'BK_likelihood_bandpowers_20150129.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2015-01-29\n');
  fprintf(fh,'# \n');
  fprintf(fh,'# BICEP2/Keck Array combined actual and simulated bandpowers necessary for Hamimeche-Lewis bandpower likelihood approximation.\n');
  fprintf(fh,'# See Section 9.1 of Barkats et al. (2014) for details.\n');
  fprintf(fh, '#\n');
  fprintf(fh, '# Bandpowers are organized into one matrix per ell bin.\n');
  fprintf(fh, '# Each matrix is symmetric 3x3:\n');
  fprintf(fh, '#         | TT  TE  TB |\n');
  fprintf(fh, '#         | TE  EE  EB |\n');
  fprintf(fh, '#         | TB  EB  BB |\n');
  fprintf(fh, '# However, for this release bandpowers TT, TE, and TB are not available because our constrained T simulations do not include appropriate sample variance. See Section 5.1.1 of BK-I for more details.\n');
  fprintf(fh, '# Note also that our polarization angle calibration removes any polarization rotation from the EB spectrum.\n');
  fprintf(fh, '# In all cases, bandpowers represent ell*(ell+1)C_{ell}/(2*pi).\n');
  fprintf(fh, '# Units are uK^2 CMB.\n\n');

  fprintf(fh, '# 1.\n# Fiducial model bandpowers D^f\n');
  fprintf(fh, '# Calculated as the ensemble average of signal+noise simulations.\n');
  fprintf(fh, '# Noise bias is included.\n');
  for iBin = 1:length(ellbins)
    fprintf(fh, ['\n# Bin ' num2str((iBin)) ' (ell~' sprintf('%1.1f', r.lc(ellbins(iBin))) ')\n']);
    fprintf(fh, '   %1.4e   %1.4e   %1.4e\n', (p.C_fl(:,:,iBin)));
  end

  fprintf(fh, '\n# 2.\n');
  fprintf(fh, '# Data bandpowers \\hat{D}\n');
  fprintf(fh, '# Real data bandpowers from BICEP2/Keck Array combined result.\n');
  fprintf(fh, '# These differ from bandpowers displayed in Figure 9 of BK-V, because noise bias is included here.\n');
  for iBin = 1:length(ellbins)
    fprintf(fh, ['\n# Bin ' num2str((iBin)) ' (ell~' sprintf('%1.1f', r.lc(ellbins(iBin))) ')\n']);
    fprintf(fh, '   %1.4e   %1.4e   %1.4e\n', (p.C_l_hat(:,:,iBin)));
  end

  fprintf(fh, '\n# 3.\n');
  fprintf(fh, '# Noise bandpowers N, including instrumental noise and E->B leakage.\n');
  fprintf(fh, '# Calculated from the ensemble average of noise-only simulations (and signal-only simulations for E->B leakage).\n');
  fprintf(fh, '# Subtracting these bandpowers from the real data bandpowers listed above will recover the BICEP2/Keck Array combined bandpowers from Figure 9 of BK-V.\n');
  for iBin = 1:length(ellbins)
    fprintf(fh, ['\n# Bin ' num2str((iBin)) ' (ell~' sprintf('%1.1f', r.lc(ellbins(iBin))) ')\n']);
    fprintf(fh, '   %1.4e   %1.4e   %1.4e\n', (p.N_l(:,:,iBin)));
  end

  fclose(fh);


  filename = 'BK_bpcm_no-sysuncer_20150129.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2015-01-29\n');
  fprintf(fh,'# \n');
  fprintf(fh,['# Bandpower covariance matrix M_{cc''}\n']);
  fprintf(fh,'# Covariance for bandpowers expressed as ell*(ell+1)C_{ell}/(2*pi).\n');
  fprintf(fh,'# Units are uK^4 CMB.\n');
  fprintf(fh,'# Statistical uncertainties only. \n');
  fprintf(fh,'#\n');
  fprintf(fh,'# Symmetric 54x54 matrix. \n');
  fprintf(fh,'# Ordering of the columns/rows is (TT, EE, BB, TE, EB, TB) for the first ell bin, then (TT, EE, BB, TE, EB, TB) for the second ell bin, etc.\n');
  fprintf(fh, '# However, for this data release we do not include appropriate sample variance for TT, TE, and TB because the simulations constrain T to match the observed sky. You should not use these spectra for constraining cosmology.\n\n');
  fprintf(fh, '%5.3e  %5.3e  %5.3e  %5.3e  %5.3e  %5.3e\n', p.M_f);
  fclose(fh);


return


function make_inputspectrafiles
  [r,im,ml, bpwf]=get_b2k_comb();
  ellbins = 2:10;

  filename = 'BK_camb_planck_lensed_uK_20150129.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2015-01-29 \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# This file contains the LCDM power spectra that are derived from Planck (2013) parameters and\n');
  fprintf(fh,'# used for data vs. model comparisons in the pipeline and in the paper. These spectra are not fits\n');
  fprintf(fh,'# to the Keck Array/BICEP data. The spectra are in [ell*(ell+1)*C_{ell}/(2*pi)] in units of uK^2.\n');
  fprintf(fh, '# These spectra include the effect of gravitational lensing.\n');
  fprintf(fh, '# No tensor modes are included.\n');
  fprintf(fh, '# Note that the ordering of the spectra is different from the standard CAMB output:\n');
  fprintf(fh,'# Columns: l, TT, TE, EE, BB, TB, EB\n');
  for i = 1:1901
    fprintf(fh,'%3d   %1.6e   %1.6e   %1.6e   %1.6e   %1.6e   %1.6e  \n',ml.l(i), ml.Cs_l(i,1:6));
  end
  fclose(fh);

  filename = 'BK_cl_expected_lensed_20150129.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2015-01-29\n');
  fprintf(fh,'# \n');
  fprintf(fh,'# This file contains the expected bandpowers for BICEP2/Keck Array (combined) associated to the LCDM power spectra that are derived from Planck (2013) parameters and\n');
  fprintf(fh,'# used for data vs. model comparisons in the pipeline and in the paper. These spectra are not fits\n');
  fprintf(fh,'# to the Keck Array/BICEP data. The spectra are in [ell*(ell+1)*C_{ell}/(2*pi)] in units of uK^2.\n');
  fprintf(fh, '# These spectra include the effect of gravitational lensing.\n');
  fprintf(fh, '# No tensor modes are included.\n');
  fprintf(fh, '# These expected bandpowers were computed by multiplying the power spectra by the bandpower window functions.\n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, TB, EB\n');
  r = calc_expvals(r, ml, bpwf);
  for i = ellbins
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,'%3d   %1.1f   %3d   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e \n',lmin, r.lc(i), lmax, r.expv(i,:));
  end
  fclose(fh);


return
