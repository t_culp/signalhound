clear

% load data
data_directory = 'sample_data/long_turns/short_turns/';
data_file_indices = 4; % can be an array (i.e. 4:10) if analyzing multiple files
data_files = dir(data_directory);
motor_indices = 1; % can be an array (i.e. 1:4) if analyzing a system with multiple (i.e. 4) HWPs

% load a data file to get the motor names
data_filename = data_files(data_file_indices(1)).name(1:(end-4));
load([data_directory data_filename]);

% load template
template_filename = '15-Jun-2011-14-12-53'; % long turn
template_directory = 'sample_data/long_turns/';

for i_motor = motor_indices
    template = load([template_directory template_filename '_motor_' num2str(i_motor) '_' out.motor(i_motor).motor_name '_processed.txt']);
    template_angle{i_motor} = template(:,1);
    template_abs{i_motor} = template(:,2);
    template_half_double{i_motor} = template(:,3);
    template_half_single{i_motor} = template(:,4);
    
    clear template;
end

for i_files = data_file_indices
    % load this data file
    data_filename = data_files(i_files).name(1:(end-4));
    load([data_directory data_filename]);
        
    for i_motor = motor_indices;
        % downsample to ~150 Hz sampling
        n_down = floor(out.sample_rate/150);
        data_time = out.time(1:n_down:end);
        data_shaft = out.motor(i_motor).shaft(1:n_down:end);
        data_half_double = out.motor(i_motor).half_double(1:n_down:end);
        data_half_single = out.motor(i_motor).half_single(1:n_down:end);
        data_abs = out.motor(i_motor).abs(1:n_down:end);
        
        f_sample = out.sample_rate / n_down;
        if isfield(out.motor(i),'ring_gear_teeth')
            hwp_speed = (out.motor(i_motor).speed / 200)*(360/out.motor(i).ring_gear_teeth);
        else
            hwp_speed = (out.motor(i_motor).speed / 200)*(360/465)
        end
        first_turning = -1;
        last_turning = -1;
        this_motor_string = out.motor(i_motor).motor_name;
        
        tmp_angle = template_angle{i_motor};
        tmp_double = template_half_double{i_motor};
        tmp_single = template_half_single{i_motor};
        tmp_abs = template_abs{i_motor};
        
        % pass this to the angle reconstruction C code
        [turn_params model_half_double model_half_single model_abs final_angle] = reconstruct_hwp_angle_wrapper( ...
            tmp_angle, tmp_double, tmp_single, tmp_abs, ...
            data_shaft, data_half_double, data_half_single, data_abs, ...
            hwp_speed, f_sample);
        
        disp(['Turned ' num2str(final_angle - turn_params(5)) ' degrees'])
        
        %% make a plot of the results
        figure(1)
        clf
        
        subplot(3,1,1)
        plot(data_time,data_half_double,data_time,model_half_double);
        ylabel('Double Encoder [V]')
        title(data_filename)
        
        subplot(3,1,2)
        plot(data_time,data_half_single,data_time,model_half_single);
        ylabel('Single Encoder [V]')
        
        subplot(3,1,3)
        plot(data_time,data_abs,data_time,model_abs);
        ylabel('Absolute Encoder [V]')
        legend('Data','Model','Location','SouthEast')
        xlabel('Time [s]')
        
        %% make a struct to save the turn reconstruction parameters
        turn(i_motor).start_time = turn_params(1);
        turn(i_motor).stop_time = turn_params(3);
        turn(i_motor).spinup_time = turn_params(2);
        turn(i_motor).speed = turn_params(4);
        turn(i_motor).start_angle = turn_params(5);
        turn(i_motor).stop_angle = final_angle;
        turn(i_motor).encoder_signal_amplitude = turn_params(6);
        
        turn(i_motor).model.abs = model_abs;
        turn(i_motor).model.half_double = model_half_double;
        turn(i_motor).model.half_single = model_half_single;
        
        ang_noise_double = (std(data_half_double(1:100))/(max(data_half_double) - min(data_half_double)))*.25;
        ang_noise_single = (std(data_half_single(1:100))/(max(data_half_single) - min(data_half_single)))*.25;
        turn(i_motor).expected_angle_uncertainty = sqrt(1/((1/ang_noise_double)^2 + (1/ang_noise_single)^2));
        
        save([data_directory 'reconstructed_turns/' data_filename '_motor_' num2str(i_motor) '_' this_motor_string '_turn_params.mat'],'turn');
    end
end