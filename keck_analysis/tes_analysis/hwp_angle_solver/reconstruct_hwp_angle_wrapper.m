function [turn_params model_half_double model_half_single model_abs final_angle] = reconstruct_hwp_angle_wrapper(template_angle, template_half_double, template_half_single, template_abs, data_shaft, data_half_double, data_half_single, data_abs, hwp_speed, f_sample)


[turn_params model_half_double model_half_single model_abs final_angle] = ...
              reconstruct_hwp_angle([template_angle - 360 ; template_angle ; template_angle + 360]', ...
                                    [template_half_double ; template_half_double ; template_half_double]', ...
                                    [template_half_single ; template_half_single ; template_half_single]', ... 
                                    [template_abs ; template_abs ; template_abs]', ...
                                    data_half_double', data_half_single', data_abs', hwp_speed, f_sample);



end

