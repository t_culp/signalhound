#include "mex.h"
#include <math.h>

double reconstruct_hwp_angle(double *, double *, double *, double *, int ,
        double *, double *, double *, int ,
        double , double, double *, double *, double *, double *);
double logp(double *,int,double *, double *, double *, double *, int,
           double *, double *, double *, int,
           double, double, int, double *, double *, double *);

/* --- "slow" max and min finder for doubles --- */
/* http://ndevilla.free.fr/median/median.pdf */
int compare(const void *, const void *);
double max_double(double *, int);
double min_double(double *, int);

/* bsearch comparison function for nearest-neighbor */
/* http://bytes.com/topic/c/answers/222361-search-nearest-item-bsearch */
int search_compare(const void *, const void *);

/* linear fit no error bars */
void linfit(double *, int, int, double *); 

double reconstruct_hwp_angle(double *template_angle, double *template_half_double, double *template_half_single, double *template_abs, mwSize n_template,
        double *data_half_double, double *data_half_single, double *data_abs, mwSize n_data,
        double hwp_speed_in_dps, double f_sample_in_hz, double *params_out, double *model_half_double, double *model_half_single, double *model_abs) {
    
    mwSize i,j,k,l;
    
    int first_turning = -1;
    int last_turning  = -1;
    
    double i_double = 0;
     
    int int_f_sample = (int) ceil(f_sample_in_hz);
    // make sure ring encoder ticks are at least 75% of the expected number of samples apart
    // to count as real ticks
    int ring_encoder_reject_threshold = ( int ) ceil( 0.75 * (f_sample_in_hz/(hwp_speed_in_dps*4)) );
    mexPrintf("Minimum %d samples make a Ring Encoder tick\n", ring_encoder_reject_threshold);
    mexEvalString("drawnow;");
    
    /* find the start point*/
    // get the slope and intercept of the starting trend (slope should be pretty small...)
    double double_a[2];
    linfit(data_half_double,int_f_sample,int_f_sample*4,double_a);
    double single_a[2];
    linfit(data_half_single,int_f_sample,int_f_sample*4,single_a);
    
    // find the noise
    double double_noise_start = 0;
    double single_noise_start = 0;
    for(i=int_f_sample;i<(int_f_sample*4);i++) {
        double_noise_start += (data_half_double[i] - (double_a[0] + double_a[1]*((double) i))) * 
                              (data_half_double[i] - (double_a[0] + double_a[1]*((double) i)));
        single_noise_start += (data_half_single[i] - (single_a[0] + single_a[1]*((double) i))) * 
                              (data_half_single[i] - (single_a[0] + single_a[1]*((double) i)));
    }
    double_noise_start = sqrt(double_noise_start / (f_sample_in_hz*3 - 1));
    single_noise_start = sqrt(single_noise_start / (f_sample_in_hz*3 - 1));
    
    // find the first ~10-sigma point
    int nominal_first_turning = 0;
    double double_enc_threshold = (max_double(data_half_double,n_data)-min_double(data_half_double,n_data))/5;
    double single_enc_threshold = (max_double(data_half_single,n_data)-min_double(data_half_single,n_data))/5;
    
    for(i=0;i<n_data;i++) {
        if((fabs(data_half_double[i] - (double_a[0] + double_a[1]*((double) i))) > double_enc_threshold) || 
           (fabs(data_half_single[i] - (single_a[0] + single_a[1]*((double) i))) > single_enc_threshold)) {
            nominal_first_turning = i;
            break;
        }
    }
    
    // head backwards until you get to the mean level
    // (i.e. the difference between the current point and the mean level gets small)
    first_turning = nominal_first_turning;
    for(i=nominal_first_turning;i>(nominal_first_turning - 3*ring_encoder_reject_threshold);i--) {
        if((fabs((data_half_double[i] - (double_a[0] + double_a[1]*((double) i)))/double_noise_start) < 2) && 
           (fabs((data_half_single[i] - (single_a[0] + single_a[1]*((double) i)))/single_noise_start) < 2)) {
            first_turning = i;
            break;
        }
    }
    
    mexPrintf("Found first-turning point at i = %d\n",first_turning);
    mexEvalString("drawnow;");
    /* end finding the start point*/
    
    /* find the stop point*/
    // get the slope and intercept of the ending trend (slope should be pretty small...)
    double_a[0] = 0;
    double_a[1] = 0;
    linfit(data_half_double,n_data - (int_f_sample*4),n_data - (int_f_sample),double_a);
    single_a[0] = 0;
    single_a[1] = 0;
    linfit(data_half_single,n_data - (int_f_sample*4),n_data - (int_f_sample),single_a);
    
    // find the noise
    double double_noise_stop = 0;
    double single_noise_stop = 0;
    for(i=(n_data - (int_f_sample*4));i<(n_data - (int_f_sample));i++) {
        double_noise_stop += (data_half_double[i] - (double_a[0] + double_a[1]*((double) i))) * 
                             (data_half_double[i] - (double_a[0] + double_a[1]*((double) i)));
        single_noise_stop += (data_half_single[i] - (single_a[0] + single_a[1]*((double) i))) * 
                             (data_half_single[i] - (single_a[0] + single_a[1]*((double) i)));
    }
    double_noise_stop = sqrt(double_noise_stop / (f_sample_in_hz*3 - 1));
    single_noise_stop = sqrt(single_noise_stop / (f_sample_in_hz*3 - 1));
    
    // find the first ~10-sigma point
    int nominal_last_turning = 0;
    for(i=(n_data-1); i>first_turning; i--) {
        if((fabs(data_half_double[i] - (double_a[0] + double_a[1]*((double) i))) > double_enc_threshold) || 
           (fabs(data_half_single[i] - (single_a[0] + single_a[1]*((double) i))) > single_enc_threshold)) {
            nominal_last_turning = i;
            break;
        }
    }
    
    // head forwards until you get to the mean level
    // (i.e. the difference between the current point and the mean level gets small)
    last_turning = nominal_last_turning;
    for(i=nominal_last_turning;i<(nominal_last_turning + 3*ring_encoder_reject_threshold);i++) {
        if((fabs((data_half_double[i] - (double_a[0] + double_a[1]*((double) i)))/double_noise_stop) < 2) && 
           (fabs((data_half_single[i] - (single_a[0] + single_a[1]*((double) i)))/single_noise_stop) < 2)) {
            last_turning = i;
            break;
        }
    }
    
    mexPrintf("Found last-turning point at i = %d\n",last_turning);
    mexEvalString("drawnow;");
    /* end finding the stop point*/
    
    mexPrintf("Searching entire template for offset angle and turn direction...\n");
    mexEvalString("drawnow;");
    
    double params_offset_search[6] = {(( double ) first_turning)/f_sample_in_hz, .5,  
                        (( double ) last_turning)/f_sample_in_hz, hwp_speed_in_dps,
                        0,1};                    
    // assume forward turn
    double logp_forward[n_template/3];
    // initialize to -inf likelihood
    for(i=0;i<(n_template/3);i++)
        logp_forward[i] = log(0);
    mexPrintf("\nTrying forward turns...\n");
    mexEvalString("drawnow;");
    for(i=0;i<(n_template/3);i+=ring_encoder_reject_threshold) {
        // pick out an offset angle between 0 and 360
        params_offset_search[4] = template_angle[i + (n_template/3)];
        // calculate the likelihood assuming that offset
        logp_forward[i] = logp(params_offset_search,6,
                               template_angle, template_half_double, template_half_single, template_abs, n_template,
                               data_half_double, data_half_single, data_abs, n_data,
                               hwp_speed_in_dps, f_sample_in_hz,100,
                               model_half_double, model_half_single, model_abs);
        
        // status update
        if(!(i % 1000))
                mexPrintf("\ti = %d of %d\n",i,n_template/3);
                mexEvalString("drawnow;");
    }
    double logpmax_forward = max_double(logp_forward,n_template/3);
        
    // assume backward turn
    params_offset_search[3] = -params_offset_search[3];
    double logp_backward[n_template/3];
    // initialize to -inf likelihood
    for(i=0;i<(n_template/3);i++)
        logp_backward[i] = log(0);
    mexPrintf("\nTrying backward turns...\n");
    mexEvalString("drawnow;");
    for(i=0;i<(n_template/3);i+=ring_encoder_reject_threshold) {
        // pick out an offset angle between 0 and 360
        params_offset_search[4] = template_angle[i + (n_template/3)];
        // calculate the likelihood assuming that offset
        logp_backward[i] = logp(params_offset_search,6,
                                template_angle, template_half_double, template_half_single, template_abs, n_template,
                                data_half_double, data_half_single, data_abs, n_data,
                                hwp_speed_in_dps, f_sample_in_hz,100,
                                model_half_double, model_half_single, model_abs);
        
        if(!(i % 1000))
                mexPrintf("\ti = %d of %d\n",i,n_template/3);
                mexEvalString("drawnow;");
    }
    double logpmax_backward = max_double(logp_backward,n_template/3);
    
    // find out whether it's forward or backward, and what the offset is
    double direction = 0;
    double offset = 0;
    int i_best = -1;
    if(logpmax_forward > logpmax_backward) {
        // store the direction
        direction = +1;
        mexPrintf("\nIt was a forward turn.\n");
        mexEvalString("drawnow;");
        
        // find the offset angle and store it
        for(i=0;i<(n_template/3);i++) {
         if(logp_forward[i] == logpmax_forward) {
             offset = template_angle[i + (n_template/3)];
             i_best = i;
         }
        }
        
    } else {
        // store the direction
        direction = -1;
        mexPrintf("\nIt was a backward turn.\n");
        mexEvalString("drawnow;");

        
        // find the offset angle and store it
        for(i=0;i<(n_template/3);i++) {
         if(logp_backward[i] == logpmax_backward) {
             offset = template_angle[i + (n_template/3)];
             i_best = i;
         }
        }
        
    }
    
    
    // look at more offsets near the best one
    // assume forward turn
    params_offset_search[3] = -params_offset_search[3];
    for(i=(i_best-(5*ring_encoder_reject_threshold));i<(i_best+(5*ring_encoder_reject_threshold));i++) {
        // pick out an offset angle between 0 and 360
        params_offset_search[4] = template_angle[i + (n_template/3)];
        // calculate the likelihood assuming that offset
        logp_forward[i] = logp(params_offset_search,6,
                               template_angle, template_half_double, template_half_single, template_abs, n_template,
                               data_half_double, data_half_single, data_abs, n_data,
                               hwp_speed_in_dps, f_sample_in_hz,100,
                               model_half_double, model_half_single, model_abs);
    }
    logpmax_forward = max_double(logp_forward,n_template/3);
        
    // assume backward turn
    params_offset_search[3] = -params_offset_search[3];
    for(i=(i_best-(5*ring_encoder_reject_threshold));i<(i_best+(5*ring_encoder_reject_threshold));i++) {
        // pick out an offset angle between 0 and 360
        params_offset_search[4] = template_angle[i + (n_template/3)];
        // calculate the likelihood assuming that offset
        logp_backward[i] = logp(params_offset_search,6,
                                template_angle, template_half_double, template_half_single, template_abs, n_template,
                                data_half_double, data_half_single, data_abs, n_data,
                                hwp_speed_in_dps, f_sample_in_hz,100,
                                model_half_double, model_half_single, model_abs);
    }
    logpmax_backward = max_double(logp_backward,n_template/3);
    
    // find out whether it's forward or backward, and what the offset is
    direction = 0;
    offset = 0;
    if(logpmax_forward > logpmax_backward) {
        // store the direction
        direction = +1;
        
        // find the offset angle and store it
        for(i=0;i<(n_template/3);i++) {
         if(logp_forward[i] == logpmax_forward) {
             offset = template_angle[i + (n_template/3)];
         }
        }
        
    } else {
        // store the direction
        direction = -1;
        
        // find the offset angle and store it
        for(i=0;i<(n_template/3);i++) {
         if(logp_backward[i] == logpmax_backward) {
             offset = template_angle[i + (n_template/3)];
         }
        }
        
    }
    
    // combine results to a "good start" turn parameter array
    double params_start[6];
    params_start[0] = (( double ) first_turning)/f_sample_in_hz;
    params_start[1] = .5;
    params_start[2] = (( double ) last_turning)/f_sample_in_hz;
    params_start[3] = direction*hwp_speed_in_dps;
    params_start[4] = offset;
    params_start[5] = 1;
    
    // make ranges for the grid search to improve some of the parameters
    mexPrintf("\nStarting grid search to improve other turn parameters...\n");
    mexEvalString("drawnow;");
    double range[4];
    range[0] = .4; // spinup time
    range[1] = .1*direction*hwp_speed_in_dps; // speed
    range[2] = 2.5; // offset
    range[3] = .3; // amplitude
    
    int n_grid[4]; // number of grid points in each direction
    n_grid[0] = 8; 
    n_grid[1] = 25; 
    n_grid[2] = 25; 
    n_grid[3] = 8; 
    
    // declare variables for the search
    double logp_best_fit = -DBL_MAX;
    double logp_here;
    double params_i[6];
    
    // do the grid search
    for(i=0;i<n_grid[0];i++) {
        mexPrintf("\tSubgrid %d of %d\n",i,n_grid[0]);
        mexEvalString("drawnow;");
        for(j=0;j<n_grid[1];j++) {
            for(k=0;k<n_grid[2];k++) {
                for(l=0;l<n_grid[3];l++) {
                    // make this set of parameters
                    params_i[0] = params_start[0];
                    params_i[1] = params_start[1] - (range[0]/2) + (range[0] * ((double) i / (double) n_grid[0]));
                    params_i[2] = params_start[2];
                    params_i[3] = params_start[3] - (range[1]/2) + (range[1] * ((double) j / (double) n_grid[1]));
                    params_i[4] = params_start[4] - (range[2]/2) + (range[2] * ((double) k / (double) n_grid[2]));
                    params_i[5] = params_start[5] - (range[3]/2) + (range[3] * ((double) l / (double) n_grid[3]));
                    
                    // calculate the likelihood here
                    logp_here = logp(params_i,6,
                                template_angle, template_half_double, template_half_single, template_abs, n_template,
                                data_half_double, data_half_single, data_abs, n_data,
                                hwp_speed_in_dps, f_sample_in_hz,10,
                                model_half_double, model_half_single, model_abs);
                    
                    // if here is better than the best so far, log it away
                    if(logp_here > logp_best_fit) {
                        logp_best_fit = logp_here;
                        params_out[0] = params_i[0];
                        params_out[1] = params_i[1];
                        params_out[2] = params_i[2];
                        params_out[3] = params_i[3];
                        params_out[4] = params_i[4];
                        params_out[5] = params_i[5];  
                    }  
                }
            }
        }
    }
    
    // do it again, but at finer resolution
    params_start[0] = params_out[0];
    params_start[1] = params_out[1];
    params_start[2] = params_out[2];
    params_start[3] = params_out[3];
    params_start[4] = params_out[4];
    params_start[5] = params_out[5];
    
    range[0] = 3 * (range[0] / n_grid[0]);
    range[1] = 3 * (range[1] / n_grid[1]);
    range[2] = 3 * (range[2] / n_grid[2]);
    range[3] = 3 * (range[3] / n_grid[3]);
    
    for(i=0;i<n_grid[0];i++) {
        mexPrintf("\t\tSub-subgrid %d of %d\n",i,n_grid[0]);
        mexEvalString("drawnow;");
        for(j=0;j<n_grid[1];j++) {
            for(k=0;k<n_grid[2];k++) {
                for(l=0;l<n_grid[3];l++) {
                    // make this set of parameters
                    params_i[0] = params_start[0];
                    params_i[1] = params_start[1] - (range[0]/2) + (range[0] * ((double) i / (double) n_grid[0]));
                    params_i[2] = params_start[2];
                    params_i[3] = params_start[3] - (range[1]/2) + (range[1] * ((double) j / (double) n_grid[1]));
                    params_i[4] = params_start[4] - (range[2]/2) + (range[2] * ((double) k / (double) n_grid[2]));
                    params_i[5] = params_start[5] - (range[3]/2) + (range[3] * ((double) l / (double) n_grid[3]));
                    
                    // calculate the likelihood here
                    logp_here = logp(params_i,6,
                                template_angle, template_half_double, template_half_single, template_abs, n_template,
                                data_half_double, data_half_single, data_abs, n_data,
                                hwp_speed_in_dps, f_sample_in_hz,10,
                                model_half_double, model_half_single, model_abs);
                    
                    // if here is better than the best so far, log it away
                    if(logp_here > logp_best_fit) {
                        logp_best_fit = logp_here;
                        params_out[0] = params_i[0];
                        params_out[1] = params_i[1];
                        params_out[2] = params_i[2];
                        params_out[3] = params_i[3];
                        params_out[4] = params_i[4];
                        params_out[5] = params_i[5];  
                    }  
                }
            }
        }
    }
    
    // calculate the likelihood once more to get the model out
    double theta_end = logp(params_out,6,
                            template_angle, template_half_double, template_half_single, template_abs, n_template,
                            data_half_double, data_half_single, data_abs, n_data,
                            hwp_speed_in_dps, f_sample_in_hz,1000,
                            model_half_double, model_half_single, model_abs);
    
    mexPrintf("\nDone!\n");
    return theta_end;
}

double logp(double *params,int n_params,double *template_angle, double *template_half_double, double *template_half_single, double *template_abs, int n_template,
            double *data_half_double, double *data_half_single, double *data_abs, int n_data,
            double hwp_speed_in_dps, double f_sample_in_hz, int runmode, double *model_half_double, double *model_half_single, double *model_abs) {
    
    // runmode = 0: use all three timestreams to form likelihood
    // runmode = 10: use all three timestreams, AND write out the model to a file
    // runmode = 100: just use absolute encoder to form likelihood
    // runmode = 1000: all three timestreams, but return theta_final instead of logp
    
    int i;
    
    // get the noise level
    double mean_level = 0;
    double noise_level = 0;
    for(i=(n_data-50);i<n_data;i++) {
        mean_level += data_half_double[i];
    }
    mean_level = mean_level/50;
    for(i=(n_data-50);i<n_data;i++) {
        noise_level += (data_half_double[i] - mean_level)*(data_half_double[i] - mean_level);
    }
    noise_level = sqrt(noise_level)/49;    
        
    // unpack parameters
    double t0 = params[0];
    double tau = params[1];
    double te = params[2];
    double speed = params[3];
    double theta0 = params[4];
    double amp = params[5];
    
    // make alpha a derived parameter
    double alpha = speed / tau;
    
    double t;
    double theta;
    int i_nearest;
    double chi2 = 0;
    int not_moving = 0;
    for(i=0;i<n_data;i++) {       
        // find the current angle assuming the HWP starts at rest,
        // accelerates up to speed, runs at speed,
        // deccelerates down to a stop, and stays there
        t = ((double) i) / f_sample_in_hz;
        
        if(t <= t0) {
            theta = theta0;
            not_moving = 1;
        }
        
        if((t0 < t) && (t <= (t0 + tau))) {
            theta = theta0 + (0.5)*alpha*t*t  - alpha*t0*t
                              + (0.5)*alpha*t0*t0;
            not_moving = 0;
        }
        
        if(((t0+tau) < t) && (t <= (te - tau))) {
            theta = theta0 + (0.5)*alpha*(t0+tau)*(t0+tau) - alpha*t0*(t0+tau)
                              + (0.5)*alpha*t0*t0 + alpha*tau*(t - t0 - tau);
            not_moving = 0;
        }
        
        if(((te-tau) < t) && (t <= te)) {
            theta = theta0 + (0.5)*alpha*(t0+tau)*(t0+tau) - alpha*t0*(t0+tau)
                              + (0.5)*alpha*t0*t0 + alpha*tau*(te - t0 - 2.0*tau)
                              - (0.5)*alpha*t*t + alpha*te*t + (0.5)*alpha*(te - tau)*(te - tau) - alpha*te*(te - tau);
            not_moving = 0;
        }
        
        if(t > te) {
            theta = theta0 + (0.5)*alpha*(t0+tau)*(t0+tau) - alpha*t0*(t0+tau)
                              + (0.5)*alpha*t0*t0 + alpha*tau*(te - t0 - 2.0*tau)
                              - (0.5)*alpha*te*te + alpha*te*te + (0.5)*alpha*(te - tau)*(te - tau) - alpha*te*(te - tau);
            not_moving = 1;
        }
        
        
        // find the nearest-neighbor point from the template
        i_nearest = (double *)bsearch(&theta, template_angle, n_template, sizeof(double), search_compare) - template_angle;
        
        // make the model template
        model_half_double[i] = template_half_double[i_nearest]*amp;
        model_half_single[i] = template_half_single[i_nearest]*amp;
        model_abs[i] = template_abs[i_nearest]*amp;
        
        // calculate the joint chi-squared
        if(runmode < 20) {// full likelihood
            chi2 += ((model_half_double[i] - data_half_double[i])*(model_half_double[i] - data_half_double[i]))/(noise_level*noise_level)
                 +  ((model_half_single[i] - data_half_single[i])*(model_half_single[i] - data_half_single[i]))/(noise_level*noise_level)
                 +  ((model_abs[i]         - data_abs[i])        *(model_abs[i]         - data_abs[i]))        /(noise_level*noise_level);
        } else if(not_moving) {// just using abs, and it's not moving so ignore this data point
            chi2 += 0;
        } else {// just using abs, and we're moving so use this data point
            chi2 += ((model_abs[i]         - data_abs[i])        *(model_abs[i]         - data_abs[i]))        /(noise_level*noise_level);
            
        }
    }
    
    if(runmode == 1000){// theta_final
        return theta;
    } else {// return the log likelihood
    return -0.5*chi2;
    }
    
}

/* --- "slow" max and min finder for doubles --- */
/* http://ndevilla.free.fr/median/median.pdf */
int compare(const void *f1, const void *f2) {
    return ( *(double*)f1 > *(double*)f2 ) ? 1 : -1;
}

double max_double(double * array, int n) {
    // make a local copy
    int i;
    double local_array[n];
    for(i=0;i<n;i++) {
        local_array[i] = array[i];
    }
    // run the quicksort algorithm
    qsort(local_array, n, sizeof(double), compare);
    // return the max
    return local_array[n-1];
}

double min_double(double * array, int n) {
    // make a local copy
    int i;
    double local_array[n];
    for(i=0;i<n;i++) {
        local_array[i] = array[i];
    }
    // run the quicksort algorithm
    qsort(local_array, n, sizeof(double), compare);
    // return the max
    return local_array[0];
}
/* --- "slow" max and min finder for doubles --- */

/* --- bsearch compare function for nearest neighbor --- */
int search_compare(const void *kp, const void *vp) {
    
    double key = *(const double*)kp;
    const double *vec = (const double*)vp;
    
    if (key < vec[0]) {
        if (key < (vec[-1] + vec[0]) / 2.0)
            return -1;
    }
    
    if (key > vec[0]) {
        if (key > (vec[0] + vec[1]) / 2.0)
            return +1;
    }
    
    return 0;
}
/* --- bsearch compare function for nearest neighbor --- */

/* --- linear fit no error bars --- */
void linfit(double *y, mwSize min, mwSize max, double *a) {
    double sxx = 0;
    double sxy = 0;
    double sx  = 0;
    double sy  = 0;
    
    mwSize i;
    for(i=min;i<max;i++) {
        sxx += ((double) i)*((double) i);
        sxy += ((double) i)*y[i];
        sx  += ((double) i);
        sy  += y[i];  
    }
    
    double N = ((double) max) - ((double) min);
    double delta = N*sxx - sx*sx;
    
    a[1] = (1/delta)*(N*sxy  - sx*sy);  // slope
    a[0] = (1/delta)*(sxx*sy - sx*sxy); // intercept
    
}
/* --- linear fit no error bars --- */

// The gateway function
void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    /* Start variable declaration */
    // input variables
    double *template_angle;
    double *template_half_double;
    double *template_half_single;
    double *template_abs;
    double *data_half_double;
    double *data_half_single;
    double *data_abs;
    double hwp_speed_in_dps;
    double f_sample_in_hz;
    mwSize n_template;
    mwSize n_data;
    
    // output variables
    double *params_out;
    double *model_half_double;
    double *model_half_single;
    double *model_abs;
    double *final_angle;
    /* end variable declaration */
    
    /* Start actually getting input values */
    template_angle = mxGetPr(prhs[0]);
    template_half_double = mxGetPr(prhs[1]);
    template_half_single = mxGetPr(prhs[2]);
    template_abs = mxGetPr(prhs[3]);
    data_half_double = mxGetPr(prhs[4]);
    data_half_single = mxGetPr(prhs[5]);
    data_abs = mxGetPr(prhs[6]);
    hwp_speed_in_dps = mxGetScalar(prhs[7]);
    f_sample_in_hz = mxGetScalar(prhs[8]);
    n_template = mxGetN(prhs[0]);
    n_data = mxGetN(prhs[4]);    
    /* End getting input values */
    
    /* Start making output variable */
    // create the output matrix
    plhs[0] = mxCreateDoubleMatrix(1, 6, mxREAL);
    plhs[1] = mxCreateDoubleMatrix(1, n_data, mxREAL);
    plhs[2] = mxCreateDoubleMatrix(1, n_data, mxREAL);
    plhs[3] = mxCreateDoubleMatrix(1, n_data, mxREAL);
    plhs[4] = mxCreateDoubleMatrix(1, 1, mxREAL);
    
    // get a pointer to the real data in the output matrix
    params_out = mxGetPr(plhs[0]);
    model_half_double = mxGetPr(plhs[1]);
    model_half_single = mxGetPr(plhs[2]);
    model_abs = mxGetPr(plhs[3]);
    final_angle = mxGetPr(plhs[4]);
    /* End making output variable */
    
    /* Start actual calculation */
    double tmp_angle = 0;
    tmp_angle = reconstruct_hwp_angle(template_angle, template_half_double, template_half_single, template_abs, n_template,
        data_half_double, data_half_single, data_abs, n_data,
        hwp_speed_in_dps, f_sample_in_hz, params_out, model_half_double, model_half_single, model_abs);
    
    final_angle[0] = tmp_angle;
    /* End actual calculation */
}