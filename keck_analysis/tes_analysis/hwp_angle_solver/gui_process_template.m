clear


% load template
template_filename = '15-Jun-2011-14-12-53'; % long turn
template_directory = 'sample_data/long_turns/';

% choose which motor
i_motor = 1;

load([template_directory template_filename '.mat']);

% downsample to get to ~150 Hz sampling
n_down = floor(out.sample_rate/150);

figure(1)
clf
plot(out.motor(i_motor).abs)

% ask if we should flip it or not
flip_template = input('Should the template be flipped? [1 for yes, 0 for no] : ');
if flip_template
    template_time = out.time(1:n_down:end);
    template_shaft = flipud(out.motor(i_motor).shaft(1:n_down:end));
    template_half_double = flipud(out.motor(i_motor).half_double(1:n_down:end));
    template_half_single = flipud(out.motor(i_motor).half_single(1:n_down:end));
    template_abs = flipud(out.motor(i_motor).abs(1:n_down:end));
else
    template_time = out.time(1:n_down:end);
    template_shaft = (out.motor(i_motor).shaft(1:n_down:end));
    template_half_double = (out.motor(i_motor).half_double(1:n_down:end));
    template_half_single = (out.motor(i_motor).half_single(1:n_down:end));
    template_abs = (out.motor(i_motor).abs(1:n_down:end));
end

f_sample = out.sample_rate / n_down;
hwp_speed = (out.motor(i_motor).speed / 200)*(360/465);
this_motor_name = out.motor(i_motor).motor_name;

clear out;

% run the tick finder
[template_tick_indices] = find_peaks(template_time,template_half_double,f_sample,hwp_speed);

gui_plot_module

% go through the data set and make sure it looks right
for i = 1:(5*f_sample):length(template_half_double)
   % tuck away the old list of ticks in case we want to undo this
   old_template_tick_indices = template_tick_indices;
    
   % change the x limits on the plot
   this_limit = i + [-f_sample 6*f_sample];
   
   figure(1)
   subplot(4,1,1); xlim(this_limit);
   subplot(4,1,2); xlim(this_limit);
   subplot(4,1,3); xlim(this_limit);
   subplot(4,1,4); xlim(this_limit);
   
   % start off unhappy with this screen
   % that's why we're asking questions from the user
   unhappy = 'y';
   while unhappy
       % if the user wants to delete some points
       if input('Any to delete? [enter for no, ''y'' to delete ticks] : ','s')
           n_delete = input('      How many to delete? : ');
           disp(['      Click on ' num2str(n_delete) ' ticks.'])
           [x,y] = ginput(n_delete);
           % find the ticks nearest to the clicks
           for j = 1:n_delete
               ind_diff = abs(template_tick_indices - x(j));
               i_bad = find(ind_diff == min(ind_diff));
               template_tick_indices = [template_tick_indices(1:(i_bad-1)) ; template_tick_indices((i_bad+1):end)];
           end
           
           % replot
           gui_plot_module
           figure(1)
           subplot(4,1,1); xlim(this_limit);
           subplot(4,1,2); xlim(this_limit);
           subplot(4,1,3); xlim(this_limit);
           subplot(4,1,4); xlim(this_limit);         
       end
       
       % if the user wants to add some points
       if input('Any to add? [enter for no, ''y'' to add ticks] : ','s')
           n_add = input('      How many to add? : ');
           disp(['      Click on ' num2str(n_add) ' ticks.'])
           
           [x,y] = ginput(n_add);
           
           % add those on
           template_tick_indices = sort([template_tick_indices ; round(x)]);
           
           % replot
           gui_plot_module
           figure(1)
           subplot(4,1,1); xlim(this_limit);
           subplot(4,1,2); xlim(this_limit);
           subplot(4,1,3); xlim(this_limit);
           subplot(4,1,4); xlim(this_limit);  
       end
       
       % if the user is STILL unhappy, then start over
       unhappy_now = input('Does this look bad? [enter for good, ''y'' for bad] : ','s');
       if strcmp(unhappy_now,'y')
           template_tick_indices = old_template_tick_indices;
           unhappy = 'y';
       else
           unhappy = [];
       end
   end
   
   % if the user says we're done, then break out
   if strcmp(unhappy_now,'done')
       break
   end
end

% go through the tick locations, fit a parabola, and find the max or min of that
n_samples_apart = ceil( 1.0 * (f_sample/(hwp_speed*4)) );
for i = 1:length(template_tick_indices)
    nearby_indices = template_tick_indices(i) + [round(-.3*n_samples_apart):round(.3*n_samples_apart)];
    nearby_template = template_half_double(nearby_indices)';
    
    % do the fit
    [p s mu] = polyfit(nearby_indices,nearby_template,2);
    fit_vals = polyval(p,(nearby_indices - mu(1))./mu(2));
    if mean(diff(diff(fit_vals))) > 0
        best_nearby_indices = nearby_indices(find(fit_vals == min(fit_vals)));
        template_tick_indices(i) = best_nearby_indices(1);
    else
        best_nearby_indices = nearby_indices(find(fit_vals == max(fit_vals)));
        template_tick_indices(i) = best_nearby_indices(1);
    end
end

% bail on the first and last couple ticks in case they are spurious
% since this is the template, they aren't important anyway
template_tick_indices = template_tick_indices(3:(end - 3));

% make a coarse angle timestream
coarse_rel_angles = 0.25 .* (0:(length(template_tick_indices)-1));

% interpolate this up to the full timestream
fine_rel_angles = interp1(template_tick_indices,coarse_rel_angles,1:length(template_half_double),'cubic',NaN);

% keep going until we're happy
while 1  
    % plot the encoder timestream with relative angles
    figure(2)
    clf
    plot(fine_rel_angles,template_abs)
    xlabel('Relative Angle [deg]')
    ylabel('Absolute Encoder [V]')
    
    % ask for an offset angle
    disp( 'Zoom around to find zero degrees, ')
    input('and press enter when ready to click on zero degrees')
    [offset y] = ginput(1);
    
    % fit a parabola to make the offset a sample nearest to the minimum
    i_nearby = min(find(fine_rel_angles > offset));
    nearby_indices = i_nearby + [round(-.3*n_samples_apart):round(.3*n_samples_apart)];
    [p s mu] = polyfit(nearby_indices,template_abs(nearby_indices)',2);
    fit_vals = polyval(p,(nearby_indices - mu(1))./mu(2));    
    if mean(diff(diff(fit_vals))) > 0
        offset = fine_rel_angles(nearby_indices(find(fit_vals == min(fit_vals))));
    else
        offset = fine_rel_angles(nearby_indices(find(fit_vals == max(fit_vals))));
    end
    
    disp(['Look near theta = ' num2str(360-offset) ' for discontinuity']);
    
    % make absolute angle vector
    i_good = find((fine_rel_angles >= 0) & (fine_rel_angles < 360));
    % cut down template
    final_template.absolute_angles = fine_rel_angles(i_good)' - offset;
    final_template.abs = template_abs(i_good);
    final_template.half_double = template_half_double(i_good);
    final_template.half_single = template_half_single(i_good);
    
    i_pos = find(final_template.absolute_angles >= 0);
    i_neg = find(final_template.absolute_angles < 0);
    
    % shuffle things so it goes from 0 to 360 insteas of -100 to 260 or whatever
    final_template.absolute_angles = [final_template.absolute_angles(i_pos) ; final_template.absolute_angles(i_neg) + 360]; 
    final_template.abs             = [final_template.abs(i_pos)             ; final_template.abs(i_neg)];
    final_template.half_double     = [final_template.half_double(i_pos)     ; final_template.half_double(i_neg)];
    final_template.half_single     = [final_template.half_single(i_pos)     ; final_template.half_single(i_neg)];
    
    % plot it up
    figure(2)
    clf
    plot([final_template.absolute_angles-360 final_template.absolute_angles final_template.absolute_angles+360], ...
         [final_template.abs final_template.abs final_template.abs], ...
         ...
         [final_template.absolute_angles-360 final_template.absolute_angles final_template.absolute_angles+360], ...
         [final_template.half_double final_template.half_double final_template.half_double], ...
         ...
         [final_template.absolute_angles-360 final_template.absolute_angles final_template.absolute_angles+360], ...
         [final_template.half_single final_template.half_single final_template.half_single])
    xlabel('Absolute Angle [deg]')
    ylabel('[V]')
    legend('Absolute','Half-Double','Half-Single')
    
    go_with_it = input('Does this look good? [enter if bad, ''y'' if good] : ','s');
    
    if go_with_it
        break
    end
end

% save ascii version of the template
output_array = [final_template.absolute_angles final_template.abs final_template.half_double final_template.half_single];

save([template_directory template_filename '_motor_' num2str(i_motor) '_' this_motor_name '_processed.txt'],'output_array','-ascii')