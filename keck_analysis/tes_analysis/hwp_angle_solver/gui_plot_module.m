% plot an overview
figure(1)
clf
subplot(4,1,1)
plot(template_shaft);
ylabel('Template: Shaft Encoder [V]')

subplot(4,1,2)
plot(template_half_double,'k')
hold on
plot(template_tick_indices,template_half_double(template_tick_indices),'o','MarkerSize',15);
ylabel('Template: Double Encoder [V]')

subplot(4,1,3)
plot(template_half_single);
ylabel('Template: Single Encoder [V]')

subplot(4,1,4)
plot(template_abs);
ylabel('Template: Absolute Encoder [V]')