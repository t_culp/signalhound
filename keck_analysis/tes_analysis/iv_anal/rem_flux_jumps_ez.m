% REM_FLUX_JUMPS_EZ (FB, FLUX_JUMPS)
%
% Removes MCE flux jumping from feedback signal
%
% 090924 JPF Version for modes  with flux jumps in known positions
%            In general, take data with en_fb_jump=1 and good flx_quanta
% 110521 JPF Fit only once for each pixel; handle multi-jumps correctly

function fb_clean = rem_flux_jumps_ez (fb_dirty, flux_jumps)

NFIT = 6;

datlen = length(fb_dirty);

% find the times at which we jump
idx = find(diff(flux_jumps));
% drop times at the ends of the range, for which we can't fit
idx(idx==1 | idx==datlen)=[];

O = optimset ('lsqcurvefit');
O.Display = 'off';

jj = [];

for (ii = 1:length (idx))
    xx = max(-NFIT,-(idx(ii)-1)):min(NFIT,(datlen-idx(ii)));
    yy = fb_dirty(idx(ii)+xx)';
    mean0 = mean(yy);
    slp0 = median(diff(yy));
	amp0 = yy(end)-yy(1)-slp0*(xx(end)-xx(1));
    A0 = [mean0, slp0, amp0, 1];
	A = lsqcurvefit (@jumpfun, A0, xx, yy, [], [], O);
	jj(ii) = A(3);
	ofsp(ii) = A(4);
	% plot (pidx(ii) + (-NFIT:NFIT), jumpfun (A, -NFIT:NFIT), 'r-');
end;

jumpsize = median (jj);
if isnan (jumpsize)
	fb_clean = fb_dirty;
	return;
end;

jump = zeros (size (fb_dirty));
jumploc = idx+1+floor(ofsp');
% handle pathological cases in curves which are bad anyway
jumploc(jumploc>datlen)=datlen;
jumploc(jumploc<1)=1;
jump(jumploc) = jumpsize;
% subtract the jumps
fb_clean = fb_dirty - cumsum (jump);

if sum (isnan (fb_clean)) > 0
    keyboard;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function y = inrange (x, a, b)
	y = (x >= a) & (x <= b);

function y = jumpfun (A, x)
    y = A(1) + A(2)*x + A(3)/2*erf(5*(x - A(4)));


