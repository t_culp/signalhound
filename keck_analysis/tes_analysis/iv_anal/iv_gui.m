function varargout = iv_gui(varargin)
    % IV_GUI M-file for iv_gui.fig
    %
    % ABOUT THE IV_GUI
    %      Displays a GUI for interacting with IV plots and the associated data
    %      analysis.
    %
    %      BUTTONS:
    %           Auto: Updates the ranges and branches on the main plot
    %           Print: calls iv_plots to save a few figures
    %
    % CALLING THE IV_GUI
    %      IV_GUI, by itself, creates a new IV_GUI or raises the existing
    %      singleton*.
    %
    %      H = IV_GUI returns the handle to a new IV_GUI or the handle to
    %      the existing singleton*.
    %
    %      IV_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in IV_GUI.M with the given input arguments.
    %
    %      IV_GUI('Property','Value',...) creates a new IV_GUI or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before iv_gui_OpeningFunction gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to iv_gui_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES
    
    % Edit the above text to modify the response to help iv_gui
    
    % Last Modified by GUIDE v2.5 29-May-2008 15:35:35
    % 2009-07-22 ART - R_SH->3.0mOhm; display Beta (not n), and G_450;
    %                   change title to reflect rc even when can't plot;
    % 2009-07-28 ART - offsets handled correctly when you save new ranges
    % 2010-03-11 ASR - fixed a few plotting glitches
    % 2010-03-18 ASR - added keyboard shortcuts (see keypress_Callback for details)
                  
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
        'gui_Singleton',  gui_Singleton, ...
        'gui_OpeningFcn', @iv_gui_OpeningFcn, ...
        'gui_OutputFcn',  @iv_gui_OutputFcn, ...
        'gui_LayoutFcn',  [] , ...
        'gui_Callback',   []);
    if nargin && ischar(varargin{1}) && ~exist (varargin{1}, 'dir')
        gui_State.gui_Callback = str2func(varargin{1});
    end
    
    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
    
    % --- Executes just before iv_gui is made visible.
function iv_gui_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to iv_gui (see VARARGIN)
    
    % Choose default command line output for iv_gui
    handles.output = hObject;
    
    % set keypress callback function
    set(handles.figure1,'keypressfcn',@keypress_Callback);
    set(handles.norm_max_edit,'keypressfcn',@keypress_Callback);
    set(handles.norm_min_edit,'keypressfcn',@keypress_Callback);
    set(handles.norm_max_slider,'keypressfcn',@keypress_Callback);
    set(handles.norm_min_slider,'keypressfcn',@keypress_Callback);
    set(handles.bn_max_edit,'keypressfcn',@keypress_Callback);
    set(handles.bn_min_edit,'keypressfcn',@keypress_Callback);
    set(handles.bn_max_slider,'keypressfcn',@keypress_Callback);
    set(handles.bn_min_slider,'keypressfcn',@keypress_Callback);
    set(handles.sc_max_edit,'keypressfcn',@keypress_Callback);
    set(handles.sc_min_edit,'keypressfcn',@keypress_Callback);
    set(handles.sc_max_slider,'keypressfcn',@keypress_Callback);
    set(handles.sc_min_slider,'keypressfcn',@keypress_Callback);
    set(handles.save_button,'keypressfcn',@keypress_Callback);
    set(handles.auto_button,'keypressfcn',@keypress_Callback);
    set(handles.print_button,'keypressfcn',@keypress_Callback);
    set(handles.is_good_checkbox,'keypressfcn',@keypress_Callback);
    
    % Measured shunt resistance
    % This must be known beforehand for proper calibration.
    handles.R_SH = 0.0030;  % Ohms
    
    % initialize pixel row and column
    handles.mux_row = 0;
    set (handles.row_num_edit, 'String', {num2str(handles.mux_row)});
    handles.mux_col = 0;
    set (handles.col_num_edit, 'String', {num2str(handles.mux_col)});
    
    % choose working directory of data files
    if ~isempty (varargin) && exist (varargin{1}, 'dir')
        workdir = varargin{1};
    else
        workdir = uigetdir ('.', 'Select directory with data files');
    end;
    handles.workdir = workdir;
    
    % populate file list from working directory
    d = dir (fullfile (workdir, '*.bias'));
    flist = {};
    temp = [];
    templist = {};
    for (ii = 1:length (d))
        bare_fname = d(ii).name(1:(end-5));
        if exist (fullfile (workdir, bare_fname), 'file') ...
                && exist (fullfile (workdir, [bare_fname '.run']), 'file')
            flist = {flist{:}, fullfile(workdir,bare_fname)};
            on_temp = guess_base_temp (bare_fname);
            if isempty (on_temp)
                on_temp = ii;
            end;
            temp = [temp(:); on_temp];
            templist = {templist{:}, ...
                [num2str(temp(end)) ' mK']};
        else
            disp (['Skipping ' bare_fname]);
        end;
    end;
    if isempty (flist)
        disp (['No IV ramp data found.']);
        return;
    end;
    
    % perform IV analysis for any files without _iv_dir data
    for (ii = 1:length (flist))
        if ~exist ([flist{ii} '_iv_dir'], 'dir')
            disp('  Performing IV analysis, please wait...');
            iv_analysis (flist{ii});
            disp('    ... IV analysis complete!');
        end;
    end;
    
    % put file data and temperatures into handles data structure for later use
    handles.flist = flist;
    handles.dirf = {};
    handles.temp = temp;
    templist = {templist{:}, 'V for all', 'P for all', 'R for all', 'P vs. R', 'P vs. T', 'R vs. T'};
    handles.templist = templist;
    set (handles.temp_listbox, 'String', templist);
    handles.onf = 1;
    set (handles.temp_listbox, 'Value', handles.onf);
    
    % This sets up the initial plot - only do when we are invisible
    % so window can get raised using iv_gui.
    handles = load_dataset (handles);
    plot_dataset (handles);
        
    % Update handles structure
    guidata (hObject, handles);
    
    % UIWAIT makes iv_gui wait for user response (see UIRESUME)
    % uiwait(handles.figure1);
    
    
function handles = load_dataset (h_in)
    % loads a single pixel from a single data set
    handles = h_in;
    fname = handles.flist{handles.onf};
    mux_row = handles.mux_row;
    mux_col = handles.mux_col;
    if length(handles.dirf)<handles.onf || isempty(handles.dirf{handles.onf})
        % if we haven't loaded the dir file yet ...
        if ~exist ([fname '_iv_dir'], 'dir')
            % ... perform the IV analysis if needed and ...
            disp('  Performing IV analysis, please wait...');
            iv_analysis (fname, mux_row, mux_col);
            disp('    ... IV analysis complete!');
        end;
        % ... load the dir file
        handles.dirf{handles.onf} = dirfile_open ([fname '_iv_dir']);
    end;
    D = handles.dirf{handles.onf};
    pixname = ['r' num2str(mux_row) 'c' num2str(mux_col)];
    if isempty (dirfile_fields (D, pixname, 'regexp'))
        % no such pixel
        handles.i_bias = [];
        handles.i_tes = [];
        handles.branch = [];
        handles.is_good = 0;
        handles.msg = 'No such pixel';
    else
        % load fields from the dir file
        [handles.is_good D] = dirfile_get (D, ['is_good_' pixname]);
        [handles.fj D] = dirfile_get (D, ['fj_' pixname]);
        [handles.branch D] = dirfile_get (D, ['branch_' pixname]);
        [handles.i_bias D] = dirfile_get (D, ['i_b_' pixname]);
        [fb D] = dirfile_get (D, ['fb_' pixname]);
        [handles.i_s_cal D] = dirfile_get (D, ['i_s_cal_' pixname]);
        [handles.i_tes D] = dirfile_get (D, ['i_s_' pixname]);
        handles.i_tes_raw = fb * handles.i_s_cal/2^14;
        [handles.msg D] = dirfile_get (D, ['msg_' pixname]);
        handles.msg = char (handles.msg');
    end;
    if ~isempty (handles.branch)
        % if we have a partitioning into branches, note the divisions
        i_bias = handles.i_bias;
        branch = handles.branch;
        handles.sc_min = min (i_bias (branch==1));
        handles.sc_max = max (i_bias (branch==1));
        handles.bn_min = min (i_bias (branch>1));
        handles.bn_max = max (i_bias (branch>1));
        handles.norm_min = min (i_bias (branch==3));
        handles.norm_max = max (i_bias (branch==3));
    else
        % if not, clear out these values
        % note that these are NOT valid array indices!
        handles.sc_min = 0;
        handles.sc_max = 0;
        handles.bn_min = 0;
        handles.bn_max = 0;
        handles.norm_min = 0;
        handles.norm_max = 0;
    end;
    handles.dirf{handles.onf} = D;
    update_range_labels (handles); % display the branch divisions
    
function update_range_labels (handles)
    % display the branch divisions in their respective slots
    set (handles.sc_min_edit, 'String', num2str(handles.sc_min * 1e6, '%.1f'));
    set (handles.sc_max_edit, 'String', num2str(handles.sc_max * 1e6, '%.1f'));
    set (handles.bn_min_edit, 'String', num2str(handles.bn_min * 1e6, '%.1f'));
    set (handles.bn_max_edit, 'String', num2str(handles.bn_max * 1e6, '%.1f'));
    set (handles.norm_min_edit, 'String', num2str(handles.norm_min * 1e6, '%.1f'));
    set (handles.norm_max_edit, 'String', num2str(handles.norm_max * 1e6, '%.1f'));
    set (handles.is_good_checkbox, 'Value', handles.is_good);
    
function update_range_all (handles)
    update_range_labels (handles);
    update_range_plots (handles);
    
function update_range_plots (handles)
    % overlay the bias ranges in color on the main plot
    if isempty (handles.i_tes)
        return;
    end;
    i_bias = handles.i_bias;
    i_tes = handles.i_tes;
    % ranges = handles.ranges; % not used, field not filled yet!
    branch = handles.branch;
    hold on;
    
    old_lines = get (gca, 'UserData');
    if ~isempty (old_lines)
        delete (old_lines);
    end;
    % superconducting
    cut = zeros (size (i_bias));
    if ~isempty (handles.sc_min) && ~isempty (handles.sc_max)
        cut = inrange (i_bias, handles.sc_min, handles.sc_max);
    end;
    if (sum (cut) == 0) && ~isempty (branch)
        cut = (branch == 1);
    end;
    try
        sc_line = plot (1e6 * i_bias(cut), 1e6 * i_tes(cut), 'go', 'LineWidth', 2);
    catch, keyboard; end;
    % bias/normal
    cut = zeros (size (i_bias));
    if ~isempty (handles.bn_min) && ~isempty (handles.bn_max)
        cut = inrange (i_bias, handles.bn_min, handles.bn_max);
    end;
    if sum (cut) == 0 && ~isempty (branch)
        cut = (branch > 1);
    end;
    bn_line = plot (1e6 * i_bias(cut), 1e6 * i_tes(cut), 'rx', 'LineWidth', 2);
    % normal
    cut = zeros (size (i_bias));
    if ~isempty (handles.norm_min) && ~isempty (handles.norm_max)
        cut = inrange (i_bias, handles.norm_min, handles.norm_max);
    end;
    if sum (cut) == 0 && ~isempty (branch)
        cut = (branch == 3);
    end;
    norm_line = plot (1e6 * i_bias(cut), 1e6 * i_tes(cut), 'ko', 'LineWidth', 2);
    % save the handles to these curves in the axes' UserData field
    set (gca, 'UserData', [sc_line bn_line norm_line]);
    
function plot_dataset (handles)
    % plot a single IV curve to the main axes
    cla;
    hold off;
    if isempty (handles.i_tes) || isempty (handles.i_bias)
        % Plot an "X" if we don't have an IV curve
        axis ([0 1 0 1]);
        line ([0 1], [1 0], 'color', 'k');
        hold on;
        line ([0 1], [0 1], 'color', 'k');
        % prettify the plot (otherwise labels wont update for new pixel)
        title (['Row ' num2str(handles.mux_row) ', Col ' num2str(handles.mux_col) ...
        ' at ' num2str(handles.temp(handles.onf)) ' mK']);
    return;
    end;
    i_bias = handles.i_bias;
    i_tes = handles.i_tes;
    branch = handles.branch;
    fj = handles.fj;
    msg = handles.msg;
    % plot fake points for the legend
    plot (-1e12, -1e12, 'b.', 'MarkerSize', 6);
    hold on;
    plot (-1e12, -1e12, 'go', 'LineWidth', 2);
    plot (-1e12, -1e12, 'rx', 'LineWidth', 2);
    plot (-1e12, -1e12, 'ko', 'LineWidth', 2);
    
    % plot the IV curve
    plot (i_bias*1e6, i_tes*1e6, 'b.', 'MarkerSize', 6);
    hold on;
    for (ii = 1:length(fj))
        % identify the flux jumps with vertical lines
        line (1e6 * i_bias(fj(ii)) * [1 1], ...
            1e6 * [min(i_tes) max(i_tes)], ...
            'LineWidth', 1, 'color', 'k', 'LineStyle', '--'); % , ...
        % 'ButtonDownFcn', @clicky);
    end;
    % overlay the branch divisions in color
    set (gca, 'UserData', []);
    update_range_plots (handles);
    legend ('All', 'Superconducting', 'Bias/normal', 'Normal');
    
    % prettify the plot
    grid on;
    xlabel ('I_b / \muA');
    ylabel ('I_s / \muA');
    title (['Row ' num2str(handles.mux_row) ', Col ' num2str(handles.mux_col) ...
        ' at ' num2str(handles.temp(handles.onf)) ' mK']);
    axis (1e6 * [0 max(i_bias) min(i_tes-1e-20) max(i_tes+1e-20)]);
    zoom reset;
    if ~isempty (msg)
        axax = axis;
        text (1/2*sum(axax(1:2)), 0.4*axax(3)+0.6*axax(4), ...
            msg, 'FontSize', 12, 'HorizontalAlignment', 'Center');
    end;
    
    
function handles = load_all_temps (h_in)
    
    handles = h_in;
    handles.i_bias = {};
    handles.i_tes = {};
    handles.p_s = {};
    handles.is_good = [];
    for (onf = 1:length (handles.flist))
        if (onf>length(handles.dirf)) || isempty(handles.dirf{onf})
            fname = handles.flist{onf};
            if ~exist ([fname '_iv_dir'], 'file')
                handles.is_good(onf) = [];
                continue;
            end;
            D = dirfile_open ([fname '_iv_dir']);
        else
            D = handles.dirf{onf};
        end;
        pixname = ['r' num2str(handles.mux_row) 'c' num2str(handles.mux_col)];
        if isempty(dirfile_fields (D, ['is_good_' pixname]))
            handles.i_bias{onf} = [];
            handles.i_tes{onf} = [];
            handles.is_good(onf) = false;
        else
            [handles.is_good(onf) D] = dirfile_get (D, ['is_good_' pixname]);
            if handles.is_good(onf)
                [branch D] = dirfile_get (D, ['branch_' pixname]);
                % do not use SC region for calculations!
                cc = (branch > 1);
                [tmp D] = dirfile_get (D, ['i_b_' pixname]);
                handles.i_b{onf} = tmp(cc);
                [tmp D] = dirfile_get (D, ['i_s_' pixname]);
                handles.i_s{onf} = tmp(cc);
                [tmp D] = dirfile_get (D, ['p_s_' pixname]);
                handles.p_s{onf} = tmp(cc);
                [tmp D] = dirfile_get (D, ['r_s_' pixname]);
                handles.r_s{onf} = tmp(cc);
                [tmp D] = dirfile_get (D, ['v_s_' pixname]);
                handles.v_s{onf} = tmp(cc);
                [handles.r_n{onf} D] = dirfile_get (D, ['r_n_' pixname]);
            end;
        end;
        handles.dirf{onf} = D;
    end;
    
    
function plot_p_all (handles)
    hold off;
    cla;
    axmax = [0 0 0];
    hold on;
    legleg = {};
    CC = 'bgrkcmy';
    SS = '*s+voxd';
    for (onf = 1:length(handles.temp))
        if (onf > length(handles.is_good)) || ~handles.is_good(onf) ...
                || (onf > length(handles.p_s)) || isempty (handles.p_s{onf})
            continue;
        end;
        i_b = handles.i_b{onf};
        i_s = handles.i_s{onf};
        p_s = handles.p_s{onf};
        r_s = handles.r_s{onf};
        legleg = {legleg{:}, [num2str(handles.temp(onf)) ' mK']};
        sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
        plot (i_b*1e6, p_s*1e12, sym);
        axmax(1) = max (axmax(1), max(i_b)*1e6);
        axmax(2) = max (axmax(2), max(p_s)*1e12);
        axmax(3) = min (axmax(3), max([0 min(p_s)*1e12]));
    end;
    if sum (abs (axmax)) == 0
        axis ([0 1 0 1]);
        line ([0 1], [0 1], 'color', 'k');
        line ([0 1], [1 0], 'color', 'k');
        legend off;
        title (['Row ' num2str(handles.mux_row) ...
            ', Col ' num2str(handles.mux_col)]);
        return;
    end;
    legend (legleg{:}, 'Location', 'NorthWest');
    grid on;
    axis ([0 axmax(1) axmax(3) axmax(2)]);
    zoom reset;
    xlabel ('I_b / \muA');
    ylabel ('P_s / pJ');
    title (['Row ' num2str(handles.mux_row) ', Col ' num2str(handles.mux_col) ': Power']);
    
    
function plot_r_all (handles)
    hold off;
    cla;
    axmax = [0 0];
    hold on;
    legleg = {};
    CC = 'bgrkcmy';
    SS = '*s+voxd';
    for (onf = 1:length(handles.temp))
        if (onf > length(handles.is_good)) || ~handles.is_good(onf) ...
                || (onf > length(handles.p_s)) || isempty (handles.p_s{onf})
            continue;
        end;
        i_b = handles.i_b{onf};
        i_s = handles.i_s{onf};
        p_s = handles.p_s{onf};
        r_s = handles.r_s{onf};
        legleg = {legleg{:}, [num2str(handles.temp(onf)) ' mK']};
        sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
        plot (i_b*1e6, r_s*1e3, sym);
        axmax(1) = max (axmax(1), max(i_b)*1e6);
        axmax(2) = max (axmax(2), max(r_s)*1e3);
    end;
    if sum (abs (axmax)) == 0
        axis ([0 1 0 1]);
        line ([0 1], [0 1], 'color', 'k');
        line ([0 1], [1 0], 'color', 'k');
        legend off;
        return;
        title (['Row ' num2str(handles.mux_row) ...
            ', Col ' num2str(handles.mux_col)]);
    end;
    legend (legleg{:}, 'Location', 'NorthWest');
    grid on;
    axis ([0 axmax(1) 0 axmax(2)]);
    zoom reset;
    xlabel ('I_b / \muA');
    ylabel ('R_s / m\Omega');
    title (['Row ' num2str(handles.mux_row) ', Col ' num2str(handles.mux_col) ': Resistance']);
    
    
    
function plot_v_all (handles)
    hold off;
    cla;
    axmax = [0 0];
    hold on;
    legleg = {};
    CC = 'bgrkcmy';
    SS = '*s+voxd';
    for (onf = 1:length(handles.temp))
        if (onf > length(handles.is_good)) || ~handles.is_good(onf) ...
                || (onf > length(handles.p_s)) || isempty (handles.p_s{onf})
            continue;
        end;
        i_b = handles.i_b{onf};
        i_s = handles.i_s{onf};
        p_s = handles.p_s{onf};
        r_s = handles.r_s{onf};
        v_s = handles.v_s{onf};
        legleg = {legleg{:}, [num2str(handles.temp(onf)) ' mK']};
        sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
        plot (i_b*1e6, v_s*1e3, sym);
        axmax(1) = max (axmax(1), max(i_b)*1e6);
        axmax(2) = max (axmax(2), max(v_s)*1e3);
    end;
    if sum (abs (axmax)) == 0
        axis ([0 1 0 1]);
        line ([0 1], [0 1], 'color', 'k');
        line ([0 1], [1 0], 'color', 'k');
        legend off;
        title (['Row ' num2str(handles.mux_row) ...
            ', Col ' num2str(handles.mux_col)]);
        return;
    end;
    legend (legleg{:}, 'Location', 'NorthWest');
    grid on;
    axis ([0 axmax(1) 0 axmax(2)]);
    zoom reset;
    xlabel ('I_b / \muA');
    ylabel ('V_s / mV');
    title (['Row ' num2str(handles.mux_row) ', Col ' num2str(handles.mux_col) ': Voltage']);
    
    
    
function plot_p_r_all (handles)
    hold off;
    cla;
    axmax = [0 0];
    hold on;
    legleg = {};
    CC = 'bgrkcmy';
    SS = '*s+voxd';
    for (onf = 1:length(handles.temp))
        if (onf > length(handles.is_good)) || ~handles.is_good(onf) ...
                || (onf > length(handles.p_s)) || isempty (handles.p_s{onf})
            continue;
        end;
        i_b = handles.i_b{onf};
        i_s = handles.i_s{onf};
        p_s = handles.p_s{onf};
        r_s = handles.r_s{onf};
        legleg = {legleg{:}, [num2str(handles.temp(onf)) ' mK']};
        sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
        plot (r_s*1e3, p_s*1e12, sym);
        axmax(1) = max (axmax(1), max(r_s)*1e3);
        axmax(2) = max (axmax(2), max(p_s)*1e12);
    end;
    if (sum (abs (axmax)) == 0)
        axis ([0 1 0 1]);
        line ([0 1], [0 1], 'color', 'k');
        line ([0 1], [1 0], 'color', 'k');
        legend off;
        title (['Row ' num2str(handles.mux_row) ...
            ', Col ' num2str(handles.mux_col)]);
        return;
    end;
    legend (legleg{:}, 'Location', 'NorthWest');
    grid on;
    axis ([0 axmax(1) 0 axmax(2)]);
    zoom reset;
    xlabel ('R_s / m\Omega');
    ylabel ('P_s / pJ');
    title (['Row ' num2str(handles.mux_row) ', Col ' num2str(handles.mux_col) ': P_J(R)']);
    
    
function plot_p_t (handles)
    hold off;
    cla;
    axmax = [0 0];
    hold on;
    legleg = {};
    [p_out r_out] = collate_p_r (handles.r_s, handles.p_s, handles.temp, handles.r_n, 4);
    pp = NaN * ones (size (handles.temp));
    psig = NaN * ones (size (handles.temp));
    for (onf = 1:length(handles.temp))
        if (onf > length(handles.is_good)) || ~handles.is_good(onf) ...
                || (onf > length(p_out)) || isempty (p_out{onf})
            continue;
        end;
        % sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
        % plot (handles.temp(onf), p_out{onf}*1e12, sym);
        % errorbar (handles.temp(onf), nanmedian (p_out{onf})*1e12, nanstd (p_out{onf})*1e12, 'b*', 'LineWidth', 2);
        axmax(2) = max (axmax(2), max(p_out{onf})*1e12);
        pp(onf) = nanmedian (p_out{onf});
        psig(onf) = nanstd (p_out{onf});
    end;
    errorbar (handles.temp, pp*1e12, psig*1e12, 'b*', 'LineWidth', 2);
    % legend (legleg{:}, 'Location', 'NorthEast');
    legend off;
    if sum (abs (axmax)) == 0
        axis ([0 1 0 1]);
        line ([0 1], [0 1], 'color', 'k');
        line ([0 1], [1 0], 'color', 'k');
        title (['Row ' num2str(handles.mux_row) ...
            ', Col ' num2str(handles.mux_col)]);
        return;
    end;
    [M2 M] = g_fit_from_p (handles.temp * 1e-3, pp);
    grid on;
    Trange = [min(handles.temp) max(handles.temp)];
    taxlim(1) = Trange(1)*1.2 - Trange(2)*0.2;
    taxlim(2) = Trange(2)*1.2 - Trange(1)*0.2;
    tt = linspace (taxlim(1), taxlim(2), 100) / 1e3;
    pfit = M(4) * (M(2).^M(3) - tt.^M(3));
    % kap2 = M(1) / (M(3)+1) / M(2)^(M(3));
    % pfit2 = kap2 * (M(2).^(M(3)+1) - tt.^(M(3)+1));
    % pfit2 = M2(4) * (M2(2).^M2(3) - tt.^M2(3));
    for (n = [1])
        M2 = g_fit_from_p (handles.temp * 1e-3, pp, n);
        pfit2 = M2(4) * (M2(2).^M2(3) - tt.^M2(3));
        plot (tt*1e3, pfit2*1e12, 'g-', 'LineWidth', 0.5);
    end;
    plot (tt*1e3, pfit*1e12, 'r--', 'LineWidth', 2);
    plot (handles.temp, pp*1e12, 'b*', 'LineWidth', 2);
    axis ([taxlim 0 axmax(2)*1.5]);
    axax = axis;
    zoom reset;
    text (axax(1)*0.5+axax(2)*0.5, axax(3)*0.1+axax(4)*0.9, ...
        ['G(T_c) = ' num2str(M(1)*1e12,'%.1f') ' pW/K'], 'FontSize', 14);
    text (axax(1)*0.5+axax(2)*0.5, axax(3)*0.2+axax(4)*0.8, ...
        ['G(450mK) = ' ...
        num2str(M(1)*1e12*(0.450/M(2))^(M(3)-1),'%.1f') ' pW/K'], ...
        'FontSize', 14);
    text (axax(1)*0.5+axax(2)*0.5, axax(3)*0.3+axax(4)*0.7, ...
        ['\beta = ' num2str(M(3)-1,'%.2f')], 'FontSize', 14);
    text (axax(1)*0.5+axax(2)*0.5, axax(3)*0.4+axax(4)*0.6, ...
        ['T_c = ' num2str(M(2)*1e3,'%.0f') ' mK'], 'FontSize', 14);
    % text (axax(1)*0.5+axax(2)*0.5, axax(3)*0.4+axax(4)*0.6, ...
    % 	['\kappa = ' num2str(M(4)*1e12,'%.1f') ' pW/K^n'], 'FontSize', 14);
    xlabel ('T_{sub} / mK');
    ylabel ('P / pJ');
    title (['Row ' num2str(handles.mux_row) ', Col ' num2str(handles.mux_col) ': P_J(T_{sub})']);
    
    
function plot_r_t (handles)
    hold off;
    cla;
    axmax = [0 0];
    hold on;
    legleg = {};
    CC = 'bgrkcmy';
    SS = '*s+voxd';
    [p_out r_out] = collate_p_r (handles.r_s, handles.p_s, handles.temp, handles.r_n, 4);
    pp = NaN * size (handles.temp);
    for (onf = 1:length(handles.temp))
        if (onf > length(handles.is_good)) || ~handles.is_good(onf)
            continue;
        end;
        sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
        plot (-1e6, -1e6, sym);
        legleg = {legleg{:}, [num2str(handles.temp(onf)) ' mK']};
        if onf<=length(p_out) && ~isempty (p_out{onf})
            pp(onf) = nanmedian (p_out{onf});
        end;
    end;
    if sum (~isnan (pp)) == 0
        legend off;
        axis ([0 1 0 1]);
        line ([0 1], [0 1], 'color', 'k');
        line ([0 1], [1 0], 'color', 'k');
        title (['Row ' num2str(handles.mux_row) ...
            ', Col ' num2str(handles.mux_col)]);
        return;
    end;
    legend (legleg{:}, 'Location', 'SouthEast');
    
    [M2 M] = g_fit_from_p (handles.temp * 1e-3, pp);
    grid on;
    
    kap = M2(4);
    n = M2(3);
    for (onf = 1:length (handles.temp))
        if (onf>length(handles.is_good)) || ~handles.is_good(onf) || isempty (handles.p_s{onf})
            continue;
        end;
        pp = handles.p_s{onf};
        rr = handles.r_s{onf};
        t_s = handles.temp(onf) / 1e3;
        tt = (pp/kap + t_s.^n) .^ (1/n);
        sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
        plot (real(tt) * 1e3, real(rr) * 1e3, sym);
        axmax(1) = max (axmax(1), max(real(tt*1e3)));
        axmax(2) = max (axmax(2), max(real(rr*1e3)));
    end;
    if axmax(1)==0 || axmax(2) == 0 % sum (abs (axmax)) == 0
        legend off;
        axis ([0 1 0 1]);
        line ([0 1], [0 1], 'color', 'k');
        line ([0 1], [1 0], 'color', 'k');
        return;
    end;
    
    axis ([0 axmax(1) 0 axmax(2)]);
    zoom reset;
    xlabel ('T_{el} / mK');
    ylabel ('R / m\Omega');
    title (['Row ' num2str(handles.mux_row) ', Col ' num2str(handles.mux_col) ': R(T_{el})']);
    
    
    
function plot_wrapper (handles)
    if (handles.onf <= length (handles.temp))
        plot_dataset (handles);
    else
        switch (handles.onf - length(handles.temp))
            case 1, plot_v_all (handles);
            case 2, plot_p_all (handles);
            case 3, plot_r_all (handles);
            case 4, plot_p_r_all (handles);
            case 5, plot_p_t (handles);
            case 6, plot_r_t (handles);
            otherwise, plot_p_all (handles);
        end;
    end;
    
function handles = load_wrapper (h_in)
    if (h_in.onf <= length (h_in.temp))
        handles = load_dataset (h_in);
    else
        handles = load_all_temps (h_in);
    end;
    
function handles = auto_params (h_in)
    handles = h_in;
    
    i_bias = handles.i_bias;
    i_tes = handles.i_tes;
    [is_good, ranges, fj, branch1, msg] = select_iv_branches (i_bias, i_tes);
    handles.is_good = is_good;
    handles.ranges = ranges;
    handles.fj = fj;
    if isempty (ranges)
        handles.branch = branch1;
    else
        handles.branch = [];
    end;
    handles.msg = msg;
    if ~isempty (ranges)
        handles.sc_min = min (i_bias (ranges (1, :)));
        handles.sc_max = max (i_bias (ranges (1, :)));
        handles.bn_min = min (i_bias (ranges (2, :)));
        handles.bn_max = max (i_bias (ranges (2, :)));
        handles.norm_min = min (i_bias (ranges (3, :)));
        handles.norm_max = max (i_bias (ranges (3, :)));
    elseif ~isempty (branch1)
        handles.sc_min = min (i_bias (branch1==1));
        handles.sc_max = max (i_bias (branch1==1));
        handles.bn_min = min (i_bias (branch1>1));
        handles.bn_max = max (i_bias (branch1>1));
        handles.norm_min = min (i_bias (branch1==3));
        handles.norm_max = max (i_bias (branch1==3));
    else
        handles.sc_min = 0;
        handles.sc_max = 0;
        handles.bn_min = 0;
        handles.bn_max = 0;
        handles.norm_min = 0;
        handles.norm_max = 0;
    end;
    % update_range_labels (handles);
    plot_dataset (handles);
    
    
function handles = save_params (h_in)
    handles = h_in;
    fname = handles.flist {handles.onf};
    if (handles.onf>length(handles.dirf)) || isempty(handles.dirf{handles.onf})
        if exist ([fname '_iv_dir'], 'dir')
            D = dirfile_open ([fname '_iv_dir']);
        else
            return;
        end;
    else
        D = handles.dirf{handles.onf};
    end;
    pixname = ['r' num2str(handles.mux_row) 'c' num2str(handles.mux_col)];
    if isempty(dirfile_fields(D,pixname,'regexp'))
        return;
    end;
    
    cut = inrange (handles.i_bias, handles.sc_min, handles.sc_max);
    sc_min = min (find (cut));
    sc_max = max (find (cut));
    cut = inrange (handles.i_bias, handles.bn_min, handles.bn_max);
    bn_min = min (find (cut));
    bn_max = max (find (cut));
    cut = inrange (handles.i_bias, handles.norm_min, handles.norm_max);
    norm_min = min (find (cut));
    norm_max = max (find (cut));
    if isempty (sc_min) || isempty (sc_max)
        sc_min = 0;
        sc_max = 0;
    end;
    if isempty (bn_min) || isempty (bn_max)
        bn_min = 0;
        bn_max = 0;
    end;
    if isempty (norm_min) || isempty (norm_max)
        norm_min = 0;
        norm_max = 0;
    end;
    branches = zeros (size (handles.i_bias));
    branches (sc_min:sc_max) = 1;
    branches (bn_min:bn_max) = 2;
    branches (norm_min:norm_max) = 3;
    ranges = [sc_min sc_max; bn_min bn_max; norm_min norm_max];
    
    D = dirfile_save (D, ['msg_' pixname], '');
    D = dirfile_save (D, ['is_good_' pixname], handles.is_good);
    D = dirfile_save (D, ['branch_' pixname], branches);
    if ~isempty (ranges)
        [cal, r_n, i_b, i_s, p_s, r_s, v_s] = iv_ramp_calc (handles.i_bias, handles.i_tes_raw, handles.R_SH, ranges);
        D = dirfile_save (D, ['cal_' pixname], cal);
        D = dirfile_save (D, ['r_n_' pixname], r_n);
        D = dirfile_save (D, ['i_s_' pixname], i_s);
        D = dirfile_add_field (D, ['i_s_dac_' pixname], 'LINCOM', 1, ...
            ['i_s_' pixname], 2^14 / handles.i_s_cal, 0);
        D = dirfile_save (D, ['r_s_' pixname], r_s);
        D = dirfile_add_field (D, ['v_s_' pixname], 'MULTIPLY', ...
            ['i_s_' pixname], ['r_s_' pixname]);
        D = dirfile_add_field (D, ['p_s_' pixname], 'MULTIPLY', ...
            ['i_s_' pixname], ['v_s_' pixname]);
    else
        D = dirfile_delete_field (D, 'cal', 1);
        D = dirfile_delete_field (D, 'r_n', 1);
        D = dirfile_delete_field (D, 'p_s', 1);
        D = dirfile_delete_field (D, 'r_s', 1);
        D = dirfile_delete_field (D, 'v_s', 1);
        D = dirfile_add_field (D, ['i_s_' pixname], 'LINCOM', 1, ...
            ['fb_' pixname], handles.i_s_cal/2^14, 0);
        D = dirfile_delete_field (D, ['i_s_dac_' pixname]);
    end;
    
    % save ([fname '.iv.mat'], 'temp', 'T', 'i_bias_full');
    D = dirfile_flush (D);
    handles.dirf{handles.onf} = D;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % End of "real" code, beginning of user interface mechanics
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Callbacks for list box with temperatures
    
    % --- Executes on selection change in temp_listbox.
function temp_listbox_Callback(hObject, eventdata, handles)
    % hObject    handle to temp_listbox (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: contents = get(hObject,'String') returns temp_listbox contents as cell array
    %        contents{get(hObject,'Value')} returns selected item from temp_listbox
    
    handles.onf = get (hObject, 'Value');
    if (handles.onf <= length(handles.temp))
        handles = load_dataset (handles);
        activate_fine_controls (handles, 'on');
    else
        handles = load_all_temps (handles);
        activate_fine_controls (handles, 'off');
    end;
    guidata (hObject, handles);
    plot_wrapper (handles);
    
function activate_fine_controls (handles, onoff)
    set (handles.sc_min_edit, 'Enable', onoff);
    set (handles.sc_max_edit, 'Enable', onoff);
    set (handles.sc_min_slider, 'Enable', onoff);
    set (handles.sc_max_slider, 'Enable', onoff);
    set (handles.bn_min_edit, 'Enable', onoff);
    set (handles.bn_max_edit, 'Enable', onoff);
    set (handles.bn_min_slider, 'Enable', onoff);
    set (handles.bn_max_slider, 'Enable', onoff);
    set (handles.norm_min_edit, 'Enable', onoff);
    set (handles.norm_max_edit, 'Enable', onoff);
    set (handles.norm_min_slider, 'Enable', onoff);
    set (handles.norm_max_slider, 'Enable', onoff);
    set (handles.is_good_checkbox, 'Enable', onoff);
    set (handles.is_good_checkbox, 'Visible', onoff);
    set (handles.auto_button, 'Enable', onoff);
    set (handles.save_button, 'Enable', onoff);
    if strcmpi (onoff, 'off')
        set (handles.sc_min_edit, 'String', '');
        set (handles.sc_max_edit, 'String', '');
        set (handles.bn_min_edit, 'String', '');
        set (handles.bn_max_edit, 'String', '');
        set (handles.norm_min_edit, 'String', '');
        set (handles.norm_max_edit, 'String', '');
        set (handles.is_good_checkbox, 'Value', 0);
    end;
    
    % --- Executes during object creation, after setting all properties.
function temp_listbox_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to temp_listbox (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: listbox controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    set(hObject, 'ListboxTop', 1);
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % The following are all the callback functions needed to make the edit
    % boxes & sliders work together.  This is trivial in LabVIEW...
    % Feel free to use as a template for other numeric inputs, but hopefully
    % it shouldn't be necessary to edit this.  No detector physics
    % in this section.
    
    
function sc_min_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to sc_min_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'String') returns contents of sc_max_edit as text
    %        str2double(get(hObject,'String')) returns contents of sc_max_edit as a double
    vv = str2double (get (hObject, 'String'));
    if ~isempty (vv) && vv >= 0
        handles.sc_min = vv * 1e-6;
        guidata (hObject, handles);
    end;
    update_range_all (handles);
    
    % --- Executes during object creation, after setting all properties.
function sc_min_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to sc_min_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    
    % --- Executes on slider movement.
function sc_min_slider_Callback(hObject, eventdata, handles)
    % hObject    handle to sc_min_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    
    vv = get (hObject, 'Value');
    set (hObject, 'Value', 0);
    old_val = handles.sc_min;
    if isempty (old_val)
        return;
    end;
    if (vv < 0)
        new_val = max (handles.i_bias (handles.i_bias < old_val));
        if isempty (new_val)
            new_val = min (handles.i_bias);
        end;
    elseif (vv > 0)
        new_val = min (handles.i_bias (handles.i_bias > old_val));
        if isempty (new_val)
            new_val = max (handles.i_bias);
        end;
    end;
    
    if (vv ~= 0)
        handles.sc_min = new_val;
        update_range_all (handles);
        guidata (hObject, handles);
    end;
    
    % --- Executes during object creation, after setting all properties.
function sc_min_slider_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to sc_min_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
    
    
function sc_max_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to sc_max_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'String') returns contents of sc_max_edit as text
    %        str2double(get(hObject,'String')) returns contents of sc_max_edit as a double
    vv = str2double (get (hObject, 'String'));
    if ~isempty (vv) && vv >= 0
        handles.sc_max = vv * 1e-6;
        guidata (hObject, handles);
    end;
    update_range_all (handles);
    
    % --- Executes during object creation, after setting all properties.
function sc_max_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to sc_max_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    
    % --- Executes on slider movement.
function sc_max_slider_Callback(hObject, eventdata, handles)
    % hObject    handle to sc_max_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    vv = get (hObject, 'Value');
    set (hObject, 'Value', 0);
    old_val = handles.sc_max;
    if isempty (old_val)
        return;
    end;
    if (vv < 0)
        new_val = max (handles.i_bias (handles.i_bias < old_val));
        if isempty (new_val)
            new_val = min (handles.i_bias);
        end;
    elseif (vv > 0)
        new_val = min (handles.i_bias (handles.i_bias > old_val));
        if isempty (new_val)
            new_val = max (handles.i_bias);
        end;
    end;
    
    if (vv ~= 0)
        handles.sc_max = new_val;
        update_range_all (handles);
        guidata (hObject, handles);
    end;
    
    % --- Executes during object creation, after setting all properties.
function sc_max_slider_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to sc_max_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
    
    
function bn_min_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to bn_min_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'String') returns contents of bn_min_edit as text
    %        str2double(get(hObject,'String')) returns contents of bn_min_edit as a double
    vv = str2double (get (hObject, 'String'));
    if ~isempty (vv) && vv >= 0
        handles.bn_min = vv * 1e-6;
        guidata (hObject, handles);
    end;
    update_range_all (handles);
    
    
    % --- Executes during object creation, after setting all properties.
function bn_min_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bn_min_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    
    % --- Executes on slider movement.
function bn_min_slider_Callback(hObject, eventdata, handles)
    % hObject    handle to bn_min_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    vv = get (hObject, 'Value');
    set (hObject, 'Value', 0);
    old_val = handles.bn_min;
    if isempty (old_val)
        return;
    end;
    
    if (vv < 0)
        new_val = max (handles.i_bias (handles.i_bias < old_val));
        if isempty (new_val)
            new_val = min (handles.i_bias);
        end;
    elseif (vv > 0)
        new_val = min (handles.i_bias (handles.i_bias > old_val));
        if isempty (new_val)
            new_val = max (handles.i_bias);
        end;
    end;
    
    if (vv ~= 0)
        handles.bn_min = new_val;
        update_range_all (handles);
        guidata (hObject, handles);
    end;
    
    % --- Executes during object creation, after setting all properties.
function bn_min_slider_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bn_min_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
    
    
function bn_max_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to bn_max_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'String') returns contents of bn_max_edit as text
    %        str2double(get(hObject,'String')) returns contents of bn_max_edit as a double
    vv = str2double (get (hObject, 'String'));
    if ~isempty (vv) && vv >= 0
        handles.bn_max = vv * 1e-6;
        guidata (hObject, handles);
    end;
    update_range_all (handles);
    
    % --- Executes during object creation, after setting all properties.
function bn_max_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bn_max_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    
    % --- Executes on slider movement.
function bn_max_slider_Callback(hObject, eventdata, handles)
    % hObject    handle to bn_max_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    vv = get (hObject, 'Value');
    set (hObject, 'Value', 0);
    old_val = handles.bn_max;
    if isempty (old_val)
        return;
    end;
    
    if (vv < 0)
        new_val = max (handles.i_bias (handles.i_bias < old_val));
        if isempty (new_val)
            new_val = min (handles.i_bias);
        end;
    elseif (vv > 0)
        new_val = min (handles.i_bias (handles.i_bias > old_val));
        if isempty (new_val)
            new_val = max (handles.i_bias);
        end;
    end;
    
    if (vv ~= 0)
        handles.bn_max = new_val;
        update_range_all (handles);
        guidata (hObject, handles);
    end;
    
    % --- Executes during object creation, after setting all properties.
function bn_max_slider_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bn_max_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
    
    
function norm_min_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to norm_min_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'String') returns contents of norm_min_edit as text
    %        str2double(get(hObject,'String')) returns contents of norm_min_edit as a double
    vv = str2double (get (hObject, 'String'));
    if ~isempty (vv) && vv >= 0
        handles.norm_min = vv * 1e-6;
        guidata (hObject, handles);
    end;
    update_range_all (handles);
    
    % --- Executes during object creation, after setting all properties.
function norm_min_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to norm_min_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    
    % --- Executes on slider movement.
function norm_min_slider_Callback(hObject, eventdata, handles)
    % hObject    handle to norm_min_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    vv = get (hObject, 'Value');
    set (hObject, 'Value', 0);
    old_val = handles.norm_min;
    if isempty (old_val)
        return;
    end;
    
    if (vv < 0)
        new_val = max (handles.i_bias (handles.i_bias < old_val));
        if isempty (new_val)
            new_val = min (handles.i_bias);
        end;
    elseif (vv > 0)
        new_val = min (handles.i_bias (handles.i_bias > old_val));
        if isempty (new_val)
            new_val = max (handles.i_bias);
        end;
    end;
    
    if (vv ~= 0)
        handles.norm_min = new_val;
        update_range_all (handles);
        guidata (hObject, handles);
    end;
    
    % --- Executes during object creation, after setting all properties.
function norm_min_slider_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to norm_min_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
    
    
function norm_max_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to norm_max_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'String') returns contents of norm_max_edit as text
    %        str2double(get(hObject,'String')) returns contents of norm_max_edit as a double
    vv = str2double (get (hObject, 'String'));
    if ~isempty (vv) && vv >= 0
        handles.norm_max = vv * 1e-6;
        guidata (hObject, handles);
    end;
    update_range_all (handles);
    
    % --- Executes during object creation, after setting all properties.
function norm_max_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to norm_max_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    
    % --- Executes on slider movement.
function norm_max_slider_Callback(hObject, eventdata, handles)
    % hObject    handle to norm_max_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    vv = get (hObject, 'Value');
    set (hObject, 'Value', 0);
    old_val = handles.norm_max;
    if isempty (old_val)
        return;
    end;
    
    if (vv < 0)
        new_val = max (handles.i_bias (handles.i_bias < old_val));
        if isempty (new_val)
            new_val = min (handles.i_bias);
        end;
    elseif (vv > 0)
        new_val = min (handles.i_bias (handles.i_bias > old_val));
        if isempty (new_val)
            new_val = max (handles.i_bias);
        end;
    end;
    
    if (vv ~= 0)
        handles.norm_max = new_val;
        update_range_all (handles);
        guidata (hObject, handles);
    end;
    
    % --- Executes during object creation, after setting all properties.
function norm_max_slider_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to norm_max_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    
function row_num_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to row_num_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'String') returns contents of row_num_edit as text
    %        str2double(get(hObject,'String')) returns contents of row_num_edit as a double
    
    vv = str2double (get (hObject, 'String'));
    if ~isempty (vv) && vv >= 0
        handles.mux_row = vv;
        handles = load_wrapper (handles);
        guidata (hObject, handles);
        plot_wrapper (handles);
    else
        set (hObject, 'String', {num2str(handles.mux_row)});
    end;
    
    % --- Executes during object creation, after setting all properties.
function row_num_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to row_num_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
function col_num_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to col_num_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'String') returns contents of col_num_edit as text
    %        str2double(get(hObject,'String')) returns contents of col_num_edit as a double
    
    vv = str2double (get (hObject, 'String'));
    if ~isempty (vv) && vv >= 0
        handles.mux_col = vv;
        handles = load_wrapper (handles);
        guidata (hObject, handles);
        plot_wrapper (handles);
    else
        set (hObject, 'String', {num2str(handles.mux_col)});
    end;
    
    
    
    % --- Executes during object creation, after setting all properties.
function col_num_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to col_num_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    
    % --- Executes on slider movement.
function row_num_slider_Callback(hObject, eventdata, handles)
    % hObject    handle to row_num_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    
    vv = get (hObject, 'Value');
    handles.mux_row = handles.mux_row + sign(vv);
    if (handles.mux_row < 0)
        handles.mux_row = 0;
    end;
    set (handles.row_num_edit, 'String', {num2str(handles.mux_row)});
    set (hObject, 'Value', 0);
    handles = load_wrapper (handles);
    guidata (hObject, handles);
    plot_wrapper (handles);
    
    
    % --- Executes during object creation, after setting all properties.
function row_num_slider_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to row_num_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    set (hObject, 'Min', -1);
    set (hObject, 'Max', +1);
    
    
    % --- Executes on slider movement.
function col_num_slider_Callback(hObject, eventdata, handles)
    % hObject    handle to col_num_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    vv = get (hObject, 'Value');
    handles.mux_col = handles.mux_col + sign(vv);
    if (handles.mux_col < 0)
        handles.mux_col = 0;
    end;
    set (handles.col_num_edit, 'String', {num2str(handles.mux_col)});
    set (hObject, 'Value', 0);
    handles = load_wrapper (handles);
    guidata (hObject, handles);
    plot_wrapper (handles);
    
    % --- Executes during object creation, after setting all properties.
function col_num_slider_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to col_num_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end
    set (hObject, 'Min', -1);
    set (hObject, 'Max', +1);
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Generic sorts of callbacks, totally boring
    
    
    % --- Outputs from this function are returned to the command line.
function varargout = iv_gui_OutputFcn(hObject, eventdata, handles)
    % varargout  cell array for returning output args (see VARARGOUT);
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Get default command line output from handles structure
    varargout{1} = handles.output;
    
    
    % --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
    % hObject    handle to FileMenu (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    
    % --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
    % hObject    handle to OpenMenuItem (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    file = uigetfile('*.fig');
    if ~isequal(file, 0)
        open(file);
    end
    
    % --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
    % hObject    handle to PrintMenuItem (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    printdlg(handles.figure1)
    
    % --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
    % hObject    handle to CloseMenuItem (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
        ['Close ' get(handles.figure1,'Name') '...'],...
        'Yes','No','Yes');
    if strcmp(selection,'No')
        return;
    end
    
    delete(handles.figure1)
    
    
    % --- Executes on button press in auto_button.
function auto_button_Callback(hObject, eventdata, handles)
    % hObject    handle to auto_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    handles = auto_params (handles);
    guidata (hObject, handles);
    update_range_all (handles);
    
    
    % --- Executes on button press in save_button.
function save_button_Callback(hObject, eventdata, handles)
    % hObject    handle to save_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    handles = save_params (handles);
    handles = load_wrapper(handles);
    guidata (hObject, handles);
    plot_wrapper(handles);
    
    
    % --- Executes on button press in is_good.
function is_good_checkbox_Callback(hObject, eventdata, handles)
    % hObject    handle to is_good (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Hint: get(hObject,'Value') returns toggle state of is_good
    handles.is_good = get (hObject, 'Value');
    guidata (hObject, handles);
    
    
    % --- Executes on button press in print_button.
function print_button_Callback(hObject, eventdata, handles)
    % hObject    handle to print_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    iv_plots (handles.workdir, fullfile (handles.workdir, 'output'));

        % --- Executes on key press in (almost) any window.
function keypress_Callback(hObject, eventdata)
    % these key combinations will perform certain functions in the gui:
    % ctrl+s     save current settings
    % ctrl+a     auto setup parameters
    % ctrl+p     print plots
    % g          toggle is_good tag
    % s          go to SC branch min edit box
    % shift+s    go to SC branch max edit box
    % b          go to Bias/Normal branch min edit box
    % shift+b    go to Bias/Normal branch max edit box
    % n          go to Normal branch min edit box
    % shift+n    go to Normal branch max edit box
    % r          go to row num edit box
    % c          go to col num edit box
    % up         increment one row up
    % down       increment one row down
    % shift+up   increment one column up
    % shift+down increment one column down
    % ctrl+up    increment one plot view up
    % ctrl+down  increment one plot view down
    
    % NB: ctrl is cmnd on a mac
    
    handles = guidata(hObject);
    
    if ismac, ctrlkey = 'command'; else ctrlkey = 'control'; end
    
    switch (eventdata.Key)
        case 's',
            if isempty(eventdata.Modifier),
                uicontrol(handles.sc_min_edit);
            else
                if strcmpi(eventdata.Modifier{1},'shift'),
                    uicontrol(handles.sc_max_edit);
                elseif strcmpi(eventdata.Modifier{1},ctrlkey),
                    save_button_Callback(handles.save_button,eventdata,handles);
                end
            end
        case 'b',
            if isempty(eventdata.Modifier),
                uicontrol(handles.bn_min_edit);
            elseif strcmpi(eventdata.Modifier{1},'shift'),
                uicontrol(handles.bn_max_edit);
            end
        case 'n',
            if isempty(eventdata.Modifier),
                uicontrol(handles.norm_min_edit);
            elseif strcmpi(eventdata.Modifier{1},'shift'),
                uicontrol(handles.norm_max_edit);
            end
        case 'r',
            if isempty(eventdata.Modifier),
                uicontrol(handles.row_num_edit);
            end
        case 'c',
            if isempty(eventdata.Modifier),
                uicontrol(handles.col_num_edit);
            end
        case 'uparrow',
            if isempty(eventdata.Modifier),
                set(handles.row_num_slider,'value',1);
                row_num_slider_Callback(handles.row_num_slider,eventdata,handles);
            elseif strcmpi(eventdata.Modifier{1},'shift'),
                set(handles.col_num_slider,'value',1);
                col_num_slider_Callback(handles.col_num_slider,eventdata,handles);
            elseif strcmpi(eventdata.Modifier{1},ctrlkey),
                handles.onf = handles.onf-1;
                if handles.onf < 1, handles.onf = 1; end
                set (handles.temp_listbox, 'Value', handles.onf);
                temp_listbox_Callback(handles.temp_listbox,eventdata,handles);
                % uicontrol(handles.temp_listbox);
            end
        case 'downarrow',
            if isempty(eventdata.Modifier),
                set(handles.row_num_slider,'value',-1);
                row_num_slider_Callback(handles.row_num_slider,eventdata,handles);
            elseif strcmpi(eventdata.Modifier{1},'shift'),
                set(handles.col_num_slider,'value',-1);
                col_num_slider_Callback(handles.col_num_slider,eventdata,handles);
            elseif strcmpi(eventdata.Modifier{1},ctrlkey),
                handles.onf = handles.onf+1;
                if handles.onf > numel(handles.templist),
                    handles.onf = numel(handles.templist);
                end
                set (handles.temp_listbox, 'Value', handles.onf);
                temp_listbox_Callback(handles.temp_listbox,eventdata,handles);
                % uicontrol(handles.temp_listbox);
            end
        case 'a',
            if ~isempty(eventdata.Modifier) && strcmpi(eventdata.Modifier{1},ctrlkey),
                auto_button_Callback(handles.auto_button,eventdata,handles);
            end
        case 'p',
            if ~isempty(eventdata.Modifier) && strcmpi(eventdata.Modifier{1},ctrlkey),
                print_button_Callback(handles.print_button,eventdata,handles);
            end
        case 'g',
            if isempty(eventdata.Modifier),
                v = get(handles.is_good_checkbox,'value');
                set(handles.is_good_checkbox,'value',1-v);
                is_good_checkbox_Callback(handles.is_good_checkbox, eventdata, handles);
            end
    end
    