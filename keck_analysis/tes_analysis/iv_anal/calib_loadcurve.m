function [bias, current, calib]=calib_loadcurve(rawbias, rawcurrent)
% calibrate a loaDdcurve

npts=size(rawcurrent, 1);
calib=calib_default();
col=reshape(repmat(0:15, 33, 1), 528, 1);
vref=calib.V_FB1_MAX(col+1)/(2^calib.BITS_FB1);
currentcal=vref./((calib.R_FB1(col+1)+calib.R_WIRE(col+1)).*calib.M_FB1(col+1));
current = rawcurrent.*repmat(currentcal, npts, 1);

vref=calib.V_B_MAX(col+1)/(2^calib.BITS_BIAS);
biascal=vref./(calib.R_BIAS(col+1)+calib.R_WIRE(col+1));
biascurr=single(rawbias).*repmat(biascal, npts, 1);
bias=biascurr*calib.R_SH;
end
