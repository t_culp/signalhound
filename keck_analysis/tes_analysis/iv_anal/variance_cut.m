function mask=variance_cut(rawcurrent, threshold, winsize)
% note bad channels based on the median variance of the current.

x=conv2(ones(1, winsize)/winsize, [1], diff(rawcurrent(:, :)));
y=conv2(ones(1, winsize)/winsize, [1], diff(rawcurrent(:, :)).^2);
z=y-x.^2;
  
m=median(z(1:2000,:));
mask=m>threshold | m == 0;
end
