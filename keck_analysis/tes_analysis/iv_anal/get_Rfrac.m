function [ rfrac, rn, isgood, iset] = get_Rfrac( IVfname, biasset)
% [ rfrac, rn ] = get_Rfrac( IVfname, biasset, calib);
%
% Arguments:
%   IVfname: file name of load curve (assumes iv_analysis already run)
%   biasset: matrix of (num_biases, num_cols) biases in ADU
%   calib: IV_analysis calib structure
%
% Outputs:
%   rfrac: Ratio of R at this bias to Rn
%   rn: Normal resistance
%
% 2010Mar30 JPF

num_rows = 33;
num_cols = 16;
numsteps = size(biasset,1);

% first, load the data from IV curve analysis
% assumes that we have already run iv_analysis.m
% (and done any desired tweaking of the branches)
B = read_mce_biasfile([IVfname '.bias']);
IVfit = dirfile_open([IVfname '_iv_dir']);

% preallocate
isgood = false(num_rows,num_cols); % is the fit good?
rfrac = NaN(num_rows,num_cols,numsteps); % r_tes / r_n
rn = NaN(num_rows,num_cols); % r_n
iset = NaN(num_rows,num_cols,numsteps); % I_tes


% --- 1. IV Analysis ---
disp('-- performing IV analysis --');
for row = 0:(num_rows-1)
    for col = 0:(num_cols-1)
        px = ['r' int2str(row) 'c' int2str(col)];
        
        % see which seem to have been well fit
        isgood(row+1,col+1) = dirfile_get(IVfit,['is_good_' px]);
        
        if isgood(row+1,col+1)
            % only bother if the fit was good...
            
            % extract resistances at these biases
            r_fit = dirfile_get(IVfit,['r_s_' px]);
            branch = dirfile_get(IVfit,['branch_' px]);
            r_fit(branch==1) = 0; % hack the superconducting resistances
            rset = interp1(B.tes_bias,r_fit,biasset(:,col+1));
            
            % convert biases to r/rn
            rn(row+1,col+1) = dirfile_get(IVfit,['r_n_' px]);
            rfrac(row+1,col+1,:) = rset/rn(row+1,col+1);
           
            % calculate TES currents
            iset(row+1,col+1,:) = interp1(B.tes_bias, ...
                dirfile_get(IVfit,['i_s_' px]), biasset(:,col+1));
            
            % remove any garbage
            rfrac(row+1,col+1,rset<-0.05)=NaN;
            rfrac(row+1,col+1,rset>1.5)=NaN;
            
        end
    end
end


end

