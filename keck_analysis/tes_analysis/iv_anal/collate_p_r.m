function [p_out r_out] = collate_p_r (r_s, p_s, T, r_n, n)

% ART 090722 - solve problem of kk=find(..) sometimes producing empty

if iscell (r_n)
	new_rn = Inf;
	for (ii = 1:length(r_n))
		if ~isempty (r_n{ii})
			new_rn = min (r_n{ii}, new_rn);
		end;
	end;
	r_n = new_rn;
end;

r_step = (0:1:100)/100 * r_n;
n_ramps = min (length (r_s), length (p_s));
i1 = [];
i2 = [];
p_interp = [];

for (ii = 1:n_ramps)
	if isempty (r_s{ii}) || isempty (p_s{ii})
		% disp (['Skipping ' num2str(ii) ', no data.']);
		continue;
	end;

	if (p_s{ii}(end) < p_s{ii}(1))
		r_s{ii} = r_s{ii}(end:-1:1);
		p_s{ii} = p_s{ii}(end:-1:1);
	end;
    
	kk = find (r_s{ii} >= 0.05 * r_n);
	if ~isempty (kk)
		first_sample = kk(1);
    else
        p_interp{ii} = [];
		continue;
	end;

	kk = find (r_s{ii} >= 0.95 * r_n);
	if ~isempty (kk)
		last_sample = kk(1)-1;
    else
        last_sample = length(r_s{ii});
	end;

	r_s{ii} = r_s{ii}(first_sample:last_sample);
	p_s{ii} = p_s{ii}(first_sample:last_sample);

	[r_s{ii} kk] = sort (r_s{ii});
	p_s{ii} = p_s{ii}(kk);

	if length (r_s{ii}) < 2
		% disp (['Skipping ' num2str(ii) ', not enough data.']);
		p_interp{ii} = [];
		continue;
	end;

	p_interp{ii} = interp1 (r_s{ii}, p_s{ii}, r_step);

	if isempty (i1)
		i1 = ii;
	elseif isempty (i2)
		i2 = ii;
	end;
end;

p_out = p_interp;
r_out = r_step;
