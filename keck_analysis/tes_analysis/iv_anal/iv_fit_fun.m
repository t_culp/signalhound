function y = iv_fit_fun (A, x)

A([1 3 4 6 7]) = A([1 3 4 6 7]) / 1e6;
x = x / 1e6;
y1 = x*A(2) + A(3);
y2 = A(4) + x ./ (1 + A(5)/2 * (1 + erf((x-A(6))/A(7))));
% SMOOTHSCALE = 1e-7;
SMOOTHSCALE = abs (median (diff (x)));
y = y1 .* 1/2 .* (1 - erf ((x - A(1)) / SMOOTHSCALE)) ...
  + y2 .* 1/2 .* (1 + erf ((x - A(1)) / SMOOTHSCALE));
% y (x > A(1)) = y2 (x > A(1));
y = y * 1e6;
