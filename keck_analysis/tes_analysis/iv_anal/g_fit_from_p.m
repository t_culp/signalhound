function [M, M2] = g_fit_from_p (T_s, p, n)
    % function [M, M2] = g_fit_from_p (T_s, p, n)
    %
    % Fits the TES power plateau p as a function of bath temperature T_s to
    % a curve of the form: p = kap*(T_s^n - T^n), where kap = g/(n T^(n-1))
    %
    % INPUTS:
    %   T_s: vector of temperatures (K)
    %   p:   vector of powers (W)
    %   n:   (optional) fixed value of exponent n.  If n is not specified,
    %        we fit for it.
    %
    % OUTPUTS:
    %   M:   Fitted parameters [g T n kap], fitted in two steps
    %           g: characteristic conductivity
    %           T: characteristic temperature
    %           n: power law exponent
    %           kap = g/(n T^(n-1)): coefficient of power law
    %   M2:  Same, but fitted in one step
    %
    % -----------
    %
    %         RWO  Created
    % 090323  JPF  Added comments, minor cleanup
    % 090722  ART don't try to fit with <3 points
    
    % Starting values for the fit parameters
    T_0 = 500e-3;
    n_0 = 3.6;
    
    % massage inputs
    cc = ~isnan(p) & p~=0;  % select valid power values
    if sum (cc) < 3
        % can't fit to one point
        M = [NaN NaN NaN NaN];
        M2 = M;
        return;
    end;
    g_0 = mean (p(cc)) / mean(T_s(cc));  % initial value for g fit
    p = p(:)';
    T_s = T_s(:)';
    
    % keep n fixed in the fit if told to
    if (nargin >= 3) && ~isempty(n)
        A0 = [g_0*1e12/50 T_0];
        A = lsqcurvefit (@(A, X) PJ_4 (A, X, n/2), A0, T_s(cc), p(cc)*1e12);
        
        g = A(1) / 1e12 * 50;
        T = A(2);
        kap = g / (n * T^(n-1));
        M = [g T n kap];
        M2 = M; % don't bother to re-fit
        return;
    end;
    
    % best linear fit: average conductivity and characteristic temperature
    A0 = [g_0*1e12/50 T_0];
    A1 = lsqcurvefit (@(A, X) PJ_1 (A, X), A0, T_s(cc), p(cc)*1e12);
    disp (A1);
    A1 = abs(A1);
    % now fit for the power law as a deviation from the fit above
    A2 = lsqcurvefit (@(A, X) PJ_2 (A, X, A1), [n_0/2], T_s(cc), p(cc)*1e12);
    
    % Assemble the output vector M
    g = A1(1) / 1e12 * 50;
    T = A1(2);
    n = A2(1) * 2;
    kap = g / (n * T^(n-1));
    M = [g T n kap];
    
    % if requested, re-fit for g, T, and n all at once
    if (nargout >= 2)    
        try
            A0 = [A1 A2];
            A3 = lsqcurvefit (@(A, X) PJ_3 (A, X), A0, T_s(cc), p(cc)*1e12);
        catch
            keyboard; 
        end;
        % Assemble the secondary output vector M2
        g2 = A3(1) / 1e12 * 50;
        T2 = A3(2);
        n2 = A3(3) * 2;
        kap2 = g2 / (n2 * T2^(n2-1));
        M2 = [g2 T2 n2 kap2];
    end;
    
    % =========
    
function Y = PJ_1 (A, X)
    % Linear fit for (g,T) in simplified form P = g (T - T_s)
    g = A(1)*50;
    T = A(2);
    T_s = X;
    Y = g * (T - T_s);
    
function Y = PJ_2 (A, X, C)
    % Fit for (n) in P = (g / n T^(n-1)) (T^n - T_s^n) 
    n = A(1)*2;
    g = C(1)*50;
    T = C(2);
    T_s = X;
    kap = g / (n * T^(n-1));
    
    Y = kap * (T^n - T_s.^n);
    
    
function Y = PJ_3 (A, X)
    % Fit for (g,T,n) in P = (g / n T^(n-1)) (T^n - T_s^n)
    g = A(1)*50;
    T = A(2);
    n = A(3)*2;
    T_s = X;
    kap = g / (n * T^(n-1));
    
    Y = kap * (T^n - T_s.^n);
    
function Y = PJ_4 (A, X, C)
    % Fit for (g,T) in P = (g / n T^(n-1)) (T^n - T_s^n)
    g = A(1)*50;
    T = A(2);
    n = C(1)*2;
    T_s = X;
    kap = g / (n * T^(n-1));
    
    Y = kap * (T^n - T_s.^n);
