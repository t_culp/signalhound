function [A good_fit] = fit_iv_curve (ib, is, do_plots)

cc = ~isnan (is);
ib = ib(cc);
is = is(cc);

xx = 1:length(ib);

% A0 = [1e6*max(ib)/10 1e6*is(end-3) 200/3.4 0.3e3 0.1e3 1e6*is(3)];
A0 = [1e6*max(ib)/10 1 1e6*is(end-3) 1e6*min(is(ib>max(ib)/10)) 100/3.4 1e6*max(ib)/5 1e6*max(ib)/10];
O = optimset ('lsqcurvefit');
O.MaxFunEvals = 1000 * 7;
A = lsqcurvefit (@iv_fit_fun, A0, ib*1e6, is*1e6, [], [], O);

good_fit = inrange (A(1)/1e6, min(ib), max(ib)) ...
         & inrange (A(2), 0.8, 1.2) ...
         & inrange (A(5), 20/3.4, 500/3.4) ...
         & inrange (A(6)/1e6, A(1)/1e6, max(ib));

if (nargin > 2) & (do_plots == 1)
	figure;
	plot (ib, is, 'b.');
	hold on;
	plot (ib, iv_fit_fun (A0, ib*1e6) / 1e6, 'g-');
	plot (ib, iv_fit_fun (A, ib*1e6) / 1e6, 'r-');
end;
