function [g dat plc]=partial_loadcurve(d,p,ch)
% [g dat]=partial_loadcurve(d,p,ch)
%
% g(n,m,1)=pj_ave
% g(n,m,2)=pj(end)
% g(n,m,3)=rnorm
% g(n,m,4)=rdet(end)
%
% dat(n,m) has fields pj, vdet,idet, rdet, and bias
%
% plc has the start and end times
%
% JAB 20100126
% SAS 20110307
%    added calib file differences for Keck and BICEP 
%    Tells the difference based on if it contains hk0
% SAS 20110512
%    fixes b2 and keck differences. and make it loop over pixels  
% SAS 20120328
%    now tweaks the ending (plc.ef) for each receiver.  necessary for 5 rx.  
% KW 20150427
%    modify for BICEP3: rx --> mce, number of columns in MCE = 30
% KW 20150507; get nmcecol from fp_data 

exptn = get_experiment_name();
[p,ind]=get_array_info();
nmcecol = max(p.mce_col)+1;

% find the region of the partial loadcurve - could be done in reduc_initial
[sampratio,samprate]=get_sampratio_rate(d);
plc=find_blk(bitand(d.array.frame.features,2^14),sampratio);

% tweak the ends of plc a bit due to feature timing issue
% finds the return of the user_word to the original value
% Done by MCE since they end at different times
stdword=mode(double(d.mce0.header.user_word));
ll=size(d.mce0.header.user_word,1);
plcefini = plc.ef;
for ii=1:length(plc.sf)
  for mce=unique(p.mce)'
    jj=0;
    plc.ef(ii,mce+1)=plcefini(ii);
    do_while=true;
    while do_while
      jj=jj+1;
      do_while=d.mce0.header.user_word(plc.ef(ii,mce+1)+jj+1,mce+1)~=stdword(mce+1) & (plc.ef(ii,mce+1)+jj+1)<ll;
      if ~do_while && stdword(mce+1)==2560
        % deal with a bug that had default user_word of 2560
        % check if 2560 is part of a bias sweep or part of the baseline
        if (plc.ef(ii,mce+1)+jj+1)<ll && ~all(d.mce0.header.user_word(min(plc.ef(ii,mce+1)+jj+(1:10),ll),mce+1)==2560)
          do_while=true;
        end
      end
    end
    plc.ef(ii,mce+1)=plc.ef(ii,mce+1)+jj;
  end
end

%fix the sizes of the outputs
n=length(plc.s); m=size(d.mce0.data.fb,2);
g=NaN*zeros(n,m,4);
dat(n,m).idet=[]; dat(n,m).vdet=[]; dat(n,m).pj=[]; dat(n,m).rdet=[]; dat(n,m).bias=[];

%Read in the calibration files for the MCE's
if strcmp(exptn,'bicep3')
  calib_file = calib_bicep3_run8;
else
  if isfield(d.antenna0,'hk4')
    calib_file = calib_keck_pole2012;
  elseif isfield(d.antenna0,'hk0')
    calib_file = calib_keck_pole2011;
  else 
    calib_file = calib_bicep2_run8;
  end
end

% standard biases for each column
% use these to determine when partial load curve has finished
std_bias=mode(double(d.mce0.tes.bias));

% don't loop over channels with std_bias==0
chuse=ch(std_bias(p.mce_col(ch)+1+nmcecol*p.mce(ch))~=0);

% cut initial frames to get around tes not biased at plc.sf, and random jumps sets to 2560 around plc.ef
% may not be needed in some cases; but doesn't affect plc fit overall
if strcmpi(exptn,'bicep3')
  if stdword(1) == 2560 
    nframescut = 30;
    nframesend = 8;
  else
    nframescut = 0;
    nframesend = 0;
  end
else
  nframescut = 0;
  nframesend = 0;
end

for j=chuse

  for ii=1:length(plc.sf)
    % deglitching sometimes NaN's out so much data
    % reduction of the plc is not possible
    try
      mce=p.mce(j);
      fb=d.mce0.data.fb(plc.sf(ii)+nframescut:plc.ef(ii,mce+1),j);
      jumps=double(d.mce0.data.numfj(plc.sf(ii)+nframescut:plc.ef(ii,mce+1),j));
      bias=double(d.mce0.header.user_word(plc.sf(ii)+nframescut:plc.ef(ii,mce+1),p.mce(j)+1));

      % Cut out any dropped portion
      dropcut=(fb==0 & jumps==0 & bias==0 | isnan(fb) | isnan(bias));
      fb=fb(~dropcut); fb=fb(1:end-nframesend);
      jumps=jumps(~dropcut); jumps=jumps(1:end-nframesend);
      bias=bias(~dropcut);bias=bias(1:end-nframesend);

      bias(bias>20000)=0;  %zeros out where the clock card is still recording
      [max_bias max_biasindx]=max(bias);
      min_bias=std_bias(p.mce_col(j)+1+p.mce(j)*nmcecol);
      min_biasindx=find(bias(max_biasindx:end)<=min_bias);
      min_biasindx=min_biasindx(1)+max_biasindx-1;
      

      if mean(fb)~=0
        fb=fb(max_biasindx:min_biasindx);
        jumps=double(jumps(max_biasindx:min_biasindx));
        bias=double(bias(max_biasindx:min_biasindx));
        [pj_ave, pj, vdet, idet, rdet, rnorm, bias]=loadcurve_detparams(fb,bias,p.mce_col(j),0,jumps,calib_file);
  
        g(ii,j,1)=pj_ave;
        g(ii,j,2)=pj(end);
        g(ii,j,3)=rnorm;
        g(ii,j,4)=rdet(end);
        dat(ii,j).pj=pj;
        dat(ii,j).vdet=vdet;
        dat(ii,j).idet=idet;
        dat(ii,j).rdet=rdet;
        dat(ii,j).bias=bias;
      end
    catch
      g(ii,j,1)=NaN;
      g(ii,j,2)=NaN;
      g(ii,j,3)=NaN;
      g(ii,j,4)=NaN;
      dat(ii,j).pj=NaN;
      dat(ii,j).vdet=NaN;
      dat(ii,j).idet=NaN;
      dat(ii,j).rdet=NaN;
      dat(ii,j).bias=NaN;
    end
  end
end
