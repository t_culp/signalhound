function [res, ranges, fj, branch1, msg] = select_iv_branches (i_bias, i_tes)
    % SELECT_IV_BRANCHES matches patterns in IV curves to select
    % the best superconducting, normal, and biased regions.  It
    % recognizes traces in which there's no transition or no good
    % biased region, and avoids flux jumps.
    %
    % [E, R] = SELECT_IV_BRANCHES (IB, IS)
    %
    % IB = bias current (through bias resistor)
    % IS = sensor current (through TES)
    %
    % E = 1 for good data; 0 on any error
    % R = index ranges for good superconducting, bias/normal,
    %     and normal regions.  This is an array of minimum
    %     and maximum indices:
    %          [SC_MIN      SC_MAX
    %           BN_MIN      BN_MAX
    %           NORM_MIN  NORM_MAX]
    %     The normal region is a subset of the bias/normal
    %     branch, which should be linear (Ohmic).
    %
    % There are also several optional outputs:
    %
    % [E, R, F, B, M] = SELECT_IV_BRANCHES (...)
    %
    % F = list of indices where flux jumps are identified
    % B = first pass at tagging branches: classify each sample
    %     Codes:   1 for superconducting branch
    %              2 for biased / normal branch
    %              3 for linear normal region, to be used
    %              for fitting normal-state resistance.
    % M = message describing any problem with the data
    
    % RWO 080501
    % RWO 080513 Refactor output variables
    % RWO 080514 Separate error code / error message
    %            Improve bias/norm region selection
    % JPF 090123 Alternate bias/norm region selection 
    %            based upon filtered derivatives
    % ART 090729 Use median slope comparison to improve branch
    %           discrimination and avoid triggering on glitches
    % JPF 091006 Slight tweaks to range-finding; disable "bias region too
    %            small", since it bombs out on some large squid jumps.
    
    i_bias = i_bias(:);
    i_tes = i_tes(:);
    
    res = logical (0);
    msg = '';
    ranges = [];
    sc_range = [];
    bn_range = [];
    norm_range = [];
    if (nargout >= 3)
        fj = [];
    end;
    if (nargout >= 4)
        branch1 = [];
    end;
    
    %if is_unlocked (i_bias, i_tes)
    %    msg = 'Channel not locked';
    %    return;
    %end;
    
    if (length (unique (i_tes)) <= 1)
        msg = 'No signal';
        return;
    end;
    
    if (1)
        % A TES's differential resistance (slope of IV curve) is positive when
        % it acts resistively (normal or superconducting) but negative when in
        % the bias regime.  We thus identify the bias regime(s) by the negative
        % slope of the IV curve, calculated using a smoothed estimator.
        % 090123 - JPF
        
        if length(unique(i_tes))<=1
            msg = 'No signal';
            return;
        end
        
        % choose a smoothing window size for the fine derivative
        %smooth_window = max([4 ceil(length(i_tes)/100)]); % default
        smooth_window = max([10 ceil(length(i_tes)/50)]); % default
        
        % compute the smoothed derivative (fine resolution)
        di_fine = movingslope(i_tes,smooth_window,2);
        
        % identify critical points (sign changes in the derivative)
        % (Note: identifies point just BEFORE the sign change)
        crit_pts = find(sign(di_fine(1:(end-1))) ~= sign(di_fine(2:end)));
        
        % remove any critical points very close to one another
        if (0)
            min_diff = 5;
            crit_ok = 0;
            while ~crit_ok
                dcp = diff(crit_pts);
                bad_ind = find(dcp<min_diff,1);
                if length(bad_ind)>0
                    crit_pts(bad_ind + [0,1])=[];
                end
            end
        end
            
        if length(crit_pts)<2
            % we don't have two cusps, so we have no bias region
            msg = 'Bias region too small';
            return;
        % identify the last (lowest bias) critical point
        %   this must be the SC-bias transition (the obvious one)
        elseif length(crit_pts)==2
            crit_peak = crit_pts(2);
        else
            crit_peak = crit_pts(end);
            % avoid triggering on SQUID jumps or glitches in SC region...
            med_slp_sc=median(di_fine([crit_pts(end):length(i_tes)]));
            for i=length(crit_pts)-1:-1:1
                med_slp=median(di_fine([crit_pts(i):crit_pts(i+1)]));
                if ( 0.8<med_slp/med_slp_sc && med_slp/med_slp_sc<1.2)
                    crit_peak=crit_pts(i);
                end;
             end;
        end;
        ranges(1,:) = [crit_peak length(i_bias)]; % superconducting region
        
        if length(crit_pts)<2
            % we don't have two cusps, so we have no bias region
            msg = 'Bias region too small';
            return;
        elseif length(crit_pts)==2
            crit_valley=crit_pts(1);
            ranges(2,:) = [1 crit_peak]; % bias/normal
            ranges(3,:) = [1 crit_valley]; % normal
        else
            % find the real normal region
            crit_end=1;
            crit_valley=crit_pts(1);
            crit_top=crit_peak;
            for i=1:find(crit_pts<crit_peak,1,'last')-1
                med_slp = median(di_fine([crit_pts(i):crit_pts(i+1)]));
                similar_slp = find(1.1>di_fine([crit_pts(i):crit_pts(i+1)])/med_slp>0.9);
                if (length(similar_slp)>0.8*(crit_pts(i+1)-crit_pts(i)))
                    crit_end=crit_pts(i)+1;
                    crit_valley=crit_pts(i+1);
                end;
            end;
            ranges(3,:) = [crit_end crit_valley]; % normal
            crit_top=crit_valley;
            while abs(di_fine(crit_top))<abs(med_slp_sc)
                crit_top=crit_top+1;
            end
            crit_top=crit_top-3;
            ranges(2,:) = [crit_end crit_top]; % bias/normal
        end;
        
        % shrink the ranges slightly to avoid possible badness
        % drop the first and last bits of the norm/bias region
        len_2 = diff(ranges(2,:));
        %ranges(2,1) = min(ranges(2,1)+5,ranges(2,2)+floor(len_2/10)); % drop highest-bias bit
        if diff(ranges(2,:))<1
            ranges(2,2)=ranges(2,1);
        end
        ranges(3,1) = ranges(2,1);
        ranges(3,2) = ranges(3,2)-floor(len_2/3); % drop lowest-bias bit
        if diff(ranges(3,:))<1
            ranges(3,2)=ranges(3,1);
        end
        
        % check sizes of ranges
        if abs(diff(ranges(1,:)))<2
            msg = 'SC region too small.';
            return;
        end;
        if (ranges(2,2)-ranges(3,2))<20
            msg = 'Bias region too small.';
            return;
        end
        if diff(ranges(3,:))<10
            msg = 'Normal region too small.';
            return;
        end;
        
        B = zeros(size(i_tes));
        B(ranges(1,1):ranges(1,2))=1;
        B(ranges(2,1):ranges(2,2))=2;
        B(ranges(3,1):ranges(3,2))=3;
        
        res = 1;        
        
    end;
    
    if (0)
        % identify relevant regions with a global fit
        [A g] = fit_iv_curve (i_bias, i_tes);
        [snap snapidx] = max (i_tes (i_bias < A(6)/1e6));
        snapidx = snapidx + min (find (i_bias < A(6)/1e6)) - 1;
        [turn turnidx] = min (abs (i_bias - A(6)/1e6));
        normidx = length(i_tes) - 1.5 * (length(i_tes)-turnidx);
        if (length(i_tes)-snapidx < 7)
            msg = 'SC region too small.';
            return;
        end;
        if isempty(snapidx) | (turnidx < 20) || (snapidx-turnidx < 20)
            msg = 'Bias region too small';
            return;
        end;
        if (normidx < 10)
            msg = 'Normal region too small';
            return;
        end;
        ranges(1,:) = [snapidx+1 length(i_tes)];
        ranges(2,:) = [1 snapidx-1];
        ranges(3,:) = round ([1 length(i_tes)-1.5*(length(i_tes)-turnidx)]);
        res = (g == 1);
        return;
    end;
    
    if (0)
        % identify the turnaround peaks in the IV curve
        [snap snapidx] = max (prominence (i_tes, 5));
        [turn turnidx] = max (prominence (-i_tes, 5));
        normidx = length(i_tes) - 1.5 * (length(i_tes)-turnidx);
        if (length(i_tes)-snapidx < 7)
            msg = 'SC region too small.';
            return;
        end;
        if (turnidx < 20) || (snapidx-turnidx < 20)
            msg = 'Bias region too small';
            return;
        end;
        if (normidx < 10)
            msg = 'Normal region too small';
            return;
        end;
        ranges(1,:) = [snapidx+1 length(i_tes)];
        ranges(2,:) = [1 snapidx-1];
        ranges(3,:) = round ([1 length(i_tes)-1.5*(length(i_tes)-turnidx)]);
        res = logical (1);
        return;
    end;
    
    if (0)
        % find turnaround peaks, searching within approximate windows
        ss = floor (length (i_tes) * 4/5);
        [snap snapidx] = max (i_tes(ss:end));
        snapidx = snapidx + ss - 1;
        [turn turnidx] = min (i_tes(1:snapidx-20));
        normidx = length(i_tes) ...
            - 1.5 * (length(i_tes)-turnidx);
        try
            if (length(i_tes)-snapidx < 5) || (snapidx-ss < 5)
                msg = 'SC region too small.';
                keyboard;
                return;
            end;
            if (turnidx < 5) || (snapidx-20-turnidx < 5)
                msg = 'Bias region too small';
                return;
            end;
            if (normidx < 10)
                msg = 'Normal region too small';
                return;
            end;
        catch, keyboard;
        end;
        ranges(1,:) = [snapidx+1 length(i_tes)];
        ranges(2,:) = [1 snapidx-1];
        ranges(3,:) = round ([1 length(i_tes)-1.5*(length(i_tes)-turnidx)]);
        res = logical (1);
        return;
    end;
    
    i_sig = est_noise_level (i_tes);
    chunk = chunk_data (i_tes, i_sig);
    
    if (0)
        % final code from Walter
        [branch msg] = guess_branches (i_tes, i_sig);
        if (nargout >= 4)
            branch1 = branch;
        end;
        if ~isempty (msg)
            return;
        end;
        
        [sc_range bn_range] = find_best_branches (i_tes, chunk, branch);
        if isempty(sc_range)
            msg = 'No good superconducting branch found';
            return;
        end;
        if isempty (bn_range)
            msg = 'No good biased / normal branch found';
            return;
        end;
        
        norm_range = safe_normal_region (i_bias, i_tes, bn_range);
        if isempty(norm_range)
            msg = 'No safe normal region found';
            return;
        end;
        
        ranges = [sc_range(:)'; bn_range(:)'; norm_range(:)'];
        res = logical (1);
    end
    
    if (nargout >= 3)
        fj = find (diff (chunk) > 0) + 1;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
function r = is_unlocked (i_bias, i_tes)
    
    % channel is not locked if TES current
    % slews faster than bias current!
    r = (max(i_tes) - min(i_tes)) > (max(i_bias) - min(i_bias));
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
function norm_range = safe_normal_region (i_bias, i_tes, bn_range)
    
    i_bias = abs (i_bias);
    
    % Consider "safe" normal region to start at 2.0x the bias
    % at which the load curve peaks.
    
    [i_pk n_pk] = max (i_tes (bn_range(1):bn_range(2)));
    n_pk = n_pk + (bn_range(1)-1);
    
    safe_rgn = zeros(size(i_bias));
    safe_rgn (bn_range(1) : bn_range(2)) = 1;
    safe_rgn = safe_rgn & (i_bias >= 2.0*i_bias(n_pk));
    
    if sum (safe_rgn) < 2
        warning (['Insufficient flux-jump-free normal region found!']);
        disp (['Will try anyway using small normal region.']);
        safe_rgn = zeros (size (i_bias));
        safe_rgn (bn_range(1) : bn_range(2)) = 1;
        safe_rgn = safe_rgn & (i_bias >= 1.125*i_bias(n_pk));
    end;
    
    if sum (safe_rgn) <= 2
        norm_range = [];
        return;
    end;
    
    % Check for slope changes such as another transition!
    safe_1 = max (find (safe_rgn));
    safe_slp = mean (diff (i_tes ((safe_1-10) : safe_1)));
    safe_icpt = i_tes (safe_1);
    isig = percentile (diff (i_tes (safe_rgn)), [0.1 0.9]);
    isig = diff(isig) / 1.8;
    discrep = abs (i_tes(:) - safe_icpt - ((1:length(i_tes))' - safe_1) * safe_slp) / isig;
    safe_rgn_2 = safe_rgn & discrep < 100;
    safe_rgn_2 = find (safe_rgn);
    
    norm_range = [min(safe_rgn_2) max(safe_rgn_2)];
    
    % keyboard;
    
    if 0
        % Take special care to eliminate flux jumps in normal region
        d1 = diff (i_tes);
        d1mean = mean (d1 (norm_range(1) + 1 : norm_range(2) - 1));
        d1sig = percentile (d1 (norm_range(1) + 1 : norm_range(2) - 1), [0.1 0.9]);
        d1sig = (d1sig(2) - d1sig(1)) / 1.8;
        fj = find (abs (d1 - d1mean) > d1sig * 4);
        fj = fj (fj > norm_range(1) & fj < norm_range(2));
        if ~isempty (fj)
            fj = max (fj);
            disp (['Cutting end of normal range from ' num2str(norm_range(2)) ' to ' num2str(fj)]);
            norm_range (1) = fj;
        end;
    end;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
function [sc_range bn_range] = find_best_branches (i_tes, chunk, branch);
    
    maxchunk = max (chunk);
    
    % Find largest region of SC branch with no FJs
    sc_chunk = -1;
    sc_length = 0;
    for (ii = 0:maxchunk)
        if (sum (chunk==ii & branch==1) > max (sc_length, 10))
            sc_chunk = ii;
            sc_length = sum (chunk == ii & branch==1);
        end;
    end;
    
    % Find "largest" region of biased + normal branch
    % with no FJs.
    % "Largest" is an empirical rule of thumb.
    bn_chunk = -1;
    bn_score = 0;
    for (ii = 0:maxchunk)
        n_norm = cumsum (chunk==ii & branch==3);
        tmp = (chunk==ii & branch==2);
        tmp = cumsum (tmp(end:-1:1));
        n_bias = tmp(end:-1:1);
        qual_score = max (n_norm .* n_bias);
        % n_bias = sum (chunk==ii & branch==2);
        % n_norm = sum (chunk==ii & branch==3);
        % qual_score = n_bias * min (n_norm, 3*n_bias);
        
        if (qual_score > max (bn_score, 100))
            bn_chunk = ii;
            bn_score = qual_score;
        end;
    end;
    
    sc_range = [min(find(chunk==sc_chunk&branch==1)) max(find(chunk==sc_chunk&branch==1))];
    bn_range = [min(find(chunk==bn_chunk&branch>1)) max(find(chunk==bn_chunk&branch>1))];
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Estimate the noise level in TES current
    
function i_sig = est_noise_level (i_tes)
    
    dd = diff (diff (i_tes));
    dd_1090 = percentile (dd, [0.1 0.9]);
    dd_1090 = dd_1090(2) - dd_1090(1);
    i_sig = dd_1090 / 1.8;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Guess which parts are superconducting, normal,
    % and biased branches based on slope.
    % Returns 1 for SC branch, 2 for biased, 3 for normal,
    % zero for no classification.
    
function [branch res] = guess_branches (i_tes, i_sig)
    
    sl = diff (i_tes);
    
    branch = [];
    res = '';
    
    sl_tmp = sort (sl);
    sc_cutoff = sl_tmp(end-50) / 1.5;
    
    if sum (sl > sc_cutoff) < 10 % 4*i_sig) < 10
        res = 'No superconducting branch found';
        return;
    elseif sum (sl < sc_cutoff) < 10 % 4*i_sig) < 10
        res = 'No biased / normal branch found';
        return;
    end;
    
    sl_sc_range = percentile (sl (sl > sc_cutoff), [0.1 0.9]);
    sl_normbias_range = percentile (sl (sl < sc_cutoff), [0.1 0.9]);
    sl_sc = median (sl (sl > sc_cutoff));
    sl_norm = median (sl (sl < sc_cutoff));
    
    sl_norm_sig = sl_normbias_range(2) - sl_norm;
    sl_sc_sig = max (sl_sc_range(2)-sl_sc, sl_sc-sl_sc_range(1));
    
    branch = zeros (size (i_tes));
    
    % Superconducting branch
    c_sc = (abs (sl - sl_sc) <= sl_sc_sig * 1.5);
    c_sc = ([c_sc; 0] & [0; c_sc]);
    branch (c_sc) = 1;
    
    % Normal branch
    c_norm = inrange (sl, -sl_norm_sig, sl_norm + sl_norm_sig*3);
    c_norm = ([c_norm; 0] & [0; c_norm]);
    branch (c_norm) = 3;
    
    % Biased region
    c_bias = inrange (sl, sl_norm + (sl_normbias_range(1) - sl_norm)*50, -sl_norm_sig);
    c_bias = ([c_bias; 0] & [0; c_bias]);
    branch (c_bias) = 2;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Divide data up into chunks separated by flux jumps.
    % Each chunk has no flux jumps inside it.
    
function chunk = chunk_data (i_tes, i_sig)
    
    
    if 1
        N_SIG_LIMIT = 6; % 3;
        
        d1 = diff(i_tes(:));
        d_comp = 1/2 * ([0; 0; d1(1:(end-1))] + [d1(2:end); 0; 0]);
        d2 = [0; diff(diff(i_tes(:))); 0];
        fj_list = abs(d2) >= (abs(d_comp) + 2*i_sig) * N_SIG_LIMIT;
        
        chunk = cumsum (fj_list);
    else
        % This number is far more hand-tuned than I would like...
        N_SIG_LIMIT = 10;
        
        d1 = [0; diff(i_tes(:))];
        d2 = [0; diff(diff(i_tes(:))); 0];
        fj_list = (d1 < 0 & abs(d2) > N_SIG_LIMIT*i_sig) | (d1 >= 0 & abs(d2) > N_SIG_LIMIT*i_sig/2);
        % fj_list = [0; (abs(d2)>N_SIG_LIMIT*i_sig); 0];
        
        chunk = cumsum (fj_list);
    end;
