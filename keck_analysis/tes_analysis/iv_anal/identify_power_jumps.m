function [pjump_idx, minr]=identify_power_jumps(power, res, ...
    transition_idx, instability_idx)
% detect 10 sigma jumps in power in the transition
% note the index of this jump
nchan=size(power, 2);
minr=ones(1, nchan);
pjump_idx=uint32(zeros(1, nchan));

for i=1:nchan
  if transition_idx(i) == 0 
    minr(i)=min(res(:, i));
    continue;
  end
  if transition_idx(i) == instability_idx(i)
    minr(i)=min(res(:, i));
    continue;
  end
  if instability_idx(i)==0 
    transpow=power(transition_idx(i):end, i);
    transres=res(transition_idx(i):end, i);
  else
    transpow=power(transition_idx(i):instability_idx(i), i);
    transres=res(transition_idx(i):instability_idx(i), i);
  end  
  
  pdiff=diff(transpow);
  med=median(pdiff);
  % robust standard deviation, not affected by outliers
  med2=sqrt(median((pdiff-med).^2));
  % flag any points as bad if they are more than 10 sigma from the mean
%  keyboard
  badpts=(pdiff > med+25*med2);
  if(sum(badpts)>0)
    tmp=min(find(badpts));
    minr(i)=min(transres(1:tmp));
    pjump_idx(i)=tmp+transition_idx(i);
  else
    minr(i)=min(transres);
  end
end
