function [mask,meanres]=slope_cut(normres)
  slope=1./normres;
  nchan=length(normres);
  g=isfinite(normres);
  ntiles=nchan/132;
  mask=logical(zeros(1, nchan));
  meanres=zeros(1, ntiles);
  for i=1:ntiles
    tidx=(1:132)+132*(i-1);
    tg=g(tidx);
    r=normres(tidx(tg));
    sl=1./r;
    % first cut: identify any outliers with 5x the mean resistance
    mr=mean((r(isfinite(r))));
    bad=(r > 5*mr | ~isfinite(r) | r < 0);
    % generate statistics with this first cut
    find(bad);
    mr=mean(r(~bad));
    sr=std(r(~bad));
    % also generate statistics for 1/r to get rid of low resistnace guys
    msl=abs(mean(sl(~bad)));
    ssl=std(sl(~bad));
    cut=(r < (mr+5*sr)) & (r > (mr-5*sr)) & (r > 0);
    cut2=(sl < (msl+5*ssl)) & (sl > (msl-5*ssl));
    mask(tidx(tg))=~cut | ~cut2;
    meanres(i)=mean(r(cut & cut2));
  end 
end
