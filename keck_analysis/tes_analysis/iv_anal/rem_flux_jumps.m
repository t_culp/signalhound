% REM_FLUX_JUMPS (FB, PHI0)
%
% Removes MCE flux jumping from feedback signal
% if PHI0~=0, jumps are every PHI0
% if PHI0=0, jumps are in middle of digitizer range
%

function fb_clean = rem_flux_jumps (fb_dirty, phi0)

enbl = (phi0>0);
    
if enbl
	fb_wrap = mod (fb_dirty(:)/phi0 + 0.51, 1);
	NFIT = 6;
else
	fb_wrap = mod (fb_dirty, 2^14);
	NFIT = 5;
end;
fb_wr_next = [fb_wrap(2:end); 0];
pidx = find (inrange (fb_wrap, 0.4, 0.5) & inrange (fb_wr_next, 0.5, 0.6));
pidx = pidx (inrange (pidx, NFIT+1, length(fb_wrap)-NFIT));
nidx = find (inrange (fb_wrap, 0.5, 0.6) & inrange (fb_wr_next, 0.4, 0.5));
nidx = nidx (inrange (nidx, NFIT+1, length(fb_wrap)-NFIT));

if (enbl)
	pidx (pidx == length(fb_dirty)) = [];
	nidx (nidx == length(fb_dirty)) = [];

	cmask = zeros (size (fb_dirty));
	cmask (pidx+1) = 1;
	cmask (nidx+1) = 1;
	cmask = cumsum(cmask);
	cmask = cmask - [0; 0; 0; 0; 0; cmask(1:(end-5))];
	cmask = (cmask ~= 0);

	pidx(cmask(pidx)) = [];
	nidx(cmask(nidx)) = [];
end;

% figure;
% plot (fb_wrap, 'b.');
% hold on;

O = optimset ('lsqcurvefit');
O.Display = 'off';

jj = [];
kk = [];

if (enbl)
	jumpfun = @jumpfun1;
else
	jumpfun = @jumpfun0;
end;

for (ii = 1:length (pidx))
	A0 = [0 1e-2 0.05 0];
	A = lsqcurvefit (jumpfun, A0, -NFIT:NFIT, fb_wrap(pidx(ii) + (-NFIT:NFIT))' - 0.5, [], [], O);
	jj(ii) = A(3);
	ofsp(ii) = A(4);
	% plot (pidx(ii) + (-NFIT:NFIT), jumpfun (A, -NFIT:NFIT), 'r-');
end;

for (ii = 1:length (nidx))
	A0 = [0 -1e-2 -0.05 0];
	A = lsqcurvefit (jumpfun, A0, -NFIT:NFIT, fb_wrap(nidx(ii) + (-NFIT:NFIT))' - 0.5, [], [], O);
	kk(ii) = -1 * A(3);
	ofsn(ii) = A(4);
	% plot (nidx(ii) + (-NFIT:NFIT), jumpfun (A, -NFIT:NFIT), 'r-');
end;

jumpsize = median ([jj kk]);
if isnan (jumpsize)
	fb_clean = fb_dirty;
	return;
end;

if (~enbl)
	fb_clean = fb_wrap;
	fb_clean (fb_clean <= 0.5) = fb_clean (fb_clean <= 0.5) - jumpsize;
	nofs = floor (fb_dirty(:));
	fb_clean = fb_clean + nofs * (1 - jumpsize);
else
	jump = zeros (size (fb_dirty));
	for (ii = 1:length(jj))
		jump(pidx(ii)+NFIT+floor(ofsp(ii))) = jumpsize;
	end;
	for (ii = 1:length(kk))
		jump(nidx(ii)+NFIT+floor(ofsn(ii))) = -jumpsize;
	end;
	fb_clean = fb_dirty + phi0 * cumsum (jump);
end;

if sum (isnan (fb_clean)) > 0
	keyboard;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function y = inrange (x, a, b)
	y = (x >= a) & (x <= b);

function y = jumpfun1 (A, x)
	y = A(1) + A(2)*x + A(3)/2*erf(0.5*(x - A(4)));

function y = jumpfun0 (A, x)
        y = A(1) + A(2)*x + A(3)/2*erf(5*(x - A(4)));


