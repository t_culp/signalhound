function [pj_ave, pj, vdet, idet, rdet, rnorm, bias]=loadcurve_detparams(fb_data,tbias_data,col,plots,numfjs,calib_file)
% [pj_ave, pj, vdet, idet, rdet, rnorm, bias]=loadcurve_detparams(fb_data,tbias_data,plot,calib_file)
%
% INPUTS:
% fb_data = fb_data, correctly scaled for data_mode and filter gain
% tbias_data = applied detector bias data
% col = mce column of detector
% plots:  0=no 1=yes (optional)
% calib_file: calibration file (optional)
%
% OUTPUTS:
% pj_ave = average joule power of detector in transition (W)
% pj = vector of detector joule power at each tbias point (W)
% vdet = vector of detetor voltage at each tbias point (V)
% idet = vector of detector current at each tbias point (A)
% rdet = vector of detector resistance at each tbias point (Ohms)
% rnorm = detector normal resistance (Ohms)
%
% JAB 20100126
% SAS 20110307
%   added optionality for calibration file

if nargin<=2; plot_stuff=0; else plot_stuff=plots; end

bias=tbias_data;
fb=-1*fb_data;

%Need to apply calibration before computing the branches because
%of the differences in the windings between Keck and BICEP2. SAS.

if nargin<=5
  calib=calib_bicep2_run8();
else
  calib=calib_file();
end

% compute calibration factors from these circuit parameters
input_current = fb*(calib.V_FB1_MAX(col+1)/((2^calib.BITS_FB1)*(calib.R_FB1(col+1)+calib.R_WIRE(col+1))*calib.M_FB1(col+1)));
tbias_current=bias.*(calib.V_B_MAX(col+1)./((2^calib.BITS_BIAS)*(calib.R_BIAS(col+1))));

[sc_val, sc_branch]=min(input_current);
if sc_branch==1
  pj_ave=NaN;
  pj=NaN*ones(size(bias));
  vdet=pj;
  idet=pj;
  rdet=pj;
  rnorm=NaN;
  return
end

sblen=length(1:sc_branch);
sbfract=.50;
sb_end=round(sbfract*sblen);
sb_start=1;

p=polyfit(tbias_current(sb_start:sb_end),input_current(sb_start:sb_end),1);

input_current=input_current-p(2);

p=polyfit(tbias_current(sb_start:sb_end),input_current(sb_start:sb_end),1);
yfit=polyval(p,[0 max(tbias_current)]);
yfitdiff=polyval(p,tbias_current);

rdet=tbias_current.*(calib.R_SH./input_current)-calib.R_SH;
vdet=rdet.*input_current;
pj=vdet.*input_current;
idet=input_current;
rnorm=calib.R_SH*((1/p(1))-1);

jumpindx=find(abs(diff(numfjs))>=.99);
%indx=find(rdet<=.98*rnorm);
indx=[floor((1-.05)*length(rdet)):length(rdet)];

% if sum(indx==0); keyboard; end
pj_ave=mean(pj(indx));

if plot_stuff
h1=make_fig;
subplot(211)
plot(tbias_current*1e6,input_current*1e6,'k');hold on;
plot(tbias_current(sb_start:sb_end)*1e6,input_current(sb_start:sb_end)*1e6,'g');
plot([0 max(tbias_current)]*1e6,yfit*1e6,'r--');
plot(tbias_current(sb_start:sb_end)*1e6,input_current(sb_start:sb_end)*1e6,'g');
ylabel('detector current (uA)');
set(gca,'YLim',[0 max(input_current*1e6)]);
title(['LOAD CURVE']);
h=legend('data','fit_data','fit','Location','SE');
set(h,'Interpreter','none');
text(.2,.8,['Rnorm = ' num2str(rnorm*1e3,3) 'mOhms'],'units','normalized','FontSize',14)
subplot(212)
plot(tbias_current*1e6,(input_current-yfitdiff)*1e6,'k'); hold on;
plot(tbias_current(sb_start:sb_end)*1e6,(input_current(sb_start:sb_end)-yfitdiff(sb_start:sb_end))*1e6,'g');
plot(tbias_current(jumpindx)*1e6,(input_current(jumpindx)-yfitdiff(jumpindx))*1e6,'r*');
text(.75,.1,['* = feedback jump'],'units','normalized','Color','r');
xlabel('applied bias current (uA)');
ylabel('fit residuals (uA)');
set(gca,'YLim',[-.1 .1],'YGrid','on');

h2=make_fig;
plot(rdet*1e3,pj*1e12);hold on;
plot(rdet(indx)*1e3,pj(indx)*1e12,'g');
xlabel('detector resistance (mOhm)')
ylabel('Pj (pW)');
title(['POWER IN TRANSITION']);
text(.2,.8,['Pj = ' num2str(pj_ave*1e12,3) 'pW'],'units','normalized','FontSize',14)
end
