function [rawbias, rawcurrent]=read_full_loadcurve(mtag, p)
%Read the full a full loadcurve associated with a GCP tag. 

d=read_run(mtag);

[sampratio,samprate]=get_sampratio_rate(d);
mask=(bitand(d.array.frame.features, 2^6) ~=0);

if sum(mask)==0
  rawbias=[];
  rawcurrent=[];
  return;
end

lc=find_blk(mask, sampratio);

nfpts=lc.ef-lc.sf;


% tweak the ends of plc a bit due to feature timing issue
% should be sufficient to extend plc.ef 100 samples
% but could also find the return of the user_word to the original
% value
stdword=mode(double(d.mce0.header.user_word(lc.sf:lc.ef,p.rx+1)));

nchan=size(p.rx, 1);
user_word=d.mce0.header.user_word(lc.sf:lc.ef, p.rx+1); 

good_mask=(user_word~=repmat(stdword, nfpts+1, 1));
good_mask=(good_mask & user_word);
is_good=find(sum(good_mask, 2) == nchan);

rawbias=user_word(is_good, :);
idx=lc.sf:lc.ef;
idx=idx(is_good);
rawcurrent=d.mce0.data.fb(idx, :);
rawbias=rawbias(2:end, :);
rawcurrent=rawcurrent(2:end, :);

rawcurrent=-rawcurrent;
end

