function pcurrent=prettify_current(current, cuts, instability_idx)
% PRETTIFY current: Mark bad data as nans
% Martin Lueker 06/12/11
nchans=size(current, 2);
pcurrent=current;
g=~(cuts.high_variance | cuts.badcurve | cuts.badres | ...
    cuts.gaincomp);
%g=~(cuts.high_variance | cuts.badres | cuts.gaincomp);
pcurrent(:, ~g)=nan;
for i=1:nchans
  if(instability_idx(i) ~= 0)
    pcurrent(instability_idx(i):end, i)=nan;
  end
end
end
