function [transition, instability]=identify_transition(current, winsize)
%find the transition portion of the IV curve and the place where ...
    %the TES goes unstable

% the transistion is defined as the point where dI/dv becomes
% negative
% if there is a lot of garbage in the time stream this may be unique
% from the minimum current point.  
deriv=conv2(ones(1, winsize)/winsize, [1], diff(current(:, :)), 'same');

nchan=size(current, 2);
% now find the normal portion
transition=uint32(zeros(1, nchan));
instability=uint32(zeros(1, nchan));

for i=1:nchan
  tmp=find(deriv(winsize:end, i)>0)+101;
  if(length(tmp)== 0)
    transition(i)=0;
    instability(i)=0;
  else    
    transition(i)=tmp(1);
    % now find where these are latching
    % latch identification condition A: the place where the transition region peaks
    [tmp, idx]=max(current(transition(i):end, i));
    instability_conda(i)=idx+transition(i)-1;
    % latch identification condition A: the place where the transition
    % region derivative goes negative
    tmp=find(deriv((transition(i)+winsize):end, i)<0);
    if length(tmp) > 0
      idx=min(tmp);
      instability_condb(i)=idx+transition(i)+winsize-1;
      instability(i)=min([instability_conda(i), ...
	    instability_condb(i)]); 
    else
      instability=instability_conda(i);
    end
  end   
end
end

