function [cal, r_n, i_b, i_s, p_s, r_s, v_s] = iv_ramp_calc (i_bias, i_tes, r_sh, ranges)
% IV_RAMP_CALC fits the normal and superconducting
% regions of the IV curve to find the normal resistance
% and an adjustment to the turns ratio.  It calculates
% sensor current, power, voltage, and resistance on a 
% true absolute scale.
%
% [C RN IB IS PS RS VS] = IV_RAMP_CALC (IB_IN, IS_IN, RSH, B)
%
% IB_IN is the bias current (through bias resistor)
% IS_IN is the sensor current (through TES) with arbitrary offset
% RSH is the shunt resistor (Ohms)
% B is the list of branch ranges, from SELECT_IV_BRANCHES.
%
% C is a calibration adjustment factor
% RN is fitted normal resistance (Ohms)
% The following output quantities are properly zeroed,
% physical values, defined in the selected good data range:
%	IB = bias current
%	IS = sensor current
%	PS = Joule power across sensor
%	RS = sensor resistance
%	VS = voltage across TES

% RWO 080501
% RWO 090211 - do offset subtraction for bad regions too
% JPF 090212 - return bad regions in i_b, i_s, etc. as well
% ART 090303 - fixes to bad region subtraction, saving

% Cuts for superconducting, bias/normal, normal regions
c_sc = logical (zeros (size (i_bias)));
c_sc (ranges(1,1) : ranges(1,2)) = logical (1);
c_bn = logical (zeros (size (i_bias)));
c_bn (ranges(2,1) : ranges(2,2)) = logical (1);
c_norm = logical (zeros (size (i_bias)));
c_norm (ranges(3,1) : ranges(3,2)) = logical (1);

% Find the slope & offset of the linear branches:
% superconducting, then normal.

[sc_ofs sc_slp] = branch_linfit (i_bias, i_tes, c_sc);
[norm_ofs norm_slp] = branch_linfit (i_bias, i_tes, c_norm);

% Assuming parasitic resistance is very small, we can adjust the
% calibration using the SC branch.  It should have slope=1.

cal = 1 / sc_slp;

% usecal = 1 / -0.958;
usecal = 1;

% Find normal-state resistance from normal branch.

r_n = r_sh * (1 / (norm_slp * usecal) - 1);

% Set correct zeros of biased/normal and SC branches

i_tes (c_sc) = i_tes (c_sc) - sc_ofs;
i_tes (c_bn) = i_tes (c_bn) - norm_ofs;

% Do offset subtraction even for "bad" regions
c_bad = ~(c_sc | c_bn);
c_left = (i_bias < min(i_bias(c_bn)));
i_tes (c_bad & c_left) = i_tes (c_bad & c_left) - sc_ofs;
i_tes (c_bad & ~c_left) = i_tes (c_bad & ~c_left) - norm_ofs;

% Now calculate output power, voltage, resistance, etc.
i_b = i_bias;
i_s = i_tes * usecal;
r_s = (i_b ./ i_s - 1) * r_sh;
v_s = i_s .* r_s;
p_s = i_s .* v_s;

cal = abs (cal);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ofs slp] = branch_linfit (i_bias, i_tes, cc)

        xm = mean (i_bias(cc));
        ym = mean (i_tes(cc));
        slp = mean ((i_bias(cc)-xm) .* (i_tes(cc)-ym)) / mean ((i_bias(cc)-xm).^2);
        ofs = ym - slp*xm;
        