function varargout = iv_plots (workdir, outpath, do_mux_row, do_mux_rc_col)

% IV_PLOTS

if (nargin < 1) || isempty (workdir) || ~exist (workdir, 'dir')
	workdir = uigetdir ('.', 'Select directory with data files');
end;
disp (['Scanning files in directory.']);
[flist temp good_list isgood_any] = scan_iv_data (workdir);

disp (['Done scanning, making plots.']);

if (nargin < 3) || isempty (do_mux_row)
	do_mux_row = 0:(size(isgood_any,1)-1);
end;
if (nargin < 4) || isempty (do_mux_rc_col)
	do_mux_rc_col = 0:(size(isgood_any,2)-1);
end;

if exist (fullfile (outpath, 'params.mat'))
	outparams = load (fullfile (outpath, 'params.mat'))
else
	outparams.Tc = NaN * ones (max(do_mux_row)+1, max(do_mux_rc_col)+1);
	outparams.n = outparams.Tc;
	outparams.G_Tc = outparams.Tc;
	outparams.G_450 = outparams.Tc;
	outparams.Tc_25 = outparams.Tc;
	outparams.Rn = outparams.Tc;
	outparams.sig_Rn = outparams.Tc;
end;

pix = [];
fig = figure;
set (fig, 'Visible', 'off');
for (mux_row = do_mux_row)
	if (length (do_mux_row) > 1)
		disp (['On row ' num2str(mux_row)]);
	end;
	for (mux_rc_col = do_mux_rc_col)
		if ~isgood_any (mux_row+1, mux_rc_col+1)
			continue;
		end;
		IFlist = {};
		ITlist = {};
		pixname = ['r' num2str(mux_row) 'c' num2str(mux_rc_col)];
		for (onf = 1:length(temp))
			if ~good_list (mux_row+1, mux_rc_col+1, onf)
				continue;
			end;
			[D flist] = load_dataset (mux_row, mux_rc_col, onf, flist);
			if (D.is_good)
				figure (fig);
				plot_dataset (D, temp, onf);
				fname = ['raw-' pixname '-' num2str(temp(onf)) 'mK'];
				print (fig, '-dpng', '-r75', fullfile (outpath, fname));

				IFlist = {IFlist{:}, [fname '.png']};
				ITlist = {ITlist{:}, ['Raw / ' num2str(temp(onf)) ' mK']};
			else
				disp ('Can''t happen!');
				keyboard
			end;
		end;
		[D flist] = load_all_temps (flist, good_list(mux_row+1, mux_rc_col+1, :), mux_row, mux_rc_col);
		if sum (D.is_good) > 0
			ITlist = {ITlist{:}, 'Power', 'Voltage', 'Resistance', 'Power vs. Resistance', ...
				'Power vs. Temp', 'Resistance vs. Temp'};
			figure (fig);

			plot_p_all (D, temp);
			fname = ['power-' pixname];
			print (fig, '-dpng', '-r75', fullfile (outpath, fname));
			IFlist = {IFlist{:}, [fname '.png']};

                        plot_v_all (D, temp);
                        fname = ['voltage-' pixname];
                        print (fig, '-dpng', '-r75', fullfile (outpath, fname));
                        IFlist = {IFlist{:}, [fname '.png']};

                        plot_r_all (D, temp);
                        fname = ['resistance-' pixname];
                        print (fig, '-dpng', '-r75', fullfile (outpath, fname));
                        IFlist = {IFlist{:}, [fname '.png']};

			plot_p_r_all (D, temp);
                        fname = ['p-vs-r-' pixname];
                        print (fig, '-dpng', '-r75', fullfile (outpath, fname));
                        IFlist = {IFlist{:}, [fname '.png']};

			params = plot_p_t (D, temp);
                        fname = ['p-vs-t-' pixname];
                        print (fig, '-dpng', '-r75', fullfile (outpath, fname));
                        IFlist = {IFlist{:}, [fname '.png']};

                        plot_r_t (D, temp);
                        fname = ['r-vs-t-' pixname];
                        print (fig, '-dpng', '-r75', fullfile (outpath, fname));
                        IFlist = {IFlist{:}, [fname '.png']};
		end;
		pix (mux_row+1, mux_rc_col+1).IFlist = IFlist;
		pix (mux_row+1, mux_rc_col+1).ITlist = ITlist;
		pix (mux_row+1, mux_rc_col+1).is_good = sum (D.is_good) > 0;
		if ~isempty (params)
			outparams.Tc(mux_row+1, mux_rc_col+1) = params.Tc;
			outparams.n(mux_row+1, mux_rc_col+1) = params.n;
			outparams.G_Tc(mux_row+1, mux_rc_col+1) = params.G_Tc;
			outparams.G_450(mux_row+1, mux_rc_col+1) = params.G_450;
			outparams.Tc_25(mux_row+1, mux_rc_col+1) = params.Tc_25;
			outparams.Rn(mux_row+1, mux_rc_col+1) = nanmean ([D.r_n{:}]);
			outparams.sig_Rn(mux_row+1, mux_rc_col+1) = nanstd ([D.r_n{:}]);
		end;
		save (fullfile (outpath, 'params.mat'), '-STRUCT', 'outparams');
	end;
end;
close (fig);

for (onf = 1:length (flist))
	dirfile_close (flist(onf));
end;

save (fullfile (outpath, 'params.mat'), '-STRUCT', 'outparams');
f = fopen (fullfile (outpath, 'params.txt'), 'wt');
fprintf (f, '%10s%10s%10s%10s%10s%10s%10s%10s%10s\n', 'MUX_ROW', 'MUX_COL', ...
	'T_C', 'BETA', 'G_TC', 'G_450', 'TC_2.5', 'R_N', 'SIG_R_N');
for (mux_row = 0:32)
	for (mux_rc_col = (1:size(outparams.Tc,2))-1)
		fprintf (f, '%10d%10d', mux_row, mux_rc_col);
		fprintf (f, '%10.3f', outparams.Tc(mux_row+1,mux_rc_col+1));
		fprintf (f, '%10.2f', outparams.n(mux_row+1,mux_rc_col+1));
		fprintf (f, '%10.3f', outparams.G_Tc(mux_row+1,mux_rc_col+1));
		fprintf (f, '%10.3f', outparams.G_450(mux_row+1,mux_rc_col+1));
		fprintf (f, '%10.3f', outparams.Tc_25(mux_row+1,mux_rc_col+1));
		fprintf (f, '%10.4f', outparams.Rn(mux_row+1,mux_rc_col+1));
		fprintf (f, '%10.4f', outparams.sig_Rn(mux_row+1,mux_rc_col+1));
		fprintf (f, '\n');
	end;
end;
fclose (f);

Hname = fullfile (outpath, 'index.html');
H = fopen (Hname, 'wt');
fprintf (H, '<html>\n<head>\n<title>IV analysis</title>\n</head>\n\n');
fprintf (H, '<body>\n');
fprintf (H, '<h1>IV analysis from %s</h1>\n', workdir);
fprintf (H, '<h2>%s</h2>\n', datestr(now));

fprintf (H, '<a name="index">\n');
fprintf (H, '<h3>Index:</h3>\n<ul>\n');
for (mux_row = 0:size(pix,1)-1)
	for (mux_rc_col = 0:size(pix,2)-1)
		if pix(mux_row+1, mux_rc_col+1).is_good
			fprintf (H, '    <li><a href="#r%dc%d">Row %d, Column %d</a></li>\n', ...
				mux_row, mux_rc_col, mux_row, mux_rc_col);
		end;
	end;
end;
fprintf (H, '</ul>\n\n');

for (mux_row = 0:size(pix,1)-1)
        for (mux_rc_col = 0:size(pix,2)-1)
                if pix(mux_row+1, mux_rc_col+1).is_good
			fprintf (H, '<hr>\n<a name="r%dc%d">\n', mux_row, mux_rc_col);
			fprintf (H, '<h3>Row %d, Column %d</h3>\n', mux_row, mux_rc_col);
			fprintf (H, '<a href="#index">(back to top)</a>\n');
			html_plots_table (H, pix(mux_row+1, mux_rc_col+1).IFlist, pix(mux_row+1, mux_rc_col+1).ITlist, 2);
                end;
        end;
end;

fprintf (H, '</body>\n</html>\n');
fclose (H);

function html_plots_table (H, IFlist, ITlist, numcols)
	if isempty (IFlist) || isempty (ITlist)
		return;
	end;

	if (nargin < 4) || isempty (numcols)
		numcols = 2;
	end;

	fprintf (H, '<table border="0" cellspacing="0" cellpadding="5">\n');
	ic = 0;
	for (ii = 1:length (IFlist))
		if (ic == 0)
			fprintf (H, '<tr>\n');
		end;
		fprintf (H, '    <td align="center">%s<br>\n        <img src="%s">\n    </td>\n', ...
			ITlist{ii}, IFlist{ii});
		ic = ic + 1;
		if (ic == numcols) || (ii == length(IFlist))
			fprintf (H, '</tr>\n\n');
			ic = 0;
		end;
	end;
	fprintf (H, '</table>\n\n');

function [dirfiles, temp, good_list, isgood_any] = scan_iv_data (workdir)
	d = dir (fullfile (workdir, '*.run'));
	flist = {};
	temp = [];
	templist = {};
	for (ii = 1:length (d))
		bare_fname = d(ii).name(1:(end-4));
		if exist (fullfile (workdir, bare_fname), 'file')
			flist = {flist{:}, fullfile(workdir,bare_fname)};
			temp = [temp(:); guess_base_temp(bare_fname)];
			templist = {templist{:}, ...
				[num2str(temp(end)) ' mK']};
		else
			disp (['Skipping ' bare_fname]);
		end;
	end;
	for (ii = 1:length (flist))
		if ~exist ([flist{ii} '_iv_dir'], 'dir')
            disp (['Performing iv_analysis for ' flist{ii}]);
			iv_analysis (flist{ii}, 0);
		end;
	end;

	good_list = {};
	maxrow = 0;
	maxcol = 0;
	for (ii = 1:length (flist))
		disp (['Checking good pixels in ' flist{ii}]);
		tic;
		D = dirfile_open ([flist{ii} '_iv_dir']);
		t1 = toc; tic;
		fieldlist = dirfile_fields (D, 'is_good_');
		t2 = toc; tic;
		for (p = 1:length(fieldlist))
			mux_addr = sscanf (fieldlist{p}, 'is_good_r%dc%d');
			[tmp_is_good D] = dirfile_get (D, fieldlist{p});
			good_list{ii}(mux_addr(1)+1, mux_addr(2)+1) = tmp_is_good;
		end;
		t3 = toc; tic;
		maxrow = max (maxrow, size (good_list{ii}, 1));
		maxcol = max (maxcol, size (good_list{ii}, 2));
		dirfiles(ii) = D;
		% dirfile_close (D);
		t4 = toc;
		disp (['t1=' num2str(t1) ', t2=' num2str(t2) ', t3=' num2str(t3) ', t4=' num2str(t4)]);
	end;
	tmp = zeros (maxrow, maxcol, length(flist));
	for (ii = 1:length(flist))
		tmp (1:size(good_list{ii},1), 1:size(good_list{ii},2), ii) = good_list{ii};
	end;
	good_list = tmp;
	if (nargout > 3)
		isgood_any = any (good_list, 3);
	end;

function [D flist_out] = load_dataset (mux_row, mux_col, onf, flist)

	if iscell(flist) && ischar (flist{onf})
		fname = flist{onf};
		if ~exist ([fname '.iv.mat'], 'file')
			iv_analysis (fname, 0, mux_row, mux_col);
		end;
		F = dirfile_open ([fname '_iv_dir']);
		do_close = 1;
	elseif isstruct(flist)
		F = flist(onf);
		do_close = 0;
		flist_out = flist;
	end;

	pixname = ['r' num2str(mux_row) 'c' num2str(mux_col)];
	D.mux_row = mux_row;
	D.mux_col = mux_col;
	if isempty(dirfile_fields (F, pixname, 'regexp'))
		D.i_bias = [];
		D.i_tes = [];
		D.ranges = [];
		D.fj = [];
		D.is_good = 0;
		D.msg = 'No such pixel';
		D.branch1 = [];
    else
        [D.is_good F] = dirfile_get (F, ['is_good_' pixname]);
        [D.msg F] = dirfile_get (F, ['msg_' pixname]);
        [D.i_bias F] = dirfile_get (F, ['i_b_' pixname]);
        if isempty (dirfile_fields (F, ['i_s_' pixname])),
			[D.i_tes F] = dirfile_get (F, ['fb_' pixname]);
            D.i_tes = D.i_tes / 2^14;
		else
			[D.i_tes F] = dirfile_get (F, ['i_s_' pixname]);
        end;
		[D.branch F] = dirfile_get (F, ['branch_' pixname]);
		[D.fj F] = dirfile_get (F, ['fj_' pixname]);
	end;
	if do_close
		dirfile_close (F);
	else
		flist_out(onf) = F;
	end;

function update_range_plots (D)
	if isempty (D.i_tes)
		return;
	end;
	i_bias = D.i_bias;
	i_tes = D.i_tes;
	branch = D.branch;
	hold on;

	cut = (branch == 1);
	sc_line = plot (1e6 * i_bias(cut), 1e6 * i_tes(cut), 'go', 'LineWidth', 2);
	cut = (branch == 2);
	bn_line = plot (1e6 * i_bias(cut), 1e6 * i_tes(cut), 'rx', 'LineWidth', 2);
	cut = (branch == 3);
	norm_line = plot (1e6 * i_bias(cut), 1e6 * i_tes(cut), 'ko', 'LineWidth', 2);

function plot_dataset (D, temp, onf)
	cla;
	hold off;
	if isempty (D.i_tes)
		axis ([0 1 0 1]);
		line ([0 1], [1 0], 'color', 'k');
		hold on;
		line ([0 1], [0 1], 'color', 'k');
		return;
	end;
	i_bias = D.i_bias;
	i_tes = D.i_tes;
	branch = D.branch;
	fj = D.fj;
	msg = D.msg;
	plot (-1e12, -1e12, 'b.', 'MarkerSize', 6);
	hold on;
	plot (-1e12, -1e12, 'go', 'LineWidth', 2);
	plot (-1e12, -1e12, 'rx', 'LineWidth', 2);
	plot (-1e12, -1e12, 'ko', 'LineWidth', 2);

	plot (i_bias*1e6, i_tes*1e6, 'b.', 'MarkerSize', 6);
	hold on;
	for (ii = 1:length(fj))
		line (1e6 * i_bias(fj(ii)) * [1 1], ...
			1e6 * [min(i_tes) max(i_tes)], ...
			'LineWidth', 1, 'color', 'k', 'LineStyle', '--'); % , ...
			% 'ButtonDownFcn', @clicky);
	end;
	update_range_plots (D);
	legend ('All', 'Superconducting', 'Bias/normal', 'Normal');

	grid on;
	xlabel ('I_b / \muA');
	ylabel ('I_s / \muA');
	title (['Row ' num2str(D.mux_row) ', Col ' num2str(D.mux_col) ...
		' at ' num2str(temp(onf)) ' mK']);
	smaxis (1e6 * [0 max(i_bias) min(i_tes) max(i_tes)]);
	if ~isempty (msg)
		axax = axis;
		text (1/2*sum(axax(1:2)), 0.4*axax(3)+0.6*axax(4), ...
			msg, 'FontSize', 12, 'HorizontalAlignment', 'Center');
	end;


function [D flist_out] = load_all_temps (flist, goodlist, mux_row, mux_col)

	goodlist = goodlist(:);
	D.i_bias = {};
	D.i_tes = {};
	D.p_s = {};
	D.is_good = [];
	D.mux_row = mux_row;
	D.mux_col = mux_col;
	if (nargout > 1)
		flist_out = flist;
	end;
	for (onf = 1:length (flist))
		if (0 == goodlist(onf))
			D.i_bias{onf} = [];
			D.i_tes{onf} = [];
			D.is_good(onf) = 0;
			continue;
		end;
		if iscell(flist) && ischar(flist{onf})
			fname = flist{onf};
			F = dirfile_open ([fname '_iv_dir']);
			do_close = 1;
		else
			F = flist(onf);
			do_close = 0;
		end;
		pixname = ['r' num2str(mux_row) 'c' num2str(mux_col)];
		if isempty (dirfile_fields (F, ['i_b_' pixname]))
			D.i_bias{onf} = [];
			D.i_tes{onf} = [];
			D.is_good(onf) = 0;
		else
			branch = dirfile_get (F, ['branch_' pixname]);
			cc = (branch ~= 0);
			tmp = dirfile_get (F, ['i_b_' pixname]);
			D.i_b{onf} = tmp(cc);
			tmp = dirfile_get (F, ['i_s_' pixname]);
			D.i_s{onf} = tmp(cc);
			tmp = dirfile_get (F, ['p_s_' pixname]);
			D.p_s{onf} = tmp(cc);
			tmp = dirfile_get (F, ['r_s_' pixname]);
			D.r_s{onf} = tmp(cc);
			tmp = dirfile_get (F, ['v_s_' pixname]);
			D.v_s{onf} = tmp(cc);
			D.r_n{onf} = dirfile_get (F, ['r_n_' pixname]);
			D.is_good(onf) = dirfile_get (F, ['is_good_' pixname]);
		end;
		if do_close
			dirfile_close (F);
		else
			flist_out(onf) = F;
		end;
	end;


function plot_p_all (D, temp)
	hold off;
	cla;
	hold on;
	legleg = {};
	CC = 'bgrkcmy';
	SS = '*s+voxd';
	max_ib = 0;
	max_ps = 0;
	for (onf = 1:length(temp))
		if (onf > length(D.is_good)) || ~D.is_good(onf) ...
			|| (onf > length(D.p_s)) || isempty (D.p_s{onf})
			continue;
		end;
		i_b = D.i_b{onf};
		i_s = D.i_s{onf};
		p_s = D.p_s{onf};
		r_s = D.r_s{onf};
		legleg = {legleg{:}, [num2str(temp(onf)) ' mK']};
		sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
		plot (i_b*1e6, p_s*1e12, sym);
                max_ib_tmp = 2.0 * min (i_b (r_s >= D.r_n{onf} * 1/2));
                max_ib = max ([max_ib, max_ib_tmp]);
                max_ps_tmp = min (p_s (i_b >= max_ib));
                max_ps = max ([max_ps, max_ps_tmp]);
	end;
	axmax = [max_ib*1e6, max_ps*1e12];
	if sum (abs (axmax)) == 0
		smaxis ([0 1 0 1]);
		line ([0 1], [0 1], 'color', 'k');
		line ([0 1], [1 0], 'color', 'k');
		legend off;
		return;
	end;
	legend (legleg{:}, 'Location', 'NorthWest');
	grid on;
	smaxis ([0 axmax(1) 0 axmax(2)]);
	xlabel ('I_b / \muA');
	ylabel ('P_s / pJ');
	title (['Row ' num2str(D.mux_row) ', Col ' num2str(D.mux_col) ': Power']);


function plot_r_all (D, temp)
	hold off;
	cla;
	hold on;
	legleg = {};
	CC = 'bgrkcmy';
	SS = '*s+voxd';
	max_ib = 0;
	max_rs = 0;
	for (onf = 1:length(temp))
		if (onf > length(D.is_good)) || ~D.is_good(onf) ...
			|| (onf > length(D.p_s)) || isempty (D.p_s{onf})
			continue;
		end;
		i_b = D.i_b{onf};
		i_s = D.i_s{onf};
		p_s = D.p_s{onf};
		r_s = D.r_s{onf};
		legleg = {legleg{:}, [num2str(temp(onf)) ' mK']};
		sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
		plot (i_b*1e6, r_s*1e3, sym);
                max_ib_tmp = 2.0 * min (i_b (r_s >= D.r_n{onf} * 1/2));
                max_ib = max ([max_ib, max_ib_tmp]);
                max_rs_tmp = min (r_s (i_b >= max_ib));
                max_rs = max ([max_rs, max_rs_tmp]);
	end;
	axmax = [max_ib*1e6, max_rs*1e3];
	if sum (abs (axmax)) == 0
		axis ([0 1 0 1]);
		line ([0 1], [0 1], 'color', 'k');
		line ([0 1], [1 0], 'color', 'k');
		legend off;
		return;
	end;
	legend (legleg{:}, 'Location', 'NorthWest');
	grid on;
	smaxis ([0 axmax(1) 0 axmax(2)]);
	xlabel ('I_b / \muA');
	ylabel ('R_s / m\Omega');
	title (['Row ' num2str(D.mux_row) ', Col ' num2str(D.mux_col) ': Resistance']);



function plot_v_all (D, temp)
	hold off;
	cla;
	hold on;
	legleg = {};
	CC = 'bgrkcmy';
	SS = '*s+voxd';
	max_ib = 0;
	max_vs = 0;
	for (onf = 1:length(temp))
		if (onf > length(D.is_good)) || ~D.is_good(onf) ...
			|| (onf > length(D.p_s)) || isempty (D.p_s{onf})
			continue;
		end;
		i_b = D.i_b{onf};
		i_s = D.i_s{onf};
		p_s = D.p_s{onf};
		r_s = D.r_s{onf};
		v_s = D.v_s{onf};
		legleg = {legleg{:}, [num2str(temp(onf)) ' mK']};
		sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
		plot (i_b*1e6, v_s*1e3, sym);
		max_ib_tmp = 2.0 * min (i_b (r_s >= D.r_n{onf} * 1/2));
		max_ib = max ([max_ib, max_ib_tmp]);
		max_vs_tmp = min (v_s (i_b >= max_ib));
		max_vs = max ([max_vs, max_vs_tmp]);
	end;
	axmax = [max_ib*1e6 max_vs*1e3];
	if sum (abs (axmax)) == 0
		axis ([0 1 0 1]);
		line ([0 1], [0 1], 'color', 'k');
		line ([0 1], [1 0], 'color', 'k');
		legend off;
		return;
	end;
	legend (legleg{:}, 'Location', 'NorthWest');
	grid on;
	smaxis ([0 axmax(1) 0 axmax(2)]);
	xlabel ('I_b / \muA');
	ylabel ('V_s / mV');
	title (['Row ' num2str(D.mux_row) ', Col ' num2str(D.mux_col) ': Voltage']);



function plot_p_r_all (D, temp)
	hold off;
	cla;
	hold on;
	legleg = {};
	CC = 'bgrkcmy';
	SS = '*s+voxd';
	rn_max = 0;
	ps_max = 0;
	for (onf = 1:length(temp))
		if (onf > length(D.is_good)) || ~D.is_good(onf) ...
			|| (onf > length(D.p_s)) || isempty (D.p_s{onf})
			continue;
		end;
		i_b = D.i_b{onf};
		i_s = D.i_s{onf};
		p_s = D.p_s{onf};
		r_s = D.r_s{onf};
		legleg = {legleg{:}, [num2str(temp(onf)) ' mK']};
		sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
		plot (r_s*1e3, p_s*1e12, sym);
		rn_max = max ([rn_max, D.r_n{onf} * 1.5]);
		ps_max_tmp = min (p_s (r_s >= D.r_n{onf}*0.5)) * 1.5;
		ps_max = max ([ps_max, ps_max_tmp]);
	end;
	axmax = [rn_max*1e3, ps_max*1e12];
	if (sum (abs (axmax)) == 0)
		axis ([0 1 0 1]);
		line ([0 1], [0 1], 'color', 'k');
		line ([0 1], [1 0], 'color', 'k');
		legend off;
		return;
	end;
	legend (legleg{:}, 'Location', 'NorthWest');
	grid on;
	smaxis ([0 axmax(1) 0 axmax(2)]);
	xlabel ('R_s / m\Omega');
	ylabel ('P_s / pJ');
	title (['Row ' num2str(D.mux_row) ', Col ' num2str(D.mux_col) ': P_J(R)']);


function param = plot_p_t (D, temp)
	hold off;
	cla;
	axmax = [0 0];
	hold on;
	legleg = {};
	[p_out r_out] = collate_p_r (D.r_s, D.p_s, temp, D.r_n, 4);
	pp = NaN * ones (size (temp));
	psig = NaN * ones (size (temp));
	for (onf = 1:length(temp))
		if (onf > length(D.is_good)) || ~D.is_good(onf) ...
			|| (onf > length(p_out)) || isempty (p_out{onf})
			continue;
		end;
		% sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
		% plot (handles.temp(onf), p_out{onf}*1e12, sym);
		% errorbar (handles.temp(onf), nanmedian (p_out{onf})*1e12, nanstd (p_out{onf})*1e12, 'b*', 'LineWidth', 2);
		axmax(2) = max (axmax(2), max(p_out{onf})*1e12);
		pp(onf) = nanmedian (p_out{onf});
		psig(onf) = nanstd (p_out{onf});
	end;
	errorbar (temp, pp*1e12, psig*1e12, 'b*', 'LineWidth', 2);
	% legend (legleg{:}, 'Location', 'NorthEast');
	legend off;
	if sum (abs (axmax)) == 0
		axis ([0 1 0 1]);
		line ([0 1], [0 1], 'color', 'k');
		line ([0 1], [1 0], 'color', 'k');
		if (nargout > 0)
			param = [];
		end;
		return;
	end;
	if sum (D.is_good) >= 3
		[M2 M] = g_fit_from_p (temp(logical(D.is_good)) * 1e-3, pp(logical(D.is_good)));
	else
		M = NaN * ones (4, 1);
	end;
	grid on;
	Trange = [min(temp) max(temp)];
	taxlim(1) = Trange(1)*1.2 - Trange(2)*0.2;
	taxlim(2) = Trange(2)*1.2 - Trange(1)*0.2;
	tt = linspace (taxlim(1), taxlim(2), 100) / 1e3;
	pfit = M(4) * (M(2).^M(3) - tt.^M(3));
	% kap2 = M(1) / (M(3)+1) / M(2)^(M(3));
	% pfit2 = kap2 * (M(2).^(M(3)+1) - tt.^(M(3)+1));
	% pfit2 = M2(4) * (M2(2).^M2(3) - tt.^M2(3));
	if (sum (D.is_good) >= 2)
		for (n = [2.5])
			M2 = g_fit_from_p (temp(logical(D.is_good)) * 1e-3, ...
				pp(logical(D.is_good)), n);
			pfit2 = M2(4) * (M2(2).^M2(3) - tt.^M2(3));
			plot (tt*1e3, pfit2*1e12, 'g-', 'LineWidth', 0.5);
		end;
	else
		M2 = NaN * ones (4, 1);
	end;
	% M450 = g_fit_from_p (temp (logical (D.is_good)) * 1e-3, pp(logical (D.is_good)), 2.5);
	plot (tt*1e3, pfit*1e12, 'r--', 'LineWidth', 2);
	% smaxis ([taxlim 0 axmax(2)*1.5]);
	smaxis ([400 650 0 axmax(2)*1.5]);
	axax = axis;
	FPROP = {'FontSize', 14, 'Units', 'Normalized'};
	text (0.7, 0.8, ...
		['G(T_c) = ' num2str(M(1)*1e12,'%.1f') ' pW/K'], FPROP{:});
	text (0.7, 0.7, ...
		['T_c = ' num2str(M(2)*1e3,'%.0f') ' mK'], FPROP{:});
	text (0.7, 0.9, ...
		['n = ' num2str(M(3),'%.2f')], FPROP{:});

	text (0.7, 0.55, ...
		['G_{450}^{2.5} = ' num2str(M2(1)*1e12,'%.1f') ' pW/K'], FPROP{:});
	% text (axax(1)*0.5+axax(2)*0.5, axax(3)*0.4+axax(4)*0.6, ...
	% 	['\kappa = ' num2str(M(4)*1e12,'%.1f') ' pW/K^n'], 'FontSize', 14);
	xlabel ('T_{sub} / mK');
	ylabel ('P / pJ');
	title (['Row ' num2str(D.mux_row) ', Col ' num2str(D.mux_col) ': P_J(T_{sub})']);
	if (nargout >= 1)
		param.Tc = M(2)*1e3;
		param.n = M(3);
		param.G_Tc = M(1)*1e12;
		param.G_450 = M2(1)*1e12;
		param.Tc_25 = M2(2)*1e3;
	end;

function plot_r_t (D, temp)
	hold off;
	cla;
	axmax = [0 0];
	hold on;
	legleg = {};
	CC = 'bgrkcmy';
	SS = '*s+voxd';
	[p_out r_out] = collate_p_r (D.r_s, D.p_s, temp, D.r_n, 4);
	pp = NaN * size (temp);
	for (onf = 1:length(temp))
		if (onf > length(D.is_good)) || ~D.is_good(onf)
			continue;
		end;
		sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
		plot (-1e6, -1e6, sym);
		legleg = {legleg{:}, [num2str(temp(onf)) ' mK']};
		if onf<=length(p_out) && ~isempty (p_out{onf})
			pp(onf) = nanmedian (p_out{onf});
		end;
	end;
	if sum (~isnan (pp)) == 0
		legend off;
		axis ([0 1 0 1]);
		line ([0 1], [0 1], 'color', 'k');
		line ([0 1], [1 0], 'color', 'k');
		return;
	end;
	legend (legleg{:}, 'Location', 'SouthEast');

	[M2 M] = g_fit_from_p (temp * 1e-3, pp);
	grid on;

	kap = M2(4);
	n = M2(3);
	for (onf = 1:length (temp))
		if (onf>length(D.is_good)) || ~D.is_good(onf) || isempty (D.p_s{onf})
			continue;
		end;
		pp = D.p_s{onf};
		rr = D.r_s{onf};
		t_s = temp(onf) / 1e3;
		tt = (pp/kap + t_s.^n) .^ (1/n);
		sym = [CC(mod(onf-1,7)+1) SS(mod(onf-1,7)+1)];
		plot (tt * 1e3, rr * 1e3, sym);
		axmax(1) = max (axmax(1), max(real(tt*1e3)));
		axmax(2) = max (axmax(2), max(real(rr*1e3)));
	end;
	if sum (abs (axmax)) == 0
		legend off;
		axis ([0 1 0 1]);
		line ([0 1], [0 1], 'color', 'k');
		line ([0 1], [1 0], 'color', 'k');
		return;
	end;

	% smaxis ([0 axmax(1) 0 axmax(2)]);
	smaxis ([500 600 0 axmax(2)*1.25]);
	xlabel ('T_{el} / mK');
	ylabel ('R / m\Omega');
	title (['Row ' num2str(D.mux_row) ', Col ' num2str(D.mux_col) ': R(T_{el})']);
