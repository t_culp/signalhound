function [current, normres, gaincomp]=rnormal(bias, current, ...
    transition)
%Determine the normal resistance of a calibrated bolometer
do_quadfit=1;
nchan=size(current, 2);
slope=zeros(1, nchan);
gaincomp=zeros(1, nchan);
for i=1:nchan
  if transition(i) > 0
    fitrange=floor(single(transition(i))*0.9);
    if(do_quadfit)
      [q, s, mu]=polyfit(single(bias(1:fitrange, i)), ...
	  current(1:fitrange, i), 2);
      pf(3)=(q(3)-q(2)*mu(1)/mu(2)+q(1)*(mu(1)/mu(2))^2);
      pf(2)=(q(2)-2*q(1)*mu(1)/mu(2))/mu(2);
      pf(1)=q(1)/mu(2)^2;
      gaincomp(i)=pf(1);
      current(:, i)=current(:, i)-pf(3);
      slope(i)=pf(2);
    else
      [q, s, mu]=polyfit(single(bias(1:fitrange, i)), ...
	  current(1:fitrange, i), 1);
      pf(2)=(q(2)-q(1)*mu(1)/mu(2));
      pf(1)=q(1)/mu(2);
      gaincomp(i)=0;
      current(:, i)=current(:, i)-pf(2);
      slope(i)=pf(1);
    end
  end
end
normres=1./slope;
end