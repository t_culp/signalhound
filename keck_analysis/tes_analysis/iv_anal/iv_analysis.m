function iv_analysis (fname, row, col, calib, rem_jumps)
% IV_ANALYSIS performs a load curve analysis from raw data.
%
% IV_ANALYSIS (F, R, C, calib, rem_jumps)
%
% - F = name of input MCE data file.  There should also
% be a run file and a bias file with extensions
% '.run' and '.bias' respectively.
%
% - R, C (optional) = analyzes only the pixel(s)
% identified by mux row R and column C.  Note that
% columns 0-7 belong to readout card 1, columns 8-15
% to readout card 2, etc.
%
% - calib (optional) = structure containing measured resistances
%   and other electronics parameters.  If not present, we use
%   the values from iv_anal/calib/calib_default.m.
% - rem_jumps (optional) = if true, attempt to remove flux
%   jumps from the traces (very slow, imperfect)s
%
% RWO 080501
% RWO 080626 Compat with new read_mce_biasfile
%            Move bias file calibration factor here
% RWO 081010 Save results to dirfiles, don't make plots
% JPF 090120 Fixed pixel selection arguments; make flux jump removal
%            optional; allow measured values for resistances, etc.
% JPF 090227 More explicit code for voltage/resistance calibrations,
%            including new default values from SPIDER test dewar
% ART 090303 Changed to save even 'bad data; removed repeated operations
% JPF 090317 Updated for new read_mce convention: all modes in ADC units
% JAB 090318 Changed the hardcoded values for the RevC FPU.  Also changed
%            line 180 to index the R_BIAS vector rather than use a scalar.
% ART 090722 R_SH->3.0mOhm, correct Spider Run1.1 R_WIRE values
% JPF 090928 Move calibration constants entirely out of this code
% JPF 100719 Do not select branches again if they are already present in a
%            pre-existing dirfile (simplifies tweaking the calibration).

% If it's a directory, loop over all the files in it.
if exist (fname, 'dir')
    arglist = {};
    if (nargin >= 2)
        arglist = {arglist{:}, row};
    end;
    if (nargin >= 3)
        arglist = {arglist{:}, col};
    end;
    if (nargin >= 4)
        arglist = {arglist{:}, calib};
    end
    if (nargin >= 5)
        arglist = {arglist{:}, rem_jumps};
    end
    
    % Make list by run files
    d = dir (fullfile (fname, '*.run'));
    nfiles = length(d);
    for (ii = 1:nfiles)
        fn = d(ii).name (1:(end-4));
        % data file must exist, too
        if ~exist (fullfile (fname, fn))
            continue;
        end;
        disp(['File ' int2str(ii) ' of ' int2str(nfiles) ': ' fn]);
        iv_analysis (fullfile (fname, fn), arglist{:});
    end;
    return;
end;

if (nargin <5)
    %rem_jumps = 1; % hard-code "yes" for old data ...
    rem_jumps = 0; % don't remove flux jumps by default
    % it's slow and rarely necessary in good, recent data
end

% Measured circuit parameters
% These must be known beforehand for proper calibration.
if (nargin<4) || isempty(calib)
    % call a default script
    calib = calib_default();
end

% compute calibration factors from these circuit parameters
% Bias ADC bins => bias current (Amps)
BIAS_CAL = (calib.V_B_MAX/(2^calib.BITS_BIAS)) ./ ...
    (calib.R_BIAS + calib.R_WIRE);
% SQ1 FB DAC bins => TES current (Amps)
FB1_CAL = (calib.V_FB1_MAX/(2^calib.BITS_FB1)) ./ ...
    (calib.R_FB1+calib.R_WIRE) ./ calib.M_FB1;

% Read ramp data + bias values
[S R H] = read_mce (fname);
B = read_mce_biasfile ([fname '.bias']);
if ~isfield (B, 'tes_bias')
    error ('No tes_bias column in bias file.');
end;
i_bias = [];

% File name to save IV analysis parameters and results in
outfname = [fname '_iv_dir'];
D = dirfile_open (outfname);
% Get temperature from MAS file name or from dir file
if ~isempty (dirfile_fields (D, 'temp', 'exact'))
    [temp D] = dirfile_get (D, 'temp');
else
    temp = guess_base_temp (fname);
    D = dirfile_save (D, 'temp', temp);
end;
D = dirfile_save (D, 'bias_dac', B.tes_bias);


% Now loop over pixels

for (p = 1:length (S))
    if (nargin >= 2) && ~isempty (row)
        if ~ismember (S(p).mux_row, row)
            continue;
        end;
    end;
    if (nargin >= 3) && ~isempty (col)
        if ~ismember (S(p).mux_rc_col, col)
            continue;
        end;
    end;
    
    if S(p).rc>2
        continue;
    end
    
    pixname = ['r' num2str(S(p).mux_row) 'c' num2str(S(p).mux_rc_col)];
    %disp(['   ' pixname]);
    
    i_bias = B.tes_bias * BIAS_CAL(S(p).mux_rc_col + 1);
    D = dirfile_add_field (D, ['i_b_' pixname], 'LINCOM', 1, ...
        'bias_dac', BIAS_CAL(S(p).mux_rc_col + 1), 0);
    v_bias = i_bias * calib.R_BIAS(S(p).mux_rc_col + 1);
    D = dirfile_add_field (D, ['v_b_' pixname], 'LINCOM', 1, ...
        ['i_b_' pixname], calib.R_BIAS(S(p).mux_rc_col + 1), 0);
    
    % Remove SQUID jumps (if desired)
    data_mode = R.header.rc(S(p).rc).data_mode;
    en_fb_jump = R.header.rc(S(p).rc).en_fb_jump;
    if rem_jumps
        if ismember(data_mode,[5 8 9 10]) && en_fb_jump
            i_tes = -1 * rem_flux_jumps_ez (S(p).fb, S(p).num_flux_jumps);
        else
            phi0 = R.header.rc(S(p).rc).( ...
                ['flx_quanta' int2str(S(p).mux_col)])( ...
                S(p).mux_row + 1);
            i_tes = -1 * rem_flux_jumps (S(p).fb, phi0);
        end
    else
        i_tes = -1 * S(p).fb;
    end
    D = dirfile_save (D, ['fb_' pixname], i_tes*2^14);
    D = dirfile_save (D, ['i_s_cal_' pixname], FB1_CAL(S(p).mux_rc_col+1));
    i_tes = i_tes * FB1_CAL(S(p).mux_rc_col+1);
    
    % only select the curve branches if they do not exist in the dirfile
    if isempty (dirfile_fields (D, ['is_good_' pixname], 'exact'))
        % Select superconducting/normal branches of the IV curve
        [res, ranges, fj, branch1, msg] ...
            = select_iv_branches (i_bias, i_tes);
        D = dirfile_save (D, ['is_good_' pixname], res);
        D = dirfile_save (D, ['msg_' pixname], msg, 'uint8');
        D = dirfile_save (D, ['fj_' pixname], fj);
        T(p).ranges = ranges;
        if isempty (ranges)
            D = dirfile_save (D, ['branch_' pixname], branch1);
        else
            branch = zeros (size (i_tes));
            branch (ranges(1,1) : ranges(1,2)) = 1;
            branch (ranges(2,1) : ranges(2,2)) = 2;
            branch (ranges(3,1) : ranges(3,2)) = 3;
            D = dirfile_save (D, ['branch_' pixname], branch);
        end;
    else
        % load results from the dirfile instead
        [res D] = dirfile_get (D, ['is_good_' pixname]);
        [msg D] = dirfile_get (D, ['msg_' pixname]);
        [fj D] = dirfile_get (D, ['fj_' pixname]);      
        [branch D] = dirfile_get (D, ['branch_' pixname]);
        % must reconstruct the ranges variable from branch...
        ranges=zeros(3,2);
        ranges(1,1)=find(branch==1,1,'first');
        ranges(1,2)=find(branch==1,1,'last');
        ranges(2,1)=find(branch==2,1,'first');
        ranges(2,2)=find(branch==2,1,'last');
        ranges(3,1)=find(branch==3,1,'first');
        ranges(3,2)=find(branch==3,1,'last');
    end
    
    if (res)
        % if the branches were selected adequately ...
        % ... calculate the relevant resistances, powers, etc.
        [cal, r_n, i_b, i_s, p_s, r_s, v_s] ...
            = iv_ramp_calc (i_bias, i_tes, calib.R_SH, ranges);
        
        % ... save that data to the dir file
        D = dirfile_save (D, ['cal_' pixname], cal);
        D = dirfile_save (D, ['r_n_' pixname], r_n);
        D = dirfile_save (D, ['i_s_' pixname], i_s);
        D = dirfile_add_field (D, ['i_s_dac_' pixname], 'LINCOM', 1, ...
            ['i_s_' pixname], 2^calib.BITS_FB1 / FB1_CAL(S(p).mux_rc_col+1), 0);
        D = dirfile_save (D, ['r_s_' pixname], r_s);
        D = dirfile_save (D, ['v_s_' pixname], v_s);
        D = dirfile_save (D, ['p_s_' pixname], p_s);
    else
        D = dirfile_add_field (D, ['i_s_' pixname], 'LINCOM', 1, ...
            ['fb_' pixname], FB1_CAL(S(p).mux_rc_col+1)/2^calib.BITS_FB1, 0);
    end;
end;

dirfile_close (D);
