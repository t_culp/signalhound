% calib_Spider_R1p1.m
%
% Sets calibration values to match Spider r1.1-1.2
% (After wires changed manganin -> Cu, before replacing FB1 resistors)
%
% 090928 JPF

function calib = calib_Spider_R1p1()
    disp(' Using calibrations from SPIDER Run 1.1 ...');
    
    % use hard-coded values from Justus for now
    calib.R_SH = 0.0030;	% Ohms (at 4K, different chip)
    
    calib.M_FB1 = 15.47; %JAB value measured from RvsT for dark SQ1 on RevC FPU
    
    % new values for SPIDER test stand MCE - jpf 090927
    calib.R_WIRE = 135 *ones(1,16); % ohms
    calib.R_BIAS = [405.3 405.7 407.5 406.8 ...
        404.6 405.3 405.3 409.0 ...
        404.6 406.3 404.4 404.7 ...
        404.9 404.7 404.9 405.0 ]; % ohms
    calib.V_B_MAX = [2.4993 2.4994 2.4994 2.4994 ...
        2.4994 2.4993 2.4993 2.4994 ...
        2.4994 2.4994 2.4994 2.4995 ...
        2.4995 2.4994 2.4995 2.4995]; % Max output voltage (16-bit)
    calib.R_FB1 = 2110 * ones(1,16); % Ohms; will re-measure
    calib.V_FB1_MAX = 1 * ones(1,16); % Volts; will re-measure
    
    calib.BITS_BIAS = 16; % number of bits in TES bias DAC
    calib.BITS_FB1 = 14; % number of bits in FB1 DAC
    
