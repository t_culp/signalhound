% calib_Spider_R5p1.m
%
% Sets calibration values to match Spider r5.1

function calib = calib_Spider_R5p1()
    disp(' Using calibrations from SPIDER Run 5.1 ...');
    
    % use hard-coded values from Justus for now
    calib.R_SH = 0.0030;	% Ohms (at 4K, different chip)
    
    calib.M_FB1 = [-8.0 -8.0 -8.0 -8.0 -8.0 -8.0 -8.0 -8.0 ...
        -8.0 -8.0 -8.0 -8.0 -8.0 -8.0 -8.0 -8.0 ] ;
    
    % ART/JPF 20101108 measurement notated in elog
    calib.R_WIRE = [140 140 140 140 140 140 140 140 ...
        140 140 140 140 140 140 140 140]; % ohms
    calib.R_BIAS = (3920+202.4)*ones(1,16); % ohms
    calib.V_B_MAX = 2.500*ones(1,16); % Max output voltage (16-bit)
    % JPF 20110906 measurement
    calib.R_FB1 = [15039.8 15039.8 15055.6 15038.2 ...
        15039.8 15039.8 15045.7 15041.3 ...
        15023.2 15039.8 15031.5 15039.0 ...
        15030.0 15039.0 15039.0 15046.5]; % Ohms
    calib.V_FB1_MAX = [0.965 0.965 0.966 0.971 ...
        0.965 0.965 0.973 0.959 ...
        0.967 0.965 0.966 0.968 ...
        0.972 0.968 0.968 0.970]; % Volts
    
    calib.BITS_BIAS = 16; % number of bits in TES bias DAC
    calib.BITS_FB1 = 14; % number of bits in FB1 DAC
    
    % Bias ADC bins => bias current (Amps)
    calib.BIAS_CAL = (calib.V_B_MAX/(2^calib.BITS_BIAS)) ./ ...
        (calib.R_BIAS + calib.R_WIRE);
    % SQ1 FB DAC bins => TES current (Amps)
    calib.FB1_CAL = (calib.V_FB1_MAX/(2^calib.BITS_FB1)) ./ ...
        (calib.R_FB1+calib.R_WIRE) ./ calib.M_FB1;
