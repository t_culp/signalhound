% calib_k4_3.m
%
% Sets calibration values for the E.5 FPU in K4
%
% MVL 20111019

function calib=calib_k4_3()

   
    disp('Using K4r3 calibrations')
    % use hard-coded values from Justus for now
    calib.R_SH = 0.0030;	% Ohms (at 4K, different chip)
    
    calib.M_FB1 = -7.99*ones(1,16);
    
    calib.R_WIRE = 132 *ones(1,16); % ohms include in R_FB1 and R_BIAS
    
    calib.R_BIAS = [400.00 400.00 400.00 400.00 ...
        400.00 400.00 400.00 400.00 ...
        400.00 400.00 400.00 400.00 ...
        400.00 400.00 400.00 400.00 ];
    
    calib.V_B_MAX = [2.500 2.500 2.500 2.500 ...
        2.500 2.500 2.500 2.500 ...
        2.500 2.500 2.500 2.500 ...
        2.500 2.500 2.500 2.500 ];
    
%     calib.R_FB1 =     [14500.00 14500.00 14500.00 14500.00 ...
%         14500.00 14500.00 14500.00 14500.00 ...
%         14500.00 14500.00 14500.00 14500.00 ...
%         14500.00 14500.00 14500.00 14500.00 ]; 

    calib.R_FB1 = 15000*ones(1,16);
    
    calib.V_FB1_MAX =   2* [.500 .500 .500 .500 ...
        .500 .500 .500 .500 ...
        .500 .500 .500 .500 ...
        .500 .500 .500 .500 ];
    
    calib.BITS_BIAS = 16; % number of bits in TES bias DAC
    calib.BITS_FB1 = 14; % number of bits in FB1 DAC
    
