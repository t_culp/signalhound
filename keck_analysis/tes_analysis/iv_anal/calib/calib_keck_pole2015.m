% calib_keck_pole2015.m
%
% Sets calibration values to match Keck for the 2012 season
%

function calib=calib_keck_pole2015()
 
% S48-006 15kOhm Run Keck3_2, Pole
% S48-005 15kOhm Run Keck2_4, Pole
% S48-007 15kOhm Run Keck1_4, Pole
% Using the generic values since things are within a few percent and 
%   it has been argued that we calibrate astronomically anyways.
%
% removed the bicep2 focal plane/resistor values

    calib.R_SH = 0.0030;	% Ohms (at 4K, different chip)
    
    calib.M_FB1 = 8*ones(1,80);
    
    calib.R_WIRE = 48 *ones(1,80); % ohms include in R_FB1 and R_BIAS
    
    calib.R_BIAS = 400*ones(1,80);
    
    calib.V_B_MAX = 2.5*ones(1,80);
    
    calib.R_FB1 = 15000*ones(1,80);
    
    calib.V_FB1_MAX = 1*ones(1,80);
    
    calib.BITS_BIAS = 16; % number of bits in TES bias DAC
    calib.BITS_FB1 = 14; % number of bits in FB1 DAC
    
