% calib_Spider_R5p0.m
%
% Sets calibration values to match Spider r5.0
%
% 20110325 JPF First-pass measured values

function calib = calib_Spider_R5p0()
    disp(' Using calibrations from SPIDER Run 5.0 ...');
    
    % use hard-coded values from Justus for now
    calib.R_SH = 0.0030;	% Ohms (at 4K, different chip)
    
    % approx. for MUX09a
    calib.M_FB1 = [ -8.0 -8.0 -8.0 -8.0 -8.0 -8.0 -8.0 -8.0 ...
        -8.0 -8.0 -8.0 -8.0 -8.0 -8.0 -8.0 -8.0 ] ;
    
    % BT/JPF 20110325 measurement 
    calib.R_WIRE = 140*ones(1,16); % ohms
    % mean values for new SPIDER test stand MCE - BT/JPF 20110325
    calib.R_BIAS = 402.4*ones(1,16); % ohms
    calib.V_B_MAX = 2.500*ones(1,16); % Max output voltage (16-bit)
    % JPF 20110906 measurement
    calib.R_FB1 = [15039.8 15039.8 15055.6 15038.2 ...
        15039.8 15039.8 15045.7 15041.3 ...
        15023.2 15039.8 15031.5 15039.0 ...
        15030.0 15039.0 15039.0 15046.5]; % Ohms
    calib.V_FB1_MAX = [0.965 0.965 0.966 0.971 ...
        0.965 0.965 0.973 0.959 ...
        0.967 0.965 0.966 0.968 ...
        0.972 0.968 0.968 0.970]; % Volts
    
    calib.BITS_BIAS = 16; % number of bits in TES bias DAC
    calib.BITS_FB1 = 14; % number of bits in FB1 DAC
    
    % Bias ADC bins => bias current (Amps)
    calib.BIAS_CAL = (calib.V_B_MAX/(2^calib.BITS_BIAS)) ./ ...
        (calib.R_BIAS + calib.R_WIRE);
    % SQ1 FB DAC bins => TES current (Amps)
    calib.FB1_CAL = (calib.V_FB1_MAX/(2^calib.BITS_FB1)) ./ ...
        (calib.R_FB1+calib.R_WIRE) ./ calib.M_FB1;
