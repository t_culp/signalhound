% calib_keck3_2.m
%
% Sets calibration values to match BICEP2 deployment values for RevD.2
%
% JAB 20100413  Based on calib_Spider_R2p0 from JPF
% SAS 20101227  Made for Keck3_2

function calib=calib_keck3_2()
 
% S48-006 15kOhm Run Keck3_2, Pole

    % use hard-coded values from Justus for now
    calib.R_SH = 0.0030;	% Ohms (at 4K, different chip)
    
    calib.M_FB1 = 8*ones(1,16);
    
    calib.R_WIRE = 48 *ones(1,16); % ohms include in R_FB1 and R_BIAS
    %calib.R_WIRE = 106*ones(1,16); %ohms for the Hienostat
    
    calib.R_BIAS = [   502.87 502.99 503.45 503.21 ...
		       503.95 502.32 503.87 503.21 ...
		       475.89 504.45 505.77 504.45 ...
		       505.69 502.06 506.74 504.87 ];
    
    calib.V_B_MAX = [   2.490 2.489 2.492 2.489 ...
			2.493 2.491 2.492 2.489 ...
			2.493 2.494 2.500 2.494 ...
			2.499 2.493 2.499 2.494 ];
    
    calib.R_FB1 =     [   14043.82 14124.25 14057.23 14004.10 ...
			  14235.33 14921.57 13936.75 14637.64 ...
			  14386.18 14654.09 15226.36 14546.86 ...
			  14608.09 14141.87 14480.82 14283.61 ]; 
    
    calib.V_FB1_MAX =   2* [  .455 .459 .454 .458 ...
			   .464 .483 .453 .468 ...
			   .463 .466 .485 .474 ...
			   .473 .461 .466 .467 ];
    
    calib.BITS_BIAS = 16; % number of bits in TES bias DAC
    calib.BITS_FB1 = 14; % number of bits in FB1 DAC
    
