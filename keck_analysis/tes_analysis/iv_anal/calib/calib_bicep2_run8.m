% calib_bicep2_run8.m
%
% Sets calibration values to match BICEP2 deployment values for RevD.2
%
% JAB 20100413  Based on calib_Spider_R2p0 from JPF

function calib=calib_bicep2_run8()
 
% CSR-001 6.2kOhm Run8, Pole

    % use hard-coded values from Justus for now
    calib.R_SH = 0.0030;	% Ohms (at 4K, different chip)
    
    calib.M_FB1 = 15.5*ones(1,16);
    
    calib.R_WIRE = 0 *ones(1,16); % ohms include in R_FB1 and R_BIAS
    
    calib.R_BIAS = [    523.56 523.45 523.72 522.54 ...
			   523.17 522.44 523.12 523.24 ...
			   523.63 523.70 523.50 523.21 ...
			   523.05 523.43 523.92 522.83 ];
    
    calib.V_B_MAX = [   2.4990 2.4990 2.4990 2.4990 ...
			2.5000 2.4990 2.4990 2.4990 ...
			2.5000 2.5000 2.4990 2.5000 ...
			2.4990 2.5000 2.5000 2.5000 ];
    
    calib.R_FB1 =     [ 6385.46 6360.04 6412.51 6433.39 ...
			   6418.75 6409.18 6363.12 6403.19 ...
			   6386.49 6423.52 6395.69 6386.03 ...
			   6376.11 6352.47 6382.83 6369.60 ]; 
    
    calib.V_FB1_MAX =   [  .9580 .9670 .9620 .9650 ...
				.9630 .9680 .9610 .9670 ...
				.9710 .9700 .9660 .9710 ...
				.9630 .9660 .9640 .9620 ];
    
    calib.BITS_BIAS = 16; % number of bits in TES bias DAC
    calib.BITS_FB1 = 14; % number of bits in FB1 DAC
    
