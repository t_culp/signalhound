% calib_bicep3_run8.m
%
% Copy values from calib BICEP3 run 2.2 and change the number of columns
% 2014-08-31 KW
%
% JAG 2014-09-09
% Updated to mean values of full mce calibration probing for 3 run 4 MCE's. 
% Still need to update to full arrays, but the variation is tiny, all < 1%. 
%
% HH 2014-09-20
% Updated R_shunt from 0.003 to 0.004 from measurement, this is assuming the nyquist chips from NIST are uniform
%
% KW 2015-04-27; values for 2015 season; changed numcols from 32 to 30;  moved in tes_analysis/iv_anal/calib/ folder.


function calib=calib_bicep3_run8()

    disp('Using calibration: calib_bicep3_run8 (adjust in calib_select.m)')

    numcols = 30; % number of columns being used
 
    calib.R_SH = 0.004;	% Caltech dip test (09/20/2014)
    
    calib.R_WIRE = 151.0*ones(1,numcols); %measured wire resistance
    
    calib.R_BIAS = 475.0*ones(1,numcols); %measured mean MCE bias resistance
    calib.BITS_BIAS = 15; % number of bits in TES bias DAC (2^16 / 2, since volt range is plus/minus)
    calib.V_B_MAX = 5.000*ones(1,numcols); %measured mean MCE bias output (plus/minus 4.999V)
    
    calib.R_FB1 =  2114.0*ones(1,numcols); %measured mean MCE fb resistance
    calib.BITS_FB1 = 14; % number of bits in FB1 DAC
    calib.V_FB1_MAX =  0.972*ones(1,numcols); %measured mean MCE fb output
    calib.M_FB1 = 25*ones(1,numcols); %waiting on Kent; measured from Ti superconducting slope
    
return
