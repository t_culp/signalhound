% calib_sk1.m
%
% Sets calibration values for the hybrid E.4 FPU in SK
%
% GPT 20100613

function calib=calib_sk1()
 
% We haven't measured everything directly, so we can borrow calib_keck_pole2011 nominal values for most.
% Use the calib_bicep2_run8 turns ratio because we reused rev A chips for E.4.
% The MCE should have the 2000 ohm resistors to match the chips.

    % use hard-coded values from Justus for now
    calib.R_SH = 0.0030;	% Ohms (at 4K, different chip)
    
    calib.M_FB1 = 15.5*ones(1,16); % rev A chips
    
    calib.R_WIRE = 48 *ones(1,16); % ohms include in R_FB1 and R_BIAS
    
    calib.R_BIAS = [500.00 500.00 500.00 500.00 ...
        500.00 500.00 500.00 500.00 ...
        500.00 500.00 500.00 500.00 ...
        500.00 500.00 500.00 500.00 ];
    
    calib.V_B_MAX = [2.500 2.500 2.500 2.500 ...
        2.500 2.500 2.500 2.500 ...
        2.500 2.500 2.500 2.500 ...
        2.500 2.500 2.500 2.500 ];
    
%     calib.R_FB1 =     [14500.00 14500.00 14500.00 14500.00 ...
%         14500.00 14500.00 14500.00 14500.00 ...
%         14500.00 14500.00 14500.00 14500.00 ...
%         14500.00 14500.00 14500.00 14500.00 ]; 

    calib.R_FB1 = 2000*ones(1,16);
    
    calib.V_FB1_MAX =   2* [.500 .500 .500 .500 ...
        .500 .500 .500 .500 ...
        .500 .500 .500 .500 ...
        .500 .500 .500 .500 ];
    
    calib.BITS_BIAS = 16; % number of bits in TES bias DAC
    calib.BITS_FB1 = 14; % number of bits in FB1 DAC
    
