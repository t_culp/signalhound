% find dir of tes_analysis archive
p=fileparts(mfilename('fullpath'));

% add paths to sub-dirs
addpath([p,'/file_io']);
addpath([p,'/file_io/arcfile']);
addpath([p,'/file_io/dirfile']);
addpath([p,'/iv_anal']);
addpath([p,'/magfield']);
addpath([p,'/mex_bin']);
addpath([p,'/util']);
addpath([p,'/viewer']);
addpath([p,'/iv_anal/calib/']);
clear p
