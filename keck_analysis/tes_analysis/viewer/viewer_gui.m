function varargout = viewer_gui(varargin)
% VIEWER_GUI M-file for viewer_gui.fig
%      VIEWER_GUI, by itself, creates a new VIEWER_GUI or raises the existing
%      singleton*.
%
%      H = VIEWER_GUI returns the handle to a new VIEWER_GUI or the handle to
%      the existing singleton*.
%
%      VIEWER_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VIEWER_GUI.M with the given input arguments.
%
%      VIEWER_GUI('Property','Value',...) creates a new VIEWER_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before viewer_gui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to viewer_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help viewer_gui

% Last Modified by GUIDE v2.5 02-Sep-2008 10:59:04

%#function read_mce time2psd

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @viewer_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @viewer_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1}) && ~exist (varargin{1}, 'dir')
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before viewer_gui is made visible.
function viewer_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to viewer_gui (see VARARGIN)

% Choose default command line output for viewer_gui
handles.output = hObject;

handles.mux_row = 0;
set (handles.row_num_edit, 'String', {num2str(handles.mux_row)});
handles.mux_col = 0;
set (handles.col_num_edit, 'String', {num2str(handles.mux_col)});

handles.list_row = [];
handles.list_col = [];
handles.point_files = [];

if ~isempty (varargin) && iscell (varargin{1})
	dirlist = varargin{1};
elseif ~isempty (varargin) && ischar (varargin{1}) && exist (varargin{1}, 'dir')
	dirlist = {varargin{1}};
else
	if exist ('/data/cryo', 'dir')
		startdir = '/data/cryo';
	else
		startdir = '.';
	end;
	dirlist = {uigetdir(startdir,'Select directory with data files')};
end;
% workdir = '/home/reuben/bicep/forWalter/';

handles.dirlist = dirlist;
[handles.flist handles.full_flist] = scan_data_files (dirlist);
set (handles.file_listbox, 'String', handles.flist);
set (handles.file_listbox, 'Value', 1);
handles.onf = 1;
handles.freq_domain = 0;


handles.topAxes = [];
handles.btmAxes = [];
handles.all_figures = [];
handles.current_fig = [];
handles.typ_rescan = [];
handles.typ_rescan = @indv_rescanFunc;
handles.hold_views = [];
handles.all_checked = 0;
handles.bypass_stor = 0;
handles.rem_ref_point = 0;
handles.ys_all_views = 0;
handles.X_limits = [];
handles.tm_rng_checked = 0;
handles.tim_running = 0;
handles.auto_pos = 1;
handles.in_submit_tm = 0;
handles.where_jc = [];
handles.first_add = 1;
handles.cld_by_prs = 1;
handles.color = {};
handles.first_rescanFunc = 0;

handles.help = 0;
set(handles.help_display, 'String', ['Check the box above next to the word "Help" to go into help mode,', ... 
    ' in which you can learn how to use objects in the GUI by clicking on ', ...
    'them (or, if they are an edit box, click and press enter). A short description will be displayed in this box (the object ', ...
    'will not have its other effects). Unchecking the box will cause the GUI to operate normally.']);
% This sets up the initial plot - only do when we are invisible
% so window can get raised using viewer_gui.
handles = load_dataset (handles);
%plot_dataset (handles);
% Update handles structure
guidata (hObject, handles);
%rescanFunc(hObject, handles);

% UIWAIT makes viewer_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function [flist full_flist] = scan_data_files (dirlist)
	flist = {};
	full_flist = {};
	for (jj = 1:length (dirlist))
		if exist (dirlist{jj}, 'dir')
			d = dir (fullfile (dirlist{jj}, '*.run'));
			for (ii = 1:length (d))
				bare_fname = d(ii).name(1:(end-4));
				if exist (fullfile (dirlist{jj}, bare_fname), 'file') ...
				&& exist (fullfile (dirlist{jj}, [bare_fname '.run']), 'file')
					flist = {flist{:}, bare_fname};
					full_flist = {full_flist{:}, fullfile(dirlist{jj},bare_fname)};
				else
					disp (['Skipping ' bare_fname]);
				end;
			end;
		elseif exist (dirlist{jj}, 'file')
			[ondir bare_fname] = fileparts (dirlist{jj});
			flist = {flist{:}, bare_fname};
			full_flist = {full_flist{:}, dirlist{jj}};
		end;
	end;
	if isempty (flist)
		disp (['No MAS data found.']);
		return;
	end;

function handles = load_dataset (h_in)
	handles = h_in;
	mux_row = handles.mux_row;
	mux_col = handles.mux_col;
	handles.fb = {};
	handles.tofs = {};
	for (ii = 1:length (handles.onf))
        fname = handles.full_flist{handles.onf(ii)};
		S = read_mce (fname, [mux_row, mux_col], [], ...
			'DO_UNPACK', handles.unpack, 'DO_UNWRAP', handles.unwrap, 'DO_SCALE', handles.scale);
		p = find ([S(:).mux_row] == mux_row & [S(:).mux_col] == mux_col);
		if isempty(p)
			handles.fb{ii} = [];
			hanldes.err{ii} = [];
			handles.tofs{ii} = [];
		else
			handles.fb{ii} = S(p).fb;
			handles.err{ii} = S(p).err;
			handles.tofs{ii} = S(p).tofs;
		end;
	end;


function [LL1, g_objs, color] = plot_dataset (handles)
	if (handles.freq_domain)
		xlab = 'frequency / Hz';
		if (handles.unpack == 1) && (handles.scale == 1)
			ylab_fb = 'fb / V/rtHz nom. at ADC';
		else
			ylab_fb = 'fb / raw bins/rtHz';
		end;
		ylab_err = 'error signal';
	else
		xlab = 'time / s';
		if (handles.unpack == 1) && (handles.scale == 1)
			ylab_fb = 'fb / V nom. at ADC';
		else
			ylab_fb = 'fb / raw bins';
		end;
		ylab_err = 'error signal';
	end;
	if (handles.show_err == 1) && (handles.show_fb == 1)
        set_two_axes (handles);
        axes (handles.topAxes{handles.rem_ref_point});
		[axlim1, LL1, g_objs, color] = plot_ts_or_psd (handles.fb, ...
			handles.freq_domain, handles);
        %so next axes remembers correct color
        handles.color = color;
		ylabel (ylab_fb);
		set (gca, 'XTickLabel', []);
        
		axes (handles.btmAxes{handles.rem_ref_point});
		[axlim2, LL2, g_objs, color] = plot_ts_or_psd (handles.err, ...
			handles.freq_domain, handles);
		if isempty (axlim2)
			axlim2 = [0 1 0 1];
		end;
		if isempty (axlim1)
			axlim1 = axlim2;
			axlim(1:2) = axlim2(1:2);
		else
			axlim(1:2) = axlim1(1:2);
		end;
		axis ([axlim(1), axlim(2), ...
			axlim2(3), axlim2(4)]);
		zoom reset;
		xlabel (xlab);
		ylabel (ylab_err); 
		axes (handles.topAxes{handles.rem_ref_point});
                axis ([axlim(1), axlim(2), ...
                        axlim1(3), axlim1(4)]);
		zoom reset;
	else
		set_one_axis (handles);        
		axes (handles.topAxes{handles.rem_ref_point});
		if (handles.show_err == 0)
			[axlim1, LL1, g_objs, color] = plot_ts_or_psd (handles.fb, ...
				handles.freq_domain, handles);
			ylabel (ylab_fb);
		else
			[axlim1, LL1, g_objs, color] = plot_ts_or_psd (handles.err, ...
				handles.freq_domain, handles);
			ylabel (ylab_err);
        end
		axis ([axlim1]);
		zoom reset;
		xlabel (xlab);
	end;
    
	axes (handles.topAxes{handles.rem_ref_point});

% Ensure we have only one visible axes, using full axis area
function set_one_axis (handles)
	posstr = 'Position';
	if strcmp (get (handles.btmAxes{handles.rem_ref_point}, 'Visible'), 'on')
		pos1 = get (handles.topAxes{handles.rem_ref_point}, posstr);
		pos2 = get (handles.btmAxes{handles.rem_ref_point}, posstr);
		newpos(1) = min (pos1(1), pos2(1));
		newpos(2) = min (pos1(2), pos2(2));
		newpos(3) = max (pos1(1)+pos1(3), pos2(1)+pos2(3)) - newpos(1);
		newpos(4) = max (pos1(2)+pos1(4), pos2(2)+pos2(4)) - newpos(2);
		set (handles.btmAxes{handles.rem_ref_point}, 'Visible', 'off');
		set (handles.topAxes{handles.rem_ref_point}, 'Visible', 'on');
		set (handles.topAxes{handles.rem_ref_point}, posstr, newpos);
	else
		set (handles.topAxes{handles.rem_ref_point}, 'Visible', 'on');
	end;

% Ensure we have two visible axes, stacked vertically
function set_two_axes (handles)
	posstr = 'Position';
    if handles.just_created == 1
		oldpos = get (handles.topAxes{handles.rem_ref_point}, posstr);
		newpos1(1) = oldpos(1);
		newpos2(1) = oldpos(1);
		newpos1(3) = oldpos(3);
		newpos2(3) = oldpos(3);
		newpos1(4) = oldpos(4) / 2;
		newpos2(4) = oldpos(4) / 2;
		newpos2(2) = oldpos(2);
		newpos1(2) = oldpos(2) + oldpos(4) / 2;
		set (handles.topAxes{handles.rem_ref_point}, posstr, newpos1);
		set (handles.btmAxes{handles.rem_ref_point}, posstr, newpos2);
		set (handles.topAxes{handles.rem_ref_point}, 'Visible', 'on');
		set (handles.btmAxes{handles.rem_ref_point}, 'Visible', 'on');
    end

function [axlim, LL, g_objs, color] = plot_ts_or_psd (ydat, ispsd, handles, dat_scale)
	%color and g_objs need to be put out to be used later in
	%indv_rescanFunc; same thing goes for the outputs added in plot_dataset
    if (nargin < 4) || isempty (dat_scale)
		dat_scale = 1;
	end
    hold all;
	CC = 'bgrkcmy';
	axlim = [];
    LL = handles.LL;
    g_objs = handles.g_objs;
	style = [];
	if (handles.draw_points)
		style = [style '.'];
	end;
	if (handles.draw_lines)
		style = [style '-'];
	end;
	MS = handles.markersize;
    color = handles.color;
	for (ii = 1:length(handles.onf))
        %reference for storing handles to use for legend
        obj_num = (length(handles.onf)*(handles.which_pix - 1)) + ii;
        %when a data set is plotted, you have to give it a color, which it
        %has to remember each time it rescans (so don't define it again
        %with random numbers each time). However, an error is given if a   
        %reference (such as using isempty) is made to a position beyond the
        %current limits of color; when that happens, you just have to
        %define it outright
        new = 0;
        try 
            if isempty(color{handles.rem_ref_point, handles.which_pix, ii})
                color{handles.rem_ref_point, handles.which_pix, ii} = [rand rand rand];
                new = 1;
            end
        catch 
            color{handles.rem_ref_point, handles.which_pix, ii} = [rand rand rand];
            new = 1;
        end
        %get new color if same as other color on same plot, or if it is
        %a light color; then test this new one
        while new == 1
            new = 0;
            for pix_on = 1:(handles.which_pix - 1)
                for file_on = 1:(ii - 1)
                    if isequal(color{handles.rem_ref_point, handles.which_pix, ii}, color{handles.rem_ref_point, pix_on, file_on}) || sum(color{handles.rem_ref_point, handles.which_pix, ii}) < 1
                        color{handles.rem_ref_point, handles.which_pix, ii} = [rand rand rand];
                        new = 1;
                    end
                end
            end
        end
        
        %loglog doesn't work if axes had data on them created using the
        %plot function and hold is on
        if ii == 1 && handles.which_pix == 1
            hold off;
        end 
		if ispsd        
			g_objs(obj_num) = loglog (1e-10, 1e-10, [CC(mod(ii-1,7)+1) style], 'MarkerSize', 10, 'Color', color{handles.rem_ref_point, handles.which_pix, ii}); 
        else
			g_objs(obj_num) = plot (-1e6, -1e6, [CC(mod(ii-1,7)+1) style], 'MarkerSize', 10, 'Color', color{handles.rem_ref_point, handles.which_pix, ii}); 
		end;
        str_lgnd_entry = ['r:', num2str(handles.mux_row), ' c:', ...
            num2str(handles.mux_col), '; ', ...
            strrep(handles.flist{handles.onf(ii)},'_','\_')];
        LL = {LL{:}, str_lgnd_entry};

		hold all
	end;
	for (ii = 1:length (handles.onf))
		yy = ydat{ii};
		tofs = handles.tofs{ii};
		if ~isempty (yy)
			if ispsd
                        	[psd ff] = time2psd (yy, 1/mean(diff(tofs)));
				axtmp = [min(ff(ff>0)) max(ff) dat_scale*min(psd(ff>0)) dat_scale*max(psd(ff>0))];
                loglog (ff, psd*dat_scale, [CC(mod(ii-1,7)+1) style], 'MarkerSize', MS, 'Color', color{handles.rem_ref_point, handles.which_pix, ii});
			else
				axtmp = [min(tofs) max(tofs) dat_scale*min(yy) dat_scale*max(yy)];
				plot (tofs, yy*dat_scale, [CC(mod(ii-1,7)+1) style], 'MarkerSize', MS, 'Color', color{handles.rem_ref_point, handles.which_pix, ii});
			end;
			if isempty (axlim)
				axlim = axtmp;
			else
				axlim(1) = min (axlim(1), axtmp(1));
				axlim(2) = max (axlim(2), axtmp(2));
				axlim(3) = min (axlim(3), axtmp(3));
				axlim(4) = max (axlim(4), axtmp(4));
			end;
            
		end;
	end;
	grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% End of "real" code, beginning of user interface mechanics


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Callbacks for list box with temperatures

% --- Executes on selection change in file_listbox.
function file_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to file_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns file_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from file_listbox

if handles.help == 0
handles.onf = get (hObject, 'Value');
if (handles.onf <= length(handles.flist))
	handles = load_dataset (handles);
end;
else
    set(handles.help_display, 'String', ...
        'The data sources to take the pixel information from. Click on the one you want, and use ctrl-left click (or just drag your mouse down) to select more. Use the "Add data" button from the "Actions" menu to add another file containing data.');
end
guidata (hObject, handles);


% --- Executes during object creation, after setting all properties.
function file_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to file_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The following are all the callback functions needed to make the edit
% boxes & sliders work together.  This is trivial in LabVIEW...
% Feel free to use as a template for other numeric inputs, but hopefully
% it shouldn't be necessary to edit this.  No detector physics
% in this section.


function row_num_edit_Callback(hObject, eventdata, handles)
% hObject    handle to row_num_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of row_num_edit as text
%        str2double(get(hObject,'String')) returns contents of row_num_edit as a double
if handles.help == 0
vv = str2double (get (hObject, 'String'));
if ~isempty (vv) && vv >= 0
	handles.mux_row = vv;
	guidata (hObject, handles);
else
	set (hObject, 'String', {num2str(handles.mux_row)});
end;
else
    set(handles.help_display, 'String', ...
        'Change the row number of the pixel by typing in the box, or using the sliders to the side');
end


% --- Executes during object creation, after setting all properties.
function row_num_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to row_num_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function col_num_edit_Callback(hObject, eventdata, handles)
% hObject    handle to col_num_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of col_num_edit as text
%        str2double(get(hObject,'String')) returns contents of col_num_edit as a double
if handles.help == 0
vv = str2double (get (hObject, 'String'));
if ~isempty (vv) && vv >= 0
    handles.mux_col = vv;
    guidata (hObject, handles);
else
    set (hObject, 'String', {num2str(handles.mux_col)});
end;
else
    set(handles.help_display, 'String', ...
        'Change the column number of the pixel by typing in the box, or using the sliders to the side.');
end


% --- Executes during object creation, after setting all properties.
function col_num_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to col_num_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function row_num_slider_Callback(hObject, eventdata, handles)
% hObject    handle to row_num_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
if handles.help == 0
vv = get (hObject, 'Value');
handles.mux_row = handles.mux_row + sign(vv);
if (handles.mux_row < 0)
	handles.mux_row = 0;
end;
set (handles.row_num_edit, 'String', {num2str(handles.mux_row)});
set (hObject, 'Value', 0);
else
    set(handles.help_display, 'String', ...
        'Press the arrows to change the row number of the pixel, or use the edit box to the side.');
end
guidata (hObject, handles);



% --- Executes during object creation, after setting all properties.
function row_num_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to row_num_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set (hObject, 'Min', -1);
set (hObject, 'Max', +1);


% --- Executes on slider movement.
function col_num_slider_Callback(hObject, eventdata, handles)
% hObject    handle to col_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
	if handles.help == 0
    vv = get (hObject, 'Value');
	handles.mux_col = handles.mux_col + sign(vv);
	if (handles.mux_col < 0)
		handles.mux_col = 0;
	end;
	set (handles.col_num_edit, 'String', {num2str(handles.mux_col)});
	set (hObject, 'Value', 0);
    else
        set(handles.help_display, 'String', ...
            'Press the arrows to change the column number of the pixel, or use the edit box to the side.');
    end
    guidata (hObject, handles);
    
	
% --- Executes during object creation, after setting all properties.
function col_num_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to col_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
	
	% Hint: slider controls usually have a light gray background.
	if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
	set(hObject,'BackgroundColor',[.9 .9 .9]);
	end
	set (hObject, 'Min', -1);
	set (hObject, 'Max', +1);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Generic sorts of callbacks, totally boring


% --- Outputs from this function are returned to the command line.
function varargout = viewer_gui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 1
    set(handles.help_display, 'String', ...
        'These controls are for the GUI itself, not the plots it creates; the plots have all the normal figure controls inside their respective windows.');
end

% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
printdlg(handles.figure1)
end

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)
end

% --------------------------------------------------------------------
function view_menu_Callback(hObject, eventdata, handles)
% hObject    handle to view_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 1
    set(handles.help_display, 'String', ...
        'Change how the data is displayed in the plot(s).');
end

% --------------------------------------------------------------------
function points_menu_Callback(hObject, eventdata, handles)
% hObject    handle to points_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
handles.draw_points = 1;
if handles.markersize == 6
    handles.markersize = 1;
else
    handles.markersize = 6;
end
guidata (hObject, handles);
%indv_rescan serves to change plot and store info
if handles.ys_all_views == 0
    [handles.X_limits handles.hold_views handles.color] = indv_rescanFunc(hObject, handles);
else
    handles.hold_views = chng_all_views(hObject, handles);
    rescanFunc(hObject, handles);
end
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
guidata(hObject, handles);
end


% --------------------------------------------------------------------
function lines_menu_Callback(hObject, eventdata, handles)
% hObject    handle to lines_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
if handles.draw_lines == 0
    handles.draw_lines = 1;
else
    handles.draw_lines = 0;
end
guidata (hObject, handles);

if handles.ys_all_views == 0
    [handles.X_limits handles.hold_views handles.color] = indv_rescanFunc(hObject, handles);
else
    handles.hold_views = chng_all_views(hObject, handles);
    rescanFunc(hObject, handles);
end
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
guidata(hObject, handles);
end


% --- Executes during object creation, after setting all properties.
function lines_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lines_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.draw_lines = 1;
guidata (hObject, handles);

% --- Executes during object creation, after setting all properties.
function points_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to points_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.draw_points = 1;
handles.markersize = 1;
guidata (hObject, handles);


% --------------------------------------------------------------------
function errsig_menu_Callback(hObject, eventdata, handles)
% hObject    handle to errsig_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
if handles.show_err == 0
    handles.just_created = 1;
    handles.show_err = 1;
else
    handles.show_err = 0;
end

guidata (hObject, handles);
if handles.ys_all_views == 0
    [handles.X_limits handles.hold_views handles.color] = indv_rescanFunc(hObject, handles);
else
    handles.hold_views = chng_all_views(hObject, handles);
    rescanFunc(hObject, handles);
end
guidata(hObject, handles);
handles.just_created = 0;
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
guidata(hObject, handles);
end


% --- Executes during object creation, after setting all properties.
function errsig_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to errsig_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.show_err = 0;
handles.show_fb = 1;
guidata (hObject, handles);

% --------------------------------------------------------------------
function timedomain_menu_Callback(hObject, eventdata, handles)
% hObject    handle to timedomain_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
if handles.freq_domain == 1
    handles.freq_domain = 0;
else
    handles.freq_domain = 1;
end
guidata (hObject, handles);

if handles.ys_all_views == 0
    [handles.X_limits handles.hold_views handles.color] = indv_rescanFunc(hObject, handles);
else
    handles.hold_views = chng_all_views(hObject, handles);
    rescanFunc(hObject, handles);
end
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
guidata(hObject, handles);
end


% --------------------------------------------------------------------
function freqdomain_menu_Callback(hObject, eventdata, handles)
% hObject    handle to freqdomain_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
if handles.freq_domain == 0
    handles.freq_domain = 1;
else
    handles.freq_domain = 0;
end
guidata (hObject, handles);

if handles.ys_all_views == 0
    [handles.X_limits handles.hold_views handles.color] = indv_rescanFunc(hObject, handles);
else
    handles.hold_views = chng_all_views(hObject, handles);
    rescanFunc(hObject, handles);
end
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
guidata(hObject, handles);
end


% --- Executes during object creation, after setting all properties.
function timedomain_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to timedomain_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.freq_domain = 0;
guidata (hObject, handles);

% --- Executes during object creation, after setting all properties.
function freqdomain_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freqdomain_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --------------------------------------------------------------------
function rawwords_menu_Callback(hObject, eventdata, handles)
% hObject    handle to rawwords_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
if handles.unpack == 1
    handles.unpack = 0;
else
    handles.unpack = 1;
end

handles = load_dataset (handles);
guidata (hObject, handles);
if handles.ys_all_views == 0
    [handles.X_limits handles.hold_views handles.color] = indv_rescanFunc(hObject, handles);
else
    handles.hold_views = chng_all_views(hObject, handles);
    rescanFunc(hObject, handles);
end
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
guidata(hObject, handles);
end


% --------------------------------------------------------------------
function unwrap_menu_Callback(hObject, eventdata, handles)
% hObject    handle to unwrap_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
if handles.unwrap == 0
    handles.unwrap = 1;
else
    handles.unwrap = 0;
end

handles = load_dataset (handles);
guidata (hObject, handles);
if handles.ys_all_views == 0
    [handles.X_limits handles.hold_views handles.color] = indv_rescanFunc(hObject, handles);
else
    handles.hold_views = chng_all_views(hObject, handles);
    rescanFunc(hObject, handles);
end
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
guidata(hObject, handles);
end


% --------------------------------------------------------------------
function rawint_menu_Callback(hObject, eventdata, handles)
% hObject    handle to rawint_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
if handles.scale == 1
    handles.scale = 0;
else
    handles.scale = 1;
end

handles = load_dataset (handles);
guidata (hObject, handles);
if handles.ys_all_views == 0
    [handles.X_limits handles.hold_views handles.color] = indv_rescanFunc(hObject, handles);
else
    handles.hold_views = chng_all_views(hObject, handles);
    rescanFunc(hObject, handles);
end
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
guidata(hObject, handles);
end


% --------------------------------------------------------------------
function nomv_menu_Callback(hObject, eventdata, handles)
% hObject    handle to nomv_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
if handles.scale == 0
    handles.scale = 1;
else
    handles.scale = 0;
end

handles = load_dataset (handles);
guidata (hObject, handles);
if handles.ys_all_views == 0
    [handles.X_limits handles.hold_views handles.color] = indv_rescanFunc(hObject, handles);
else
    handles.hold_views = chng_all_views(hObject, handles);
    rescanFunc(hObject, handles);
end
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
guidata(hObject, handles);
end


% --- Executes during object creation, after setting all properties.
function rawwords_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rawwords_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
%set (hObject, 'Checked', 0);
handles.unpack = 1;
guidata (hObject, handles);


% --- Executes during object creation, after setting all properties.
function unwrap_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to unwrap_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.unwrap = 1;
guidata (hObject, handles);


% --- Executes during object creation, after setting all properties.
function rawint_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rawint_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
%set (hObject, 'Checked', 0);
handles.scale = 1;
guidata (hObject, handles);


% --- Executes during object creation, after setting all properties.
function nomv_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nomv_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


function timerFcn_Callback(obj, event)
    handles = get (obj, 'UserData');
    handles.typ_rescan(handles.hObj_ref, handles);
    %when stop is called, it immediately aborts function; that is why the
    %start delay is used as a period, and the start call is in the stop
    %function
    stop(handles.tim);

function stopFcn_Callback (obj, event)
    handles = get (obj, 'UserData');
    start(handles.tim);
    
function rescan_edit_box_Callback(hObject, eventdata, handles)
% hObject    handle to rescan_edit_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rescan_edit_box as text
%        str2double(get(hObject,'String')) returns contents of rescan_edit_box as a
%        double
if handles.help == 0
handles.rescan_period = (str2double(get(hObject, 'String')))*60;
else
    set(handles.help_display, 'String', ...
        'The amount of time (in minutes) between calls of the rescan function. Whether or not the rescan function applies to one or all of the plots depends on the check box above.');
end
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function rescan_edit_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rescan_edit_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in start_button.
function start_button_Callback(hObject, eventdata, handles)
% hObject    handle to start_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
    
handles.hObj_ref = hObject;
handles.tim = timer ('StartDelay', handles.rescan_period, 'Period', 1, ...
        'UserData', handles, ...
        'TimerFcn', @timerFcn_Callback, 'StopFcn', @stopFcn_Callback);
guidata (hObject, handles);
start(handles.tim);
%every time a change is made to handles, tim needs to know about it; that's
%why other functions need to know if tim is running
handles.tim_running = 1;
set(handles.tim, 'UserData', handles);
else
    set(handles.help_display, 'String', ...
        'Start the timer running. The first rescan call will happen after the period (defined above) has elapsed one time through. Changes can be made to the plots, and new plots can be added while the timer is running, but avoid having them coincide with a rescan call by the timer.');
end
guidata(hObject, handles);


% --- Executes on button press in stop_button.
function stop_button_Callback(hObject, eventdata, handles)
% hObject    handle to stop_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0

handles.tim_running = 0;
%calling the stop function would merely restart it; this is the source of
%the error you get when you hit the stop button
delete(handles.tim);

else
    set(handles.help_display, 'String', ...
        'Stop the timer running. Use this before you change the period, if you have started the timer before.');
end
guidata(hObject, handles);


% --------------------------------------------------------------------
function actions_menu_Callback(hObject, eventdata, handles)
% hObject    handle to actions_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 1
    set(handles.help_display, 'String', ...
        'Rescan the plot(s) or add datafiles the the listbox.');
end
guidata(hObject, handles);


function rescanFunc(hObject, handles)

[num_figs, num_pixs] = size(handles.list_row);
handles.just_created = 0;
for ref_point = 1:num_figs
    %loop through each open plot, rescan them; make sure view setting is
    %the same for each
    handles.rem_ref_point = ref_point;
    figure(handles.all_figures{ref_point});
    handles.markersize = handles.hold_views(ref_point, 1);
    handles.draw_lines = handles.hold_views(ref_point, 2);
    handles.show_err = handles.hold_views(ref_point, 3);
    handles.freq_domain = handles.hold_views(ref_point, 4);
    handles.unpack = handles.hold_views(ref_point, 5);
    handles.unwrap = handles.hold_views(ref_point, 6);
    handles.scale = handles.hold_views(ref_point, 7);
    %if plots have to be changed to show error, and some already have, you
    %have to know when to make the axes smaller, and when to keep them the
    %same
    if handles.first_rescanFunc == 1
        if handles.where_jc(ref_point) == 0
            handles.just_created = 1;
        else
            handles.just_created = 0;
        end
    end
    guidata(hObject, handles);
    indv_rescanFunc(hObject, handles);
    guidata(hObject, handles);
    
end
guidata(hObject, handles);


% --------------------------------------------------------------------
function add_data_menuItem_Callback(hObject, eventdata, handles)
% hObject    handle to add_data_menuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
    
newdir = uigetdir('.','Select directory with data files');
handles.dirlist = {handles.dirlist{:}, newdir};

remember_flist = {handles.flist{handles.onf}};
[handles.flist handles.full_flist] = scan_data_files (handles.dirlist);
set (handles.file_listbox, 'String', handles.flist);
stor_matches = [];
for loop_flist = 1:length(remember_flist) 
    tmp = strmatch(remember_flist{loop_flist}, handles.flist, 'exact');
    if ~(isempty(tmp))
        stor_matches = [stor_matches, tmp(1)];
    end
end
handles.onf = stor_matches;

if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end

guidata(hObject, handles);

end


% --------------------------------------------------------------------
function rescan_menuItem_Callback(hObject, eventdata, handles)
% hObject    handle to rescan_menuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
figure(handles.all_figures{handles.rem_ref_point});
guidata(hObject, handles);
handles.typ_rescan(hObject, handles);
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end        
guidata(hObject, handles);
end


% --- Executes on button press in add_channel_button.
function add_channel_button_Callback(hObject, eventdata, handles)
% hObject    handle to add_channel_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%this just deals with the string part, presenting the row and column in a
%readable fashion. Later the parentheses, commas, etc. are ignored
if handles.help == 0
empty_check = get(handles.multi_pixels_edit, 'String');
if numel(empty_check) == 0
    multi_string = ['(', num2str(handles.mux_row), ',', num2str(handles.mux_col), ')'];
else
    multi_string = get(handles.multi_pixels_edit, 'String');
    multi_string = [multi_string, ', (', num2str(handles.mux_row), ',', num2str(handles.mux_col), ')'];    
end
set (handles.multi_pixels_edit, 'String', multi_string);
else
    set(handles.help_display, 'String', ...
        'Press this button to add a pixel, whose row and column number are displayed in the boxes above, to the list (in the edit box below) of pixels to plot. You can also edit the box by hand.');
end
guidata(hObject, handles);

% --- Executes on button press in clear_channels_button.
function clear_channels_button_Callback(hObject, eventdata, handles)
% hObject    handle to clear_channels_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
set (handles.multi_pixels_edit, 'String', '');
else
    set(handles.help_display, 'String', ...
        'Press this button to clear the list of pixels below.');
end
guidata(hObject, handles);


% --- Executes on button press in all_refresh_check.
function all_refresh_check_Callback(hObject, eventdata, handles)
% hObject    handle to all_refresh_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of all_refresh_check
if handles.help == 0
handles.typ_rescan = [];
%either direct rescan calls to rescanFunc or indv_rescanFunc
if (get(hObject, 'Value') == get(hObject, 'Max'))
    handles.typ_rescan = @rescanFunc;
    handles.all_checked = 1;
else
    handles.typ_rescan = @indv_rescanFunc;
    handles.all_checked = 0;
end
guidata(hObject, handles);
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
else
    set(handles.help_display, 'String', ...
        'When checked, a call to the rescan function (either from the "Actions" menu, or coming from the timer) will cause all plots to rescan.');
    if get(hObject, 'Value') == 1
        set(hObject, 'Value', 0);
    else
        set(hObject, 'Value', 1);
    end
end
guidata(hObject, handles);

function select_figure_edit_Callback(hObject, eventdata, handles)
% hObject    handle to select_figure_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of select_figure_edit as text
%        str2double(get(hObject,'String')) returns contents of select_figure_edit as a double
if handles.help == 0
%this is sometimes called from other functions (such as submit time), but we don't want it to get
%the rem_ref_point it displays necessarily
if handles.in_submit_tm == 0
    handles.rem_ref_point = str2double(get(hObject,'String'));
end
%brings figure to front
figure(handles.all_figures{handles.rem_ref_point});
%make sure all the views are right
handles.markersize = handles.hold_views(handles.rem_ref_point, 1);
handles.draw_lines = handles.hold_views(handles.rem_ref_point, 2);
handles.show_err = handles.hold_views(handles.rem_ref_point, 3);
handles.freq_domain = handles.hold_views(handles.rem_ref_point, 4);
handles.unpack = handles.hold_views(handles.rem_ref_point, 5);
handles.unwrap = handles.hold_views(handles.rem_ref_point, 6);
handles.scale = handles.hold_views(handles.rem_ref_point, 7);
    
guidata(hObject, handles);

indv_rescanFunc(hObject, handles);

if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
guidata(hObject, handles);

else
    set(handles.help_display, 'String', ...
        'Enter the number of a figure you want to bring to the front. View changes, time range changes, and rescan calls affect that plot only, unless one of the checkboxes above states otherwise. By default, the current figure is the one most recently created.');
end


function [X_limits hold_views color] = indv_rescanFunc(hObject, handles)

%store views for later calls
handles.hold_views(handles.rem_ref_point, 1) = handles.markersize;
handles.hold_views(handles.rem_ref_point, 2) = handles.draw_lines;
handles.hold_views(handles.rem_ref_point, 3) = handles.show_err;
handles.hold_views(handles.rem_ref_point, 4) = handles.freq_domain;
handles.hold_views(handles.rem_ref_point, 5) = handles.unpack;
handles.hold_views(handles.rem_ref_point, 6) = handles.unwrap;
handles.hold_views(handles.rem_ref_point, 7) = handles.scale;
hold_views = handles.hold_views;

%when the user wants one plot to take from only one data file, and another
%plot to take from multiple, and their names are listed in the same array,
%zeroes will be created to fill in the holes where no data file was assigned
handles.onf = handles.point_files(handles.rem_ref_point, :);
where_zero = 1;
yes_zero = 0;
while (yes_zero == 0) && (where_zero <= length(handles.onf))
    if handles.onf(where_zero) == 0
        yes_zero = 1;
    else
        where_zero = where_zero + 1;
    end
end
if yes_zero == 1
    for cut_zeros = 1:length(handles.onf) - where_zero + 1;
        handles.onf(:, where_zero) = [];
    end
end

%when add data is called, handles.onf can change
remember_flist = {handles.flist{handles.onf}};
[handles.flist handles.full_flist] = scan_data_files (handles.dirlist);
set (handles.file_listbox, 'String', handles.flist);
stor_matches = [];
for loop_flist = 1:length(remember_flist) 
    tmp = strmatch(remember_flist{loop_flist}, handles.flist, 'exact');
    if ~(isempty(tmp))
        stor_matches = [stor_matches, tmp(1)];
    end
end
handles.onf = stor_matches;

[num_figs, num_pixs] = size(handles.list_row);

stor_limits = zeros(1, 2);
stor_limits2 = zeros(1, 2);

%when there are more pixels in one plot than another, the program fills in
%a NaN at the end of the pixels the user chose; this makes sure those
%aren't refrenced here
for no_nan = num_pixs:-1:1
    if isnan(handles.list_row(handles.rem_ref_point, no_nan))
        num_pixs = no_nan - 1;
    end
end

handles.LL = {};
handles.g_objs = [];

for which_pix = 1:num_pixs
    %set the row and column which were stored earlier
    handles.mux_row = handles.list_row(handles.rem_ref_point, which_pix);
    handles.mux_col = handles.list_col(handles.rem_ref_point, which_pix);
    %allows the plot_ts_or_psd to refrence which pixel loop is on
    handles.which_pix = which_pix;
    guidata(hObject, handles);
    handles = load_dataset(handles);
    guidata(hObject, handles);
    [handles.LL, handles.g_objs, handles.color] = plot_dataset(handles);
    hold all
    
    %makes sure plot shows all pixels
    limits = get(handles.topAxes{handles.rem_ref_point}, 'YLim');
    if which_pix == 1
        stor_limits(1, 1) = limits(1, 1);
        handles.just_created = 0;
    end
    if limits(1, 1) < stor_limits(1, 1)
        stor_limits(1,1) = limits(1, 1);
    end
    if limits(1, 2) > stor_limits(1, 2)
        stor_limits(1, 2) = limits(1, 2);
    end
    if handles.show_err == 1
        limits2 = get(handles.btmAxes{handles.rem_ref_point}, 'YLim');
        if which_pix == 1
            stor_limits2(1, 1) = limits2(1, 1);
        end
        if limits2(1, 1) < stor_limits2(1, 1)
            stor_limits2(1,1) = limits2(1, 1);
        end
        if limits2(1, 2) > stor_limits2(1, 2)
            stor_limits2(1, 2) = limits2(1, 2);
        end
    end
end
legend(handles.topAxes{handles.rem_ref_point}, handles.g_objs, handles.LL, 'Location', 'NorthEast');
hold off

set(handles.topAxes{handles.rem_ref_point}, 'YLim', [stor_limits(1, 1), stor_limits(1, 2)]);
if handles.show_err == 1
    set(handles.btmAxes{handles.rem_ref_point}, 'YLim', [stor_limits2(1, 1), stor_limits2(1, 2)]);
end
%get user defined x-limits
handles.min_edit = handles.X_limits(1, handles.rem_ref_point);
handles.max_edit = handles.X_limits(2, handles.rem_ref_point);
if handles.max_edit >= 0
    set_axes(hObject, handles);
end
%make sure zoom resets to what it has just been set to
zoom reset;
%for output; color must be output so that it can be saved in handles
X_limits = handles.X_limits;
color = handles.color;

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function select_figure_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to select_figure_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in all_views_check.
function all_views_check_Callback(hObject, eventdata, handles)
% hObject    handle to all_views_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of all_views_check
if handles.help == 0
if (get(hObject, 'Value') == get(hObject, 'Max'))
    handles.ys_all_views = 1;
    %have to know which plots already show error and which do not, so we
    %don't change the axes of the ones who don't need it
    handles.where_jc = [];
    [num_figs, num_pixs] = size(handles.list_row);
    for ys_cleared = 1:num_figs
        handles.where_jc(ys_cleared) = handles.hold_views(ys_cleared, 3);
    end
    handles.hold_views = chng_all_views(hObject, handles);
    %where_jc should only be used first time after checking this box; after
    %that, all the plots have had their axes set correctly
    handles.first_rescanFunc = 1;
    rescanFunc(hObject, handles);
    handles.first_rescanFunc = 0;
else
    handles.ys_all_views = 0;
end
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
else
    set(handles.help_display, 'String', ...
        'When checked, the view for all plots will be the same as the view for the most recently created plot, or the plot you selected using the control below. Also, changes to the view of any one plot will affect all plots.');
    if get(hObject, 'Value') == 1
        set(hObject, 'Value', 0);
    else
        set(hObject, 'Value', 1);
    end
end
guidata(hObject, handles);


function [hold_views] = chng_all_views(hObject, handles)
handles.hold_views(:, 1) = handles.markersize;
handles.hold_views(:, 2) = handles.draw_lines;
handles.hold_views(:, 3) = handles.show_err;
handles.hold_views(:, 4) = handles.freq_domain;
handles.hold_views(:, 5) = handles.unpack;
handles.hold_views(:, 6) = handles.unwrap;
handles.hold_views(:, 7) = handles.scale;
hold_views = handles.hold_views;
guidata(hObject, handles);


% --- Executes on button press in tm_rng_check.
function tm_rng_check_Callback(hObject, eventdata, handles)
% hObject    handle to tm_rng_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tm_rng_check
if handles.help == 0
if (get(hObject, 'Value') == get(hObject, 'Max'))
    handles.tm_rng_checked = 1;
    [num_figs, num_pixs] = size(handles.list_row);
    for cur_fig = 1:num_figs
        %cycle through all plots, change x-axis
        handles.rem_ref_point = cur_fig;
        figure(handles.all_figures{cur_fig});
        handles.X_limits(1, cur_fig) = handles.min_edit;
        handles.X_limits(2, cur_fig) = handles.max_edit;
        set_axes(hObject, handles);
    end
else
    handles.tm_rng_checked = 0;
end
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
zoom reset;
else
    set(handles.help_display, 'String', ...
        'When checked, the time range setting (definite limits, last x seconds,  or all data) for all plots will be the same as that for the most recently created plot, or the plot you selected using the control below. Also, changes to the time range of any one plot will affect all plots.');
    if get(hObject, 'Value') == 1
        set(hObject, 'Value', 0);
    else
        set(hObject, 'Value', 1);
    end
end
guidata(hObject, handles);

function frst_tm_edit_Callback(hObject, eventdata, handles)
% hObject    handle to frst_tm_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of frst_tm_edit as text
%        str2double(get(hObject,'String')) returns contents of frst_tm_edit as a double
if handles.help == 0
handles.crnt_min_edit = str2double(get(hObject, 'String'));
else
    set(handles.help_display, 'String', ...
        'Start time (or frequency) for x-axis (see the "Submit" button help description for more).');
end
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function frst_tm_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frst_tm_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
handles.crnt_min_edit = -1;
handles.min_edit = -1;
guidata(hObject, handles);


function scnd_tm_edit_Callback(hObject, eventdata, handles)
% hObject    handle to scnd_tm_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of scnd_tm_edit as text
%        str2double(get(hObject,'String')) returns contents of scnd_tm_edit as a double
if handles.help == 0
handles.crnt_max_edit = str2double(get(hObject, 'String'));
else
    set(handles.help_display, 'String', ...
        'Stop time (or frequency) for x-axis (see the "Submit" button help description for more).');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function scnd_tm_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scnd_tm_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
handles.crnt_max_edit = -1;
handles.max_edit = -1;
guidata(hObject, handles);


% --- Executes on button press in submit_times_button.
function submit_times_button_Callback(hObject, eventdata, handles)
% hObject    handle to submit_times_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
handles.max_edit = handles.crnt_max_edit;
handles.min_edit = handles.crnt_min_edit;
if handles.tm_rng_checked == 0
    %individual change; indv_rescanFunc has set axes inside it
    handles.X_limits(1, handles.rem_ref_point) = handles.min_edit;
    handles.X_limits(2, handles.rem_ref_point) = handles.max_edit;
    indv_rescanFunc(hObject, handles);
else
    %go through all and change
    handles.in_submit_tm = 1;
    [num_figs, num_pixs] = size(handles.list_row);
    for cur_fig = 1:num_figs
        handles.rem_ref_point = cur_fig;
        handles.X_limits(1, cur_fig) = handles.min_edit;
        handles.X_limits(2, cur_fig) = handles.max_edit;
        guidata(hObject, handles);
        select_figure_edit_Callback(hObject, eventdata, handles);
    end
    handles.in_submit_tm = 0;
end
%make sure when user zooms, default is new axes setting
zoom reset;
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end

else
    set(handles.help_display, 'String', ...
        'Change the x-axis for one or all plots, depending on the check box above. Here is how the range is determined: If just the first number is negative: the # in the second box will be the # of seconds (or Hz) back from the last data point the plot will display. If both numbers are negative: the plot will display all data. Otherwise, the two boxes are taken as the exact start and stop times (or Hz) of the X-axis.');
end
    guidata(hObject, handles);


function set_axes(hObject, handles)

if handles.max_edit >= 0
    if handles.min_edit >= 0
        %if both are positive, user is setting both limits
        min = handles.min_edit;
        max = handles.max_edit;
    else
        %if first is positive, get the limits automatically made, and
        %change
        limits = get(handles.topAxes{handles.rem_ref_point}, 'XLim');
        max = limits(1, 2);
        min = max - handles.max_edit;
    end
    set(handles.topAxes{handles.rem_ref_point}, 'XLim', [min, max]);
    if handles.show_err == 1
        set(handles.btmAxes{handles.rem_ref_point}, 'XLim', [min, max]);
    end
end
guidata(hObject, handles);


% --- Executes on button press in auto_pos_check.
function auto_pos_check_Callback(hObject, eventdata, handles)
% hObject    handle to auto_pos_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of auto_pos_check
if handles.help == 0
if (get(hObject, 'Value') == get(hObject, 'Max'))
    handles.auto_pos = 1;
else
    handles.auto_pos = 0;
end
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end
else
    set(handles.help_display, 'String', ...
        'The plots will be placed in two rows and three columns. Subsequent plots will overlap them.');
    if get(hObject, 'Value') == 1
        set(hObject, 'Value', 0);
    else
        set(hObject, 'Value', 1);
    end
end
guidata(hObject, handles);



function [list_row list_col rrf1] = multi_pixels_edit_Callback(hObject, eventdata, handles)
% hObject    handle to multi_pixels_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of multi_pixels_edit as text
%        str2double(get(hObject,'String')) returns contents of multi_pixels_edit as a double
if handles.help == 0
multi_string = get(handles.multi_pixels_edit, 'String');
RorC_on = 0;
pix_entered = [];
%find the numbers in the characters
% for current_char = 1:length(multi_string)
%     is_number = str2double(multi_string(current_char));
%     if isnan(is_number) == 0
%         RorC_on = RorC_on + 1;
%         pix_entered(RorC_on) = is_number;
%     end    
% end
tmp = regexprep (multi_string, '(,|\(|\))', ' ');
pix_entered = str2num (tmp);
%rem_ref_point, which is a refrence for each plot, has to start out at one,
%but shouldn't get to 2 until the next time plot_all is called
if handles.first_add == 1
    handles.rem_ref_point = handles.rem_ref_point + 1;
    handles.first_add = 0;
end
%split into columns and rows, and store
for into_rrp = 1:length(pix_entered)
    handles.rrp2 = ceil(into_rrp/2);
    if rem(into_rrp, 2) == 1
        handles.list_row(handles.rem_ref_point, handles.rrp2) = pix_entered(into_rrp);
    else
        handles.list_col(handles.rem_ref_point, handles.rrp2) = pix_entered(into_rrp);
    end
end

handles.list_row(handles.rem_ref_point, (handles.rrp2 + 1)) = NaN;
%for output
list_row = handles.list_row;
list_col = handles.list_col;
rrf1 = handles.rem_ref_point;
else
    set(handles.help_display, 'String', ...
        'The list of pixels to be plotted in the next graph. You can edit this by hand, or use the add button. Row comes first, then column. Parentheses and commas are not necessary.');
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function multi_pixels_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to multi_pixels_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plot_all_button.
function plot_all_button_Callback(hObject, eventdata, handles)
% hObject    handle to plot_all_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
if handles.rem_ref_point > 1
    %store limits and views for last plot
    handles.X_limits(1, handles.rem_ref_point) = handles.min_edit;
    handles.X_limits(2, handles.rem_ref_point) = handles.max_edit;
    
    handles.hold_views(handles.rem_ref_point, 1) = handles.markersize;
    handles.hold_views(handles.rem_ref_point, 2) = handles.draw_lines;
    handles.hold_views(handles.rem_ref_point, 3) = handles.show_err;
    handles.hold_views(handles.rem_ref_point, 4) = handles.freq_domain;
    handles.hold_views(handles.rem_ref_point, 5) = handles.unpack;
    handles.hold_views(handles.rem_ref_point, 6) = handles.unwrap;
    handles.hold_views(handles.rem_ref_point, 7) = handles.scale;
end

guidata(hObject, handles);
%get current pixels
[handles.list_row handles.list_col handles.rem_ref_point] = multi_pixels_edit_Callback(hObject, eventdata, handles);

[num_figs num_pixs] = size(handles.list_row);
%store the files chosen
for onf_ref = 1:length(handles.onf)
    handles.point_files(num_figs, onf_ref) = handles.onf(onf_ref);
end

%create a new figure, make it the current figure, create axes on it
handles.all_figures{handles.rem_ref_point} = figure;
figure(handles.all_figures{handles.rem_ref_point});
handles.topAxes{handles.rem_ref_point} = axes;
handles.btmAxes{handles.rem_ref_point} = axes;


if handles.auto_pos == 1
    set(handles.all_figures{handles.rem_ref_point}, 'Units', 'normalized');
    %plots layed in two rows, three on top, three on bottom; process than
    %repeats, overlapping the first plots
    fig_ypos = rem(ceil(handles.rem_ref_point/3), 2)*0.5;
    fig_xpos = (rem((handles.rem_ref_point - 1), 3))*0.3; 

    pos = [fig_xpos, fig_ypos, 0.3, 0.5];
    set(handles.all_figures{handles.rem_ref_point}, 'Position', pos);
end

%for error sig
handles.just_created = 1;

%store limits
handles.X_limits(1, handles.rem_ref_point) = handles.min_edit;
handles.X_limits(2, handles.rem_ref_point) = handles.max_edit;

guidata(hObject, handles);
[handles.X_limits handles.hold_views handles.color] = indv_rescanFunc(hObject, handles);

handles.just_created = 0;

if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end

handles.first_add = 1;

else
    set(handles.help_display, 'String', ...
        'Plots the pixels listed above using the data from the files chosen on the right in a figure that pops up. The print, save, zoom, pan, etc. controls will be in that new window.');
end
guidata(hObject, handles);


% --- Executes on button press in close_all_button.
function close_all_button_Callback(hObject, eventdata, handles)
% hObject    handle to close_all_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.help == 0
%close all plot windows
for pop_ups = 1:numel(handles.all_figures)
    close (handles.all_figures{pop_ups});
end
%clear all the data about them
handles.all_figures = [];
handles.list_row = [];
handles.list_col = [];
handles.point_files = [];
handles.hold_views = [];
clear_channels_button_Callback(hObject, eventdata, handles);
handles.rem_ref_point = 0;
handles.num_all_plot = 0;
handles.g_objs = [];
handles.color = {};
if handles.tim_running == 1
    set(handles.tim, 'UserData', handles);
end

else
    set(handles.help_display, 'String', ...
        'Deletes all figure windows that are open. USE THIS INSTEAD OF DOING IT BY HAND, otherwise the plot will reappear next time you rescan all plots.');
end
guidata(hObject, handles);


% --- Executes on button press in summary_button.
function summary_button_Callback(hObject, eventdata, handles)
% hObject    handle to summary_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.help_display, 'String', ...
    ['This MATLAB GUI was created as a "quick-look" for data in MCE file format taken from BICEP II pixels. ', ... 
    'Multiple pixels, taking data from multiple files, can be displayed on the same plot, and multiple ', ...
    'plots can be created. One can then adjust the view and/or the time range of the data on each plot, ', ...
    'either on an individual basis or to all of the plots at once. One can also rescan the data (keeping ', ...
    'the view and time range settings the same), again on either an individual or all plots basis. The ', ...
    'plots can also rescan at regular, user-defined, intervals. The user can zoom in on an area in each ', ...
    'individual plot or print, save, etc. using the built in controls MATLAB has for the figure object. There ', ...
    'is a help button to describe each aspect of the plot individually. ', ...
    'Created by Walter Ogburn and Eli Weinstein. Most recent change: 9/1/08']);
guidata(hObject, handles);


% --- Executes on button press in help_check.
function help_check_Callback(hObject, eventdata, handles)
% hObject    handle to help_check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of help_check
if (get(hObject, 'Value') == get(hObject, 'Max'))
    handles.help = 1;
else
    handles.help = 0;
end
guidata(hObject, handles);
