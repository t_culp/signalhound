function [ivmat bias]=quick_iv_plots(loadcurvefile,col,row);
% [ivmat bias]=quick_iv_plots(loadcurvefile,col,row);
%
% JAB 20091219

figure('Position',[0 0 1750 1000]);
bias=read_mce_biasfile([loadcurvefile '.bias']);
[iv h]=read_mce(loadcurvefile);
ivlen=length(iv);
if ivlen == 264
    ivmat=reshape(iv,33,8);
else
    ivmat=reshape(iv,33,16);
end

rowlist=[0 2:32];

if isempty(row);
for ii=1:32; 
row=rowlist(ii);
subplot(8,4,ii)
indx=find(bias.tes_bias<=5500);
plot(bias.tes_bias(indx),ivmat(row+1,col+1).fb(indx));
title(['COL' num2str(col) ' ROW' num2str(row)]);
xlabel('tes bias');
ylabel('fb1');
end
else
if ~isempty(col);    
indx=find(bias.tes_bias<=5500);
plot(bias.tes_bias(indx),ivmat(row+1,col+1).fb(indx));
title(['COL' num2str(col) ' ROW' num2str(row)]);
xlabel('tes bias');
ylabel('fb1');  
else
for icol=0:15;
subplot(4,4,icol+1);
indx=find(bias.tes_bias<=5500);
plot(bias.tes_bias(indx),ivmat(row+1,icol+1).fb(indx));
title(['COL' num2str(icol) ' ROW' num2str(row)]);
xlabel('tes bias');
ylabel('fb1');
end
end


ivmat=ivmat;
bias=bias.tes_bias;
end

