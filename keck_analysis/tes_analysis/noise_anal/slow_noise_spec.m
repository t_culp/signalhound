function [freq spec] =  slow_noise_spec(noisedir, row, col, bias)
%
% This calculates the noise spectrum for a detector
%
% Example [f s]=slow_noise_spec('~/data/noise_01092011/noise_slow_everythingon/',0,0,4000);

%
% SAS 20101231
%
%ZKS 20110109

%savedir=[noisedir{1} '/figdir/'];

%if col < 8
%   rc_col = col;
%else
%   rc_col = col-8;
%end

rc_col=col;


nfft =100;
refer_to_input=1;
colors={'r','g','b','cm'};

%extn = ['/bias' int2str(bias) '/all_' rc];  % 400 Hz filename
extn = ['/bias' int2str(bias) '/all_rcs'];  % 400 Hz filename
[noisedir extn];
%Loop through the timestreams

[A r]=read_mce([noisedir extn]);

row_len = r.header.cc.row_len;
cc_nr = r.header.cc.num_rows_reported;
data_rate = r.header.cc.data_rate;
fsamp=50e6/row_len/cc_nr/data_rate;
ind=rc2ind(row,col);
data=A(ind).fb;
noverlap=0;
window=boxcar(nfft);
[spec,f]=psdmat(data,nfft,fsamp,[],noverlap);
spec=(2*spec(2:end)/fsamp).^.5;
if refer_to_input;
  spec=((FB1adc_to_inputcurrent(spec,col)));
end
   
freq = f(2:end);

