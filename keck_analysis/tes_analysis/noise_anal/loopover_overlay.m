function loopover_overlay(noisedir)
%loopover_overlay(noisedir,gainmatrix)
%
% JAB 20091221

biasdirs=dir([noisedir '/bias*']);
files=dir([noisedir '/' biasdirs(1).name '/superfast*']);
npix=length(files);

for ii=1:npix;
    rowindx=strfind(files(ii).name,'row');
    colindx=strfind(files(ii).name,'col');
    row=str2num(files(ii).name(rowindx+3:colindx-2))
    col=str2num(files(ii).name(colindx+3:end-0))
      superfast_spectra_overlay(noisedir,row,col,'calib');
    close gcf

end
