function gain = fb_err_gain_fastacq(dir,row,col)
% gain = fb_err_gain_fastacq(calib_file)
%
%
% edited SAS 12/31/2010 to read in non-runfile data
% edited to read in the ramp step size and ramp step period
%

%[s r]=read_mce(calib_dir,0,0);

calib_file = [dir '/calib_row' int2str(row) '_col' int2str(col)];
scrfname = [dir '/noise_superfast.scr.row' int2str(row)];


if ~exist(calib_file)
     disp('Error.  No Calib file');
end

numframes=10000;

[data aux h]=read_mce_datfile(calib_file,0,numframes);

f = fopen(scrfname,'rt');
while ~feof(f)
  ll = fgetl(f);
  tok = regexp(ll, 'ramp_step_size (\d+)', 'tokens');
  tok1 = regexp(ll, 'ramp_step_period (\d+)', 'tokens');
  if ~isempty(tok)
    tok = tok{1};
    ramp_step_size=str2num(tok{1});
  end
  if ~isempty(tok1)
    tok1 = tok1{1};
    ramp_step_period=str2num(tok1{1});
  end
end
fclose(f);

cc_ncols=bin2dec(num2str(bitget(h(1,1), 20:-1:17)));
rc_present=bitget(h(1,1), 1 + [10 11 12 13]);
num_rc=sum(rc_present);
cc_nc=cc_ncols*num_rc;
cc_nr=h(1,4);
rc_nc=cc_nc;    
rc_nr=1;     %always single channel
data_rate=h(1,5);
row_len=h(1,3);
num_rows=h(1,10);

sqrwave_frameperiod = ramp_step_period*2;
step = ramp_step_size;

per=sqrwave_frameperiod;
gain=zeros(rc_nc,1);

for j=1:rc_nc

err=rvec(aux(j,1,:));

err_len=length(err);  
cycles=(err_len/per);

%remove overall shifts
if cycles>1
  x=1:length(err);
  p=polyfit(x,err,1);
  err=err-polyval(p,x);
end

%remove the first couple of cycles if you can
if cycles>20
  err=err(per*3:end);
end
cycles=cycles-3;

err_ave=zeros(1,per);

for i=1:cycles;
    err_ave=err_ave + err((1+(i-1)*per):(i*per));
end
indx1=[ ((per/2)+7):per];
indx2=[7:((per/2) +3)];

err_ave=err_ave/cycles;
ave1=mean(err_ave(indx1));
ave2=mean(err_ave(indx2));

if(0)
figure
plot(err_ave,'b.-');hold on;
plot(indx1,err_ave(indx1),'r.');plot(indx2,err_ave(indx2),'g.');
title(['fb_err_gain for col' int2str(j-1)]);
end
gain(j)=abs(ave1-ave2)/step;

end


