function [freq,spec] = fast_psd_comp(noisedir, row, col, bias)
%
% This is to compare the 15 kHz data between different modes
% Example: fast_psd_comp({'~/data/noise_12312010/noise_newps',
%  '~/data/noise_12292010/noise_pton'}, 0,0,0)
%
% SAS 20101231
%

savedir=[noisedir{1} '/figdir/'];

if ~exist(savedir,'dir')
      mkdir(savedir);
end

if col < 8
   rc = 'rc1';
   rc_col = col;
   
else
   rc = 'rc2';
   rc_col = col-8;
end

nfft = 10000;
refer_to_input=1;
colors={'r','g','b','cm'};

extn = ['/bias' int2str(bias) '/fast_' rc '_row' int2str(row)];  % 400 Hz filename

nnoise=length(noisedir);

%Loop through the timestreams

for i=1:nnoise

    	[A r]=read_mce([noisedir{i} extn]);
        row_len = r.header.cc.row_len;
        cc_nr = r.header.cc.num_rows;
        data_rate = r.header.cc.data_rate;

        fsamp=50e6/row_len/cc_nr/data_rate;

        ind=find([A(:).mux_row]==row & [A(:).mux_col]==rc_col);
        data=A(ind).fb;
        noverlap=0;
        window=boxcar(nfft);
        [spec,f]=psdmat(data,nfft,fsamp,[],noverlap);
        spec=(2*spec(2:end)/fsamp).^.5;
        if refer_to_input;
             spec=((FB1adc_to_inputcurrent(spec,col)));
        end
    	f=f(2:end);
        figure(200);
        loglog(f,spec*1e12,colors{i});
        hold on;
        
        figure(200+i);
        plot(data);
        title(['Timestream for Row' int2str(row) 'Col' int2str(col) ' at bias ' int2str(bias)]) 

end

freq = f;

figure(200)
title(['15 kHz Data for Row' int2str(row) ' Col' int2str(col) ' at bias' int2str(bias)]);
%ylim([1 1e4]);
xlabel('Frequency (Hz)');
ylabel('PSD RTI (pA/rtHz)');
legdir=strrep(noisedir,'_','\_');
legend(cellstr(legdir),'Location','SouthWest');

savefile = [savedir 'fast_psd_comp_r' int2str(row) 'c' int2str(col) '_bias' int2str(bias) '.png']; 

print('-dpng','-r75',savefile);
