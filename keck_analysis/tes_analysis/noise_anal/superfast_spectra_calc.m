function [superfast_freq superfast_spec fsamp] = superfast_spectra_calc(noisedir,row,col,gain)
%
% [superfast_freq superfast_spec fsamp] = superfast_spectra_calc(noisedir,row,col,gain)
% output:   superfast_spec returns the spectra in A/rtHz if refer_to_input=1, else fbu/rtHz
%           fsamp = sampling frequency calculated from header info
%
% JAB 20090719
% SAS 20110112 edited to get calibration correctly
% status: development

%usage
%addpath(genpath('~/analysis'));
%>> pwd
%ans =
%/home/spud/data/noise_01112011/noise_400KHz_noresetclean_noflxlp/bias4000
%[sf ss]=superfast_spectra_calc('.',0,0,'calib');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% user defined inputs:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
refer_to_input=1;
nfft=100000;
%nfft=1000;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(gain,'calib');
    gains=fb_err_gain_fastacq(noisedir,row,col);
    rc_col=mod(col,8);
    gain = gains(rc_col+1);
end

fastfile=dir([noisedir '/superfast*' 'row' num2str(row) '*col' num2str(col)]);
if exist([noisedir '/' fastfile(1).name '.run']);
    [fast rfast]=read_mce([noisedir '/' fastfile(1).name]);
    fast=fast.err;
    cc_nc=rfast.header.cc.num_cols_reported;
    cc_nr=rfast.header.cc.num_rows_reported;
    rc_nc=rfast.header.rc(2).num_cols_reported;
    rc_nr=rfast.header.rc(2).num_rows_reported;
    data_rate=rfast.header.cc.data_rate;
    row_len=rfast.header.cc.row_len;
    num_rows=rfast.header.cc.num_rows;
else

   numframes=600000; 
   [data aux h]=read_mce_datfile([noisedir '/' fastfile(1).name],0,numframes);
   fast=rvec(aux);
   cc_ncols=bin2dec(num2str(bitget(h(1,1), 20:-1:17)));
   rc_present=bitget(h(1,1), 1 + [10 11 12 13]);
   num_rc=sum(rc_present);
   cc_nc=cc_ncols*num_rc;
   cc_nr=h(1,4);
   rc_nc=1;     %always single channel
   rc_nr=1;     %always single channel
   data_rate=h(1,5);
   row_len=h(1,3);
   num_rows=h(1,10);
end

fsamp=(50e6/(row_len*num_rows*data_rate))*((cc_nr*cc_nc)/(rc_nr*rc_nc));

noverlap=0;
window=boxcar(nfft);
[spec,f]=psdmat(fast',nfft,fsamp,[],noverlap);
fastspec=(2*spec(2:end)/fsamp).^.5;
if refer_to_input;
    calib=calib_keck_pole2011();
    FB12input = (calib.V_FB1_MAX(col+1)./((2^calib.BITS_FB1).*(calib.R_FB1(col+1)+calib.R_WIRE(col+1))*calib.M_FB1(col+1)))./gain;
    superfast_spec=fastspec.*FB12input;
else
    superfast_spec=fastspec;
end
superfast_freq=f(2:end);

figure
plot(fast)
title(['Timestream for Row' int2str(row) 'Col' int2str(col) ' at ' noisedir]);
xlabel('Samples');
ylabel('Err');
