function gain=fb_err_gain(calib_file)
% gain=fb_err_gain(calib_file)
% 
%
% JAB 20091018

quiet=0;

%[s r]=read_rectmode(calib_file);
[s r]=read_mce(calib_file);
colindx=strfind(calib_file,'col');
col=str2num(calib_file(colindx+3:end));

%sqrwave_frameperiod=r.header.cc.ramp_step_period*2;
ccnr=r.header.cc.num_rows_reported;
step=r.header.cc.ramp_step_size;
rc=s(1).rc;
err=s(col+1 - 8*(rc-1)).err;

%per=sqrwave_frameperiod;
per=ccnr*2;
err_len=length(err);  
cycles=(err_len/per);

err_ave=zeros(per,1);

for i=1:cycles;
    err_ave=err_ave + err((1+(i-1)*per):(i*per));
end

blocks=find(abs(diff(err_ave))>(2*std(diff(err_ave))-mean(diff(err_ave))));
indx1=(blocks(1)+2):(blocks(2)-2);
indx2=(blocks(2)+2:per);

err_ave=err_ave/cycles;
ave1=mean(err_ave(indx1));
ave2=mean(err_ave(indx2));

if ~quiet
    figure;plot(err);
    figure;plot(diff(err_ave));
    figure
    plot(err_ave,'b.-');hold on;
    plot(indx1,err_ave(indx1),'r.');plot(indx2,err_ave(indx2),'g.');
end

gain=abs(ave1-ave2)/step;


