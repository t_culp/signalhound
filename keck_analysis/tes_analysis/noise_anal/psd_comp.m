function[f1 f2 s1 s2 fsamp1 fsamp2]=psd_comp(datadir,noisedir, row, col,bias)

%JET 20101229
%SAS 20110111

%Example
%>> [f1 f2 s1 s2 fsamp1 fasmp2]=psd_comp('~/data/noise_12292010/',{'noise_mi_pton_3', 'noise_mi_ptoff_3'}, 0, 0, '0');
%
% Edited to take more than 2 noisedirs and to calculate the calibration
% from fb1 steps

gain = 'calib';

savedir=[ datadir '/figdir/'];

if ~exist(savedir,'dir')
      mkdir(savedir);
end

nnoise=length(noisedir);

%plot specifications
font='courier';
fontsize=10;
titlesize=14;
textsize=10;
legsize=8;
notesize=8;

%set the frequency and spec
freq = [];
spec = [];
  
savepre = [savedir];
%Calculate psds, plot timestreams
%%%%%%%%%%

for i=1:nnoise
    savedir_timestream = [datadir '/' noisedir{i} '/figdir/'];
    if ~exist(savedir_timestream,'dir')
         mkdir(savedir_timestream);
    end

    [f s fsamp] = superfast_spectra_calc([datadir, '/',noisedir{i},'/bias',int2str(bias)],row,col,gain);

    xlabel('sample');ylabel('ADU');
    title(['RAW TIMESTREAM COL' num2str(col) ' ROW' num2str(row)],'FontSize',titlesize);

    savepre_timestream=[savedir_timestream 'timestream_c' num2str(col) 'r' num2str(row) '_bias' int2str(bias)];

    savefile=[savepre_timestream '.png'];
    saveas(gcf,savefile);
    freq = [freq f];
    spec = [spec s];
    savepre = [savepre noisedir{i}];
    if i < nnoise
        savepre = [savepre '-vs-'];
    end
end


%plot comparison of two psds
%%%%%%%%%%%%%%%%%%

font='courier';
fontsize=10;
titlesize=14;
textsize=10;
legsize=8;
notesize=8;
figure('Position',[1 1 1000 700],'DefaultAxesFontName',font,'DefaultAxesFontSize',...
fontsize,'DefaultTextFontSize',textsize,'DefaultTextFontName',font) ;

loglog(freq,spec.*10^12);
set(0,'DefaultTextInterpreter','none')
legdir=strrep(noisedir,'_','\_');
legend(cellstr(legdir),'Location','SouthWest');
xlabel('frequency (Hz)');ylabel('PSD RTI (pA/rtHz)');
title(['SUPERFAST (400 kHz) NOISE SPECTRA Ti TRANSITION COL' num2str(col) ' ROW' num2str(row) ' at bias ' int2str(bias)],'FontSize',titlesize);

savepre=[savepre '_c' num2str(col) 'r' num2str(row)];
savefile=[savepre '.png'];
disp(['Saving Plot as ' savefile]);
saveas(gcf,savefile);
