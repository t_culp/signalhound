function tbias_current = TBIASadc_to_current(tbias_adc,col)
%
% det_current = TBIASadc_to_current(tbias_datain,col)
%
% JAB

%CSR-001 6.2kOhm Run8, Pole

 R_WIRE = 0 *ones(1,16);                     % line impedance included in Rs below.
 R_TBIAS =   [    523.56 523.45 523.72 522.54 ...
                523.17 522.44 523.12 523.24 ...
                523.63 523.70 523.50 523.21 ...
                523.05 523.43 523.92 522.83 ];

V_TBIAS_MAX = [   2.4990 2.4990 2.4990 2.4990 ...
                2.5000 2.4990 2.4990 2.4990 ...
                2.5000 2.5000 2.4990 2.5000 ...
                2.4990 2.5000 2.5000 2.5000 ];
     
          
          
 BITS_TBIAS = 16; % number of bits in FB1 DAC

% compute calibration factors from these circuit parameters

tbias_current=tbias_adc.*(V_TBIAS_MAX(col+1)./((2^BITS_TBIAS)*(R_TBIAS(col+1))));
