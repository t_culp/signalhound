function [freqout specarr]=superfast_spectra_overlay(noisedir,row,col,gain)
%[freqout specarr]=superfast_spectra_overlay(noisedir,row,col,gain)

save=1;
savedir=[ noisedir '/figdir/']
biasdirs=dir([noisedir '/bias*']);
ndirs=length(biasdirs);

if ~exist(savedir,'dir')
    mkdir(savedir);
end

specarr=[];
legstr=[];

for idir=1:ndirs
    disp(biasdirs(idir));
    [freqout specout]=superfast_spectra_calc([noisedir '/'  biasdirs(idir).name '/'],row,col,gain);
    specarr=[specarr specout];
    legstr=[legstr; cellstr(biasdirs(idir).name)];
end

colorset=jet(ndirs);
font='courier';
fontsize=10;
titlesize=14;
textsize=10;
legsize=8;
notesize=8;
figure('Position',[1 1 1000 700],'DefaultAxesFontName',font,'DefaultAxesFontSize',...
    fontsize,'DefaultTextFontSize',textsize,'DefaultTextFontName',font) 
set(gcf,'DefaultAxesColorOrder',colorset);
loglog(freqout,[specarr.*(10^12)]); 
legend(cellstr(legstr),'Position',[0.9 0.50 0.05 0.333],'FontSize',legsize);
%legend(cellstr(legstr),'Location','SW');
xlabel('frequency (Hz)');ylabel('PSD RTI (pA/rtHz)');
title(['RAW NOISE SPECTRA Ti TRANSITION COL' num2str(col) ' ROW' num2str(row)],'FontSize',titlesize);
set(gca,'XLim',[freqout(1) freqout(end)]);
set(gca,'YLim',[1e0,1e5]);
text(-0.15,-0.10,noisedir,'Interpreter','none','FontSize',notesize,'Units','normalized')
text(-0.15,-0.12,'superfast_spectra_overlay','Interpreter','none','FontSize',notesize,'Units','normalized')


if save
    savepre=[savedir 'superfastspec_overlay_c' num2str(col) 'r' num2str(row)];
%     savefile=[savepre '.fig'];
%     saveas(gcf,savefile);
    savefile=[savepre '.png'];
    saveas(gcf,savefile);
end
