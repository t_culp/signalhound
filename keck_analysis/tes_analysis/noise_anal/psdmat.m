function [pxx,f]=psdmat(x,nfft,fsamp,window,noverlap)
% [pxx,f]=psdmat(x,nfft,fsamp,window,noverlap)
%

% compute PSD
if isempty(nfft); nfft=length(x);end
if isempty(window); window=1;normwin=norm(boxcar(nfft));end

[n,chan] = size(x);		    % Number of data points
nwind = nfft; % length of window
if n < nwind            % zero-pad x if it has length less than the window length
    x(nwind)=0;  n=nwind;
end	

k = fix((n-noverlap)/(nwind-noverlap));	% Number of windows
                    					% (k = fix(n/nwind) for noverlap=0)
                                      
index = 1:nwind;
KMU = k*normwin^2;	% Normalizing scale factor ==> asymptotically unbiased

Spec = zeros(nfft,chan); % Spec2 = zeros(nfft,1);
for i=1:k
    xw = window*(x(index,:));
    index = index + (nwind - noverlap);
    Xx = abs(fft(xw,nfft)).^2;
    Spec = Spec + Xx;
end

% Select first half
if ~any(any(imag(x)~=0)),   % if x is not complex
    if rem(nfft,2),    % nfft odd
        select = (1:(nfft+1)/2)';
    else
        select = (1:nfft/2+1)';
    end
    Spec = Spec(select,:);

else
    select = (1:nfft)';
end
f = (select - 1)*fsamp/nfft;

pxx = Spec.*(1/KMU);   % normalize
