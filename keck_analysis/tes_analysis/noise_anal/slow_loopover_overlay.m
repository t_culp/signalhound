function slow_loopover_overlay(noisedir)

%This loops over all of the pixels and plots the 400 Hz data for different
%biases.
%
% SAS 1/4/2010

noisedir=[noisedir '/'];
biasdirs=dir([noisedir 'bias*']);
nbiases=length(biasdirs);
savedir=[noisedir 'figdir/'];

refer_to_input=1;
npix=528;
nfft = 500;
legstr=[];

%read in the data
for i=1:nbiases
    disp(['Reading in ' noisedir biasdirs(i).name]);
    [A r] = read_mce([noisedir biasdirs(i).name '/all_rcs']);
    row_len = r.header.cc.row_len
    cc_nr = r.header.cc.num_rows_reported;
    data_rate = r.header.cc.data_rate;
    fsamp(i) = 50e6/row_len/cc_nr/data_rate;
    
    %B = read_mce([noisedir biasdirs(i).name '/all_rc2']);
    %assuming here that B is in the same format as A
    
    data(i).fb = [A(:).fb];   %[A(:).fb B(:).fb];
    legstr=[legstr; cellstr(biasdirs(i).name)];
end

%loop over pixels
for j=1:npix
    specarr=[];
    [row col] = gcp2mce(j-1);
    
    for i=1:nbiases
        noverlap=0;
        window=boxcar(nfft);
        [spec,f]=psdmat(data(i).fb(:,j),nfft,fsamp,[],noverlap);
        spec=(2*spec(2:end)/fsamp(i)).^.5;
        freqout=f(2:end,1);
        
        if refer_to_input;
            spec=((FB1adc_to_inputcurrent(spec,col)));
        else
            spec=spec;
        end
        
        specarr = [specarr spec];
        
    end
    
    figure(100)
    colorset=jet(nbiases);
    font='courier';
    fontsize=10;
    titlesize=14;
    textsize=10;
    legsize=8;
    notesize=8;
    set(gcf,'DefaultAxesColorOrder',colorset);
    loglog(freqout,[specarr.*(10^12)],'k'); 
    legend(cellstr(legstr),'Position',[0.9 0.50 0.05 0.333],'FontSize',legsize);
    xlabel('frequency (Hz)');ylabel('PSD RTI (pA/rtHz)');
    title(['400 Hz Noise SPECTRA Ti TRANSITION COL' num2str(col) ' ROW' num2str(row)],'FontSize',titlesize);
    %set(gca,'XLim',[freqout(1) freqout(end)]);
    %set(gca,'YLim',[1e0,1e5]);
    text(-0.15,-0.10,noisedir,'Interpreter','none','FontSize',notesize,'Units','normalized')
    text(-0.15,-0.12,'superfast_spectra_overlay','Interpreter','none','FontSize',notesize,'Units','normalized')
    
    
    savefile = [savedir 'slow_noise_overlay_r' int2str(row) 'c' int2str(col) '.png']; 

    print('-dpng','-r75',savefile);
end
    
    
