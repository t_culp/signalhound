function [f,tot_in]=noise_model_v2(Xo,freq_input,spec_input)
% X=[Io,Ro,To,Tc,Gc,Cc,beta,alpha_i,L,Rsh,F,beta_i,excess,amp_rti(A/rtHz),
%       Qload,band_center,frac_bandwidth]
% spec_input = measured spectra to overplot for model fit (pA/rtHz)
%
%
%  JAB 2009

aliasing=0;
fit_taus=0;
disp_nep=0;
disp_in=1;

fdata=logspace(0,5.3,1000);
Io=Xo(1);
Ro=Xo(2);
To=Xo(3);
Tc=Xo(4);
Gc=Xo(5);
Cc=Xo(6);
beta=Xo(7);
alpha_i=Xo(8);
L=Xo(9);
Rsh=Xo(10);
F=Xo(11);
beta_i=Xo(12);
excess_johnson=Xo(13);
amp_rti=Xo(14);
if length(Xo)>14
    qload=Xo(15);
    nu=Xo(16);
    bw=Xo(17);
end


Zi=1;
kb=1.38e-23;
Tsh=.500;
fnyq=15460.7/2;

txtstr=['Io=' num2str(Xo(1),2) ' Ro=' num2str(Xo(2),2) ' To=' num2str(Xo(3),2) ' Tc=' num2str(Xo(4),2) ...
        ' Gc=' num2str(Xo(5),2) ' Cc=' num2str(Xo(6),2) ' beta=' num2str(Xo(7),2) ' alpha=' num2str(Xo(8),2) ...
        ' L=' num2str(Xo(9),2) ' Rsh=' num2str(Xo(10),2) ' F(To,Tbath)=' num2str(Xo(11),2) ' beta_i=' num2str(Xo(12),2)];

tau_nat=Cc/Gc;
ell_i=(Io^2*Ro)*alpha_i/(Gc*Tc); %old denom: Gc*(To/Tc)^beta*Tc);
tau_i=tau_nat/(1-ell_i);
tau_el=L/(Rsh+Ro*(1+beta_i));
tau_plus=((1/(2*tau_el))+(1/(2*tau_i))+.5*sqrt(((1/tau_el)-(1/tau_i))^2 - 4*((Ro*ell_i*(2+beta_i))/(L*tau_nat))))^(-1);
tau_minus=((1/(2*tau_el))+(1/(2*tau_i))-.5*sqrt(((1/tau_el)-(1/tau_i))^2 - 4*((Ro*ell_i*(2+beta_i))/(L*tau_nat))))^(-1);
tau_eff=tau_nat*((1+beta_i+(Rsh/Ro))/(1+beta_i+(Rsh/Ro)+(1-(Rsh/Ro))*ell_i));
Lcrit_plus=((Ro*tau_nat)/(ell_i-1)^2)*(ell_i*(3+beta_i-(Rsh/Ro))+(1+beta_i+(Rsh/Ro))+2*sqrt(ell_i*(2+beta_i)*(ell_i*(1-(Rsh/Ro))+(1+beta_i+(Rsh/Ro)))));
Lcrit_minus=((Ro*tau_nat)/(ell_i-1)^2)*(ell_i*(3+beta_i-(Rsh/Ro))+(1+beta_i+(Rsh/Ro))-2*sqrt(ell_i*(2+beta_i)*(ell_i*(1-(Rsh/Ro))+(1+beta_i+(Rsh/Ro)))));

resp=(-1/(Io*Ro))*((L/(tau_el*Ro*ell_i)) + (1-(Rsh/Ro)) + (2*pi*fdata).*((i*L*tau_nat)/(Ro*ell_i))*((1/tau_i)+(1/tau_el)) - ((2*pi*fdata).^2).*((tau_nat*L)/(ell_i*Ro))).^(-1);

disp('========================================');
disp('========================================');
disp(txtstr);
disp('========================================');
disp(['tau_nat=' num2str(tau_nat*10^6,3) 'us']);
disp(['loop gain=' num2str(ell_i,3)]);
disp(['tau_i=' num2str(tau_i*10^6,3) 'us']);
disp(['tau_el=' num2str(tau_el*10^6,3) 'us']);
disp(['tau_eff=' num2str(tau_eff*10^6,3) 'us']);
disp(['tau_+=' num2str(tau_plus*10^6,3) 'us']);
disp(['tau_-=' num2str(tau_minus*10^6,3) 'us']);
disp(['Lcrit-=' num2str(Lcrit_minus*10^6,3) 'uH']);
disp(['L=' num2str(L*10^6,3) 'uH']);
disp(['Lcrit+=' num2str(Lcrit_plus*10^6,3) 'uH']);
disp(['dc Resp=' num2str(sqrt(abs(resp(1)).^2),3)]);
disp(['dc approx Resp=' num2str(1/(Io*(Ro-Rsh)),3)]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TES STABILIITY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (isreal(tau_plus) && isreal(tau_minus))
    disp('TES IS DAMPED'), 
    if (Ro > ((ell_i-1)/(ell_i+1+beta_i))*Rsh), disp('TES IS STABLE'), else disp('TES IS UNSTABLE');end
else
    disp('TES IS UNDERDAMPED')
    if real(tau_plus) >= 0, disp('TES IS STABLE'), else disp('TES IS UNSTABLE');end
    disp(['OSCILLATION @ ' num2str(1/(2*pi*sqrt(abs(tau_minus)^2)),3) 'Hz']);
end

disp('========================================');

sptes=4*kb*To*Io^2*Ro*Zi*(1+(2*pi*fdata*tau_nat).^2)./(ell_i).^2;
spexcess=sptes.*excess_johnson;
sptfn=4*kb*To^2*Gc*(To/Tc)^beta*F;
sptfn=ones(1,length(fdata)).*sptfn;
spsh=(4*kb*Tsh*Io^2*Rsh*((ell_i-1)/ell_i)^2).*((1+(2*pi*fdata*tau_i).^2));
if length(Xo) > 14;
    spphoton=2*6.626e-34*nu*qload + (2/nu*bw)*qload^2; 
else
    spphoton=0;
end
spphoton=ones(1,length(fdata)).*spphoton;

sites=abs(resp).^2 .* sptes;
siexcess=abs(resp).^2 .* spexcess;
sitfn=abs(resp).^2 .* sptfn;
sish=abs(resp).^2 .* spsh;
siphoton=abs(resp).^2 .* spphoton;

siamp=(ones(1,length(fdata)).*amp_rti).^2;
spamp=siamp./(abs(resp).^2);

sptot=(sptfn + sptes + spamp + spsh + spexcess + spphoton);
sitot=(sitfn + sites + siamp + sish + siexcess + siphoton);
f=fdata;
tot_in=sqrt(sitot);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% two pole noise fit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    X0_onepole=[1e-1,1e3,1e-2];
    X0_twopole=[5e-11,25e-6,1e-6,2e-12,50e-12];
    %weights=sqrt((sitot));  
    weights=ones(1,length(sitot));
   %[X,chisqout,dof]=chisqfit(@lowpass,X0_onepole,f(f_index),pxx(f_index),weights);
    [X,chisqout,dof]=chisqfit(@twopolelow,X0_twopole,fdata,sqrt(sitot),weights);
    twopolefit=twopolelow(X,fdata);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if aliasing 
    [aliased_freq, aliased_spec] = aliasing_simulator_v2(2*max(fdata),fdata,tot_in,fnyq*2);
    close gcf
end
%%%%%%%%%%%%%%%%

if disp_nep
figure; loglog(fdata,sqrt(sptes),'b');xlabel('frequency (Hz)');ylabel('NEP (W/rtHz)');
        title(['NEP']);hold on;
        %loglog(fdata,sptes_filt,'b--');
        loglog(fdata,sqrt(sptfn),'r');
        %loglog(fdata,sptfn_filt,'r--');
        loglog(fdata,sqrt(spsh),'g');
        %loglog(fdata,spsh_filt,'g--');
        loglog(fdata,sqrt(spamp),'c');
        loglog(fdata,sqrt(sptot),'k--');
        
        %loglog(fdata,sqrt(sptfn_filt.^2+sptes_filt.^2+spamp.^2+spsh_filt.^2),'k--','LineWidth',2);
        %legend('johnson','johnson+L/R','phonon','phonon+L/R','shunt','shunt+L/R','amplifier','total','total+L/R');
        legend('johnson','phonon','shunt','amplifier','total','Location','SE');
        if length(Xo)>14; 
            loglog(fdata,sqrt(spphoton),'y'); 
            legend('johnson','phonon','shunt','amplifier','total','photon','Location','SE');
        end
       % annotation('string',txtstr,'textbox','FitBoxToText','on');
       
end

ylims=[1e0 2e2];
if disp_in
figure;
        if nargin>1; loglog(freq_input,spec_input*10^12,'color',[0.75 0.75 0.75]); end
        hold on;
        loglog(fdata,sqrt(sites)*10^12,'b');xlabel('frequency (Hz)');ylabel('PSD RTI (pA/rtHz)');
        title(['PSD RTI TES']);
        loglog(fdata,sqrt(siexcess)*10^12,'m');
        %loglog(fdata,sites_filt,'b--')
        loglog(fdata,sqrt(sitfn)*10^12,'r')
        %loglog(fdata,sitfn_filt,'r--')
        loglog(fdata,sqrt(sish)*10^12,'g')
        %loglog(fdata,sish_filt,'g--')
        loglog(fdata,sqrt(siamp)*10^12,'c')
        loglog(fdata,sqrt(sitot)*10^12,'k','LineWidth',2);
        %loglog(fdata,sqrt(sites_filt.^2+sitfn_filt.^2+siamp.^2+sish_filt.^2),'k--','LineWidth',2);
        %legend('johnson','johnson+L/R','phonon','phonon+L/R','shunt','shunt+L/R','amplifier','total','total+L/R');
        legend('tes','excess','phonon','shunt','amplifier','total','Location','NE');
        if length(Xo)>14
            loglog(fdata,sqrt(siphoton)*10^12,'y');
            legend('tes','excess','phonon','shunt','amplifier','total','photon','Location','NE');
        end
        if nargin>1
            legend('measured','tes','excess','phonon','shunt','amplifier','total','photon','Location','NE');
        end
       % annotation('string',txtstr,'textbox','FitBoxToText','on');
       if fit_taus;
           text(.1,.15,['\tau_- = ' num2str(X(2)*10^6,3) 'us  ' 'f3dB_- = ' num2str(1/(2*pi*X(2))*1e-3,3) 'kHz'],'Units','normalized');
           text(.1,.1,['\tau_+ =' num2str(X(3)*10^6,3) 'us  ' 'f3dB_+ = ' num2str(1/(2*pi*X(3))*1e-3,3) 'kHz'],'Units','normalized'); 
           loglog(fdata,twopolefit*10^12,'m--','LineWidth',2); 
       end
       if aliasing; 
           loglog([fnyq fnyq],ylims,'r--');
           loglog(aliased_freq,aliased_spec,'m');
           %sispecinterp=interp1(fdata,sitot,aliased_freq,'cubic');
           %loglog(aliased_freq,sqrt(aliased_spec.^2+(sqrt(sispecinterp).*1e12).^2),'k--','LineWidth',2);
           aliasedinterp=interp1(aliased_freq,aliased_spec,fdata(fdata<=fnyq),'cubic');
           loglog(fdata(fdata<=fnyq),sqrt(aliasedinterp.^2 + (sqrt(sitot(fdata<=fnyq)).*1e12).^2),'b','LineWidth',2);
       end
       set(gca,'YLim',ylims)
end

function ydata=twopolelow(X,xdata)
ydata=sqrt(((X(1))./sqrt(1+(2*pi*X(2)*xdata).^2)).^2 + ((X(5))./sqrt(1+(2*pi*X(3)*xdata).^2)).^2 + ((X(4)).^2) );
%ydata=sqrt((X(1).*((sqrt(1+(2*pi*X(2)*xdata).^2)).*(sqrt(1+(2*pi*X(3)*xdata).^2))).^(-1)).^2 + (X(4)^2)); 

function ydata=twotau(X,xdata)
%(X(1)-X(2)*e^(-t/X(3)) * (X(4)-X(5)*e^(-t/X(3))
ydata=(X(1)-exp(xdata./((-1)*X(3))).*X(2)).*(X(4)-exp(xdata./((-1)*X(6))).*X(5));
