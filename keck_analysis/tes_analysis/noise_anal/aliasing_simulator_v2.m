function [aliased_freq, aliased_spec, unaliasedspec] = aliasing_simulator_v2(raw_fsamp,raw_freq,raw_spectra,sim_fsamp,input_freq,input_spectra,titlestr)
%
% [aliased_freq, aliased_spec, unaliasedspec] = aliasing_simulator_v2(raw_fsamp,raw_freq,raw_spectra,sim_fsamp,input_freq,input_spectra,titlestr)
%
% This is a simple spectral aliasing simulator.  The inputs are the
% simulated sampling frequency, the sampling freq for the raw data (raw_fsamp),
% raw_spectra PSD (pA/rtHz), and the frequency bins for the raw_spectra (raw_freq).
%
% Optional input: input_freq and input_spectra (pA/rtHz)
%
% output:   the input raw_spectra aliased into the measurement band defined
%           by sim_fsamp, along with the corresponding freq bins.  Also,
%           the in-band input spectra with in-band aliased noise removed,
%           given in pA/rtHz.
%
% JAB 20090409
% status: development

plot_fig=0;
alias_folds=0;                          % 0 = don't show all the folded spectra in-band

fres=5;                               % resolution of freq bins in simulated spectra
fsamp=sim_fsamp
sim_fsamp=floor(sim_fsamp/(fres*10))*fres*10;

raw_fnyq=raw_fsamp/2;
sim_fnyq=sim_fsamp/2;

raw_freq=raw_freq(2:length(raw_freq));
raw_spectra=(raw_spectra(2:length(raw_spectra)))*10^12;

if nargin<7; titlestr='Noise Aliasing Simulator'; end

ymin=1e-2; ymax=1e3; xmin=1e1; xmax=raw_fnyq;

if plot_fig
  font='courier';
  fontsize=12;
  titlesize=14;
  textsize=12;
  figure('Position',[1 1 1000 700],'DefaultAxesFontName',font,'DefaultAxesFontSize',...
    fontsize,'DefaultTextFontSize',textsize,'DefaultTextFontName',font,...
    'Name',titlestr,'NumberTitle','off')
  loglog(raw_freq,raw_spectra); hold on;
  plot([sim_fnyq sim_fnyq],[ymin ymax*100],'r--');
  axis([xmin xmax ymin ymax])
  xlabel('frequency (Hz)');
  ylabel('PSD RTI (pA/rtHz)');
  title(titlestr,'FontSize',titlesize,'FontName',font)
    %'FontWeight','bold',)
end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fundfreq=[xmin:fres:sim_fnyq];
nfundfreq=length(fundfreq);
aliased_spec=[];
for ff=1:nfundfreq
    harmfreq=[];
    kk=1;
    fund=fundfreq(ff);
    while (kk*fsamp + fund) <= raw_fnyq
        harmfreq = [harmfreq (kk*fsamp - fund) (kk*fsamp + fund)];
        kk=kk+1;
    end
    specinterp=interp1(raw_freq,raw_spectra,harmfreq,'cubic');
    sumsqr=0;
    for ii=1:length(specinterp), sumsqr=[sumsqr + specinterp(ii)^2];,end
    aliased_spec=[aliased_spec sumsqr^.5];
end
aliased_freq=fundfreq;
loglog(aliased_freq,aliased_spec,'k');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% freqinterp=[1:fres:raw_fnyq];
% freqplot=[1:fres:sim_fnyq];
% specinterp=interp1(raw_freq,raw_spectra,freqinterp,'cubic');
% nfoldings=floor((raw_fnyq-sim_fnyq)/(sim_fnyq));
% specsum=zeros(1,ceil(sim_fnyq/fres));
% for i=1:nfoldings
%     index=find(freqinterp>=(i*sim_fnyq) & freqinterp<=(i*sim_fnyq+sim_fnyq));
%     if mod(i,2);, specplot=fliplr(specinterp(index));, else, specplot=specinterp(index);,end;
%     if alias_folds, loglog(freqplot,specplot,'g');end    
%     specsum=(specsum + specplot.^2);
% end
% aliased_spec=sqrt(specsum);
% aliased_freq=freqplot;
% loglog(aliased_freq,aliased_spec,'k');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin >4
    specinterp=interp1(aliased_freq,aliased_spec,input_freq,'cubic');
    unaliasedspec=sqrt(input_spectra.^2-specinterp.^2);
    loglog(input_freq,input_spectra,'g');
    loglog(input_freq,unaliasedspec,'c');
end
%loglog(raw_freq,raw_spectra)
legend('raw spectra','nyquist freq','total aliased noise','aliased input spectra','input spectra aliasing removed');
hold off

%saving option added 20101219 (JET)
save=0;
savedir='/home/spud/analysis/tes_analysis/jab_noise/figs/K3_2/20101229/';
row=0;
col=0;
if save
    savepre=[savedir 'aliasing_simulator_c' num2str(col) 'r' num2str(row)];
%     savefile=[savepre '.fig'];
%     saveas(gcf,savefile);
    savefile=[savepre '.png'];
    saveas(gcf,savefile);
end
