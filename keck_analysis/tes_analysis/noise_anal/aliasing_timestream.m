function [slow_f slow_spec superfast_freq superfast_spec] = aliasing_timestream(noisedir, samplingfreq, row, col, gain)

%SAS 1/11/2011
%
%Takes in the timestream and just samples it at a lower frequency.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% user defined inputs:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
refer_to_input=1;
nfft=25000;
%nfft=1000;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin<5
  gain='calib';
end

if strcmp(gain,'calib');
  gains=fb_err_gain_fastacq(noisedir,row,col);
  rc_col=mod(col,8);
  gain = gains(rc_col+1);
end


numframes=375000;
fastfile=dir([noisedir '/superfast*' 'row' num2str(row) '*col' num2str(col)]);
[data aux h]=read_mce_datfile([noisedir '/' fastfile(1).name],0,numframes);
fast=rvec(aux);
cc_ncols=bin2dec(num2str(bitget(h(1,1), 20:-1:17)));
rc_present=bitget(h(1,1), 1 + [10 11 12 13]);
num_rc=sum(rc_present);
cc_nc=cc_ncols*num_rc;
cc_nr=h(1,4);
rc_nc=1;     %always single channel
rc_nr=1;     %always single channel
data_rate=h(1,5);
row_len=h(1,3);
num_rows=h(1,10); 

fsamp=(50e6/(row_len*num_rows*data_rate))*((cc_nr*cc_nc)/(rc_nr*rc_nc));

noverlap=0;
window=boxcar(nfft);
[spec,f]=psdmat(fast',nfft,fsamp,[],noverlap);
fastspec=(2*spec(2:end)/fsamp).^.5;
if refer_to_input;
    superfast_spec=((FB1adc_to_inputcurrent(fastspec,col))./gain);
else
    superfast_spec=fastspec;
end
superfast_freq=f(2:end);

%Now it is read in the same way as superfast_calc
%
%Downsample

ratio=round(fsamp/samplingfreq);
nsamp=floor(length(fast)/ratio);

slow=zeros(ratio,nsamp);

for i=1:ratio
  for j=1:nsamp
    slow(i,j)=fast(ratio*(j-1)+i);
  end
end

[spec,slow_f]=psdmat(slow',round(nfft/ratio),samplingfreq,[],noverlap);
slow_spec=(2*mean(spec(2:end,:),2)/samplingfreq).^.5;
if refer_to_input;
  slow_spec=((FB1adc_to_inputcurrent(slow_spec,col))./gain);
end
slow_f=slow_f(2:end);
