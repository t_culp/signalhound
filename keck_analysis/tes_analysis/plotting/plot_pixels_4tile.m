function plot_pixels_4tile(data,varargin)
% function plot_pixels_4tile(data, ...
%               [colorbar_axis,titlestr,datatype,tilenames],[keywords])
%
% This program plots a set of scalar detector parameters to the
% corresponding physical location on the tile.  This version of the code
% plots all 4 tiles as they would appear on the focal plane, with the proper
% relative rotations of the tiles.
% 
% ARGUMENTS:    
%       data:
%           a (1x528) vector or (33x16) matrix of detector parameters (Tc,
%           Rn, etc.)  The ordering of the data is in mce coordinates
%           rows 0-32, cols 0-15.  The top left corner of the matrix is
%           0,0.  The ordering of the vector follows that given by
%           read_mce, as shown below.
%
%       colorbar_axis: (default: [data_min, data_max])
%           colorbar_axis=[axis_min,axis_max], specifying the range used by
%           the colorbar.
%
%       titlestr: (default: '')
%           titlestr=string for tile label, proceeds 'TILE #', eg. 'Rn'
%
%       datatype: (default: '')
%           datatype='string for colorbar label', eg. 'm\Omega', 'mK'.
%
%       tilenames: (default: {'Tile1', ... 'Tile4'})
%           tilenames={'Tile1name','Tile2name','Tile3name','Tile4name'}
%
% KEYWORDS:
%       TileLayoutSpec: (default: 'Standard8x8_DS1')
%           specifies the name of a .tile file describing the
%           correspondence between physical and MUX coordinates.  Can be a
%           single string (if all tiles match) or a cell array of four
%           strings (in tile number order).  The extension '.tile' is added
%           if not present, and if the full path is not specified the files
%           are assumed to reside in the same directory as this function.
%       TileOrder: (default: [1,2,4,3])
%           arrangement of the four tiles on the focal plane in Matlab
%           subplot coordinates: [r1c1,r1c2,r2c1,r2c2].  Note that this
%           differs for RevA/C/D/E focal planes and RevX focal planes.
%           RevA/C/D/E: [1,2,4,3]
%           RevX:       [3,1,2,4]
%       ColorMap: (default: 'jet')
%           name of a Matlab colormap to use when coloring the plots
% 
%
% JAB 20090317 - STATUS: development
% ART 20090718 - add ability to provide tile names at cmd line 
% JPF 20110804 - Major rewrite for greater generality in tile layout
%                Now also illustrates A/B polarization axes

% --- USEFUL PARAMETERS ---
Ntile = 4;
Ncol = 4*Ntile;
Nrow = 33;
% code is NOT as general as these parameterizations indicate!

% --- PLOTTING DEFAULTS ---
if ~isunix
    font='Times New Roman';
else
    font = 'Helvetica';
end
fontsize=14;
% specify subplots (numbered as in Matlab subplot)
tileposX = [0.1 0.5 0.1 0.5]; % absolute X positions for subplots 1-4
tileposY = [0.55 0.55 0.08 0.08]; % absolute X positions for subplots 1-4
xAxisLoc = {'bottom','bottom','top','top'}; % side where the X labels lie
yAxisLoc = {'right','left','right','left'}; % side where the Y labels lie
CBarLoc = {'WestOutside','EastOutside','WestOutside','EastOutside'}; % side where the colorbar lies
CBarLabelLoc = {'top','top','top','top'}; % side where the colorbar label lies
TitleLoc = {'top','top','bottom','bottom'}; % side where the tile title lies


% --- HANDLE ARGUMENTS ---
% 1. define the argument list
p = inputParser;
% the data matrix to plot
p.addRequired('data',@(x) isequal(size(x),[Nrow,Ncol]) || length(x(:))==Nrow*Ncol);
% the colorbar range
p.addOptional('colorbar_axis',[],@(x) isnumeric(x) && length(x)==2);
% the title of the plot
p.addOptional('titlestr','',@ischar);
% the title of the colorbar (i.e. the units)
p.addOptional('datatype','',@ischar);
% titles for each tile
p.addOptional('tilenames',{'Tile1','Tile2','Tile3','Tile4'},...
    @(x) iscell(x) && length(x)==Ntile);
% Tile layout description files
p.addParamValue('TileLayoutSpec','Standard8x8_DS1',@(x) ischar(x) || ...
    (iscell(x) && length(x(:))==Ntile))
% Tile arrangement: order to plot in the FPU: [r1c1, r1c2, r2c1, r2c2]
p.addParamValue('TileOrder',[1,2,4,3],@(x) isequal(unique(x),1:Ntile))
% Matlab colormap to use
p.addParamValue('ColorMap','jet',@ischar)
% 2. parse the actual arguments
p.parse(data,varargin{:});
% 3. move results to the main namespace
colorbar_axis = p.Results.colorbar_axis;
titlestr = p.Results.titlestr;
datatype = p.Results.datatype;
tilenames = p.Results.tilenames;
TileLayoutSpec = p.Results.TileLayoutSpec;
TileOrder = p.Results.TileOrder;
cm = p.Results.ColorMap;
% 4. do any cleanup work
% ... unspecified colorbar limits: set to full range
if isempty(colorbar_axis)
    colorbar_axis=[nanmin(data(:)) nanmax(data(:))];
end
% ... one tile layout specified => all tiles
if ischar(TileLayoutSpec)
    TileLayoutSpec = repmat({TileLayoutSpec},1,Ntile);
end
% ... force data to consistent shape
data = reshape(data,Nrow*Ncol,1);


% --- BUILD THE TILE LAYOUT LOOKUP TABLE ---
TileLayout = {};
TileDim = zeros(Ntile,1);
for tile=1:Ntile
    % For each tile
    % ... load the layout definition (adding path/extension if needed)
    layout_fname = TileLayoutSpec{tile};
    [dummyPath,dummyName,dummyExt] = fileparts(layout_fname);
    if ~strcmp(dummyExt,'.tile')
        layout_fname = [layout_fname '.tile'];
    end
    if isempty(dummyPath)
        layout_fname = fullfile(fileparts(which(mfilename)),layout_fname);
    end
    fid = fopen(layout_fname);
    TileLayoutFile = textscan(fid,'%d %d %s %d %d\n','CommentStyle','%');
    % .... PhysRow,PhysCol,PhysPol,MuxRow,MuxCol ....
    fclose(fid);
    TileDim(tile) = floor(sqrt(length(TileLayoutFile{1})/2));
    % ... make a table matching face indices (r1c1A,r1c1B,r1c2A,...)
    % to mce data vector indices (1 + col*Nrow+row)
    faceind = 1 + double((TileLayoutFile{1}-1)*2*TileDim(tile) + ...
        2*(TileLayoutFile{2}-1))+([TileLayoutFile{3}{:}]'-'A');
    muxind = 1 + double(TileLayoutFile{4} + Nrow*(TileLayoutFile{5}+(tile-1)*4));
    TileLayout{tile} = muxind(faceind); % sort into face order
end


% assemble vertices and faces for each tile
vertices={};
faces = {};
for tile = 1:Ntile
    % Assemble vertices for corners of a TileDim x TileDim array
    [Y,X] = meshgrid(0:TileDim(tile),0:TileDim(tile));
    vertices{tile} = [X(:) Y(:)]; % counts along each row, starting at r1c1
    % Define the triangular faces: r1c1A,r1c1B,r1c2A,r1c2B,...
    onePxAB = [0,TileDim(tile)+1,TileDim(tile)+2; ...
        0,1,TileDim(tile)+2]; % one pixel A,B
    faces{tile} = zeros(TileDim(tile)^2,3); % preallocate for faces
    for tileRow = 1:TileDim(tile)
        for tileCol = 1:TileDim(tile)
            pxInd = 1+2*((tileRow-1)*TileDim(tile) + (tileCol-1));
            faces{tile}(pxInd:pxInd+1,:)= onePxAB + ...
                (tileRow-1)*(TileDim(tile)+1)+(tileCol-1)+1;
        end
    end
    % vertices and faces now define (TileDim^2) pixels to color in later
end

% ---- PREPARE THE FIGURE ----
figure('DefaultAxesFontSize',fontsize,'DefaultAxesFontName',font,'DefaultTextFontSize', fontsize,'DefaultTextFontName',font,...
    'Position',[1 1 1000 750]);
if ~isempty(titlestr)
    set(gcf,'Name',['4 TILE FPU ' titlestr],'NumberTitle','off');
end
hold on;
colormap(cm);

for tile = 1:Ntile
    % TILE #(tile)
    % position the plot
    subplotnum = find(TileOrder==tile);
    subplotpos = [tileposX(subplotnum) tileposY(subplotnum) 0.4 0.4];
    subplot(2,2,subplotnum,'Position',subplotpos);
    % render the patch objects
    p1=patch('Faces',faces{tile},'Vertices',vertices{tile}, 'FaceColor','flat',...
        'CData',data(TileLayout{tile}),'CDataMapping','scaled', ...
        'EdgeColor','k','LineWidth',1);
    % overlay a pixel definition grid
    hold on;
    for ii=0:TileDim(tile)
        plot([0 TileDim(tile)],ii*[1 1],'k','LineWidth',2);
        plot(ii*[1 1],[0 TileDim(tile)],'k','LineWidth',2);
    end
    hold off;
    % arrange titles and colorbars
    caxis(colorbar_axis);
    axis ([0 TileDim(tile) 0 TileDim(tile)]); 
    axis square; 
    cb=colorbar('XAxisLocation',CBarLabelLoc{subplotnum},'YAxisLocation', ...
        yAxisLoc{subplotnum}, 'Location',CBarLoc{subplotnum});
    set(gca,'XTick',0.5:7.5,'XTickLabel',char('1','2','3','4','5','6','7','8'),...
        'YTick',0.5:7.5,'YTickLabel',char('1','2','3','4','5','6','7','8'),...
        'XAxisLocation',xAxisLoc{subplotnum},'YAxisLocation',yAxisLoc{subplotnum},...
        'TickLength',[.001 .001]);
    if subplotnum<=2
        set(gca,'YDir','reverse');
    else
        set(gca,'XDir','reverse');
    end
    set(get(cb,'XLabel'),'String',datatype)
    titleuse=[titlestr ' ' tilenames{tile}];
    t1=title(titleuse,'FontSize',fontsize,'FontName',font,'FontWeight','bold');
    if subplotnum>2
        % fix title position
        pos_t1 = get(t1,'position');
        pos_t1(2) = pos_t1(2)-TileDim(tile)*1.2;
        set(t1,'position',pos_t1);
    end
   
    % Positioning only one copy of some labels
    if ismember(subplotnum,[2,4])
        ylabel('det row');
    end
    if ismember(subplotnum,[1,2])
        xlabel('det col');
        xl_pos=get(get(gca,'XLabel'),'Position');
        %xl_pos(2) = xl_pos(2)-1.5;
        set(get(gca,'XLabel'),'Position',xl_pos);
    end
    
    % Label the A and B polarizations once
    if subplotnum<=2
        text(.4,.7,'\underline{A}','HorizontalAlignment','center','Interpreter','latex');
        text(.75,.3,'B|','HorizontalAlignment','center');
    else
        text(.3,.75,'\underline{A}','HorizontalAlignment','center','Interpreter','latex');
        text(.7,.35,'B|','HorizontalAlignment','center');
    end
end



