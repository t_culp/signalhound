function plot_4tile_hist(data,titlestr,xlabelstr,xbin_edges, ...
    xtick_spacing,tile_order)
% function plot_4tile_hist(data,titlestr,xlabelstr,xbin_edges, ...
%   xtick_spacing,[tile_order])
%
% Makes a plot of some value with separate histograms for each tile.
%
% INPUTS:
%  data: (1,528) or (33,16) matrix of data for histograms.
%  titlestr: String to add to the title of each histogram.
%  xlabelstr: String to label the x-axis of each histogram.
%  xbin_edges: [x1...xn] vector of bin edges for histograms
%  xtick_spacing: spacing between labels on the x-axis
%  tile_order: arrangement of tiles in the plot, like plot_pixels_4tile
%              default: [1,2,4,3] (ReV A/C/D/E); for RevX use [1,3,4,2]
%
% 2009      ART Created
% 17Sep2011 JPF Some rearranging and changes of arguments
%

% handle arguments
if nargin<6
    tile_order = [1,2,4,3]; % Default for Rev A/C/D/E focal planes
end

figure('Position',[0 0 1000 750]);
if ~isunix
    fontstr='Times New Roman';
    set(gcf,'DefaultTextFontName',fontstr,'DefaultAxesFontName',fontstr);
end
set(gcf,'Name',titlestr);
datadim = sort(size(data));
if (datadim(1)==1 && datadim(2)==528)
    data=reshape(data,33,16);
end

rescale=1;
autoscale_y=1;
full_stats=1; % add means and standard deviations

% compute the histograms for later plotting
for tile = 1:4
    histdata=reshape(data(:,(1:4)+4*(tile-1)).*rescale,1,132);
    [n{tile}, bin{tile}]=histc(histdata,xbin_edges);
    med(tile) = nanmedian(histdata(bin{tile}>0));
end

if autoscale_y
    ymax=max([n{1},n{2},n{3},n{4}]);
end

h=[];
for plot_pos = 1:4
    tile = tile_order(plot_pos);
    subplot(2,2,plot_pos);
    h(plot_pos) = bar(xbin_edges,n{tile},'b');
    xlabel(xlabelstr,'FontSize',14);
    ylabel('counts','FontSize',14);
    title(['TILE ' int2str(tile) '  (N=' num2str(sum(n{tile})) ')'], 'FontSize',14);
    set(gca,'XLim',[xbin_edges(1) xbin_edges(end)]);
    set(gca,'XTick',xbin_edges(1):xtick_spacing:xbin_edges(end));
    if autoscale_y
        set(gca,'YLim',[0 ymax]);
    end
    grid on;
    if full_stats
        xl = xlim;
        yl = ylim;
        xpos = xl(1) + diff(xl)*0.05;
        ypos = yl(1) + diff(yl)*0.85;
        text(xpos,ypos, ...
            ['med: ' num2str(med(tile),'%2.3f')],'FontSize',16);
    end
end;

