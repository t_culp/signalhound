function createTileLayoutSpec(filename, varargin)
    % function createTileLayoutSpec(filename, varargin)
    %
    % Function to create a text file describing a tile layout, matching MCE
    % coordinates (row,col) to physical coordinates (row,col,A/B).  This
    % generalizes our usual tile coordinate plotting schemes and enables
    % cross-platform use.
    %
    % This helps to automate the creation of "standard" types of tiles, but
    % for oddball configurations you may need to alter this.
    %
    % ARGUMENTS
    %    filename: filename to write to
    %
    % OPTIONAL PARAMETERS
    %
    %    TileDim: Number of pixels along each side of the tile (default 8)
    %    DSrow: Mux row for the dark SQUID (default 0)
    %
    % TODO: Specify dark TESs?  Make algorithm more generic?
    %
    % JPF 20110804
    
    % --- BASIC CONSTANTS ---
    Ncol_MUX = 4; % per tile
    Nrow_MUX = 33; % in the MCE
    
    % --- HANDLE ARGUMENTS ---
    p = inputParser;
    % 1. define the argument list
    % ... the file name to save to
    p.addRequired('filename',@ischar);
    % ... number of pixels across the tile
    p.addParamValue('TileDim',8,@(x) ismember(x,[6 8]));
    % ... row for the dark SQUID
    p.addParamValue('DSrow',0,@(x) ismember(x,0:(Nrow_MUX-1)));
    % ... comment for the file
    p.addParamValue('Comment','',@char);
    % ... unbonded MUX rows (mostly relevant for 90 GHz tiles)
    %p.addParamValue('SkipRows',[],@(x) ismember(x,0:(Nrow_MUX-1)))
    % ... MUX rows corresponding to physical r1c1A/B
    %p.addParamValue('MuxRows1AB',[8,7],@(x) isnumeric(x)&&length(x(:))==2;
    % 2. parse the actual arguments
    p.parse(filename,varargin{:});
    
    % --- ESTABLISH PIXEL SEQUENCES ---
    % sequence in which mux rows map onto polarizations
    pol_sequence = {'B','A'};
    
    % sequence in which mux columns map onto pixels
    %col_sequence = [zeros(2*p.Results.TileDim,1); ...
    %    ones(2*p.Results.TileDim,1)];
    
    % sequence in which mux rows map onto pixels
    %row_sequence = (p.Results.TileDim-1):-2:-(p.Results.TileDim-1);
    %row_sequence(row_sequence<1) = abs(row_sequence(row_sequence<1))+1;
    %row_sequence = reshape(repmat(row_sequence,2,2), ...
    %    4*p.Results.TileDim,1); % 2 pols, 2 cols
    
    % multiplexer row sequence
    %MCErow_skip = p.Results.DSrow;
    %if p.Results.TileDim==6
        % add more skipped rows   
    %end
    
    % adapted from det2mce.m
    % sequence of columns
    if p.Results.TileDim==8
        mce_col_index=[0 0 1 1 2 2 3 3]';
    elseif p.Results.TileDim==6
        mce_col_index=[0 1 1 2 2 3]';
    end

    if p.Results.TileDim==8
        mce_row_index=[7 9 5 11 3 13 1 15]';
    elseif p.Results.TileDim==6
        mce_row_index=[7 9 5 11 3 13]';
    end
    
    % open the file for writing
    fid = fopen(filename,'w');
    
    % print the header
    [dummy,froot] = fileparts(filename);
    fprintf(fid,'%% %s\n',froot);
    fprintf(fid,'%% %s\n',p.Results.Comment);
    fprintf(fid,'%%\n');
    fprintf(fid,'%% PhysRow PhysCol PhysPol MuxRow MuxCol\n');
    fprintf(fid,'%% -------------------------------------\n');
    
    % loop over physical pixels
    for colPhys = 1:p.Results.TileDim
        for rowPhys = 1:p.Results.TileDim
            mux_col = mce_col_index(colPhys);
            mux_row = mce_row_index(rowPhys);
            if colPhys>1 && mce_col_index(colPhys-1)==mux_col
                mux_row = mux_row+16;
            end
            if p.Results.DSrow==1
                % handle DS=r1 **ONLY**
                mux_row = mod(mux_row+1,Nrow_MUX);
            end
            
            % print the two pixels
            for ii=1:2
                fprintf(fid,'%d %d %c %d %d\n', ...
                    rowPhys,colPhys,pol_sequence{ii}, ...
                    mux_row+(ii-1),mux_col);
            end
            
        end
    end
    fclose(fid);
end