function plot_pixels_4tile_OLD(data,colorbar_axis,titlestr,datatype,tilenames)
% 
% This program plots a set of scalar detector parameters to the
% corresponding physical location on the tile.  This version of the code
% plots all 4 tiles as they would appear on the focal plane, with the proper
% relative rotations of the tiles.
% 
% input:    
%    
%       data:
%           a (1x528) vector or (33x16) matrix of detector parameters (Tc,
%           Rn, etc.)  The ordering of the data is in mce coordinates
%           rows 0-32, cols 0-15.  The top left corner of the matrix is
%           0,0.  The ordering of the matrix follows that given by
%           mce_read, as shown below.
%
%       colorbar_axis:
%           colorbar_axis=[axis_min,axis_max], specifying the scale used by
%           the colorbar.
%
%       titlestr:
%           titlestr='string for tile label', proceeds 'TILE #', eg. 'Rn'
%
%       datatype:
%           datatype='string for colorbar label', eg. 'm\Omega', 'mK'.
%
%       tilenames:
%           tilenames={'Tile1name','Tile2name','Tile3name','Tile4name'}
%
% output:   a 4 tile plot of the parameters, with detectors A & B overlayed in
%           a single pixel.  A scalable colorbar is also plotted for each
%           tile.
% 
%
% JAB 20090317 - STATUS: development
% ART 20090718 - add ability to provide tile names at cmd line 
% JPF 20100616 - Default to dark SQUID on row 0

font='Times New Roman';
fontsize=14;

% handle arguments
if nargin<5 || isempty(tilenames)
    tilenames{1}='Tile1';
    tilenames{2}='Tile2';
    tilenames{3}='Tile3';
    tilenames{4}='Tile4';
end
if nargin<4 || isempty(datatype)
    datatype='';
end
if nargin<3 || isempty(titlestr)
    titlestr='';
end
if nargin<2 || isempty(colorbar_axis)
    colorbar_axis=[nanmin(data(:)) nanmax(data(:))];
end

%reshape data from mce indices to detector indices using mce_det_map.m
%data for the 1 tile patch plots should be size=3x128, with all rows the same
%value for each column.
%
% mce_read gives s=(1x528) in order: 
%               [(row,col) ... (0,0),(1,0),(2,0)...(0,1),(1,1),(2,1)...]
%                                       
%
% data plotting order:  row: 1 1 1 1     1 1 2 2
%                       col: 1 1 2 2 ... 8 8 1 1 ... 
%                       pix: A B A B     A B A B
 
datadim=size(data);
if (datadim(1)==33 && datadim(2)==16), data=reshape(data,1,528);,end

%coordinates of data input
row_order_data=[repmat([0:32],1,16)];
col_order_data=[];
for i=0:15, col_order_data=[col_order_data repmat(i,1,33)];,end;

%plotting coordinates for 1 tile
row_order_plot=[];
for i=1:8,row_order_plot=[row_order_plot repmat(i,1,16)];,end;
col_order_plot=[repmat([1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8],1,8)];
pix_order_plot=transpose([repmat(['A';'B'],64,1)]);

%filling up the color vector for the patch objects
c1=[];
c2=[];
c3=[];
c4=[];

for j=1:128,
    [mce_row_t1 mce_col_t1]=det2mce(row_order_plot(j),col_order_plot(j),pix_order_plot(j),1,'run4');
    [mce_row_t2 mce_col_t2]=det2mce(row_order_plot(j),col_order_plot(j),pix_order_plot(j),2,'run4');
    [mce_row_t3 mce_col_t3]=det2mce(row_order_plot(j),col_order_plot(j),pix_order_plot(j),3,'run4');
    [mce_row_t4 mce_col_t4]=det2mce(row_order_plot(j),col_order_plot(j),pix_order_plot(j),4,'run4');
    
    s1=find(row_order_data == mce_row_t1);
    plot_val1=data(s1(ismember(find(row_order_data == mce_row_t1),find(col_order_data == mce_col_t1))));
    s2=find(row_order_data == mce_row_t2);
    plot_val2=data(s2(ismember(find(row_order_data == mce_row_t2),find(col_order_data == mce_col_t2))));
    s3=find(row_order_data == mce_row_t3);
    plot_val3=data(s3(ismember(find(row_order_data == mce_row_t3),find(col_order_data == mce_col_t3))));
    s4=find(row_order_data == mce_row_t4);
    plot_val4=data(s4(ismember(find(row_order_data == mce_row_t4),find(col_order_data == mce_col_t4))));
    c1=[c1 plot_val1];
    c2=[c2 plot_val2];
    c3=[c3 plot_val3];
    c4=[c4 plot_val4];
end
    c1=[c1;c1;c1];
    c2=[c2;c2;c2];
    c3=[c3;c3;c3];
    c4=[c4;c4;c4];
    
figure('DefaultAxesFontSize',fontsize,'DefaultAxesFontName',font,'DefaultTextFontSize', fontsize,'DefaultTextFontName',font,...
    'Position',[1 1 1000 750]);
if nargin > 2, set(gcf,'Name',['4 TILE FPU ' titlestr],'NumberTitle','off'),end

hold on;
colormap('jet');
    cmin=colorbar_axis(1);
    cmax=colorbar_axis(2);

%Creating patch objects
xseed=[];
yseed=[];
yr2=[];
for ii=1:7, xseed=[xseed ii ii];, end;
xr1=repmat([0 0 xseed],1,8);
xr2=repmat([0 xseed 8],1,8);
xr3=repmat([xseed 8 8],1,8);
for ii=1:7 yseed=[yseed repmat(ii,1,16)];, end;
yr1=[repmat(0,1,16) yseed];
yr3=[yseed repmat(8,1,16)];
for ii=0:7 yr2=[yr2 (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii];, end;
x=[xr1;xr2;xr3];
y=[yr1;yr2;yr3];

%Displaying patch objects

%TILE 1
subplot(2,2,1,'Position',[.1 .55 .4 .4])
h1=patch(x,y,c1,'CDataMapping','scaled');
titleuse=[titlestr ' ' tilenames{1}];
title(titleuse,'FontSize',14,'FontName',font,'FontWeight','bold');
caxis([cmin cmax]);
axis ([0 8 0 8]); axis square; 
cb=colorbar('XAxisLocation','top','YAxisLocation','Left', ...
    'Location','WestOutside');
%xlabel('det col');
%ylabel('det row');
set(gca,'XTick',0.5:7.5,'XTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YTick',0.5:7.5,'YTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YDir','reverse','XAxisLocation','bottom','YAxisLocation','right',...
    'TickLength',[.001 .001]);
if nargin > 3, set(get(cb,'XLabel'),'String',datatype),end;
text(.25,.7,'A');
text(.6,.3,'B'); hold on;
for i=0:8,
plot([0:8],repmat(i,1,9),'k','LineWidth',2);
plot(repmat(i,1,9),[0:8],'k','LineWidth',2);
end; hold off;

%TILE 2
subplot(2,2,2,'Position',[.5 .55 .4 .4])
h2=patch(x,y,c2,'CDataMapping','scaled');
titleuse=[titlestr ' ' tilenames{2}];
title(titleuse,'FontSize',14,'FontName',font,'FontWeight','bold');
caxis([cmin cmax]);
axis ([0 8 0 8]); axis square; 
cb=colorbar('XAxisLocation','top');
set(cb,'YAxisLocation','Left'); %JF: bug workaround for R2009a
set(cb,'YAxisLocation','Right'); %JF: bug workaround for R2009a
%xlabel('det col');
ylabel('det row');
set(gca,'XTick',0.5:7.5,'XTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YTick',0.5:7.5,'YTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YDir','reverse','XAxisLocation','bottom',...
    'TickLength',[.001 .001]);
if nargin > 3, set(get(cb,'XLabel'),'String',datatype),end;
text(.25,.7,'A');
text(.6,.3,'B'); hold on;
for i=0:8,
plot([0:8],repmat(i,1,9),'k','LineWidth',2);
plot(repmat(i,1,9),[0:8],'k','LineWidth',2);
end; hold off;

%TILE 3
subplot(2,2,4,'Position',[.5 .08 .4 .4])
h3=patch(x,y,c3,'CDataMapping','scaled');
titleuse=[titlestr ' ' tilenames{3}];
t1=title(titleuse,'FontSize',14,'FontName',font,'FontWeight','bold');
pos1=get(t1,'position');
pos1(2)=pos1(2)-8.9;
set(t1,'position',pos1);
caxis([cmin cmax]);
axis ([0 8 0 8]); axis square; 
cb=colorbar('XAxisLocation','top');
set(cb,'YAxisLocation','Left'); %JF: bug workaround for R2009a
set(cb,'YAxisLocation','Right'); %JF: bug workaround for R2009a
xlabel('det col');
ylabel('det row');
set(gca,'XTick',0.5:7.5,'XTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YTick',0.5:7.5,'YTickLabel',char('1','2','3','4','5','6','7','8'),...
    'XAxisLocation','top','TickLength',[.001 .001],'XDir','reverse');
if nargin > 3, set(get(cb,'XLabel'),'String',datatype),end;
text(.4,.7,'A');
text(.75,.3,'B'); hold on;
for i=0:8,
plot([0:8],repmat(i,1,9),'k','LineWidth',2);
plot(repmat(i,1,9),[0:8],'k','LineWidth',2);
end; hold off;

%TILE 4
subplot(2,2,3,'Position',[.1 .08 .4 .4])
h4=patch(x,y,c4,'CDataMapping','scaled');
titleuse=[titlestr ' ' tilenames{4}];
t2=title(titleuse,'FontSize',14,'FontName',font,'FontWeight','bold');
pos2=get(t2,'position');
pos2(2)=pos2(2)-8.9;
set(t2,'position',pos2);
caxis([cmin cmax]);
axis ([0 8 0 8]); axis square; 
cb=colorbar('XAxisLocation','top','Location','WestOutside');
set(cb,'YAxisLocation','Left');
xlabel('det col');
%ylabel('det row');
set(gca,'XTick',0.5:7.5,'XTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YTick',0.5:7.5,'YTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YAxisLocation','right','XAxisLocation','top','TickLength',[.001 .001],...
    'XDir','reverse');
if nargin > 3, set(get(cb,'XLabel'),'String',datatype),end;
text(.4,.7,'A');
text(.75,.3,'B'); hold on;
for i=0:8,
plot([0:8],repmat(i,1,9),'k','LineWidth',2);
plot(repmat(i,1,9),[0:8],'k','LineWidth',2);
end; hold off;






