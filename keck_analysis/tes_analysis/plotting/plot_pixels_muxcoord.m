function h = plot_pixels_muxcoord(data,colorbar_axis,titlestr,datatype)
% function h = plot_pixels_muxcoord(data,colorbar_axis,titlestr,datatype)
%
% INPUTS
%  - data: a (1x528) vector or (33x16) matrix of detector parameters 
% Any elements of data which are NaN or Inf 
% will appear white (actually transparent) in the plot.
%  - colorbar: 2-element vector indicating the [minimum maximum] values of
%  data for the color axis.  If empty, uses data min and max.
%  - titlestr: Plot title string.  None if empty.
%  - datatype: Header for the colorbar.  None if empty.
%
% OUTPUTS
%  - h (optional): figure handle
%
% 090318 JPF
% 090331 JPF Allow for empty options.
% 090409 JPF Treat Inf and -Inf as NaN when plotting (useful for log plots)
% 090718 ART Allow input of 33x16 OR 1x528 OR 528x1 matrix

font='Times New Roman';
fontsize=14;

% reshape data from mce indices to detector indices
datadim=size(data);
if (datadim(1)==528 && datadim(2)==1), data=reshape(data,33,16); end
if (datadim(1)==1 && datadim(2)==528), data=reshape(data,33,16); end

% convert non-finite data to NaN for convenience
data(~isfinite(data))=NaN;

% handle arguments
if nargin<4 || isempty(datatype)
    datatype='';
end
if nargin<3 || isempty(titlestr)
    titlestr='';
end
if nargin<2 || isempty(colorbar_axis)
    colorbar_axis=[nanmin(data(:)) nanmax(data(:))];
end

% create the figure (and basic properties
h = figure('DefaultAxesFontSize',fontsize,...
    'DefaultAxesFontName',font,'DefaultTextFontSize', fontsize,...
    'DefaultTextFontName',font,...
    'Position',[1 1 1000 750]);

% plot the image, using AlphaData to remove all NaN or Inf pixels
image('XData',[0:15],'YData',[0:32],'CData',data, ...
    'AlphaData',~isnan(data),'CDataMapping','scaled');
set(gca,'Clim',colorbar_axis);
colormap('jet');
hold on;

% labels
xlabel('MUX Column','FontName',font,'FontSize',fontsize+2,'FontWeight','bold');
ylabel('MUX Row','FontName',font,'FontSize',fontsize+2,'FontWeight','bold');
if nargin >=3
    title(titlestr,'FontSize',fontsize+4,'FontName',font,...
        'FontWeight','bold');
end


cb = colorbar('XAxisLocation','top','Location','EastOutside');
if nargin > 3   
    set(get(cb,'XLabel'),'String',datatype);
end

if nargin > 3
    set(get(cb,'XLabel'),'String',datatype);
end



% overlay grid
colborder = -0.5:15.5;
rowborder = -0.5:32.5;
line(repmat(colborder,2,1),repmat(ylim',1,length(colborder)),'Color',0.5*[1 1 1],'LineStyle','-');
line(repmat(xlim',1,length(rowborder)),repmat(rowborder,2,1),'Color',0.5*[1 1 1],'LineStyle','-');

% fix the axis
axis([-0.5 15.5001 -0.5 32.5001])


