function plot_pixels_1tile(data,colorbar_axis,tile,titlestr,datatype)
% 
% This program plots a set of scalar detector parameters to the
% corresponding physical location on the tile.  This version of the code
% plots a single tile.
% 
% input:    
%    
%       data:
%           a (1x528) vector or (33x16) matrix of detector parameters (Tc,
%           Rn, etc.)  The ordering of the data is in mce coordinates
%           rows 0-32, cols 0-15.  The top left corner of the matrix is
%           0,0.  The ordering of the matrix follows that given by
%           mce_read, as shown below.
%
%       colorbar_axis:
%           colorbar_axis=[axis_min,axis_max], specifying the scale used by
%           the colorbar.
%
%       tile:
%           tile=1-4, specifying the detector array you want displayed.
%
%       titlestr:
%           titlestr='string for tile label', proceeds 'TILE #', eg. 'Rn'
%
%       datatype:
%           datatype='string for colorbar label', eg. 'm\Omega', 'mK'.
%
% output:   a 1 tile plot of the parameters, with detectors A & B overlayed in
%           a single pixel.  A scalable colorbar is also plotted.
% 
%
%JAB 20090305
%STATUS: development

%reshape data from mce indices to detector indices using mce_det_map.m
%data for the 1 tile patch plots should be size=3x128, with all rows the same
%value for each column.
%
% mce_read gives s=(1x528) in order: 
%               [(row,col) ... (0,0),(1,0),(2,0)...(0,1),(1,1),(2,1)...]
%                                       
%
% data plotting order:  row: 1 1 1 1     1 1 2 2
%                       col: 1 1 2 2 ... 8 8 1 1 ... 
%                       pix: A B A B     A B A B
 
datadim=size(data);
if (datadim(1)==33 && datadim(2)==16), reshape(data,1,528);,end

%coordinates of data input
row_order_data=[repmat([0:32],1,16)];
col_order_data=[];
for i=0:15, col_order_data=[col_order_data repmat(i,1,33)];,end;

%plotting coordinates for 1 tile
row_order_plot=[];
for i=1:8,row_order_plot=[row_order_plot repmat(i,1,16)];,end;
col_order_plot=[repmat([1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8],1,8)];
pix_order_plot=transpose([repmat(['A';'B'],64,1)]);

%filling up the color vector for the patch objects
c=[];
for j=1:128,
    [mce_row mce_col]=det2mce(row_order_plot(j),col_order_plot(j),pix_order_plot(j),tile);
    s1=find(row_order_data == mce_row);
    plot_val=data(s1(ismember(find(row_order_data == mce_row),find(col_order_data == mce_col))));
    c=[c plot_val];
end
    c=[c;c;c];

font='Times New Roman';
fontsize=12;
figure('DefaultAxesFontSize',fontsize,'DefaultAxesFontName',font,...
    'DefaultTextFontSize', fontsize,'DefaultTextFontName',font);
if nargin > 3, set(gcf,'Name',['1 TILE ' titlestr],'NumberTitle','off'),end

hold on;
colormap('jet');
    cmin=colorbar_axis(1);
    cmax=colorbar_axis(2);

%Creating patch objects
xseed=[];
yseed=[];
yr2=[];
for ii=1:7, xseed=[xseed ii ii];, end;
xr1=repmat([0 0 xseed],1,8);
xr2=repmat([0 xseed 8],1,8);
xr3=repmat([xseed 8 8],1,8);
for ii=1:7 yseed=[yseed repmat(ii,1,16)];, end;
yr1=[repmat(0,1,16) yseed];
yr3=[yseed repmat(8,1,16)];
for ii=0:7 yr2=[yr2 (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii];, end;
x=[xr1;xr2;xr3];
y=[yr1;yr2;yr3];

%Displaying patch objects
h=patch(x,y,c,'CDataMapping','scaled');
tilename=['TILE ' num2str(tile)];
if nargin < 4,titleuse=tilename;,else,titleuse=[titlestr ' ' tilename];end
title(titleuse,'FontSize',14,'FontName',font,'FontWeight','bold');
caxis([cmin cmax]);
axis ([0 8 0 8]); axis square; 
cb=colorbar('XAxisLocation','top');
xlabel('det col');
ylabel('det row');
set(gca,'XTick',0.5:7.5,'XTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YTick',0.5:7.5,'YTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YDir','reverse','XAxisLocation','top','TickLength',[.001 .001]);
if nargin > 4, set(get(cb,'XLabel'),'String',datatype),end;
text(.25,.75,'A');
text(.6,.3,'B');

for i=0:8,
plot([0:8],repmat(i,1,9),'k','LineWidth',2);
plot(repmat(i,1,9),[0:8],'k','LineWidth',2);
end







