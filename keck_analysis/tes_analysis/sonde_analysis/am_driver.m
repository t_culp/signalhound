function am_driver(amc_dir,out_dir)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20121112 GPT
%
% For each .amc file in amc_dir, generate an atmospheric model in am and
% save it in out_dir.
%
% Before calling, you should have a copy of the latest version of am in
% your home directory and the .am directory for the cache.
%
% export AM_CACHE_PATH=~/.am
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Where is the latest version of am to execute?
am='~/am-7.2/am';

% Set the frequency coverage in GHz.
frequency_low=0;
frequency_high=500;
frequency_step=0.1;

% We are centered near el=57.5 degrees for most CMB observations.
za=90-57.5;

% CMB monopole
T0=2.725;

amc_files=dir([amc_dir,'/*.amc']);
N=length(amc_files);
kk=0;

tic

for jj=1:N
    amc_file=amc_files(jj).name;
    out_file=[amc_file(1:(end-4)),'.out'];
    err_file=[amc_file(1:(end-4)),'.err'];
    dummy=dir(fullfile(out_dir,out_file));
    if ~exist(fullfile(out_dir,out_file),'file') || ~dummy.bytes
        kk=kk+1;
        disp(['Processing ',amc_file,' file ',num2str(kk),' of ',num2str(N),'...'])
        system([...
            am,' ',...
            fullfile(amc_dir,amc_file),' ',...
            num2str(frequency_low),' ',...
            num2str(frequency_high),' ',...
            num2str(frequency_step),' ',...
            num2str(za),' ',...
            num2str(T0),' ',...
            '> ',...
            fullfile(out_dir,out_file),' ',...
            '2> ',...
            fullfile(out_dir,err_file)...
            ]);
    end
end

disp([num2str(kk),' of ',num2str(N),' models processed in ',num2str(toc),' seconds.'])