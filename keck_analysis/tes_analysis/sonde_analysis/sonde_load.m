function sonde_out=sonde_load(sonde_file)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20111001 GPT
%
% Grab and reduce data from a sonde file.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Input:
%
%  sonde_file = name of a file containing sonde data
%   The sonde files are usually of the form 'spYYYYMMDDHH.l100' or similar.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Output:
%
%  sonde_out = structure containing reduced data
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist(sonde_file,'file');
    error('sld:fil','Sorry, I can''t find the file.');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Import the sonde.  The first 17 lines of a sonde file are text.
sonde=importdata(sonde_file,' ',17);

% There's a slight format change starting from sp2012010321.l100 that adds
% an extra line of header.
if ~isstruct(sonde)
    sonde=importdata(sonde_file,' ',18);
end

% The second column is the pressure in hPa.
P=sonde.data(:,2);
if min(P)>10
    if min(P)>=15
        % Interrupt if there isn't enough data.
        sonde_out='I don''t have enough data.';
        return
    else
        % Otherwise pretend the next data point is at 10 hPa.
        sonde.data(end,2)=10;
    end
end

% The fifth column is the temperature in degrees Celsius.
T=273.15+sonde.data(:,5);

% The sixth column is the frost point in degrees Celsius.
frost_point=273.15+sonde.data(:,6);

% The seventh column is the relative humidity in percent.
%RH=sonde.data(:,7);

% The ninth column is the ozone volume mixing ratio in ppmv.
ozone_vmr=(1e-6)*sonde.data(:,9);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get approximate values for the equilibrium vapor pressure in hPa until
% the hygrometer fails.  We use the Goff–Gratch equation.
evp=goff_gratch(frost_point);

% The volume mixing ratio is the number density of water divided by the
% total number density.
% n_water=evp./(k_B*frost_point);
% n_total=P./(k_B*T);
% water_vmr=n_water./n_total;
water_vmr=(evp./frost_point)./(P./T);

% Find the last valid hygrometer datum.
hf=find(water_vmr,1,'last');
if hf<30
    % Interrupt if there isn't enough data.
    sonde_out='I can''t figure out the high altitude water content.';
    return
end

% Fit a power law to the tail of the data.
tail_data=((hf-10):hf)';
p=polyfit(log(P(tail_data)),log(water_vmr(tail_data)),1);
% Make sure the power law is at least somewhat strong.
k=max([p(1),3.5]);
water_vmr(hf:end)=water_vmr(hf).*(P(hf:end)./P(hf)).^k;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Figure out the month of the year.
sonde_out.header=sonde.textdata;
sonde_out.month=get_sonde_month(sonde.textdata{10,1});

% Sample more sparsely.
sonde_out.P=(10:10:max(P))';
% Occasionally two pressure data points are equal in the low pressure tail.
% Perturb the higher altitude point down to allow interpolation
if numel(P)~=numel(unique(P))
    P(find(~diff(P))+1)=P(find(~diff(P))+1)-0.0001;
end
sonde_out.T=interp1(P,T,sonde_out.P,'linear','extrap');
sonde_out.ozone_vmr=interp1(P,ozone_vmr,sonde_out.P,'linear','extrap');
sonde_out.water_vmr=interp1(P,water_vmr,sonde_out.P,'linear','extrap');

% The third column is geopotential altitude in km.
% The thirteenth column is the residual DU.
% Comment out unless debugging.
% altitude=sonde.data(:,3);
% sonde_out.altitude=interp1(P,altitude,sonde_out.P,'linear');
% sonde_out.dobson=sonde.data(1,13);

% Round down the highest altitude water data.
sonde_out.water_vmr(sonde_out.water_vmr<1e-8)=0;