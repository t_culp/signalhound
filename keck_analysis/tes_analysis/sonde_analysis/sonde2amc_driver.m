function sonde2amc_driver(sonde_dir,amc_dir)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20121112 GPT
%
% Convert all .l100 files in sonde_dir to .amc files in amc_dir.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sonde_files=dir([sonde_dir,'/*.l100']);

for jj=1:length(sonde_files)
    sonde_file=sonde_files(jj).name;
    amc_file=[sonde_file(1:(end-5)),'.amc'];
    if ~exist(fullfile(amc_dir,amc_file),'file')
        sonde=sonde_load(fullfile(sonde_dir,sonde_file));
        if isstruct(sonde)
            sonde2amc(sonde,fullfile(amc_dir,amc_file));
        elseif ischar(sonde)
            disp([sonde_file,' message: ',sonde])
        else
            error('sad:sld','Is the sonde file corrupt? Sorry.');
        end
    end
end