function month=get_sonde_month(sonde_string)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20111002 GPT
%
% Figure out the month of a sonde file using its contents, not its name.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  sonde_string = usually sonde.textdata{10,1}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

a=false(12,1);

a(1)=~isempty(strfind(sonde_string,'January'));
a(2)=~isempty(strfind(sonde_string,'February'));
a(3)=~isempty(strfind(sonde_string,'March'));
a(4)=~isempty(strfind(sonde_string,'April'));
a(5)=~isempty(strfind(sonde_string,'May'));
a(6)=~isempty(strfind(sonde_string,'June'));
a(7)=~isempty(strfind(sonde_string,'July'));
a(8)=~isempty(strfind(sonde_string,'August'));
a(9)=~isempty(strfind(sonde_string,'September'));
a(10)=~isempty(strfind(sonde_string,'October'));
a(11)=~isempty(strfind(sonde_string,'November'));
a(12)=~isempty(strfind(sonde_string,'December'));

month=find(a);