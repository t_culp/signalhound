function evp=goff_gratch(frost_point)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20111002 - GPT
%
% Estimate the equilibrium vapor pressure given a frost point.
%
% The sonde data lists frost point.  Most likely the balloons are equipped
% with a chilled mirror hygrometer.  The mirror is basically cooled down
% until frost starts to condense on it.  The temperature of the mirror at
% this phase transition is the frost point.
%
% Several formulas are available for converting from frost point to evp.
% They all seem to agree with one another to within experimental
% uncertainty over our temperature range of interest, so we'll just use the
% classic Goff–Gratch equation.  We do not take any "enhancement factor"
% into account.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Input:
%
%  frost_point = temperature in K
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Output:
%
%  evp = equilibrium vapor pressure in hPa
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

T0=273.16; % triple point in K
e0=6.1071; % evp at the triple point in hPa

evp=10.^(...
    -9.09718*((T0./frost_point)-1)...
    -3.56654*log10(T0./frost_point)...
    +0.876793*(1-(frost_point/T0))...
    +log10(e0)...
    );

% Drop nonsense values to zero.
evp(evp>100)=0;