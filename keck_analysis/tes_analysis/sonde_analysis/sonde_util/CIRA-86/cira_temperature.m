function T_0=cira_temperature(T_10, month)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20110930 GPT
%
% Get an effective temperature at "zero pressure" from the CIRA-86 table.
% The goal is to have am interpolate reasonably to 5 hPa.
%
% This function simply uses the CIRA-86 values at 5.316 hPa at the location
% of 80 degrees south and assumes these are close enough.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  T_10 = temperature at ~10 hPa
%
%  month = e.g. 1, 'jan', or 'january'
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  T_0 = effective temperature at 0 hPa
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch month
    case {1,'jan','january','Jan','January'}
        T_5=267.6;
    case {2,'feb','february','Feb','February'}
        T_5=257.5;
    case {3,'mar','march','Mar','March'}
        T_5=235.0;
    case {4,'apr','april','Apr','April'}
        T_5=215.8;
    case {5,'may','May'}
        T_5=211.3;
    case {6,'jun','june','Jun','June'}
        T_5=206.2;
    case {7,'jul','july','Jul','July'}
        T_5=213.7;
    case {8,'aug','august','Aug','August'}
        T_5=225.8;
    case {9,'sep','september','Sep','September'}
        T_5=250.9;
    case {10,'oct','october','Oct','October'}
        T_5=264.5;
    case {11,'nov','november','Nov','November'}
        T_5=266.4;
    case {12,'dec','december','Dec','December'}
        T_5=269.1;
    otherwise
        error('cir:mth','invalid month');
end

% Make sure T_5 is at least a little bit colder than T_10.

T_5=min([T_5,T_10-15]);

% Let T_5 be the average of T_0 and T_10, but round off so as not to
% pretend that this method is more accurate than it really is.

T_0=round(2*T_5-T_10);