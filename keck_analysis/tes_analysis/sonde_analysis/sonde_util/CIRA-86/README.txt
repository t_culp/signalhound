20110930 - GPT

I grabbed http://badc.nerc.ac.uk/browse/badc/cira/data/sht.lsn .
Below is a description from
http://badc.nerc.ac.uk/cgi-bin/data_browser/data_browser/badc/cira/doc/nssdc_help.txt .

We assume that South Pole data are similar to 80 degree south data.
The upper atmosphere at its very low pressure is unlikely to contribute
much to the model.  Suppose we know the temperature at ~10 hPa.  Since am
likes to assign midpoint values, we would like to assign an effective
"zero pressure" value such that it will give a reasonable temperature at
~5 hPa.  The Matlab function is basically a tool that gives the zero
pressure values so that the average comes out correctly.

=====

"The COSPAR International Reference Atmosphere (CIRA) provides empirical
models of atmospheric temperature and densities as recommended by the
Committee on Space Research (COSPAR). Since the early sixties different
editions of CIRA have been published. The CIRA Working Group meets bi-
annual during the COSPAR General Assemblies. CIRA-86 is described in 
Advances in Space Research, Volume 8, Numbers 5-6, 1988; a more extensive 
report will be published as MAP Handbook. In the thermosphere (above 
about 100 km) CIRA-86 is identical with the MSIS model, which is also 
available from NSSDC (MI-91E)."
