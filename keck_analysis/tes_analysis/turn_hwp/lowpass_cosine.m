function [ filtered ] = lowpass_cosine( y,tau,f_3db, width )
%LOWPASS_COSINE Summary of this function goes here
%   Detailed explanation goes here

% say if we're padding the data with copies or not
we_are_padding = 1;

% kill the last data point if y has an odd length
if mod(length(y),2)
    y = y(1:(length(y) - 1));
end

% add the weird padd
if we_are_padding
    y = [ fliplr(y) y fliplr(y) ];
end

% take the FFT
ffty=fft(y);

% make the companion frequency array
delta = 1/(length(y)*tau);
nyquist = 1/(2*tau);
freq = 0:delta:(2*nyquist - delta);

% make the transfer function for the first half of the data
i_f_3db = find(freq >= f_3db,1,'first');
f_min = f_3db - (width/2);
i_f_min = find(freq >= f_min,1,'first');
f_max = f_3db + (width/2);
i_f_max = find(freq >= f_max,1,'first');

transfer_function(1:i_f_min) = 1;
transfer_function(i_f_min:i_f_max) = (1 + sin(-pi .* ((freq(i_f_min:i_f_max) - freq(i_f_3db))./width)))./2;
transfer_function(i_f_max:(length(freq)/2)) = 0;

% symmetrize this to be [1 .9 .8 ... .8 .9 1] to match the FFT
transfer_function = [transfer_function fliplr(transfer_function)];

% apply the filter, and invert the fft
filtered=real(ifft(ffty.*transfer_function));

% remove the pad
filtered = filtered((end/3):(2*(end/3) - 1));

end

