function [ musteps ] = send_turn(degrees, speed_steps, current, ring_gear_teeth, motor_id, port)
%turn_waveplate moves the waveplate to a given position relative to when you
%turned the motor power supply on

% check that current isn't too high
if current > 1.21 % A
    disp('Current is too high, aborting')
    return;
end

microsteps_per_step = 4;
% calculate speed in microsteps per second
speed_musteps = microsteps_per_step * speed_steps;

% calculate number of microsteps to turn
musteps = get_number_of_musteps( degrees, ring_gear_teeth );
    
%% Make a big command string so this stuff can all get sent at once
% set the number of microsteps per step
set_musteps = [motor_id 'P45S' num2str(microsteps_per_step)];

% set the hold current to zero
set_hold_current = [motor_id 'P40S' '0'];

% set the run current
% convert from amps to Phytron units
curr_phytron_units = round((current / 2.5) * 25);
set_current = [motor_id 'P41S' num2str(curr_phytron_units)];

% set the boost current
set_boost_current = [motor_id 'P42S' num2str(curr_phytron_units)];

% set the speed
set_speed = [motor_id 'P14S' num2str(speed_musteps)];

% set the turn
if musteps > 0
    set_move_length = [motor_id '+' num2str(musteps)];
else
    set_move_length = [motor_id '-' num2str(abs(musteps))];
end

% combine all of the command strings into a cell array
command_strings = {set_musteps, ...
                   set_hold_current, ...
                   set_current, ...
                   set_boost_current, ...
                   set_speed, ...
                   set_move_length };

% send all of the commands, one at a time
send_command_mcc2lin(port,command_strings);
end
