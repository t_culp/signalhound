function [ musteps ] = get_number_of_musteps( degrees, ring_gear_teeth )
%GET_NUMBER_OF_STEPS Summary of this function goes here
%   Detailed explanation goes here

microsteps_per_step = 4;

steps_per_rev = 200;

% calculate position to go to
musteps = round( (degrees/360) * ring_gear_teeth * steps_per_rev * microsteps_per_step);

end
