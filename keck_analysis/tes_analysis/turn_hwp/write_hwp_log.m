function write_hwp_log(path,fileName,out,degTurned)

logfile = [path '/hwp_run_log.csv'];

fp=fopen(logfile,'a+');


nm=length(out.motor);
for j=1:nm
    %file, motor index, speed, current, degree req, degree single, degrees double

    if out.motor(j).degrees % write everything if we turned
        fprintf(fp,'%s, %s, %d, %d, %g, %g, %g\n',fileName,out.motor(j).motor_name,...
            out.motor(j).speed,out.motor(j).current,...
            out.motor(j).degrees,degTurned(j).single,degTurned(j).double);
    else % otherwise just write that it didn't happen
        fprintf(fp,'%s, %s, %d, %d, %g\n',fileName,out.motor(j).motor_name,...
            out.motor(j).speed,out.motor(j).current,...
            out.motor(j).degrees);
    end
end

fclose(fp);

end