\documentclass[letterpaper,12pt]{revtex4}

\usepackage{fullpage}
\usepackage{setspace}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{verbatim}

\begin{document}

\singlespacing

\title{Matlab Code for HWP Control in the Lab with a Windows PC}
\author{Sean Bryan}
 \affiliation{Case Western Reserve University}
 
\maketitle

\lstset{language=Matlab}

\section{Filenames}

\verb#turn_waveplate_wrapper#: Calling this matlab function actually turns the waveplate. Editing this file lets you edit COM ports and output data filenames.

\verb#turn_waveplate#: This is a mid-level function that delegates to the other subroutines.

\verb#send_turn, send_command_mcc2lin, get_number_of_musteps,#

\verb#usb6009_init_module, find_peaks, plot_hwp_moves, write_hwp_log,#

\verb#lowpass_cosine#: Low-level subroutines

\section{Initial Configuration}

All of the configuration should be within \verb#turn_waveplate_wrapper#. Give the motor(s) a name on line 5. Make a cell array of strings, one cell array element for each motor. Since we're using a USB-6009 to sample the encoders, currently the maximum number of HWPs is 2. Supporting 6 or whatever would only require re-writing the three lines of code in \verb#usb6009_init_module#.

Set the MCC2LIN output numbers on line 6, again by making a cell array of strings. For example, the first output of MCC2LIN device 0 would be to put the string ``OX'' on line 6.

Put a run note on line 8. All of this metadata will be saved in the output data struct, so you can look at it later in analysis.

You need to figure out the COM port that the MCC2LIN driver is connected to. This can change if you're using a USB-to-serial converter, unfortunately. Right-click on ``My Computer,'' go to ``Properties,'' go to the ``Hardware'' tab, go to ``Devices,'' and scroll down to ``Ports.'' If necessary, unplug and re-plug the USB-to-serial to see which COM port it is. Once you have the COM port, it goes on line 9 of \verb#turn_waveplate_wrapper#.

You need to figure out the Dev number of the USB-6009 device. Use the Measurement and Automation Explorer program to list all of the NI-DAQ devices. If necessary, unplug and re-plug the USB-6009 to see which Dev number it is. Put the Dev number on line 10 of \verb#turn_waveplate_wrapper#.

Put the name of a folder on line 11. This is where encoder data files will be created every time you turn the HWP. If you want, set the encoder sample time on like 12. If you don't set it, the code will calculate an overestimated guess on how long to record encoder data. Set the sample frequency on like 13.

Set the speed in full steps per second on line 16. 200 steps per second = 1 motor revolution per second. Each motor revolution is 1/465 of a HWP revolution. Basically, 500 steps per second = 2 HWP degrees per second. Set the drive current in Amperes on line 17. Stay at 1 A or below but 0.8 A is totally fine. Running below 0.3 A most likely won't turn the waveplate.

\section{Running}

To turn the first waveplate 22.5 degrees, type \verb#turn_waveplate_wrapper(22.5)# into the Matlab prompt. If you have two HWPs under control, to turn the first one 10 degrees and the second one 30 degrees, type \verb#turn_waveplate_wrapper([10 30])# into the Matlab prompt. To turn only the second waveplate 22.5 degrees, type \verb#turn_waveplate_wrapper([0 22.5])# into the Matlab prompt. 

\section{Results}

After the turn, you'll get some plots of the encoder timestreams on the screen. A copy of those plots gets saved in the output directory. Also, the raw data for each turn gets saved as a Matlab structure in a file in the output directory.

\subsection{Raw Data}
In the output directory, there will be lots of files each named with the date and time the data was taken. These file each contain a Matlab struct called \verb#out#.

Looking at one in Matlab gives:
\begin{verbatim}
>> out

out = 

          abstime: [2011 4 4 16 21 23.8065]
             time: [26230x1 double]
             port: 'COM22'
    nidaq_dev_num: 'Dev15'
      sample_rate: 610
             note: 'cold'
            motor: [1x2 struct]
\end{verbatim}

The elements are basically the same as the settings from the config section. The sub-struct \verb#out.motor# is a struct array, with one element for each HWP under control.

\pagebreak
Looking at \verb#out.motor(1)# shows the encoder data for the first HWP.

\begin{verbatim}
>> out.motor(1)

ans = 

       half_double: [26230x1 double]
               abs: [26230x1 double]
       half_single: [26230x1 double]
             shaft: [26230x1 double]
    analog_channel: 1
        motor_name: 'Keck'
     motor_channel: '0X'
           degrees: 22.5000
             speed: 500
           current: 0.8000
\end{verbatim}
The data vectors contain the encoder timestreams in Volts, sampled at $f_{sample}~=~\verb#out.sample_rate#$~[Hz]. The data vector \verb#out.time# is a companion time vector in seconds. The \verb#half_double# and \verb#half_single# timestreams are the continuous ticks on the HWP ring. Each tick is a half-degree apart, and the two are phased roughly in quadrature with each other. The \verb#abs# timestream is the absolute encoder that has unique location codes every 22.5 degrees. This is in phase with the \verb#half_double# encoder. The \verb#shaft# timestream is for the shaft encoder. This has 20 ticks for every motor revolution, and may be sloppy at the .05 degree level relative to the other encoders.

\subsection{Plots}
Because disk space is cheap, the program also logs a plot of each turn into the \verb#plots# subdirectory in the output directory. Figure~\ref{plot} shows what one looks like. All four encoder timestreams are shown, and the ticks as found by a simple tickfinder are shown. A full turn reconstruction algorithm, including the absolute encoder, is in development. Currently I have a nice system that lets you try some reconstructed turn parameters by hand and see if it's a good fit. That works great, so it's clear that getting to significantly better than a tenth of a degree is possible. I also have an automatic algorithm that gets you to within a half-degree or so, but getting that last bit of precision automatically is in progress. This code is not included in this folder.

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{04-Apr-2011-16-21-23Keck.pdf}
\caption{Plot of encoder timestreams displayed on the screen and also saved in the plots subdirectory of the output directory. The red circles show the encoder ticks as detected by a simple tickfinder algorithm. A full turn reconstruction algorithm, including the absolute encoder, is under development. \label{plot}}
\end{center}
\end{figure}

\subsection{Run log}
Opening the file \verb#hwp_run_log.csv# in Excel will show list of all of the turns that have been logged into that directory.


\end{document}