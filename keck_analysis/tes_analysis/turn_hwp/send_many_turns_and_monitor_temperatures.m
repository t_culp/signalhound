clear

% choose port for temperature samples
temp_port = 'COM8';
% choose how many turns to do
n_turns = 15000; % 150 = 1 day with a ~10 minute delay
% choose how long to wait between turns
delay = 8*60; % seconds
              % should work out to 10 minutes total delay including the turn_hwp code
% choose how many degrees to turn each hwp
angles = [22.5 22.5]; % degrees

% make the output filename for the temperature data
% example: 16-May-2007-17-41-18.dat is a data set that started on
%          May 16 2007 at 5:41:18 PM
filename = datestr(now);
filename = regexprep(filename, ' ', '-');
filename = regexprep(filename, ':', '-');
filename = ['thermometer_runs/' filename '.dat'];
temp_filename = filename;
clear filename

% start the turns
for i = 1:n_turns
    % send the turn
    turn_waveplate_wrapper(angles);
    
    % wait
    pause(10)
    
    % figure out how many more ten-second intervals there are until the next turn
    n_10_seconds = floor(delay / 10); % round down since we already had a 10-second delay
    
    % take a temperature sample every 10 seconds
    for j = 1:n_10_seconds
        write_a_temperature_sample(temp_port, temp_filename);
        pause(4); % results in a total time between samples of 10 seconds...roughly
    end
        
    
end

% keep taking temperature data afterwards until ctrl-c happens
while 1
    write_a_temperature_sample(temp_port, temp_filename);
    pause(4); % results in a total time between samples of 10 seconds...roughly
end