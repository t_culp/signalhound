function write_a_temperature_sample(port, filename)

% make serial port object
obj = serial(port,'BaudRate',9600, 'DataBits', 7, 'StopBits', 1, 'Parity', 'odd', 'Terminator', 'LF');
% open port
fopen(obj);

pause(0.5)
% write string to port asking for the temperatures
fprintf(obj,'KRDG?');
pause(0.5)

%Print returned string
temps_string = fscanf(obj);
pause(0.5)

% close the port
fclose(obj);

% delete the serial port object
delete(obj)
clear obj

% append the current time
data = [now str2num(temps_string)];

% write out the temperatures to the file
save(filename, 'data', '-ascii', '-append', '-double');

end


