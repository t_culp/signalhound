function s = send_command_mcc2lin( port, command )
%SEND_SERIAL Sends "command" string to serial "port"
% a typical command looks like <STX>1GR1000:XX<ETX>

% also defined for cell arrays of command strings

% define the start and stop bits
start = char(02); % STX
stop = char(03); % ETX

% make serial port object
%115200 is the old baudrate
obj = serial(port,'BaudRate',9600, 'DataBits', 8, 'StopBits', 1, 'Parity', 'none', 'Terminator', stop,'Timeout',10);

%% Do the commanding in a try-catch. Three tries and you're out.
done_yet = 0;
try_counter = 0;
while not(done_yet)
    try
        try_counter = try_counter + 1;
        % open port
        fopen(obj);

        % see if this is a cell array of many commands, or just a command string
        if iscell(command)
            for i = 1:length(command)
                % append the start to the command
                this_command = [start char(command{i})];
                disp(this_command)

                % write string to port
                %fprintf(1,'com %c\n',this_command);
                %a=length(
                
                fprintf(obj,this_command);

                % pause a bit
                pause(0.1)
            end
        else
            % append the start to the command
            command = [start command];
            disp(command)

            % write string to port
            %fprintf(1,'%s\n',command);
            fprintf(obj,command);

            % pause a bit
            pause(0.1)
        end

        % close the port
        fclose(obj);
        done_yet = 1;
    catch
        disp(['=== Command Error...try number ' num2str(try_counter) ' of 3 ==='])
        if try_counter >= 3
            done_yet = 1;
        else
            disp('=== Retrying soon ===')
            pause(2*pi)
            disp('=== Retrying... ===')
            done_yet = 0;
        end
    end
end

% delete the variable
delete(obj)
clear obj

end