clear

% choose port for temperature samples
temp_port = 'COM8';

% make the output filename for the temperature data
% example: 16-May-2007-17-41-18.dat is a data set that started on
%          May 16 2007 at 5:41:18 PM
filename = datestr(now);
filename = regexprep(filename, ' ', '-');
filename = regexprep(filename, ':', '-');
filename = ['thermometer_runs/' filename '.dat'];
temp_filename = filename;
clear filename

% keep taking temperature data afterwards until ctrl-c happens
while 1
    write_a_temperature_sample(temp_port, temp_filename);
    pause(4);
end