function degTurned=plot_hwp_moves(filepath, fileName,out)

curfig = 1;
nc=length(out.motor);

for j=1:nc
    if out.motor(j).degrees % if there was an actual turn
        chname=['CH' num2str(out.motor(j).analog_channel)];
        motorname=out.motor(j).motor_name;
        motorID = out.motor(j).motor_channel;

        titleString=[fileName ' ' motorname ' ' motorID ' ' chname];


        halfDouble=out.motor(j).half_double;
        halfSingle=out.motor(j).half_single;
        absEnc=out.motor(j).abs;
        shaftEnc=out.motor(j).shaft;

        degTarget=out.motor(j).degrees;

        time=out.time;

        % get half degree ticks
        n_down = floor(out.sample_rate/150);
        [idh] = find_peaks(time,halfDouble(1:n_down:end));
        idh = idh.*n_down;
        [ish] = find_peaks(time,halfSingle(1:n_down:end));
        ish = ish.*n_down

        degTurned(j).single=length(ish)/4;
        degTurned(j).double=length(idh)/4;

        fprintf(1,'\n%s, %s\n',motorname,fileName);
        fprintf(1,'Degrees Requested %d\n',degTarget);
        fprintf(1,'Degrees counted: Single Enc %f, Double Enc %f\n\n',length(ish)/4,length(idh)/4);

        x_start = 100;
        x_end = 1100;
        y_start = 100;
        y_end = 600;
        fig_pos_arr = [x_start y_start x_end y_end];
        h=figure(curfig);
        clf;
        set(curfig,'Position',fig_pos_arr);


        subplot(4,1,1)
        plot(time,shaftEnc);
        title([titleString ' Shaft Enc']);

        subplot(4,1,2)
        plot(time,halfDouble);
        title([titleString ' Double Enc, 1/2 deg']);
        hold on;
        scatter(time(idh),halfDouble(idh),'r');
        hold off;

        subplot(4,1,3)
        plot(time,halfSingle);
        title([titleString ' Single Enc, 1/2 deg']);
        hold on;
        scatter(time(ish),halfSingle(ish),'r');
        hold off;


        subplot(4,1,4)
        plot(time,absEnc);
        title([titleString ' Abs Enc, [V]']);
        xlabel('Time [s]')

        [s,mess,messid] = mkdir(filepath,'plots');
        saveas(h,[filepath '/plots/' fileName motorname '.fig']);

        curfig=curfig+1;
    end
end
end