function out = turn_waveplate(in)
%

%% Parse the input struct
if isfield(in, 'path')
    filepath=in.path;
else
    filepath='../HWP_RunData/';
end
[s,mess,messid] = mkdir(filepath);

if isfield(in, 'sample_time')
    sample_time = in.sample_time;
else
    % estimate the time to turn
    for i = 1:length(in.motor)
       est_duration(i) = get_number_of_musteps(in.motor(i).degrees, in.motor(i).ring_gear_teeth) / (in.motor(i).speed * 4); % for 4 microsteps per step 
    end
    
    sample_time = round(max(abs(est_duration))*1.1 + 30);  
end
disp(['Taking encoder data for ' num2str(sample_time) ' seconds.']);

if isfield(in,'f_sample')
    f_sample = in.f_sample;
else
    f_sample = 610; % Hz
end

%% Create a filename to save the data
% example: 16-May-2007-17-41-18.dat is a data set that started on
%          May 16 2007 at 5:41:18 PM
filename = datestr(now);
filename = regexprep(filename, ' ', '-');
filename = regexprep(filename, ':', '-');

%% Set up Analog Input
try
    usb6009_init_module
catch
    fprintf(1,'Daq init error, power cycle NIDAQ\n');
    return
end

% set up sampling
try
    set(AI,'SampleRate',f_sample)
    set(AI,'SamplesPerTrigger',round(sample_time*f_sample))
    set(AI,'TriggerType','Manual')
catch
    fprintf(1,'Daq configuration error, power cycle NIDAQ\n');
    return
end

%% Start taking encoder data
disp('Starting analog input...')
try
    start(AI)
    trigger(AI)
catch
    fprintf(1,'Daq error, power cycle NIDAQ\n');
    return
end
% let the daq get going...
pause(1)

%% Move HWP
for i = 1:length(in.motor)
    if in.motor(i).degrees % if we're asking for a non-zero turn
        send_turn(in.motor(i).degrees, in.motor(i).speed, in.motor(i).current, in.motor(i).ring_gear_teeth, in.motor(i).motor_channel, in.port);
    end
    pause(3);
end

%% Stop taking encoder data
try
    wait(AI,sample_time*1.1 + 3)
    % get the data
    [data,time,abstime] = getdata(AI); % if we're logging to memory
    delete(AI)
    clear AI
catch
    fprintf(1,'Daq error, data could not be read, power cycle NIDAQ\n');
    delete(AI)
    clear AI    
end

%% Save encoder data
% make the output struct
out.abstime = abstime;
out.time = time;
out.port=in.port;
out.nidaq_dev_num=in.nidaq_dev_num;
out.sample_rate = in.f_sample;
out.note=in.note

% put analog data in the struct
for i = 1:length(in.motor)
    out.motor(i).half_double = data(:,1 + (i-1)*4);
    out.motor(i).abs         = data(:,2 + (i-1)*4);
    out.motor(i).half_single = data(:,3 + (i-1)*4);
    out.motor(i).shaft       = data(:,4 + (i-1)*4);
    out.motor(i).analog_channel = i;
    out.motor(i).motor_name = in.motor(i).motor_name;
    out.motor(i).motor_channel = in.motor(i).motor_channel;
    out.motor(i).degrees = in.motor(i).degrees;
    out.motor(i).speed = in.motor(i).speed;
    out.motor(i).current = in.motor(i).current;
    out.motor(i).ring_gear_teeth = in.motor(i).ring_gear_teeth;
end

fprintf(1,'output file: %s\n',filename);
save([filepath '/' filename '.mat'],'out');

% make plots
degrees_turned = plot_hwp_moves(filepath,filename,out);

% write a logfile about this turn
write_hwp_log(filepath,filename,out,degrees_turned);

end
