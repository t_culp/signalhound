function turn_waveplate_wrapper(degrees)
% ex: to not turn motor1, and to turn motor2 -22.5 degrees,
%     type turn_waveplate_wrapper([0 -22.5])

motor_names    = {'Spider1'};
motor_channels = {'0X'};

in.note='Warm Spider Test Cryostat Box Test';
in.port='COM22';
in.nidaq_dev_num='Dev13';
in.path='../HWP_RunData/apr_18_2011'
%in.sample_time=30; % s
in.f_sample = 610; % hz


speed=500; %500 is nominal speed
current=0.8; % 0.8 is nominal current

ring_gear_teeth = [465];
%S1: ???
%S2: 464
%S3,S4, and S6: 463
%S5: 462
%S7: 465

for i=1:length(degrees)
    in.motor(i).degrees=degrees(i);
    in.motor(i).speed=speed;
    in.motor(i).current=current;
    in.motor(i).ring_gear_teeth = ring_gear_teeth(i);
    in.motor(i).number=char(motor_channels{i});
    in.motor(i).motor_name=char(motor_names{i});
    in.motor(i).motor_channel=char(motor_channels{i});
    in.motor(i).analog_channel=i;
end

out = turn_waveplate(in);

end