function [peak_indices] = find_peaks(time,encdata)
% this returns the peak indices/times in the timestream plus the points just before the
% wave starts and after it stops.
% motor: 1 or 2
% enc: shaft or ring_a or ring_b
% threshold is the only parameter this program needs to determine if the
% rotor is turning in a given section.  It should be smaller than the
% amplitude of the waveform's derivative


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% do a lowpass filter

%y0 is used as the data stream

doplot=0;

y0=encdata;
t0=time;


musteps_per_step = 2;
speed_1 = 500; % maybe? maybe way off?
f_sample = 2500; % Hz
% peak_ring_enc_freq_motor_1 = (speed_1/musteps_per_step)/(200); % rough estimate...
peak_ring_enc_freq_motor_1 = 4*2*pi;

y0_filtered = lowpass_cosine(y0',1/f_sample,...
    3*peak_ring_enc_freq_motor_1,2)';
%% get the derivative of the filtered timestream
dy0 = diff(y0_filtered);

% disp(['The rms of the derivative of the filtered wave is ' num2str(std(dy0))]);

%%%%%%%%%%THRESHOLD%%%%%%%%%%%%%%
% threshold = 0.25/100;
threshold = std(dy0)/3; % still need a good way of choosing this

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % plot it
%figure(1)
% plot(t0, y0_filtered)%, t0(1:length(t0)-1), 100*dy0);

%% make a list of the times where it crosses 0
if dy0(1) > 0
    toggle = 1;
else
    toggle = -1;
end

% initialize zero_indices : a vector containing all the places where the
% derivative is 0
zero_indices_big = zeros(length(dy0),1);
count = 1;
for i=30:length(dy0)-30 % ignore the edges due to ringing
    % is the new point + or -?
    if dy0(i) > 0
        newtoggle = 1;
    else
        newtoggle = -1;
    end
    
    % if newtoggle is different from toggle
    if (toggle ~= newtoggle)
        % record it
        zero_indices_big(count) = i;
        % update the toggle
        toggle = newtoggle;
        count = count+1;
    end
end

% shrink the array
zero_indices = zero_indices_big(1:count-1);

%% filter out the points where the derivative didn't change much in between the two zeros
peak_indices_big = zeros(count-1,1);
count = 1;

for i = 2:length(zero_indices)
    index_between = zero_indices(i-1) + round((zero_indices(i) - zero_indices(i-1))/2);
    
    % test that the change in the derivative is above the threshold
    if abs(dy0(index_between)) > threshold
        % if it is, add both points to the list
        peak_indices_big(count) = zero_indices(i-1);
        peak_indices_big(count+1) = zero_indices(i);
        count = count + 2;
    end
end

% this records lots of points twice. filtering them out:
peak_indices_smaller = zeros(count-1,1);
peak_indices_smaller(1) = peak_indices_big(1);
count = 2;
for i = 2:length(peak_indices_big)
    if (peak_indices_smaller(count-1) < peak_indices_big(i))
        peak_indices_smaller(count) = peak_indices_big(i);
        count = count + 1;
    end
end

% shrink it
peak_indices = peak_indices_smaller(1:count - 1);
% peak_indices = peak_indices_big(1:count - 1);

% make a list of the peak times, too
peak_times = zeros(count-1,1);
for i = 1:length(peak_indices)
    peak_times(i) = t0(peak_indices(i));
end
%% plot the peaks

if doplot==1
% plot the wave
figure(1)
clf
plot(t0,y0);

hold on
for i = 1:length(peak_times)
    scatter(peak_times(i), y0(peak_indices(i)))
end
hold off

end
%disp(['The number of peaks is ' num2str(length(peak_indices))])
end