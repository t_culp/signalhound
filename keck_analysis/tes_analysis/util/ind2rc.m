function [row, col] = ind2rc(index)
% Utility function to convert frame indices to rows and columns
% Assumes:
%   - 33 rows in a column, numbered 0-32
%   - columns are numbered from 0
%
% 090115 JPF
row = mod(index-1,33);
col = floor((index-1)./33);