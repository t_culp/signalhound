function is_bad = is_bad_default(mux_row,mux_col)
% ART

if nargin==0
    is_bad = is_bad_Spider_Run4pt0();
    display('Using Run 4.0 bad detector mask...');
elseif nargin==2
    is_bad = is_bad_Spider_Run4pt0(mux_row,mux_col);
else
    error('nargin should be 0 or 2');
end;
