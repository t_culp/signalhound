function log=parse_pxpollog(days)
% input days should be of the form:
% days=['20101120';'20101121';'20101122';'20101123'];

[mm nn]=size(days);
B=[];
for ii=1:mm
  log=dir(['log/' days(ii,:) '*']);
  [A b]=system(['grep pixel: log/' log.name]);
  if strcmp(days(ii,:),'20101120')
    b=b(49:end);
  end
  B=[B b];
end

tmp = sscanf (B, '%f %f : %f : %f moving to next pixel: [ %f ]');
log.date=tmp(1:5:end)';
log.hr=tmp(2:5:end)';
log.min=tmp(3:5:end)';
log.s=tmp(4:5:end)';
log.px=tmp(5:5:end)';

for ii=1:length(log.px)-1
  log.ehr(ii)=log.hr(ii+1);
  log.emin(ii)=log.min(ii+1);
  log.es(ii)=log.s(ii+1);
  log.edate(ii)=log.date(ii+1);
  inst(ii)=sum(log.px(ii)==log.px(ii:end))-1;
end

[indd inst]=unique([log.px(:)],'last');

for ii=1:length(log.px)-1
  if log.px(ii)>log.px(ii+1)
    log.es(ii)=log.s(ii)+15; dm=0;
    if log.es(ii)>60, dm=1; log.es(ii)=log.es(ii)-60; end
    log.emin(ii)=log.min(ii)+dm+41; dh=0;
    if log.emin(ii)>60, dh=1; log.emin(ii)=log.emin(ii)-60; end
    log.ehr(ii)=log.hr(ii)+dh; dd=0;
    if log.ehr(ii)>24, dd=1; log.ehr(ii)=log.ehr(ii)-24; end
    log.edate(ii)=log.date(ii)+dd;
  end
end

mo = floor(log.date(1)/100)*100;
if mo==101100 || mo==101200 || mo==111100
  log.es(end+1)=log.s(end)+15; dm=0;
  if log.es(end)>60, dm=1; log.es(end)=log.es(end)-60; end
  log.emin(end+1)=log.min(end)+dm+41; dh=0;
  if log.emin(end)>60, dh=1; log.emin(end)=log.emin(end)-60; end
  log.ehr(end+1)=log.hr(end)+dh; dd=0;
  if log.ehr(end)>24, dd=1; log.ehr(end)=log.ehr(end)-24; end
  log.edate(end+1)=log.date(end)+dd;
end

% log.date=log.date(~logical(inst));
% log.edate=log.edate(~logical(inst));
% log.hr=log.hr(~logical(inst));
% log.ehr=log.ehr(~logical(inst));
% log.min=log.min(~logical(inst));
% log.emin=log.emin(~logical(inst));
% log.s=log.s(~logical(inst));
% log.es=log.es(~logical(inst));
% log.px=log.px(~logical(inst));

log.date=log.date(inst);
log.edate=log.edate(inst);
log.hr=log.hr(inst);
log.ehr=log.ehr(inst);
log.min=log.min(inst);
log.emin=log.emin(inst);
log.s=log.s(inst);
log.es=log.es(inst);
log.px=log.px(inst);

mo = floor(log.date(1)/100)*100;

for ii=1:length(log.s)
  if mo==101100
    logr.t1(ii)={['2010-nov-' num2str(log.date(ii)-mo,'%02.0f') ':' num2str(log.hr(ii),'%02.0f') ':' num2str(log.min(ii),'%02.0f') ':' num2str(log.s(ii),'%02.0f')]};
    logr.t2(ii)={['2010-nov-' num2str(log.edate(ii)-mo,'%02.0f') ':' num2str(log.ehr(ii),'%02.0f') ':' num2str(log.emin(ii),'%02.0f') ':' num2str(log.es(ii),'%02.0f')]};
  elseif mo==101200
    logr.t1(ii)={['2010-dec-' num2str(log.date(ii)-mo,'%02.0f') ':' num2str(log.hr(ii),'%02.0f') ':' num2str(log.min(ii),'%02.0f') ':' num2str(log.s(ii),'%02.0f')]};
    logr.t2(ii)={['2010-dec-' num2str(log.edate(ii)-mo,'%02.0f') ':' num2str(log.ehr(ii),'%02.0f') ':' num2str(log.emin(ii),'%02.0f') ':' num2str(log.es(ii),'%02.0f')]};       
  elseif mo==111100
    logr.t1(ii)={['2011-nov-' num2str(log.date(ii)-mo,'%02.0f') ':' num2str(log.hr(ii),'%02.0f') ':' num2str(log.min(ii),'%02.0f') ':' num2str(log.s(ii),'%02.0f')]};
    logr.t2(ii)={['2011-nov-' num2str(log.edate(ii)-mo,'%02.0f') ':' num2str(log.ehr(ii),'%02.0f') ':' num2str(log.emin(ii),'%02.0f') ':' num2str(log.es(ii),'%02.0f')]};       
  end
end

[p ind]=get_array_info();

logr.a=ind.e(ismember(p.gcp,log.px));
logr.b=ind.b(ismember(ind.a,logr.a));

log=logr;
