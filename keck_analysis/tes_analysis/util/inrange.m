function m = inrange (a, b, c)
% INRANGE (A, B, C)
%    returns 1 for B <= A <= C,
%            0 otherwise.
%

m = (b <= a) & (a <= c);
