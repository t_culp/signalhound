% SQUARE_DEMOD  Demodulate a square-wave signal
%   from a chopped source
%
% [C, S] = SQUARE_DEMOD (X, B)
%
%    X = modulated detector signal
%    B = chop signal (1 for source on, <=0 for source off)
%    C = demodulated cosine part
%    S = demodulated sine part
%
% C and S are returned at one value per chop cycle,
% effectively downsampled to the chope rate.  (See
% below for an option to expand the outputs to the 
% same length as the input.)
%
% [C, S, IC, IS] = SQARE_DEMOD(...) also returns IC
% and IS, the index at the middle of each chop cycle
% for cosine and sine demodulation.
%
% X may be a matrix of N rows by M columns, representing
% N samples in each of M channels.  In this case, the
% length of B must be N.  C and S will each have M
% columns.
%
% SQUARE_DEMOD (..., 'deglitch') removes glitches from
% the chop reference signal.  It is assumed that the
% true chop is a clean square wave with a fixed period,
% and any deviations from this are spurious.  The cleaned
% signal is also used to construct the sine-phase reference
% more precisely.
%
% [C, S] = SQUARE_DEMOD (..., 'expand') expands C and S
% to have the same number of samples as the input X.  This
% is done by repeating the demodulated values over a chop
% cycle.  It's also possible to interpolate using a spe-
% cified method, e.g. 'linear' or 'spline'.

% RWO 090804

function [c, s, ic, is] = square_demod (x, b, varargin)

% Parse extra options
DO_DEGLITCH = 0;
DO_INTERP = '';
if (nargin > 2)
	for (ii = 1:length(varargin))
		switch (lower (varargin{ii}))
			case 'deglitch', DO_DEGLITCH=1;
			case 'expand', DO_INTERP='nearest';
			case {'linear','spline','nearest'}, DO_INTERP = lower(varargin{ii});
			otherwise, error (['Unknown option ' varargin{ii}]);
		end;
	end;
end;

if nanmax(b)>1 || nanmin(b)<0
	error(['Chop reference b should be in range 0-1.']);
end

b = (b(:) >= 0.5);
if size(x,1)==1
        x = x(:);
end;

% Deglitch the chop reference, if requested
if (DO_DEGLITCH)
	b_old = b;
	b = (b - 0.5) *2;
	[p f] = time2psd (b, 1);
	p (1:4) = 0;
	p ((end-3):end) = 0;
	[pmax imax] = max (p);
	fchop = f (imax);
	chcos = cos (2*pi*fchop * (1:length(b))');
	chsin = sin (2*pi*fchop * (1:length(b))');
	[bflt aflt] = butter (2, 2*fchop/5, 'low');
	bcos = filtfilt (bflt, aflt, chcos .* b);
	bsin = filtfilt (bflt, aflt, chsin .* b);
	ph = atan2 (bsin, bcos);
	b = cos (2*pi*fchop * (1:length(b))' - ph) > 0;
	% keyboard;
        b90 = sin (2*pi*fchop * (1:length(b))' - ph) > 0;
end;

% A fudge is needed here.  We demodulate by dividing the chop
% reference into four phases.  Whenever a single isolated
% sample is high or low, this fails.  In this case, double the
% whole thing to be safe.  Ugly but effective and reasonably
% quick.
dd = diff (b);
if 0 && sum (dd~=0 & [0; dd(1:(end-1))]~=0) > 0
	disp ('Found some singletons in chop reference, performing x2 kludge.');
	[m n] = size (x);
	x = repmat (x(:)', 2, 1);
	x = x(:);
	x = reshape (x, 2*m, n);
	b = repmat (b(:)', 2, 1);
	b = b(:);
	[c s itmp] = square_demod (x, b);
	if (nargout > 2)
		i = round (itmp/2);
	end;
	return;
end;

% Construct b90, 90 degrees out of phase with b.
% Only do this if we don't already have b90 from 'deglitched' pure-tone chop reference.
if ~exist('b90','var') || isempty(b90)
	%disp (['Constructing b90']);
	% Construct b90, 90 degrees out of phase with b
	bdiff = ([0; diff(b)] + [diff(b); 0]) / 2;
	b90 = -1 * bdiff;
        % faster method
	jj = (1:length(b90))';
	b90(b90==0) = interp1(jj(b90~=0),b90(b90~=0),jj(b90==0)+1e-5*(rand(sum(b90==0),1)-0.5),'nearest','extrap');
	b90 = (b90(:) >= 0.5);
end

%disp (['Constructing phases']);
% Construct the four phases
cphase{1} = (~b & b90);
cphase{2} = (~b & ~b90);
cphase{3} = (b & ~b90);
cphase{4} = (b & b90);

% Find point just before zero phase and mid-phase
clastsamp = false (size(b));
clastsamp(end) = true;
bnext = [b(2:end); 0];
czerophase = (b & ~bnext);
cmidphase = (~b & bnext);
b90next = [b90(2:end); 0];
cquartphase = (b90 & ~b90next & ~clastsamp);
cthreephase = (~b90 & b90next & ~clastsamp);

%disp (['Normalizing']);
% Normalize the four phases in each cycle
normphase = zeros (size (b));
for (iph = 1:4)
	%disp (['Phase ' num2str(iph)]);
	clast = [0; cphase{iph}(1:(end-1))];
	cnext = [cphase{iph}(2:end); 0];
	c0 = (cphase{iph} & ~clast);
	c9 = (cphase{iph} & ~cnext);
	n = zeros (size (c0));
	n (c0) = find(c9) - find(c0) + 1;
	while sum (n == 0 & cphase{iph}) > 0
		nlast = [0; n(1:(end-1))];
		n (n == 0 & cphase{iph}) = nlast (n==0 & cphase{iph});
	end;
	normphase(cphase{iph}) = 1./n(cphase{iph});
end;

disp (['Applying']);
% Construct cos weightings
ycos = zeros (size (b));
ycos (cphase{1}) = -1;
ycos (cphase{2}) = -1;
ycos (cphase{3}) = +1;
ycos (cphase{4}) = +1;
ycos = ycos .* normphase;
% And sum up

c = zeros (sum(cquartphase), size(x,2));
for (ii = 1:size(x,2))
	csum = cumsum (ycos .* x(:,ii));
	c(:,ii) = diff ([0; csum(cquartphase)]) / 2;
end;

% Construct sin weightings
ysin = zeros (size (b));
ysin (cphase{1}) = +1;
ysin (cphase{2}) = -1;
ysin (cphase{3}) = -1;
ysin (cphase{4}) = +1;
ysin = ysin .* normphase;
% And sum up
s = zeros (sum(cmidphase), size(x,2));
for (ii = 1:size(x,2))
	ssum = cumsum (ysin .* x(:,ii));
	s(:,ii) = diff ([0; ssum(cmidphase)]) / 2;
end;

% Find sample indices if requested
if (nargout >= 3 || ~isempty(DO_INTERP))
	im = find (cmidphase);
	iz = find (czerophase);
	iq = find (cquartphase);
	i3 = find (cthreephase);
	ic=round((iq + [0; iq(1:(end-1))] + 1)/2 + 1e-5*(rand(size(iq))-0.5));
	is=round((im + [0; im(1:(end-1))] + 1)/2 + 1e-5*(rand(size(im))-0.5));
end

% Interpolate to expand output to full length, if requested
if ~isempty(DO_INTERP)
	ctmp = zeros (size (x));
	stmp = zeros (size (x));
	jj = 1:size(x,1);
	for i=1:size(x,2)
		ctmp(:,i) = interp1 (ic, c(:,i), jj + 1e-5*(rand(size(jj))-0.5), DO_INTERP, 'extrap');
		stmp(:,i) = interp1 (is, s(:,i), jj + 1e-5*(rand(size(jj))-0.5), DO_INTERP, 'extrap');
	end
	c = ctmp;
	s = stmp;
end

return

