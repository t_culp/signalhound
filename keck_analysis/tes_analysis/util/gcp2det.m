function [det_row det_col det_pixel tile] = gcp2det (gcp_chan, flag, mce_name)
% For BICEP2/Keck
%   GCP2DET  map from GCP channel number (0-527)
%   to mce coordinates (0-8 by 0-8 by 2 polarizations by 4 tiles)
%   GCP2DET(CHAN,'run4') uses the pre-run 5 channel mapping
%
% For BICEP3, specify all three arguments
% Input:
%   gcp_ch: 0, 1, ..., or 659 for each MCE in BICEP3 (30 columns 22 row)
%   flag: a string that denotes which mapping to use. (e.g., 'run5')
%   mce_name: 0, 1, 2, or 3 for BICEP3
% Output:
%   det_tile: Tile slot that the chosen detector resides in
%              (1, 2, ..., or 21 for BICEP3).
%              as displayed in the drawing below focal plane configuration
%              viewed from below to the sky. Note that this is tile slot,
%              and has nothing to do with the module number that actually
%              goes into the slot.
%               PT   1   2   3
%               4    5   6   7   8
%               9   10  11  12  13
%               14  15  16  17  18
%                   19  20  21
%   det_row: Row number within this tile (1, 2, ..., or 8 for BICEP3)
%   det_col: Col number within this tile (1, 2, ..., or 8 for BICEP3)
%   det_pixel: Polarization slot 'A' or 'B'
%
% RWO 090511
% JHK 141014 Update to be also used in BICEP3. Added mce_name argument.

% Experiment name (bicep2, keck, or bicep3)
expt=get_experiment_name;

switch expt
    case {'bicep2','keck'}
        % Comment from bicep2/keck era:
        % Note that nothing here depends on tile coordinates - there
        % should be no need to modify this function unless more readout
        % cards are used
        if (nargin < 2)
            flag = '';
        end;
        [mce_row mce_col] = gcp2mce (gcp_chan, flag);
        [det_row det_col det_pixel tile] = mce2det (mce_row, mce_col, flag);
    case 'bicep3'
        if (nargin < 3)
            error('Specify mce_name. Syntax: gcp2det(gcp_ch,flag,mce_name)')
        end
        switch flag
            % For each flag, read corresponding fp_data file
            case 'run4'
                % Read the fp_data file for BICEP3 run4
                [p,ind]=get_array_info(20140911);
            case 'run5'
                % Read the fp_data file for BICEP3 run5
                [p,ind]=get_array_info(20141001);
            otherwise
                error('Specify date for fp_data file to use')
        end
        % Look up coordinates in the fp_data table
        detidx=find(p.mce==mce_name & p.gcp==gcp_chan);
        if isempty(detidx)
          error(sprintf('In %s %s: No detector matches the gcp ch%s on mce%s',expt,flag,num2str(gcp_chan),num2str(mce_name)))
        elseif numel(detidx) > 1
          error(sprintf('In %s %s: More than 1 detector have the gcp ch%s on mce%s',expt,flag,num2str(gcp_chan),num2str(mce_name)))
        else
        tile=p.tile(detidx);
        det_row=p.det_row(detidx);
        det_col=p.det_col(detidx);
        det_pixel=p.pol(detidx);
        end
end


