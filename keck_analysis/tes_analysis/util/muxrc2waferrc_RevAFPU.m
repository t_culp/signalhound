function [row col AorB] = muxrc2waferrc(mux_row, mux_col)

col = mod(2*mux_col + (mux_row>16),8)+1;

switch mod(mux_row-1,16)+1
    case {1,2}
        row = 7;
    case {3,4}
        row = 5;
    case {5,6}
        row = 3;
    case {7,8}
        row = 1;
    case {9,10}
        row = 2;
    case {11,12}
        row = 4;
    case {13,14}
        row = 6;
    case {15,16}
        row = 8;
end

if mod(mux_row,2) == 1
    AorB = 'B';
else
    AorB = 'A';
end