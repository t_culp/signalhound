function [mr mc] = phys2mux(dr,dc,dp,dt,layout_fname)
% [mr mc] = phys2mux(dr,dc,dp,dt,layout_fname)
% Function takes physical (tile) coordinates and returns mux/mce
% coordinates.
% dr = detector row
% dc = detector column
% dp = detector polarization ('A' or 'B')
% dt = detector tile (1-4)
% layout_fname = name of file that contains the layout information for this
% particular type of tile (e.g. Standard6x6_DS0.tile)
% mr = mux row
% mc = mux column
% Originally written by Becky Tucker on 20110908
% Last updated by Becky Tucker on 20110909

% Open the layout file
fid = fopen(layout_fname);
TileLayoutFile = textscan(fid,'%d %d %s %d %d\n','CommentStyle','%');
% .... PhysRow,PhysCol,PhysPol,MuxRow,MuxCol ....
fclose(fid);

% Check which tile the pixel is in and use the corresponding row/col
if dt==1 %Tile 1
    ind = find(TileLayoutFile{1}==dr & TileLayoutFile{2}==dc ...
        & strcmp(TileLayoutFile{3},dp)==1);
    mr = TileLayoutFile{4}(ind);
    mc = TileLayoutFile{5}(ind);
elseif dt==2 %Tile 2
    ind = find(TileLayoutFile{1}==dr & TileLayoutFile{2}==dc ...
        & strcmp(TileLayoutFile{3},dp)==1);
    mr = TileLayoutFile{4}(ind);
    mc = TileLayoutFile{5}(ind)+4;
elseif dt==3 %Tile 3
    ind = find(TileLayoutFile{1}==dr & TileLayoutFile{2}==dc ...
        & strcmp(TileLayoutFile{3},dp)==1);
    mr = TileLayoutFile{4}(ind);
    mc = TileLayoutFile{5}(ind)+8;
elseif dt==4 %Tile 4
    ind = find(TileLayoutFile{1}==dr & TileLayoutFile{2}==dc ...
        & strcmp(TileLayoutFile{3},dp)==1);
    mr = TileLayoutFile{4}(ind);
    mc = TileLayoutFile{5}(ind)+12;
else
    dr = NaN;
    dc = NaN;
    dt = NaN;
    dp = NaN;
end
