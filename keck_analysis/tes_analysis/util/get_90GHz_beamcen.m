function [r, theta]= get_90GHz_beamcen()
  %90GHz Layout with drumangle = 0
  load('beamcenters_90GHz.mat'); %File from SPIDER measurement
  %remove offset
  beamcenters_90GHz(:,1) = beamcenters_90GHz(:,1) - nanmean(beamcenters_90GHz(:,1));
  beamcenters_90GHz(:,2) = beamcenters_90GHz(:,2) - nanmean(beamcenters_90GHz(:,2));
  old_r =sqrt(beamcenters_90GHz(:,1).*beamcenters_90GHz(:,1) +beamcenters_90GHz(:,2).*beamcenters_90GHz(:,2));
  old_theta = atan2(beamcenters_90GHz(:,2), beamcenters_90GHz(:,1)) * 180 / pi; %radian to degree

  %SPIDER has different tile ordering than Keck. convert to Keck ordering here
  tile2 = 132+1:132*2;
  tile3 = tile2 + 132;
  tile4 = tile3 + 132;
  r = old_r;
  theta = old_theta;
  r(tile2) = old_r(tile4);
  theta(tile2) = old_theta(tile4);
  r(tile3) = old_r(tile2);
  theta(tile3) = old_theta(tile2);
  r(tile4) = old_r(tile3);
  theta(tile4) = old_theta(tile3);

  %SPIDER has different mux to physical layout convert here:
  ind_SPIDER_gcp = (1:528) - 1;
  ind_Keck_gcp = ind_SPIDER_gcp - mod(ind_SPIDER_gcp, 33) ...% column
    +  mod(mod(ind_SPIDER_gcp, 33) + 1, 33); %shift row by 1
  ind_SPIDER_matlab = ind_SPIDER_gcp + 1;
  ind_Keck_matlab = ind_Keck_gcp + 1;
  r_new(ind_Keck_matlab) = r(ind_SPIDER_matlab);
  theta_new(ind_Keck_matlab) = theta(ind_SPIDER_matlab);

  r = r_new;
  theta = theta_new;

  return
end
