function [o1, o2, o3] = time2psd(x, Fs, window , window2 , window3)
% PSD	Power Spectral Density estimate
% [PSD, Freq] = time2psd( x , Fs , [plotStyle] , [window] )
% [PSD, Angle, Freq] = time2psd( x , Fs , [plotStyle] , [window] , [compact count])
%
% rjg 28Sep97
% rjg 25Feb98 altered normalisation so that it is approporiate for DSP
% rjg 27Feb98 Note dc amplitude handled corrcetly
% rjg 6Mar98  x can be matrix, assumes each column is time trace
%             window should still be just a vector
% rjg 8Mar98 Minor mods
% rjg 27Jun98 window can also be set to a plot style (narly but q&d)
% rjg 21Mar99 Change x label, & initial figure handling
% rjg 25Mar99 Add angle as output argument
% rjg 26Mar99 Added compact count
%
% This is a simplified version of Matlab code that allows you to
% move from time trace (in V, say) to psd (in V/rtHz) with correct
% normlisation
%
%	[Pxx,F] = PSD(X,Fs,WINDOW) estimates the Power Spectral Density of 
%	signal vector X.  X is windowed by the WINDOW parameter.
%    Pxx is length NFFT/2+1 for NFFT even, (NFFT+1)/2
%	for NFFT odd, or NFFT if the signal X is complex.  Fs is the 
%	sampling frequency. f is the frequency scale of the psd.
%	
%	PSD with no output arguments plots the PSD in the current figure window.
%
%	The default values for the parameters are WINDOW = BOXCAR, Fs = nfft Hz.
%    This default will maintain Parsavals Theorem sum(X^2) = 1/nfft * sum(Pxx^2)
%	See also Matlab PSD function
%
% SPECIAL NOTE: if window is set to a string this is used to specify plot style
%

%//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%//% Changes for compiling:
%//% RWO Oct 13 2003: omit graphical stuff!
%//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

error(nargchk(1,5,nargin))

nfft = length(x(:,1));		% Number of data points

cc = inf; ccx = 1.02; % Parameters used by compact() when displaying;
if( nargin==5 ) plotStyle = window; window = window2; cc = window3; end;
if( nargin==4 ) plotStyle = window; window = window2; end;
if( nargin==3 && isstr(window) ) plotStyle = window; window = boxcar(nfft); end;
if(nargin<3) window = boxcar(nfft); end;
if(nargin<2) Fs = nfft; end;

% For the time being some of code is written assuming window is a vector (not a matrix)
if( min(size(window)) > 1 ) error('window should be a vector, not matrix'); end;
if isempty(window) window = boxcar(nfft); end;

% compute PSD
if( min(size(x))==1 )
	x = x(:);		% x is a column, if a vector
end;
if( min(size(window))==1 )
	window = window(:);	% Make window a column, if a vector
end;

% if window is shorter than trace then trace within bounds will be used, or
nwind = length(window(:,1)); % length of window
if nfft < nwind    % zero-pad x if it has length less than the window length
    x(nwind,:)=0;  nfft=nwind;
end

index = 1:nwind;

KMU = norm(window)^2;	% Normalizing scale factor ==> asymptotically unbiased
			% norm() needs vector for correct value to be returned
			% KMU must be a scaler
KMU = KMU/nfft; % Normalise so that KMU=1 for boxcar


ftemp = ( fft( ( window * ones(size(x(1,:))) ).*(x(index,:)) ) );	
Spec = abs( ftemp ).^2;
Spec_Angle = angle( ftemp );

% Select first half
if ~any(any(imag(x)~=0)),   % if x is not complex
    if rem(nfft,2),    % nfft odd
        select = (1:(nfft+1)/2)';
	Spec = 2 * Spec(select,:)*(1/KMU); % Factor 2 for -ve and +ve 
	Spec(1,:) = Spec(1,:)/2; % Freq=0 dc bin is should not be doubled for +/- freqs
    else		% nfft even
        select = (1:nfft/2+1)';
	Spec = 2 * Spec(select,:)*(1/KMU); % Factor 2 for -ve and +ve 
	Spec(1,:) = Spec(1,:)/2; % Freq=0 dc bin is should not be doubled for +/- freqs
	Spec(end,:) = Spec(end,:)/2; % Freq=max bin should not be doubled for +/- freqs
    end
else
    select = (1:nfft)';
	Spec = Spec(select,:)*(1/KMU);
end
Spec_Angle = Spec_Angle(select,:);



% Normalise so that coefficients agree with Parseval's Theorem
%	Spec = Spec/nfft;
% Normalise for rtHz
% 	Spec = Spec/Fs;
% which gives net correction of ...
%	Spec = Spec/(nfft*Fs);
% So for an answer in Signal per root Hz ....

Spec = sqrt( Spec/(Fs*nfft ));
freq_vector = (select - 1)*Fs/nfft;



% set up output parameters
if (nargout == 3),
   o1 = Spec;
   o2 = Spec_Angle;
   o3 = freq_vector;
elseif (nargout == 2),
   o1 = Spec;
   o2 = freq_vector;
elseif (nargout == 1),
   o1 = Spec;
elseif (nargout == 0),

%//.	disp ('time2psd.m: omitting graphical output.');
%//compile 'OFF'
  %figure(gcf);
  if( ~exist( 'plotStyle' , 'var' ) )
     clf; plotStyle = 'b-';
  %else
  %   hold on; % keep curent plot
  end; % user specs style
  if( min(size(Spec))== 1 )
    loglog( ...
      compact(freq_vector,cc,ccx) ...
      , sqrt(compact(abs(Spec).^2,cc,ccx)) ...
      , plotStyle); grid on; hold on;
  else
    loglog( ...
      compact(freq_vector,cc,ccx) ...
      , sqrt(compact( mean( (Spec.^2)')',cc,ccx) ) ...
      , plotStyle); grid on; hold on;
  end
   
   xlabel(sprintf( ...
     'Frequency (Hz)  Bin Width %.1f Hz' ...
   , freq_vector(2) ...
   ));
   ylabel('Signal Magnitude (/rtHz)');
%//compile 'ON'

end









