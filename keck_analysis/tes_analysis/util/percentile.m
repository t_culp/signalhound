function Y = percentile (X, P, excludeNaN)
% PERCENTILE finds the Nth percentile
%    of a data set.  The PRCTILE function from the
%    stats toolbook is similar.
%
%    Y = PERCENTILE (X, P)
%
%        X = vector of data values
%        P = percentile to be found
%            (expressed as a fraction
%            between 0 and 1)
%        Y = resulting value such that
%            a fraction P of the data
%            set is less than Y.
%
%    If P is a vector, several percentile
%    points will be found and returned
%    in Y.
%   
%    If P=0.5, this is equivalent to
%    finding the median. 
%    
%     if excludeNaN = 0 (default), this reverts to behavior
%                     of including NaN when calculating percentile
%     if excludeNaN = 1 this  will exclude NaNs from input array
%                     and thus be equivalent to nanmedian for the
%                     P=0.5 case.
%
% RWO 070815
% dB  may 2015
  
  
if ~exist('excludeNaN');
  excludeNaN = 0;
end

if (excludeNaN)
  X = X(~isnan(X));
  if isempty(X)
    warning('All input data is NaN, cannot produce percentiles...');
    Y = [];
    return 
  end
end
X = sort(X);
N = ((1:length(X))-1) / (length(X)-1);
Y = interp1 (N, X, P);
