function conductivity4K(t1,t2,getDataFlag,rxin,idx,power)
% function conductivity4K(t1,t2,getDataFlag,rxin)
% Analyze the 4K loading schedule and figure out G/L for various paths
%
% INPUTS
%
%  t1:            Start time of schedule (e.g. '2012-jan-29:03:30:00')
%  t2:            Stop time of schedule
%  getDataFlag:   1 to read from arcfile, 0 to load the file
%                 '4k_loading.mat' from the working directory
%  rxin:          rxs to read (selects hk? from arcfile)
%  idx:           Cell array with index ranges of voltage steps
%  power:         Array with wattage of applied power
%                 Must be same length as idx
%
% USAGE
%
%  First run a 4K loading schedule (applies voltage to He4 Condenser
%  thermometer, waits 10-45 minutes) such as 3_keck_7_4Kloading_001.sch.
%  Then run the script to make the timestream plot and figure out the frame
%  number ranges for each voltage step.  Then make a variable idx, power
%  >> idx{1} = 1:500; idx{2} = 1000:2000; idx{3} = 2500:3500; % etc
%  >> power = [0 0.04 0.09] ; % 0, 2, 3 V
%  % Input power should be P = V^2/R (normally R = 100 Ohms)
%  Then rerun with idx and power to make the fits

% If we input the power, scale it down to actual power deposited on the
% resistor: we also have 100 Ohms in the backpack and another 50 in the wire
if exist('power','var')
  power = power.*0.16; 
end

clear fourKTemp_all bracketTemp_all plateTemp_all he4condTemp_all 
clear strapTemp_all spitoonTemp_all FPUTemp_all fiftyKTemp_all 

% Load the data or read it in, depending on the flag
regs = {'antenna0'};
if getDataFlag == 1 
  a = load_arc('arc/',t1,t2,regs);
  save('4k_loading.mat', 'a');
else
  load('4k_loading.mat');
end
sampratio = ...
    length(a.antenna0.time.utcfast)/length(a.antenna0.time.utcslow);

% Loop over receivers
for ii = 1:length(rxin)
  rx = rxin(ii)
  % Find temperatures of interest
  % Make sure to check these when using on a new system!!!
  hk = eval(['a.antenna0.hk' num2str(rx) ';']);
  he4condTemp_all(rx+1,:) = hk.slow_temp(:,13);
  % From Rx master:
  % 4k cold head = fast_temp[0][0]
  % 4k strap = fast_temp[1][0]
  % 4k plate = fast_temp[2][0]
  % Fridge bracket = fast_temp[3][0]
  fourKTemp_all(rx+1,:) = hk.fast_temp(1:sampratio:end,1);
  strapTemp_all(rx+1,:) = hk.fast_temp(1:sampratio:end,2);
  plateTemp_all(rx+1,:) = hk.fast_temp(1:sampratio:end,3);
  bracketTemp_all(rx+1,:) = hk.fast_temp(1:sampratio:end,4);

  x = [1:size(plateTemp_all,2)];
  
  % Plot timestreams
  figure(rx*2 + 1); clf;
  clf
  plot(x(1:end),fourKTemp_all(rx+1,1:end),'b'); % 4K coldhead
  hold on;
  plot(x(1:end),strapTemp_all(rx+1,1:end),'r') % 4K strap
  plot(x(1:end),plateTemp_all(rx+1,1:end),'g') % 4K plate
  plot(x(1:end),bracketTemp_all(rx+1,1:end),'m') % Fridge bracket
  plot(x(1:end),he4condTemp_all(rx+1,1:end),'k') % He4 condensation
  legend('4K Coldhead','4K Heatstrap','4K Plate','Fridge Bracket',...
      'He4 Condensation','Location','NorthEast')
  xlabel('Frame Number')
  ylabel('Temperature (K)')
  keyboard

  % Calculate mean of each step
  for jj = 1:length(idx)
    coldhead_all(rx+1,jj) = mean(fourKTemp_all(rx+1,idx{jj}));
    heatstrap_all(rx+1,jj) = mean(strapTemp_all(rx+1,idx{jj}));
    plate_all(rx+1,jj) = mean(plateTemp_all(rx+1,idx{jj}));
    bracket_all(rx+1,jj) = mean(bracketTemp_all(rx+1,idx{jj}));
    cond_all(rx+1,jj) = mean(he4condTemp_all(rx+1,idx{jj}));
  end

  % Square temps for ease of plotting/fitting
  heatstrap = double(heatstrap_all(rx+1,:));
  heatstrap2 = heatstrap'.*heatstrap';
  coldhead = double(coldhead_all(rx+1,:));
  coldhead2 = coldhead'.*coldhead';
  bracket = double(bracket_all(rx+1,:));
  bracket2 = bracket'.*bracket';
  plate = double(plate_all(rx+1,:));
  plate2 = plate'.*plate';
  cond = double(cond_all(rx+1,:));
  cond2 = cond'.*cond';

  % Plot T^2 vs Power 
  figure(rx*2 + 2); clf;
  plot(power,plate2-heatstrap2,'bx','linewidth',2)
  hold on
  plot(power,bracket2-plate2,'rx','linewidth',2)
  plot(power,cond2-bracket2,'gx','linewidth',2)
  plot(power,cond2-heatstrap2,'mx','linewidth',2)
  plot(power,cond2-plate2,'kx','linewidth',2)
  plot(power,bracket2-heatstrap2,'cx','linewidth',2)
  plot(power,plate2-coldhead2,'bo','linewidth',2)
  plot(power,cond2-coldhead2,'co','linewidth',2)
  plot(power,bracket2-coldhead2,'mo','linewidth',2)
  plot(power,heatstrap2-coldhead2,'ko','linewidth',2)
  
  % Fit T^2 vs power plot
  [c1,gof1] = fit(power',plate2-heatstrap2,'poly1');
  plot(c1,'b')
  [c2,gof2] = fit(power',bracket2-plate2,'poly1');
  plot(c2,'r')
  [c3,gof3] = fit(power',cond2-bracket2,'poly1');
  plot(c3,'g')
  [c7,gof7] = fit(power',cond2-heatstrap2,'poly1');
  plot(c7,'m')
  [c9,gof9] = fit(power',cond2-plate2,'poly1');
  plot(c9,'k')
  [c11,gof11] = fit(power',bracket2-heatstrap2,'poly1');
  plot(c11,'c')
  [c13,gof13] = fit(power',plate2-coldhead2,'poly1');
  plot(c13,'b')
  [c14,gof14] = fit(power',cond2-coldhead2,'poly1');
  plot(c14,'c')
  [c15,gof15] = fit(power',bracket2-coldhead2,'poly1');
  plot(c15,'m')
  [c16,gof16] = fit(power',heatstrap2-coldhead2,'poly1');
  plot(c16,'k')
  
  killme = legend('removemeplease');
  set(killme,'visible','off');
  legend('Plate-Strap','Bracket-Plate','Cond-Bracket','Cond-Strap',...
      'Cond-Plate','Bracket-Strap','Plate-CH','Cond-CH',...
      'Bracket-CH','Strap-CH','Location','East');
  xlabel('Power (W)')
  ylabel('Delta T^2')

  % Do fits for getting Gs and load, and assign to variable for printing
  T_0 = 4; % Assume 4 K
  
  [c4,gof4] = fit(plate2-heatstrap2,power','poly1');
  G(rx+1).plate_heatstrap = 2*T_0*c4.p1;
  L(rx+1).plate_heatstrap = -1.*c4.p2;

  [c5,gof5] = fit(bracket2-plate2,power','poly1');
  G(rx+1).bracket_plate = 2*T_0*c5.p1;
  L(rx+1).bracket_plate = -1.*c5.p2;
  
  [c6,gof6] = fit(cond2-bracket2,power','poly1');
  G(rx+1).cond_bracket = 2*T_0*c6.p1;
  L(rx+1).cond_bracket = -1*c6.p2;

  [c8,gof8] = fit(cond2-heatstrap2,power','poly1');
  G(rx+1).cond_heatstrap = 2*T_0*c8.p1;
  L(rx+1).cond_heatstrap = -1.*c8.p2;
  
  [c10,gof10] = fit(cond2-plate2,power','poly1');
  G(rx+1).cond_plate = 2*T_0*c10.p1;
  L(rx+1).cond_plate = -1.*c10.p2;
  
  [c12,gof12] = fit(bracket2-heatstrap2,power','poly1');
  G(rx+1).bracket_heatstrap = 2*T_0*c12.p1;
  L(rx+1).bracket_heatstrap = -1.*c12.p2;
  
  [c17,gof17] = fit(bracket2-coldhead2,power','poly1');
  G(rx+1).bracket_coldhead = 2*T_0*c17.p1;
  L(rx+1).bracket_coldhead = -1.*c17.p2;
  
  [c18,gof18] = fit(cond2-coldhead2,power','poly1');
  G(rx+1).cond_coldhead = 2*T_0*c18.p1;
  L(rx+1).cond_coldhead = -1.*c18.p2;

  [c19,gof19] = fit(plate2-coldhead2,power','poly1');
  G(rx+1).plate_coldhead = 2*T_0*c19.p1;
  L(rx+1).plate_coldhead = -1*c19.p2;
 
  [c20,gof20] = fit(heatstrap2-coldhead2,power','poly1');
  G(rx+1).heatstrap_coldhead = 2*T_0*c20.p1;
  L(rx+1).heatstrap_coldhead = -1.*c20.p2;

  % Print Gs and Load to the screen
  disp(['G for rx' num2str(rx) ' (W/K):']);
  G(rx+1)
  disp(['L for rx' num2str(rx) ' (W):']);
  L(rx+1)
  
  keyboard
  
end

return
