function [out] = fft2psd( in )
% Assume (in) is a fft (V/rtHz), fold it so that it is one sided power spectrum
% 12Apr99 corrected index error

	n = size(in,1);
	if( iseven(n) )
		k = (1:(n/2-1));
		out = [abs(in(1+0,:)) ; ...
                    sqrt( abs(in(1+k,:)).^2+abs(in(1+(n-k),:)).^2 ) ; ...
                    abs(in(1+(n/2),:)) ...
                    ];
        else
		k = (1:(n-1)/2);
		out = [abs(in(1+0,:)) ; ...
                    sqrt( abs(in(1+k,:)).^2+abs(in(1+(n-k),:)).^2 ) ; ...
                    ];
	end

function m = iseven (d)
	m = mod (d, 2) == 0;
