function [D, S] = parse_zemax (fname)
% PARSE_ZEMAX Parse Zemax data files.
%
%   [D, S] = PARSE_ZEMAX (F)
%
%   F = Zemax data file name
%   D = main data from file
%   S = structure with header info
%
% RWO 090226

f = fopen (fname, 'rt');
S.line1 = fgetl_noblank (f);
S.file = sscanf (fgetl_noblank (f), 'File : %[^\n]s');
S.title = sscanf (fgetl_noblank (f), 'Title: %[^\n]s');
S.date = sscanf (fgetl_noblank (f), 'Date : %[^\n]s');
S.tot_irr_file = sscanf (fgetl_noblank (f), 'Total Irradiance file %[^\n]s');
tmp = sscanf (fgetl_noblank (f), 'Grid size (X by Y): %d by %d');
S.grid_x = tmp(1);
S.grid_y = tmp(2);
tmp = sscanf (fgetl_noblank (f), 'Point spacing (X by Y): %f by %f Millimeters');
S.spacing_x = tmp(1);
S.spacing_y = tmp(2);
tmp = sscanf (fgetl_noblank (f), 'Wavelength %f µm in index %f');
S.wavelength = tmp(1);
S.index = tmp(2);
tmp = sscanf (fgetl_noblank (f), 'Display X Width = %f, Y Height = %f Millimeters');
S.display_x = tmp(1);
S.display_y = tmp(2);
tmp = sscanf (fgetl_noblank (f), 'Peak Irradiance = %f Watts/Millimeters^2, Total Power = %f Watts');
S.peak_irr = tmp(1);
S.tot_pow = tmp(2);

tmp = sscanf (fgetl_noblank (f), ...
	'X Pilot: Size= %f, Waist= %f, Pos= %f, Rayleigh= %f');
S.x_pilot.size = tmp(1);
S.x_pilot.waist = tmp(2);
S.x_pilot.pos = tmp(3);
S.x_pilot.rayleigh = tmp(4);

tmp = sscanf (fgetl_noblank (f), ...
        'Y Pilot: Size= %f, Waist= %f, Pos= %f, Rayleigh= %f');
S.y_pilot.size = tmp(1);
S.y_pilot.waist = tmp(2);
S.y_pilot.pos = tmp(3);
S.y_pilot.rayleigh = tmp(4);

D = fscanf (f, '%f');
D = reshape (D, S.grid_x, [])';
fclose (f);

%%%%

function ll = fgetl_noblank (f)
	ll = '';
	while (isempty (ll))
		ll = fgetl (f);
		if feof (f)
			error ('End of file!');
		end;
	end;
