function beamfitter_par(t1, t2, el_res, source_az, source_el,type,dofit,dk_tar,rxNum,experiment, rot, freq)
% beamfitter_par(t1, t2, el_res, source_az, source_el,type,dofit,dk_tar)
% Construct the maps from demodulated data created by beammap_demod_par. If
% requested, perform a 2-D gaussian fit.  Data products are the maps, saved
% to beammaps/ and a beam csv file saved to pxfits with the best-fit beam
% parameters r, theta, fwhm_maj, fwhm_min, and alpha

% t1 = start time
% t2 = stop time
% el_res = Resolution of elevation steps
% source_az = source type CENTER azimuth position
% source_el = source type CENTER el position
% type: Specify type to map. options are:
%      sin: sin demodulation (defaul)
%      cos: cos demodulation
%      quad: quadrature sum of sin and cos
%      raw: raw fb (no demodulation)
% dofit: option to perform beamfitting. 
%      1: Will perform fit and make windowed maps (default)
%      0: Will not perform fit, will make full-sized maps
% save_map: option to save map to disk
%      0: do nothing
%      1: save map to beammaps/ named according to beammap timestamp

% v2 - 20110902 - RWA

% Set defaults and condition input arguments:
if ~exist('experiment','var')
  experiment='bicep2';
end
if ~exist('rxNum','var')
  rxNum=0;
end
if ~exist('source_el','var')
  error('Not enough input arguments! Hows about you try again when you have your shit together?')
end
if ~strcmpi(type,'sin') & ~strcmpi(type,'cos') & ~strcmpi(type,'quad') & ~strcmpi(type,'raw') & ~strcmpi(type,'moon')
  error('Parameter type should be `sin`, `cos`, `quad`,`raw`, or `moon`')
end
if ~exist('dofit','var')
  dofit=1;
end
if isempty(dofit)
  dofit='dofit';
end
if strcmp(dofit,'dofit')
  window=1;
  coarse=0;
elseif strcmp(dofit,'coarse')
  coarse=1;
  window=0;
elseif strcmp(dofit,'coarsefit')
  coarse=1;
  window=1;
else
  window=0;
  coarse=0;
end

% Get the files:
files=list_arc_files('arc/',t1,t2);
n_files=length(files);
if strcmp(experiment,'keck') | strcmp(experiment,'highbay')
  kk=0;
  while ~exist(strcat('scratch/beammaps/bm_',files{kk+1}(1,1:15),'_rx',int2str(rxNum),'.mat'),'file')
    kk=kk+1;
  end 
else
  kk=0;
  while ~exist(strcat('scratch/beammaps/bm_',files{kk+1}(1,1:15),'.mat'),'file')
    kk=kk+1;
  end 
end
filename=files{kk+1}(1,1:15);


tic
% Concatenate files from beammap_demod_par
for fl=1:n_files
  disp(files{fl}(1,1:15))
  if exist(strcat('scratch/beammaps/bm_',files{fl}(1,1:15),'_rx',int2str(rxNum),'.mat'),'file')
    if strcmp(experiment,'keck') | strcmp(experiment,'highbay')
      e=load(strcat('scratch/beammaps/bm_',files{fl}(1,1:15),'_rx',int2str(rxNum)));
    else
      e=load(strcat('scratch/beammaps/bm_',files{fl}(1,1:15)));
    end
    switch type % Throwout unnecessary fields to save memory
      case 'sin'
	e=rmfield(e.d,{'cos','fb'});
      case 'cos'
	%e=rmfield(e.d,{'sin','fb'});
	e=rmfield(e.d,{'sin'});
      case 'quad'
	e=rmfield(e.d,'fb');  
      case 'raw'
	e=rmfield(e.d,{'sin','cos'});
      case 'moon'
	e=e.d;
    end
    d(fl)=e;
  end
end
toc

clear e

d=structcat(1,d);
% find average dk angle:
dk=nanmean(d.pos(:,3));
dk_com = nanmean(d.com(:,3));

% Gather sin demod, cos demod, or quadrature sum:
switch type 
  case 'sin' 
    d.amp=d.sin;
    d=rmfield(d,'sin');
  case 'cos'
    d.amp=d.cos;
    d=rmfield(d,'cos');
  case 'quad'
    d.amp=sqrt(d.cos.^2+d.sin.^2);
    d=rmfield(d,{'cos','sin'});
  case 'raw'
    d.amp=d.fb;
    d=rmfield(d,'fb');
  case 'moon'
    d.amp=d.fb;
    source_az=0;
    source_el=0;
    d=rmfield(d,'fb');    
end

% If specified, do dk cut:
if ~isempty(dk_tar)
  cc=round(d.com(:,3))==dk_tar;
  d=cut_on_dk(d,cc,type);
end

thing=d.pos(:,1)>10;
d=structcut(d,thing);

% Get the nominal pixel centers:
if strcmp(experiment,'keck') 
  %[p,k]=ParameterRead(sprintf('aux_data/beams/beams_cmb_4param_rx%d_20120324.csv',rxNum));
  %[pp ind]=get_array_info('20120201','obs');
  [pp ind]=get_array_info(filename(1:8),'obs');
  cutind=(pp.rx==rxNum);
  pp=structcut(pp,cutind);
  ind=make_ind(pp);
  %p.theta=p.theta-pp.drumangle;
  %p.drumangle=pp.drumangle;
  mount.drum_angle=mean(pp.drumangle);
elseif strcmp(experiment,'highbay')
  [tmp,k]=ParameterRead(sprintf('aux_data/beams/beams_cmb_4param_20120324.csv',rxNum));
  [pp ind]=get_array_info('20120130');
  cutind=(pp.rx==rxNum);
  pp=structcut(pp,cutind);
  ind=make_ind(pp);
  pp.theta=pp.theta+pp.drumangle;
  if freq == 90
%    [r_90 theta_90] = get_90GHz_beamcen();
%    pp.r(:) = r_90;
%    pp.theta(:) = theta_90;
    pp.r(isnan(pp.r)) = 0;
    pp.theta(isnan(pp.theta)) = 0;
%    pp.drumangle(:) = 0;
  end
else
  [p,k]=ParameterRead('aux_data/beams/beams.csv');
  [pp ind]=get_array_info('20101101','obs');
end

tic

disp('Starting pointing calculation');

az_ap=[];
el_ap=[];
% Find the apparent az and el of each pixel centroid:
if strcmp(experiment,'keck') 
  %[az_ap, el_ap, pa_out] = keck_beam_map_pointing(d.pos(:,1),d.pos(:,2),d.pos(:,3),mount,[],[],p.r,p.theta);
  [az_ap, el_ap, pa_out] = keck_beam_map_pointing(d.pos(:,1),d.pos(:,2),d.pos(:,3), mount,[],[], pp,'Legacy');
elseif strcmp(experiment,'highbay')
  %dk=rot;
%  rot=-90;
  dk = rot;
  dks=ones(length(d.pos(:,3)),1);
  dks=dks*rot;
  d.pos(:,2)=90+d.pos(:,2);
  %[az_ap el_ap]=projection_3d(d.pos(:,1),d.pos(:,2),-dk,d.com(:,1),d.com(:,2),-dk_com,p,ind,strcmp(type,'moon'),0,experiment);
  mount.aperture_offr = 0;
  mount.aperture_offz = 0.6;
  mount.dk_offx = 0;
  mount.dk_offy = 0;
  mount.drum_angle = 0;
  mount.el_tilt = 0;
  mount.el_offz = 0;
  mount.el_offx = 0;
  mount.az_tilt_ha = 0;
  mount.az_tilt_lat = 0;
  mirror.height = 0.7;
  mirror.tilt = -45;     %% Not sure about this one...
  mirror.roll = 0;
  source.distance = 70;
  [az_ap, el_ap, pa_out] = keck_beam_map_pointing(d.pos(:,1),d.pos(:,2),dks,mount,mirror,source,pp, 'Legacy');
  az_ap(az_ap<90)=az_ap(az_ap<90)+360;
  az_ap(az_ap>360)=az_ap(az_ap>360)-360;
else
  [az_ap el_ap]=projection_3d(d.pos(:,1),d.pos(:,2),-dk,d.com(:,1),d.com(:,2),-dk_com,p,ind,strcmp(type,'moon'),0,experiment);
end
toc

% If moon mapping, construct maps in terms of the offset from the source
% position
if strcmp(type,'moon')
  nch=length(p.r);
  az_ap=az_ap-repmat(d.source_az,1,nch);
  el_ap=el_ap-repmat(d.source_el,1,nch);
  az_ap(az_ap>90)=az_ap(az_ap>90)-360;
  az_ap(az_ap<-270)=az_ap(az_ap<-270)+360;
end

% Make the maps
[map x_bin y_bin]=create_map(d,pp,ind,el_res,window,source_az,source_el,az_ap,el_ap,coarse);

%for rx3's wierd column in 2012 data
if rxNum==3 && strcmp(t1(1:4),'2012')
  for ii=34:66
    map(:,:,ii)=-map(:,:,ii);
  end
end



% Exit here if you aren't doing any fitting
if ~window
  if strcmp(experiment,'keck') |strcmp(experiment,'highbay')
    savename=strcat('beammaps/map_',filename,'_rx',int2str(rxNum));
  else
    savename=strcat('beammaps/map_',filename);
  end
  save(savename,'map','x_bin','y_bin','source_az','source_el','dk','-v7.3')
  return
end

% Do the fitting on the windowed maps:
[pf A]=fit_beamparams(x_bin,y_bin,map,pp,source_az,source_el,dk);
%ParameterWrite(strcat('pxfits/beams_bicep2_obs_',filename,'.csv'),pf,k);
p=pf;

% Save out the windowed maps and the fits:
if strcmp(experiment,'keck') | strcmp(experiment,'highbay')
  savename=strcat('beammaps/mapwin_',filename,'_rx',int2str(rxNum));
else
  savename=strcat('beammaps/mapwin_',filename);
end
if strcmp(experiment,'keck') | strcmp(experiment,'highbay')
  save(savename,'map','x_bin','y_bin','source_az','source_el','p','A','dk','-v7.3')
else 
  save(savename,'map','x_bin','y_bin','source_az','source_el','p','A','-v7.3')
end

function [map x_bin y_bin] = create_map(d,p,ind,el_res,window,source_az,source_el,az_ap,el_ap,coarse)

disp('In create_map')

% if we're doing a fit, then we're going to window to just around the main
% beam:
if window
  x_bin=(-1:el_res*1.25:1)+source_az;
  y_bin=(-1:el_res*1.25:1)+source_el;
  %x_bin=(-1.5:el_res*2:1.5)+source_az;
  %y_bin=(-1.5:el_res*2:1.5)+source_el;
elseif coarse
  x_bin=min(min(az_ap(:,ind.l))):el_res*2:max(max(az_ap(:,ind.l)));
  y_bin=min(min(el_ap(:,ind.l))):el_res*2:max(max(el_ap(:,ind.l)));
else
  x_bin=min(min(az_ap(:,ind.l))):el_res*2:max(max(az_ap(:,ind.l)));
  y_bin=min(min(el_ap(:,ind.l))):el_res*2:max(max(el_ap(:,ind.l)));
end
map=zeros(length(y_bin),length(x_bin),length(p.r));

for ii=1:length(p.r)
  if isnan(p.r(ii))
    continue % Skip darks
  end
%  keyboard

  % Generate x and y map coordinates
  x=az_ap(:,ii);
  y=el_ap(:,ii);

  map(:,:,ii)=single(grid_map(x,y,d.amp(:,ii),x_bin,y_bin));
  
  A=map(:,:,ii);
  if ~isempty(A(~isnan(A))) && window
    map(:,:,ii)=nan_interp(A);
  end   
end

%insist the map be positive definite
%map(map<0)=0;

function [pf A]=fit_beamparams(x_bin,y_bin,map,p,source_az,source_el,dk)
pf=p;

% Old version:
% Get the x & y position of the boresight as seen by this pixel
[y x]=reckon(source_el,source_az,p.r,180+(p.theta+90+dk));
x(x>90)=x(x>90)-360;

A=nan(7,528);

for ii=1:length(p.r)
%  if ii==7 || ii==447 % HArdcode bad channels
%    continue
%  end
  disp(['Matlab ' num2str(ii)]);
  if sum(sum(map(:,:,ii)))~=0 & ~isnan(sum(sum(map(:,:,ii))))
%    A(:,ii)=normfit2d(x_bin, y_bin, map(:,:,ii));

A(:,ii)=normfit2d(x_bin, y_bin, map(:,:,ii));
%{
if ~isnan(A(1,ii))
      % Get range and azimuth of the fitted beam center to the boresight
      % position as seen by this pixel
      [pf.r(ii) bearing]=distance(A(3,ii),A(2,ii),y(ii),x(ii));
      pf.theta(ii)=180+(bearing-90-dk);

      % Get the beam width params:
      varxy=A(6,ii)*A(5,ii)*A(4,ii);
      Sigma= [A(4,ii).^2 varxy ; varxy A(5,ii).^2];
      [V, D]=eigs(Sigma);
      pf.fwhm_maj(ii)=sqrt(8*log(2))*sqrt(D(1,1));
      pf.fwhm_min(ii)=sqrt(8*log(2))*sqrt(D(2,2));
      pf.alpha(ii)=pf.theta(ii)+dk-acosd(V(1,1));
    end
    %}
  end
end

pf.theta(pf.theta>360)=pf.theta(pf.theta>360)-360;
pf.theta(pf.theta<0)=pf.theta(pf.theta<0)+360;

function d=cut_on_dk(d,cc,type)
d.ref=d.ref(cc);
d.pos=d.pos(cc,:);
d.com=d.com(cc,:);
d.skips=d.skips(cc);

d.ind=d.ind(cc);
if ~strcmpi(type,'moon')
%   d.sin=d.sin(cc);
%   d.cos=d.cos(cc);
else
  d.source_az=d.source_az(cc);
d.source_el=d.source_el(cc);
d.source_pa=d.source_pa(cc);
end
d.amp=d.amp(cc,:);
