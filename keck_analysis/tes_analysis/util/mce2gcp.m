function chan = mce2gcp(mce_row,mce_col,flag)
% MCE2GCP  map from MCE coordinates (row 0-32, col 0-15)
% to GCP channel number (0-527)
%
% Currently assumes 2 readout cards
%
% MCE2GCP(ROW,COL,'run4') uses pre-run 5 channel assignments
    
% JPF 090515

if (nargin < 3) || isempty (flag)
        map_type = 5;
elseif ischar (flag)
        flag = lower(flag);
        flag (flag == ' ') = '';
        if strcmp (flag, '4') || strcmp (flag, 'run4')
                map_type = 4;
        elseif strcmp (flag, '5') || strcmp (flag, 'run5')
                map_type = 5;
        % GPT: adding a kluge for K3_2, but I don't know the details.
        elseif strcmp (flag, 'k3_2')
                map_type = 5;
        else error (['Unknown flag ' flag]);
        end;
elseif isnumeric (flag)
        map_type = flag;
else error (['Don''t know what to do with flag of type ' class(flag)]);
end;

% Note that nothing here depends on tile coordinates

switch (map_type)
        case 4,    chan = mce_row*(2*8) + mce_col;
        case 5,    chan = mce_col*33 + mce_row;
        otherwise, chan = [];
end;
