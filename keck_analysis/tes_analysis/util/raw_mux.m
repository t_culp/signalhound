function raw_mux(filename);

row_len=98;

files=dir([filename '_*']);  
mkdir([filename '_figdir'])

for col=0:15
  d=load([filename '_' num2str(col) '.mat']);
  figure(1); clf;
  plot(d.trace); hold on;
  title(['Column ' num2str(col)])
  xlabel('Samples @ 50 MHz')
  ylabel('SSA Output (ADU)')

  offsets=zeros(1,length(d.trace));
  for jj=1:33
    offsets((jj-1)*row_len+1:(jj)*row_len)=double(d.offsets(jj));
  end
  plot(offsets,'r.','MarkerSize',2)

  miny=nanmin(d.trace);
  maxy=nanmax(d.trace);
  midy=(miny+maxy)/2;
  %set the yaxis to +/-1500
  maxy=midy+2000;
  miny=midy-2000;
    
  for jj=1:33
    rowswitch=(jj-1)*row_len;
    fbswitch=(jj-1)*row_len+7;
    datatak=(jj)*row_len-10;
    plot([rowswitch rowswitch],[miny maxy],'c');
    plot([fbswitch fbswitch],[miny maxy],'g');
    plot([datatak datatak],[miny maxy],'m');
  end
    
  ylim([miny maxy])
  legend('raw trace','adc offset','row switch')
  saveas(gcf,[filename '_figdir/raw_mux_c' num2str(col) '.png'])

  xlim([0 400])
  legend('raw trace','adc offset','row switch','fb switch','beginning of data')
  saveas(gcf,[filename '_figdir/raw_mux_zoomed_c' num2str(col) '.png'])

  %Plot std
  if isfield(d,'var')
    y=(d.var-d.trace.^2).^.5;
    miny=nanmin(y);
    maxy=nanmax(y);
    if maxy==miny
      maxy=miny+10;
    end

    clf;
    plot(y);
    hold on;
    title(['Column ' num2str(col)])
    xlabel('Samples @ 50 MHz')
    ylabel('SSA Output (ADU)')

    for jj=1:33
      rowswitch=(jj-1)*row_len;
      fbswitch=(jj-1)*row_len+7;
      datatak=(jj)*row_len-10;
      plot([rowswitch rowswitch],[miny maxy],'c');
      plot([fbswitch fbswitch],[miny maxy],'g');
      plot([datatak datatak],[miny maxy],'m');
    end
    
    ylim([miny maxy])
    legend('STD of Raw Trace','adc offset','row switch')
    saveas(gcf,[filename '_figdir/raw_mux_var_c' num2str(col) '.png'])

    xlim([0 400])
    legend('std of raw trace','adc offset','row switch','fb switch','beginning of data')
    saveas(gcf,[filename '_figdir/raw_mux_var_zoomed_c' num2str(col) '.png'])

    end %var plots
  end

end

