function plot_cooldown(t1,t2,filename)
% last used to plot k7 Run11 cooldown.
% Plot the K7 cooldown
% K7_9 from 2014-11-21 to 2014-12-01
  
if ~exist('t1','var')
  t1 = '2015-sep-23:18:00:00'; 
  t2 = '2015-sep-23:19:00:00';
end
  
files = list_arc_files('arc/',t1,t2,'.dat.gz');

for fl = 1:length(files)
  
  load_file = char(strcat('arc/',files(fl)))
  d = load_arc(load_file);
  
  time(fl).datenumfast = utc2datenum(d.antenna0.time.utcfast);
  time(fl).datenumslow = utc2datenum(d.antenna0.time.utcslow);
  temp(fl).t4k_coldhead = d.antenna0.hk0.fast_temp(:,1);
  temp(fl).t4k_strap = d.antenna0.hk0.fast_temp(:,2);
  temp(fl).t4k_plate = d.antenna0.hk0.fast_temp(:,3);
  temp(fl).t4k_fridgebracket = d.antenna0.hk0.fast_temp(:,4);
  temp(fl).t50k = d.antenna0.hk0.slow_temp(:,34);
  temp(fl).he4_pump =  d.antenna0.hk0.slow_temp(:,1);
  temp(fl).he3_pump =  d.antenna0.hk0.slow_temp(:,3);
  temp(fl).he4_hs =  d.antenna0.hk0.slow_temp(:,5);
  temp(fl).he3_hs =  d.antenna0.hk0.slow_temp(:,7);
  temp(fl).he4_evap =  d.antenna0.hk0.slow_temp(:,15);
  temp(fl).ic_evap =  d.antenna0.hk0.slow_temp(:,17);
  temp(fl).uc_evap =  d.antenna0.hk0.slow_temp(:,19);
  temp(fl).he4_cond=  d.antenna0.hk0.slow_temp(:,13) ;

  % Cut down fast sampling so that we have the same length as slow.  Don't
  % need to be the exact same time points (in general they won't be), this
  % is just to keep file size down
  sampratio = ...
      length(d.antenna0.time.utcfast)/length(d.antenna0.time.utcslow);
  cutind = 1:sampratio:length(d.antenna0.time.utcfast);
  time(fl).datenumfast = time(fl).datenumfast(cutind);
  temp(fl).t4k_coldhead = temp(fl).t4k_coldhead(cutind);
  temp(fl).t4k_strap = temp(fl).t4k_strap(cutind);
  temp(fl).t4k_plate = temp(fl).t4k_plate(cutind);
  temp(fl).t4_fridgebracketk = temp(fl).t4k_fridgebracket(cutind);
  clear d
  
end

time = structcat(1,time);
temp = structcat(1,temp);

time.fast_hours = (time.datenumfast - time.datenumfast(1))*24;
time.slow_hours = (time.datenumslow - time.datenumslow(1))*24;

%figure(1); clf;
%plot(time.slow_hours,temp.t50k,'-b'); hold on;
%plot(time.fast_hours,temp.t4k,'-r');
%legend('50K Coldhead','4K Coldhead','Location','NorthEast');
%ylabel('Temperature (K)')
%xlabel('Hours');

cooldown.temp = temp;
cooldown.time = time;
cooldown.t1 = t1;
cooldown.t2 = t2;

save(filename,'cooldown');
keyboard

return