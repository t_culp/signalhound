function beamfitter_pxpol(days)
% beamfitter_par(t1, t2, el_res, source_az, source_el,par,do_fit,save_map)
% Construct the maps from demodulated data created by beammap_demod_par. If
% requested, perform a 2-D gaussian fit.  Data products are the maps, saved
% to beammaps/ and a beam csv file saved to pxfits with the best-fit beam
% parameters r, theta, fwhm_maj, fwhm_min, and alpha

% t1 = start time
% t2 = stop time
% el_res = Resolution of elevation steps
% source_az = source type CENTER azimuth position
% source_el = source type CENTER el position
% type: Specify type to map. options are:
%      sin: sin demodulation (defaul)
%      cos: cos demodulation
%      quad: quadrature sum of sin and cos
%      raw: raw fb (no demodulation)
% dofit: option to perform beamfitting. 
%      1: Will perform fit and make windowed maps (default)
%      0: Will not perform fit, will make full-sized maps
% save_map: option to save map to disk
%      0: do nothing
%      1: save map to beammaps/ named according to beammap timestamp

% v2 - 20110902 - RWA

% Get the files:
log=parse_pxpollog(days);
[ps ind]=get_array_info('20110101','obs');

% Concatenate files from beammap_demod_par
for fl=1:length(log.a)
  disp(num2str(log.a(fl)))
  load(strcat('scratch/pxpol/px_',int2str(log.a(fl))))
  d.amp=d.sin;
  d=rmfield(d,{'sin','cos','fb'});
  [dks uf]=unique(round(d.pos(:,3)),'first');
  [dum ul]=unique(round(d.pos(:,3)),'last');
  
  pxs=[log.a(fl) log.b(fl)]; % pixels of interest
  pf=structcut(ps,pxs);
  indc.a=1; indc.b=1;
  pol.ind(pxs(1))=pxs(1); pol.ind(pxs(2))=pxs(2);
%   pol.dk(:,pxs)=repmat(dks,1,2);
  for ii=1:length(dks)
    indx=uf(ii):ul(ii);
    pol.dk(ii,pxs)=mean(d.pos(uf(ii):ul(ii),3));
    pol.az(ii,pxs)=mean(d.pos(uf(ii):ul(ii),1));
    pol.el(ii,pxs)=mean(d.pos(uf(ii):ul(ii),2));
    ds=structcut(d,indx);
%     [az_ap el_ap]=projection_3d(ds.pos(:,1),ds.pos(:,2),-dks(ii),pf,0);
%     [az_ap el_ap]=projection_3d(d.pos(:,1),d.pos(:,2),-dks(ii),d.com(:,1),d.com(:,2),-dks(ii),pf,indc,0);
%     [map x_bin y_bin]=create_map(ds,pf,az_ap,el_ap);
    % Restrict ourselves to just the integrated power to help speed things
    % up
%     [pk sumfit sumdata]=calc_power(x_bin,y_bin,map,pf);
%     [sumdata]=calc_power(x_bin,y_bin,map,pf);    
    sumdata=sum(ds.amp);
%     pol.pk(ii,pxs)=pk;
%     pol.sumfit(ii,pxs)=sumfit;
    pol.sumdata(ii,pxs)=sumdata;
  end
end
save('pxpol_analysis','pol')
return

% ==================Exit before doing the sine fit  ==================

for ii=1:length(ps.r)
  [A pol.sf(:,ii)]=fit_sin(pol.dks(ii),pol.pk(:,ii));
  pol.ph(ii)=A(2);
end

% Get the nominal pixel centers:
[p,k]=ParameterRead('aux_data/beams/beams.csv');



function [map x_bin y_bin] = create_map(d,p,az_ap,el_ap)
% There are only 7 elevation steps per map:
[n x_bin]=hist(az_ap,7);
[n y_bin]=hist(el_ap,7);

map=zeros(length(y_bin),length(x_bin),length(p.r));

for ii=1:length(p.r)
  % Generate x and y map coordinates
  x=az_ap(:,ii);
  y=el_ap(:,ii);
  map(:,:,ii)=single(grid_map(x,y,d.amp(:,ii),x_bin,y_bin));
end

% function [pk sumfit sumdata]=calc_power(x_bin,y_bin,map,p)
function [sumdata]=calc_power(x_bin,y_bin,map,p)
for ii=1:length(p.r)
  sumdata(ii)=nansum(nansum(map(:,:,ii)));
  if 0
    [A Z]=normfit2d(x_bin, y_bin, map(:,:,ii));
    pk(ii)=A(1);
    sumfit(ii)=sum(sum(Z));
%   else
%     pk(ii)=NaN;
%     sumfit(ii)=NaN;
  end
end




