function [gcp_chan,mce_name] = det2gcp(det_row,det_col,det_pix,det_tile,flag)
% [gcp_chan,mce_name] = det2gcp(det_row,det_col,det_pix,det_tile,flag)
% For BICEP2/Keck
%   DET2GCP  map from detector coordinates to GCP
%   channel number (0-527) (for bicep2,keck)
%   CH = DET2GCP (DET_ROW,DET_COL,DET_PIX,DET_TILE)
%   CH = DET2GCP(...,'run4') uses pre-run 5 channel mapping
%
% For BICEP3
% Input:
%   det_tile: Tile slot that the chosen detector resides in
%              (1, 2, ..., or 21 for BICEP3).
%              as displayed in the drawing below focal plane configuration
%              viewed from below to the sky. Note that this is tile slot,
%              and has nothing to do with the module number that actually
%              goes into the slot.
%               PT   1   2   3
%               4    5   6   7   8
%               9   10  11  12  13
%               14  15  16  17  18
%                   19  20  21
%   det_row: Row number within this tile (1, 2, ..., or 8 for BICEP3)
%   det_col: Col number within this tile (1, 2, ..., or 8 for BICEP3)
%   det_pix: Polarization 'A' or 'B'
%   flag: a string that denotes which mapping to use. (e.g., 'run5')
%
% Output:
%   mce_name: 0, 1, 2, or 3 for BICEP3
%   gcp_chan: 0, 1, ..., or 659 for each MCE in BICEP3 (30 columns 22 row)
%
% RWO 090903 
% JHK 141014 Update to be also used in BICEP3. Added mce_name argument.

% Experiment name (bicep2, keck, or bicep3)
expt=get_experiment_name;

switch expt
    case {'bicep2','keck'}
        if (nargin < 5) || isempty (flag)
            flag = '';
        end;
        [mr mc] = det2mce (det_row, det_col, det_pix, det_tile, flag);
        gcp_chan = mce2gcp (mr, mc, flag);
    case 'bicep3'
        switch flag
            case 'run4'
                % Read the fp_data file for BICEP3 run4
                [p,ind]=get_array_info(20140911);
            case 'run5'
                % Read the fp_data file for BICEP3 run5
                [p,ind]=get_array_info(20141001);
	    case 'run6'
		[p,ind]=get_array_info(20150201);
        end
        % Do conversion
        detidx=find(p.tile==det_tile & p.det_row==det_row & p.det_col==det_col & strcmp(p.pol,det_pix));
        if isempty(detidx)
          error(sprintf('In %s %s: No mce/gcp coordinate matches the det_tile %s, det_row %s, det_col %s, det_pixel %s',expt,flag,num2str(det_tile),num2str(det_row),num2str(det_col),det_pix))
        elseif numel(detidx) > 1
          error(sprintf('In %s %s: More than 1 mce/gcp coordinates have the det_tile %s, det_row %s, det_col %s, det_pixel %s',expt,flag,num2str(det_tile),num2str(det_row),num2str(det_col),det_pix))
        else
        mce_name=p.mce(detidx);
        gcp_chan=p.gcp(detidx);
        end
end

