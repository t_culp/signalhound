function [mce_row mce_col]=det2mce(det_row,det_col,det_pixel,det_tile,flag,frequency)
% [MR MC]=DET2MCE(DR,DC,DP,DT)
% This function maps from detector array coordinates to mce coordinates.
% As a reference, see 'det_map&oper...xls' for the run or FPU revision. 
%
% DET2MCE(DR,DC,DP,DT,'run4') uses the pre-run 5 detector layout.
%
%JAB 20090310
%STATUS: development
% ART 20100318 - modified to treat 96GHz tile layout (if as in Spider Run 2)

if (nargin < 5) || isempty (flag)
        map_type = 5;
elseif ischar (flag)
        flag = lower(flag);
        flag (flag == ' ') = '';
        if strcmp (flag, '4') || strcmp (flag, 'run4')
                map_type = 4;
        elseif strcmp (flag, '5') || strcmp (flag, 'run5')
                map_type = 5;
        % GPT: adding a kluge for K3_2, but I don't know the details.
        elseif strcmp (flag, 'k3_2')
                map_type = 100;
        else error (['Unknown flag ' flag]);
        end;
elseif isnumeric (flag)
        map_type = flag;
else error (['Don''t know what to do with flag of type ' class(flag)]);
end;
if (nargin < 6) || isempty(frequency)
    frequency = 145;
end

det_col = det_col(:);
det_row = det_row(:);
det_tile = det_tile(:);

if (frequency==145) mce_col_index=[0 0 1 1 2 2 3 3]';
elseif (frequency==96) mce_col_index=[0 1 1 2 2 3]'; end;

mce_col=4*(det_tile-1)+mce_col_index(det_col);

if iscell(det_pixel)
        det_pixel = char(det_pixel);
end;
det_pixel = det_pixel(:);

if(frequency==145) mce_row_index=[7 9 5 11 3 13 1 15]';
elseif(frequency==96) mce_row_index=[7 9 5 11 3 13]'; end;

mce_row = mce_row_index(det_row) + (upper(det_pixel)=='A') + 16*mod(det_col+1,2);
    
if (map_type == 5)
        mce_row = mod (mce_row+1, 33);
% GPT: adding a kluge for K3_2, but I don't know the details.
elseif (map_type == 100)
        mce_row = mod (mce_row+3, 33);
end;
