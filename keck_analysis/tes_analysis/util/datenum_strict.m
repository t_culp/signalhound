% n=datenum_strict()
% Identical to standard datenum(), but consistently honors
% user-supplied format strings.  Gives an error if the input
% doesn't match the format string.  This is especially
% imporant with Matlab versions >= R2013a, where datestr()
% often gives random plausible dates on bad input.
function n=datenum_strict(varargin)

% Datenum() can be called in lots of ways.  Pass through to
% standard datenum except in cases where we need to do
% something.
if nargin~=2 || ~ischar(varargin{1}) && ~iscell(varargin{1})
  n=datenum(varargin{:});
  return
end

dstr=varargin{1};
fmt=varargin{2};

% Convert format string to a regexp.
re=fmt2regexp(fmt);
% disp(re);
s=regexp(dstr,['^' re '$']);
if ~iscell(s)
  s={s};
end
% If regexp doesn't match, give an error.
if any(cellfun('isempty',s))
  error(['Date string does not match specified format.']);
end

% If regexp does match, pass through to standard datenum.
n=datenum(dstr,fmt);

return

function [re,specs]=fmt2regexp(fmt)

re='';
specs={};
% isnum and isstr are used in cases where there's no separator,
% e.g. multiple numeric fields or multiple string fields run up
% against each other.  In this case demand consistent zero
% padding, so that March 1, 2012 can be 2012-mar-1 or 2012-mar-01
% or 20120301, but not 2012031.
isnum=false;
isstr=false;
i=1;
while i<=length(fmt)
  wasnum=isnum;
  wasstr=isstr;
  isnum=false;
  isstr=false;
  j=find(~isletter(fmt(i:end)),1);
  if isempty(j)
    j=length(fmt)-i+2;
  end
  if j==1
    switch(fmt(1))
      case {'/','(',')','{','}','[',']'}, re=[re '\' fmt(i)];
      otherwise, re=[re fmt(i)];
    end
    i=i+1;
    continue
  end
  dpart=fmt(i-1+(1:(j-1)));
  i=i+(j-1);
  repart='';
  switch(dpart)
    case 'yyyy',
       isnum=true;
       if wasnum
         repart='[0-9]{4}';
       else
         repart='[0-9]{1,4}';
       end
    case 'yy',
      isnum=true;
      repart='[0-9]{2}';
    case 'QQ',
      repart='Q[0-9]';
    case 'mmmm',
      isstr=true;
      repart='[A-Za-z]+';
    case 'mmm',
      isstr=true;
      repart='[A-Za-z]{3,}';
    case 'mm',
      isnum=true;
      if wasnum
        repart='(0[0-9])|(1[0-2])';
      else
        repart='(0?[0-9])|(1[0-2])';
      end
    case 'dd',
      isnum=true;
      if wasnum
        repart='([0-2][0-9])|(3[01])';
      else
        repart='([0-2]?[0-9])|(3[01])';
      end
    case 'HH',
      isnum=true;
      if wasnum
        repart='([0-1][0-9])|(2[0-3])';
      else
        repart='([0-1]?[0-9])|(2[0-3])';
      end
    case {'MM','SS'},
      isnum=true;
      if wasnum
        repart='[0-5][0-9]';
      else
        repart='[0-5]?[0-9]';
      end
    case 'm',
      isstr=true;
      repart='[A-Z]';
    case 'dddd',
      isstr=true;
      repart='[A-Za-z]+';
    case 'ddd',
      isstr=true;
      repart='[A-Za-z]{3,}';
    case 'd',
      isstr=true;
      repart='[A-Z]';
    case 'FFF',
      isnum=true;
      if wasnum
        repart='[0-9]{3}';
      else
        repart='[0-9]{1,3}';
      end
    case {'AM','PM'},
      isstr=true;
      repart='(A|P)M';
    otherwise,
      error(['Unknown date format specifier "' dpart '".']);
  end
  specs=[specs,{dpart}];
  re=[re '(' repart ')'];
end

return
