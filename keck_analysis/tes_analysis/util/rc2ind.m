function [index] = rc2ind(row, col)
% Utility function to convert rows and columns to frame indices
% Assumes:
%   - 33 rows in a column, numbered 0-32
%   - columns are numbered from 0
%
% 090115 JPF
index = 1+row+33*col;