function [ NTDtemp ] = ntd2temp( NTD_adc )
%ntd2temp Converts adc units to temperature for NTDs on Tile3, FPU, Strap,
%Tile1.  Also replaces bad data with NaN.
%   NTD_adc is of the form [blastbus_channel_6 blastbus_channel_8 9 10]

Tile3_cal=[-0.00449615039615097 -0.0322265069626025 0.278718570765502];
FPU_cal=[0.00171170183575451 -0.0353648620352547 0.290038305774191];
Strap_cal=[-0.00384965236416244 -0.0349094244900457 0.277927073378241];
Tile1_cal=[-0.00392019237140959 -0.0353103969912814 0.284465575701620];

% Calibrations are of the form:
% 
%       A1*(log(NTD_adc/1e9))^2 + A2*log(NTD_adc/1e9) + A3
% 
% where xx_cal = [A1 A2 A3]


% ---------------------------------
%   Replacing bad data with NaN
% ---------------------------------

    NTD_datiszero=(NTD_adc>1e6);

    NTD = double(NTD_adc);
    NTD(~NTD_datiszero)=NaN('double');

% ---------------------------------

div1e9=NTD./1e9;
logdiv=log(div1e9);

NTDtemp=[polyval(Tile3_cal,logdiv(:,1)) polyval(FPU_cal,logdiv(:,2)) ...
    polyval(Strap_cal,logdiv(:,3)) polyval(Tile1_cal,logdiv(:,4))];

end

