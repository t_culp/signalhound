function [slope, yint, chi2]=linfitMontroy(x,y)

% T. Montroy, 26Apr12
% nothing special, just convenient to use
% based on my old c code 



nf=length(y);

sy=sum(y);
sx=sum(x);
sxy=sum(x.*y);
sxx=sum(x.^2);
ss=nf;

det=sxx*ss-sx*sx;
ia=ss/det;
ib=-sx/det;
ic=-sx/det;
id=sxx/det;

slope=ia*sxy+ib*sy;
yint=ic*sxy+id*sy;

chi2=y-slope*x-yint;
chi2=sum(chi2.^2);

end
