function gcp_pair = gcp2pair(gcp)
% gcp_pair = gcp2pair(gcp)
%
% for a given detector (in gcp coord), gives the other det in the pixel pair 
%
% JAB 20100122

for ii=1:length(gcp)

    [r c p t]=gcp2det(gcp(ii));

    if r>0
        if iscell(p)
          p=p{1};
        end
        switch(p)
          case 'A', px='B';
          case 'B', px='A';
          otherwise,  px=p;
        end
        gcp_pair(ii)=det2gcp(r,c,px,t);
    else
        gcp_pair(ii)=NaN;
    end

end
 

    

