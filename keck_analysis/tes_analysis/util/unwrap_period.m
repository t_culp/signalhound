function data_unwrap = unwrap_period(data, period, dim)
    % Unwraps the matrix data along dimension dim (default: last
    % non-singleton dimension), assuming that it wraps around in a
    % finite range (period).
    %
    % Ex: unwrap_period(data,2*pi) mimics the built-in Matlab function
    % unwrap to unwrap angles (though with a different handling of offsets)
    %
    % Ex: unwrap_period(data,2^28) unwraps a long binary number that has
    % been truncated to a 28-bit field.
    %
    % 03Nov2011 JPF
    
    dat_size = size(data);
    
    if nargin<3
        % take dim to be the last non-singleton dimension
        dim = find(dat_size>1,1,'last');
    end
    
    diffdata = diff(data,1,dim);
    ups = (diffdata > period/2);
    dns = (diffdata < -period/2);
    newblock = period*(cumsum(dns,dim)-cumsum(ups,dim));
    
    % this matrix is 1 shorter than the original matrix along dim
    aug_size = dat_size;
    aug_size(dim) = 1;
    newblock = cat(dim,zeros(aug_size),newblock);
    
    % add it to give the final answer
    data_unwrap = data + newblock;
    
end