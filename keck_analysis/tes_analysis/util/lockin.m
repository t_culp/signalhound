function [A V_out A0_out] = lockin (t, v, f0, df)
% LOCKIN  Lock-in to a sinusoid in a time stream
%
% A = LOCKIN (T, V, F0) fits a sinusoidal signal
% near frequency F0 from the time series T, V.
%
% A(0) = offset
% A(1) = amplitude
% A(2) = frequency
% A(3) = phase
%
% A = LOCKIN (T, V) finds the largest signal
% from the power spectral density.
%
% A = LOCKIN (T, V, F0, DF) finds the largest
% signal within DF of frequency F0.
%
% RWO 090306

% If given a time step instead of time
% ticks, calculate ticks
if length(t)==1
	t = t * (1:length(v));
end;

v = v(:);
t = t(:);

% If not given an expected frequency,
% take a PSD and find largest component
if (nargin < 3)
	dt = mean (diff (t));
	[p, f] = time2psd (v, 1/dt);
	[pmax imax] = max (p (3:end));
	f0 = f(imax + 2);
end;

% If requested, search for largest signal
% within df of f0.
if (nargin >= 4 && ~isempty (df))
	dt = mean (diff (t));
	[p, f] = time2psd (v, 1/dt);
	cc = inrange (f, f0-df, f0+df);
	p = p(cc);
	f = f(cc);
	[pmax imax] = max (p);
	f0 = f (imax);
end;

% Condition inputs to ensure good convergence

dt = mean (diff (t));
t_ofs = min (t);
t_tmp = (t - t_ofs) / dt;
v_ofs = mean (v);
v_scale = std (v);
v_tmp = (v - v_ofs) / v_scale;
f0_tmp = f0 * dt;

% Do the real fit
[A V A0] = lockin_real (t_tmp, v_tmp, f0_tmp);

% Convert back to input units

A = A .* [v_scale v_scale 1/dt 1] + [v_ofs 0 0 0];
ftmp = A(3);
A(4) = A(4) + 2*pi * t_ofs * ftmp;

A0 = A0 .* [v_scale v_scale 1/dt 1] + [v_ofs 0 0 0];
ftmp = A0(3);
A0(4) = A0(4) + 2*pi * t_ofs * ftmp;

V = V * v_scale + v_ofs;

if (nargout >= 2)
	V_out = V;
end;
if (nargout >= 3)
	A0_out = A0;
end;

%%%%%%%%%%%%%%%%%%%%%%%%
%
% Core fitting routine
%

function [A V A0] = lockin_real (t, v, f0);
	A0 = [mean(v) std(v) f0 0];
	A = lsqcurvefit (@(a,td) a(1) + a(2) * sin (2*pi*a(3) * td + a(4)), ...
		A0, t, v);

	V = A(1) + A(2) * sin(2*pi*A(3) * t + A(4));
	A = A;
	A0 = A0;
