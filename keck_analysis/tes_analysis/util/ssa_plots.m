% ssa_plots(tuning_dir,figdir)
%
% Useful plots of SSA v-phi curve from auto_setup_squids
%
% fname: the directory of the tuning
% figdir: directory to save figure files (don't save if not specified)
%
%
% SAS 11/5/2010

function ssa_plots(tuning_dir,figdir)

% Constants
MCE_COLS = 8;

saveplots = 1;
if nargin<2
    saveplots=0;
    figdir='';
end
if ~exist(figdir,'dir')
    mkdir(figdir);
end

files=dir([tuning_dir '/*ssa']);
n=length(files);

for i=1:n
    [A r]=read_mce([tuning_dir '/' char(files(i).name)]);
    loopval = r.par_ramp.par_step.loop1.par1;
    nfb = loopval(3);
    ssafb = loopval(1) + loopval(2)*([0:(nfb-1)]');
    nn=16;
    for j=1:nn
        index=j*32;
        channel=(i-1)*8+j-1;
        figure(1); clf;
        
        plot(ssafb,A(index).err/1000)
        title(['SA V-Phi Curve for Column' int2str(channel)]);
        xlabel('SAFB');
        ylabel('SA Output/1000');
        %xlim([1000,10000]);
        
        
        if saveplots
            print('-dpng','-r75',fullfile(figdir, ...
                ['ssaservo_c' int2str(channel)]));
        end
    end
end
