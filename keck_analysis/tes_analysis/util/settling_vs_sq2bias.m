function settling_vs_sq2bias(directories,fine)
%
% Made to plot the raw mux vs sq2bias and find the optimal bias
%
% settling_vs_sq2bias({'settling_vs_sq2bias_rx0'})
%
% SASK

doplots = 0;
if ~exist('fine','var')
  fine=0;
end

for ii=1:length(directories)

  directory=directories{ii};
  figdir=[directory '/figdir'];
  if ~exist(figdir)
    mkdir(figdir)
  end

  b=dir([directory '/*_0.mat']);
  for i=1:length(b)
    if fine
      sq2biases(i)=str2num(b(i).name(12:end-7));
    else
      sq2biases(i)=str2num(b(i).name(8:end-6));
    end
  end

  %arrange sq2biases in order.  kinda skanky, but don't have internet to find the better way.
  for i=1:length(sq2biases)
    [sq2biases_tmp(i) minind]=min(sq2biases);
    sq2biases(minind)=NaN;
  end
  sq2biases=sq2biases_tmp;

  %first go through and make the plots
  if(doplots)
  
    for i=1:length(sq2biases)
      sq2bias=sq2biases(i);
      raw_mux([directory '/sq2biasdiff' num2str(sq2bias)]);
      %Do the "calculations"  
      raw_mux_calc([directory '/sq2biasdiff' num2str(sq2bias)]);
    end
  end

  %Now for analysis
  for i=1:length(sq2biases)
    if fine
      fname=[directory '/sq2biasdiff' num2str(sq2biases(i))];
    else
      fname=[directory '/sq2bias' num2str(sq2biases(i))];
    end
    [med_settling(:,i) mean_settling(:,i)] = raw_mux_calc(fname,0,60);
  end

  %Calculate the optimal sq2bias and plot

  for col=0:15
    [minset(col+1) ind] = min(med_settling(col+1,:));
    optsq2bias(col+1,ii) = sq2biases(ind);
  end
  plot(sq2biases,med_settling');
  title(['Median Settling Time per Column vs SQ2 Bias'])
  xlabel('SQ2 Bias')
  ylabel('Median Settling Time')
  saveas(gcf,[figdir '/medset_vs_sq2bias.png']);
  legend('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15')

  disp(optsq2bias');
 
  keyboard
end
