% PLOT_PIXELS_4TILE  Plot detector parameters in four-tile view, as laid
% out on the focal plane.
%
% PLOT_PIXELS_4TILE(DATA) plots the values in the matrix DATA, which
%   may be a 1x528 vector (GCP coordinates) or a 33x16 matrix (MCE
%   coordinates).
%
% There are several optional arguments:
% PLOT_PIXELS_4TILE(DATA,CAX,TITLESTR,CLAB,TILENAMES)
%
%   CAX=[cax_min, cax_max] gives the scale for the colorbar.
%   TITLESTR gives the overall title of the figure.
%   CLAB gives a label for the colorbar.
%   TILENAMES gives a title or label for each tile.
function h = plot_pixels_4tile (dat, cax, ttl, clab, tlab)

% 0. Reform data if needed
% if (size(dat,1)==33 && size(dat,2)==16)
% 	dat = dat(:);
% end;
dat = dat(:)';
if (nargin < 2)
	cax = [];
end;
if (nargin < 3)
	ttl = '';
end;
if (nargin < 4)
	clab = '';
end;
if (nargin < 5)
	tlab = {};
end;

% 0.5. Font size heuristics
FS1 = get (0, 'DefaultAxesFontSize');
FS2 = floor (FS1 / 2);
if (FS2 < 10)
	if (FS1 >= 10)
		FS2 = 10;
	else
		FS2 = FS1;
	end;
end;
FS3 = floor (FS1 * 2/3);
if (FS3 < 12)
	if (FS1 >= 12)
		FS3 = 12;
	else
		FS3 = FS1;
	end;
end;

% 1. Draw title and colorbar, and calculate
%    how much space we have left to work with.
figure;
title (ttl);
set (gca, 'XTick', []);
set (gca, 'YTick', []);
hcol = colorbar;
if ~isempty(cax)
	caxis(cax);
end;
if ~isempty(clab)
	% set (hcol, 'XAxisLocation', 'top');
	title (hcol, clab);
	% set (get (hcol, 'XLabel'), 'String', clab);
end;
p = get (gca, 'Position');
max_h = 1 - (1 - (p(2) + p(4))) * 0.75;
p = get (hcol, 'OuterPosition');
max_w = 1 - p(3);
axis off;

% 2. Define patch polygons
% LL + UR triangles
xxll = [[0; 1; 0], [1; 1; 0]];
yyll = [[0; 0; 1], [0; 1; 1]];
% first column
xx1 = [];
yy1 = [];
for (ir = 0:7)
  xx1 = [xx1, xxll];
  yy1 = [yy1, yyll+(7-ir)];
end;
% all columns
xx = [];
yy = [];
for (ic = 0:7)
  xx = [xx, xx1+ic];
  yy = [yy, yy1];
end;
dr = repmat (1:8, 1, 8);
dc = repmat (1:8, 8, 1);
dr = dr(:);
dc = dc(:);

% 3. Loop over 4 tiles

for (it = 1:4)

	% 3a. set boundaries
	lower_margin=max_h/20;
	left_margin = max_w/10;
	max_use_h = max_h - lower_margin;
	max_use_w = max_w - left_margin;
	p = [left_margin lower_margin max_use_w/2 max_use_h/2];
	if (it < 3)
		p(2) = p(2) + max_use_h/2;
	end;
	if ((it == 2) || (it == 3))
		p(1) = max_use_w/2;
	end;
	axes ('outerposition', p, 'FontSize', FS2);

	% 3b. set up axes
	axis ([0 8 0 8]);
	axis square;
	set (gca, 'XTick', 0.5:1:7.5);
	set (gca, 'YTick', 0.5:1:7.5);
	if (it < 3)
		set (gca, 'YTickLabel', num2str((8:-1:1)'));
		set (gca, 'XTickLabel', num2str((1:8)'));
        else
                set (gca, 'XAxisLocation', 'top');
		set (gca, 'YTickLabel', num2str((1:8)'));
		set (gca, 'XTickLabel', num2str((8:-1:1)'));
        end;
        if ((it == 1) || (it == 4))
                set (gca, 'YAxisLocation', 'right');
        end;

	% 3c. Draw tile title
	if ~isempty (tlab)
		use_title = tlab{it};
	else
		use_title = ['Tile ' num2str(it)];
	end;
	h = text (4, 8.5 - 9.5*(it>2), use_title, ...
		'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FontSize', FS3);

	% 3d. Draw patch object
	gac = det2gcp (dr, dc, 'A', it);
	gbc = det2gcp (dr, dc, 'B', it);

	if (it < 3)
		gc = [gac, gbc]';
		gc = gc(:) + 1;
		patch (xx, yy, dat(gc), 'EdgeColor', 'black');
	else
		gc = [gbc, gac]';
		gc = gc(:) + 1;
		patch (8-xx, 8-yy, dat(gc), 'EdgeColor', 'black');
	end;
	if ~isempty(cax)
		caxis(cax);
	end;

end;

