
% sq1_bias_plots(fname,phi0_2,figdir)
%
% Useful plots from bias-ramped SQ1servo operations.
%
% fname: sq1servo file name (only a single RC at a time, drop ".rXX")
% phi0_2: measured phi0 (ADU) of the SQ2s for THIS RC
% figdir: directory to save figure files (don't save if not specified)
%
% Ic_max: bias current at peak modulation depth
% sugg_bias: suggested biases for the SQ2's
%
% JPF 100113 Adapted from sq2_bias_plots.m

function [Ic_max, Ic_min, Ic_max2] = sq1_bias_plots(fname,phi0_2,figdir)

% Constants
MCE_COLS = 16;
MAX_AMP_OVER_PHI0_2 = 0.75; % keep amplitude below 3/4 of phi0_A
IC_MAX_RATIO = 1.1; % bias at least 10% above Ic_max
IC_MAX_TOL = 0.99; % take Ic_max to be first point this close to peak value
                  % (limits impact of long flat regions)
MCE_COLS = 16;
do_individuals = 1; % should I plot the individual modulation curves (ugly)
s1bcalib = 1./2^14*(1./2150);  %14 bit, 1 volt max, 2150 ohms (2000 on backplane, 100 on cable, 50 on card

saveplots = 1;
if nargin<3
    saveplots=0;
    figdir='';
end
if length(phi0_2)<MCE_COLS
    error('Not enough phi0 values provided');
end
if ~exist(figdir,'dir')
    mkdir(figdir);
end

% A. GRAB THE BIAS AND FB RAMPs
% Load the run file
R = read_mce_runfile([fname '.run']);
loopvalsB = R.par_ramp.par_step.loop1.par1;
loopvalsF = R.par_ramp.par_step.loop2.par1;
% contains [start step numpoints]
nbias = loopvalsB(3);
s1b = loopvalsB(1) + loopvalsB(2)*([0:(nbias-1)]');
nfb = loopvalsF(3);
s1fb = loopvalsF(1) + loopvalsF(2)*([0:(nfb-1)]');

% B. Grab the real data in the .bias files (one per row!)
for rr = 0:32
    rstr = ['r' num2str(rr,'%02d')];
    B.(rstr) = read_mce_biasfile([fname '.' rstr '.bias']);
end

% which readout card? (kind of a hack...)
if ismember('error00_r00',fieldnames(B.r00))
    rc = 1;
else
    rc = 2;
end


for row = 0:32
    % cycle through the rows (one plot per row)
    rstr = ['r' sprintf('%02d',row)];
    
    disp(['starting row ' int2str(row)]);
    % prepare for modulation depth overlay plot
    figsum=10;
    figure(figsum);clf;
    color_figsum = jet(MCE_COLS);
    axis([0 10 0 15]);
    
    for col = 0:MCE_COLS-1
        rc_col = col + (rc-1)*MCE_COLS;
        % B. GRAB THE DATA
        % In this servo configuration, we extract the signal
        % from the BIAS file.
        s2fbraw = B.(rstr).(['sq2fb' sprintf('%02d',rc_col) '_' rstr])';
        errraw = B.(rstr).(['error' sprintf('%02d',rc_col) '_' rstr])';
        % reshape 
        s2fbraw = reshape(s2fbraw(1:(nbias*nfb)),nfb,nbias);        
        errraw = reshape(errraw(1:(nbias*nfb)),nfb,nbias);
        
        % smooth the data a bit
        s2fb{col+1} = sgolayfilt(s2fbraw,3,31);
        err{col+1} = sgolayfilt(errraw,3,31);
        
        % massage any SQ2 phi0 jumps (up to 1 per trace)
        df = median(diff(s2fb{col+1},1,2),1); % diff at each FB
        [dfmin ind] = min(df); % smallest difference
        if dfmin < -(phi0_2(col+1)/2)
            % large negative jump, so add a phi0
            s2fb{col+1}(:,(ind+1):end) = ...
                s2fb{col+1}(:,(ind+1):end) + ...
                phi0_2(col+1);
        end
        
        if do_individuals
            figure(100);clf;
            plot(s1fb/1000,s2fb{col+1}/1000);
            set(gca,'FontSize',14);
            grid on;
            xlabel('SQ1 FB (ADU/1000)','FontSize',16);
            ylabel('SQ2 FB (ADU/1000)','FontSize',16);
            title(['Row ' int2str(row) ' Column ' int2str(rc_col)], ...
                'FontSize',16);
            yl = ylim;
            % axis([xlim max(-40,yl(1)) min(200,yl(2))]);
            if saveplots
                print('-dpng','-r75',fullfile(figdir, ...
                    ['sq1servo_r' int2str(row) 'c' int2str(rc_col)]));
            end
            
        %plot the error too
            figure(200);clf;
            plot(s1fb/1000,err{col+1}/1000);
            axis([0 4 -5 5]);
            set(gca,'FontSize',14);
            grid on;
            xlabel('SQ1 FB (ADU/1000)','FontSize',16);
            ylabel('Err (ADU/1000)','FontSize',16);
            title(['Column ' int2str(rc_col)],'FontSize',16);
            if saveplots
                print('-dpng','-r75',fullfile(figdir, ...
                    ['sq1servoerr_r' int2str(row) 'c' int2str(rc_col)]));
        end
        end
        
        % find peak-to-peak within last 80% of range
        indmin = floor(nfb*.2);
        maxfb = max(s2fb{col+1}(indmin:end,:),[],1);
        minfb = min(s2fb{col+1}(indmin:end,:),[],1);
        p2p = maxfb-minfb;
        
            % find peak-to-peak within last 80% of range
        % find Icmax
        p2p_max = max(p2p); % true peak
        p2p_ok = p2p_max*IC_MAX_TOL; % close is good enough...
        Ic_max(col+1,row+1) = min(s1b(p2p>=p2p_ok));
        %find Icmin
        stddf=std(diff(s2fb{col+1}));  %std dev of the derivative
        indmin = find(stddf>min(stddf)*2); %where there is a first jump in the derivative
        indmax = find(stddf >.99*max(stddf)); %near the max when it starts to fall off.
        %check to see if there is any modulation
        if std(stddf)<10 || isempty(indmin) || isempty(indmax)
            Ic_max2(col+1,row+1) = 0;
            Ic_min(col+1,row+1) = 0;
        else
    	Ic_min(col+1,row+1) = s1b(min(indmin)); 
        Ic_max2(col+1,row+1) = s1b(max(indmax));
        end

        % add to modulation depth summary plot
        figure(figsum);
        plot(s1b/1000,p2p/1000, 'color',color_figsum(col+1,:), ...
            'LineWidth',2);
        hold on;
        
    end

    % prettify modulation depth summary plot
    set(gca,'FontSize',14);
    line(xlim,mean(phi0_2)/1000*[1 1],'Color','b','LineStyle','--','LineWidth',2);
    grid on;
    ylim([0 10]);
    xlabel('SQ1 Bias (ADU/1000)','FontSize',16);
    ylabel('Modulaton depth (SQ2 FB ADU/1000)','FontSize',16);
    title(['Modulation depth: RC ' int2str(rc) ' row ' int2str(row)],'FontSize',16);
    if saveplots
        print('-dpng','-r75',fullfile(figdir, ...
            ['sq1depth_rc' int2str(rc) '_r' int2str(row)]));
    end
    
end

%    Ic_max = Ic_max'*s1bcalib;
%    Ic_max2 = Ic_max2'*s1bcalib;
%    Ic_min = Ic_min'*s1bcalib;
    
    %save the IC's
%    save Ic_max_sq1.dat Ic_max -ascii     %by finding max p2p
%    save Ic_max2_sq1.dat Ic_max2 -ascii   %by finding drop in spike in the derivative
%    save Ic_min_sq1.dat Ic_min -ascii
    
