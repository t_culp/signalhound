function [fft1] = psd2fft(mag , ang)
% function [fft1] = psd2fft(magnitude , phase angles) 
% Convert from mag,angle to fft notation
% psd2time(mag) assumes all angles are zero
% rjg 28Mar99

if( nargin < 2 )
  % If only one argument assume angles are all zero
  z = 1/sqrt(2) * mag;
else
  z = 1/sqrt(2) * mag .* exp( i * ang);
end

nz = size( z , 1 );
if( ~iseven(nz) )
   % Original fft is even in length
   % dc & nyquist elements need to be adjusted with rt2 again
   fft1 = [ sqrt(2) * z(1,:); ...
            z(2:end-1,:); sqrt(2)*z(end,:); ...
            conj(z(end-1:-1:2,:)); ...
          ];
else
   % Original fft was odd
   fft1 = [ sqrt(2) * z(1,:); ...
            z(2:end,:); ...
            conj(z(end:-1:2,:)); ...
          ];
end










