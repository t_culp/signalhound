function isbad = is_bad_bicep2_Run3pt0(mux_row, mux_col)
% Selects bad detectors from this run
% 
% 20090415 JPF Created from 20090410/experiment.cfg.

bad_detectors = [ ...
0  0  1  0  0  0  0  0  0  0  0  1  0  0  0  0 ... %col0
0  0  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  ...
0  0  1  0  0  0  0  0  0  0  0  1  0  0  0  0 ... %col1
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  ...
0  0  1  0  0  0  0  0  0  0  0  1  0  0  0  0 ... %col2
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  ...
0  0  1  0  0  0  0  0  0  0  0  1  1  1  1  0 ... %col3
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1 ... 
0  0  1  0  0  1  0  0  0  0  0  1  0  0  0  0 ... %col4
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  ... 
0  1  1  1  0  0  0  0  0  0  0  1  0  0  0  0  ... %col5
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  ... 
0  0  1  0  0  0  0  0  0  0  0  1  0  0  0  0  ... %col6
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  ... 
0  0  1  0  0  0  0  0  0  0  0  1  0  0  0  0  ... %col7
0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  0  0 ...
0  0  1  0  0  0  0  0  0  0  0  1  0  0  0  0  ... %col8
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  ... 
0  0  1  0  0  0  0  0  0  0  0  1  0  0  0  0  ... %col9
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  ... 
0  0  1  0  0  0  0  0  0  0  1  1  0  0  0  0  ... %col10
0  0  0  0  0  1  0  0  0  0  0  0  0  0  0  0  1  ... 
0  0  1  0  0  0  0  0  0  0  0  1  0  0  0  0  ... %col11
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  ...
0  0  1  0  0  0  0  0  0  0  0  1  0  0  0  0  ... %col12 
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  ... 
0  0  1  0  1  0  0  0  0  0  0  1  0  0  0  0   ... %col13
0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  0  0  ... 
0  0  1  0  0  0  0  0  0  0  0  1  0  0  0  0  ...  %col14
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0   ...
0  0  1  0  0  0  0  0  0  0  0  1  0  0  0  0  ...  %col15
1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0];

if nargin==0
    % if no arguments, return the full boolean vector
    isbad = logical(bad_detectors);
elseif nargin==1
    % single pixe: "rc2ind" index
    isbad = bad_detectors(mx_row);    
else
    % single pixel: row and column indices
    isbad = bad_detectors(rc2ind(mux_row,mux_col));
end