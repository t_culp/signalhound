function [dr dc dp dt] = mux2phys(row,col,layout_fname)
% [dr dc dp dt] = mux2phys(row,col,layout_fname)
% Function takes mux/mce coordinates and returns physical (tile)
% coordinates.
% dr = detector row
% dc = detector column
% dp = detector polarization ('A' or 'B')
% dt = detector tile (1-4)
% layout_fname = name of file that contains the layout information for this
% particular type of tile (e.g. Standard6x6_DS0.tile)
% row = mux row
% col = mux column
% Originally written by Becky Tucker on 20110908
% Last updated by Becky Tucker on 20110909

% Open the layout file
fid = fopen(layout_fname);
TileLayoutFile = textscan(fid,'%d %d %s %d %d\n','CommentStyle','%');
% .... PhysRow,PhysCol,PhysPol,MuxRow,MuxCol ....
fclose(fid);

% Check which tile the pixel is on and return the corresponding
% coordinates.
if col>=0 && col<=3 %Tile 1
    ind = find(TileLayoutFile{4}==row & TileLayoutFile{5}==col);
    dr = TileLayoutFile{1}(ind);
    dc = TileLayoutFile{2}(ind);
    dt = 1;
    dp = TileLayoutFile{3}(ind);
elseif col>=4 && col<=7 %Tile 2
    ind = find(TileLayoutFile{4}==row & TileLayoutFile{5}==col-4);
    dr = TileLayoutFile{1}(ind);
    dc = TileLayoutFile{2}(ind);
    dt = 2;
    dp = TileLayoutFile{3}(ind);
elseif col>=8 && col<= 11 %Tile 3
    ind = find(TileLayoutFile{4}==row & TileLayoutFile{5}==col-8);
    dr = TileLayoutFile{1}(ind);
    dc = TileLayoutFile{2}(ind);
    dt = 3;
    dp = TileLayoutFile{3}(ind);
elseif col>=12 &&col<=15 %Tile 4
    ind = find(TileLayoutFile{4}==row & TileLayoutFile{5}==col-12);
    dr = TileLayoutFile{1}(ind);
    dc = TileLayoutFile{2}(ind);
    dt = 4;
    dp = TileLayoutFile{3}(ind);
else
    dr = NaN;
    dc = NaN;
    dt = NaN;
    dp = NaN;
end
