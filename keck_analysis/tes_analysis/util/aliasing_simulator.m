function [aliased_freq, aliased_spec] = aliasing_simulator(raw_fsamp,raw_freq,raw_spectra,sim_fsamp,input_freq,input_spectra)
%
% This is a simple spectral aliasing simulator.  The inputs are the
% simulated sampling frequency, the sampling freq for the raw data (raw_fsamp),
% raw_spectra PSD (pA/rtHz), and the frequency bins for the raw_spectra (raw_freq).
%
% Optional input: input_freq and input_spectra (pA/rtHz)
%
% output: the input raw_spectra aliased into the measurement band defined
% by sim_fsamp, along with the corresponding freq bins.
%
% JAB 20090409
% status: development

alias_folds=0;                          % 0 = don't show all the folded spectra in-band

fres=100;                               % resolution of freq bins in simulated spectra
sim_fsamp=floor(sim_fsamp/(fres*10))*fres*10;

raw_fnyq=raw_fsamp/2;
sim_fnyq=sim_fsamp/2;

raw_freq=raw_freq(2:length(raw_freq));
raw_spectra=(raw_spectra(2:length(raw_spectra)))*10^12;

ymin=1e-3; ymax=1e3; xmin=1e1; xmax=raw_fnyq;

font='Courier';
fontsize=14;
textsize=10;
figure('Position',[1 1 1000 700],'DefaultAxesFontName',font,'DefaultAxesFontSize',...
    fontsize,'DefaultTextFontSize',textsize,'DefaultTextFontName',font)
loglog(raw_freq,raw_spectra); hold on;
axis([xmin xmax ymin ymax])
xlabel('frequency (Hz)');
ylabel('PSD RTI (pA/rtHz)');
title('NOISE ALIASING SIMULATOR','FontSize',18,'FontWeight','bold')

plot([sim_fnyq sim_fnyq],[ymin ymax],'r--');

freqinterp=[1:fres:raw_fnyq];
freqplot=[1:fres:sim_fnyq];
specinterp=interp1(raw_freq,raw_spectra,freqinterp,'cubic');
nfoldings=floor((raw_fnyq-sim_fnyq)/(sim_fnyq));
specsum=zeros(1,ceil(sim_fnyq/fres));
for i=1:nfoldings
    index=find(freqinterp>=(i*sim_fnyq) & freqinterp<=(i*sim_fnyq+sim_fnyq));
    if mod(i,2);, specplot=fliplr(specinterp(index));, else, specplot=specinterp(index);,end;
    if alias_folds, loglog(freqplot,specplot,'g');end
    specsum=(specsum + specplot.^2);
end
aliased_spec=sqrt(specsum);
aliased_freq=freqplot;
loglog(aliased_freq,aliased_spec,'k');   

if nargin >4
    specinterp=interp1(aliased_freq,aliased_spec,input_freq,'cubic');
    unaliasedspec=sqrt(input_spectra.^2-specinterp.^2);
    loglog(input_freq,input_spectra,'y');
    loglog(input_freq,unaliasedspec,'g');
end
legend('raw spectra','nyquist freq','total aliased noise','aliased input spectra','input spectra aliasing removed');
hold off

