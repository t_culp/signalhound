function [det_row det_col det_pixel det_tile]=mce2det(mce_row,mce_col,flag)
% [DR DC DP DT] = MCE2DET(MR,MC)
%
% This function maps from mce coordinates to detector array coordinates.
% As a reference, see 'det_map&oper...xls' for the run or FPU revision. 
%
% MCE2DET(MR,MC,'run4') uses the pre-run 5 detector mapping.

%JAB 20090310
%RWO 20101005 - handle vectorized inputs, simplify code

if (nargin < 3) || isempty (flag)
        map_type = 5;
elseif ischar (flag)
        flag = lower(flag);
        flag (flag == ' ') = '';
        if strcmp (flag, '4') || strcmp (flag, 'run4')
                map_type = 4;
        elseif strcmp (flag, '5') || strcmp (flag, 'run5')
                map_type = 5;
        % GPT: adding a kluge for K3_2, but I don't know the details.
        elseif strcmp (flag, 'k3_2')
                map_type = 100;
        else error (['Unknown flag ' flag]);
        end;
elseif isnumeric (flag)
        map_type = flag;
else error (['Don''t know what to do with flag of type ' class(flag)]);
end;

if isscalar(mce_row)
	mce_row=mce_row*ones(size(mce_col));
end
if isscalar(mce_col)
	mce_col=mce_col*ones(size(mce_row));
end

det_row = NaN * ones (size(mce_row));
det_col = NaN * ones (size(mce_row));
det_pixel = cell (size(mce_row));
det_tile = NaN * ones (size(mce_row));

if (map_type == 5)
        mce_row = mod (mce_row-1, 33);
elseif (map_type == 100)
        mce_row = mod (mce_row-3, 33);
end;

isdark = (mce_row==0);
if sum (isdark) > 0
	%disp ('dark squid');
	det_row(isdark) = 0;
	det_col(isdark) = 0;
	det_pixel(isdark) = {0};
	det_tile(isdark) = 0;
end

det_col(~isdark) = 2*mce_col(~isdark) + 1;
det_col(~isdark & mce_row>16) = det_col(~isdark & mce_row>16) + 1;
det_col(~isdark) = 1 + mod (det_col(~isdark)-1, 8);

det_row_index=[7,7,5,5,3,3,1,1,2,2,4,4,6,6,8,8,7,7,5,5,3,3,1,1,2,2,4,4,6,6,8,8];
det_pix_index=transpose(repmat(cellstr(['B';'A']),16,1));

det_row(~isdark)=det_row_index(mce_row(~isdark));
det_pixel(~isdark)=det_pix_index(mce_row(~isdark));
det_tile(~isdark)=1+floor(mce_col(~isdark)/4);

