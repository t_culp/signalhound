function s = structcmp (a, b, astr, bstr)
% STRUCTCMP compares two struct arrays.
%    S = STRUCTCMP(A,B) returns 1 if A and B are identical,
%    and 0 otherwise.  Any differences that are found
%    are described.
%
%    Currently, A and B can only be two-dimensional
%    structure arrays, and there are some limitations
%    on the properties of their fields.
%
% rwo 050915
% rwo 090212 - add support for cell arrays

% astr, bstr arguments can be allowed here, too
if (nargin == 2)
    astr = 'A';
    bstr = 'B';
elseif (nargin ~= 4)
    error ('structcmp takes two inputs, structures A and B.');
end;

if (~isstruct(a) | ~isstruct(b))
    error ('Both input arguments must be of class struct.');
end;

if (nargout > 0)
    s = internal_compare_struct (a, b, astr, bstr);
else
    if (internal_compare_struct (a, b, astr, bstr))
        disp ('Structures match.');
    else
        disp ('Structures do not match.');
    end;
end;

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function s = internal_compare (a, b, astr, bstr)

% Check that classes match.  If not, quit!
inclass = class (a);
if (~strcmp (inclass, class (b)))
    disp (['Different classes: ' astr ' is ' inclass ', and ' ...
           bstr ' is ' class(bstr) '.']);
    s = logical (0);
    return;
end;

switch (class (a))
    case 'struct',
        s = internal_compare_struct (a, b, astr, bstr);
    case {'char', 'double', 'single', 'logical', 'int8', 'uint8', 'int16', 'uint16', 'int32', 'uint32'},
        s = internal_compare_matrix (a, b, astr, bstr);
    case 'cell',
        s = internal_compare_cell (a, b, astr, bstr);
    otherwise,
        warning (['Don''t know how to compare class ' class(a) '!']);
        s = logical (1);
end;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Determine how to compare two 2d matrices of any type.  The return
% value is a code, determined by the two sizes, that stands for a
% style of comparison:
%
%  0. Sizes match.
%  1. If rows of one = cols of the other and vice versa, assume transposed.
%  2. If same # of rows, only compare up to min number of cols
%  3. If same # of cols, only compare up to min number of rows
%  4. Otherwise, treat each as a vector and compare up to min index.

function match_logic = size_logic (am, an, bm, bn, astr, bstr)

match_logic = 0;
if ((am ~= bm) | (an ~= bn))
    disp (['Different sizes: ' astr ' is ' num2str(am) 'x' num2str(an) ...
                          ', ' bstr ' is ' num2str(bm) 'x' num2str(bn) '.']);
    % Maybe they're transposed.
    if ((am == bn) & (an == bm))
        disp (['Assuming ' bstr ' is transposed relative to ' astr '.']);
        match_logic = 1;
    elseif (am == bm)
        disp (['Same number of rows.  Will compare all rows and first ' ...
               num2str(min(an,bn)) ' columns.']);
        match_logic = 2;
    elseif (an == bn)
        disp (['Same number of columns.  Will compare first ' ...
               num2str(min(am,bm)) ' rows and all columns.']);
        match_logic = 3;
    elseif (am * an < bm * bn)
        disp (['Assuming first ' num2str(am*an) ' elements of ' bstr ...
               ' match up with all elements of ' astr '.']);
        match_logic = 4;
    elseif (am * an > bm * bn)
        disp (['Assuming first ' num2str(bm*bn) ' elements of ' astr ...
               ' match up with all elements of ' bstr '.']);
        match_logic = 4;
    end;
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Handles comparisons of 2d struct matrices

function s = internal_compare_struct (a, b, astr, bstr)

s = logical (1);

% Check that sizes match.
[am an] = size (a);
[bm bn] = size (b);
match_logic = size_logic (am, an, bm, bn, astr, bstr);
if (match_logic ~= 0)
    s = logical (0);
end;

% Check that field names match.
afn = fieldnames(a);
bfn = fieldnames(b);
common_fn = intersect (afn, bfn);
a_only_fn = setdiff (afn, bfn);
if (~isempty (a_only_fn))
    disp ([astr ' has ' num2str(length(a_only_fn)) ...
          ' fields not found in ' bstr ':']);
    s = logical (0);
    for (ii = 1:length (a_only_fn))
        disp (a_only_fn {ii});
        end;
end;
b_only_fn = setdiff (bfn, afn);
if (~isempty (b_only_fn))
    disp ([bstr ' has ' num2str(length(b_only_fn)) ...
           ' fields not found in ' astr ':']);
    s = logical (0);
    for (ii = 1:length (b_only_fn))
        disp (b_only_fn {ii});
    end;
end;

% Now run through indices and compare fields.

if (match_logic == 4)
    if (am * an < bm * bn)
        cm = am; cn = an;
    else
        cm = bm; cn = bn;
    end;
else
    cm = min (am, bm);
    cn = min (an, bn);
end;

for (mm = 1:cm)
    for (nn = 1:cn)

        ai = sub2ind ([am an], mm, nn);
        if (am == 1 & an == 1)
            a_indstr = '';
        elseif (am == 1)
            a_indstr = ['(' num2str(nn) ')'];
        elseif (an == 1)
            a_indstr = ['(' num2str(mm) ')'];
        else
            a_indstr = ['(' num2str(mm) ',' num2str(nn) ')'];
        end;

        if (match_logic == 4)
            bi = ai;
        elseif (match_logic == 1)
            bi = sub2ind ([bm bn], nn, mm);
        else
            bi = sub2ind ([bm bn], mm, nn);
        end;
        [b_mm b_nn] = ind2sub ([bm bn], bi);
        if (bm == 1 & bn == 1)
            b_indstr = '';
        elseif (bm == 1)
            b_indstr = ['(' num2str(b_nn) ')'];
        elseif (bn == 1)
            b_indstr = ['(' num2str(b_mm) ')'];
        else
            b_indstr = ['(' num2str(b_mm) ',' num2str(b_nn) ')'];
        end;

% Inside of loop: compare A(ai) and B(bi) as appropriate for
% their class.

        % For structs : loop over fields, recurse.
        for (ii = 1:length (common_fn))
            s = s & internal_compare ...
                   (a(ai).(common_fn{ii}), ...
                    b(bi).(common_fn{ii}), ...
                   [astr a_indstr '.' common_fn{ii}], ...
                   [bstr b_indstr '.' common_fn{ii}]);
        end;
    end;
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Handles comparisons of numeric types when NaN may be present

function s = anyneqnan (a, b)

s = any (a (~isnan (a)) ~= b (~isnan (a))) ...
  | any (isnan (a) ~= isnan (b));

while size (s) > 1
    s = any (s);
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Handles comparisons of 2d matrices of logical, numeric, or character types

function s = internal_compare_matrix (a, b, astr, bstr)

s = logical (1);

% Check that classes match.  If not, quit!
inclass = class (a);
if (~strcmp (inclass, class (b)))
    disp (['Different classes: ' astr ' is ' inclass ', and ' ...
           bstr ' is ' class (bstr) '.']);
    s = logical (0);
    return;
end;

% Check that sizes match.
[am an] = size(a);
[bm bn] = size(b);
match_logic = size_logic (am, an, bm, bn, astr, bstr);
if (match_logic ~= 0)
    s = logical (0);
end;

switch (match_logic)
    case 0,
        if anyneqnan (a, b)
            s = 0;
            disp ([inclass ' matrices ' astr ' and ' bstr ...
                   ' (' num2str(am) 'x' num2str(an) ') ' ...
                   'differ in ' num2str(sum(sum(a~=b))) ' elements.']);
        end;

    case 1,
        if anyneqnan (a, b')
            s = 0;
            disp ([inclass ' matrices ' astr ' and transpose of ' bstr ...
                   ' (' num2str(am) 'x' num2str(bm) ') ' ...
                   'differ in ' num2str(sum(sum(a~=b'))) ' elements.']);
        end;

    case 2,
        if anyneqnan (a(:,1:min(an,bn)), b(:,1:min(an,bn)))
            s = 0;
            disp ([inclass ' matrices ' astr ' (' num2str(am) ...
                   'x' num2str(an) ') and ' bstr ' (' num2str(bm) ...
                   'x' num2str(bn) ') differ in ' ...
                   num2str(sum(sum(a(:,1:min(an,bn))~=b(:,1:min(an,bn))))) ...
                   ' elements.']);
        end;

    case 3,
        if anyneqnan (a(1:min(am,bm),:), b(1:min(am,bm),:))
            s = 0;
            disp ([inclass ' matrices ' astr ' (' num2str(am) ...
                   'x' num2str(an) ') and ' bstr ' (' num2str(bm) ...
                   'x' num2str(bn) ') differ in ' ...
                   num2str(sum(sum(a(1:min(am,bm),:)~=b(1:min(am,bm),:)))) ...
                   ' elements.']);
        end;

    case 4,
        if anyneqnan (a(1:min(am*an,bm*bn)), b(1:min(am*an,bm*bn)))
            s = 0;
            disp ([inclass ' matrices ' astr '(:) (' num2str(am) ...
                   'x' num2str(an) ') and ' bstr(:) ' (' num2str(bm) ...
                   'x' num2str(bn) ') differ in ' ...
                   num2str(sum(sum(a(1:min(am*an,bm*bn))~=b(1:min(am*an,bm*bn))))) ...
                   ' elements.']);
        end;
end;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Handles comparisons of cell arrays

function s = internal_compare_cell (a, b, astr, bstr)

s = logical (1);

% Check that sizes match.
[am an] = size (a);
[bm bn] = size (b);
match_logic = size_logic (am, an, bm, bn, astr, bstr);
if (match_logic ~= 0)
    s = logical (0);
end;

% Now compare cells one-by-one.
if (match_logic == 0)
    for (mm = 1:am)
        for (nn = 1:an)
            s = s & internal_compare (a{mm,nn}, b{mm,nn}, ...
                [astr '{' num2str(mm) ',' num2str(nn) '}'], ...
                [bstr '{' num2str(mm) ',' num2str(nn) '}']);
        end;
    end;
elseif (match_logic == 1)
    for (mm = 1:am)
        for (nn = 1:an)
            s = s & internal_compare (a{mm,nn}, b{nn,mm}, ...
                [astr '{' num2str(mm) ',' num2str(nn) '}'], ...
                [bstr '{' num2str(nn) ',' num2str(mm) '}']);
        end;
    end;
end;

return;
