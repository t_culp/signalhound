function conductivityICUC(t1,t2,getDataFlag,type,rxin,idx,power)
% function conductivityICUC(t1,t2,getDataFlag,type,rxin,idx,power)
% Analyze the IC/UC 4K loading schedule and figure out G/L for various paths
%
% INPUTS
%
%  t1:           Start time of schedule (e.g. '2012-jan-29:03:30:00')
%  t2:           Stop time of schedule
%  type:         'IC','UC'
%  getDataFlag:  1 to read from arcfiles
%                0 to load the file '' from the working directory
%  rxin:         rxs to read (selects hk? from arcfile)
%  idx:          Cell array with index ranges of voltage steps
%  power:        Array with wattage of applied power (if not default)
%                Must be same length as idx
%
% USAGE
%
%  First run the IC/UC loading schedules (apply voltages to the spittoon
%  bottom and FPU Cu TCMs respectively, after the He-4 evap runs out).
%  e.g. 2_keck_rx1_UCloading_002.sch, 1_keck_rx1_ICloading_003.sch
%  Then run the script to make the timestream plot and figure out the frame
%  number ranges for each voltage step.  Then make a variable idx, power 
%  >> idx{1} = 1:500, idx{2} = 1000:2000, idx{3} = 2500:3500 % etc
%  Then rerun with idx to make the fits

% Default power deposited on resistors 
% IC: 0, 0.707, 1, 2, 3 V on 100 kOhm spittoon bottom heater
% UC: 0, 2, 3, 4 V on 1 MOhm FPU Cu heater
if ~exist('power','var')
  switch type
    case 'IC'
      power = [0,5e-6,10e-6,40e-6,90e-6]; % Watts
    case 'UC'
      power = [0,4e-6,9e-6,16e-6]; % Watts
  end
end

clear icEvapTemp_all spittoonTemp_all ucEvapTemp_all ucStrapTemp_all
clear fpucuTemp_all fpunbTemp_all fourKTemp_all

% Load the data or read it in, depending on the flag
regs = {'antenna0'};
if getDataFlag == 1
  a = load_arc('arc/',t1,t2,regs);
  switch type
    case 'IC'
      save('IC_loading.mat','a');
    case 'UC'
      save('UC_loading.mat','a');
  end
else
  switch type
    case 'IC'
      load('IC_loading.mat');
    case 'UC'
      load('UC_loading.mat');
  end
end

% Loop over receivers
for ii = 1:length(rxin)
  rx = rxin(ii)
  % Find temperatures of interest
  % Make sure to check these when using on a new system!!!
  hk = eval(['a.antenna0.hk' num2str(rx) ';']);
  switch type
    case 'IC'
      icEvapTemp_all(rx+1,:) = hk.slow_temp(:,17);
      spittoonTemp_all(rx+1,:) = hk.slow_temp(:,23);
      x = [1:size(icEvapTemp_all,2)];
    case 'UC'
      ucEvapTemp_all(rx+1,:) = hk.slow_temp(:,19);
      ucStrapTemp_all(rx+1,:) = hk.slow_temp(:,41);
      fpucuTemp_all(rx+1,:) = hk.slow_temp(:,11);
      fpunbTemp_all(rx+1,:) = hk.slow_temp(:,21);
      x = [1:size(ucEvapTemp_all,2)];
  end
  
  % Plot timestreams
  figure(rx*2 + 1); clf;
  switch type
    case 'IC'
      plot(x(1:end),icEvapTemp_all(rx+1,1:end),'r'); % IC Evap
      hold on;
      plot(x(1:end),spittoonTemp_all(rx+1,1:end),'b'); % Spittoon
      legend('IC Evap','Spittoon');
      xlabel('Frame Number');
      ylabel('Temperature (K)')
    case 'UC'
      plot(x(1:end),fpucuTemp_all(rx+1,1:end),'b'); % FPU Cu
      hold on;
      plot(x(1:end),fpunbTemp_all(rx+1,1:end),'r'); % FPU Nb
      plot(x(1:end),ucEvapTemp_all(rx+1,1:end),'g') % UC Evap
      plot(x(1:end),ucStrapTemp_all(rx+1,1:end),'m'); % UC Strap
      legend('FPU Cu','FPU Nb','UC Evap','UC Strap');
      xlabel('Frame Number');
      ylabel('Temperature (K)');
  end
  keyboard % If first time, use this to figure out indices for mean calculation

  % Calculate mean of each step
  for jj = 1:length(idx)
    switch type
      case 'IC'
        icEvap_all(rx+1,jj) = mean(icEvapTemp_all(rx+1,idx{jj}));
        spittoon_all(rx+1,jj) = mean(spittoonTemp_all(rx+1,idx{jj}));
      case 'UC'
        fpucu_all(rx+1,jj) = mean(fpucuTemp_all(rx+1,idx{jj}));
        fpunb_all(rx+1,jj) = mean(fpunbTemp_all(rx+1,idx{jj}));
        ucEvap_all(rx+1,jj) = mean(ucEvapTemp_all(rx+1,idx{jj}));
        ucStrap_all(rx+1,jj) = mean(ucStrapTemp_all(rx+1,idx{jj}));
    end
  end

  % Square temps for ease of plotting/fitting
  switch type
    case 'IC'
      icEvap = double(icEvap_all(rx+1,:));
      icEvap2 = icEvap'.*icEvap';
      spittoon = double(spittoon_all(rx+1,:));
      spittoon2 = spittoon'.*spittoon';
    case 'UC'
      fpucu = double(fpucu_all(rx+1,:));
      fpucu2 = fpucu'.*fpucu';
      fpunb = double(fpunb_all(rx+1,:));
      fpunb2 = fpunb'.*fpunb';
      ucEvap = double(ucEvap_all(rx+1,:));
      ucEvap2 = ucEvap'.*ucEvap';
      ucStrap = double(ucStrap_all(rx+1,:));
      ucStrap2 = ucStrap'.*ucStrap';
  end

  % Plot T^2 vs power and fit
  switch type
    case 'IC'
      figure(rx*2 + 2); clf;
      plot(power,spittoon2-icEvap2,'bx','linewidth',2);
      hold on;
      [c1,gof1] = fit(power',spittoon2-icEvap2,'poly1');
      plot(c1,'b');
      killme = legend('removemeplease');
      set(killme,'visible','off');
      legend('Spittoon - IC Evap');
      xlabel('Power (W)');
      ylabel('Delta T^2')
    case 'UC'
      figure(rx*2 + 2); clf;
      plot(power,fpucu2-ucEvap2,'bx','linewidth',2);
      hold on;
      plot(power,fpucu2-ucStrap2,'rx','linewidth',2);
      [c2,gof2] = fit(power',fpucu2-ucEvap2,'poly1');
      [c3,gof3] = fit(power',fpucu2-ucStrap2,'poly1');
      plot(c2,'b');
      plot(c3,'r');
      killme = legend('removemeplease');
      set(killme,'visible','off');
      legend('FPU Cu - UC Evap','FPU Cu - UC Strap');
      xlabel('Power (W)')
      ylabel('Delta T^2')
  end

  % Do fits for getting Gs and load, and assign to variable for printing
  switch type
    case 'IC'
      T_0 = 0.350; % Assume 350 mK
      [c4,gof4] = fit(spittoon2-icEvap2,power','poly1');
      L(rx+1).spittoon_ICevap = -1.*c4.p2;
      G(rx+1).spittoon_ICevap = 2*T_0*c4.p1;
    case 'UC'
      T_0 = 0.250; % Assume 250 mK
      [c5,gof5] = fit(fpucu2-ucEvap2,power','poly1');
      [c6,gof6] = fit(fpucu2-ucStrap2,power','poly1');
      L(rx+1).fpuCu_UCevap = -1.*c5.p2;
      G(rx+1).fpuCu_UCevap = 2*T_0*c5.p1;
      L(rx+1).fpuCu_UCstrap = -1.*c6.p2;
      G(rx+1).fpuCu_UCstrap = 2*T_0*c6.p1;
  end
  
  % Print Gs and load to screen
  disp(['G for rx' num2str(rx) ' (W/K):']);
  G(rx+1)
  disp(['L for rx' num2str(rx) ' (W):']);
  L(rx+1)

  keyboard
end % Rx loop  

return

