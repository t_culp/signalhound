% INTERP1X  1-d resampling by maximum, minimum, or bitwise union
%
%   YI=INTERP1X(X,Y,XI,METHOD) is similar to INTERP1, but
%   is intended to ensure that features in the input Y
%   data are preserved.  The (X,Y) points are grouped by
%   the nearest value of XI, and the max/min/union is
%   calculated within the group to give YI.
%
%   METHOD may be:
%     'max' - maximum nearby value
%     'min' - minimum nearby value
%     'union' - bitwise OR of nearby values
%   

% RWO 110531

function YI = interp1x (X,Y,XI,METHOD)

if (nargin < 4) || isempty(METHOD)
	METHOD = 'max';
end;
METHOD = lower(METHOD);

[b idx] = sort (XI);

if (size(X,2) == 1)
	X = X';
end;
X = double (X);
if (size(Y,2) == 1)
	Y = Y';
end;
Y = double (Y);

if length(b) == 1
  ib = 1;
elseif length(b) == 0
  YI = [];
  return;
else
  ib = interp1 (b, 1:length(b), X, 'nearest');
end
ib (X < b(1)) = 1;
ib (X > b(end)) = length(b);

tab = stats_by_bin (Y, ib, length(b));

switch(METHOD)
	case 'max', yy = tab(2,:);
	case 'min', yy = tab(3,:);
	case {'union', 'or', 'bitor'}, yy = tab(4,:);
	otherwise, error (['Unknown interpolation method ' METHOD '.']);
end;
YI = zeros (size (XI));
YI(idx) = yy;

return;
