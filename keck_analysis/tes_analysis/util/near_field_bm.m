function [px x y sq]=near_field_bm(t1, t2, steps, type, outname, choplet, mergefix)
%JET, last modified by AGV.
% function [px x y sq]=near_field_bm(t1, t2, steps, type)
% Demodulates and filters raw time streams and returns maps
% Inputs:
% t1=start of scan
% t2=end of scan
% steps = number of steps (assumed to be the same for x and y)
% type = option to select sin, cos, or quadrature sum returned from demodulation.
% outname = name of output file in local directory
% Outputs:
% px(row,col,tile) = structure with elements px.a_map = A polmap, px.b_map = B pol map
% x = x coordinates in encoder counts
% y = y coordinates in encoder counts
% sq(gcp_ind) = structure with element sq.map = dark squid maps 

%NOTE:
%Make sure arc/ points to the correct directory!
%A 60x60 beam map takes 6hrs4minutes.
%-----------------------------------------
%EXAMPLE
%[px x y sq]=near_field_bm('2013-dec-04:05:34:57','2013-dec-04:11:38:57', 60, 'quad','testnfbm','','mergefix')
%-----------------------------------------

if nargin<5
  outname='test';
end
if nargin<6
  choplet=[];
end
if nargin<7
  mergefix=[];
end

files=list_arc_files('arc/',t1,t2);

% create filter parameters
[bf af]=butter(1, 1/50, 'high');

%loop over files
for fl=1:length(files)
  % Load the data for one file
  load_file=char(strcat('arc/', files(fl)))
  %if you asked for mergefix (deglitcher), which takes longer, then do it
  if strcmp(mergefix,'mergefix')
    b=load_arc(load_file,'2013-nov-01','2013-dec-20',{'mce0.data.fb','array.frame','mce0.syncBox','antenna0.syncBox','mce0.header.clock_counter','antenna0.pmac.fast_enc_pos[6,7]','antenna0.pmac.fast_aux_input[0]','antenna0.time'},'DO_MERGEFIX',1);
  else
    b=load_arc(load_file,[],[],{'mce0.data.fb','antenna0.syncBox.sampleNumber','mce0.header.clock_counter','antenna0.pmac.fast_enc_pos[6,7]','antenna0.pmac.fast_aux_input[0]','antenna0.time'});
  end
  %create x and y arrays in inches based on encoder values
  y=double(b.antenna0.pmac.fast_enc_pos(:,1))/16000;
  x=double(b.antenna0.pmac.fast_enc_pos(:,2))/16000;

  %rename variables
  fb=b.mce0.data.fb;
  %Shift indices.  GCP index 1:527 = matlab index 1:527, GCP index 0= matlab index 528.
  fb=circshift(fb,[0,-1]);
  
  %Turn ref signal to a logical sqw. The reference level is defined 
  %by the signal that comes back from the optical chopper.
  sqw=b.antenna0.pmac.fast_aux_input<1000;
    
  % Reform data to eliminate some types of dropped bins
  ind_nz=b.mce0.header.clock_counter~=0 & b.antenna0.syncBox.sampleNumber~=0 | (y~=0 & x~=0);
  fb=fb(ind_nz,:);
  x=x(ind_nz);
  y=y(ind_nz);
  sqw=sqw(ind_nz);
  
  if ~strcmp(choplet,'choplet')
    % Filter the fb using the filter defined way above:
    hp_a=filtfilt(bf, af, fb);
    
    % demodulate the data.
    [cos_data, sin_data, edges]=square_demod(hp_a, sqw);
    
    %make sure that the length of sin and cos match so we can do quad 
    % sum if we want to
    if length(sin_data(:,1)) > length(cos_data(:,1))
      sin_data=sin_data(2:length(cos_data(:,1))+1,:);
    elseif length(sin_data(:,1)) < length(cos_data(:,1))
      sin_data=[sin_data;zeros(length(cos_data(:,1))-length(sin_data(:,1)),528)];
    end
    
    %define quadrature sum
    quad=sqrt(cos_data.^2+sin_data.^2);
    
    %fill in x and y position 
    part(fl).x_pos=x(edges)';
    part(fl).y_pos=y(edges)';
    
    %fill in array according to type you chose on input
    if strcmp(type,'sin')
      part(fl).amp=sin_data';
      disp('using sin')
    elseif strcmp(type,'cos')
      part(fl).amp=cos_data';
      disp('using cos')
    else
      part(fl).amp=quad';
      disp('using quadrature sum')
    end
  end %end if not choplet
  
  if strcmp(choplet,'choplet')
    %use choplet demodulator
    d=demod_choplet(b);
    part(fl).amp=d.mce0.data.fb';
    part(fl).x_pos=x';
    part(fl).y_pos=y';
  end %end choplet
  
end %end loop over files
  
% Make inputs to grid_map
x_pos=[part(:).x_pos];
y_pos=[part(:).y_pos];
amp=[part(:).amp];
amp=amp';
%get indeces right for choplet demod
if strcmp(choplet,'choplet')
  amp=circshift(amp,[0,-1]);
end

%define spacing for the map
x=linspace(min(x_pos),max(x_pos),steps);
y=linspace(min(y_pos),max(y_pos),steps);
  
%loop over tiles,rows,columns to make the map itself
for ti=1:4
  for ro=1:8
    for co=1:8
      %get indices for A and B
      a_ind=det2gcp(ro,co,'A',ti);
      b_ind=det2gcp(ro,co,'B',ti);
      if a_ind==0
	a_ind=528;
      end
      %make the map
      A=grid_map(x_pos,y_pos,amp(:,a_ind),x,y);
      B=grid_map(x_pos,y_pos,amp(:,b_ind),x,y);
      A=imrotate(A,180,'bicubic'); %rotate 180 degrees for standard NFBM configuration at Pole
      B=imrotate(B,180,'bicubic');
      A=fliplr(A); %flip map to get correct sense for standard configuration at Pole.
      B=fliplr(B);
      px(ro,co,ti).a_map=A;
      px(ro,co,ti).b_map=B;
    end %end co
  end %end ro
end %end ti
  
%dark squid indeces
dsq=[1,34,67,100,133,166,199,232,265,298,331,364,397,430,463,496];
%define variable for dark squid maps
sq=[];
%make maps for dark squids
for ii=1:length(dsq)
	 sq(ii).map=grid_map(x_pos,y_pos,amp(:,dsq(ii)),x,y);
end

%save output file
save(outname, 'px', 'sq', 'x', 'y')
