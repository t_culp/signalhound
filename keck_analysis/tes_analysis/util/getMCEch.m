function pxstruct = get_MCEch( S, row, col )
    % pxstruct = get_MCEch( S, row, col ) 
    %   
    % Extracts a single pixel (identified by MCE coordinates row and col)
    % from the structure array S, returned by read_mce.
    %
    % row = 0..32 (for num_rows=33)
    % col = 0..15 (for two readout cards)
    %
    % This replaces the kludgier function rc2ind, which will fail if the
    % number of rows in the data file changes.
    %
    % 12Feb2010 JPF
    
    pxstruct = S( ...
        ([S(:).mux_row]==row) & ...
        ([S(:).mux_rc_col]==col) );
    
end