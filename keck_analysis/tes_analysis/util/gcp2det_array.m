function [A B DS]=gcp2det_array(indexed_data,run_flag)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Grab numerically indexed data and turn it into a physical FPU map.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  indexed_data = N*528 array of gcp-indexed data
%
%  run_flag = set to 'run4' if old data
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  A = 16x16xN array in map coordinates of polarization A
%
%  B = 16x16xN array in map coordinates of polarization B
%
%  DS = 2x8xN array of positioned dark SQUIDs
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Don't use 'run4' flag unless specified.
if nargin<2
    run_flag='run5';
end

% Check to see that the input is in gcp coordinates.
if size(indexed_data,2)~=528
    error('i2m:size','Please use N*528 array input.')
end

% Set a dark SQUID counter.
DS_counter=0;

N=size(indexed_data,1);
A=NaN(16,16,N);
B=A;
DS=NaN(2,8,N);

for jj=0:527
    % Find the coordinates.
    [row col pol tile]=gcp2det(jj,run_flag);
    % Place the data into 'A', 'B', or 'DS'.
    if cell2mat(pol)=='A'
        if tile==1
            A(row,col,:)=indexed_data(:,jj+1); % add +1 for Matlab
        elseif tile==2
            A(row,8+col,:)=indexed_data(:,jj+1);
        elseif tile==3
            A(17-row,17-col,:)=indexed_data(:,jj+1);
        elseif tile==4
            A(17-row,9-col,:)=indexed_data(:,jj+1);
        end
    elseif cell2mat(pol)=='B'
        if tile==1
            B(row,col,:)=indexed_data(:,jj+1);
        elseif tile==2
            B(row,8+col,:)=indexed_data(:,jj+1);
        elseif tile==3
            B(17-row,17-col,:)=indexed_data(:,jj+1);
        elseif tile==4
            B(17-row,9-col,:)=indexed_data(:,jj+1);
        end
    else
        DS_counter=DS_counter+1; % Increment the counter.
        if DS_counter<9
            DS(1,DS_counter,:)=indexed_data(:,jj+1);
        else
            DS(2,17-DS_counter,:)=indexed_data(:,jj+1);
        end
    end
end