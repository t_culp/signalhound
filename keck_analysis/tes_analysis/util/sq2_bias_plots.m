% [Ic_max, sugg_bias, Ic_min] = sq2_bias_plots(fname,phi0_A,figdir)
%
% Useful plots from bias-ramped SQ2servo operations.
%
% fname: sq2servo file name (only a single RC at a time)
% phi0_A: measured phi0 (ADU) of the SSAs and SQ2 for THIS RC
% figdir: directory to save figure files (don't save if not specified)
%
% Ic_max: bias current at peak modulation depth
% sugg_bias: suggested biases for the SQ2's
%
% JPF 09111
% SAS 100909 edited to also give IC_min
% JPF 100727 Added Rdyn, slight updates to the the plotting

function [Ic_max, sugg_bias, Ic_min, Ic_max2] = sq2_bias_plots(fname,phi0_A,figdir,sq2bias)


% Constants
MCE_COLS = 8;
MAX_AMP_OVER_PHI0_A = 0.75; % keep amplitude below 3/4 of phi0_A
IC_MAX_RATIO = 1.1; % bias at least 10% above Ic_max
IC_MAX_TOL = 0.99; % take Ic_max to be first point this close to peak value
                  % (limits impact of long flat regions)
saveplots = 1;
do_rdyn = (nargin>=4);
if nargin<3
    saveplots=0;
    figdir='';
end
if ~exist(figdir,'dir')
    mkdir(figdir);
end
if length(phi0_A)<MCE_COLS
    error('Not enough phi0 values provided');
end

% Load the data files (only bias and run are needed)
R = read_mce_runfile([fname '.run']);
B = read_mce_biasfile([fname '.bias']);

% A. GRAB THE BIAS AND FB RAMPs
loopvalsB = R.par_ramp.par_step.loop1.par1;
loopvalsF = R.par_ramp.par_step.loop2.par1;
% contains [start step numpoints]
nbias = loopvalsB(3);
s2b = loopvalsB(1) + loopvalsB(2)*([0:(nbias-1)]');
nfb = loopvalsF(3);
s2fb = loopvalsF(1) + loopvalsF(2)*([0:(nfb-1)]');

% which readout card? (kind of a hack)
if ismember('error00',fieldnames(B))
    rc = 1;
else
    rc = 2;
end

% drop extra data from incomplete fb ramps (shouldn't be any, but ...)
nbias = floor(length(B.( ...
    ['ssafb' sprintf('%02d',(rc-1)*MCE_COLS)]) ...
    )/nfb);
s2b = s2b(1:nbias);

% prepare for modulation depth overlay plot
figsum = figure(10);clf;
h_figsum = [];
leg_figsum=[];
color_figsum = {'k-','b-','g-','r-','c-','m-','k--','b--'};

for col = [0:7]
    rc_col = col + (rc-1)*MCE_COLS;
    % B. GRAB THE DATA
    % In this servo configuration, we extract the signal
    % from the BIAS file.
    safbraw = B.(['ssafb' sprintf('%02d',rc_col)])';
    errraw = B.(['error' sprintf('%02d',rc_col)])';
    % reshape
    safbraw = reshape(safbraw(1:(nbias*nfb)),nfb,nbias);
    errraw = reshape(errraw(1:(nbias*nfb)),nfb,nbias);
    % smooth the data a bit
    safb{col+1} = sgolayfilt(safbraw,3,31);
    err{col+1} = sgolayfilt(errraw,3,31);
    
    % massage any SSA phi0 jumps
    df = median(diff(safb{col+1},1,2),1); % diff at each FB
    ind_jump_down = find(abs(df)>phi0_A(col+1)/2);
    for ind=ind_jump_down
        nphi0 = round(df(ind)/phi0_A(col+1));
        safb{col+1}(:,(ind+1):end) = ...
            safb{col+1}(:,(ind+1):end) - ...
            df(ind);
    end
  
    figure(100);clf;

    plot(s2fb/1000,safb{col+1}/1000)
    set(gca,'FontSize',14);
    grid on;
    xlabel('SQ2 FB (ADU/1000)','FontSize',16);
    ylabel('SSA FB (ADU/1000)','FontSize',16);
    title(['Column ' int2str(rc_col)],'FontSize',16);
    if saveplots
        print('-dpng','-r75',fullfile(figdir, ...
            ['sq2servo_c' int2str(rc_col)]));
    end
    
    %plot the error too
    figure(300);clf;
    axis([0 16 -5 5]);
    plot(s2fb/1000,err{col+1}/1000)
    xlim([0 16]);ylim([-5 5]);
    set(gca,'FontSize',14);
    grid on;
    xlabel('SQ2 FB (ADU/1000)','FontSize',16);
    ylabel('Err (ADU/1000)','FontSize',16);
    title(['Column ' int2str(rc_col)],'FontSize',16);
    if saveplots
        print('-dpng','-r75',fullfile(figdir, ...
            ['sq2servoerr_c' int2str(rc_col)]));
    end
    
    % find peak-to-peak within last 80% of range
    indmin = floor(nfb*.2);
    maxfb = max(safb{col+1}(indmin:end,:),[],1);
    minfb = min(safb{col+1}(indmin:end,:),[],1);
    p2p = maxfb-minfb;
    % find Icmax
    p2p_max = max(p2p); % true peak
    p2p_ok = p2p_max*IC_MAX_TOL; % close is good enough...
    Ic_max(col+1) = min(s2b(p2p>=p2p_ok));
    %find Icmin
    stddf=std(diff(safb{col+1}));  %std dev of the derivative
    indmin = find(stddf>min(stddf)*2); %where there is a first jump in the derivative
    indmax = find(stddf >.99*max(stddf)); %near the max when it starts to fall off.
    %check to see if there is any modulation
    if std(stddf)<5
    	Ic_max2(col+1) = 0;
    	Ic_min(col+1) = 0;
    else          
        Ic_min(col+1) = s2b(min(indmin)); 
        Ic_max2(col+1) = s2b(max(indmax));
    end

    % choose bias
    sugg_bias(col+1) = IC_MAX_RATIO*Ic_max(col+1);
    % find expected amplitude at that bias
    exp_amp = interp1(s2b,p2p,sugg_bias(col+1));
    % if this amplitude causes too much wrapping, pick a higher bias
    if exp_amp > phi0_A(col+1)*MAX_AMP_OVER_PHI0_A
        cok = p2p<phi0_A(col+1)*MAX_AMP_OVER_PHI0_A & ...
            s2b'>sugg_bias(col+1);
        if sum(cok)>0
            sugg_bias(col+1) = min(s2b(cok));
        else
            % we didn't find something acceptable, so max out the bias
            sugg_bias(col+1)=max(s2b);
        end
    end
    
    % plot amplitude versus bias
    figure(200);clf;
    h1=plot(s2b/1000,maxfb/1000,'kx-','LineWidth',2);
    hold on;
    plot(s2b/1000,minfb/1000,'kx-','LineWidth',2);
    h2=plot(s2b/1000,p2p/1000,'bo-','LineWidth',2);
    set(gca,'FontSize',14);
    grid on;
    h3 = line(sugg_bias(col+1)/1000*[1,1], ylim,...
        'LineWidth',2,'Color','r','LineStyle','--');
    h4 = line(xlim,phi0_A(col+1)/1000*[1,1], ...
        'LineWidth',2,'Color','m','LineStyle','--');
    title(['Column ' int2str(rc_col)],'FontSize',16);
    xlabel('SQ2 Bias (ADU/1000)','FontSize',16);
    ylabel('SAFB (ADU/1000)','FontSize',16);
    legend([h1 h2 h3 h4],'Max/Min','Amp','Sugg','\Phi_0^{(A)}', ...
        'Location','NorthWest');
    if saveplots
        print('-dpng','-r75',fullfile(figdir, ...
            ['sq2amp_c' int2str(rc_col)]));
    end
    
    if do_rdyn
        % make a dynamic resistance (dV/dI) plot at fixed bias
        
        % Calibration factors
        calib = calib_default();
        % B2 (ADU) to I2 (SQ2+output coil current)
        BITS_B2 = 16;
        VMAX_B2 = 2.5; % volts
        R_B2 = calib.R_WIRE(rc_col+1)+2200; % ohms
        CAL_B2 = (VMAX_B2/R_B2) / (2^BITS_B2); % amps/ADU
        % SAFB (ADU) to I2
        BITS_FBA = 16;
        VMAX_FBA = 2.5; % volts
        R_FBA = calib.R_WIRE(rc_col+1)+5190; % ohms
        R_OUT2 = 0.09; % ohms
        M_FB2 = 3.3; % approx. ratio of input to FB turns
        CAL_FBA = (VMAX_FBA/R_FBA) / (2^BITS_FBA) / M_FB2; % amps/ADU
        % S2FB (ADU) to I2out
        BITS_FB2 = 14; % (BAC)
        VMAX_FB2 = 0.97; % volts (BAC)
        R_FB2 = calib.R_WIRE(rc_col+1)+10100; % ohms
        CAL_FB2 = (VMAX_FB2/R_FB2) / (2^BITS_FB2); % amps/ADU
        
        % Calibrate to physical units
        Isq2 = safb{col+1}*CAL_FBA; % current through SQ2
        Ib2 = s2b*CAL_B2; % current through SQ2+Rbias2
        Ifb2 = s2fb*CAL_FB2; % SQ2 flux feedback current
        
        % SQ2 voltage
        Vsq2 = R_OUT2*(repmat(Ib2',size(Isq2,1),1)-Isq2);
        
        % R_DYN
        Rdyn = diff(Vsq2,1,2)./diff(Isq2,1,2);
        Rdyn(abs(Rdyn)>10)=0;
        %smooth it
        for i=1:size(Rdyn,2)
          Rdyn_smooth(:,i)=smooth(Rdyn(100:end-200,i),300);
        end
        maxrdyn=max(Rdyn_smooth);
        
        % R_DYN at bias
        s2bmid = (s2b(1:(end-1))+s2b(2:end))/2;
        Rdyn0 = interp1(s2bmid,Rdyn',sq2bias(col+1));
        modcurve = interp1(s2b,safb{col+1}',sq2bias(col+1));
        
        % R_DYN plot
        figure(400);
        clf;
        plot(s2fb/1000,(modcurve-min(modcurve))/1000,'k-','LineWidth',2);
        hold on
        plot(s2fb/1000,Rdyn0,'r-','LineWidth',3);
        set(gca,'FontSize',14);
        grid on;
        axis([xlim 0 25]);
        xlabel('SQ2 FB (ADU/1000)','FontSize',16);
        ylabel('SSA FB (ADU/1000), R_{dyn} (\Omega)','FontSize',16);
        title(['Column ' int2str(rc_col)],'FontSize',16);
        if saveplots
            print('-dpng','-r75',fullfile(figdir, ...
                ['Rdyn_c' int2str(rc_col)]));
        end

        %add to rdyn summary plot
        figure(500)
        plot(s2bmid/1000,maxrdyn, color_figsum{col+1}, 'LineWidth',2);
        hold on;
    end
    
    % add to modulation depth summary plot
    figure(10);
    h_figsum(col+1) = plot(s2b/1000,p2p/1000, color_figsum{col+1}, ...
        'LineWidth',2);
    leg_figsum{col+1} = ['c' int2str(rc_col)];
    hold on;
    
    
end

% prettify modulation depth summary plot
set(gca,'FontSize',14);
grid on;
%axis([0 60 0 60]);
xlabel('SQ2 Bias (ADU/1000)','FontSize',16);
ylabel('SAFB (ADU/1000)','FontSize',16);
title(['Modulation depth: RC ' int2str(rc)],'FontSize',16);
legend(h_figsum,leg_figsum,'Location','NorthWest');
if saveplots
    print('-dpng','-r75',fullfile(figdir, ...
        ['sq2depth_rc' int2str(rc)]));
end
   
if do_rdyn 
  figure(500)
  %prettify rdyn summary plot
  set(gca,'FontSize',14);
  grid on;
  xlabel('SQ2 Bias (ADU/1000)','FontSize',16);
  ylabel('R_{dyn} (\Omega)','FontSize',16);
  title(['Dynamical Resistance: RC ' int2str(rc)],'FontSize',16);
  legend(h_figsum,leg_figsum,'Location','NorthWest');
  if saveplots
    print('-dpng','-r75',fullfile(figdir, ...
        ['rdyn_rc' int2str(rc)]));
  end
end

