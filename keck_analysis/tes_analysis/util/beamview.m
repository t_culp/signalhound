function beamview (gcp_dir)
% BEAMVIEW show live images of TES data
%

% RWO 090713

if (nargin < 1 || isempty(gcp_dir))
	gcp_dir = [getenv('HOME') '/Documents/'];
end;

disp (['Opening data connection']);
[df cf] = open_data (gcp_dir, gcp_dir);
% disp (['Adding registers']);
% add_reg (cf, 'mce0.data.fb[0-527][0]');
disp (['Starting read']);
% start_read (cf);

f = [];
vzero = [];
do_draw = 1;
do_dark_sub = 0;
cax = [-1e4 1e4];
cmask = [];
% set (f, 'CurrentCharacter', char(0));
aviobj = [];
while (do_draw >= 0)
        disp ('Reading data');
	tic;
	[utc vv] = read_dat (df);
	toc
        
        if isempty (vzero)
          vzero = zeros (size (vv));
        end;
	if isempty (cmask)
	  cmask = logical (ones (size (vv)));
	end;
        if (isempty(vv))
          continue;
        end;
	if (do_draw)
	  try
          zz = vv-vzero;
          catch keyboard; end;
	  zz(~cmask) = 0;
          zz(zz==0) = NaN;
          if (do_dark_sub)
		try
	    zz = zz - repmat (zz(1:16), 1, 33);
		catch, keyboard; end;
          end;
          % f = plot_po_po (f, zz, cax);
          try,
	    f = plot_pixels_4tile (f, zz, cax);
          catch,
            disp (['plot_pixels_4tile failed: size(zz) = ' num2str(size(zz,1)) 'x' num2str(size(zz,2))]);
          end;
	  if ~isempty (aviobj)
		aviobj = addframe (aviobj, f);
	  end;
         if (0)
	  if isempty(f) f = figure; end;
	  plot (zz, 'bx');
	  ylim (cax);
         end;
	end;
	  drawnow;
        % pause;
	ch = lower (get(f,'CurrentCharacter'));
	if ~isempty (ch)
		% disp (['User pressed "' num2str(ch) '"']);
		switch (ch)
			case 'p', do_draw = 1 - do_draw; drawnow;
				  if (do_draw == 0)
					disp ('Paused');
				  else
                                        disp ('Resumed');
				  end;
			case 'c', cax = percentile (zz(~isnan(zz)), [0.1 0.99]);
                                  cax = mean(cax) + [1.5*cax(1)-mean(cax) 3*(cax(2)-mean(cax))];
				  if (cax(2) - cax(1) < 50)
                                    cax = mean(cax) + [-20 30];
                                  end;
                                  disp ('Color scale set');
			case 's', do_draw = -1;
                                  disp ('Stopped');
			case 'd', do_dark_sub = 1 - do_dark_sub;
				  if (do_dark_sub==0)
				         disp ('Dark squid subtraction off.');
				  else
					 disp ('Dark squid subtraction on.');
			          end;
			case {'z', '0'}, vzero = vv;
                                  disp ('Zeroed');
			case 'm', cmask (vv < vzero - 500) = 0;
				  disp ('Masked');
			case 'r', cmask(:) = 1;
			          disp ('Mask reset');
			case 'b', if ~isempty (aviobj)
					aviobj = close (aviobj);
			          end;
				  avifname = ['~bicep/bicep2_' datestr(now,'yymmdd_HHMMSS')];
				  aviobj = avifile (avifname);
			case 'e', if ~isempty (aviobj)
					aviobj = close (aviobj);
					aviobj = [];
				  end;

					
			otherwise, disp ('Character commands:');
				   disp ('p = Pause / Unpause');
				   disp ('s = Stop');
				   disp ('z = Zero');
				   disp ('c = Set color scale');
                                   disp (' ');
		end;
		set (f, 'CurrentCharacter', char(0));
	end;
end;
close_data (df, cf);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [df cf] = open_data (gcp_dir, tmp_dir)

% dat_fifo = fullfile (tmp_dir, ['beamview_dat_' datestr(now,'yyyymmdd_HHMMSS')]);
% cmd_fifo = fullfile (tmp_dir, ['beamview_cmd_' datestr(now,'yyyymmdd_HHMMSS')]);

dat_fifo = fullfile (tmp_dir, 'beamview_dat');
cmd_fifo = fullfile (tmp_dir, 'beamview_cmd');

disp (['dat_fifo = ' dat_fifo]);
disp (['cmd_fifo = ' cmd_fifo]);

fclose all;
if exist(cmd_fifo, 'file')
	delete (cmd_fifo);
end;
if exist(dat_fifo,'file')
	delete (dat_fifo);
end;
system (['mkfifo ' dat_fifo]);
system (['mkfifo ' cmd_fifo]);
% syscmd = [fullfile(gcp_dir,'bin','bicepMonitor') ' host=' host ...
% 	' calfile=' fullfile(gcp_dir,'control','conf','bicep','cal') ...
% 	' > ' dat_fifo ' < ' cmd_fifo ' & '];
% syscmd = ['/home/reuben/masutils/src/mas_live/mas_live > ' dat_fifo ' < ' cmd_fifo ' & '];
if ~exist ('/data/cryo/mas_live')
	system (['mkfifo /data/cryo/mas_live']);
end;
syscmd = ['tail -f /data/cryo/mas_live > ' dat_fifo ' < ' cmd_fifo ' & '];
% disp (syscmd);
[r t] = system (['bash -c ''LD_LIBRARY_PATH="" ' syscmd '''']);

% dat_fifo = '~bicep/mas_live';
df = fopen (dat_fifo, 'rt');
cf = fopen (cmd_fifo, 'wt');
cf = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function add_reg (cf, reg)

fprintf (cf, 'add %s\n', reg);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function start_read (cf);

fprintf (cf, 'read\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function close_data (cf, df)
fclose (cf);
fclose (df);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [utc vv] = read_dat (df)
ll = -1;
while (ll == -1)
ll = fgetl (df);
end;
vv = str2num (ll);
utc = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Y = percentile (X, P)
% PERCENTILE finds the Nth percentile
%    of a data set.
%
%    Y = PERCENTILE (X, P)
%
%        X = vector of data values
%        P = percentile to be found
%            (expressed as a fraction
%            between 0 and 1)
%        Y = resulting value such that
%            a fraction P of the data
%            set is less than Y.
%
%    If P is a vector, several percentile
%    points will be found and returned
%    in Y.
%
%    If P=0.5, this is equivalent to
%    finding the median.
%
% RWO 070815

X = sort (X);
N = ((1:length(X))-1) / (length(X)-1);
Y = interp1 (N, X, P);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function fnum_out = plot_po_po (fnum, data, cax)

if isempty (fnum)
	fnum = figure;
else
        set (0, 'CurrentFigure', fnum);
end;
persistent amap bmap

if isempty (amap)
	for (ii = 1:8)
		for (jj = 1:8)
			[mr mc] = det2mce (ii, jj, 'A', 1);
			amap (ii, jj) = mce2gcp (mr, mc);
			[mr mc] = det2mce (ii, jj, 'A', 2);
			amap (ii, 8 + jj) = mce2gcp (mr, mc);
			[mr mc] = det2mce (ii, jj, 'A', 3);
			amap (17 - ii, 17 - jj) = mce2gcp (mr, mc);
			[mr mc] = det2mce (ii, jj, 'A', 4);
			amap (17 - ii, 9 - jj) = mce2gcp (mr, mc);
		end;
	end;
	amap = amap(end:-1:1,:);
	amap = amap(:,end:-1:1);
end;

if isempty (bmap)
        for (ii = 1:8)
                for (jj = 1:8)
                        [mr mc] = det2mce (ii, jj, 'B', 1);
                        bmap (ii, jj) = mce2gcp (mr, mc);
                        [mr mc] = det2mce (ii, jj, 'B', 2);
                        bmap (ii, 8 + jj) = mce2gcp (mr, mc);
                        [mr mc] = det2mce (ii, jj, 'B', 3);
                        bmap (17 - ii, 17 - jj) = mce2gcp (mr, mc);
                        [mr mc] = det2mce (ii, jj, 'B', 4);
                        bmap (17 - ii, 9 - jj) = mce2gcp (mr, mc);
                end;
        end;
        bmap = bmap(end:-1:1,:);
        bmap = bmap(:,end:-1:1);
end;

va = zeros (size (amap));
vb = zeros (size (bmap));
vc = zeros (size (amap));

data = (data - cax(1)) / (cax(2) - cax(1));
va(:) = data(amap(:));
vb(:) = data(bmap(:));
vc(:) = (va(:) + vb(:)) / 20;
va(isnan(va)) = vb(isnan(va));
vb(isnan(vb)) = va(isnan(vb));

vv = cat (3, va, vb, vc) * 2 - 0.3;
vv = vv.^2;
vv (vv < 0) = 0;
vv (vv > 1) = 1;
image (vv);
axis off;

fnum_out = fnum;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function fnum_out = plot_pixels_4tile(fnum, data,colorbar_axis,titlestr,datatype)
%                                                               
% This program plots a set of scalar detector parameters to the 
% corresponding physical location on the tile.  This version of the code
% plots all 4 tiles as they would appear on the focal plane, with the proper
% relative rotations of the tiles.                                          
%                                                                           
% input:                                                                    
%                                                                           
%       data:                                                               
%           a (1x528) vector or (33x16) matrix of detector parameters (Tc,  
%           Rn, etc.)  The ordering of the data is in mce coordinates       
%           rows 0-32, cols 0-15.  The top left corner of the matrix is     
%           0,0.  The ordering of the matrix follows that given by          
%           mce_read, as shown below.                                       
%                                                                           
%       colorbar_axis:                                                      
%           colorbar_axis=[axis_min,axis_max], specifying the scale used by 
%           the colorbar.                                                   
%                                                                           
%       titlestr:                                                           
%           titlestr='string for tile label', proceeds 'TILE #', eg. 'Rn'   
%                                                                           
%       datatype:                                                           
%           datatype='string for colorbar label', eg. 'm\Omega', 'mK'.      
%                                                                           
% output:   a 4 tile plot of the parameters, with detectors A & B overlayed in
%           a single pixel.  A scalable colorbar is also plotted for each     
%           tile.                                                             
%                                                                             
%                                                                             
%JAB 20090317                                                                 
%STATUS: development                                                          

%reshape data from mce indices to detector indices using mce_det_map.m
%data for the 1 tile patch plots should be size=3x128, with all rows the same
%value for each column.                                                      
%                                                                            
% mce_read gives s=(1x528) in order:                                         
%               [(row,col) ... (0,0),(1,0),(2,0)...(0,1),(1,1),(2,1)...]     
%                                                                            
%                                                                            
% data plotting order:  row: 1 1 1 1     1 1 2 2                             
%                       col: 1 1 2 2 ... 8 8 1 1 ...                         
%                       pix: A B A B     A B A B                             

datadim=size(data);
if 1
if (min(datadim)==1 && max(datadim==528))
	data = reshape(data,16,33);
	data = data';
	datadim = size(data);
end;
elseif 1
if (min(datadim)==1 && max(datadim==528))
        data = reshape(data,33,16);
        data = data';
        datadim = size(data);
end;
end;
% if (datadim(1)==33 && datadim(2)==16), reshape(data,1,528);,end
data = data(:);

%coordinates of data input
row_order_data=[repmat([0:32],1,16)];
col_order_data=[];                   
for i=0:15, col_order_data=[col_order_data repmat(i,1,33)];,end;

%plotting coordinates for 1 tile
row_order_plot=[];              
for i=1:8,row_order_plot=[row_order_plot repmat(i,1,16)];,end;
col_order_plot=[repmat([1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8],1,8)];
pix_order_plot=transpose([repmat(['A';'B'],64,1)]);            

%filling up the color vector for the patch objects
c1=[];                                            
c2=[];                                            
c3=[];                                            
c4=[];                                            

for j=1:128,
    [mce_row_t1 mce_col_t1]=det2mce(row_order_plot(j),col_order_plot(j),pix_order_plot(j),1);
    [mce_row_t2 mce_col_t2]=det2mce(row_order_plot(j),col_order_plot(j),pix_order_plot(j),2);
    [mce_row_t3 mce_col_t3]=det2mce(row_order_plot(j),col_order_plot(j),pix_order_plot(j),3);
    [mce_row_t4 mce_col_t4]=det2mce(row_order_plot(j),col_order_plot(j),pix_order_plot(j),4);
                                                                                             
    s1=find(row_order_data == mce_row_t1);                                                   
    plot_val1=data(s1(ismember(find(row_order_data == mce_row_t1),find(col_order_data == mce_col_t1))));
    s2=find(row_order_data == mce_row_t2);                                                              
    plot_val2=data(s2(ismember(find(row_order_data == mce_row_t2),find(col_order_data == mce_col_t2))));
    s3=find(row_order_data == mce_row_t3);                                                              
    plot_val3=data(s3(ismember(find(row_order_data == mce_row_t3),find(col_order_data == mce_col_t3))));
    s4=find(row_order_data == mce_row_t4);                                                              
    plot_val4=data(s4(ismember(find(row_order_data == mce_row_t4),find(col_order_data == mce_col_t4))));
    c1=[c1 plot_val1];                                                                                  
    c2=[c2 plot_val2];                                                                                  
    c3=[c3 plot_val3];                                                                                  
    c4=[c4 plot_val4];                                                                                  
end                                                                                                     
    c1=[c1;c1;c1];                                                                                      
    c2=[c2;c2;c2];                                                                                      
    c3=[c3;c3;c3];                                                                                      
    c4=[c4;c4;c4];                                                                                      
                                                                                                        
font='Times New Roman';                                                                                 
fontsize=12;                                                                                            
if isempty(fnum)
fnum = figure('DefaultAxesFontSize',fontsize,'DefaultAxesFontName',font,'DefaultTextFontSize', fontsize,'DefaultTextFontName',font,...                                                                                                             
    'Position',[1 1 1000 750], 'renderer', 'painters');                                                                                       
if nargin > 3, set(gcf,'Name',['4 TILE FPU ' titlestr],'NumberTitle','off'),end                                       
else
% figure (fnum);
set (0, 'CurrentFigure', fnum);
end;

hold on;
colormap('jet');
%  colormap gray;

    cmin=colorbar_axis(1);
    cmax=colorbar_axis(2);

%Creating patch objects
xseed=[];              
yseed=[];              
yr2=[];                
for ii=1:7, xseed=[xseed ii ii];, end;
xr1=repmat([0 0 xseed],1,8);          
xr2=repmat([0 xseed 8],1,8);          
xr3=repmat([xseed 8 8],1,8);          
for ii=1:7 yseed=[yseed repmat(ii,1,16)];, end;
yr1=[repmat(0,1,16) yseed];                    
yr3=[yseed repmat(8,1,16)];                    
for ii=0:7 yr2=[yr2 (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii (ii+1) ii];, end;
x=[xr1;xr2;xr3];                                                                                           
y=[yr1;yr2;yr3];                                                                                           

%Displaying patch objects

%TILE 1
subplot(2,2,1,'Position',[.1 .55 .4 .4])
set (gca, 'color', [0 0 0]);
h1=patch(x,y,c1,'CDataMapping','scaled');
if nargin < 4,titleuse='TILE 1';,else,titleuse=[titlestr ' TILE 1'];end
title(titleuse,'FontSize',14,'FontName',font,'FontWeight','bold');     
caxis([cmin cmax]);                                                    
axis ([0 8 0 8]); axis square;                                         
%xlabel('det col');                                                    
%ylabel('det row');                                                    
set(gca,'XTick',0.5:7.5,'XTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YTick',0.5:7.5,'YTickLabel',char('1','2','3','4','5','6','7','8'),...    
    'YDir','reverse','XAxisLocation','bottom','YAxisLocation','right',...     
    'TickLength',[.001 .001]);                                                
text(.25,.7,'A');                                                             
text(.6,.3,'B'); hold on;                                                     
for i=0:8,                                                                    
plot([0:8],repmat(i,1,9),'k','LineWidth',2);                                  
plot(repmat(i,1,9),[0:8],'k','LineWidth',2);                                  
end; hold off;                                                                

%TILE 2
subplot(2,2,2,'Position',[.5 .55 .4 .4])
set (gca, 'color', [0 0 0]);
h2=patch(x,y,c2,'CDataMapping','scaled');
if nargin < 4,titleuse='TILE 2';else titleuse=[titlestr ' TILE 2'];end
title(titleuse,'FontSize',14,'FontName',font,'FontWeight','bold');    
caxis([cmin cmax]);                                                   
axis ([0 8 0 8]); axis square;                                        
%xlabel('det col');                                                   
ylabel('det row');                                                    
set(gca,'XTick',0.5:7.5,'XTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YTick',0.5:7.5,'YTickLabel',char('1','2','3','4','5','6','7','8'),...    
    'YDir','reverse','XAxisLocation','bottom',...                             
    'TickLength',[.001 .001]);                                                
text(.25,.7,'A');                                                             
text(.6,.3,'B'); hold on;                                                     
for i=0:8,                                                                    
plot([0:8],repmat(i,1,9),'k','LineWidth',2);                                  
plot(repmat(i,1,9),[0:8],'k','LineWidth',2);                                  
end; hold off;                                                                

%TILE 3
subplot(2,2,4,'Position',[.5 .08 .4 .4])
set (gca, 'color', [0 0 0]);
h3=patch(x,y,c3,'CDataMapping','scaled');
if nargin < 4,titleuse='TILE 3';,else,titleuse=[titlestr ' TILE 3'];end
t1=title(titleuse,'FontSize',14,'FontName',font,'FontWeight','bold');  
pos1=get(t1,'position');                                               
pos1(2)=pos1(2)-8.9;                                                   
set(t1,'position',pos1);                                               
caxis([cmin cmax]);                                                    
axis ([0 8 0 8]); axis square;                                         
xlabel('det col');                                                     
ylabel('det row');                                                     
set(gca,'XTick',0.5:7.5,'XTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YTick',0.5:7.5,'YTickLabel',char('1','2','3','4','5','6','7','8'),...
    'XAxisLocation','top','TickLength',[.001 .001],'XDir','reverse');
text(.4,.7,'A');
text(.75,.3,'B'); hold on;
for i=0:8,
plot([0:8],repmat(i,1,9),'k','LineWidth',2);
plot(repmat(i,1,9),[0:8],'k','LineWidth',2);
end; hold off;

%TILE 4

subplot(2,2,3,'Position',[.1 .08 .4 .4])
set (gca, 'color', [0 0 0]);
h4=patch(x,y,c4,'CDataMapping','scaled');
if nargin < 4,titleuse='TILE 4';,else,titleuse=[titlestr ' TILE 4'];end
t2=title(titleuse,'FontSize',14,'FontName',font,'FontWeight','bold');
pos2=get(t2,'position');
pos2(2)=pos2(2)-8.9;
set(t2,'position',pos2);
caxis([cmin cmax]);
axis ([0 8 0 8]); axis square;
xlabel('det col');
%ylabel('det row');
set(gca,'XTick',0.5:7.5,'XTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YTick',0.5:7.5,'YTickLabel',char('1','2','3','4','5','6','7','8'),...
    'YAxisLocation','right','XAxisLocation','top','TickLength',[.001 .001],...
    'XDir','reverse');
text(.4,.7,'A');
text(.75,.3,'B'); hold on;
for i=0:8,
plot([0:8],repmat(i,1,9),'k','LineWidth',2);
plot(repmat(i,1,9),[0:8],'k','LineWidth',2);
end; hold off;

text (-0.1, 1.05, [num2str(colorbar_axis(1),'%.1f') '-' num2str(colorbar_axis(2),'%.1f')], ...
	'units', 'normalized', 'rotation', 90, 'horizontalalignment', 'center');
fnum_out = fnum;
