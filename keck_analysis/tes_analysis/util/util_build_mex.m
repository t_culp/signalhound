function util_build_mex

% Find directory with arcfile functions
p = mfilename ('fullpath');
util_dir = fileparts (p);

% Where mex source and binaries live
mex_code_dir = fullfile (util_dir, '..', 'mex_code', 'util');
mex_bin_dir = fullfile (util_dir, '..', 'mex_bin');

% Create binary directory if it's not there
if ~exist (mex_bin_dir, 'dir')
	mkdir (mex_bin_dir);
	% Also add to path
	addpath (mex_bin_dir);
end;

build_mex({'mce_unpack.c','data_mode.c'},'mce_unpack',mex_bin_dir,mex_code_dir);
build_mex({'tab_by_bin.c'},'tab_by_bin',mex_bin_dir,mex_code_dir);
build_mex({'stats_by_bin.c'},'stats_by_bin',mex_bin_dir,mex_code_dir);

return;

function build_mex(src_list,out_name,mex_bin_dir,mex_code_dir)
	for (ii=1:length (src_list))
		src_list{ii} = fullfile (mex_code_dir, src_list{ii});
	end;
	if exist ('OCTAVE_VERSION', 'builtin')
		mex (src_list{:}, '-o', fullfile (mex_bin_dir, [out_name '.' mexext]));
	else
		mex (src_list{:}, '-outdir', mex_bin_dir, '-output', out_name);
	end;
	return;
