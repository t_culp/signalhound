function [mce_row mce_col] = gcp2mce (chan, flag)
% GCP2MCE  map from GCP channel number (0-527)
% to mce coordinates (0-15 x 0-32)
%
% GCP2MCE(CHAN,'run4') uses the channel assignments
% from Run 4 and earlier.
%

% RWO 090511

if (nargin < 2) || isempty (flag)
	map_type = 5;
elseif ischar (flag)
	flag = lower(flag);
	flag (flag == ' ') = '';
	if strcmp (flag, '4') || strcmp (flag, 'run4')
		map_type = 4;
	elseif strcmp (flag, '5') || strcmp (flag, 'run5')
		map_type = 5;
        % GPT: adding a kluge for K3_2, but I don't know the details.
        elseif strcmp (flag, 'k3_2')
                map_type = 5;
	else error (['Unknown flag ' flag]);
	end;
elseif isnumeric (flag)
	map_type = flag;
else error (['Don''t know what to do with flag of type ' class(flag)]);
end;

% Note that nothing here depends on tile coordinates

switch (map_type)
	case 4,    mce_col = mod (chan, 2*8);
                   mce_row = floor (chan / (2*8));
        case 5,    mce_row = mod (chan, 33);
                   mce_col = floor (chan / 33);
        otherwise, mce_row = [];
                   mce_col = [];
end;

