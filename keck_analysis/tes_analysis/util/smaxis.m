function h = smaxis (varargin)
% Same as AXIS, but catches all kinds
% of annoying little errors in a
% sensible way.

if (nargin == 0)
	h = axis;
	return;
end;

tmp = varargin;
if (nargin >= 2) && isnumeric (varargin{2})
	tmp{2} = smack (varargin{2});
elseif (nargin == 1) && isnumeric (varargin{1})
	tmp{1} = smack (varargin{1});
else
	disp ('All text or something?');
end;

try
axis (tmp{:});
catch
keyboard;
end;

%%%%

function y = smack (x)
	y = x;
	[y(1) y(2)] = subsmack (x(1), x(2));
	[y(3) y(4)] = subsmack (x(3), x(4));

function [a b] = subsmack (c, d)
	a = real (c);
	b = real (d);
	if (b < a)
		[b a] = deal (a, b);
	end;
	if (b == a)
		if (a == 0)
			b = 1e-10;
			a = -b;
		else
			b = b * (1 + 1/10);
			a = a * (1 - 1/10);
		end;
	end; 
