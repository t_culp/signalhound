function nf_beam_plotter(x,y,px,sq,ra,gauss,rotate,theta,orto)
% x = x coord of scan
% y = y coord of scan
% px(row, col, tile) = structure containing element px.a_map and px.b_map
% ra = z range for plots
% gauss - set keyword to do gaussian fit instead of using max value
% for A/B difference maps
% rotate - set keyword to rotate images
% theta = angle by which image is rotated
% orto - set to 'small' to generate only thumbnails, set to 'large' to
% only generate large maps.

figure(1)
cf=gcf;
colormap jet

if nargin<9
  orto=[];
end

if nargin<6
gauss=[];
end

if nargin<7
rotate=[];
end

%set default z range if no colorscale is given
if nargin==4
  ra=30;
end

%Individual plots
% Thumbnails
if strcmp(orto,'small') || isempty(orto);
  if strcmp(orto,'small'), disp('Skipping large'), end
  disp('Small maps')
  for rr=1:8
    for cc=1:8
      for ti=1:4
	%rotate maps if needed
	if strcmp(rotate,'rotate')
	  A=imrotate(px(rr,cc,ti).a_map,theta,'bicubic');
	  B=imrotate(px(rr,cc,ti).b_map,theta,'bicubic');
	else
	  A=px(rr,cc,ti).a_map;
	  B=px(rr,cc,ti).b_map;
	end
	
	%set up and plot for A
	imagesc(x,x,A)
	set(gca,'YDir','normal')
	axis square
	caxis([0,ra])
	set(gca,'xtick',[],'ytick',[])
	img_str=['small/det_row_' int2str(rr) '_col_' int2str(cc) '_tile_' int2str(ti) '_A_sm'];
	print('-dpng', img_str);
	clf
	%set up and plot for B
	imagesc(x,x,B)
	set(gca,'YDir','normal')
	axis square
	caxis([0,ra])
	set(gca,'xtick',[],'ytick',[])
	img_str=['small/det_row_' int2str(rr) '_col_' int2str(cc) '_tile_' int2str(ti) '_B_sm'];
	print('-dpng', img_str);
	clf
	
	%check to make sure both maps exist, and if so do difference.
	if ~isempty(A) && ~isempty(B)
	  %if fit is desired for normalization, do fit.
	  if strcmp(gauss,'gauss')
	    disp('doing gauss fit')
	    %fit on A
	    isnana=find(isnan(A));                 
	    A(isnana)=0 ;
	    norma=normfit2d([1:size(A,2)], [1:size(A,1)], A);	    
	    %fit on B
	    isnanb=find(isnan(B));                 
	    B(isnanb)=0 ;
	    normb=normfit2d([1:size(B,2)], [1:size(B,1)], B);
	    %make difference image
	    imagesc(x,x,A/norma(1)-B/normb(1))
	  else
	    imagesc(x,x,A/max(max(A(10:end-10,10:end-10)))-B/max(max(B(10:end-10,10:end-10))))
	  end
	  %set up and plot for A-B
	  set(gca,'YDir','normal')
	  axis square
          caxis([-.1,.1])
          set(gca,'xtick',[],'ytick',[])
          img_str=['small/det_row_' int2str(rr) '_col_' int2str(cc) '_tile_' int2str(ti) '_dif_sm'];
          print('-dpng', img_str);
	  clf        
	end 
      end
    end
  end

  disp('Shrinking')
  system('mogrify -resize ''7%x7%'' small/*')
end %end small maps

if strcmp(orto,'large') || isempty(orto)
if strcmp(orto,'large'), disp('skipping small'), end
disp('Large maps')
%Individual plots
for rr=1:8
  for cc=1:8
    for ti=1:4
      %rotate as needed
      if strcmp(rotate,'rotate')
	A=imrotate(px(rr,cc,ti).a_map,theta,'bicubic');
	B=imrotate(px(rr,cc,ti).b_map,theta,'bicubic');
      else
	A=px(rr,cc,ti).a_map;
	B=px(rr,cc,ti).b_map;
      end
      %plot for A
      imagesc(x,(x),A)
      caxis([0,ra])
      set(gca,'YDir','normal')
      axis square
      xlabel('Inches')
      ylabel('Inches')
      colorbar
      title(['Tile ' int2str(ti) ' Row ' int2str(rr) ' Col ' int2str(cc) ' Pol A'])
      img_str=['full/det_row_' int2str(rr) '_col_' int2str(cc) '_tile_' int2str(ti) '_A'];
      print('-dpng', img_str);
      clf
      %plot for B
      imagesc(x,(x),B)
      set(gca,'YDir','normal')
      caxis([0,ra])
      axis square
      xlabel('Inches')
      ylabel('Inches')
      colorbar
      title(['Tile ' int2str(ti) ' Row ' int2str(rr) ' Col ' int2str(cc) ' Pol B'])
      img_str=['full/det_row_' int2str(rr) '_col_' int2str(cc) '_tile_' int2str(ti) '_B'];
      print('-dpng', img_str);
      clf
      %check that A and B exist and if so do the difference map
      if ~isempty(A) && ~isempty(B)
	%if gauss fit called for normalization
	if strcmp(gauss,'gauss')
	  %gauss fit for A
	  isnana=find(isnan(A));                 
	  A(isnana)=0; 
	  norma=normfit2d([1:size(A,2)], [1:size(A,1)], A);
	  %gauss fit for B
	  isnanb=find(isnan(B));                 
	  B(isnanb)=0; 
	  normb=normfit2d([1:size(B,2)], [1:size(B,1)], B);
	  %make image
	  imagesc(x,x,A/norma(1)-B/normb(1))
	else
	  imagesc(x,x,A/max(max(A(10:end-10,10:end-10)))-B/max(max(B(10:end-10,10:end-10))))
	end 
	set(gca,'YDir','normal')
	axis square
	xlabel('Inches')
	ylabel('Inches')
	colorbar
	caxis([-.1,.1])
	title(['Tile ' int2str(ti) ' Row ' int2str(rr) ' Col ' int2str(cc) ', A-B'])
	img_str=['full/det_row_' int2str(rr) '_col_' int2str(cc) '_tile_' int2str(ti) '_dif'];
	print('-dpng', img_str);
      end
    end
  end
end

%Plot dark Squids
dsq=[1,34,67,100,133,166,199,232,265,298,331,364,397,430,463,496];
for i=1:16
  A=sq(i).map;
  imagesc(x,(x),A)
  set(gca,'YDir','normal')
  axis square
  xlabel('Inches')
  ylabel('Inches')
  colorbar
  title(['Dark Squid, GCP=' int2str(dsq(i))])
  img_str=['full/dark_sq_gcp' int2str(dsq(i))];
  print('-dpng', img_str);
end

end
