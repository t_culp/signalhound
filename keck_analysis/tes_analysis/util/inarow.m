% S=INAROW(X) -- running sums of values in a row
%
% This is similar to CUMSUM, except that the sums
% get reset on encountering a zero value (or other
% separator value as specified).
%
% 
function s=inarow(x,varargin)

OPT.keyval = 0;
OPT.running = true;
OPT = parse_opts(OPT,varargin{:});

if isfinite(OPT.keyval)
  c = (x~=OPT.keyval);
else
  c = isfinite(x);
end
x(~isfinite(x)) = 0;

s = inarow_helper(x,c);
if ~OPT.running
  s2 = inarow_helper(x(end:-1:1),c(end:-1:1));
  s = s + s2(end:-1:1);
  s(c) = s(c) - x(c);
end

return


function s=inarow_helper(v,c)

v(~c)=0;

clast=false(size(c));
clast(2:end)=c(1:(end-1));

w=cumsum(v);

wlast=zeros(size(v));
wlast(1)=0;
wlast(2:end)=w(1:(end-1));

e=wlast(c & ~clast);
f=diff([0,reshape(e,1,[])]);

g=zeros(size(v));
g(c & ~clast) = f;

d=v - g;
s=cumsum(d);
s(~c)=0;

return


function opt = parse_opts(opt,varargin)

for i=1:2:length(varargin)
  prop = varargin{i};
  if ~ischar(prop)
    error(['Parameter-value pair property names must be strings.']);
  end
  if length(varargin)<(i+1)
    error(['Parameter "' prop '" has no value specified.']);
  end
  val = varargin{i+1};
  if isfield(opt,lower(prop))
    opt.(lower(prop)) = val;
  else
    error(['Unrecognized parameter name "' prop '".']);
  end
end

return
