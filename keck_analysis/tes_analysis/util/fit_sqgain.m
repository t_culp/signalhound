function [g123 g1 g2 g3 fb1 fb2 fb3] = ...
    fit_sqgain(tundir,servo,s1fb,s2fb,safb)
% function [g123 g1 g2 g3 fb1 fb2 fb3] = ...
%   fit_sqgain(tundir,servo,s1fb,s2fb,safb)
% Returns the open-loop gains of the various SQUID stages at lockpoint,
% based upon fits to data from auto_setup_squids.pro.
%
% INPUTS
% - tundir: directory containing the particular set autotune data files
% - OPTIONAL: servo is a three-element vector [slope1 slope2 slope3] giving
%   the slopes on which each stage locks: rising (falling) = +1 (-1)
%   If omitted, assumes [-1 -1 -1].  In reality, these are set according
%   to the rules on the Locking_slopes page of the MCE wiki.  We could read
%   all of this from the experiment.cfg file, but that gets overwritten so
%   we wouldn't be certain of having the right value.  In all cases, the
%   overall locking slope for sq1rampc is set by the sign of the PID gain.
% - OPTIONAL: s1fb, s2fb, safb are feedback values in use during partial-
%   stage data runs.  If omitted (or empty), the code makes good guesses.
%
% OUTPUTS
%   - g123 = (OUTPUT ADC) / (FB1 DAC)   [from sq1rampc]
%     This is a (numrows)x(numcols) array, one entry per pixel
%   - g1 = (FB2 DAC) / (FB1 DAC)        [from sq1servo]
%     This is a (numrows)x(numcols) array, one entry per pixel
%   - g2 = (FBA DAC) / (FB2 DAC)        [from sq2servo]
%     This is a 1x(numcols) array, one entry per column
%   - g3 = (OUTPUT DAC) / (FBA DAC)     [from ssa]
%     This is a 1x(numcols) array, one entry per column
%   - fb1 = SQ1 FB DAC values at lockpoint
%   - fb2 = SQ2 FB DAC values at lockpoint
%   - fb3 = SSA FB DAC values at lockpoint
%
% GENERAL NOTES
% - The open-loop gain from FB1 to output with all stages on is g123
% - The open-loop gain from FB2 to output with 2+A on is g2*g3
% - The open-loop gain from FBA to output with SSA on is g3
% - We should have g123=g1*g2*g3, but this is not a complete check since we
%   commonly compute g1 for only one row
% - These gains are bins-to-bins; to get gains for currents or voltages the 
%   user would need to scale these values by various circuit resistances 
%   and DAC/ADC specs.
%
% - FUTURE NOTES: This code should be able to figure out the locking
%   slopes on its own.
%
% =======
% 090312  JPF  Created
% 090313  JPF  Version to handle all pixels with a single file load
% 090318  JPF  Fixed feedback arguments; tweaked help
% 090325  JPF  Upgraded to handle ramp_sa_bias on; still have problems when
%              we don't specify sa_fb
% 090331  JPF  Upgraded to grab correct lockpoints from run files by
%              default.  Now returns g1 and fb*.
% 090417  JPF  Use correct (falling) slope for SSA; read correct sq1rampc
%              slope from PID parameters.
% 090908  JPF  Fix slope defaults, now works with fast SQ2 fb (but NOT
%              with a BAC but no fast fb)

verbose = 0;

numcols = 16;
numrows = 33;

nfit = 6; % window width (bins) for timestream filtering (even for now)

useBAC = 0; % Assume that we are NOT using a BAC (will check later)

% pre-allocate
g123=NaN(numrows,numcols);
g1=NaN(numrows,numcols);
g2=NaN(1,numcols);
g3=NaN(1,numcols);
fb1=NaN(numrows,numcols);
fb2=NaN(1,numcols);
fb3=NaN(1,numcols);

% check for feedback arguments
if nargin<5
    s1fb = [];
end
if nargin<4
    s2fb = [];
end
if nargin<3
    safb = [];
end
if nargin<2 || isempty(servo)
    servo = [-1 +1 +1];
end

% pull out servo slopes
ssa_slope = servo(3); %
sq2servo_slope = servo(2); % 
sq1servo_slope = servo(1); % 
% specify overall_slope for sq1rampc after we've loaded the data files

% ===== 0. LOAD ALL FILES =====
% Load all of the data files at the start.  The files are small, and this 
% simplifies the rest of the code.
if ~exist(tundir,'dir')
    error(['No directory ' tundir ' found!']);
end
% directory exists
if verbose
    disp(['  Loading all data files from ' tundir]);
end
for rc=1:2
    % cycle through the RC cards, each with a different data file
    rcn = ['rc' int2str(rc)];
    % === SSA ===
    fpattern_ssa = fullfile(tundir,['*_RC' int2str(rc) '_ssa']);
    [Sssa.(rcn) Rssa.(rcn) Fssa.(rcn)] = load_datafile(fpattern_ssa);
    % === SQ2SERVO ===
    fpattern_s2 = fullfile(tundir,['*_RC' int2str(rc) '_sq2servo']);
    [Ss2.(rcn) Rs2.(rcn) Fs2.(rcn)] = load_datafile(fpattern_s2);
    Bs2.(rcn) = read_mce_biasfile([Fs2.(rcn) '.bias']);
    % === SQ1SERVO ===
    fpattern_s1 = fullfile(tundir,['*_RC' int2str(rc) '_sq1servo']);
    [Ss1.(rcn) Rs1.(rcn) Fs1.(rcn)] = load_datafile(fpattern_s1);
    try
        Bs1.(rcn) = read_mce_biasfile([Fs1.(rcn) '.bias']);
    catch
        useBAC = 1; % we must be using a BAC
        for rr = 0:(numrows-1)
            rstr = ['r' num2str(rr,'%2.2d')];
            Bs1.(rcn).(rstr) = read_mce_biasfile( ...
                [Fs1.(rcn) '.' (rstr) '.bias']);
        end
    end
    % === SQ1RAMPC ===
    fpattern_s1r = fullfile(tundir,['*_RC' int2str(rc) '_sq1rampc']);
    [Ss1r.(rcn) Rs1r.(rcn) Fs1r.(rcn)] = load_datafile(fpattern_s1r);
    % === LOCK ===
    fpattern_lock = fullfile(tundir,['*_RC' int2str(rc) '_lock']);
    [Slock.(rcn) Rlock.(rcn) Flock.(rcn)] = load_datafile(fpattern_lock);
end
% now we've got all the data we'll need for later

% ===== 1. SSA-ONLY GAIN: SSA =====
if verbose
    disp('  Fitting for SSA ...')
end
for rc=1:2
    rcn = ['rc' int2str(rc)];
    if strcmp(Rssa.(rcn).par_ramp.loop_list,'loop1')
        % we didn't ramp_sa_bias, so this is a simple fb ramp
        % A. GRAB THE BIAS RAMP
        % extract the input ramp parameters from the run file
        loopvals = Rssa.(rcn).par_ramp.par_step.loop1.par1;
        % contains [start step numpoints]
        bias = loopvals(1) + loopvals(2)*([0:(loopvals(3)-1)]');
        % B. GRAB THE DATA
        % extract the error signal from the data file
        % note that we extract the mean of all rows within each column
        out = squeeze(mean(reshape([Sssa.(rcn)(:).err],[],numrows,8),2));
        blah = [Sssa.(rcn)(:).err];
        disp(int2str(size(blah)))
        disp(int2str(blah(1:5,1:5)))
        % each column is a pixel
    else
        % BUT: if we enabled ramp_sa_bias during auto_setup_squids 
        % then there are two loops: loop1 over bias, loop2 over fb
        % A. GRAB BOTH BIAS RAMPS
        loopvalsbias = Rssa.(rcn).par_ramp.par_step.loop1.par1;
        rampbias = loopvalsbias(1) + ...
            loopvalsbias(2)*([0:(loopvalsbias(3)-1)]');
        loopvalsfb = Rssa.(rcn).par_ramp.par_step.loop2.par1;
        rampfb = loopvalsfb(1) + ...
            loopvalsfb(2)*([0:(loopvalsfb(3)-1)]');
        rampbias = reshape(repmat(rampbias',loopvalsfb(3),1),[],1);
        bias = rampfb;  % this is the x-axis of the SSA plot
        % B. GRAB THE DATA
        % extract the error signal from the data file
        % but ONLY the ramp portion from the appropriate bias
        % note that we extract the mean of all rows within each column
        out0 = squeeze(mean(reshape([Sssa.(rcn)(:).err],[],numrows,8),2));
        % each column is a pixel
        % choose the appropriate ramp portions
        out = zeros(loopvalsfb(3),8);
        % look up each column's bias
        biastarget = Rs2.(rcn).header.sa.bias;
        for col=1:8
            out(:,col) = out0(find(rampbias==biastarget((rc-1)*8+col)),col);
        end
        clear out0 rampbias rampfb biastarget;
    end
    
    % smooth the data a bit    
    out = sgolayfilt(out,2,11);
    
    % Cycle through the COLUMNS
    for col=1:8
        % choose a lockpoint at which to measure the slope
        if isempty(safb)
            % we weren't told, so we need to look up the right safb value
            % this is determined by sa_offset and upward-going slope
            net_offset = Rssa.(rcn).header.rc(rc).sample_num*(...
                Rs2.(rcn).header.rc(rc).(['adc_offset' int2str(col-1)]) - ...
                Rssa.(rcn).header.rc(rc).(['adc_offset' int2str(col-1)]));
            % measure FB and slope at likely lockpoint
            [fb3(col+(rc-1)*8) g3(col+(rc-1)*8)] = find_zero_crossing( ...
                bias,out(:,col)-net_offset(col),ssa_slope,nfit);
        else
            % we were given a value, so use it
            [dummy indcross] = min(abs(bias-safb(col+(rc-1)*8)));
            % fit a line in the vicinity
            indfit = floor(indcross-nfit/2)+(1:nfit);
            afit = polyfit(bias(indfit),out(indfit,col),1);
            g3(col+(rc-1)*8)=afit(1);
            fb3(col+(rc-1)*8)=safb(col+(rc-1)*8);
        end
    end
end
clear bias out
if verbose
    disp('    Fitting SSA complete.');
end



% ===== 2. 2nd-STAGE GAIN: SQ2SERVO =====

if verbose
    disp('  Fitting for SQ2SERVO ...')
end
for rc=1:2
    rcn = ['rc' int2str(rc)];
    % A. GRAB THE BIAS RAMP
    % extract the input ramp parameters from the run file
    % Note that there are two loops (as in ramp_sa_bias), but in this case
    % we don't care: the outer loop just runs over [full_bias 0_bias], so
    % we just extract the first part of the data
    loopvals = Rs2.(rcn).par_ramp.par_step.loop2.par1;
    % contains [start step numpoints]
    bias = loopvals(1) + loopvals(2)*([0:(loopvals(3)-1)]');

    % B. GRAB THE DATA
    % In this servo configuration, we extract the signal
    % from the BIAS file.
    for col=1:8
        rc_col = (col-1) + (rc-1)*8;
        out(:,col) = Bs2.(rcn).(['ssafb' sprintf('%02d',rc_col)])';
    end
    % each column is a pixel
    % smooth the data a bit    
    out = sgolayfilt(out,2,11);
    
    % Cycle through the columns
    for col=1:8
        % choose a lockpoint at which to measure the slope
        if isempty(s2fb)
            % we weren't told, so we need to look up the right sq2fb value
            % this is determined by final sa_fb and downward-going slope
            lp = Rs1.(rcn).header.sa.fb(col+(rc-1)*8);
            % measure FB and slope at likely lockpoint
            [fb2(col+(rc-1)*8) g2(col+(rc-1)*8)] = find_zero_crossing( ...
                bias,out(:,col)-lp,-1,nfit);
        else
            % we were given a value, so use it
            [dummy indcross] = min(abs(bias-s2fb(col+(rc-1)*8)));
            % fit a line in the vicinity
            indfit = floor(indcross-nfit/2)+(1:nfit);
            afit = polyfit(bias(indfit),out(indfit,col),1);
            g2(col+(rc-1)*8)=afit(1);
            fb2(col+(rc-1)*8)=s2fb(col+(rc-1)*8);
        end
    end
end
clear bias out;
if verbose
    disp('    Fitting SQ2SERVO complete.');
end


% ===== 3. 1st-STAGE GAIN: SQ1SERVO =====

if verbose
    disp('  Fitting for SQ1SERVO ...')
end
for rc=1:2
    rcn = ['rc' int2str(rc)];
    if useBAC
        servo_row = repmat({0:(numrows-1)},1,8); % all rows available
    else
        servo_row = num2cell(Rs1.(rcn).servo_init.row_init+1); % one row only
    end
    
    % A. GRAB THE BIAS RAMP
    % extract the input ramp parameters from the run file
    % Note that there are two loops (as in ramp_sa_bias), but in this case
    % we don't care: the outer loop just runs over [full_bias 0_bias], so
    % we just extract the first part of the data
    loopvals = Rs1.(rcn).par_ramp.par_step.loop2.par1;
    % contains [start step numpoints]
    bias = loopvals(1) + loopvals(2)*([0:(loopvals(3)-1)]');

    % Cycle through the columns
    for col=1:8
        rc_col = (col-1) + (rc-1)*8;
        
        if useBAC
            % many-column version
            for rr = servo_row{col}
                rstr = ['r' num2str(rr,'%2.2d')];
                out = Bs1.(rcn).(rstr).(['sq2fb' ...
                    sprintf('%02d_r%02d',rc_col,rr)]);
                % smooth the data a bit
                out = sgolayfilt(out,2,11);
                % choose a lockpoint at which to measure the slope
                if isempty(s1fb)
                    % we weren't told, so we need to look up the right sq2fb value
                    % this is determined by final sa_fb and downward-going slope
                    lp = Rs1r.(rcn).header.sq(2).( ...
                        ['fb_col' int2str(col-1)])(col+(rc-1)*8);
                    % measure FB and slope at likely lockpoint
                    [dummy g1(rr+1,col+(rc-1)*8)] = ...
                        find_zero_crossing(bias,out-lp,-1,nfit);
                else
                    % we were given a value, so use it
                    [dummy indcross] = min(abs(bias-s1fb(col+(rc-1)*8)));
                    % fit a line in the vicinity
                    indfit = floor(indcross-nfit/2)+(1:nfit);
                    afit = polyfit(bias(indfit),out(indfit),1);
                    g1(servo_row(col),col+(rc-1)*8)=afit(1);
                end
            end
        else
            % one-column version
            out = Bs1.(rcn).(['sq2fb' sprintf('%02d',rc_col)]);
            % smooth the data a bit
            out = sgolayfilt(out,2,11);
            % choose a lockpoint at which to measure the slope
            if isempty(s1fb)
                % we weren't told, so we need to look up the right sq2fb value
                % this is determined by final sa_fb and downward-going slope
                lp = Rs1r.(rcn).header.sq(2).fb(col+(rc-1)*8);
                % measure FB and slope at likely lockpoint
                [dummy g1(servo_row(col),col+(rc-1)*8)] = ...
                    find_zero_crossing(bias,out-lp,-1,nfit);
            else
                % we were given a value, so use it
                [dummy indcross] = min(abs(bias-s1fb(col+(rc-1)*8)));
                % fit a line in the vicinity
                indfit = floor(indcross-nfit/2)+(1:nfit);
                afit = polyfit(bias(indfit),out(indfit),1);
                g1(servo_row(col),col+(rc-1)*8)=afit(1);
            end
        end
    end
    
end
if verbose
    disp('    Fitting SQ1SERVO complete.');
end


% ===== 4. ALL-STAGE GAIN: SQ1RAMPC =====
if verbose
    disp('  Fitting for SQ1RAMPC ...')
end
% cycle through the RC cards, since each fills a different data file
for rc=1:2
    rcn = ['rc' int2str(rc)];
    % A. GRAB THE BIAS RAMP
    % extract the input ramp parameters from the run file
    loopvals = Rs1r.(rcn).par_ramp.par_step.loop1.par1;
    % contains [start step numpoints]
    bias = loopvals(1) + loopvals(2)*([0:(loopvals(3)-1)]');
    % B. GRAB THE DATA
    % extract the error signal from the data file
    % note that we extract the mean of all rows within each column
    out = [Ss1r.(rcn)(:).err];
    % each column is a pixel
    npx = size(out,2);
    
    % smooth the data a bit
    out = sgolayfilt(out,2,11);
    
    % Cycle through the pixels
    for px=1:npx
        [r c] = ind2rc(px);
        % DETERMINE THE LOCKING SLOPE
        % this is opposite to the sign of gaini in locked mode
        overall_slope = -sign(Rlock.(rcn).header.rc(rc).( ...
            ['gaini' int2str(mod(c,8))])(r+1));
        if overall_slope==0
            % the PID isn't set, so we can't actually lock...
            if verbose
                disp(['WARNING: no gainI set for r' ...
                    int2str(r) 'c' int2str(c) ', assuming -1'])
            end
            overall_slope=-1;
        end
        % FIND THE LOCK POINT IN FB
        if isempty(s1fb)
            % we weren't told, so we need to look up the right s1fb value
            % we're all configured, so the lockpoint is zero
            % measure FB and slope at likely lockpoint
            [fb1(px+(rc-1)*npx) g123(px+(rc-1)*npx)] = ...
                find_zero_crossing(bias,out(:,px),overall_slope,nfit);
        else
            % we were given a value, so use it
            [dummy indcross] = min(abs(bias-s1fb(c+1)));
            % fit a line in the vicinity
            indfit = floor(indcross-nfit/2)+(1:nfit);
            afit = polyfit(bias(indfit),out(indfit,col),1);
            g123(px+(rc-1)*npx)=afit(1);
        end
    end
end
clear bias out
if verbose
    disp('    Fitting SQ1RAMPC complete.');
end



% =====================
%   Utility functions
% =====================

function [S R fname] = load_datafile(fpattern)
% returns desired data file and its name, based upon a search pattern
% (e.g. including wildcards)
tundir = fileparts(fpattern);
dd = dir(fpattern);
if length(dd)~=1
    error(['Error in fit_sqgain->load_data: No file (or many files) ' ...
           'satisfying ' fpattern]);
    S = [];
    R = [];
    fname = [];
    return;
end
fname = fullfile(tundir,dd.name);
[S R] = read_mce(fname,[],[],'DO_UNWRAP',0);

function [xv der] = find_zero_crossing(xx,yy,slope,nfit,xguess)
% Using the sampled function yy=f(xx), finds an x-value xv at which the
% function crosses zero in the direction indicated by slope (+1 = upward, 
% -1 = downward).  There are often multiple zero-crossings, so it chooses
% one vaguely near the middle of the range of xx.  If desired, also returns
% an estimate der of the derivative of f at (xv,0).  These estimates are
% computed using polynomial interpolation with a window nfit.
%
% Optionally, we can specify a guess for the x-value of the lockpoint.
% The code then picks the closest available lockpoint to xguess.
if nargin<5 || isempty(xguess)
    xguess = median(xx); % midpoint
end

% Find zero-crossings in the desired direction
lpcross = find(slope*yy(1:(end-1)) <= 0 & slope*yy(2:end) > 0);
% Keep only those crossings which are sufficiently far from the edges
lpcross = lpcross(inrange(lpcross,nfit/2+1,length(xx)-nfit/2-1));
if isempty(lpcross)
    xv = NaN;
    der = NaN;
else
    % we found a crossing point, so we can go on
    % choose the crossing point closest to xguess
    [dummy ind] = min(abs(xx(lpcross)-xguess));
    indcross = lpcross(ind);
    xind = indcross + [-nfit/2:nfit/2];
    % interpolate the crossing point
    xv = round(interp1(yy(xind),xx(xind),0,'pchip'));
    if nargout>1
        % fit a cubic in this area
        % subtract the lockpoint for better performance
        afit = polyfit(xx(xind)-xv,yy(xind),3);
        % find the slope of that cubic at the lockpoint
        der = polyval(polyder(afit),0);
    end
end