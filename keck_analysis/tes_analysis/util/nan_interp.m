function A = nan_interp(B)
% A = nan_interp(B)
%
% Function to replace NaNs in a map with interpolated values from the rest
% of the map

A = B;
[m n] = size(B);
[C R] = meshgrid(1:n,1:m);
bad = isnan(B);
c = C(bad);
r = R(bad);

for ii = 1:length(c)
  [cols rows] = nearest_neighbor(c(ii),r(ii),m,n);
  A(r(ii),c(ii)) = nanmean(nanmean(B(rows,cols)));
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [cols rows] = nearest_neighbor(c,r,m,n)

cols = [c-1,c,c+1];
rows = [r-1,r,r+1];
cols = cols(cols<n & cols>0);
rows = rows(rows<m & rows>0);

return

