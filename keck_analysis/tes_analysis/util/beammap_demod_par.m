function beammap_demod_par(t1,t2,run,dk_tar,par,type,days,rxNum,notlastscan)
% beammap_demod_par(t1,t2,run,dk_tar,par,type,days,rxNum,notlastscan)
% 
% Reduction of beammap timestreams 
%
% The Az and El coordinates are reformed accordingly.
% Data products are saved to scratch_local
% The data products include bm_*.mat and pos_*.mat.  bm_*.mat includes all
% of the demodulated timestreams.  There is one file saved for every
% corresponding arc file.  For each scan, there is a single pos_*.mat file
% that stores all of the position information across the entire raster.
% The file is named according to the time stamp of the first arc file
% included in the raster.
%
% t1 = start time (UTC)
% t2 = stop time (UTC)
% run = B2 beammapping run
%       'r5': B2 run 5, 'r8': B2 run 8
%       'k0r4': K0 run 4, 'k0r4t2': K0 run 4 tile 2 only
%       'h083': K7 run oct 2013
%       'k--': Keck at pole
% dk_tar = Target for dk angle (optional argument).  If supplied, will cut
%          data to include only data at the user-supplied dk angle. 
%          Useful for pixel pol rasters.
% par: set to 1 to run in parallel.  Default is 0.
% days: days of pxpol measurements - must be used with type 'pxpol'
% rxNum, insert the rx number, or 'all' to demodulate all receivers at once
% notlastscan = 0 by default
%             = 1 keeps the last scan if it's incomplete,
%               does not demodulate that scan
%               and attaches that scan to the following file. 
%               We have to start this from the beginning of the beam
%               map since the code is not smart enough to figure out
%               whether or not the beginning of the file is a complete
%               scan. This is slow, and takes something like 11 hours to
%               demodulate an entire beam map (7 hours).
%
% Full example:
% beammap_demod_par('2010-nov-20:07:00:24','2010-nov-20:11:52:43','cos','r81',90,1,0)
% RWA 20101027
% Rev .2 RWA 20101216
% Last edit: CLW 20140514, added notlastscan option

% Check input argument:
if ~exist('rxNum','var')
  rxNum=0;
end
if ~exist('dk_tar','var')
  dk_tar=[];
end
if ~exist('par','var')
  par=0;
end
if ~exist('type','var') | type==0
  moon=0;
  pxpol=0;
  type=0;
  sidelobe=0;
  fisheye=0;
end
if ~exist('notlastscan','var') 
  notlastscan=0;
end
if isempty(rxNum)
  rxNum=0;
end
if isempty(par)
  par=0;
end
if isempty(type)
  moon=0;
  pxpol=0;
  type=0;
  sidelobe=0;
  fisheye=0;
  
end
if type~=0
  if strcmp(type,'moon')
    moon=1;
    pxpol=0;
    sidelobe=0;
    fisheye=0;
  elseif strcmp(type,'pxpol')
    pxpol=1;
    moon=0;
    sidelobe=0;
    fisheye=0;
  elseif strcmp(type,'sidelobe')
    pxpol=0;
    moon=0; 
    sidelobe=1;
    fisheye=0;
  elseif strcmp(type,'fisheye')
    pxpol=0;
    moon=0; 
    sidelobe=1; 
    fisheye=1;
  else
    error('unrecognized `type` argument')
  end  
end
% Identify run:

%Harvard highbay
if strncmp(run,'h',1)
  highbay = 1;
  run = 'k083';
else
  highbay = 0;
end

if strcmpi(deblank(run),'r5')
  % Run 5 - Fall 2009 Bicep2 highbay, no mirror
  run=5;
elseif strcmpi(deblank(run),'r8')
  % Run 8 - Pole summer 2009-2010, far-field flat mirror
  run=8;
elseif strcmpi(deblank(run),'k0r4')
  % keck0 run 4, harvard highbay, mini-far-field flat
  run=40;
elseif strcmpi(deblank(run),'r81')
  % Run 8.1 - Pole summer 2010-2011, far field flat mirror
  run=81;
elseif strcmpi(deblank(run),'r82')
  %Run 8.2 Pole summer 2012-2013, ffflat, using uber chopper
  run=82;
elseif strcmpi(deblank(run),'r83')
   %Run 8.3 Pole summer 2012-2013, ffflat, using ze chopper
  run=83;
elseif strcmpi(deblank(run),'r84')
  %for px pol raster summer 2012-2013
  run=84;
elseif strcmpi(deblank(run),'r85')
  run=85; %for sidelobe maps summer 2012-2013 (7_fisheye_04_005/006)
elseif strcmpi(deblank(run),'r86')
  run=86; %for sidelobe maps summer 2012-2013(7_fisheye_04_007/008)
elseif strncmp(run,'k',1) %a keck pole run 2012
  run=1001;
elseif strncmp(run,'b3',2) %B3 run at Pole 2015
  run=2001;
else
  run=0;
end

if pxpol && ~exist('days','var')
  error('Must specify argument days for pxpol data')
end

% Set up parallel processing:
if par==1
  poolsize=matlabpool('size');
  if poolsize==0
    disp('Opening 8 parallel worker bees for you.')
    matlabpool open 8
  end
end

% Get a list of the relevant files:
if pxpol
  files=parse_pxpollog(days);
  n=length(files.t1);
else
  if highbay
    files=list_arc_files('arc/',t1,t2, '.dat.gz');
  else
    files=list_arc_files('arc/',t1,t2);
  end 
  n=length(files);
end

% Define high-pass filter coefficients:
[bf af]=butter(1, 1/4000, 'high');

% get p from get array info for keck deconv
p=get_array_info(files{1}(1:8));
% Hardcode for FSL mapping since no 2014 ukpv
%p = get_array_info('20131231');

%for concatnating previous files
e=[];

% loop over arc files:
for fl=1:n 
  tic
  if pxpol
    d=load_arc('arc/',files.t1{fl},files.t2{fl},...
	{'antenna0','array','mce0.rc1','mce0.syncBox',...
	['mce0.data.fb(' int2str(files.a(fl)) ',' int2str(files.b(fl)) ')'],...
	['mce0.data.numfj(' int2str(files.a(fl)) ',' int2str(files.b(fl)) ')']});
  else
    load_file=char(strcat('arc/', files(fl)))
    % Load the data
    d=load_arc(load_file);
  end

  disp('File loaded');

  %concatnate from previous file
  %if i kept the previous file
  %only if requested
  if notlastscan
    dtmp=d;
    if ~isempty(e)
      clear d;    
      d=structcat(1,[e,dtmp]);
    end
    clear dtmp;
    dtmp=d;
  end

  % Data valid flag:
  mark=d.array.frame.features;

  % Check the data mode
  if ~strcmp(rxNum,'all')
    dm=eval(sprintf('d.mce%d.rc1.data_mode(logical(mark))',rxNum));
  else
    dm=d.mce0.rc1.data_mode(logical(mark));
  end
  
  % Check to see that there is valid data:
  if numel(dm)==0
    disp(['File ' load_file ' is empty'])
    continue
  end
  
  % Remove dropped samped samples
  syncant=d.antenna0.syncBox.sampleNumber;
  if ~strcmp(rxNum,'all')
    syncmce=eval(sprintf('d.mce%d.syncBox.sampleNumber(:,1)',rxNum));
  else
    syncmce=d.mce0.syncBox.sampleNumber(:,1);
  end
  
  % Identify a skip as any instance when they don't match
  d.skips=syncant~=syncmce;

  % generate field scans
  sampratio=length(d.antenna0.pmac.fast_az_pos)/length(d.antenna0.pmac.az_pos); % fast to slow ratio
  if pxpol
    fs=find_scans(d.antenna0.tracker.scan_off(:,1),bitand(d.array.frame.features,1),sampratio,'keepend');
  elseif sidelobe && ~fisheye
    try
      fs=find_scans(d.antenna0.tracker.scan_off(:,1),bitand(d.array.frame.features,2^1),sampratio,'keepend');
    catch
      warning('No field scans found in this file.  Just so`s you know. Moving to next file.')
      continue
    end
  elseif sidelobe && fisheye
    try
      fs=find_scans(d.antenna0.tracker.scan_off(:,1),bitand(d.array.frame.features,2^1),sampratio,'keepend');
    catch
      warning('No field scans found in this file.  Just so`s you know. Moving to next file.')
      continue
    end
  elseif ~sidelobe && ~fisheye
    try
      fs=find_scans(d.antenna0.tracker.scan_off(:,1),bitand(d.array.frame.features,2^1),sampratio,'keepend');
    catch
      warning('No field scans found in this file.  Just so`s you know. Moving to next file.')
      continue
    end
  end

  disp('Found scans');

  % Be sure there isn't a crazy turnaround right at the beginning where the
  % start of the scan happens after the beginning:
  
  fs=structcut(fs,fs.s<fs.e);
  if run==1001 | run==2001
    for ii=1:length(fs.e)
      if fs.e(ii)> length(d.mce0.cc.data_rate) fs.e(ii)=length(d.mce0.cc.data_rate); end
    end
    %deconvolution for keck
    if ~highbay
      % for Keck there are multiple mce's - concatenate these
      for m={'mce1','mce2','mce3','mce4'}
	if(isfield(d,m))
	  d.mce0=structcat(2,[d.mce0,getfield(d,m{1})]);
	  d=rmfield(d,m);
	end
      end
      d=get_xferfunc(d, fs, 0, 0);
      % Work-around for a bug when running get_xferfunc with low-pass
      % filter off.
      for jj=1:size(d.tf,1)
	for ii=1:size(d.tf,2)
	  d.tf(jj,ii).lpf = 1;
	end
      end
      d=deconv_scans(d, p,1);
    end
  end  

  d.t=d.antenna0.time.utcfast(:,1);

  disp('Deconv scans done');

  if moon
    d=get_source_pos(d);
  end

  % Generate pointing model
  d=arcvar_to_azel(d);
  pm=get_command_coords(mean(d.t),run);
  if ~sidelobe
    d=invpointing_model(d,pm,1,fs);
  else
    d=invpointing_model(d,pm);
  end
  
  d.command = d.pointing.hor;
  pm=get_pointing_model(mean(d.t));
  
  if ~sidelobe
    if run == 1001%keck's pointing model doesn't req mirror
      d=invpointing_model(d,pm,0,fs);
    elseif run == 2001 
      d.pointing.hor=d.rawpoint.hor; %BICEP3
    else
      d=invpointing_model(d,pm,1,fs);
    end
  else
    d=invpointing_model(d,pm);
  end
  
  disp('Done pointing model');

  if moon
    d=filter_scans(d,fs,'p3');
  end
  
  % get the data rate
  data_rate=mode(double(d.antenna0.syncBox.data_rate));
  
  if sidelobe
%   if sidelobe
    d=cut_the_crap(d,fs,rxNum);
  end 

  % keep only fields pos, fb, skips, and ref chop 
  d=rename_fields(d,rxNum,run,p);

  if notlastscan
    if ~isempty(e)
      %if there is a file before, take the last scan from the file before
      tmp1=max(find(fs.sf<length(e.mce0.data.fb(:,1))));
    else
      %else start from the first scan
      tmp1=1;
    end
    if fs.ef(end)==length(d.fb(:,1));
      %if the scan goes to the end of the file, 
      %i take it to mean that the last scan ends in the 
      %file that comes after this, so skip the last scan
      %save the last bit of the file to go onto the next file
      tmp2=length(fs.ef)-1;
      clear e;
      cutscan=structcut(fs,tmp2+1);
      e=cutdstruct(dtmp,cutscan);
      clear dtmp;
    else
      %if the scan doesn't go to the end of the file,
      %don't save the file
      %demodulate to end of file
      clear e;
      e=[];
      tmp2=length(fs.ef);
      clear dtmp;
    end
    if tmp2>tmp1 & tmp2~=0
      %keep the sample before the start of the last az scan
      indexcut=[fs.sf(tmp1):fs.ef(tmp2)];
      d=structcut(d,indexcut);
      %also have to cut fs
      fstmp=fs;
      fs=structcut(fstmp,[tmp1:tmp2]);
      fs.sf=fs.sf-fstmp.sf(tmp1)+1;
      fs.ef=fs.ef-fstmp.sf(tmp1)+1;
    else 
      continue;
    end
  end

  % If specified, do dk cut:
  if ~isempty(dk_tar)
    cc=round(d.pos(:,3))==dk_tar;
    d=structcut(d,cc);
  end

  % Turn reference chop into a logic signal:

  if run==1001 %keck run
    sqw=d.ref>mean(d.ref); %for keck
    
%    for k=1:length(sqw)-2 %this for loop is a hack for uneven pole sidelobe map sampling - AGV
%      if sqw(k+1)-sqw(k)~=0 && sqw(k+2)-sqw(k+1)~=0
%	sqw(k)=sqw(k+1);
%      end
%    end
  
  else
    sqw=d.ref<mean(d.ref); %for bicep2
  end
 
  % If run 8 or k0 r4, symmetrically high-pass filter the fb:
  if run==8 || run==40
    d.fb=filtfilt(bf,af,d.fb);
  end

  % Make sure the ordering of the indices is correct:
  if pxpol
    if files.a(fl)<files.b(fl)
      d.fb=circshift(d.fb,[0,1]);
      % pol B is in first index, pol A is in second.
    end
  end
  % Deglitch chop reference for bicep2 data @ pole, summer 2010-11
  if run==81 || run==82
    sqw=deglitch_sqw(sqw);
  end
  if run==83 || run==84 || run==85 || run==86
    sqw=deglitch_sqw(sqw);
  end

  disp('Do demodulation');
  % Core demodulation routine:
  d=demod_blocks(d,fs,sqw,mean(dm),moon,pxpol,data_rate,fisheye,run, highbay);

  disp('Done demodulation');

  % Cut out dropped samples
  d=structcut(d,~d.skips);
  
  % Zero out transients:
  trans=diff([d.ind; 0]);
  d.sin(trans>100,:)=0;
  d.cos(trans>100,:)=0;
  
  % Save out the data:
  if pxpol
    svstr=['scratch/pxpol/px_' int2str(files.a(fl))];
    save_data(svstr,d)
    system(['chmod g+w ' svstr '.mat']);
  elseif run==1001 %keck run
    if ~strcmp(rxNum,'all')
      svstr=['scratch/beammaps/bm_' files{fl}(1,1:15) '_rx' int2str(rxNum)]; % for multiple receivers 
      save_data(svstr,d)
      system(['chmod g+w ' svstr '.mat']);
    else
      dtmp=d;
      for rx=0:4
	xx=find(p.rx==rx);
	d.fb=dtmp.fb(:,xx);
	d.cos=dtmp.cos(:,xx);
	d.sin=dtmp.sin(:,xx);
	svstr=['scratch/beammaps/bm_' files{fl}(1,1:15) '_rx' int2str(rx)]; % for multiple receivers
	save_data(svstr,d)
	system(['chmod g+w ' svstr '.mat']);
      end 
      clear dtmp;
    end
  else
    svstr=['scratch/beammaps/bm_' files{fl}(1,1:15)]; 
    save_data(svstr,d)
    system(['chmod g+w ' svstr '.mat']);
  end
    toc
end
% Disband worker bees
if par==1
  poolsize=matlabpool('size');
  if poolsize ~=0
    matlabpool close
  end
end 

function d=get_source_pos(d)
samprate=length(d.antenna0.pmac.fast_az_pos)/size(d.antenna0.tracker.horiz_mount,1);

% Get source ra and dec.  Equatorial geocentric apparent position:
source_ra=repmat(double(d.antenna0.tracker.equat_geoc(:,1))'/3.6E6,samprate,1);
d.source_ra=cvec(source_ra);
source_dec=repmat(double(d.antenna0.tracker.equat_geoc(:,2))'/3.6E6,samprate,1);
d.source_dec=cvec(source_dec);

% Get source az and el.  Horizontal geocentric apparent position:
source_az=repmat(double(d.antenna0.tracker.horiz_geoc(:,1))'/3.6E6,samprate,1);
d.source_az=cvec(source_az);
source_el=repmat(double(d.antenna0.tracker.horiz_geoc(:,2))'/3.6E6,samprate,1);
d.source_el=cvec(source_el);
source_pa=repmat(double(d.antenna0.tracker.horiz_geoc(:,3))'/3.6E6,samprate,1);
d.source_pa=cvec(source_pa);


function d=demod_blocks(d,fs,sqw,dm,moon,pxpol,data_rate,fisheye,run, highbay)
% demodulate over the whole damn thing:
% kluge - rwa 20110826.  I've struggled quite a bit to get the
% demodulation routine phased with the pointing data correct.
% The demod looks a lot better for the sin than the cos, so I'm using
% that and advancing the fb relative to the reference by 1
% sample.  Then I shift both the sqw and the fb relative to the pointing
% data by -4 samples to get the minimal left-right splitting.

%get rid of nans in timestream and sqw for keck
if run==1001
  indices=find(isnan(d.fb(:,300)));
  for ii=length(indices):-1:1
    d.fb(indices(ii),:)=[];
    d.ref(indices(ii))=[];
    d.pos(indices(ii),:)=[];
    d.com(indices(ii),:)=[];
    d.skips(indices(ii))=[];
    sqw(indices(ii))=[];
  end
end
d.t=d.t(1);
if run ~=1001 & run ~=2001 %this is any Bicep2 case
  if moon
    e.ind=(1:5:length(d.fb(:,1)))';
  else 
    if run==81 && ~pxpol
      if round(dm)==1
	[e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), circshift(sqw,[3,0]));
      elseif round(dm)==10
	[e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), ...
	    circshift(sqw,[5,0]));
      end
    elseif dm==10 && ~pxpol
      if run==83
	[e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), circshift(sqw,[5,0]));
      elseif run==82
	[e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), ...
	    circshift(sqw,[-2,0]));
      else
	[e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), circshift(sqw,[3,0]));
      end
    elseif dm==1 && ~pxpol && data_rate==75
      [e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), circshift(sqw,[-6,0]));
    elseif dm==1 && ~pxpol && data_rate==150
      [e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), circshift(sqw,[2,0]));   
    elseif pxpol && data_rate==75
      [e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), circshift(sqw,[2,0]));
    elseif pxpol && data_rate==150
      if run==84
	[e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), circshift(sqw,[2,0]));
      else
      [e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), circshift(sqw,[-1,0]));
      end
    elseif ~pxpol && data_rate==125 && ~fisheye
      [e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), circshift(sqw,[0,0]));
    elseif fisheye && data_rate==125
      if run==85
	[e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), ...
	    circshift(sqw,[-3,0]));
      elseif run==86
	[e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), ...
	    circshift(sqw,[-2,0]));
      else
	[e.cos, e.sin, e.ind]=square_demod(circshift(d.fb,[-3,0]), circshift(sqw,[-5,0]));
      disp('yep') 
      end
    end
    
  end
else %this is the keck case
%  [e.cos,
%  e.sin,e.ind]=square_demod(circshift(d.fb,[0,0]),circshift(sqw,[0,0]));
  if highbay % k7, 2013-10
    [e.cos, e.sin, ...
         e.ind]=square_demod(circshift(d.fb,[0,0]),circshift(sqw,[-1,0]));
  else
    if d.t<date2mjd(2013) % 2012-02
      [e.cos, e.sin, ...
	    e.ind]=square_demod(circshift(d.fb,[0,0]),circshift(sqw,[2,0]));
    elseif d.t<date2mjd(2013,2,14) % 2013-02
      [e.cos, e.sin, ...
	    e.ind]=square_demod(circshift(d.fb,[0,0]),circshift(sqw,[7,0]));
    elseif d.t>date2mjd(2013,2,18)& d.t<date2mjd(2013,3,10) % 2013-02 to 2013-03
      [e.cos, e.sin, ...
	    e.ind]=square_demod(circshift(d.fb,[0,0]),circshift(sqw,[2,0]));
    elseif d.t>date2mjd(2014,1,30) & d.t<date2mjd(2014,2,14,02,16,04) % 2014-02a
      [e.cos, e.sin, ...
	    e.ind]=square_demod(circshift(d.fb,[0,0]),circshift(sqw,[4,0]));
    elseif d.t>date2mjd(2014,2,14,02,16,04) & d.t<date2mjd(2014,3,1,07,35,37) % 2014-02b
      [e.cos, e.sin, ...
	    e.ind]=square_demod(circshift(d.fb,[0,0]),circshift(sqw,[3,0]));
    elseif d.t>date2mjd(2014,3,1,07,35,37) & d.t<date2mjd(2014,3,13) % 2014-03
      [e.cos, e.sin, ...
	    e.ind]=square_demod(circshift(d.fb,[0,0]),circshift(sqw,[-5,0]));
    elseif d.t<date2mjd(2015) % 2014-01 FSL mapping on MAPO
      [e.cos, e.sin, ...
	    e.ind]=square_demod(circshift(d.fb,[0,0]),circshift(sqw,[-3,0]));
    elseif d.t<date2mjd(2016) %2015-02 far field mapping
      [e.cos, e.sin, ...
	      e.ind]=square_demod(circshift(d.fb,[0,0]),circshift(sqw,[9,0]));
    else
      disp('No demod phase chosen for this date!');
    end
  end

  if length(e.sin(:,1)) > length(e.cos(:,1))
    e.sin=e.sin(2:length(e.cos(:,1))+1,:);
  elseif length(e.sin(:,1)) < length(e.cos(:,1))
    e.sin=[e.sin;zeros(length(e.cos(:,1))-length(e.sin(:,1)),528)];
  end
  d=rmfield(d,'t');
end  

% downselect indices that are within the field scans:
cc=[];
for ii=1:length(fs.sf)
  keep=e.ind(e.ind > fs.sf(ii) & e.ind < fs.ef(ii));
  cc = [cc; keep];
end

% discard first few and last few samples to avoid transients
%if run~=1001
%  cc=cc(4:end-4);
%end
d=structcut(d,cc);

if run==1001
  e.cos = e.cos(ismember(e.ind,cc),:); %for keck 
  e.sin = e.sin(ismember(e.ind,cc),:); %for keck 
  e.ind = e.ind(ismember(e.ind,cc),:); %for keck 
elseif run==81 | run==82 || run==83 ||run==84 || run==85 ||run==86
  if size(e.cos)==size(e.sin)
    e=structcut(e,ismember(e.ind,cc));
  else
    e.sin = e.sin(1:length(e.cos),:);
    e.cos = e.cos(ismember(e.ind,cc),:); %for bicep2 summer 2012 
    e.sin = e.sin(ismember(e.ind,cc),:); 
    e.ind = e.ind(ismember(e.ind,cc),:); 
  end
else
  e=structcut(e,ismember(e.ind,cc)); % for Bicep2
end

if ~moon
  d.cos=e.cos; d.sin=e.sin; 
end
d.ind=e.ind;

function save_data(svstr,d)
save(svstr,'d') 

function b=rename_fields(d,rxNum,run,p)
if run==1001 
  b.ref=double(d.antenna0.pmac.fast_aux_input(:,1)); %for keck 
  %keep time
  b.t=d.t;
elseif run==2001
  b.ref=double(d.antenna0.pmac.fast_aux_input(:,1)); %for bicep3 
  b.t=d.t;
elseif run==81
  b.ref=double(d.antenna0.pmac.fast_aux_input(:,2)); %for bicep2 
else  
  b.ref=double(d.antenna0.pmac.fast_aux_input(:,4)); %for bicep2 
end

b.pos(:,1)=d.pointing.hor.az;
b.pos(:,2)=d.pointing.hor.el;
b.pos(:,3)=d.pointing.hor.dk;

b.com(:,1)=d.command.az;
b.com(:,2)=d.command.el;
b.com(:,3)=d.command.dk;

b.skips=d.skips;

% Assign variable names
if run==1001 %keck
  if~strcmp(rxNum,'all')
    xx=find(p.rx==rxNum);
    if ~isempty(d.mce0.data.fb(:,xx))
     b.fb=double(d.mce0.data.fb(:,xx));
    elseif ~isempty(d.mce0.data.raw(:,xx))
      b.fb=double(d.mce0.data.raw(:,xx));
    end
  else
    if ~isempty(d.mce0.data.fb)
      b.fb=double(d.mce0.data.fb);
    elseif ~isempty(d.mce0.data.raw)
      b.fb=double(d.mce0.data.raw);
    end
  end
else %bicep2
  if ~isempty(d.mce0.data.fb)
    b.fb=double(d.mce0.data.fb);
  elseif ~isempty(d.mce0.data.raw)
    b.fb=double(d.mce0.data.raw);
  end
end
  
%sign flip for d2 in rx3
%only in keck2013
if d.t(1)>date2mjd(2013) & run==1001
  if rxNum==3
    b.fb=-b.fb;
  elseif strcmp(rxNum, 'all')  % if we are doing multiple rx
    xx=find(p.rx==3);
    b.fb(:,xx)=-b.fb(:,xx);
  end
end

% Keep source position if we're observing the moon:
if isfield(d,'source_az')
  b.source_az=d.source_az;
  b.source_el=d.source_el;
  b.source_pa=d.source_pa;
end

function sqrw=deglitch_sqw(sqw)
glitch=sqw;
deb=diff([0; sqw]);
for jj=2:length(sqw)-1
  if deb(jj)==-1 && deb(jj+1)==1
    glitch(jj)=1;
  end
  if deb(jj)==1 && deb(jj+1)==-1
    glitch(jj)=0;
  end
end
sqrw=glitch;

function pm=get_command_coords(t,run)
pmi=get_pointing_model(t);

% John changed the encoder_zeros in schedlib on Jan 17 2011 based on star
% pointing results.  These values are hard-coded into schedlib - I can't
% figure out a way to get them from the data except to hard code it here

if run~=1001 %bicep2 case
  if t>date2mjd(2011,01,17)
    pm.az_zero=-322.493;
    pm.el_zero=-82.436;
  else
    pm.az_zero=-322.79;
    pm.el_zero=-82.398;
  end
  pm.el_tilt=0;
  pm.az_tilt_lat=0;
  pm.az_tilt_ha=0;
else %keck case, from pointing.init on keck32 from Feb-13-2011. Hopefully this is right
  pm.az_zero=1.175575;
  pm.el_zero=0.106166;
  pm.el_tilt=-0.000030;
  pm.az_tilt_lat=-0.006825;
  pm.az_tilt_ha=0;
end


function d=cut_the_crap(d,fs,rxNum)

for i=1:length(fs.s)
  s=fs.sf(i); e=fs.ef(i);
  fbstd=nanstd(d.mce0.data.fb(s:e,:));
  d.mce0.data.fb(s:e,fbstd>2000)=NaN;
  
end

function e=cutdstruct(d,cutscan)
e=[];
thing1=fieldnames(d);
for ii=1:length(thing1)
  thing2=fieldnames(getfield(d,thing1{ii}));
  for jj=1:length(thing2)
    thing3=fieldnames(getfield(d,thing1{ii},thing2{jj}));
    for kk=1:length(thing3)
      tmp=getfield(d,thing1{ii},thing2{jj},thing3{kk});
      if size(tmp,1)==0
	e=setfield(e,thing1{ii},thing2{jj},thing3{kk},tmp);
      elseif size(tmp,1)<4001
	tmpnew=tmp(cutscan.s:cutscan.e,:);
	e=setfield(e,thing1{ii},thing2{jj},thing3{kk},tmpnew);
      elseif size(tmp,1)>4000
	tmpnew=tmp(cutscan.sf:cutscan.ef,:);
	e=setfield(e,thing1{ii},thing2{jj},thing3{kk},tmpnew);
      else
	
      end
      clear tmp;
      clear tmpnew;
    end
  end
end
