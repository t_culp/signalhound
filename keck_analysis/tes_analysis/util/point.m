function [ph, th] = point (az, el, dk, x, y)
% POINT - perform simple coordinate transformation
%
% [PH, TH] = POINT (AZ, EL, DK, X, Y)
%
% AZ, EL, DK = telescope mount position
% X, Y = pixel offsets in radians
% PH, TH = polar angles giving the direction
%          in which the pixel is looking

xx = ones (size (x));
if 1
	yy = tan (-x);
	zz = tan (-y);
else
	yy = -x;
	zz = -y;
end;

[xx yy zz] = rot (xx, yy, zz, [1 0 0], dk);
[xx yy zz] = rot (xx, yy, zz, [0 1 0], -el);
[xx yy zz] = rot (xx, yy, zz, [0 0 1], az);

ph = atan2 (yy, xx);
th = atan2 (zz, sqrt(xx.^2 + yy.^2));

%%%%

function [x1 y1 z1] = rot (x0, y0, z0, v, th)

	x1 = x0 * cos (th);
	y1 = y0 * cos (th);
	z1 = z0 * cos (th);

	x1 = x1 + (v(2)*z0 - v(3)*y0) * sin(th);
	y1 = y1 + (v(3)*x0 - v(1)*z0) * sin(th);
	z1 = z1 + (v(1)*y0 - v(2)*x0) * sin(th);

	dp = (x0*v(1) + y0*v(2) + z0*v(3));
	x1 = x1 + dp * (1 - cos(th)) * v(1);
	y1 = y1 + dp * (1 - cos(th)) * v(2);
	z1 = z1 + dp * (1 - cos(th)) * v(3);
