function T = guess_coldload_temp (fname)
% GUESS_COLDLOAD_TEMP finds the Cold Load temperature of
% a load curve run based on the file name.  The
% data files often have names like TiIVCurve_378.3mK_10.5K...
% including the cold load temperature.  This routine scans
% the file name for a likely temperature.
%
% T = GUESS_COLDLOAD_TEMP (F)

% ART 100318

[S E] = regexp (lower (fname), '\d+(\.\d+)?k');
if isempty (S)
	T = [];
	return;
end;

S = S(end);
E = E(end) - 1;
T = str2num (fname (S:E));
