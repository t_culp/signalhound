function T = guess_base_temp (fname)
% GUESS_BASE_TEMP finds the base temperature of
% a load curve run based on the file name.  The
% data files often have names like rampTES_378mK_...
% including the temperature.  This routine scans
% the file name for a likely temperature.
%
% T = GUESS_BASE_TEMP (F)

% rwo 080502

[S E] = regexp (lower (fname), '\d+(\.\d+)?mk');
if isempty (S)
	T = [];
	return;
end;

S = S(end);
E = E(end) - 2;
T = str2num (fname (S:E));
