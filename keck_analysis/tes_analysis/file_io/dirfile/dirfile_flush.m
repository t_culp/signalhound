function D = dirfile_flush (D_in)
% DIRFILE_FLUSH  Update dirfile format file.
%   After calling DIRFILE_FLUSH, the dirfile
%   remains open, and the format file on disk
%   is up to date.
%
% D = DIRFILE_FLUSH (D)
%   D = structure containing dirfile info
%
% See also:
%    DIRFILE_OPEN, DIRFILE_CLOSE
%    DIRFILE_FIELDS
%    DIRFILE_GET, DIRFILE_PUT
%    DIRFILE_ADD_FIELD, DIRFILE_DELETE_FIELD

% RWO 081009


D = D_in;
if (D.modified == 0)
	return;
end;

D = write_fmt (D);
D = rm_deleted_raw (D);

D.modified = 0;

%%%%

function D = write_fmt (D_in)
	D = D_in;
	f = fopen (D.format, 'wt');
	if (f == -1)
		return;
	end;

	if ~isempty (D.version)
		fprintf (f, '%20s %10d\n', '/VERSION', D.version);
	end;
	switch (D.endian)
		case 'b', endstr = 'big';
		case 'l', endstr = 'little';
		otherwise, endstr = '';
	end;
	if ~isempty (endstr)
		fprintf (f, '%20s %10s\n', '/VERSION', endstr);
	end;
	if ~isempty (D.frameoffset)
		fprintf (f, '%20s %10d\n', '/FRAMEOFFSET', D.frameoffset);
	end;

	for (ii = 1:length (D.fields))
		fprintf (f, '%20s ', D.fields{ii}.name);
		switch (D.fields{ii}.type)
			case 'RAW', ...
				switch (D.fields{ii}.datatype)
					case 'single', dtstr = 'FLOAT32';
					case 'double', dtstr = 'FLOAT64';
					otherwise, dtstr = upper (D.fields{ii}.datatype);
				end;
				fprintf (f, '%10s %10s %10d\n', ...
					'RAW', dtstr, D.fields{ii}.spf);
			case 'LINCOM', ...
				fprintf (f, '%10s %10d ', ...
					'LINCOM', D.fields{ii}.n);
				for (jj = 1:D.fields{ii}.n)
					fprintf (f, '%10s %10e %10e ', D.fields{ii}.dep{jj}, ...
						D.fields{ii}.a(jj), D.fields{ii}.b(jj));
				end;
				fprintf (f, '\n');
			case 'LINTERP', ...
				fprintf (f, '%10s ', ...
					'LINTERP');
				table = D.fields{ii}.table;
				if ~isempty (D.fields{ii}.root)
					table = fullfile (D.fields{ii}.root, table);
				end;
				fprintf (f, '%10s %10s\n', D.fields{ii}.dep, table);
			case 'MULTIPLY', ...
				fprintf (f, '%10s %10s %10s\n', ...
					'MULTIPLY', ...
					D.fields{ii}.dep{1}, D.fields{ii}.dep{2});
			case 'BIT', ...
				fprintf (f, '%10s %10d', ...
					'BIT', ...
					D.fields{ii}.firstbit);
				if (D.fields{ii}.bits == 1)
					fprintf (f, '\n');
				else
					fprintf (f, ' %10d\n', D.fields{ii}.bits);
				end;
			case 'PHASE', ...
				fprintf (f, '%30s %10s %10s %10d\n', D.fields{ii}.name, ...
					'PHASE', D.fields{ii}.dep, D.fields{ii}.shift);
		end;
	end;
	fclose (f);

function D = rm_deleted_raw (D_in)
	D = D_in;
	if isempty (D.deleted_raw)
		return;
	end;
	drf = fieldnames (D.deleted_raw);
	for (ii = 1:length(drf))
		if exist (D.deleted_raw.(drf{ii}), 'file')
			delete (D.deleted_raw.(drf{ii}));
		end;
	end;
	D.deleted_raw = [];
