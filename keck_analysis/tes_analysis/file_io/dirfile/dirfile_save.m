function D_out = dirfile_save (D, key, val, varargin)
% DIRFILE_SAVE  Write a variable to a dirfile.  The
%   dirfile may have already been opened using
%   DIRFILE_OPEN, or may be passed as a file name.
%   The varable will be added as a new field in the
%   dirfile if it doesn't already exist.
%
% D = DIRFILE_SAVE (D, N, V)
%
%   D = dirfile's file name OR a struct from
%       DIRFILE_OPEN
%   N = name of variable to be written
%   V = data vector to be written
%
% D = DIRFILE_SAVE (D, N, V, T, S)
%
%   T = field data type
%       (optional, keeps current type of field in the
%       dirfile if possible, or defaults to double)
%   S = samples per frame number
%       (optional, keeps current value from field in
%       the dirfile if possible, or defaults to length
%       of V).

% RWO 081010

dirfname = '';
if ischar(D)
	dirfname = D;
	D = dirfile_open (dirfname);
end;

if isfield (D.names, key)
	idx = D.names.(key);
else
	idx = [];
end;
if ~isempty (idx) && strcmp (D.fields{idx}.type, 'RAW')
	datatype = D.fields{idx}.datatype;
	spf = D.fields{idx}.spf;
	do_reset = 0;
else
	datatype = 'double';
	spf = max (length(val), 1);
	do_reset = 1;
end;

if length(varargin) >= 1
	do_reset = 1;
	if ~isempty (varargin{1})
		datatype = varargin{1};
	end;
	if length(varargin) >= 2
		if ~isempty (varargin{2})
			spf = varargin{2};
		end;
	end;
end;

if do_reset
	D = dirfile_add_field (D, key, 'RAW', datatype, spf);
end;

D = dirfile_put (D, key, val);
% D = dirfile_flush (D);

if ~isempty (dirfname)
	dirfile_close (D);
else
	D_out = D;
end;
