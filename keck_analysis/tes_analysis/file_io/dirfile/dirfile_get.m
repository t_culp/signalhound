function [val D S] = dirfile_get (D_in, key)
% DIRFILE_GET  Retrieve data from an open dirfile.
%   Currently, the entire time stream is retrieved;
%   it is not possible to get a specified range of
%   samples.
%
% [V D] = DIRFILE_GET (D, N)
%
%   D = structure containing dirfile info
%   N = name of field to retrieve
%   V = retrieved data samples
%
% See also:
%    DIRFILE_OPEN, DIRFILE_CLOSE, DIRFILE_FLUSH
%    DIRFILE_FIELDS
%    DIRFILE_PUT
%    DIRFILE_ADD_FIELD, DIRFILE_DELETE_FIELD

% RWO 081009


if ~isfield (D_in.names, key)
	error (['Dirfile has no field ' key '.']);
end;

D = D_in;
idx = D.names.(key);
switch (D.fields{idx}.type)
	case 'RAW', [val D spf] = get_raw (D, idx);
	case 'LINCOM', [val D spf] = get_lincom (D, idx);
	case 'MULTIPLY', [val D spf] = get_multiply (D, idx);
    case 'BIT', [val D spf] = get_bit (D, idx);
	otherwise, ...
		error (['Getting values for ' D.fields{idx}.type ' fields ' ...
			'not yet supported.']);
end;

if (nargout >= 3)
	S = spf;
end;

%%%%

function [val D spf] = get_raw (D_in, idx)
	D = D_in;
	if isempty (D.fields{idx}.f)
		D.fields{idx}.f = fopen (D.fields{idx}.fname, 'rb');
	else
		fseek (D.fields{idx}.f, 0, -1);
	end;
	if (D.fields{idx}.f == -1)
		error (['No data file for raw field ' D.fields{idx}.name '.']);
	end;
	val = fread (D.fields{idx}.f, Inf, D.fields{idx}.datatype, ...
		0, D.endian);
	spf = D.fields{idx}.spf;
	fclose (D.fields{idx}.f);
	D.fields{idx}.f = [];

function [val D spf1] = get_lincom (D_in, idx)
	D = D_in;
	if 0==D.fields{idx}.isgood
		error (['LINCOM field ' D.fields{idx}.name ' has bad dependencies.']);
	end;
	[tmp1 D spf1] = dirfile_get (D, D.fields{idx}.dep{1});
	tmp1 = D.fields{idx}.a(1) * tmp1 + D.fields{idx}.b(1);
	for (ii = 2:D.fields{idx}.n)
		[tmp2 D spf2] = dirfile_get (D, D.fields{idx}.dep{ii});
		tmp2 = D.fields{idx}.a(ii) * tmp2 + D.fields{idx}.b(ii);
		if (spf2 == spf)
			tmp1 = tmp1 + tmp2;
		else
			% Samples per frame don't match.  According to dirfile
			% specification, we rectify everything to match the first
			% source field.  But the interpolation method is not
			% specified.  Check this!
			xx1 = (1:length(tmp1)) / spf;
			xx2 = (1:length(tmp2)) / spf2;
			yy2 = tmp2;
			tmp1 = tmp1 + interp (xx2, yy2, xx1, 'nearest', 'extrap');
		end;
	end;
	val = tmp1;
    
function [val D spf1] = get_multiply (D_in, idx)
    D = D_in;
    if 0==D.fields{idx}.isgood
        error (['MULTIPLY field ' D.fields{idx}.name ' has bad dependencies.']);
    end;
    [tmp1 D spf1] = dirfile_get (D, D.fields{idx}.dep{1});
    [tmp2 D spf2] = dirfile_get (D, D.fields{idx}.dep{2});
    if (spf2 == spf1)
        val = tmp1 .* tmp2;
    else
        % Samples per frame don't match.  According to dirfile
        % specification, we rectify everything to match the first
        % source field.  But the interpolation method is not
        % specified.  Check this!
        xx1 = (1:length(tmp1)) / spf1;
        xx2 = (1:length(tmp2)) / spf2;
        yy2 = tmp2;
        val = tmp1 .* interp (xx2, yy2, xx1, 'nearest', 'extrap');
    end;

function [val D spf1] = get_bit (D_in, idx)
    D = D_in;
    if 0==D.fields{idx}.isgood
        error(['BIT field ' D.fields{idx}.name ' has bad dependencies.']);
    end;
    [tmp D spf1] = dirfile_get (D, D.fields{idx}.dep{1});
    tmp = uint32(mod(int64(tmp),2^32)); % Hack to deal with negative values
    val = bitshift(tmp, -D.fields{idx}.firstbit, ...
        D.fields{idx}.bits+D.fields{idx}.firstbit);