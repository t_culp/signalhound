function D = dirfile_delete_field (D_in, key, opts)
% DIRFILE_DELETE_FIELD  Delete a field from an open dirfile.
%   All derived fields depending on the deleted field are
%   also deleted.  Note that the format file on disk is not
%   updated until the next call to DIRFILE_FLUSH or
%   DIRFILE_CLOSE.  When a field of type RAW is deleted,
%   the corresponding raw data file on disk is also
%   removed at the next call to DIRFILE_FLUSH or
%   DIRFILE_CLOSE.
%
% D = DIRFILE_DELETE_FIELD (D, N)
%
%   D = structure containing dirfile info
%   N = name of field to delete
%
% D = DIRFILE_DELETE_FIELD (D, N, 1) silently ignores requests
%   to delete any fields that don't exist in the first place.
%   Ordinarily, this would give an error.
%
% See also:
%    DIRFILE_OPEN, DIRFILE_CLOSE, DIRFILE_FLUSH
%    DIRFILE_FIELDS
%    DIRFILE_GET, DIRFILE_PUT
%    DIRFILE_ADD_FIELD

% RWO 081009

D = D_in;
permissive = (nargin >= 3) && (opts == 1);
if ~isfield (D.names, key)
	if (permissive)
		% Field doesn't exist, so no need to do anything.
		return;
	else
		error (['Dirfile has no field ' key '.']);
	end;
end;
idx = D.names.(key);
D.fields(idx) = [];

% Close any related open files
if isfield (D.fields(idx), 'f')
	if ~isempty (D.fields(idx).f)
		fclose (D.fields(idx).f);
	end;
end;

% For fields of type RAW, list field name
% so that raw data file can be deleted on
% next flush
if strcmp (D.fields(idx).type, 'RAW')
	D.deleted_raw.(key) = D.fields(idx).fname;
end;

% Also delete any derived fields that
% depend on the one being deleted

first_dep = -1;
while ~isempty (first_dep)
	first_dep = [];
	for (ii = 1:length (D.fields))
		if ~isfield (D.fields(ii), 'dep')
			continue;
		end;
		for (jj = 1:length (D.fields(ii).dep))
			if ~strcmp (D.fields(ii).dep{jj}, key)
				continue;
			end;
			first_dep = ii;
			break;
		end;
		if ~isempty (first_dep)
			break;
		end;
	end;
	if ~isempty (first_dep)
		D = dirfile_delete_field (D, D.fields(first_dep).name);
	end;
end;

D.names = rmfield (D.names, key);
D.modified = 1;
