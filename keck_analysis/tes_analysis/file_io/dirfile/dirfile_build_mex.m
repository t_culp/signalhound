function dirfile_build_mex

% Find directory with dirfile functions
p = mfilename ('fullpath');
dirfile_dir = fileparts (p);

% Where mex source and binaries live
mex_code_dir = fullfile (dirfile_dir, '..', '..', 'mex_code', 'dirfile');
mex_bin_dir = fullfile (dirfile_dir, '..', '..', 'mex_bin');

% Create binary directory if it's not there
if ~exist (mex_bin_dir, 'dir')
	mkdir (mex_bin_dir);
	% Also add to path
	addpath (mex_bin_dir);
end;

% Functions to compile
compile_list = {'dirfile_read_fmt_mex', 'dirfile_init_fields_mex'};

% ... and do it
for (ii = 1:length(compile_list))
	if exist ('OCTAVE_VERSION', 'builtin')
        	mex (fullfile (mex_code_dir, [compile_list{ii} '.c']), ...
             		'-o', fullfile(mex_bin_dir,[compile_list{ii} '.' mexext]));
	else
		mex (fullfile (mex_code_dir, [compile_list{ii} '.c']), ...
             		'-outdir', mex_bin_dir);
	end
end;
