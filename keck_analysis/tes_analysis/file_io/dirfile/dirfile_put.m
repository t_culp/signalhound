function D = dirfile_put (D_in, key, val)
% DIRFILE_PUT  Write data to an open dirfile.
%   Currently, the entire time stream is written or
%   rewritten; it is not possible to write a specified
%   range of samples.
%
% D = DIRFILE_PUT (D, V)
%
%   D = structure containing dirfile info
%   N = name of field to retrieve
%   V = data samples to write
%
% See also:
%    DIRFILE_OPEN, DIRFILE_CLOSE, DIRFILE_FLUSH
%    DIRFILE_FIELDS
%    DIRFILE_GET
%    DIRFILE_ADD_FIELD, DIRFILE_DELETE_FIELD

% RWO 081009


if ~isfield (D_in.names, key)
	error (['Dirfile has no field ' key '.']);
end;
idx = D_in.names.(key);
if ~strcmp (D_in.fields{idx}.type, 'RAW')
	error (['Writing fields of type ' D_in.fields{idx}.type ' not supported.']);
end;

D = D_in;
if ~isempty (D.fields{idx}.f)
	fclose (D.fields{idx}.f);
end;
f = fopen (D.fields{idx}.fname, 'wb');
fwrite (f, val, D.fields{idx}.datatype, 0, D.endian);
fclose (f);
