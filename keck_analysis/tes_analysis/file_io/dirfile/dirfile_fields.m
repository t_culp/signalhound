function f = dirfile_fields (D, filter, opts)
% DIRFILE_FIELDS  List fields of an open dirfile.
%   This can be used to list all fields, or to match
%   a pattern.
%
% F = DIRFILE_FIELDS (D) returns all fields of D
%   in a cell array F.
% F = DIRFILE_FIELDS (D, N) finds only fields
%   beginning with the string N.
% F = DIRFILE_FIELDS (D, N, 'exact') searches for
%   a field named N.
% F = DIRFILE_FIELDS (D, N, 'regexp') finds fields
%   matching the regular expression N.
%
% See also:
%    DIRFILE_OPEN, DIRFILE_CLOSE, DIRFILE_FLUSH
%    DIRFILE_GET, DIRFILE_PUT
%    DIRFILE_ADD_FIELD, DIRFILE_DELETE_FIELD

% RWO 081009


if (nargin == 1)
	f = fieldnames (D.names);
	return;
end;

if isempty (D.names)
    f = '';
    return;
end;

if (nargin == 2) || (nargin == 3 && strcmpi(opts,'start'))
	n = fieldnames (D.names);
	idx = strmatch (filter, char(n));
	f = {n{idx}};
	return;
elseif (nargin == 3)
	switch (lower (opts))
		case 'exact', ...
			f = '';
			tmp = isfield (D.names, filter);
			if isfield (D.names, 'filter')
				f = filter;
			end;
		case 'regexp', ...
			n = fieldnames (D.names);
			idx = ~cellfun ('isempty', regexp (n, filter));
			f = {n{idx}};
		otherwise,
			error (['Unknown matching option ' opts '.']);
	end;

end;
