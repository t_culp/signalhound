function dirfile_close (D)
% DIRFILE_CLOSE Close an open dirfile.  This ensures
%   that the format file is up to date and closes any
%   open file handles.
%
% DIRFILE_CLOSE (D)
%
%   D = structure containing dirfile info
%
% See also:
%    DIRFILE_OPEN, DIRFILE_FLUSH
%    DIRFILE_FIELDS
%    DIRFILE_GET, DIRFILE_PUT
%    DIRFILE_ADD_FIELD, DIRFILE_DELETE_FIELD

% RWO 081009


D = dirfile_flush (D);
for (ii = 1:length (D.fields))
	if isfield (D.fields{ii}, 'f') && ~isempty (D.fields{ii}.f)
		fclose (D.fields{ii}.f);
		D.fields{ii}.f = [];
	end;
end;
