function D = dirfile_add_field (D_in, key, ftype, varargin)
% DIRFILE_ADD_FIELD  Add a field to an open dirfile.  If a
%   field already exists with the same name, it will be
%   replaced.  Note that the format file on disk is not
%   updated until the next call to DIRFILE_FLUSH
%   or DIRFILE_CLOSE.
%
% D = DIRFILE_ADD_FIELD (D, N, T, ...)
%
%   D = structure containing dirfile info
%   N = name of new field
%   T = type of new field
% ... = type-specific arguments
%
% DIRFILE_ADD_FIELD uses the same arguments as field
% definition lines in the dirfile format file.
% (See dirfile documentation at
% http://getdata.sourceforge.net/dirfile.html).
%
% Valid field types are RAW, LINCOM, LINTERP, MULTIPLY,
% BIT, and PHASE.
%
% Examples:
%    D = dirfile_add_field (D, 'bolo1', 'RAW', 'double', 100)
%    D = dirfile_add_field (D, 'v1', 'LINCOM', 1, 'bolo1', 0.751, 0.109)
% 
% See also:
%    DIRFILE_OPEN, DIRFILE_CLOSE, DIRFILE_FLUSH
%    DIRFILE_FIELDS
%    DIRFILE_GET, DIRFILE_PUT
%    DIRFILE_DELETE_FIELD

% RWO 081009


D = D_in;
switch (upper (ftype))
	case 'RAW', field = gen_raw (key, varargin, D.root);
	case 'LINCOM', field = gen_lincom (key, varargin);
	case 'LINTERP', field = gen_linterp (key, varargin);
	case 'MULTIPLY', field = gen_multiply (key, varargin);
	case 'BIT', field = gen_bit (key, varargin);
	case 'PHASE', field = gen_phase (key, varargin);
	otherwise, ...
		error (['Unknown dirfile field type ' ftype '.']);
end;
check_deps (D.names, field);
if isempty (field)
	error (['Error adding field ' key ' of type ' ftype '.']);
end;

% idx = strmatch (key, D.names, 'exact');
% if isempty (idx)
if ~isfield (D.names, key)
	ii = length (D.fields);
	D.fields{ii+1} = field;
	D.names.(key) = ii+1;
else
	D.fields{D.names.(key)} = field;
end;

D.modified = 1;

%%%%

function field = gen_raw (key, args, root)
	field.name = key;
	field.type = 'RAW';
	if (length(args)>=1) && ischar(args{1})
		field.datatype = lower (args{1});
		switch (field.datatype)
			case {'float32', 'float'}, field.datatype = 'single';
			case 'float64', field.datatype = 'double';
		end;
	else
		field.datatype = 'double';
	end;
	if (length(args)>=2) && isnumeric(args{2})
		field.spf = args{2};
	else
		field.spf = 1;
	end;
	field.root = '';
	field.fname = fullfile (root, key);
	field.f = [];

function field = gen_lincom (key, args)
	field.name = key;
	field.type = 'LINCOM';
	if length(args)<1 || ~isnumeric(args{1})
		error (['Bad new dirfile LINCOM field.']);
	end;
	field.n = args{1};
	if length(args) ~= 1 + 3*field.n
		error (['Bad new dirfile LINCOM field.']);
	end;
	for (ii = 1:field.n)
		field.dep{ii} = args{3*ii - 1};
		field.a(ii) = args{3*ii};
		field.b(ii) = args{3*ii + 1};
	end;
	field.isgood = 1;

function field = gen_linterp (key, args, root)
	field.name = key;
	field.type = 'LINTERP';
	if length(args)~=2
		error (['Bad new dirfile LINTERP field.']);
	end;
	field.dep = args{1};
	if isempty (fileparts (args{2}))
		field.table = args{2};
		field.root = [];
		field.fname = fullfile (root, field.table);
	else
		[field.root table ext] = fileparts (args{2});
		field.table = [table ext];
		field.fname = fullfile (field.root, field.table);
	end;
	field.lookup = [];
	if ~exist (field.fname, 'file')
		warning (['Dirfile LINTERP table file ' field.table ' not found.']);
		field.isgood = 0;
	else
		field.isgood = 1;
	end;

function field = gen_multiply (key, args)
	field.name = key;
	field.type = 'MULTIPLY';
	if length (args)~=2
		error (['Bad new dirfile MULTIPLY field.']);
	end;
	field.dep{1} = args{1};
	field.dep{2} = args{2};
	field.isgood = 1;

function field = gen_bit (key, args)
	field.name = key;
	field.type = 'BIT';
	if length (args) < 2
		error (['Bad new dirfile BIT field.']);
	end;
	field.dep = args{1};
	field.firstbit = args{2};
	if length(args)>2
		field.bits = args{3};
	else
		field.bits = 1;
	end;
	field.isgood = 1;

function field = gen_phase (key, args)
	field.name = key;
	field.type = 'PHASE';
	if length(args)~=2
		error (['Bad new dirfile PHASE field.']);
	end;
	field.dep = args{1};
	field.shift = args{2};
	field.isgood = 1;

%%%%

function field = check_deps (names, f_in)
	field = f_in;
	if ~isfield (field, 'dep')
		return;
	end;
	for (ii = 1:length (field.dep))
		if ~isfield (names, field.dep{ii})
		% if ~strmatch (field.dep{ii}, names, 'exact')
			field.isgood = 0;
		end;
	end;

%%%%
