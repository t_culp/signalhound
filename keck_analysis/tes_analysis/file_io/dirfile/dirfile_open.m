function D = dirfile_open (fname)
% DIRFILE_OPEN  Open an existing or new dirfile
%   for reading or writing.
%
% D = DIRFILE_OPEN (F)
%
%   F = dirfile's directory name
%   D = structure containing dirfile info
%
% See also:
%    DIRFILE_CLOSE, DIRFILE_FLUSH
%    DIRFILE_FIELDS
%    DIRFILE_GET, DIRFILE_PUT
%    DIRFILE_ADD_FIELD, DIRFILE_DELETE_FIELD

% RWO 081009


if exist (fname, 'dir')
	D.root = fname;
elseif exist (fname, 'file')
	D.root = fileparts (fname);
else
	D.root = fname;
	mkdir (fname);
end;
D.root = absdir (D.root);

D.frameoffset = [];
D.version = [];
D.endian = 'n';		% Default to local machine endianness
D.format = fullfile (D.root, 'format');
D.fields = {};
if exist (D.format, 'file')
      try
        D = dirfile_read_fmt_mex (D);
      catch
        disp ('Warning: mex utilities not available.  This may be slow.');
        disp ('Try running dirfile_build_mex for more speed.');
	D = read_fmt (D);
      end;
else
	D = new_fmt (D);
end;

D = index_fields (D);
try
  D = dirfile_init_fields_mex (D);
catch
  disp ('No mex for init_fields.');
  D = init_fields (D, D.root);
end;
D.deleted_raw = [];
D.modified = 0;

%%%%

function p = absdir (p_in)
        if ~exist (p_in, 'dir')
            p = p_in;
        else
        	tmpdir = pwd;
		cd (p_in);
		p = pwd;
        	cd (tmpdir);
        end;

function D = new_fmt (D_in)
	D = D_in;
	D.fields = [];
	D.version = 5;
	D.names = [];

function D = read_fmt (D_in)
	D = D_in;
	N_ALLOC = 100;
	f = fopen (D.format, 'rt');
	if (f == -1)
		D.fields = [];
		return;
	end;

	while ~feof (f)
		ll = fgetl (f);
		C = textscan (ll, '%s');
		key = C{1}{1};
		C = {C{1}{2:end}};

		% Comment line
		if (key(1) == '#')
			continue;
		end;
		
		% Directive line
		[res D] = try_parse_directive (D, key, C);
		if ~isempty (res)
			continue;
		end;

		% Field specification line
		[res D] = try_parse_field (D, key, C);
		if ~isempty (res)
			continue;
		end;

		% What else???
		warning (['Could not parse a line of format file!']);
		disp (sprintf ('%s ', key, C{:}));
	end;
	fclose (f);

%%%%

function [res D] = try_parse_directive (D_in, key, C)
	D = D_in;
	res = 1;
	if length(C) ~= 1
		if key(1)=='/'
			warning (['Wrong number of args for ' key ' directive.']);
		else
			res = [];
			return;
		end;
	end;

	switch (key)
		case {'ENDIAN', '/ENDIAN'}, ...
			switch (lower (C{1}))
				case 'big', D.endian = 'b';
				case 'little', D.endian = 'l';
				otherwise, ...
					warning (['Improperly formed ENDIAN directive.']);
			end;
		case {'FRAMEOFFSET', '/FRAMEOFFSET'}, ...
			fo = str2double (C{1});
			if isempty(fo) || length(fo)>1 || (fo ~= fix(fo))
				warning (['Improperly formed FRAMEOFFSET directive.']);
			else
				D.frameoffset = fo;
			end;
		case {'VERSION', '/VERSION'}, ...
			v = str2double (C{1});
			if isempty(v) || length(v)~=1 || (v ~= fix(v))
				warning (['Improperly formed VERSION directive.']);
			else
				D.version = v;
			end;
		case {'INCLUDE', '/INCLUDE'}, ...
			warning (['INCLUDE directive is not supported.']);
		otherwise,
			if key(1) == '/'
				warning (['Unrecognized directive ' key ' ignored.']);
			else
				res = [];
			end;
	end;

%%%%

function [res D] = try_parse_field (D_in, key, C)
	D = D_in;
	res = 1;
	if strcmp(key,'INDEX') || strcmp(key,'FILEFRAM')
		warning (['Forbidden field name ' key ' ignored.']);
		return;
	end;

	ftype = C{1};
	if isempty (ftype)
		res = [];
		return;
	end;

	switch (ftype)
		case 'RAW', field = try_parse_raw ([], key, C);
		case 'LINCOM', field = try_parse_lincom ([], key, C);
		case 'LINTERP', field = try_parse_linterp ([], key, C);
		case 'MULTIPLY', field = try_parse_multiply ([], key, C);
		case 'BIT', field = try_parse_bit ([], key, C);
		case 'PHASE', field = try_parse_phase ([], key, C);
		otherwise, ...
			res = [];
			return;
	end;
	if isempty (field)
		res = [];
		return;
	end;
	D.fields{length(D.fields)+1} = field;
	% D.fields = {D.fields{:}, field};

%%%%

function field = try_parse_raw (field, key, C)
	% field = empty_field;
	field.name = key;
	field.type = 'RAW';
	if length(C) < 3
		field = [];
		return;
	end;
	if length(C) > 3
		if C{4}(1) ~= '#'
			warning (['Too many arguments for RAW field.']);
		end;
	end;

	datatype = C{2};
	spf = str2double (C{3});
	if isempty (spf)
		field = [];
		return;
	end;
	
	field.spf = spf;

	switch (datatype)
		case {'c', 'UINT8'}, 			field.datatype = 'uint8';
		case {'INT8'},				field.datatype = 'int8';
		case {'u', 'UINT16'},			field.datatype = 'uint16';
		case {'s', 'INT16'},			field.datatype = 'int16';
		case {'U', 'UINT32'},			field.datatype = 'uint32';
		case {'S', 'i', 'INT32'},		field.datatype = 'int32';
		case {'UINT64'},			field.datatype = 'uint64';
		case {'INT64'},				field.datatype = 'int64';
		case {'f', 'FLOAT32', 'FLOAT'},		field.datatype = 'single';
		case {'d', 'FLOAT64', 'DOUBLE'},	field.datatype = 'double';
		otherwise, ...
			field = [];
			return;
	end;
	field.root = '';

%%%%

function field = try_parse_lincom (field, key, C)
	field.name = key;
	field.type = 'LINCOM';
	if (length(C) < 2)
		field = [];
		return;
	end;

	n = str2double (C{2});
	if isempty(n) || n<=0 || n~=fix(n)
		field = [];
		return;
	end;
	field.n = n;
	for (ii = 1:n)
		dep = C{3+3*(ii-1)};
		if isempty(dep)
			field = [];
			return;
		end;
		field.dep{ii} = dep;
		a = str2double (C{4+3*(ii-1)});
		b = str2double (C{5+3*(ii-1)});
		if isempty(a) || isempty(b)
			field = [];
			return;
		end;
		field.a(ii) = a;
		field.b(ii) = b;
	end;
	if length(C)>(2+3*n) && C{3+3*n}(1)~='#'
		warning (['Extra characters at end of LINCOM line:']);
		disp (sprintf ('%s ', C{(3+3*n):end}));
	end;

function field = try_parse_linterp (field, key, ll)
	ll = sprintf ('%s ', ll{2:end});

	field.name = key;
	field.type = 'LINTERP';
	[dep ll] = strtok (ll);
	ll = strtrim(ll);
	if isempty(dep)
		field = [];
		return;
	end;
	field.dep = dep;
	[table ll] = strtok(ll);
	ll = strtrim(ll);
	if isempty(ll)
		field = [];
		return;
	end;
	if isempty (fileparts (table))
		field.root = '';
		field.table = table;
	else
		[field.root table ext] = fielparts (table);
		field.table = [table ext];
	end;
	
	if ~isempty(ll) && ll(1)~='#'
		warning (['Extra characters at end of LINTERP line:']);
		disp (ll);
	end;

function field = try_parse_multiply (field, key, ll)
	ll = sprintf ('%s ', ll{2:end});
	% field = empty_field;
	field.name = key;
	field.type = 'MULTIPLY';
	for (ii = 1:2)
		[dep ll] = strtok (ll);
		ll = strtrim(ll);
		if isempty(dep)
			field = [];
			return;
		end;
		field.dep{ii} = dep;
	end;
	if ~isempty(ll) && ll(1)~='#'
		warning (['Extra characters at end of MULTIPLY line:']);
		disp (ll);
	end;

function field = try_parse_bit (field, key, ll)
	field.name = key;
	field.type = 'BIT';
	[dep ll] = strtok(ll);
	ll = strtrim(ll);
	if isempty(dep)
		field = [];
		return;
	end;
	field.dep = dep;
	[firstbit ll] = strtok(ll);
	ll = strtrim(ll);
	[bits ll] = strtok(ll);
	ll = strtrim(ll);
	if isempty(firstbit) || isempty(bits) || (firstbit~=fix(firstbit)) || (bits~=fix(bits))
		field = [];
		return;
	end;
	field.firstbit = firstbit;
	field.bits = bits;
	if ~isempty(ll) && ll(1)~='#'
		warning (['Extra characters at end of BIT line:']);
		disp (ll);
	end;

function field = try_parse_phase (field, key, ll)
	field.name = key;
	field.type = 'PHASE';
	[dep ll] = strtok(ll);
	ll = strtrim(ll);
	if isempty(dep)
		field = [];
		return;
	end;
	field.dep = dep;
	[shift ll] = strtok(ll);
	ll = strtrim(ll);
	shift = str2double (shift);
	if isempty(shift) || shift~=fix(shift)
		field = [];
		return;
	end;
	if ~isempty(ll) && ll(1)~='#'
		warning (['Extra characters at end of PHASE line:']);
		disp (ll);
	end;

%%%%

function D = index_fields (D_in)
	% For now, don't do any sorting at all!
	D = D_in;
	for (ii = 1:length (D.fields))
		D.names.([D.fields{ii}.name]) = ii;
	end;

%%%%

function D = init_fields (D_in, root)
	D = D_in;
	for (ii = 1:length(D.fields))
		f = D.fields{ii};
		switch (D.fields{ii}.type)
			case 'RAW', f = init_raw (f, root);
			case {'LINCOM', 'LINTERP', 'MULTIPLY', 'BIT', 'PHASE'}, ...
				f = check_dep (D.names, f);
			case 'LINTERP', ...
				f = check_dep (D.names, f);
				f = init_linterp (f, root);
		end;
		D.fields{ii} = f;
	end;

function f = check_dep (names, f_in)
	f = f_in;
	isgood = 1;
	for (ii = 1:length(f.dep))
		if ~isfield (names, f.dep{ii})
			warning (['Derived field ' f.name ' depends on ' ...
				'unknown field ' f.dep{ii} '.']);
			isgood = 0;
		end;
	end;
	f.isgood = isgood;

function f = init_raw (f_in, root)
	f = f_in;
	if isempty (f.root)
		f.fname = fullfile (root, f.name);
	else
		f.fname = fullfile (f.root, f.name);
	end;
	f.f = [];
	if ~isempty (dir (f.fname))	% exist (f.fname, 'file')
		d = dir (f.fname);
		switch (f.datatype)
			case {'uint8', 'int8'}, ...
				f.nsamp = d.bytes;
			case {'uint16', 'int16'}, ...
				f.nsamp = d.bytes / 2;
			case {'uint32', 'int32', 'single'}, ...
				f.nsamp = d.bytes / 4;
			case {'uint64', 'int64', 'double'}, ...
				f.nsamp = d.bytes / 8;
			otherwise,
				f.nsamp = d.bytes;
		end;
	else
		f.nsamp = 0;
	end;

function f = init_linterp (f_in, root)
	f = f_in;
	if isempty (f.root)
		f.fname = fullfile (root, f.table);
	else
		f.fname = fullfile (f.root, f.table);
	end;
	if ~isempty (dir (f.fname))	% ~exist (f.fname, 'file')
		warning (['LINTERP table file ' f.fname ' not found.']);
		f.isgood = 0;
	end;
	f.lookup = [];

%%%%

function f = empty_field
	f = struct ('name', '', 'type', '', ...
		    'root', '', 'dep', [], ...
		    'n', [], 'a', [], 'b', [], ...
		    'fname', '', 'f', [], ...
		    'isgood', [], 'shift', [], ...
		    'firstbit', [], 'bits', [], ...
		    'nsamp', [], 'spf', [], 'datatype', '', ...
		    'table', '', 'lookup', []);
