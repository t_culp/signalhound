function h = read_filter_def (fname)
% READ_FILTER_DEF  Read in FIR filter coefficients from
%  a text file.  This is used to read files of the type
%  that the GCP mediator uses to define its antialiasing
%  filter.

f = fopen (fname, 'rt');
h = [];
ii = 0;
while ~feof (f)
        ll = fgetl (f);
        ll = strtrim (ll);
        if isempty(ll) || (ll(1) == '#')
                continue;
        end;
        tmp = str2num (ll);
        if (length(tmp) ~= 2) || (tmp(1) ~= ii)
                error (['Bad line <' ll '> in file ' fname]);
        end;
        ii = ii + 1;
        h = [h, tmp(2)];
end;
fclose (f);

