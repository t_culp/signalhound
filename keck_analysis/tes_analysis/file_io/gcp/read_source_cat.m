% p=read_source_cat(filename)
%
% Read a source catalog as used by GCP. These
% can be found in gcp/control/ephem/.  The
% output structure has the following fields:
%
%   p.name: Source name
%   p.type: Source type
%   For type 'FIXED':
%     p.az, p.el
%   For type 'CENTER':
%     p.az, p.el, p.dk
%   For type 'J2000':
%     p.ra, p.de, p.dra, p.dde

function p = read_source_cat(filename)

f = fopen(filename,'rt');
if f<=0
  error(['Failed to open source catalog ' filename]);
end

p = [];

j = 0;
while ~feof(f)
  % Read one line
  ll = fgetl(f);
  % Disregard comments and blank lines
  [ll cmt] = strtok([' ' ll],'#');
  ll = strtrim(ll);
  if isempty(ll)
    continue
  end

  [srctype ll] = strtok(ll);
  ll = strtrim(ll);
  [srcname ll] = strtok(ll);
  ll = strtrim(ll);
  alias=''; az=0; el=0; dk=0; ra=0; de=0; dra=0; dde=0;

  switch(lower(srctype))
    case 'fixed',
      [azstr ll] = strtok(ll);
      ll = strtrim(ll);
      [elstr ll] = strtok(ll);
      ll = strtrim(ll);
      az = parse_deg(azstr);
      el = parse_deg(elstr);
    case 'center',
      [azstr ll] = strtok(ll);
      ll = strtrim(ll);
      [elstr ll] = strtok(ll);
      ll = strtrim(ll);
      [dkstr ll] = strtok(ll);
      ll = strtrim(ll);
      az = parse_deg(azstr);
      el = parse_deg(elstr);
      dk = parse_deg(dkstr);
    case 'j2000',
      [rastr ll] = strtok(ll);
      ll = strtrim(ll);
      [destr ll] = strtok(ll);
      ll = strtrim(ll);
      if isempty(ll)
        drastr = '0';
        ddestr = '0';
      else
        [drastr ll] = strtok(ll);
        ll = strtrim(ll);
        [ddestr ll] = strtok(ll);
        ll = strtrim(ll);
      end
      ra = parse_hrs(rastr);
      de = parse_deg(destr);
      dra = parse_hrs(drastr);
      dde = parse_deg(ddestr);
    case {'alias','ephem'},
      alias = sscanf(ll,'%s');
    otherwise,
      error(['Don''t know what to do with source of type ' srctype]);
  end

  j = j + 1;
  p.name{j} = srcname;
  p.type{j} = srctype;
  p.az(j) = az;
  p.el(j) = el;
  p.dk(j) = dk;
  p.ra(j) = ra;
  p.de(j) = de;
  p.dra(j) = dra;
  p.dde(j) = dde;
  p.alias{j} = alias;

end

fclose(f);

return



function deg = parse_deg (degstr)

  if strcmp(degstr,'*')
    deg = 0;
    return
  end

  tok = regexp(degstr,'^([+-]?\d+(\.\d+)?)$','tokens');
  if ~isempty(tok)
    deg = str2num(tok{1}{1});
    return
  end

  tok = regexp(degstr,'^([+-]?)(\d+):(\d+(\.\d+)?)$','tokens');
  if ~isempty(tok)
    sgn = +1;
    if strcmp(tok{1}{1},'-')
      sgn = -1;
    end
    deg = str2num(tok{1}{2});
    frac = str2num(tok{1}{3})/60;
    deg = sgn * (deg + frac);
    return
  end

  tok = regexp(degstr,'^([+-]?)(\d+):(\d+):(\d+(\.\d+)?)$','tokens');
  if ~isempty(tok)
    sgn = +1;
    if strcmp(tok{1}{1},'-')
      sgn = -1;
    end
    deg = str2num(tok{1}{2});
    frac = str2num(tok{1}{3})/60 + str2num(tok{1}{4})/3600;
    deg = sgn * (deg + frac);
    return
  end

  error(['Failed to parse angular coordinate ' degstr]);

  return

function hrs = parse_hrs (hrsstr)
  hrs = parse_deg (hrsstr);
  return

