% p=read_ephem(filename)
%
% Read an ephemeris file as used by GCP. These
% can be found in gcp/control/ephem/.  The
% output structure has the following fields:
%
%   p.mjd: Modified Julian date
%   p.datenum: time as a Matlab datenum
%   p.ra: right ascension in hours
%   p.ra_deg: right ascension in degrees
%   p.de: declination in degrees
%   p.dist: distance in AU

function p = read_ephem(ephemfile)

[a b c d e f g h i] = textread(ephemfile, ...
    '%f %d:%d:%f %c%d:%d:%f %f', ...
    'commentstyle','shell');

p.mjd = a;
p.ra = abs(b) + c/60 + d/3600;
% p.ra(b<0) = -1 * p.ra(b<0);
p.de = abs(f) + g/60 + h/3600;
p.de(e=='-') = -1 * p.de(e=='-');
p.dist = i;

% Convert MJD to datenum
[yy,mm,dd,HH,MM,SS]=mjd2date(p.mjd);
p.datenum=datenum(yy,mm,dd,HH,MM,SS);

% Express RA in degrees
p.ra_deg = p.ra * 360/24;

return

