function R = read_mce_runfile (fname)
% S = READ_MCE_RUNFILE (F)  reads MCE .run files.
%
% The run file contains all information about the
% configuration of MCE: the SQUID settings and
% biases, the data mode, etc.  Raw data from MCE
% can be interpreted only with the help of the
% corresponding run file.  The run file typically
% has the same file name as the data file, but
% with the extension '.run' added.
%
% F is the run file's file name.
%
% S is a structure whose fields correspond to the
% sections of the run file.
%
% Note: you should ordinarily use READ_MCE,
% which reads the run file and the raw data,
% returning everything in a convenient structure.
%
% rwo 080425
% rwo 080626 handle non-standard "pixel_flag" section

R = [];
f = fopen (fname, 'rt');

% Read one section at a time, until there are no more.
while ~feof (f)
	[sect, sectname] = read_runfile_section (f);
	if isempty (sectname)
		break;
	end;
	sectname = lower (sectname);
	R.(sectname) = sect;
end;
fclose (f);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [sect, sectname] = read_runfile_section (f)

	% read line with section name
	[H D] = read_runfile_line (f);

	% Might have come to end of file
	if isempty(H) && isempty(D)
		sect = [];
		sectname = [];
		return;
	end;

	% Some run files have a "pixel_flag" section that
	% gratuitously breaks the run file format specification
	if (length(H)==1) && (length(D)>0) && ischar(H{1}) && strcmp(H{1}, 'PIXEL_FLAG')
		sectname = 'pixel_flag';
		sect = read_pixel_flag (D);
		return;
	end;

	% check section header line makes sense
	if (length(H)~=1) || (length(D)>0)
		error (['Malformed section header line.']);
	end;
	sectname = H{1};

	% Keep reading lines until end of file or
	% end of section.
	isdone = 0;
	sect = [];
	while (isdone == 0) && ~feof (f)
		[H D] = read_runfile_line (f);

		% Found end of section...
		if H{1}(1) == '/'
			isdone = 1;
			if (length(H)~=1) || (length(D)>0)
				error (['Malformed section ender line.']);
			end;
			if ~strcmp (H{1}(2:end), sectname)
				error (['Found closer for section "' H{1}(2:end) '", expected "' sectname '".']);
			end;

		% ... or ordinary line.
		else
			sect = parse_one_line (sect, H, D);
		end;
	end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [H D] = read_runfile_line (f)

	ll = '';
	while isempty (ll)
		ll = fgetl (f);
		if ~ischar(ll)
			H = {}; D = {};
			return;
		else
			ll = strtrim (ll);
		end;
		if length(ll)>=1 && (ll(1) == '#')	% comment
			ll = '';
		end;
	end;
	if (ll(1) ~= '<')
		error (['Malformed run file line beginning "' ll(1:min(20,length(ll))) '".']);
	end;
	ll = ll(2:end);

	% Parse everything inside the < >
	% into a cell array
	[hh ll] = strtok (ll, '>');
	H = {};
	while ~isempty (hh)
		[tok hh] = strtok (hh);
		H = {H{:}, tok};
	end;

	% Parse everything past the < >
	ll = strtrim (ll(2:end));

	% If it's empty, stop.
	if isempty (ll)
		D = [];
		return;
	end;

	% Run files sometimes use "**" for blank.  Replace with NaN.
	ll = strrep (ll, '**', 'NaN');

	% Interpret as list of numbers if possible.
	D = str2num (ll);
	if ~isempty (D)
		return;
	end;

	% Otherwise, just return it as a string.
	D = ll;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sect = parse_one_line (sect_in, H, D)

	sect = sect_in;

	% Basically a list of special cases...
	% have to know what sorts of lines go into
	% runfiles.

	% Column-numbered quantities from squid section
	if (length(H)==1) && ~isempty(regexp(H{1}, '^Col(\d)+_'))
		tmp = H{1}(4:end);
		[colnum tmp] = strtok (tmp, '_');
		tmp = tmp(2:end);
		colnum = str2num (colnum);
		sect.Col(colnum+1).(tmp) = D;
		sect.Col(colnum+1).col_num = colnum;
		return;
	end;

	% Quantities read from board in "HEADER" section
	if (length(H)==3) && strcmp(H{1}, 'RB')
		% Interpret board names a little bit...
		% some are numbered:
		board_num = str2num (H{2}(end));
		if isempty (board_num)
			board_num = 1;
			board_name = H{2};
		else
			board_name = H{2}(1:(end-1));
		end;
		sect.(board_name)(board_num).(H{3}) = D;
		return;
	end;

	% ... everything else!
        if exist ('OCTAVE_VERSION', 'builtin')
		for (ii = 1:length(H))
			H{ii} = strrep (H{ii}, '.', '_');
		end;
	else
		H = strrep (H, '.', '_');	% can't have . in field names
	end;
	sect = setfield (sect, H{:}, D);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sect = read_pixel_flag (D)

	% Remove trailing </PIXEL_FLAG>
	[A, B] = strtok (D, '<');
	if isempty (B) || ~strcmpi (B, '</PIXEL_FLAG>')
		sect = [];
		error (['Bad PIXEL_FLAG section.']);
		return;
	end;

	sect = str2num (A);
