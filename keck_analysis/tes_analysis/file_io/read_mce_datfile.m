function [D A H] = read_mce_datfile (fname, mode, numframes, pix, varargin)
% D = READ_MCE_DATFILE (F, M)  reads MCE raw data frames
%
% F is a raw data file (usually with no extension)
% M is the data mode (defaults to 4)
%
% READ_MCE_DATFILE (F, M, N) saves some time by
% pre-allocating N frames of data.  If N is not
% specified, it will allocate one frame at a time.
%
% READ_MCE_DATFILE (F, M, N, P) reads from each frame
% a single pixel specified by P = [ROW, COL].  This is
% much faster than reading the whole array.  When more
% than one readout card is present, RC 1 is indicated
% by columns 0-7, RC 2 by columns 8-15, etc.
%
% Output:
%    [D A H] = READ_MCE_DATFILE (...)
%
%    D(frame,row,column) is Squid 1 feedback signal
%                        in DAC units.
%    A(frame,row,column) is aux signal: either error signal
%                        (in ADC units)
%                        or number of flux jumps.
%    H(frame,1:43)       is data header from each frame.
%
% READ_MCE_DATFILE (F, M, N, P, PARAM, VALUE) accepts
% options as parameter-value pairs.  Recognized options
% and default values are:
%
%    DO_UNPACK=1    : unpack the feedback and auxiliary
%                     parts of each data word.  If set to
%                     zero, the raw words are returned.
%    DO_UNWRAP=1    : automatically recognize when the feedback
%                     value for a pixel wraps around from the
%                     high to the low end of the digitizer
%                     range.  Has effect only when DO_UNPACK=1.
%     DO_SCALE=1    : return feedback values in DAC units=
%                     and error signal in ADC units * num_samples.
%                     If set to zero, output is in integer
%                     form.  Has effect only when DO_UNPACK=1.
% DO_FILT_GAIN=1    : correct for the filter gain, 2044,
%                     in filtered data modes.
%  FILTER_GAIN=1218 : use the gain from the 150-Hz filter.
%                     Default is 2044, appropriate for use with
%                     the new 30-Hz filter.
%
% See docs at
% http://www.phas.ubc.ca/~mce/mcedocs/Software/SC2_ELE_S580_526_mce_file_format.pdf
%
% NOTE: you should ordinarily use READ_MCE,
% which reads both the run file and the raw data,
% and returns everything in a convenient structure.
%
% NOTE on rectangle mode: data taken in rectangle mode has a
% different value of num_rows and num_cols in the clock card
% and readout cards.  The frame sizes must be commensurable,
% so that several frames from the readout card get packed in
% to a single frame of the clock card, and then we find them
% packed into a single frame in the frame file.  In order to
% recover the raw TES time streams, we need a rearrangement.
% This can only be done using information from the run file,
% because the RC's values for num_rows and num_cols are only
% found there, not in the frame file header.  Therefore, the
% reshaping is done in read_mce.  If you load rectangle mode
% data with read_mce_datfile alone, it will not be reshaped.
%
% Single pixel readout is not supported with rectangle mode.
%
% rwo 080425
% rwo 080602 - speedups, fast single-pixel mode
% rwo 080702 - add PVPs for more "raw" output
% jpf 090226 - fixed scaling errors in filtered data modes
% rwo 090302 - fix counting of readout cards when some are off
% jpf 090312 - fix mode 0 to return signed integer 
% rwo 090317 - use new unpacking code with bit values straight from UBC wiki
% rwo 090318 - use mas_data.pro to correct errors in MCE wiki table
%            - add DO_FILT_GAIN option
% jab 091013 - add rectangle mode support, mods are in lines 198:206
% rwo 100106 - add FILTER_GAIN option
% rwo 100211 - fix handling of rectangle mode; rearrange in read_mce, not here
% jpf 100310 - default to DO_UNWRAP=0
% rwo 101229 - fix scaling in data mode 7 (use correct scaling, not the one from IDL code)
% rwo 140123 - handle header v7, speed up when reading header
    
    if (nargin < 2) || isempty(mode)
        mode = 4;
    end;
    if (nargin < 3) || isempty (numframes)
        numframes = [];
    end;
    if (nargin < 4) || isempty (pix)
        pix = [];
    end;
    if (nargin < 5) || isempty (varargin)
        varargin = {};
    end;
    
    % Check for any parameter options
    [opts.DO_UNPACK, opts.DO_UNWRAP, opts.DO_SCALE, opts.DO_FILT_GAIN, opts.FILTER_GAIN] ...
        = paramparse ...
        ({'DO_UNPACK', 'DO_UNWRAP', 'DO_SCALE', 'DO_FILT_GAIN', 'FILTER_GAIN'}, ...
        {1, 1, 1, 1, 1218}, varargin{:});
    
    if (nargout < 3)
        if ~isempty (pix)
            % Read one pixel, fast
            dd = read_one_pix (fname, numframes, pix(1), pix(2));
        else
            % Read everything, but drop header
            dd = read_all_pix (fname, numframes);
        end;
    else
        % Keep header

	% read_everything is old, slow version -- still useful for
	% testing if MAS file format changes
        % [dd H] = read_everything (fname, numframes);

	% read_all_pix_and_headers is faster version
        [dd H] = read_all_pix_and_headers (fname, numframes);
        % Pick out one pixel, if desired
        if ~isempty (pix)
            dd = dd(pix(1)+1, pix(2)+1);
        end;
    end;
    
    % Now separate primary data (in D) from
    % auxiliary data (usually error signal, flux jumps, etc.)
    % in A
    [D A] = mce_unpack_scale (dd, mode, opts);

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Slower routine to read in all pixels + all headers
%

function [dd H] = read_everything (fname, numframes)
    f = fopen (fname, 'rb');
    ii = 0;
    nrows = NaN;
    ncols = NaN;
    dd = [];
    if (nargout >= 2)
        H = NaN * ones (numframes, 43);
    end;
    if isempty (numframes)
        numframes = 1;
    end;
    
    % Read in frames, one at a time.  Each one has its own header,
    % so we can do some consistency checks.  The 3-d array dd
    % will hold the results for now, with feedback, error signal,
    % num flux jumps, etc. still combined within each word (for now).
    while ~feof (f)
        ii = ii + 1;
        
        % Check header, get num rows & cols if not yet known.
        hh = read_mce_header (f);
        if isempty (hh)
            ii = ii - 1;
            break;
        end;
        if hh(7)~=6 && hh(7)~=7
            fmt = check_for_text (hh);
            if ~isempty (fmt)
                error (['File ' fname ' is not a frame file.  Looks like bias or runfile beginning with "' fmt '".']);
            end
            error ('Only MCE header versions 6 and 7 are supported!');
        end;
        if isnan (nrows)
            nrows = hh(4);
        elseif hh(4) ~= nrows
            error (['Frame ' num2str(ii) ' reports ' num2str(hh(4)) ' rows, expected ' ...
                num2str(nrows) '.']);
        end;
        num_readout_cards = sum (bitget (hh(1), 1 + [10 11 12 13]));
        % disp (['Num readout cards = ' num2str(num_readout_cards)]);
        if isnan (ncols)
            % We have to do a little work here to support both older data
            % (in which we always have 8 columns per RC)
            % and newer data (with rectangle mode, in which
            % the # cols is stored in the frame status word).
            % Look for this info; if not found, it's 8.
            % Note that in rectangle mode, this gives the value for
            % the CC, which may not match the RC.
            ncols = bin2dec(num2str(bitget(hh(1), 20:-1:17)));      %JAB bits 19:16(LSB) of word 0 contain cols per RC, i.e. cc_numrows
            if (ncols == 0)
                ncols = 8;
            end;
            ncols = ncols * num_readout_cards;
        end;
        % Store header if it's wanted as output.
        if (nargout >= 2)
            H(ii,:) = hh;
        end;
        
        if isempty (dd)
            dd = uint32 (zeros (ncols, nrows, numframes));
        end;
        
        dd(:,:,ii) = read_dat_block (f, nrows, ncols);
    end;
    fclose (f);
    
    % Drop any allocated but unfilled frames
    dd = dd(:,:,1:ii);
    if (nargout >= 2)
        H = H(1:ii,:);
    end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    
% Faster routine to read in all pixels in all frames,
% and the headers too.  Drop in replacement for
% read_everything, but not tested much.
%

function [dd H] = read_all_pix_and_headers (fname, numframes)
    if isempty (numframes)
        numframes = Inf;
    end;
    f = fopen (fname, 'rb');

    % Read first header only
    hh = read_mce_header (f);
    if isempty (hh)
        dd = [];
        return;
    end;
    if hh(7)~=6 && hh(7)~=7
        fmt = check_for_text (hh);
        if ~isempty (fmt)
            error (['File ' fname ' is not a frame file.  Looks like bias or runfile beginning with "' fmt '".']);
        end
        error ('Only MCE header versions 6 and 7 supported!');
    end;
    cc_nrows = hh(4);
    rc_present = bitget (hh(1), 1 + [10 11 12 13]);
    num_rc = sum (rc_present);

    % We have to do a little work here to support both older data
    % (in which we always have 8 columns per RC)
    % and newer data (with rectangle mode, in which
    % the # cols is stored in the frame status word).
    % Look for this info; if not found, it's 8.
    % Note that in rectangle mode, this gives the value for
    % the CC, which may not match the RC.
    cc_ncols = bin2dec(num2str(bitget(hh(1), 20:-1:17)));      %JAB bits 19:16(LSB) of word 0 contain cols per RC, i.e. cc_numrows 
    if (cc_ncols == 0)
        cc_ncols = 8;
    end;
    cc_ncols = cc_ncols * num_rc;

    % Back to beginning
    fseek (f, 0, 'bof');

    % Read in all frames at once, using sexy Matlab command
    dd = fread (f, numframes*(cc_nrows*cc_ncols+43+1), [num2str(cc_nrows*cc_ncols) '*uint32=>uint32']);

    % Reshape to get header out
    dd = reshape (dd, (cc_nrows*cc_ncols+43+1), []);
    H = dd (1:43, :)';
    dd = dd(44:(end-1), :);

    % Reshape to arrange things according to clock card frame layout.  In rectangle
    % mode, this will not directly recover the TES time streams: another rearrangement
    % is needed.  This subsequent rearrangement will be done in read_mce.m, NOT here,
    % since information from the runfile is needed.
    dd = reshape (dd, cc_ncols, cc_nrows, []);

    fclose (f);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    
% Faster routine to read in all pixels in all frames,
% but skipping all headers after the first
%

function dd = read_all_pix (fname, numframes)
    if isempty (numframes)
        numframes = Inf;
    end;
    f = fopen (fname, 'rb');
    
    % Read first header only
    hh = read_mce_header (f);
    if isempty (hh)
        dd = [];
        return;
    end;
    if hh(7)~=6 && hh(7)~=7
        fmt = check_for_text (hh);
        if ~isempty (fmt)
            error (['File ' fname ' is not a frame file.  Looks like bias or runfile beginning with "' fmt '".']);
        end
        error ('Only MCE header versions 6 and 7 are supported!');
    end;
    cc_nrows = hh(4);
    rc_present = bitget (hh(1), 1 + [10 11 12 13]);
    num_rc = sum (rc_present);

    % We have to do a little work here to support both older data
    % (in which we always have 8 columns per RC)
    % and newer data (with rectangle mode, in which
    % the # cols is stored in the frame status word).
    % Look for this info; if not found, it's 8.
    % Note that in rectangle mode, this gives the value for
    % the CC, which may not match the RC.
    cc_ncols = bin2dec(num2str(bitget(hh(1), 20:-1:17)));      %JAB bits 19:16(LSB) of word 0 contain cols per RC, i.e. cc_numrows 
    if (cc_ncols == 0)
        cc_ncols = 8;
    end;
    cc_ncols = cc_ncols * num_rc;

    % Read in all frames at once, using sexy Matlab command
    dd = fread (f, numframes*cc_nrows*cc_ncols, [num2str(cc_nrows*cc_ncols) '*uint32=>uint32'], (43+1)*4);

    % Reshape to arrange things according to clock card frame layout.  In rectangle
    % mode, this will not directly recover the TES time streams: another rearrangement
    % is needed.  This subsequent rearrangement will be done in read_mce.m, NOT here,
    % since information from the runfile is needed.
    dd = reshape (dd, cc_ncols, cc_nrows, []);

    fclose (f);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    
% Faster routine to read in a single pixel across all frames,
% also skipping all headers after the first
%

function dd = read_one_pix (fname, numframes, row, col)
    if isempty (numframes)
        numframes = Inf;
    end;
    f = fopen (fname, 'rb');
    
    % Read first header only
    hh = read_mce_header (f);
    if isempty (hh)
        dd = [];
        return;
    end;
    if hh(7)~=6 && hh(7)~=7
        fmt = check_for_text (hh);
        if ~isempty (fmt)
            error (['File ' fname ' is not a frame file.  Looks like bias or runfile beginning with "' fmt '".']);
        end
        error ('Only MCE header versions 6 and 7 are supported!');
    end;
    nrows = hh(4);
    rc_present = bitget (hh(1), 1 + [10 11 12 13]);
    num_rc = sum (rc_present);
    ncols = num_rc * 8;
    % If needed,
    % translate column number to count only
    % readout cards that are actually present
    if (num_rc > 1) || (col > 7)
        get_rc = floor (col / 8) + 1;
        get_col = mod (col, 8);
        % rc_tmp = find (rc_present.*cumsum(rc_present) == get_rc);
        rc_tmp = sum (rc_present (1:get_rc));
        if (rc_present (get_rc) == 0)
            error (['Readout card ' num2str(get_rc) ' not present in data file.']);
        end;
        col = (rc_tmp-1)*8 + get_col;
        if length(col)>1
            error (['Found more than one readout card ' num2str(get_rc) '!']);
        end;
    end;
    
    frame_size = (43 + nrows*ncols + 1) * 4;
    initial_skip = (ncols*row + col)*4;
    % initial_skip = (nrows*col + row)*4;
    
    % Skip to desired pixel in 1st frame
    r = fseek (f, initial_skip, 'cof');
    if (r ~= 0)
        dd = [];
        return;
    end;

    % Read in all frames at once, using sexy Matlab command
    dd = fread (f, numframes, 'uint32=>uint32', frame_size-4);
    dd = dd(:);
    fclose (f);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% For use with data header version 6 or 7
%

function H = read_mce_header (f)
    H = [];
    [tmp c] = fread (f, 43, 'uint32');
    if (c == 43)
        H = tmp;
    end;
    
% For use in all modes except "raw mode" (mode 3)
function D = read_dat_block (f, nrows, ncols)
    D = NaN * ones (ncols, nrows);
    [tmp c] = fread (f, ncols*nrows + 1, 'uint32=>uint32');
    if (c == ncols*nrows+1)
        D(:) = tmp(1:(end-1));
        D = D;
    end;
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% separate out feedback, flux jumps, err signal, etc.
% also take out any scaling of the feedback signal.
% Up-to-date (but unreliable) info is here:
%   http://cmbr.phas.ubc.ca/mcewiki/index.php/Data_mode
% I have checked against UBC's script mas_data.pro.
%
% Want to scale feedback to have 0 to 2^14 as full DAC range.
%
% Docs give each mode's calibration rel. to mode 1.
% Scale mode 1 by 2^12 for 12 additional bits from averaging.
%
% The digital filter has a gain of 2044 in DAC units, so
% its "true" gain on the mode 1 feedback value is 2044/2^12.  
% This means that we don't need to divide out the factor of 
% 2^12 in filtered data: the value returned in the bit field 
% has already gone through that.
%
% Note that this does NOT match the convention of the IDL
% script mas_data.pro.  That script leaves out the factor of
% 2044 from the digital filter (on the grounds that the filter
% might change).
%

function [D A] = mce_unpack_scale (dd, mode, opts)
    
    if ~opts.DO_UNPACK
        D = dd;
        A = [];
        return;
    end;

  if (0)
    [D A] = mce_unpack (dd, mode);
    if (~opts.DO_FILT_GAIN) && ismember (mode, [2 6:10])
        disp ('Undoing filter gain scaling.');
        D = D * opts.FILTER_GAIN;
    end;
    return;
  end;

    % Data mode info:
    % *.g = gain
    % *.b = [b1 b2 b3], where
    %       b1 = sign bit if used (-1 otherwise)
    %       b1-b2 = main range of bits (-1 if absent)
    fb.g = 1;
    fb.b = [-1 -1 -1];
    aux.g = 1;
    aux.b = [-1 -1 -1];
    if (opts.DO_FILT_GAIN)
        FILTER_GAIN = opts.FILTER_GAIN;
    else
        FILTER_GAIN = 1;
    end;
    switch (mode)
        case {0, 3, 12},	% 32-bit err signal only
            aux.b = [31 30 0];
            
        case 1,		% 32-bit Sq 1 fb data
            fb.g = 2^12;
            fb.b = [31 30 0];
            
        case 2,		% low-pass filtered version of mode 1
            fb.g = FILTER_GAIN;
            fb.b = [31 30 0];
            
        case 4,		% 18b Sq1 fb + 14b error signal
            fb.g = 1;
            fb.b = [31 28 12];
            aux.g = 1;
            aux.b = [31 12 0];
            
        case 5,		% 24b Sq1 fb + 8b num flux jumps
            fb.g = 2^-8;
            fb.b = [31 30 8];
            aux.g = 1;
            aux.b = [7 6 0];
            
        case 6,		% 18b filtered fb + 14b error
            fb.g = FILTER_GAIN/2^11;
            fb.b = [31 27 11];
            aux.g = 1;
            aux.b = [31 12 0];

	% Note: Proper gain of error signal in mode 7 is
	% with aux.g = 2^-4.  This is consistent with the
	% MCE wiki, but differs from the IDL code and
	% possibly the python code.  Unpacking here was
	% previously made to match the IDL code, but now
	% correcting to follow the documentation.
	%	-- RWO 2010-dec-29
 
        case 7,		% 22b filtered fb + 10b err sig
            fb.g = FILTER_GAIN/2^7;
            fb.b = [31 27 7];
            aux.g = 2^-4;
            aux.b = [31 12 4];
            
        case 8,     % 24b filtered + 8b num flux jumps
            fb.g = FILTER_GAIN/2^8;
            fb.b = [31 30 8];
            aux.g = 1;
            aux.b = [7 6 0];
            
        case 9,     % 24b filtered +8b num flux jumps
            fb.g = FILTER_GAIN/2;
            fb.b = [31 23 1];
            aux.g = 1;
            aux.b = [7 6 0];
            
        case 10,     % 25b filtered + 7b num flux jumps
            fb.g = FILTER_GAIN/2^3;
            fb.b = [27 26 3];
            aux.g = 1;
            aux.b = [6 5 0];
            
        otherwise,
            error (['Unknown data mode ' num2str(mode)]);
    end;

    on_bit = 31;
    sgn = zeros (size (dd));
    if (fb.b(1) > -1)
        sgn = bitshift (dd, -on_bit);
        dd = bitset (dd, on_bit+1, 0);
        on_bit = on_bit - 1;
    end;
    if (fb.b(2) ~= -1)
    	n = fb.b(2) - fb.b(3) + 1;
        val = bitshift (dd, - (on_bit-n+1));
        val = bitshift (val, fb.b(3));
        on_bit = on_bit - n;
        dd = bitand (dd, 2^(on_bit+1)-1);
        % dd = mod (dd, 2^(on_bit+1));
	D = double (val) - double (sgn * 2^(fb.b(2)+1));
        if (opts.DO_UNWRAP)
            % For Mode 10 and up, UBC stopped sliding the
            % sign bit around.
            if (mode < 10)
    	        D = unwrap_range (D, fb.b(2)+1);
            else
                D = unwrap_range (D, fb.b(2)+2);
            end;
        end;
        if (opts.DO_SCALE)
	    D = D / fb.g / 2^fb.b(3);
        end;
    else
        D = zeros (size (dd));
    end;
    sgn = zeros (size (dd));
    if (aux.b(1) > -1)
        sgn = bitshift (dd, -on_bit);
        dd = bitset (dd, on_bit+1, 0);
        on_bit = on_bit - 1;
    end;
    if (aux.b(2) ~= -1)
        n = aux.b(2) - aux.b(3) + 1;
        val = bitshift (dd, - (on_bit-n+1));
        val = bitshift (val, aux.b(3));
        on_bit = on_bit - n;
        dd = bitand (dd, 2^(on_bit+1)-1);
        % dd = mod (dd, 2^(on_bit+1));
	A = double (val) - double (sgn * 2^(aux.b(2)+1));
        if (opts.DO_UNWRAP)
    	    A = unwrap_range (A, aux.b(2)+1);
        end;
        if (opts.DO_SCALE)
    	    A = A / aux.g / 2^aux.b(3);
        end;
    else
        A = zeros (size (dd));
    end;

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% handle data that may exceed the range of the output
% variable.  This is OK as it wraps around; we can unwrap it.
%
    
function D = unwrap_range (D_in, numbits)
    
    n2 = 2^(numbits-1);
    % D = mod (D_in, n2*2);
    D = D_in;
    
    adj_val = zeros (size(D,3), 1);
    
    % Loop over pixels
    for (i_row = 1:size(D,2))
        for (i_col = 1:size(D,1));
            fb = squeeze(D (i_col,i_row,:));
            d = diff (fb);
            
            % Find ticks that hop from one end of range to the other
            jump_frame = (abs (d) >= n2);

            adj_val(:) = 0;
            adj_val ([jump_frame; false]) = -1 * sign (d(jump_frame)) * 2*n2;
            adj_val = cumsum ([0; adj_val(1:(end-1))]);
            
            D (i_col, i_row, :) = fb + adj_val;
        end;
    end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% check whether file matches an associated
% text file format.
%

function fmt = check_for_text (h)

    str = char (typecast (cast (h, 'uint32'), 'uint8'));
    str = reshape (str, 1, []);
    tok = regexp (str, '<([_a-zA-Z])+>', 'tokens');
    if ~isempty (tok)
        fmt = tok{1}{1};
    else
        fmt = '';
    end

    

