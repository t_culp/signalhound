function B = read_mce_biasfile (fname)
% B = READ_MCE_BIASFILE (F)
%
% Reads the bias values used in an
% I-V ramp used for characterizing detectors,
% or in tuning squids, etc.
%
% F is the bias file name, usually the same
% as the raw data file plus the extension
% '.bias'
%
% The bias file may contain a number of
% columns, giving various squid feedback
% settings, TES biases, etc.  They are
% returned in a structure B with one field
% per column.
%
% RWO 080430
% RWO 080625 allow multiple columns
% JPF 091117 use textscan (much faster for large files)

f = fopen (fname, 'rt');

% Read header row
ll = fgetl (f);
C = textscan (ll, '<%[^>]>', -1);
if ~isempty (C) && iscell (C)
	C = C{1};
end;
if isempty (C)
	error ('Unrecognized bias file format.');
end;

% grab the data with textscan
% one numeric value per header entry
fpattern = strtrim(repmat('%f ',1,length(C)));
Btmp = textscan(f,fpattern);

fclose (f);

for (ii = 1:length (C))
	B.(C{ii}) = Btmp{ii};
end;
