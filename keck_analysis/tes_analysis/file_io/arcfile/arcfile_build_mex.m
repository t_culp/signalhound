function arcfile_build_mex

% Find directory with arcfile functions
p = mfilename ('fullpath');
arcfile_dir = fileparts (p);

% Where mex source and binaries live
mex_code_dir = fullfile (arcfile_dir, '..', '..', 'mex_code', 'arcfile');
mex_bin_dir = fullfile (arcfile_dir, '..', '..', 'mex_bin');

% Create binary directory if it's not there
if ~exist (mex_bin_dir, 'dir')
	mkdir (mex_bin_dir);
	% Also add to path
	addpath (mex_bin_dir);
end;

src_list = {'mex_readarc.c', 'databuf.c', 'dataset.c', 'arcfile.c', 'endian.c', 'fileset.c', 'namelist.c', 'readarc.c', 'reglist.c', 'utcrange.c', 'handlesig.c'};
for (ii = 1:length (src_list))
	src_list{ii} = fullfile (mex_code_dir, src_list{ii});
end;
if exist ('OCTAVE_VERSION', 'builtin')
	mex (src_list{:}, '-o', fullfile (mex_bin_dir, ['readarc.' mexext]));
else
	mex (src_list{:}, '-outdir', mex_bin_dir, '-output', 'readarc', '-lz', '-lbz2');
end;

src_list = {'mex_listarc.c', 'databuf.c', 'dataset.c', 'arcfile.c', 'endian.c', 'fileset.c', 'namelist.c', 'readarc.c', 'reglist.c', 'utcrange.c', 'handlesig.c'};
for (ii = 1:length (src_list))
        src_list{ii} = fullfile (mex_code_dir, src_list{ii});
end;
if exist ('OCTAVE_VERSION', 'builtin')
        mex (src_list{:}, '-o', fullfile (mex_bin_dir, ['list_arc.' mexext]));
else
        mex (src_list{:}, '-outdir', mex_bin_dir, '-output', 'list_arc', '-lz', '-lbz2');
end;

