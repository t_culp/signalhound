function dat = read_Labview_data(fname, n_meas, datepos, columns_to_read)
    % Reads in numerical data from a text file saved from the SPIDER
    % LabView DAQ.  Returns the desired values from all or some columns,
    % along with the associated time stamps (in Matlab datenum format).
    %
    % File format:
    %  Each row is a single time-stamped measurement, space- or
    %  tab-delimited, that ends with a date string:
    %    100.00 100.00 100.00 100.00 09/25/2009 06:59:05
    %    100.00 100.00 100.00 100.00 09/25/2009 06:59:35
    %
    % Takes as arguments:
    %  - fname: the data file name to read in
    %  - n_meas: number of data columns in the data file (not including the
    %  date string).  In the example above, this is 4
    %  - datepos: flag indicating location of date string
    %             0 = first, 1 = last (default)
    %  - columns_to_read (optional): column numbers of the quantities to
    %  return (not including the date string).  If empty, read all columns.
    %
    % Returns a structure containing fields:
    %  - date: a vector giving each measurement's Matlab datenum (as seen
    %  by the clock used in the LabView time stamp)
    %  - meas: an array of floats in which each column is a reading and
    %  the number of rows matches the length of date
    %  - columns_read: a vector of integers giving the column numbers read
    %
    % 20Sep2009 JPF
    % 11Feb2010 JPF Allow optional readout of a subset of columns
    % 16Feb2010 JPF Option for dates to go first or last in file
    
    % check arguments
    if nargin<3
        % no datepos: assume dates are LAST
        datepos = 1;
    end
    if nargin>3 && ~isempty(columns_to_read)
        % the user has specified which columns to read!
        columns_to_read = columns_to_read(inrange(columns_to_read,1,n_meas));
    else
        columns_to_read = [];
    end
    
    if isempty(columns_to_read)
        columns_to_read = 1:n_meas;
    end
    
    % open the input file
    fid = fopen(fname);
    if fid==-1
        error('Error loading input file!');
        return;
    end
    
    % design the parsing string
    get_float = '%f';
    skip_float = '%*f';
    get_date = '%s%s'; % date and time strings
    fmt = '';
    for ii=1:n_meas
        if ismember(ii,columns_to_read)
            fmt = [fmt get_float];
        else
            fmt = [fmt skip_float];
        end
    end
    if datepos
        % read date/time strings at the end of each row
        fmt = [fmt get_date];
    else
        % read date/time strings at the end of each row
        fmt = [get_date fmt];
    end
    
    % parse and close the data file
    C = textscan(fid,fmt);
    fclose(fid);
    
    % prepare the output structure
    dat = [];
    
    % columns read
    dat.columns_read = columns_to_read;
    n_read = length(columns_to_read);
    
    % isolate the measurements from the date
    if datepos
        dd = [C{[n_read+1,n_read+2]}];
        dat.meas = [C{[1:n_read]}];
    else
        dd = [C{[1,2]}];
        dat.meas = [C{[3:(n_read+2)]}];
    end
    
    % parse the date strings to Matlab date numbers
    dat.date = datenum(strcat(dd(:,1),repmat({' '},size(dd,1),1),dd(:,2)));
