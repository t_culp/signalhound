function [S R_out H_out] = read_mce (fname, pix, runfname, varargin)
% S = READ_MCE (F)  reads in MCE raw data and runfiles.
%
% F is a raw data file name (usually with no extension).
% By default, the runfile is assumed to have the same name,
% with the extension .run.
%
% READ_MCE (F, P) reads a single pixel specified by
% P = [ROW, COL].  This is much faster than reading
% the whole array.  When more than one readout card
% is present, RC 1 is indicated by columns 0-7, RC 2
% by columns 8-15, etc.
%
% NOTE: Single-pixel readout is not supported for
% rectangle-mode or raw-mode data.
%
% READ_MCE (F, P, R) specifies the run file R.
%
% Output:
%    [S R H] = READ_MCE (...)
%
%    S is a structure array containing the data and auxiliary
%    information for each pixel read out.
%    R is a structure containing the runfile information.
%    H(frame,1:43) is the data header from the first frame.
%
% READ_MCE (F, P, R, PARAM, VALUE) accepts
% options as parameter-value pairs.  Recognized options
% and default values are:
%
%    DO_UNPACK=1    : unpack the feedback and auxiliary
%                     parts of each data word.  If set to
%                     zero, the raw words are returned.
%    DO_UNWRAP=1    : automatically recognize when the feedback
%                     value for a pixel wraps around from the
%                     high to the low end of the digitizer
%                     range.  Has effect only when DO_UNPACK=1.
%    DO_SCALE=1     : return feedback values in DAC units.
%                     If set to zero, output is in integer form.
%                     Has effect only when DO_UNPACK=1.
%    DO_FILT_GAIN=1 : correct for the filter gain of 2044.
%                     Has effect only in filtered data modes.


fname = deblank (fname);
if (nargin == 1)
    pix = [];
    runfname = '';
end;
if (nargin == 2)
    if ischar (pix)
        runfname = pix;
        pix = [];
    else
        runfname = '';
    end;
end;

if isempty(runfname)
    runfname = [fname '.run'];
end;
if ~exist (runfname, 'file')
    error (['Run file ' runfname ' not found.']);
end;
R = read_mce_runfile (runfname);
if (nargout >= 2)
    R_out = R;
end;

data_mode = [];
filter_gain = 1218;
fw2044 = hex2dec({'5000005','5000007'}); % from MCE wiki
for (ii = 1:length (R.header.rc))
    if ~ismember (ii, R.frameacq.RC)
        continue;
    end;

    
    if ~isempty (R.header.rc(ii).data_mode)
        data_mode = R.header.rc(ii).data_mode;
    end;
    rcstruct=R.header.rc(ii);
    if isfield(R.header.rc(ii), 'fltr_type')
        switch R.header.rc(ii).fltr_type
            case 1
                filter_gain = 1218;
            case 2
                filter_gain = 2044;
            case 255
                par=R.header.rc(ii).fltr_coeff;
                quad1gn=4./(1.-single(par(1))/2^14+single(par(2))/2^14)/2^par(5);
	        quad2gn=4./(1.-single(par(3))/2^14+single(par(4))/2^14)/2^par(6);
                filter_gain=quad1gn*quad2gn;
            otherwise
                error('Unknown Filter type!')
        end 
    elseif isfield(R.header.rc(ii), 'fw_rev')
        if ismember(R.header.rc(ii).fw_rev,fw2044)
            filter_gain = 2044;
        else
            filter_gain = 1218;
        end;
    end;
end;
disp(['filter_gain: ' num2str(filter_gain)]);

if isempty (data_mode)
    error (['No data mode found in run file!']);
end;
% append to varargin the filter gain
% (will be ignored if the user has specified to ignore it)
varargin{length(varargin)+1}='FILTER_GAIN';
varargin{length(varargin)+1}=filter_gain;

numframes = R.frameacq.DATA_FRAMECOUNT;

% Special raw data modes - handle separately.
% I assume that there is no possible intersection of rectangle mode and raw mode.
% If somebody tries to take data that way, they're on their own!
if ((data_mode == 3) || (data_mode == 12))
    if (nargout >= 3)
        [D A H_out] = read_mce_datfile (fname, data_mode, numframes, [], varargin{:});
    else
        [D A] = read_mce_datfile (fname, data_mode, numframes, [], varargin{:});
    end;
    if (data_mode == 12)
        S.fb = [];
        S.num_fj = [];
        S.err = A;
        S.err = S.err(:);
        S.tofs = (1:length(S.err))' / 50e6;
    else
        tofs = (1:(33*size(A,1)))' / 50e6;
        tmperr = permute (A, [2 3 1]);
        tmperr = reshape (tmperr, size(tmperr,1)*size(tmperr,2), size(tmperr,3));
        for (onrc = R.frameacq.RC(:)')
            for (oncol = 1:8)
                S(oncol).mux_col = (oncol-1);
                S(oncol).rc = onrc;
                S(oncol).mux_rc_col = 8 * (onrc-1) + (oncol-1);
                S(oncol).fb = [];
                S(oncol).num_fj = [];
                S(oncol).tofs = tofs;
                S(oncol).err = tmperr (:, 8 * (onrc-1) + (oncol-1) + 1);
            end;
        end;
    end;
    return;
end;

if (nargout >= 3)
    [D A H] = read_mce_datfile (fname, data_mode, numframes, [], varargin{:}, 'FILTER_GAIN', filter_gain);
    S = reorg_data (D, A, data_mode, R);
    S = add_timing_info (S, R.header);
    H_out = H;
elseif isempty (pix)
    [D A] = read_mce_datfile (fname, data_mode, numframes, [], varargin{:}, 'FILTER_GAIN', filter_gain);
    S = reorg_data (D, A, data_mode, R);
    S = add_timing_info (S, R.header);
else
    [D A] = read_mce_datfile (fname, data_mode, numframes, pix, varargin{:}, 'FILTER_GAIN', filter_gain);
    S = reorg_one_pix (D, A, data_mode, R, pix);
    S = add_timing_info (S, R.header);
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Turn time stream for a single pixel into structure

function S = reorg_one_pix (D, A, M, R, P)

% determine which readout cards are reporting.
rc_responding = [0 0 0 0];
rc_responding (R.frameacq.RC) = 1;

S.rc = find (rc_responding.*cumsum(rc_responding) == floor (P(2) / 8) + 1);
S.mux_row = P(1);
S.mux_col = mod (P(2), 8);
S.mux_rc_col = (S.rc-1)*8 + S.mux_col;

if (M == 0)
    S.fb = [];
else
    S.fb = D(:);
end;
if ismember (M, [0 4 6 7])
    S.err = A(:);
else
    S.err = [];
end;
if ismember (M, [5 8 9 10])
    S.num_flux_jumps = A(:);
else
    S.num_flux_jumps = [];
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Turn 3-d arrays of data into a structure arranged by
% pixel, with time streams + info.
%
% The reshaping needed to account for rectangle mode
% is handled here.

function S = reorg_data (D, A, M, R)

% determine which readout cards are reporting.
% Some Spider data has more readout cards listed
% in the runfile RC line than are installed...
% that's screwy.  Filter them out using the
% number of actual header RC sections found.
rc_responding = [0 0 0 0];
rc_responding (R.frameacq.RC) = 1;
rc_responding ((length(R.header.rc)+1):end) = 0;

if (M == 0)
    cc_nrows = size (A,2);
    cc_ncols = size (A,1);
else
    cc_nrows = size (D,2);
    cc_ncols = size (D,1);
end;

% Get readout card rectangle parameters, if recorded.  Make sure all RCs match.
rc_nrows = NaN;
rc_row_index = NaN;
rc_ncols = NaN;
rc_col_index = NaN;
for (onrc = 1:length(R.header.rc))
    if isfield (R.header.rc(onrc), 'num_rows_reported')
        if isnan (rc_nrows)
            rc_nrows = R.header.rc(onrc).num_rows_reported;
        end;
        if (rc_nrows ~= R.header.rc(onrc).num_rows_reported)
            error (['Readout card ' num2str(onrc) ' has non-matching num_rows_reported']);
        end;
    end;
    if isfield (R.header.rc(onrc), 'num_cols_reported')
        if isnan (rc_ncols)
            rc_ncols = R.header.rc(onrc).num_cols_reported;
        end;
        if (rc_ncols ~= R.header.rc(onrc).num_cols_reported)
            error (['Readout card ' num2str(onrc) ' has non-matching num_cols_reported']);
        end;
    end;
    if isfield (R.header.rc(onrc), 'readout_row_index')
        if isnan (rc_row_index)
            rc_row_index = R.header.rc(onrc).readout_row_index;
        end;
        if (rc_row_index ~= R.header.rc(onrc).readout_row_index)
            error (['Readout card ' num2str(onrc) ' has non-matching readout_row_index']);
        end;
    end;
    if isfield (R.header.rc(onrc), 'readout_col_index')
        if isnan (rc_col_index)
            rc_col_index = R.header.rc(onrc).readout_col_index;
        end;
        if (rc_col_index ~= R.header.rc(onrc).readout_col_index)
            error (['Readout card ' num2str(onrc) ' has non-matching readout_col_index']);
        end;
    end;
end;
% If not recorded, we have pre-rectangle-mode data.  Use standard settings.
if isnan (rc_nrows)
    rc_nrows = cc_nrows;
end;
if isnan (rc_ncols)
    rc_ncols = 8;
end;
if isnan (rc_row_index)
    rc_row_index = 0;
end;
if isnan (rc_col_index)
    rc_col_index = 0;
end;

% In rectangle mode, several real frames are packed into the same CC frame in
% the data file.
packing_ratio = (cc_ncols*cc_nrows) / (sum(rc_responding)*rc_ncols*rc_nrows);
if abs (packing_ratio - round(packing_ratio)) > 1e-12
    error (['Data is taken in rectangle mode, on Bizarro Island!  Panic!']);
end;
packing_ratio = round (packing_ratio);
if (packing_ratio > 1)
    if ~isempty (D)
        D = reshape (D, (rc_ncols * sum(rc_responding)), rc_nrows, []);
    end;
    if ~isempty (A)
        A = reshape (A, (rc_ncols * sum(rc_responding)), rc_nrows, []);
    end;
end;

% Loop over real pixels in mux coordinates, not
% slots within CC frame.
onpix = 0;
onrc = 0;
for (oncol = 1:(sum(rc_responding)*rc_ncols))
    % Keep up with which RC's columns we're handling
    if mod(oncol,rc_ncols) == 1
        onrc = onrc + 1;
        while (rc_responding(onrc) == 0)
            onrc = onrc + 1;
            if (onrc > 4)
                error (['Wrong number of readout cards responding!']);
            end;
        end;
    end;
    for (onrow = 1:rc_nrows)
        onpix = onpix + 1;
        S(onpix).rc = onrc;
        S(onpix).mux_row = rc_row_index + (onrow-1);
        S(onpix).mux_col = rc_col_index + mod (oncol-1, rc_ncols);
        S(onpix).mux_rc_col = (onrc-1)*8 + S(onpix).mux_col;
        
        if (M == 0)
            S(onpix).fb = [];
        else
            S(onpix).fb = squeeze (D(oncol,onrow,:));
        end;
        if ismember (M, [0 4 6 7])
            S(onpix).err = squeeze (A(oncol,onrow,:));
        else
            S(onpix).err = [];
        end;
        if ismember (M, [5 8 9 10])
            S(onpix).num_flux_jumps = squeeze (A(oncol,onrow,:));
        else
            S(onpix).num_flux_jumps = [];
        end;
    end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Figure out the time that each sample is taken,
% based on info from the run file "header" section.

function S = add_timing_info (S_in, RH)

% Overall timing setup of MCE:
%    - 50 MHz digitization rate, always constant
%    - take M samples on each row (usually averaged together)
%    - each frame cycles over N rows
%    - therefore, each frame cycle lasts (M * N) / 50 MHz.
%    - take one live frame every P cycles.

M = RH.cc.row_len;
N = RH.cc.num_rows;
P = RH.cc.data_rate;
S = S_in;

row_time = M / 50e6;			% in seconds
frame_time = P * N * row_time;		% in seconds

if ~isempty (S(1).fb)
    nframes = length(S(1).fb);
else
    nframes = length(S(1).err);
end;

for (ii = 1:length(S))
    S(ii).tofs = (S(ii).mux_row * row_time) + (0:(nframes-1)) * frame_time;
end;
