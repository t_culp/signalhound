function parameters=fts_analysis_h6()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20110711 GPT
%
% Define FTS analysis parameters used for run H6 data.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Raw interferograms usually have a large gain drift, sometimes nonlinear.
% This must be removed before anything else happens.  We set the order of a
% polynomial to be removed from the data.

parameters.poly_order=4;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Please place the white light fringe as close to the center of your
% interferograms as possible.  This value is your guess of the worst case
% scenario of how many data points off center the wlf may be.  Large values
% will slow down the computation and cause end points to be truncated.

parameters.wlf_guess_tolerance=25;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Apodization is highly recommended for reducing spectral leakage.  Cutting
% down side lobes comes at the expense of lower resolution.  A Filler
% diagram can help you visualize this compromise.  The classic paper "On
% the Use of Windows for Harmonic Analysis with the Discrete Fourier
% Transform" by Frederic J. Harris and chapter 11 of "Fourier Transform
% Spectrometry" by Sumner P. Davis, Mark C. Abrams, James W. Brault show
% that the Blackman-Harris family of tapers seem the best for our purposes.
% Blackman-Harris tapers are similar to Hamming, but they do a better job
% of cutting out side lobes without sacrificing much resolution.  We can
% afford slightly reduced resolution because the data is probably noisy.

% Matlab has some tapers in its signal processing toolbox.  They aren't
% always up to snuff, especially in our version.  I have written some code
% to generate the Blackman-Harris tapers in an alternative way.  They are
% located in the fts_tapers directory.  Uncomment the one you like, or name
% another Matlab taper like 'hamming'.

%parameters.taper='blackmanharris3';
parameters.taper='blackmanharris4';
%parameters.taper='blackmanharris3_partial';
%parameters.taper='blackmanharris4_partial';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Roughly speaking, traditional filtering usually involves taking a Fourier
% transform of the data, setting unwanted frequencies to a smaller value,
% and then undoing the transform.  In our case, however, blindly removing
% frequencies will bias the very spectrum we are trying to measure!!!

% If we want to denoise the data, we have to do so locally, i.e. in the
% wavelet domain.  Matlab has a toolbox devoted to this, and "wden" is the
% function of interest.

% The FTS analysis code allows for denoising at two stages.  Activate them
% by setting the switches to 1.

parameters.wden_switch{1}=1;
parameters.wden_switch{2}=1;

% Interferograms should be symmetric.  We want to select wavelets that
% preserve this property as much as possible.  The symmetrical wavelets
% supported by the wden function are the discrete Meyer, biorthogonal, and
% reverse biorthogonal wavelets.

parameters.wname{1}='bior2.2';
parameters.wname{2}='dmey';

% decomposition level
parameters.N{1}=2;
parameters.N{2}=2;

% Specify any other options you want to send to wden here.  You probably
% will not have to adjust these, but I'm including them for completeness.
% The Matlab syntax is weird, but I'm matching it.

% threshold selection rule
parameters.TPTR{1}='heursure';
parameters.TPTR{2}='heursure';

% soft or hard thresholding
parameters.SORH{1}='s';
parameters.SORH{2}='s';

% multiplicative threshold rescaling
parameters.SCAL{1}='one';
parameters.SCAL{2}='one';