function [spectral_response ...
      frequencies]=fts_make_spectra(data,wlf_guesses,half_length, det_select)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20110713 GPT
%
% Get spectra for all bolometers.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  data = matrix obtained from d.mce0.data.fb or similar
%
%  wlf_guesses = vector of roughly which points may be white light fringes
%   Plot a working detector and find these indices by eye.
%
%  half_length = maximum number of acceptable data points on either side of
%   an interferogram
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  spectral_response = N_detectors*N_scans*N_frequencies array of spectra
%
%  frequencies = N_detectors*N_scans*N_frequencies array of frequencies
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin<3
    error('fts:inp','Please give me three input arguments.')
end

if wlf_guesses(1)<half_length+10 || wlf_guesses(end)>length(data)-half_length-10
    error('fts:lft','You may be truncating the ends of your data.')
end
if ~exist('det_select','var'), det_select=[]; end

% Load up additional parameters and calibration values.
parameters=fts_analysis_default;
calibration=fts_calibration_default;

% Allocate space for the spectra and frequencies.
N_detectors=size(data,2);
N_scans=length(wlf_guesses);
spectral_response=NaN(N_detectors,N_scans,half_length+1);
frequencies=NaN(N_detectors,N_scans,half_length+1);

% if no explicit channels to loop over are given, use all:
if isempty(det_select) det_select = 1:N_detectors; end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Finding interferograms...')
tic

% Get slightly better estimates of the white light fringes.
wlf_better_guesses=NaN(N_detectors,N_scans);
wlf_guess_temporary=NaN;
for kk=1:length(det_select) % Loop over all detectors.
%for ii=1:N_detectors % Loop over all detectors.
    ii = det_select(kk);
    for jj=1:N_scans % Loop over all interferograms.
        if any(data(:,ii))>0 % Don't bother processing without data.
            % Run the find_interferogram program.
            wlf_guess_temporary=wlf_guesses(jj)-half_length...
                +find_interferogram(...
                data((wlf_guesses(jj)-half_length):(wlf_guesses(jj)+half_length),ii));
        end
        % Don't accept the output of find_interferogram unless it's
        % reasonably close to the initial guess.
        if abs(wlf_guess_temporary-wlf_guesses(jj))<50
            wlf_better_guesses(ii,jj)=wlf_guess_temporary;
        end
    end
end  
% Accept the median value of wlf_better_guesses.
wlf_better_guesses=ceil(nanmedian(wlf_better_guesses,1));
wlf_better_guesses(isnan(wlf_better_guesses))=wlf_guesses(isnan(wlf_better_guesses));
disp([num2str(N_scans),' interferograms found in ',num2str(toc),' seconds.'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Processing interferograms...')
tic
% Generate the spectra.
for kk=1:length(det_select) % Loop over all detectors.
    ii = det_select(kk);
    disp(ii)
    for jj=1:N_scans % Loop over all interferograms.
        if any(data(:,ii))>0 % Don't bother processing without data.
            % Process the interferograms.
            [s,f]=interferogram2spectrum(data((wlf_better_guesses(jj)-half_length):(wlf_better_guesses(jj)+half_length),ii),parameters,calibration);
            % Fill in the data arrays.
            spectral_response(ii,jj,1:length(s))=s;
            frequencies(ii,jj,1:length(f))=f;
        end
    end
end

disp([num2str(numel(find(~isnan(spectral_response(:,:,1))))),' of ',...
    num2str(N_detectors*N_scans),' spectra generated in ',num2str(toc),' seconds.'])
