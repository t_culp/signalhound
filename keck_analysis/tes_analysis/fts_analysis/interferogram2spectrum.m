function [spectral_response frequencies]=interferogram2spectrum(interferogram,parameters,calibration)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20110712 GPT
%
% Cleanly get spectral response vs. frequency from an interferogram.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  interferogram = column vector of a two-sided interferogram
%   The white light fringe should be near the center.
%
%  parameters = output of fts_analysis_default or similar
%
%  calibration = output of fts_calibration_default or similar
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  spectral_response = spectral distribution of the detector response
%
%  frequencies = set of frequencies where spectral_response is estimated
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Subtract a polynomial from the data.
% Repeat a few times because Matlab's precision sometimes sucks.
interferogram=poly_subtract(interferogram,parameters.poly_order,3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Gently denoise if requested.
if parameters.wden_switch{1}==1
    interferogram=wden(interferogram,...
        parameters.TPTR{1},parameters.SORH{1},parameters.SCAL{1},...
        parameters.N{1},parameters.wname{1});
end

% Find the index of the white light fringe.  Symmetrize the two-sided
% interferogram by averaging the two sides.  Since the DCT is a linear
% operation, the DCT of the average is the same as the average of the DCTs.
% This won't be exactly true if the second denoising step is requested.
wlf=find_wlf(interferogram,parameters.wlf_guess_tolerance);
interferogram=symmetrize_interferogram(interferogram,wlf);
% Check the WLF, since it might have moved after symmetrizing
wlf=find_wlf(interferogram,parameters.wlf_guess_tolerance);

% Denoise again if requested, maybe more aggressively this time.
if parameters.wden_switch{2}==1
    interferogram=wden(interferogram,...
        parameters.TPTR{2},parameters.SORH{2},parameters.SCAL{2},...
        parameters.N{2},parameters.wname{2});
    % Resymmetrize to be sure.  The wlf is unlikely to have moved much.
    wlf=find_wlf(interferogram,10);
    interferogram=symmetrize_interferogram(interferogram,wlf);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Apodize by a symmetric taper.  Since the interferogram has a highly
% defined center, multitapering is probably a bad idea.
taper=get_fts_taper(parameters.taper,numel(interferogram));

% Choose a sign convention that makes the wlf positive.
N=floor(wlf);
if interferogram(N)>0
    interferogram=taper.*interferogram;
else
    interferogram=-taper.*interferogram;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% We want to perform a discrete cosine transform on the right side of the
% interferogram.  Use the DCT-V if the wlf is a sampled data point.  Use
% the DCT-II if the wlf is between sampled data points.  I prefer the
% unitary variant so that adu in the time domain scales to adu in the
% frequency domain.

if mod(wlf,1)==0
    spectral_response=dct_v(interferogram,1);
else
    % Matlab's built-in function dct is a unitary DCT-II.
    spectral_response=dct(interferogram((N+1):(2*N)));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% If requested, also generate the frequencies vector.
if nargout==2
    if mod(wlf,1)==0
        frequencies=0.5*linspace(0,(N-1)/(N-0.5),N)';
    else
        frequencies=0.5*linspace(0,(N-1)/N,N)';
    end
    % Scale frequencies so that they are in GHz.
    %frequencies=linspace(0,1-0.5/N,N)'*f_s*299792458*(10^-9)/(4*v_mirror);
    frequencies=1e-9*frequencies...
        *calibration.f_s*299792458/(2*calibration.v_mirror);
end
