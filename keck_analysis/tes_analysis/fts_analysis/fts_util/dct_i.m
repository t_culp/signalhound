function output=dct_i(interferogram,unitarity_switch)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Take a two-sided interferogram with white light fringe in the center.
% Perform the unitary DCT-I on the right half of the data.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% For more information, read Stephen A. Martucci,
% "Symmetric Convolution and the Discrete Sine and Cosine Transforms"
% IEEE Transactions on Signal Processing, Vol. 42. No. 5, May 1994
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  interferogram = column vector representing a two-sided interferogram
%   Please place the wlf in the center.
%
%  unitarity_switch = set to 1 if you want the unitary variant
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  output = unitary DCT-I of the one-sided interferogram
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the default to be the unitary variant.
if nargin<2
    unitarity_switch=1;
end

% The input data is of the form ABCDEDCBA.  We want to take the DCT-I of
% EDCBA.  This is very similar to an FFT of EDCBABCD.  Let's arrange the
% data in that order.
interferogram=ifftshift(interferogram);

% Find the length of EDCBA.
N=(1+numel(interferogram))/2;

% Throw away the redundant "A" term.
interferogram(N)=[];

if unitarity_switch==1
    interferogram(1)=interferogram(1)*sqrt(2);
    interferogram(N)=interferogram(N)*sqrt(2);
end

% Take the FFT/2.  Matlab can have precision issues that result in a small
% imaginary part, so we take the real part.
full_spectrum=0.5*real(fft(interferogram));

% We don't need the redundant second half.
output=full_spectrum(1:N);

if unitarity_switch==1
    output(1)=output(1)/sqrt(2);
    output(N)=output(N)/sqrt(2);
    output=output*sqrt(2/(N-1));
end