function [x_out,y_out]=pre_interp1(x_in,y_in)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20130204 GPT
%
% The function interp1 often complains,
% "The grid vectors are not strictly monotonic increasing."
% This simple function sorts and rebins your x and y input and turns it
% into an output that Matlab can handle.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  x_in = column vector where data is sampled
%
%  y_in = column vector of data values
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  x_out = sorted, unique column vector where data is sampled
%
%  y_out = column vector of averaged data values
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if size(x_in,2)>1 || size(y_in,2)>1
    x_out='please use a column vector for now';
    y_out=[];
    return
end

% ignore NaN
nan_index=union(find(isnan(x_in)),find(isnan(y_in)));
x_in(nan_index)=[];
y_in(nan_index)=[];

% Sort into ascending x.  Note that we can't use sortrows because the y
% data may be complex, which will confuse sorting real x.
[x_in,ix]=sort(x_in);
y_in=y_in(ix);

% If there are multiple y values for a given x, take their mean.
x_out=unique(x_in);
N=length(x_out);
y_out=nan(N,1);
n=hist(x_in,x_out);
n_sum=cumsum(n);
y_out(1)=mean(y_in(1:n_sum(1)));
for jj=2:N
    y_out(jj)=mean(y_in((n_sum(jj-1)+1):n_sum(jj)));
end