function symmetric_interferogram=symmetrize_interferogram2(raw_interferogram,wlf)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Average the left and right halves of a two-sided interferogram.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  raw_interferogram = column vector representing a two-sided interferogram
%
%  wlf = index of the white light fringe
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  symmetric_interferogram = average of the data pairs
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N=numel(raw_interferogram);
%half_length=min(floor(wlf),floor(N-wlf+1));

% Find the indices just above and below the wlf.
below_wlf=floor(wlf-0.1);
above_wlf=ceil(wlf+0.1);

% Find the length of the one-sided interferogram.
half_length=min(below_wlf,N-above_wlf+1);

% Grab equal amounts of data to the left and right of the wlf.
left_data=raw_interferogram((below_wlf-half_length+1):below_wlf);
right_data=raw_interferogram(above_wlf:(above_wlf+half_length-1));

% Average them together.
left_data=mean([left_data,flipud(right_data)],2);

if mod(wlf,1)==0
    % The wlf is a sampled data point.
    symmetric_interferogram=...
        [left_data; raw_interferogram(wlf); flipud(left_data)];
else
    % The wlf is between sampled data points.
    symmetric_interferogram=...
        [left_data; flipud(left_data)];
end