function find_wlf_test

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Run this script to test the functionality of find_wlf().
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% We will generate half a period of a sine wave.  The program should simply
% find the index where the wave is maximal (x=pi/2, p=500).

x1=linspace(0,pi,1001)';
y1=sin(x1);
p1=find_wlf(y1);

% Now we generate the same function without sampling directly on the
% maximum.  The new index (p=500.5) should lie halfway between two points.

x2=linspace(0,pi,1000)';
y2=sin(x2);
p2=find_wlf(y2);

% The sampling now places the maximum in the borderline case.  The true
% maximum of x=pi/2 occurs at the unsampled index 500.75.  The program
% rounds down to p=500.5.

x3=x1+pi/4000;
y3=sin(x3);
p3=find_wlf(y3);

% We can move the central value off center.  This fails unless when the
% guess_tolerance value is too small.

x4=linspace(0,pi-0.2,1001)';
y4=sin(x4);
p4a=find_wlf(y4,25);
p4b=find_wlf(y4,50);

% Finally, we try this on noisy data.

x5=linspace(0,pi,1001)';
y5=sin(x5)+0.05*randn(1001,1);
p5=find_wlf(y5);

figure('units','normalized','outerposition',[.1 .1 .9 .9]);

subplot(3,1,1)
plot(x1,y1,x1(p1),y1(p1),'o',...
    x2,y2,mean([x2(floor(p2)),x2(ceil(p2))]),sin(mean([x2(floor(p2)),x2(ceil(p2))])),'*',...
    x3,y3,mean([x3(floor(p3)),x3(ceil(p3))]),sin(mean([x3(floor(p3)),x3(ceil(p3))])),'*')
axis([-0.1+pi/2,0.1+pi/2,0.995,1.005])

subplot(3,1,2)
plot(x4(p4a),y4(p4a),'o',x4(p4b),y4(p4b),'o',x4,y4)
legend('guess tolerance=25','guess tolerance=50')
axis([-0.1+pi/2,0.1+pi/2,0.995,1.005])

subplot(3,1,3)
if mod(p5,1)==0
    plot(x5(p5),y5(p5),'o',x5(500),y5(500),'*',x5,y5)
else
    plot(mean([x5(floor(p5)),x5(ceil(p5))]),sin(mean([x5(floor(p5)),x2(ceil(p5))])),'o',...
    x1(p1),y1(p1),'*',x5,y5)
end
legend('observed','ideal')
axis([-1+pi/2,1+pi/2,0,1]), axis 'auto y'