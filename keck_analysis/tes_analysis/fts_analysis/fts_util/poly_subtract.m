function y=poly_subtract(x,poly_order,n_repetitions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Subtract a polynomial from your data.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  x = column vector of time-ordered data
%
%  poly_order = order of the polynomial subtracted from the data
%   default = 0
%
%  n_repetitions = number of times to repeat
%   Matlab's precision sometimes sucks, so you may want to repeat.
%   Consider rescaling the data if you still have precision issues.
%   default = 1
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  y = column vector with the polynomial fit removed
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

y=x;
clear x

% Set the default values.
if nargin<3, n_repetitions=1; end
if nargin<2, poly_order=0; end

if poly_order==0
    for jj=1:n_repetitions
        y=detrend(y,'constant'); % faster for poly_order=0
    end
elseif poly_order==1
    for jj=1:n_repetitions
        y=detrend(y,'linear'); % about as fast for poly_order=1
    end
else
    domain=(1:numel(y))';
    for jj=1:n_repetitions
        [p S mu]=polyfit(domain,y,poly_order); % fit a polynomial
        y=y-polyval(p,domain,S,mu); % subtract the fitted polynomial
    end
end
