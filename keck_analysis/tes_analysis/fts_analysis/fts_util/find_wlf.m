function wlf=find_wlf(interferogram,guess_tolerance)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Find the index of the white light fringe.
% Note that the wlf may be halfway between data points.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  interferogram = column vector representing a two-sided interferogram
%   Please use an odd number of data points with the wlf near the center.
%
%  guess_tolerance = estimate of how far the wlf could be from center
%   Large values of guess_tolerance will slow down the computation and may
%   become incompatible with the interferogram length.
%   default = 25
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  wlf = index of the white light fringe, possibly half-integer
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the default guess_tolerance.
if nargin==1
    guess_tolerance=25;
end

% Truncate the last data point if the input is even.
if mod(numel(interferogram),2)==0
    interferogram(end)=[];
end

N=numel(interferogram);

% Consider guess_tolerance indices to the left and right of the center.
wlf_guesses=(-guess_tolerance+(N+1)/2):0.5:(guess_tolerance+(N+1)/2);
half_length=wlf_guesses(1)-1;
correlations=zeros(4*guess_tolerance+1,1);

for jj=1:(4*guess_tolerance+1)
    % Find the indices just above and below the wlf guess.
    below_wlf=floor(wlf_guesses(jj)-0.1);
    above_wlf=ceil(wlf_guesses(jj)+0.1);
    % Separate the data into the two halves, excluding the wlf and anything
    % on the tail end longer than half_length.
    left_data=interferogram((below_wlf-half_length+1):below_wlf);
    right_data=interferogram(above_wlf:(above_wlf+half_length-1));
    % See how well the two halves correlate.
    correlations(jj)=xcorr(left_data,flipud(right_data),0,'coeff');
end

% Find the index where the correlation is maximal.
[dummy,peak_correlation]=max(correlations);
wlf=wlf_guesses(1)+0.5*(peak_correlation-1);