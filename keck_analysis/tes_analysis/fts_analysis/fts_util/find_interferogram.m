function wlf_guess=find_interferogram(data)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20110713 GPT
%
% Get a quick estimate of where the white light fringe of an interferogram
% is.  This is only a "by eye" estimate to help you select what data to
% use.  For a final estimate, you should use find_wlf.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  data = column vector containing a two-sided interferogram
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  wlf_guess = guess of where the wlf is
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Detrend and smooth out the data.
data=smooth(poly_subtract(data,1,1),5);

% Find the local extrema.
[xmax imax xmin imin]=extrema(smooth(data));

% Create a matrix representing the envelope of the interferogram.
envelope=sortrows([[imax xmax];[imin xmin]]);

% Find where the second derivative of the envelope is maximal.
[dummy,most_extreme]=max(abs(diff(envelope(:,2),2)));

% Return the corresponding index in the data.
wlf_guess=envelope(most_extreme+1,1);