function taper=get_fts_taper(taper_family,N)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Get either a user-defined taper or a Matlab-defined taper.
% This function does not support dpss.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  taper_family = name of the taper family
%
%  N = length of the taper
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  taper = symmetric taper of length N
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The first ones are custom functions in the fts_tapers directory.
switch taper_family
    case 'blackman_exact'
        taper=blackman_exact(N);
    case 'blackmanharris3'
        taper=blackmanharris3(N);
    case 'blackmanharris4'
        taper=blackmanharris4(N);
    case 'blackmanharris3_partial'
        taper=blackmanharris3_partial(N);
    case 'blackmanharris4_partial'
        taper=blackmanharris4_partial(N);
    case 'nortonbeer_weak'
        taper=nortonbeer_weak(N);
    case 'nortonbeer_medium'
        taper=nortonbeer_medium(N);
    case 'nortonbeer_strong'
        taper=nortonbeer_strong(N);

% These other tapers are defined by Matlab.
    case {'blackmanharris','nuttallwin'}
    % In this version of Matlab, these tapers are not symmetric by default.
    % This seems to have been corrected in newer versions.
    taper=window(str2func(taper_family),N-1);
    taper=[taper; taper(1)];
    case {'barthannwin','bartlett','blackman','bohmanwin','chebwin',...
        'flattopwin','gausswin','hamming','hann','kaiser','parzenwin',...
        'rectwin','taylorwin','triang','tukeywin'}
    taper=window(str2func(taper_family),N);
    otherwise
    error(fts:tpr,'Sorry, I am unfamiliar with that taper.')
end