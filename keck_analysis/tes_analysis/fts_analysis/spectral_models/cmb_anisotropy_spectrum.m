function [I_delta_cmb]=cmb_anisotropy_spectrum(frequencies,delta_T)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Generate the residual spectral radiance of the CMB anisotropy.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  frequencies = GHz where the spectrum is evaluated
%
%  delta_T = deviation from 2.725 K
%    If left blank, the output will be (1 K)*(dI/dT)|_{2.725 K}.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  I_cmb = spectral radiance in Jy/sr
%    If only the first input argument is present, then the output is
%    actually (1 K)*(dI/dT)|_{2.725 K}.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define some physical constants for convenience.
c=299792458; % m/s
h=6.62606957e-34; % J*s
k_B=1.3806488e-23; % J/K
T_cmb=2.725; % K

% Convert GHz to Hz.
frequencies=1e9*frequencies;

% Define an auxiliary parameter for convenience.
z=h*frequencies/(k_B*T_cmb);

% Give the exact answer if an explicit delta_T is called.
if nargin==2
    % Define another auxiliary parameter.
    y=h*frequencies/(k_B*(T_cmb+delta_T));
    % Take I(2.725 K + delta_T) - I(2.725 K).
    I_delta_cmb=1e26*((2*h/c^2)*(frequencies.^3)./(exp(y)-1)...
        -(2*h/c^2)*(frequencies.^3)./(exp(z)-1));
else
    % Since delta_T is generally small ( ~1 mK ) compared to T (2725 mK),
    % we may approximate I(delta_T) as delta_T*(dI/dT)|_{2.725 K}.  In a
    % given patch of sky, delta_T will be a fixed multiplier, which can be
    % ignored for many applications.
    % Take (1 K)*(dI/dT)|_{2.725 K}.
    I_delta_cmb=1e26*(2*k_B/c^2)*(frequencies.^2).*(z.^2).*exp(z).*((exp(z)-1).^-2);
end