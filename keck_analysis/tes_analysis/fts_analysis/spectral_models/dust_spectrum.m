function [I_dust]=dust_spectrum(frequencies,T,beta)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Generate the spectral radiance of Galactic dust.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  frequencies = GHz where the spectrum is evaluated
%
%  T = greybody temperature, default=19.6
%
%  beta = emissivity spectral index, default=1.59
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  I_dust = spectral radiance in arbitrary units
%    The source brightness varies widely across the sky, so here we simply
%    normalize it to have a value of 1 at 150 GHz.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define some physical constants for convenience.
h=6.62606957e-34; % J*s
k_B=1.3806488e-23; % J/K

if nargin<3 || isempty(beta) || isempty(T)
  disp('Using central values for the dust spectrum from PIP XXX.')
  T=19.6;
  beta=1.59;
end

% Convert GHz to Hz.
frequencies=1e9*frequencies;

% Define an auxiliary parameter for convenience.
z=h*frequencies/(k_B*T);
z150=h*150e9/(k_B*T);

I_dust=(frequencies.^(3+beta))./(exp(z)-1);
I_dust=I_dust*(exp(z150)-1)/(150e9^(3+beta));