function [I_sync]=sync_spectrum(frequencies,spectral_index)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Generate the spectral radiance of synchrotron (or a simple power law).
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  frequencies = GHz where the spectrum is evaluated
%
%  spectral_index = synchrotron spectral index, default=-3.3
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  I_sync = spectral radiance in arbitrary units
%    The source brightness varies widely across the sky, so here we simply
%    normalize it to have a value of 1 at 150 GHz.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin<2 || isempty(spectral_index)
  disp('Using mean of WMAP synchrotron in BICEP field.')
  spectral_index=-3.3;
end

% spectral index is relative to R-J
I_sync=(frequencies/150).^(2+spectral_index);