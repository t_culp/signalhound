function [I_cmb]=cmb_primary_spectrum(frequencies)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Generate the spectral radiance of the primary CMB temperature.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  frequencies = GHz where the spectrum is evaluated
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  I_cmb = spectral radiance in Jy/sr
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define some physical constants for convenience.
c=299792458; % m/s
h=6.62606957e-34; % J*s
k_B=1.3806488e-23; % J/K
T_cmb=2.725; % K

% Convert GHz to Hz.
frequencies=1e9*frequencies;

% Define an auxiliary parameter for convenience.
z=h*frequencies/(k_B*T_cmb);

I_cmb=1e26*(2*h/c^2)*(frequencies.^3)./(exp(z)-1);