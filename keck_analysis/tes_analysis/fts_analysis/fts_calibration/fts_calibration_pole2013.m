function calibration=fts_calibration_pole2013()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20130115 GPT
%
% Define FTS calibration values used at South Pole for r3d2 in 2013 January
% These parameters remained the default in 2013 December for all rx.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sample frequency
calibration.f_s=180.4; % Hz

% mirror speed
calibration.v_mirror=0.001905; % m/s
