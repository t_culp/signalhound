function calibration=fts_calibration_default()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20110711 GPT
%
% Set the default FTS calibration to be what was used for run H6.
% 20140110 - new default is pole2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

calibration=fts_calibration_pole2013;
