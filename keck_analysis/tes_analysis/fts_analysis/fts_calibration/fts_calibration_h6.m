function calibration=fts_calibration_h6()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20110711 GPT
%
% Define FTS calibration values used in run H6.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sample frequency
calibration.f_s=103.6; % Hz

% mirror speed
calibration.v_mirror=0.002; % m/s