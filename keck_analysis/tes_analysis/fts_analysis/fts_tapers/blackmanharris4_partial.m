function taper=blackmanharris4_partial(N)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calculate a 4-term Blackman-Harris taper.  This one only achieves -74 dB
% in its highest side lobe, so it is somewhere between blackmanharris4 and
% a Hamming taper.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  N = taper length
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  taper = symmetric taper of length N
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if N==1
    taper=1;
else
    taper = 0.40217*ones(N,1)...
           -0.49703*cos(2*pi*(0:N-1)'/(N-1))...
           +0.09392*cos(4*pi*(0:N-1)'/(N-1))...
           -0.00183*cos(6*pi*(0:N-1)'/(N-1));
end