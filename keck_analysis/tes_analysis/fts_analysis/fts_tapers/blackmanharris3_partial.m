function taper=blackmanharris3_partial(N)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calculate a 3-term Blackman-Harris taper.  This one only achieves -61 dB
% in its highest side lobe, so it is somewhere between blackmanharris3 and
% a Hamming taper.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  N = taper length
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  taper = symmetric taper of length N
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if N==1
    taper=1;
else
    taper = 0.44959*ones(N,1)...
           -0.49364*cos(2*pi*(0:N-1)'/(N-1))...
           +0.05677*cos(4*pi*(0:N-1)'/(N-1));
end