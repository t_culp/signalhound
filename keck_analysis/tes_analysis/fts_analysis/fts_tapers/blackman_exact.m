function taper=blackman_exact(N)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calculate the "exact" 3-term Blackman taper.
% Matlab's function blackman() gives an inexact approximation. Both the
% exact and inexact forms are commonplace.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  N = taper length
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  taper = exact, symmetric Blackman taper of length N
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if N==1
    taper=1;
else
    taper = (7938/18608)*ones(N,1)...
           -(9240/18608)*cos(2*pi*(0:N-1)'/(N-1))...
           +(1430/18608)*cos(4*pi*(0:N-1)'/(N-1));
end
