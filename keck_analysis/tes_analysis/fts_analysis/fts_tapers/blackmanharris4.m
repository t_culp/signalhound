function taper=blackmanharris4(N)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calculate the minimum 4-term Blackman-Harris taper.
%
% Matlab's function blackmanharris() gives the periodic taper.
% Newer versions correct this deficiency and allow either option.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  N = taper length
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  taper = symmetric taper of length N
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if N==1
    taper=1;
else
    taper = 0.35875*ones(N,1)...
           -0.48829*cos(2*pi*(0:N-1)'/(N-1))...
           +0.14128*cos(4*pi*(0:N-1)'/(N-1))...
           -0.01168*cos(6*pi*(0:N-1)'/(N-1));
end