function [xi dxi_df f]=spectral_gain_mismatch(spectra,frequencies,atmospheric_model,source,band_center)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Estimate the spectral gain mismatch of all pixels.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  spectra = N_frequencies*N_detectors fts_coadd_spectra output
%
%  frequencies = GHz where the spectra are evaluated
%
%  atmospheric_model = name of a file containing am output data
%   The file should be [frequencies, transmittance, spectral radiance].
%
%  source = 'cmb' or 'galaxy', default='cmb'
%
%  band_center = central frequency of the FPU in GHz, default=150
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  xi = spectral gain mismatch
%
%  dxi_df = derivative of xi with respect to frequency
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin<5
    band_center=150;
end

if nargin<4
    source='cmb';
end

if strcmp(source,'gal')
    source='galaxy';
end

% Consider only values over believable frequencies.
lower_index=find(frequencies<band_center-50,1,'last');
upper_index=find(frequencies>=band_center+50,1,'first');
f=frequencies(lower_index:upper_index);
spectra=spectra(lower_index:upper_index,:);

% Sort the data into A and B polarizations.
[A B]=gcp2det_array(spectra);

% Import the atmospheric model.
am=importdata(atmospheric_model);

% Interpolate the atmospheric model to the same frequencies as the spectra.
% Since am can be much more finely evaluated than the FTS data can, the
% 'nearest' interpolation method will not incur significant error.
transmittance=interp1(am(:,1),am(:,2),f,'nearest');
I_am=1e26*interp1(am(:,1),am(:,3),f,'nearest'); % Jy/sr

% Load the source spectrum.
if strcmp(source,'cmb')
    % I'm using the same "I" notation as above, but the quantity being
    % computed is really (1 K)*(dI/dT)|_(2.725 K).
    I_source=transmittance.*cmb_anisotropy_spectrum(f); % Jy/sr
elseif strcmp(source,'galaxy')
    I_source=transmittance.*galaxy_spectrum(f); % nothing yet
else
    error('sgm:src','Sorry, I am unfamiliar with that microwave source.');
end

% Calculate the Gamma factors (http://arxiv.org/pdf/1103.0289v3).
Gamma_A_am=NaN(16,16);
Gamma_B_am=NaN(16,16);
Gamma_A_source=NaN(16,16);
Gamma_B_source=NaN(16,16);
dxi_df=NaN(16,16,length(f));
delta_f=f(2)-f(1);
for ii=1:16
    for jj=1:16
        a=squeeze(A(ii,jj,:));
        b=squeeze(B(ii,jj,:));
        Gamma_A_am(ii,jj)=delta_f*sum(a.*I_am.*f.^-2);
        Gamma_B_am(ii,jj)=delta_f*sum(b.*I_am.*f.^-2);
        Gamma_A_source(ii,jj)=delta_f*sum(a.*I_source.*f.^-2);
        Gamma_B_source(ii,jj)=delta_f*sum(b.*I_source.*f.^-2);
        % Also calculate something useful later for dxi_df.
        dxi_df(ii,jj,:)=(a/Gamma_A_source(ii,jj))-(b/Gamma_B_source(ii,jj));
    end
end

% Calculate the G factors.
G_A=Gamma_A_am./Gamma_A_source;
G_B=Gamma_B_am./Gamma_B_source;

% Calculate the spectral gain mismatches.
xi=(G_A-G_B)./(G_A+G_B);

% Calculate the derivatives in GHz^-1.
for ii=1:16
    for jj=1:16
        dxi_df(ii,jj,:)=squeeze(dxi_df(ii,jj,:))...
            .*I_am.*(f.^-2)/(G_A(ii,jj)+G_B(ii,jj));
    end
end
