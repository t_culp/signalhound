function [bc, bw]=band_center_width(spectra,frequencies,divide_f2,band_center_estimate,source)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% GPT - 2014 April 7
%
% Calculate the band centers and band widths.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  spectra = N_frequencies*N_detectors fts_coadd_spectra output
%
%  frequencies = GHz where the spectra are evaluated
%
%  divide_f2 = flag for whether to divide by frequency^2, default=true
%
%  band_center_estimate = central frequency of the FPU in GHz, default=150
%    currently supports only 100 GHz or 150 GHz
%    used only to determine where to cut off the limits of integration
%
%  source = use effective centers for different source, default='rj'
%    'none' with divide_f2=false is the same as 'rj' with divide_f2=true
%    'cmb' uses the proper blackbody derivative
%    'sync' uses f^-3.3 power law
%    'dust' uses f^1.59 times a T=19.6 K black body
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  bc = band center in GHz
%
%  bw = band width in GHz
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin<5
  source='none';
end

if nargin<4
  band_center_estimate=150;
end

if nargin<3
  divide_f2=true;
end

% Consider only values over believable frequencies.
if band_center_estimate==150;
  lower_band=100;
  upper_band=200;
elseif band_center_estimate==100;
  lower_band=65;
  upper_band=125;
end
lower_index=find(frequencies<lower_band,1,'last');
upper_index=find(frequencies>=upper_band,1,'first');
f=frequencies(lower_index:upper_index);
s=spectra(lower_index:upper_index,:);

% divide out frequency^2 if requested
% This is appropriate for a beam-filling source.
if divide_f2
  s=bsxfun(@times,s,f.^-2);
end

% multiply by source
switch source
  case 'rj'
    I=f.^2;
  case 'none'
    I=ones(size(f));
  case 'cmb'
    I=cmb_anisotropy_spectrum(f);
  case 'sync'
    I=sync_spectrum(f);
  case 'dust'
    I=dust_spectrum(f);
  otherwise
    err('fts:mod','Sorry, I don''t know that source spectrum.')
end
s=bsxfun(@times,s,I);

% integral normalize
s=bsxfun(@rdivide,s,trapz(f,s,1));

% band centers as the mean
bc=trapz(f,bsxfun(@times,s,f),1);

% band width as we defined it in BK-II
bw=(trapz(f,s,1).^2)./trapz(f,s.^2,1);