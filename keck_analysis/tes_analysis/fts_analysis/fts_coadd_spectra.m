function [spectra_out frequencies_out]=fts_coadd_spectra(spectra_in,frequencies_in,band_center)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20110713 GPT
%
% Cut and coadd spectra generated from different interferograms.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%
%  spectra_in = N_detectors*N_scans*N_frequencies fts_make_spectra output
%
%  frequencies_in = GHz where the spectra are evaluated
%
%  band_center = central frequency of the FPU in GHz, default=150
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Outputs:
%
%  spectra_out = N_frequencies*N_detectors array of spectra
%
%  frequencies_out = common vector of frequencies
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin<3
    band_center=150; % GHz
end

N_detectors=size(frequencies_in,1);
N_scans=size(frequencies_in,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Count the number of trailing NaN values occur in each spectrum.
N_nan=sum(isnan(frequencies_in),3);

% Find the smallest possible number of trailing NaNs.
N_nan_min=min(min(N_nan));
N_frequencies=size(frequencies_in,3)-N_nan_min;

% Find a spectrum that achieves the minimum trailing NaNs.
[detector scan]=ind2sub([N_detectors N_scans],find(N_nan==N_nan_min,1,'first'));

% Generate a common frequency vector.
frequencies_out=squeeze(frequencies_in(detector,scan,1:N_frequencies));

% Truncate superfluous trailing NaNs from the frequencies and spectra.
frequencies_in=frequencies_in(:,:,1:N_frequencies);
spectra_in=spectra_in(:,:,1:N_frequencies);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Interpolating spectra...')
tic

% Interpolate the spectra to be evaluated at frequencies_out.
spectra_out=NaN(N_detectors,N_scans,N_frequencies);
for ii=1:N_detectors % Loop over all detectors.
    for jj=1:N_scans % Loop over all interferograms.
        if any(spectra_in(ii,jj,:))>0 % Don't bother interpolating without data.
            dummy1=squeeze(frequencies_in(ii,jj,:));
            dummy2=squeeze(spectra_in(ii,jj,:));
            spectra_out(ii,jj,:)=interp1(...
                dummy1(~isnan(dummy1)),...
                dummy2(~isnan(dummy2)),...
                frequencies_out,'nearest');
            % The 'nearest' interpolation may incur a tiny error, but it
            % should greatly speed up the computation.
        end
    end
end

disp(['Interpolation completed in ',num2str(toc),' seconds.'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Our next goal is to apply some cuts.  We will consider only those spectra
% that show response in the passband but little response at an unphysically
% high frequency.

% disp('Applying cuts...')
% tic
% 
% % Find the indices where the band edges should roughly be.
% band_lower=find(frequencies_out>band_center-20,1,'first');
% band_upper=find(frequencies_out<band_center+20,1,'last');
% 
% % Find the index of 2 THz.
% out_of_band=find(frequencies_out>2000,1,'first');
% 
% % Apply the cut.
% for ii=1:N_detectors % Loop over all detectors.
%     for jj=1:N_scans % Loop over all interferograms.
%         if ~any(spectra_out(ii,jj,band_lower:band_upper)>5) || ...
%                 any(spectra_out(ii,jj,out_of_band:end)>0.1)
%             spectra_out(ii,jj,:)=NaN(N_frequencies,1);
%         end
%     end
% end
% 
% disp(['Cuts completed in ',num2str(toc),' seconds.'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Coadd and return.
if N_detectors==1
    spectra_out=squeeze(nansum(spectra_out,2));
else
    spectra_out=squeeze(nansum(spectra_out,2))';
end