#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mex.h"
#ifndef HAVE_OCTAVE
#  include "matrix.h"
#endif

#define ACCUM_MAX(A,B) A = ((A) > (B) ? (A) : (B))
#define ACCUM_MIN(A,B) A = ((A) < (B) ? (A) : (B))
#define ACCUM_UNI(A,B) A = ((unsigned int)(A)) | ((unsigned int)(B))

void mexFunction (int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[])
{
    double * tab_out;
    double * ibin;
    double * xdat;
    mxArray * mx_tab_out;
    int nchan, nsamp, nbins;
    double xx;
    int nn;
    int num_nans = 0;

    int i, j;

    if (nrhs < 3)
        mexErrMsgTxt ("Usage: tab = stats_by_bins (x, i, n)");

    ibin = mxGetData (prhs[1]);
    xdat = mxGetData (prhs[0]);
    nbins = mxGetScalar (prhs[2]);
    nchan = mxGetM (prhs[0]);
    nsamp = mxGetN (prhs[0]);

    /* printf ("%d channels, %d samples, %d bins.\n", nchan, nsamp, nbins); */
    mx_tab_out = mxCreateDoubleMatrix (nchan*4, nbins, mxREAL);
    tab_out = mxGetData (mx_tab_out);
    for (i=0; i<(nchan*4)*nbins; i++)
      tab_out[i] = 0;

    for (i=0; i<nchan; i++)
    {
      for (j=0; j<nsamp; j++)
      {
        xx = xdat[i+j*nchan];
	nn = ibin[j];
        if ((nn < 0) || (nn > nbins))
          printf ("Bin %d out of bounds! (max is %d.)\n", nn, nbins);
        if ((xx == xx) && (nn != 0)) 
        {
          if((tab_out[(4*i+0) + (nn-1) * (4*nchan)]++)==0)
          {
            tab_out[(4*i+1) + (nn-1) * (4*nchan)] = xx;
            tab_out[(4*i+2) + (nn-1) * (4*nchan)] = xx;
            tab_out[(4*i+3) + (nn-1) * (4*nchan)] = xx;
          }
          else
          {
            ACCUM_MAX(tab_out[(4*i+1) + (nn-1) * (4*nchan)],xx);
            ACCUM_MIN(tab_out[(4*i+2) + (nn-1) * (4*nchan)],xx);
            ACCUM_UNI(tab_out[(4*i+3) + (nn-1) * (4*nchan)],xx);
          }
        }
	if (xx != xx)
          num_nans++;
      }
    }

    /* printf ("Found %d NaNs.\n", num_nans); */
    plhs[0] = mx_tab_out;

    return;
}

