/*
RWO 090212 - use mex to speed up parsing format file
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mex.h"
#ifndef HAVE_OCTAVE
#  include "matrix.h"
#endif

#define FILESEP_STR "/"
#define FILESEP_CHR '/'
#define MAXLINELENGTH 256

/* Duplicate a dirfile structure, leaving out parts that */
/* will get completely rewritten on reading format file. */
int dup_D (mxArray ** D, mxArray * D_old)
{
    int ii;
    const char * field_names[] = {"frameoffset", "version", "endian", "format",
                                  "root", "fields", "names"};
    const int num_names = sizeof(field_names) / sizeof(char *);

    *D = mxCreateStructMatrix (1, 1, num_names, field_names);
    if (D_old == NULL)
    {
        for (ii=0; ii<2; ii++)
            mxSetField (*D, 0, field_names[ii], mxCreateDoubleMatrix (0, 0, mxREAL));
        mxSetField (*D, 0, "endian", mxCreateString ("n"));
        mxSetField (*D, 0, "root", mxCreateString(""));
        mxSetField (*D, 0, "format", mxCreateString(""));
        mxSetField (*D, 0, "fields", mxCreateCellMatrix (0, 0));
        mxSetField (*D, 0, "names", mxCreateString(""));
    }
    else
    {
        mxArray * tmp;
        for (ii=0; ii<5; ii++)
        {
            tmp = mxGetField (D_old, 0, field_names[ii]);
            if (tmp == NULL)
                tmp = mxCreateDoubleMatrix (0, 0, mxREAL);
            else tmp = mxDuplicateArray (tmp);
            mxSetField (*D, 0, field_names[ii], tmp);
        }
        mxSetField (*D, 0, "fields", mxCreateCellMatrix (0, 0));
        mxSetField (*D, 0, "names", mxCreateString(""));
    }
    return 0;
}

char * get_fmtfile_D (mxArray * D)
{
#if 1==0 
    mxArray * tmp;
    char * dir;
    char * shortfname;
    char * fmtfile;

    if (!mxIsStruct (D))
    {
        printf ("ERROR in get_fmtfile_D : not a struct!\n");
        return NULL;
    }

    tmp = mxGetField (D, 0, "root");
    dir = mxArrayToString (tmp);
    tmp = mxGetField (D, 0, "format");
    shortfname = mxArrayToString (tmp);

    fmtfile = mxMalloc (strlen(dir) + strlen(shortfname) + 2);
    strcpy (fmtfile, dir);
    strcat (fmtfile, FILESEP_STR);
    strcat (fmtfile, shortfname);

    mxFree (dir);
    mxFree (shortfname);
#else
    mxArray * tmp;
    char * fmtfile;

    tmp = mxGetField (D, 0, "format");
    fmtfile = mxArrayToString (tmp);
#endif
    printf ("format file = %s.\n", fmtfile);

    return fmtfile;
}

int set_fmtfile_D (mxArray * D, char * fmtfile)
{
    mxArray * tmp;
    char * tmpstr;

    tmp = mxGetField (D, 0, "root");
    mxDestroyArray (tmp);
    tmp = mxGetField (D, 0, "format");
    mxDestroyArray (tmp);

    tmpstr = strrchr (fmtfile, FILESEP_CHR);
    if (tmpstr == NULL)
    {
        mxSetField (D, 0, "root", mxCreateString("."));
        mxSetField (D, 0, "format", mxCreateString(fmtfile));
    }
    else
    {
        tmpstr++;
        mxSetField (D, 0, "format", mxCreateString(tmpstr));
        tmpstr--;
        /* tmpstr[0] = '\0'; */
        mxSetField (D, 0, "root", mxCreateString(fmtfile));
        /* tmpstr[0] = '/'; */
    }
    return 0;
}

int add_to_1d_cell_array (mxArray ** CA, mxArray * V, int * n, int * n_max)
{
    if (*n < *n_max)
    {
        (*n)++;
        mxSetCell (*CA, (*n)-1, V);
        return 0;
    }
    else
    {
        int ii;
        mxArray * new_CA;
        mxArray * tmp;
        if (*n_max < 50) *n_max = 100;
        else *n_max *= 2;
        new_CA = mxCreateCellMatrix (*n_max, 1);
        for (ii=0; ii<*n; ii++)
        {
            tmp = mxGetCell (*CA, ii);
            mxSetCell (new_CA, ii, tmp);
            mxSetCell (*CA, ii, NULL);
        }
        mxDestroyArray (*CA);
        *CA = new_CA;
       (*n)++;
       mxSetCell (*CA, (*n)-1, V);
    }
    return 0;
}

int shrink_1d_cell_array (mxArray ** CA, int * n, int * n_max)
{
    if (*n == *n_max)
        return 0;
    else
    {
        /* printf ("Shrinking from %d to %d.\n", *n_max, *n); */
        mxArray * new_CA;
        mxArray * tmp;
        int ii;
        *n_max = *n;
        new_CA = mxCreateCellMatrix (*n_max, 1);
        for (ii=0; ii<*n; ii++)
        {
            tmp = mxGetCell (*CA, ii);
            if (tmp == NULL)
                printf ("Got a null at ii=%d!\n", ii);
            mxSetCell (new_CA, ii, tmp);
            mxSetCell (*CA, ii, NULL);
        }
        mxDestroyArray (*CA);
        *CA = new_CA;
    }
    return 0;
}

int try_parse_directive (mxArray * D, char * l)
{
    char * s = l;
    int r = 0;
    const char * directive_list[] = {"ENDIAN", "VERSION", "INCLUDE", "FRAMEOFFSET"};
    int ii;

    if (s[0] == '/') s++;
    for (ii=0; ii<sizeof(directive_list)/sizeof(char *); ii++)
    {
        if (!strncmp (directive_list[ii], s, strlen(directive_list[ii])))
        {
            s += strlen(directive_list[ii]);
            while (isspace (s[0])) s++;
            r = 1;
            break;
        }
    }
    if (r == 0) return 0;

    switch (ii)
    {
        case 0 : if (!strncasecmp ("big", s, 3))
                 {
                     mxDestroyArray (mxGetField (D, 0, "endian"));
                     mxSetField (D, 0, "endian", mxCreateString ("b"));
                     return 1;
                 }
                 if (!strncasecmp ("little", s, 6))
                 {
                     mxDestroyArray (mxGetField (D, 0, "endian"));
                     mxSetField (D, 0, "endian", mxCreateString ("l"));
                     return 1;
                 }
                 break;
        case 1 : if (1 == sscanf (s, "%d", &r))
                 {
                     mxDestroyArray (mxGetField (D, 0, "version"));
                     mxSetField (D, 0, "version", mxCreateDoubleScalar (r));
                     return 1;
                 }
                 break;
        case 2 : printf ("WARNING: INCLUDE directive is not supported.\n");
                 break;
        case 3 : if (1 == sscanf (s, "%d", &r))
                 {
                     mxDestroyArray (mxGetField (D, 0, "frameoffset"));
                     mxSetField (D, 0, "frameoffset", mxCreateDoubleScalar (r));
                     return 1;
                 }
                 break;
    }
    printf ("ERROR in dirfile header directive line:\n");
    printf ("    %s\n", l);
    return -1;
}

int try_parse_raw (mxArray ** f, mxArray * name, char * l)
{
    const char * field_list[] = {"name", "type", "datatype", "spf", "root", "f", "nsamp", "fname"};
    const char * dataspec_list[] = {"c", "UINT8", "INT8", "u", "UINT16",
                                    "s", "INT16", "U", "UINT32",
                                    "S", "i", "INT32", "UINT64", "INT64",
                                    "f", "FLOAT32", "FLOAT",
                                    "d", "FLOAT64", "DOUBLE"};
    const char * datatype_list[] = {"uint8", "uint8", "int8", "uint16", "uint16",
                                    "int16", "int16", "uint32", "uint32",
                                    "int32", "int32", "int32", "uint64", "int64",
                                    "single", "single", "single",
                                    "double", "double", "double"};
    int ii, spf, cmplen;
    int r=0;

    *f = mxCreateStructMatrix (1, 1, sizeof(field_list)/sizeof(char *), field_list);
    mxSetField (*f, 0, "name", name);
    mxSetField (*f, 0, "type", mxCreateString ("RAW"));

    cmplen = 0;
    while (!isspace (l[cmplen]) && l[cmplen] != '\0') cmplen++;
    if (l[cmplen] == '\0') return -1;

    for (ii=0; ii<sizeof(dataspec_list)/sizeof(char *); ii++)
    {
        if (!strncmp (dataspec_list[ii], l, cmplen))
        {
            /*if (!isspace (l[strlen(dataspec_list[ii])]))*/
            if (cmplen != strlen(dataspec_list[ii]))
            {
                printf ("%s : (%c)\n", l, l[strlen(dataspec_list[ii])]);
                continue;
            }
            r = 1;
            break;
        }
    }
    if (r == 0)
    {
        printf ("Unrecognized RAW data type in line:\n");
        printf ("    %s\n", l);
        return -1;
    }

    mxSetField (*f, 0, "datatype", mxCreateString (datatype_list[ii]));
    sscanf (l, "%*s %d", &spf);
    mxSetField (*f, 0, "spf", mxCreateDoubleScalar (spf));
    mxSetField (*f, 0, "root", mxCreateString (""));

    return 1;
}

int try_parse_lincom (mxArray ** f, mxArray * name, char * l)
{
    const char * field_list[] = {"name", "type", "dep", "a", "b", "n", "isgood"};
    char tmp[MAXLINELENGTH];
    int n, nchar, ii;
    char * s;
    mxArray * dep_mxa;
    mxArray * a_mxa;
    mxArray * b_mxa;
    double * a_pr;
    double * b_pr;

    *f = mxCreateStructMatrix (1, 1, sizeof(field_list)/sizeof(char *), field_list);
    mxSetField (*f, 0, "name", name);
    mxSetField (*f, 0, "type", mxCreateString ("LINCOM"));

    sscanf (l, "%d%n", &n, &nchar);
    mxSetField (*f, 0, "n", mxCreateDoubleScalar (n));
    s = l + nchar;

    dep_mxa = mxCreateCellMatrix (n, 1);
    a_mxa = mxCreateDoubleMatrix (n, 1, mxREAL);
    a_pr = mxGetPr (a_mxa);
    b_mxa = mxCreateDoubleMatrix (n, 1, mxREAL);
    b_pr = mxGetPr (b_mxa);
    for (ii=0; ii<n; ii++)
    {
        sscanf (s, "%s %lf %lf%n", tmp, a_pr + ii, b_pr + ii, &nchar);
        s += nchar;
        mxSetCell (dep_mxa, ii, mxCreateString (tmp));
    }
    mxSetField (*f, 0, "dep", dep_mxa);
    mxSetField (*f, 0, "a", a_mxa);
    mxSetField (*f, 0, "b", b_mxa);
    return 1;
}

int try_parse_linterp (mxArray ** f, mxArray * name, char * l)
{
    printf ("LINTERP fields not yet supported.  Bug Walt about it.\n");
    return 0;
}

int try_parse_multiply (mxArray ** f, mxArray * name, char * l)
{
    const char * field_list[] = {"name", "type", "dep", "isgood"};
    char tmp[MAXLINELENGTH];
    int nchar, ii;
    char * s = l;
    mxArray * dep_mxa;

    *f = mxCreateStructMatrix (1, 1, sizeof(field_list)/sizeof(char *), field_list);
    mxSetField (*f, 0, "name", name);
    mxSetField (*f, 0, "type", mxCreateString ("MULTIPLY"));

    dep_mxa = mxCreateCellMatrix (2, 1);
    for (ii=0; ii<2; ii++)
    {
        sscanf (s, "%s%n", tmp, &nchar);
        s += nchar;
        mxSetCell (dep_mxa, ii, mxCreateString (tmp));
    }
    mxSetField (*f, 0, "dep", dep_mxa);
    return 1;
}

int try_parse_bit (mxArray ** f, mxArray * name, char * l)
{
    const char * field_list[] = {"name", "type", "dep", "firstbit", "bits", "isgood"};
    char tmp[MAXLINELENGTH];
    int firstbit, bits;
    int nchar;
    mxArray * dep_mxa;
    mxArray * firstbit_mxa;
    mxArray * bits_mxa;
    double * firstbit_pr;
    double * bits_pr;
    
    *f = mxCreateStructMatrix (1, 1, sizeof(field_list)/sizeof(char *), field_list);
    mxSetField (*f, 0, "name", name);
    mxSetField (*f, 0, "type", mxCreateString ("BIT"));
    
    dep_mxa = mxCreateCellMatrix (1,1);
    firstbit_mxa = mxCreateDoubleMatrix(1,1,mxREAL);
    firstbit_pr = mxGetPr(firstbit_mxa);
    bits_mxa = mxCreateDoubleMatrix(1,1,mxREAL);
    bits_pr = mxGetPr(bits_mxa);
    
    /* printf("%s\n",l); */
    sscanf(l, "%s %d %d", tmp, &firstbit, &bits);
    /* printf("%s|%d|%d\n",tmp,firstbit,bits); */
    *firstbit_pr = (double) firstbit;
    *bits_pr = (double) bits;
    mxSetCell (dep_mxa, 0, mxCreateString (tmp));
    mxSetField (*f, 0, "dep", dep_mxa);
    mxSetField (*f, 0, "firstbit", firstbit_mxa);
    mxSetField (*f, 0, "bits", bits_mxa);
    
    return 1;

    /*printf ("BIT fields not yet supported.  Bug Walt about it.\n");
    printf ("%s",l);
    return 0;*/
}

int try_parse_phase (mxArray ** f, mxArray * name, char * l)
{
    printf ("PHASE fields not yet supported.  Bug Walt about it.\n");
    return 0;
}

int try_parse_field (mxArray ** f, char * l)
{
    int r = 0;
    char * s = l;
    mxArray * field_name;
    const char * type_list[] = {"RAW", "LINCOM", "LINTERP", "MULTIPLY", "BIT", "PHASE"};
    int ii;

    /* Some field names are not allowed. */
    if (!strncmp (s, "INDEX", 5) || !strncmp (s, "FILEFRAM", 8))
    {
        printf ("WARNING: Forbidden field name ignored in line:\n");
        printf ("    %s\n");
        return 1;
    }

    /* Find end of field name */
    while (!isspace(s[0]) && s[0] != '\0') s++;
    if (s[0] == '\0') return 0;

    s[0] = '\0';
    field_name = mxCreateString (l);
    s[0] = ' ';

    /* Find beginning of next token, which should be field type */
    while (isspace (s[0])) s++;

    for (ii=0; ii<sizeof(type_list)/sizeof(char *); ii++)
    {
        if (!strncmp (type_list[ii], s, strlen(type_list[ii])))
        {
            s += strlen(type_list[ii]);
            while (isspace (s[0])) s++;
            r = 1;
            break;
        }
    }
    if (r == 0) return 0;

    switch (ii)
    {
        case 0 : return try_parse_raw (f, field_name, s);
        case 1 : return try_parse_lincom (f, field_name, s);
        case 2 : return try_parse_linterp (f, field_name, s);
        case 3 : return try_parse_multiply (f, field_name, s);
        case 4 : return try_parse_bit (f, field_name, s);
        case 5 : return try_parse_phase (f, field_name, s);
    }

    printf ("ERROR parsing field definition line:\n");
    printf ("    %s\n", l);

    return -1;
}

int read_fmt (mxArray * D, char * fmtfile)
{
    FILE * f;
    char l[MAXLINELENGTH];
    char * s;
    int r;
    mxArray * newfield;
    mxArray * fields;
    int numfields = 0;
    int maxfield = 0;

    f = fopen (fmtfile, "rt");
    if (f == NULL)
    {
        printf ("ERROR: Format file %s does not exist.\n", fmtfile);
        return -1;
    }

    fields = mxCreateCellMatrix (0, 0);

    while (!feof (f))
    {
        s = fgets (l, MAXLINELENGTH, f);

        /* At EOF */
        if (s == NULL)
            break;

        /* Skip leading spaces */
        while (isspace(s[0])) s++;

        /* Comment or blank line */
        if (s[0] == '#' || s[0] == '\0')
            continue;

        /* Try parsing as a directive */
        r = try_parse_directive (D, s);
        if (r != 0)
            continue;

        /* Try parsing as field specification */
        r = try_parse_field (&newfield, s);
        if (r == 1)
        {
            add_to_1d_cell_array (&fields, newfield, &numfields, &maxfield);
            /* printf ("Got an entry, now numfields=%d, maxfield=%d.\n", numfields, maxfield); */
            continue;
        }

        /* What else?!?! */
        printf ("WARNING: Could not parse a line of format file:\n");
        printf ("    %s\n", l);
    }

    /* printf ("Done, closing file.\n"); */
    fclose (f);

#if 1==1
    shrink_1d_cell_array (&fields, &numfields, &maxfield);
    /* printf ("Shrunk cell array to maxfield=%d, numfields=%d.\n", maxfield, numfields); */
#endif
#if 1==1
    newfield = mxGetField (D, 0, "fields");
    mxDestroyArray (newfield);
    mxSetField (D, 0, "fields", fields);
#else
    mxDestroyArray (fields);
#endif

    return 0;
}

void mexFunction (int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[])
{
    char * fmtfile;
    mxArray * D;
    mxClassID inpt_class;
    int r;

    if (nrhs != 1)
        mexErrMsgTxt ("dirfile_read_format takes one argument.");

    inpt_class = mxGetClassID (prhs[0]);

    switch (inpt_class)
    {
        case mxCHAR_CLASS  : dup_D (&D, NULL);
                             fmtfile = mxArrayToString (prhs[0]);
                             set_fmtfile_D (D, fmtfile);
                             break;
        case mxSTRUCT_CLASS: dup_D (&D, (mxArray *)prhs[0]);
                             fmtfile = get_fmtfile_D (D);
                             break;
        default:
            mexErrMsgTxt ("dirfile_read_format takes a struct as its argument.");
    }

    r = read_fmt (D, fmtfile);
    /* printf ("Finished reading file, result=%d.\n", r); */

    if (r == 0)
        plhs[0] = D;
    else
        mxDestroyArray (D);

    mxFree (fmtfile);
    return;
}

