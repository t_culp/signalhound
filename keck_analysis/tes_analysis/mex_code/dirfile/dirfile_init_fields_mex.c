/*
RWO 090212 - use mex to speed up size test on RAW files,
             dependency check on derived fields
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "mex.h"
#ifndef HAVE_OCTAVE
#  include "matrix.h"
#endif

#ifndef mwSize
#define mwSize int
#endif

#define FILESEP_STR "/"
#define FILESEP_CHR '/'

int set_fname (mxArray * f, char * dir_root, char * name_field_name)
{
    mxArray * tmp;
    char * field_root;
    char * name;
    char * fname;

    tmp = mxGetField (f, 0, "root");
    field_root = mxArrayToString (tmp);
    tmp = mxGetField (f, 0, name_field_name);
    name = mxArrayToString (tmp);
    tmp = mxGetField (f, 0, "fname");
    mxDestroyArray (tmp);
    if (strlen (field_root) == 0)
    {
        fname = mxMalloc (strlen (dir_root) + strlen (name) + 2);
        strcpy (fname, dir_root);
    }
    else
    {
        fname = mxMalloc (strlen (field_root) + strlen (name) + 2);
        strcpy (fname, field_root);
    }
    strcat (fname, FILESEP_STR);
    strcat (fname, name);
    tmp = mxCreateString (fname);
    mxSetField (f, 0, "fname", tmp);

    mxFree (field_root);
    mxFree (fname);
    return 0;
}

int datatype_size (char * dtype)
{
    const char * type_list[] = {"uint8", "int8", "uint16", "int16",
                                "uint32", "int32", "uint64", "int64",
                                "single", "double"};
    const int nbytes_list[] = {1, 1, 2, 2, 4, 4, 8, 8, 4, 8};

    int ii;

    for (ii=0; ii<sizeof(type_list)/sizeof(char *); ii++)
    {
        if (!strcmp (dtype, type_list[ii]))
            return nbytes_list[ii];
    }
    return 0;
}

int init_raw (mxArray * f, char * dir_root)
{
    mxArray * tmp;
    struct stat st;
    char * fname;
    int r;

    set_fname (f, dir_root, "name");
    tmp = mxGetField (f, 0, "f");
    mxDestroyArray (tmp);
    tmp = mxCreateDoubleMatrix (0, 0, mxREAL);
    mxSetField (f, 0, "f", tmp);
    tmp = mxGetField (f, 0, "fname");
    fname = mxArrayToString (tmp);

    tmp = mxGetField (f, 0, "nsamp");
    mxDestroyArray (tmp);

    r = stat (fname, &st);
    if (r == -1)
    {
        tmp = mxCreateDoubleScalar (0);
        mxSetField (f, 0, "nsamp", tmp);
    }
    else
    {
        char * dtype;
        tmp = mxGetField (f, 0, "datatype");
        dtype = mxArrayToString (tmp);
        tmp = mxCreateDoubleScalar (st.st_size / datatype_size(dtype));
        mxSetField (f, 0, "nsamp", tmp);
        mxFree (dtype);
    }    

    mxFree (fname);
    return 0;
}

int check_dep (mxArray * f, mxArray * names)
{
    int isgood = 1;
    mxArray * tmp;
    mxArray * dep;
    mwSize ndims;
    mwSize * dims;
    int ndep, ii;
    char * depname;

    dep = mxGetField (f, 0, "dep");
    ndims = mxGetNumberOfDimensions (dep);
    if (ndims != 2)
    {
        printf ("Badly formed 'dep' field!\n");
        return -1;
    }
    dims = (mwSize *)mxGetDimensions (dep);
    ndep = dims[0];
    if (ndep == 1) ndep = dims[1];

    for (ii=0; ii<ndep; ii++)
    {
        tmp = mxGetCell (dep, ii);
        depname = mxArrayToString (tmp);
        tmp = mxGetField (names, 0, depname);
        if (tmp == 0)
        {
            mxArray * fieldname;
            char * namestr;
            fieldname = mxGetField (f, 0, "name");
            namestr = mxArrayToString (fieldname);
            printf ("WARNING: derived field %s depends on unknown field %s.\n",
                namestr, depname);
            isgood = 0;
            mxFree (namestr);
        }
        mxFree (depname);
    }

    tmp = mxGetField (f, 0, "isgood");
    mxDestroyArray (tmp);
    tmp = mxCreateDoubleScalar (isgood);
    mxSetField (f, 0, "isgood", tmp);

    return 0;
}

int init_linterp (mxArray * f, char * root)
{
    return 0;
}

int init_fields (mxArray * D)
{
    mwSize ndims;
    mwSize * dims;
    mxArray * flist;
    mxArray * f;
    mxArray * tmp;
    mxArray * names;
    char * root;
    char * ftype;
    int n, ii;

    flist = mxGetField (D, 0, "fields");
    if (flist == NULL)
    {
        printf ("Missing field 'fields'!\n");
        return -1;
    }

    /* Get number of fields */
    ndims = mxGetNumberOfDimensions (flist);
    if (ndims != 2)
    {
        printf ("Badly formed 'fields' in dirfile structure!\n");
        mxFree (dims);
        return -1;
    }
    dims = (mwSize *)mxGetDimensions (flist);
    n = dims[0];
    if (n == 1) n = dims[1];

    tmp = mxGetField (D, 0, "root");
    root = mxArrayToString (tmp);
    names = mxGetField (D, 0, "names");
    for (ii=0; ii<n; ii++)
    {
        f = mxGetCell (flist, ii);
        tmp = mxGetField (f, 0, "type");
        ftype = mxArrayToString (tmp);
        if (!strcmp (ftype, "RAW"))
            init_raw (f, root);
        else if (!strcmp (ftype, "LINTERP"))
        {
            check_dep (f, names);
            init_linterp (f, root);
        }
        else if (!strcmp (ftype, "LINCOM") || !strcmp (ftype, "MULTIPLY")
             || !strcmp (ftype, "BIT") || !strcmp (ftype, "PHASE"))
            check_dep (f, names);
        mxFree (ftype);
    }

    mxFree (root);
    return 0;
}

void mexFunction (int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[])
{
    mxArray * D;
    mxClassID inpt_class;
    int r;

    if (nrhs != 1)
        mexErrMsgTxt ("dirfile_init_fields takes one argument.");

    inpt_class = mxGetClassID (prhs[0]);

    switch (inpt_class)
    {
        case mxSTRUCT_CLASS: D = mxDuplicateArray (prhs[0]);
                             break;
        default:
            mexErrMsgTxt ("dirfile_init_fields takes a struct as its argument.");
    }

    r = init_fields (D);
    /* printf ("Finished initializing fields, result=%d.\n", r); */

    if (r == 0)
        plhs[0] = D;
    else
        mxDestroyArray (D);

    return;
}

