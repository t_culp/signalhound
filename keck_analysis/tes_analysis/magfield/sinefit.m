function [amp ph amperr pherr dc cv] = sinefit(tdata,ydata,freq,phase,doTruncate)
% function [amp ph amperr pherr dc cv] = sinefit(tdata,ydata,freq, phase,doTruncate)
%
% Fits a sinusoid of frequency freq to a time series.
%    (ydata-mean(ydata)) = amp * sin (2*pi*freq*tdata + ph);
% The fitting algorithm is exact for a single sinusoid amidst white noise.
%
% INPUTS:
% * tdata: vector of sample times (seconds)
% * ydata: vector of sampled amplitudes
% * freq: frequency of the sine wave to fit (Hz)
% * phase: [OPTIONAL] phase (radians) of sine wave to fit
% * doTruncate: [OPTIONAL] if true, truncates the time stream to an
%   integer number of periods before fitting.  Default is false.
%
% OUTPUTS:
% * amp: amplitude of fitted sinusoid, same units as ydata
% * ph: phase (radians) of fitted sinusoid
% * amperr,pherr: 1-sigma errors on the fitted parameters, assuming white
%   noise.
% * dc: DC offset (mean value) of the trace
% * cv: covariance matrix of the fit
%
% 090216 JPF
% 090319 JPF Can now truncate to an (approximately) integral number
%            of periods.  This isn't necessary for white noise, but may
%            limit the effects of non-white noise and large offsets.
%

% === Check the arguments ===
if length(freq)>1
    error('ERROR in sinefit: freq must be a scalar');
end
szT = size(tdata);
N = max(szT); % number of samples
if min(szT)~=1
    error('ERROR in sinefit: tdata must be a vector!');
end
szY = size(ydata);
if szY~=szT
    error('ERROR in sinefit: vector ydata must be the same size as tdata!');
end
if nargin<5 || isempty(doTruncate)
    doTruncate = 0;   % default
end

% subtract off baseline
dc = mean(ydata);
ydata = ydata - dc;

% should we truncate the trace?
if doTruncate
    nperiods = tdata*freq;
    nmax = floor(nperiods(end));
    indmax = find(nperiods>=nmax,1,'first');
    ydata = ydata(1:indmax);
end

if nargin==4
    % phase argument
    if length(phase)>1
        error('ERROR in sinefit: phase must be a scalar');
    end
    % === Linear fit for amplitude with fixed phase ===
    ss = sin(2*pi*tdata*freq+phase); % fit function
    % fit for amplitude of this in data
    amp = sum(ydata.*ss)/sum(ss.^2);
    % === find the errors ===
    % find the variance after subtracting the fit (good for near-white
    % noise, lousy with large 1/f)
    sig2 = var(ydata-amp*ss);
    amperr = sqrt(sig2/sum(ss.^2));
    ph = phase;
    pherr = 0;
else
    % no phase argument supplied
    % === Linear fit for sine and cosine components ===
    % y = a*sin + b*cos
    ss = sin(2*pi*tdata*freq);
    cc = cos(2*pi*tdata*freq);
    % fit for coefficients of these two terms: vhat = [ahat; bhat]
    M = [sum(ss.^2)  sum(ss.*cc); ...
        sum(ss.*cc) sum(cc.^2) ];
    vv = [sum(ss.*ydata); sum(cc.*ydata)];
    % vv = M * vhat
    vhat = M\vv;
    % === Convert to amplitude and phase ===
    amp = sqrt(sum(vhat.^2));
    ph = atan2(vhat(2),vhat(1));
    if nargout>2
        % === compute the covariance matrix ===
        % find the variance after subtracting the fit
        sig2 = var(ydata-(vhat(1)*ss+vhat(2)*cc));
        % compute the covariance matrix in these variables
        invcov = M/sig2;
        % transform the covariance matrix to new variables
        T = [amp/vhat(1)    amp^2/vhat(2); ...
            -amp^2/vhat(2)  amp^2/vhat(2)];
        cv = (T'/invcov)*T;
        % let's just use the projected errors
        amperr = sqrt(cv(1,1));
        pherr = sqrt(cv(2,2));
    end
end