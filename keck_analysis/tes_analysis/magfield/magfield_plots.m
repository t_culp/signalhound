%plots the data from magfield testing
%fn is the file that contains the info on the files
%figdir is the directory to save the figures
%squid is the squid that we are looking at.  etiher 'ssa', 'sq1',or 'sq2'.  only matters if in open loop
%test is the test we are looking at.  so 0=freq sweep, 1=amp sweep
% example: mapgfield_plots('/data/cryo/current_data/z_closed_loop_10amp.dat','/data/cryo/current_data/figdir','sq1',0)

function [] = magfield_plots(fn,figdir,squid,test);

saveplots=1;
if nargin<2
    saveplots=0;
    figdir='';
end

if ~exist(figdir,'dir')
    mkdir(figdir);
end


%Get the gains for when we look at the other stages
[g123 g1 g2 g3]=fit_sqgain('/export/raid/bicep32/cryo/20101115/1289892421',[-1 1 -1]); 

%Grab the filenames

files = parse_magtest_record(fn);
fn=fn(1:end-4)

%Pass it to magfits_TD

A = magfits_TD(files);

save([fn '.mat'],'A');

nfreq=size(A,1);
nr=size(A(1).amp,1);
nc=size(A(1).amp,2);

%Get rid of bad columns and bad rows
for i=1:nfreq
    A(i).amp(:,1)=0;
    A(i).amp(:,2)=0;
    A(i).amp(2,:)=0;
    A(i).amp(3,:)=0;
    A(i).amp(4,:)=0;
    A(i).amp(5,:)=0; 
    A(i).amp(6,:)=0;
    A(i).amp(7,:)=0;
    A(i).amp(8,:)=0;
    A(i).amp(9,:)=0;
    A(i).amp(10,:)=0;
    A(i).amp(12,:)=0;
    A(i).amp(13,:)=0;
    A(i).amp(14,:)=0;
    A(i).amp(15,:)=0;
    A(i).amp(16,:)=0;
    A(i).amp(17,:)=0; 
    A(i).amp(18,:)=0;
    A(i).amp(19,:)=0; 
    A(i).amp(20,:)=0;
    A(i).amp(21,:)=0;
    A(i).amp(22,:)=0;
    A(i).amp(23,:)=0; 
    A(i).amp(24,:)=0; 
    A(i).amp(25,:)=0; 
    A(i).amp(26,:)=0;
    A(i).amp(28,:)=0;
    A(i).amp(29,:)=0;
    A(i).amp(30,:)=0;
    A(i).amp(31,:)=0;
    A(i).amp(33,:)=0;
end

color_col = {'c.','b.','g.','r.','k.','m.','c+','b+','c.','b.','g.','r.','k.','m.','c+','b+'};


if (test == 0) 
    
%big plot with all of the pixels and freq
figure(10)
for i=1:nfreq
    if A(i).fieldmag ~=0 & A(1).fieldmag ~=0
        norm=A(1).fieldmag/A(i).fieldmag;
    else
        norm=1;
    end        
    for c=1:nc
        for r=1:nr
	 loglog(exp(log(A(i).freq)+(r-16)*.01),A(i).amp(r,c)/A(1).amp(r,c)*norm,color_col{c})
          hold on;
        end
    end
end

ylabel('arb');
xlabel('Frequency (Hz)');
title(['Frequency Sweep'])
xlim([0.04,105])
ylim([1e-4,1e2])

if saveplots
    print('-dpng','-r75',fullfile(figdir, ...
        ['magtest_freqsweep_' fn]));
end




%plot the amplitude at the highest frequency
figure(30)
for r=1:nr
    for c=1:nc
        if A(nfreq).fieldmag ~=0 
            norm=1/A(nfreq).fieldmag;
        else
            norm=1;
        end
        if A(nfreq).data_mode == 10
          norm=norm/2400.;
        elseif squid == 'ssa'
          norm=abs(norm/(31000*g3(c)));
        elseif squid == 'sq2'
            norm=abs(norm/(10000*g3(c)*g2(c)));
        elseif squid == 'sq1'
            norm=abs(norm/(2600*g123(r,c)));
            keyboard
        end
        semilogy(c+r*0.01-.16,A(nfreq).amp(r,c)*16/14*norm,color_col{c})
        hold on;
    end
end

ylabel([squid ' flux/A']);
xlabel('Mux Column');
title(['Magnitude in ' squid ' flux per amp at ' num2str(A(nfreq).freq) 'Hz (' fn ')'])
xlim([0,17]);
ylim([1e-7,1e-3]);

if saveplots
    print('-dpng','-r75',fullfile(figdir, ...
        ['magtest_amp_' num2str(A(nfreq).freq) 'Hz_' fn ]));
end


elseif (test==1)


%plot with magnitude versus amplitude
figure(10)
for i=1:nfreq
%        if A(i).fieldmag ~=0 & A(1).fieldmag ~=0
%        norm=A(1).fieldmag/A(i).fieldmag;
%    else
        norm=1;
%    end        
    for c=1:nc
        for r=1:nr
	 semilogy(A(i).fieldmag+(r-16)*.005,A(i).amp(r,c)/A(1).amp(r,c)*norm,color_col{c})
          hold on;
        end
    end
end

ylabel('arb');
xlabel('Magnetic Field Magnitude');
title(['Amplitude Sweep'])
ylim([1e-1,1e2])

if saveplots
    print('-dpng','-r75',fullfile(figdir, ...
        ['magtest_ampsweep_' fn]));
end


end


%plot with the total amplitude versus muxcolumn

figure(21)
for r=1:nr
    for c=1:nc
       if A(2).fieldmag ~=0 
            norm=1/A(2).fieldmag;
        else
            norm=1;
       end
        
        if A(2).data_mode == 10
            norm=norm/2600.;
        elseif squid == 'ssa'
          norm=abs(norm/(31000*g3(c)));
        elseif squid == 'sq2'
            norm=abs(norm/(10000*g3(c)*g2(c)));
        elseif squid == 'sq1'
            norm=abs(norm/(2600*g123(r,c)));
        end 
        semilogy(c+r*0.01-.16,A(2).amp(r,c)*norm*16/14,color_col{c})
        hold on;
    end
end

ylabel([squid ' flux/A']);
xlabel('Mux Column');
title(['Magnitude in ' squid ' flux per amp at ' num2str(A(2).freq) 'Hz (' fn ')'])
xlim([0,17]);
ylim([1e-7,1e-3]);

if saveplots
    print('-dpng','-r75',fullfile(figdir, ...
        ['magtest_amp_' num2str(A(2).freq) 'Hz_' fn]));
end

keyboard
