function A = magfits_FD2(A);
% function A = magfits_FD2(A);
% function A = magfits_FD2(fn);
%
% Perform frequency-domain fits to sinusoidally-driven magnetic field data.
%
% Note that this code is pickier than magfits_TD.m since it needs to have a
% sample of the noise, i.e. data with the same settings and the magnetic 
% field off.  The code assumes we have two data sets (0 App and 9 App) for 
% each combination of axis and stage.  In particular, to allow for the case
% in which there are several data sets in the same readout configuration
% (e.g. for different sets of SQ1 biases), the code assumes that each file
% with non-zero bias corresponds to the most recent zero-bias run.  This
% makes strong assumptions about the order of files.
%
% INPUTS:
%   - A is a structure array generated by parse_magtest_record.m
%   - fn is an input file with which parse_magtest_record can create A
%
% OUTPUTS:
%   - Returns the same array with new fields for amplitude (amp), phase
%   (ph), and 1-sigma errors on these (amperr and pherr)
%
% 090320 JPF Frequency-domain code created
% 090324 JPF Refactored to match magfits_TD.m
% 090409 JPF New version with different conventions for noise selection

nrows = 33;
ncols = 16;
    
% check arguments
if ~isstruct(A)
    % we didn't get an array of field data ...
    if isstr(A) && exist(A,'file')
        % we got a data file, so parse it
        A = parse_magtest_record(A);
    else
        % I'm confused ...
        error('Input must be a structure array or filename!');
    end
end

% add fields to the data structure to store amplitudes, etc.
empt = zeros(nrows,ncols,'single'); % empty array: single-precision
[A.amp] = deal(empt);
[A.amperr] = deal(empt);
[A.phase] = deal(empt);
[A.pherr] = deal(empt);
[A.baseline] = deal(empt);
clear empt;

noisepsd=[];

for filenum = 1:length(A)
    % cycle through the files
    disp(['Processing file ' int2str(filenum) ' of ' int2str(length(A))]);
    if A(filenum).stage==1 & A(filenum).servo_mode==3
        % closed loop
        stagestr = 'C';
    else
        % open loop
        stagestr = ['0' int2str(A(filenum).stage)];
    end
    disp(['  STAGE = ' stagestr]);
    
    % read the data
    disp('    Reading data file ...');
    S = read_mce(A(filenum).name);
    tdata = (0:(A(filenum).frames-1))'/A(filenum).f_sample;
    if A(filenum).servo_mode==3
        % closed loop
        ydata = [S(:).fb];
    else
        % open loop
        ydata = [S(:).err];
    end
    clear S
    
    % branch based upon amplitude
    disp(['      Field magnitude: ' num2str(A(filenum).fieldmag)]);
    if A(filenum).fieldmag==0
        % zero-field: generate a noise spectrum
                disp('      Generating noise PSDs...');
        noisepsd = time2psd(ydata,400);
        sz = size(noisepsd);
        % filter a bit, set DC noise to infinity
        noisepsd = [Inf(1,sz(2)); ...
            sgolayfilt(noisepsd(2:end,:),1,11,[],1)];
        % we now have a matrix in which each column is a noise PSD
        
        % === Field off: Fitting sinusoids ===
        disp('      Fitting sinusoids...');
        % fit for the amplitudes and phases
        for rr=0:32
            for cc=0:15
                ind = rc2ind(rr,cc);
                if std(ydata(:,ind))~=0
                    [amp ph amperr pherr bs] = ...
                        sinefit_FD(tdata,ydata(:,ind), ...
                        A(filenum).freq,noisepsd(:,ind));
                    A(filenum).phase(rr+1,cc+1)=ph;
                    A(filenum).pherr(rr+1,cc+1)=pherr;
                    A(filenum).amp(rr+1,cc+1)=amp;
                    A(filenum).amperr(rr+1,cc+1)=amperr;
                    A(filenum).baseline(rr+1,cc+1)=bs;
                end
            end
        end
        clear ydata tdata

    else
        % nonzero-field: use most recent noise spectrum
        disp('      Fitting sinusoids...');
        % fit for the amplitudes and phases
        for rr=0:32
            for cc=0:15
                ind = rc2ind(rr,cc);
                if std(ydata(:,ind))~=0
                    [amp ph amperr pherr bs] = ...
                        sinefit_FD(tdata,ydata(:,ind), ...
                        A(filenum).freq,noisepsd(:,ind));
                    A(filenum).phase(rr+1,cc+1)=ph;
                    A(filenum).pherr(rr+1,cc+1)=pherr;
                    A(filenum).amp(rr+1,cc+1)=amp;
                    A(filenum).amperr(rr+1,cc+1)=amperr;
                    A(filenum).baseline(rr+1,cc+1)=bs;
                end
            end
        end
        clear ydata tdata
    end
end


