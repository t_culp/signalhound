function [amp ph amperr pherr dc cv] = sinefit_FD(tdata,ydata,freq,noisepsd)
% function [amp ph amperr pherr dc cv] = sinefit_FD(tdata,ydata,freq,noisepsd)
%
% Fits a sinusoid of frequency freq to a time series.
%    (ydata-mean(ydata)) = amp * sin (2*pi*freq*tdata + ph);
% The fitting algorithm is exact for a single sinusoid amidst white noise.
%
% INPUTS:
% * tdata: vector of sample times (seconds)
% * ydata: vector of sampled amplitudes
% * freq: frequency of the sine wave to fit (Hz)
% * noisepsd: assumed PSD of the noise for a freq-domain fit
%
% OUTPUTS:
% * amp: amplitude of fitted sinusoid, same units as ydata
% * ph: phase (radians) of fitted sinusoid
% * amperr,pherr: 1-sigma errors on the fitted parameters, assuming white
%   noise.
% * dc: DC offset (mean value) of the trace
% * cv: covariance matrix of the fit
%
% 090320 JPF Now accepts a noise PSD for a frequency-domain estimate.
% 090802 JPF Truncates a too-long noise PSD (assumes same sampling rate)
%

% === Check the arguments ===
if length(freq)>1
    disp('ERROR in sinefit: freq must be a scalar');
    return;
end
szT = size(tdata);
N = max(szT); % number of samples
if min(szT)~=1
    disp('ERROR in sinefit: tdata must be a vector!');
    return;
end
szY = size(ydata);
if szY~=szT
    disp('ERROR in sinefit: vector ydata must be the same size as tdata!');
    return;
end

% subtract off baseline
dc = mean(ydata);
ydata = fft(ydata - dc);

% truncate the noise psd


% unfold the noise psd into a quasi-fft
J(1) = Inf; % infinite DC noise
J = psd2fft(noisepsd).^2;

% no phase argument supplied
% === Linear fit for sine and cosine components ===
% y = a*sin + b*cos
% perform a bunch of FFTs
yy = fft(ydata);
ss = fft(sin(2*pi*tdata*freq));
cc = fft(cos(2*pi*tdata*freq));
% fit for coefficients of these two terms: vhat = [ahat; bhat]
M = [ss'*(ss./J)  real(ss'*(cc./J)); ...
    real(ss'*(cc./J)) cc'*(cc./J) ];
vv = [ss'*(ydata./J); cc'*(ydata./J)];
% vv = M * vhat
vhat = real(M\vv);
% === Convert to amplitude and phase ===
amp = sqrt(sum(vhat.^2));
ph = atan2(vhat(2),vhat(1));
if nargout>2
    % === compute the covariance matrix ===
    % transform the covariance matrix to new variables
    % vhat2 = [amp; ph];
    %P = [vhat(1)/amp    vhat(2)/amp; ...
    %    vhat(1)/amp^2  -vhat(2)/amp^2];
    T = [amp/vhat(1)    amp^2/vhat(2); ...
        -amp^2/vhat(2)  amp^2/vhat(2)];
    %cv = P*inv(M)*P';
    %cv = (P/invcov)*P';
    cv = real((T'/M)*T);
    % let's just use the projected errors
    amperr = sqrt(cv(1,1));
    pherr = sqrt(cv(2,2));
end