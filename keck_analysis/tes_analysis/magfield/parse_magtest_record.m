function A = parse_magtest_record(fn);
% Reads a text file fn of magnetic field test data and assembles a Matlab
% data structure with information about the various files.
%
% INPUT FILE FORMAT
%   One file per line, tab-delimited, with the following fields:
%     - filename 
%     - amplitude of applied field (Amps peak-to-peak)
%     - frequency of applied field (Hz)
%     - axis of applied field (x, y, or z)
%     - DC offset of applied field (Amps)
%   Example:
%   /data2/cryo/20090224/fast_1235517444	0	0.1	z   0
%   /data2/cryo/20090224/fast_1235517585	12	0.1	z   0
%   ... etc.
%   File may contain comment lines, which begin with % (as in Matlab)
%
%
% OUTPUT STRUCTURE FORMAT
%   The output structure A is an array with one entry per file and fields:
%     - name: filename string, including full path
%     - fieldmag: applied current amplitude (App)
%     - freq: applied current frequency
%     - dc_offset: DC offset (amps) of applied field
%     - axis: applied field axis (character x, y, or z)
%     - data_mode: integer from MCE (0=error signal, 10=filtered fb, etc.)
%     - servo_mode: integer from MCE (0=open loop, 3=closed loop, etc.)
%     - f_sample: sampling frequency (Hz)
%     - frames: integer number of frames acquired
%     - tes_biased: boolean - are the TES biases nonzero?
%     - stage: integer characterizing loop configuration
%              0 = closed, all stages
%              1 = open, all stages
%              2 = open, SQ1 off, SQ2+SSA on
%              3 = open, SQ1+SQ2 off, SSA on
%   Later scripts may add fields to this structure, e.g. fitted amplitudes.
% 
%
% 090218 JPF Created magfits.m
% 090324 JPF Spun off parse_magtest_record from magfits.m
%            added dc_offset, sampling frequency, commenting
% 090729 JPF Extended for nonzero SQ1 off_bias

fid = fopen(fn);

% FORMAT: name amp freq axis={x,y,z}
ff = textscan(fid,'%s %n %n %c %n','CommentStyle','%'); % makes cell array

N = length(ff{1}); % number of files listed in the text file

% Assemble output structure from columns
A = struct('name',ff{1}, ...
    'fieldmag',num2cell(ff{2}),'freq',num2cell(ff{3}), ...
    'dc_offset',num2cell(ff{5}),'axis',cellstr(ff{4}), ...
    'data_mode',0,'servo_mode',0, ...
    'f_sample',0,'frames',0,...
    'tes_biased',0,'stage',0);

fclose(fid);

clear empt ff;

for ii=1:N
    disp(['  File ' int2str(ii) ' of ' int2str(N)]);
    
    % load the runfile
    R = read_mce_runfile([A(ii).name '.run']);
    
    % get the data and servo modes
    % (assume all channels the same)
    A(ii).data_mode = R.header.rc(1).data_mode;
    A(ii).servo_mode = max(R.header.rc(1).servo_mode);
    
    % are any TESs biased?
    A(ii).tes_biased = sum(R.header.tes.bias)~=0;
    
    % sampling frequency
    A(ii).f_sample = 50e6 / (...
        R.header.rc(1).num_rows * R.header.rc(1).row_len * ...
        R.header.cc.data_rate);
    
    % number of frames
    A(ii).frames = R.frameacq.DATA_FRAMECOUNT;
    
    % find the amplifier configuration
    % stage = -1 (error), 1 (all), 2 (2+ssa only), 3 (ssa only)
    sq1_on = sum(R.header.sq(1).bias-R.header.ac.off_bias)~=0;
    sq2_on = sum(R.header.sq(2).bias)~=0;
    ssa_on = sum(R.header.sa.bias)~=0;
    if ssa_on
        if sq2_on
            if sq1_on
                A(ii).stage = 1;
            else
                A(ii).stage = 2;
            end
        else
            A(ii).stage = 3;
        end
    else
        % wtf?
        A(ii).stage = 0;
    end
end

