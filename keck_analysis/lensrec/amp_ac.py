
# * calculating A_Lens for each analysis choice
# - Last Modified: Thu 12 May 2016 05:29:35 PM EDT

import numpy as np

# variables
bmax   = 10
bn     = 10
data   = '/n/bicepfs3/users/namikawa/BK14/lensrec/'

# analysis choices
default = ['1var_lx30_l30-700_lB150-700']
ellmax  = ['1var_lx30_l30-650_lB150-700','1var_lx30_l30-600_lB150-700','1var_lx30_l30-350_lB150-700']
ellmin  = ['1var_lx30_l150-700_lB150-700','1var_lx30_l200-700_lB150-700']
deproj  = ['1var_lx30_l30-700_lB150-700']
beammap = ['1var_lx30_l30-700_lB150-700']
window  = ['ap_lx30_l30-700_lB150-700']
lBmax   = ['1var_lx30_l30-700_lB150-350']
planck  = ['1var_lx30_l30-700_lB150-700_p2013']
tag = default + ellmax + ellmin + deproj + beammap + window + lBmax #+ planck

#//// set parameters ////#
# observed cl
for j, CX in enumerate(['X','C']):
  print CX
  for I, T in enumerate(tag):
    if CX=='X': 
      idx = 1
      simn = 100
      root = data + 'BKxPLK/'
    if CX=='C': 
      idx = 2
      simn = 499
      root = data + 'clkk/'
    if I == 6: 
      dp = '_dp1100'
    elif I == 7:
      #dp = '_dp11020'
      #dp = '_dp11021'
      dp = '_dp11023'
    else:
      dp = '_dp1102'
    b, C = np.loadtxt(data+'derived/obs/o'+CX+'b_'+T+dp+'_b'+str(bmax)+'.dat',unpack=True,usecols=(0,idx))
    C = C[:bn]

    #//// read cls ////#
    cpp = np.array([np.loadtxt(root+'b'+str(bmax)+'_r'+str(i+1)+'_'+T+'.dat',unpack=True)[idx][:bn] for i in range(simn)])

    # mean
    mcl = np.array([np.mean(cpp[:,n]) for n in range(bn)])
    amp = np.array([cpp[:,n]/mcl[n] for n in range(bn)]).T 

    # covariance
    cov = np.cov(amp,rowvar=0)
    cov[np.isnan(cov)] = 0
    fac = np.sum(np.linalg.inv(cov),axis=0)

    # variance of simulated amplitude estimators
    AL = np.zeros((simn))
    for i in range(simn):
      neum = np.sum(fac*amp[i,:])
      deno = np.sum(fac)
      AL[i] = neum/deno
    m2A  = np.sqrt(np.var(AL))

    # observed amplitude
    neum = np.sum(fac*C/mcl)
    deno = np.sum(fac)
    obsA = neum/deno
    print T, obsA, m2A, obsA/m2A

