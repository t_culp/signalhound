# * Measured vs Simulated ClEE and ClBB to estimate calibration uncertainties
# - Last Modified: Fri 29 Apr 2016 07:41:36 PM EDT

import numpy as np

# read cls
# 1:EE, 2:BB
m = 1

# multipole bins
n0 = 0
n1 = 100
fsky = 0.01

root = '/n/bicepfs3/users/namikawa/BK14/lensrec/derived/cls/'
l,scl,snl,sdl,vcl,ocl = np.loadtxt(root+'1var_lx30_1d.dat',unpack=True,usecols=(0,m,m+2,m+4,m+6,m+12))

fac = 2*l+np.ones(len(l))
amp = ocl/(scl+snl+sdl)

# observed amplitude
neum = np.sum(fac[n0:n1]*amp[n0:n1])
deno = np.sum(fac[n0:n1])
obsA = neum/deno
print obsA, 1./np.sqrt(deno*fsky)

