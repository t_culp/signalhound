# AL posterior (no longer used)
# Last Modified: <Tue 19 Apr 2016 03:28:32 PM EDT>

import numpy as np
import matplotlib as mpl
from matplotlib.pyplot import *

# start plotting
mpl.rcParams.update({'font.size': 16,'legend.fontsize':13})
l = 4
xlim(0.,2.)
ylim(0,1.0)
xlabel(r'$A_{\rm lens}$')
ylabel('Normalized probability distribution')
mA0 = 1.18
vA0 = 0.20
mA1 = 1.30
vA1 = 0.37
AL = np.linspace(0.,2.,100)
plot(AL,np.exp(-(AL-mA0)**2/(2.*vA0**2)),label=r'$C_L^{\kappa\kappa}$ (BK14 x Planck)')
plot(AL,np.exp(-(AL-mA1)**2/(2.*vA1**2)),label=r'$C_L^{\kappa\kappa}$ (BK14)')
AL, pAL = np.loadtxt('/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/BK14_lensing/BK14_lensing_p_Alens.dat',unpack=True)
plot(AL,pAL,label='$C_{\ell}^{BB}$ (BK14)')
legend(loc=0,frameon=False)
savefig('hist_ALL.png',bbox_inches='tight')
show()

