# * jackknife power spectrum
# - Last Modified: Tue 10 May 2016 02:46:34 PM EDT

import numpy as np
import matplotlib as mpl
from matplotlib.pyplot import *

# parameter
tag = '1var_lx30_l30-700_lB150-700'
bmax = 10
dp = '_dp1102'
root = '/n/bicepfs3/users/namikawa/BK14/lensrec/derived/obs/'

#//// plot starting ////#
c    = ['r','g','b','m','c']
lab  = ['Tag','Tile','Phase','Deck','Mux']
jack = [3,4,5,1]

for X in ['X','C']:
#for X in ['C']:

  mpl.rcParams.update({'font.size': 16,'legend.fontsize':13})
  ylim(-.1,.15)
  xlim(15,710)
  xlabel(r'Multipole $L$',fontsize=22)

  # set parameters
  m = 1
  n = m + 5
  if X=='C':
    m = 2
    n = 10
  if X=='X': ylabel(r'$10^6 \times C_L^{\kappa\kappa}$ ($\kappa^{\rm BK}\times\kappa^{\rm P}$)',fontsize=22)
  if X=='C': ylabel(r'$10^6 \times C_L^{\kappa\kappa}$ ($\kappa^{\rm BK}\times\kappa^{\rm BK}$)',fontsize=22)

  # observed cl
  for i, j in enumerate(jack): 
    f = X+'b_'+tag + '_j'+str(j)
    b, oCb = np.loadtxt(root+'/o'+f+dp+'_b'+str(bmax)+'.dat',unpack=True,usecols=(0,m))
    b, sCb, vCb = np.loadtxt(root+'/s'+f+'_b'+str(bmax)+'.dat',unpack=True,usecols=(0,m,n))
    s = 1e6
    oCb = oCb*s;  vCb = vCb*s
    b = b + 7*i*np.ones(len(b))
    errorbar(b,oCb,yerr=vCb,label=lab[i],fmt='o',lw=2,color=c[i])

  plot([0,1000],[-0,0],'k--')
  legend(loc=0,numpoints=1,frameon=False)
  lL, dd = np.loadtxt(root+'../cls/fid_BKsim.dat',usecols=(0,4)).T
  plot(lL,dd*1e6/4,'k-',label="$C_L^{\kappa\kappa}$")
  if X=='X':  savefig('jack_cross.png',bbox_inches='tight')
  if X=='C':  savefig('jack_auto.png',bbox_inches='tight')
  show()
  clf()

