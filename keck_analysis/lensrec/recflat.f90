!///////////////////////////////////////////////////////////!
! * Lensing Reconstruction Kernel
! * Toshiya Namikawa
! - Last Modified : Sun 29 Nov 2015 08:24:07 PM EST
!///////////////////////////////////////////////////////////!

module recflat
  use myconst, only: iu, pi, dlc, twopi
  use myutils, only: interp_lin
  use myfftw, only: DFT
  implicit none

  private iu, pi, dlc, twopi
  private interp_lin
  private DFT

contains 


subroutine elvec(nn,D,el,ei2p)
  implicit none
  !I/O
  integer, intent(in) :: nn(2)
  double precision, intent(in) :: D(2)
  double precision, intent(out) :: el(:,:)
  complex(dlc), intent(out), optional :: ei2p(:)
  !internal
  integer :: n, i, j

  if(present(ei2p)) ei2p = 0d0

  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      el(1:2,n) = twopi*[ dble(i-1-nn(1)*0.5d0)/D(1), dble(j-1-nn(2)*0.5d0)/D(2) ]
      !* compute exp(2i*phi) = cos(2phi) + i*sin(2phi)
      if(present(ei2p)) then
        if(sum(el(:,n)**2)/=0d0) ei2p(n) = ((el(1,n)**2-el(2,n)**2)+iu*2d0*el(1,n)*el(2,n))/sum(el(:,n)**2)
      end if
      n = n + 1
    end do
  end do 

end subroutine elvec


subroutine QUADTT(nn,D,T1,T2,fC,rL,glm,clm)
  implicit none
!
! [input]
!   nn(1:2) --- 2D grids
!   rL(1:2) --- multipole range of reconstruction
!   D(1:2)  --- map size (radian)
!   fC(:)   --- TT correlation in 2D grids
!   T1(:)   --- Temperature 1
!   T2(:)   --- Temperature 2
  integer, intent(in) :: nn(1:2), rL(1:2)
  double precision, intent(in) :: fC(:), D(1:2)
  complex(dlc), intent(in) :: T1(:), T2(:)
!
! (optional)
  complex(dlc), intent(out), optional :: glm(:),clm(:)
!
! [internal]
  integer :: i, n, npix
  double precision :: l, ll(2,nn(1)*nn(2))
  complex(dlc), allocatable :: aT(:), alm(:,:)

  npix = nn(1)*nn(2)
  allocate(aT(npix),alm(2,npix));  aT = 0d0;  alm = 0d0
  call elvec(nn,D,ll)

  if (size(fC)/=npix)  stop "error (quadte): fC is strange."

  !* filtering
  do n = 1, npix
    l = dsqrt(sum(ll(:,n)**2))
    if(rL(1)<=l.and.l<=rL(2)) then 
      aT(n) = T1(n)
      alm(:,n) = ll(:,n)*fC*T2(n)
    end if
  end do 

  !* convolution
  CALL DFT(aT,nn,D,-1)
  do i = 1, 2
    CALL DFT(alm(i,:),nn,D,-1)
    alm(i,:) = aT*alm(i,:)
    CALL DFT(alm(i,:),nn,D,1)
  end do
  deallocate(aT)

  !* estimator 
  ! multiply iu^2
  if(present(glm)) glm = -(alm(1,:)*ll(1,:)+alm(2,:)*ll(2,:))
  if(present(clm)) clm = -(alm(1,:)*ll(2,:)-alm(2,:)*ll(1,:))

  deallocate(alm)

end subroutine QUADTT


subroutine QUADTE(nn,D,T,E,fC,rL,glm,clm,mlm,slm)
  implicit none
!
! [input]
!   nn(1:2) --- 2D grids
!   rL(1:2) --- multipole range of reconstruction
!   D(1:2)  --- map size (radian)
!   fC(:)   --- TE correlation in 2D grids
!   T(:)    --- Temperature
!   E(:)    --- E-modes
  integer, intent(in) :: nn(1:2), rL(1:2)
  double precision, intent(in) :: fC(:), D(1:2)
  complex(dlc), intent(in) :: T(:), E(:)
  complex(dlc), intent(out), optional :: glm(:),clm(:),mlm(:),slm(:)
!
! [internal]
  integer :: i, j, n, npix
  double precision :: l, elv(2,nn(1)*nn(2))
  complex(dlc) :: ei2p(nn(1)*nn(2))
  complex(dlc), dimension(:), allocatable :: aT, aE
  complex(dlc), dimension(:,:), allocatable :: bT, bE, alm

  npix = nn(1)*nn(2)
  allocate(aE(npix),aT(npix),bE(4,npix),bT(4,npix)); aE = 0d0; bE = 0d0; aT = 0d0
  call elvec(nn,D,elv,ei2p)

  if (size(fC)/=npix)  stop "error (quadte): fC is strange."

  !* filtering
  do n = 1, npix
    l = dsqrt(sum(elv(:,n)**2))
    if(rL(1)<=l.and.l<=rL(2)) then 
      aE(n)     = E(n)*conjg(ei2p(n))
      bE(1:2,n) = elv(1:2,n)*fC(n)*T(n)*ei2p(n)
      bE(3,n)   = fC(n)*T(n)*ei2p(n)
      bE(4,n)   = 0d0
      aT(n)     = T(n)
      bT(1:2,n) = fC(n)*elv(1:2,n)*E(n)
      bT(3,n)   = fC(n)*E(n)
      bT(4,n)   = 0d0
    end if
  end do 

  !* convolution
  call DFT(aE,nn,D,-1)
  call DFT(aT,nn,D,-1)
  do i = 1, 4
    call DFT(bE(i,:),nn,D,-1)
    call DFT(bT(i,:),nn,D,-1)
  end do
  allocate(alm(4,npix))
  alm(1,:) = real(aE*bE(1,:)) - aT*bT(1,:)
  alm(2,:) = real(aE*bE(2,:)) - aT*bT(2,:)
  alm(3,:) = aimag(aE*bE(3,:)) - aT*bT(3,:)
  alm(4,:) = 0.d0
  deallocate(aE,bE,aT,bT)
  do i = 1, 4
    CALL DFT(alm(i,:),nn,D,1)
  end do

  !* form estimator 
  if(present(glm)) glm = -(alm(1,:)*elv(1,:)+alm(2,:)*elv(2,:))
  if(present(clm)) clm = -(alm(1,:)*elv(2,:)-alm(2,:)*elv(1,:))
  if(present(mlm)) mlm = alm(3,:)
  if(present(slm)) slm = alm(4,:)
  deallocate(alm)

end subroutine QUADTE


subroutine QUADTB(nn,D,T,B,fC,rL,glm,clm,mlm,slm)
  implicit none
!
! [input]
!   nn(1:2) --- 2D grids
!   rL(1:2) --- multipole range of reconstruction
!   D(1:2)  --- map size (radian)
!   fC(:)   --- TE correlation in 2D grids
!   T(:)    --- Temperature
!   B(:)    --- B-modes
  integer, intent(in) :: nn(1:2), rL(1:2)
  double precision, intent(in) :: fC(:), D(1:2)
  complex(dlc), intent(in) :: T(:), B(:)
!
! (optional)
  complex(dlc), intent(out), optional :: glm(:),clm(:),mlm(:),slm(:)
!
! [internal]
  integer :: i, n, npix
  double precision :: l, iCTE, elv(2,nn(1)*nn(2))
  complex(dlc) ::  ei2p(nn(1)*nn(2))
  complex(dlc), allocatable :: aE(:),bE(:,:),alm(:,:)


  npix = nn(1)*nn(2)
  allocate(aE(npix),bE(4,npix));  aE = 0d0;  bE = 0d0
  call elvec(nn,D,elv,ei2p)

  if (size(fC)/=npix)  stop "error (quadtb): fC is strange."

  !* filtering
  do n = 1, npix
    l = dsqrt(sum(elv(:,n)**2))
    if(rL(1)<=l.and.l<=rL(2)) then 
      aE(n)     = B(n)*conjg(ei2p(n))
      bE(1:2,n) = elv(1:2,n)*fC(n)*T(n)*ei2p(n)
      bE(3,n)   = fC(n)*T(n)*ei2p(n)
      bE(4,n)   = 0d0
    end if
  end do 

  !* convolution
  call DFT(aE,nn,D,-1)
  do i = 1, 4
    call DFT(bE(i,:),nn,D,-1)
  end do
  allocate(alm(4,npix))
  alm(1,:) = real(aE(:)*bE(1,:))/iu
  alm(2,:) = real(aE(:)*bE(2,:))/iu
  alm(3,:) = aimag(aE(:)*bE(3,:))
  alm(4,:) = 0d0
  deallocate(aE,bE)
  do i = 1, 4
    call DFT(alm(i,:),nn,D,1)
  end do

  !* form estimator 
  if(present(glm)) glm = -(alm(1,:)*elv(1,:)+alm(2,:)*elv(2,:))
  if(present(clm)) clm = -(alm(1,:)*elv(2,:)-alm(2,:)*elv(1,:))
  if(present(mlm)) mlm = alm(3,:)
  if(present(slm)) slm = alm(4,:)
  deallocate(alm)

end subroutine QUADTB


subroutine QUADEE(nn,D,X1,X2,fC,rL,glm,clm,eL)
  implicit none
! [input]
!   nn(1:2) --- 2D grids
!   rL(1:2) --- multipole range of reconstruction
!   D(1:2)  --- map size (radian)
!   fC(:)   --- E-mode Cl in 2D grids
!   X1(:)   --- E-modes 1
!   X2(:)   --- E-modes 2
  integer, intent(in) :: nn(1:2), rL(1:2)
  double precision, intent(in) :: fC(:), D(1:2)
  complex(dlc), intent(in) :: X1(:), X2(:)
!
! (optional)
!   eL(1:2) --- multipole range of output
!   glm(:)  --- gradient mode
!   clm(:)  --- curl mode
  integer, intent(in), optional :: eL(1:2)
  complex(dlc), intent(out), optional :: glm(:), clm(:)
!
! [internal]
  integer :: i, n, npix
  double precision :: l, ll(2,nn(1)*nn(2))
  complex(dlc) :: ei2p(nn(1)*nn(2))
  complex(dlc), allocatable :: aE(:), bE(:,:), alm(:,:)

  npix = nn(1)*nn(2)
  allocate(aE(npix),bE(2,npix)) ;  aE = 0d0 ;  bE = 0d0
  call elvec(nn,D,ll,ei2p)

  if (size(fC)/=npix)  stop "error (quadee): fC is strange."

  !* filtering
  do n = 1, npix
    l = dsqrt(sum(ll(:,n)**2))
    if(rL(1)<=l.and.l<=rL(2)) then 
      aE(n)     = X1(n)*conjg(ei2p(n))
      bE(1:2,n) = ll(1:2,n)*fC(n)*X2(n)*ei2p(n)
    end if
  end do 

  !* convolution
  call DFT(aE,nn,D,-1)
  call DFT(bE(1,:),nn,D,-1)
  call DFT(bE(2,:),nn,D,-1)
  allocate(alm(2,npix))
  alm(1,:) = aimag(aE*bE(1,:))*iu
  alm(2,:) = aimag(aE*bE(2,:))*iu
  deallocate(aE,bE)
  call DFT(alm(1,:),nn,D,1)
  call DFT(alm(2,:),nn,D,1)

  !* estimator 
  do n = 1, npix
    if(present(eL)) then
      l = sum(ll(:,n)**2)
      if(l<eL(1)**2.or.l>eL(2)**2) alm(:,n) = 0d0
    end if
    if(present(glm)) glm(n) = (ll(1,n)*alm(1,n)+ll(2,n)*alm(2,n))
    if(present(clm)) clm(n) = (ll(2,n)*alm(1,n)-ll(1,n)*alm(2,n))
  end do
  deallocate(alm)

end subroutine QUADEE


subroutine QUADEB(nn,D,E,B,fC,rL,glm,clm,eL)
  implicit none
! [inputs]
!   nn(1:2) --- 2D grids
!   rL(1:2) --- multipole range of reconstruction
!   D(1:2)  --- map size (radian)
!   fC(:)   --- E-mode Cl in 2D grids
!   E(:)    --- filtered E-modes
!   B(:)    --- filtered B-modes
  integer, intent(in) :: nn(1:2), rL(1:2)
  double precision, intent(in) :: fC(:), D(1:2)
  complex(dlc), intent(in) :: E(:), B(:)
!
! (optional)
!   eL(1:2) --- multipole range of output
!   glm(:)  --- gradient mode
!   clm(:)  --- curl mode
  integer, intent(in), optional :: eL(1:2)
  complex(dlc), intent(out), optional :: glm(:), clm(:)
!
! [internal]
  integer :: i, n, npix
  double precision :: l, lx, ly, ll(2,nn(1)*nn(2))
  complex(dlc), allocatable :: aE(:), aB(:), alm(:,:), bE(:,:), bB(:,:), ei2p(:)

  npix = nn(1)*nn(2)
  allocate(aB(npix),bE(2,npix),ei2p(npix));  aB=0d0;  bE=0d0;  ei2p=0d0
  call elvec(nn,D,ll,ei2p)

  if (size(fC)/=npix)  stop "error (quadeb): fC is strange."

  !* filtering
  do n = 1, npix
    l = dsqrt(sum(ll(:,n)**2))
    if(rL(1)<=l.and.l<=rL(2)) then 
      aB(n)     = B(n)*conjg(ei2p(n))
      bE(1:2,n) = ll(1:2,n)*fC(n)*E(n)*ei2p(n)
    end if
  end do
  deallocate(ei2p)

  !* convolution
  call DFT(aB,nn,D,-1)
  call DFT(bE(1,:),nn,D,-1)
  call DFT(bE(2,:),nn,D,-1)
  allocate(alm(2,npix))
  alm(1,:) = dble(aB*bE(1,:))
  alm(2,:) = dble(aB*bE(2,:))
  deallocate(aB,bE)
  call DFT(alm(1,:),nn,D,1)
  call DFT(alm(2,:),nn,D,1)

  !* estimator 
  do n = 1, npix
    if(present(eL)) then
      l = sum(ll(:,n)**2)
      if(l<eL(1)**2.or.l>eL(2)**2) alm(:,n) = 0d0
    end if
    if(present(glm)) glm(n) = sum(ll(:,n)*alm(:,n))/iu
    if(present(clm)) clm(n) = (ll(2,n)*alm(1,n)-ll(1,n)*alm(2,n))/iu
  end do

  deallocate(alm)

end subroutine QUADEB


subroutine QUADBB(nn,DD,X1,X2,fC,tL,glm,clm,mlm,slm)
  implicit none
  !I/O
  integer, intent(in) :: nn(2), tL(2)
  double precision, intent(in) :: fC(:), DD(2)
  complex(dlc), intent(in) :: X1(:), X2(:)
  complex(dlc), intent(out), optional :: glm(:),clm(:),mlm(:),slm(:)
  !internal
  integer :: i, n, npix
  double precision :: l, elv(2,nn(1)*nn(2))
  complex(dlc) ::  ei2p(nn(1)*nn(2))
  complex(dlc), allocatable :: aB(:), bB(:,:), alm(:,:)

  npix = nn(1)*nn(2)
  allocate(aB(npix),bB(4,npix));  aB = 0d0;  bB = 0d0
  call elvec(nn,DD,elv,ei2p)

  if (size(fC)/=npix)  stop "error (quadbb): fC is strange."

  do n = 1, npix
    l = dsqrt(sum(elv(:,n)**2))
    if(tL(1)<=l.and.l<=tL(2)) then 
      aB(n)     = X1(n)*conjg(ei2p(n))
      bB(3,n)   = fC(n)*X1(n)*ei2p(n)
      bB(1:2,n) = elv(1:2,n)*bB(3,n)
    end if
  end do 

  !* convolution
  call DFT(aB,nn,DD,-1)
  do i = 1, 4
    call DFT(bB(i,:),nn,DD,-1)
  end do
  allocate(alm(4,npix))
  alm(1,:) = aimag(aB*bB(1,:))*iu
  alm(2,:) = aimag(aB*bB(2,:))*iu
  alm(3,:) = real(aB*bB(3,:))
  deallocate(aB,bB)
  do i = 1, 4
    call DFT(alm(i,:),nn,DD,1)
  end do

  !* form estimator 
  if(present(glm)) glm = -(alm(1,:)*elv(1,:)+alm(2,:)*elv(2,:))
  if(present(clm)) clm = -(alm(1,:)*elv(2,:)-alm(2,:)*elv(1,:))
  if(present(mlm)) mlm = alm(3,:)
  if(present(slm)) slm = alm(4,:)
  deallocate(alm)

end subroutine QUADBB

!/////////////////////////////////////////////////////////////////////////////////////!
! 2D normalization

subroutine ALFLAT_EE(nn,D,fE,CE,rL,eL,Alg,Alc)
  implicit none
  !I/O
  integer, intent(in) :: nn(1:2), el(1:2), rL(1:2)
  double precision, intent(in) :: CE(:), fE(:), D(1:2)
  double precision, intent(out), optional :: Alg(:), Alc(:)
! [internal]
  integer :: i, n, npix
  double precision :: l, lx, ly, ll(2,nn(1)*nn(2))
  complex(dlc) :: vec(1:2)
  complex(dlc), allocatable :: Al(:,:), Bl(:,:), A1(:,:), A2(:,:), ei2p(:), B1(:,:), B2(:,:)

  write(*,*) "EE flat"

  npix = nn(1)*nn(2)
  allocate(A1(6,npix),A2(2,npix),B1(4,npix),B2(4,npix),ei2p(npix)); A1=0d0; A2=0d0; ei2p=0d0; B1=0d0; B2=0d0
  call elvec(nn,D,ll,ei2p)

  if (size(CE)/=npix)  stop "error (alflat_ee): CE is strange."

  !* filtering
  do n = 1, npix
    l = dsqrt(sum(ll(:,n)**2))
    if(rL(1)<=l.and.l<=rL(2)) then
      vec       = [ei2p(n)**2,cmplx(1d0,kind=dlc)] * CE(n)**2 * fE(n)
      A1(1:2,n) = ll(1,n)**2 * vec
      A1(3:4,n) = 2d0*ll(1,n)*ll(2,n) * vec
      A1(5:6,n) = ll(2,n)**2 * vec
      A2(1:2,n) = 0.5d0*fE(n)*[conjg(ei2p(n))**2,cmplx(1d0,kind=dlc)]
      vec       = [ei2p(n)**2,cmplx(1d0,kind=dlc)] * CE(n) * fE(n)
      B1(1:2,n) = ll(1,n) * vec
      B1(3:4,n) = ll(2,n) * vec
      B2(:,n)   = conjg(B1(:,n))*0.5d0
    end if
  end do
  deallocate(ei2p)

  !* convolution
  call DFT(A2(1,:),nn,D,-1)
  call DFT(A2(2,:),nn,D,-1)
  allocate(Al(6,npix))
  do i = 1, 6
    call DFT(A1(i,:),nn,D,-1)
    if (mod(i,2)==0)  Al(i,:) = A2(2,:)*A1(i,:)
    if (mod(i,2)==1)  Al(i,:) = A2(1,:)*A1(i,:)
    call DFT(Al(i,:),nn,D,1)
    if (mod(i,2)==1)  Al(i,:) = dble(Al(i,:))
  end do
  deallocate(A1,A2)

  do i = 1, 4
    call DFT(B1(i,:),nn,D,-1)
    call DFT(B2(i,:),nn,D,-1)
  end do
  allocate(Bl(8,npix))
  do i = 1, 4
    Bl(i,:) = B1(i,:)*B2(i,:)
    if (i==1)  Bl(i+4,:) = B1(i,:)*B2(3,:)
    if (i==2)  Bl(i+4,:) = B1(i,:)*B2(4,:)
    if (i==3)  Bl(i+4,:) = B1(i,:)*B2(1,:)
    if (i==4)  Bl(i+4,:) = B1(i,:)*B2(2,:)
    call DFT(Bl(i,:),nn,D,1)
    call DFT(Bl(i+4,:),nn,D,1)
    if (mod(i,2)==1)  Bl(i,:) = dble(Bl(i,:))
    if (mod(i,2)==1)  Bl(i+4,:) = dble(Bl(i+4,:))
  end do
  deallocate(B1,B2)

  !* normalization
  do n = 1, npix
    l = sum(ll(:,n)**2)
    if(l<eL(1)**2.or.l>eL(2)**2)  cycle
    if (present(Alg)) then
      Alg(n) = ll(1,n)**2*sum(Al(1:2,n)) + ll(1,n)*ll(2,n)*sum(Al(3:4,n)) + ll(2,n)**2*sum(Al(5:6,n)) &
        + ll(1,n)**2*sum(Bl(1:2,n)) + ll(2,n)**2*sum(Bl(3:4,n)) + ll(1,n)*ll(2,n)*sum(Bl(5:8,n))
      Alg(n) = 1d0/Alg(n)
    end if
    if (present(Alc)) then
      Alc(n) = ll(2,n)**2*sum(Al(1:2,n)) - ll(1,n)*ll(2,n)*sum(Al(3:4,n)) + ll(1,n)**2*sum(Al(5:6,n)) &
        + ll(2,n)**2*sum(Bl(1:2,n)) + ll(1,n)**2*sum(Bl(3:4,n)) - ll(1,n)*ll(2,n)*sum(Bl(5:8,n))
      Alc(n) = 1d0/Alc(n)
    end if
  end do

  deallocate(Al)

end subroutine ALFLAT_EE


subroutine ALFLAT_EB(nn,D,fE,fB,CE,rL,eL,Alg,Alc)
  implicit none
  !I/O
  integer, intent(in) :: nn(1:2), el(1:2), rL(1:2)
  double precision, intent(in) :: CE(:), fE(:), fB(:), D(1:2)
  double precision, intent(out), optional :: Alg(:), Alc(:)
! [internal]
  integer :: i, n, npix
  double precision :: l, lx, ly, iCEE, ll(2,nn(1)*nn(2))
  complex(dlc) :: vec(1:2)
  complex(dlc), allocatable :: Al(:,:), A1(:,:), A2(:,:), ei2p(:)

  write(*,*) "EB flat"

  npix = nn(1)*nn(2)
  allocate(A1(6,npix),A2(2,npix),ei2p(npix));  A1=0d0;  A2=0d0;  ei2p=0d0
  call elvec(nn,D,ll,ei2p)

  if (size(CE)/=npix)  stop "error (alflat_eb): CE is strange."

  !* filtering
  do n = 1, npix
    l = dsqrt(sum(ll(:,n)**2))
    if(rL(1)<=l.and.l<=rL(2)) then
      vec       = [-ei2p(n)**2,cmplx(1d0,kind=dlc)] * CE(n)**2 * fE(n)
      A1(1:2,n) = ll(1,n)**2 * vec
      A1(3:4,n) = 2d0*ll(1,n)*ll(2,n) * vec
      A1(5:6,n) = ll(2,n)**2 * vec
      A2(1:2,n) = 0.5d0*fB(n)*[conjg(ei2p(n))**2,cmplx(1d0,kind=dlc)]
    end if
  end do
  deallocate(ei2p)

  !* convolution
  call DFT(A2(1,:),nn,D,-1)
  call DFT(A2(2,:),nn,D,-1)
  allocate(Al(6,npix))
  do i = 1, 6
    call DFT(A1(i,:),nn,D,-1)
    if (mod(i,2)==0)  Al(i,:) = A2(2,:)*A1(i,:)
    if (mod(i,2)==1)  Al(i,:) = A2(1,:)*A1(i,:)
    call DFT(Al(i,:),nn,D,1)
    if (mod(i,2)==1)  Al(i,:) = dble(Al(i,:))
  end do
  deallocate(A1,A2)

  !* normalization
  do n = 1, npix
    l = sum(ll(:,n)**2)
    if(l<eL(1)**2.or.l>eL(2)**2)  cycle
    if (present(Alg))  Alg(n) = 1d0 / (ll(1,n)**2*sum(Al(1:2,n)) + ll(1,n)*ll(2,n)*sum(Al(3:4,n)) + ll(2,n)**2*sum(Al(5:6,n)))
    if (present(Alc))  Alc(n) = 1d0 / (ll(2,n)**2*sum(Al(1:2,n)) - ll(1,n)*ll(2,n)*sum(Al(3:4,n)) + ll(1,n)**2*sum(Al(5:6,n)))
  end do

  deallocate(Al)

end subroutine ALFLAT_EB


!/////////////////////////////////////////////////////////////////////////////////////!
! Delensing

subroutine LensingB(nn,D,Elm,glm,rL,Blm)
  implicit none
!
! [input]
!   nn  --- x and y grids
!   rL  --- multipole ranges for E and phi
!   D   --- map size
!   Elm --- E-mode
!   glm --- phi map
  integer, intent(in) :: nn(1:2), rL(1:2)
  double precision, intent(in) :: D(1:2)
  complex(dlc), intent(in) :: Elm(:), glm(:)
!
! [output]
!   Blm --- estimated lensing B-mode
  complex(dlc), intent(out) :: Blm(:)
!
! [internal]
  integer :: n, npix
  double precision :: l, ll(2,nn(1)*nn(2))
  complex(dlc), allocatable :: phi(:,:), alm(:,:), bE(:,:), ei2p(:)

  npix = nn(1)*nn(2)
  allocate(phi(2,npix),bE(4,npix),ei2p(npix));  phi=0d0;  bE=0d0;  ei2p=0d0

  call elvec(nn,D,ll,ei2p)

  do n = 1, npix
    l = dsqrt(sum(ll(:,n)**2))
    if(rL(1)<=l.and.l<=rL(2)) then 
      phi(1:2,n) = ll(1:2,n)*glm(n)
      bE(1:2,n)  = ll(1:2,n)*Elm(n)*ei2p(n)
      bE(3:4,n)  = ll(1:2,n)*Elm(n)*conjg(ei2p(n))
    end if
  end do 

  !* convolution
  call DFT(phi(1,:),nn,D,-1)
  call DFT(phi(2,:),nn,D,-1)
  call DFT(bE(1,:),nn,D,-1)
  call DFT(bE(2,:),nn,D,-1)
  call DFT(bE(3,:),nn,D,-1)
  call DFT(bE(4,:),nn,D,-1)
  allocate(alm(2,npix))
  alm(1,:) = phi(1,:)*bE(1,:) + phi(2,:)*bE(2,:)
  alm(2,:) = phi(1,:)*bE(3,:) + phi(2,:)*bE(4,:)
  deallocate(phi,bE)
  call DFT(alm(1,:),nn,D,1)
  call DFT(alm(2,:),nn,D,1)
  Blm = -(alm(1,:)*conjg(ei2p)-alm(2,:)*ei2p)/(2d0*iu)
  deallocate(alm,ei2p)

end subroutine LensingB


!/////////////////////////////////////////////////////////////////////////////////////!
! other quadratic estimators
!

subroutine QUADTT_ADD(nn,D,T1,T2,CTT,tL,mlm,slm)
  implicit none
  !I/O
  integer, intent(in) :: nn(1:2), tL(1:2)
  double precision, intent(in) :: CTT(:), D(1:2)
  complex(dlc), intent(in) :: T1(:), T2(:)
  complex(dlc), intent(out), optional :: mlm(:), slm(:)
  !internal
  integer :: i, n, npix
  double precision :: l, iC, elv(2,nn(1)*nn(2))
  complex(dlc), allocatable :: aT(:), alm(:,:)

  CALL ELVEC(nn,D,elv)

  npix = nn(1)*nn(2)
  allocate(aT(npix),alm(2,npix));  aT = 0d0 ;  alm = 0d0

  !* filtering
  do n = 1, npix
    l = dsqrt(elv(1,n)**2+elv(2,n)**2)
    if(tL(1)<=l.and.l<=tL(2)) then 
      iC = interp_lin(l,dble(int(l)),dble(int(l)+1),CTT(int(l)),CTT(int(l)+1))
      aT(n) = T1(n)
      alm(1,n) = cmplx(iC)*T2(n)
      alm(2,n) = 0.5d0*T2(n)
    end if
  end do 

  !* convolution
  CALL DFT(aT,nn,D,-1)
  do i = 1, 2
    CALL DFT(alm(i,:),nn,D,-1)
    alm(i,:) = aT(:)*alm(i,:)
    CALL DFT(alm(i,:),nn,D,1)
  end do
  deallocate(aT)

  !* form estimator 
  ! multiply iu^2
  if(present(mlm)) mlm = alm(1,:)
  if(present(slm)) slm = alm(2,:)

  deallocate(alm)

end subroutine QUADTT_ADD


subroutine QUADEE_BHE(nn,DD,X1,X2,CEE,tL,mlm,slm)
  implicit none
  !I/O
  integer, intent(in) :: nn(2), tL(2)
  double precision, intent(in) :: CEE(:), DD(2)
  complex(dlc), intent(in) :: X1(:), X2(:)
  complex(dlc), intent(out), optional :: mlm(:),slm(:)
  !internal
  integer :: i, n, npix
  double precision :: l, iC, elv(2,nn(1)*nn(2))
  complex(dlc) :: ei2p(nn(1)*nn(2))
  complex(dlc), allocatable :: aE(:), bE(:,:), alm(:,:)

  CALL ELVEC(nn,DD,elv,ei2p)

  npix = nn(1)*nn(2)
  allocate(aE(npix),bE(2,npix));  aE = 0d0;  bE = 0d0

  !* filtering
  do n = 1, npix
    l = dsqrt(sum(elv(:,n)**2))
    if(tL(1)<=l.and.l<=tL(2)) then 
      iC = interp_lin(l,dble(int(l)),dble(int(l)+1),CEE(int(l)),CEE(int(l)+1))
      aE(n) = X1(n)*conjg(ei2p(n))
      bE(1,n) = iC*X1(n)*ei2p(n)
      bE(2,n) = X1(n)*ei2p(n)
    end if
  end do

  !* convolution
  call DFT(aE,nn,DD,-1)
  call DFT(bE(1,:),nn,DD,-1)
  call DFT(bE(2,:),nn,DD,-1)
  allocate(alm(2,npix))
  alm(1,:) = real(aE*bE(1,:))
  alm(2,:) = real(aE*bE(2,:))
  deallocate(aE,bE)
  call DFT(alm(1,:),nn,DD,1)
  call DFT(alm(2,:),nn,DD,1)

  !* form estimator 
  if(present(mlm)) mlm = alm(1,:)
  if(present(slm)) slm = alm(2,:)
  deallocate(alm)

end subroutine QUADEE_BHE


subroutine QUADEB_BHE(nn,D,E,B,LCEE,LCBB,rL,mlm,slm,rlm)
! * BHE for masking, point source, and polarization angle
! 
  implicit none
  !I/O
  integer, intent(in) :: nn(2), rL(2)
  double precision, intent(in) :: LCEE(:), LCBB(:), D(2)
  complex(dlc), intent(in) :: E(:),B(:)
  complex(dlc), intent(out), optional :: mlm(:),slm(:),rlm(:)
  !internal
  integer :: i, n, npix
  double precision :: l, ll(2,nn(1)*nn(2))
  complex(dlc) :: ei2p(nn(1)*nn(2))
  complex(dlc), allocatable :: X(:,:), alm(:), bE(:,:), bB(:,:)

  npix = nn(1)*nn(2)
  allocate(X(2,npix),bE(2,npix),bB(2,npix)); X = 0d0 ;  bE = 0d0 ;  bB = 0d0
  call elvec(nn,D,ll,ei2p)

  !* filtering
  do n = 1, npix
    l = dsqrt(sum(ll(:,n)**2))
    if(rL(1)<=l.and.l<=rL(2)) then 
      X(1:2,n) = [E(n),B(n)]*conjg(ei2p(n))
      bE(1,n) = LCEE(int(l))*E(n)*ei2p(n)
      bB(1,n) = LCBB(int(l))*B(n)*ei2p(n)
      bE(2,n) = E(n)*ei2p(n)*0.5d0
      bB(2,n) = B(n)*ei2p(n)*0.5d0
    end if
  end do 

  !* convolution
  do i = 1, 2
    call DFT(X(i,:),nn,D,-1)
    call DFT(bE(i,:),nn,D,-1)
    call DFT(bB(i,:),nn,D,-1)
  end do

  !* estimator 
  allocate(alm(npix));  alm=0d0
  if(present(mlm)) then
    alm = aimag(X(2,:)*bE(1,:)-X(1,:)*bB(1,:))
    call DFT(alm,nn,D,1)
    mlm = alm
  end if
  if(present(slm)) then
    alm = aimag(X(2,:)*bE(2,:)-X(1,:)*bB(2,:))
    call DFT(alm,nn,D,1)
    slm = alm
  end if
  if(present(rlm)) then
    alm = dble(X(2,:)*bE(1,:)-X(1,:)*bB(1,:))
    call DFT(alm,nn,D,1)
    rlm = alm
  end if
  deallocate(X,bE,bB,alm)

end subroutine QUADEB_BHE


end module recflat


