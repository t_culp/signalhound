
# - Last Modified: Wed 11 May 2016 09:49:20 PM EDT

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

# parameter
tag = '1var_lx30_l30-700_lB150-700'
bmax = 10
s = 1e6
dp = '_dp1102'
root = '/n/bicepfs3/users/namikawa/BK14/lensrec/derived/obs/'

#//// plot starting ////#
mpl.rcParams.update({'font.size': 16,'legend.fontsize':13})
plt.xlabel(r'Multipole',fontsize=22)
c = ['r','g']

#//// set parameters ////#
m = 1
n = m + 5
y = [-0.11,0.45]
lab = ['BK14 x Planck2015','BK14 x Planck2013']
plt.ylabel(r'$C_L^{\kappa\kappa}\times 10^6$',fontsize=22)

#//// read cls ////#
plt.xlim(30,700)
plt.ylim(y)

# observed cl
for i in [0,1]: 
  if i==0: f = 'Xb_'+tag
  if i==1: f = 'Xb_'+tag+'_p2013'
  b, oCb, vCb = np.loadtxt(root+'/o'+f+dp+'_b'+str(bmax)+'.dat',unpack=True,usecols=(0,m,n))
  b, sCb, v2Cb = np.loadtxt(root+'/s'+f+'_b'+str(bmax)+'.dat',unpack=True,usecols=(0,m,n))
  oCb = oCb*s;  vCb = vCb*s
  b = b + 7*i*np.ones(len(b))
  plt.errorbar(b,oCb,yerr=vCb,label=lab[i],fmt='o',lw=2,color=c[i])

lL, dd = np.loadtxt(root+'../cls/fid_BKsim.dat',usecols=(0,4)).T
plt.plot(lL,dd*s/4,'k-',label="Theoretical $C_L^{\kappa\kappa}$")
b,gEB,gTB = np.loadtxt(root+'../n0/bk14p15.dat',unpack=True,usecols=(0,1,3))
plt.plot(b,(gEB+gTB)*s*10,'k:',label=r"Disconnected bias (BK14 x Planck2015) $\times 10$")
plt.plot([0,1000],[-0,0],'k--')
plt.legend(loc=0,numpoints=1,frameon=False)
plt.savefig('clkk_planck.png',bbox_inches='tight')
plt.show()

