
# - Last Modified: Wed 11 May 2016 09:44:48 PM EDT

import numpy as np
from matplotlib.pyplot import *
import matplotlib as mpl

# parameter
tag = '1var_lx30_l30-700_lB150-700'
bmax = 10
dp = '_dp1102'
root = '/n/bicepfs3/users/namikawa/BK14/lensrec/derived/obs/'

#//// plot starting ////#
mpl.rcParams.update({'font.size': 16,'legend.fontsize':13})
xlabel(r'Multipole $L$',fontsize=22)
c = ['r','g','b']

#//// set parameters ////#
y = [-1,1]
#lab = [r'$\psi^{\rm BK14}\times\kappa^{\rm Planck}$',r'$\psi^{\rm BK14}\times\psi^{\rm BK14}$',r'$\psi^{\rm BK14}\times\kappa^{\rm BK14}$']
lab = [r'$C_L^{\psi\kappa}$ (BK14 x Planck)',r'$C_L^{\psi\psi}$ (BK14)',r'$C_L^{\psi\kappa}$ (BK14)']
ylabel(r'$C_L \times 10^6$',fontsize=22)

#//// read cls ////#
xlim(15,710)
ylim(y)

# observed cl
for i, X in enumerate(['cK','cc','ck']): 
  if X=='cK':
    f = 'Xb_'+tag
    m = 2
    n = m + 5
  if X=='cc':
    f = 'Cb_'+tag
    m = 4
    n = m + 8
  if X=='ck':
    f = 'Cb_'+tag
    m = 7
    n = m + 8
  b, oCb, vCb = np.loadtxt(root+'/o'+f+dp+'_b'+str(bmax)+'.dat',unpack=True,usecols=(0,m,n))
  s = 1e6
  oCb = oCb*s;  vCb = vCb*s
  b = b + 7*i*np.ones(len(b))
  errorbar(b,oCb,yerr=vCb,label=lab[i],fmt='o',lw=2,color=c[i])

lL, dd = np.loadtxt(root+'../cls/fid_BKsim.dat',usecols=(0,4)).T
plot(lL,dd*1e6/4,'k-',label="Theoretical $C_L^{\kappa\kappa}$")
plot([0,1000],[-0,0],'k--')
legend(loc=0,numpoints=1,frameon=False)
savefig('curl.png',bbox_inches='tight')
show()

