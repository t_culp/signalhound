function pure(type,zmax,do_proj,j,dp)
% * making the purified maps and save to ASCII files
% 
% * external functions (dependencies): 
%  - write_QUmap_dat.m
%  - write_Tmap_dat.m
%  - do_projection_tn.m
%
% * input parameters
%  - type (integer)    : choose simulation type  ( 2: unlensed,  3: dust,  4: beam,  5: lensed,  6: noise )
%  - zmax (integer)    : total realization number
%  - do_proj (integer) : whether do purification ( 0: false,  1: true )
%  - j  (integer)      : jackknife number
%  - dp (integer)      : deprojection type
% 

% check variables
if(~exist('type','var'))     error('no simulation type specified');  end;
if(~exist('zmax','var'))     zmax = 0;  end;
if(~exist('do_proj','var'))  do_proj = 0;  end;
if(~exist('j','var'))        j = 0;  end;
if(~exist('dp','var'))       dp = 1102;  end;

data = 'BK14';
zmin = 2;

switch type
case 2
  strtype = 'unlensed';
  dp = 1100;
case 3
  strtype = 'dust';
  dp = 1100;
case 4
  strtype = 'lensed';
  zmin = 1;
  zmax = 0;
case 5
  strtype = 'lensed';
  zmin = 1;
case 6
  strtype = 'noise';
  dp = 1100;
end

% fixed parameters
if data == 'BK13'  %BK13
  root = '/n/bicepfs1/bicep2/pipeline/maps/1456/';
  sims = strcat('_aab_filtp3_weight3_gs_dp1100_jack',int2str(j),'.mat');
  real = strcat('_aab_filtp3_weight3_gs_dp',int2str(dp),'_jack',int2str(j),'.mat');
  proj = '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat';
  beam = '/n/bicepfs2/keck/pipeline/maps/2505/0002_cda_filtp3_weight3_gs_dp11020_jack0.mat';
  outd = strcat('/n/bicepfs3/users/namikawa/BK13/lensrec/map_',strtype);
end
if data == 'BK14'  %BK14
  switch j
  case 0
    root = '/n/bicepfs1/bicep2/pipeline/maps/1459/';
  case num2cell(3:5) 
    root = '/n/panlfs2/bicep/keck/pipeline/maps/1459/';
  otherwise
    root = '/n/bicepfs3/users/namikawa/BK14/reduc/';
  end
  sims = strcat('_aabd_filtp3_weight3_gs_dp1100_jack',int2str(j),'.mat');
  real = strcat('_aabd_filtp3_weight3_gs_dp',int2str(dp),'_jack',int2str(j),'.mat');
  proj = '/n/bicepfs3/keck/pipeline/matrixdata/c_t/1459/healpix_red_spectrum_lmax700_beamB2bbns_reob1459_aabd_150GHz_QQQUUU_proj.mat';
  beam = '/n/panlfs2/bicep/keck/pipeline/maps/2549/0001_aabd_filtp3_weight3_gs_dp1102_jack0.mat';
  outd = strcat('/n/bicepfs3/users/namikawa/BK14/lensrec/map_',strtype);
end
if j > 0  outd = strcat(outd,'_j',int2str(j)); end; 

%ukpervolt=get_ukpervolt;
ukpervolt = 1.0;
pure_b = 'normal';

for z = zmin:zmax+1,

  r = z-1;

  % setting filename to be read
  id = int2str(r);
  disp(id)
  switch r
  case 0
    readfile = 'real';
  case num2cell(1:9)
    readfile = strcat('00',id,int2str(type));
  case num2cell(10:99)
    readfile = strcat('0',id,int2str(type));
  case num2cell(100:499)
    readfile = strcat(id,int2str(type));
  end
  if r>0     readfile = strcat(root,readfile,sims); end; 
  if r==0    readfile = strcat(root,readfile,real); end; 
  if type==4 readfile = beam; end;

  % get the maps
  if type == 4 & data=='BK13'
    load(sprintf(readfile));
  else
   load(sprintf(readfile));
    map = make_map(ac,m,coaddopt);
    clear ac
  end
  if data=='BK14' %use 150GHz
    if (j == 0) map = map(2); end;
    if (j > 0)  map = map(2,[1,2]); end;
  end

  % cal the maps
  map = cal_coadd_maps(map,ukpervolt);

  % jackknife the maps
  if (j > 0) map = jackknife_map(map); end;

  % smooth the var maps to reduce mode mixing
  map = smooth_varmaps(m,map);

  % if pol ap mask is not already available make it
  maps{z} = add_masks(m,map);

  % save to file
  f{z} = strcat(outd,'/map_',int2str(z-1));
  if (r==0 & type~=4) f{z} = strcat(f{z},'_dp',int2str(dp)); end; 
  if (type==4)        f{z} = strcat(f{z},'_dp11020'); end; 
  write_QUmap_dat(maps{z},strcat(f{z},'_ap'),0,m.nx,m.ny);
  write_Tmap_dat(maps{z},strcat(f{z},'_T_ap'),m.nx,m.ny);

end


% matrix purification
if do_proj

  for k = 0:1

    % loading projection matrix
    disp('loading projection matrix');
    tic
    if k == 0
      if data=='BK13'  projdata = load(proj,'pb','obs_pixels','reob','m'); end; 
      if data=='BK14'  projdata = load(proj,'projmatopt','pb','obs_pixels','reob','m'); end;
    else
      if data=='BK13'  projdata = load(proj,'pe','obs_pixels','reob','m'); end; 
      if data=='BK14'  projdata = load(proj,'projmatopt','pe','obs_pixels','reob','m'); end;
    end
    toc

    % calc pure E/B-mode
    for z = zmin:zmax+1
      [mapE,mapB] = do_projection_tn(maps{z},projdata,k);
      if k == 0
        maps{z}.QprojB = mapB.Q;
        maps{z}.UprojB = mapB.U;
      else
        maps{z}.QprojE = mapE.Q;
        maps{z}.UprojE = mapE.U;
      end
    end

    clear projdata;

  end

  for z = zmin:zmax+1
    write_QUmap_dat(maps{z},f{z},1,m.nx,m.ny);
  end

end

