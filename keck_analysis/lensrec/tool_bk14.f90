!///////////////////////////////////////////////////////////////////////!
! * subroutines
! * Toshiya Namikawa
! - Last Modified: Sun 29 Nov 2015 08:20:09 PM EST
!///////////////////////////////////////////////////////////////////////!

module tool_bk14
  use myconst,     only: dlc, TT, EE, BB, TE, pi, Tcmb, iu
  use myutils,     only: savetxt, loadtxt, meanvar, str, linspace
  use mycls,       only: alm2bcl_flat, calcbcl_flat, binned_ells, cb2cl, cl_interp_lin
  use anaflat,     only: elarray, fourier_filter
  use myfftw,      only: DFT_POL, DFT, pureEB
  use recflat,     only: QUADEE, QUADEB, LensingB
  !use nldd_kernel, only: AlEE
  use nldd_flat,   only: AlEB_flat, TBEB_flat
  use bkutils,     only: readcl_bk, read_nkk_plk
  use recbk,       only: data2glm
  implicit none
  integer, parameter :: gEE=1, gEB=2, cEE=3, cEB=4, gMV=5, cMV=6

  private dlc, TT, EE, BB, TE, pi, Tcmb, iu
  private savetxt, loadtxt, meanvar, str, linspace
  private alm2bcl_flat, calcbcl_flat, cb2cl, cl_interp_lin
  private elarray, fourier_filter
  private DFT_POL, DFT, pureEB
  private quadee, quadeb, LensingB
  !private AlEE
  private aleb_flat, tbeb_flat
  private readcl_bk, read_nkk_plk
  private data2glm

contains


subroutine calc_norm(eL,rL,els,fC,OC2d,A2d,lB,lxcut)
  implicit none
  !I/O
  integer, intent(in) :: eL(2), rL(2), lB, lxcut
  double precision, intent(in) :: els(:), fC(:), OC2d(:,:)
  double precision, intent(out) :: A2d(:,:)
  !internal
  integer :: bmax = 150, l, i
  double precision, allocatable :: OCb(:,:), OC(:,:), bc(:), W1(:), W2(:), Al(:,:)

  !* computing Cl^EE and Cl^BB
  allocate(OCb(2,bmax),OC(2,eL(2)),bc(bmax));  OC = 0d0
  call binned_ells(eL,bc=bc)
  call calcbcl_flat(bmax,eL,els,OC2d(1,:),OCb(1,:))
  call calcbcl_flat(bmax,eL,els,OC2d(2,:),OCb(2,:))
  call cb2cl(bc,OCb(1,:),OC(1,:))
  call cb2cl(bc,OCb(2,:),OC(2,:))
  deallocate(OCb,bc)

  allocate(Al(6,eL(2)),W1(eL(2)),W2(eL(2))); W1=0d0; W2=0d0; Al=0d0

  !* Al^EE
  !CALL ALEE(rL,eL,Al(gEE,:),Al(cEE,:),fC,OC(1,:)) ! not used

  !* Al^EB
  do l = eL(1), eL(2)
    W1(l) = 1d0/OC(1,l)
    if(lB<l) W2(l) = 1d0/OC(2,l)
  end do
  call ALEB_FLAT(rL,eL,Al(gEB,:),Al(cEB,:),fC,W1,W2,200,lxcut=lxcut)

  !* Al^MV : not used
  do l = eL(1), eL(2)
    Al(:,l) = Al(:,l)*dble(l)**4/4d0
    Al(gMV,l) = 1d0 / (1d0/Al(gEE,l) + 1d0/Al(gEB,l))
    Al(cMV,l) = 1d0 / (1d0/Al(cEE,l) + 1d0/Al(cEB,l))
  end do

  do i = 1, 6
    A2d(i,:) = cl_interp_lin(els,Al(i,:),eL)
  end do

  deallocate(OC,W1,W2,Al)

end subroutine calc_norm


subroutine calc_bkxp15(eL,rL,els,f,OC2d,lB,lxcut,fnkk)
  implicit none
  !I/O
  character(*), intent(in) :: f, fnkk
  integer, intent(in) :: eL(2), rL(2), lB, lxcut
  double precision, intent(in) :: els(:), OC2d(:,:)
  !internal
  integer :: bmax = 150, l, n = 200
  double precision :: NT, NP
  double precision, dimension(:), allocatable :: bc, W1, W2, nkk
  double precision, dimension(:,:), allocatable :: OCb, OC, ls, tAl, LC, Al

  allocate(OCb(2,bmax),OC(2,eL(2)),bc(bmax));  OC = 0d0
  call binned_ells(eL,bc=bc)
  call calcbcl_flat(bmax,eL,els,OC2d(1,:),OCb(1,:))
  call calcbcl_flat(bmax,eL,els,OC2d(2,:),OCb(2,:))
  call cb2cl(bc,OCb(1,:),OC(1,:))
  call cb2cl(bc,OCb(2,:),OC(2,:))
  deallocate(OCb,bc)

  allocate(ls(1,eL(2)),tAl(2,eL(2)),W1(eL(2)),W2(eL(2))); ls=0d0;  tAl=0d0;  W1=0d0;  W2=0d0
  allocate(LC(4,eL(2)),Al(4,eL(2)));  LC=0d0;  Al=0d0
  ls(1,:) = linspace(1,eL(2),eL(2))
  call READCL_BK(f,eL,.true.,LC(TT,:),LC(EE,:),LC(BB,:),LC(TE,:))
  NT = (70d0*pi/10800d0/Tcmb)**2*0.5d0
  NP = (70d0*pi/10800d0/Tcmb)**2

  !* Al^EB,BK
  do l = eL(1), eL(2)
    W1(l) = 1d0/OC(1,l)
    if(lB<l) W2(l) = 1d0/OC(2,l)
  end do
  call ALEB_FLAT(rL,eL,tAl(1,:),tAl(2,:),LC(EE,:),W1,W2,n,lxcut=lxcut)

  !* EB^BK x EB^P15
  do l = eL(1), eL(2)
    W1(l) = LC(EE,l)/((LC(EE,l)+NP)*OC(1,l))
    W2(l) = LC(BB,l)/((LC(BB,l)+NP)*OC(2,l))
  end do
  call ALEB_FLAT(rL,eL,Al(1,:),Al(2,:),LC(EE,:),W1,W2,n,lxcut=lxcut)

  !* EB^BK x TB^P15
  do l = eL(1), eL(2)
    W1(l) = LC(TE,l)/((LC(TT,l)+NT)*OC(1,l))
    W2(l) = LC(BB,l)/((LC(BB,l)+NP)*OC(2,l))
  end do
  call TBEB_FLAT(rL,eL,Al(3,:),Al(4,:),LC(TE,:),LC(EE,:),W1,W2,n,lxcut=lxcut)

  !* combined
  allocate(nkk(eL(2))); nkk=0d0
  call read_nkk_plk(fnkk,nkk,eL)
  do l = eL(1), eL(2)
    Al(1:2,l) = nkk(l)*tAl(1:2,l)/Al(1:2,l)
    Al(3:4,l) = nkk(l)*tAl(1:2,l)*Al(3:4,l)
    tAl(1:2,l) = dsqrt(nkk(l)*tAl(1:2,l)*dble(l)**4/4d0)
  end do
  call savetxt("N0_BKP.dat",ls,Al,tAl)
  deallocate(OC,ls,tAl,Al,LC,nkk)

end subroutine calc_bkxp15


subroutine set_file(dir,dir_plk,j,dp,tag,nophi,falm,fmap,gmap)
  implicit none
  !I/O
  character(*), intent(in) :: dir, dir_plk, tag
  character(*), intent(out) :: falm(:,:), fmap(:,:), gmap(:,:)
  logical, intent(in) :: nophi
  integer, intent(in) :: j, dp
  !internal
  character(LEN=3) :: jack
  integer :: i

  !* set jackknife
  jack = ''
  if (j>0) jack = "_j"//str(j)

  !* real map
  falm(:,1) = trim(dir)//"alm_comb"//trim(jack)//"/"//trim(tag)//"_dp"//str(dp)//"_r0.dat"
  fmap(:,1) = trim(dir)//"map_lensed"//trim(jack)//"/map_0_dp"//str(dp)//".dat"

  !* sim maps
  do i = 2, 500
    falm(1,i) = trim(dir)//"alm_comb"//trim(jack)//"/"//trim(tag)//"_r"//str(i-1)//".dat"
    if (nophi) falm(1,i) = trim(dir)//"alm_nophi"//trim(jack)//"/"//trim(tag)//"_r"//str(i-1)//".dat"
    falm(2,i) = trim(dir)//"alm_lens"//trim(jack)//"/"//trim(tag)//"_r"//str(i-1)//".dat"
    falm(3,i) = trim(dir)//"alm_noise"//trim(jack)//"/"//trim(tag)//"_r"//str(i-1)//".dat"
    if (size(falm,dim=1)>3) falm(4,i) = trim(dir)//"alm_dust"//trim(jack)//"/"//trim(tag)//"_r"//str(i-1)//".dat"
    fmap(1,i) = trim(dir)//"map_lensed"//trim(jack)//"/map_"//str(i-1)//".dat"
    if (nophi) fmap(1,i) = trim(dir)//"map_unlensed"//trim(jack)//"/map_"//str(i-1)//".dat"
    fmap(2,i) = trim(dir)//"map_noise"//trim(jack)//"/map_"//str(i-1)//".dat"
    fmap(3,i) = trim(dir)//"map_dust"//trim(jack)//"/map_"//str(i-1)//".dat"
  end do

  do i = 1, 500
    gmap(1,i) = trim(dir)//"../../kappa/proj/n0512_l20-700/"//str(i-1)//".dat"
    gmap(2,i) = trim(dir_plk)//"derived/proj/rec_n2048_c/map_r"//str(i-1)//".dat"
  end do
  gmap(3,1) = trim(dir_plk)//"derived/proj/mask.dat"

end subroutine set_file


subroutine write_gmap(f,nn,D,els,eL,glm)
  implicit none
  !I/O
  character(*), intent(in) :: f
  integer, intent(in) :: nn(2), eL(2)
  double precision, intent(in) :: D(2), els(:)
  complex(dlc), intent(in) :: glm(:)
  !internal
  complex(dlc), dimension(:), allocatable :: klm

  allocate(klm(size(glm)));  klm=0d0
  klm = glm
  call fourier_filter(els,eL,klm)
  call DFT(klm,nn,D,-1)
  call savetxt(f,real(klm))
  deallocate(klm)

end subroutine write_gmap


subroutine write_gmaps(nn,D,eL,els,est,glm,klm,Wn,sig,i)
  implicit none 
  integer, intent(in) :: i, eL(2), nn(2)
  double precision, intent(in) :: D(2), els(:), Wn(:), sig(:,:)
  complex(dlc), intent(in) :: est(:), glm(:), klm(:)

  call write_gmap("grec"//str(i-1)//".dat",nn,D,els,eL,est/Wn(2)*sig(4,:))
  call write_gmap("gplk"//str(i-1)//".dat",nn,D,els,eL,glm/Wn(1)*sig(4,:))
  call write_gmap("gdif"//str(i-1)//".dat",nn,D,els,eL,(sig(1,:)*est/Wn(2)-sig(2,:)*glm/Wn(1))*sig(3,:)*sig(4,:))
  call write_gmap("gopt"//str(i-1)//".dat",nn,D,els,eL,(sig(1,:)*est/Wn(2)+sig(2,:)*glm/Wn(1))*sig(3,:)*sig(4,:))
  if (i==2) then
    call write_gmap("ginp.dat",nn,D,els,eL,klm/Wn(1)*sig(4,:))
    call write_gmap("gnlm.dat",nn,D,els,eL,(glm-klm)/Wn(1)*sig(4,:))
    call write_gmap("gdif.dat",nn,D,els,eL,(sig(1,:)*(est/Wn(2)-klm/Wn(1))-sig(2,:)*(glm-klm)/Wn(1))*sig(3,:)*sig(4,:))
  end if

end subroutine write_gmaps


end module tool_bk14

