
# * measured power spectrum
# - Last Modified: Wed 11 May 2016 09:47:36 PM EDT

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

# parameter
tag = '1var_lx30_l30-700_lB150-700'
bmax = 10
s = 1e6
dp = '_dp1102'
root = '/n/bicepfs3/users/namikawa/BK14/lensrec/derived/obs/'

#//// plot starting ////#
mpl.rcParams.update({'font.size': 16,'legend.fontsize':13})
plt.xlabel(r'Multipole $L$',fontsize=22)
c = ['r','b']

#//// set parameters ////#
y = [-0.5,1.5]
lab = [r'BK14 x Planck','BK14']
plt.ylabel(r'$C_L^{\kappa\kappa}\times 10^6$',fontsize=22)

#//// read cls ////#
plt.xlim(15,710)
plt.ylim(y)

# theoretical ckk and ratio
b, Ckk = np.loadtxt(root+'sXb_'+tag+'_b'+str(bmax)+'.dat',unpack=True,usecols=(0,3))

# observed cl
for i, X in enumerate(['X','C']): 
  f = X+'b_'+tag
  if X=='X':
    m = 1
    n = m + 5
  if X=='C':
    m = 2
    n = m + 8
  b, oCb, vCb = np.loadtxt(root+'/o'+f+dp+'_b'+str(bmax)+'.dat',unpack=True,usecols=(0,m,n))
  #b, sCb = np.loadtxt(root+'/s'+f+'_b'+str(bmax)+'.dat',unpack=True,usecols=(0,m,n))
  oCb = oCb*s;  vCb = vCb*s
  b = b + 7*i*np.ones(len(b))
  plt.errorbar(b,oCb,yerr=vCb,label=lab[i],fmt='o',lw=2,color=c[i])

lL, dd = np.loadtxt(root+'../cls/fid_BKsim.dat',usecols=(0,4)).T
plt.plot(lL,dd*s/4,'k-',label="Theoretical $C_L^{\kappa\kappa}$")
plt.plot([0,1000],[-0,0],'k--')
plt.legend(loc=0,numpoints=1,frameon=False)
plt.savefig('clkk.png',bbox_inches='tight')
plt.show()

