
import numpy as np
import matplotlib as mpl
import scipy.stats as st
from matplotlib.pyplot import *

# set parameters
tag  = '1var_lx30_l30-700_lB150-700'
bmax = 10
bn   = 10
simn = 100
histbn = 12
root = '/n/bicepfs3/users/namikawa/BK14/lensrec/BKxPLK/'
m    = 1
ocl  = np.loadtxt(root+'../derived/obs/oXb_'+tag+'_dp1102_b'+str(bmax)+'.dat',unpack=True)[m][:bn]

# read cls
cpp = np.array([np.loadtxt(root+'b'+str(bmax)+'_r'+str(i+1)+'_'+tag+'.dat',unpack=True)[m][:bn] for i in range(simn)])

# mean
mcl = np.array([np.mean(cpp[:,n]) for n in range(bn)])
amp = np.array([cpp[:,n]/mcl[n] for n in range(bn)]).T

# covariance
cov = np.cov(amp,rowvar=0)
cov[np.isnan(cov)] = 0
ic = np.linalg.inv(cov)
a = np.sum(ic,axis=0)

# amplitude estimator
A = np.zeros((simn))
for i in range(simn):
  neum = np.sum(a[:]*amp[i,:])
  deno = np.sum(a)
  A[i] = neum/deno

# observed amplitude
neum = np.sum(a[:]*ocl/mcl)
deno = np.sum(a)
obsA = neum/deno
m1A  = np.mean(A)
m2A  = np.var(A)**0.5
print obsA, m2A, obsA/m2A

# start plotting
mpl.rcParams.update({'font.size': 16,'legend.fontsize':13})
l = 4
xlim(0.,2.)
ylim(0,25.0)
hist(A,bins=histbn,normed=False,alpha=0.5,lw=0)
#y, x = np.histogram(A,bins=np.linspace(0.,2.,histbn*2))
#y = y/np.float(np.max(y))
#bar(x[:-1],y,x[1]-x[0])
xlabel(r'$A^{\phi\phi}_{\rm L}$')
ylabel('Number Count')
plot([obsA,obsA],[0,25],'k-')
#AL, pAL = np.loadtxt(root+'../derived/obs/BK14_lensing_p_Alens.dat',unpack=True)
#plot(AL,pAL)
figtext(0.60,0.75,r'$A_{\rm L}^{\phi\phi, {\rm obs}}$ ='+str(obsA)[:l])
savefig('hist_cross.png',bbox_inches='tight')
show()

