!///////////////////////////////////////////////////////////////////////!
! * Lensing analysis from Q/U maps
! * Toshiya Namikawa
! - Last Modified: Sun 29 Nov 2015 05:01:13 PM PST
!///////////////////////////////////////////////////////////////////////!

program main
  use readfile, only: SET_PARAMS_FILE, read_prm, read_str, read_log, read_int, read_dbl, read_val
  use myutils,  only: str, meanvar, savetxt, loadtxt, linspace
  use myconst,  only: dlc, pi, iu, twopi, Tcmb
  use mycls,    only: READCl_CAMB, alm2bcl_flat, binned_ells, calcbcl_flat, cl_interp_lin
  use anaflat,  only: elarray, window_generate, fourier_filter, array_extend, window_norm_multi
  use myfftw,   only: DFT, DFT_POL
  use recflat,  only: QUADEE, QUADEB, QUADEB_BHE, LensingB, alflat_ee, alflat_eb
  use bkutils,  only: set_defparams, readcl_bk, set_file_bkspt, delensed_cls, make_filter, write_cls, wiener_gfilter
  use recbk,    only: delensing_bias, n0_bias, rdn0_bias, mean_field, calc_transfer, map2alm, set_lfactor
  use tool_bkspt
  implicit none
  character(LEN=128) :: tag, bktag, tmptag, dir, gmap(3,500), falm(9,500), fmap(4,500), fcl(17)
  logical :: nophi, do_delensing, spt3g
  integer :: el(2), rL(2), dL(2), simn(2), nn(2), npix, mapn, i, j, k, l, n, r, bmax, lB, cln=8, bln=7, lxcut, lxbcut, bns
  double precision :: D(2), Wn(0:6,0:6), sP
  double precision, dimension(:), allocatable :: els, W2, lfac, bp, elx
  double precision, dimension(:,:), allocatable :: Fl, f2d, Il, A2d, N0, RDN0, RDN0_1D, Gl, W1, C1d, C2d, bc, Tl, sig, Al, f2dbk, Bl, FF
  double precision, dimension(:,:,:), allocatable :: Cb, Bb, Yb
  complex(dlc), dimension(:), allocatable :: glm, klm, dlm
  complex(dlc), dimension(:,:), allocatable :: alm, est, mest, wlm, blm, llm, bkalm

  call SET_PARAMS_FILE

  !* variables
  dir   = read_str("dir")

  mapn  = read_int('mapnum')
  lB    = read_int("lB")
  lxcut = read_int("lxcut")
  lxbcut = read_int("lxbcut")
  bmax  = read_int("bmax")
  tag   = "chi_lx"//str(lxcut)//"_lxb"//str(lxbcut)
  bktag = "chi_lx"//str(lxcut)
  nophi = read_log("nophi")
  spt3g = read_log("spt3g")
  do_delensing = read_log("do_delensing")
  if (spt3g) tag = trim(tag)//"_3g"
  if (nophi) tag = trim(tag)//"_nophi"

  call read_prm('simn',simn)
  call set_defparams(nn,npix,D,r=read_int("resol"))

  !* multipoles
  call read_prm("rL",rL)
  call read_prm("dL",dL)
  allocate(bp(bmax+1),bc(1,bmax))
  call BINNED_ELLS(dL,bp,bc(1,:))
  eL(1) = min(rL(1),dL(1))
  eL(2) = max(rL(2),dL(2))
  allocate(els(npix),lfac(npix),elx(npix),FF(2,npix));  els=0d0;  lfac=0d0;  elx=0d0;  FF=0d0
  call set_lfactor(nn,D,dL,els,lfac,elx)
  do n = 1, npix
    if (eL(1)<=els(n).and.els(n)<=eL(2).and.abs(elx(n))>=lxcut) FF(1,n)=1d0
    if (max(eL(1),lB)<=els(n).and.els(n)<=eL(2).and.abs(elx(n))>=lxcut.and.abs(elx(n))<=lxbcut) FF(2,n)=1d0
  end do
  deallocate(elx)

  !* file name
  call set_file_bkspt(dir,tag,bktag,trim(tag)//"_l"//str(rL(1))//"-"//str(rL(2))//"_lB"//str(lB),bmax,falm,fmap,gmap,fcl)

  !* read theoretical CMB power spectrum for filtering
  allocate(C1d(3,eL(2)),C2d(3,npix))
  call READCL_BK(read_str("clee"),eL,.true.,CEE=C1d(1,:),CBB=C1d(2,:))
  call READCL_BK(read_str("clkk"),eL,.false.,Ckk=C1d(3,:))
  do i = 1, 3
    C2d(i,:) = cl_interp_lin(els,C1d(i,:),eL)
  end do

  !* window apodization
  allocate(W1(2,npix),W2(23600))
  call WINDOW_GENERATE(nn,D,W1(1,:),ap=read_dbl("afac"),cut=read_dbl("cutf"))
  !call loadtxt(gmap(3,3),W)
  !W2 = W2/(sum(W2)/dble(23600))
  !call array_fine([236,100],[3,3],W2,W1(2,:))
  W1(2,:) = W1(1,:)
  call window_norm_multi(W1(1,:),W1(2,:),Wn)

  !* alm and optimal diagonal filter
  allocate(Fl(2,npix),f2d(2,npix),Il(2,npix),f2dbk(2,23600));  Fl=0d0; f2d=0d0; Il=0d0; f2dbk=0d0
  if (read_log("do_filt")) call map2alm(fmap,falm,nn,D,FF,W1(1,:),C2d(1:2,:),"smith",[fcl(1),fcl(16)],spt3g,sim=51)
  if (spt3g)  sP = dsqrt(0.1d0)
  call make_filter(fcl(1),nn,els,Fl=Fl,f2d=f2d,Il=Il,sPin=sP)
  call make_filter(fcl(2),[236,100],els,f2d=f2dbk)

  !//// main calculation ////

  !1) Analytic Normalization
  allocate(A2d(6,npix),Al(7,50));  A2d=0d0;  Al=0d0
  call ALFLAT_EE(nn,D,Il(1,:),C2d(1,:),rL,eL,A2d(gEE,:),A2d(cEE,:))
  call ALFLAT_EB(nn,D,Il(1,:),Il(2,:),C2d(1,:),rL,eL,A2d(gEB,:),A2d(cEB,:))
  !---- check ----
  do i = 1, 4
    A2d(i,:) = A2d(i,:)*els**4/4d0
    call CALCBCl_FLAT(50,eL,els,A2d(i,:),Al(i,:))
  end do
  Al(gMV,:) = 1d0/(1d0/Al(gEE,:)+1d0/Al(gEB,:))
  Al(cMV,:) = 1d0/(1d0/Al(cEE,:)+1d0/Al(cEB,:))
  Al(7,:) = eL(1)+(eL(2)-eL(1))*linspace(1,50,50)/50d0
  call savetxt(fcl(3),Al(7:7,:),Al(1:6,:))
  !---------------
  deallocate(Al)

  !2) Transfer function
  write(*,*) "transfer"
  allocate(Tl(3,npix),Gl(2,npix));  Tl=1d0; Gl=0d0
  Gl(1,:) = lfac*A2d(1,:)
  Gl(2,:) = lfac*A2d(2,:)
  if(read_log("do_trans")) then
    if (.not.spt3g)  bns=20
    call calc_transfer(falm(1,:),gmap(1,:),nn,D,rL,dL,W1(1,:),C2d(1,:),els,Fl,Gl,Tl,fcl(4:5),sim=50,bns=bns)
  else
    call loadtxt(fcl(5),Tl)
  end if
  A2d(1:2,:) = Tl(1:2,:)*A2d(1:2,:)
  do n = 1, npix
    if(A2d(1,n)/=0d0) A2d(gMV,n) = 1d0/(1d0/A2d(gEE,n)+1d0/A2d(gEB,n))
    if(A2d(3,n)/=0d0) A2d(cMV,n) = 1d0/(1d0/A2d(cEE,n)+1d0/A2d(cEB,n))
  end do
  deallocate(Il,W1,W2,Gl)

  !* gmap wiener filtering
  write(*,*) "gmap wiener filter"
  allocate(sig(3,npix));  sig=0d0
  call wiener_gfilter(eL,els,A2d,sig,C2d(3,:))

  !3) Expected N0 bias
  write(*,*) "N0"
  allocate(N0(6,npix));  N0 = 1d0
  if (read_log("do_norm")) then
    call n0_bias(falm(1,:),25,nn,D,rL,C2d(1,:),N0,1d0/Wn(4,0),lfac,Fl,fcl(6))
  else
    call loadtxt(fcl(6),N0(1:4,:))
    N0(gMV,:) = N0(gEE,:)+N0(gEB,:)
    N0(cMV,:) = N0(cEE,:)+N0(cEB,:)
  end if

  !4) Computing Mean Field Bias
  write(*,*) "mean field"
  allocate(mest(4,npix));  mest = 0d0
  if (read_log("do_mean")) then
    call mean_field(falm(1,:),50,nn,D,rL,C2d(1,:),mest,lfac,Fl,fcl(7))
  else
    call loadtxt(fcl(7),mest)
  end if

  !5) Optimal filtering
  !call calc_optfilt(falm(1,:),gmap,nn,D,rL,W,CE1d,els,lfac,theta,A2d,mest(2,:),Tl,Fl,sig)

  !6) Delensing bias
  if (simn(2)>=1) then
    do i = 2, mapn
      write(*,*) i, "read Elm and Blm"
      allocate(alm(2,npix),dlm(npix)); alm=0d0
      call loadtxt(falm(1,i),alm)
      call delensing_bias(i,simn(2),alm(2,:),falm(1,:),nn,D,rL,dL,els,C2d(1,:),dlm,Tcmb,Fl,sig(2,:)*lfac**2*A2d(gEB,:))
      call savetxt(falm(7,i),dlm)
      deallocate(alm,dlm)
    end do
  end if

  !7) delensing filter
  write(*,*) "delensing filter"
  do n = 1, 23600
    if (f2dbk(2,n)/=0d0)  f2dbk(2,n) = Tcmb/f2dbk(2,n)
  end do
  allocate(Gl(2,npix),Bl(2,npix))
  Gl(1,:) = sig(1,:)*lfac
  Gl(2,:) = sig(2,:)*lfac
  if (read_log("do_dfl")) then
    call delensing_filter_bkspt(falm,fcl(14:15),dL,rL,nn,D,C2d(1,:),Fl,f2dbk(2,:),Gl,A2d,mest,Bl,FF(2,:))
  else
    call loadtxt(fcl(15),Bl)
  end if

  !8) Performing Reconstruction / Delensing and Power Spectrum Calculation
  allocate(Cb(mapn,cln,bmax),Bb(mapn,bln,bmax),Yb(mapn,bln,bmax));  Cb = 0d0; Bb=0d0; Yb=0d0

  write(*,*) "reconstruction / delensing"
  do i = 2, mapn

    write(*,*) i, "read Elm and Blm"
    allocate(alm(2,npix),wlm(3,npix)); alm=0d0; wlm=0d0
    call loadtxt(falm(1,i),alm)

    !* wiener filtering for delensing
    allocate(dlm(npix))
    if (do_delensing) then
      do n = 1, npix
        if (dL(1)<=els(n).and.els(n)<=dL(2))  wlm(1,n) = alm(1,n) * Fl(1,n) * C2d(1,n) * Tcmb
      end do
      allocate(bkalm(4,23600))
      call loadtxt(falm(4,i),bkalm(1:2,:))
      call loadtxt(falm(5,i),bkalm(3:4,:))
      call array_extend([236,100],[3,3],f2dbk(2,:)*bkalm(2,:),wlm(2,:))
      call array_extend([236,100],[3,3],f2dbk(2,:)*bkalm(4,:),wlm(3,:))
      deallocate(bkalm)
      call loadtxt(falm(7,i),dlm) !* delensing bias
    end if

    !* C^-1 filtering for lensing reconstruction
    alm = alm * Fl

    !* compute unnormalized estimators
    allocate(est(6,npix));  est = 0d0
    CALL QUADEE(nn,D,alm(1,:),alm(1,:),C2d(1,:),rL,est(gEE,:),est(cEE,:))
    CALL QUADEB(nn,D,alm(1,:),alm(2,:),C2d(1,:),rL,est(gEB,:),est(cEB,:))

    !* to corrected unnormalized estimators
    do j = 1, 4
      est(j,:) = ( est(j,:)*lfac - mest(j,:) )
    end do
    est(gMV,:) = est(gEE,:) + est(gEB,:)
    est(cMV,:) = est(cEE,:) + est(cEB,:)
    do j = 1, 6
      est(j,:) = est(j,:) * A2d(j,:)
    end do

    !//// auto-power spectrum ////!

    !* RDN0-bias estimate
    allocate(RDN0(6,npix));  RDN0 = 0d0
    RDN0 = N0(1:6,:)
    deallocate(alm)

    allocate(RDN0_1D(6,bmax));  RDN0_1D=0d0
    do k = 1, 6
      call CALCBCl_flat(bmax,dL,els,RDN0(k,:)*A2d(k,:)**2,RDN0_1D(k,:))
    end do
    !RDN0_1D(gMV,:) = RDN0_1D(gEE,:) + RDN0_1D(gEB,:)
    !RDN0_1D(cMV,:) = RDN0_1D(cEE,:) + RDN0_1D(cEB,:)
    deallocate(RDN0)

    !* binned power spectrum
    do j = 1, 6
      call alm2bcl_flat(bmax,dL,els,D,est(j,:),Cb=Cb(i,j,:),norm=1d0/Wn(4,0))
      Cb(i,j,:) = Cb(i,j,:) - RDN0_1D(j,:)
    end do
    deallocate(RDN0_1D)
    call alm2bcl_flat(bmax,dL,els,D,est(gEE,:),est(gEB,:),Cb=Cb(i,7,:),norm=1d0/Wn(4,0))
    call alm2bcl_flat(bmax,dL,els,D,est(cEE,:),est(cEB,:),Cb=Cb(i,8,:),norm=1d0/Wn(4,0))
    if(i>=2) call savetxt(falm(8,i),bc,Cb(i,1:8,:))

    !* lensing B-modes
    allocate(blm(2,npix));  blm=0d0
    if (do_delensing) then
      allocate(alm(3,npix));  alm=0d0
      alm(1:2,:) = est(1:2,:)
      alm(3,:) = est(gMV,:)
      call DFT(alm(1,:),nn,D,-1)
      call DFT(alm(2,:),nn,D,-1)
      call DFT(alm(3,:),nn,D,-1)
      call savetxt(falm(9,i),dble(alm(1,:)),dble(alm(2,:)),dble(alm(3,:)),sig(1,:),sig(2,:),sig(3,:))
      deallocate(alm)
      est(gEE,:) = est(gEE,:)*lfac*sig(1,:)
      est(gEB,:) = est(gEB,:)*lfac*sig(2,:)
      call LensingB(nn,D,wlm(1,:),est(gEE,:),dL,blm(1,:))
      call LensingB(nn,D,wlm(1,:),est(gEB,:),dL,blm(2,:))
      blm(2,:) = FF(2,:)*(blm(2,:)-dlm)
      !call delensed_cls(bmax,dL,els,D,wlm(2,:),blm,Bb(i,:,:),Wn(1:6,0),Bl)
      call delensed_cls(bmax,dL,els,D,wlm(3,:),blm,Yb(i,:,:),Wn(1:6,0),Bl)
      if (i==2) then
        do n = 1, npix
          if(20>els(n).or.els(n)>100) then
            blm(1,n) = 0d0
            blm(2,n) = 0d0
            wlm(3,n) = 0d0
          end if
        end do
        call DFT(blm(1,:),nn,D,-1)
        call DFT(blm(2,:),nn,D,-1)
        call DFT(wlm(3,:),nn,D,-1)
        call savetxt("test1.dat",real(blm(1,:))/Wn(3,0))
        call savetxt("test2.dat",real(blm(2,:))/Wn(3,0))
        call savetxt("test3.dat",real(wlm(3,:))/Wn(0,1))
      end if
    end if
    deallocate(blm,wlm,est,dlm)

  end do

  deallocate(C1d,C2d,Fl,lfac,mest,A2d,N0,Tl,els,f2d,f2dbk,Gl,Bl)

  !//// output realization average ////!
  !* auto-power spectrum
  call write_cls(Cb,bc,fcl(8),fcl(9))
  if (do_delensing) then
    call write_cls(Bb,bc,fcl(10),fcl(11))
    call write_cls(Yb,bc,fcl(12),fcl(13))
  end if
  deallocate(Cb,Bb,Yb,bp,bc)

end program main

