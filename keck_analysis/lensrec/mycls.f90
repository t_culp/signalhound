!////////////////////////////////////////////////////!
! * Angular power spectrum calculation
! * Toshiya Namikawa
! - Last Modified : Sat 14 May 2016 05:29:58 PM EDT
!////////////////////////////////////////////////////!

module mycls
  use myconst, only: dlc, pi, TT, TE, EE, BB, dd, Td, Ed, oo
  use myutils, only: splint, spline, FileColumns, FileLines, savetxt, linspace, interp_lin
  implicit none

  private dlc, pi, TT, TE, EE, BB, dd, Td, Ed, oo
  private splint, spline, FileColumns, FileLines, savetxt, linspace, interp_lin

contains


subroutine binned_ells(eL,bp,bc,IsLog)
! * return binned multipole edges and centers
  implicit none
  !I/O
  logical, intent(in), optional :: IsLog
  integer, intent(in) :: eL(:)
  double precision, intent(out), optional :: bp(:), bc(:)
  !internal
  integer :: i, n
  double precision :: d
  double precision, allocatable :: p(:)

  if (present(bc)) n = size(bc)
  if (present(bp)) n = size(bp) - 1

  allocate(p(n+1))
  p(1) = eL(1)

  do i = 2, n+1
    if (present(IsLog)) then
      d = log(dble(eL(2))/dble(eL(1)))/dble(n)
      p(i) = eL(1)*exp(d*(i-1))
    else
      d = dble(eL(2)-eL(1))/dble(n)
      p(i) = d*(i-1) + p(1)
    end if
    if (present(bc)) bc(i-1) = (p(i)+p(i-1))*0.5d0
  end do

  if (present(bp)) bp = p
 
  deallocate(p)

end subroutine binned_ells


subroutine cl2cb(bc,Cl,Cb,f)
! * compute a simple binned Cl from non-binned Cl
  implicit none
  !I/O
  character(*), intent(in), optional :: f
  double precision, intent(in) :: Cl(:), bc(:)
  double precision, intent(out) :: Cb(:)
  !internal
  integer :: i

  Cb = 0d0
  do i = 1, size(bc)
    Cb(i) = Cl(int(bc(i)))
  end do

  if (present(f)) call savetxt(f,Cb)

end subroutine cl2cb


subroutine cb2cl(bc,Cb,Cl,f,bp)
! * compute a simple non-binned Cl from binned Cl
  implicit none
  !I/O
  character(*), intent(in), optional :: f
  double precision, intent(in) :: Cb(:), bc(:)
  double precision, intent(in), optional :: bp(:)
  double precision, intent(out) :: Cl(:)
  !internal
  integer :: bmax, i, j
  double precision :: y2(size(Cb))

  bmax = size(Cb)

  if (present(bp)) then
    do i = 1, bmax
      do j = int(bp(i)), int(bp(i+1))
        !Cl(j) = interp_lin(dble(j),bp(i),bp(i+1),Cb(i),Cb(i+1))
        Cl(j) = Cb(i)
      end do
    end do
  else
    call spline(bc,Cb,bmax,0d0,0d0,y2)
    Cl = 0d0
    do j = 2, size(Cl)
      Cl(j) = splint(dble(j),bc,Cb,y2)
    end do
  end if

  if (present(f)) call savetxt(f,Cl)

end subroutine cb2cl


subroutine cb2c2d(bc,els,eL,Cb,C2d)
  implicit none
  integer, intent(in) :: eL(2)
  double precision, intent(in) :: bc(:), els(:), Cb(:)
  double precision, intent(out) :: C2d(:)
  double precision, allocatable :: Cl(:)

  allocate(Cl(eL(2))) 
  Cl = 0d0
  call cb2cl(bc,Cb,Cl)
  C2d = cl_interp_lin(els,Cl,eL)
  deallocate(Cl)

end subroutine cb2c2d


function cl_interp_lin(els,Cl,eL) result(icl)
!* Linear interpolation for Cl
! do not use function in do loop for optimazation
  implicit none
  !I/O
  integer, intent(in) :: eL(:)
  double precision, intent(in) :: els(:), Cl(:)
  double precision :: icl(size(els))
  !internal
  integer :: n, l0, l1

  icl = 0d0
  do n = 1, size(els)
    if(eL(1)<=els(n).and.els(n)<=eL(2)) then
      l0 = int(els(n))
      l1 = l0 + 1
      icl(n) = Cl(l0) + (els(n)-l0)*(Cl(l1)-Cl(l0))
      if(icl(n)<0d0) then
        write(*,*) Cl(l0), Cl(l1), l0, els(n)
        stop "error (cl_interp_lin): interpolated Cl is negative"
      end if
    end if
  end do

end function cl_interp_lin


subroutine cl_interp_spline(bls,bCl,tL,Cl,islog)
  implicit none
  !I/O
  logical, intent(in) :: islog 
  integer, intent(in) :: tL(:)
  double precision, intent(in) :: bls(:), bCl(:)
  double precision, intent(out) :: Cl(:)
  !internal
  integer :: l, n, i, nmax
  double precision, allocatable :: tbCl(:), tbls(:), tbCldd(:)

  !choose non-zero bCls
  n = 0
  do i = 1, size(bCl)
    if(bCl(i)>0) n = n + 1
  end do
  nmax = n

  allocate(tbCl(nmax),tbls(nmax),tbCldd(nmax))
  n = 0
  do i = 1, size(bCl)
    if(bCl(i)>0) then 
      n = n + 1
      tbCl(n) = bCl(i)
      tbls(n) = bls(i)
    end if
  end do

  Cl = 0d0
  call spline(tbls,tbCl,nmax,0d0,0d0,tbCldd)
  do l = tL(1), tL(2)
    if(islog) then 
      Cl(l) = splint(dlog(dble(l)),tbls,tbCl,tbCldd)
    else 
      Cl(l) = splint(dble(l),tbls,tbCl,tbCldd)
    end if
  end do

  deallocate(tbCl,tbls)

end subroutine cl_interp_spline


subroutine alm2bcl_flat(bmax,el,els,D,alm1,alm2,Cb,Vb,norm,f)
!* computing binned cl from alm(s). 
  implicit none
  character(*), intent(in), optional :: f
  integer, intent(in) :: bmax, el(1:2)
  double precision, intent(in) :: D(1:2), els(:)
  double precision, intent(in), optional :: norm
  double precision, intent(out), optional :: Cb(:), Vb(:)
  complex(dlc), intent(in) :: alm1(:)
  complex(dlc), intent(in), optional :: alm2(:)
  !internal
  integer :: npix
  double precision, allocatable :: C(:)

  npix = size(alm1)
  allocate(C(npix))
  if(present(alm2)) then
    C = (alm1*conjg(alm2)+alm2*conjg(alm1))*0.5d0/(D(1)*D(2)) 
  else
    C = abs(alm1)**2/(D(1)*D(2))
  end if

  if(present(norm)) C = C*norm

  if(present(Cb)) then
    if(present(Vb)) then
      if(present(f)) then
        call CALCBCl_FLAT(bmax,el,els,C,Cb,Vb,f=f)
      else
        call CALCBCl_FLAT(bmax,el,els,C,Cb,Vb)
      end if
    else
      if(present(f)) then
        call CALCBCl_FLAT(bmax,el,els,C,Cb,f=f)
      else
        call CALCBCl_FLAT(bmax,el,els,C,Cb)
      end if
    end if
  else
    if(present(f)) call CALCBCl_FLAT(bmax,el,els,C,f=f)
  end if

  deallocate(C)

end subroutine alm2bcl_flat



subroutine CALCBCl_FLAT(bmax,el,els,Cl,Cb,Vb,tCl,tVl,f)
! * calculate binned cls 
  implicit none
  !I/O
  character(*), intent(in), optional :: f
  integer, intent(in) :: el(2), bmax
  double precision, intent(in) :: els(:), Cl(:)
  double precision, intent(in), optional :: tCl(:), tVl(:)
  double precision, intent(out), optional :: Cb(:), Vb(:)
  !internal
  integer :: i, nmax
  double precision :: bp(bmax+1), b(bmax)
  double precision, allocatable, dimension(:) :: AL, BL, Ab, vAb, iCl, iVl

  if (present(Cb)) then
    if (.not.size(Cb)==bmax) stop "error (CALCBCL_FLAT): size(Cb) is not equal to bmax"
  end if

  call BINNED_ELLS(el,bp,b)
  nmax = size(els,dim=1)
  allocate(AL(nmax),BL(nmax),Ab(bmax),vAb(bmax),iCl(nmax),iVl(nmax))

  iCl = 1d0
  iVl = 1d0
  !* interpolate theoretical values at each els(i)
  if(present(tCl)) iCl = cl_interp_lin(els,tCl,el)
  if(present(tVl)) iVl = cl_interp_lin(els,tVl,el)
  !* transfer to the amplitude parameter and its variance
  AL = 0d0
  BL = 0d0
  do i = 1, nmax
    if(els(i)>=el(1).and.els(i)<el(2)) then
      if(iCl(i)<=0d0) stop "error (calcbcl_flat): iCl"
      if(iVl(i)<=0d0) stop "error (calcbcl_flat): iVl"
      AL(i) = Cl(i)/iCl(i)
      BL(i) = 1d0
      if(present(tVl)) BL(i) = iCl(i)**2/iVl(i)**2
    end if
  end do
  call POWER_BINNING_FLAT(bp,els,AL,BL,Ab,vAb)
  deallocate(AL,BL,iCl,iVl)

  if(present(tCl)) then
    !* transfer to Cls
    Ab = Ab*cl_interp_lin(b,tCl,el)
    vAb = vAb*cl_interp_lin(b,tCl,el)
  end if

  if(present(Cb)) Cb = Ab
  if(present(Vb)) Vb = vAb
  if(present(f))  call savetxt(f,b,Ab,vAb)

  deallocate(Ab,vAb)

end subroutine CALCBCL_FLAT


subroutine POWER_BINNING_FLAT(bp,els,AL,BL,bA,bV)
!* compute averaging factor: 
!   bA = bV^2 * \sum_{l=bp(i)}^{bp(i+1)} BL(l)*AL(l)
! AL: amplitude
! BL: band-pass filter
!
  implicit none
!
! [input]
!   bp  --- edges of multipole binning
!   els --- multipoles as a function of npix
!   AL  --- amplitude
!   BL  --- band-pass filter
  double precision, intent(in) :: bp(:), els(:), AL(:), BL(:)
!
! [output]
!   bA  --- binned amplitude
!   bV  --- binned variance
  double precision, intent(out) :: bA(:), bV(:)
!
! [internal]
  integer :: b, i, npix, bmax
  double precision, allocatable :: bW(:)

  npix = size(els)
  bmax = size(bp) - 1

  allocate(bW(bmax))
  !* bW = [ sum_i BL(i) ]^-1 = bV^2 : optimal variance at b-th bin
  bW = 0d0;  bA = 0d0;  bV = 0d0
  do b = 1, bmax
    do i = 1, npix
      if (els(i)>=bp(b).and.els(i)<bp(b+1)) then
        bW(b) = bW(b) + BL(i) 
        bA(b) = bA(b) + BL(i)*Al(i)
      end if
    end do
    if (bW(b) > 0d0) then
      bW(b) = 1d0/bW(b) !square of variance
      bA(b) = bA(b)*bW(b)
      bV(b) = dsqrt(bW(b))
    end if
  end do

  deallocate(bW)

end subroutine POWER_BINNING_FLAT


subroutine POWER_BINNING(bp,Al,vAl,bA,bV,els,eL)
  implicit none 
  !I/O
  integer, intent(in), optional :: eL(2)
  double precision, intent(in) :: bp(:), Al(:), vAl(:)
  double precision, intent(in), optional :: els(:)
  double precision, intent(out) :: bA(:), bV(:)
  !internal 
  integer :: b, i, bmax
  double precision, allocatable :: bW(:)

  bmax = size(bp) - 1

  allocate(bW(bmax))
  !* compute optimal variance of b-th bin, bW(b)
  bW = 0.d0; bA = 0.d0; bV = 0.d0
  do b = 1, bmax
    if(present(els)) then
      do i = 1, size(els)
        if(els(i)>=bp(b).and.els(i)<bp(b+1)) then
          bW(b) = bW(b) + vAl(i) 
          bA(b) = bA(b) + vAl(i)*Al(i)*bW(b)
        end if
      end do
    else if(present(el)) then
      do i = el(1), el(2)
        if(i>=bp(b).and.i<bp(b+1)) then
          bW(b) = bW(b) + vAl(i)
          bA(b) = bA(b) + vAl(i)*Al(i)
        end if
      end do
    end if
    if(bW(b)>0.d0) then
      bW(b) = 1.d0/bW(b)
      bA(b) = bA(b)*BW(b)
      bV(b) = dsqrt(bW(b))
    end if
  end do

end subroutine POWER_BINNING


subroutine POWER_LABELING(el,els,nmax,label)
  implicit none 
  !I/O
  integer, intent(in) :: el(2)
  double precision, intent(in) :: els(:)
  integer, intent(out) :: nmax, label(:)
  !internal
  integer :: npix, m, n, indep

  npix = size(els)
  nmax = 0

  do n = 1, npix
    indep=-1
    if(els(n)>=el(1).and.els(n)<el(2)) then 
      do m = 1, n-1
        if(els(n)==els(m)) then
          indep = m
          go to 20
        end if
      end do
20    if(indep==-1) then 
        nmax = nmax + 1
        label(n) = nmax 
      else 
        label(n) = label(indep)
      end if
    else 
      label(n) = 0
    end if
  end do

end subroutine POWER_LABELING


subroutine cl_elcut(nn,D,Cl) !added Apr 17, 2015
!assign zeros to Cls outsize of the largest annuli
  implicit none
  !I/O
  integer, intent(in) :: nn(1:2)
  double precision, intent(in) :: D(1:2)
  double precision, intent(inout) :: Cl(:)
  !internal
  integer :: i, j, n
  double precision :: x, y, l, lmax

  lmax = 2*pi * min( dble(nn(1)*0.5d0-1)/D(1), dble(nn(2)*0.5d0-1)/D(2))
  n = 1
  do i = 1, nn(1)
    x = 2*pi*dble(i-1-nn(1)*0.5d0)/D(1)
    do j = 1, nn(2)
      y = 2*pi*dble(j-1-nn(2)*0.5d0)/D(2)
      l = dsqrt(x**2+y**2)
      if(l>lmax) Cl(n) = 0.d0
      n = n + 1
    end do
  end do

end subroutine cl_elcut


subroutine CALCCL_FLAT(D,alm1,alm2,Cl,els,f)
  !compute power spectrum from Fourier grids: C_ell
  implicit none
  !I/O
  character(*), intent(in), optional :: f
  double precision, intent(in) :: D(2)
  double precision, intent(in), optional :: els(:)
  double precision, intent(out), optional :: Cl(:)
  complex(dlc), intent(in) :: alm1(:), alm2(:)
  !internal
  integer :: i, npix
  double precision, allocatable :: C(:)

  npix = size(alm1)
  allocate(C(npix))
  C = (alm1*conjg(alm2)+alm2*conjg(alm1))*0.5d0/(D(1)*D(2))  ! divided by delta(l=0)
  if(present(Cl)) Cl = C
  if(present(f).and.present(els))  call savetxt(f,els,C)
  deallocate(C)

end subroutine CALCCL_FLAT


subroutine OUTPUT_CL(f,fname,min)
  implicit none
  !I/O
  character(*), intent(in) :: fname
  integer, intent(in), optional :: min
  double precision, intent(in) :: f(:)
  !internal
  integer :: n, nmax, nmin

  if(present(min)) then
    nmin = min
  else
    nmin = 1
  end if
  nmax = size(f,dim=1)
  open(unit=20,file=trim(fname),status="replace")
  do n = nmin, nmax
    write(20,"(I6,1X,E12.5)") n, f(n)
  end do
  close(20)

end subroutine OUTPUT_CL


subroutine READCL_CAMB(Cl,f,el,HasBB,rowCl)
  implicit none 
  !I/O
  character(*), intent(in) :: f
  logical, intent(in), optional :: HasBB, rowCl
  integer, intent(in) :: eL(1:2)
  double precision, intent(out) :: Cl(:,:)
  !internal
  double precision :: rC(1:10)
  integer :: i, l, n, m

  open(unit=20,file=trim(f),status="old")
  n = FileColumns(20)
  m = FileLines(20)
  do i = 1, m
    read(20,*) l, rC(1:n-1)
    if(.not.present(rowCl))  rC = rC*2d0*pi/dble(l**2+l)
    if(el(1)<=l.and.l<=el(2)) then 
      CL(TT,l) = rC(1)
      CL(EE,l) = rC(2)
      if(present(HasBB).and.HasBB.eqv..true.) then
        CL(BB,l) = rC(3)
        CL(TE,l) = rC(4)
        if(n>=6) CL(oo,l) = rC(5)
      else
        CL(TE,l) = rC(3)
        CL(dd,l) = rC(4)
        CL(Td,l) = rC(5)
        if(n>=7) CL(Ed,l) = rC(6)
      end if
    end if
  end do
  close(20)

end subroutine READCL_CAMB


end module mycls


