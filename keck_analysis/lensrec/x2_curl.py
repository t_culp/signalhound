# * Computing chi^2
# - Last Modified: Thu 14 Apr 2016 03:52:41 PM EDT

import numpy as np

bmax = 10
bn   = 10
dir  = '/n/bicepfs3/users/namikawa/BK14/lensrec/'
tag  = '1var_lx30_l30-700_lB150-700'

for X in ['kc','Kc','cc']:
  if X=='Kc':
    simn = 100
    root = dir+'BKxPLK/'
    oclf = dir+'derived/obs/oXb_'+tag+'_dp1102_b'+str(bmax)+'.dat'
    m = 2
  else:
    simn = 499
    root = dir+'clkk/'
    oclf = dir+'derived/obs/oCb_'+tag+'_dp1102_b'+str(bmax)+'.dat' 
    if X=='kc':  m = 7 # kappa x curl
    if X=='cc':  m = 4 # curl x curl

  # simulated cls
  ocl = np.loadtxt(oclf,unpack=True)[m][:bn]   # observed cl
  cpp = np.array([np.loadtxt(root+'b'+str(bmax)+'_r'+str(i+1)+'_'+tag+'.dat',unpack=True)[m][:bn] for i in range(simn)])
  mcl = np.mean(cpp,axis=0)
  cpp = cpp - mcl

  # covariance
  cov = np.cov(cpp,rowvar=0)
  ic  = np.linalg.inv(cov)

  # observed X^2
  oX2 = np.dot((ocl-mcl),np.dot(ic,(ocl-mcl)))

  # chi^2 for each realization
  sX2 = np.zeros((simn))
  for i in range(simn):
    cov = np.cov(np.delete(cpp,i,0),rowvar=0)
    ic  = np.linalg.inv(cov)
    sX2[i] = np.dot(cpp[i,:],np.dot(ic,cpp[i,:]))

  print 'type =', X, 'PTE =', (sX2>oX2).sum()/np.float(simn), 'chi^2 =', oX2

