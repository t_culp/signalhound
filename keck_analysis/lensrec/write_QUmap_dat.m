function write_QUmap_dat(map,fname,do_proj,nx,ny)

if do_proj
  QB = reshape(full(map.QprojB),nx*ny,1);
  UB = reshape(full(map.UprojB),nx*ny,1);
  QE = reshape(full(map.QprojE),nx*ny,1);
  UE = reshape(full(map.UprojE),nx*ny,1);
  QB(isnan(QB)) = 0;
  UB(isnan(UB)) = 0;
  QE(isnan(QE)) = 0;
  UE(isnan(UE)) = 0;
  fID = fopen(strcat(fname,'.dat'),'w');
  for i = 1:nx*ny
    fprintf(fID,'%12.5E %12.5E %12.5E %12.5E\n', QE(i), UE(i), QB(i), UB(i));
  end
  fclose(fID);
else
  Q = reshape(map.Q,nx*ny,1);
  U = reshape(map.U,nx*ny,1);
  Q(isnan(Q)) = 0;
  U(isnan(U)) = 0;
  fID = fopen(strcat(fname,'.dat'),'w');
  for i = 1:nx*ny
    fprintf(fID,'%12.5E %12.5E\n', Q(i), U(i));
  end
  fclose(fID);
end

