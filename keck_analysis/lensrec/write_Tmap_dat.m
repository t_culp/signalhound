function write_Tmap_dat(map,fname,nx,ny)

T = reshape(map.T,nx*ny,1);
T(isnan(T)) = 0;
fID = fopen(strcat(fname,'.dat'),'w');
for i = 1:nx*ny
  fprintf(fID,'%12.5E\n', T(i));
end
fclose(fID);

