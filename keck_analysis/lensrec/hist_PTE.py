
import numpy as np
import matplotlib as mpl
from matplotlib.pyplot import *

# set parameters
histbn = 20
x2 = np.loadtxt('PTE.dat')

# start plotting
mpl.rcParams.update({'font.size': 16,'legend.fontsize':13})
xlim(0.,1.)
#ylim(0,25.0)
hist(x2,bins=np.linspace(0.,1.,21),normed=False,alpha=0.5,lw=0)
xlabel(r'$\chi^2$ PTE')
savefig('hist_PTE.png',bbox_inches='tight')
show()

