!////////////////////////////////////////////////////!
! * DFT module
! * Toshiya Namikawa
! - Last Modified : Thu 26 Nov 2015 09:02:20 PM EST
!////////////////////////////////////////////////////!

module myfftw
  use myconst, only: dl, dlc, iu, pi, twopi
  use myarray, only: MAP_CUT
  use anaflat, only: spin_weight, gaussian_alm
  implicit none

  INTEGER FFTW_ESTIMATE
  PARAMETER (FFTW_ESTIMATE=64)

  interface DFT
    module procedure DFT_1darray, DFT_2darray
  end interface

  interface DFT_POL
    module procedure DFT_POL_1darray, DFT_POL_2darray
  end interface

  private FFTW_ESTIMATE
  private dl, dlc, iu, pi, twopi
  private spin_weight, gaussian_alm
  private MAP_CUT

contains 


!//////////////////////////////////////////////////////////////////////!
! * QU <-> EB transform in Fourier space

subroutine QU2EB(nn,D,QU,EB,trans)
! * trans = 1  :  QU -> EB
! * trans = -1 :  EB -> QU
  implicit none
  !I/O
  integer, intent(in) :: nn(2), trans
  real(dl), intent(in) :: D(2)
  complex(dlc), intent(inout), dimension(:,:) :: QU,EB
  !internal
  integer :: i, j, n
  real(dl) :: x, y, sin2t, cos2t

  n = 1
  do i = 1, nn(1)
    x = twopi*dble(i-1-nn(1)*0.5d0)/D(1)
    do j = 1, nn(2)
      y = twopi*dble(j-1-nn(2)*0.5d0)/D(2)
      if (x==0.d0.and.y==0.d0) then
        cos2t = 0.d0
        sin2t = 0.d0
      else
        cos2t = (x**2-y**2)/(x**2+y**2)
        sin2t = 2.d0*x*y/(x**2+y**2)
      end if
      if (trans==1) then
        EB(1,n) = QU(n,1)*cos2t + QU(n,2)*sin2t
        !B(n) = Q(n)*sin2t - U(n)*cos2t
        EB(2,n) = -QU(n,1)*sin2t + QU(n,2)*cos2t
      else if (trans==-1) then
        QU(n,1) = EB(1,n)*cos2t - EB(2,n)*sin2t
        QU(n,2) = EB(1,n)*sin2t + EB(2,n)*cos2t
      end if
      n = n + 1
    end do
  end do 

end subroutine QU2EB


subroutine TEST_DFT(nn,D)
! * test DFT routines: 
!  1) alm = 1 -> map = Delta(x,y)
!  2) map = 1 -> alm = Delta(lx,ly)
  implicit none
  !I/O
  integer, intent(in) :: nn(:)
  real(dl), intent(in) :: D(:)
  !internal
  integer :: npix, i, j, n
  complex(dlc), allocatable, dimension(:) :: alm

  npix = nn(1)*nn(2)
  allocate(alm(npix))
  alm = 1.d0
  write(*,*) "TEST_DFT: Delta(x=y=0)=", dble(nn(1)*nn(2))/D(1)/D(2)
  write(*,*) "TEST_DFT: Delta(lx=ly=0)=", D(1)*D(2)

  !* inverse DFT
  call DFT_1DARRAY(alm,nn,D,-1)
  open(unit=20,file="testdft_alm.dat",status="replace")
  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      write(20,"(I7,1X,I7,2(1X,E12.5))") i, j, alm(n)
      n = n + 1
    end do
  end do
  close(20)

  !* DFT
  alm = 1.d0
  call DFT_1DARRAY(alm,nn,D,1)
  open(unit=20,file="testdft_map.dat",status="replace")
  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      write(20,"(I7,1X,I7,2(1X,E12.5))") i, j, alm(n)
      n = n + 1
    end do
  end do
  close(20)

  call DFT_1DARRAY(alm,nn,D,-1)
  open(unit=20,file="testdft_unity.dat",status="replace")
  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      write(20,"(I7,1X,I7,2(1X,E12.5))") i, j, alm(n)
      n = n + 1
    end do
  end do
  close(20)

  deallocate(alm)

end subroutine TEST_DFT


subroutine TEST_DFT_RED(nn,D)
! * test DFT routines: 
  implicit none
  !I/O
  integer, intent(in) :: nn(:)
  real(dl), intent(in) :: D(:)
  !internal
  integer :: npix, i, j, n
  real(dl) :: x, y
  complex(dlc), allocatable, dimension(:) :: alm

  npix = nn(1)*nn(2)
  allocate(alm(npix))

  n = 1
  do i = 1, nn(1)
    x = twopi*dble(i-1-nn(1)*0.5d0)/D(1)
    do j = 1, nn(2)
      y = twopi*dble(j-1-nn(2)*0.5d0)/D(2)
      if(.not.x*y==0)  alm(n) = 1.d0/(x**2+y**2)
      n = n + 1
    end do
  end do

  !* inverse DFT
  call DFT_1DARRAY(alm,nn,D,-1)
  open(unit=20,file="testdft_alm.dat",status="replace")
  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      write(20,"(I7,1X,I7,2(1X,E12.5))") i, j, alm(n)
      n = n + 1
    end do
  end do
  close(20)

  deallocate(alm)

end subroutine TEST_DFT_RED


subroutine DFT_2DARRAY(map,nn,D,trans)
  implicit none
  !I/O
  integer, intent(in) :: nn(2), trans
  real(dl), intent(in) :: D(2)
  complex(dlc), intent(inout) :: map(:,:)
  !internal
  integer :: i, j, n, plan(8)
  real(dl) :: dS, mean
  complex(dlc), dimension(0:nn(1)-1,0:nn(2)-1) :: amap 

  do i = 1, nn(1)
    do j = 1, nn(2)
      amap(i-1,j-1) = (-1)**(i+j)*map(i,j)
    end do
  end do

  call DFFTW_PLAN_DFT_2D(plan,nn(1),nn(2),amap,amap,trans,FFTW_ESTIMATE)
  call DFFTW_EXECUTE_DFT(plan,amap,amap)
  call DFFTW_DESTROY_PLAN(plan)

  dS = D(1)*D(2)/dble(nn(1)*nn(2)) ! area of one real-space pixel (dxdy)

  mean = 0d0
  do i = 1, nn(1)
    do j = 1, nn(2)
      if(trans==-1) then
        ! multiply 1/dS * 1/(nn(1)*nn(2))
        map(i,j) = amap(i-1,j-1)*(-1)**(i+j)/D(1)/D(2)
      else if(trans==1) then
        map(i,j) = amap(i-1,j-1)*(-1)**(i+j)*dS
      end if
      mean = mean + abs(map(i,j))
    end do
  end do

  ! error control
  mean = mean/dble(nn(1)*nn(2))
  do i = 1, nn(1)
    do j = 1, nn(2)
      if(abs(map(i,j))<mean*1d-15) map(i,j) = 0d0
    end do
  end do


end subroutine DFT_2DARRAY


subroutine DFT_1DARRAY_OLD(map,nn,D,trans)
  implicit none
  !I/O
  integer, intent(in) :: nn(2), trans
  real(dl), intent(in) :: D(2)
  complex(dlc), intent(inout) :: map(:)
  !internal
  complex(dlc), dimension(nn(1),nn(2)) :: temp

  temp = reshape(map,(/nn(1),nn(2)/),order=(/2,1/))
  call DFT_2DARRAY(temp,nn,D,trans)
  map = reshape(transpose(temp),(/nn(1)*nn(2)/))

end subroutine DFT_1DARRAY_OLD


subroutine DFT_1DARRAY(map,nn,D,trans)
  implicit none
  !I/O
  integer, intent(in) :: nn(1:2), trans
  double precision, intent(in) :: D(1:2)
  complex(dlc), intent(inout) :: map(:)
  !internal
  integer :: i, j, n, plan(8)
  double precision :: dS, mean
  complex(dlc), allocatable :: amap(:,:)

  allocate(amap(0:nn(1)-1,0:nn(2)-1))
  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      amap(i-1,j-1) = (-1)**(i+j)*map(n)
      n = n + 1
    end do
  end do

  call DFFTW_PLAN_DFT_2D(plan,nn(1),nn(2),amap,amap,trans,FFTW_ESTIMATE)
  call DFFTW_EXECUTE_DFT(plan,amap,amap)
  call DFFTW_DESTROY_PLAN(plan)

  dS = D(1)*D(2)/dble(nn(1)*nn(2)) ! area of one real-space pixel (dxdy)

  mean = 0d0
  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      if(trans==-1) then
        ! multiply 1/dS * 1/(nn(1)*nn(2))
        map(n) = amap(i-1,j-1)*(-1)**(i+j)/D(1)/D(2)
      else if(trans==1) then
        map(n) = amap(i-1,j-1)*(-1)**(i+j)*dS
      end if
      mean = mean + abs(map(n))
      n = n + 1
    end do
  end do
  deallocate(amap)

  ! error control (values smaller than double precision is assigned to zero)
  mean = mean/dble(nn(1)*nn(2))
  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      if(abs(map(n))<mean*1d-15) map(n) = 0d0
      n = n + 1
    end do
  end do

end subroutine DFT_1DARRAY


subroutine DFT_POL_2DARRAY(QU,nn,D,EB,trans)
  !trans=-1 : E,B -> Q+iU
  !trans=+1 : Q+iU -> E,B
  implicit none
  !I/O
  integer, intent(in) :: trans, nn(2)
  real(dl), intent(in) :: D(2)
  real(dl), intent(inout) :: QU(:,:,:)
  complex(dlc), intent(inout) :: EB(:,:,:)
  !internal
  complex(dlc), allocatable :: conjgP(:,:), P(:,:)

  if(trans==-1) then
    allocate(P(nn(1),nn(2)))
    P = EB(1,:,:) + iu*EB(2,:,:)
    call spin_weight(P,nn,D,-1,2)
    call DFT(P,nn,D,-1)
    QU(:,:,1) = real(P)
    QU(:,:,2) = aimag(P)
    deallocate(P)
  else if(trans==1) then
    allocate(P(nn(1),nn(2)),conjgP(nn(1),nn(2)))
    P = QU(:,:,1) + iu*QU(:,:,2)
    conjgP = conjg(P)
    call DFT(P,nn,D,1)
    call spin_weight(P,nn,D,1,2)
    call DFT(conjgP,nn,D,1)
    call spin_weight(conjgP,nn,D,1,-2)
    EB(1,:,:) = (P+conjgP)*0.5d0
    EB(2,:,:) = -iu*(P-conjgP)*0.5d0
    deallocate(P,conjgP)
  end if

end subroutine DFT_POL_2DARRAY


subroutine DFT_POL_1DARRAY(QU,nn,D,EB,trans)
  !trans=-1 : E,B -> Q+iU
  !trans=+1 : Q+iU -> E,B
  implicit none
  !I/O
  integer, intent(in) :: trans, nn(2)
  real(dl), intent(in) :: D(2)
  real(dl), intent(inout) :: QU(:,:)
  complex(dlc), intent(inout) :: EB(:,:)
  !internal
  complex(dlc), allocatable :: conjgP(:), P(:), temp(:,:)

  if(trans==-1) then
    allocate(P(nn(1)*nn(2)))
    P = EB(1,:) + iu*EB(2,:)
    call spin_weight(P,nn,D,-1,2)
    call DFT(P,nn,D,-1)
    QU(:,1) = real(P)
    QU(:,2) = aimag(P)
    deallocate(P)
  else if(trans==1) then
    allocate(P(nn(1)*nn(2)),conjgP(nn(1)*nn(2)))
    P = QU(:,1) + iu*QU(:,2)
    conjgP = conjg(P)
    call DFT(P,nn,D,1)
    call spin_weight(P,nn,D,1,2)
    call DFT(conjgP,nn,D,1)
    call spin_weight(conjgP,nn,D,1,-2)
    EB(1,:) = (P+conjgP)*0.5d0
    EB(2,:) = -iu*(P-conjgP)*0.5d0
    deallocate(P,conjgP)
  end if

end subroutine DFT_POL_1DARRAY


subroutine pureEB(P,nn,D,EB,Wf)
! * compute Smith's pure EB estimator
  implicit none
  !I/O
  integer, intent(in) :: nn(1:2)
  real(dl), intent(in) :: D(1:2), Wf(:)
  real(dl), intent(inout) :: P(:,:)
  complex(dlc), intent(inout) :: EB(:,:)
  !internal
  integer :: i, j, n, npix
  real(dl) :: lx, ly
  real(dl), allocatable :: WP(:,:)
  complex(dlc), allocatable, dimension(:) :: Wl,E1,E2x,E2y,B1,B2x,B2y
  complex(dlc), allocatable, dimension(:) :: Wx,Wy,Wxx,Wxy,Wyy
  complex(dlc), allocatable, dimension(:,:) :: WEB

  npix = nn(1)*nn(2)

  allocate(WP(npix,2),WEB(2,npix))
  WP(:,1) = P(:,1)*Wf
  WP(:,2) = P(:,2)*Wf
  call DFT_POL(WP,nn,D,WEB,1)
  deallocate(WP)

  !//// derivatives of window functions ////!
  allocate(Wl(npix))
  Wl = Wf
  call DFT(Wl,nn,D,1)

  !* Window functions in Fourier space
  allocate(Wx(npix),Wy(npix),Wxx(npix),Wxy(npix),Wyy(npix))
  n = 1
  do i = 1, nn(1)
    lx = twopi*dble(i-1-nn(1)*0.5d0)/D(1)
    do j = 1, nn(2)
      ly = twopi*dble(j-1-nn(2)*0.5d0)/D(2)
      Wx(n) = iu*lx*Wl(n)
      Wy(n) = iu*ly*Wl(n)
      Wxx(n) = -lx**2*Wl(n)
      Wxy(n) = -lx*ly*Wl(n)
      Wyy(n) = -ly**2*Wl(n)
      n = n + 1
    end do
  end do 
  deallocate(Wl)

  !* Derivatives of windows functions
  call DFT(Wx,nn,D,-1)
  call DFT(Wy,nn,D,-1)
  call DFT(Wxx,nn,D,-1)
  call DFT(Wxy,nn,D,-1)
  call DFT(Wyy,nn,D,-1)
 
  !//// correction terms ////!
  allocate(E1(npix),B1(npix),E2x(npix),E2y(npix),B2x(npix),B2y(npix))
  E1  = P(:,1)*(Wyy-Wxx) - 2*P(:,2)*Wxy
  B1  = P(:,2)*(Wxx-Wyy) + 2*P(:,1)*Wxy
  E2x =  2*iu*(P(:,1)*Wx + P(:,2)*Wy)
  E2y =  2*iu*(P(:,2)*Wx - P(:,1)*Wy)
  B2x =  2*iu*(P(:,2)*Wx - P(:,1)*Wy) 
  B2y = -2*iu*(P(:,1)*Wx + P(:,2)*Wy) 
  deallocate(Wx,Wy,Wxx,Wxy,Wyy)

  !* Transform to Fourier Space
  call DFT(E1,nn,D,1)
  call DFT(B1,nn,D,1)
  call DFT(E2x,nn,D,1)
  call DFT(E2y,nn,D,1)
  call DFT(B2x,nn,D,1)
  call DFT(B2y,nn,D,1)

  !* add corrections
  n = 1
  do i = 1, nn(1)
    lx = twopi*dble(i-1-nn(1)*0.5d0)/D(1)
    do j = 1, nn(2)
      ly = twopi*dble(j-1-nn(2)*0.5d0)/D(2)
      if(lx/=0.or.ly/=0) then 
        EB(1,n) = WEB(1,n) + (E1(n)+lx*E2x(n)+ly*E2y(n))/(lx**2+ly**2)
        EB(2,n) = WEB(2,n) + (B1(n)+lx*B2x(n)+ly*B2y(n))/(lx**2+ly**2)
      else
        EB(:,n) = 0d0
      end if
      n = n + 1
    end do
  end do
  deallocate(E1,B1,E2x,E2y,B2x,B2y,WEB)

end subroutine pureEB



!///////////////////////////////////////////////////////////////////////!
!* generate Gaussian random fluctuations on 2D map

subroutine GAUSSIAN_MAP(nn,mm,D,eL,Cl,map,fix)
!simulate Gaussian alm with (mm), and mapping as (nn)-pixel map
  implicit none
  !I/O
  logical, intent(in), optional :: fix
  integer, intent(in) :: nn(1:2), mm(1:2), eL(1:2)
  real(dl), intent(in) :: D(1:2), Cl(:)
  complex(dlc), intent(out) :: map(:)
  !internal
  integer :: iL(1:2)
  complex(dlc), dimension(:), allocatable :: alm

  allocate(alm(mm(1)*mm(2)))

  if(present(fix)) then
    call Gaussian_Alm(mm,D,eL,alm,Cl,fix)
  else
    call Gaussian_Alm(mm,D,eL,alm,Cl)
  end if

  call DFT(alm,mm,D,-1)
  call MAP_CUT(alm,mm,map,nn)
  deallocate(alm)

end subroutine GAUSSIAN_MAP


subroutine GAUSSIAN_MAP_Pol(nn,mm,D,eL,EE,BB,QU,P,fix)
!simulate Gaussian Elm and Blm with (mm), and mapping to (nn)-pixel QU map
  implicit none
  !I/O
  logical, intent(in), optional :: fix
  integer, intent(in) :: nn(1:2), mm(1:2), eL(1:2)
  double precision, intent(in) :: D(1:2)
  double precision, intent(in), optional :: EE(:), BB(:)
  double precision, intent(out), optional :: QU(:,:)
  complex(dlc), intent(out), optional :: P(:)
  !internal
  double precision, dimension(:,:), allocatable :: QUfull
  complex(dlc), dimension(:,:), allocatable :: alm

  allocate(alm(2,mm(1)*mm(2)))
  alm = 0d0

  if(present(fix)) then
    if(present(EE)) call gaussian_alm(mm,D,eL,alm(1,:),EE,fix)
    if(present(BB)) call gaussian_alm(mm,D,eL,alm(2,:),BB,fix)
  else
    if(present(EE)) call gaussian_alm(mm,D,eL,alm(1,:),EE)
    if(present(BB)) call gaussian_alm(mm,D,eL,alm(2,:),BB)
  end if

  allocate(QUfull(mm(1)*mm(2),2))
  call DFT_POL_1DARRAY(QUfull,mm,D,alm,-1)
  if (mm(1)==nn(1).and.mm(2)==nn(2)) then
    QU = QUfull
  else
    if(present(QU)) then
      call MAP_CUT(QUfull(:,1),mm,QU(:,1),nn)
      call MAP_CUT(QUfull(:,2),mm,QU(:,2),nn)
    else if(present(P)) then
      call MAP_CUT(QUfull(:,1)+iu*QUfull(:,2),mm,P,nn)
    end if
  end if
  deallocate(QUfull)
  deallocate(alm)

end subroutine GAUSSIAN_MAP_Pol


end module myfftw


