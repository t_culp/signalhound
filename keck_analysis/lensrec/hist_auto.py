# * A_Lens histogram
# - Last Modified: Wed 11 May 2016 09:53:32 PM EDT

import numpy as np
from matplotlib.pyplot import *
import matplotlib as mpl
import scipy.stats as st

usecov = True
tag = '1var_lx30_l30-700_lB150-700'
bmax = 10
bn = 10

#//// set parameters ////#
simn = 499
histbn = 15
root = '/n/bicepfs3/users/namikawa/BK14/lensrec/'
m = 2
l = 4
c = ['b','g']
lab = ['Lensed $\Lambda$CDM','Unlensed $\Lambda$CDM']

#//// start plotting ////#
mpl.rcParams.update({'font.size': 15,'legend.fontsize':13})
xlim(-1.,3.)
ylim(0.,80.)
xlabel(r'$A_{\rm L}^{\phi\phi}$')
ylabel('Number Count')
mcl = np.zeros(bn)

#//// read cls ////#
for I in range(0,2):
  cpp = np.zeros((simn,bn))
  if I ==1: tag = tag + '_nophi'
  tags = tag+'_dp1102'
  ocl = np.loadtxt(root+'derived/obs/oCb_'+tags+'_b'+str(bmax)+'.dat',unpack=True)[m][:bn]
  for i in range(simn):
    cpp[i,:bn] = np.loadtxt(root+'clkk/b'+str(bmax)+'_r'+str(i+1)+'_'+tag+'.dat',unpack=True)[m][:bn]

  # mean
  amp = np.zeros((simn,bn))
  for n in range(bn):
    if I==0:  mcl[n] = np.mean(cpp[:,n])
    amp[:,n] = cpp[:,n]/mcl[n]

  # covariance
  cov = np.cov(amp,rowvar=0)
  cov[np.isnan(cov)] = 0.
  ic = np.linalg.inv(cov)
  a = np.sum(ic,axis=0)

  # amplitude estimator
  A = np.zeros((simn))
  for i in range(simn):
    neum = np.sum(a[:]*amp[i,:])
    deno = np.sum(a)
    A[i] = neum/deno

  # observed amplitude
  neum = np.sum(a[:]*ocl/mcl)
  deno = np.sum(a)
  obsA = neum/deno
  m1A = np.mean(A)
  m2A = np.sqrt(np.var(A))
  print obsA, m2A, obsA/m2A

  hist(A,bins=histbn,normed=False,alpha=.5,lw=0)
  if I==0: plot([obsA,obsA],[0,80],'-',color='k',lw=1)
  if I==0: figtext(0.60,0.75,r'$A_{\rm L}^{\phi\phi, {\rm obs}}$ ='+str(obsA)[:l])

#legend(loc=0)
savefig('hist_auto.png',bbox_inches='tight')
show()

