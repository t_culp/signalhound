!///////////////////////////////////////////////////////////////////////!
! * Lensing reconstruction / Lensing B-mode template from Q/U maps
! * Toshiya Namikawa
! - Last Modified: Wed 04 May 2016 04:07:26 PM EDT
!///////////////////////////////////////////////////////////////////////!

program main
  use readfile, only: SET_PARAMS_FILE, read_prm, read_str, read_log, read_int, read_dbl, read_val
  use myutils, only: str, meanvar, savetxt, loadtxt, linspace
  use myconst, only: dlc, pi, iu, twopi, Tcmb
  use mycls, only: READCl_CAMB, alm2bcl_flat, binned_ells, calcbcl_flat, cl_interp_lin, cb2c2d
  use anaflat, only: elarray, window_generate, fourier_filter, window_norm
  use myfftw, only: DFT, DFT_POL
  use recflat, only: QUADEE, QUADEB, QUADEB_BHE, LensingB, alflat_eb, alflat_ee
  use bkutils,  only: set_defparams, readcl_bk, delensed_cls , make_filter, subtract_beammap, write_cls
  use recbk, only: delensing_bias, n0_bias, rdn0_bias, mean_field, calc_transfer, map2alm, set_lfactor, calc_optfilt, data2glm
  use tool_bk14
  implicit none
  character(LEN=128) :: tag, ftag, tltag, xtag, rdn0_tag, dblm_tag, dir, gmap(3,500), falm(4,500), fmap(4,500), fcl(16), dir_plk
  logical :: nophi, outputgmap, do_delensing
  integer :: el(2), rL(2), dL(2), simn(2)
  integer :: nn(2), npix, mapn, maps, i, j, k, l, n, r, bmax, lB, cln=8, xln=5, bln=7, jack, lxcut, dp
  double precision :: D(2), Wn(6), d0, clkk, theta=3d0
  double precision, dimension(:), allocatable :: els, W, lfac, bp, elx, bc2
  double precision, dimension(:,:), allocatable :: Fl, f2d, OC, A2d, N0, RDN0, RDN0_1D, C1d, C2d, bc, Tl, sig, Gl, FF, Il, Al
  double precision, dimension(:,:,:), allocatable :: Cb, Xb, Bb, Yb
  complex(dlc), dimension(:), allocatable :: glm, klm, dlm
  complex(dlc), dimension(:,:), allocatable :: alm, est, mest, wlm, blm, alm2

  call SET_PARAMS_FILE

  !* variables
  mapn  = read_int('mapnum')
  maps  = min(mapn,101)
  lB    = read_int("lB")
  lxcut = read_int("lxcut")
  jack  = read_int("jack")
  dp    = read_int("dp")
  bmax  = read_int("bmax")
  tag   = trim(read_str("tag"))//"_lx"//str(lxcut)
  nophi = read_log("nophi")
  do_delensing = read_log("do_delensing")
  outputgmap   = read_log("outputgmap")
  call read_prm('simn',simn)

  call set_defparams(nn,npix,D,d0) !* map parameters
  dir   = trim(read_str("dir"))
  if(read_int("planck_yr")==2015)  dir_plk = trim(dir)//"PLANCK/2015/"
  if(read_int("planck_yr")==2013)  dir_plk = trim(dir)//"PLANCK/2013/"
  dir   = trim(dir)//"BK14/lensrec/"

  !* multipoles
  call read_prm("rL",rL)
  call read_prm("dL",dL)
  allocate(bp(bmax+1),bc(1,bmax))
  call binned_ells(dL,bp,bc(1,:))

  !* multipole factors
  allocate(els(npix),lfac(npix),elx(npix),FF(2,npix));  els=0d0;  lfac=0d0;  elx=0d0;  FF=0d0
  call set_lfactor(nn,D,dL,els,lfac,elx)
  do n = 1, npix
    if (rL(1)<=els(n).and.els(n)<=rL(2).and.abs(elx(n))>=lxcut) FF(1,n)=1d0
    if (max(rL(1),lB)<=els(n).and.els(n)<=rL(2).and.abs(elx(n))>=lxcut) FF(2,n)=1d0
  end do
  deallocate(elx)

  eL(1) = min(rL(1),dL(1))
  eL(2) = max(rL(2),dL(2))

  !* read theoretical CMB power spectrum for filtering
  allocate(C1d(3,eL(2)),C2d(3,npix))
  call READCL_BK(read_str("clee"),eL,.true.,CEE=C1d(1,:),CBB=C1d(2,:))
  call READCL_BK(read_str("clkk"),eL,.false.,Ckk=C1d(3,:))
  do i = 1, 3
    C2d(i,:) = cl_interp_lin(els,C1d(i,:),eL)
  end do

  !* window apodization
  allocate(W(npix))
  if (read_str("tag")=='1var') then
    call set_file(dir,dir_plk,jack,dp,"1var_lx"//str(lxcut),nophi,falm,fmap,gmap)
    call loadtxt(trim(dir)//"varmap.dat",W)
    W = W/(sum(W)/dble(npix))
  else
    call set_file(dir,dir_plk,jack,dp,"chi_lx"//str(lxcut),nophi,falm,fmap,gmap)
    call window_generate(nn,D,W,ap=read_dbl("afac"),cut=read_dbl("cutf"))
  end if
  call window_norm(W,Wn)
  write(*,*) Wn(2)

  !* optimal diagonal filter
  allocate(Fl(2,npix),f2d(2,npix),Il(2,npix),OC(2,npix));  Fl=0d0; f2d=0d0; Il=0d0; OC=0d0
  fcl(1) = trim(dir)//"derived/cls/"//trim(tag)//"_2d.dat"
  fcl(2) = trim(dir)//"derived/cls/"//trim(tag)//"_1d.dat"
  if (read_log("do_filt"))  call map2alm(fmap,falm,nn,D,FF,W,C2d(1:2,:),"matrix",fcl(1:2),100,sim=500,dust=.true.)
  call make_filter(fcl(1),nn,els,Fl,f2d,Il,OC)
  deallocate(FF)

  !* output file string
  ftag = trim(tag)//"_l"//str(rL(1))//"-"//str(rL(2))//"_lB"//str(lB)

  !* computing expected disconnected bias from BKxP
  !call calc_bkxp15(eL,rL,els,READ_STR("clee"),OC,lB,lxcut,READ_STR("nlkk"))

  !//// main calculation ////

  !1) Analytic Normalization
  allocate(A2d(6,npix),Al(7,50),bc2(50));  A2d=0d0;  Al=0d0
  call ALFLAT_EE(nn,D,Il(1,:),C2d(1,:),rL,eL,A2d(gEE,:),A2d(cEE,:))
  call ALFLAT_EB(nn,D,Il(1,:),Il(2,:),C2d(1,:),rL,eL,A2d(gEB,:),A2d(cEB,:))
  call binned_ells(eL,bc=bc2)
  do i = 1, 4
    A2d(i,:) = A2d(i,:)*els**4/4d0
    call CALCBCl_FLAT(50,eL,els,A2d(i,:),Al(i,:))
    call cb2c2d(bc2,els,eL,Al(i,:),A2d(i,:))
  end do
  !call calc_norm(eL,rL,els,C1d(1,:),OC,A2d,lB,lxcut)
  !---- check -----
  do i = 1, 4
    call CALCBCl_FLAT(50,eL,els,A2d(i,:),Al(i,:))
  end do
  Al(gMV,:) = 1d0/(1d0/Al(gEE,:)+1d0/Al(gEB,:))
  Al(cMV,:) = 1d0/(1d0/Al(cEE,:)+1d0/Al(cEB,:))
  Al(7,:) = eL(1)+(eL(2)-eL(1))*linspace(1,50,50)/50d0
  call savetxt(trim(dir)//"derived/al/"//trim(ftag)//".dat",Al(7:7,:),Al(1:6,:))
  !----------------
  deallocate(Il,OC)

  !2) Transfer function
  allocate(Tl(3,npix),Gl(2,npix));  Tl=0d0;  Gl=0d0
  Gl(1,:) = lfac*A2d(1,:)
  Gl(2,:) = lfac*A2d(2,:)
  tltag = ftag
  if (read_int("planck_yr")==2013) tltag = trim(ftag)//"_p2013"
  fcl(1) = trim(dir)//"derived/trans/"//trim(tltag)//"_1d.dat"
  fcl(2) = trim(dir)//"derived/trans/"//trim(tltag)//"_2d.dat"
  if (read_log("do_trans")) then
    call calc_transfer(falm(1,:),gmap(1,:),nn,D,rL,dL,W,C2d(1,:),els,Fl,Gl,Tl,fcl(1:2),gmap(3,1))
  else
    call loadtxt(fcl(2),Tl)
  end if
  A2d(1:2,:) = Tl(1:2,:)*A2d(1:2,:)
  !---- check ----
  do i = 1, 4
    call calcbcl_flat(50,eL,els,A2d(i,:),Al(i,:))
  end do
  call savetxt(trim(dir)//"derived/al/"//trim(ftag)//"_corr.dat",Al(7:7,:),Al(1:6,:))
  !---------------
  deallocate(Al,Gl)

  !3) Expected disconnected bias

  !* tag added
  if (jack>0) ftag = trim(ftag)//"_j"//str(jack)
  if (nophi)  ftag = trim(ftag)//"_nophi"

  allocate(N0(6,npix));  N0 = 1d0
  fcl(1) = trim(dir)//"derived/n0/"//trim(ftag)//".dat"
  if (read_log("do_norm")) then
    call n0_bias(falm(1,:),250,nn,D,rL,C2d(1,:),N0,1d0/Wn(4),lfac,Fl,fcl(1))
  else
    call loadtxt(fcl(1),N0(1:4,:))
  end if

  !4) Computing Mean Field Bias
  allocate(mest(4,npix));  mest = 0d0
  fcl(1) = trim(dir)//"derived/mean/"//trim(ftag)//".dat"
  if (read_log("do_mean")) then
    call mean_field(falm(1,:),499,nn,D,rL,C2d(1,:),mest,lfac,Fl,fcl(1))
  else
    call loadtxt(fcl(1),mest)
  end if

  !5) Optimal filtering
  allocate(sig(6,npix));  sig=0d0
  if (do_delensing.or.outputgmap) then
    if (READ_VAL("theta"))  theta = read_dbl("theta")
    theta = theta*pi/180d0
    !call make_filter_gmap(read_str("nlkk"),dL,els,A2d,theta,sig,ckk)
    call calc_optfilt(falm(1,:),gmap,nn,D,rL,dL,W,C2d(1,:),els,lfac,theta,A2d,mest(2,:),Tl,Fl,sig,"fl_"//trim(tltag)//".dat")
  end if

  !6) Delensing bias
  if (do_delensing.and.simn(2)>=1) then
    do i = 1, mapn
      allocate(alm(2,npix)); alm=0d0
      call loadtxt(falm(1,i),alm)
      dblm_tag = ftag
      if (i==1) dblm_tag = trim(dblm_tag)//"_dp"//str(dp)
      call delensing_bias(i,simn(2),alm(2,:),falm(1,:),nn,D,rL,dL,els,C2d(1,:),dlm,Tcmb,Fl,sig(5,:)*lfac**2*A2d(gEB,:))
      call savetxt(trim(dir)//"/dblm/"//trim(dblm_tag)//"_r"//str(i-1)//".dat",dlm)
      deallocate(alm)
    end do
  end if

  !7) Performing Reconstruction / Delensing and Power Spectrum Calculation
  allocate(Cb(mapn,cln,bmax),Xb(maps,xln,bmax),Bb(maps,bln,bmax),Yb(maps,bln,bmax));  Cb = 0d0;  Xb=0d0;  Bb=0d0;  Yb=0d0

  !* output xtag
  xtag = ftag
  if (read_int("planck_yr")==2013) xtag = trim(ftag)//"_p2013"

  do i = 1, mapn

    write(*,*) i, "read Elm and Blm"
    allocate(alm(2,npix),wlm(3,npix)); alm=0d0; wlm=0d0
    call loadtxt(falm(1,i),alm)
    if (dp==11020)  call subtract_beammap(trim(dir)//"alm_comb/"//trim(tag)//"_dp1102_r0.dat",alm)

    !* wiener filtering for delensing
    allocate(dlm(npix))
    if (do_delensing) then
      !* read lensing only E and B-modes
      allocate(alm2(2,npix))
      call loadtxt(falm(4,i),alm2)
      wlm(1,:) = alm(1,:) * Fl(1,:) * C2d(1,:) * Tcmb
      do n = 1, npix
        if (f2d(2,n)/=0d0)  wlm(2,n) = alm(2,n)/(f2d(2,n)/Tcmb)
        if (f2d(2,n)/=0d0)  wlm(3,n) = alm2(2,n)/(f2d(2,n)/Tcmb)
      end do
      deallocate(alm2)
      dblm_tag = ftag
      if (i==1) dblm_tag = trim(dblm_tag)//"_dp"//str(dp)
      call loadtxt(trim(dir)//"/dblm/"//trim(dblm_tag)//"_r"//str(i-1)//".dat",dlm)
    end if

    !* C^-1 filtering for lensing reconstruction
    alm = alm * Fl

    !* compute unnormalized estimators
    allocate(est(6,npix));  est = 0d0
    CALL QUADEB(nn,D,alm(1,:),alm(2,:),C2d(1,:),rL,est(gEB,:),est(cEB,:))
    est(gEB,:) = ( est(gEB,:)*lfac - mest(gEB,:) ) * A2d(gEB,:)
    est(cEB,:) = ( est(cEB,:)*lfac - mest(cEB,:) ) * A2d(cEB,:)

    !//// auto-power spectrum ////!

    !* RDN0-bias estimate
    allocate(RDN0(4,npix));  RDN0 = 0d0
    rdn0_tag = ftag
    if (i==1) rdn0_tag = trim(rdn0_tag)//"_dp"//str(dp)
    if (simn(1)>=1) then
      call rdn0_bias(falm(1,:),simn(1),nn,D,rL,C2d(1,:),i,alm,RDN0,1d0/Wn(4),lfac,Fl)
      RDN0 = RDN0 - N0(1:4,:)
      call savetxt(trim(dir)//"/RDN0/"//trim(rdn0_tag)//"_r"//str(i-1)//".dat",RDN0)
    else
      call loadtxt(trim(dir)//"/RDN0/"//trim(rdn0_tag)//"_r"//str(i-1)//".dat",RDN0)
    end if
    deallocate(alm)

    allocate(RDN0_1D(6,bmax));  RDN0_1D=0d0
    call CALCBCl_flat(bmax,dL,els,RDN0(gEB,:)*A2d(gEB,:)**2,RDN0_1D(gEB,:))
    call CALCBCl_flat(bmax,dL,els,RDN0(cEB,:)*A2d(cEB,:)**2,RDN0_1D(cEB,:))
    deallocate(RDN0)

    !* binned power spectrum
    do j = 1, 4
      call alm2bcl_flat(bmax,dL,els,D,est(j,:),Cb=Cb(i,j,:),norm=1d0/Wn(4))
      Cb(i,j,:) = Cb(i,j,:) - RDN0_1D(j,:)
    end do
    deallocate(RDN0_1D)
    if(i>=2) call savetxt(trim(dir)//"/clkk/b"//str(bmax)//"_r"//str(i-1)//"_"//trim(ftag)//".dat",bc,Cb(i,1:8,:))

    !//// cross-power spectrum ////!
    if (i<=101) then
      allocate(glm(npix),klm(npix))
      call data2glm(nn,D,gmap(2,i),glm,W)  ! reading Planck kappa
      call fourier_filter(els,dL,glm)
      glm = glm*Tl(3,:)  ! correcting ptsr-mask suppression
      call alm2bcl_flat(bmax,dL,els,D,est(gEB,:),glm,Cb=Xb(i,1,:),norm=1d0/Wn(3))
      call alm2bcl_flat(bmax,dL,els,D,est(cEB,:),glm,Cb=Xb(i,2,:),norm=1d0/Wn(3))
      call alm2bcl_flat(bmax,dL,els,D,glm,Cb=Xb(i,3,:),norm=1d0/Wn(2))
      if (2<=i) then !* use simulated noise
        call data2glm(nn,D,gmap(1,i),klm,W)
        call alm2bcl_flat(bmax,dL,els,D,glm-klm,Cb=Xb(i,4,:),norm=1d0/Wn(2)) !noise
        call alm2bcl_flat(bmax,dL,els,D,klm,Cb=Xb(i,5,:),norm=1d0/Wn(2)) !input kappa
        call savetxt(trim(dir)//"/BKxPLK/b"//str(bmax)//"_r"//str(i-1)//"_"//trim(xtag)//".dat",bc,Xb(i,:,:))
      end if
      if (i<=2.and.outputgmap)  call write_gmaps(nn,D,dL,els,est(gEB,:),glm,klm,Wn,sig,i)

      !* lensing B-modes
      if (do_delensing) then
        allocate(blm(2,npix))
        call LensingB(nn,D,wlm(1,:),sig(5,:)*est(gEB,:)*lfac,dL,blm(1,:))
        call LensingB(nn,D,wlm(1,:),sig(6,:)*glm*lfac,dL,blm(2,:))
        blm(2,:) = blm(2,:) - dlm
        call delensed_cls(bmax,dL,els,D,wlm(2,:),blm,Bb(i,:,:),Wn)
        call delensed_cls(bmax,dL,els,D,wlm(3,:),blm,Yb(i,:,:),Wn)
        deallocate(blm)
      end if
      deallocate(glm,klm)
    end if

    deallocate(wlm,est,dlm)

  end do

  deallocate(C1d,C2d,Fl,lfac,mest,A2d,N0,Tl,els,W,f2d)

  !//// output realization average ////!
  !* auto-power spectrum
  dir = trim(dir)//"derived/obs/"
  call write_cls(Cb,bc,trim(dir)//"sCb_"//trim(ftag)//"_b"//str(bmax)//".dat",trim(dir)//"oCb_"//trim(ftag)//"_dp"//str(dp)//"_b"//str(bmax)//".dat")
  call write_cls(Xb,bc,trim(dir)//"sXb_"//trim(ftag)//"_b"//str(bmax)//".dat",trim(dir)//"oXb_"//trim(ftag)//"_dp"//str(dp)//"_b"//str(bmax)//".dat")
  if (do_delensing) then
    call write_cls(Bb,bc,trim(dir)//"sBb_"//trim(ftag)//"_b"//str(bmax)//".dat",trim(dir)//"oBb_"//trim(ftag)//"_dp"//str(dp)//"_b"//str(bmax)//".dat")
    call write_cls(Yb,bc,trim(dir)//"sYb_"//trim(ftag)//"_b"//str(bmax)//".dat",trim(dir)//"oYb_"//trim(ftag)//"_dp"//str(dp)//"_b"//str(bmax)//".dat")
  end if
  deallocate(Cb,Xb,Bb,Yb,bp,bc)

end program main

