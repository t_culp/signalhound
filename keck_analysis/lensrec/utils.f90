!////////////////////////////////////////////////////!
! * Intrinsic Subtourintes
! * Toshiya Namikawa
! - Last Modified : Sun 29 Nov 2015 08:30:43 PM EST
!////////////////////////////////////////////////////!


module myutils
  implicit none
  integer, parameter :: dlc = KIND(0d0)
  double precision, parameter :: pi = 3.1415926535897932384626433832795d0
  complex(dlc), parameter :: iu = (0d0,1d0)

  !* Gauss-Legendre Quadrature
  type GAUSS_LEGENDRE_PARAMS
    integer :: n
    double precision :: eps
    double precision, dimension(:), allocatable :: z, w
  end type GAUSS_LEGENDRE_PARAMS

  interface loadtxt
    module procedure loadtxt_1d_d, loadtxt_1d_c, loadtxt_2d_d, loadtxt_2d_c
  end interface loadtxt

  interface savetxt
    module procedure savetxt_1d_d, savetxt_1d_c, savetxt_2d_d, savetxt_2d_c
  end interface savetxt

  interface ran
    module procedure ran_dble, ran_int, ran_dble_int, ran_int_dble
  end interface

  interface raneq
    module procedure raneq_int, raneq_int_dble
  end interface

  interface linspace
    module procedure arr_dble, arr_int, arr_dble_div, arr_int_div
  end interface

  interface LLsq
    module procedure LL_INT, LL_DBLE, LL_DBLE_INT
  end interface 

  private dlc, pi, iu

contains


function FileColumns(aunit) result(n)
  implicit none
  integer, intent(in) :: aunit
  integer :: n
  character(LEN=4096*32) :: InLine

  n=0
  read(aunit,'(a)', end = 10) InLine
  n = TxtNumberColumns(InLine)
10 rewind aunit
  
end function FileColumns


function FileLines(aunit) result(n)
  implicit none
  integer, intent(in) :: aunit
  integer n
  character(LEN=4096) :: InLine

  n=0
  do
    read(aunit,'(a)', end = 10) InLine
    n = n + 1
  end do
10 rewind aunit
    
end function FileLines


function TxtNumberColumns(InLine) result(n)
!* taken from CAMB
  implicit none
  character(*) :: InLine
  integer :: n, i
  logical :: isNum    
   
  n = 0
  isNum = .false.
  do i=1, len_trim(InLIne)
    if (verify(InLine(i:i),'-+eE.0123456789') == 0) then
      if (.not. IsNum) n=n+1
      IsNum=.true.
    else
      IsNum=.false.     
    end if
  end do
   
end function TxtNumberColumns


! load/save to text file
subroutine loadtxt_1d_d(f,dat,usecols)
  implicit none
  !I/O
  integer, intent(in), optional :: usecols(:)
  character(*), intent(in) :: f
  double precision, intent(out) :: dat(:)
  !internal
  integer :: n, n1

  open(unit=20,file=trim(f),status="old")
  n1 = FileLines(20)
  do n = 1, n1
    read(20,*) dat(n)
  end do
  close(20)

end subroutine loadtxt_1d_d


subroutine loadtxt_1d_c(f,dat,usecols)
  implicit none
  !I/O
  integer, intent(in), optional :: usecols(:)
  character(*), intent(in) :: f
  complex(dlc), intent(out) :: dat(:)
  !internal
  integer :: n, n1
  double precision :: r, i

  open(unit=20,file=trim(f),status="old")
  n1 = FileLines(20)
  do n = 1, n1
    read(20,*) r, i
    dat(n) = r + iu*i
  end do
  close(20)

end subroutine loadtxt_1d_c


subroutine loadtxt_2d_d(f,dat,trans,usecols)
  implicit none
  !I/O
  integer, intent(in), optional :: usecols(:)
  logical, intent(in), optional :: trans
  character(*), intent(in) :: f
  double precision, intent(out) :: dat(:,:)
  !internal
  integer :: i, n, n1, n2
  double precision, allocatable :: read_data(:)

  open(unit=20,file=trim(f),status="old")
  n1 = FileColumns(20)
  n2 = FileLines(20)
  allocate(read_data(n1))
  do n = 1, n2
    read(20,*) read_data(:)
    if (present(trans).and.trans) then
      if (present(usecols)) then
        do i = 1, size(usecols)
          dat(n,i) = read_data(usecols(i))
        end do
      else
        dat(n,:) = read_data
      end if
    else
      if (present(usecols)) then
        do i = 1, size(usecols)
          dat(i,n) = read_data(usecols(i))
        end do
      else
        dat(:,n) = read_data
      end if
    end if
  end do
  deallocate(read_data)
  close(20)

end subroutine loadtxt_2d_d


subroutine loadtxt_2d_c(f,dat,usecols)
  implicit none
  !I/O
  integer, intent(in), optional :: usecols(:)
  character(*), intent(in) :: f
  complex(dlc), intent(out) :: dat(:,:)
  !internal
  integer :: n, i, n1, n2
  double precision, allocatable :: read_data(:)

  open(unit=20,file=trim(f),status="old")
  n1 = FileColumns(20)
  n2 = FileLines(20)
  allocate(read_data(n1))
  do n = 1, n2
    read(20,*) read_data(1:n1)
    do i = 1, n1/2
      dat(i,n) = read_data(i*2-1) + iu*read_data(i*2)
    end do
  end do
  deallocate(read_data)
  close(20)

end subroutine loadtxt_2d_c


subroutine savetxt_1d_d(f,d1,d2,d3,d4,d5,d6,d7,ac)
  implicit none
  !I/O
  character(*), intent(in) :: f
  integer, intent(in), optional :: ac
  double precision, dimension(:), intent(in) :: d1
  double precision, dimension(:), intent(in), optional :: d2, d3, d4, d5, d6, d7
  !internal
  integer :: i, l, n, a0, a1, m(7)
  double precision, allocatable :: dat(:,:)

  !set output format
  a0 = 14
  if (present(ac)) a0 = ac
  a1 = a0 - 7

  ! count columns
  m = 0
  m(1) = 1
  if (present(d2))  m(2) = 1 + maxval(m)
  if (present(d3))  m(3) = 1 + maxval(m)
  if (present(d4))  m(4) = 1 + maxval(m)
  if (present(d5))  m(5) = 1 + maxval(m)
  if (present(d6))  m(6) = 1 + maxval(m)
  if (present(d7))  m(7) = 1 + maxval(m)

  n = maxval(m)
  l = size(d1,dim=1)

  ! set data
  allocate(dat(n,l))
  dat(m(1),:) = d1
  if (present(d2)) then
    if (size(d2)/=l) stop "error: data 2 size should be equal"
    dat(m(2),:) = d2 
  end if
  if (present(d3)) then
    if (size(d3)/=l) stop "error: data 3 size should be equal"
    dat(m(3),:) = d3 
  end if
  if (present(d4)) then
    if (size(d4)/=l) stop "error: data 4 size should be equal"
    dat(m(4),:) = d4 
  end if
  if (present(d5)) then
    if (size(d5)/=l) stop "error: data 5 size should be equal"
    dat(m(5),:) = d5 
  end if
  if (present(d6)) then
    if (size(d6)/=l) stop "error: data 6 size should be equal"
    dat(m(6),:) = d6 
  end if
  if (present(d7)) then
    if (size(d7)/=l) stop "error: data 7 size should be equal"
    dat(m(7),:) = d7 
  end if

  ! output
  open(unit=20,file=trim(f),status="replace")
  do i = 1, l
    write(20,"("//str(n)//"(E"//str(a0)//"."//str(a1)//",1X))") dat(1:n,i)
  end do
  close(20)

  deallocate(dat)

end subroutine savetxt_1d_d


subroutine savetxt_1d_c(f,d1,d2,d3,d4,d5,d6,d7,ac)
  implicit none
  !I/O
  character(*), intent(in) :: f
  integer, intent(in), optional :: ac
  complex(dlc), dimension(:), intent(in) :: d1
  complex(dlc), dimension(:), intent(in), optional :: d2, d3, d4, d5, d6, d7
  !internal
  integer :: i, l, n, a0, a1, m(7)
  complex(dlc), allocatable :: dat(:,:)

  !set output format
  a0 = 14
  if (present(ac)) a0 = ac
  a1 = a0 - 7

  ! count columns
  m = 0
  m(1) = 1
  if (present(d2))  m(2) = 1 + maxval(m)
  if (present(d3))  m(3) = 1 + maxval(m)
  if (present(d4))  m(4) = 1 + maxval(m)
  if (present(d5))  m(5) = 1 + maxval(m)
  if (present(d6))  m(6) = 1 + maxval(m)
  if (present(d7))  m(7) = 1 + maxval(m)

  n = maxval(m)
  l = size(d1,dim=1)

  ! set data
  allocate(dat(n,l))
  dat(m(1),:) = d1
  if (present(d2)) then
    if (size(d2)/=l) stop "error: data 2 size should be equal"
    dat(m(2),:) = d2 
  end if
  if (present(d3)) then
    if (size(d3)/=l) stop "error: data 3 size should be equal"
    dat(m(3),:) = d3 
  end if
  if (present(d4)) then
    if (size(d4)/=l) stop "error: data 4 size should be equal"
    dat(m(4),:) = d4 
  end if
  if (present(d5)) then
    if (size(d5)/=l) stop "error: data 5 size should be equal"
    dat(m(5),:) = d5 
  end if
  if (present(d6)) then
    if (size(d6)/=l) stop "error: data 6 size should be equal"
    dat(m(6),:) = d6 
  end if
  if (present(d7)) then
    if (size(d7)/=l) stop "error: data 7 size should be equal"
    dat(m(7),:) = d7 
  end if

  ! output
  open(unit=20,file=trim(f),status="replace")
  do i = 1, l
    write(20,"("//str(2*n)//"(E"//str(a0)//"."//str(a1)//",1X))") dat(1:n,i)
  end do
  close(20)

  deallocate(dat)

end subroutine savetxt_1d_c


subroutine savetxt_2d_d(f,d1,d2,d3,d4,d5,ac)
  implicit none
  !I/O
  character(*), intent(in) :: f
  integer, intent(in), optional :: ac
  double precision, dimension(:,:), intent(in) :: d1
  double precision, dimension(:,:), intent(in), optional :: d2, d3, d4, d5
  !internal
  integer :: i, l, n, m(5), a0, a1
  double precision, allocatable :: dat(:,:)

  ! set output format
  a0 = 14
  if (present(ac)) a0 = ac
  a1 = a0 - 7

  ! count columns
  m = 0
  m(1) = size(d1,dim=1)
  if (present(d2))  m(2) = size(d2,dim=1)
  if (present(d3))  m(3) = size(d3,dim=1)
  if (present(d4))  m(4) = size(d4,dim=1)
  if (present(d5))  m(5) = size(d5,dim=1)

  n = sum(m)
  l = size(d1,dim=2)

  ! set data
  allocate(dat(n,l));  dat=0d0
  dat(1:m(1),:) = d1
  if (present(d2)) then
    if (size(d2,dim=2)/=l) stop "error: data size should be equal"
    dat(m(1)+1:sum(m(1:2)),:) = d2(1:m(2),:)
  end if
  if (present(d3)) then
    if (size(d3,dim=2)/=l) stop "error: data size should be equal"
    dat(sum(m(1:2))+1:sum(m(1:3)),:) = d3 
  end if
  if (present(d4)) then
    if (size(d4,dim=2)/=l) stop "error: data size should be equal"
    dat(sum(m(1:3))+1:sum(m(1:4)),:) = d4 
  end if
  if (present(d5)) then
    if (size(d5,dim=2)/=l) stop "error: data size should be equal"
    dat(sum(m(1:4))+1:sum(m(1:5)),:) = d5 
  end if

  ! output
  open(unit=20,file=trim(f),status="replace")
  do i = 1, l
    write(20,"("//str(n)//"(E"//str(a0)//"."//str(a1)//",1X))") dat(1:n,i)
  end do
  close(20)

  deallocate(dat)

end subroutine savetxt_2d_d


subroutine savetxt_2d_c(f,d1,d2,d3,d4,d5,ac)
  implicit none
  !I/O
  character(*), intent(in) :: f
  integer, intent(in), optional :: ac
  complex(dlc), dimension(:,:), intent(in) :: d1
  complex(dlc), dimension(:,:), intent(in), optional :: d2, d3, d4, d5
  !internal
  integer :: i, l, n, m(5), a0, a1
  complex(dlc), allocatable :: dat(:,:)

  ! set output format
  a0 = 14
  if (present(ac)) a0 = ac
  a1 = a0 - 7

  ! count columns
  m = 0
  m(1) = size(d1,dim=1)
  if (present(d2))  m(2) = size(d2,dim=1)
  if (present(d3))  m(3) = size(d3,dim=1)
  if (present(d4))  m(4) = size(d4,dim=1)
  if (present(d5))  m(5) = size(d5,dim=1)

  n = sum(m)
  l = size(d1,dim=2)

  ! set data
  allocate(dat(n,l));  dat=0d0
  dat(1:m(1),:) = d1
  if (present(d2)) then
    if (size(d2,dim=2)/=l) stop "error: data size should be equal"
    dat(m(1)+1:sum(m(1:2)),:) = d2(1:m(2),:)
  end if
  if (present(d3)) then
    if (size(d3,dim=2)/=l) stop "error: data size should be equal"
    dat(sum(m(1:2))+1:sum(m(1:3)),:) = d3 
  end if
  if (present(d4)) then
    if (size(d4,dim=2)/=l) stop "error: data size should be equal"
    dat(sum(m(1:3))+1:sum(m(1:4)),:) = d4 
  end if
  if (present(d5)) then
    if (size(d5,dim=2)/=l) stop "error: data size should be equal"
    dat(sum(m(1:4))+1:sum(m(1:5)),:) = d5 
  end if

  ! output
  open(unit=20,file=trim(f),status="replace")
  do i = 1, l
    write(20,"("//str(2*n)//"(E"//str(a0)//"."//str(a1)//",1X))") dat(1:n,i)
  end do
  close(20)

  deallocate(dat)

end subroutine savetxt_2d_c


!//// interface RAN ////!

function RAN_DBLE(r,ranges) result(ran)
  implicit none
  double precision, intent(in) :: r, ranges(:)
  logical :: ran

  ran = (r >= ranges(1)) .and. (r < ranges(2))

end function RAN_DBLE


function RAN_DBLE_INT(r,ranges) result(ran)
  implicit none
  integer, intent(in) :: ranges(:)
  double precision, intent(in) :: r
  logical :: ran

  ran = (r >= ranges(1)) .and. (r < ranges(2))

end function RAN_DBLE_INT

function RAN_INT(r,ranges) result(ran)
  implicit none
  integer, intent(in) :: r, ranges(:)
  logical :: ran

  ran = (r >= ranges(1)) .and. (r < ranges(2))

end function RAN_INT

function RAN_INT_DBLE(r,ranges) result(ran)
  implicit none
  integer, intent(in) :: r
  double precision, intent(in) :: ranges(:)
  logical :: ran

  ran = (r >= ranges(1)) .and. (r < ranges(2))

end function RAN_INT_DBLE

!//// interface RANEQ ////!

function RANEQ_INT(r,ranges) result(ran)
  implicit none
  integer, intent(in) :: r, ranges(:)
  logical :: ran

  ran = (r >= ranges(1)) .and. (r <= ranges(2))

end function RANEQ_INT

function RANEQ_INT_DBLE(r,ranges) result(ran)
  implicit none
  integer, intent(in) :: r
  double precision, intent(in) :: ranges(:)
  logical :: ran

  ran = (r >= ranges(1)) .and. (r <= ranges(2))

end function RANEQ_INT_DBLE

!//// interface LLsq ////!

function LL_DBLE_INT(l,s)
  implicit none
  integer, intent(in) :: s
  double precision, intent(in) :: l
  double precision :: LL_DBLE_INT

  LL_DBLE_INT = dsqrt((l-dble(s))*(l+dble(s)+1))

end function LL_DBLE_INT

function LL_DBLE(l,s)
  implicit none
  double precision, intent(in) :: l, s
  double precision :: LL_DBLE

  LL_DBLE = dsqrt((l-s)*(l+s+1.d0))

end function LL_DBLE

function LL_INT(l,s)
  implicit none
  integer, intent(in) :: l, s
  double precision :: LL_INT

  LL_INT = dsqrt(dble(l-s)*dble(l+s+1))

end function LL_INT


subroutine sqlls(eL,s,ll)
  implicit none
  !I/O
  integer, intent(in) :: eL(1:2), s
  double precision, intent(out) :: ll(eL(1):eL(2))
  !internal
  integer :: l

  ll = 0.d0
  do l = eL(1), eL(2)
    ll(l) = LL_INT(l,s)
  end do

end subroutine sqlls


!//// interface arr ////!

function arr_dble(ini,fin,n)  result(f)
  implicit none
  integer, intent(in) :: n
  double precision, intent(in) :: ini, fin
  integer :: i
  double precision :: f(1:n), d

  d = (fin-ini)/dble(n-1)
  f = [ ( (ini+d*(i-1)), i=1,n ) ]

end function arr_dble


function arr_dble_div(div,n)  result(f)
  implicit none
  integer, intent(in) :: n
  double precision, intent(in) :: div(1:2)
  double precision :: f(1:n)

  f = arr_dble(div(1),div(2),n)

end function arr_dble_div


function arr_int(ini,fin,n)  result(f)
  implicit none
  integer, intent(in) :: n, ini, fin
  integer :: i
  double precision :: f(1:n), d

  d = dble(fin-ini)/dble(n-1)
  f = [ ( (ini+d*(i-1)), i=1,n ) ]

end function arr_int


function arr_int_div(div,n)  result(f)
  implicit none
  integer, intent(in) :: n, div(1:2)
  double precision :: f(1:n)

  f = arr_int(div(1),div(2),n)

end function arr_int_div


!//// functions/subroutines ////!

function PARAM_ID(str,LAB)
  implicit none
  character(*), intent(in) :: str, LAB(:)
  integer :: param_id
  integer :: p

  do p = 1, size(LAB)
    if(trim(LAB(p))==str) param_id = p
  end do

end function PARAM_ID


function str(n)
  implicit none
  integer, intent(in) :: n
  character(1+int(log10(dble(n+0.9)))) :: str
  character(LEN=128) :: a

  write(a,*) n
  str = adjustl(a)

end function str


function str4(n)
  implicit none 
  integer, intent(in) :: n
  character(LEN=4) :: str4, a

  if(n<10) a="000"
  if(n>=10.and.n<100) a="00"
  if(n>=100) a="0"
  str4 = trim(a)//str(n)

end function str4


!//// interpolation ////!

function interp_lin(x,x0,x1,y0,y1)
  implicit none
  double precision, intent(in) :: x,x0,x1,y0,y1
  double precision :: interp_lin

  interp_lin = y0 + ((x-x0)/(x1-x0))*(y1-y0)

end function interp_lin


subroutine spline(x,y,n,yp1,ypn,y2)
! * from numerical recipe, modified for f90.
! Given arrays x(1:n) and y(1:n) containing a tabulated function, i.e.
! y(i)=f(x(i)), with x(1)<x(2)<...<x(n), and given values yp1 and ypn for
! the first derivative of the interpolating function at points 1 and n,
! respectively, this routine returns an array y2(1:n) of length n which
! contains the second derivatives of the interpolating function at the
! tabulated points x(i).  If yp1 and/or ypn are equal to 1.e30 or larger,
! the routine is signaled to set the corresponding boundary condition for a
! natural spline with zero second derivative on that boundary.
! Parameter: nmax is the largest anticipiated value of n. 
  implicit none
  !I/O
  integer, intent(in) :: n
  double precision, intent(in)  :: yp1, ypn, x(n), y(n)
  double precision, intent(out) :: y2(n)
  !internal
  integer :: i, k
  double precision :: p, qn, sig, un, u(n)

  if (yp1.gt..99d30) then
    y2(1) = 0d0
    u(1)  = 0d0
  else
    y2(1) = -0.5d0
    u(1)  = (3.d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
  end if

  do i=2, n-1
    sig   = (x(i)-x(i-1))/(x(i+1)-x(i-1))
    p     = sig*y2(i-1)+2d0
    y2(i) = (sig-1d0)/p
    u(i)  = (6d0*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1)) / (x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
  end do

  if (ypn.gt..99d30) then
    qn = 0d0
    un = 0d0
  else
    qn = 0.5d0
    un = (3d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
  endif
  y2(n) = (un-qn*u(n-1))/(qn*y2(n-1)+1d0)
  do k = n-1, 1, -1
    y2(k) = y2(k)*y2(k+1) + u(k)
  end do

end subroutine spline 


function splint(x,xa,ya,y2a)  result(f)
  implicit none
  double precision, intent(in) :: x,xa(:),y2a(:),ya(:)
  double precision :: f
  integer :: khi,klo

  klo = neighb(x,xa)
  khi = klo + 1
  f = interp_sp(x,xa(klo),xa(khi),ya(klo),ya(khi),y2a(klo),y2a(khi))

end function splint


function neighb(x,arr)  result(nn)
! return nearest neighbour (nn) which satisfies arr(nn)<=x<=arr(nn+1)
  implicit none
  double precision, intent(in) :: x, arr(:)
  integer :: nn, n, lo, hi

  lo = 1
  hi = size(arr)

1 if (hi-lo.gt.1) then
    n = (hi+lo)/2
    if(arr(n).gt.x)then
      hi = n
    else
      lo = n
    end if
    goto 1
  end if
  nn = lo

end function neighb


function interp_sp(x,x0,x1,y0,y1,sp0,sp1)  result(f)
  implicit none
  double precision, intent(in) :: x,x0,x1,y0,y1,sp0,sp1
  double precision :: f, a, b, h

  h = x1-x0
  if (h.eq.0.) stop 'error (interp_sp): h is zero'
  a = (x1-x)/h
  b = (x-x0)/h
  f = a*y0 + b*y1 + ((a**3-a)*sp0+(b**3-b)*sp1)*h**2/6.0
 
end function interp_sp


!//// MC simulation ////!

subroutine MEANVAR(Cl,mCl,vCl,f) 
!Compute mean and variance of 1D array
!Input: 1D array for number of realizations (Cl)
!Outputs: mean value (mCl), variance (vCl) 
  implicit none
  !I/O
  character(*), intent(in), optional :: f
  double precision, intent(in) :: Cl(:,:)
  double precision, intent(out), optional :: mCl(:), vCl(:)
  !internal
  integer :: n, l, i
  double precision, allocatable :: mean(:), var(:)

  n = size(Cl,dim=1)
  l = size(Cl,dim=2)

  allocate(mean(l),var(l));  mean = 0d0;  var = 0d0

  if(n == 1) then 
    mean = Cl(1,:)
  else if (n >= 2) then 
    mean = sum(Cl,dim=1)/dble(n) !mean
    var = sum(Cl**2,dim=1)/dble(n) - mean**2 !variance
    var = dsqrt(var)
  end if

  if(present(mCl)) mCl = mean
  if(present(vCl)) vCl = var
  if(present(f))   call savetxt(f,linspace(1,l,l),mean,var)
  deallocate(mean,var)

end subroutine MEANVAR


subroutine SAVE_MEAN_VAR_CL(fname,Cl1,Cl2,el)
  implicit none
  !I/O
  character(*), intent(in) :: fname
  double precision, intent(in) :: Cl1(:,:)
  double precision, intent(in), optional :: Cl2(:,:),el(:)
  !intenral
  integer :: l, lmax
  double precision, allocatable :: mCl1(:), vCl1(:)
  double precision, allocatable :: mCl2(:), vCl2(:)

  lmax = size(Cl1,dim=2)
  allocate(mCl1(lmax),mCl2(lmax),vCl1(lmax),vCl2(lmax))
  call meanvar(Cl1,mCl1,vCl1)
  if(present(Cl2)) call meanvar(Cl2,mCl2,vCl2)

  open(unit=20,file=fname,status="replace")
  do l = 1, lmax
    if(present(Cl2)) then
      if(present(el)) then
        write(20,"(5(1X,E12.5))") el(l),mCl1(l),vCl1(l),mCl2(l),vCl2(l)
      else
        write(20,"(I4,4(1X,E12.5))") l,mCl1(l),vCl1(l),mCl2(l),vCl2(l)
      end if
    else 
      if(present(el)) then
        write(20,"(3(1X,E12.5))") el(l),mCl1(l),vCl1(l)
      else
        write(20,"(I4,2(1X,E12.5))") l,mCl1(l),vCl1(l)
      end if
    end if
  end do
  close(20)

end subroutine SAVE_MEAN_VAR_CL


!//// histogram ////!

subroutine GET_HISTOGRAM_BIN(a1,a2,bnum,b_edge)
  implicit none
  !I/O
  integer, intent(in) :: bnum
  double precision, intent(in) :: a1, a2
  double precision, intent(out) :: b_edge(0:bnum)
  !internal
  integer :: b
  double precision :: width

  width = (a2-a1)/dble(bnum)
  do b = 0, bnum
    b_edge(b) = dble(b)*width + a1
  end do

end subroutine GET_HISTOGRAM_BIN


subroutine GET_HISTOGRAM(hist,a1,a2,bnum,inum,f,x)
  implicit none
  !I/O
  integer, intent(in) :: bnum, inum
  double precision, intent(in) :: a1, a2, f(:)
  double precision, intent(in), optional :: x(:)
  double precision, intent(out) :: hist(:)
  !internal
  integer :: b, i
  double precision :: width, b_edge(0:bnum)

  call GET_HISTOGRAM_BIN(a1,a2,bnum,b_edge)

  !make histogram
  hist=0
  do b = 1, bnum
    do i = 1, inum
      if(present(x)) then 
        if(ran(x(i),b_edge(b-1:b))) hist(b) = hist(b) + f(i)
      else
        if(ran(f(i),b_edge(b-1:b))) hist(b) = hist(b) + 1.d0
      end if
    end do
  end do
  hist = hist/dble(inum)

end subroutine GET_HISTOGRAM


function CHISQ(obs,var,model)
  implicit none
  !I/O
  double precision, intent(in) :: obs(:), var(:), model(:)
  double precision :: chisq

  chisq = - sum((model(:)-obs(:))**2/(2.d0*var(:)**2))

end function CHISQ


!//// Quadratures ////!

subroutine get_zeropoint(GLn,GLw,GLz,GLeps)
! Zero points of Gauss-Legendre !
  implicit none
  !I/O
  integer, intent(in) :: GLn
  double precision, intent(in) :: GLeps
  double precision, intent(out) :: GLw(:), GLz(:)
  !internal
  integer :: m, j, i
  double precision :: z1, z, pp, p3, p2 ,p1

  m = (GLn+1)/2
  do i = 1, m
    z = dcos(pi*(dble(i)-0.25d0)/(dble(GLn)+0.5d0))
1   continue
    p1 = 1d0
    p2 = 0d0
    do j = 1, GLn
      p3 = p2
      p2 = p1
      p1 = ((2d0*j-1d0)*z*p2-(j-1d0)*p3)/dble(j)
    end do
    pp = GLn*(z*p1-p2)/(z*z-1d0)
    z1 = z
    z  = z1-p1/pp
    if(abs(z-z1).gt.GLeps) goto 1
    GLz(i) = -z
    GLz(GLn+1-i) = z
    GLw(i) = 2d0/((1d0-z*z)*pp*pp)
    GLw(GLn+1-i) = GLw(i)
  end do

end subroutine get_zeropoint


subroutine gl_initialize(GL,gln,eps)
  implicit none
  integer, intent(in) :: gln
  double precision, intent(in), optional :: eps
  type(GAUSS_LEGENDRE_PARAMS), intent(out) :: GL

  GL%n = gln
  GL%EPS = 5d-16
  if (present(eps)) GL%EPS = eps
  allocate(GL%z(GL%n),GL%w(GL%n))
  call get_zeropoint(GL%n,GL%w,GL%z,GL%eps)

end subroutine gl_initialize


subroutine gl_finalize(GL)
  implicit none
  type(GAUSS_LEGENDRE_PARAMS), intent(inout) :: GL

  deallocate(GL%z,GL%w)

end subroutine gl_finalize


subroutine ZeroPoint(GL)
  implicit none
  !I/O
  type(GAUSS_LEGENDRE_PARAMS), intent(inout) :: GL

  call get_zeropoint(GL%n,GL%w,GL%z,GL%eps)

end subroutine ZeroPoint

function GLpoint(xran,GLz)  result(x)
!* x values for GL quadrature
!    (b+a)/2 + (b-a)/2 x_{i,0}
! where x_{i,0} are the solution of P_{n+1}(x)=0
  implicit none
  double precision :: xran(:), GLz
  double precision :: x, xm, xl

  xm = 0.5d0*(xran(2)+xran(1))
  xl = 0.5d0*(xran(2)-xran(1))
  x = xm + xl*GLz

end function GLpoint

function GLpoints(xran,GLz)  result(x)
  implicit none
  double precision :: xran(:), GLz(:)
  double precision :: x(size(GLz))
  integer :: i

  do i = 1, size(GLz)
    x(i) = GLpoint(xran,GLz(i))
  end do

end function GLpoints

function GLdx(xran,GLw)  result(dx)
!* interval for GL quadrature
!    (b-a)/2 * w_i
  implicit none
  double precision :: xran(:), GLw
  double precision :: dx, xl

  xl = 0.5d0*(xran(2)-xran(1))
  dx = xl*GLw

end function GLdx

function GLdxs(xran,GLw)  result(dx)
  implicit none
  double precision :: xran(:), GLw(:)
  double precision :: dx(size(GLw)), xl
  integer :: i

  do i = 1, size(GLw)
    dx(i) = GLdx(xran,GLw(i))
  end do

end function GLdxs

function Integ(dx,fx)  result(f)
!Gauss Legendre Quadrature
  implicit none
  double precision, intent(in) :: dx(:), fx(:)
  integer :: i
  double precision :: f

  f = 0.d0
  do i = 1, size(dx)
    f = f + dx(i)*fx(i)
  end do

end function Integ


!///////////////////////////////////////////////////////!
! * Matrix Analysis
!///////////////////////////////////////////////////////!

subroutine multigrid_obscov(Cov,CovM,npix,mpix,s,nn,symmetric)
!* transform pixel-pixel covariance into multigride space
  implicit none
  logical, intent(in), optional :: symmetric
  integer, intent(in) :: npix, mpix, nn(1:2), s
  double precision, intent(in) :: Cov(npix,npix)
  double precision, intent(out) :: CovM(mpix,mpix)
  !internal
  logical :: sym = .false.
  integer, allocatable :: label(:,:,:), a(:,:), b(:,:)
  integer :: mi, mj, si, sj
  double precision :: x

  if(present(symmetric)) sym = symmetric
  allocate(label(0:mpix-1,s,2))
  call multigrid_index(nn(1),mpix,s,label)

  allocate(a(s,2),b(s,2))
  do mi = 1, mpix
    do mj = 1, mpix
      if(.not.sym.or.mj>=mi) then
        a = label(mi-1,:,:)
        b = label(mj-1,:,:)
        if(mi==1.and.mj==1) write(*,*) a(:,:)
        x = 0.d0
        do si = 1, s
          do sj = 1, s
            if(product(Cov(a(si,1):a(si,2),b(sj,1):b(sj,2)))==0.d0) then
              x = 0.d0
              goto 11
            end if
            x = x + sum(Cov(a(si,1):a(si,2),b(sj,1):b(sj,2)))
          end do
        end do
11      CovM(mi,mj) = x/dble(s**4)
        if(sym.and..not.mi==mj) CovM(mj,mi) = CovM(mi,mj)
      end if
    end do
  end do  
  deallocate(label,a,b)


end subroutine multigrid_obscov


subroutine multigrid_index(mmx,mpix,s,label)
!generate matrix index for multigrid
  implicit none
  !I/O
  integer, intent(in) :: mmx,mpix,s
  integer, intent(out) :: label(0:mpix-1,s,2)
  !internal
  integer :: m, xi, yi, si, pix

  do m = 0, mpix-1
    do si = 1, s
      xi = mod(m,mmx)
      yi = int(m/mmx)
      pix = yi*s**2*mmx
      label(m,si,1) = pix + 1 + s*xi + (si-1)*s*mmx
      label(m,si,2) = pix + s*(xi+1) + (si-1)*s*mmx
    end do
  end do

end subroutine multigrid_index


end module myutils

