!////////////////////////////////////////////////////!
! * Toshiya Namikawa
! - Last Modified : Sun 29 Nov 2015 08:24:45 PM EST
! - Time-stamp: <Tue Jan 06 13:29:22 PST 2015>
!////////////////////////////////////////////////////!

module anaflat
  use myrandom, only: InitRandom, ranmar, Gaussian1
  use myconst, only: dl, dlc, iu, pi
  use myutils, only: str, ran, filelines, savetxt
  use myarray, only: write_arr, map_cut
  implicit none
  type WINDOW_FUNCTION_PARAMS
    double precision :: ap_factor, sky_frac, mask_factor
  end type WINDOW_FUNCTION_PARAMS

  interface MAP_SMOOTHING
    module procedure MAP_SMOOTHING_1D, MAP_SMOOTHING_2D, MAP_SMOOTHING_1D_CMPLX, MAP_SMOOTHING_2D_CMPLX
  end interface MAP_SMOOTHING

  interface spin_weight
    module procedure spin_weight_1darray, spin_weight_2darray
  end interface 

  interface Gaussian_alm
    module procedure gaussian_alm_1darray, gaussian_alm_2darray
    !module procedure gaussian_alm_1darray_old, gaussian_alm_2darray
  end interface Gaussian_alm

  interface fourier_filter
    module procedure fourier_filter_1d, fourier_filter_2d
  end interface fourier_filter

  private InitRandom, ranmar, gaussian1
  private dl, dlc, iu, pi
  private str, ran, filelines, savetxt
  private write_arr, map_cut

  !local variable
  double precision, parameter :: twopi = 2d0*pi

contains 


subroutine array_extend(nn,s,arr1,arr2,f)
!* extending array to large grids
  implicit none
!
! [input]
! nn   --- x and y grids
! s    --- increasing factors
! arr1 --- input array : nn(1) x nn(2) grids
  integer, intent(in) :: nn(1:2), s(1:2)
  complex(dlc), intent(in) :: arr1(:)
!
! [output]
! arr2 --- output array : nn(1)*s(1) x nn(2)*s(2) grids
  complex(dlc), intent(out) :: arr2(:)
!
! (optional)
! f    --- input and output array file names
  character(*), intent(in), optional :: f(1:2)
!
! [internal]
  integer :: mm(2), ii, jj, i, j
  complex(dlc), allocatable :: tlm1(:,:), tlm2(:,:)

  mm = s*nn
  if(present(f))  call savetxt(f(1),abs(arr1)**2)

  allocate(tlm1(nn(1),nn(2)),tlm2(mm(1),mm(2)))

  tlm1 = reshape(arr1,nn,order=[2,1])
  tlm2 = 0d0
  ii = 0
  do i=1, mm(1)
    jj = 0
    if (i-mm(1)/2<-nn(1)/2+1.or.i-mm(1)/2>=nn(1)/2) cycle
    ii = ii + 1
    do j=1, mm(2)
      if (j-mm(2)/2<-nn(2)/2+1.or.j-mm(2)/2>=nn(2)/2) cycle
      jj = jj + 1
      tlm2(i,j) = tlm1(ii,jj)
    end do
  end do
  arr2 = reshape(transpose(tlm2),[mm(1)*mm(2)])
  if(present(f))  call savetxt(f(2),abs(arr2)**2)

  deallocate(tlm1,tlm2)

end subroutine array_extend


subroutine array_fine(nn,s,arr1,arr2,f)
!* make finer array with same information
  implicit none
! [input]
! nn   --- x and y grids
! s    --- increasing factors 
! arr1 --- finer array with nn(1) x nn(2) grids
  integer, intent(in) :: nn(1:2), s(1:2)
  double precision, intent(in) :: arr1(:)
!
! [output]
! arr2 --- finer array with nn(1)*s(1) x nn(2)*s(2) grids
  double precision, intent(out) :: arr2(:)
!
! (optional)
! f    --- file names for input and output arrays
  character(*), intent(in), optional :: f(1:2)
!
! [internal]
  integer :: mm(2), i, j
  double precision, allocatable :: map1(:,:), map2(:,:)

  mm = nn*s
  allocate(map1(nn(1),nn(2)),map2(mm(1),mm(2)))
  if (present(f))  call savetxt(f(1),arr1)

  map1 = reshape(arr1,nn,order=[2,1])
  do i = 1, nn(1)
    do j = 1, nn(2)
      map2((i-1)*s(1)+1:(i-1)*s(1)+s(1),(j-1)*s(2)+1:(j-1)*s(2)+s(2)) = map1(i,j)
    end do
  end do
  arr2 = reshape(transpose(map2),[mm(1)*mm(2)])
  if (present(f))  call savetxt(f(2),arr2)

  deallocate(map1,map2)

end subroutine array_fine


subroutine MAP_SMOOTHING_2D(nn,rn,s,MAP,sMAP)
!input map (MAP,nn) is smoothed (sMAP,rn) with a smoothing size (s)
  implicit none
  !I/O
  integer, intent(in) :: nn(2), rn(2), s
  double precision, intent(in) :: MAP(:,:)
  double precision, intent(out) :: sMAP(:,:)
  !internal
  integer :: i, j

  do i = 1, rn(1)
    do j = 1, rn(2)
      sMAP(i,j) = sum(MAP((i-1)*s+1:i*s,(j-1)*s+1:j*s))/dble(s**2)
    end do
  end do

end subroutine MAP_SMOOTHING_2D


subroutine MAP_SMOOTHING_2D_ADV(nn,rn,s,MAP,sMAP)
! set zero if one of pixels has zero value
  implicit none
  !I/O
  integer, intent(in) :: nn(2), rn(2), s
  double precision, intent(in) :: MAP(:,:)
  double precision, intent(out) :: sMAP(:,:)
  !internal
  integer :: i, j

  do i = 1, rn(1)
    do j = 1, rn(2)
      if(product(map((i-1)*s+1:i*s,(j-1)*s+1:j*s))==0) then
        smap(i,j) = 0.d0
      else
        smap(i,j) = sum(map((i-1)*s+1:i*s,(j-1)*s+1:j*s))/dble(s**2)
      end if
    end do
  end do

end subroutine MAP_SMOOTHING_2D_ADV


subroutine MAP_SMOOTHING_1D(nn,rn,s,MAP1D,sMAP1D)
!input map (MAP,nn) is smoothed (sMAP,rn) with a smoothing size (s)
  implicit none
  !I/O
  integer, intent(in) :: nn(2), rn(2), s
  double precision, intent(in) :: MAP1D(:)
  double precision, intent(out) :: sMAP1D(:)
  !internal
  double precision :: MAP(nn(1),nn(2)), sMAP(rn(1),rn(2))

  MAP = reshape(MAP1D,(/nn(1),nn(2)/),order=(/2,1/))
  CALL MAP_SMOOTHING_2D(nn,rn,s,MAP,sMAP)
  sMAP1D = reshape(transpose(sMAP),(/rn(1)*rn(2)/))

end subroutine MAP_SMOOTHING_1D


subroutine MAP_SMOOTHING_1D_CMPLX(nn,rn,s,MAP1D,sMAP1D)
!input map (MAP,nn) is smoothed (sMAP,rn) with a smoothing size (s)
  implicit none
  !I/O
  integer, intent(in) :: nn(2), rn(2), s
  complex(dlc), intent(in) :: MAP1D(:)
  complex(dlc), intent(out) :: sMAP1D(:)
  !internal
  complex(dlc) :: MAP(nn(1),nn(2)), sMAP(rn(1),rn(2))

  MAP = reshape(MAP1D,(/nn(1),nn(2)/),order=(/2,1/))
  CALL MAP_SMOOTHING_2D_CMPLX(nn,rn,s,MAP,sMAP)
  sMAP1D = reshape(transpose(sMAP),(/rn(1)*rn(2)/))

end subroutine MAP_SMOOTHING_1D_CMPLX


subroutine MAP_SMOOTHING_2D_CMPLX(nn,rn,s,MAP,sMAP)
!input map (MAP,nn) is smoothed (sMAP,rn) with a smoothing size (s)
  implicit none
  !I/O
  integer, intent(in) :: nn(2), rn(2), s
  complex(dlc), intent(in) :: MAP(:,:)
  complex(dlc), intent(out) :: sMAP(:,:)
  !internal
  integer :: i, j

  do i = 1, rn(1)
    do j = 1, rn(2)
      sMAP(i,j) = sum(MAP((i-1)*s+1:i*s,(j-1)*s+1:j*s))/dble(s**2)
    end do
  end do

end subroutine MAP_SMOOTHING_2D_CMPLX


function elarray_x(nn,D)  result(f)
! return ell_x in 2D
  implicit none
  !I/O
  integer, intent(in) :: nn(1:2)
  double precision, intent(in) :: D(1:2)
  !internal
  integer :: i, j, n
  double precision :: lx, ly, f(nn(1)*nn(2))

  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      f(n) = twopi*dble(i-1-nn(1)*0.5d0)/D(1)
      n = n + 1
    end do
  end do

end function elarray_x


function elarray(nn,D)  result(f)
! return absolute value of multipole in 2D
  implicit none
  !I/O
  integer, intent(in) :: nn(1:2)
  double precision, intent(in) :: D(1:2)
  !internal
  integer :: i, j, n
  double precision :: lx, ly, f(nn(1)*nn(2))

  n = 1
  do i = 1, nn(1)
    lx = twopi*dble(i-1-nn(1)*0.5d0)/D(1)
    do j = 1, nn(2)
      ly = twopi*dble(j-1-nn(2)*0.5d0)/D(2)
      if (lx**2+ly**2>0d0) then
        f(n) = dsqrt(lx**2+ly**2)
      else 
        f(n) = 0d0
      end if
      n = n + 1
    end do
  end do

end function elarray


function el1d(i,j,nn,D)
  implicit none
  integer, intent(in) :: i,j,nn(2)
  double precision, intent(in) :: D(2)
  double precision :: el1d

  el1d = dsqrt(lgrid(i,nn(1),D(1))**2 + lgrid(j,nn(2),D(2))**2)

end function el1d


function lgrid(i,nn,D)
  implicit none
  integer, intent(in) :: i, nn
  double precision, intent(in) :: D
  double precision :: lgrid

  lgrid = twopi*dble(i-1-nn*0.5d0)/D

end function lgrid


function ei2phi(lx,ly)
  implicit none
  double precision, intent(in) :: lx, ly
  complex(dlc) :: ei2phi

  if(lx**2+ly**2 > 0.d0) then
    ei2phi = ((lx**2-ly**2) + iu*2.d0*lx*ly)/(lx**2 + ly**2)
  else
    ei2phi = 0.d0
  end if

end function ei2phi


subroutine fourier_filter_1d(els,eL,alm,Fl,Fn)
! * assign zerros outside of annular region
! * optionally multiply a filter function
  implicit none
! [inputs]  
!   eL  --- multipole range of annular region
!   els --- multipoles as a function of pixel (npix)
!   alm --- Fourier modes
  integer, intent(in) :: eL(1:2)
  double precision, intent(in) :: els(:)
  complex(dlc), intent(inout) :: alm(:)
!
! (optional)
!   Fl --- filtering function as a function of multipole
!   Fn --- filtering function as a function of npix
  double precision, intent(in), optional :: Fl(:)
  double precision, intent(in), optional :: Fn(:)
!
! [internal]
  integer :: n, l

  if (present(Fl).and.present(Fn)) stop "error: two filters appeared (fourier filtering)"

  do n = 1, size(alm)
    l = int(els(n))
    if (l>=eL(1).and.l<=eL(2)) then
      if (present(Fl)) alm(n) = alm(n)*Fl(l)
      if (present(Fn)) alm(n) = alm(n)*Fn(n)
    else
      alm(n) = 0d0
    end if
  end do

end subroutine fourier_filter_1d


subroutine fourier_filter_2d(els,eL,alm,Fl,Fn)
! * same as "fourier_filter_1d" but for 2d array
  implicit none
! [inputs]  
!   eL  --- multipole range of annular region
!   els --- multipoles as a function of pixel (npix)
!   alm --- Fourier modes
  integer, intent(in) :: eL(1:2)
  double precision, intent(in) :: els(:)
  complex(dlc), intent(inout) :: alm(:,:)
!
! (optional)
!   Fl --- filtering function as a function of multipole
!   Fn --- filtering function as a function of npix
  double precision, intent(in), optional :: Fl(:,:)
  double precision, intent(in), optional :: Fn(:,:)
!
! [internal]
  integer :: n, l

  if (present(Fl).and.present(Fn)) stop "error: two filters appeared (fourier filtering)"

  do n = 1, size(alm,dim=2)
    l = int(els(n))
    if (l>=eL(1).and.l<=eL(2)) then
      if (present(Fl)) alm(:,n) = alm(:,n)*Fl(:,l)
      if (present(Fn)) alm(:,n) = alm(:,n)*Fn(:,n)
    else
      alm(:,n) = 0d0
    end if
  end do

end subroutine fourier_filter_2d



subroutine GAUSSIAN_ALM_2DARRAY(nn,D,iL,alm,Cl,fix)
!//// Generate Multipoles (al) in 2D Fourier space ////!
!note: satisfy a^*_l = a_{-l}
  implicit none
  !I/O
  logical, intent(in), optional :: fix
  integer, intent(in) :: iL(2), nn(2)
  double precision, intent(in) :: Cl(:), D(2)
  complex(dlc), intent(inout) :: alm(:,:)
  !internal
  integer :: i, j, n
  integer :: ijmin(2), ijmax(2)
  double precision :: els(2), amp(size(Cl)), x, y, dx, dy, d0, l

  ! check
  if(.not.size(iL)==2) stop "error (gaussian_alm) : size of iL is not 2"
 
  call InitRandom(-1)

  alm = 0d0
  amp = 0d0
  d0 = D(1)*D(2)
  do i = iL(1), iL(2)
    amp(i) = dsqrt(d0*Cl(i)*0.5d0)  ! \sqrt(\delta(l=0)*Cl(l)/2)
  end do
  if(present(fix)) amp = dsqrt(d0*Cl)

  !* alm=0 if i=1 or j=1 for symmetry
  !* center: (ic,jc) = (nn(1)/2+1, nn(2)/2+1)

  !* maximum nn
  !ijmin(:) = max ( 2, int( nn(:)*0.5d0 + 1 - iL(2)*D(:)/twopi ) )
  !ijmax(:) = min ( nn(:), int( nn(:)*0.5d0 + 1 + iL(2)*D(:)/twopi ) + 1 )
  ijmin(:) = 2
  ijmax(:) = nn(:)

  !* dx, dy in l-space
  dx = twopi/D(1)
  dy = twopi/D(2)

  !* check
  if(dx*(nn(1)*0.5-1)<iL(2).or.dy*(nn(2)*0.5-1)<iL(2)) then
    write(*,*) "error: inclusion of Fourier mode is incorrect"
    write(*,*) "maximum ell should be lower than", dx*(nn(1)*0.5-1), "or", dy*(nn(2)*0.5-1)
    stop
  end if

  ! half side (i < ic)
  do i = ijmin(1), nn(1)/2
    x = dx*dble(i-1-nn(1)*0.5d0)
    do j = ijmin(2), ijmax(2)
      y = dy*dble(j-1-nn(2)*0.5d0)
      l = dsqrt(x**2+y**2)
      if(l>=iL(1).and.l<=iL(2)) then
        if(present(fix)) then
          alm(i,j) = amp(int(l))
        else
          alm(i,j) = cmplx(Gaussian1(),Gaussian1())*amp(int(l))
        end if
      end if
    end do
  end do

  ! values on axis (i=ic) but avoid the origin (ell=0) 
  i = nn(1)/2+1
  ! x=0
  do j = ijmin(2), nn(2)/2
    y = dy*dble(j-1-nn(2)*0.5d0)
    l = abs(y)
    if(l>=iL(1).and.l<=iL(2)) then
      if(present(fix)) then
        alm(i,j) = amp(int(l))
      else
        alm(i,j) = cmplx(Gaussian1(),Gaussian1())*amp(int(l))
      end if
    end if
  end do
  do j = nn(2)/2+2, ijmax(2)
    alm(i,j) = conjg(alm(i,nn(2)-j+2))
  end do
  
  ! the other half side (i>ic)
  do i = nn(1)/2+2, ijmax(1)
    do j = ijmin(2), ijmax(2)
      alm(i,j) = conjg(alm(nn(1)-i+2,nn(2)-j+2))
    end do
  end do


end subroutine GAUSSIAN_ALM_2DARRAY


subroutine GAUSSIAN_ALM_1DARRAY(nn,D,iL,alm,Cl,fix)
!//// Generate Multipoles (al) in 1D Fourier space ////!
  implicit none
  !I/O
  logical, intent(in), optional :: fix
  integer, intent(in) :: iL(2), nn(2)
  double precision, intent(in) :: Cl(:), D(2)
  complex(dlc), intent(inout) :: alm(:)
  !internal
  integer :: i, j, n, npixc
  double precision :: amp(size(Cl)), x, y, dd(1:2), d0, l

  ! check
  if(.not.size(iL)==2) stop "error (gaussian_alm) : size of iL is not 2"
 
  call InitRandom(-1)

  alm = 0d0
  amp = 0d0
  d0 = D(1)*D(2)
  do i = iL(1), iL(2)
    amp(i) = dsqrt(d0*Cl(i)*0.5d0)  ! \sqrt(\delta(l=0)*Cl(l)/2)
  end do
  if(present(fix)) amp = dsqrt(d0*Cl)

  !* alm=0 if i=1 or j=1 for symmetry
  !* center: (ic,jc) = (nn(1)/2+1, nn(2)/2+1)
  npixc = nn(2)*nn(1)/2 + nn(2)/2 + 1

  !* dx, dy in l-space
  dd = twopi/D

  ! half side (i < ic)
  n = nn(2)+1 ! adding from j=1 to nn(2)
  do i = 2, nn(1)/2
    x = dd(1)*dble(i-1-nn(1)*0.5d0)
    n = n + 1 ! j=1
    do j = 2, nn(2)
      y = dd(2)*dble(j-1-nn(2)*0.5d0)
      l = dsqrt(x**2+y**2)
      if(l>=iL(1).and.l<=iL(2)) then
        if(present(fix)) then
          alm(n) = amp(int(l))
        else
          alm(n) = cmplx(Gaussian1(),Gaussian1())*amp(int(l))
        end if
      end if
      n = n + 1
    end do
  end do

  ! values on axis (i=ic) but avoid the origin (ell=0) 
  i = nn(1)/2+1
  ! x=0
  n = n + 1  ! j = 1
  do j = 2, nn(2)/2
    y = dd(2)*dble(j-1-nn(2)*0.5d0)
    l = abs(y)
    if(l>=iL(1).and.l<=iL(2)) then
      if(present(fix)) then
        alm(n) = amp(int(l))
      else
        alm(n) = cmplx(Gaussian1(),Gaussian1())*amp(int(l))
      end if
    end if
    n = n + 1
  end do

  !complex conjugate
  do n = npixc+1, nn(1)*nn(2)
    alm(n) = conjg(alm(2*npixc-n))
  end do
  
end subroutine GAUSSIAN_ALM_1DARRAY


subroutine GAUSSIAN_ALM_1DARRAY_OLD(nn,D,iL,alm,Cl,fix)
  !//// Generate Multipoles (al) in 2D Fourier space ////!
  !note: should satisfy a^*_l = a_{-l}
  implicit none
  !I/O
  logical, intent(in), optional ::fix
  integer, intent(in) :: iL(2), nn(2)
  double precision, intent(in) :: Cl(:), D(2)
  complex(dlc), intent(out) :: alm(:)
  !internal
  complex(dlc), dimension(nn(1),nn(2)) :: glm

  if(present(fix)) then
    call GAUSSIAN_ALM_2DARRAY(nn,D,iL,glm,Cl,fix)
  else
    call GAUSSIAN_ALM_2DARRAY(nn,D,iL,glm,Cl)
  end if
  alm = reshape(transpose(glm),(/nn(1)*nn(2)/))

end subroutine GAUSSIAN_ALM_1DARRAY_OLD


function lchi(i,j,nn,D)
  implicit none
  integer, intent(in) :: i,j,nn(2)
  double precision, intent(in) :: D(2)
  double precision :: lchi

  lchi = -atan2(lgrid(j,nn(2),D(2)),lgrid(i,nn(1),D(1))) + pi*0.5

end function lchi


subroutine spin_weight_1darray(sw,nn,D,trans,S)
  implicit none
  !I/O
  integer, intent(in) :: nn(2), trans, S
  double precision, intent(in) :: D(2)
  complex(dlc), intent(out) :: sw(:)
  !internal
  integer :: i, j, n
  double precision :: ss, x, y

  n = 1
  ss = -trans*S/abs(S)
  do i = 1, nn(1)
    x = twopi*dble(i-1-nn(1)*0.5d0)/D(1)
    do j = 1, nn(2)
      y = twopi*dble(j-1-nn(2)*0.5d0)/D(2)
      if(.not.x*y==0.d0) sw(n) = sw(n) &
          * ((x**2-y**2)+iu*2.d0*ss*x*y)/(x**2+y**2)
      n = n + 1
    end do
  end do

end subroutine spin_weight_1darray


subroutine spin_weight_2darray(sw,nn,D,trans,S)
  implicit none
  !I/O
  integer, intent(in) :: nn(2), trans, S
  double precision, intent(in) :: D(2)
  complex(dlc), intent(out) :: sw(:,:)
  !internal
  integer :: i, j, n
  double precision :: ss, x, y

  ss = -trans*S/abs(S)
  do i = 1, nn(1)
    x = twopi*dble(i-1-nn(1)*0.5d0)/D(1)
    do j = 1, nn(2)
      y = twopi*dble(j-1-nn(2)*0.5d0)/D(2)
      if(.not.x*y==0.d0) sw(i,j) = sw(i,j) &
          * ((x**2-y**2)+iu*2.d0*ss*x*y)/(x**2+y**2)
    end do
  end do

end subroutine spin_weight_2darray


subroutine WINDOW_FLAT(nn,D,Wf,ap)
  implicit none
  !I/O
  integer, intent(in) :: nn(1:2)
  double precision, intent(in) :: D(1:2)
  double precision, intent(in), optional :: ap
  double precision, intent(out) :: Wf(:)
  !internal
  integer :: i, j, n
  double precision :: xi, xj, dx, dy, sx ,sy

  sx = D(1)/dble(nn(1))
  sy = D(2)/dble(nn(2))

  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      xi = dble(i-1-(nn(1)-1)*0.5)*sx
      xj = dble(j-1-(nn(2)-1)*0.5)*sy
      dx = abs(xi)
      dy = abs(xj)
      if(present(ap)) then
        Wf(n) = Wap(D(1),dx,ap)*Wap(D(2),dy,ap)
      else
        Wf(n) = Wap(D(1),dx)*Wap(D(2),dy)
      end if
      n = n + 1
    end do
  end do

end subroutine WINDOW_FLAT


subroutine window_norm(W,Wn)
  implicit none
  double precision, intent(in) :: W(:)
  double precision, intent(out) :: Wn(:)
  integer :: n, npix

  npix = size(W)
  do n = 1, size(Wn)
    Wn(n) = sum(W**n)/dble(npix)
  end do

end subroutine window_norm


subroutine window_norm_multi(W1,W2,Wn)
  implicit none
  double precision, intent(in) :: W1(:), W2(:)
  double precision, intent(out) :: Wn(0:5,0:5)
  integer :: n, m, npix

  npix = size(W1)
  do n = 0, size(Wn,dim=1)-1
    do m = 0, size(Wn,dim=2)-1
      Wn(n,m) = sum(W1**n*W2**m)/dble(npix)
    end do
  end do

end subroutine window_norm_multi


subroutine window_generate(nn,D,Wf,mr,mn,f,ap,cut)
  implicit none
  !I/O
  character(*), intent(in), optional :: f
  integer, intent(in) :: nn(2)
  integer, intent(in), optional :: mn
  double precision, intent(in) :: D(2)
  double precision, intent(in), optional :: mr, ap, cut
  double precision, intent(inout) :: Wf(:)
  !internal
  integer :: i, j, n, m, maskn
  double precision :: xi, xj, dx, dy, sx ,sy
  double precision, allocatable :: px(:), py(:)
  double precision, dimension(nn(1),nn(2)) :: mask

  mask = 1d0
  sx = D(1)/dble(nn(1))
  sy = D(2)/dble(nn(2))

  if(present(mr).and..not.mr==0) then
    if(present(f)) then
      write(*,*) "read masked points from a file"
      open(unit=20,file=trim(adjustl(f)),status="old")
      maskn = FileLines(20)
      write(*,*) "mask points =", maskn
      allocate(px(maskn),py(maskn))
      do m = 1, maskn
        read(20,*) i, px(m), py(m)
      end do
      close(20)
    else if(present(mn)) then
      maskn = mn
      allocate(px(maskn),py(maskn))
      call MASK_GENERATE(nn,D,maskn,px,py,"genpoints.dat")
    else
      stop "error: require number of masks to be generated"
    end if

    do i = 1, nn(1)
      do j = 1, nn(2)
        do m = 1, maskn
          xi = dble(i-1-(nn(1)-1)*0.5)*sx
          xj = dble(j-1-(nn(2)-1)*0.5)*sy
          dx = abs(xi-px(m))
          dy = abs(xj-py(m))
          mask(i,j) = (1d0-Mwin(mr*sx,dx)*Mwin(mr*sy,dy))*mask(i,j)
        end do
      end do
    end do
    deallocate(px,py)
  end if

  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      xi = dble(i-1-(nn(1)-1)*0.5)*sx
      xj = dble(j-1-(nn(2)-1)*0.5)*sy
      dx = abs(xi)
      dy = abs(xj)
      if(present(ap).and..not.present(cut)) then
        Wf(n) = Wap(D(1),dx,ap)*Wap(D(2),dy,ap)*mask(i,j)
      else if(.not.present(ap).and.present(cut)) then
        Wf(n) = Wap(D(1),dx,skycut=cut)*Wap(D(2),dy,skycut=cut)*mask(i,j)
      else if(present(ap).and.present(cut)) then
        Wf(n) = Wap(D(1),dx,ap,cut)*Wap(D(2),dy,ap,cut)*mask(i,j)
      else
        Wf(n) = Wap(D(1),dx)*Wap(D(2),dy)*mask(i,j)
      end if
      n = n + 1
    end do
  end do

end subroutine window_generate


subroutine MASK_GENERATE(nn,D,Nm,px,py,f)
  implicit none
  !I/O
  character(*), intent(in), optional :: f
  integer, intent(in) :: nn(2), Nm
  double precision, intent(in) :: D(2)
  double precision, intent(out) :: px(:), py(:)
  !internal
  integer :: i, j, n, m

  m = Nm
  write(*,*) "generate masked points"
  call InitRandom(-1)
  n = 1
  do i = 1, nn(1)
    do j = 1, nn(2)
      if(m>0.and.dble(m)/dble(nn(1)*nn(2)-n)>ranmar()) then
        px(m) = dble(i-1-(nn(1)-1)*0.5)*D(1)/dble(nn(1))
        py(m) = dble(j-1-(nn(2)-1)*0.5)*D(2)/dble(nn(2))
        m = m - 1
      end if
      n = n + 1
    end do
  end do

  if(present(f)) then
    write(*,*) "output masked points"
    open(unit=20,file=f,status="replace")
    do i = 1, Nm
      write(20,"(I3,2(1X,E12.5))") i, px(i), py(i)
    end do
    close(20)
  end if

end subroutine MASK_GENERATE


function WAP(mapsize,s,apfactor,skycut)
!* 1D window function
  implicit none
  !I/O
  double precision, intent(in) :: mapsize, s
  double precision, intent(in), optional :: skycut, apfactor
  !internal
  double precision :: s0, a, Wap, ss, x

  !map size
  a = mapsize*0.5d0
  if(present(skycut)) a = a*skycut

  !apodization length
  s0 = 1d0
  if(present(apfactor)) s0 = apfactor

  !normalized coordinate
  ss = abs(s)/a
  if(.not.s0==1d0) x = (1d0-ss)/(1d0-s0)
  
  !window function
  if (ss<s0) then
    Wap = 1d0
  else if (ss>=s0.and.ss<1d0) then
    Wap = x - dsin(2*pi*x)/(2*pi) 
  else 
    Wap = 0d0
  end if

end function WAP


function Mwin(mr,t,maskfactor) 
!* apodized mask window function
  implicit none
  !I/O
  double precision, intent(in) :: mr, t
  double precision, intent(in), optional :: maskfactor
  !internal
  double precision :: t0, Mwin, x, b

  b = mr
  if(present(maskfactor)) b = b*maskfactor

  if (t<mr) then
    Mwin = 1d0
  else if (t>=mr.and.t<b.and..not.b==mr) then
    x = (b-t)/(b-mr)
    Mwin = x - sin(2*pi*x)/(2*pi)
  else 
    Mwin = 0d0
  end if

end function Mwin


end module anaflat

