function read_phi()
% function [] = read_phi()
% 
% Reading lensing potential obtained from the SPT simulation
%

% root directory where the phi maps are saved
root = '/net/bicepfs2/bicepfs2/keck/pipeline/maps/3200/BKSPT/lensrec/wplm/';

% number of girds for the SPT simulation
m.nx = 236*3;
m.ny = 100*3;

% set file string
strtype = '3g_'
%strtype = ''
tag = strcat('chi_lx30_lxb1000_',strtype,'l30-1500_lB150')

% loop for 50 realizations
for z = 1:50,

  filename = strcat(root,tag,'_r',int2str(z),'.dat')

  [EE,EB,MV,WEE,WEB,WMV] = textread(filename,'%f %f %f %f %f %f');
  phi.EE = reshape(EE,[m.ny,m.nx]);
  phi.EB = reshape(EB,[m.ny,m.nx]);
  phi.MV = reshape(MV,[m.ny,m.nx]);
  W.EE = reshape(WEE,[m.ny,m.nx]);
  W.EB = reshape(WEB,[m.ny,m.nx]);
  W.MV = reshape(WMV,[m.ny,m.nx]);

  % check
  %disp((r1(150,400)^2+i1(150,400)^2)^0.5*1e7)

  % save to file
  save(strcat(root,tag,'_r',int2str(z),'.mat'),'m','phi','W')

end

