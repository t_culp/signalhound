!///////////////////////////////////////////////////////////////////////!
! * Lensing reconstruction/ Delensing tools
! * Toshiya Namikawa
! - Last Modified: Sat 14 May 2016 05:25:50 PM EDT
!///////////////////////////////////////////////////////////////////////!

module recbk
  use myconst, only: dlc, pi, Tcmb
  use myutils, only: savetxt, loadtxt, meanvar
  use mycls,   only: binned_ells, alm2bcl_flat, cb2c2d
  use anaflat, only: elarray, elarray_x
  use myfftw,  only: DFT, DFT_POL, pureEB
  use recflat, only: quadee, quadeb, lensingb
  implicit none

  private dlc, pi, Tcmb
  private savetxt, loadtxt, meanvar
  private binned_ells, alm2bcl_flat, cb2c2d
  private elarray, elarray_x
  private DFT, DFT_POL, pureEB
  private quadee, quadeb, lensingb

contains

subroutine delensing_bias(I,simn,alm,falm,nn,D,rL,dL,els,C2d,blm,Tcmb,Fl,Gl)
  implicit none
  !I/O
  character(*), intent(in) :: falm(:)
  integer, intent(in) :: I, nn(2), rL(2), dL(2), simn
  double precision, intent(in) :: D(2), els(:), C2d(:) !CEE
  double precision, intent(in), optional :: Tcmb, Fl(:,:), Gl(:)
  complex(dlc), intent(in) :: alm(:)
  complex(dlc), intent(out) :: blm(:)
  !Internal
  integer :: npix, j, n
  complex(dlc), dimension(:,:), allocatable :: slm, tlm

  npix = nn(1)*nn(2)
  write(*,*) "compute delening bias"
  blm = 0d0

  do j = 2, simn

    if (j/=I) then
      allocate(slm(2,npix),tlm(2,npix)); slm=0d0; tlm=0d0
      call loadtxt(falm(j),slm)
      tlm(2,:) = alm
      if (present(Fl)) then
        slm = slm*Fl
        tlm = tlm*Fl
      end if
      CALL QUADEB(nn,D,slm(1,:),tlm(2,:),C2d,rL,glm=tlm(1,:))

      !* wiener filtering
      slm(1,:) = slm(1,:) * C2d
      if (present(Tcmb)) slm = slm*Tcmb

      !* delensing bias
      if (present(Gl))  tlm(1,:) = tlm(1,:)*Gl
      call LensingB(nn,D,slm(1,:),tlm(1,:),dL,tlm(2,:))
      if (I/=1) blm = blm + tlm(2,:)/dble(simn-1)
      if (I==1) blm = blm + tlm(2,:)/dble(simn)
      deallocate(slm,tlm)
    end if

  end do

end subroutine delensing_bias


subroutine map2alm(fmap,falm,nn,D,FF,W,Cl2d,pEB,f,bn,spt3g,sim,dust,oL)
  implicit none
  logical, intent(in), optional :: spt3g, dust
  character(*), intent(in) :: fmap(:,:), falm(:,:), f(1:2), pEB
  integer, intent(in) :: nn(1:2)
  integer, intent(in), optional :: bn
  double precision, intent(in) :: D(2), W(:), Cl2d(:,:), FF(:,:)
  integer, intent(in), optional :: sim, oL(1:2)

  integer :: npix, bmax=100, i, j, n, simn=51, eL(2)
  double precision :: W2
  double precision, allocatable, dimension(:) :: els, bp
  double precision, allocatable, dimension(:,:) :: C2d, bc, mCb, vCb, fl, Cl
  double precision, allocatable, dimension(:,:,:) :: Cb
  complex(dlc), allocatable :: alm(:,:)

  npix = nn(1)*nn(2)
  W2   = sum(W**2)/dble(npix)

  eL = [20,700]
  if (present(bn))   bmax = bn
  if (present(oL))   eL = oL
  if (present(sim))  simn = sim

  allocate(bp(bmax+1),bc(1,bmax))
  call binned_ells(eL,bp,bc(1,:))

  allocate(els(npix))
  els = elarray(nn,D)

  allocate(Cb(simn,6,bmax),C2d(6,npix));  Cb=0d0;  C2d=0d0

  do i = 1, simn

    write(*,*) i, "read Elm and Blm"
    allocate(alm(6,npix)); alm=0d0
    if (i==1) then
      call data2alm(nn,D,fmap(1,i),alm(1:2,:),W,pEB,falm(1,i))
      do j = 1, 2
        alm(j,:) = alm(j,:)*(els*(els+1d0)/(2*pi))**0.5
        call alm2bcl_flat(bmax,eL,els,D,alm(j,:),Cb=Cb(i,j,:),norm=1d0/W2)
      end do
    else
      call data2alm(nn,D,fmap(1,i),alm(1:2,:),W,pEB)
      call data2alm(nn,D,fmap(2,i),alm(3:4,:),W,pEB)
      if (present(dust).and.dust)  call data2alm(nn,D,fmap(3,i),alm(5:6,:),W,pEB)
      call savetxt(falm(2,i),alm(1:2,:))
      call savetxt(falm(3,i),alm(3:4,:))
      if (present(spt3g).and.spt3g)  alm(3:4,:) = alm(3:4,:)*dsqrt(0.1d0)
      call savetxt(falm(1,i),alm(1:2,:)+alm(3:4,:)+alm(5:6,:))
      !call fourier_filter(els,eL,alm) !filtering
      C2d = C2d + abs(alm)**2/dble(simn-1)/(D(1)*D(2)*W2) !2D cls
      !* 1D cls (only for check)
      do j = 1, 6
        alm(j,:) = alm(j,:)*(els*(els+1d0)/(2*pi))**0.5
        call alm2bcl_flat(bmax,eL,els,D,alm(j,:),Cb=Cb(i,j,:),norm=1d0/W2)
      end do
    end if

    deallocate(alm)

  end do

  !* 2d Cls and filtering function
  allocate(fl(2,npix),Cl(2,npix))
  fl=0d0;  Cl=0d0
  do n = 1, npix
    if (FF(1,n)/=0d0)  fl(1,n) = dsqrt((C2d(1,n))/Cl2d(1,n))
    if (FF(2,n)/=0d0)  fl(2,n) = dsqrt((C2d(2,n))/Cl2d(2,n))
  end do
  do i = 1, 3
    C2d(2*i-1,:) = C2d(2*i-1,:)*FF(1,:)
    C2d(2*i,:)   = C2d(2*i,:)*FF(2,:)
  end do
  call savetxt(f(1),C2d,fl,Cl2d)
  deallocate(C2d,els,fl,Cl)

  !* output binned 1d angular power spectrum 
  allocate(mCb(6,bmax),vCb(6,bmax))
  do i = 1, 6
    call meanvar(Cb(2:simn,i,:),mCb(i,:),vCb(i,:)) 
  end do
  call savetxt(f(2),bc,mCb,vCb,Cb(1,1:2,:))
  deallocate(mCb,vCb,Cb,bp,bc)

end subroutine map2alm


subroutine n0_bias(falm,simn,nn,D,rL,fC,N0,norm,lfac,Fl,f)
  implicit none
  !I/O
  character(*), intent(in) :: falm(:)
  character(*), intent(in), optional :: f
  integer, intent(in) :: simn, nn(2), rL(2)
  double precision, intent(in) :: D(2), fC(:)
  double precision, intent(in), optional :: norm, lfac(:), Fl(:,:)
  double precision, intent(out) :: N0(:,:)
  !Internal
  integer :: npix, i, j, n
  complex(dlc), dimension(:,:), allocatable :: alm1,alm2,est12,est21

  npix = nn(1)*nn(2)
  N0 = 0d0

  write(*,*) "compute N0 bias"

  do i = 2, simn

    write(*,*) i, i+simn
    allocate(alm1(2,npix),alm2(2,npix)); alm1=0d0; alm2=0d0
    call loadtxt(falm(i),alm1)
    call loadtxt(falm(i+simn),alm2)
    if (present(Fl)) then
      alm1 = alm1*Fl
      alm2 = alm2*Fl
    end if

    !* compute unnormalized estimators
    allocate(est12(4,npix),est21(4,npix)) ;  est12 = 0d0 ;  est21 = 0d0

    !EE
    CALL QUADEE(nn,D,alm1(1,:),alm2(1,:),fC,rL,est12(1,:),est12(3,:))
    CALL QUADEE(nn,D,alm2(1,:),alm1(1,:),fC,rL,est21(1,:),est21(3,:))

    !EB
    CALL QUADEB(nn,D,alm1(1,:),alm2(2,:),fC,rL,est12(2,:),est12(4,:))
    CALL QUADEB(nn,D,alm2(1,:),alm1(2,:),fC,rL,est21(2,:),est21(4,:))

    deallocate(alm1,alm2)

    !* 2d N0 bias
    est12 = est12 + est21
    if (present(lfac)) then
      do j = 1, 4
        est12(j,:) = est12(j,:) * lfac
      end do
    end if
    N0(1:4,:) = N0(1:4,:) + abs(est12)**2*0.5d0/((simn-1)*D(1)*D(2))
    deallocate(est12,est21)

  end do

  N0(5,:) = N0(1,:) + N0(2,:)
  N0(6,:) = N0(3,:) + N0(4,:)

  if (present(norm)) N0 = N0*norm
  if (present(f)) call savetxt(trim(f),N0)

end subroutine n0_bias


subroutine rdn0_bias(falm,simn,nn,D,rL,fC,I,alm,RDN0,norm,lfac,Fl)
  implicit none
  !I/O
  character(*), intent(in) :: falm(:)
  integer, intent(in) :: nn(2), rL(2), I, simn
  double precision, intent(in) :: D(2), fC(:)
  double precision, intent(in), optional :: norm, lfac(:), Fl(:,:)
  complex(dlc), intent(in) :: alm(:,:)
  double precision, intent(out) :: RDN0(:,:)
  !Internal
  integer :: npix, j, n, k
  double precision :: d0
  complex(dlc), dimension(:,:), allocatable :: est12, est21, slm

  npix = nn(1)*nn(2)
  d0 = D(1)*D(2)
  RDN0 = 0d0

  write(*,*) "computing RDN0"
  do j = 2, simn+1

    if (j/=I) then
      allocate(slm(2,npix));  slm = 0d0
      call loadtxt(falm(j),slm)
      if (present(Fl)) slm = slm * Fl

      !* compute unnormalized estimators
      allocate(est12(4,npix),est21(4,npix));  est12 = 0d0;  est21 = 0d0

      !EE
      CALL QUADEE(nn,D,slm(1,:),alm(1,:),fC,rL,est12(1,:),est12(3,:))
      CALL QUADEE(nn,D,alm(1,:),slm(1,:),fC,rL,est21(1,:),est21(3,:))

      !EB
      CALL QUADEB(nn,D,slm(1,:),alm(2,:),fC,rL,est12(2,:),est12(4,:))
      CALL QUADEB(nn,D,alm(1,:),slm(2,:),fC,rL,est21(2,:),est21(4,:))

      deallocate(slm)

      est12 = est12 + est21
      if (present(lfac)) then
        do k = 1, 4
          est12(k,:) = est12(k,:) * lfac
        end do
      end if

      !* 2D power spectrum
      if (I/=1) RDN0 = RDN0 + abs(est12)**2/(d0*dble(simn-1))
      if (I==1) RDN0 = RDN0 + abs(est12)**2/(d0*dble(simn))

      deallocate(est12,est21)

    end if

  end do

  if (present(norm))  RDN0 = RDN0*norm

end subroutine rdn0_bias


subroutine mean_field(falm,simn,nn,D,rL,fC,mest,lfac,Fl,f)
  implicit none
  !I/O
  character(*), intent(in) :: falm(:)
  character(*), intent(in), optional :: f
  integer, intent(in) :: nn(2), rL(2), simn
  double precision, intent(in) :: D(2), fC(:)
  double precision, intent(in), optional :: lfac(:), Fl(:,:)
  complex(dlc), intent(out) :: mest(:,:)
  !Internal
  integer :: npix, i, j
  complex(dlc), dimension(:,:), allocatable :: alm, est

  npix = nn(1)*nn(2)
  mest = 0d0

  write(*,*) "computing mean-field bias"

  do i = 2, simn+1

    allocate(alm(2,npix));  alm = 0d0
    call loadtxt(falm(i),alm)
    if (present(Fl)) alm = alm * Fl

    !* compute unnormalized estimators
    allocate(est(4,npix));  est = 0d0
    CALL QUADEE(nn,D,alm(1,:),alm(1,:),fC,rL,est(1,:),est(3,:))
    CALL QUADEB(nn,D,alm(1,:),alm(2,:),fC,rL,est(2,:),est(4,:))
    deallocate(alm)

    !* to unnormalized convergence estimators
    if (present(lfac)) then
      do j = 1, 4
        est(j,:) = est(j,:) * lfac
      end do
    end if
    mest = mest + est/dble(simn)

    deallocate(est)

  end do

  if (present(f))  call savetxt(f,mest)

end subroutine mean_field


subroutine calc_transfer(falm,gmap,nn,D,rL,eL,W,fC,els,Fl,Gl,Tl,f,mask,sim,bns)
!* Computing transfer function
!   T^B = <k^inpB k^inpB>/<k^recB k^inpB>
!   Ckk = <k^inpB k^inpB>
  implicit none
  !I/O
  character(*), intent(in) :: falm(:), gmap(:), f(2)
  character(*), intent(in), optional :: mask
  integer, intent(in) :: nn(2), rL(2), eL(2)
  integer, intent(in), optional :: sim, bns
  double precision, intent(in) :: D(2), W(:), fC(:), els(:), Fl(:,:), Gl(:,:)
  double precision, intent(out) :: Tl(:,:)
  !Internal
  integer :: npix, i, n, cln=4, bmax=100, simn=499
  double precision :: W2, W3
  double precision, allocatable :: bp(:), bc(:,:), Cb(:,:,:), mCb(:,:), vCb(:,:), M(:), C2d(:,:)
  complex(dlc), dimension(:,:), allocatable :: alm, est, glm

  write(*,*) "computing transfer function and projected Ckk"

  if (present(sim))  simn = sim
  if (present(bns))  bmax = bns

  !* binned multipoles
  allocate(bp(bmax+1),bc(1,bmax))
  call binned_ells(eL,bp,bc(1,:))

  npix = nn(1)*nn(2)
  W2 = sum(W**2)/dble(npix)
  W3 = sum(W**3)/dble(npix)

  allocate(Cb(simn,cln,bmax));  Cb=0d0

  do i = 1, simn
    write(*,*) i
    !* compute estimator
    allocate(alm(2,npix),est(2,npix),glm(2,npix));  alm=0d0;  est=0d0;  glm = 0d0
    call loadtxt(falm(i+1),alm)
    alm = alm * Fl
    CALL QUADEE(nn,D,alm(1,:),alm(1,:),fC,rL,est(1,:))
    CALL QUADEB(nn,D,alm(1,:),alm(2,:),fC,rL,est(2,:))
    est = est * Gl
    call data2glm(nn,D,gmap(i+1),glm(1,:),W)  !load input kappa
    !* 1D power
    call alm2bcl_flat(bmax,eL,els,D,glm(1,:),est(1,:),Cb=Cb(i,1,:),norm=1d0/W3) !EE x input
    call alm2bcl_flat(bmax,eL,els,D,glm(1,:),est(2,:),Cb=Cb(i,2,:),norm=1d0/W3) !EB x input
    call alm2bcl_flat(bmax,eL,els,D,glm(1,:),Cb=Cb(i,4,:),norm=1d0/W2)          !input
    deallocate(glm,alm,est)
  end do

  if (present(mask)) then
    allocate(M(npix),glm(2,npix))
    call loadtxt(mask,M)
    do i = 1, simn
      call data2glm(nn,D,gmap(i+1),glm(1,:),W)    !load input kappa
      call data2glm(nn,D,gmap(i+1),glm(2,:),W,M)  !masked input
      call alm2bcl_flat(bmax,eL,els,D,glm(1,:),glm(2,:),Cb=Cb(i,3,:),norm=1d0/W2) !mask x input
    end do
    deallocate(M,glm)
  end if

  !* averaged cls
  allocate(mCb(cln,bmax),vCb(cln,bmax))
  do i = 1, cln
    call meanvar(Cb(:,i,:),mCb(i,:),vCb(i,:))
    do n = 1, bmax
      if (mCb(i,n)<0d0) then
        write(*,*) "waring: not enough MC realizations or too finite binning, force to assing non-zero value", n
        mCb(i,n) = mCb(i,n-1)
      end if
    end do
  end do
  call savetxt(f(1),bc,mCb,vCb)

  !* interpolation to 2d Cl
  allocate(C2d(cln,npix));  C2d=0d0
  do i = 1, cln
    call cb2c2d(bc(1,:),els,eL,mCb(i,:),C2d(i,:))
  end do

  !* get transfer function
  Tl = 0d0
  do n = 1, npix
    if(C2d(1,n)>0d0)  Tl(1:2,n) = C2d(4,n)/C2d(1:2,n) 
    if(C2d(3,n)>0d0)  Tl(3,n)   = C2d(4,n)/C2d(3,n) 
  end do
  call savetxt(f(2),Tl)

  deallocate(Cb,mCb,vCb,bp,bc,C2d)

end subroutine calc_transfer


subroutine calc_optfilt(falm,gmap,nn,D,rL,eL,W,fC,els,lfac,theta,A2d,mest,Tl,Fl,sig,f)
  implicit none
  !I/O
  character(*), intent(in) :: falm(:), gmap(:,:), f
  integer, intent(in) :: nn(2), rL(2), eL(2)
  double precision, intent(in) :: D(2), W(:), fC(:), els(:), lfac(:), A2d(:,:), Tl(:,:), theta, Fl(:,:)
  double precision, intent(out) :: sig(:,:)
  complex(dlc), intent(in) :: mest(:)
  !Internal
  integer :: npix, i, j, n, l, cln=3, bmax=100, simn=100
  double precision :: d0, W2, W4
  double precision, allocatable :: bp(:), bc(:,:), Cb(:,:,:), mCb(:,:), vCb(:,:), C2d(:,:)
  complex(dlc), dimension(:), allocatable :: est, glm, klm
  complex(dlc), dimension(:,:), allocatable :: alm

  allocate(bp(bmax+1),bc(1,bmax))
  call binned_ells(eL,bp,bc(1,:))
  npix = nn(1)*nn(2)
  W2 = sum(W**2)/dble(npix)
  W4 = sum(W**4)/dble(npix)

  allocate(Cb(simn,cln,bmax));  Cb = 0d0
  write(*,*) "computing optimal filtering"
  do i = 2, simn+1
    write(*,*) i
    allocate(alm(2,npix));  alm = 0d0
    call loadtxt(falm(i),alm)
    alm = alm * Fl
    allocate(est(npix),glm(npix),klm(npix)); est=0d0; glm=0d0; klm=0d0
    CALL QUADEB(nn,D,alm(1,:),alm(2,:),fC,rL,est)
    est = ( est*lfac - mest ) * A2d(2,:)
    call data2glm(nn,D,gmap(1,i),klm,W)  !
    call data2glm(nn,D,gmap(2,i),glm,W)  ! reading Planck kappa
    glm = glm*Tl(3,:)
    call alm2bcl_flat(bmax,eL,els,D,est,Cb=Cb(i,1,:),norm=1d0/W4)
    call alm2bcl_flat(bmax,eL,els,D,glm,Cb=Cb(i,2,:),norm=1d0/W2)
    call alm2bcl_flat(bmax,eL,els,D,klm,Cb=Cb(i,3,:),norm=1d0/W2)
    deallocate(glm,alm,est,klm)
  end do

  !* averaged cls
  allocate(mCb(cln,bmax),vCb(cln,bmax))
  do j = 1, cln
    call meanvar(Cb(:,j,:),mCb(j,:),vCb(j,:))
  end do

  !* interpolation to multipoles
  allocate(C2d(cln,npix));  C2d=0d0
  do i = 1, cln
    call cb2c2d(bc(1,:),els,eL,mCb(i,:),C2d(i,:))
  end do

  sig = 0d0
  do n = 1, npix
    if (C2d(1,n)>0d0) then
      sig(1:2,n) = 1d0/(C2d(1:2,n)-C2d(3,n)) 
      sig(3,n)   = 1d0/(sig(1,n)+sig(2,n))
      sig(4,n)   = dexp(-els(n)**2*theta**2/16d0/dlog(2d0))
      sig(5:6,n) = C2d(3,n)/C2d(1:2,n)
    end if
  end do
  call savetxt(f,sig)

  deallocate(Cb,mCb,vCb,bp,bc)

end subroutine calc_optfilt


subroutine set_lfactor(nn,D,dL,els,lfac,elx)
  implicit none
  integer, intent(in) :: nn(1:2), dL(1:2)
  double precision, intent(in) :: D(1:2)
  double precision, intent(out) :: els(:), lfac(:)
  double precision, intent(out), optional :: elx(:)
  integer :: n

  els = elarray(nn,D)
  do n = 1, nn(1)*nn(2)
    if (dL(1)<=els(n).and.els(n)<=dL(2)) lfac(n) = 2d0/els(n)**2
  end do
  if (present(elx))  elx = elarray_x(nn,D)

end subroutine set_lfactor


subroutine data2glm(nn,D,f,glm,W,M)
  implicit none
  !I/O
  integer, intent(in) :: nn(1:2)
  character(*), intent(in) :: f
  double precision, intent(in) :: D(1:2), W(:)
  double precision, intent(in), optional :: M(:)
  complex(dlc), intent(out) :: glm(:)
  !internal
  integer :: n, npix
  double precision, allocatable :: map(:)
  complex(dlc), allocatable :: rlm(:)

  glm = 0d0
  npix = nn(1)*nn(2)
  allocate(map(npix));  map = 0d0

  call loadtxt(f,map)

  allocate(rlm(npix));  rlm = 0d0
  rlm = map*W
  if (present(M)) rlm = rlm*M
  deallocate(map)
  call DFT(rlm,nn,D,1)
  glm = rlm
  deallocate(rlm)

end subroutine data2glm


subroutine data2alm(nn,D,f,alm,W,pEB,of)
  implicit none
  !I/O
  integer, intent(in) :: nn(2)
  character(*), intent(in), optional :: of
  character(*), intent(in) :: f, pEB
  double precision, intent(in) :: D(2), W(:)
  complex(dlc), intent(out) :: alm(:,:)
  !internal
  integer :: npix, j
  double precision, allocatable :: map(:,:)
  complex(dlc), allocatable :: rlm(:,:)

  alm = 0d0
  npix = nn(1)*nn(2)

  if (pEB=="matrix") then
    allocate(rlm(2,npix),map(npix,4));  rlm = 0d0;  map = 0d0
    call loadtxt(f,map,trans=.true.)
    map(:,2) = -map(:,2)
    map(:,4) = -map(:,4)
    ! apodization
    do j = 1, 4
      map(:,j) = map(:,j)*W(:)
    end do
    call DFT_POL(map(:,1:2),nn,D,rlm,1)
    alm(1,:) = rlm(1,:)
    call DFT_POL(map(:,3:4),nn,D,rlm,1)
    alm(2,:) = rlm(2,:)
  else
    allocate(rlm(2,npix),map(npix,2));  rlm = 0d0;  map = 0d0
    call loadtxt(f,map,trans=.true.)
    map(:,2) = -map(:,2) !IAU -> Healpix
    if (pEB=="smith") then
      call pureEB(map,nn,D,rlm,W)
    else
      do j = 1, 2
        map(:,j) = map(:,j)*W(:)
      end do
      call DFT_POL(map,nn,D,rlm,1)
    end if
    alm = rlm
  end if

  if (present(of)) call savetxt(of,alm)

  deallocate(map,rlm)

end subroutine data2alm

end module recbk


