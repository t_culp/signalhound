#///////////////////////////////////////////////////////////#
# * Plot 1D Cls
# - Last Modified: Tue 10 May 2016 11:52:07 AM EDT
#///////////////////////////////////////////////////////////#

import numpy as np
from matplotlib.pyplot import *

b,sEE,sBB,nEE,nBB,dEE,dBB,vsE,vsB,vnE,vnB,vdE,vdB,oEE,oBB = np.loadtxt('/n/bicepfs3/users/namikawa/BK14/lensrec/derived/cls/1var_lx30_1d_b50.dat').T
mEE = sEE+nEE+dEE
mBB = sBB+nBB+dBB
vEE = np.sqrt(vsE**2+vnE**2+vdE**2)
vBB = np.sqrt(vsB**2+vnB**2+vdB**2)
xlim(150,np.max(b))
ylim(1e-2,3)
yscale("log")
xlabel('$\ell$',fontsize=15)
ylabel(r'$\ell(\ell+1)C_\ell/2\pi$ [$\mu$K$^2$]',fontsize=15)
plot(b,oEE,'g-',label='$EE$',lw=2)
plot(b,mEE,'g--',lw=1)
fill_between(b,mEE-vEE,mEE+vEE,facecolor='g',edgecolor='g',lw=2,alpha=.3)
plot(b,oBB,'b-',label='$BB$',lw=2)
plot(b,mBB,'b--',lw=1)
fill_between(b,mBB-vBB,mBB+vBB,facecolor='b',edgecolor='b',lw=2,alpha=.3)
figtext(0.15,0.4,'solid: observation')
figtext(0.15,0.35,'dashed: simulation (lensed-$\Lambda$CDM+dust+noise)')
legend(loc=1)
savefig('test.png')
show()

