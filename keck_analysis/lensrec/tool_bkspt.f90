!///////////////////////////////////////////////////////////////////////!
! * subroutines
! * Toshiya Namikawa
! - Last Modified: Sat 28 Nov 2015 03:35:17 PM PST
!///////////////////////////////////////////////////////////////////////!

module tool_bkspt
  use myconst, only: dlc, TT, EE, BB, TE, pi, Tcmb, iu
  use myutils, only: savetxt, loadtxt, meanvar, str, linspace
  use mycls,   only: alm2bcl_flat, calcbcl_flat, binned_ells, cb2cl
  use anaflat, only: elarray, elarray_x, array_extend
  use recflat, only: quadee, quadeb, LensingB
  use myfftw,  only: DFT_POL, DFT, pureEB
  use bkutils, only: delensed_cls
  use recbk,   only: data2alm, data2glm, set_lfactor
  implicit none
  integer, parameter :: gEE=1, gEB=2, cEE=3, cEB=4, gMV=5, cMV=6

  private dlc, TT, EE, BB, TE, pi, Tcmb, iu
  private savetxt, loadtxt, meanvar, str, linspace
  private alm2bcl_flat, calcbcl_flat, binned_ells, cb2cl
  private elarray, elarray_x, array_extend
  private quadee, quadeb, LensingB
  private DFT_POL, DFT, pureEB
  private delensed_cls
  private data2glm, data2alm, set_lfactor

contains


subroutine delensing_filter_bkspt(falm,f,dL,rL,nn,D,CE1d,Fl,flbk,Gl,A2d,mest,Bl,FF)
  implicit none
  character(*), intent(in) :: falm(:,:), f(1:2)
  integer, intent(in) :: nn(2), dL(2), rL(2)
  double precision, intent(in) :: D(2), Fl(:,:), flbk(:), CE1d(:), Gl(:,:), A2d(:,:), FF(:)
  complex(dlc), intent(in) :: mest(:,:)
  double precision, intent(out) :: Bl(:,:)
  integer :: npix, bmax=40, simn=50, i, n, cln=7, eL(2)
  double precision, allocatable :: bp(:), bc(:,:), Yb(:,:,:), lfac(:), els(:), mCb(:,:), vCb(:,:), iC(:,:), elx(:)
  complex(dlc), allocatable, dimension(:) :: dlm
  complex(dlc), allocatable, dimension(:,:) :: alm, wlm, bkalm, est, blm

  npix = nn(1)*nn(2)
  eL(1) = min(rL(1),dL(1))
  eL(2) = max(rL(2),dL(2))

  allocate(els(npix),lfac(npix),elx(npix))
  call set_lfactor(nn,D,dL,els,lfac,elx)

  allocate(bp(bmax+1),bc(1,bmax),Yb(simn,cln,bmax));  Yb=0d0
  call binned_ells(eL,bp,bc(1,:))

  do i = 1, simn
    write(*,*) i
    allocate(alm(2,npix),wlm(2,npix),dlm(npix)); alm=0d0; wlm=0d0; dlm=0d0
    call loadtxt(falm(1,i+1),alm)
    !* wiener filtering for delensing
    do n = 1, npix
      if (dL(1)<=els(n).and.els(n)<=dL(2))  wlm(1,n) = alm(1,n) * Fl(1,n) * CE1d(int(els(n))) * Tcmb
    end do
    allocate(bkalm(2,23600))
    call loadtxt(falm(5,i+1),bkalm(1:2,:))
    call array_extend([236,100],[3,3],flbk*bkalm(2,:),wlm(2,:))
    deallocate(bkalm)
    call loadtxt(falm(7,i+1),dlm)  !load delensing bias
    allocate(est(2,npix));  est = 0d0
    alm = alm * Fl
    CALL QUADEE(nn,D,alm(1,:),alm(1,:),CE1d,rL,est(1,:))
    CALL QUADEB(nn,D,alm(1,:),alm(2,:),CE1d,rL,est(2,:))
    deallocate(alm)
    do n = 1, 2
      est(n,:) = ( est(n,:)*lfac - mest(n,:) ) * A2d(n,:)
    end do
    !* lensing B-modes
    allocate(blm(2,npix));  blm=0d0
    call LensingB(nn,D,wlm(1,:),est(1,:)*Gl(1,:),dL,blm(1,:))
    call LensingB(nn,D,wlm(1,:),est(2,:)*Gl(2,:),dL,blm(2,:))
    blm(2,:) = FF*(blm(2,:)-dlm)
    call delensed_cls(bmax,dL,els,D,wlm(2,:),blm,Yb(i,:,:))
    deallocate(blm,wlm,est,dlm)
  end do

  !* averaged cls
  allocate(mCb(cln,bmax),vCb(cln,bmax));  mCb=0d0;  vCb=0d0
  do i = 1, cln
    call MEANVAR(Yb(:,i,:),mCb(i,:),vCb(i,:))
  end do
  call savetxt(f(1),bc,mCb,vCb)

  !* interpolation to multipoles
  allocate(iC(cln,eL(2)));  iC = 0d0
  do i = 1, cln
    call cb2cl(bp,mCb(i,:),iC(i,:))
  end do

  Bl = 0d0
  do n = 1, npix
    if(eL(1)<=els(n).and.els(n)<=eL(2)) then
      Bl(1,n) = iC(3,int(els(n)))/iC(2,int(els(n)))
      Bl(2,n) = iC(5,int(els(n)))/iC(4,int(els(n)))
    end if
  end do
  call savetxt(f(2),Bl)

  deallocate(iC,mCb,vCb,Yb,els,lfac)

end subroutine delensing_filter_bkspt



end module tool_bkspt


