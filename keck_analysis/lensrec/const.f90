!////////////////////////////////////////////////////!
! * Toshiya Namikawa
! - Last Modified : Mon 27 Jul 2015 06:26:56 PM PDT
!////////////////////////////////////////////////////!


module myconst
  implicit none

  !* precision
  integer, parameter :: dl = KIND(1.d0)
  integer, parameter :: dlc = KIND(0d0)

  !* constants
  real(dl), parameter :: pi = 3.1415926535897932384626433832795_dl
  real(dl), parameter :: ac2rad = pi/180d0/60d0
  real(dl), parameter :: twopi=2._dl*pi, fourpi=4._dl*pi
  real(dl), parameter :: const_c = 2.99792458e8_dl
  real(dl), parameter :: const_c_km = 2.99792458e5_dl
  real(dl), parameter :: const_hP = 6.62606896e-34_dl
  real(dl), parameter :: const_G=6.67428e-11_dl
  real(dl), parameter :: sigma_thomson = 6.6524616e-29_dl
  real(dl), parameter :: sigma_boltz = 5.6704e-8_dl  
  real(dl), parameter :: const_kB = 1.3806504e-23_dl 
  real(dl), parameter :: const_mp = 1.672621637e-27_dl  ! 1.672623e-27_dl
  real(dl), parameter :: const_mH = 1.673575e-27_dl !av. H atom
  real(dl), parameter :: const_me = 9.10938215e-31_dl
  real(dl), parameter :: mass_ratio_He_H = 3.9715_dl
  real(dl), parameter :: const_Gyr=3.1556926e16_dl
  real(dl), parameter :: Tcmb = 2.726e6  ! micro K
  complex(dlc), parameter :: iu = (0.0d0,1.0d0)

  !* statistical significance
  real(dl), parameter :: signif(3) = (/0.68269d0,0.95450d0,0.99730d0/)

  !* CMB
  integer, parameter :: TT=1, TE=2, EE=3, BB=4, dd=5, Td=6, Ed=7, oo=8

end module myconst

