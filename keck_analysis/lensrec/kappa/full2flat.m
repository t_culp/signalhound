function full2flat(rmax)
% projecting full sky convergence map to BK region
% e.g. full2flat(100)

% directories
root = '/n/bicepfs3/users/namikawa/kappa/';

% parameters
m  = get_map_defn('bicep');
m.pixsize=0.25/3.0;
m.nx=236*3;
m.ny=100*3;
sx=(m.hx-m.lx)/m.nx;
m.x_tic=m.lx+sx/2:sx:m.hx-sx/2;
sy=(m.hy-m.ly)/m.ny;
m.y_tic=m.ly+sy/2:sy:m.hy-sy/2;
disp(m.nx)

nx = m.nx;
ny = m.ny;

for r = 1:rmax,
  disp(r)
  hmap = read_fits_map(strcat(root,'kmap/n0512_l20-1500/map_',int2str(r),'.fits'));
  pmap = healpix_to_bicepmap(hmap,m,'healpixnearest','C');
  kmap = reshape(pmap.T,nx*ny,1);
  kmap(isnan(kmap)) = 0;
  fID = fopen(strcat(root,'proj_x3/n0512_l20-1500/',int2str(r),'.dat'),'w');
  for i = 1:nx*ny
    fprintf(fID,'%12.5E\n', kmap(i));
  end
  fclose(fID);
end

