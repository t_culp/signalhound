#///////////////////////////////////////////////////////////#
# * From lensing potential of BK sims to convergence
# - alms only at lmin<=l<=lmax are used
# - Last Modified: Sun 15 Nov 2015 03:44:59 PM EST
#///////////////////////////////////////////////////////////#

import healpy as hp
import numpy as np

rootb = '/n/bicepfs3/users/namikawa/kappa/'
rootp = '/n/panlfs2/bicep/keck/pipeline/input_maps/lensing_potentials/camb_planck2013_r0/'

nside = 512
simn = 499
lmin = 20
lmax = 1500

L = np.linspace(0,lmax,lmax+1)
filt = (L**2+L)*0.5

for i in range(1,simn+1):
  if i < 10:
    id = '000'+str(i)
  elif i >=10 and i < 100:
    id = '00'+str(i)
  else:
    id = '0'+str(i)
  print id
  map = hp.fitsfunc.read_map(rootp+'phi_r'+id+'.fits')
  plm = hp.map2alm(map,lmax=lmax)
  alm = hp.sphtfunc.almxfl(plm,filt)
  dat = hp.sphtfunc.alm2map(alm,nside,lmax=lmax)
  hp.fitsfunc.write_map(rootb+'kmap/n0512_l'+str(lmin)+'-'+str(lmax)+'/map_'+str(i)+'.fits',dat)

