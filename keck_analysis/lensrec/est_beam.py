
# * measured power spectrum
# - Last Modified: Wed 11 May 2016 09:54:42 PM EDT

import numpy as np
from matplotlib.pyplot import *
import matplotlib as mpl

# parameter
bmax = 10
root = '/n/bicepfs3/users/namikawa/BK14/lensrec/derived/obs/'

#//// plot starting ////#
mpl.rcParams.update({'font.size': 16,'legend.fontsize':13})
xlabel(r'Multipole $L$',fontsize=22)

#//// set parameters ////#
lab = [r'BK14 x Planck','BK14']
ylabel(r'$\Delta C_L^{\kappa\kappa}/\sigma(C_L^{\kappa\kappa})$',fontsize=22)

#//// read cls ////#
xlim(15,710)
ylim(-3.,3.)

# observed cl
tag = '1var_lx30_l30-700_lB150-700'
mX0, vX0 = np.loadtxt(root+'/oXb_'+tag+'_dp1102_b'+str(bmax)+'.dat',unpack=True,usecols=(1,6))
mC0, vC0 = np.loadtxt(root+'/oCb_'+tag+'_dp1102_b'+str(bmax)+'.dat',unpack=True,usecols=(2,10))
b, mX1   = np.loadtxt(root+'/oXb_'+tag+'_dp11020_b'+str(bmax)+'.dat',unpack=True,usecols=(0,1))
c, mC1   = np.loadtxt(root+'/oCb_'+tag+'_dp11020_b'+str(bmax)+'.dat',unpack=True,usecols=(0,2))
c = c + 7*np.ones(len(c))
plot(b,(mX1-mX0)/vX0,'ro',label=lab[0])
plot(c,(mC1-mC0)/vC0,'go',label=lab[1])
plot([0,1000],[-0,0],'k:')
plot([0,1000],[-1,-1],'k--')
plot([0,1000],[-2,-2],'k-')
plot([0,1000],[1,1],'k--')
plot([0,1000],[2,2],'k-')
legend(loc=0,numpoints=1,frameon=False)
savefig('clkk_beam.png',bbox_inches='tight')
show()

