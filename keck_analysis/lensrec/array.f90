!////////////////////////////////////////////////////!
! * Toshiya Namikawa
! - Last Modified : Sun 29 Nov 2015 08:25:10 PM EST
!////////////////////////////////////////////////////!

module myarray
  implicit none

  !* local variables
  integer, parameter :: dl = KIND(1.d0)
  integer, parameter :: dlc = KIND(0d0)
  complex(dlc), parameter :: iu = (0.0d0,1.0d0)

  private dl, dlc, iu

  !* interfaces

  interface write_arr
    module procedure write_arr2d_dble, write_arr2d_cmplx, write_arr1d_dble
  end interface

  interface MAP_CUT
    module procedure MAP_CUT_DBLE, MAP_CUT_CMPLX
  end interface MAP_CUT

  interface MAP_TO_ARRAY
    module procedure MAP_TO_ARRAY_DBLE, MAP_TO_ARRAY_CMPLX
  end interface MAP_TO_ARRAY

  interface ARRAY_TO_MAP
    module procedure ARRAY_TO_MAP_DBLE, ARRAY_TO_MAP_CMPLX
  end interface ARRAY_TO_MAP


contains

!//// operator ////!

function Vec_To_Mat_DBLE(a,b) result(M)
  implicit none
  double precision, intent(in) :: a(:), b(:)
  double precision :: M(size(a),size(b))
  integer :: i,j

  do i = 1, size(a)
    do j = 1, size(b)
      M(i,j) = a(i)*b(j)
    end do
  end do

end function Vec_To_Mat_DBLE


function Vec_To_Mat_CMPLX(a,b) result(M)
  implicit none
  complex(dlc), intent(in) :: a(:), b(:)
  complex(dlc) :: M(size(a),size(b))
  integer :: i,j

  do i = 1, size(a)
    do j =1, size(b)
      M(i,j) = a(i)*b(j)
    end do
  end do

end function Vec_To_Mat_CMPLX


function Vec_To_Mat_Diag(vec) result(M)
  implicit none
  real(dl), intent(in) :: vec(:)
  integer :: i
  real(dl) :: M(size(vec),size(vec))

  M = 0.d0
  do i = 1, size(vec)
    M(i,i) = vec(i)
  end do

end function Vec_To_Mat_Diag


!//// utils for array ////!

subroutine write_arr1d_dble(arr,f,nn)
  implicit none
  character(*), intent(in) :: f
  integer, intent(in) :: nn(:)
  real(dl), intent(in) :: arr(:)
  real(dl) :: map(nn(1),nn(2))

  map = reshape(arr,(/nn(1),nn(2)/),order=(/2,1/))
  call write_arr2d_dble(map,f)

end subroutine write_arr1d_dble


subroutine write_arr2d_dble(arr,f)
  implicit none
  character(*), intent(in) :: f
  real(dl), intent(in) :: arr(:,:)
  integer :: i,j

  open(unit=20,file=trim(f),status="replace") 
  do i = 1, size(arr,dim=1)
    do j = 1, size(arr,dim=2)
      write(20,"(2(I6,1X),1(E12.5,1X))") i, j, arr(i,j)
      if(j==size(arr,dim=2)) write(20,"(2X)") 
    end do
  end do
  close(20)

end subroutine write_arr2d_dble


subroutine write_arr2d_cmplx(arr,f)
  implicit none
  character(*), intent(in) :: f
  complex(dlc), intent(in) :: arr(:,:)
  integer :: i,j

  open(unit=20,file=trim(f),status="replace") 
  do i = 1, size(arr,dim=1)
    do j = 1, size(arr,dim=2)
      write(20,"(2(I6,1X),2(E12.5,1X))") i, j, arr(i,j)
      if(j==size(arr,dim=2)) write(20,"(2X)") 
    end do
  end do
  close(20)

end subroutine write_arr2d_cmplx


subroutine ARRAY_TO_MAP_DBLE(nn,a1D,a2D)
!* transform 1D-array (a1D) to 2D-array (a2D)
  implicit none
  !I/O
  integer, intent(in) :: nn(2)
  real(dl), intent(in) :: a1D(:)
  real(dl), intent(out) :: a2D(:,:)

  a2D = reshape(a1D,(/nn(1),nn(2)/),order=(/2,1/))

end subroutine ARRAY_TO_MAP_DBLE


subroutine ARRAY_TO_MAP_CMPLX(nn,a1D,a2D)
  implicit none
  !I/O
  integer, intent(in) :: nn(2)
  complex(dlc), intent(in) :: a1D(:)
  complex(dlc), intent(out) :: a2D(:,:)

  a2D = reshape(a1D,(/nn(1),nn(2)/),order=(/2,1/))

end subroutine ARRAY_TO_MAP_CMPLX


subroutine MAP_TO_ARRAY_DBLE(nn,a2D,a1D,do_trans)
!* transform 2D-array (a2D) to 1D-array (a1D)
!* If do_trans is presented, part of elements is transformed
  implicit none
  !I/O
  logical, intent(in) :: do_trans(:,:)
  integer, intent(in) :: nn(2)
  real(dl), intent(in) :: a2D(:,:)
  real(dl), intent(out) :: a1D(:)
  !internal
  integer :: i,j
  complex(dlc) :: b1D(nn(1)*nn(2)), b2D(nn(1),nn(2))

  b2D = a2D
  call MAP_TO_ARRAY_CMPLX(nn,b2D,b1D,do_trans)
  a1D = dble(b1D)

end subroutine MAP_TO_ARRAY_DBLE


subroutine MAP_TO_ARRAY_CMPLX(nn,a2D,a1D,do_trans)
  implicit none
  !I/O
  logical, intent(in) :: do_trans(:,:)
  integer, intent(in) :: nn(2)
  complex(dlc), intent(in) :: a2D(:,:)
  complex(dlc), intent(out) :: a1D(:)
  !internal
  logical :: trans(size(a2D,dim=1),size(a2D,dim=2))
  integer :: i,j,n

  !if(present(do_trans)) then !do loop is ~10 time slower than reshape
    n = 1
    do i=1, nn(1)
      do j=1, nn(2)
        if(do_trans(i,j)) then
          a1D(n) = a2D(i,j)
          n = n + 1
        end if
      end do
    end do
  !else
  !  a1D = reshape(a2D,(/nn(1)*nn(2)/))
  !end if

end subroutine MAP_TO_ARRAY_CMPLX


subroutine MAP_CUT_DBLE(MAP,mm,cMAP,nn)
!* cut input map (MAP,mm) into subregion (cMAP,nn)
  implicit none
  !I/O
  integer, intent(in) :: nn(:), mm(:)
  real(dl), intent(in) :: MAP(:)
  real(dl), intent(out) :: cMAP(:)
  !internal
  integer :: i, j
  real(dl), dimension(mm(1),mm(2)) :: inmap
  real(dl), dimension(nn(1),nn(2)) :: outmap

  inmap = reshape(MAP,(/mm(1),mm(2)/),order=(/2,1/))
  do i = 1, nn(1)
    do j = 1, nn(2)
      outmap(i,j) = inmap(i,j) 
    end do
  end do
  cMAP = reshape(transpose(outmap),(/nn(1)*nn(2)/))

end subroutine MAP_CUT_DBLE


subroutine MAP_CUT_CMPLX(MAP,mm,cMAP,nn)
  implicit none
  !I/O
  integer, intent(in) :: nn(2), mm(2)
  complex(dlc), intent(in) :: MAP(:)
  complex(dlc), intent(out) :: cMAP(:)
  !internal
  real(dl), dimension(mm(1)*mm(2),2) :: imap
  real(dl), dimension(nn(1)*nn(2),2) :: outmap

  imap(:,1) = dble(MAP)
  imap(:,2) = aimag(MAP)
  call MAP_CUT_DBLE(imap(:,1),mm,outmap(:,1),nn)
  call MAP_CUT_DBLE(imap(:,2),mm,outmap(:,2),nn)
  cMAP = outmap(:,1) + iu*outmap(:,2)

end subroutine MAP_CUT_CMPLX


subroutine map_finer_2(cc,ff,cmap,fmap)
  implicit none
  !I/O
  integer, intent(in) :: cc(1:2), ff(1:2)
  double precision, intent(in) :: cmap(:)
  double precision, intent(out) :: fmap(:)
  !internal
  integer :: i, j, i1, i2, j1, j2
  double precision, allocatable :: cmap2d(:,:), fmap2d(:,:)

  allocate(cmap2d(cc(1),cc(2)),fmap2d(ff(1),ff(2)))
  cmap2d = reshape(cmap,(/cc(1),cc(2)/),order=(/2,1/))

  !make finer map
  do i = 1, cc(1)
    do j = 1, cc(2)
      i1 = 2*i-1
      i2 = 2*i
      j1 = 2*j-1
      j2 = 2*j
      fmap2d(i1:i2,j1:j2) = cmap2d(i,j)
    end do
  end do
  
  fmap = reshape(transpose(fmap2d),(/ff(1)*ff(2)/))
  deallocate(cmap2d,fmap2d)

end subroutine map_finer_2



!//// 2D array as matrix ////!

!diagonal elements of n*n matrix
function MDIAG(i,n)
  ! 1 + n + (n-1) + (n-2) + ...
  !   = n*(n+1)/2 - (n-i)(n-i+1)/2 - (n-i)
  implicit none
  !I/O
  integer, intent(in) :: i, n
  integer :: MDIAG

  MDIAG = n*(i-1) + i*(3-i)/2

end function MDIAG


!elements of n*n matrix
!(1,1), (1,2), (1,3), ..., (2,2), (2,3), ... -> 1,2,3,...
function MLAB(i,j,n)
  implicit none
  !I/O
  integer, intent(in) :: i,j,n
  integer :: MLAB

  if(j>=i) MLAB = mdiag(i,n) + (j-i)
  if(j<i) MLAB = mdiag(j,n) + (i-j)

end function MLAB


subroutine SYMMETRIC(M)
! symmetrize matrix (M_ji -> M_ij)
  implicit none
  !I/O
  double precision, intent(inout) :: M(:,:)
  !internal
  integer :: i, j, n

  n = size(M,dim=1)
  do i = 1, n
    do j = i + 1, n
      M(j,i) = M(i,j)
    end do
  end do

end subroutine SYMMETRIC


function Mat_Identity(N) result(M)
! get N*N indentity matrix
  implicit none
  integer, intent(in) :: N
  integer :: i
  real(dl) :: M(N,N)

  M = 0.d0
  do i = 1, N
    M(i,i) = 1.d0
  end do

end function Mat_Identity



subroutine Inv_Gauss_Jordan(K)
  implicit none
  real(dl), intent(inout) :: K(:,:)
  integer :: IPIV(size(K,dim=1)), i, j, N
  real(dl), allocatable, dimension(:) :: Lam 
  real(dl), allocatable, dimension(:,:) ::L, M, U, MM

  N = size(K,dim=1)
  if(.not.size(K,dim=1)==size(K,dim=2)) stop "size of matrix is incorrect"
  allocate(L(N,N),M(N,N),U(N,N),MM(N,N),LAM(N))

  do i = 1, N
    Lam(i) = K(i,i)
  end do

  do i = 1, N
    do j = 1, N
      K(i,j) = K(i,j)/(dsqrt(Lam(i)*Lam(j)))
    end do
  end do

  M = K

  call gaussj2(K,N,Mat_Identity(N),N) 

  do i = 1, N
    do j = 1, N
      K(i,j) = K(i,j)/(dsqrt(Lam(i)*Lam(j)))
    end do
  end do

  deallocate(L,M,U,MM,LAM)


end subroutine Inv_Gauss_Jordan


!+++++++++++++++++++++!
! Gauss-Jordan Method !
!+++++++++++++++++++++!

SUBROUTINE gaussj2(a,n,b,m)
  !implicit double precision (a-h,o-z)
  INTEGER::  m, mp, n, np
  REAL(dl) :: a(:,:), b(:,:) 
  ! Linear equation solution by Gauss-Jordan elimination, eq.(2.1.1) above. 
  ! a(1:n,1:n) is an input matrix stored in an array of physical dimensions
  ! np by np. 
  ! b(1:n,1:m) is an input matrix containing the m right-hand side 
  ! vectors, stored in an array of physical dimensions np by mp. 
  ! On output, a(1:n,1:n) is replaced by its matrix inverse, 
  ! and b(1:n,1:m) is replaced by the corresponding set of solution 
  ! vectors. Parameter: NMAX is the largest anticipated value of n. 
  integer, parameter :: NMAX = 50 
  INTEGER :: i,icol,irow,j,k,l,ll,indxc(NMAX),indxr(NMAX),ipiv(NMAX) 
  ! The integer arrays ipiv, indxr,andindxc are used for bookkeeping 
  ! on the pivoting. 
  REAL(dl) :: big,dum,pivinv 

  np = size(a,dim=1)
  mp = size(b,dim=2)
  ipiv(1:n) = 0

  do i = 1, n  ! This is the main loop over the columns to be re-duced. 
    big = 0.d0 
    do j = 1, n  ! This is the outer loop of the search for a pivot element.
      if(ipiv(j).ne.1) then
        do k = 1, n 
          if (ipiv(k).eq.0) then 
            if (abs(a(j,k)).ge.big)then 
              big = abs(a(j,k)) 
              irow = j 
              icol = k 
            end if
          end if
        end do
      end if
    end do
    ipiv(icol) = ipiv(icol) + 1 
    if (irow.ne.icol) then 
      do l = 1, n 
        dum = a(irow,l) 
        a(irow,l) = a(icol,l) 
        a(icol,l) = dum 
      end do
      do l = 1, m 
        dum = b(irow,l) 
        b(irow,l) = b(icol,l) 
        b(icol,l) = dum 
      end do
    end if
    indxr(i) = irow 
    ! We are now ready to divide the pivot row by the pivot element, 
    ! located at irow and icol. 
    indxc(i) = icol 
    if (a(icol,icol).eq.0.) stop 'singular matrix in gaussj'  
    ! singular matrix in gaussj  
    pivinv = 1./a(icol,icol) 
    a(icol,icol) = 1. 
    a(icol,1:n) = a(icol,1:n)*pivinv 
    b(icol,1:m) = b(icol,1:m)*pivinv 
    do ll = 1, n  !Next, we reduce the rows... 
      if(ll.ne.icol)then  !...except for the pivot one, of course. 
        dum = a(ll,icol)
        a(ll,icol) = 0. 
        a(ll,1:n) = a(ll,1:n) - a(icol,1:n)*dum 
        b(ll,1:m) = b(ll,1:m) - b(icol,1:m)*dum 
      end if
    end do
  end do  !This is the end of the main loop over columns of the reduction. 
  do l = n, 1, -1 
  !It only remains to unscramble the solution in view of the 
  !column interchanges. We do this by in-terchanging pairs of 
  !columns in the reverse order that the permutation was built up. 
    if (indxr(l).ne.indxc(l))then 
      do k = 1, n 
        dum = a(k,indxr(l)) 
        a(k,indxr(l)) = a(k,indxc(l)) 
        a(k,indxc(l)) = dum 
      end do
    end if
  end do
  return 

END SUBROUTINE gaussj2


!//// Re-ordering array ////!

subroutine SORT_1D(refval,ii)
!Sorting input array 
  implicit none
  !I/O
  integer, intent(out) :: ii(:) !label
  real(dl), intent(inout) :: refval(:) !values
  !internal
  integer :: i, n, m, id, pn
  real(dl) :: dummy
  real(dl), allocatable :: array(:)

  pn = size(refval)
  allocate(array(pn))

  !set array
  array = refval
  do n = 1, pn
    ii(n) = n
  end do

  !sort
  do n = 1, pn
    do m = n + 1, pn
      if(array(n)<array(m)) then 
        dummy = array(n)
        id = ii(n)
        array(n) = array(m)
        ii(n) = ii(m)
        array(m) = dummy
        ii(m) = id
      end if
    end do
  end do 

  refval = array

  deallocate(array)

end subroutine SORT_1D


subroutine SORT_2D(refval,ii,jj)
  implicit none
  !I/O
  integer, intent(inout) :: ii(:), jj(:)
  real(dl), intent(inout) :: refval(:,:)
  !internal
  integer :: i, j, n, m, id, jd, pn1, pn2
  real(dl) :: dummy
  real(dl), allocatable :: array(:)

  pn1 = size(refval,dim=1)
  pn2 = size(refval,dim=2)

  allocate(array(pn1*pn2))

  !set to array
  n = 1
  do i = 1, pn1
    do j = 1, pn2
      array(n) = refval(i,j) 
      ii(n) = i
      jj(n) = j
      n = n + 1
    end do
  end do

  !sort
  do n = 1, pn1*pn2
    do m = n + 1, pn1*pn2
      if(array(n)<array(m)) then 
        dummy = array(n)
        id = ii(n)
        jd = jj(n)
        array(n) = array(m)
        ii(n) = ii(m)
        jj(n) = jj(m)
        array(m) = dummy
        ii(m) = id
        jj(m) = jd
      end if
    end do
  end do

  do n=1, pn1*pn2
    refval(ii(n),jj(n)) = array(n)
  end do

  deallocate(array)

end subroutine SORT_2D


end module myarray

