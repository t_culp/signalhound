function [mapE, mapB]=do_projection(maps, proj_file,do_e)
% [mapE,mapB]=do_projection(maps,proj_file,do_e)
%
% Uses a projection matrix from proj_file to project the map 
%
% INPUTS:
%  maps       : Input map structure (non-padded) 
%               OR
%               cell of map names (strings)
%               & will save output maps        
%
%  proj_file : Name of projection matrix file (string)
% 
%  do_e       : 1: also find pureE
%             : 0: (default) don't find pureE.           
%
% OUTPUTS:
% mapE       :[QU] maps of Emodes
% mapB       :[QU] maps of Bmodes
%
% eg
% proj_file='matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat';
% load(proj_file, 'reob')
% load(reob.file, 'map')
% [mapE,mapB]=do_projection(map,proj_file)
%
% eg
% proj_file='matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat';
% for i=1:100;maps{i}=['matrix/maps/0704/' num2str(i, '%.3d') '1_cmb2012_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final_healpix_camb_planck2013_r0_lmax700_beamB2bbns.mat'];end
% do_projection(maps, proj_file);
%

disp('applying purification matrix')

if(~exist('do_e','var') || isempty(do_e));
  do_e=0;
end

%if maps is jsut one string, make into cell
if ischar(maps)
  maps={maps};
end

%------%

%get B proj matrix
if(~isstruct(proj_file))
  disp('loading B projection matrix...')
  % This will probably emit a warning, either because projmatopt is
  % missing for 0704/0706-era matrices or that obs_pixels and reob
  % are missing with modern matrices. This isn't a problem, though,
  % since we know how to deal with that in the code below.
  tic
  projdata = load(proj_file,'projmatopt','pb','obs_pixels','reob','m'); %TN
  %projdata = load(proj_file,'pb','obs_pixels','reob','m'); %TN added
  toc
else
  projdata = proj_file;
end

% For legacy 0704/0706-era matrices, the projmatopt didn't exist yet, so
% provide some data from the top-level variables
if ~isfield(projdata, 'projmatopt')
  obs_pixels = projdata.obs_pixels;
  reob = projdata.reob;
% Otherwise that data has now been moved into a projmatopt structure.
else
  obs_pixels = projdata.projmatopt.obs_pixels;
  reob = projdata.projmatopt.reob;
end
% These should be at the top level no matter what
m = projdata.m;

if do_e
  disp('projection to pure E-mode')
  pe = projdata.pe;
  map = do_mult(pe,obs_pixels,maps,reob,m);
  coaddopt.proj='projE';
  mapE=map;
  mapB=map;
else
  disp('projection to pure B-mode')
  pb = projdata.pb;
  map = do_mult(pb,obs_pixels,maps,reob,m);
  coaddopt.proj='projB';
  mapE = map;
  mapB = map;
end



if(0)
%find map B
for i=1:length(maps)

  %if iscell(maps)
  %  load(maps{i})
  %  if exist('ac', 'var')
  %    map=make_map(ac,m,coaddopt);
  %  end
  %else
    map=maps;
  %end

  %get map B
  if ~do_e

    if(iscell(maps)) %save only if input was filenames
      fname=strtok(maps{i}, '.');
      disp(['saving ' fname '_projB'])
      saveandtest([fname '_projB'], 'map', 'coaddopt','m')
    end
  end

  %output
  mapB=map;

end

%------%
if do_e
%get E proj matrix
  % Free up large pb from memory.
  clear pb;
  projdata = rmfield(projdata, 'pb');

  if ~isstruct(proj_file)
    disp('loading E projection matrix...')
    tic
    load(proj_file,'pe');
    toc
  else
    pe = projdata.pe;
  end
end

%find map E
for i=1:length(maps)

  if iscell(maps)
    load(maps{i})
    if exist('ac', 'var')
      map=make_map(ac,m,coaddopt);
    end
  else
    map=maps;
  end

  if do_e
    %get map E
    map=do_mult(pe,obs_pixels,map,reob,m);
    coaddopt.proj='projE';

    if(iscell(maps)) %save only if input was filenames
      fname=strtok(maps{i}, '.');
      disp(['saving ' fname '_projE'])
      saveandtest([fname '_projE'], 'map', 'coaddopt', 'm')
    end
  end

  %output
  mapE=map;

end

%------%
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function map=do_mult(proj,obs_pixels,map,reob,m)

%expand to vectors
[m,mapv.Q]=map2vect(m,map.Q./reob.Qvar);
[m,mapv.U]=map2vect(m,map.U./reob.Uvar);
vect=[mapv.Q,mapv.U];

%just take the observed_pixels
vect=vect(obs_pixels);    

%if we are using projection matrix that doesnt
%perfectly match the map, there may be pixels in
%vect which are still NaN
if any(isnan(vect))
  disp('Warning: removing NaNs from map before projection')
  disp('You probably have a non-matching matrix and map')
  vect(isnan(vect))=0;
end

%do multiplication
vect_out=proj*vect';

%make back into a map
mapout=expand_vect(m,vect_out,obs_pixels);

%fill in Q,U entries (un wieght)
map.Q=mapout.Q.*reob.Qvar;
map.U=mapout.U.*reob.Uvar;

%and pad the maps (since we skipped this earlier)
if(size(map.T,1)~=size(map.Q,1))
  p=2^nextpow2(max([m.nx,m.ny]));
  %but avoid double padding T
  mapT=map;
  [m,map]=pad_map(m,map,p);
  map.T=mapT.T;
end

