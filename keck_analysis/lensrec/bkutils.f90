!///////////////////////////////////////////////////////////////////////!
! * Utils for BK analysis
! * Toshiya Namikawa
! - Last Modified: Sun 29 Nov 2015 08:25:25 PM EST
!///////////////////////////////////////////////////////////////////////!

module bkutils
  use myconst, only: pi, dlc
  use myutils, only: str, savetxt, loadtxt, meanvar
  use mycls,   only: alm2bcl_flat
  implicit none

  private pi, dlc
  private str, savetxt, loadtxt, meanvar
  private alm2bcl_flat

contains


subroutine subtract_beammap(f,alm)
  implicit none
  character(*), intent(in) :: f
  complex(dlc), intent(inout) :: alm(:,:)
  complex(dlc), allocatable :: blm(:,:)

  allocate(blm(2,size(alm,dim=2)))
  call loadtxt(f,blm)
  alm = blm - alm
  deallocate(blm)

end subroutine subtract_beammap


subroutine read_nkk_plk(f,nkk,eL)
  implicit none
  character(*), intent(in) :: f
  integer, intent(in) :: eL(2)
  double precision, intent(out) :: nkk(:)
  integer :: l
  double precision :: n, nlkk, clkk

  nkk = 0d0
  open(unit=20,file=trim(f),status="old")
  do l = 8, 2048
    read(20,*) n, nlkk, clkk
    if(eL(1)<=l.and.l<=eL(2)) nkk(l) = nlkk
  end do
  close(20)

end subroutine read_nkk_plk


subroutine set_file_bkspt(dir,tag_bkspt,tag_bk14,tag_extra,bmax,falm,fmap,gmap,fcl)
  implicit none
  !I/O
  character(*), intent(in) :: dir, tag_bkspt, tag_bk14, tag_extra
  character(*), intent(out) :: falm(:,:), fmap(:,:), gmap(:,:), fcl(:)
  integer, intent(in) :: bmax
  !internal
  character(128) :: dir_bkspt, dir_bk14, dir_plk15, dir_plk13, dir_kappa, tag_add='_dp1100', tag1, tag2
  integer :: i

  !* set data directories
  dir_bkspt = trim(dir)//"/BKSPT/lensrec/"
  dir_bk14  = trim(dir)//"/BK14/lensrec/"
  dir_plk15 = trim(dir)//"/PLANCK/2015/"
  dir_plk13 = trim(dir)//"/PLANCK/2013/"
  dir_kappa = trim(dir)//"/kappa/"

  !* sim maps
  do i = 1, 500
    if (i>1) tag_add = ''
    tag1 = trim(tag_bkspt)//trim(tag_add)
    tag2 = trim(tag_bk14)//trim(tag_add)
    fmap(1,i) = trim(dir_bkspt)//"map_lensed/"//str(i-1)//"_ap"//trim(tag_add)//".dat"
    fmap(2,i) = trim(dir_bkspt)//"map_noise/"//str(i-1)//"_ap"//trim(tag_add)//".dat"
    falm(1,i) = trim(dir_bkspt)//"alm_comb/"//trim(tag1)//"_r"//str(i-1)//".dat"
    falm(2,i) = trim(dir_bkspt)//"alm_lens/"//trim(tag1)//"_r"//str(i-1)//".dat"
    falm(3,i) = trim(dir_bkspt)//"alm_noise/"//trim(tag1)//"_r"//str(i-1)//".dat"
    falm(4,i) = trim(dir_bkspt)//"alm_comb_bk14/"//trim(tag2)//"_r"//str(i-1)//".dat"
    falm(5,i) = trim(dir_bkspt)//"alm_lens_bk14/"//trim(tag2)//"_r"//str(i-1)//".dat"
    falm(6,i) = trim(dir_bkspt)//"alm_noise/"//trim(tag2)//"_r"//str(i-1)//".dat"
    falm(7,i) = trim(dir_bkspt)//"dblm/"//trim(tag_extra)//"_r"//str(i-1)//".dat"
    falm(8,i) = trim(dir_bkspt)//"clkk/"//trim(tag_extra)//"_b"//str(bmax)//"_r"//str(i-1)//".dat"
    falm(9,i) = trim(dir_bkspt)//"wplm/"//trim(tag_extra)//"_r"//str(i-1)//".dat"
  end do

  !* BK simulated kappa and Planck maps
  do i = 1, 500
    gmap(1,i) = trim(dir_kappa)//"/proj_x3/n0512_l20-1500/"//str(i-1)//".dat"
    gmap(2,i) = trim(dir_plk15)//"/derived/proj_x3/rec_n2048_c/map_r"//str(i-1)//".dat"
  end do
  gmap(3,1) = trim(dir_plk15)//"/derived/proj_x3/mask.dat"
  gmap(3,2) = trim(dir_plk15)//"/public/data/nlkk.dat"
  gmap(3,3) = trim(dir_bk14)//"/varmap.dat"

  !* derived 1d and 2d cl files
  ! filter function
  fcl(1) = trim(dir_bkspt)//"derived/cls/"//trim(tag_bkspt)//"_2d.dat"
  fcl(2) = trim(dir_bk14)//"derived/cls/"//trim(tag_bk14)//"_2d.dat"
  ! normalization
  fcl(3) = trim(dir_bkspt)//"derived/al/"//trim(tag_extra)//".dat"
  ! transfer function
  fcl(4) = trim(dir_bkspt)//"derived/trans/"//trim(tag_extra)//"_1d.dat"
  fcl(5) = trim(dir_bkspt)//"derived/trans/"//trim(tag_extra)//"_2d.dat"
  ! disconnected bias
  fcl(6) = trim(dir_bkspt)//"derived/n0/"//trim(tag_extra)//".dat"
  ! mean field bias
  fcl(7) = trim(dir_bkspt)//"derived/mean/"//trim(tag_extra)//".dat"
  ! clkk
  fcl(8)  = trim(dir_bkspt)//"derived/kappa/sCb_"//trim(tag_extra)//"_b"//str(bmax)//".dat"
  fcl(9)  = trim(dir_bkspt)//"derived/kappa/oCb_"//trim(tag_extra)//trim(tag_add)//"_b"//str(bmax)//".dat"
  fcl(10) = trim(dir_bkspt)//"derived/kappa/sBb_"//trim(tag_extra)//"_b"//str(bmax)//".dat"
  fcl(11) = trim(dir_bkspt)//"derived/kappa/oBb_"//trim(tag_extra)//trim(tag_add)//"_b"//str(bmax)//".dat"
  fcl(12) = trim(dir_bkspt)//"derived/kappa/sLb_"//trim(tag_extra)//"_b"//str(bmax)//".dat"
  fcl(13) = trim(dir_bkspt)//"derived/kappa/oLb_"//trim(tag_extra)//trim(tag_add)//"_b"//str(bmax)//".dat"
  fcl(14) = trim(dir_bkspt)//"derived/dfl/"//trim(tag_extra)//"_1d.dat"
  fcl(15) = trim(dir_bkspt)//"derived/dfl/"//trim(tag_extra)//"_2d.dat"
  fcl(16) = trim(dir_bkspt)//"derived/cls/"//trim(tag_bkspt)//"_1d.dat"

end subroutine set_file_bkspt


subroutine set_defparams(nn,npix,D,d0,r)
  implicit none
  integer, intent(in), optional :: r
  integer, intent(out) :: nn(1:2), npix
  double precision, intent(out) :: D(1:2)
  double precision, intent(out), optional :: d0

  nn = [236,100]
  if (present(r)) nn = nn*r
  npix = nn(1)*nn(2)
  D  = [59d0,25d0]*pi/180d0
  if (present(d0)) d0 = D(1)*D(2) !delta(0)
  write(*,*) "fundamental mode and maximum multipole:", 2*pi/D, nn*pi/D

end subroutine set_defparams


subroutine make_filter(f,nn,els,Fl,f2d,Il,OC,sPin,sDin)
  implicit none
  !I/O
  character(*), intent(in) :: f
  integer, intent(in) :: nn(2)
  double precision, intent(in) :: els(:)
  double precision, intent(in), optional :: sPin, sDin
  double precision, intent(out), optional :: Fl(:,:), f2d(:,:), Il(:,:), OC(:,:)
  !internal
  integer :: n
  double precision :: rC(10)

  open(unit=20,file=trim(f),status="old")
  do n = 1, nn(1)*nn(2)
    read(20,*) rC(1:10)
    if (present(sPin))  rC(3:4) = rC(3:4)*sPin**2
    if (present(sDin))  rC(5:6) = rC(5:6)*sDin**2
    if (present(Fl)) then
      if(rC(1)>0d0)  Fl(1,n) = rC(7)/(rC(1)+rC(3)+rC(5))
      if(rC(2)>0d0)  Fl(2,n) = rC(8)/(rC(2)+rC(4)+rC(6))
    end if
    if(present(f2d)) f2d(1:2,n) = rC(7:8)
    if(present(Il)) then
      if(rC(7)/=0d0) Il(1,n) = 1d0 / (rC(9)  + (rC(3)+rC(5))/rC(7)**2 )
      if(rC(8)/=0d0) Il(2,n) = 1d0 / (rC(10) + (rC(4)+rC(6))/rC(8)**2 )
    end if
    if(present(OC)) then
      if(rC(7)/=0d0) OC(1,n) = rC(9)  + (rC(3)+rC(5))/rC(7)**2
      if(rC(8)/=0d0) OC(2,n) = rC(10) + (rC(4)+rC(6))/rC(8)**2
    end if
  end do
  close(20)

end subroutine make_filter



subroutine READCL_BK(f,eL,lensed,CTT,CEE,CBB,CTE,Ckk)
  implicit none 
  !I/O
  character(*), intent(in) :: f
  integer, intent(in) :: eL(1:2)
  logical, intent(in) :: lensed
  double precision, intent(out), optional :: CTT(:),CTE(:),CEE(:),CBB(:),Ckk(:)
  !internal
  double precision :: rC(1:5)
  integer :: i, l, n, m

  m = 5
  if(lensed) m = 4
  if(present(CTT)) CTT=0d0
  if(present(CEE)) CEE=0d0
  if(present(CBB)) CBB=0d0
  if(present(CTE)) CTE=0d0
  if(present(Ckk)) Ckk=0d0

  open(unit=20,file=trim(f),status="old")
  do i = 1, 1800
    read(20,*) l, rC(1:m)
    if(el(1)<=l.and.l<=el(2)) then 
      if(present(CTT)) CTT(l) = rC(1)*2d0*pi/dble(l**2+l)
      if(present(CEE)) CEE(l) = rC(2)*2d0*pi/dble(l**2+l)
      if(present(CBB)) CBB(l) = rC(3)*2d0*pi/dble(l**2+l)
      if(present(CTE)) CTE(l) = rC(4)*2d0*pi/dble(l**2+l)
      if(present(Ckk)) Ckk(l) = rC(4)/4d0
    end if
  end do
  close(20)

end subroutine READCL_BK


subroutine delensed_cls(bmax,eL,els,D,alm,blm,Cb,Wn,Bl)
!* calculating delensed Cls
  implicit none
!
! [input]
!   bmax       --- number of multipole bins
!   eL(2)      --- minimum and maximum multipoles
!   els(npix)  --- multipoles as a function of pixel index
!   D(2)       --- map side lengths
!   alm(npix)  --- original B-modes
!   blm(npix)  --- lensing B-mode template
  integer, intent(in) :: bmax, eL(2)
  double precision, intent(in) :: els(:), D(2)
  complex(dlc), intent(in) :: alm(:), blm(:,:)
!
! [output]
!   Cb(7,bmax) --- binned Cls
  double precision, intent(out) :: Cb(:,:)
!
! (optional)
!   Wn(6)      --- window norm (\int W^n)
!   Bl(2,npix) --- optimal filtering function to be multiplied to blm
  double precision, intent(in), optional :: Wn(:), Bl(:,:)
!
! [internal]
  integer :: n
  double precision :: W(1:6), Bb(10,bmax)

  !check
  if (size(W)/=6)  stop "error (delensed_cls): size(W) is not 6."
  if (size(Cb)<7)  stop "error (delensed_cls): size(Cb) is should be >=7."

  !window norm
  W = 1d0
  if (present(Wn)) W = Wn

  !cls
  call alm2bcl_flat(bmax,eL,els,D,alm,Cb=Bb(1,:),norm=1d0/W(2))
  call alm2bcl_flat(bmax,eL,els,D,blm(1,:),Cb=Bb(2,:),norm=1d0/W(6))
  call alm2bcl_flat(bmax,eL,els,D,alm,blm(1,:),Cb=Bb(3,:),norm=1d0/W(4))
  call alm2bcl_flat(bmax,eL,els,D,blm(2,:),Cb=Bb(4,:),norm=1d0/W(6))
  call alm2bcl_flat(bmax,eL,els,D,alm,blm(2,:),Cb=Bb(5,:),norm=1d0/W(4))

  if (present(Bl)) then
    call alm2bcl_flat(bmax,eL,els,D,alm-Bl(1,:)*blm(1,:),Cb=Bb(6,:),norm=1d0/W(2))
    call alm2bcl_flat(bmax,eL,els,D,alm-Bl(2,:)*blm(2,:),Cb=Bb(7,:),norm=1d0/W(2))
  else
    Bb(6,:) = (Bb(1,:) - 2d0*Bb(3,:) + Bb(2,:))
    Bb(7,:) = (Bb(1,:) - 2d0*Bb(5,:) + Bb(4,:))
  end if

  !output
  Cb(1:7,:) = Bb(1:7,:)

end subroutine delensed_cls


subroutine write_cls(Cb,bc,f1,f2)
  implicit none
  character(*), intent(in) :: f1
  character(*), intent(in), optional :: f2
  double precision, intent(in) :: Cb(:,:,:), bc(:,:)
  integer :: cln, mapn, bmax, j
  double precision, allocatable :: mCb(:,:), vCb(:,:)

  mapn = size(Cb,dim=1)
  cln  = size(Cb,dim=2)
  bmax = size(Cb,dim=3)

  allocate(mCb(cln,bmax),vCb(cln,bmax)); mCb=0d0; vCb=0d0
  do j = 1, cln
    call meanvar(Cb(2:mapn,j,:),mCb(j,:),vCb(j,:))
  end do
  call savetxt(f1,bc,mCb,vCb)
  if (present(f2))  call savetxt(f2,bc,Cb(1,:,:),vCb)
  deallocate(mCb,vCb)

end subroutine write_cls


subroutine wiener_gfilter(eL,els,A2d,sig,ckk)
  implicit none
  integer, intent(in) :: eL(2)
  double precision, intent(in) :: ckk(:), A2d(:,:), els(:)
  double precision, intent(out) :: sig(:,:)
  integer :: l, n

  sig = 0d0
  do n = 1, size(els)
    if (A2d(1,n)/=0) sig(1,n) = ckk(n)/(ckk(n)+A2d(1,n))
    if (A2d(2,n)/=0) sig(2,n) = ckk(n)/(ckk(n)+A2d(2,n))
    if (A2d(5,n)/=0) sig(3,n) = ckk(n)/(ckk(n)+A2d(5,n))
  end do

end subroutine wiener_gfilter


end module bkutils

