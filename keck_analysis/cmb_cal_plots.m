function cmb_cal_plots(tag)
%
%
% make plots of cal quantities over run
%
%

load(['data/cmb_',tag]);

% interested here only in blip height for rowcals
rc.g=-rc.g(:,:,1);

i=d.t<55000; d.t(i)=d.t(i)+86400;

% find mutual xlim
ind2=[cs.gr.s;en.s;rc.s];
xl=[min(d.t(ind2)),max(d.t(ind2))];

% Find the time for each rowcal
rcc=rc;
rc.s=rc.s(3:7:length(rc.s));

% the thing to write at bottom of each plot
runtag=sprintf('%s %s %s',tstart,tend,strrep(runname,'_','\_'));

% plot the various cals over time

close all
figure(1); clf
setwinsize(gcf,800,850);
defcm=get(gcf,'DefaultAxesColorOrder');


set(gcf,'DefaultAxesColorOrder',jet(length(ind.rgl)));

subplot(6,5,[1,2,3,4])
plot(d.t(cs.md.s),cs.md.g(:,ind.rgl),'.-');
ylabel('calsrc step (V)')
axis tight; xlim(xl);
h=legend(p.channel_name(ind.rgl));
pos=get(h,'Position'); pos(1)=pos(1)+0.2;
set(h,'Position',pos)

subplot(6,5,[6,7,8,9])
plot(d.t(cs.gr.s),cs.gr.g(:,ind.rgl),'.-');
ylabel('calsrc rot (V)')
axis tight; xlim(xl);
yl=ylim; ylim([0,yl(2)]);

subplot(6,5,[11,12,13,14])
y=-mean(en.g,3);
plot(d.t(en.s),y(:,ind.rgl),'.-');
ylabel('elnod (V/airmass)')
axis tight; xlim(xl);
yl=ylim; ylim([0,yl(2)]);

if(numel(rc.s)>0)
  subplot(6,5,[16,17,18,19])
  plot(d.t(rc.s),rc.g(:,ind.rgl),'.-');
  ylabel('rowcal blip (V)')
  axis tight; xlim(xl);
  yl=ylim; ylim([0,yl(2)]);
end

set(gcf,'DefaultAxesColorOrder',defcm);

subplot(6,5,[21,22,23,24])
h=plotyy(d.t(ind2),d.tracker.actual(ind2,2),d.t(ind2),d.bias.rms150GHz(ind2));
set(get(h(1),'YLabel'),'String','elevation (deg)')
set(get(h(2),'YLabel'),'String','bias rms (raw units)')
set(h(1),'XLim',xl); set(h(2),'XLim',xl);
%axis tight; xl=get(h(1),'XLim'); set(h(2),'XLim',xl);
set(get(h(1),'Children'),'Marker','.'); set(get(h(1),'Children'),'LineStyle','none')
set(get(h(2),'Children'),'Marker','.'); set(get(h(2),'Children'),'LineStyle','none')

subplot(6,5,[26,27,28,29])
h=plotyy(d.t(ind2),d.cal.temperature(ind2),d.t(ind2),d.weather.air_temp(ind2));
set(get(h(1),'YLabel'),'String','cal temp (C)')
set(get(h(2),'YLabel'),'String','air temp')
set(h(1),'XLim',xl); set(h(2),'XLim',xl);
%axis tight; xl=get(h(1),'XLim'); set(h(2),'XLim',xl);
set(get(h(1),'Children'),'Marker','.'); set(get(h(1),'Children'),'LineStyle','none')
set(get(h(2),'Children'),'Marker','.'); set(get(h(2),'Children'),'LineStyle','none')

plot_title('all cal quantities versus time',runtag);
drawnow; mkpng(sprintf('%s/fig1.png',tag));


figure(2);
setwinsize(gcf,800,850); clf
plot_mean_ratios(d,p,cs.gr,ind.rgl150,1);
plot_mean_ratios(d,p,cs.gr,ind.rgl100,2);
plot_title('calsrc each / mean all',runtag);
drawnow; mkpng(sprintf('%s/fig2.png',tag));

figure(3);
setwinsize(gcf,800,850); clf
plot_mean_ratios(d,p,en,ind.rgl150,1);
plot_mean_ratios(d,p,en,ind.rgl100,2);
plot_title('elnod each / mean all',runtag);
drawnow; mkpng(sprintf('%s/fig3.png',tag));

if(numel(rc.s)>0)
  figure(4);
  setwinsize(gcf,800,850); clf
  plot_mean_ratios(d,p,rc,ind.rgl150,1);
  plot_mean_ratios(d,p,rc,ind.rgl100,2);
  plot_title('rowcal each / mean all',runtag);
  drawnow; mkpng(sprintf('%s/fig4.png',tag));
end

figure(5)
setwinsize(gcf,800,850); clf
cs.gr.r150=plot_pair_ratios(d,p,cs.gr,ind.rgl150,1);
cs.gr.r100=plot_pair_ratios(d,p,cs.gr,ind.rgl100,2);
plot_title('calsrc A/B ratios',runtag);
drawnow; mkpng(sprintf('%s/fig5.png',tag));

figure(6);
setwinsize(gcf,800,850); clf
en.r150=plot_pair_ratios(d,p,en,ind.rgl150,1);
en.r100=plot_pair_ratios(d,p,en,ind.rgl100,2);
plot_title('elnod A/B ratios',runtag);
drawnow; mkpng(sprintf('%s/fig6.png',tag));

if(numel(rc.s)>0)
  figure(7)
  setwinsize(gcf,800,850); clf
  rc.r150=plot_pair_ratios(d,p,rc,ind.rgl150,1);
  rc.r100=plot_pair_ratios(d,p,rc,ind.rgl100,2);
  plot_title('rowcal A/B ratios',runtag);
  drawnow; mkpng(sprintf('%s/fig7.png',tag));
end

figure(8)
setwinsize(gcf,600,600); clf
subplot(2,2,1)
set(gcf,'DefaultAxesColorOrder',jet(size(en.r150,2)));
plot(en.r150,cs.gr.r150,'.');
axis equal; axis square; grid
line([0,2],[0,2],'Color','k'); xlim([0.5,1.5]); ylim([0.5,1.5])
title('calsrc vs elnod')
ylabel('150GHz')

if(numel(rc.s)>0)
  subplot(2,2,2)
  x=mean(en.r150); y=mean(rc.r150);
  plot([x;x],[y;y],'.');
  axis equal; axis square; grid
  line([0,2],[0,2],'Color','k'); xlim([0.5,1.5]); ylim([0.5,1.5])
  title('rowcal vs elnod')
end

subplot(2,2,3)
set(gcf,'DefaultAxesColorOrder',jet(size(en.r100,2)));
plot(en.r100,cs.gr.r100,'.');
axis equal; axis square; grid
line([0,2],[0,2],'Color','k'); xlim([0.5,1.5]); ylim([0.5,1.5])
ylabel('100GHz')

if(numel(rc.s)>0)
  subplot(2,2,4)
  x=mean(en.r100); y=mean(rc.r100);
  plot([x;x],[y;y],'.');
  axis equal; axis square; grid
  line([0,2],[0,2],'Color','k'); xlim([0.5,1.5]); ylim([0.5,1.5])
end

plot_title('correlations of cal quantities',runtag);
drawnow; mkpng(sprintf('%s/fig8.png',tag));

figure(9)
setwinsize(gcf,600,600); clf
subplot(2,1,1)
plot(d.t(cr.a.m),cr.a.p(:,2,1),'b.');
hold on
plot(d.t(cr.a.m),cr.a.p(:,2,2),'g.');
plot(d.t(cr.a.m),cr.a.p(:,2,3),'r.');
plot(d.t(cr.a.m),cr.a.p(:,2,4),'m.');
legend({'A+','B+','A-','B-'},-1);
ylabel('x offset');
hold off
subplot(2,1,2)
plot(d.t(cr.e.m),cr.e.p(:,2,1),'b.');
hold on
plot(d.t(cr.e.m),cr.e.p(:,2,2),'g.');
plot(d.t(cr.e.m),cr.e.p(:,2,3),'r.');
plot(d.t(cr.e.m),cr.e.p(:,2,4),'m.');
ylabel('y offset');
hold off
plot_title('Pointing offset of central feed from cross obs',runtag);
drawnow; mkpng(sprintf('%s/fig9.png',tag));

unix(sprintf('cp -p index.html %s',tag));
unix(sprintf('scp -rp %s find:/home/quad/public_html/reducplots',tag));

return

%%%%%%%%%%%%%%%%%%%%%%%%
function plot_title(tex1,tex2)

gtitle(tex1);

text(0.5,0.03,tex2,'Units','Normalized','FontSize',10,...
    'HorizontalAlignment','center');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_mean_ratios(d,p,e,ind,n)
x=mean(e.g(:,ind,:),3);
y=mean(x,2); y=repmat(y,[1,size(x,2)]); x=x./y;

if(n==1)
  subplot(4,2,[1,3,5])
else
  subplot(4,2,[2,4,6])
end
set(gcf,'DefaultAxesColorOrder',jet(length(ind)))
plot(d.t(e.s),x,'.-');
%xlim([min(d.t(e.m)),max(d.t(e.m))]);
axis tight; ylim([0.5,1.6]);
legend(p.channel_name(ind),-1);
grid

if(n==1)
  subplot(4,2,7)
else
  subplot(4,2,8)
end
cmean=nanmean(x,1); cmean=repmat(cmean,[size(x,1),1]);
x=x./cmean;
hfill(nanstd(x)*100,20,0);
xlabel('Percent fluctuation over run');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ratio=plot_pair_ratios(d,p,e,ind,f)

n=0;
x=mean(e.g,3);
for i=1:length(ind)
  if(p.channel_name{ind(i)}(end)=='A')
    ch=p.channel_name{ind(i)}(1:end-2);
    if(strcmp(p.channel_name{ind(i+1)},[ch,'-B']))
      n=n+1;
      ratio(:,n)=x(:,ind(i))./x(:,ind(i+1));
      chname{n}=p.channel_name{ind(i)}(1:end-2);
    end
  end
end

if(f==1)
  subplot(4,2,[1,3,5])
else
  subplot(4,2,[2,4,6])
end
set(gcf,'DefaultAxesColorOrder',jet(size(ratio,2)))
plot(d.t(e.s),ratio,'.-');
legend(chname,-1);
axis tight; ylim([0.5,1.5]);
%ylim([min(ratio(:)),max(ratio(:))]);
%xlim([min(d.t(e.m)),max(d.t(e.m))]);
grid

if(f==1)
  subplot(4,2,7)
else
  subplot(4,2,8)
end
cmean=nanmean(ratio,1); cmean=repmat(cmean,[size(ratio,1),1]);
nratio=ratio./cmean;
hfill(nanstd(nratio)*100,20,0);
xlabel('Percent fluctuation over run');

return
