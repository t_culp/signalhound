
function checkplot(tstart,tend,runname,tag)
% checkplot(d)
%
% Make standard plots to assess QUaD run
%
% checkplot('12-Mar-2005:02:38:42','12-Mar-2005:18:12:36','9_raster_map_rcw38_dk+30_003.sch','050312') 
% checkplot('13-Mar-2005:01:14:58','13-Mar-2005:16:47:04','9_raster_map_rcw38_dk-30_003.sch','050313')
% checkplot('17-Mar-2005:02:11:30','17-Mar-2005:17:34:43','9_raster_map_rcw38_dk+00_001.sch','050317')
% checkplot('19-Mar-2005:06:28:38','19-Mar-2005:21:50:30','9_raster_map_rcw38_dk+15_003.sch','050319')
% checkplot('20-Mar-2005:03:09:04','20-Mar-2005:18:42:48','9_raster_map_rcw38_dk-15_003.sch','050320')
% checkplot('26-Mar-2005:12:35:25','26-Mar-2005:23:04:56','8_point_cross_rcw38_000.sch','050326')
% checkplot('27-Mar-2005:09:46:25','27-Mar-2005:23:27:03','8_point_cross_rcw38_000.sch','050327')
% checkplot('28-Mar-2005:04:21:00','28-Mar-2005:21:19:41','8_point_cross_rcw38_001.sch','050328')
% checkplot('29-Mar-2005:03:45:46','29-Mar-2005:20:26:20','8_point_cross_rcw38_002.sch','050329')
% checkplot('30-Mar-2005:06:36:02','31-Mar-2005:00:17:24','8_point_cross_2src_3dk_000.sch','050330') *
% checkplot('31-Mar-2005:06:20:30','31-Mar-2005:23:50:01','8_point_cross_2src_3dk_001.sch','050331') * 
% checkplot('01-Apr-2005:04:34:09','01-Apr-2005:19:59:38','8_raster_map_vela_dk+00_000.sch','050401') * 
% checkplot('02-Apr-2005:06:00:33','03-Apr-2005:00:20:18','8_point_cross_1src_2dk_000.sch','050402') *
% checkplot('03-Apr-2005:05:03:55','03-Apr-2005:23:06:51','8_point_cross_2src_1dk_000.sch','050403') X 
% checkplot('04-Apr-2005:06:07:13','04-Apr-2005:22:04:00','8_point_cross_4src_5dk_000.sch','050404') XX
% checkplot('05-Apr-2005:05:43:42','05-Apr-2005:22:20:11','8_point_cross_4src_5dk_000.sch','050405') XXX
% checkplot('06-Apr-2005:04:08:22','06-Apr-2005:20:38:46','8_point_cross_4src_5dk_001.sch','050406') X
% checkplot('07-Apr-2005:04:58:15','07-Apr-2005:20:15:58','9_raster_map_j0455-462_dk+00_000.sch','050407')

regs={'frame.utc','frame.features',...
    'lockin.adcData[][99]',...
    'lockin.mjdMs[0][99]',...
    'lockin.azPos[0][99]','lockin.elPos[0][99]','lockin.dkPos[0][99]',...
    'tracker.source string',...
    'tracker.actual','tracker.errors',...
    'tracker.horiz_off','tracker.scan_off',...
    'weather.air_temp[0]','weather.humidity[0]','weather.pressure[0]',...
    'pmac.x_tilt','pmac.y_tilt',...
    'fridge.grttemp','bias.pidEnable',...
    'pmac.mtr_com_i','pmac.enc_az_diff'};
d=read_arc(regs,tstart,tend);

% time
d.t=d.frame.utc(:,2)/1e3;

figure(1)
clf; setwinsize(gcf,900,1100);

xl=[min(d.t),max(d.t)];

% features
subplot_stack(9,1,1)
semilogy(d.t,d.frame.features,'.','MarkerSize',0.2);
y=ylim; ylim([y(1)-0.1,y(2)+0.1]);
xlim(xl);
ylabel('frame.features');
title(sprintf('%s %s %s',tstart,tend,strrep(runname,'_','\_')));
subplot_stack2(9,1,1)

% az/el/dk
subplot_stack(9,1,2)
plot(d.t,d.tracker.actual,'.','MarkerSize',0.2);
ylabel('pointing dir');
axis tight
xlim(xl);
legend({'az','el','dk'});
subplot_stack2(9,1,2)

% az el offsets
subplot_stack(9,1,3)
off=d.tracker.scan_off+d.tracker.horiz_off;
plot(d.t,off(:,1:2));
ylabel('pointing offsets');
xlim(xl);
legend({'az','el'});
subplot_stack2(9,1,3)

% focal plane temp
subplot_stack(9,1,4)
y=d.fridge.grttemp(:,3);
plot(d.t,y,'.','MarkerSize',0.2);
ylabel('fridge.grttemp');
xlim(xl);
ylim([min(y)-0.001,max(y)+0.001]);
subplot_stack2(9,1,4)

%subplot_stack(9,1,5)
%plot(d.t,d.bias.pidEnable,'.','MarkerSize',0.2);
%xlim(xl);
%keyboard

% Get array info
[p,ind]=get_array_info(tag); p.chan=p.adc_channel+1;

% All bolometers
subplot_stack(9,1,5)
ind=(strcmp(p.channel_type,'LIGHT')|strcmp(p.channel_type,'DARK'))&strcmp(p.channel_status,'OK');
plot(d.t,d.lockin.adcData(:,p.chan(ind)),'.','MarkerSize',0.2);
ylabel('all bolo');
xlim(xl);
subplot_stack2(9,1,5)

% filter to raster mapping or crossing parts only
d=framecut(d,logical(bitand(d.frame.features,1)==1));

% Light bolo scanning
subplot_stack(9,1,6)
ind=strcmp(p.channel_type,'LIGHT')&strcmp(p.channel_status,'OK');
plot(d.t,d.lockin.adcData(:,p.chan(ind)),'.','MarkerSize',0.2);
ylabel('light scanning');
xlim(xl); ylim([-5,5]);
subplot_stack2(9,1,6)

% Dark bolo scanning
subplot_stack(9,1,7)
ind=strcmp(p.channel_type,'DARK')&strcmp(p.channel_status,'OK');
plot(d.t,d.lockin.adcData(:,p.chan(ind)),'.','MarkerSize',0.2);
ylabel('dark scanning');
xlim(xl); ylim([-5,5]);
subplot_stack2(9,1,7)

% Motor current
%subplot_stack(9,1,7)
%plot(d.t,d.pmac.mtr_com_i(:,2),'.','MarkerSize',0.2);
%ylabel('el motor current');
%xlim(xl);

% tracking error
subplot_stack(9,1,8)
plot(d.t,d.tracker.errors,'.','MarkerSize',0.2);
ylim([-0.1,0.1]);
xlabel('sec into day (UTC)'); ylabel('track err');
legend({'az','el','dk'});
xlim(xl);
subplot_stack2(9,1,8)

% enc_az_diff
subplot_stack(9,1,9)
plot(d.t,d.pmac.enc_az_diff,'.','MarkerSize',0.2);
xlabel('sec into day (UTC)'); ylabel('end az diff');
xlim(xl);
subplot_stack2(9,1,9)

keyboard

drawnow
mkgif(sprintf('checkplot_%s.gif',tag));

return

figure(2); clf
ind=find(strcmp(p.channel_type,'LIGHT')&strcmp(p.channel_status,'OK')&p.frequency==150);
for i=1:length(ind)
  [n]=sscanf(p.channel_name{ind(i)},'%d');
  subplot(10,2,n(1));
  if(p.channel_name{ind(i)}(end)=='A')
    plot(d.t,d.lockin.adcData(:,p.chan(ind(i))),'b.','MarkerSize',0.2);
  else
    hold on
    plot(d.t,d.lockin.adcData(:,p.chan(ind(i))),'r.','MarkerSize',0.2);
    hold off
  end
  ylabel(p.channel_name{ind(i)}(1:end-2));
  ylim([-5,5]); box on
end

figure(3); clf
ind=find(strcmp(p.channel_type,'LIGHT')&strcmp(p.channel_status,'OK')&p.frequency==100);
for i=1:length(ind)
  [n]=sscanf(p.channel_name{ind(i)},'%d');
  subplot(6,2,n(1));
  if(p.channel_name{ind(i)}(end)=='A')
    plot(d.t,d.lockin.adcData(:,p.chan(ind(i))),'b.','MarkerSize',0.2);
  else
    hold on
    plot(d.t,d.lockin.adcData(:,p.chan(ind(i))),'r.','MarkerSize',0.2);
    hold off
  end
  ylabel(p.channel_name{ind(i)}(1:end-2));
  ylim([-5,5]); box on
end

figure(4); clf
ind=find(strcmp(p.channel_type,'DARK')&strcmp(p.channel_status,'OK'));
for i=1:length(ind)
  subplot(2,2,floor(((ind(i)-62)+1)/2));
  if(p.channel_name{ind(i)}(end)=='A')
    plot(d.t,d.lockin.adcData(:,p.chan(ind(i))),'b.','MarkerSize',0.2);
  else
    hold on
    plot(d.t,d.lockin.adcData(:,p.chan(ind(i))),'r.','MarkerSize',0.2);
    hold off
  end
  ylabel(p.channel_name{ind(i)}(1:end-2));
  ylim([-5,5]); box on
end

figure(5); clf
ind=find(strcmp(p.channel_type,'LIGHT')&strcmp(p.channel_status,'OK')&p.frequency==150);
c=cov(d.lockin.adcData(:,p.chan(ind)));
imagesc(c); colorbar; axis square
set(gca,'YTick',1:length(c));
set(gca,'YTickLabel',p.channel_name(ind))
title('150GHz channel covariance')

figure(6); clf
ind=find(strcmp(p.channel_type,'LIGHT')&strcmp(p.channel_status,'OK')&p.frequency==100);
c=cov(d.lockin.adcData(:,p.chan(ind)));
imagesc(c); colorbar; axis square
set(gca,'YTick',1:length(c));
set(gca,'YTickLabel',p.channel_name(ind))
title('100GHz channel covariance')

keyboard

%gtitle(runname)

return
