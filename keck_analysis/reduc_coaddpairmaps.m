function reduc_coaddpairmaps(tags,coaddopt)
% reduc_coaddpairmaps(tags,coaddopt)
%
% coadd map making is now a three step process:
%   - reduc_makepairmaps accumulates tod into map for each pair for
%     each scanset
%   - reduc_coaddpairmaps coadds over pairs and scansets
%   - make_map finalizes the maps
%
% this prog loops over tags:
%   - read in pairmaps
%   - accumulate over tags and pairs to make all jackknifes
%
% ----------------
% coaddopt options
% ----------------
%
% sernum - specifies the serial number and realization to coadd (no default, must be
%          specified )
%
% jacktype - is jack knife split types (2nd map dim). Note default is to gen all if
%            coaddtype=0 or 1, gen only 0 if coaddtype>1. It is specified as single
%            string, i.e. '0123456789abcd'. For backwards compatibility if only
%            requesting numeric jacks, a numeric vector may be used that is then
%            converted to a string.
%   0 = no jack knife - accumulate single 100/150GHz maps
%   ~=0 means accumulate maps splitting data various ways:
%   1 = deck split
%   2 = Forward, Backward scans
%   3 = 1st half of tag list versus 2nd half
%   4 = Tile split
%   5 = Phases B/C/E vs. F/H/I
%   6 = mux column split (even vs. odd)
%   7 = alternative deck split
%   8 = mux row split (0-16 vs. 17-33)
%   9 = dk/tile (tile orientation) split
%   a = focal plane inner / outer
%   b = tile top / bottom
%   c = tile inner / outer
%   d = moon up / down
%   e = best/worst A/B offsets (defined by 'obs' input to get_array_info)
%   f = reserved for frequency jack (reused as "experiment jack", not made by default)
%   g = pulsetube sensitivity
%   h = diff ellip plus split at median value of rgl's
%   i = diff ellip cross split at median value of rgl's
%
% coaddtype -  controls coadding over pairs (1st map dim)
%   0 = coadd over pairs (default)
%   1 = make ind rx T,Q,U maps
%   2 = make ind pair T,Q,U maps
%   3 = make ind pair sum/diff maps
%   4 = make ind channel maps (requires that input come from reduc_makeabmaps)
%   5 = make per tile maps
% 
% deproj - is...
%  if mapopt.deproj is not cell
%    vector specifying which differential beam modes to deproject
%      deproj(1) = relgain
%      deproj(2) = A/B offsets
%      deproj(3) = beamwidth
%      deproj(4) = ellipticity
%      deproj(5) = beam map residual template cleaned dp1100
%      deproj(6) = beam map residual template uncleaned
%    for each element
%      1 = regress against the data to find coefficient
%      2 = take coefficient as returned by get_array_info
%      3 = take coefficient from coaddopt.bav (as input from the
%      output of a previous run - see function avdeprojcoeff)
%    if deproj is an array then each row will be treated as a
%    separate set of deproj options. (mixing length 4 and length 6
%    option sets is not allowed.)
%  if mapopt.deproj = cell
%    arbitrarily sized vector specifying which templates to deproject. The ordering of
%    templates is user specified in mapopt.deproj.
%  if coaddopt.deproj is false no deprojection is done and nothing is appended to filename
%     (default)
%
% deproj_timescale = 'by_scanset'
%                    'by_phase' (default)
%                    'by_raster'
%                    'forever'
%
% daughter - default 'a'; specifies the daughter realization to output - by varying
%            the tags, chflags and cut inputs one can create various daughter
%            realization outputs
%
% do_covariance - controls whether to coadd terms for the covariance matrix This is only
%                 possible in a second coadd of the data.  If do_covariance~=0 then you
%                 must also specify the fully coadded ac to do
%                 signal subtractions. Specify with coaddopt.m and
%                 coaddopt.ac.wz, etc.
%                  0 = do not calculate covariance (default)
%                  1 = calculate TT block terms
%                  2 = calculate TQ and TU block terms
%                  3 = calculate QQ, QU, and UU block terms
%                  4 = calculate all block terms serially (requires lots of memory)
%                     
% chflags - a structure of additional files which get_array_info
%           should read from the aux_data area and apply cuts based on the
%           parameters found there. For instance if we have beams files we could
%           do:
%            chflags.filebase={'beams/beams_dummy'}
%            chflags.par_name={'fwhm_maj'}
%            chflags.low=.4
%            chflags.high=.6
%          or:
%            chflags=get_default_chflags('bicep2')
%
% cut - a structure of "second round" cut thresholds
%
% sign_flip_type = false (default)   - do not do sign flip for pseudo-noise
%                                      realizations (default) 
%                = 'by_scanset'      - flips the sign the data if the corresponding
%                                      element of sign_flip_seq is true
%                Note: If a full season coaddopt.ac and coaddopt.m is specified, then the
%                signal is subtracted from wcz etc immediately
%                before the sign flipping process
%
% sign_flip_seq - defines sequence of pluses and minuses used in constructing sign flip
%                 pseudo noise realizations. Not defined by default, and only used if
%                 coaddopt.sign_flip_type is specified. The first
%                 row is the flip sequence for pair sum and the
%                 second row the corresponding sequence for pair diff.
%
% tagsublist - defines the cannonical tag "flavors" to be used when coadding tag-subset,
%              signal only sim realizations over a full tag list. It is a two line cell
%              array. For each element in the first row the corresponding second row
%              element gives the tag which should be substituted.
%  
% save_cuts = true (default) - save all cut info.
%           = false          - cut structure is not saved, use only for repetetive
%                              realizations of signflip noise 
%
% realpairmapset = string specifying which real pairmaps to use when loading in real
%                  cuts and weights. Default coaddopt.sernum(1:4), i.e. the real
%                  pairmaps corresponding to the same serial number. This can be a
%                  dangerous option and should not ordinarily be used.
%
% diff_x_ellip_sub = 'fixed'. This flag is marks coadds that were made
%                             once differential ellipticity 
%                             subtraction has been corrected on July 16 2014
%
% recalopt - A structure containing information to perform a per-tag rescaling
%            of detector gains. At a minimum, recalopt.cal is required.
%              -> size(recalopt.cal) = [ntags, nfreqs]
%            Other struct members may be added for annotation purposes (i.e. to
%            identify the analysis source for the particular recalibration.)
%
% ---------------------------------------------------
% Following options don't actually do anything in this prog except
% control which pairmap files are read in:
%
%   filt is string specifying half scan filtering method
%
%   weight is half scan weighting in co-add
%     0 = uniform (no weighting)
%     1 = weight by reciprocal of half scan variance pre-filtering
%     2 = weight by reciprocal of half scan variance post-filtering
%     3 = weight by reciprocal of scan set variance post-pairdiff&filtering
%
%   gs is a flag indicating that real data is to be ground subtracted. do the same on the
%   sims (default =  0)
%
%   proj is projection used for map 'radec' 'ortho' 'arc', or 'tan'
% ----------------------------------------------------
%
% e.g.
%      tags=get_tags('cmb2011');
%      clear coaddopt; coaddopt.sernum='0001real';
%      reduc_coaddpairmaps(tags,coaddopt)

if isdeployed
  % farmit set's matlab mcr cache to scratch. Make sure it is usable to everyone.
  system_safe('chmod -R g+rwX /scratch/.mcrCache4 || true');   
  % save the farmfile name for later deletion
  farmfile=tags;
  % if using the compiled version of this code, tags will instead point to the .mat
  % farmfile.  Load it to get coaddopts and tag list.  If running the normal,
  % non-compiled version of this code, this if block is not executed.
  x=load(tags);
  % if compiled function is called from runsim (farmit) then tags is the farmfile.
  % If running the compiled version directly from the shell prompt, then tags should be
  % the location of a .mat file that contains the variables tags and coaddopt 
  % also, simopt points to the full path directory where reduc_makesim lies.
  if isfield(x,'val')
    % executed if compiled code is called from farm_coaddpairmaps
    % find which element stores the tags.
    tagvar=find(strcmp(x.var,'tags'));
    tags=x.val{tagvar};
    % find which element stores the coadd options
    coaddoptvar=find(strcmp(x.var,'coaddopt'));
    coaddopt=x.val{coaddoptvar};
  else
    % executed if compiled code run from shell prompt
    tags=x.tags;
    coaddopt=x.coaddopt;
  end
  clear x tagvar coaddoptvar  
end

if(~exist('tags','var'))
  tags=[];
end
if(isempty(tags))
  tags=get_tags;
end

if(~isfield(coaddopt,'sernum'))
  error('No serial number (coaddopt.sernum) specified! Cannot proceed!')
end

% record the list of tags to be used for round2 cuts
if ~isfield(coaddopt,'tags')
  coaddopt.tags=tags;
end

coaddopt=get_default_coaddopt(coaddopt);

% output coaddopt for the record
coaddopt
  
% get the filename saving the maps
fname=get_map_filename(coaddopt);

if ~iscell(fname)
  fname={fname};
end

% make sure output sub-directory exists
coadddir=fileparts(fname{1});
if ~isdir(coadddir)
  mkdir(coadddir)
end

% accumulate over tags
[acc,m,coaddopt]=accumulate(tags,coaddopt);

% now really record the list of tags whose pairmaps were coadded
coaddopt.tags=tags;

% option to remove the cut stucture which is 
% usefull for repetetive realizations of signflip noise
if isfield(coaddopt,'c') && ~coaddopt.save_cuts
  coaddopt = rmfield(coaddopt,'c');
end
if isfield(coaddopt,'hsmax') && ~coaddopt.save_cuts
  coaddopt = rmfield(coaddopt,'hsmax');
end

% this helps reduce file size
if isfield(coaddopt,'c')
  coaddopt.c.cp=structcat(1,coaddopt.c.cp);
  coaddopt.c.c1=structcat(1,coaddopt.c.c1);
  coaddopt.c.c2=structcat(1,coaddopt.c.c2);
end
if isfield(coaddopt,'hsmax')
  coaddopt.hsmax=structcat(1,coaddopt.hsmax);
end

% remove the deproj recordkeeping if we didn't deproject
if ~any(coaddopt.deproj)
  coaddopt=rmfield(coaddopt,'bi');
  coaddopt=rmfield(coaddopt,'b');
  coaddopt=rmfield(coaddopt,'bw');
end

% "backup" the coaddopt structure
coaddopt_bu=coaddopt;

% write a separate file for each requested jackknife
for i=1:length(coaddopt_bu.jacktype)

  % adapt coaddopt to this jack type
  coaddopt=coaddopt_bu;
  coaddopt.jacktype=coaddopt.jacktype(i);
  coaddopt.jackname=coaddopt.jackname{i};
  coaddopt.jackmask=coaddopt.jackmask(i,:);
 
  %also write a seperate file per deproj
  for kk=1:size(coaddopt_bu.deproj,1) 

    % par down the deprojection coefficients
    if isfield(coaddopt_bu,'b')
      coaddopt.b=coaddopt_bu.b(:,:,:,:,kk);
      coaddopt.bw=coaddopt_bu.bw(:,:,:,kk);
    end
    coaddopt.deproj=coaddopt_bu.deproj(kk,:);

    % mat files are compressed - and this apparently makes them smaller
    ac=desparse_ac(acc{i,kk});
  
    % real coadd maps files are very big due to storage of
    % diagnostics - need to be saved as v7.3
    disp(['Saving output file: ',fname{i,kk}])
    saveandtest(fname{i,kk},'ac','m','coaddopt','-v7.3');
    setpermissions(fname{i,kk});
  end
end

% if using the compiled version of the code, the .mat farmfile will not get deleted by
% farmit.m, so delete it here since the code should have successfully finished if it's
% made it to this point.
if isdeployed
  delete(farmfile);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [acc,m,coaddopt]=accumulate(tags,coaddopt)

% initialize index for storing the regression coefficients
bi=0;

% read each file of data
ii=1;
for i=1:length(tags)
  
  % READ IN THE DATA, UNPACK AND GET THE CHANNEL LIST

  % read in the ac file
  if(~isfield(coaddopt,'tagsublist'))
    filename=get_pairmap_filename(tags{i},coaddopt);
    disp(sprintf('\nloading ac from %s',filename));
    load(filename);
    
    % screwed up and put a bogus simopt into 1620/real pairmaps -
    % causes trouble - take it out
    if(strfind(filename,'1620/real'))
      if(isfield(mapopt,'simopt'))
        mapopt=rmfield(mapopt,'simopt');
      end
    end
    
  else
    % lookup which tag we are in sublist (normally tags and
    % coaddopt.tagsublist will be the same length but we might want
    % the latter to be a superset of the former so do it this way
    % for greater generality)
    thistag=strmatch(cvec(tags{i}),cvec(coaddopt.tagsublist(1,:)));
    % read in the tag which is to be substituted
    filename=get_pairmap_filename(coaddopt.tagsublist{2,thistag},coaddopt);
    % we always want weight 0 when doing substitution
    n=strfind(filename,'weight'); filename(n+6)='0';
    disp(sprintf('\nloading ac from %s as substitution for %s',...
                 filename,tags{i}));
    load(filename);
  end
  
  [day,phase,scanset,dk]=parse_tag(tags{i});

  % unpack the ac structure
  ac=acunpack(ac);
  
  % check if the non-genereralized version of 
  % deprojection was used, if yes convert it:
  % (should that be before or after acunpack ?)
  ac = check_deproj_templates(ac);
  
  % Get the set of good channels for this day - use chflags structure
  % to control the set of used channels.
  % This is in effect an additional "second round cut" - note that this also
  % effects some of the cuts proper because some depend on the set of
  % good channels to determine what fraction of potentially good
  % channels are passing.
  [p,ind]=get_array_info(tags{i},[],[],[],[],coaddopt.chflags);

  % store the mapopt/ind - this results in very big size so only do it for
  % first scanset
  if(i==1)
    coaddopt.mapopt{i}=mapopt;
    coaddopt.ind(i)=ind;
    coaddopt.p(i)=p;
    
    % If this is a second pass at reduc_coaddpairmaps for calculating noise,
    % then we need a template map to get deviations.
    % do this here since coadd_ac_overfreq need the coaddopt to have the
    % p and ind structures - however, only do it once.
    if isfield(coaddopt,'ac')
      % gives a freqs x 1 size ac structure on per freq basis
      % freqs are in increasing order, e.g. 100, 150 :
      acs = coadd_ac_overfreq(coaddopt.ac,coaddopt);
      % freqs x 1 size map
      coaddopt.map=make_map(acs,coaddopt.m,coaddopt);
      clear acs;
    end

  end
  
  % pre-allocate regression coefficients for efficiency.
  % do this even when not deprojecting, as we want to use the same tag
  % groupings either way.
  if i==1
    coaddopt=preallocate_b(coaddopt,tags,ind);
  end
 
  % DO PAIRMAP RECALIBRATION, IF RECALOPT EXISTS

  if isfield(coaddopt,'recalopt') && ~isempty(coaddopt.recalopt)
    % Using farm_coaddpairmaps splits up the tag list, and the value passed
    % into tags is that truncated list. The signflip sequence, though, needs
    % to know the whole tag list to be coadded, so that is already placed
    % into coaddopt.tags. We use that to match the current tag which is
    % loaded with an index into the recalopt.cal values.
    tagidx = strmatch(tags{i}, coaddopt.tags);

    % Default to unity scaling (mainly for dark detectors which don't match a
    % particular frequency)
    scale = ones(1, length(ind.a));
    % Then set per-frequency corrections
    freqs = unique(p.band(ind.la));
    for f=1:length(freqs)
      scale(p.band(ind.a)==freqs(f)) = coaddopt.recalopt.cal(tagidx,f);
    end

    % Calibrate all the elements of the ac structure
    ac = cal_coadd_ac(ac, scale, coaddopt);
    
    clear tagidx freqs scale
  end

  % DO "SECOND ROUND" CUTS
  
  % if cuts have been specified
  if(~isempty(coaddopt.cut))
    
    if(isfield(mapopt,'simopt'))
      % if we are sim get the cut params from the corresponding real pairmaps file
      coaddoptreal=coaddopt;
      coaddoptreal.sernum=sprintf('%s%s',coaddopt.realpairmapset,'real');
      if isfield(coaddopt,'realpairmapopt')
        fld=fieldnames(coaddopt.realpairmapopt);
        for k=1:numel(fld)
          coaddoptreal=setfield(coaddoptreal,fld{k},getfield(coaddopt.realpairmapopt,fld{k}));
        end
      end
      filename=get_pairmap_filename(tags{i},coaddoptreal);
      disp(sprintf('loading round2 cut parameters from %s',filename));
      x=load(filename,'mapopt');
      mapopt.c=x.mapopt.c;
      
      if(isfield(coaddopt,'tagsublist'))
        % if we are using tag substitution, reweight substituted ac
        % with weights and variances from real pairmap
        disp('tag substitution sim in effect: doing reweight')
        tic
          ac=redo_weights(ac,x.mapopt.hs,ind);
        toc
        % copy in the weights and std's which now apply (these
        % are only used for diagnostics but might as well)
        mapopt.hs.w=x.mapopt.hs.w; mapopt.hs.s=x.mapopt.hs.s;
      end
      
      clear x
    end
  
    % evaluate the second round cuts
    [c,mapopt.c.cp]=eval_round2_cuts(mapopt.c.cp,mapopt.c.c,coaddopt.cut,p,ind);
    
    % combine the cuts to get final channel selection
    c.overall=combine_cuts(c,ind);
    
    % convert good pair list from get_array_info above into a cut for
    % later visualization
    c.rgl=false(1,c.nch); c.rgl(ind.rgl)=true;
    
    % delete all flagged sums/diffs from the relevant fields of the ind
    % structure - this applies the cut
    ind.rgl=intersect(ind.rgl,find(c.overall));
    ind.rgla=intersect(ind.rgla,find(c.overall));
    ind.rglb=intersect(ind.rglb,find(c.overall));
    
    % compress the cutparams and and per scanset cut masks - impossible
    % to store per scanset resolution info over 1000's of scansets
    mapopt.c.cp=compress_perhs(mapopt.c.cp);
    mapopt.c.c=compress_perhs(mapopt.c.c);
    
    % if we are not sim store the cut parameters and masks for later
    % reference. (There is little point in storing this info for sims as
    % for a given daughter set it will be identical to that for the real
    % maps.)
    if(~isfield(mapopt,'simopt'))
      coaddopt.c.cp(i)=mapopt.c.cp;
      coaddopt.c.c1(i)=mapopt.c.c;
      coaddopt.c.c2(i)=c;
    end
    
  end
  
  
  % RECORD POST CUT DIAGNOSTIC QUANTITIES
 
  % only do for real data...
  if(~isfield(mapopt,'simopt')) 
    % take maximum weight over the scanset
    coaddopt.hsmax(i).w=nanmax(mapopt.hs.w);
    % the maximum deviation - max(max-value-in-half-scan/std-of-half-scan)
    coaddopt.hsmax(i).d=nanmax(mapopt.hs.m./mapopt.hs.s);
    
    % mask to only the data which passed all cuts and is going into the maps
    k=setdiff(ind.e,ind.rgl);
    coaddopt.hsmax(i).w(k)=NaN;
    coaddopt.hsmax(i).d(k)=NaN;
  end

  % Also accumulate histogram of scanset/channel weights
  % accumulated over good sum and diff channels
  [bcs,ns]=hfill(mapopt.hs.w(:,ind.rgla),100,0,25);
  [bcd,nd]=hfill(mapopt.hs.w(:,ind.rglb),100,0,100);
  if(~isfield(coaddopt,'whist'))
    coaddopt.whist.sum.bc=bcs; coaddopt.whist.sum.n=ns;
    coaddopt.whist.dif.bc=bcd; coaddopt.whist.dif.n=nd;
  else
    coaddopt.whist.sum.n=coaddopt.whist.sum.n+ns;
    coaddopt.whist.dif.n=coaddopt.whist.dif.n+nd;
  end
    
  % coadd the histograms of "deviation" (tod/std) over good sum and
  % diff channels
  if(~isfield(coaddopt,'devhist'))
    coaddopt.devhist.sum.bc=mapopt.devhist.bc;
    coaddopt.devhist.dif.bc=mapopt.devhist.bc;
    coaddopt.devhist.sum.n=sum(mapopt.devhist.n(ind.rgla,:),1);
    coaddopt.devhist.dif.n=sum(mapopt.devhist.n(ind.rglb,:),1);
  else
    coaddopt.devhist.sum.n=coaddopt.devhist.sum.n+sum(mapopt.devhist.n(ind.rgla,:),1);
    coaddopt.devhist.dif.n=coaddopt.devhist.dif.n+sum(mapopt.devhist.n(ind.rglb,:),1);
  end

  % keep extra info to determine scanset flavor
  if(isfield(mapopt,'traj'))
    if(i==1)
      coaddopt.traj=mapopt.traj;
    else
      coaddopt.traj=structcat(1,[coaddopt.traj,mapopt.traj]);
    end
  end

  % replace pairmaps with weighted deviations
  % this is for instance the signal subtraction used in sign flip noise
  if isfield(coaddopt,'map')
    disp('Subtracting the template means from the ac.')
    % The ac structure coming from the pair maps is size = npairs x 2, where
    % 2 is left vs right going scans. Loop over the pairs:
    for jj=1:size(ac,1)
      % Do not subtract for dark channels:
      if any(ind.a(jj)==ind.d)
        continue;
      end
      % pic the right frequency band index for this pair. The coaddopt.map
      % has been coadded per freq above.
      f = get_band_ind(p,ind,p.band(ind.a(jj)));
      % loop over the scan direction:
      for kk=[1,2]
        % do the subtraction:
        ac(jj,kk).wz =sparse(ac(jj,kk).wz-ac(jj,kk).wsum.*coaddopt.map(f).T);
        ac(jj,kk).wcz=sparse(ac(jj,kk).wcz-ac(jj,kk).wcc.*coaddopt.map(f).Q-ac(jj,kk).wcs.*coaddopt.map(f).U);
        ac(jj,kk).wsz=sparse(ac(jj,kk).wsz-ac(jj,kk).wcs.*coaddopt.map(f).Q-ac(jj,kk).wss.*coaddopt.map(f).U);
      end
    end
  end
  
  % DO SIGN FLIPPING TO MAKE NOISE PSEUDO REALIZATIONS

  if(ischar(coaddopt.sign_flip_type))
    if(~isfield(coaddopt,'tags'))
      % normal situation - tag-in-list is just the point in the list
      til=i;
      if length(tags)~=length(coaddopt.sign_flip_seq)
        error(['Tag list does not match signflip sequence: ' ...
              'length(tags)~=length(coaddopt.sign_flip_seq)'])
      end
    else
      % lookup which tag-in-list we are
      til=strmatch(tags{i},coaddopt.tags);
      if length(coaddopt.tags)~=length(coaddopt.sign_flip_seq)
        error(['Tag list does not match signflip sequence: ' ...
              'length(coaddopt.tags)~=length(coaddopt.sign_flip_seq)'])
      end
    end
    
    switch coaddopt.sign_flip_type
     case 'by_scanset'
      % flip sign of whole scansets
      
      % if single row sign_flip_seq has been provided assume it
      % applies to both pair sum and pair diff
      if(size(coaddopt.sign_flip_seq,1)==1)
        coaddopt.sign_flip_seq=[coaddopt.sign_flip_seq;coaddopt.sign_flip_seq];
      end
      
      % invert the sum quantity
      if(coaddopt.sign_flip_seq(1,til))
        disp('sign flip whole scanset pair sum');
        for j=1:numel(ac)
          ac(j).wz    =-ac(j).wz;
        end
        % also flip the psub/gsub components
        if(isfield(ac,'wz_psub'))
          ac(j).wz_psub=-ac(j).wz_psub;
          ac(j).wz_gsub=-ac(j).wz_gsub;
        end
      end
      
      % invert the diff quantities
      if(coaddopt.sign_flip_seq(2,til))
        disp('sign flip whole scanset pair diff');
        for j=1:numel(ac)
          ac(j).wzdiff=-ac(j).wzdiff;
          ac(j).wcz   =-ac(j).wcz;
          ac(j).wsz   =-ac(j).wsz;
          % also flip the deproj. templates
          if(isfield(ac,'wcd'))
            for k=1:numel(ac(j).wcd)
              ac(j).wcd{k}=-ac(j).wcd{k};
              ac(j).wsd{k}=-ac(j).wsd{k};
            end
          end
          % also flip the psub/gsub components
          if(isfield(ac,'wcz_psub'))
            ac(j).wcz_psub=-ac(j).wcz_psub;
            ac(j).wsz_psub=-ac(j).wsz_psub;
            ac(j).wcz_gsub=-ac(j).wcz_gsub;
            ac(j).wsz_gsub=-ac(j).wsz_gsub;
          end
        end     
      end
        
     case 'none'
      % do nothing
      
    otherwise
      error('unknown sign flip type');
    end
  end

  % make an all zero ac structure - this is used in two places
  acz=make_aczero(ac(1));
  

  % ACCUMULATE TAGS UP TO DEPROJECTION TIMESCALE

  if(~exist('acp','var'))
    % if we are first (or only) tag of phase make an all zero acp
    acp=repmat(acz,size(ac));
  end
  
  % add this scanset to any previous using only good pairs for this
  % specific scanset - it's highly dynamic due to cuts
  [dum,j]=intersect(ind.a,ind.rgla);
  j=j(:)'; % guarantees row vector in all versions of Matlab
  if(coaddopt.coaddtype==4)
    j=[j, j+length(ind.a)];
  end
  for k=j
    acp(k,1)=addac(acp(k,1),ac(k,1));
    acp(k,2)=addac(acp(k,2),ac(k,2));
  end
 
  % there are several things which may trigger us to deproject and
  % coadd into jackknifes
  deproj=false;
  
  % use tag groupings already defined for deproj
  if(i==length(tags))
    % if this is the last tag we need to finish up
    deproj=true;
  elseif coaddopt.bi(i+1)~=coaddopt.bi(i)
    deproj=true;
  end

  if(~deproj)    
    % skip to next tag <-- NB SKIPS ALL CODE BELOW!
    continue
  end

  disp('done accumulating tags...')  
  
  % increment the regression coefficient by one
  bi=bi+1;
  
  
  % DO DEPROJECTION

  % get the parameters to use if any of the deproj coeffs are
  % to be fixed at externally derived values
  if(any(coaddopt.deproj(:)==2))
    [p,ind]=get_array_info(tags{i},'obs','obs','obs','obs',[],'obs','obs');
    % calculate gain as recip of ukpv
    g=1./p.ukpv; 
    % get the x/y centroid offsets
    [dec,ra]=reckon(0,0,p.r,p.theta-90-180);
    [p.x,p.y]=radec_to_gnomonic(ra,-dec,0,0);
    % translate ellip to s,c,p params
    [p.s,p.c,p.p]=egauss2_mmt2scp(p.fwhm_maj,p.fwhm_min,p.alpha+p.theta);
    % zero the delta parameters
    z=zeros(size(p.r));
    p.dg=z; p.dx=z; p.dy=z; p.ds=z; p.dc=z; p.dp=z; 
    % fill in the delta parameters
    gbar=(g(ind.a)+g(ind.b))/2;
    p.dg=(g(ind.a)-g(ind.b))./gbar;
    p.dx=p.x(ind.a)-p.x(ind.b);
    p.dy=p.y(ind.a)-p.y(ind.b);
    p.ds=p.s(ind.a)-p.s(ind.b);
    p.dc=p.c(ind.a)-p.c(ind.b);
    p.dp=p.p(ind.a)-p.p(ind.b);
    % average ukpv by pair
    p.ukpv=(p.ukpv(ind.a)+p.ukpv(ind.b))/2;
    % average the beam widths which are also used
    p.sigma=sqrt((p.fwhm_maj(ind.a).^2+p.fwhm_min(ind.a).^2+...
	p.fwhm_maj(ind.b).^2+p.fwhm_min(ind.b).^2)/4)/2.35;
    p.sigma(isnan(p.sigma))=0;
    % if available use "direct" diff ellip params instead - this is
    % a tiny refinement - see 20150910_diffellip_refined
    if(isfield(p,'dfwhm_maj'))
      [dum,p.dc,p.dp]=egauss2_mmt2scp(p.dfwhm_maj,p.dfwhm_min,p.dalpha+p.theta);
      p.dp=p.dp(ind.a); p.dc=p.dc(ind.a);
    end
      
  end
 
  % loop over sets of deprojection options
  for kk=1:size(coaddopt.deproj,1)

    % reinitialize ac here before starting to do deprojection so we know we are starting fresh
    ac=acp;

    % do deprojection/subtraction of templates
    if any(coaddopt.deproj(kk,:))
      disp('doing deprojection/subtraction of templates...')
      if(isfield(ac,'wcd'))
        % prepare point source mask on map, if requested
        if(isfield(coaddopt,'deproj_src')&&~isempty(coaddopt.deproj_src))
          [psm_x,psm_y]=meshgrid(m.x_tic,m.y_tic);
          psm=get_mask(psm_x,psm_y,coaddopt.deproj_src);
        else
          psm=[];
        end
        % do the regression on all possible pairs since the used set
        % varies by scanset
        [dum,j]=intersect(ind.a,ind.la);
        for k=j
          % note that we are allowing here seperate regressor scalings for
          % forward and backward scans - this may not be what we
          % would want
          coaddopt_tmp=coaddopt;
          coaddopt_tmp.deproj=coaddopt.deproj(kk,:);
          [ac(k,1),coaddopt.b(k,1,:,bi,kk),coaddopt.bw(k,1,bi,kk)]=regress_templates(ac(k,1),coaddopt_tmp,psm,p,k);
          [ac(k,2),coaddopt.b(k,2,:,bi,kk),coaddopt.bw(k,2,bi,kk)]=regress_templates(ac(k,2),coaddopt_tmp,psm,p,k);
      	  coaddopt.diff_x_ellip_sub='fixed'; %flag to say that the ...
      	  %    cross ellipticity subtraction has been fixed
      	end
      end
    end

    
    % ON FIRST ITERATION SETUP FOR THE CO-ADD
  
    disp('doing coadds over pairs')

    % if making (approx) ind pair sum/diff maps massage ac
    % structure to have the necessary fields
    if(coaddopt.coaddtype==3)
      ac=ac_pol_to_dif_quant(ac);
      % remake the ac zero structure
      acz=make_aczero(ac(1));
    end
    
    % setup the acc cell arrays to contain the various jack splits
    if(~exist('acc','var'))

      % fill the cell array of accumulation arrays
      for k=1:size(coaddopt.deproj,1);
        for j=1:length(coaddopt.jacktype)

          switch coaddopt.coaddtype
          case 0
            ncoadd=1;
          case 1
       	    ncoadd=numel(unique(p.rx));
          case 4
            ncoadd=numel(ind.a)+numel(ind.b);
          case 5
            ncoadd=numel(unique(p.rx))*numel(unique(p.tile(ind.l)));
          otherwise
            ncoadd=numel(ind.a);
          end
      
          switch coaddopt.jacktype(j)
          case '0'
            acc{j,k}=repmat(acz,[ncoadd,1]);
            % also preallocate for covariance terms if requested
            switch coaddopt.do_covariance
            case 1
              acz_cov.wdzwdz=zeros(m.nx*m.ny);
              acc_cov{j,k}=repmat(acz_cov,[ncoadd,1,]);
            case 2
              acz_cov.wdzwdcz=zeros(m.nx*m.ny);
              acz_cov.wdzwdsz=zeros(m.nx*m.ny);
              acc_cov{j,k}=repmat(acz_cov,[ncoadd,1,]);
            case 3
              acz_cov.wdczwdcz=zeros(m.nx*m.ny);
              acz_cov.wdczwdsz=zeros(m.nx*m.ny);
              acz_cov.wdszwdcz=zeros(m.nx*m.ny);
              acz_cov.wdszwdsz=zeros(m.nx*m.ny);
              acc_cov{j,k}=repmat(acz_cov,[ncoadd,1,]);
            case 4
              acz_cov.wdzwdz=zeros(m.nx*m.ny);
              acz_cov.wdzwdcz=zeros(m.nx*m.ny);
              acz_cov.wdzwdsz=zeros(m.nx*m.ny);
              acz_cov.wdczwdcz=zeros(m.nx*m.ny);
              acz_cov.wdczwdsz=zeros(m.nx*m.ny);
              acz_cov.wdszwdcz=zeros(m.nx*m.ny);
              acz_cov.wdszwdsz=zeros(m.nx*m.ny);
              acc_cov{j,k}=repmat(acz_cov,[ncoadd,1,]);
            end
            clear acz_cov
          otherwise
            acc{j,k}=repmat(acz,[ncoadd,2,]);
          end
      
        end % this jacktype  
      end % deprojs
    end % if first tag

    
    % ACCUMULATE TAGS AND PAIRS INTO THE VARIOUS JACK SPLITS
    
    ct=coaddopt.coaddtype;
    
    % pre-add over left-right
    aclr=repmat(acz,[size(ac,1),1]);
    for k=1:size(ac,1)
      aclr(k,1)=addac(aclr(k,1),ac(k,1));
      aclr(k,1)=addac(aclr(k,1),ac(k,2));
    end

    % pre-add over channels
    if ct==0
      acch=repmat(acz,[1,2]);
      for k=1:size(ac,1)
        acch(1,1)=addac(acch(1,1),ac(k,1));
        acch(1,2)=addac(acch(1,2),ac(k,2));
      end
      % (trick acquant into adding the one element)
      indch.a=1; indch.la=1; 
    else
      acch=ac;
      indch=ind;
    end

    % pre-add over channels and over left-right
    for k=1:size(acch,1)
      acchlr(k,1)=addac(acch(k,1),acch(k,2));
    end
  
    % also make terms for the noise covariance matrix if requested
    if coaddopt.do_covariance
      for k=1:size(acchlr,1)
        % Usually we only care about acchlr for the noise covariance.
        ac_cov(k,1)=do_covariance(acchlr(k,1),coaddopt);
      end
    end
  
    % make empty jackknife record keeping masks
    if(~isfield(coaddopt,'jackmask'))
      for j=1:length(coaddopt.jacktype)
        coaddopt.jackmask{j,1}=false(length(tags),length(ind.e));
        coaddopt.jackmask{j,2}=false(length(tags),length(ind.e));
      end
    end
    
    % do the actual jacks
    for j=1:length(coaddopt.jacktype)
      switch coaddopt.jacktype(j)

      case '0'
        % no-split - full coadd map
        coaddopt.jackname{j}='none';
    	acc{j,kk}(:,1)=acquant(p,indch,acc{j,kk}(:,1),acchlr,ct);
        
        % dummy jackmasks
        coaddopt.jackmask{j,1}(ii:i,ind.la)=true;
        coaddopt.jackmask{j,2}(ii:i,ind.la)=true;
        
      case '1'
    	% deck split
        coaddopt.jackname{j}='dk';
    	switch dk
        case {68,85,113,130,23,158}
          acc{j,kk}(:,1)=acquant(p,indch,acc{j,kk}(:,1),acchlr,ct);
          coaddopt.jackmask{j,1}(ii:i,ind.la)=true;
        case {248,265,293,310,338,203}
          acc{j,kk}(:,2)=acquant(p,indch,acc{j,kk}(:,2),acchlr,ct);
          coaddopt.jackmask{j,2}(ii:i,ind.la)=true;
	    end
         
      case '2'
        % scan direction split
    	coaddopt.jackname{j}='scan dir';
        acc{j,kk}(:,1)=acquant(p,indch,acc{j,kk}(:,1),acch(:,1),ct);
      	acc{j,kk}(:,2)=acquant(p,indch,acc{j,kk}(:,2),acch(:,2),ct);
        % dummy jackmasks
        coaddopt.jackmask{j,1}(ii:i,ind.la)=true;
        coaddopt.jackmask{j,2}(ii:i,ind.la)=true;
        
      case '3'
        % temporal split
        coaddopt.jackname{j}='1st/2nd half';
        if(~isfield(coaddopt,'temporaljack_splitdate'))
          % traditional definition - just half way through tag list
          n=round(length(tags)/2);
          h=(i>n)+1;
        else
          % split date specified
          if(str2num(tags{i}(1:8))>str2num(coaddopt.temporaljack_splitdate(1:8)))
            h=2;
          else
            h=1;
          end
        end
        acc{j,kk}(:,h)=acquant(p,indch,acc{j,kk}(:,h),acchlr,ct);
        coaddopt.jackmask{j,h}(ii:i,ind.la)=true;
        
      case '4'
    	% tile split
        % 1,2 vs 3,4 works very badly for the B2 choice of deck
        % angles - ends up effectively being a deck jack
        % instead do 1,3 vs 2,4
        coaddopt.jackname{j}='tile';
    	indc=ind;
        indc.la=[ind.la(p.tile(ind.la)==1),ind.la(p.tile(ind.la)==3)];
    	acc{j,kk}(:,1)=acquant(p,indc,acc{j,kk}(:,1),aclr,ct);
        coaddopt.jackmask{j,1}(ii:i,indc.la)=true;
        indc=ind;
    	indc.la=[ind.la(p.tile(ind.la)==2),ind.la(p.tile(ind.la)==4)];
        acc{j,kk}(:,2)=acquant(p,indc,acc{j,kk}(:,2),aclr,ct);
    	coaddopt.jackmask{j,2}(ii:i,indc.la)=true;
	
      case '5'
        % az jack
        coaddopt.jackname{j}='az';
        switch(get_experiment_name)
          case 'bicep2'
            switch phase
              case {'B','E','H'}
                acc{j,kk}(:,1)=acquant(p,indch,acc{j,kk}(:,1),acchlr,ct);
                coaddopt.jackmask{j,1}(ii:i,ind.la)=true;
              case {'C','F','I'}
    		acc{j,kk}(:,2)=acquant(p,indch,acc{j,kk}(:,2),acchlr,ct);
                coaddopt.jackmask{j,2}(ii:i,ind.la)=true;	    
            end
         case 'keck'	
          switch phase
            case {'B','E'}
              acc{j,kk}(:,1)=acquant(p,indch,acc{j,kk}(:,1),acchlr,ct);
              coaddopt.jackmask{j,1}(ii:i,ind.la)=true;
            case {'C','F'}
              acc{j,kk}(:,2)=acquant(p,indch,acc{j,kk}(:,2),acchlr,ct);
              coaddopt.jackmask{j,2}(ii:i,ind.la)=true;
          end
         case 'bicep3'
          switch phase
            case {'B','H'}
              acc{j,kk}(:,1)=acquant(p,indch,acc{j,kk}(:,1),acchlr,ct);
              coaddopt.jackmask{j,1}(ii:i,ind.la)=true;
            case {'D','F'}
              acc{j,kk}(:,2)=acquant(p,indch,acc{j,kk}(:,2),acchlr,ct);
              coaddopt.jackmask{j,2}(ii:i,ind.la)=true;
          end
        end

      case '6'
        % mux column split -- even columns vs. odd columns
        % even mux columns
        coaddopt.jackname{j}='mux col';
        indc=ind;
        indc.la=ind.la(~mod(p.mce_col(ind.la),2));
        acc{j,kk}(:,1)=acquant(p,indc,acc{j,kk}(:,1),aclr,ct);
        coaddopt.jackmask{j,1}(ii:i,indc.la)=true;
        % odd mux columns
        indc=ind;
        indc.la=ind.la(logical(mod(p.mce_col(ind.la),2)));
        acc{j,kk}(:,2)=acquant(p,indc,acc{j,kk}(:,2),aclr,ct);
        coaddopt.jackmask{j,2}(ii:i,indc.la)=true;

      case '7'
        % alternative deck split
        coaddopt.jackname{j}='alt dk';
        rinfo=get_run_info(tags(ii:i));
        [tmpyr tmpmo tmpdy tmphr tmpmn tmpsc]=datevec(utc2datenum(rinfo.tstart{1}));
        switch(get_experiment_name)
         case {'bicep2','keck'}
           if tmpyr<2013        
             switch dk
              case {68,85,293,310}
               acc{j,kk}(:,1)=acquant(p,indch,acc{j,kk}(:,1),acchlr,ct);
               coaddopt.jackmask{j,1}(ii:i,ind.la)=true;
              case {113,130,248,265}
               acc{j,kk}(:,2)=acquant(p,indch,acc{j,kk}(:,2),acchlr,ct);
               coaddopt.jackmask{j,2}(ii:i,ind.la)=true;
             end
           elseif tmpyr>=2013
             switch dk
              case {68,113,248,293}
               acc{j,kk}(:,1)=acquant(p,indch,acc{j,kk}(:,1),acchlr,ct);
               coaddopt.jackmask{j,1}(ii:i,ind.la)=true;
              case {23,158,203,338}
               acc{j,kk}(:,2)=acquant(p,indch,acc{j,kk}(:,2),acchlr,ct);
               coaddopt.jackmask{j,2}(ii:i,ind.la)=true;
             end
           end
         case 'bicep3'
          switch dk
           case {68,203}
            acc{j,kk}(:,1)=acquant(p,indch,acc{j,kk}(:,1),acchlr,ct);
            coaddopt.jackmask{j,1}(ii:i,ind.la)=true;
           case {23,248}
            acc{j,kk}(:,2)=acquant(p,indch,acc{j,kk}(:,2),acchlr,ct);
            coaddopt.jackmask{j,2}(ii:i,ind.la)=true;
          end
        end
        
      case '8'
        % mux row split -- 0-16 vs 17-33
        % mux rows 0-16
        coaddopt.jackname{j}='mux row';
        indc=ind;
        indc.la=ind.la(p.mce_row(ind.la)<=16);
        acc{j,kk}(:,1)=acquant(p,indc,acc{j,kk}(:,1),aclr,ct);
        coaddopt.jackmask{j,1}(ii:i,indc.la)=true;
        % mux rows 17-33
        indc=ind;
        indc.la=ind.la(p.mce_row(ind.la)>17);
        acc{j,kk}(:,2)=acquant(p,indc,acc{j,kk}(:,2),aclr,ct);
        coaddopt.jackmask{j,2}(ii:i,indc.la)=true;

      case '9'
        % dk/tile split to test for tile fixed beam effects which
        % otherwise evade jackknifes
        coaddopt.jackname{j}='tile/dk';
        indc12=ind; indc12.la=[ind.la(p.tile(ind.la)==1),ind.la(p.tile(ind.la)==2)];
        indc34=ind; indc34.la=[ind.la(p.tile(ind.la)==3),ind.la(p.tile(ind.la)==4)];
        switch dk
        case {68,85,113,130,23,158}
          acc{j,kk}(:,1)=acquant(p,indc12,acc{j,kk}(:,1),aclr,ct);
          coaddopt.jackmask{j,1}(ii:i,indc12.la)=true;
          acc{j,kk}(:,2)=acquant(p,indc34,acc{j,kk}(:,2),aclr,ct);
          coaddopt.jackmask{j,2}(ii:i,indc34.la)=true;
        case {248,265,293,310,203,338}
          acc{j,kk}(:,2)=acquant(p,indc12,acc{j,kk}(:,2),aclr,ct);
          coaddopt.jackmask{j,2}(ii:i,indc12.la)=true;
          acc{j,kk}(:,1)=acquant(p,indc34,acc{j,kk}(:,1),aclr,ct);
          coaddopt.jackmask{j,1}(ii:i,indc34.la)=true;
        end

      case 'a'
        % inner/outer FP split
        coaddopt.jackname{j}='FP inner/outer';
        rr=p.r(ind.la);
        indc=ind;
        indc.la=indc.la(rr>=median(rr));
        acc{j,kk}(:,1)=acquant(p,indc,acc{j,kk}(:,1),aclr,ct);
        coaddopt.jackmask{j,1}(ii:i,indc.la)=true;
    	indc=ind;
        indc.la=indc.la(rr<median(rr));
    	acc{j,kk}(:,2)=acquant(p,indc,acc{j,kk}(:,2),aclr,ct);
        coaddopt.jackmask{j,2}(ii:i,indc.la)=true;

      case 'b'
        % top/bottom of each tile split
        coaddopt.jackname{j}='tile top/bottom';
        det_row=p.det_row(ind.la);
    	indc=ind;
        indc.la=indc.la(det_row>=1 & det_row<=4);
        acc{j,kk}(:,1)=acquant(p,indc,acc{j,kk}(:,1),aclr,ct);
        coaddopt.jackmask{j,1}(ii:i,indc.la)=true;
    	indc=ind;
        indc.la=indc.la(det_row>=5 & det_row<=8);
        acc{j,kk}(:,2)=acquant(p,indc,acc{j,kk}(:,2),aclr,ct);
        coaddopt.jackmask{j,2}(ii:i,indc.la)=true;
        
      case 'c'
        % inner/outer tile split
        coaddopt.jackname{j}='tile inner/outer';
        det_row=p.det_row(ind.la);
        det_col=p.det_col(ind.la);
        row0=(max(det_row)+1)/2;
        col0=(max(det_col)+1)/2;
        det_r=sqrt((det_row-row0).^2 + (det_col-col0).^2);
    	indc=ind;
        indc.la=indc.la(det_r<=median(det_r));
        acc{j,kk}(:,1)=acquant(p,indc,acc{j,kk}(:,1),aclr,ct);
        coaddopt.jackmask{j,1}(ii:i,indc.la)=true;
        indc=ind;
        indc.la=indc.la(det_r>median(det_r));
    	acc{j,kk}(:,2)=acquant(p,indc,acc{j,kk}(:,2),aclr,ct);
        coaddopt.jackmask{j,2}(ii:i,indc.la)=true;
        
      case 'd'
        % moon up/down split
        coaddopt.jackname{j}='moon up/down';
        rinfo=get_run_info(tags(ii:i));
        [tmpyr tmpmo tmpdy tmphr tmpmn tmpsc]=datevec(utc2datenum(rinfo.tstart{1}));
        mjd=date2mjd(tmpyr,tmpmo,tmpdy,tmphr,tmpmn,tmpsc);
        [moonra,moondec]=moonpos(mjd);
        if moondec<=0      % el>=0, up
          acc{j,kk}(:,1)=acquant(p,indch,acc{j,kk}(:,1),acchlr,ct);
          coaddopt.jackmask{j,1}(ii:i,ind.la)=true;
        else               % el<0, down
          acc{j,kk}(:,2)=acquant(p,indch,acc{j,kk}(:,2),acchlr,ct);
          coaddopt.jackmask{j,2}(ii:i,ind.la)=true;
        end

      case 'e'
        % best/worst A/B offsets
        coaddopt.jackname{j}='diffpoint best/worst';
        [pe,inde]=get_array_info(tags{i},[],[],[],'obs');
        [x,y]=pol2cart(pe.theta*pi/180,pe.r);
        dx=x(inde.la)-x(inde.lb);dy=y(inde.la)-y(inde.lb);
        dp=sqrt(dx.^2+dy.^2);dpmed=nanmedian(dp);
        % CLP 150908: this doesn't seem good - it makes the split
        % line using all pairs not just the good ones. And it uses
        % a single split line regardless of frequency band - see
        % example of diff ellip jacks below for how to improve
        indc=ind;
        indc.la=indc.la(dp<=dpmed);
        acc{j,kk}(:,1)=acquant(p,indc,acc{j,kk}(:,1),aclr,ct);
        coaddopt.jackmask{j,1}(ii:i,indc.la)=true;
        indc=ind;
        indc.la=indc.la(dp>dpmed);
        acc{j,kk}(:,2)=acquant(p,indc,acc{j,kk}(:,2),aclr,ct);
        coaddopt.jackmask{j,2}(ii:i,indc.la)=true;

      case 'g'
        % most/least pulsetube sensitive 
        coaddopt.jackname{j}='pulsetube most/least';
        chflags_pt.filebase={'fp_data/fp_data_pt'};
        chflags_pt.par_name={'ptline'};
        % least
        chflags_pt.low=[-10]; chflags_pt.high=[1.05];
        [pg indg]=get_array_info(tags{i},[],[],[],[],chflags_pt);
        indc=ind;
        indc.la=indg.rgla;
        acc{j,kk}(:,1)=acquant(p,indc,acc{j,kk}(:,1),aclr,ct);
        coaddopt.jackmask{j,1}(ii:i,indc.la)=true;
        % most
        chflags_pt.low=[1.05]; chflags_pt.high=[10];
        [pg indg]=get_array_info(tags{i},[],[],[],[],chflags_pt);
        indc=ind;
        indc.la=indg.rgla;
        acc{j,kk}(:,2)=acquant(p,indc,acc{j,kk}(:,2),aclr,ct);
        coaddopt.jackmask{j,2}(ii:i,indc.la)=true;
     
      case {'h','i'}
        % diff ellip plus/cross above/below median
        % get the diff ellip pars from beam maps - include the channel
        % flags so the median is that of the pairs
        % actually being used in this run
        [pe,inde]=get_array_info(tags{i},[],[],'obs',[],coaddopt.chflags);
        switch coaddopt.jacktype(j)
          case 'h'
            coaddopt.jackname{j}='diff ellip plus';
            dpc_fp=pe.dp_fp;
          case 'i'
            coaddopt.jackname{j}='diff ellip cross';
            dpc_fp=pe.dc_fp;
        end
        % take the split lines as the median in each
        % band (assuming that we will look at the result coadded to
        % the frequency band level)
        if(isfield(inde,'rgl100a'))
          med100=nanmedian(dpc_fp(inde.rgl100a));
          l100af=inde.l100a(dpc_fp(inde.l100a)<=med100);
          l100as=inde.l100a(dpc_fp(inde.l100a)>med100);
        else
          l100af=[]; l100as=[];
        end
        if(isfield(inde,'rgl150a'))
          med150=nanmedian(dpc_fp(inde.rgl150a));
          l150af=inde.l150a(dpc_fp(inde.l150a)<=med150);
          l150as=inde.l150a(dpc_fp(inde.l150a)>med150);
        else
          l150af=[]; l150as=[];
        end
        if(isfield(inde,'rgl220a'))
          med220=nanmedian(dpc_fp(inde.rgl220a));
          l220af=inde.l220a(dpc_fp(inde.l220a)<=med220);
          l220as=inde.l220a(dpc_fp(inde.l220a)>med220);
        else
          l220af=[]; l220as=[];
        end
        indc=ind;
        indc.la=[l100af,l150af,l220af];
        acc{j,kk}(:,1)=acquant(p,indc,acc{j,kk}(:,1),aclr,ct);
        coaddopt.jackmask{j,1}(ii:i,indc.la)=true;
        indc=ind;
        indc.la=[l100as,l150as,l220as];
        acc{j,kk}(:,2)=acquant(p,indc,acc{j,kk}(:,2),aclr,ct);
        coaddopt.jackmask{j,2}(ii:i,indc.la)=true;
        
      end

      % fill in the ind.b entries corresponding to the ind.a's in the
      % jackmask - this is basically cosmetic but might as well
      [dum,pairs]=intersect(ind.la,find(coaddopt.jackmask{j,1}(i,:)));
      coaddopt.jackmask{j,1}(ii:i,ind.lb(pairs))=true;
      [dum,pairs]=intersect(ind.la,find(coaddopt.jackmask{j,2}(i,:)));
      coaddopt.jackmask{j,2}(ii:i,ind.lb(pairs))=true;    
      
    end % of this jack
  
    % Nest covariance stuff behind a separate loop that is ignored unless
    % called specifically.
    if coaddopt.do_covariance
      for jj=1:length(coaddopt.jacktype)
        switch coaddopt.jacktype
        case '0'
          acc_cov{jj,kk}(:,1)=acquant(p,indch,acc_cov{jj,kk}(:,1),ac_cov,ct);
        end
      end
    end
  
  end % of this deproj option

  % keep track of next tag number
  ii=i+1;

  % remove acp so that it starts fresh
  clear acp

end % of this deproj time block

% Merge acc_cov into acc.
if coaddopt.do_covariance
  for jj=1:length(coaddopt.jacktype)
    fn=[fieldnames(acc{jj});fieldnames(acc_cov{jj})];
    acc{jj}=cell2struct([struct2cell(acc{jj});struct2cell(acc_cov{jj})],fn,1);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function acc=acquant(p,ind,acc,ac,coaddtype)

% accumulate quantities over pairs

% the order of pairs in the ac array is the order of ind.a/b

switch coaddtype
  case 0 % coadd over all light pairs - the choice as to which are
        % good has already been made
    [dum,j]=intersect(ind.a,ind.la);
    k=ones(size(j));
    
  case 1 % make individual receiver maps
    [dum,j]=intersect(ind.a,ind.la);  
    k=p.rx(dum)+1;
    
  case {2,3} % make individual pair maps or sum&diff maps
    [dum,j]=intersect(ind.a,ind.la);
    k=j;

  case 4 % make individual channel maps
    j=1:(numel(ind.a)+numel(ind.b));
    k=j;

  case 5 % make individual tile maps
    [dum,j]=intersect(ind.a,ind.la);
    k=p.rx(dum)*numel(unique(p.tile(dum)))+p.tile(dum);

end

for i=1:numel(j)
  % add each field
  acc(k(i))=addac(acc(k(i)),ac(j(i)));
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%
function [ac,b,bw]=regress_templates(ac,coaddopt,psm,p,k)

% reduce maps to vectors of non-zero pixels; DO NOT USE ACPACK. Each regression over
% pairs takes 5.5 seconds with acpack / acunpack, .25 seconds the way it is below. Over
% many tags, this causes an unacceptable slowdown.
% Use psm if provided.
if isempty(psm)
  pixind=find(ac.wsum~=0 & isfinite(ac.wsum));
else
  pixind=find(ac.wsum~=0 & isfinite(ac.wsum) & ~psm);
end

% Expand out the modes we are deprojecting
if(~iscell(coaddopt.mapopt{1}.deproj))
  switch length(coaddopt.deproj)
   case 4
    % Standard modes of a gaussian
    dp=coaddopt.deproj([1,2,2,3,4,4]);
   case 5
    % Standard modes plus residual template
    dp=coaddopt.deproj([1,2,2,3,4,4,5]);
   case 6
    % Standard modes plus residual template, plus uncleaned
    % residual template
    dp=coaddopt.deproj([1,2,2,3,4,4,5,6]);
  end
else
  % arbitrarily specified templates
  
  % Check to see that the number of modes specified in coaddopt matches the number of
  % modes accumulated in reduc_makepairmaps
  if iscell(mapdp)
    ntemp=0;
    for kk=1:numel(mapdp)
      ntemp=ntemp+numel(mapdp{kk});
    end
  else
    ntemp=numel(mapdp);
  end
  if ntemp~=numel(coaddopt.deproj)
    error(['the number of elements of coaddopt.deproj must equal the number of templates ' ...
           'accumulated at pairmap stage and specified in mapopt.deproj']);
  end
  
  dp=coaddopt.deproj;
end
  
ntemplates=numel(dp);
nfit=sum(dp==1);

% set coeffs to NaN
b=NaN(ntemplates,1); bw=NaN;

% return if no data
if isempty(pixind)
  return
end

% pre-subtract any templates where coeff is known - from
% get_array_info for Gaussian modes and by dead reckoning for
% residual templates
for i=find(dp==2)
  switch i
   case 1 % relgain
    b(i)=p.dg(k)/(2*p.ukpv(k));
   case 2 % diff point x
    b(i)=p.dx(k)/(2*p.ukpv(k));
   case 3 % diff point y
    b(i)=p.dy(k)/(2*p.ukpv(k));
   case 4 % diff width
    b(i)=p.ds(k)*p.sigma(k)/(2*p.ukpv(k));
   case 5 % diff ellip plus
    b(i)=p.dp(k)*p.sigma(k)^2/(4*p.ukpv(k));
   case 6 % diff ellip cross
    b(i)=p.dc(k)*p.sigma(k)^2/(4*p.ukpv(k));
   case 7 % residual subtraction
    b(i)=1./(2*p.ukpv(k));
   case 8 % uncleaned residual subtraction
    b(i)=1./(2*p.ukpv(k));
  end
    
  % scale the templates
  ac.wcd{i}=ac.wcd{i}*b(i); ac.wsd{i}=ac.wsd{i}*b(i);
  % remove from the maps if coeff not nan
  if ~isnan(b(i))
    ac.wcz=ac.wcz-ac.wcd{i};
    ac.wsz=ac.wsz-ac.wsd{i}; 
  end
end

% pre-subtract any templates where coeff is average of previous run
for i=find(dp==3)
  b(i)=coaddopt.bav(k,i);
  % scale the templates
  ac.wcd{i}=ac.wcd{i}*b(i); ac.wsd{i}=ac.wsd{i}*b(i);
  % remove from the maps
  ac.wcz=ac.wcz-ac.wcd{i};
  ac.wsz=ac.wsz-ac.wsd{i}; 
end

% do regression if required for any templates
if(nfit>0)  
  % pre-allocate regressor for memory efficiency
  X=zeros(numel(pixind)*2,nfit);

  % Construct regressor
  xi=1;
  for i=find(dp==1)
    X(:,xi)=[ac.wcd{i}(pixind);ac.wsd{i}(pixind)];
    xi=xi+1;
  end

  % get the data to fit
  y=[ac.wcz(pixind);ac.wsz(pixind)];


  % Do weighted linear regression

  % Variance of wcz is wcc.
  v=[ac.wcc(pixind);ac.wss(pixind)];
  w=1./v;

  % Uniform weight linear regression
  b0=regress( y./sqrt(v) , X./sqrt(repmat(v,[1,size(X,2)])) );

  % Weighted linear regression (gives same answer as above)
  %b0=lscov(X,y,w);

  % Put fitted coeffs into b vector
  b(find(dp==1))=b0;
end

% The weight to use when combining regression coefficients over time
bw=nansum(ac.w(pixind));

% scale each deprojected mode by the regression coefficient and remove
for i=find(dp==1)
  % scale the templates
  ac.wcd{i}=ac.wcd{i}*b(i); ac.wsd{i}=ac.wsd{i}*b(i);
  % remove from the maps
  ac.wcz=ac.wcz-ac.wcd{i};
  ac.wsz=ac.wsz-ac.wsd{i}; 
end

% zero out un-used templates
for i=find(dp==0)
  % scale the templates
  ac.wcd{i}=ac.wcd{i}*0; ac.wsd{i}=ac.wsd{i}*0;
end
% get rid of extra ones not mentioned in dp (if any)
for i=(length(dp)+1):length(ac.wcd)
  ac.wcd{i}=ac.wcd{i}*0; ac.wsd{i}=ac.wsd{i}*0;
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ac=check_deproj_templates(ac)
%look up if the old deproj structure is there,
%if yes, convert it...:
if(isfield(ac,'wcd1'))
  for j=1:size(ac,1)
    for n=1:size(ac,2)
      ac(j,n).wcd{1} = ac(j,n).wcd1;
      ac(j,n).wcd{2} = ac(j,n).wcd2;
      ac(j,n).wcd{3} = ac(j,n).wcd3;
      ac(j,n).wcd{4} = ac(j,n).wcd4;
      ac(j,n).wcd{5} = ac(j,n).wcd5;
      ac(j,n).wcd{6} = ac(j,n).wcd6;
      
      ac(j,n).wsd{1} = ac(j,n).wsd1;
      ac(j,n).wsd{2} = ac(j,n).wsd2;
      ac(j,n).wsd{3} = ac(j,n).wsd3;
      ac(j,n).wsd{4} = ac(j,n).wsd4;
      ac(j,n).wsd{5} = ac(j,n).wsd5;
      ac(j,n).wsd{6} = ac(j,n).wsd6;
    end
  end
  %and get rid of the old data fields:
  ac = rmfield(ac,{'wsd1','wsd2','wsd3','wsd4','wsd5','wsd6'});
  ac = rmfield(ac,{'wcd1','wcd2','wcd3','wcd4','wcd5','wcd6'});
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function coaddopt=preallocate_b(coaddopt,tags,ind)

npairs=numel(ind.a);
ndp=size(coaddopt.deproj,1);
if(~iscell(coaddopt.mapopt{1}.deproj))
  switch length(coaddopt.deproj)
   case 1
    % Used for weekly reduction perphase maps where deprojection templates aren't
    % accumulated.  Need a value for ncoeff so assignment of coaddopt.b below doesn't
    % fail. coaddopt.bi .b and .bw will be removed later in the main function. 
    ncoeff=1;
   case 4
    % Standard modes of a gaussian
    ncoeff=6;
   case 5
    % Standard modes with plus residual template
    ncoeff=7;
   case 6
    % Standard modes with plus residual template, plus uncleaned
    % residual template
    ncoeff=8;
  end
else
  % deprojecting arbitrary number of templates
  ncoeff=numel(coaddopt.mapopt{1}.deproj);
end

switch coaddopt.deproj_timescale
 case 'by_scanset'
  nt=numel(tags);
  idx=1:numel(tags);
 case 'by_phase'
  % use 'inclusive' option to keep tag together across a schedule
  % restart mid-phase
  [ph,idx]=taglist_to_phaselist(tags,'inclusive');
  nt=numel(ph.name);
 case 'by_raster'
  [ph,idx]=taglist_to_phaselist(tags,'inclusive','raster');
  nt=numel(ph.name);
 case 'forever'
  nt=1;
  idx=ones(size(tags));
end

% Assign tags to deproj groups here, and use this same assignment throughout.
coaddopt.bi=idx;
coaddopt.b=NaN(npairs,2,ncoeff,nt,ndp);
coaddopt.bw=NaN(npairs,2,nt,ndp);

return


%%%%%%%%%%%%%%%%%%%%%%%%%
function ac=desparse_ac(ac)

% de-sparse ac
for i=1:numel(ac)
  aci=ac(i);
  fn=fieldnames(aci);
  for k=1:numel(fn)
    if iscell(aci.(fn{k}))
      for j=1:numel(aci.(fn{k}))
        aci.(fn{k}){j}=full(aci.(fn{k}){j});
      end
    else
      aci.(fn{k})=full(aci.(fn{k}));
    end
  end
  ac(i)=aci;
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%
function ac_cov=do_covariance(ac,coaddopt)

% Convert the weighted deviations into long (row) vectors.
[m,wdz]=map2vect(coaddopt.m,ac.wz);
[m,wdcz]=map2vect(coaddopt.m,ac.wcz);
[m,wdsz]=map2vect(coaddopt.m,ac.wsz);

switch coaddopt.do_covariance
  case 1
    ac_cov.wdzwdz=wdz'*wdz;
  case 2
    ac_cov.wdzwdcz=wdz'*wdcz;
    ac_cov.wdzwdsz=wdz'*wdsz;
  case 3
    ac_cov.wdczwdcz=wdcz'*wdcz;
    ac_cov.wdczwdsz=wdcz'*wdsz;
    ac_cov.wdszwdcz=wdsz'*wdcz;
    ac_cov.wdszwdsz=wdsz'*wdsz;
  case 4
    ac_cov.wdzwdz=wdz'*wdz;
    ac_cov.wdzwdcz=wdz'*wdcz;
    ac_cov.wdzwdsz=wdz'*wdsz;
    ac_cov.wdczwdcz=wdcz'*wdcz;
    ac_cov.wdczwdsz=wdcz'*wdsz;
    ac_cov.wdszwdcz=wdsz'*wdcz;
    ac_cov.wdszwdsz=wdsz'*wdsz;
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%
function ac=ac_pol_to_dif_quant(ac)
% reconstruct the unpolarized quantities needed to make pair diff
% maps
% In principle things are engineered so the pol angle can vary freely
% in the tod. In practice it is nearly constant during a scanset
% and so we can back out the cosine to recover simple pair diff
% quantities

for i=1:numel(ac)
  % fetch the mean cosine in each pixel
  c=ac(i).wc./ac(i).w;
  % arrange for unhit areas to remain zero in wcz etc below -
  % rather than become NaN
  c(ac(i).w==0)=inf;
  
  % wdiff already exists and can't get rid of it as used for noise
  % estimation - so no need to do anything for actual pair diff
  % signal
  
  % can't call wwv as already used for pair sum
  ac(i).wwvd=ac(i).wwccv./c.^2;
  
  ac(i).wzd_psub=ac(i).wcz_psub./c;
  ac(i).wzd_gsub=ac(i).wcz_gsub./c;
  
  if(isfield(ac,'wcd'))
    for k=1:size(ac(i).wcd)
      ac(i).wd{k}=ac(i).wcd{k}./c;
    end
  end
end

% remove all quantities involving c or s

ac=rmfield(ac,{'wcz','wsz','wcc','wss','wcs','wwccv','wwssv', ...
               'wwcsv','wc','wcz_psub','wsz_psub','wcz_gsub','wsz_gsub'});
if isfield(ac,'wcd')
  ac=rmfield(ac,{'wcd','wsd'});
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%
function ac=make_aczero(ac)
% make an all zero ac structure

fnames=fieldnames(ac);
for f=1:length(fnames)
  if iscell(ac.(fnames{f}))
    for k=1:numel(ac.(fnames{f}))
      % doing it this way maintains size/sparseness etc
      ac.(fnames{f}){k}(:)=0;
    end
  else
    ac.(fnames{f})(:)=0;
  end
end

return
