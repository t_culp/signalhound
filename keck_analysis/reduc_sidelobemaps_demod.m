function reduc_sidelobemaps_demod(t1,t2, rxNum)
% function reduc_sidelobemaps(t1,t2,filename)

if ~exist('rxNum','var')
  rxNum=0;
end

% May want to investigate whether parallelizing will be faster
% Demod:
beammap_demod_par(t1,t2,'k1001',[],0,'fisheye', [], rxNum);

