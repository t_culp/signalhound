# GPT 20140118
# The files in this folder contain output of Python code written by Colin.
# Upper limits are used when available.
# When unavailable, the code spits out the result of the offset-lognormal approximation.
# CBI's lowest band edge strangely goes to zero, so I manually truncated to ell~300.
