function reduc_comptodspec_makepager(tags)
% reduc_comptodspec_makepager(tags)
%
% Make pager to go through comptodspec plots
%
% reduc_comptodspec_makepager({'060402'})
% reduc_comptodspec_makepager([get_tags('2006use'),get_tags('2007use')])

for t=1:length(tags) 
  for f=1:7
    for fil={'p0','p3'}
      make_file(t,tags,f,fil);
    end
  end
end
return
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_file(t,tags,f,fil)

fil=fil{1};

fd=fopen(sprintf('comptodspec/%s_fig%1d_%s.html',tags{t},f,fil),'w');
fprintf(fd,'<a href="..">up</a> --- ');

if(1)
l=t-1;
if(l==0)
  l=length(tags);
end
n=t+1;
if(n>length(tags))
  n=1;
end
fprintf(fd,'<a href="%s_fig%1d_%s.html">last</a>, ',tags{l},f,fil);
fprintf(fd,'<a href="%s_fig%1d_%s.html">next</a> --- ',tags{n},f,fil);
end

for i=1:7
  if(f==i)
    fprintf(fd,'fig%1d, ',i);
  else
    fprintf(fd,'<a href="%s_fig%1d_%s.html">fig%1d</a>, ',tags{t},i,fil,i);
  end
end
fprintf(fd,'--- ');

if(strcmp(fil,'p0'))  
  fprintf(fd,'new-p0, ');
  fprintf(fd,'<a href="%s_fig%1d_p3.html">new-p3</a>',tags{t},f);
else
  fprintf(fd,'<a href="%s_fig%1d_p0.html">new-p0</a>, ',tags{t},f);
  fprintf(fd,'new-p3');
end

fprintf(fd,'<p><img src="%s_fig%1d_%s.png">',tags{t},f,fil);

fclose(fd);
  
return
