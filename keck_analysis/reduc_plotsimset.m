function reduc_plotsimset(apssetname,pltype,lmax, pl)
% reduc_plotsimset(apssetname)
%
% Plot an apsset from simulation
%% pltype is a vector specifying which plot types to display
%
% 1 - plot all realizations as ind scatter points
% 2 - plot mean with spread as std errobar
% 3 - plot mean with spread as eom errorbar
% 4 - plot the histogram in 1st/mid/last bins

% pl=1 plots to gif

% e.g. reduc_plotsimset('sim06xxx5_filtp3_weight2_jack0.mat')

if(~exist('pl','var'))
    pl=0;
end
if(~exist('pltype','var'))
  pltype=1;
end

if(~exist('lmax','var'))
  lmax=500;
end

nplots=length(pltype);
pp=1;
  

% get the apsset
load(sprintf('aps/%s',apssetname));

disp(sprintf('%03d realizations in this set', size(aps(1).Cs_l,3)));

apssetname=strrep(apssetname,'_','\_');

% get the model values
inpmod=load_cmbfast([],coaddopt);

% merge aps bins if wanted
%aps=aps_bin_merge(aps,4);

% plot the ind point scatter
if(~isempty(intersect(pltype,1)))
  figure(pp);clf;
  setwinsize(gcf,900,700)
  plot_aps(1,aps(1),inpmod,'150GHz', lmax);
  %plot_aps(2,aps(2),inpmod,'150GHz', lmax);
  %plot_aps(3,aps(3),inpmod,'cross', lmax);
  gtitle('scatter of ind sims')
  printname1=sprintf('aps/%s_scatter_%03d.gif',apssetname,lmax);
  if(pl) printfig(pp,printname1,'pp');end
  pp=pp+1;
 end

% calc mean, std and eom
aps=process_simset(aps);

% plot the mean with spread as errorbars
if(~isempty(intersect(pltype,2)))
  figure(pp);clf;
  setwinsize(gcf,900,700)
  plot_bars(1,aps(1),aps(1).std,inpmod,'150GHz',lmax);
  %plot_bars(2,aps(2),aps(2).std,inpmod,'150GHz',lmax);
  %plot_bars(3,aps(3),aps(3).std,inpmod,'cross',lmax);
  gtitle(sprintf('set %s - errorbar is std of sims',apssetname));
  printname1=sprintf('aps/%s_stderrorbar_%03d.gif',apssetname,lmax);
  if(pl) printfig(pp,printname1,'pp');end
  pp=pp+1;
end

% plot the mean with eom as errorbars
if(~isempty(intersect(pltype,3)))
  figure(pp);clf;
  setwinsize(gcf,900,700)
  plot_bars(1,aps(1),aps(1).eom,inpmod,'150GHz',lmax);
  %plot_bars(2,aps(2),aps(2).eom,inpmod,'150GHz',lmax);
  %plot_bars(3,aps(3),aps(3).eom,inpmod,'cross',lmax);
  gtitle(sprintf('set %s - errorbar is eom of sims',apssetname));
  printname1=sprintf('aps/%s_eomerrorbar_%03d.gif',apssetname,lmax);
  if(pl) printfig(pp,printname1,'pp');end
  pp=pp+1;
end


% plot the histogram in 1st/mid/last bins
if(~isempty(intersect(pltype,4)))
  figure(pp);clf;
  pp=pp+1;
  setwinsize(gcf,1200,1000)
  plot_hist(1,aps(1),inpmod,'150GHz');
  %plot_hist(5,aps(2),inpmod,'150GHz');
  %plot_hist(9,aps(3),inpmod,'cross');
  gtitle('scatter of sample bandpowers')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_bars(n,aps,bar,inpmod,freq,lmax)

lab={'TT (\muK^2)','TE(\muK^2)','EE(\muK^2)','BB(\muK^2)','TB(\muK^2)','EB(\muK^2)'};
for i=1:6
  subplot_grid(6,3,(i-1)*3+n)
  errorbox(aps.l,aps.mean(:,i),bar(:,i));
  hold on; plot(inpmod.l,inpmod.Cs_l(:,i),'r'); hold off;  
  xlim([0,lmax]);
  ylabel(lab{i});
  if(i==1)
    title(freq);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_aps(n,aps,inpmod,freq, lmax)

lab={'TT (\muK^2)','TE(\muK^2)','EE(\muK^2)','BB(\muK^2)','TB(\muK^2)','EB(\muK^2)'};
for i=1:6
  subplot_grid(7,3,(i-1)*3+n);
  plot(aps.l,squeeze(aps.Cs_l(:,i,:)))%,'.','MarkerSize',3);
  hold on; plot(inpmod.l,inpmod.Cs_l(:,i),'r'); hold off;
  xlim([0,lmax]);
  ylabel(lab{i});
  %subplot_grid(7,3,(i-1)*3+n)
  if(i==1)
    title(freq);
  end
end

subplot_grid(7,3,18+n);
plot(aps.l,squeeze(aps.Cs_l(:,4,:)./aps.Cs_l(:,3,:)))%,'.','MarkerSize',3);
axis tight
xlim([0,lmax]);
ylim([0,2]);
ylabel('BB/EE')


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_hist(n,aps,inpmod,freq)

f=2; m=3; l=4;

%f=1;
%l=size(aps.l,1);
%m=round(l/2);
nb=30;

lab={'TT','TE','EE','BB','TB','EB'};
for i=1:6
  subplot(6,11,(i-1)*11+0+n); hfill(aps.Cs_l(f,i,:),nb);
  %hold on; plot(inpmod.Cs_l(f,i),0,'r*'); hold off;
  xlabel(sprintf('%s bin%d',lab{i},f));
  
  subplot(6,11,(i-1)*11+1+n); hfill(aps.Cs_l(m,i,:),nb);
  %hold on; plot(inpmod.Cs_l(f,i),0,'r*'); hold off;
  xlabel(sprintf('%s bin%d',lab{i},m));
  title(freq);
  
  subplot(6,11,(i-1)*11+2+n); hfill(aps.Cs_l(l,i,:),nb);
  %hold on; plot(inpmod.Cs_l(f,i),0,'r*'); hold off;
  xlabel(sprintf('%s bin%d',lab{i},l));
end

return
