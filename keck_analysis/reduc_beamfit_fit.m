function reduc_beamfit_fit(xcorrfiles,outfile)
% reduc_beamfit_fit(xcorrfiles,outfile)
%
% Fit beam positions to the WMAP-correlated CMB map
% data produced by reduc_beamfit_correlate.  The input
% arguments are a list of correlation files (may use
% wildcards), and the file in which to save the output.
%
% The output file contains the variables:
%   - dklist: list of dk angles included
%
% and several sets of beam positions.  Note the [r,th] are
% without the drum angle rotation, i.e. they are as in the
% fp_data CSV file, not as returned by get_array_info.
%   - r0,th0: 'ideal' beam positions from current fp_data file.
%   - r1,th1: beams used in making the maps.
%   - r2,th2: beam positions from pair-sum fits (no A/B
%             diff pointing). Indices are r1(ch,sdir,idk),
%             where ch is the detector index (GCP number+1),
%             sdir is the scan direction (1=right, 2=left,
%             3=mean), and idk is the dk-angle index (the
%             last value is the mean over dk angles).
%   - r3,th3: beam positions from separate A and B fits.
%             Indices are as in r1,th1.
%   - r4,th4: per-pair average beam centers.  These are the
%             means over scan direction, dk angle, and
%             polarization from r3,th3.
%   - r5,th5: 4-parameter fit from pair-sum beams r2,th2
%   - r6,th6: 4-parameter fit from pol-avg beams r4,th4
%   - r7,th7: 4-parameter per-tile fit from pair-sum beams r4,th4
%   - r8,th8: 4-parameter per-tile fit from pol-avg beams r4,th4

clear xc

% Get files and dk angles
[flist,dklist]=select_files(xcorrfiles);

% Get schedule date from first file.
schdate='';
load(flist{1},'beamcorropt');
schdate=beamcorropt.schdate;
if isempty(schdate)
  schdate='20120105';
  disp(['Couldn''t get schedule date from xcorr file.  Using default value ' schdate '.']);
end

% Use beamcorropt to generate output file name if one not provided.
if nargin<2 || isempty(outfile)
  outfile=get_beamfit_filename(beamcorropt);
end

% Get 'ideal' beams.  Note the beams used to make the maps are
% in map_p, map_ind.  These may not be the same!
[ideal_p,ideal_ind] = get_array_info(schdate);

r0=ideal_p.r;
th0=ideal_p.theta;
r1=[];
th1=[];
r2=[];
th2=[];
amp2=[];
r3=[];
th3=[];
amp3=[];

% Loop over dk angles and do the fits
for idk=1:length(dklist)

  % Load the correlation data
  fname=flist{idk};
  ac=load(fname);
  tmp_p=ac.beamcorropt.map_p;
  tmp_p.pol = strrep(tmp_p.pol,'sum','A');
  tmp_p.pol = strrep(tmp_p.pol,'diff','B');
  ac.beamcorropt.map_ind = make_ind(tmp_p);

  % Do we have separate left/right going maps?
  NUM_SDIR=size(ac.xc,4);

  % Beam positions used to make the maps
  r1=ac.beamcorropt.map_p.r;
  th1=ac.beamcorropt.map_p.theta;

  % if this is is the first dk angle, initialize peak amplitude from fit variable
  if idk==1
    amp2=NaN(numel(ideal_ind.e),NUM_SDIR,length(dklist));
    amp3=amp2;
  end
  
  % Loop over scan directions (if present), do the fits
  for sdir=1:NUM_SDIR
    rcos=zeros(size(ideal_ind.e));
    rsin=zeros(size(ideal_ind.e));

    % Beam centers from pair sum
    [tmprcos,tmprsin,tmpamp]=do_the_fit(ac.xc(:,:,:,sdir),ac.m.x_tic,ac.m.y_tic, ...
      dklist(idk),ac.beamcorropt.map_p,ac.beamcorropt.map_ind);

    % A/B offsets from pair diff -- OBSOLETE
    % tmpdrcos=ac.diffcorr(:,sdir,1) ./ ac.diffcorr(:,sdir,3) * 2;
    % tmpdrsin=ac.diffcorr(:,sdir,2) ./ ac.diffcorr(:,sdir,3) * 2;
    tmpdrcos=0;
    tmpdrsin=0;

    % Note that standard pair diffs are defined as (A-B)
    rcos(ideal_ind.a)=tmprcos'+tmpdrcos/2;
    rcos(ideal_ind.b)=tmprcos'-tmpdrcos/2;
    rsin(ideal_ind.a)=tmprsin'+tmpdrsin/2;
    rsin(ideal_ind.b)=tmprsin'-tmpdrsin/2;    

    r2(:,sdir,idk)=sqrt(rcos.^2+rsin.^2);
    th2(:,sdir,idk)=180/pi * atan2(rsin,rcos);

    % Peak amplitude from fit
    amp2(ideal_ind.a,sdir,idk)=tmpamp;
    amp2(ideal_ind.b,sdir,idk)=tmpamp;

    % Fit beams including A/B offsets from separate A and B maps
    if ~isfield(ac,'xca')
      disp(['-- No separate A/B fits in file ' fname ' for dk=' num2str(dklist(idk)) '.']);
    else
      [tmprcos,tmprsin,tmpamp]=do_the_fit(ac.xca(:,:,:,sdir),ac.m.x_tic,ac.m.y_tic, ...
        dklist(idk),ac.beamcorropt.map_p,ac.beamcorropt.map_ind);
      rcos(ideal_ind.a)=tmprcos';
      rsin(ideal_ind.a)=tmprsin';
      amp3(ideal_ind.a,sdir,idk)=tmpamp;
      [tmprcos,tmprsin,tmpamp]=do_the_fit(ac.xcb(:,:,:,sdir),ac.m.x_tic,ac.m.y_tic, ...
        dklist(idk),ac.beamcorropt.map_p,ac.beamcorropt.map_ind);
      rcos(ideal_ind.b)=tmprcos';
      rsin(ideal_ind.b)=tmprsin';
      amp3(ideal_ind.b,sdir,idk)=tmpamp;
 
      r3(:,sdir,idk)=sqrt(rcos.^2+rsin.^2);
      th3(:,sdir,idk)=180/pi * atan2(rsin,rcos);
    end
  end
end

% Take out drum angle rotation
th0=th0+ideal_p.drumangle;
da=ac.beamcorropt.map_p.drumangle;
th1=th1+da;
th2=th2+repmat(da,[1,size(th2,2),size(th2,3)]);
th3=th3+repmat(da,[1,size(th3,2),size(th3,3)]);

% Construct means over scan direction, dk angle
[r2,th2]=add_mean_values(r2,th2);
[r3,th3]=add_mean_values(r3,th3);

% Construct means over polarization
[r4,th4]=mean_beam_center_values(r3(:,end,end),th3(:,end,end),ideal_ind);

% Construct 4-parameter model fitted to pair sums
cc=calc_beamfit_cut(r2,th2,amp2,ideal_p,ideal_ind);
[r5,th5,radio5]=fit_4param(r0,th0,r2(:,end,end),th2(:,end,end),ideal_p,ideal_ind,cc,0);
[r7,th7,radio7]=fit_4param(r0,th0,r2(:,end,end),th2(:,end,end),ideal_p,ideal_ind,cc,1);

% Construct 4-parameter model fitted to means of A and B fits
cc=calc_beamfit_cut(r3,th3,amp3,ideal_p,ideal_ind);
[r6,th6,radio6]=fit_4param(r0,th0,r4,th4,ideal_p,ideal_ind,cc,0);
[r8,th8,radio8]=fit_4param(r0,th0,r4,th4,ideal_p,ideal_ind,cc,1);

% Grab map p,ind
map_p=ac.beamcorropt.map_p;
map_ind=ac.beamcorropt.map_ind;

% And out it goes
save(outfile,'r0','th0','r1','th1','r2','th2','amp2','r3','th3','amp3','r4','th4','r5','th5','r6','th6','r7','th7','r8','th8','dklist','ideal_p','ideal_ind','map_p','map_ind','radio5','radio7','radio6','radio8');

return


% Helper function: just fit the maximum of the correlation
% map.
function [dx,dy,amp] = fit_corr(xc,xtic,ytic)
  for jdx=1:size(xc,3)

    zi = xc(:,:,jdx);

    [tmp i] = max(zi);
    [tmp j] = max(tmp);
    i=i(j);

    cx = abs (xtic - xtic(j)) < 0.5;
    cy = abs (ytic - ytic(i)) < 0.5;

    if 0
      A = normfit2d(xtic(cx),ytic(cy),zi(cy,cx));

      dxmax(jdx) = xtic(j);
      dymax(jdx) = ytic(i);
      dxfit(jdx) = A(2);
      dyfit(jdx) = A(3);
      amp(jdx) = A(1);
    else
      A = polyfitnd({xtic(cx),ytic(cy)}, zi(cy,cx)', 2);

      dxmax(jdx) = xtic(j);
      dymax(jdx) = ytic(i);
      dxfit(jdx) = (A(3)*A(5) - 2*A(2)*A(6)) / (4*A(4)*A(6) - A(5)^2);
      dyfit(jdx) = (A(2)*A(5) - 2*A(3)*A(4)) / (4*A(4)*A(6) - A(5)^2);
      amp(jdx) = A(1) + A(2)*dxfit(jdx) + A(3)*dyfit(jdx) + A(4)*dxfit(jdx)^2 + A(5)*dxfit(jdx)*dyfit(jdx) + A(6)*dyfit(jdx)^2;
    end
  end

  % Take the fitted values rather than the max-pixel values.
  % This gives sub-pixel resolution (probably).
  dx=dxfit;
  dy=dyfit;

  return


% Helper function: Do the fits and reckon corrected
% focal plane r cos th and r sin th values.
function [rcos,rsin,amp]=do_the_fit(xc,xtic,ytic,dk,p,ind)
  [dx,dy,amp]=fit_corr(xc,xtic,ytic);

  % Reckon as if boresight is pointing at center
  % of CMB field.  Hard-coded in.  Lame.
  long0 = 0;
  lat0 = -57.5;

  for i=1:length(ind.a)
    jdx=ind.a(i);
    % Reckon from center of field to center of where this pix
    % observes at this dk angle.
    % Note that dk is in horizontal coordinates, while lat
    % and long are in equatorial coordinates.  Therefore, reckon
    % with theta+90-dk instead of theta-90-dk.
    [lat1 long1]=reckon(lat0,long0,p.r(jdx),p.theta(jdx)+90-dk);
    % Apply fitted offset
    lat2 = lat1 + dy(i);
    long2 = long1 + dx(i);
    [rtmp thtmp] = distance(lat0,long0,lat2,long2);
    corr_r(i) = rtmp;
    corr_th(i) = thtmp + dk-90;
  end

  rcos=corr_r .* cos(corr_th * pi/180);
  rsin=corr_r .* sin(corr_th * pi/180);

  return


% Helper function: add in the scan-direction and dk-averaged
% values.
function [rnew,thnew]=add_mean_values(r,th)

  xx=r.*cos(th*pi/180);
  yy=r.*sin(th*pi/180);

  nsdir=size(xx,2);
  if nsdir>1
    xx(:,nsdir+1,:)=nanmean(xx(:,1:nsdir,:),2);
    yy(:,nsdir+1,:)=nanmean(yy(:,1:nsdir,:),2);
  end

  ndk=size(xx,3);
  if ndk>1
    xx(:,:,ndk+1)=nanmean(xx(:,:,1:ndk),3);
    yy(:,:,ndk+1)=nanmean(yy(:,:,1:ndk),3);
  end

  rnew=sqrt(xx.^2+yy.^2);
  thnew=180/pi * atan2(yy,xx);

  % r(:,nsdir+1,1:ndk+1)=rnew(:,nsdir+1,1:ndk+1);
  % th(:,nsdir+1,1:ndk+1)=thnew(:,nsdir+1,1:ndk+1);
  % r(:,1:nsdir+1,ndk+1)=rnew(:,1:nsdir+1,ndk+1);
  % th(:,1:nsdir+1,ndk+1)=thnew(:,1:nsdir+1,ndk+1);

  rnew(:,1:nsdir,1:ndk)=r;
  thnew(:,1:nsdir,1:ndk)=th;
    
  return


% Helper function: calculate beam center positions,
% averaging A and B
function [r,th]=mean_beam_center_values(r,th,ind)

  % Use dk- and scan-dir-averaged versions
  r=r(:,end,end);
  th=th(:,end,end);

  xx=r.*cos(th*pi/180);
  yy=r.*sin(th*pi/180);

  xxmean=1/2 * (xx(ind.a) + xx(ind.b));
  yymean=1/2 * (yy(ind.a) + yy(ind.b));

  rmean=sqrt(xxmean.^2+yymean.^2);
  thmean=180/pi * atan2(yymean,xxmean);

  r(ind.a)=rmean;
  r(ind.b)=rmean;
  th(ind.a)=thmean;
  th(ind.b)=thmean;

  return


% Helper function: fit 4-parameter model to observed beam positions
% r0,th0 = ideal beam positions
% r1,th1 = observed beam positions
function [rout,thout,radio]=fit_4param(r0,th0,r1,th1,p,ind,cut,bytile)
  % Note: drum angle has already been taken out in correlate stage.
  rcos0=r0.*cos(th0*pi/180);
  rsin0=r0.*sin(th0*pi/180);
  rcos1=r1.*cos(th1*pi/180);
  rsin1=r1.*sin(th1*pi/180);
  rcos2=zeros(size(rcos0));
  rsin2=zeros(size(rsin0));
  
  if bytile
    tiles=unique(p.tile(~isnan(p.tile)))';
  else
    tiles=0;
  end

  for rxnum=unique(p.rx)'
    for tile=tiles;
      if tile~=0;
        ind_tmp=strip_ind(ind,find(p.rx==rxnum & p.tile==tile));
      else
        ind_tmp=strip_ind(ind,find(p.rx==rxnum));
      end
      pixlist=ind_tmp.rgla;
      pixlist=pixlist(cut(pixlist));

      % Fit obs beams as a transformation of ideal beams
      A = fminsearch (@(a) sum((-rsin1(pixlist) - (1+a(1)) * (cos(a(2))*(-rsin0(pixlist)-a(3)) + sin(a(2))*(rcos0(pixlist) -a(4)))).^2 ...
                           + (rcos1(pixlist)  - (1+a(1)) * (cos(a(2))*(rcos0(pixlist) -a(4)) - sin(a(2))*(-rsin0(pixlist)-a(3)))).^2), ...
                    [0 0 0 0]);
      if bytile
        indx=rxnum*4+tile;
      else
        indx=rxnum+1;
      end
      radio(indx).delta_plate = A(1);
      radio(indx).delta_dk = A(2)*180/pi;
      radio(indx).delta_borex = A(3);
      radio(indx).delta_borey = A(4);
      
      tmp = -1 * (1+A(1)) * (cos(A(2))*(-rsin0 - A(3)) + sin(A(2))*( rcos0 - A(4)));
      rsin2(ind_tmp.e) = tmp(ind_tmp.e);
      tmp =      (1+A(1)) * (cos(A(2))*( rcos0 - A(4)) - sin(A(2))*(-rsin0 - A(3)));
      rcos2(ind_tmp.e) = tmp(ind_tmp.e);
    end
  end

  rout=sqrt(rcos2.^2+rsin2.^2);
  thout=180/pi * atan2(rsin2,rcos2);

  return


%% Helper function: identify files to use, extract deck angles,
% check that names differ only by dk; suggest an output file
% name.
function [flist,dk]=select_files(xcorrfiles)

% Identify beamfit correlation files to use -- may have been
% specified in a few different ways.
flist={};
if iscell(xcorrfiles)
  % User gave us an explicit list of files
  flist=xcorrfiles;
elseif exist(xcorrfiles,'dir')
  % User gave us a directory, list plausible files
  d=dir(fullfile(xcorrfiles,'*_dk*_filtp3_weight*_jack24.mat'));
  if isempty(d)
    d=dir(fullfile(xcorrfiles,'beamfit_xcorr_dk*_filtp3_weight*_jack23.mat'));
  end
  for i=1:length(d)
    flist{i}=fullfile(xcorrfiles,d(i).name);
  end
else
  % User gave us a single file or file name with wildcards
  [fdir fname fext]=fileparts(xcorrfiles);
  if isempty(fext)
    fext='.mat';
  end
  d=dir(fullfile(fdir,[fname fext]));
  if isempty(d)
    fdir=fullfile('beamfit',fdir);
    d=dir(fullfile(fdir,[fname fext]));
  end
  for i=1:length(d)
    flist{i}=fullfile(fdir,d(i).name);
  end
end

% Loop over input files, extract dk angle.
dk=[];
for idk=1:length(flist)
  [fdir fname fext]=fileparts(flist{idk});
  tok=regexp(fname,'_dk([0-9]{3})_','tokens');
  if length(tok)>0 && length(tok{1})>0
    dk(idk)=str2num(tok{1}{1});
  else
    dk(idk)=NaN;
  end
end

if length(dk)>length(unique(dk))
  error(['Files passed to reduc_beamfit_fit have duplicate dk angles.']);
end

[dk,i]=unique(dk);
flist=flist(i);
return

