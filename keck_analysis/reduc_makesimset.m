function reduc_makesimset(apsname)
% reduc_makesimset(apsname)
%
% Collect spectra from a set of simulations
% and store in sim0NxxxY file
% Needed only to speed things up
% 
% e.g. reduc_makesimset('sim06[0-9]??4_filtp3_weight2_jack0.mat')
  
  
[s,d]=system_safe(sprintf('ls -1 aps/%s',apsname));
% for some bizarre reason d contains non-printing nonsense...
d=strip_nonsense(d);
apsfiles=strread(d,'%s');

setname=apsfiles{1};
x=strfind(setname, '_');
startnum=setname(x(1)-4:x(1)-2);
setname=strrep(setname,startnum,'xxx');

% make sure the output file is not in the apsfiles
apsfiles=apsfiles(~strcmp(setname,apsfiles));

sprintf('Processing simset %s with %d realizations',setname,length(apsfiles))

% load each aps in turn
for i=1:length(apsfiles)
  if (mod(i,10)==0) display(num2str(i)); end
  load(apsfiles{i});
  for j=1:numel(aps)
    apsset(j).Cs_l(:,:,i)=aps(j).Cs_l;
    apsset(j).l=cvec(aps(j).l);
  end
end
apsset=reshape(apsset,size(aps));

if exist('apsoptc', 'var') && ~exist('apsopt', 'var')
  apsopt=apsoptc;
end

aps=apsset;
saveandtest(setname,'aps','coaddopt','apsopt');

return
