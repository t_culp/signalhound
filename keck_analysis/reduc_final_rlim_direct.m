function [r,rfropt]=reduc_final_rlim_direct(rf,rfropt,pl)
% r=reduc_final_rlim_direct(rf,rfropt,pl)
%
% calculate r 95% conf limit and max likelihood for real/sims from r structure using direct estimation methods
%
% input:
% r structure = loaded from final/XXX.mat file
%
% rfropt structure of options:
%   nspec           = if r is multi spectrum structure use this one
%   rvals           = range of r values at which to compute likelihood
%   chibins         = bandpowers to use (defaults to standard get_chibins)
%   method          = 'rho'   use the rho statistic (default)
%                     'bandpowers' use the individual bandpowers
%   lh_est          = 'fit' (default), the bandpower/rho distribution is fitted 
%                      with the bp_fitfunc
%                   = 'binned' the likelihood is read from the binned histogram
%   bp_fitfunc      = 'chisq' (default) - with which function to fit the bandpower/rho dists
%                   = 'variance_gamma' - appropriate for cross spectra
%                   = 'gauss'
%   fit_est         : the parameters of the bp_fitfunc are determined via;
%                   = 'unbinnedfit' (default) to the distribution
%                   = 'histfit' fit to a histogram of the distribution
%                   = 'parestimate' the parameter are just estimated from the mean
%                      and variance of the distribution
%   rscaling        : how the sims at different levels of r being generated
%                     look at http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20140117_RScaling/
%                   = 'mean_rescale' (default) rescale the S+N sims
%                   = '0751/xxx7_a_filtp3_weight3_gs_dp1100_jack0_0014_a_filtp3_weight3_gs_dp1100_jack0_matrix_overrx.mat'
%                      An aps file that matches the nspec-th spectrum of your final file. 
%                      It contains (S+N type7 auto; B type4 auto; the cross between the two)
%                      This triggers the use of the cross spectral method to scale r.
%   showreal        = show the r-limit for the real data (default=0)
%   debias_lensing  = add lensing B modes to the model (default=1)
%   addsysuncer     = add systematic uncertainty (default=1)
%   sysuncer_type   = 0 systematic uncertainty is only applied to covariance matrix
%                     1 (default) systematic uncertainty is applied as random number to bandpower distributions
%                     2 systematic uncertainty is used to rescale the bp_fitfunc appropriately using analytic expression. See 2014-03-04 posting.
%   sysuncer_rseed  = 0 (default) : to do the systematic uncertainty as random shift in the bandpower distributions a random sequence
%                     needs to be picked and fixed when rerunning the code. This number is added to the random seed
%                     to switch to a different realization. (the fig title is only working up to sysuncer_rseed=9)
%   noffdiag        = number of off diags to use in bpcm (default=1)
%   fiducialr       = rescale the error bars to this value of r - only
%                     valid for auto spectrum!
%   fgmod           = filename or aps of foreground model to debias
%        
% output: r with fields rvals, rlike, slike, rconf95, sconf95 
%	Note - r.expv is modified by this function! 
%
% e.g.
% 
% reduc_final_rlim_direct('0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_matrix_overrx')
% or
% rfropt.rscaling='0751/xxx4_a_filtp3_weight3_gs_dp1100_jack0_xxx5_a_filtp3_weight3_gs_dp1100_jack0_xxx6_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat'
% reduc_final_rlim_direct('0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf_rbc.mat',rfropt)
% will make the bicep2 constraint 
% or
% rfropt.rscaling='0751x1651/xxx4_a_filtp3_weight3_gs_dp1100_jack0_xxx5_a_filtp3_weight3_gs_dp1100_jack0_xxx6_a_filtp3_weight3_gs_dp1100_jack0_xxx4_a_filtp3_weight3_gs_dp1000_jack0_xxx5_a_filtp3_weight3_gs_dp1000_jack0_xxx6_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix.mat'
% rfropt.nspec=[4,5];
% reduc_final_rlim_direct('0751x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix.mat',rfropt)
%


if(~exist('rfropt','var'))
  rfropt=[];
end
if(~exist('pl','var'))
  pl=1;
end
rfropt.lvals=1;
if(~isfield(rfropt,'nspec'))
  rfropt.nspec=1;
end
if(~isfield(rfropt,'method'))
  rfropt.method='rho';
end
if(~isfield(rfropt,'lh_est'))
  rfropt.lh_est='fit';
end
if(~isfield(rfropt,'bp_fitfunc'))
  rfropt.bp_fitfunc='chisq';
end
if(~isfield(rfropt,'fit_est'))
  rfropt.fit_est='unbinnedfit';
end
if(~isfield(rfropt,'chibins'))
  rfropt.chibins=get_chibins;
end
if(~isfield(rfropt,'addsysuncer'))
  rfropt.addsysuncer=1;
end
if(~isfield(rfropt,'sysuncer_type'))
  rfropt.sysuncer_type=1;
end
if(~isfield(rfropt,'sysuncer_rseed'))
  rfropt.sysuncer_rseed=0;
end
if(~isfield(rfropt,'showreal'))
  rfropt.showreal=1;
end
if(~isfield(rfropt,'noffdiag'))
  rfropt.noffdiag=1;
end
if(~isfield(rfropt,'fiducialr'))
  rfropt.fiducialr=0;
end
if(~isfield(rfropt,'debias_lensing'))
  rfropt.debias_lensing=1;
end
% default parameter ranges depend on calc type
if(~isfield(rfropt,'rvals'))
  rfropt.rvals=0:.01:0.75;
end
if(~isfield(rfropt,'r_show'))
  rfropt.r_show=rfropt.rvals(8);
end
if(~isfield(rfropt,'rscaling'))
  rfropt.rscaling='mean_rescale';
end

% if r is string load it
if(ischar(rf))
  load(sprintf('final/%s',rf));
else
  r=rf;
  rf='input r struct';
end

scaling = 'mean_rescale';
if ~strcmp(rfropt.rscaling,scaling)
  scaling='cross';
end
ffile = strrep(rf,'.mat','');
ffile = [ffile,strrep(num2str(rfropt.nspec),' ','')];
ffile = strrep(ffile,'/','_');
fig_title=[ffile,'_',rfropt.method,'_',rfropt.lh_est,'_',rfropt.bp_fitfunc,'_',rfropt.fit_est,...
          '_',scaling,'_',num2str(rfropt.r_show),...
          '_r',num2str(rfropt.fiducialr),...
          '_',num2str(100*rfropt.sysuncer_rseed+10*rfropt.addsysuncer+rfropt.sysuncer_type,'%.3d')];

% keep a copy of the full r structure since we will need the weight
% from the uncombined spectra in the case we combine 100 and 150
r0=r;
% if r multi spec use the desired one...
if length(rfropt.nspec)==1
  r=r(rfropt.nspec);
else
  % ... or handle the bicep1 case where we
  % might want to combine the 100 and 150 spectra.
  % rfropt.nspec=[4,5] will do the b2xb1combined cross spectrum
  r=comb_spec(r(rfropt.nspec(1)),r(rfropt.nspec(2))); % combination of B2xB1 cross spectra - used
end

% if desired debias foreground model
if(isfield(rfropt,'fgmod'))
  if(ischar(rfropt.fgmod))
    load(sprintf('aps/%s',rfropt.fgmod));
    aps=apply_supfac(aps,r);
  else
    aps=rfropt.fgmod;
  end
  r.real(:,4)=r.real(:,4)-aps.Cs_l(:,4);
end

% if blinding substitute first sim for real
if(~rfropt.showreal)
  r.real=r.sim(:,:,1);
end
  
% Get expected r bandpowers from signal only sims and scale to r=1
expv_r1 = r.expv; 
expv_r1(:,4) = mean(r.sig(:,4,:),3)*10;

if(rfropt.debias_lensing)
  % Use mean of signal+noise sims as expected lensing baseline
  expv_lensing = mean(r.sim, 3);
else
  expv_lensing = expv_r1;
  expv_lensing(:) = 0;
end

if(rfropt.fiducialr>0)
  r=set_fiducialr(r,rfropt.fiducialr,rfropt,supfac,opt.finalopt,r0);
end

% get the cov matrix either w/ or w/o sys uncertainty
if(rfropt.addsysuncer)
  r=add_sys_uncer(r,rfropt.noffdiag,[],[],1);
else
  r=get_bpcov(r);
  r=trim_bpcov(r,rfropt.noffdiag);
end

for kk=1:length(rfropt.rvals)
  fid_r=rfropt.rvals(kk);
  cr=set_fiducialr(r,fid_r,rfropt,supfac,opt.finalopt,r0);
  
  if(0)
    % recalculate the covariance matrix depending on the fiducial model.
    % for the rho statistic this does not work: at each r value the
    % bias due to lensing changes break the linear calibration model.
    % To fix this the bias alpha can be calculated and subtracted 
    % for each r individually.
    
    % get the cov matrix either w/ or w/o sys uncertainty
    if(rfropt.addsysuncer)
      cr=add_sys_uncer(cr,rfropt.noffdiag,[],[],1);
    else
      cr=get_bpcov(cr);
      cr=trim_bpcov(cr,rfropt.noffdiag);
    end
  end

  switch rfropt.method
   case 'rho'
    % prepare the weight matrix
    cov=cr.cov{4}(rfropt.chibins{4},rfropt.chibins{4});
    % and the weights. We record the weights 
    % since they are needed to estimate the chisquare distribution
    % later on and might change as a function of r:
    wB(:,kk)=inv(cov)*expv_r1(rfropt.chibins{4},4)/10;
    % apply it to the real data...
    % as long as the bp covariance matrix is not changed
    % as a function of r, this will result in the same
    % value for all r:
    est_real(kk,1) = sum(cr.real(rfropt.chibins{4},4).*wB(:,kk));
    for jj=1:size(cr.sim,3)
      % to the sims of the current level of r...
      est_sim(kk,jj) = sum(cr.sim(rfropt.chibins{4},4,jj).*wB(:,kk));
      % to the sims at the fiducial level of r,
      % these are used to get the simulated 95% limit distribution
      est_sim0(kk,jj)= sum(r.sim(rfropt.chibins{4},4,jj).*wB(:,kk));
    end
    
    % Calculate contributions to rho variance from abscal and beam
    % width uncertainty.
    if rfropt.addsysuncer & (rfropt.sysuncer_type == 2)
      % Expand rho weights from [nbin,1] to [nbin,nsim].
      weight = repmat(wB(:,kk), 1, size(cr.var_abscal.SS, 2));
      % Calculate abscal contribution to rho variance.
      var_abscal(kk) = ...
          mean(sum(cr.var_abscal.SS(rfropt.chibins{4},:) .* ...
                   weight, 1).^2) + ...
          mean(sum(cr.var_abscal.BB(rfropt.chibins{4},:) .* ...
                   weight, 1).^2) + ...
          4 * mean(sum(cr.var_abscal.BS(rfropt.chibins{4},:) .* ...
                       weight, 1).^2) + ...
          2 * mean(sum(cr.var_abscal.SS(rfropt.chibins{4},:) .* ...
                       weight, 1) .* ...
                   sum(cr.var_abscal.BB(rfropt.chibins{4},:) .* weight, 1));
      % Calculate beam width contribution to rho variance.
      var_beam(kk) = ...
          mean(sum(cr.var_beam.SS(rfropt.chibins{4},:) .* ...
                   weight, 1).^2) + ...
          mean(sum(cr.var_beam.BB(rfropt.chibins{4},:) .* ...
                   weight, 1).^2) + ...
          4 * mean(sum(cr.var_beam.BS(rfropt.chibins{4},:) .* ...
                       weight, 1).^2) + ...
          2 * mean(sum(cr.var_beam.SS(rfropt.chibins{4},:) .* ...
                       weight, 1) .* ...
                   sum(cr.var_beam.BB(rfropt.chibins{4},:) .* weight, 1));
    end
   case 'bandpowers'
    % diagonalize the bandpowers; note that this changes the 
    % real bandpowers, the noffdiag was already handeled
    % when bpcov was calculated
    cr = diag_bandpow(cr,-1);
    % fetch r with the sims still at the fiducial r model,
    % and apply the diagonalization as to real data:
    cr0=r;
    cr0.cov=cr.cov;
    cr0=diag_bandpow(cr0,-1);
    % loop over the bandpower bins:
    for ii=1:length(rfropt.chibins{4})
      bin = rfropt.chibins{4}(ii);
      % record bandpowers for sim(r), sim(r_fid) and real:
      est_sim(kk,:,ii) = cr.sim(bin,4,:);
      est_sim0(kk,:,ii)= cr0.sim(bin,4,:);
      est_real(kk,ii)  = cr.real(bin,4);
    end
  end
end

% for rho find calibration values alpha and beta
% we expect a linear realiation ship; 
% (note that this is, however, not valid if in the loop above
%    if(0)
%      cr=get_bpcov(cr);
%    end
% is done. For each of the wb(r) the offset due to
% lensing is different.)
rho_pf=[0,1];
if strcmp(rfropt.method,'rho')
  % linear fit:
  rho_pf = polyfit(rfropt.rvals,mean(est_sim'),1);
  if(pl>0)
    figure()
    setwinsize(gcf,420,260)
    dr = rfropt.rvals(2)-rfropt.rvals(1);
    xr = repmat(rfropt.rvals',1,size(est_sim,2));
    ylims = [floor(min(est_sim(:))),ceil(max(est_sim(:)))];
    [x_tic,y_tic,n]=hfill2(xr(:),est_sim(:),length(rfropt.rvals),rfropt.rvals(1)-0.5*dr,rfropt.rvals(end)+0.5*dr,30,ylims(1),ylims(2));
    imagesc(x_tic,y_tic,n);axis xy;   
    xlabel('input r')
    ylabel('uncalibrated \rho')
    colormap(flipud(colormap))
    hold
    plot(rfropt.rvals,mean(est_sim'),'b.')
    plot(rfropt.rvals,rho_pf(2)+rfropt.rvals*rho_pf(1),'r-')
    title('calibration of rho with fit')
    ylim(ylims)
  end
  % calibrate real:
  est_real = (est_real-rho_pf(2))/rho_pf(1);
  % calibrate the sim distributions:
  est_sim = (est_sim-rho_pf(2))/rho_pf(1);
  est_sim0= (est_sim0-rho_pf(2))/rho_pf(1);    
end

r.likeBB=zeros(length(rfropt.rvals),size(est_sim,3));
r.slikeBB=zeros(length(rfropt.rvals),size(est_sim,2),size(est_sim,3));

% now there is two possibilities, either read the pdf off the histogram, or fit the distribution 
% to get an unbinned estimate of the likelihood...
switch rfropt.lh_est
 % ... start with reading it off the histogram:
 case 'binned'
  % prepare histogram binning
  dr = rfropt.rvals(2)-rfropt.rvals(1);
  % use the extrema of the distribution as range of the histogram:
  ylims = [min(est_sim(:)),max(est_sim(:))];
  % for 200 sims, 30 bin seems ok.
  nbins = 30;
  % these are the r values for each of the sims:
  xr = repmat(rfropt.rvals',1,size(est_sim,2));
  % loop over bins; no effect on rho
  for ii=1:size(est_sim,3)
    cest_sim=est_sim(:,:,ii);
    % histogram the distribution
    [x_tic,y_tic,n]=hfill2(xr(:),cest_sim(:),length(rfropt.rvals),rfropt.rvals(1)-0.5*dr,rfropt.rvals(end)+0.5*dr,nbins,ylims(1),ylims(2));
    % loop through the real values to see into which
    % bin of the distribution along the y-axis they fall,
    % (in principle the real values can change as a function of r)
    for kk=1:length(rfropt.rvals)
      % this is the hist bin into which the real data falls
      eind = find(abs(y_tic-est_real(kk,ii))==min(abs(y_tic-est_real(kk,ii))));
      % so pick the likelihood as value of the hist:
      r.likeBB(kk,ii) = n(eind,kk);
      % do the same for each of the sims:
      for jj=1:size(est_sim,2)
        eind = find(abs(y_tic-est_sim0(kk,jj,ii))==min(abs(y_tic-est_sim0(kk,jj,ii))));
        r.slikeBB(kk,jj,ii) = n(eind,kk);
      end
    end
  end
 
 case 'fit'
  if pl>0 
    cc=1;
    figure(); 
    setwinsize(gcf,420*size(est_sim,3),320*length(rfropt.r_show)); 
  end
  % fitting the bandpowers raises the following points:
  % - the dist of auto spectrum bandpowers should be chi^2 distributed
  % - for the distribution of cross spectrum bandpowers the distribution
  % is variance gamma. 
  % - also try a gaussian:
  % Note: the normalization parameter should be the last in the vector p
  switch rfropt.bp_fitfunc
   case 'gauss'
    % the normal gaussian with a scaling parameter p(3):
    fitfun = @(p,x) normpdf(x,p(:,1),p(:,2)).*p(:,3);
   case 'chisq'
    % the chisquare fit function based on the chisquare pdf:
    % p(1) = k;
    % p(2) = sigma; a scaling of the function; this is the sigma of the
    %        underlying normal distribution
    % p(3) = shift along the x-axis to account for debiasing
    % p(4) = normalization in the case of fitting to a histogram
    %        is number of entries divided by the bin density
    fitfun = @(p,x) chi2pdf((x-p(:,3))./p(:,2).^2,p(:,1))./p(:,2).^2.*p(:,4);
    % if systematic uncertainties are included the distribution is not
    % chisquare anymore, eventually extending the undebiased distributions
    % to negative values. When doing the unbinned likelihood fit these cases
    % need to be caught. This is done by setting values of the pdf x<0 to
    % nan instead of 0. The fit then avoid taking these points into account.
    if strcmp(rfropt.fit_est,'unbinnedfit')
      fitfun = @(p,x) chi2pdfnan((x-p(:,3))./p(:,2).^2,p(:,1))./p(:,2).^2.*p(:,4);
    end
   case 'variance_gamma'
    % the variance-gamma fit function based on the pdf:
    % p(1) = k; number of degrees of freedom
    % p(2) = sigma; geometric mean of the sigmas of the
    %        underlying normal distributions
    % p(3) = correlation coefficient of the underlying normal distributions
    %        actually taken to be tan(corrcoef*pi/2)) to fool the
    %        minimization algorithm
    % p(4) = shift along the x-axis to account for debiasing
    % p(5) = normalization in the case of fitting to a histogram
    %        is number of entries divided by the bin density
    fitfun = @(p,x) sumprodxynan(x-p(:,4),p(:,2),atan(p(:,3))*2/pi,p(:,1)).*p(:,5);
  end  
  % - In all cases we have debiased the spectra. In the case of the chi^2
  % distribution, this mean we either have to undo the debias step, or
  % allow for an addtional shift parameter to the fit function.
  % This also applies to the rho distribution which has been calibrate 
  % (which includes a shift)
  % - the fit to the distribution is preferably an unbinned fit. However,
  % if we need to find the goodness of fit, using a fit to a histogram
  % is more straight forward to use - also because the properness of the
  % fit can be visualized.
  
  vg_usedfitfun = zeros(size(est_sim,3),length(rfropt.rvals));
  pf=[];
  % loop over bins; no effect on rho
  for ii=1:size(est_sim,3)
    cest_sim=est_sim(:,:,ii);
    bin = rfropt.chibins{4}(ii);
    % loop over the tested values of r,
    % do it backwards which is helpfull when we fit variance gamma:
    % the estimate of the intials for var gam are easier at high r = high correlation
    % when decreasing r we can generate a guess from the previous fit.
    for kk=length(rfropt.rvals):-1:1
      % the initial parameter estimates...:
      switch rfropt.bp_fitfunc
       % ...for gaussian fit simple:
       case 'gauss'
        pc0=[mean(cest_sim(kk,:)),std(cest_sim(kk,:)),1];
        pcfree=[1,1,0];

       case 'chisq'
        % ...for chisq fit more involved since the bandpowers
        % and the rho statistic differ quite a bit:
        switch rfropt.method
         case 'bandpowers'
          % for the bandpowers it is simple...:
          % ...the debiased portion (r.db) must be taken into account by
          % adding it to the mean of the dist:
          se = sqrt( var(cest_sim(kk,:)) / (mean(cest_sim(kk,:))+r.db(bin,4) )/2 );
          ke = var(cest_sim(kk,:))/2/se^4;
          % the fit function is now shifted back (3rd parameter) by the  amount
          % of the debias.
          pc0=[ke,se,-r.db(bin,4),1];
          pcfree=[1,1,0,0];
        
         case 'rho'
          % for the rho distribution the finding the chisq parameters is more
          % involved since the bandpowers were debiased, combined and then scaled
          % and shifted (calibrated) by the polyfit.
          % The distribution in cest_sim is at this point:
          % rho = (rho_hat - alpha)/beta
          % with rho_hat = sum(w_i*(b_i-db_i)) = sum(w_i*b_i) - sum(w_i*db_i)
          % the parameters of the chisq distribution are estimated from sum(w_i*b_i) =(def)=rho'
          % for the mean and variances follows:
          % var(rho') = var(rho)*beta^2
          % <rho'>    = <rho'>*beta + alpha + sum(w_i*db_i)
          db = rho_pf(2)+sum(wB(:,kk).*r.db(rfropt.chibins{4},4));
          v = var(cest_sim(kk,:))*rho_pf(1)^2;
          if (rfropt.sysuncer_type == 2)
            % Add abscal/beam contributions to variance.
            % These variance contributions are in unscaled rho
            % units already, so no need to multiply by rho_pf(1)^2.
            v = v + var_abscal(kk) + var_beam(kk);
          end
          m = mean(cest_sim(kk,:))*rho_pf(1)+db;
          se = sqrt(v/2/m);
          ke = v/2/se^4;
          % the fit function is now shifted by the debias scaled with beta (4th parameter)
          % beta is also absorbed in the scaling (2nd parameter) of the fit function:
          pc0=[ke,se/sqrt(rho_pf(1)),-db/rho_pf(1),1];
          pcfree=[1,1,0,0];
          
        end % rfropt.method
       case 'variance_gamma'
       % estimating intial parameters for variance gamma is more involved:
        switch rfropt.method
         case 'bandpowers'
          db = r.db(bin,4);
          m = mean(cest_sim(kk,:));
          s = std(cest_sim(kk,:));
          pc0=est_vg_initials(cest_sim(kk,:),m,s);
          if isnan(pc0(3))
            % nan indicates the gaussian fall back
            % disp('norm')
            vg_usedfitfun(ii,kk)=2;
            pcfree=[1,1,0,0,0];        
          else
            % disp('vg')
            vg_usedfitfun(ii,kk)=1;
            pcfree=[1,1,1,0,0];
            pc0(4)=-db; % take the shift into account
          end
          
         case 'rho'
          % also look at case 'rho' for chisquare distribution above,
          % to see where this calculation comes from...
          db = rho_pf(2)+sum(wB(:,kk).*r.db(rfropt.chibins{4},4));
          % as if no calibration was done:
          s = std(cest_sim(kk,:))*rho_pf(1);
          m = mean(cest_sim(kk,:))*rho_pf(1)+db;
          pc0=est_vg_initials(cest_sim(kk,:),m,s);
          if isnan(pc0(3))
            % nan indicates the gaussian fall back
            % disp('norm')
            vg_usedfitfun(ii,kk)=2;
            pcfree=[1,1,0,0,0];            
          else
            % disp('vg')
            vg_usedfitfun(ii,kk)=1;
            pcfree=[1,1,1,0,0];
            pc0(2)=pc0(2)/sqrt(rho_pf(1)); % modify the sigma parameter according to rho calibration
            pc0(4)=-db/rho_pf(1); % take the shift into account
          end
        end % rfropt.method
%         
%         % don't trust in the leptokurtic case which sets the
%         % correlation to 0 and co<0.2. Also try avoiding the normal
%         % distribution when doing a real fit.
%         if pc0(3)<0.2 | isnan(pc0(3))
%           if strcmp(rfropt.fit_est,'parestimate')
%             % just go back to gaussian, since the estimate was bogus
%             pc0=[mean(cest_sim(kk,:)),std(cest_sim(kk,:)),nan,0,1]; 
%             vg_usedfitfun(ii,kk)=2;
%           else
%             % make a guess to start the fitting process, use the one of
%             % the previous r, note that we are running backwards through
%             % the list of r. This assumes that we had a valid estimate for
%             % the first (highest) r
%             pc0=pf(kk+1,:,ii)
%           end
%         end

      end % switch rfropt.bp_fitfunc
      
      % if we perform a fit to the histogrammed distribution
      % set up the binning, 30 bins within +-5 sigma seems good
      % for 200 sims. We create the histogram here by default,
      % since it will be used in the case of plotting
      nbins = 30;
      nsig = 5;
      xlo = mean(cest_sim(kk,:))-nsig*std(cest_sim(kk,:));
      xhi = mean(cest_sim(kk,:))+nsig*std(cest_sim(kk,:));
      [x,n]=hfill(cest_sim(kk,:),nbins,xlo,xhi);
      % for the chisquare to match entries in a histogram
      % apply a norm: number of entries/bin density:
      bd = length(x)/(xhi-xlo);
      pc0(end)=length(cest_sim(kk,:))/bd;

      % finally do the fit
      switch rfropt.fit_est
       case 'histfit'
        % fit to the histogram
        freepar.free=pcfree;
        freepar.lb=pc0*0.8;
        freepar.ub=pc0*1.2;
        try
          [pc,pce]=matmin('logl',pc0,freepar,fitfun,n,x);
        catch
          pc=pc0;
          continue
        end
       case 'unbinnedfit'
        % alternatively we can perform a direct likelihood fit,
        % searching for the minimum in the negative log likehood
        % of the distribution given a model.
        % Set up the function to minimize. This the sum over the
        % log of the prob for each of the values in the dist. The
        % parameters that are fixed need to be in the end:
        fitnll = @(p) -(nansum(log( fitfun([p,pc0(~pcfree)],cest_sim(kk,:)) )));
        % search for the minimum in the free parameters:
        [pc,fval,exitflag,output] = fminsearch(fitnll,pc0(pcfree==1),optimset('MaxIter',1000));
        % assemble the result:
        pc=[pc,pc0(~pcfree)];
       case 'parestimate'
        % or just take the intial parameter estimate:
        pc=pc0;
      end
      if(pl>0)
        if ~isempty(find(rfropt.r_show==rfropt.rvals(kk)))
          subplot(length(rfropt.r_show),size(est_sim,3),cc)
%            clf
%            subplot(1,1,1)
          cc=cc+1;
          hplot(x,n)
          xlim([xlo,xhi])
          hold
          xx=linspace(xlo,xhi,1000);
          plot(xx,fitfun(pc,xx),'r')
          plot(xx,fitfun(pc0,xx),'r--')
          xlabel('estimator distribution');
          ylabel('entries');
          if size(est_sim,3)>1
            title(['sim dist in bin ',num2str(bin),' at r=',num2str(rfropt.rvals(kk))])
          else
            title(['rho at r=',num2str(rfropt.rvals(kk))])
          end
        end
      end
      
      % save the fit variables:
      if isempty(pf)
        pf = zeros(length(rfropt.rvals),size(pc,2),size(est_sim,3));
      end
      pf(kk,1:length(pc),ii)=pc;
    end % kk
  end % ii
  if(pl>1) mkpng(['rlim_direct/fp_',fig_title,'.png']); end
  % the last paramter scales the fit func to match the entries
  % in the histogram. Set it to 1:
  pf(:,end,:)=1;
  
  % now loop over the fit parameters and record the likehoods at the position
  % of the real data:
  for ii=1:size(est_sim,3)
    % record pdf at the position of the real data:
    r.likeBB(:,ii) = fitfun(pf(:,:,ii),est_real(:,ii));
    % do the same for each of the sims:
    for jj=1:size(est_sim,2)
      r.slikeBB(:,jj,ii) = fitfun(pf(:,:,ii),est_sim0(:,jj,ii));
    end
  end

end % switch rfropt.lh_est

% plot of the raw distributions
if(pl>0)
  dr = rfropt.rvals(2)-rfropt.rvals(1);
  xr = repmat(rfropt.rvals',1,size(est_sim,2));
  
  figure()
  setwinsize(gcf,420*size(est_sim,3),320)
  ylims = [min(est_sim(:)),max(est_sim(:))];
  for ii=1:size(est_sim,3)
    subplot(1,size(est_sim,3),ii)
    cest_sim=est_sim(:,:,ii);
    [x_tic,y_tic,n]=hfill2(xr(:),cest_sim(:),length(rfropt.rvals),rfropt.rvals(1)-0.5*dr,rfropt.rvals(end)+0.5*dr,30,ylims(1),ylims(2));
    imagesc(x_tic,y_tic,n);axis xy;
    xlabel('input r')
    ylabel('estimator')
    colormap(flipud(colormap))
    hold
    plot(rfropt.rvals,est_real(:,ii),'r-')
    text(rfropt.rvals(end),est_real(end,ii),'obs','HorizontalAlignment','right','VerticalAlignment','bottom','color','r')
    title('binned distribution')
  end
  if(pl>1)
    mkpng(['rlim_direct/bd_',fig_title,'.png']);
  end
  
  if(exist('pf','var'))
    figure()
    setwinsize(gcf,420*size(est_sim,3),320)
    bin = rfropt.chibins{4}(ii);
    for ii=1:size(est_sim,3)
      subplot(1,size(est_sim,3),ii)
      y_tic =  linspace(ylims(1),ylims(2),1000);
      n=zeros(length(y_tic),length(rfropt.rvals));
      for kk=1:length(rfropt.rvals)
        n(:,kk) = fitfun(pf(kk,:,ii),y_tic);
      end
      imagesc(rfropt.rvals,y_tic,n);axis xy;
      xlabel('input r')
      ylabel('estimator')
      colormap(flipud(colormap))
      hold
      plot(rfropt.rvals,est_real(:,ii),'r-')
      text(rfropt.rvals(end),est_real(end,ii),'obs','HorizontalAlignment','right','VerticalAlignment','bottom','color','r')
      if size(est_sim,3)>1
        title(['fitted distribution since bin ',num2str(bin)])
      else
        title('fitted rho distribution')
      end
      if(1) % indicate which version of the variance gamma has been used:
        plot(rfropt.rvals(vg_usedfitfun(ii,:)==1),est_real(vg_usedfitfun(ii,:)==1,ii),'rx')
        plot(rfropt.rvals(vg_usedfitfun(ii,:)==2),est_real(vg_usedfitfun(ii,:)==2,ii),'bo')
      end
    end  
    if(pl>1)
      mkpng(['rlim_direct/fd_',fig_title,'.png']);
    end
  end
end

% normalize the likelihoods
r.likeBB  =r.likeBB ./repmat(sum(r.likeBB),size(r.likeBB,1),1);
r.slikeBB =r.slikeBB./repmat(sum(r.slikeBB),size(r.slikeBB,1),1);
if(pl>0)
  figure()
  setwinsize(gcf,420*size(est_sim,3),320)
  for ii=1:size(est_sim,3)
    subplot(1,size(est_sim,3),ii)
    plot(rfropt.rvals,r.likeBB(:,ii),'k-')
    xlabel('r')
    ylabel('likelihood')
    ylim([0,max(r.likeBB(:))*1.1])
    if size(est_sim,3)>1
      title(['from bandpower, science bin ',num2str(ii)])
    else
      title('from rho')
    end
  end
  if(pl>1)
    mkpng(['rlim_direct/li_',fig_title,'.png']);
  end
end
    

% the final pdf is the product over bandpower bins
% no effect if rho was used:
r.likeBB=prod(r.likeBB,2);
r.slikeBB=prod(r.slikeBB,3);

% redonormalization to cope with the multiplication step above:
r.likeBB  =r.likeBB ./repmat(sum(r.likeBB),size(r.likeBB,1),1);
r.slikeBB =r.slikeBB./repmat(sum(r.slikeBB),size(r.slikeBB,1),1);

% copy into output structure
r.expv_r1=expv_r1;
r.expv_lensing=expv_lensing;
r0.expv_r1=expv_r1;
r0.expv_lensing=expv_lensing;
r.rvals=rfropt.rvals;

r=calc_stats_1d(r,rfropt.chibins{4});
r.lmaxlike=1;

% calculate the chisquare for the best fit model:
% first adjust the sims to reflect the best fit...
r=set_fiducialr(r,r.rmaxlike,rfropt,supfac,opt.finalopt,r0);
% ... then do calc_chi with the last option set to one which
% uses the mean of the sims as model:
r=calc_chi(r,rfropt.chibins,[],1)

% make plots of likelihood functions, and hist of max like and 95%CL
if pl>0
  figure()
  make_plots_1d(r,r0,rfropt);
  if(pl>1)
    mkpng(['rlim_direct/fi_',fig_title,'.png']);
  end
  gtitle(sprintf('%s spectrum %d using method %s',strrep(rf,'_','\_'),rfropt.nspec,rfropt.method),0.982);
end



return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function r=calc_stats_1d(r,ellbins)
% integrate positive portion of likelihood function up to 95% for CL
% find +/- sigma contours
% find max likelihood

% integrate positive portion of likelihood function
indx=find(r.rvals>=0);
rr=r.rvals(indx);

% integrate to 95% for upper limit
% select distinct sum values for interp1, ugh.
% real data:
rnorm=cumsum(r.likeBB(indx))./sum(r.likeBB(indx));
[dum un]=unique(rnorm);
r.rconf95=interp1(rnorm(un),rr(un),0.95);
% sims:
for ii=1:size(r.slikeBB,2)
  snorm=(cumsum(r.slikeBB(indx,ii))./sum(r.slikeBB(indx,ii)));
  [dum un]=unique(snorm);
  if~isnan(sum(r.slikeBB(indx,ii)))
   r.sconf95(ii)=interp1(snorm(un),rr(un),0.95);
  else
   r.sconf95(ii)=NaN;
  end
end

% find max likelihood
% real data
[r.maxlike rindx]=max(r.likeBB);
r.rmaxlike=r.rvals(rindx);
% convert real max like to chi2 and then pte
r.bfchi2=-2*log(r.maxlike);
r.bfchi2pte=1-chi2cdf(r.bfchi2,length(ellbins)-1);
% sims

for ii=1:size(r.slikeBB,2)
  [dum indx]=max(r.slikeBB(:,ii));
  r.smaxlike(ii)=r.rvals(indx);
end

[r.rcont68,r.rlo68,r.rhi68]=find_68pnts(r.likeBB,r.rvals,'numeric');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_plots_1d(r,r0,rfropt)

%if(ishandle(1))
%  set(0,'CurrentFigure',1);
%else
%  figure(1);
%end
setwinsize(gcf,1100,500)

clf
% plot the raw information
subplot(2,2,1)
plot_raw(r0,rfropt);

% plot the fit
subplot(2,2,2)
plot_fit(r,rfropt);

% make plots like in Chiang et al fig 14
% show the real max likelihood and 95%CL
% plot the max likelihood and 95%CL for the sims and show the median

%  binsml=-.5:.05:1;
%  binscl=0:.02:1;
binsml=-.525/4:.025/2:1.025/4;
binscl=0:.01:0.51;
fs=9;

legpos='NorthEast';

% plot likelihood function of real data
subplot(2,3,4)
box on; hold on
xticsiz=.25;
rlike=abs(r.likeBB./sum(r.likeBB));
plot(r.rvals,rlike,'k'); 
[a ind]=min(abs(r.rvals-r.rmaxlike));
plot([r.rmaxlike r.rmaxlike],[0 rlike(ind)],'b');
text(r.rmaxlike*1.05,rlike(ind)*1.01,'ML','color','b');
[a ind]=min(abs(r.rvals-r.rconf95));
plot([r.rconf95 r.rconf95],[0 rlike(ind)],'r');
text(r.rconf95*1.05,rlike(ind)*1.01,'95%CL','color','r');
plot([r.rlo68 r.rlo68],[0 r.rcont68],'b--');
plot([r.rhi68 r.rhi68],[0 r.rcont68],'b--');
xlabel('r'); ylabel('likelihood');
if(r.rconf95<1d-4)
  ll=legend(['data'],['r = ' sprintf('%1.3e',r.rmaxlike) ' +' ...
	sprintf('%1.3e',r.rhi68-r.rmaxlike) ' ' sprintf('%1.3e',r.rlo68-r.rmaxlike)],['r < ' sprintf('%1.3e',r.rconf95)]) 
else
  ll=legend(['data'],['r = ' sprintf('%1.3f',r.rmaxlike) ' +' ...
	sprintf('%1.3f',r.rhi68-r.rmaxlike) ' ' sprintf('%1.3f',r.rlo68-r.rmaxlike)],['r < ' sprintf('%1.3f',r.rconf95)]);
end	      
set(ll,'box','off','location',legpos,'fontsize',fs)
[a ind]=min(abs(r.rvals-r.rmaxlike));
lm=rlike(ind);
[a ind]=min(abs(r.rvals-0));
lz=rlike(ind);
title(sprintf('L(zero)/L(max)=%.1e, maxlike=%.3g -> \\chi^2=%.1f -> PTE=%.3g',lz/lm,r.maxlike,r.bfchi2,r.bfchi2pte));
set(gca,'YTick',[],'XTick',-10:xticsiz:10)
xlim([r.rvals(1) r.rvals(end)])
ylim(get(gca,'YLim').*1.10)

% histogram of max likelihood
subplot(2,3,5);
box on; hold on
n=histc([r.smaxlike],binsml);
medsim=median(r.smaxlike);
stairs(binsml,n,'color','k')
ylims=get(gca,'YLim');
xlim([min(binsml) max(binsml)])
set(gca,'YLim',[0 ylims(2)+1]); %,'YTick',0:100);
xlabel('maximum likelihood r'); ylabel('counts');
if(isfield(r,'likeBB'))
  plot([medsim medsim],[0 ylims(2)+1],'k--');
  plot([r.rmaxlike r.rmaxlike],[0 ylims(2)+1],'b');
  ll=legend(sprintf('%i sig+noi sims',size(r.sim,3)),...
    sprintf('med sims: %1.3f',medsim),sprintf('data: %1.3f',r.rmaxlike));
else
  plot([medsim medsim],[0 ylims(2)+1],'b-');
  ll=legend(sprintf('%i sig+noi sims',size(r.sim,3)),sprintf('med sims: %1.3f',medsim));
end
set(ll,'box','off','location',legpos,'fontsize',fs)

% histogram of 95%CL
subplot(2,3,6);
box on; hold on
n=histc([r.sconf95],binscl);
medsim=median(r.sconf95);
stairs(binscl,n,'color','k')
ylims=get(gca,'YLim');
xlim([min(binscl) max(binscl)])
set(gca,'YLim',[0 ylims(2)+1]); %,'YTick',0:100);
xlabel('r upper limit, 95% confidence');ylabel('counts');
if(isfield(r,'likeBB'))
  plot([medsim medsim],[0 ylims(2)+1],'k--');
  plot([r.rconf95 r.rconf95],[0 ylims(2)+1],'r');
  ll=legend(sprintf('%i sig+noi sims',size(r.sim,3)),...
    sprintf('med sims: %1.3f',medsim),sprintf('data: %1.3f',r.rconf95));
else
  plot([medsim medsim],[0 ylims(2)+1],'r');
  ll=legend(sprintf('%i sig+noi sims',size(r.sim,3)),sprintf('med sims: %1.3f',medsim));
end
set(ll,'box','off','location',legpos,'fontsize',fs)

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_raw(r,rfropt)
% plot s+n sims in light gray
plot(r.l,squeeze(r.sim(:,4,:)),'color',[0.7,0.7,0.7]);
ellh=r.l(max(rfropt.chibins{4})+3)+10;
xlim([0,ellh]); ylim([-0.01,0.07])
hold on;
% plot their spread as errorbars
eb=std(r.sim(:,4,:),[],3);
errorbar2(r.l,mean(r.sim(:,4,:),3),eb,eb,'b');
% plot templates
plot(r.l,r.expv_r1(:,4)/10,'r--');
plot(r.l,r.expv_lensing(:,4),'r--');
% plot debias
plot(r.l,r.db(:,4),'m');
% plot real points
plot(r.l,r.real(:,4),'b.');
lb=rfropt.chibins{4};
plot(r.l(lb),r.real(lb,4),'ro','MarkerFaceColor','r','MarkerSize',4);
% plot x-axis
line(xlim,[0,0],'Color','k','LineStyle',':');
title('raw information');
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_fit(r,rfropt)
% plot s+n sims in light gray
% plot the real points with used errorbar
eb=std(r.sim(:,4,:),[],3);
errorbar2(r.l,mean(r.real(:,4,:),3),eb,eb,'b.');
ellh=r.l(max(rfropt.chibins{4})+3)+10;
xlim([0,ellh]); ylim([-0.01,0.07])
hold on;
lb=rfropt.chibins{4};
plot(r.l(lb),r.real(lb,4),'ro','MarkerFaceColor','r','MarkerSize',4);
% plot the scaled r-template
plot(r.l,r.expv_r1(:,4)*r.rmaxlike,'r--');
% plot the lensing template
plot(r.l,r.expv_lensing(:,4),'r--');
% plot the sum (total fit)
plot(r.l,r.expv_r1(:,4)*r.rmaxlike+r.expv_lensing(:,4)*r.lmaxlike,'r');
% plot x-axis
line(xlim,[0,0],'Color','k','LineStyle',':');
ft = 'fit: error bars contain best fit r sample variance ';
ft = [ft,sprintf('\\chi^2=%.2f',r.rchisq(4))];
title(ft)

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function r=set_fiducialr(r,fiducialr,rfropt,supfac,finalopt,r0)
% r=set_fiducialr(r,fiducialr,rfropt,supfac)

switch rfropt.rscaling
 case 'mean_rescale'
  % add r bandpowers to signal+noise sims - increase the
  % "errorbars" to reflect presence of real signal
  % take the s+n sims and add back in the debias
  db=repmat(r.db(:,4),[1,1,size(r.sim,3)]);
  sn=r.sim(:,4,:)+db;
  % find the means of the s+n and r=0.1 sets
  msn=mean(sn,3);
  mr=mean(r(1).sig(:,4,:),3);
  % scale the s+n to the new desired mean
  sn=sn.*repmat((msn+mr*10*fiducialr)./msn,[1,1,size(r(1).sim,3)]);
  % subtract out the debias again and store back in r struct
  sn=sn-db;
  r.sim(:,4,:)=sn;
  
 otherwise
  % this is the cross spectral method
  % rfropt.rscaling is an aps filename
  % avoid reloading the file:
  persistent sim_xs;
  if (size(sim_xs,1)==0)
    disp(['load aps/',rfropt.rscaling])
    sim_xs = load(['aps/',rfropt.rscaling]);
  end
  % that's the r level of sim type 4:
  r_0 = 0.1;
  for ii=1:length(rfropt.nspec)
    switch size(sim_xs.aps,1)
     case 3 % S+N x B (old format)
      % prepare for scaling:
      SN_b = squeeze(sim_xs.aps(1).Cs_l(:,4,:));
      B_b  = squeeze(sim_xs.aps(2).Cs_l(:,4,:));
      X_b  = squeeze(sim_xs.aps(3).Cs_l(:,4,:));
      % apply the cross spectrum:
      tot_b = SN_b + B_b*fiducialr/r_0 + 2*X_b*sqrt(fiducialr/r_0);

      % Can't form analytic estimate of beam error in this case.
      if rfropt.sysuncer_type == 2
        disp(['WARNING: Need separate noise, signal, and B-no-E ' ...
              'spectra for rfropt.sysuncer_type == 2']);
      end
     case 6 % S x N x B
      % prepare for scaling:
      BB = squeeze(sim_xs.aps(1).Cs_l(:,4,:))*fiducialr/r_0;
      SS = squeeze(sim_xs.aps(2).Cs_l(:,4,:));
      NN = squeeze(sim_xs.aps(3).Cs_l(:,4,:));
      BS = squeeze(sim_xs.aps(4).Cs_l(:,4,:))*sqrt(fiducialr/r_0);
      BN = squeeze(sim_xs.aps(5).Cs_l(:,4,:))*sqrt(fiducialr/r_0);
      SN = squeeze(sim_xs.aps(6).Cs_l(:,4,:));
      % apply the cross spectrum:
      g1 = 0;
      g2 = 0;
      if isfield(r,'abscal_uncer') & (rfropt.sysuncer_type==1)
        % fix the random state to get the same realization
        % of systematic uncertainty when scaling r
        randn('state',2281248+rfropt.sysuncer_rseed);
        g1 = normrnd(0,1,1,size(BB,2)*2);
        g1 = r.abscal_uncer(:,4)*g1;
      end
      if isfield(r,'beam_uncer') & (rfropt.sysuncer_type==1)
        % fix the random state to get the same realization
        % of systematic uncertainty when scaling r
        randn('state',2281249+rfropt.sysuncer_rseed);
        g2 = normrnd(0,1,1,size(BB,2)*2);
        g2 = r.beam_uncer(:,4)*g2;
      end
      % add the two random distributions:
      g = g1 + g2;
      if length(g)>1
        % we have over produced random numbers above to be able
        % to select now the ones that yield a real number for 
        % sqrt(1+g). Check which realizations are too extreme:
        cut = sum(g<-1);
        % remove the bad ones:
        g=g(:,~cut);
        % and trim to what is actually needed
        g = g(:,1:size(BB,2));
      end
      % full bandpowers:
      tot_b = ...
        (1+g).*(SS + BB + 2*BS)+...
        2*sqrt(1+g).*(SN + BN)+...
        NN;

      % Cross-spectra used for contribution to rho variance from
      % abscal and beam width systematic.
      if (rfropt.sysuncer_type == 2)
        r.var_abscal.SS = SS .* repmat(r.abscal_uncer(:,4), 1, size(SS, 2));
        r.var_abscal.BB = BB .* repmat(r.abscal_uncer(:,4), 1, size(BB, 2));
        r.var_abscal.BS = BS .* repmat(r.abscal_uncer(:,4), 1, size(BS, 2));
      end
      if (rfropt.sysuncer_type == 2)
        r.var_beam.SS = SS .* repmat(r.beam_uncer_weight, 1, size(SS, 2));
        r.var_beam.BB = BB .* repmat(r.beam_uncer_weight, 1, size(BB, 2));
        r.var_beam.BS = BS .* repmat(r.beam_uncer_weight, 1, size(BS, 2));
      end

     case 45 % this is b2xb1, for now don't code it up more general
      % since the combination of b1_100 and b1_150 is done with the spectra
      % instead of in map space.
      switch rfropt.nspec(ii)
       case 4
        mask = [21,29,12,23,27,19,14,25,16];   
       case 5
        mask = [22,30,13,24,28,20,15,26,17];
      end
      % prepare for scaling:
      S1S2 = sim_xs.aps(mask(1)).Cs_l(:,4,:);
      N1N2 = sim_xs.aps(mask(2)).Cs_l(:,4,:);
      B1B2 = sim_xs.aps(mask(3)).Cs_l(:,4,:)*fiducialr/r_0;
      S1N2 = sim_xs.aps(mask(4)).Cs_l(:,4,:);
      N1S2 = sim_xs.aps(mask(5)).Cs_l(:,4,:);
      S1B2 = sim_xs.aps(mask(6)).Cs_l(:,4,:)*sqrt(fiducialr/r_0);
      B1S2 = sim_xs.aps(mask(7)).Cs_l(:,4,:)*sqrt(fiducialr/r_0);
      N1B2 = sim_xs.aps(mask(8)).Cs_l(:,4,:)*sqrt(fiducialr/r_0);
      B1N2 = sim_xs.aps(mask(9)).Cs_l(:,4,:)*sqrt(fiducialr/r_0);
      tot_b = S1S2+N1N2+B1B2+S1N2+N1S2+S1B2+B1S2+N1B2+B1N2;
      tot_b = squeeze(tot_b);

    end % switch size(sim_xs.aps,1)

    % the residual beam correction is done before the supression factor
    if isfield(finalopt,'residbeamcorr')
      persistent rbc;
      if (size(rbc,1)==0)
        rbc = load(finalopt.residbeamcorr);
      end
      tot_b = tot_b+squeeze(rbc.aps(rfropt.nspec(ii)).Cs_l(:,4,1:size(tot_b,2)));
    end
    
    % prepare the debias:
    db=repmat(r0(rfropt.nspec(ii)).db(:,4),[1,size(tot_b,2)]);
    % prepare suppression factor correction:
    supfacB = repmat(supfac(rfropt.nspec(ii)).rwf(:,4),1,size(tot_b,2));
    % apply it; the debias has the suppression factor applied
    tot_b = tot_b.*supfacB - db;
    % cut down if the sizes are unequal
    n_sim=min(size(tot_b,2),size(r.sim,3));
    rs(ii) = r0(rfropt.nspec(ii));
    rs(ii).sim = rs(ii).sim(:,:,1:n_sim);
    rs(ii).sim(:,4,:)=tot_b(:,1:n_sim);  
    
  end % for ii=1:length(rfropt.nspec)
  
  % finally do the spectrum combination if applies
  if length(rs)==2
    rs=comb_spec(rs(1),rs(2));
  end
  r.sim = rs.sim;
  
end % switch rfropt.rscaling

return

function y=chi2pdfnan(x,k)
% returns nans for x<0 instead of zeros
  y = chi2pdf(x,k);
  y(x<0)=nan;
return

function y=sumprodxynan(x,sigma,rho,ndof)
% switches to normal distribution when rho=nan
% where ndof serves as mean and sigma as sigma
  if length(rho)==1
    if isnan(rho)
      y = normpdf(x,ndof,sigma);
    else
      y = sumprodxy(x,sigma,rho,ndof);
    end
  else
    y=zeros(size(x));
    pnan = isnan(rho);
    y(~pnan) = sumprodxy(x(~pnan),sigma(~pnan),rho(~pnan),ndof(~pnan));
    y(pnan)  = normpdf(x(pnan),ndof(pnan),sigma(pnan));
  end
return

function pc0=est_vg_initials(dist,m,s)

if ~jbtest(dist,0.05,0.01)
  % consistent with Gaussian
  disp('constistent with Gaussian')
  pc0=[mean(dist),std(dist),nan,0,1];
else
  % use variance gamma if non-Gaussian
  sk=skewness(dist);
  ku=kurtosis(dist);
  a=m*sk/s;
  if 2>=a && a>0
    % correlation conditions are self-consistent
    nu=(m/(s*sk))*(3+sqrt(9-4*a));
    ss=(s^2/(4*abs(m)))*sqrt(-6+4*a+2*sqrt(9-4*a));
    co=sign(m)*((sqrt(9-4*a)-1)/(8-4*a))*sqrt(-6+4*a+2*sqrt(9-4*a));
    pc0=[nu,sqrt(ss),tan(co*pi/2),0,1];
  elseif ku>3
    % leptokurtic, correlation consistent with zero
    nu=6/(ku-3);
    ss=sqrt((ku-3)/6)*s;
    co=0;
    pc0=[nu,sqrt(ss),tan(co*pi/2),0,1];
  else
    % normal is the best we can do
    pc0=[mean(dist),std(dist),nan,0,1];
  end
end

return

