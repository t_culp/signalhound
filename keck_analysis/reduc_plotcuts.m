function [cm,c,cp]=reduc_plotcuts(tags,cut1,cut2,chflags,halfscans,noscreen,byrx,plotcutdir, savefilename, unqpass)
% [cm,c,cp]=reduc_plotcuts(tags,cut1,cut2,chflags,halfscans,noscreen,byrx,plotcutdir)
%
% If cut1='calc' generate and store cut parameters as derived
% from tod in save files tag_cutparams
%
% If halfscans=1, do not compress per-halfscan cuts. By default, 
% 1 if length(tags)<1000
%
% If noscreen=1 (default 0), close all open figure windows and suppress 
% printing to screen. i.e. only print to file
%
% If byrx=1 (default 0), makes additional plots split by receiver
%
% If tags is cell array get each cutparam file and concatenate. The
% first tag can be a filename with previously concatenated structures. 
% If tags is string read coaddmap file
%
% If cut1/2 not given get defaults
%
% If unqpass=1 compute unique pass fractions
%
% Plot each cut quantiity
%
% e.g.
%    reduc_plotcuts(get_tags('gal2011'),'calc');
%    reduc_plotcuts(tags(1:10));
%    reduc_plotcuts(tags(1:10),mapopt.cut,coaddopt.cut);
%    reduc_plotcuts('maps/0001/real_a_filtp3_weight3_gs_jack0',mapopt.cut);
%
% For longer data selections (1000 tags or more), the per-half-
% scan cuts and parameters are averaged down to the per-scanset
% level.  This allows reasonable memory usage and display
% resolution.  You can manually control this:
%    reduc_plotcuts(tags,[],[],[],1);   % Plot at half-scan level
%    reduc_plotcuts(tags,[],[],[],0);   % Plot at scanset level
%
% If given savefilename, saves the cpa, c1a, c2a, c1ca2, cut1, cut2 and the tags% tags saved as cut_tags for future use so it doesn't write over tags when loaded


if(~exist('cut1','var'))
  cut1=[];
end
if(~exist('cut2','var'))
  cut2=[];
end
if(~exist('chflags','var'))
  chflags=[];
end
if(~exist('halfscans','var'))
  halfscans=[];
end
if(~exist('noscreen','var'))
  noscreen=[];
end
if(~exist('byrx','var'))
  byrx=[];
end
if(~exist('plotcutdir','var'))
  plotcutdir=[];
end
if(~exist('savefilename','var'))
  savefilename=[];
end
if(~exist('unqpass','var'))
  unqpass=[];
end
if(isempty(tags))
  tags=get_tags('all2012', 'has_cuts');
end
if(isempty(cut1))
  cut1=get_default_round1_cuts;
end  
if(isempty(cut2))
  %use the year from the first tag (if you are inputing tags and not a map) 
  if iscell(tags)
    cut2=get_default_round2_cuts([],tags{1}(1:4));
  else
    cut2=get_default_round2_cuts;
  end
end

if(isempty(chflags))
  chflags=[];
end  
if(isempty(halfscans))
  % If not specified, decide based on tag count
  halfscans=(length(tags)<1000);
end
if(isempty(noscreen))
  % Display plot windows as well as print to file
  noscreen=0;
end
if(isempty(byrx))
  % Don't make additional per-rx plots
  byrx=0;
end
if(isempty(plotcutdir))
  % Save cut plots in the usual place
  plotcutdir='reduc_plotcuts';
end
if(isempty(unqpass))
  %Don't make unique plot cuts
  unqpass=0;
end
if noscreen
  % close all open windows
  close all
  FigVis=get(0,'DefaultFigureVisible');
  set(0,'DefaultFigureVisible','off');
end

% if wanted re-calc cut params for each tag - they normally get calc'd
% in batch initial reduction (called from reduc_batch1)
% files only used in preliminary phase when deciding which cutparams
% and thresholds to use
if(strcmp(cut1,'calc'))
  
  for i=1:length(tags)
    
    tags{i}
    
    % fetch the data - with relgain cal applied
    load(['data/real/',tags{i}(1:6),'/',tags{i},'_tod']);
   
    % get the array params for this tag
    [p,ind]=get_array_info(tags{i});
    
    % get the cut params
    cp=calc_cutparams(d,fs,en,lc,p,ind,dg);

    % save just the cutparams
    saveandtest(['data/real/',tags{i}(1:6),'/',tags{i},'_cutparams'],'cp');
    setpermissions(['data/real/',tags{i}(1:6),'/',tags{i},'_cutparams.mat']);
    
  end
  
  return
  
end

%make list of all cuts
% this needs to happen both for a coadded map and concatenated tags
cuts_all=[fieldnames(cut1)',fieldnames(cut2)']';

if (iscell(tags))
  % read in cut params for each tag from save files and concatenate
  for i=1:length(tags)
    tic
    tags{i}
    if length(tags{i})==4  %then it's a yearly structure to be added to...
      load(['data/real/',tags{i},'_cutparams']);
    else
      load(['data/real/',tags{i}(1:6),'/',tags{i},'_cutparams']);
    end
    
    %if the first tag is concatenated structures to add to, expand cpa and continue
    if (i==1 && exist('cpa','var'))
      cpa=repmat(cpa(1),length(tags),1);
      c1a=repmat(c1a(1),length(tags),1);
      c2a=repmat(c2a(1),length(tags),1);
      c1c2a=repmat(c1c2a(1),length(tags),1);
      %also make the first tags the tags from the saved file
      tags{1}=cut_tags;

      % Format of cut2.elnod_median changed, so convert if necessary.
      if all(size(cut2.elnod_median) == [1 4])
        en_med(1,:) = cut2.elnod_median(3:4); % 100GHz
        en_med(2,:) = cut2.elnod_median(1:2); % 150GHz
        cut2.elnod_median = en_med;
      end

      continue  
    end

    % get set of good channels as is done in reduc_makepairmaps
    [p,ind]=get_array_info(tags{i});
    
    % get set of good channels as is done in reduc_coaddpairmaps
    %do this now to save time in loop
    [pc,indc]=get_array_info(tags{i},[],[],[],[],chflags);

    if ~unqpass
      % If not creating unique cuts, just perform the round1 and round2 cuts
      [c1,cp] = eval_round1_cuts(cp, cut1, p, ind);
      c1.overall = combine_cuts(c1, ind);
      [c2,cp] = eval_round2_cuts(cp, c1, cut2, pc, indc);
      c2.overall = combine_cuts(c2, indc);

      % c1c2 is expected to contain the 'all' field in this case
      c1c2.all = repmat(c2.overall, [c1.nhs,1]) & c1.overall;

    else
      % To create unique cuts, we need to remove a cut parameter and see how
      % it affects the overall cut mask. The important thing to remember here
      % is that round2 does depend on the overall round1 cut mask, so
      % unfortunately eval_round2_cuts() must be evaluated again for every
      % round1 cut removal.

      % Doing round1 only needs to happen once, though.
      [c1,cp] = eval_round1_cuts(cp, cut1, p, ind);
      % Remove each round1 cut in turn
      for ff=fieldnames(cut1)'
        if ~isfield(c1, ff{1})
          % There may be cases where cut1 contains a field that c1 doesn't
          % contain. To add c1c2 to the end of the list in c1c2a, those
          % fields need to be inserted now, so just fill with true. (Note
          % that this is opposite of what xor_passfrac() fills with, but
          % we're setting the dummy result pre-xor-ing while it does
          % post-xor.
          if exist('c1c2a','var') && isfield(c1c2a, ff{1})
            c1c2.(ff{1}) = true([c1.nhs,c1.nch]);
          end
          continue;
        end

        c1u = c1;
        % Calculate overall with this cut removed
        c1u.overall = combine_cuts(rmfield(c1, ff{1}));

        % Now pass new information along and evaluate round2
        [c2u,cp] = eval_round2_cuts(cp, c1u, cut2, pc, indc);
        c2u = combine_cuts(c2u);

        % Create combined cut mask
        c1c2.(ff{1}) = repmat(c2u, [c1.nhs,1]) & c1u.overall;
      end
      % Finalize overall round1 mask
      c1.overall = combine_cuts(c1, ind);

      % With round1 cuts done, we can calculate round2 one last time
      [c2,cp] = eval_round2_cuts(cp, c1, cut2, pc, indc);
      % Then repeat for round2 fields like above, but no additional cut
      % evaluations are needed; just selectively combine.
      for ff=fieldnames(cut2)'
        if ~isfield(c2, ff{1})
          % There may be cases where cut2 contains a field that c2 doesn't
          % contain. To add c1c2 to the end of the list in c1c2a, those
          % fields need to be inserted now, so just fill with true. (Note
          % that this is opposite of what xor_passfrac() fills with, but
          % we're setting the dummy result pre-xor-ing while it does
          % post-xor.
          if exist('c1c2a','var') && isfield(c1c2a, ff{1})
            c1c2.(ff{1}) = true([c1.nhs,c1.nch]);
          end
          continue
        end
        c2u = combine_cuts(rmfield(c2, ff{1}));
        c1c2.(ff{1}) = repmat(c2u, [c1.nhs,1]) & c1.overall;
      end
      % Finally, finish the computations and fill in the 'all' field that
      % xor_passfrac() needs to build the unique stats.
      c2.overall = combine_cuts(c2, indc);
      c1c2.all = c2.overall;
    end

    % convert good pair list from get_array_info above into a cut for visualization
    c2.rgl=false(1,c2.nch); c2.rgl(indc.rgl)=true;
    
    % if wanted compress cutparams and and per scanset cut masks - this
    % is done in reduc_coaddpairmaps and is necessary when plotting
    % large numbers of tags
    if(~halfscans)
      cp=compress_perhs(cp);
      c1=compress_perhs(c1);
      c1c2=compress_perhs(c1c2);
    end
    

    % place in arrays for later concatenation
    cpa(i)=cp;
    c1a(i)=c1;
    c2a(i)=c2;
    c1c2a(i)=c1c2;
    
    % on first iteration expand array to full size to avoid slowdown due
    % to incremental expansion
    if (i==1)
      cpa=repmat(cpa(1),length(tags),1);
      c1a=repmat(c1a(1),length(tags),1);
      c2a=repmat(c2a(1),length(tags),1);
      c1c2a=repmat(c1c2a(1),length(tags),1);
    end

    if mod(i,100)==0
      disp i
    end
    toc
  end

else
  % read in coadd map file and take everything from there
  load(tags);
  cpa=coaddopt.c.cp;
  c1a=coaddopt.c.c1;
  c2a=coaddopt.c.c2;
  %concatenate c2a here in order to make c1c2a
  c2a=structcat(1,c2a);
  c1c2a.all=c2a.overall;
  tags=coaddopt.tags;
  cut1=coaddopt.mapopt{1}.cut;
  cut2=coaddopt.cut;
end

% concatenate
cpa=structcat(1,cpa);
c1a=structcat(1,c1a);
c2a=structcat(1,c2a);
c1c2a=structcat(1,c1c2a);

%create unique cuts
c1c2u=xor_passfrac(c1c2a, cuts_all);

%if the first tag was a file of concat tags
if iscell(tags{1})
  tags=cat(2,tags{1},tags{2:end});
end

%save the structure for future use
if ~isempty(savefilename)
  cut_tags=tags;%have to change the name of tags for loading later
  saveandtest(savefilename,'cpa','c1a','c2a','c1c2a','cut_tags','cut1','cut2','-v7.3');
  setpermissions(savefilename);
end

% get set of good channels without chflags - there is a wrinkle as
% to what is appropriate as we plot and show passing fraction based on
% the ind.rgl
[p,ind]=get_array_info(tags{1});

% plot the cut parameters and cuts thereon
% please maintain order in which they are evalulated

pcd=plotcutdir;

close all

% Do separate receiver plots
rx=unique(p.rx);
ind0=ind;
if numel(rx)==1
  % If BICEP2 or BICEP3
  dorx{1}=0; % regular plots
  dorx{2}=0; % then plots scaled in y
else
  % If Keck
  dorx{1}=rx; % first do the regular, all receiver plot
  for i=1:numel(rx)
    dorx{i+1}=rx(i); % then do individual receiver plots
  end
end

% If not requested, don't do any individual receivers
if ~byrx
  dorx=dorx(1);
end

for i=1:numel(dorx)
  % for each recevier
  ind=strip_ind(ind0,find(ismember(p.rx,dorx{i})));

  % if doing an individual receiver, scale plot y-dimension to exact channel number
  if i>1
    tvscl=dorx{i};
  else
    tvscl=[];
  end

  display('plotting and saving figures...')

  % plot the cut parameters and cuts thereon
  plotcut(1,cpa.fb_nancount(:,ind.rgl),c1a.fb_nancount(:,ind.rgl),c1c2u.fb_nancount(:,ind.rgl),[0,500],'fb_nancount',tags,cut1.fb_nancount,tvscl,pcd,unqpass)
  plotcut(2,cpa.fb_std_p0(:,ind.rgl),c1a.fb_std_p0(:,ind.rgl),c1c2u.fb_std_p0(:,ind.rgl),[0,10],'fb_std_p0',tags,cut1.fb_std_p0,tvscl,pcd,unqpass)
  if(isfield(c1a, 'fb_std_p0_darks'))
    plotcut(3,cpa.fb_std_p0(:,ind.gd),c1a.fb_std_p0_darks(:,ind.rgl),c1c2u.fb_std_p0_darks(:,ind.rgl),[0,10],'fb_std_p0_darks',tags,cut1.fb_std_p0_darks,tvscl,pcd,unqpass)
  end
  plotcut(4,cpa.fb_std_p3(:,ind.rgl),c1a.fb_std_p3(:,ind.rgl),c1c2u.fb_std_p3(:,ind.rgl),[0,10],'fb_std_p3',tags,cut1.fb_std_p3,tvscl,pcd,unqpass)
  plotcut(5,cpa.fb_std_sd_p0(:,ind.rglb),c1a.fb_std_sd_p0(:,ind.rgl),c1c2u.fb_std_sd_p0(:,ind.rgl),[0,3],'fb_std_sd_p0 (diffs)',tags,cut1.fb_std_sd_p0,tvscl,pcd,unqpass)
  plotcut(6,cpa.fb_std_sd_p3(:,ind.rglb),c1a.fb_std_sd_p3(:,ind.rgl),c1c2u.fb_std_sd_p3(:,ind.rgl),[0,3],'fb_std_sd_p3 (diffs)',tags,cut1.fb_std_sd_p3,tvscl,pcd,unqpass)  
  if(isfield(c1a,'fb_std_uncal'))
    plotcut(7,log10(cpa.fb_std_uncal(:,ind.e)),c1a.fb_std_uncal(:,ind.e),c1c2u.fb_std_uncal(:,ind.e),[-1,3],'log10(fb_std_uncal)',tags,log10(cut1.fb_std_uncal),tvscl,pcd,unqpass)  
  end
  plotcut(8,double(cpa.is_fj_row(:,ind.rgl)),c1a.is_fj_row(:,ind.rgl),c1c2u.is_fj_row(:,ind.rgl),[0,1.01],'is_fj_row',tags,cut1.is_fj_row,tvscl,pcd,unqpass)
  plotcut(9,double(cpa.is_fj_col(:,ind.rgl)),c1a.is_fj_col(:,ind.rgl),c1c2u.is_fj_col(:,ind.rgl),[0,1.01],'is_fj_col',tags,cut1.is_fj_col,tvscl,pcd,unqpass)
  
  plotcut(10,cpa.syncsampnum_diff1,c1a.syncsampnum_diff1(:,ind.rgl),c1c2u.syncsampnum_diff1(:,ind.rgl),[-5,5],'syncsampnum_diff1',tags,NaN,tvscl,pcd,unqpass)
  plotcut(11,cpa.syncsampnum_diff2,c1a.syncsampnum_diff2(:,ind.rgl),c1c2u.syncsampnum_diff2(:,ind.rgl),[-5,5],'syncsampnum_diff2',tags,NaN,tvscl,pcd,unqpass)
  
  plotcut(12,cpa.passfrac_col,c1a.passfrac_col(:,ind.rgl),c1c2u.passfrac_col(:,ind.rgl),[0,1.01],'passfrac_col',tags,cut1.passfrac_col,tvscl,pcd,unqpass)  
  plotcut(13,cpa.passfrac_chan,c1a.passfrac_chan(:,ind.rgl),c1c2u.passfrac_chan(:,ind.rgl),[0,1.01],'passfrac_chan',tags,cut1.passfrac_chan,tvscl,pcd,unqpass)  
  plotcutl(14,c1a.overall(:,ind.rgl),'round1 overall',tags,NaN,tvscl,pcd)  
  plotcutl(15,c2a.manual(:,ind.rgl),'manual',tags,NaN,tvscl,pcd)
  
  plotcut(16,cpa.elnod_mean(:,ind.rgl),c2a.elnod_mean(:,ind.rgl),c1c2u.elnod_mean(:,ind.rgl),[0,20000],'elnod_mean',tags,cut2.elnod_mean,tvscl,pcd,unqpass)
  plotcut(17,cpa.elnod_fracdel(:,ind.rgl),c2a.elnod_fracdel(:,ind.rgl),c1c2u.elnod_fracdel(:,ind.rgl),[0,0.5],'elnod_fracdel',tags,cut2.elnod_fracdel,tvscl,pcd,unqpass)
  plotcut(18,cpa.elnod_ab_ba(:,ind.rgl),c2a.elnod_ab_ba(:,ind.rgl),c1c2u.elnod_ab_ba(:,ind.rgl),[0,0.2],'elnod_ab_ba',tags,cut2.elnod_ab_ba,tvscl,pcd,unqpass)
  % If plotting for all receivers, pass all cut thresholds
  if length(rx) == length(dorx{i})
    cut2_elnod_median=cut2.elnod_median;
  % Otherwise select threshold appropriate to given receiver.
  else
    freqs = unique(p.band(ind.la)');
    rxfreq = unique(p.band(p.rx'==dorx{i} & p.band'~=0 & ~isnan(p.band')));
    idx = find(freqs == rxfreq);
    cut2_elnod_median=cut2.elnod_median(idx,:);
  end
  plotcut(19,cpa.elnod_median(:,dorx{i}+1),c2a.elnod_median(:,ind.rgl),c1c2u.elnod_median(:,ind.rgl),[0,10000],'elnod_median',tags,cut2_elnod_median,tvscl,pcd,unqpass,p,ind)
  plotcut(20,cpa.elnod_nancount(:,ind.rgl),c2a.elnod_nancount(:,ind.rgl),c1c2u.elnod_nancount(ind.rgl),[0,100],'elnod_nancount',tags,cut2.elnod_nancount,tvscl,pcd,unqpass)
 
  plotcut(21,cpa.fb_wn_sd_p0(:,ind.rglb),c2a.fb_wn_sd_p0(:,ind.rgl),c1c2u.fb_wn_sd_p0(:,ind.rgl),[0,1],'fb_wn_sd_p0',tags,cut2.fb_wn_sd_p0,tvscl,pcd,unqpass)
  plotcut(22,cpa.fb_1f_sd_p0(:,ind.rglb),c2a.fb_1f_sd_p0(:,ind.rgl),c1c2u.fb_1f_sd_p0(:,ind.rgl),[0,1],'fb_1f_sd_p0',tags,cut2.fb_1f_sd_p0,tvscl,pcd,unqpass)
  
  plotcut(23,cpa.skewness(:,ind.rglb),c2a.skewness_dif(:,ind.rgl),c1c2u.skewness_dif(:,ind.rgl),[-.5,.5],'skewness_dif',tags,cut2.skewness_dif,tvscl,pcd,unqpass)
  plotcut(24,cpa.fp_cor,c2a.fp_cor(:,ind.rgl),c1c2u.fp_cor,[0,1],'fp_cor',tags,cut2.fp_cor,tvscl,pcd,unqpass)
  plotcut(25,cpa.scanset_std(:,ind.rglb),c2a.scanset_std(:,ind.rgl),c1c2u.scanset_std(:,ind.rgl),[0,3],'scanset_std',tags,cut2.scanset_std,tvscl,pcd,unqpass)
  plotcut(26,cpa.stationarity_ab(:,ind.rgl),c2a.stationarity_ab(:,ind.rgl),c1c2u.stationarity_ab(:,ind.rgl),[0,0.5],'stationarity_ab',tags,cut2.stationarity_ab,tvscl,pcd,unqpass)
  plotcut(27,cpa.stationarity(:,ind.rglb),c2a.stationarity_dif(:,ind.rgl),c1c2u.stationarity_dif(:,ind.rgl),[0,0.5],'stationarity_dif',tags,cut2.stationarity_dif,tvscl,pcd,unqpass)
 
  plotcut(28,cpa.tfpu_mean,c2a.tfpu_mean(:,ind.rgl),c1c2u.tfpu_mean(:,ind.rgl),[0.23,0.35],'tfpu_mean',tags,cut2.tfpu_mean,tvscl,pcd,unqpass)
  plotcut(29,cpa.tfpu_std,c2a.tfpu_std(:,ind.rgl),c1c2u.tfpu_std(:,ind.rgl),[0,0.0001],'tfpu_std',tags,cut2.tfpu_std,tvscl,pcd,unqpass)
  
  % keck specific
  if(isfield(cpa,'enc_az_diff'))
    plotcut(30,cpa.enc_az_diff,c2a.enc_az_diff(:,ind.rgl),c1c2u.enc_az_diff(:,ind.rgl),[-3e4,3e4],'enc_az_diff',tags,cut2.enc_az_diff,tvscl,pcd,unqpass)
  end

 plotcut(31,cpa.az_range,c2a.az_range(:,ind.rgl),c1c2u.az_range(:,ind.rgl), [0 400],'az_range',tags,cut2.az_range,tvscl,pcd,unqpass)
  
 plotcut(32,cpa.num_fj(:,ind.rgl),c2a.num_fj(:,ind.rgl),c1c2u.num_fj(:,ind.rgl), [0 100],'num_fj',tags,cut2.num_fj,tvscl,pcd,unqpass)
 plotcut(33,cpa.num_destep(:,ind.rgl),c2a.num_destep(:,ind.rgl),c1c2u.num_destep(:,ind.rgl),[0 9],'num_destep',tags,cut2.num_destep,tvscl,pcd,unqpass)
 plotcut(34,cpa.max_fj_gap(:,ind.rgl),c2a.max_fj_gap(:,ind.rgl),c1c2u.max_fj_gap(:,ind.rgl),[0 1000],'max_fj_gap',tags,cut2.max_fj_gap,tvscl,pcd,unqpass)

 plotcut(35,cpa.passfrac_halfscan(:,ind.rgl),c2a.passfrac_halfscan(:,ind.rgl),c1c2u.passfrac_halfscan(:,ind.rgl),[0,1.01],'passfrac_halfscan',tags,cut2.passfrac_halfscan,tvscl,pcd,unqpass)  
 plotcut(36,cpa.passfrac_scanset,c2a.passfrac_scanset(:,ind.rgl),c1c2u.passfrac_scanset(:,ind.rgl),[0,1.01],'passfrac_scanset',tags,cut2.passfrac_scanset,tvscl,pcd,unqpass)  
  
 plotcutl(37,c2a.overall(:,ind.rgl),'round2 overall',tags,NaN,tvscl,pcd)
 
 plotcutl(38,c2a.rgl(:,ind.l),'rgl',tags,NaN,tvscl,pcd)

  plotcutn(39,cpa.net(:,ind.rglb),[0 1000],'net',tags,tvscl,pcd)
  plotcutn(40,cpa.net_per_col(:,:),[0 200],'net_per_col',tags,tvscl,pcd)
  plotcutn(41,cpa.net_per_tile(:,:),[0 100],'net_per_tile',tags,tvscl,pcd)
  plotcutn(42,cpa.net_per_rx(:,unique(p.rx(ind.l)+1)'),[0 40],'net_per_rx',tags,tvscl,pcd)

  plotcut(43,cpa.rtes_frac(:,ind.rgl),c2a.rtes_frac(:,ind.rgl),c1c2u.rtes_frac(:,ind.rgl),[0,1],'rtes_frac',tags,cut2.rtes_frac,tvscl,pcd,unqpass)
  plotcut(44,cpa.rnorm(:,ind.rgl),c2a.rnorm(:,ind.rgl),c1c2u.rnorm(:,ind.rgl),[0 0.12],'Rnorm (Ohm)',tags,cut2.rnorm,tvscl,pcd,unqpass)
  plotcut(45,cpa.pjoule(:,ind.rgl)*1.e12,c2a.pjoule(:,ind.rgl),c1c2u.pjoule(:,ind.rgl),[0 20],'Pjoule (pW)',tags,cut2.pjoule,tvscl,pcd,unqpass)
  plotcut(46,cpa.elnod_gof(:,ind.rgl),c2a.elnod_gof(:,ind.rgl),c1c2u.elnod_gof(:,ind.rgl),[0 250],'elnod_gof',tags,cut2.elnod_gof,tvscl,pcd,unqpass) 
  plotcut(47,cpa.elnod_chisq(:,ind.rglb),c2a.elnod_chisq_dif(:,ind.rgl), c1c2u.elnod_chisq_dif(:,ind.rgl),[0 15],'elnod_chisq_diff',tags,cut2.elnod_chisq_dif,tvscl,pcd,unqpass)

  %adding in skewness sum as well.  don't cut on it
  plotcutn(48,cpa.skewness(:,ind.rgla),[-1 1],'skewness_sum',tags,tvscl,pcd)
  plotcut(49,cpa.satcom(:,ind.rglb),c2a.satcom(:,ind.rgl),c1c2u.satcom(:,ind.rgl),[0 8],'satcom',tags,cut2.satcom,tvscl,pcd,unqpass)
  
end

if noscreen
  % close all open windows
  close all
  set(0,'DefaultFigureVisible',FigVis);
end


return



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotcut(nfig,cp,c,c1c2u,clim, cname,tlab,thresh,tvscl,plotcutdir, unqpass,p,ind)

% reduce to MMDD
for i=1:length(tlab)
  tlab{i}=tlab{i}(:,5:8);
end

h=figure; clf
if(unqpass)
%too big even of setwinsize!
set(gcf,'Position', [10,10,1580,1800]);clf
else
setwinsize(gcf,1580,1200);clf
end

colormap jet

n=size(tlab,2);
nl=min([n,30]);
l=round(linspace(1,n,nl));

if isempty(tvscl)
  whichrx='All Rxs';
else
  whichrx=sprintf('Rx %01d',tvscl);
end

%plot cutparams
if(unqpass)
subplot(3,1,1); 
else
subplot(2,1,1);
end
[dum,patchh]=imagescnan(cp'); caxis(clim);
set(patchh, 'edgecolor','none');
if ~isempty(tvscl)
  % scale axes in y so that pixel size is same as array size
  scale_axes(gcf,gca,cp);
end
colorbar
xl=xlim; sp=diff(xl)/nl; xt = l./n; xt=xl(1)+sp/2:sp:xl(2);
tmp_tlab={};
for i=1:length(xt)
  if round(xt(i))<=length(tlab)
    tmp_tlab{i}=tlab{round(xt(i))};
  else
    tmp_tlab{i}='';
  end
end
set(gca,'XTick',xt); set(gca,'XTickLabel',tmp_tlab); %set(gca,'XTickLabel',tlab(l));
title(sprintf('cut parameter %s, %s',strrep(cname,'_','\_'),whichrx));

%plot cut mask
if(unqpass)
subplot(3,1,2); 
else
subplot(2,1,2);
end

imagesc(c'); 
if ~isempty(tvscl)
  % scale axes in y so that pixel size is same as array size
  scale_axes(gcf,gca,cp);
end
caxis([0,1]);colorbar
xl=xlim; sp=diff(xl)/nl; xt = l./n; xt=xl(1)+sp/2:sp:xl(2);
tmp_tlab={};
for i=1:length(xt)
  if round(xt(i))<=length(tlab)
    tmp_tlab{i}=tlab{round(xt(i))};
  else
    tmp_tlab{i}='';
  end
end
set(gca,'XTick',xt); set(gca,'XTickLabel',tmp_tlab); %set(gca,'XTickLabel',tlab(l));

switch numel(thresh)
  case 1
    title(sprintf('with threshold %.2f fraction %4.2f passes',thresh,sum(c(:))/numel(c)));
  case 2
    title(sprintf('with range %.2f to %.2f fraction %4.2f passes',thresh,sum(c(:))/numel(c)));
  otherwise
    % This currently only happens for elnod_median when there are multiple
    % frequencies.
    tt = sprintf('with ranges %.2f to %.2f', thresh(1,:));
    if size(thresh,1) > 1
      if size(thresh,1) > 2
        % Don't forget the Oxford comma!!
        tt = [tt sprintf(', %.2f to %.2f', thresh(2:end-1,:)) ','];
      end
      tt = [tt sprintf(' and %.2f to %.2f', thresh(end,:))];
    end
    tt = sprintf('%s fraction %4.2f passes', tt, sum(c(:))/numel(c));
    title(tt);
end

%plot unique cut mask
if(unqpass) 
subplot(3,1,3); imagesc(~c1c2u'); 
if ~isempty(tvscl)
  % scale axes in y so that pixel size is same as array size
  scale_axes(gcf,gca,cp);
end
caxis([0,1]);colorbar
xl=xlim; sp=diff(xl)/nl; xt = l./n; xt=xl(1)+sp/2:sp:xl(2);
tmp_tlab={};
for i=1:length(xt)
  if round(xt(i))<=length(tlab)
    tmp_tlab{i}=tlab{round(xt(i))};
  else
    tmp_tlab{i}='';
  end
end
set(gca,'XTick',xt); set(gca,'XTickLabel',tmp_tlab); %set(gca,'XTickLabel',tlab(l));
title(sprintf('Cut fraction %.4f is unique to this cut',sum(c1c2u(:))/numel(c1c2u)));
end %unqpass fi

if isempty(tvscl)
  rxext='';
else
  rxext=sprintf('_rx%01d',tvscl);
end
mkpng(sprintf('%s/fig_%02d%s.png',plotcutdir,nfig,rxext),1);
setpermissions(sprintf('%s/fig_%02d%s.png',plotcutdir,nfig,rxext))

% plot histogram also

h=figure; clf
setwinsize(gcf,500,600); clf

ctick=clim(1)+(0:1:100)/100*(clim(2)-clim(1));
% some kludge to plot 2 histograms for elnod median (which is frequency dependent as of
% 2014), one histogram for 100 GHz and one for 150. This should be enough logic to
% determine that this is the elnod_median cut and all rx are being plotted based on the
% logic in the main function.
if numel(thresh)<=2
  N=histc(cp(:),ctick);
  N=N/sum(N);

  stairs(ctick,N,'b-');
  yl=ylim;
  hold on;

  if exist('cm','var')
    cm_orig=cm;
    % If compressed, cm may not be a logical.
    if ~islogical(cm)
      cm=(cm>0);
    end
    N2=histc(cp(cm),ctick);
    N2=N2/sum(N2);
    stairs(ctick,N2,'r-');
  end

  for i=1:length(thresh)
    line(thresh(i)*[1 1],yl,'color','black','linestyle','--');
  end
else
  freqs = unique(p.band(ind.la));

  % More need to be added here if more than 4 frequencies in use.
  colors = {'r','b','m','g'};

  legends=cell(size(freqs));
  for ii=1:length(freqs)
    rxnum = unique(p.rx(p.band==freqs(ii)));

    N=histc(cp(:,rxnum+1),ctick);
    N=N/sum(N);

    handles(ii) = stairs(ctick, N, [colors{ii} '-']);
    if (ii==1)
      hold on
      yl=ylim;
    end

    line(thresh(ii,1)*[1 1], yl, 'color',colors{ii}, 'linestyle','--');
    line(thresh(ii,2)*[1 1], yl, 'color',colors{ii}, 'linestyle','--');

    legends{ii} = sprintf('%d GHz', freqs(ii));
  end
  legend(handles, legends{:}, 'location','northwest');
end

xlabel(['cut parameter ' strrep(cname,'_','\_')]);
switch numel(thresh)
  case 1
    title(sprintf('with threshold %.2g fraction %4.2f passes',thresh,sum(c(:))/numel(c)));
  case 2
    title(sprintf('with range %.2f to %.2f fraction %4.2f passes',thresh,sum(c(:))/numel(c)));
  otherwise
    % This currently only happens for elnod_median when there are multiple
    % frequencies.
    tt = sprintf('with ranges %.2f to %.2f', thresh(1,:));
    if size(thresh,1) > 1
      if size(thresh,1) > 2
        % Don't forget the Oxford comma!!
        tt = [tt sprintf(', %.2f to %.2f', thresh(2:end-1,:)) ','];
      end
      tt = [tt sprintf(' and %.2f to %.2f', thresh(end,:))];
    end
    tt = sprintf('%s fraction %4.2f passes', tt, sum(c(:))/numel(c));
    title(tt);
end
xlim(clim);
grid on;

if isempty(tvscl)
  whichrx='All Rxs';
else
  whichrx=sprintf('Rx %01d',tvscl);
end
gtitle(whichrx,.99);

if isempty(tvscl)
  rxext='';
else
  rxext=sprintf('_rx%01d',tvscl);
end
mkpng(sprintf('%s/hist_%02d%s.png',plotcutdir,nfig,rxext),1);
setpermissions(sprintf('%s/hist_%02d%s.png',plotcutdir,nfig,rxext))

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotcutl(nfig,c,cname,tlab,thresh,tvscl,plotcutdir)

% reduce to MMDD
for i=1:length(tlab)
  tlab{i}=tlab{i}(:,5:8);
end

h=figure; clf
setwinsize(gcf,1580,600); clf
colormap jet

n=size(tlab,2);
nl=min([n,30]);
l=round(linspace(1,n,nl));

[dum,patchh]=imagescnan(c'); caxis([0,1]); colorbar;
set(patchh, 'edgecolor','none');
if ~isempty(tvscl)
  % scale axes in y so that pixel size is same as array size
  scale_axes(gcf,gca,c);
end
xl=xlim; sp=diff(xl)/nl; xt = l./n; xt=xl(1)+sp/2:sp:xl(2);
tmp_tlab={};
for i=1:length(xt)
  if round(xt(i))<=length(tlab)
    tmp_tlab{i}=tlab{round(xt(i))};
  else
    tmp_tlab{i}='';
  end
end
set(gca,'XTick',xt); set(gca,'XTickLabel',tmp_tlab); %set(gca,'XTickLabel',tlab(l));

if isempty(tvscl)
  whichrx='All Rxs';
else
  whichrx=sprintf('Rx %01d',tvscl);
end

if(~isnan(thresh))
  title(sprintf('cut %s, %s - with threshold %4.2f fraction %4.2f passes',strrep(cname,'_','\_'),whichrx,thresh,sum(c(:))/numel(c)));
else
  title(sprintf('cut %s, %s - fraction %4.2f passes',strrep(cname,'_','\_'),whichrx,sum(c(:))/numel(c)));
end

if isempty(tvscl)
  rxext='';
else
  rxext=sprintf('_rx%01d',tvscl);
end
mkpng(sprintf('%s/fig_%02d%s.png',plotcutdir,nfig,rxext),1);
setpermissions(sprintf('%s/fig_%02d%s.png',plotcutdir,nfig,rxext))

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotcutn(nfig,cp,clim,cname,tlab,tvscl,plotcutdir)
%plots with no cuts - like net

% reduce to MMDD
for i=1:length(tlab)
  tlab{i}=tlab{i}(:,5:8);
end

h=figure; clf
setwinsize(gcf,1580,500); clf
colormap jet

n=size(tlab,2);
nl=min([n,30]);
l=round(linspace(1,n,nl));

if isempty(tvscl)
  whichrx='All Rxs';
else
  whichrx=sprintf('Rx %01d',tvscl);
end

[dum,patchh]=imagescnan(cp'); caxis(clim);
set(patchh, 'edgecolor','none');
colorbar
xl=xlim; sp=diff(xl)/nl; xt = l./n; xt=xl(1)+sp/2:sp:xl(2);
tmp_tlab={};
for i=1:length(xt)
  if round(xt(i))<=length(tlab)
    tmp_tlab{i}=tlab{round(xt(i))};
  else
    tmp_tlab{i}='';
  end
end
set(gca,'XTick',xt); set(gca,'XTickLabel',tmp_tlab); %set(gca,'XTickLabel',tlab(l));
title(sprintf('cut parameter %s, %s',strrep(cname,'_','\_'),whichrx));

if isempty(tvscl)
  rxext='';
else
  rxext=sprintf('_rx%01d',tvscl);
end

mkpng(sprintf('%s/fig_%02d%s.png',plotcutdir,nfig,rxext),1);
setpermissions(sprintf('%s/fig_%02d%s.png',plotcutdir,nfig,rxext));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function c=compress_perhs(c,opt)

if ~exist('opt','var')
  opt='mean';
end

fn=fieldnames(c);
for i=1:length(fn)
  if ismember(fn{i},{'tag','nhs','nch','nrx'})
    continue
  end
  tmp=c.(fn{i});
  if(isfield(c, 'nhs'))
  if size(tmp,1)<c.nhs
    continue
  end
  end
  switch(lower(opt))
    case 'max', tmp=max(tmp,[],1);
    case 'min', tmp=min(tmp,[],1);
    case 'mean', tmp=mean(tmp,1);
  end
  c.(fn{i})=tmp;
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function scale_axes(fig,ax,cp)

figpos=get(fig,'Position');
xs=figpos(3);
ys=figpos(4);

axpos=get(ax,'Position');

% maintain mean y position
ymid=axpos(2)+axpos(4)/2;

% new size for y axis in normalized units
ny=size(cp,2);
dy=ny/ys;

% new corner for axes
y0=ymid-dy/2;

newpos=[axpos(1),y0,axpos(3),dy];
set(ax,'Position',newpos);

return


%%%%%%%%%%%%%%%%%%%%%%%%%%
function cpout=orderfields(cp,fn)

cpout=[];

for i=1:numel(fn)
  cpout=setfield(cpout,fn{i},getfield(cp,fn{i}));
end

return

%%%%%%%%%%%%%%%%%%%%%%%
function c1c2u=xor_passfrac(c1c2a, cuts_all)

fields=fieldnames(c1c2a);
c1c2u=struct();

% calculate unique cuts by xor-ing with the nominal overall cut mask
for f=fields'
  c1c2u.(f{1}) = xor(c1c2a.all, c1c2a.(f{1}));
end
% fill missing cuts from cuts_all with 0
for f=cuts_all'
  if ~isfield(c1c2u, f{1});
    c1c2u.(f{1}) = false(size(c1c2a.all));
  end
end

return
  


