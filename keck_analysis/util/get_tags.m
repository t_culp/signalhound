function tags=get_tags(tagsetname,flag,expt)
% tags=get_tags(tagsetname,flag,expt)
%
% Get a list of usable tags according to specified option
% 
% if flag='new' then only tags for which no _tod_uncal.mat file exists will be
% returned - used to update the reduction
% if flag='has_tod' only tags for which the _tod.mat file exists will
% be returned - used to avoid tags which crashed reduc_initial
% flag can also be a cell array of multiple requirements
%      
% e.g. tags=get_tags('cmb2010')
%      tags=get_tags('cmb2010','new')
%      tags=get_tags('cmb2010',{'has_tod','has_cuts'})
% Valid data selections:
%      cmb2010, cmb2011, cmb2012, cmb2013, cmb2014, cmb2015, cmb2016
%      gal2010, gal2011, gal2012, gal2013, gal2014, gal2015, gal2016
%      all2010, all2011, all2012, all2013, all2014, all2015, all2016
%      allcmb, allgal, all
%      subset2010b2, galsubset2010b2
%      cmb2010full, cmb2011full, cmb2012full, cmb2013full, cmb2014full,
%      cmb2015full, cmb2016full


if(~exist('tagsetname','var'))
  tagsetname='cmb2010';
end

if(~exist('flag','var'))
  flag='all';
end

if ischar(flag)
  flag={flag};
end

% get experiment name
expt=get_experiment_name;

auxpath='aux_data';
taglist='tag_list.csv';
[pt,kt]=ParameterRead(fullfile(auxpath,taglist));

% find desired year if included in setname
targetyear=NaN;
if(strfind(tagsetname,'2010'))
  targetyear='2010';
end
if(strfind(tagsetname,'2011'))
  targetyear='2011';
end
if(strfind(tagsetname,'2012'))
  targetyear='2012';
end
if(strfind(tagsetname,'2013'))
  targetyear='2013';
end
if(strfind(tagsetname,'2014'))
  targetyear='2014';
end
if(strfind(tagsetname,'2015'))
  targetyear='2015';
end
if(strfind(tagsetname,'2016'))
  targetyear='2016';
end

% exclude bad tags as identified by the 'good' field in the tag list:
if isfield(pt,'good')
  % get values - set empty values to 1
  pt.good=strtrim(pt.good);
  good=strcmp(pt.good,'') | strcmp(pt.good,'1');

  % downselect to exclude tags for which the 'good' field is zero
  good = logical(good);
  flds = fieldnames(pt);
  for ii=1:length(flds)
    pt.(flds{ii})=pt.(flds{ii})(good);
  end
end

% construct convenient phase/year/date/set var
phase=pt.ph;
tmp=char(pt.tag);
year=cellstr(tmp(:,1:4));
date=datenum(tmp(:,1:8),'yyyymmdd');
clear tmp
pt.set=strtrim(pt.set);
setn=zeros(size(pt.set));
setn(~strcmp(pt.set,''))=str2num(char(pt.set(~strcmp(pt.set,''))));
type=pt.type;

% define season start dates - these may need to depend on experiment
% (B2/Keck)
startdate=zeros(size(date));
startdate(strcmp(year,'2010'))=datenum(2010,02,15);
startdate(strcmp(year,'2011'))=datenum(2011,03,01);
startdate(strcmp(year,'2012'))=datenum(2012,01,01);
startdate(strcmp(year,'2013'))=datenum(2013,01,01);
startdate(strcmp(year,'2014'))=datenum(2014,03,01);
switch expt
 case('keck')
  startdate(strcmp(year,'2015'))=datenum(2015,03,01);
  startdate(strcmp(year,'2016'))=datenum(2016,01,01);
 case('bicep3')
  startdate(strcmp(year,'2015'))=datenum(2015,04,19);
  startdate(strcmp(year,'2016'))=datenum(2016,02,01);
end  

% likewise, also define a season end date.
enddate=zeros(size(date));
enddate(strcmp(year,'2010'))=datenum(2010,12,31);
enddate(strcmp(year,'2011'))=datenum(2011,12,31);
enddate(strcmp(year,'2012'))=datenum(2012,12,31);
enddate(strcmp(year,'2013'))=datenum(2013,12,31);
enddate(strcmp(year,'2014'))=datenum(2014,11,03);
switch expt
 case('keck')
  enddate(strcmp(year,'2015'))=datenum(2015,11,15);
  enddate(strcmp(year,'2016'))=datenum(2016,12,31);
 case('bicep3')
  enddate(strcmp(year,'2015'))=datenum(2015,12,31);
  enddate(strcmp(year,'2016'))=datenum(2016,12,31);
end

% find the set of matching tags
switch tagsetname
  
  % Be sure to exclude A/X and 00 tags from all sets

  case {'cmb2010full','cmb2011full','cmb2012full','cmb2013full',...
        'cmb2014full','cmb2015full','cmb2016full'}
    sel=strcmp(year,targetyear) & date>=startdate ...
      & ~ismember(phase,{'A','X'}) & setn>0 & strncmp(type,'cmb',3);
    tag=pt.tag(sel)';

  case {'cmb2010','cmb2011','cmb2012','cmb2013','cmb2014','cmb2015','cmb2016'}
    sel=strcmp(year,targetyear) & date>=startdate & date<=enddate ...
      & ~ismember(phase,{'A','X'}) & setn>0 & strncmp(type,'cmb',3);
    tag=pt.tag(sel)';

  case {'allcmb'}
    sel=date>=startdate & ~ismember(phase,{'A','X'}) ...
      & setn>0 & strncmp(type,'cmb',3);
    tag=pt.tag(sel)';
   
  case {'allgal'}
    sel=date>=startdate & ~ismember(phase,{'A','X'}) ...
      & setn>0 & strncmp(type,'gal',3);
    tag=pt.tag(sel)';

  case {'gal2010','gal2011','gal2012','gal2013','gal2014','gal2015','gal2016'}
    sel=strcmp(year,targetyear) & date>=startdate ...
      & strncmp(type,'gal',3) & ~ismember(phase,{'A','X'}) ...
      & setn>0;
    tag=pt.tag(sel)';

  case {'all2010','all2011','all2012','all2013','all2014','all2015','all2016'}
    sel=strcmp(year,targetyear) & date>=startdate ...
      & (strncmp(type,'gal',3) | strncmp(type,'cmb',3)) ...
      & ~ismember(phase,{'A','X'}) & setn>0;
    tag=pt.tag(sel)';

  case {'all'}
    sel=date>=startdate ...
      & (strncmp(type,'gal',3) | strncmp(type,'cmb',3)) ...
      & ~ismember(phase,{'A','X'}) & setn>0;
    tag=pt.tag(sel)';

  % JAB 2011-03-15
  % 8 B2 standard phases (4 days) from 2010, after TES/SQ bias changes 
  case 'subset2010b2'
    % this list drove my f'ing crazy - it is not a complete
    % coverage pattern!
    %taglist={'20100918I_dk113','20100918F_dk113','20100915C_dk068','20100915E_dk068','20100914H_dk293','20100914I_dk293','20100921B_dk248','20100921C_dk248'};
    % this one is...
    taglist={'20100914H_dk293','20100914I_dk293','20100915E_dk068','20100915F_dk068','20100918H_dk113','20100918I_dk113','20100921B_dk248','20100921C_dk248'};
    
    tl={};
    for jj=1:length(taglist);
      tagtemp=taglist{jj};
      indx=strfind(tagtemp,'_');
      for ii=1:10;
	tl{ii+10*(jj-1)}=[tagtemp(1:indx-1) sprintf('%.2d',ii) tagtemp(indx:end)];
      end
    end
    tag=tl;

  case 'galsubset2010b2'
    taglist={'20100912D_dk293','20100918D_dk113','20100915D_dk068','20100921D_dk248'};
    tl={};
    for jj=1:length(taglist);
      tagtemp=taglist{jj};
      indx=strfind(tagtemp,'_');
      for ii=1:7;
	tl{ii+10*(jj-1)}=[tagtemp(1:indx-1) sprintf('%.2d',ii) tagtemp(indx:end)];
      end
    end
    tag=tl;

  case 'subsetkeck2012'
    startdate=datenum(2012,6,6);
    enddate=datenum(2012,6,13);
    sel= date>=startdate & date<=enddate ....
      & setn>0 & strncmp(type,'cmb',3);
    tag=pt.tag(sel);

  case 'subsetkeck2013'
    startdate=datenum(2013,7,24);
    enddate=datenum(2013,8,7);
    sel= date>=startdate & date<=enddate ....
      & setn>0 & strncmp(type,'cmb',3);
    tag=pt.tag(sel);

  case 'cmbmirror2015'
    startdate=datenum(2015,4,1);
    enddate=datenum(2015,4,7);
    sel= date>=startdate & date<=enddate & setn>0 & strncmp(type,'cmb',3);
    tag=pt.tag(sel);

  case 'galmirror2015'
    startdate=datenum(2015,4,1);
    enddate=datenum(2015,4,7);
    sel= date>=startdate & date<=enddate & setn>0 & strncmp(type,'gal',3);
    tag=pt.tag(sel);

  otherwise
    tag={};
end

for i=1:length(flag)
switch flag{i}
  % If requested strip out all tags for which tod files already exist
  case 'new'
    tags=tag(~has_tod(tag) | ~has_tod(tag,[],'_cutparams.mat'));
    
  % If requested leave out all tags that don't have tod
  case 'has_tod'
    tags=tag(has_tod(tag));
    
  case 'no_cuts'
    tags=tag(~has_tod(tag,[],'_cutparams.mat'));
    
   case 'has_cuts'
    tags=tag(has_tod(tag,[],'_cutparams.mat'));
    
  otherwise
    tags=tag; 
end
tag=tags;
end
disp(sprintf('get_tags: you have chosen to analyze %d tags',length(tags)));

return
