function d=get_xferfunc(d,dc,combine,uselpf)
% d=get_xferfunc(d,dc,combine,uselpf)
%
% input:
%   dc = continuous deconv period
%   combine = explicitly state combine on/off (1/0) [optional]
%           = default is on (1)
%   uselpf = apply a low pass filter when deconvolving
%          = default is 1, which applies 5Hz lpf for reduc_initial
%          = can be a user-defined freq-domain lpf
%          = if 0, then no lpf is applied
%          Note that LPF design assumes archived data at 20 samples/s.
%          For combine off data or other archive rate, pass band will
%          change while filter coefficients stay the same.
%
% output:
%   d.tf(nrx,ndc) where ndc = num deconv blocks, nrx = num MCEs
%   tf.deconvkern = deconvolution kernel to be conv with data
%                   inverse of filter functions convolved with
%                   low pass filter to keep deconv from ringing
%                   designed to be FIR
%   tf.kernel = impulse response of convolved mce/gcp filters
%               an array or matrix of kernel values
%               if a matrix includes measured tf
%   tf.seconds = array of length(tf.kernel) in seconds
%   tf.dc = deconvolution blocks to pass to deconv_scans.m
%   tf.lpf = impulse response function of low pass filter
%   tf.info = filter info
%   tf.gcp_taps = antialiasing filter applied by GCP
%   tf.gcpds = downsample factor applied in GCP
%
% calculate bolometer impulse response
% determine appropriate mce/gcp filter kernel from d structure
% empirical transfer functions (with mce/gcp filters deconvolved) can be added in optionally
% kernel is automatically truncated, starting with a length of 10sec
% convolve inverse of filter kernel with low pass filter to create time domain deconv kernel

% default should be to apply lpf, as used in reduc_initial.m
if(~exist('uselpf','var') || isempty(uselpf))
  uselpf=1;
end

% for season 1 bicep2 data you can't tell whether combine is on/off
% some data may be taken with combine on, n=1 which would apply the 
% fir filter, but not downsample.  For season 2 bicep2/keck data the
% fir filter taps are archived, so you can determine combine on/off
% assume that data was acquired with combine on if nsnap>1, and
% combine off if nsnap=1, unless you explicitly specify combine=0
if nargin<3
  combine=[];
end

nmce=size(d.mce0.frame.status,2);

tf=[];
for ii=1:length(dc.sf)

  % Loop over MCEs
  for jj=1:nmce

    % store the deconv block for deconv_scans
    tf(jj,ii).dc.sf=dc.sf(ii);
    tf(jj,ii).dc.ef=dc.ef(ii);
    tf(jj,ii).dc.s=dc.s(ii);
    tf(jj,ii).dc.e=dc.e(ii);

    % determine the mce settings
    data_rate=double(nanmedian(d.mce0.cc.data_rate(dc.s(ii):dc.e(ii),jj)));
    num_rows=double(nanmedian(d.mce0.cc.num_rows(dc.s(ii):dc.e(ii),jj)));
    row_len=double(nanmedian(d.mce0.cc.row_len(dc.s(ii):dc.e(ii),jj)));

    % determine the sampling rate before gcp downsampling is applied
    fsamp=50e6/(num_rows*row_len*data_rate);

    % determine gcp downsample factor
    gcpds=double(nanmedian(d.array.frame.nsnap(dc.s(ii):dc.e(ii))));

    % Check if MCE is off
    if (data_rate==0 && num_rows==0 && row_len==0)
      tf(jj,ii).info{1}=['mce' num2str(jj-1) ' is off.'];
      continue
    end

    % make npts some multiple of the total downsample factor
    % (down to final rate) to keep impulse function evenly sampled
    % start with kernel of length 10s
    kernlen=10;
    dstot=data_rate*gcpds;
    nptmce=ceil(fsamp*data_rate*kernlen);
    if mod(nptmce,dstot);
      nptmce=floor(nptmce/(dstot)).*(dstot);
    end
    if gcpds==1
      nptgcp=nptmce/dstot;
    else
      nptgcp=ceil(fsamp*kernlen);
      if mod(nptgcp,gcpds);
        nptgcp=floor(nptgcp/gcpds).*gcpds;
      end
    end

    % calculate mce filter transfer function
    [hmce,fltr_coeff]=get_mce_xfer(d,dc,ii,jj,gcpds*data_rate,nptmce);

    % calculate gcp filter transfer function
    [hgcp,gcp_taps]=get_gcp_xfer(d,dc,ii,fsamp,gcpds,nptgcp,combine);

    % include measured bolometer transfer functions
    % the mce/gcp transfer functions must already be deconv
    % add this in later, for now just assume it's flat response
    hmeas=ones(length(hgcp),1);

    % normalize transfer functions before convolving
    hgcp=hgcp./hgcp(1);
    hmce=hmce./hmce(1);
    hmeas=hmeas./hmeas(1);

    htot=hgcp.*hmce.*hmeas;

    % back to the time domain
    imptot=ifft(htot,'symmetric');

    % make filter kernel as compact as possible, keep >1e-15
    if(1)
      imptot=imptot(abs(imptot)>1e-15);
    end

    % normalize kernel to one at dc, do it after compactifying
    % this is the combined impulse response function of the 
    % mce & gcp filters
    kern=imptot./repmat(abs(nansum(imptot,1)),size(imptot,1),1);

    if (length(uselpf) > 1)
      % Keep user-supplied low-pass filter.
      lpf = uselpf;
    elseif(uselpf==1)
      % design low pass filter (3-5Hz stop band) to be convolved with 
      % inverse of filter kernel, this is what is used in deconv_scans
      % low pass filter should be FIR to make invalid regions finite
      % N=32 gives the minimum for all filter combinations
      % coefficients found from chisq min of ripple < 3Hz
      N=32;
      if (N<length(kern))
        N=2*ceil(length(kern)/2);
      end
      fp=0.286069192737340;
      fs=0.5; % 0.928499208688734;
      hlpf=firpm(N,[0 fp fs 1],[1 1 0 0])';
      % make it zero-phase
      hshft=circshift(hlpf,-N/2);
      % go to the f domain and normalize
      lpf=fft(hshft);
      lpf=lpf./lpf(1);
 
      % save default the low pass filter impulse response if used     
      tf(jj,ii).lpf=hlpf;
      tf(jj,ii).info{4}=['low pass filter firpm, fp = ' num2str(fp,4) ...
       ' , fs = ' num2str(fs,4), ' with ' int2str(length(hlpf)) ' taps = ' num2str(hlpf',3)];

    elseif(uselpf==0)
      % don't apply lpf, just set it to 1
      lpf=1;
    end
    N=length(lpf)-1;

    % take normalized, truncated kernel back to f domain
    % zero pad it to the same length as the lpf
    % but shift right, pad, shift left to preserve phase
    % shift point closest to zero to end for padding
    [a,shftind]=min(abs(kern));
    shft=length(kern)-shftind;
    impp=circshift([circshift(kern,shft);zeros(N+1-length(kern),1)],-shft);
    impf=fft(impp);

    % convolve lpf with inverse of mce/gcp filter transfer functions
    % and normalize to 1 at DC
    % this is the final time domain deconv kernel to be convolved
    % with data in deconv_scans
    deconvkern=fftshift(ifft(lpf./impf,'symmetric'));
    deconvkern=deconvkern./repmat(abs(nansum(deconvkern,1)),size(deconvkern,1),1);

    % replace tf.gcp structure with tf.kernel matrix
    % if no empirical transfer function is included,
    % tf.kernel will be 1 channel wide
    tf(jj,ii).kernel=kern;
    tf(jj,ii).deconvkern=deconvkern;
    tf(jj,ii).sec_kern=(0:length(kern)-1)'./(fsamp/gcpds);
    tf(jj,ii).sec_deconvkern=(0:length(deconvkern)-1)'./(fsamp/gcpds);
    tf(jj,ii).gcp_taps=gcp_taps;
    tf(jj,ii).gcpds=gcpds;

    tf(jj,ii).info{1}=['mce filter coefficients = ' int2str(fltr_coeff)];
    tf(jj,ii).info{2}=[int2str(length(gcp_taps)) ' gcp filter taps = ' num2str(gcp_taps,3)];
    tf(jj,ii).info{3}=['final data rate = ' num2str(fsamp/gcpds,6) ' Hz'];

  end

end

% tack tf structure on
d.tf=tf;

end

%%%%%%%%%%%%%%%%%%%%%%
function [hmce,fltr_coeff]=get_mce_xfer(d,dc,ii,jj,dstot,nptmce)

% determine mce settings
% apply mce filter only if data_mode is filtered feedback
data_mode=nanmedian(d.mce0.rc1.data_mode(dc.s(ii):dc.e(ii),jj));

% if the data_mode is not for filtered data just
% return a flat mce filter transfer function
if ~sum(data_mode==[2,6:10])
  hmce=1;
  fltr_coeff=0;
  return
end

% check to see if mce type2 filter parameters are being used
% for bicep2 type2 filter is the default, but for keck this can be programmed.
% for type2 mce filter, the filter cutoff scales as frame_rate/400;
if isfield(d.mce0.rc1,'fltr_type')
  fltr_type=int32(nanmedian(d.mce0.rc1.fltr_type(dc.s(ii):dc.e(ii),jj)));

  % Check whether this MCE is on.
  if (fltr_type==0)
    warning('quad_analysis:get_xferfunc:mce_off',['mce' num2str(jj-1) ' is off.']);
    hmce=1;
    fltr_coeff=0;
    return
  end

  fltr_coeff=double(nanmedian(d.mce0.rc1.fltr_coeff(dc.s(ii):dc.e(ii),:),1));
  % Get coefficients for this MCE only
  nmces=size(d.mce0.rc1.data_mode,2);
  nparams=size(fltr_coeff,2)/nmces;
  fltr_coeff=reshape(fltr_coeff, nparams, nmces)';
  fltr_coeff=fltr_coeff(jj,:);

else % no fltr_type field
  % mce type2 filter coeff, valid for all sampling rates
  fltr_coeff=double([32295, 15915, 32568, 16188, 3, 14]);
end

% first pass at correct mce filter transfer function
% try to include filter coefficient quantization effects
% filters quantized to 15bit resolution
% mce filter is realized as 2 cascaded biquads, with
% coeff specified for the 2 second order sections (SOS)
sosmatrix=[1 2 1 1 -fltr_coeff(1)/2^14 fltr_coeff(2)/2^14;...
           1 2 1 1 -fltr_coeff(3)/2^14 fltr_coeff(4)/2^14];

% generate the impulse response functions at the appropriate sampling rates
% [impmce,timemce]=impz(bmce,amce,nptmce,fsamp.*data_rate);
impmce=sosfilt(sosmatrix,[1; zeros(nptmce-1,1)]);

% downsample mce impulse response function to the gcp sample rate
% right now, do it dumbly and don't introduce phase offset
impmceds=downsample(impmce,dstot);

hmce=fft(impmceds);

end
%%%%%%%%%%%%%%%%%%%%%%
function [hgcp,bgcp]=get_gcp_xfer(d,dc,ii,fsamp,gcpds,nptgcp,combine);

% determine if combine is on/off if possible (need d.array.filter)
% if gcpds>1 you know combine is on and a filter is applied
% if gcpds=1 combine can be on or off, if you can't determine it 
% from d.array.filter and no combine input is specified, assume on
if gcpds==1 
  if isfield(d.array,'filter')
    tapsum=nansum(d.array.filter.taps(dc.sf(ii):dc.ef(ii)));
    if tapsum==0
      combine=0;
    else
      combine=1;
    end
  else
    combine=0;
  end
else
  % don't override user input if you can't figure it out above
  if isempty(combine)
    combine=1;
  end
end

% if combine is off, return a flat gcp filter transfer function
if combine==0
  hgcp=1;
  bgcp=0;
  return
end

% if d.array.filter exists, read in filter taps
% bicep2 first season didn't record taps so we have
% to determine the applied filter from the date here
% what precision is actually used in gcp filter calcs?
if isfield(d.array,'filter')
  filtstart=find(d.array.filter.idx(dc.sf(ii):dc.ef(ii))==0);
  ntaps=nanmedian(diff(filtstart));
  filtindx=ceil(length(filtstart)/2)+1;
  bgcp=[];
  while length(bgcp)<ntaps
    disp(['Have filtindx=' num2str(filtindx) ', dropping by one to ' num2str(filtindx-1) '.']);
    filtindx=filtindx-1;
    bgcp=transpose(d.array.filter.taps((dc.sf(ii)+filtstart(filtindx))-1:(dc.sf(ii)+filtstart(filtindx+1))-2));
  end
else
  % determine bicep2 first year configuration from the date
  % d.t is mjd, convert to gregorian
  dat=str2num(datestr(datenum('nov-17-1858','mmm-dd-yyyy')+median(d.antenna0.time.utcfast),'yyyymmddHH'));
  if(dat>2010032519 & dat<2011031015)
    % starting tag: 20100325B01_dk265  ending tag:  20110309C10_dk068
    % starting date:  25-Mar-2010:20:15:53  ending date:  10-Mar-2011-14:35:46
    % fir filter fir-n-60-100-x5
    % 5x gcp downsample
    N=120;
    fp=0.12;
    fs=0.2;
    bgcp=firpm(N,[0 fp fs 1], [1 1 0 0]);
    bgcp=bgcp/sum(bgcp);
  end
  if(dat>2010101 & dat<2010032514)
    % ending tag 20100322I10_dk130
    % ending date:  25-Mar-2010:13:36:41  
    % fir filter fir-80-90-x5
    % 5x gcp downsample
    N=50;
    fp=0.16;
    fs=0.18;
    bgcp=fir2(N,[0 fp fs 1], [1 1 0 0]);    
  end
end

% generate the impulse response functions at the appropriate sampling rates
impgcp=impz(bgcp,1,nptgcp,fsamp);

% the gcp impulse response function is linear-phase, to make it
% zero-phase shift it left by (length(bgcp)-1)/2
shft=(length(bgcp)-1)/2;
impgcpzp=circshift(impgcp,-shft);

% downsample gcp impulse response function to 
% the final downsampled gcp sample rate.
% as long as the downsampling of the impulse response function is
% even, this is the correct thing to do.
impgcpzpds=downsample(impgcpzp,gcpds);

hgcp=fft(impgcpzpds);

end
%%%%%%%%%%%%%%%%%%%%%%
