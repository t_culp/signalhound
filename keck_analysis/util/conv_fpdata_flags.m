function [conv,varargout]=conv_fpdata_flags(flags)
% [conv,err]=conv_fpdata_flags(flags)
%
% Converts a series of flag IDs to their equivalent decimal values, or given
% a flag value(s) will return the ID name(s). The two cases are defined as:
%
%   CASE 1: A string or cell array of flag IDs are combined to their
%           numerical value.
%
%   CASE 2: A numeric scalar which will be converted to the corresponding
%           constituent ID strings.
%
% INPUTS
%   flags    Flags to convert to the opposite representation.
%
% OUTPUTS
%   conv     For case 1, a scalar uint64 value corresponding to the bitfield
%            created by setting each flag given by the flag ID.
%
%            For case 2, a cell array filled with the strings which
%            correspond to the set bits in the bit-field.
%
%   err      Optional. If present, error messages are returned or is empty
%            if everything worked successfully. If no present, then the
%            error exception is thrown.
%
% SEE ALSO
%   enum_fpdata_flags
%

  % Handle errors in one of two ways.
  if (nargout == 2)
    throws = false;
  else
    throws = true;
  end

  if isnumeric(flags) && length(flags) == 1
    [conv,err] = conv_uint64_to_string(flags, throws);
  elseif ischar(flags)
    % For a scalar string, convert to a cell array
    [conv,err] = conv_string_to_uint64({flags}, throws);
  elseif iscell(flags) && ischar(flags{1})
    [conv,err] = conv_string_to_uint64(flags, throws);
  else
    msg = 'Unrecognized type in argument: flags';
    if (throws)
      error('conv_fpdata_flags:type_error', msg);
    else
      err = msg;
    end
  end

  if (nargout == 2)
    varargout{1} = err;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [val,err]=conv_uint64_to_string(bitfield, throws)
  err = [];

  % Verify that the numeric values are in fact whole integers. Emit a warning
  % if fractional truncation occurs.
  truncfrac = false;
  if isa(bitfield,'float') && abs(bitfield-fix(bitfield)) > 1e-13
    warning(['conv_fpdata_flags: flags contains non-integer values. ' ...
             'Truncation may cause loss of information.']);
  end

  % Convert to binary integers
  bitfield = uint64(bitfield);

  % Explicitly handle OK/0 case
  if (bitfield == 0)
    val = {'ok'};
    return
  end

  % Get the enumerated list of known flags
  flagenum = enum_fpdata_flags();
  valsenum = horzcat(flagenum(:).flag);
  % Get the mask for all known flags
  knownbits = valsenum(1);
  for ii=2:length(valsenum)
    knownbits = bitor(knownbits, valsenum(ii));
  end

  val = {};
  % Loop through every bit to examine whether the flag is set or not
  for bit=0:63
    bitmask = bitset(uint64(0), bit+1);

    % If the bit is not set, continue with the next
    if ~bitand(bitfield, bitmask)
      continue
    end

    % If the bit is not recognized as a known enumeration, set ID to
    % "unknownN" where N is replaced by the bit place.
    if ~bitand(knownbits, bitmask)
      val{end+1} = sprintf('unknown%d', bit);

    % Otherwise locate the corresponding bit name from the enumeration
    else
      idx = find(valsenum == bitmask, 1);
      val{end+1} = flagenum(idx).id;
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [bitfield,err]=conv_string_to_uint64(flags, throws)
  err = [];
  bitfield = uint64(0);

  % Get the enumerated list of known flags
  flagenum = enum_fpdata_flags();
  idsenum = {flagenum(:).id};

  % Convert each given flag
  for ii=1:length(flags)
    % Don't do anything if given the special 'ok' flag
    if strcmp(flags, 'ok')
      % Skip the rest of the loop and do the next flag
      continue;
    end

    % Find a registered flag
    [tf,idx] = ismember(flags{ii}, idsenum);
    if (tf)
      bitfield = bitor(bitfield, flagenum(idx).flag);
      % Skip the rest of the loop and do the next flag
      continue;
    end

    % Check for the "unknownN" flags explicitly
    if strncmp(flags{ii}, 'unknown', 7)
      % Parse the number portion and make sure it processes correctly.
      numstr = flags{ii}(8:end);
      N = str2double(numstr);

      if ~isempty(N)
        % Set the N-th bit
        bitfield = bitor(bitfield, uint64(2^N));
      % Skip the rest of the loop and do the next flag
        continue;
      end

      % Fall through to the unhandled case for all other cases
    end

    % Emit an error if we don't recognize a name
    msg = ['Unknown flag ''' flags{ii} ''''];
    if (throws)
      error('conv_fpdata_flags:unknown_flag', msg)
    else
      err = msg;
    end
  end
end
