function ac=expandac(m1,m2,ac)
% ac=expand_ac(m1,m2,ac)
%
% Take ac with map defn m1 and expand to ac with map defn m2 by
% padding around the edge

if(m1.pixsize~=m2.pixsize)
  error('pixsize must be equal');
end

if(any(size(ac(1).wsum)~=[m1.ny,m1.nx]))
  error('ac does not match m1');
end

% check m1 lies wholly inside m2
%if(length(intersect(m1.x_tic,m2.x_tic))~=m1.nx|length(intersect(m1.y_tic,m2.y_tic))~=m1.ny)
%  error('m1 not wholly inside m2');
%end

xpre=find(m1.x_tic(1)==m2.x_tic)-1;
xpost=m2.nx-find(m1.x_tic(end)==m2.x_tic);

ypre=find(m1.y_tic(1)==m2.y_tic)-1;
ypost=m2.ny-find(m1.y_tic(end)==m2.y_tic);

% expand
for i=1:numel(ac)
  for fi=fieldnames(ac)'
    ac(i).(fi{1})=padarray(ac(i).(fi{1}),[ypre,xpre],'pre');
    ac(i).(fi{1})=padarray(ac(i).(fi{1}),[ypost,xpost],'post');
  end
end

return
