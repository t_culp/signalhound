function flags=enum_fpdata_flags()
% flags=enum_fpdata_flags()
%
% Enumerates the bit flags used the fp_data files with descriptive names.
%
% INPUTS
%   none
%
% OUTPUTS
%   flags    An array of structs containing three members:
%              .id         The short name for the field.
%
%              .flag       The decimal value of the flag bit. Should be set
%                          such that binary-and will test for the presense and
%                          binary-or will set the flag.
%
%              .comment    An arbitrary string comment. May be empty.
%
% SEE ALSO
%   set_fpdata_flags
%

  flags = struct('id',{}, 'flag',{}, 'comment',{});

  % For convenience of calculation, append_flag takes in just the bit which
  % is to be set and calculates the decimal value itself.
  %
  % Bits are 0-indexed despite Matlab conventions since this makes it
  % mathematically consistent with the N-th bit having the value 2^N.

  % These come from the focal plane fp_data files
  flags = append_flag(flags, 'open',           0, ...
    'Open connection found in IB test or other bench testing');
  flags = append_flag(flags, 'noisy_tes',      1, '');
  flags = append_flag(flags, 'noise_SQ_amp',   2, '');
  flags = append_flag(flags, 'other',          3, '');
  flags = append_flag(flags, 'other2',         4, '');
  flags = append_flag(flags, 'epsilon',        5, '');
  flags = append_flag(flags, 'worst_tau',      6, '');
  flags = append_flag(flags, 'changed_tau',    7, '');
  flags = append_flag(flags, 'tes_broken',     8, '');
  flags = append_flag(flags, 'tes_open',       9, '');
  flags = append_flag(flags, 'tes_not_bonded',10, '');
  flags = append_flag(flags, 'antenna_short', 11, '');
  flags = append_flag(flags, 'dark_squid',    12, ...
    'SQUID that is not associated with a TES');
  flags = append_flag(flags, 'dead_sq1',      13, ...
    'Bad 1st stage SQUID, as found during SQUID tuning');
  flags = append_flag(flags, 'dead_sq2',      14, ...
    'Bad 2nd stage SQUID, as found during SQUID tuning ');
  flags = append_flag(flags, 'dead_ssa',      15, ...
    'Bad SQUID series array module column, as found during SQUID tuning ');
  flags = append_flag(flags, 'bad_tes',       16, '');
  flags = append_flag(flags, 'tes_off_trans', 17, '');
  flags = append_flag(flags, 'antenna_broken',18, '');
  flags = append_flag(flags, 'multiple_sq1s', 19, '');
  flags = append_flag(flags, 'bad_beams',     20, '');
  flags = append_flag(flags, 'dead_rs',       21, ...
    'Bad SQUID row select, as found during SQUID tuning');
  % Reserve the first 32 bits for use in the focal plan fp_data files. If
  % any new flags are added, take the next line, rename, and move above
  % this comment.
  flags = append_flag(flags, 'fp_reserved22', 22, '');
  flags = append_flag(flags, 'fp_reserved23', 23, '');
  flags = append_flag(flags, 'fp_reserved24', 24, '');
  flags = append_flag(flags, 'fp_reserved25', 25, '');
  flags = append_flag(flags, 'fp_reserved26', 26, '');
  flags = append_flag(flags, 'fp_reserved27', 27, '');
  flags = append_flag(flags, 'fp_reserved28', 28, '');
  flags = append_flag(flags, 'fp_reserved29', 29, '');
  flags = append_flag(flags, 'fp_reserved30', 30, '');
  flags = append_flag(flags, 'fp_reserved31', 31, '');

  % Give channel flags stable bit flags since get_array_info() sets the
  % flag as a way to modify the RGL list.
  flags = append_flag(flags, 'pos_err',       32, '');
  flags = append_flag(flags, 'ukpv_pct',      33, '');
  flags = append_flag(flags, 'ukpvaoverb',    34, '');
  flags = append_flag(flags, 'aboffset_pct',  35, '');
  flags = append_flag(flags, 'sigma_pct',     36, '');
  flags = append_flag(flags, 'ellip',         37, '');
  flags = append_flag(flags, 'sigma_diff_pct',38, '');
  flags = append_flag(flags, 'pc_diff',       39, '');
  flags = append_flag(flags, 'pairdiff_contam',40,'');

  function flags=append_flag(flags, id, bit, comment)
    flags(end+1).id = char(id);
    flags(end).flag = bitset(uint64(0), bit+1);
    flags(end).comment = char(comment);
  end
end
