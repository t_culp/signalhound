function apsfile=get_apsfilename(apsopt,mapname)
% apsfile=get_apsfilename(apsopt,mapname)

%assemble the filename by summing the maps that go into it:
apsfile = '';

if iscell(mapname)

  % We have two maps to do either a cross spectrum or the experiment jack

  for ii=1:length(mapname)
    cmapname=mapname{ii};
    [p,f]=fileparts(mapname{ii});
    mapdirs{ii} = strrep(p,'maps/','');
    apsfile = [apsfile, '_', f];
  end

  %remove leading '_'
  apsfile = apsfile(2:end);

  % append .mat
  apsfile = [apsfile, '.mat'];

  % use unique basenumber to create aps subdir, with x inbetween
  % the sorting will be in the order of appearance:
  [dum,im] = unique(mapdirs);
  mapdirs=mapdirs(sort(im));
  
  apsdir = mapdirs{1};
  for ii=2:length(mapdirs)
    apsdir = [apsdir 'x' mapdirs{ii}];
  end
  apsfile = ['aps/' apsdir '/' apsfile];

else
  
  % Just a single map
  apsfile=strrep(mapname,'maps/','aps/');
  
end

% if freq jack we mod output filename
jackn=strfind(apsfile,'jack')+4;
switch(apsopt.howtojack)
  case 'dim1'
   if(apsfile(jackn)~='0')
     error('fed me a non jack0 map to freq diff');
   end
   apsfile(jackn)='f';
end

% append "pureB" if using pure-B estimator
if(strcmp(apsopt.pure_b,'kendrick'))
  apsfile=strrep(apsfile,'.mat','_pureB.mat');
end

% append "matrix" if using matrix proj
if isfield(apsopt,'purifmat')
  apsfile=strrep(apsfile,'.mat','_matrix.mat');
else
  if isfield(apsopt,'purifmatname') && ~isempty(apsopt.purifmatname)
    apsfile=strrep(apsfile,'.mat','_matrix.mat');
  end
end

% common mask filename extension
if apsopt.commonmask
  apsfile=strrep(apsfile,'.mat','_cm.mat');
end

% append overrx if coadding over rx
if apsopt.coaddrx==1
  apsfile=strrep(apsfile,'.mat','_overrx.mat');
end

% append overfreq if coadding over freq
if apsopt.coaddrx==2
  apsfile=strrep(apsfile,'.mat','_overfreq.mat');
end

% append overall if coadding overall
if (iscell(apsopt.overall) || apsopt.overall) && ~isempty(apsopt.overall)
  apsfile=strrep(apsfile,'.mat','_overall.mat');
end

% append a daughter aps set if requested
if(isfield(apsopt,'daughter'))
  apsfile=strrep(apsfile,'.mat',['_' apsopt.daughter '.mat']);
end

%append bintype if it is not bicep_norm
if(~strcmp(apsopt.bintype,'bicep_norm'))
  apsfile=strrep(apsfile,'.mat',['_' apsopt.bintype '.mat']);
end

% append noisemask to filename
if(isfield(apsopt,'noisemask'))
  apsfile=strrep(apsfile,'.mat','_t.mat');
end

return

