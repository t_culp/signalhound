function plot_map(m,map,convention,cropwindow,nancolor)
% plot_map(m,map)
%
% plot a map with correct aspect ratio and "sphere viewed from the inside"

if(~exist('convention','var') || isempty(convention))
  convention='iau';
end

if(~exist('nancolor','var') || isempty(nancolor))
  nancolor=[1,1,1];
end

if(~exist('cropwindow','var') || isempty(cropwindow))
  cropwindow=false;
elseif(length(cropwindow)~=4)
  cropwindow=[-30,30,-66,-49];
end

if any(cropwindow)
  % crop down to the specified ra/dec
  xl=find(m.x_tic<cropwindow(1),1,'last');
  xh=find(m.x_tic>cropwindow(2),1,'first');
  yl=find(m.y_tic<cropwindow(3),1,'last');
  yh=find(m.y_tic>cropwindow(4),1,'first');
  xr=range(m.x_tic);
  yr=range(m.y_tic);
  m.x_tic=m.x_tic(xl:xh);
  m.y_tic=m.y_tic(yl:yh);
  m.xdos=m.xdos*range(m.x_tic)/xr;
  m.ydos=m.ydos*range(m.y_tic)/yr;
  map_tmp=map(yl:yh,xl:xh);
  clear map
  % taper at the very edge
  taper=0.1*ones(length(m.y_tic),length(m.x_tic));
  taper(3:(length(m.y_tic)-2),3:(length(m.x_tic)-2))=1;
  taper=conv2(taper,ones(5,5),'same');
  taper=taper+flipud(taper); taper=taper+fliplr(taper);
  taper=taper/nanmedian(taper(:));
  map=map_tmp.*taper;
end

imagescnan(m.x_tic,m.y_tic,full(map),'nancolor', nancolor);
if(isfield(m,'xdos'))
  xdos=m.xdos; ydos=m.ydos;
else
  xdos=(m.x_tic(end)-m.x_tic(1))*cosd(mean(m.y_tic));
  ydos=(m.y_tic(end)-m.y_tic(1));
end
set(gca,'PlotBoxAspectRatio',[xdos,ydos,1])
axis xy;

switch convention
 case 'iau'
  % north is up
  set(gca,'XDir','reverse');
 case 'spole'
  % sky as seen by someone standing at South Pole
  set(gca,'YDir','reverse');
end

xlabel('RA (deg)'); ylabel('Dec (deg)');

return
