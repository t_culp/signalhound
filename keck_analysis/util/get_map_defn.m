function m=get_map_defn(type,resmult,proj)
% m=get_map_defn(type,resmult,proj)
%
% Fetch the map definition structure - used by reduc_makesim and
% reduc_makecomap
%
% type = map type
% resmult = resolution multiplier x2, x3. Odd integers will result
% in resolution supersets 

if(~exist('resmult','var'))
  resmult=[];
end
if(~exist('proj','var'))
  proj=[];
end

if(isempty(resmult))
  resmult=1;
end
if(isempty(proj))
  proj='radec';
end
  
if(resmult==0)
  % doesn't make sense and may happen due to old code
  resmult=1;
end

switch lower(deblank(type))
  case 'quad05'

    % There is no point in trying to have each scan fall into a
    % specific row of map pixels - the feed offset angles are not
    % mutiples of 0.02 deg so it won't happen for any but the center feed.
    
    % as agreed with Michael
    m.lx=73.5; m.hx=91.5;
    m.ly=-51.5; m.hy=-42.5;
    m.pixsize=0.02;
    % square pixels half way up
    m.xdos=(m.hx-m.lx)*cos(-47*pi/180);
    m.ydos=m.hy-m.ly;
  
  case 'quad0506'    
    % to accomodate both years 
    m.lx=73.5; m.hx=91.5;
    m.ly=-54; m.hy=-42.5;
    m.pixsize=0.02;
    % square pixels half way up
    m.xdos=(m.hx-m.lx)*cos(-47*pi/180);
    m.ydos=m.hy-m.ly;
  
    % using a map area which accomodates 05 and 06 for 06 data alone
    % doesn't work well - m.xdos is used to set the nominal "plate
    % scale" in the 2D fft later - as such it should be the best
    % possible approx to the truth - the way I have it above with -47
    % is very bad as it is at edge of 06 map area leading to
    % significant distortion of ell scale in final spectra. Would be
    % better if it was the mean([m.ly,m.hy])=48.25 and I should have doen
    % that but better still would be to use the true mid point of the
    % actual 06 area which is more like -50
  
  case 'quad06'    
    % 06 area
    m.lx=73.5; m.hx=91.5;
    m.ly=-54; m.hy=-46;
    m.pixsize=0.02;
    % square pixels half way up
    m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
    m.ydos=m.hy-m.ly;
    
  case 'bicep'
    m.pixsize=0.25;
    switch proj
      case 'radec'
	m.lx=-55; m.hx=55;
	m.ly=-70; m.hy=-45;
	% square pixels half way up
	m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
	m.ydos=m.hy-m.ly;
      otherwise
	m.racen=0;
	m.deccen=-57;
	m.lx=-32; m.hx=32;
	m.ly=-17.5; m.hy=17.5;
	m.xdos=m.hx-m.lx;
	m.ydos=m.hy-m.ly;
    end
 
 case 'bicepext'
   % extend the traditional grid to accomodate BICEP3 2016 obs
   % while maintaining the el at which the pixels are square
   % - idea is to generate a superset of the traditional 'bicep'
   % defn
   % because 2016 B3 obs are shifted down by 2 deg versus
   % traditional el step range the up/down growth is assymetric -
   % we extend up by 3 deg and down by 7
   m.pixsize=0.25;
    switch proj
      case 'radec'
	m.lx=-60; m.hx=60;
	m.ly=-73; m.hy=-38;
	% square pixels in the traditional row
	m.xdos=(m.hx-m.lx)*cos(mean([-70,-45])*pi/180);
	m.ydos=m.hy-m.ly;
      otherwise
        error('not defined');
    end   
 
  case 'bicepext1'
   % extend the traditional grid down in el while maintaining the
   % el at which the pixels are square
   m.pixsize=0.25;
    switch proj
      case 'radec'
	m.lx=-55; m.hx=55;
	m.ly=-70; m.hy=-35;
	% square pixels in the traditional row
	m.xdos=(m.hx-m.lx)*cos(mean([-70,-45])*pi/180);
	m.ydos=m.hy-m.ly;
      otherwise
        error('not defined');
    end
    
  case 'bicepext2'
   % extend the traditional grid down in el while maintaining the
   % el at which the pixels are square
   m.pixsize=0.25;
    switch proj
      case 'radec'
	m.lx=-55; m.hx=55;
	m.ly=-70; m.hy=-40;
	% square pixels in the traditional row
	m.xdos=(m.hx-m.lx)*cos(mean([-70,-45])*pi/180);
	m.ydos=m.hy-m.ly;
      otherwise
        error('not defined');
    end

  % this is for simulation of spider (Odea) 'cleanest' patch 
  case 'spider'
    m.pixsize=0.25;
    switch proj
      case 'radec'
	m.lx=5; m.hx=115;
	m.ly=-70; m.hy=-45;
	% square pixels half way up
	m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
	m.ydos=m.hy-m.ly;
      otherwise
	m.racen=60;
	m.deccen=-57;
	m.lx=-32; m.hx=32;
	m.ly=-17.5; m.hy=17.5;
	m.xdos=m.hx-m.lx;
	m.ydos=m.hy-m.ly;
    end
    
  % Same as type 'bicep', but with 2x finer pixel size.
  % To be used for fitting beam centers
  % and radio pointing model.
  % Not quite the same as resmult=0.5
  case 'bicepfine'
    m.pixsize=0.125;
    switch proj
      case 'radec'
        m.lx=-55; m.hx=55;
        m.ly=-70; m.hy=-45;
        % square pixels half way up
        m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
        m.ydos=m.hy-m.ly;
      otherwise
        m.racen=0;
        m.deccen=-57;
        m.lx=-32; m.hx=32;
        m.ly=-17.5; m.hy=17.5;
        m.xdos=m.hx-m.lx;
        m.ydos=m.hy-m.ly;
    end

  % Same as type 'bicep', but with coarser pixel size.
  % To be used for matrix operations
  % Now redundant given resmult=0.5
  case 'bicepcoarse'
    m.pixsize=0.5;
    switch proj
      case 'radec'
         m.lx=-55; m.hx=55;
         m.ly=-70; m.hy=-45;
         % square pixels half way up
         m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
         m.ydos=m.hy-m.ly;
       otherwise
         m.racen=0;
         m.deccen=-57;
         m.lx=-32; m.hx=32;
         m.ly=-17.5; m.hy=17.5;
         m.xdos=m.hx-m.lx;
         m.ydos=m.hy-m.ly;
    end
         
  % galaxy faint arm
  case 'bicepgalf'
    m.pixsize=0.25;
    switch proj
      case 'radec'
	m.lx=80; m.hx=190;
	m.ly=-70; m.hy=-40;
	% square pixels half way up
	m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
	m.ydos=m.hy-m.ly;
      otherwise
	m.racen=9;
	m.deccen=-55;
	m.ly=-17.5; m.hy=17.5;
	m.xdos=m.hx-m.lx;
	m.ydos=m.hy-m.ly;
    end
 
  % galaxy bright arm
  case 'bicepgalb'
    m.pixsize=0.25;
    switch proj
      case 'radec'
	m.lx=180; m.hx=291;
	m.ly=-70; m.hy=-40;
	% square pixels half way up
	m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
	m.ydos=m.hy-m.ly;
      otherwise
	m.racen=15.712;
	m.deccen=-55;
	m.ly=-17.5; m.hy=17.5;
	m.xdos=m.hx-m.lx;
	m.ydos=m.hy-m.ly;
    end
      
  case 'bicepboomlmc'
    m.pixsize=0.25;
    switch proj
      case 'radec'
	m.lx=12.5; m.hx=122.5;
	m.ly=-75; m.hy=-40;
	% square pixels half way up
	m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
	m.ydos=m.hy-m.ly;
      otherwise
	m.racen=67.5;
	m.deccen=-55;
	m.ly=-17.5; m.hy=17.5;
	m.xdos=m.hx-m.lx;
	m.ydos=m.hy-m.ly;
    end  
    
  case 'bicepgalgap'
    m.pixsize=0.25;
    switch proj
      case 'radec'
	m.lx=140; m.hx=250;
	m.ly=-75; m.hy=-45;
	% square pixels half way up
	m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
	m.ydos=m.hy-m.ly;
      otherwise
	m.racen=200;
	m.deccen=-65;
	m.ly=-17.5; m.hy=17.5;
	m.xdos=m.hx-m.lx;
	m.ydos=m.hy-m.ly;
    end  
    
  case 'bicepfullgal'
    m.pixsize=0.25;
    switch proj
      case 'radec'
        m.lx=80; m.hx=290;
	m.ly=-75; m.hy=-40;
	% square pixels half way up
	m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
	m.ydos=m.hy-m.ly;
      otherwise
	m.racen=150;
	m.deccen=-65;
	m.ly=-17.5; m.hy=17.5;
	m.xdos=m.hx-m.lx;
	m.ydos=m.hy-m.ly;
    end  
    
  case 'biceplastgap'
    m.pixsize=0.25;
    switch proj
      case 'radec'
        m.lx=245; m.hx=355;
        m.ly=-70; m.hy=-44;
        % square pixels half way up
        m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
        m.ydos=m.hy-m.ly;
      otherwise
        m.racen=300;
        m.deccen=-55;
        m.ly=-17.5; m.hy=17.5;
        m.xdos=m.hx-m.lx;
        m.ydos=m.hy-m.ly;
      end  
       
  case 'bicepfull'
    m.pixsize=0.25;
    switch proj
      case 'radec'
	m.lx=0.01; m.hx=360;
	m.ly=-80; m.hy=-40;
	% square pixels half way up
	m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
	m.ydos=m.hy-m.ly;
      otherwise
	m.racen=150;
	m.deccen=-65;
	m.ly=-17.5; m.hy=17.5;
	m.xdos=m.hx-m.lx;
	m.ydos=m.hy-m.ly;
    end  
         
  %########################
  case 'spud'
    m.lx=-50; m.hx=50;
    m.ly=-70; m.hy=-45;
    m.pixsize=0.1;
    % square pixels half way up
    m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
    m.ydos=m.hy-m.ly;
          
  case 'galsurv'
    m.lx=230; m.hx=265;
    m.ly=-56; m.hy=-38;
    m.pixsize=0.02;
    % square pixels half way up
    m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
    m.ydos=m.hy-m.ly;
        
  case {'cena' 'carneb' 'galcen' 'g260.4' 'rcw38' 'mat6a' 'qso' 'j0538-440' 'g326.3' 'g328.4'}
    % this is for leakage simulations - they use the whole map
    [ra,dec]=src_coords(type);
    cosmd=cos(dec*pi/180);
    halfside=2.25; % degrees per half side of box
    m.lx=ra-halfside/cosmd; m.hx=ra+halfside/cosmd;
    m.ly=dec-halfside; m.hy=dec+halfside;
    m.pixsize=0.02;
    % square pixels half way up
    m.xdos=(m.hx-m.lx)*cosmd;
    m.ydos=m.hy-m.ly;
   
  case 'test'
    % this is for testing leakage simulations
    type='rcw38';
    [ra,dec]=src_coords(type);
    cosmd=cos(dec*pi/180);
    halfside=2.25; % degrees per half side of box
    m.lx=ra-halfside/cosmd; m.hx=ra+halfside/cosmd;
    m.ly=dec-halfside; m.hy=dec+halfside;
    m.pixsize=0.02;
    % square pixels half way up
    m.xdos=(m.hx-m.lx)*cosmd;
    m.ydos=m.hy-m.ly;
   
  case 'az'
    m.lx=-180; m.hx=180;
    m.ly=-70; m.hy=-45;
    m.pixsize=0.25;
    % square pixels half way up
    m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
    m.ydos=m.hy-m.ly;
    
  case 'azel'
    m.lx=-180; m.hx=180;
    m.ly=45; m.hy=70;
    m.pixsize=0.25;
    % square pixels half way up
    m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
    m.ydos=m.hy-m.ly;
    
  % test case for using galaxy maps for 2015 keck beamcenters
  case 'bicepgalbfine'
    m.pixsize=0.125;
    switch proj
      case 'radec'
        m.lx=180; m.hx=291;
        m.ly=-70; m.hy=-40;
        % square pixels half way up
        m.xdos=(m.hx-m.lx)*cos(mean([m.ly,m.hy])*pi/180);
        m.ydos=m.hy-m.ly;
      otherwise
        m.racen=15.712;
        m.deccen=-55;
        m.ly=-17.5; m.hy=17.5;
        m.xdos=m.hx-m.lx;
        m.ydos=m.hy-m.ly;
    end
    
  otherwise
    error('unknown map defn type');
   
end

m.nx=round(m.xdos/m.pixsize);
m.ny=round(m.ydos/m.pixsize);

% do it this way to ensure pixel supersets for increased res
m.nx=m.nx*resmult;
m.ny=m.ny*resmult;
m.pixsize=m.pixsize/resmult;

% calc the pixel centers in same way as hfill2 will later
sx=(m.hx-m.lx)/m.nx;
m.x_tic=m.lx+sx/2:sx:m.hx-sx/2;
sy=(m.hy-m.ly)/m.ny;
m.y_tic=m.ly+sy/2:sy:m.hy-sy/2;

m.proj=proj;
return
