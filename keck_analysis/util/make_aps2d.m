function [ad,aps2d,map]=make_aps2d(m,map,pure_b, pad, realimag)
% [ad,aps2d,map]=make_aps2d(m,map,pure_b)
%
% pad=0 : don't pad maps
%    =1 : pad
%
% realimag=1 : plot left side imag(spec).*image(spec), right side real(spec).*real(spec)
%         =0 : plot real(spec.*conj(spec))
%
%
% broken out from from reduc_makeaps2d

disp('calc 2d aps');

if ~exist('pure_b','var')
  pure_b='normal';
end
if ~exist('pad','var')
  pad=1;
end
if ~exist('realimag','var')
  realimag=0;
end

if(realimag == 1)
  display('Plotting imag on left and real on right')
end

% must follow reduc_makeaps to produce plots which represent what
% is going into the actual power spectra...

% pad the maps
if(pad)
  disp('pad')
  tic
  pp=2^(nextpow2(max([m.nx,m.ny]))+1);
  [m,map]=pad_map(m,map,pp);
  toc
end

% calc axis data
ad=calc_ad2([m.xdos,m.ydos],[m.nx,m.ny]);

% scale factor which is normally applied in calcspec
sf=prod(ad.del_u);

for k=1:size(map,2)
  for j=1:size(map,1)
    % do TT
    mw=map(j,k).Tw;
    ft=calc_map_fts(map(j,k),ad,mw,1,pure_b); % calc the weighted modes using map weight mw
    aps2d(j).T=specplot(ft.T,ft.T,sf,realimag);
    
    % do PP (EE, BB, EB)
    mw=map(j,k).Pw;
    ft=calc_map_fts(map(j,k),ad,mw,2,pure_b);
    aps2d(j).Q=specplot(ft.Q,ft.Q,sf,realimag);
    aps2d(j).U=specplot(ft.U,ft.U,sf,realimag);
    aps2d(j).E=specplot(ft.E,ft.E,sf,realimag);
    aps2d(j).B=specplot(ft.B,ft.B,sf,realimag);
    aps2d(j).EB=specplot(ft.E,ft.B,sf,realimag);
    
    % do TP (TE, TB)
    mw=gmean(map(j,k).Tw,map(j,k).Pw);
    ft=calc_map_fts(map(j,k),ad,mw,[1,2],pure_b);
    aps2d(j).TE=specplot(ft.T,ft.E,sf,realimag);
    aps2d(j).TB=specplot(ft.T,ft.B,sf,realimag);
    aps2d(j).TQ=specplot(ft.T,ft.Q,sf,realimag);
    aps2d(j).TU=specplot(ft.T,ft.U,sf,realimag);
  end

  if(0) % 2d APS FOR BAND CROSS SPECTRA NOT CURRENTLY IMPLEMENTED!
  % append the inter row cross cross spectra in additional rows of aps array
  aps_offset = size(map,1);
  n = 1;
  for j=1:size(map,1)-1
    for c=j+1:size(map,1)
      
      % do TT
      % make a common mask as gmean of two masks
      mw=gmean(map(j,k).Tw,map(c,k).Tw); 
      % mult each map by common mask and ft
      ft1=calc_map_fts(map(j,k),ad,mw,1,apsopt.pure_b);
      ft2=calc_map_fts(map(c,k),ad,mw,1,apsopt.pure_b);
      % take the products of the two sets of modes and bin to 1d spectra
      [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,1)]=calcspec(ad,ft1.T,ft2.T,w{j},apsopt.bintype);
      
      % do PP
      mw=gmean(map(j,k).Pw,map(c,k).Pw);
      ft1=calc_map_fts(map(j,k),ad,mw,2,apsopt.pure_b);
      ft2=calc_map_fts(map(c,k),ad,mw,2,apsopt.pure_b);
      [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,3)]=calcspec(ad,ft1.E,ft2.E,w{j},apsopt.bintype);
      [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,4)]=calcspec(ad,ft1.B,ft2.B,w{j},apsopt.bintype);
      [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,6)]=calcspec(ad,ft1.E,ft2.B,w{j},apsopt.bintype);
      [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,9)]=calcspec(ad,ft1.B,ft2.E,w{j},apsopt.bintype);
      
      % do TP
      mw=gmean(map(j,k).Tw,map(c,k).Pw); 
      ft1=calc_map_fts(map(j,k),ad,mw,1,apsopt.pure_b);
      ft2=calc_map_fts(map(c,k),ad,mw,2,apsopt.pure_b);
      [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,2)]=calcspec(ad,ft1.T,ft2.E,w{j},apsopt.bintype);
      [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,5)]=calcspec(ad,ft1.T,ft2.B,w{j},apsopt.bintype);
      
      % do PT
      mw=gmean(map(j,k).Pw,map(c,k).Tw);
      ft1=calc_map_fts(map(j,k),ad,mw,2,apsopt.pure_b);
      ft2=calc_map_fts(map(c,k),ad,mw,1,apsopt.pure_b);
      [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,7)]=calcspec(ad,ft1.E,ft2.T,w{j},apsopt.bintype);
      
      % increment to next slot
      n=n+1;
    end
  end
  end
end

% mult to flat in l(l+1)C_l/2pi
ad.l_r=2*pi*ad.u_r;
sf=ad.l_r.*(ad.l_r+1)/(2*pi);
disp('mult to flat in l(l+1)C_l/2pi');
for j=1:numel(map)
  aps2d(j).Tm=aps2d(j).T.*sf;
  if isfield(map(j),'Q')
    aps2d(j).Qm=aps2d(j).Q.*sf;
    aps2d(j).Um=aps2d(j).U.*sf;
    aps2d(j).Em=aps2d(j).E.*sf;
    aps2d(j).Bm=aps2d(j).B.*sf;
    aps2d(j).TQm=aps2d(j).TQ.*sf;
    aps2d(j).TUm=aps2d(j).TU.*sf;
    aps2d(j).TEm=aps2d(j).TE.*sf;
    aps2d(j).TBm=aps2d(j).TB.*sf;
    aps2d(j).EBm=aps2d(j).EB.*sf;
  end
end

if isfield(aps2d,'ET')
  n=1;
  for j=1:size(map,1)-1
    for c=j+1:size(map,1)
      aps2d(aps_offset+n).Tm=aps2d(aps_offset+n).T.*sf;
      aps2d(aps_offset+n).Qm=aps2d(aps_offset+n).Q.*sf;
      aps2d(aps_offset+n).Um=aps2d(aps_offset+n).U.*sf;
      aps2d(aps_offset+n).Em=aps2d(aps_offset+n).E.*sf;
      aps2d(aps_offset+n).Bm=aps2d(aps_offset+n).B.*sf;
      aps2d(aps_offset+n).TQm=aps2d(aps_offset+n).TQ.*sf;
      aps2d(aps_offset+n).TUm=aps2d(aps_offset+n).TU.*sf;
      aps2d(aps_offset+n).TEm=aps2d(aps_offset+n).TE.*sf;
      aps2d(aps_offset+n).TBm=aps2d(aps_offset+n).TB.*sf;
      aps2d(aps_offset+n).EBm=aps2d(aps_offset+n).EB.*sf;
      aps2d(aps_offset+n).QTm=aps2d(aps_offset+n).QT.*sf;
      aps2d(aps_offset+n).UTm=aps2d(aps_offset+n).UT.*sf;
      aps2d(aps_offset+n).ETm=aps2d(aps_offset+n).ET.*sf;
      aps2d(aps_offset+n).BTm=aps2d(aps_offset+n).BT.*sf;
      aps2d(aps_offset+n).BEm=aps2d(aps_offset+n).BE.*sf;
      n=n+1;
    end
  end
end

return


%%%%%%%%%%%%%%%%%%%%
function spec_out=specplot(spec_in_A,spec_in_B,sf,realimag)

if(~realimag)
  spec_out=sf*real(spec_in_A.*conj(spec_in_B));
else  
  width=size(spec_in_A,2);
  imagspec=sf*imag(spec_in_A).*imag(spec_in_B);
  realspec=sf*real(spec_in_A).*real(spec_in_B);
  spec_out(:,1:floor(width/2))=imagspec(:,1:floor(width/2));
  spec_out(:,floor(width/2)+1:width)=realspec(:,floor(width/2)+1:width);
end

return
