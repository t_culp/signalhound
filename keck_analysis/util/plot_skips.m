% plot_skips('20130509C01_dk293')
% or plot_skips('data/real/20130509C01_dk293_tod.mat')
% or plot_skips(d)
%
% Tool for the reduc czar to examine skips, drops, merge failures, etc.
function plot_skips(tag)

if ischar(tag)
  if exist(tag,'file')
    fname=tag;
    [fdir basename fext]=fileparts(fname);
    tag=strrep(basename,'_tod','');
  else
    fname=fullfile('data','real',tag(1:6),[tag '_tod.mat']);
  end
  load(fname);
else
  tag='Unknown tag';
end

d.thrs = (d.t-d.t(1))*24;

% Loop over el nods, field scan block
for i=1:3
  switch(i)
    case 1, sf=en.sf(1); ef=en.ef(1); part='Leading elnod';
    case 2, sf=fsb.sf(1); ef=fsb.ef(1); part='Field scans';
    case 3, sf=en.sf(2); ef=en.ef(2); part='Trailing elnod';
  end

  tsmooth=d.thrs(sf) + (d.thrs(ef)-d.thrs(sf))*(0:(ef-sf))/(ef-sf);

  S=find_skips(d,sf,ef);
  [j,n]=print_skips(S,tsmooth,d.t(sf:ef),tag,part);
  % plot_finder(d,sf,ef,j,n,tag,part);
  % plot_details(d,sf,ef,j,n,tag,part);

end

return

%%%%
% You can edit this function to add / modify the types of skips that we look for
function S=find_skips(d,sf,ef)
S=[];

cskip = d.antenna0.syncBox.sampleNumber(sf:ef)==0 & d.t(sf:ef)~=0;
S=add_skips(S,cskip,'Skipped samples');

cfilt = d.antenna0.syncBox.sampleNumber(sf:ef)==0 & all(d.mce0.syncBox.sampleNumber(sf:ef,:)==0,2);
S=add_skips(S,cfilt,'Filter failures');

fidx=double(d.array.filter.idx(sf:ef));
cmediator = [0;diff(fidx)]>1 | ([0;diff(fidx)]<0 & fidx>0);
S=add_skips(S,cmediator,'Mediator dropped frames');

for i=1:size(d.mce0.syncBox.sampleNumber,2)
  cmcedrop{i} = d.mce0.syncBox.sampleNumber(sf:ef,i)==0 & ~cfilt;
  S=add_skips(S,cmcedrop{i},['Drop in mce' num2str(i-1)]);
end

for i=1:size(d.mce0.syncBox.sync_status,2)
  stat=nrep(d.mce0.syncBox.sync_status,20);
  csync{i} = stat(sf:ef,i)>4;
  S=add_skips(S,csync{i},['Bad sync in mce' num2str(i-1)]);
end

return

%%%%
% Print text output about the identified skips
function [j,n]=print_skips(S,t,utc,tag, part)
j0=zeros(0,1);
j1=zeros(0,1);
i=zeros(0,1);
for k=1:length(S)
  j0=[j0;S(k).j0];
  j1=[j1;S(k).j1];
  i=[i;k*ones(size(S(k).j0))];
end
[j0,idx]=sort(j0);
j1=j1(idx);
i=i(idx);
if length(j0)>0
  disp(['In tag ' tag ', ' part ':']);
end
onskip=0;
lastskip=0;
j=zeros(1,0);
n=zeros(1,0);
for k=1:length(j0)
  if(j0(k)>lastskip)
    onskip=onskip+1;
    lastskip=j0(k);
    n=[n,onskip];
    j=[j,lastskip];
  end
  [s1,s2]=fmtstr(j0(k),j1(k));
  tmpj=j0(k);
  while utc(tmpj)==0
    tmpj=tmpj-1;
    if tmpj==0
      tmpj=find(utc~=0,1);
    end
  end
  [YY MM DD HH II SS]=mjd2date(utc(tmpj));
  dn=datenum(YY,MM,DD,HH,II,SS);
  disp(['    (' num2str(onskip) ') ' S(i(k)).name ': ' s1 ' at ' s2 ' (t=' num2str(t(j0(k))) ' hrs; ' datestr(dn,'yyyy-mmm-dd:HH:MM:SS') ')']);
end
if length(j0)>0
  disp(' ');
end

return

%%%%
% Make a zoomed-out finder plot
function plot_finder(d,sf,ef,j,n,tag,part)
feat=nrep(d.array.frame.features,20);
feat=nanmedian(feat(sf:ef));
iselnod=(feat==9);
if iselnod
  pos=d.pointing.hor.el(sf:ef);
else
  pos=d.pointing.hor.az(sf:ef);
end
tsmooth=d.thrs(sf)+(d.thrs(ef)-d.thrs(sf))/(ef-sf)*(0:(ef-sf));
% tsmooth=sf:ef;
figure;
plot(tsmooth,pos,'r-');
grid on;
hold on;
axax=axis;
ylim([axax(3) axax(4)+(axax(4)-axax(3))*0.25]);
axax=axis;
for i=1:length(j)
  ypos=axax(3)+(axax(4)-axax(3))*(0.9-0.05*mod(n(i)-1,4));
  text(tsmooth(j(i)),ypos,[num2str(n(i))],'horizontalalignment','center','verticalalignment','bottom');
end
title([strrep(tag,'_','\_') ', ' part]);
% xlabel('sample number');
xlabel('time / hr');
return

%%%%
% Make a zoomed-in plot for each skip
function plot_details(d,sf,ef,j,n,tag,part)
feat=nrep(d.array.frame.features,20);
feat=nanmedian(feat(sf:ef));
iselnod=(feat==9);
if iselnod
  pos=d.pointing.hor.el(sf:ef);
  labl='el / deg';
else
  pos=d.pointing.hor.az(sf:ef);
  labl='az / deg';
end
tsmooth=d.thrs(sf)+(d.thrs(ef)-d.thrs(sf))/(ef-sf)*(0:(ef-sf));
nmce=size(d.mce0.syncBox.sampleNumber,2);
jdx=1:(ef-sf);
jdx2=1:length(d.t);
for i=1:length(j)
  figure;
  cc=abs(jdx-j(i))<100;
  cc2=abs(jdx2-sf-j(i))<100;
  strip_plot_helper(tsmooth(cc),pos(cc),1,nmce+3,labl);
  title([strrep(tag,'_','\_') ', ' part ' skip #' num2str(i)]);
  strip_plot_helper(tsmooth(cc),d.antenna0.time.utcfast(cc2),2,nmce+3,'UTC');
  strip_plot_helper(tsmooth(cc),d.antenna0.syncBox.sampleNumber(cc2),3,nmce+3,'ant sync');
  for rx=1:nmce
    strip_plot_helper(tsmooth(cc),d.mce0.syncBox.sampleNumber(cc2,rx),3+rx,nmce+3,['mce' num2str(rx-1) ' sync']);
  end
end
return

%%%%%
function strip_plot_helper(x,y,i,n,labl)
axstrip(n,i);
plot(x,y,'r-');
xlim([min(x) max(x)]);
yl=[min(y(y~=0)) max(y(y~=0))];
if any(y==0)
  if yl(1)>0
    yl(1)=2*yl(1)-yl(2);
  else
    yl(2)=2*yl(2)-yl(1);
  end
end
ylim(yl);
text(0.1,0.9,labl,'units','normalized');
set(gca,'yticklabel','');
if i<n
  set(gca,'xticklabel','');
else
  xlabel('time / hr');
end
return

%%%%%
% Remaining functions below are nuts & bolts utilities
%

%%%%
function S=add_skips(S,c,txt)
i=length(S)+1;
S(i).name=txt;
[j0,j1]=cut2idx(c);
S(i).j0=j0;
S(i).j1=j1;
for j=1:length(j0)
  [s1,s2]=fmtstr(j0(j),j1(j));
  % disp([txt ': ' s1 ' at ' s2]);
end
return

%%%%
function [j0,j1]=cut2idx(c)
j0 = find([c(1);diff(c)>0]);
j1 = find([diff(c)<0;c(end)]);
return

%%%%
function [s1,s2]=fmtstr(j0,j1)

if mod(j0,20)==1 & mod(j1,20)==0
  if j1-j0+1==20
    s1=['Single frame'];
  else
    s1=['Run of ' num2str((j1-j0+1)/20) ' frames'];
  end
else
  if j1==j0
    s1=['Single sample'];
  else
    s1=['Run of ' num2str(j1-j0+1) ' samples'];
  end
end
s2=num2str(j1);

return

%%%%
function y=nrep(x,n)
s=size(x);
s(1)=s(1)*n;
y=zeros(s);
for i=1:n
  y(i:n:end,:,:)=x;
end
return

%%%%
function ax=axstrip(m,i)
% Fractional border at LRBT
b=[0.1 0.06 0.1 0.07];
ax=axes('position',[b(1) b(3)+(1-b(3)-b(4))/m*(m-i) 1-b(1)-b(2) (1-b(3)-b(4))/m]);
return
