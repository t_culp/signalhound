function map=edge_strip_varmaps(m,map,p,ind)
% map=edge_strip_varmaps(m,map,ind)
%
% The left and right edge regions of the maps contain poly filter
% induced step artifacts due to pixel tile overlap - strip it off by
% setting them to NaN

% find the array width at each freq
r(1)=max(p.ra_off_dos(ind.rgl100a))-min(p.ra_off_dos(ind.rgl100a));
r(2)=max(p.ra_off_dos(ind.rgl150a))-min(p.ra_off_dos(ind.rgl150a));

% for each freq
for i=1:size(map,1)
  
  % strip the maps
  for j=1:size(map,2)
    map(i,j).Tvar=strip_map(m,map(i,j).Tvar,r(i));
    map(i,j).Qvar=strip_map(m,map(i,j).Qvar,r(i));
    map(i,j).Uvar=strip_map(m,map(i,j).Uvar,r(i));
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function map=strip_map(m,map,r)

% scale the range to dos
r=r./cosd(m.y_tic');

[xg,yg]=meshgrid(m.x_tic,m.y_tic);

% mask the x grid to match the map
xg(isnan(map))=NaN;

% find the lowest surviving number in each row
m=min(xg,[],2);

% add the array width and expand out to full grid 
m=repmat(m+r,[1,size(map,2)]);

% mask vals in edge region to NaN
map(xg<m)=NaN;

% repeat for high edge
m=max(xg,[],2);
m=repmat(m-r,[1,size(map,2)]);
map(xg>m)=NaN;

return
