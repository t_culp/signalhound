function expt=get_experiment_name
% expt=get_experiment_name
%
% Returns the experiment name, e.g. bicep, bicep2, keck

% Read in from a file in aux_data
tmp=textread('aux_data/experiment_name.txt','%s');

% Check for empty result
if length(tmp)==0
  expt='';
else

  % Skip any comment lines
  for i=1:length(tmp)
    expt=strtrim(tmp{i});
    if length(expt)>0 && expt(1)~='%'
      break
    end
  end
end

return
