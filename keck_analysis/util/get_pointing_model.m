function pmi=get_pointing_model(mjd,mirror,d,fs)
% pmi=interp_pointing_model(mjd,mirror,d,fs)
%
% interpolate the pointing model to mjd
%
% e.g. pm=get_pointing_model(mean(d.t(d.t~=0)))

% fetch the pointing model data
pm=ParameterRead('aux_data/pointingmodels/pointingmodel_complete_v1.csv');

if ~exist('mirror','var')
  mirror=0;
end

% get field names
fn=fieldnames(pm);

% if mirror is present, just get the dummy pointing model:
if mirror
  if ~exist('fs','var')
    pmi.az_zero=double(d.antenna0.tracker.encoder_off(1,1))/3.6e6;
    pmi.el_zero=double(d.antenna0.tracker.encoder_off(1,2))/3.6e6;
  else
    pmi.az_zero=double(d.antenna0.tracker.encoder_off(fs.s(1),1))/3.6e6;
    pmi.el_zero=double(d.antenna0.tracker.encoder_off(fs.s(1),2))/3.6e6;
  end
  pmi.el_tilt=0;
  pmi.az_tilt_lat=0;
  pmi.az_tilt_ha=0;
  pmi.bracket=0;
  return
end

% warn user if we don't have bracketing star-pointing schedules
if(mjd>max(pm.mjd) | mjd<min(pm.mjd))
  disp(['warning: using a date not bracketed by a boresight pointing' ...
        ' model measurement. re-run tod when model is updated.'])
  pmi.bracket=0;
else
  pmi.bracket=1;
end

% for each numerical field perform interp to required mjd
pmi=[];
for i=1:length(fn)
  if(isreal(getfield(pm,fn{i})))
    allvals=getfield(pm,fn{i});
    if(mjd>max(pm.mjd) | mjd<min(pm.mjd))
      if mjd>max(pm.mjd)
        val=allvals(end);
      else
        val=allvals(1);
      end      
    else
      val=interp1(pm.mjd,allvals,mjd);
    end
    pmi=setfield(pmi,fn{i},val);
  end
end

return

