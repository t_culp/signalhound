function map=insert_common_mask(m,map,opt,mapsel)
% map=insert_common_mask(m,map,opt,nmap)
%
% Copy the Tw and Pw masks along the first dimension of map - force
% the auto and cross map spectra to be computed on the same sky mask -
% when the product spectra are to be jointly considered (e.g. is a
% multispectral fit) then this is probably what we want. Note that
% it's not a total solution - because of filtering the modal
% coverage will still not be exactly the same.

% don't do this for jack maps - for some like tile the B2 and Keck
% coverage is radically different - don't want to eat this
% difference simply to make the poorly motivated jackknife cross
% spectrum better behaved
if(size(map,2)>1)
  % have this a warning instead of an error since the delta maps stacks
  % use this dimension.
  warning('commonmask for jack~=0 a funny thing to do');
end
% number of maps
n=size(map,1);

% of no selection of maps is given to use for the gmean,
% use all:
if ~exist('mapsel','var') || isempty(mapsel)
  mapsel = 1:n;
end


if ~isstr(opt)
  
  % Copy specified masks to all
  for i=1:n
    map(i).Tw=map(opt).Tw;
    map(i).Pw=map(opt).Pw;
  end
  
else
  switch opt
   case 'gmean'
    % take product of masks
    Tw = map(mapsel(1)).Tw;
    Pw = map(mapsel(1)).Pw;
    for i=2:length(mapsel)
      Tw=Tw.*map(mapsel(i)).Tw;
      Pw=Pw.*map(mapsel(i)).Pw;
    end
  
    % take the n'th root
    Tw=Tw.^(1/length(mapsel));
    Pw=Pw.^(1/length(mapsel));
  
    % copy to all
    for i=1:n
      map(i).Tw=Tw;
      map(i).Pw=Pw;
    end
  end
end

return
