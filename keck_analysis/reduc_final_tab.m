function reduc_final_tab(rf,lab)
% reduc_final_tab(rf)
%
% Make table of EE and BB spectra for publication.
%
%reduc_final_tab('sim005_filtp3_weight2_jack1.mat')
  
  
 chibins=get_chibins(15);
  
  %read combined spectra
 rf_c=strrep(rf,'.mat','_comb.mat');
 load(sprintf('final/%s',rf_c));
 r_c=r;
 
 % calc cov and derr
 r_c=get_bpcov(r_c);
 
 %read individual spectra
 load(sprintf('final/%s',rf));
 
 % calc cov and derr
 r=get_bpcov(r);
 
 % print the table straight
disp ('TT bandpowers and 1sigma error bars')
 disp ('ell   ----   100GHz   ----   150GHz   ----    cross  ----   combined')
 for i=1:size(chibins{4},2)
   b=chibins{3}(i);
   disp(sprintf('%3.1f \t%7.2f\t%7.2f \t%7.2f\t%7.2f \t%7.2f\t%7.2f \t%7.2f\t%7.2f',...
                r(1).l(b),r(1).real(b,1),r(1).derr(b,1),...
                r(2).real(b,1),r(2).derr(b,1),...
                r(3).real(b,1),r(3).derr(b,1),...
                r_c.real(b,1),r_c.derr(b,1)));
   
 end
 
 disp ('TE bandpowers and 1sigma error bars')
 disp ('ell   ----   100GHz   ----   150GHz   ----    cross  ----   combined')
 for i=1:size(chibins{4},2)
   b=chibins{3}(i);
   disp(sprintf('%3.1f \t%6.2f\t%6.2f\t%7.2f\t%7.2f\t%7.2f\t%7.2f\t%7.2f\t%7.2f',...
                r(1).l(b),r(1).real(b,2),r(1).derr(b,2),...
                r(2).real(b,2),r(2).derr(b,2),...
                r(3).real(b,2),r(3).derr(b,2),...
                r_c.real(b,2),r_c.derr(b,2)));
   
 end
 
 disp ('EE bandpowers and 1sigma error bars')
 disp ('ell   ----   100GHz   ----   150GHz   ----    cross  ----   combined')
 for i=1:size(chibins{4},2)
   b=chibins{3}(i);
   disp(sprintf('%3.1f \t%6.3f\t%6.3f \t %6.3f\t%6.3f \t %6.3f\t%6.3f \t %6.3f\t%6.3f',...
                r(1).l(b),r(1).real(b,3),r(1).derr(b,3),...
                r(2).real(b,3),r(2).derr(b,3),...
                r(3).real(b,3),r(3).derr(b,3),...
                r_c.real(b,3),r_c.derr(b,3)));
   
 end
 
 disp ('BB bandpowers and 1sigma error bars')
 disp ('ell   ----   100GHz   ----   150GHz   ----    cross  ----   combined')
 for i=1:size(chibins{4},2)
   b=chibins{4}(i);
   disp(sprintf('%3.1f \t%6.3f\t%6.3f \t %6.3f\t%6.3f \t %6.3f\t%6.3f \t %6.3f\t%6.3f',...
                r(1).l(b),r(1).real(b,4),r(1).derr(b,4),...
                r(2).real(b,4),r(2).derr(b,4),...
                r(3).real(b,4),r(3).derr(b,4),...
                r_c.real(b,4),r_c.derr(b,4)));
   
 end

 return