function [mapu,mapd,tag]=maps_read_stack(tagz,filttype)
% [mapu,mapd,tag]=maps_read_stack(ltags,filttype)
%
% Read in a series of 1 day files and save back out
% a stack of maps
%
% Updated by TLC 03/05/06 to allow use of new CLP maps names. Also
% removed stag saving argument as its probably obsolete.
%
% Tested it; seems to work
%
% OLD:
%
% maps_read_stack({'050517','050518'},'all')
% [mapu,mapd,tag]=maps_read_stack(ltags,stag)
%
% NEW
%
% maps_read_stack({'050517','050518'},0)
% [mapu,mapd,tag]=maps_read_stack(tagz,filttype)

% convert filter polynomial to a string
fpstr=int2str(filttype);

% get p,ind from last map in stack
[p,ind]=get_pind(tagz{end},fpstr);

% read in all the maps
ctag=[];
for z=1:length(tagz)
  tagz{z}
  
  load(['data/',tagz{z},'_maps_filtp',fpstr]);
  
  amapu(z,:,:)=mapu;
  amapd(z,:,:)=mapd;
  ctag=[ctag,'+',tagz{z}];

end

mapu=amapu;
mapd=amapd;

if(length(tagz)>8)
  tag=sprintf('%s through %s',tagz{1},tagz{end});
else
  tag=ctag(2:end);
end

clear z

% need a stag argument in command line to get this 
% working again
%if(exist('stag'))
%  save(['data/cmb_',stag],'mapu','mapd','tag');
%end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [p,ind]=get_pind(tag,fpstr)
load(['data/',tag,'_maps_filtp',fpstr]);
return
