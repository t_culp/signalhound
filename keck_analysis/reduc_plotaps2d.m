function reduc_plotaps2d(apsname)
% reduc_plotaps2d(apsname)
%
% Plot 2d aps made from single map or ensemble average over a set of
% maps
%
% Play with normalizing over annular bins to investigate which regions
% of f plane have extra noise and cook up weighting schemes to make
% better 1d spectra
%
% e.g.
% reduc_plotaps2d('sim13xxx4_filtn_weight2_jack0')

load(sprintf('aps2d/%s',apsname));

figure(1); clf
setwinsize(gcf,800,1000);
plot_aps2d(1,aps2d(1),mapopt,'100GHz');
plot_aps2d(2,aps2d(2),mapopt,'150GHz');
plot_aps2d(3,aps2d(3),mapopt,'cross');
%gtitle(strrep(apsname,'_','\_'));

% normalize each annulus to so that median is 1

% fine bins
[be,n]=get_bins('bicep_norm');
for j=1:numel(aps2d)
  for k=1:size(aps2d(j).Cs_l,3)
    x=aps2d(j).Cs_l(:,:,k);
              
    % Cross spectra have noise close to zero potentially with local
    % positive or negative spikes
    % For the purposes of screening them out noisy modes are those
    % which have non zero values
    % Therefore take abs val
    x=abs(x);

    y=zeros(size(x));
    for i=1:length(be)-1
      % find modes in annulus
      ind=aps2d(j).ad.l_r>be(i)&aps2d(j).ad.l_r<be(i+1);
      
      % divide modes in bin by median and enter into normalized array
      y(ind)=x(ind)/median(x(ind));
    end
    aps2d(j).mednorm(:,:,k)=y;
  end
end

figure(2); clf
setwinsize(gcf,800,1000);
plot_mednorm(1,aps2d(1),mapopt,'100GHz');
plot_mednorm(2,aps2d(2),mapopt,'150GHz');
plot_mednorm(3,aps2d(3),mapopt,'cross');
%gtitle(strrep(apsname,'_','\_'));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_mednorm(n,aps2d,mapopt,freq)

xtic=aps2d(1).ad.u_val{1}*2*pi;
ytic=aps2d(1).ad.u_val{2}*2*pi;

lab={'TT','TE','EE','BB','TB','EB'};
for i=1:6
  subplot(6,3,(i-1)*3+n)
  imagesc(xtic,ytic,aps2d.mednorm(:,:,i));  
  axis image
  xlim([-2500,2500]); ylim([-2500,2500]);  ylabel(lab{i});
  caxis([0,2]);
  bullseye;
  colorbar
  if(i==1)
    title(freq);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_aps2d(n,aps2d,mapopt,freq)

xtic=aps2d.ad.u_val{1}*2*pi;
ytic=aps2d.ad.u_val{2}*2*pi;

lab={'TT','TE','EE','BB','TB','EB'};
for i=1:6
  subplot(6,3,(i-1)*3+n)
  imagesc(xtic,ytic,aps2d.Cs_l(:,:,i));  
  axis image
  xlim([-2500,2500]); ylim([-2500,2500]);  ylabel(lab{i});
  caxis([-200,200]);
  bullseye;
  colorbar
  if(i==1)
    title(freq);
  end
end

return

%%%%%%%%%%%%%%%%%%%
function bullseye()
xl=xlim;
hold on
[x,y]=circle(0,0,[10,500:500:xl(2)],100,1);
plot(x,y,'r');
%line(xl,[0,0],'Color','r');
%line([0,0],xl,'Color','r');
hold off
return
