function reduc_applycal(tag)
% reduc_applycal(tag)
%
% read in _tod_uncal
%
% apply rel gain cal using elnod val at each  scan set.
% 
% write back out as _tod
%
% No cuts are made at this point - defer until mapping stage
%
% Assumption is made that elnods come in pairs spanning periods of
% data which need to be cal'd and that first elnod in file is the
% first of a pair
% 
% e.g:
% reduc_applycal('20110308C03_dk068')
%
% Beginning 2014, median of elnod gains is calculated in nominal power units

% Convert to nominal units of power for 2014 and beyond
if str2num(tag(1:4))>=2014
  pu = true;
  bias0 = get_bias0(tag(1:4));
else
  pu = false;
end

% read data  
matfile=['data/real/',tag(1:6),'/',tag,'_tod_uncal'];
disp(sprintf('loading matfile %s',matfile))
load(matfile);

% Get array info
[p,ind]=get_array_info(tag);

% we are using consistency of elnod before/after field scan blocks to
% select good channels. Therefore apply cal from start of first elnod
% to end of second elnod of each block - this way we see the effect
% on the elnods themselves which is useful when plotting
cb.sf=en.sf(1:2:end); cb.ef=en.ef(2:2:end);
cb.t=en.t(1:2:end);
% also add fs.s for use in cal_scans when converting to power units
cb.s=fs.s;

% if last scanset interrupted we have start en but no end - any such
% data cannot be cal'd and must be NaN'd
if(length(cb.sf)>length(cb.ef))
  d.mce0.data.fb(cb.sf(end):end,:)=NaN;
  cb.sf=cb.sf(1:end-1);
end

if(~isempty(cb.sf))
  
  % calibrate frequencies separately.  Now allows arbitrary frequencies
  freq=unique(p.band(ind.la))';
  for k=freq
    %set up field names
    fstr = sprintf('%03d', k);
    rglf=['rgl' fstr];
    glf=['gl' fstr];
    % apply elnod cal to equalize gains between channels at each time step
    % cal_scans has been modified to allow for conversion of elnod gains 
    % to nominal power units.
    [d.mce0.data.fb, en]=cal_scans(d.mce0.data.fb,cb,en,ind.(glf),...
                                   ind.(rglf),tag(1:4),lc,bias0,p,...
                                   ind,d.mce0.tes.bias);
  end
end


% save calibrated data back out
% and set permissions appropriately on TOD file
matfile=['data/real/',tag(1:6),'/',tag,'_tod'];


Ndet=size(d.mce0.data.fb,2);
if Ndet<1000
  % BICEP2 save as the superior v7
  ver='-v7';
else
  % Keck save as the lame v7.3, necessary for variables larger than 2 GB
  ver='-v7.3';
end

if ~pu  % then save as before
  save(matfile,'d','fs','en','lc','fsb','dg','ds','pm',ver);  
else  % otherwise save additional variables
  save(matfile,'d','fs','en','lc','fsb','dg','ds','pm','bias0',ver);
end

setpermissions([matfile '.mat']);

return

