function reduc_findtc(tag,method)
% reduc_findtc(tag,method)
%
% Large changes to fit double tc model to all speeds at once
%
% e.g.
% reduc_findtc('050714','deconvdata')
% reduc_findtc('060909','deconvdata')

% get full day's data!
%d=read_run(tag,'ac');
%save(sprintf('reduc_findtc_%s',tag),'d');

% load pre-extracted to speed up
load(sprintf('reduc_findtc_%s',tag));

% get nominal array info
[p,ind]=get_array_info(tag);

% determine az/el and az/el offset of center pixel at each time step
[d.azoff,d.eloff,d.az,d.el]=arcvar_to_azel(d);
% scale azoff to deg-on-sky (for center pixel)
d.azoffsky=d.azoff.*cos(d.el*pi/180);

% find each rowcal scan (whole thing with multiple blips)
rctc=find_blk(d.frame.features==2^2+2^0);

% measure the timeconstants
[tc1,tc2,tcs]=measure_timeconst(d,p,rctc,ind,method,tag);

% save raw results
%save(sprintf('timeconst/%s_%s_dual',tag(1:6),method),'tc1','tc2','tcs','tag','ind','p');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [tc1,tc2,tcs]=measure_timeconst(d,p,rctc,ind,method,tag)

% do the scan cleaning up front one scan set at a time - then do
% the fitting over whole groups of scans simultaneously
for i=1:length(rctc.s)

  i
  
  % what is the deck angle for this scan set
  dk=round(d.tracker.actual(rctc.s(i),3));

  % rotate array to this deck angle
  q=rotarray(p,dk);
    
  s=rctc.sf(i); e=rctc.ef(i);
    
  eloff=mean(d.eloff(s:e));
  
  x=d.azoffsky(s:e); y=double(d.lockin.adcData(s:e,:));
      
  % remove linear in time from each channel
  % (helps to clean up ramping channels)
  clear X
  X(:,2)=[1:length(x)]'; X(:,1)=1;
  b=lscov(X,y);
  y=y-X*b;

  % for each channel
  for j=ind.gl
  
    % if this channel nearly hits source
    if(abs(eloff-q.dec_off(j))<0.05)
      
      % make template from chan at same freq in other row
      k=intersect(ind.gl,find(q.frequency==q.frequency(j)&abs(eloff-q.dec_off)>0.05));
      clear tp
      tp(:,2)=mean(y(:,k),2); tp(:,1)=1;
      yp=y(:,j);
      
      % regress template in non-blip region
      k=abs(x+q.ra_off_dos(j))>0.2;
      b=regress(yp(k),tp(k,:));
      % subtract from full range
      y(:,j)=yp-tp*b;
    end
  end
  
  d.lockin.adcData(s:e,:)=y;
end

% load jamie lab dual tc values
jtc=ParameterRead('timeconst/041229_labtc_fit.txt');

% swap so short one always first
i=jtc.tau2a>jtc.tau2b;
jtc.ratio(i)=1./jtc.ratio(i);
t=jtc.tau2b(i);
jtc.tau2b(i)=jtc.tau2a(i);
jtc.tau2a(i)=t;

% conv from Jamie r to weight w
w=1./(1+jtc.ratio);
labtc=[jtc.tau2a;jtc.tau2b;w];

% don't have lab values for 2006 run
if(strcmp(tag,'060909'))
  labtc=NaN*labtc;
end

tc1=[]; tc2=[]; tcs=[]

% for each dk angle
%for r=1:2
for r=1
  % for each of 7 scans in set
  for i=1:7
    
    % find this one and the other 4 speeds
    k=(r-1)*70+[i+7:7:35];
    % double up to both reps at this deck angle
    k=[k,k+35]
    a=structcut(rctc,k);
    mapind=make_mapind(d,a);
    
    % what is the deck angle for this scan set
    dk=round(d.tracker.actual(a.s(1),3));

    % rotate array to this deck angle
    q=rotarray(p,dk);
    
    eloff=mean(d.eloff(mapind));

    x=d.azoffsky(mapind);
  
    % for each channel
    %for j=ind.gl
    %for j=setdiff(ind.gl,ind.rgl)
    for j=3  % 150-7-B 14, 100-8-A 53, 150-19-B 38, 100-9-A 55
      
      % if this channel nearly hits source
      if(abs(eloff-q.dec_off(j))<0.05)
	y=double(d.lockin.adcData(mapind,j));
      
	switch method
	  case 'filtmodel'
	    [tc1(r,j,:),tc2(r,j,:),tcs(r,j,:)]=filtmodel(x,y,q,j,labtc);
	  case 'deconvdata'
	    [tc1(r,j,:),tc2(r,j,:),tcs(r,j,:)]=deconv_data(x,y,q,j,labtc);
	end
	
	mkgif(sprintf('timeconst_plots/%s/%02d_%01d.gif',tag,j,r));
      end
    end
  end
end

% make sure tc full size
%tc1(2,max(ind.l),1)=0;
%tc2(2,max(ind.l),1)=0;
%tcs(2,max(ind.l),1)=0;

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [tc1,tc2,tcs]=deconv_data(x,y,p,j,labtc)

% minimize difference between forward and backward scans by adjusting
% deconvolution time constant

% find expected position of blip
xp=x+p.ra_off_dos(j);

% scan through to find blip start/end pointers
% (this is similar to find_scans)
bw=0.35;
%bw=0.7;
n=0; i=0;
while(i<length(xp))
  i=i+1;
  if(xp(i)>-bw) % found start forward
    n=n+1;
    bs.s(n,1)=i;
    
    while(xp(i)<0) % look for center forward
      i=i+1;
    end
    bs.c(n,1)=i;
    
    while(xp(i)<bw) % look for end forward
      i=i+1;
    end
    bs.e(n,1)=i-1;
    
    n=n+1;
    while(xp(i)>bw) % look for start backward
      i=i+1;
    end
    bs.s(n,1)=i;
    
    while(xp(i)>0) % look for center backward
      i=i+1;
    end
    bs.c(n,1)=i;
    
    while(xp(i)>-bw) % look for end backward
      i=i+1;
    end
    bs.e(n,1)=i-1;
    
    % kludge to prevent finding false re-start in presence of jitter
    i=i+300;
    
  end
end

% assemble index arrays allowing one to pull out for/back nominally
% matching scan data

bi.f=[]; bi.b=[];
for i=1:2:length(bs.s)
  % use the forward blip length
  l=2*round((bs.e(i)-bs.s(i))/2);

  % get the forward blip
  ind=(bs.c(i)-l/2):(bs.c(i)+l/2);
  bi.f(end+1:end+1+l)=ind;
  
  % get the backward blip
  ind=(bs.c(i+1)-l/2):(bs.c(i+1)+l/2);
  bi.b(end+1:end+l+1)=fliplr(ind);
end

tc2=1;

if(1)
  % adjust deconv timeconst to get best match between for/back blips
  tc1=0.040; lb=1e-9; ub=1.000;
  [tc1,fom,dum,ex]=lsqnonlin(@(x) timeconst_fitfunc(x,bi,y),tc1,lb,ub);
  %freepar.free=[1]; freepar.lb=lb; freepar.ub=ub;
  %[tc1,dum,fom,ex]=matmin('timeconst_fitfunc_matmin',tc1(1),freepar,[],bi,y);
  tc1
  
  % append the fom and exit flag
  tc1=[tc1;fom;ex];
  
  % fit again to double model
  % upper bound on 2nd tc is tricky - had 1sec when ran to get
  % timeconst used for xmas 06 re-crunch of 2005 data. 150-7-B wants
  % larger val for 1st deck angle...
  lb=[1e-6,0.040,0.03];
  ub=[0.200,2,1.00];
  [tc2,fom,dum,ex]=lsqnonlin(@(x) timeconst_fitfunc(x,bi,y),[tc1(1);0.2;0.5],lb,ub);
  %freepar.free=[1,1,1]; freepar.lb=lb; freepar.ub=ub;
  %[tc2,dum,fom,ex]=matmin('timeconst_fitfunc_matmin',[tc1(1);0.2;0.5],freepar,[],bi,y);
  tc2
  
  % append the fom and exit flag
  tc2=[tc2;fom;ex];
end

if(~isnan(labtc(1,j)))
  % single par fit scaling lab tc values
  lb=0.2; ub=3;
  [par,fom,dum,ex]=lsqnonlin(@(x) timeconst_fitfunc(x,bi,y,labtc(:,j)),1,lb,ub);
  %freepar.free=[1]; freepar.lb=lb; freepar.ub=ub;
  %[par,dum,fom,ex]=matmin('timeconst_fitfunc_matmin',1,freepar,[],bi,y,labtc(:,j));
  tcs=labtc(:,j);
  tcs(1:2)=tcs(1:2)*par
  
  % append the fom and exit flag
  tcs=[tcs;fom;ex];
else
  tcs=ones(5,1)*NaN;
end

if(1)
  t=0.01*[1:length(bi.f)];

  setwinsize(gcf,1000,800); clf
  
  % plot before
  subplot(3,1,1)
  plot(t,y(bi.f),'b');
  hold on
  plot(t,y(bi.b),'r');
  plot(t,y(bi.f)-y(bi.b)+0.2,'m');
  hold off
  axis tight
  ylabel('raw tod')
  
  % plot after
  subplot(3,1,2)
  yf=apply_tc(tc2(1:3),y);
  plot(t,yf(bi.f),'b');
  hold on
  plot(t,yf(bi.b),'r');
  plot(t,yf(bi.f)-yf(bi.b)+0.2,'m');
  
  yf=apply_tc(tc1(1),y);
  plot(t,yf(bi.f)-yf(bi.b)+0.3,'c');

  if(~isnan(labtc(1,j)))
    yf=apply_tc(tcs(1:3),y);
    plot(t,yf(bi.f)-yf(bi.b)+0.1,'g');
  end
  
  hold off
  axis tight
  ylabel('deconv tod')
  title(['cyan single = ',sprintf('%.3f, ',tc1),'  magenta double = ',sprintf('%.3f, ',tc2),'  green scale = ',sprintf('%.3f, ',tcs)]);
  
  subplot(3,1,3)
  plot(t,xp(bi.f),'b');
  hold on
  plot(t,xp(bi.b),'r');
  plot(t,xp(bi.f)-xp(bi.b),'m');
  hold off
  axis tight
  ylabel('az sky offset')
  xlabel('sample number')
  legend({'for','back','diff'},'Location','NorthWest');
  grid
  
  subplot(3,1,1)
  title(sprintf('fitting %s',char(p.channel_name(j))));

  %pause
  drawnow
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function d=timeconst_fitfunc(tc,bi,y,labtc)

tc

if(exist('labtc'))
  % we are doing a scaled dual tc fit against lab values
  labtc(1:2)=labtc(1:2)*tc;
  tc=labtc;
end

y=apply_tc(tc,y);

% pull out blip sections
yf=y(bi.f);
yb=y(bi.b);

% return vector of differences
d=yf-yb;

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y=apply_tc(tc,y)

% deconv timeconst from full scan group
fs.sf=1; fs.ef=length(y);
y=deconv_scans2(y,fs,1,tc);
d.v=y; d=lowpassfilt(d,fs,{'v'}); y=d.v;

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [par1,par2,pars]=filtmodel(x,y,p,j,labtc)

% fit timestream to gauss model passed through filter

% find bits of scan around expected positions of blips
i=abs(x+p.ra_off_dos(j))<0.6;

% adjust deconv timeconst to get best match between for/back blips

% initial fit to get blip height/position/width
par(1)=min(y);  par(2)=-p.ra_off_dos(j); par(3)=0.04;
par=lsqnonlin(@(t) y-gauss(t,x),par);

if(1)
  % refine with single time const
  par(4)=0.020;
  [par1,fom,dum,ex]=lsqnonlin(@(t) y-tc_fitfunc(t,x),par);
  %[par1,dum,fom,ex]=matmin('chisq',par,[],'tc_fitfunc',y,[],x);  
  par1=[par1;fom;ex]
  
  % further refine with 2nd tc
  
  % start tcb at 5x tca
  par2=[par1,par1(4)*5,0.03];

  % Lower limit of 2nd tc is 1.5x initial fit single tc
  lb=[-1e99,-1e99,0.0, 0.005,1.5*par1(4),0.03];
  ub=[    0,+1e99,0.2, 0.200,          2,1.00];
  [par2,fom,dum,ex]=lsqnonlin(@(t) y-tc_fitfunc(t,x),par2,lb,ub);
  
  %freepar.free=[1,0,0,1,1,1];
  %freepar.lb=[0,0,0,0.005,1.5*par1(4),0.03];
  %freepar.ub=[0,0,0,0.200,          2,1.00];
  %[par2,pe,fom,ex]=matmin('chisq',par,freepar,'tc_fitfunc',y,[],x);

  par2=[par2;fom;ex]
end

% use lab val to make single par fit
if(1)
  pars(4)=1;
  lb=[-1e99,-1e99,-1e99,0]; ub=[1e99,1e99,1e99,3];
  [pars,fom,dum,ex]=lsqnonlin(@(t) y-tc_fitfunc(t,x,labtc(:,j)),pars,lb,ub);

  % convert from scale factor to dual tc form
  pars(4:5)=labtc(1:2,j)*pars(4);
  pars(6)=labtc(3,j);
  
  pars=[pars;fom;ex]
end

if(1)
  setwinsize(gcf,1000,600); clf
  
  % single tc fit
  subplot(3,1,1)
  plot(y(i),'b');
  hold on
  plot(tc_fitfunc(par1,x(i)),'r');
  hold off
  axis tight

  % dual tc fit
  subplot(3,1,2)
  plot(y(i),'b');
  hold on
  plot(tc_fitfunc(par2,x(i)),'r');
  hold off
  axis tight
  
  % plot versus az pos in blip region
  subplot(3,2,5)
  plot(x(i),y(i),'b');
  hold on
  plot(x(i),tc_fitfunc(par1,x(i)),'r');
  hold off
  axis tight
  title(sprintf('%.3f, ',par1));

  subplot(3,2,6)
  plot(x(i),y(i),'b');
  hold on
  plot(x(i),tc_fitfunc(par2,x(i)),'r');
  hold off
  axis tight
  title(sprintf('%.3f, ',par2));
  
  subplot(3,1,1)
  title(sprintf('fitting %s',char(p.channel_name(j))));
  
  %pause
  drawnow
end

return
