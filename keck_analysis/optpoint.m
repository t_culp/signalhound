function [fm,fmerr,rmssa,npoints]=optpoint(tstart,tend,plotresids)
% [param,param_err,rms,n]=optpoint(tstart,tend,plotresids)
%      OR
% optpoint(d);
%
% Read optical pointing data and reduce
%
% e.g.:
%    optpoint('2005-Mar-06:13:42:45','2005-Mar-06:16:23:18');
%                            OR
%    d=load_arc('/data/keckdaq/arc/','2005-Mar-06:13:42:45','2005-Mar-06:16:23:18')
%    optpoint(d)

if(~exist('tend','var'))
  tend=[];
end
if(~exist('plotresids','var'))
  plotresids=1;
end

[om,ide,obs]=read_data(tstart,tend);
if(isempty(ide))
  disp('there were no data to reduce')
  fm=[];fmerr=[];rmssa=[];npoints=[];
  return
end
om

if(~isempty(tend))
  ttext1=sprintf('Online residuals %s to %s',tstart,tend);
  ttext2=sprintf('Re-fit %s to %s',tstart,tend);
else
  ttext1='Online residuals';
  ttext2='Re-fit';
end

if(plotresids)
  figure(1);
end
[rmssa,resids]=plot_res(om,ide,obs,ttext1,plotresids);

% get rid of absurd outliers
ind=resids<1;
obs=structcut(obs,ind);
ide=structcut(ide,ind);

om(1)=0; om(2)=0; om(5)=0;
[fm,sa,gof,fmerr]=do_fit(om,[0,0,1,1,1,0,0,1,1,1,1],obs,ide);
if(plotresids)
  figure(2);
end
[rmssa,dum,outlierind]=plot_res(fm,ide,obs,ttext2,plotresids);

% if there are outliers, refit
if ~isempty(outlierind)
  disp(sprintf('Found %d outliers; excluding and re-fitting',numel(outlierind)))
  
  keepind=setxor(1:numel(obs.az),outlierind);
  obs=structcut(obs,keepind);
  ide=structcut(ide,keepind);  
  
  [fm,sa,gof,fmerr]=do_fit(om,[0,0,1,1,1,0,0,1,1,1,1],obs,ide);
  
  if(plotresids)
    figure(3);
  end
  [rmssa,dum,outlierind]=plot_res(fm,ide,obs,[ttext2,' excluding outliers'],plotresids);
end


npoints=numel(ide.az);

% Print out results in form suitable for online config
disp(sprintf('encoder_zero_points %f, %f, 60',fm(10),fm(11)));
disp(sprintf('tilts %f, %f, %f',fm(3),fm(4),fm(5)));
disp(sprintf('flexure optical, cosFlexure=%f, sinFlexure=%f',fm(1),fm(2)));
disp(sprintf('collimate_el optical, %f, %f',fm(6),fm(7)));
disp(' ')
disp(sprintf('flexure radio,   cosFlexure=%f, sinFlexure=%f',fm(1),fm(2)));
disp(sprintf('collimate_el radio, %f, %f',fm(6),fm(7)));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%
function [om,ide,obs]=read_data(tstart,tend)

regs={'antenna0.frame.utc',...
      'antenna0.tracker.horiz_topo',...
      'antenna0.tracker.horiz_mount',...
      'antenna0.tracker.flexure',...
      'antenna0.tracker.tilts',...
      'antenna0.tracker.fixedCollimation',...
      'antenna0.tracker.polarCollimation',...
      'antenna0.tracker.encoder_off',...
      'antenna0.tracker.horiz_off',...
      'antenna0.tracker.source',...
      'antenna0.time.utcfast',...
      'antenna0.time.utcslow',...      
      'array.frame.features',
       };%,...
      %'antenna0.pmac.mtr_pos',...
      %'antenna0.pmac.x_tilt',...
      %'antenna0.pmac.y_tilt',...
      %'antenna0.tracker.sky_xy_off',...
      %'antenna0.weather.windSpeed'};
  
if(~isempty(tend))
  d=load_arc('arc/',tstart,tend,regs);
else
  d=tstart;
end

if(isempty(d))
  om=[];
  ide=[];
  obs=[];
  disp('no data read in');
  return
end
d.t=d.antenna0.time.utcfast(:,1);

if(isempty(d.t))
  om=[];
  ide=[];
  obs=[];
  disp('no data read in');
  return
end
d.ts=d.antenna0.time.utcslow(:,2);

% Get rid of any bogus archive points
d=framecut(d,d.array.frame.features==1);

if(isempty(d.t))
  om=[];
  ide=[];
  obs=[];
  disp('no stars were found');
  return
end

% convert all registers to degrees
calfactor = [1         % utc
	     1/3.6e6   % horiz_topo: mas -> deg
	     57.2958   % horiz_mount: radians -> deg (az, el, pa)
	     1/3.6e6   % flexure: mas/cos(el) -> deg/cos(el)
	     1/3.6e6   % tilts: mas -> deg
	     1/3.6e6   % fixedCollimation: mas -> deg (mag, dir)
	     1/3.6e6   % polarCollimation: mas -> deg (mag, dir)
	     1/3.6e6   % encoder_off: mas -> deg (az, el, dk)
	     57.2958   % horiz_off: radians -> deg (az, el, pa)
	     1         % source
	     1         % time
	     1         % time
	     1];        % features
	     %360/2304000 % mtr_pos: cnts -> deg
	     %360/65536 % x_tilt: ACC28 ADC -> arcmin (low gain)
	     %360/65536 % y_tilt: ACC28 ADC -> arcmin (low gain)
	     %1/3.6e6   % sky_xy_off: mas -> deg
	     %1];       % windSpeed
	     
for index=1:length(calfactor)
  eval(['d.' regs{index} ' = double(d.' regs{index} ').* calfactor(index);'])
end

% Ideal mount pointing directions
ide.az=d.antenna0.tracker.horiz_topo(:,1);
ide.el=d.antenna0.tracker.horiz_topo(:,2);
ide.dk=d.antenna0.tracker.horiz_off(:,3);

% Actual mount pointing directions not recorded
% - reconstruct by adding in encoder offsets
obs.az=d.antenna0.tracker.horiz_mount(:,1)+double(d.antenna0.tracker.encoder_off(:,1));
obs.el=d.antenna0.tracker.horiz_mount(:,2)+double(d.antenna0.tracker.encoder_off(:,2));

% Put az values into 0-360 range
ind=obs.az<0;  obs.az(ind)=obs.az(ind)+360;
ind=obs.az>360; obs.az(ind)=obs.az(ind)-360;

% It is convienient to keep the model parameters in a single array
om=  [d.antenna0.tracker.flexure ...
      d.antenna0.tracker.tilts ...
      d.antenna0.tracker.fixedCollimation ...
      d.antenna0.tracker.polarCollimation ...
      d.antenna0.tracker.encoder_off(:,1:2)];

% From now on just use online model as it was at start of run -
% shouldn't matter if it changes during a run although there is no
% reason why it ever should.
for i=1:size(om,2)
  if(any(om(:,i)~=om(1,i)))
    warning(sprintf('Online model parameter %d changed during run',i));
  end
end
om=om(1,:);

% Try to reproduce online model using offline
% - offsets still applied after model calc
onl=pointing_model(om,ide);
ind=d.antenna0.tracker.horiz_off(:,1)>180;
d.antenna0.tracker.horiz_off(ind,1)=d.antenna0.tracker.horiz_off(ind,1)-360;
onl.az=onl.az+d.antenna0.tracker.horiz_off(:,1);
onl.el=onl.el+d.antenna0.tracker.horiz_off(:,2);

% calc delta offline-online model calcs
daz=obs.az-onl.az; del=obs.el-onl.el;
ind=daz>180; daz(ind)=daz(ind)-360;

d.t=d.antenna0.time.utcslow(:,2)/1e3;

% Display some info
d.antenna0.tracker.source=cast(d.antenna0.tracker.source,'char');
for i=1:length(ide.az)
  disp(sprintf('%.0f %7.3f %7.3f %7.3f %7.3f %7.4f %7.4f %7.4f %7.4f %s',...
      d.t(i),ide.az(i),ide.el(i),obs.az(i),obs.el(i),daz(i),del(i),...
      d.antenna0.tracker.horiz_off(i,1),d.antenna0.tracker.horiz_off(i,2),d.antenna0.tracker.source(i,:)));
end

disp(sprintf('Read in %d data points',length(obs.az)));

return

