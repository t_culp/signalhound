function reduc_plotaps(apsfiles,pl,specfile)
% reduc_plotaps(file,pl,specfile)
%
% Plot a set of aps. Input can be single aps or apssets
%
% specfile is the name of the cmbfast fits file containing the input
% power spectrum to be plotted
%
% e.g.
% reduc_plotaps({'sim12_filtp3_weight2_jack0_fd','sim12xxx6_filtnp3_weight2_jack0'})
% reduc_plotaps({'sim12_filtp3_weight2_jack0_fd','sim13_filtp3_weight2_jack0_fd'})
%
% reduc_plotaps({'sim140011_filtp3_weight2_jack0','sim140013_filtn_weight2_jack0'})
% reduc_plotaps({'sim19_filtp3_weight2_jack0_fd','sim190014_filtn_weight2_jack0'})
% reduc_plotaps({'sim190014_filtn_weight2_jack0','sim19_filtp3_weight2_jack0_fd'})

  
if(~exist('pl','var'))
  pl=0;
end
if ~iscell(apsfiles)
  apsfiles={apsfiles};
end
  
for i=1:length(apsfiles)
  
  x=load(sprintf('aps/%s',apsfiles{i}));
  apsfiles{i}=strrep(apsfiles{i},'_','\_');
  if(size(x.aps(1).Cs_l,3)>1)
    for j=1:length(x.aps)
      x.aps=process_simset(x.aps);
      n=length(x.aps(j).l);
      aps(j).l=x.aps(j).l;
      aps(j).Cs_l(1:n,:,i)=x.aps(j).mean(:,1:6);
    end
  else
    for j=1:length(x.aps)
      n=length(x.aps(j).l);
      aps(j).l=x.aps(j).l;
      % 1:6 to force compat between old and new extra xspec
      aps(j).Cs_l(1:n,:,i)=x.aps(j).Cs_l(:,1:6);
    end
  end
end


% plot for bpwf comp
if(0)
  figure(1); clf
  setwinsize(gcf,1200,800)
  plot_bpwf(1,aps(1),'100GHz');
  if(length(aps)>1)
    plot_bpwf(2,aps(2),'150GHz');
    plot_bpwf(3,aps(3),'cross');
  end
  
  return
end

% get input model from cmbfast
if(~exist('specfile','var'))
  specfile=[];
end
inpmod=load_cmbfast(specfile,x.coaddopt);


% plot linear lmax=500
figure(1); clf
lmax=500;
setwinsize(gcf,900,700)
plot_aps(1,aps(1),inpmod,'100GHz', lmax);
if(length(aps)>1)
  plot_aps(2,aps(2),inpmod,'150GHz', lmax);
  plot_aps(3,aps(3),inpmod,'cross', lmax);
end
gtitle(sprintf('%s (blue), %s (green)',apsfiles{1},apsfiles{2}));

printname=strcat('aps/',apsfiles{1},'_aps_multifig_lin500.gif');
if(pl) printfig(1,printname,'pp'); end

% plot linear lmax=200
figure(2); clf
lmax=200;
setwinsize(gcf,900,700)
plot_aps(1,aps(1),inpmod,'100GHz', lmax);
if(length(aps)>1)
  plot_aps(2,aps(2),inpmod,'150GHz',lmax);
  plot_aps(3,aps(3),inpmod,'cross', lmax);
end
gtitle(sprintf('%s (blue), %s (green)',apsfiles{1},apsfiles{2}));


printname=strcat('aps/',apsfiles{1},'_aps_multifig_lin200.gif');
if(pl) printfig(2,printname,'pp'); end

return
% plot log
figure(3); clf
setwinsize(gcf,900,700)
plot_apsl(1,aps(1),inpmod,'100GHz');
if(length(aps)>1)
plot_apsl(2,aps(2),inpmod,'150GHz');
plot_apsl(3,aps(3),inpmod,'cross (abs value)');
end
printname=strcat('aps/',apsfiles{1},'_aps_multifig_log500.gif');
if(pl) printfig(3,printname,'pp'); end


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_aps(n,aps,inpmod,freq, lmax)
 
lab={'TT  (\muK^2)','TE  (\muK^2)','EE  (\muK^2)','BB  (\muK^2)','TB  (\muK^2)','EB  (\muK^2)'};
for i=1:6
  subplot_stack(6,3,(i-1)*3+n)
  plot(aps.l,squeeze(aps.Cs_l(:,i,:)),'.-');
  hold on; plot(inpmod.l,inpmod.Cs_l(:,i),'r'); hold off;
  ylabel(lab{i});
  xlim([0,lmax]);
  %ylim([1e-5,1e5]);
  subplot_stack2(6,3,(i-1)*3+n)
  if(i==1)
    title(freq);
  end
end

  
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_apsl(n,aps,inpmod,freq)

subplot(1,3,0+n)
loglog(inpmod.l,abs(inpmod.Cs_l(:,[1,3,4])));
hold on;
plot(aps.l,squeeze(abs(aps.Cs_l(:,1,:))),'.-b');
plot(aps.l,squeeze(abs(aps.Cs_l(:,3,:))),'.-g');
plot(aps.l,squeeze(abs(aps.Cs_l(:,4,:))),'.-r');
ylabel('\muK^2')
legend('TT','EE','BB','Location','SouthEast');
hold off;
axis tight
ylim([1e-5,1e4]); xl=xlim; xlim([10,500])
title(freq);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_bpwf(n,aps,freq)

lab={'TT','TE','EE','BB'};
for i=1:length(lab)
  subplot(4,3,(i-1)*3+n)
  plot(aps.l,squeeze(aps.Cs_l(:,i,:)),'.-');
  ylabel(lab{i});
  xlim([0,500]);
  %ylim([9e-7,1e5]);
  yl=ylim; ylim([0,yl(2)/10]);
  if(i==1)
    title(freq);
  end
end

return
