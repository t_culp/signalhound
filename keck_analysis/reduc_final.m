function reduc_final(realspec,finalopt,pl,lmax)
% reduc_final(realspec,finalopt,pl,lmax)
%
% Take real spectrum and:
%
% - subtract mean of nsimset (noise simset) from all spectra
% - subtract mean of ssimset (signal simset) from BB spectra for jack0 only
% - correct for filtering/beam suppression using ssimset, but if
%   bosimset is provided use this to correct the B-spectra
% - merge the bins if needed
% - snsimset (sig+noise simset) is processed identically to the real
%   spectra and used to estimate error bars
%
% All spectra should have been generated using identical reduc_makeaps
% options - including the choice of bandpower estimators
%
% Normally:
% - nsimset contains noise (pseudo) sims
% - ssimset contains unlensed EnoB sims
% - bosimset contains BnoE sims (normally r=0.1)
% - snsimset contains lensed EnoB sims added to noise sims
%
% doblind = zero out bb/tb/eb spectra
%
% if pl=0 no plots on screen - output file to disk
%      =1 plot to screen only
%      =2 plot to screen and save images
% lmax specifies max ell for plots
%  
% pl_cross: the reduc_final plot has three columns: Receiver/Freq 1, Receiver/Freq 2, cross 1x2
% if the input data has more than three receivers or frequencies pl_cross allows which combination
% to plot. default [1,2] (first vs second)
%
% e.g.
% Make final non-jack spectra
% 
% reduc_final('0751/real_a_filtp3_weight3_gs_dp1102_jack0_pureB',[],0,500)
%
% finalopt.supfacstyle='direct_bpwf';
% finalopt.bpwfname='0704/xxxx_allcmb_filtp3_weight3_gs_dp1100_jack0_pureB_directbpwf.mat'
% reduc_final('0751/real_a_filtp3_weight3_gs_dp1102_jack0_pureB',finalopt,0,500)
%
% there are more options to finalopt, than documented here. Just a list:
%
% finalopt.supfacstyle='direct_bpwf', 'mask_bpwf', 'no_bpwf'
% finalopt.kludgebbsupfac=false
% finalopt.daughter=''
% %finalopt.force_size=[]; % commented out
% finalopt.zerobbtoee
% finalopt.spec_scale % dust scaling
% finalopt.noxdb % don't debias the cross spectra

if(~exist('pl','var'))
  pl=[];
end
if(~exist('lmax','var'))
  lmax=[];
end

% record the real spectrum
finalopt.realspec=realspec;

rsp=strrep(realspec,'dp11022','dp1100');
rsp=strrep(rsp,'dp1102','dp1100');
rsp=strrep(rsp,'_norot','');
if(~isfield(finalopt,'nsimset'))
  finalopt.nsimset=strrep(rsp,'real','xxx6');
end
if(~isfield(finalopt,'ssimset'))
  finalopt.ssimset=strrep(rsp,'real','xxx2');
  finalopt.ssimset(strfind(finalopt.ssimset,'jack')+4)='0';
end
if(~isfield(finalopt,'snsimset'))
  finalopt.snsimset=strrep(rsp,'real','xxx7');
end
if(~isfield(finalopt,'bosimset'))
  finalopt.bosimset=strrep(rsp,'real','xxx4');
  finalopt.bosimset(strfind(finalopt.bosimset,'jack')+4)='0';
end
if(~isfield(finalopt,'dsimset'))
  finalopt.dsimset=strrep(rsp,'real','xxx3');
  finalopt.dsimset(strfind(finalopt.dsimset,'jack')+4)='0';
end
if(~isfield(finalopt,'snrsimset'))
  finalopt.snrsimset=strrep(rsp,'real','xxx9');
end
if(~isfield(finalopt,'sndsimset'))
  finalopt.sndsimset=strrep(rsp,'real','xxx8');
end
if(~isfield(finalopt,'slsimset'))
  finalopt.slsimset=strrep(rsp,'real','xxx5');
end
if(~isfield(finalopt,'doblind'))
  finalopt.doblind=0;
end
if(~isfield(finalopt,'pl_cross'))
  finalopt.pl_cross=[1,2];
end
if(~isfield(finalopt,'mapname'))
  finalopt.mapname={};
end
if(~isfield(finalopt,'kludgebbsupfac'))
  finalopt.kludgebbsupfac=false;
end
if(~isfield(finalopt,'daughter'))
  finalopt.daughter='';
end
if(~isfield(finalopt,'supfacstyle'))
  finalopt.supfacstyle='mask_bpwf';
end
if(~isfield(finalopt,'expand_bpwf'))
  finalopt.expand_bpwf=[];
end
if(~isfield(finalopt,'force_size'))
  finalopt.force_size=[];
end
if(~isfield(finalopt,'noxdb'))
  finalopt.noxdb=false;
end
if(~isfield(finalopt,'spec_scale'))
  finalopt.spec_scale=[];
end
if(~isfield(finalopt,'zerobbtoee'))
  switch finalopt.supfacstyle
   case 'direct_bpwf'
    finalopt.zerobbtoee=true;
   otherwise
    finalopt.zerobbtoee=false;
  end
end

if(isempty(pl))
  pl=1;
end
if(isempty(lmax))
  lmax=500;
end

% find which jackknife we are
jackn=strfind(realspec,'jack')+4;
jack=str2num(realspec(jackn(1)));
% find jacktype
jacktype=realspec(jackn(1));

% if jack is a letter, set jack to true
if isempty(jack)
  jack=1;
end

% set doblind to true if jack0
doblind=0;
if ~jack && finalopt.doblind
  doblind=1;
end

% get the real spectra and sets of sim spectra
disp('loading spectra...');
r=  load(sprintf('aps/%s',finalopt.realspec));
opt.coadd=r.coaddopt; opt.aps=r.apsopt; r=r.aps;
n=  load(sprintf('aps/%s',finalopt.nsimset)); n=n.aps;
s=  load(sprintf('aps/%s',finalopt.ssimset)); s=s.aps;
sn= load(sprintf('aps/%s',finalopt.snsimset)); sn=sn.aps;
nsims = min([size(n(1).Cs_l,3),size(s(1).Cs_l,3),size(sn(1).Cs_l,3)]);
% only fetch bo,snr,d,snd and sl if they exist
if(exist(sprintf('aps/%s.mat',finalopt.bosimset),'file'))
  bo=load(sprintf('aps/%s',finalopt.bosimset)); bo=bo.aps;
  nsims = min([nsims,size(bo(1).Cs_l,3)]);
else
  finalopt.bosimset=[];
end
if(exist(sprintf('aps/%s.mat',finalopt.snrsimset),'file'))
  snr= load(sprintf('aps/%s',finalopt.snrsimset)); snr=snr.aps;
  nsims = min([nsims,size(snr(1).Cs_l,3)]);
else
  finalopt.snrsimset=[];
end
if(exist(sprintf('aps/%s.mat',finalopt.slsimset),'file'))
  sl=load(sprintf('aps/%s',finalopt.slsimset)); sl=sl.aps;
  nsims = min([nsims,size(sl(1).Cs_l,3)]);
else
  finalopt.slsimset=[];
end
if(exist(sprintf('aps/%s.mat',finalopt.sndsimset),'file'))
  snd= load(sprintf('aps/%s',finalopt.sndsimset)); snd=snd.aps;
  nsims = min([nsims,size(snd(1).Cs_l,3)]);
else
  finalopt.sndsimset=[];
end
if(exist(sprintf('aps/%s.mat',finalopt.dsimset),'file'))
  d= load(sprintf('aps/%s',finalopt.dsimset)); d=d.aps;
  nsims = min([nsims,size(d(1).Cs_l,3)]);
else
  finalopt.dsimset=[];
end
% the force size option allows to replicate the structure of the
% sim aps to match the one of the real data. For instance for the
% Planck sims, Bicep2's type 2 is used as signal sims. That type,
% however, comes only with one map instead of 7 for each of the
% Pl freqs - the aps then has two auto and one cross spectrum.
% using force_size=[1,repmat(2,1,7),repmat(3,1,28)] expands the 3
% aps then out to use the second (auto) spectrum 7 times and the 3rd
% (cross) spectrum 28 times to generate the full 36 fold structure needed
% for a Bicep2xPlanck auto+cross spectrum.
if ~isempty(finalopt.force_size)
  force_size = finalopt.force_size;
  if length(r)~=length(force_size); r=r(force_size); end
  if length(n)~=length(force_size); n=n(force_size); end
  if length(s)~=length(force_size); s=s(force_size); end
  if length(sn)~=length(force_size); sn=sn(force_size); end
  if(exist('bo','var') && length(bo)~=length(force_size)); bo=bo(force_size); end
  if(exist('snr','var') && length(snr)~=length(force_size)); snr=snr(force_size); end
  if(exist('sl','var') && length(sl)~=length(force_size)); sl=sl(force_size); end
  if(exist('d','var') && length(d)~=length(force_size)); d=d(force_size); end
  if(exist('snd','var') && length(snd)~=length(force_size)); snd=snd(force_size); end
end

% cut down to the minimum number of sims throughout the sets:
for jj=1:size(n,1)
  n(jj).Cs_l = n(jj).Cs_l(:,:,1:nsims);
  sn(jj).Cs_l = sn(jj).Cs_l(:,:,1:nsims);
  s(jj).Cs_l = s(jj).Cs_l(:,:,1:nsims);
  if(exist('bo','var'))
    bo(jj).Cs_l = bo(jj).Cs_l(:,:,1:nsims);
  end
  if(exist('snr','var'))
    snr(jj).Cs_l = snr(jj).Cs_l(:,:,1:nsims);
  end
  if(exist('sl','var'))
    sl(jj).Cs_l = sl(jj).Cs_l(:,:,1:nsims);
  end
  if(exist('snd','var'))
    snd(jj).Cs_l = snd(jj).Cs_l(:,:,1:nsims);
  end
  if(exist('d','var'))
    d(jj).Cs_l = d(jj).Cs_l(:,:,1:nsims);
  end
end

% for the experiment jack f the sims are still jack0 and have hence
% the alternative cross spectra in aps(:,7:9,:)
% jack f first has n_auto empty spectra. The spots where usually
% the cross spectra live are filled with the subractions.
% unclear which supression factor to use, use the one that is otherwise
% applied to the corresponding cross spectrum. This happens when we cut
% down the sims to have the size of jack f:
if strcmp(realspec(jackn(1)),'f')
  for jj=1:size(n,1)
    n(jj).Cs_l = n(jj).Cs_l(:,1:6,:);
    s(jj).Cs_l = s(jj).Cs_l(:,1:6,:);
    sn(jj).Cs_l = sn(jj).Cs_l(:,1:6,:);
    if(exist('bo','var'))
      bo(jj).Cs_l = bo(jj).Cs_l(:,1:6,:);
    end
    if(exist('snr','var'))
      snr(jj).Cs_l = snr(jj).Cs_l(:,1:6,:);
    end
    if(exist('sl','var'))
      sl(jj).Cs_l = sl(jj).Cs_l(:,1:6,:);
    end
    if(exist('snd','var'))
      snd(jj).Cs_l = snd(jj).Cs_l(:,1:6,:);
    end
    if(exist('d','var'))
      d(jj).Cs_l = d(jj).Cs_l(:,1:6,:);
    end
  end
end

% finalopt.pl_cross marks two of the na auto spectra for plotting.
% From the total number of spectra N=na+nc=na+(na-1)*na/2 calculate
% the number of auto spectra:
N=size(r,1);
na = -0.5+sqrt(2*N+0.25);

% just do the same loop as during the creating of the xspectra
% first: expand out the mapname to the cross spectrum
nc_c=na+1;
if na>1
  for j=1:na-1
    for c=j+1:na
      finalopt.mapname{nc_c}=[finalopt.mapname{j},'x',finalopt.mapname{c}];
      nc_c=nc_c+1;
    end
  end
end

% find the proper third index which points to the cross spectrum:
% do this only if the pl_cross does not already have three columns
% if it is handed with three columns arbitrary combinations can be plotted (special)
if size(finalopt.pl_cross,2)~=3
  nc_c=na+1;
  if na>1
    for j=1:na-1
      for c=j+1:na
        % use here the min and max functions. In this way
        % one can swap the order of the colums that are plotted
        % by handing in for instance pl_cross = [2,1] instead of [1,2].
        if j==min(finalopt.pl_cross(1:2)) && c==max(finalopt.pl_cross(1:2))
          nc_i=nc_c;
        end
        nc_c=nc_c+1;
      end
    end
    % That are the three columns to be plotted:
    finalopt.pl_cross=[finalopt.pl_cross,nc_i];
  else
    finalopt.pl_cross=[1];
  end
end

% fetch the proper rx assignment:
if numel(finalopt.pl_cross)==1
  filename = {''};
else
  filename = {finalopt.mapname{finalopt.pl_cross(1)},finalopt.mapname{finalopt.pl_cross(2)},finalopt.mapname{finalopt.pl_cross(3)}};
end

% mapname is used in the titles, pre default the same as filename but
% changes for instance for jackf.
mapname = filename;

% set crosses to minus for the experiment jack
if strcmp(realspec(jackn(1)),'f')
  for ii=1:length(mapname)
    mapname{ii}=strrep(mapname{ii},'x','-');
  end
end

% apply a scaling to the auto and cross spectra.
% autos get the square, crosses the combined factor
% note that it is applied multiplcative, so a factor 25 for scaling
% dust from 353 to 150 GHz should be handed in as 1/25.
% avoid scaling for frequency jack
if ~isempty(finalopt.spec_scale) & ~strcmp(realspec(jackn(1)),'f')
  % loop the auto specta
  for jj=1:na
    scale_factor = finalopt.spec_scale(jj)^2;
    r(jj).Cs_l = r(jj).Cs_l*scale_factor;
    n(jj).Cs_l = n(jj).Cs_l*scale_factor;
    s(jj).Cs_l = s(jj).Cs_l*scale_factor;
    sn(jj).Cs_l = sn(jj).Cs_l*scale_factor;
    if(exist('bo','var'))
      bo(jj).Cs_l = bo(jj).Cs_l*scale_factor;
    end
    if(exist('snr','var'))
      snr(jj).Cs_l = snr(jj).Cs_l*scale_factor;
    end
    if(exist('sl','var'))
      sl(jj).Cs_l = sl(jj).Cs_l*scale_factor;
    end
    if(exist('snd','var'))
      snd(jj).Cs_l = snd(jj).Cs_l*scale_factor;
    end
    if(exist('d','var'))
      d(jj).Cs_l = d(jj).Cs_l*scale_factor;
    end
  end
  % loop the cross spectra
  nc_c=na+1;
  for j=1:na-1
    for c=j+1:na
      scale_factor = finalopt.spec_scale(j)*finalopt.spec_scale(c);
      r(nc_c).Cs_l = r(nc_c).Cs_l*scale_factor;
      n(nc_c).Cs_l = n(nc_c).Cs_l*scale_factor;
      s(nc_c).Cs_l = s(nc_c).Cs_l*scale_factor;
      sn(nc_c).Cs_l = sn(nc_c).Cs_l*scale_factor;
      if(exist('bo','var'))
        bo(nc_c).Cs_l = bo(nc_c).Cs_l*scale_factor;
      end
      if(exist('snr','var'))
        snr(nc_c).Cs_l = snr(nc_c).Cs_l*scale_factor;
      end
      if(exist('sl','var'))
        sl(nc_c).Cs_l = sl(nc_c).Cs_l*scale_factor;
      end
      if(exist('snd','var'))
        snd(nc_c).Cs_l = snd(nc_c).Cs_l*scale_factor;
      end
      if(exist('d','var'))
        d(nc_c).Cs_l = d(nc_c).Cs_l*scale_factor;
      end
      nc_c=nc_c+1;
    end
  end
  % append something to the file name
  realspec=sprintf('%s_scaled',realspec);
end

% get input models
disp('loading model...');

% this model will be used in plots to compare to real data - for
% jackknifes this model is forced to zero
% for howtojack='none' tweak the coaddopt, so that load_cmbfast
% does not zero out the model spectra
optcoadd = opt.coadd;
if strcmp(opt.aps.howtojack,'none')
  optcoadd.jacktype='0';
  jack=0;
end
inpmodr=load_cmbfast('aux_data/official_cl/camb_planck2013_r0.fits',optcoadd);

% this model is the input model used to generate the ssimset
% inpmods=load_cmbfast('aux_data/official_cl/camb_wmap5yr_noB.fits');
inpmods=load_cmbfast('aux_data/official_cl/camb_planck2013_r0.fits');
% inpmods2 is the same as inpmod, however here loaded with the coadd opt so 
% that it get set to zero if we are looking at jack knifes. This will be save
% in the .mat file
inpmods2=load_cmbfast('aux_data/official_cl/camb_planck2013_r0.fits',optcoadd);

% this model is the input model used to generate the bosimset
if(~isempty(finalopt.bosimset))
  inpmodbo=load_cmbfast('aux_data/official_cl/camb_planck2013_r0p1_noE.fits');
end

% Before any processing plot the signal+noise sims as lines with real
% data overplotted as heavy line - this is the basic result of the
% simulation - is the data just a realization of the sim?
plot_aps(pl,1,sn,r,inpmodr,'raw signal+noise sim comp to real',lmax,jack,finalopt.pl_cross,mapname,doblind);

% Subtract mean noise n from real and signal+noise sim.
% Even the cross spectra show small mean values in noise only sim so
% we subtract these too.
n=process_simset(n);
for j=1:length(r)
  mn=n(j).mean;
  % if no noise debias is applied to the cross spectra, zero
  % out the corresponding part in mn (avoid for experiment jack):
  if finalopt.noxdb && j>na && ~strcmp(realspec(jackn(1)),'f')
    mn=mn*0;
  end
  r(j).Cs_l=r(j).Cs_l-mn;
  sn(j).Cs_l=sn(j).Cs_l-repmat(mn,[1,1,size(sn(j).Cs_l,3)]);
  if(exist('snr','var'))
    snr(j).Cs_l=snr(j).Cs_l-repmat(mn,[1,1,size(snr(j).Cs_l,3)]);
  end
  if(exist('snd','var'))
    snd(j).Cs_l=snd(j).Cs_l-repmat(mn,[1,1,size(snd(j).Cs_l,3)]);
  end
  % keep a record of the debias
  db(j).Cs_l=mn;
end

if finalopt.noxdb
  % append to output filenames
  realspec=sprintf('%s_noxdb',realspec);
end

% replot
plot_aps(pl,2,sn,r,inpmodr,'after noise subtract',lmax,jack,finalopt.pl_cross,mapname,doblind);

if(isfield(finalopt,'residbeamcorr'))
  % add realizations of residual beam leakage to sig and sig+noi
  % sims - these will get debiased out in the next step correcting
  % the real data and increasing the fluctuation of the sims to
  % account for the uncertainty on that correction
  load(finalopt.residbeamcorr);
  for j=1:length(aps)
    nrlz=size(s(j).Cs_l,3);
    s(j).Cs_l(:,4,:)=s(j).Cs_l(:,4,:)+aps(j).Cs_l(:,4,1:nrlz);
    sn(j).Cs_l(:,4,:)=sn(j).Cs_l(:,4,:)+aps(j).Cs_l(:,4,1:nrlz);
    if(exist('snr','var'))
      snr(j).Cs_l(:,4,:)=snr(j).Cs_l(:,4,:)+aps(j).Cs_l(:,4,1:nrlz);
    end
    if(exist('snd','var'))
      snd(j).Cs_l(:,4,:)=snd(j).Cs_l(:,4,:)+aps(j).Cs_l(:,4,1:nrlz);
    end
  end
end

% subtract mean of ssimset for BB from real and sn sims to debias E->B
% leakage, jack0 only
s=process_simset(s);
if(jack==0 | strcmp(opt.aps.howtojack,'none'))
  for j=1:length(r)  
    ms=s(j).mean(:,4);
    r(j).Cs_l(:,4)=r(j).Cs_l(:,4)-ms;
    % why is s not debiased here? (it gets resid added above)
    sn(j).Cs_l(:,4,:)=sn(j).Cs_l(:,4,:)-repmat(ms,[1,1,size(sn(j).Cs_l,3)]);
    if(exist('snr','var'))
      snr(j).Cs_l(:,4,:)=snr(j).Cs_l(:,4,:)-repmat(ms,[1,1,size(snr(j).Cs_l,3)]);
    end
    % also do this to the sn+dust model (?)
    if(exist('snd','var'))
      snd(j).Cs_l(:,4,:)=snd(j).Cs_l(:,4,:)-repmat(ms,[1,1,size(snd(j).Cs_l,3)]);
    end
    db(j).Cs_l(:,4)=db(j).Cs_l(:,4)+ms;
  end
end

% replot
plot_aps(pl,3,sn,r,inpmodr,'after BB debias',lmax,jack,finalopt.pl_cross,mapname,doblind);

if(exist('bo','var'))
  % copy the BB spectrum into the model which is about to be used to
  % derive the expectation values and hence the supression factors
  inpmods.Cs_l(:,4)=inpmodbo.Cs_l(:,4);
  % inpmods2 is the input model saved in the .mat file. For jacks it is
  % zero - just allow filling BB in the none jack case:
  if(jack==0 | strcmp(opt.aps.howtojack,'none'))
    inpmods2.Cs_l(:,4)=inpmodbo.Cs_l(:,4);
  end
  % copy B-only sim output values into ssimset so we derive the proper
  % BB supression factor
  for j=1:length(s)
    s(j).Cs_l(:,4,:)=bo(j).Cs_l(:,4,:);
  end
else
  % if no B-only sim available copy EE to BB
  inpmods.Cs_l(:,4)=inpmods.Cs_l(:,3);
  for j=1:length(s)
    s(j).Cs_l(:,4,:)=s(j).Cs_l(:,3,:);
  end
end


if(finalopt.kludgebbsupfac)
  % The B1 and B2/Keck BB sim maps don't match - do this to prevent
  % nonsense being done to the bpwf's in the supfac calculation
  s(4).Cs_l(:,4,:)=s(1).Cs_l(:,4,:);
  s(5).Cs_l(:,4,:)=s(1).Cs_l(:,4,:); 
end

switch finalopt.supfacstyle
  
 case 'direct_bpwf'

  % In QUaD days the scheme was
  % - sims contain only E
  % - BPWF predict how this enters E and B
  % - supfac corrects E to hit it's expval which are "below the
  % line" in LCDM since some of the power goes into BB
  % - BB uses EE supfac
  % This was logically self consistent - for a given theory model
  % we can compute the expectation values of the EE and BB
  % bandpowers - but one can complain that rather than living with
  % EtoB mixing we should correct it.
  %
  % With the introduction of the b-only-simset things became
  % logically inconsistent
  % - we debias out the EtoB power (throw it away) and zero the
  % EE->BB bpwf - this is OK.
  % - however the BB->EE bpwf remained in effect causing i) the B
  % expvals to sit "below the line" and ii) some of
  % the r=0.1 spectrum to contribute to the E expvals
  % - this wasn't going on in the EnoB sims so the EE supfac was
  % slightly wrong
  % - but the r=0.1 spectrum is >1 order of mag less than the LCDM
  % EE spectrum at all ell so this was a tiny effect in practice.
  %
  % However with the switch to direct BPWF we can reconsider what
  % is best to do:
  % - We'd like it to work for pureB as well as matrix
  % so we are still going to debias EtoB mixing and zero the EtoB
  % bpwf
  % - Some leakage from BtoE still occurs. I think we have the
  % option to zero the BtoE bpwf and have the BB points "sit on the line"
  % - expvals for models with very high B would be significantly
  % wrong 
  
  % Get BPWF derived from single ell sims
  load(sprintf('aps/%s',finalopt.bpwfname));
  
  % expand the bpwf for using it multiple times
  if ~isempty(finalopt.expand_bpwf)
    disp('Expanding the bpwfs')
    bpwf = bpwf(finalopt.expand_bpwf);
  end
  
  % if we are using bpwf which are less than length r just
  % replicate it
  % use the length of s instead of r to handle jackf case
  if(length(bpwf)<length(s))
    bpwf=repmat(bpwf,[length(s),1]);
    na = -0.5+sqrt(2*length(s)+0.25);
    for jj=na+1:length(s)
      % furthermore lash in the ET cross spectrum bpwf
      bpwf(jj).Cs_l(:,:,7)=bpwf(jj).Cs_l(:,:,2);
    end      
    disp('WARNING: direct bpwfs assumed same for all spectra');
  end
  
  % since we "debias" BB we need to zero out the EE->BB BPWF as we are
  % throwing away this part of the power
  for j=1:length(s)
    bpwf(j).Cs_l(:,:,5)=0;
  end
  if(finalopt.zerobbtoee)
    % Optionally zero the BB->EE part as well - this makes the BB
    % expvals "sit on the theory curve"
    for j=1:length(s)
      bpwf(j).Cs_l(:,:,6)=0;
    end
  end
    
  % get the integrals of the bpwf (and normalize them to unit sum)
  bpwf=norm_bpwf(bpwf);
  
  % get supression factors as bpwf integrals
  % - this is adapted from calc_supfac
  for j=1:length(s)
    supfac(j).l=s(j).l; % used in plot_supfac below
    supfac(j).tt=bpwf(j).sum(:,1);
    supfac(j).te=bpwf(j).sum(:,2);
    supfac(j).ee=bpwf(j).sum(:,3);
    supfac(j).bb=bpwf(j).sum(:,4);
    % this check for a cross spectrum where the alternative
    % ET cross is present:
    if(size(s(j).Cs_l,2)==9)
      % usually the alternative cross is also present in the
      % bpwfs. However, for the expand_bpwf field may indicate
      % to use an auto spectrum derived bpwf for a cross spectum
      % it *might* be ok to use TE instead.
      if (size(bpwf(j).sum,2)==7)
        supfac(j).et=bpwf(j).sum(:,7);
      else
        warning('ET not present in bpwf, use TE instead')
        supfac(j).et=bpwf(j).sum(:,2);
      end
    end
    supfac(j).tb=sqrt(supfac(j).tt.*supfac(j).bb);
    supfac(j).eb=sqrt(supfac(j).ee.*supfac(j).bb);
  end
  for j=1:length(s)
    tt=supfac(j).tt; ee=supfac(j).ee; bb=supfac(j).bb;
    te=supfac(j).te; tb=supfac(j).tb; eb=supfac(j).eb;
    if(size(s(j).Cs_l,2)==6)
      supfac(j).rwf=1./[tt,te,ee,bb,tb,eb];
    else
      et=supfac(j).et;
      supfac(j).rwf=1./[tt,te,ee,bb,tb,eb,et,tb,eb];
    end
  end
  
  % calculate the expvals - not used in this mode but passed
  % forward
  r=calc_expvals(r,inpmodr,bpwf);  

  % append to output filenames
  realspec=sprintf('%s_directbpwf',realspec);
  
 case 'mask_bpwf'

  % Calc expectation values using band power window functions
  % derived from the sky cut
  % In principle the bandpower window function depend on the jackknife
  % as it changes the shape of the mask.
  % However we are about to compare to non-jack signal only sims so
  % non-jack bpwf are what we want here.
  bpwfname=realspec; bpwfname(jackn)='0';
  load(sprintf('aps/%s_bpwf',bpwfname));
  
  % expand the bpwf for using it multiple times
  if ~isempty(finalopt.expand_bpwf)
    disp('Expanding the bpwfs')
    bpwf = bpwf(finalopt.expand_bpwf);
  end
  
  % for the spectral jack the places where the cross specta appear in the
  % aps file have the regular 6 instead of the (6+3) spectra. The bpwf,
  % however, still have the additional column (6+1). Get rid of it in this
  % case:
  if strcmp(realspec(jackn(1)),'f')
    for j=1:length(r)
      bpwf(j).Cs_l=bpwf(j).Cs_l(:,:,1:6);
      bpwf(j).sum=bpwf(j).sum(:,1:4);
    end
  end
  
  % since we "debias" BB we need to zero out the EE->BB BPWF as we are
  % forcing the BB expectation value to zero (assuming the input model
  % reflects reality).

  % NB: reduc_bpwf.m currently does not account for "Pure-B" estimator
  % which presumably changes the BPWF for BB
  for j=1:length(r)
    % add EE->BB response to BB->BB response (so that bpwf still sums to one)
    bpwf(j).Cs_l(:,:,4)=bpwf(j).Cs_l(:,:,4)+bpwf(j).Cs_l(:,:,5);
    % zero out the EE->BB response
    bpwf(j).Cs_l(:,:,5)=0;
  end
  if(finalopt.zerobbtoee)
    % Optionally zero the BB->EE part as well - this helps the BB
    % expvals to "sit on the theory curve"
    for j=1:length(r)
      bpwf(j).Cs_l(:,:,6)=0;
    end
  end
  
  % get initial estimate of expectation values ignoring filter/beam
  % effect on BPWFs
  s=calc_expvals(s,inpmods,bpwf);
  r=calc_expvals(r,inpmodr,bpwf);
  
  % calc the mean of sig only sim set
  s=process_simset(s);
  
  % negative values of mean of sig sims are crazy but happens at
  % very low/high ell sometimes - apparently due to numerical
  % precision
  for j=1:length(s)
    nn=sum(cvec(s(j).mean(:,[1,3,4]))<0);
    if(nn>0)
      disp(sprintf('WARNING: col %d: %d negative mean of sim values found - taking abs',j,nn));
      s(j).mean(:,[1,3,4])=abs(s(j).mean(:,[1,3,4]));
    end
  end
  
  % calculate the filter/beam supression factor by comparing
  % expectation values of set of signal only sims to observed band
  % powers
  supfac=calc_supfac(s);
  
  % iteratively recalc the expvals and implied supfac - there is
  % actually very little change
  if(1)
    %figure(4); clf; plot(bpwf(1).l,bpwf(1).Cs_l(:,1,4))
    for i=1:3
      [s,bpwfo]=calc_expvals(s,inpmods,bpwf,supfac);
      [r,bpwfo]=calc_expvals(r,inpmodr,bpwf,supfac);
      supfac=calc_supfac(s);
      
      %hold on; plot(bpwfo(1).l,bpwfo(1).Cs_l(:,1,4),'r')
    end
    % on the last time through we inherit the modified bpwf's and
    % make them permanent
    bpwf=bpwfo;
  end

  if(finalopt.kludgebbsupfac)
    % The B1 and B2/Keck BB sim maps don't match so we need to kludge
    % the BB cross spectra supfacs as the gmean of the auto ones -
    % this is highly approximate at best
    supfac(4).bb=gmean(supfac(1).bb,supfac(2).bb);
    supfac(5).bb=gmean(supfac(1).bb,supfac(3).bb);
  end
  
  case 'no_bpwf'
   % If we don't have band power window functions we can approximate
   % input spectrum expectation values by interpolating input spectrum to
   % nominal center l of each bandpow 
   for j=1:length(s)
     for i=1:size(inpmods.Cs_l,2)
       s(j).expv(:,i)=interp1(inpmods.l,inpmods.Cs_l(:,i),s(j).l);
     end
   end
   for j=1:length(r)
     for i=1:size(inpmodr.Cs_l,2)
       r(j).expv(:,i)=interp1(inpmodr.l,inpmodr.Cs_l(:,i),r(j).l);
     end
   end
   supfac=calc_supfac(s);
   
end

% Plot suppression factor with error bars due to finite number of sims
if(pl>0)
  figure(4); clf
  setwinsize(gcf,1260,840)
  plot_supfac(supfac, lmax,finalopt.pl_cross, mapname);
  gtitle('filter/beam suppression factors',0.98);
end

% filter/beam correction
% (turning this off should result in near identical chi2 results
% versus null model since it just rescales everything)
if(1)

  % apply suppression factors to sims used to generate them and plot
  % - look for points which roughly follow curves
  s=apply_supfac(s,supfac);
  x=process_simset(s);
  for ii=1:length(x)
    x(ii).Cs_l=x(ii).mean;
  end
  plot_aps(pl,5,x,x,inpmods,'sig sim mean after filter/beam correct',lmax,jack,finalopt.pl_cross,mapname,doblind);
 
  % if freq jack replace supfac(1) with mean of 100/150GHz
  if(length(r)==1 & length(supfac)>1)
    supfac(1).tt=mean([supfac(1:2).tt],2);
    supfac(1).ee=mean([supfac(1:2).ee],2);
    supfac(1).bb=mean([supfac(1:2).bb],2);
    supfac(1).te=mean([supfac(1:2).te],2);
    supfac(1).et=mean([supfac(1:2).et],2);
    supfac=supfac(1);
  end
  
  % apply suppression factors to all
  r=apply_supfac(r,supfac);
  sn=apply_supfac(sn,supfac);
  if(exist('snr','var')) 
    snr=apply_supfac(snr,supfac);
  end
  if(exist('snd','var')) 
    snd=apply_supfac(snd,supfac);
  end
  if(exist('d','var')) 
    d=apply_supfac(d,supfac);
  end
  if(exist('sl','var')) 
    sl=apply_supfac(sl,supfac);
  end
  n=apply_supfac(n,supfac);
  db=apply_supfac(db,supfac); 
 
  % make standard plot after filter/beam correct
  plot_aps(pl,6,sn,r,inpmodr,'after filter/beam correct',lmax,jack,finalopt.pl_cross,mapname,doblind);

end

% merge the oversampled bin to final bins (if necessary)
if(length(r(1).l)==68)
  r=aps_bin_merge(r,4);
  sn=aps_bin_merge(sn,4);
  if(exist('snr','var'))
    snr=aps_bin_merge(snr,4);
  end
  if(exist('snd','var'))
    snd=aps_bin_merge(snd,4);
  end
  if(exist('d','var'))
    d=aps_bin_merge(d,4);
  end  
  if(exist('sl','var'))
    sl=aps_bin_merge(sl,4);
  end
  n=aps_bin_merge(n,4);
  s=aps_bin_merge(s,4);
  db=aps_bin_merge(db,4);
end

% replot
%plot_aps(pl,6,sn,r,inpmodr,'after bin merge',lmax,jack);

% finalize output r structure
for i=1:length(r)
  % copy sim spectra to r
  r(i).sim=sn(i).Cs_l;
  if(exist('snr','var'))
    r(i).simr=snr(i).Cs_l;
  end
  if(exist('snd','var'))
    r(i).simd=snd(i).Cs_l;
  end
  if(exist('d','var'))
    r(i).d=d(i).Cs_l;
  end
  if(exist('sl','var'))
    r(i).sigl=sl(i).Cs_l;
  end
  r(i).sig=s(i).Cs_l;
  r(i).noi=n(i).Cs_l;
  r(i).db=db(i).Cs_l;
  
  % rename bandpowers in r
  r(i).real=r(i).Cs_l;
  
  % put the filter/beam correction multiplier into the output
  % structure
  if exist('supfac','var');
    r(i).rwf=supfac(i).rwf;
  end

  % if this is jackknife set expectation value to zero - (what is there
  % at this point is expectation value for signal which could only
  % lead to confusion...)
  if(jack>0 & isfield(r(i),'expv') & ~strcmp(opt.aps.howtojack,'none'))
    r(i).expv=zeros(size(r(i).expv));
  end
end
r=rmfield(r,'Cs_l');

% generate full bandpower covariance matrix
for j=1:length(sn)
  x=size(sn(j).Cs_l);
  snm(j).Cs_l=reshape(sn(j).Cs_l,[x(1)*x(2),x(3)]);
end
Cs_l=vertcat(snm(:).Cs_l);
bpcm=nancov(Cs_l');

% prepend/append to output filename
realspec=sprintf('final/%s%s',realspec,finalopt.daughter);

% write out pngs
if(pl==2)
  for pp=rvec(sort(get(0,'Children')))
    if strcmp(filename{1},'')
      printname=sprintf('%s_final_%i_%i.png',realspec,lmax,pp);
    else
      printname=sprintf('%s_final_%i_%i_%sx%s.png',realspec,lmax,pp,filename{1},filename{2});
    end
    set(0,'CurrentFigure',pp)
    mkpng(printname,1);
  end
end

% make empty var if not exist
if ~exist('supfac','var')
  supfac=[];
end
if ~exist('bpwf','var')
  bpwf=[];
end

% save the final power spectrum results
if ~exist('supfac','var')
  supfac=[];
end
if ~exist('bpwf','var')
  bpwf=[];
end

% try to tersely explain what is in r
r(1).doc={'expv is expectation values for unlensed-LCDM',
       'sim is realizations of lensed-LCDM+noise (scaled)',
       'simr is realizations of lensed-LCDM+noise+r=0.2 (scaled)',
       'sigl is realizations of lensed-LCDM (scaled)'
       'sig is realizations of (quasi) model in inpmod',
       'noi is realizations of noise (scaled)',
       'db is debias which has been applied to real',
       'real is real bandpowers (scaled)',
       'rwf is reciprocal window function - scaling mentioned above'};
       
if(pl==0)
  inpmod=inpmods2;
  opt.finalopt=finalopt;
  
  fname=sprintf('%s',realspec);
  fdir=fileparts(fname);

  if ~exist(fdir,'dir')
    system(['mkdir ' fdir]);
  end
   
  disp(sprintf('saving final specta %s',realspec));
  saveandtest(realspec,'r','bpcm','inpmod','supfac','bpwf','opt')

end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_supfac(supfac,lmax,pl_cross,mapname)
% direct ones don't have error bars
if(~isfield(supfac,'ttel'))
  for i=1:length(supfac)
    supfac(i).ttel=supfac(i).tt*0; supfac(i).tteu=supfac(i).tt*0;
    supfac(i).eeel=supfac(i).tt*0; supfac(i).eeeu=supfac(i).tt*0;
    supfac(i).bbel=supfac(i).tt*0; supfac(i).bbeu=supfac(i).tt*0;
    supfac(i).teel=supfac(i).tt*0; supfac(i).teeu=supfac(i).tt*0;
  end
end

mapname={mapname{:},'cross'};
%freq={'150GHz'};
c=1;
supfac = supfac(pl_cross);
for n=1:length(supfac)
  subplot(2,3,n)
  errorbar2(supfac(n).l,supfac(n).tt/c,supfac(n).ttel/c,supfac(n).tteu/c,'b-');
  ylim([0,1])
  hold on  
  errorbar2(supfac(n).l,supfac(n).ee/c,supfac(n).eeel/c,supfac(n).eeeu/c,'r-');
  errorbar2(supfac(n).l,supfac(n).te/c,supfac(n).teel/c,supfac(n).teeu/c,'g:');
  errorbar2(supfac(n).l,supfac(n).bb/c,supfac(n).bbel/c,supfac(n).bbeu/c,'m-');
  ylim([0,1])
  hold off
  xlim([0,lmax]);
  title(strrep(mapname{n},'_','\_'));
  
  subplot(2,3,n+3)
  errorbar2(supfac(n).l,supfac(n).tt/c,supfac(n).ttel/c,supfac(n).tteu/c,'b-');
  hold on
  errorbar2(supfac(n).l,supfac(n).ee/c,supfac(n).eeel/c,supfac(n).eeeu/c,'r-');
  errorbar2(supfac(n).l,supfac(n).te/c,supfac(n).teel/c,supfac(n).teeu/c,'g:');
  errorbar2(supfac(n).l,supfac(n).bb/c,supfac(n).bbel/c,supfac(n).bbeu/c,'m-');
  hold off
  set(gca,'YScale','log');
  yl=ylim; ylim([0.001, 1]);
  xlim([0,lmax]);
  title('blue=TT, red=EE, magenta=BB, green=TE','fontsize',9);
  grid on;
  
  if(0)
  subplot(3,3,n)
  errorbar2(supfac(n).l,supfac(n).te/c,supfac(n).teel/c,supfac(n).teeu/c,'b-');
  hold on
  plot(supfac(n).l,supfac(n).xx/c,'r-');
  hold off
  ylim([0 1])
  xlim([0,lmax]);
  title(freq{n}); 
  grid on;

  subplot(3,3,n+3)
  errorbar2(supfac(n).l,supfac(n).te/c,supfac(n).teel/c,supfac(n).teeu/c,'b-');
  hold on
  plot(supfac(n).l,supfac(n).xx/c,'r-');
  hold off       
  yl=ylim; ylim([0.001, 1]);
  set(gca,'YScale','log');
  xlim([0,lmax]);
  title('blue=actual TE, red= gmean TE');
  grid on;

  subplot(3,3,n+6)
  plot(supfac(n).l,(supfac(n).xx-supfac(n).te)./supfac(n).xx,'b.-');
  ylim([-.25, .25])
  xlim([0,lmax]);
  title('Fractional difference');
  grid on;
  end

end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_aps(pl,fig,sn,r,inpmod,gt,lmax,jack,pl_cross,mapname,doblind)

if(pl>0)
  figure(fig); clf
  setwinsize(gcf,1260,840)

  if(length(r)>1) 
    for j=1:length(pl_cross)
      if j~=3
        plot_aps_sub(j,sn(pl_cross(j)),r(pl_cross(j)),inpmod,mapname{j},lmax,jack,doblind);
      else
        plot_aps_sub(3,sn(pl_cross(3)),r(pl_cross(3)),inpmod,'cross',lmax,jack,doblind);
      end
    end
  else
    plot_aps_sub(1,sn(1),r(1),inpmod,'150GHz',lmax,jack,doblind);
  end
  
  gtitle(gt,0.98);

end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_aps_sub(n,sn,r,inpmod,freq,lmax,jack,doblind)

if ~jack
  % ylim so we can see the signal spectra
  if (lmax == 500)
    yrange=[-500,8000;-150,150;-5,30;-.2,.2;-3,3;-.2,.2;-150,150;-3,3;-.2,.2];
  elseif (lmax == 200)
    yrange=[-500,7000;-50,10;-.2,1.2;-.05,0.1;-10,10;-.15,.15;-50,10;-10,10;-.15,.15];
  end
else
  if (lmax==500)
    yrange=[-1000,1000;-100,100;-50,50;-50,50;-100,100;-50,50;-150,150;-150,150;-150,150]/1000;
  elseif (lmax==200)
    yrange=[-1000,1000;-100,100;-50,50;-50,50;-100,100;-50,50;-150,150;-150,150;-150,150]/5000;
  end
end
  
lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];

for i=1:size(sn.Cs_l,2)
  z=(j(i)-1)*4+n+k(i);
  subplot_grid(6,4,z);

  plot(sn.l,squeeze(sn.Cs_l(:,i,:)),'LineWidth',0.5);
  hold on;
  % don't show real data if doblind and B is involved
  if ~doblind | isempty(strfind(lab{i},'B'))
    plot(r.l,r.Cs_l(:,i),'k','LineWidth',2);
  end
  plot(inpmod.l,inpmod.Cs_l(:,i),'r','LineWidth',2);
  hold off;
  xlim([0,lmax]);
  ylim(yrange(i,:));
  ylabel(lab{i});
  subplot_grid2(6,4,z);
  if(i==1)
    title(strrep(freq,'_','\_'));
  end
end


return
