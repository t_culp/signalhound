bicep_analysis/Matlab:
---------------------------------------------------------
A BICEP-specific branch of the
quad_analysis/Matlab CMB polarization analysis pipeline


To access it (assuming you don't already have dir quad_analysis!):

 setenv CVS_RSH ssh
 cvs -d find.uchicago.edu:/home/cvsroot co -r bicep quad_analysis/Matlab
 mv quad_analysis/Matlab bicep_analysis
 rm -rf quad_analysis

You can then freely modify files, "cvs add", "cvs commit", "cvs update -d" etc
in the bicep_analysis directory - everything you do occurs on the bicep branch.

You will not see my further changes on the main branch unless you explicitly 
cross merge them.

Using the useful tool "tkcvs" you can visualize the history of any given file 
and see the branch - look at reduc_initial.m where I made a dummy bicep 
specific mod. There are other gui tools for cvs.
