function paper_plots_b2_respap14(pl, dopr)
% paper_plots_b2_respap14(pl)
%
% Make plots for B2 2014 results paper
%
% This file should know how to make all of the plots and quoted numbers
% for the upcoming paper allowing us to:
% 1) maintain a common "look and feel"
% 2) anyone can tweak any plot rather then having to refer to its
% "keeper"
%
% Some notes:
%
% - It's probably going to be necessary to always run this script
% interactively as batch running seems to produce mysteriously
% different plots.
% eps->pdf is by far the best for a paper being infinitely rescalable
% - if it really won't work then pdflatex also eats png - jpg is
% *crap*
global dopres;
if exist('dopr','var') & dopr
  set(0,'defaultlinelinewidth',1.0);
  set(0,'DefaultAxesFontSize',14);
%    set(0,'defaulttextfontsize',14);
  dopres = 1;
else
  dopres = 0;
end
  
% make T/Q/U map plot
if(any(pl==1))
  make_mapplot;
end

% make 6 panel power spectrum plot
if(any(pl==2))
  make_powspecres;
end

% make power spectrum result data file
if(any(pl==101))
  make_powspecrestable;
end

% make bpwf data file
if(any(pl==102))
  make_bpwftable;
end

% make H-L data files
if (any(pl==103))
  make_hlproductfiles
end  

% make input model files
if any(pl==104)
  make_inputspectrafiles
end

% make sys uncer file
if any(pl==105)
  make_sysuncerfile
end

%Make Likelihood-derived error bar plot
if any(pl==106)
  make_likelihood_bperrors
end

% make E/B map plot
if(any(pl==3))
  make_ebplot;
end

% make BB devs plots
if(any(pl==4))
  make_bbdevs;
end

% make B2xB1 power spectrum plot
if(any(pl==5))
  make_b2xb1;
end

% foreground projections plot
if(any(pl==6))
  make_foreplot;
end

% beammap sims plot
if(any(pl==7))
  make_beammapsimsplot;
end

% r limit with fg model debias
if(any(pl==8))
  make_rcons_fgdebias;
end

% B2 vs. other experiments
if(any(pl==9))
  make_b2_vs_world;
end

% r limit
if(any(pl==10))
  make_rconstraint;
end

% jackknife table and pte histograms
if(any(pl==11))
  make_jacktab;
end

% make sigmas numbers etc
if(any(pl==12))
  make_quotedstats;
end

% r vs lensing
if(any(pl==13))
  make_rvsl;
end

% compare B2xB2, B2xB1 and B2xK13
if(any(pl==14))
  make_speccomp;
end

% make systematics levels plot
if(any(pl==15))
  make_syslevels;
end

% make color constraint plot
if(any(pl==16))
  make_colorconstraint;
end

% make rlikelihood file output
if(any(pl==17))
  make_rlikelihood_file;
end

% make quoted stats as to r equivalent of pntsrc and sync
if(any(pl==18))
  make_ps_rmod;
end

% make suppression factor plot
if(any(pl==19))
  make_supfac;
end

% make sync projection plot
if(any(pl==20))
  make_syncplot;
end

% make field outline ASCII file
if(any(pl==21))
  make_fieldoutline;
end

if (any(pl==22))
  make_syncdustmix;
end

if (any(pl==23))
  make_rcons_allfg
end

if (any(pl==24))
  make_prl_cover;
end

% make 150 GHz band power spectrum ASCII file
if(any(pl==25))
  make_fts_asciifile;
end
  
return

%%%%%%%%%%%%%%%%%%%%
function lw=get_lw()
lw=1;
return

%%%%%%%%%%%%%%%%%%%%%
function make_quotedstats
[dum,dum,dum, dum, filename]=get_b2xb2
% B2xB2 detection significances as quoted in sec 7.1
rfpopt.chibins=get_chibins;

disp('PTE of B2xB2 spectrum given lensed-LCDM model');

rfpopt.nsim=1e6; % 1e8 is required here to get good stats on chi2 - chi1 seems impossible to compute.
                 % 1e8 may not be runnable on odyssey login nodes -works on spud
rfpopt.chibins{4}=2:10;
disp(sprintf('Bins %d through %d:',rfpopt.chibins{4}([1,end])));
reduc_final_bb_pte(filename,rfpopt,0);
rfpopt.chibins{4}=2:6;
disp(sprintf('Bins %d through %d:',rfpopt.chibins{4}([1,end])));
reduc_final_bb_pte(filename,rfpopt,0);
rfpopt.nsim=1e6;
rfpopt.chibins{4}=7:8;
disp(sprintf('Bins %d through %d:',rfpopt.chibins{4}([1,end])));
reduc_final_bb_pte(filename,rfpopt,0);
disp(' ')

% do B2xB1
load final/0751x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix.mat
rfpopt.chibins{4}=2:6;
rfpopt.nsim=1e6;
rfpopt.crossspec=1;

% no detection really - not quoted in sec 8.4
disp('PTE of B2xB1150 spectrum given lensed-LCDM model');
disp(sprintf('Bins %d through %d:',rfpopt.chibins{4}([1,end])));
reduc_final_bb_pte(r(5),rfpopt,0);

% surprisingly strong detection - quoted in sec 9.1
disp('PTE of B2xB1100 spectrum given lensed-LCDM model');
disp(sprintf('Bins %d through %d:',rfpopt.chibins{4}([1,end])));
reduc_final_bb_pte(r(4),rfpopt,0);

% combined also detection - quoted in section 10
% combine as if B1 100&150GHz maps had been added
rc(1)=r(1); rc(1).w1=[]; rc(1).w2=[];
rc(2)=comb_spec(r(2),r(3)); % combination of B1 auto spectra - not used
rc(3)=comb_spec(r(4),r(5)); % combination of B2xB1 cross spectra - used

disp('PTE of B2xB1c spectrum given lensed-LCDM model');
disp(sprintf('Bins %d through %d:',rfpopt.chibins{4}([1,end])));
reduc_final_bb_pte(rc(3),rfpopt,0);


% get spectral jack ptes which are mentioned in secs 8.4, 9.1 and 10
rj=specjack(r,1,3,5,1); % compare against diff of simr realizations
rj=calc_chi(rj,rfpopt.chibins,[],1);
rj=calc_devs(rj,rfpopt.chibins,1);
disp(sprintf('B2xB2-B2xB1_150 Chi2 and Chi1 pte %.3f %.3f',rj(1).ptes(4),rj(1).ptes_sumdev(4)));

rj=specjack(r,1,2,4,1);
rj=calc_chi(rj,rfpopt.chibins,[],1);
rj=calc_devs(rj,rfpopt.chibins,1);
disp(sprintf('B2xB2-B2xB1_100 Chi2 and Chi1 pte %.3f %.3f',rj(1).ptes(4),rj(1).ptes_sumdev(4)));

rj=specjack(rc,1,2,3);
rj=calc_chi(rj,rfpopt.chibins,[],1);
rj=calc_devs(rj,rfpopt.chibins,1);
disp(sprintf('B2xB2-B2xB1c Chi2 and Chi1 pte %.3f %.3f',rj(1).ptes(4),rj(1).ptes_sumdev(4)));

return

%%%%%%%%%%%%%%%%%%%%%
function make_mapplot

load maps/1450/real_a_filtp3_weight3_gs_dp1102_jack0
map=make_map(ac,m,coaddopt);
maps=cal_coadd_maps(map,get_ukpervolt);

load maps/1450/real_a_filtp3_weight3_gs_dp1102_jack3
map=make_map(ac,m,coaddopt);
map=jackknife_map(map);
mapj=cal_coadd_maps(map,get_ukpervolt);

fig=figure;
clf;
dim=get_sixpaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% T

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,maps.T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
title('T signal');

ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapj.T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax2,'FontSize',7,'FontWeight','normal');
set(ax2,'Xticklabel',[],'Yticklabel',[]);
title('T jackknife');

% Q

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,maps.Q,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax3,'FontSize',7,'FontWeight','normal');
set(ax3,'Xticklabel',[]);
title('Q signal');

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapj.Q,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax4,'FontSize',7,'FontWeight','normal');
set(ax4,'Xticklabel',[],'Yticklabel',[]);
title('Q jackknife');

% U

ax5=axes('Position',[dim.x1,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,maps.U,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax5,'FontSize',7,'FontWeight','normal');
title('U signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

ax6=axes('Position',[dim.x2,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,mapj.U,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax6,'FontSize',7,'FontWeight','normal');
set(ax6,'Yticklabel',[]);
title('U jackknife');

% colorbars

ax7 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax7,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-150,150,1000),repmat(linspace(-150,150,1000)',1,10));
colormap hot;
freezeColors;
set(ax7,'Xtick',[]);
set(ax7,'YDir','normal','YAxisLocation','right','YTick',[-100 0 100]);

ax8 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax8,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax8,'Xtick',[]);
set(ax8,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

ax9 = axes('Position',[dim.x3,dim.y3,dim.cbar/dim.W,dim.ymap]);
set(ax9,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax9,'Xtick',[]);
set(ax9,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

% colorbar units

ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y3 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% this works way better than direct print to pdf
print -depsc2 paper_plots/tqu_maps.eps
fix_lines('paper_plots/tqu_maps.eps')
!epstopdf paper_plots/tqu_maps.eps

return

%%%%%%%%%%%%%%%%%%%%%
% used by make_mapplot above
function dim=get_sixpaneldims(m)
% Parameters for six-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 3 * dim.maph + dim.wide + 3 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 2 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 2 * dim.maph + 2 * dim.med) / dim.H; % Bottom edge of row 1.
dim.y2 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y3 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return

%%%%%%%%%%%%
function [r,im,ml, bpwf,filename,mt,rscaling]=get_b2xb2
% Single function fetches official B2xB2 spectra and models for
% plotting etc - one point switch!

%  filename = '0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf_rbc_lensfix.mat';
filename = '0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf_rbc.mat';
load(['final/',filename])
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN;
im=inpmod;

% get the lensing model to compare to lensing sims
%  ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing_lensfix.fits');
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing.fits');

% get the tensor only model, mainly to plot the EE tensor only
% this is currently not a fits file so be care full about the data columns
% 
mt=load('aux_data/official_cl/camb_planck2013_r0p1_tenscls.dat');
% extend it to the same l range as the other inputs. Note quite sure
% why it is not to start with. However, checked that tensor B is the same as in im
mt = interp1(mt(:,1),mt,ml.l);

%  rscaling = '0751/xxx4_a_filtp3_weight3_gs_dp1100_jack0_xxx5_a_filtp3_weight3_gs_dp1100_jack0_xxx6_a_filtp3_weight3_gs_dp1100_jack0_matrix_lensfix.mat'
rscaling = '0751/xxx4_a_filtp3_weight3_gs_dp1100_jack0_xxx5_a_filtp3_weight3_gs_dp1100_jack0_xxx6_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat'

return

%%%%%%%%%%%%
function paper_print_generic_header(fileID)
  %Use this function in every data release file generating script at the top
  fprintf(fileID, '# BICEP2 2014 Data Release\n');
  fprintf(fileID, '# The BICEP2 Collaboration, BICEP2 2014 I: DETECTION OF B-mode POLARIZATION AT DEGREE ANGULAR SCALES\n');
  fprintf(fileID, '# http://bicepkeck.org/\n#\n');
return

%%%%%%%%%%%%
function make_rlikelihood_file
  fname = 'B2_3yr_rlikelihood_20140314.txt';

  [dum,dum,dum, dum, filename,dum,rscaling]=get_b2xb2;
  rfropt.rvals=0:.001:0.75;
  rfropt.rscaling=rscaling;
  [r,rfropt] = reduc_final_rlim_direct(filename,rfropt,0);
  format shortE
  fileID = fopen(fname, 'w');
  paper_print_generic_header(fileID); %Header
  fprintf(fileID, '# File: ');
  fprintf(fileID, fname);
  fprintf(fileID, '\n');
  fprintf(fileID, '# Date: 2014-03-14\n#\n');
  fprintf(fileID, '# BICEP2 likelihood for r\n');
  fprintf(fileID, '# This text file contains the tabulated likelihood for the tensor-to-scalar ratio, r, derived from the BICEP2 BB spectrum.\n');
  fprintf(fileID, '# Calculated via the "direct likelihood" method described in Section 9.3.1 of Barkats et al. and Section 11.1 of BICEP2 2014 I.\n');
  fprintf(fileID, '# This likelihood curve corresponds to the black curve from the middle panel of Figure 10 from BICEP2 2014 I.\n');
  fprintf(fileID, '#\n# Columns: r, Likelihood(r)\n');
  fprintf(fileID, '  %5.3f\t%6.4e\n', [r.rvals; r.likeBB']);
  fclose(fileID);

  return

%%%%%%%%%%%%%%%%%%%%%
function make_sysuncerfile
  [r,im,ml, bpwf]=get_b2xb2;
  ellbins = 2:10;
  r = add_sys_uncer(r);

  filename = 'B2_3yr_sysuncer_20140314.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2014-03-14 \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# BICEP2 systematic uncertainties from absolute calibration and beam width\n');
  fprintf(fh,'# Systematic uncertainties are expressed as fractional errors on bandpowers [ell*(ell+1)C_{ell}/(2*pi)].\n');
  fprintf(fh,'# Discussed in Sections 4.7 and 6.3 of BICEP2 2014 I.\n');
  fprintf(fh,'\n');
  fprintf(fh,'# 1. Fractional uncertainty in ell*(ell+1)C_{ell}/(2*pi) from absolute calibration\n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, TB, EB\n');
  for i = ellbins
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,'  %3d  %1.1f  %3d  %1.4f  %1.4f  %1.4f  %1.4f  %1.4f  %1.4f\n',...
      lmin, r.lc(i), lmax, r.abscal_uncer(i, :));
  end

  fprintf(fh,'\n');
  fprintf(fh,'# 2. Fractional uncertainty in ell*(ell+1)C_{ell}/(2*pi) from beam width calibration\n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, TB, EB\n');
  for i = ellbins
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,'  %3d  %1.1f  %3d  %1.4f  %1.4f  %1.4f  %1.4f  %1.4f  %1.4f\n',...
      lmin, r.lc(i), lmax, r.beam_uncer(i, :));
  end


  fclose(fh);

return

%%%%%%%%%%%%%%%%%%%%%
function make_inputspectrafiles
  [r,im,ml, bpwf]=get_b2xb2;
  ellbins = 2:10;

  filename = 'B2_3yr_camb_planck_lensed_uK_20140314.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2014-03-14 \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# This file contains the LCDM power spectra that are derived from Planck (2013) parameters and\n');
  fprintf(fh,'# used for data vs. model comparisons in the pipeline and in the paper. These spectra are not fits\n');
  fprintf(fh,'# to the BICEP data. The spectra are in [ell*(ell+1)*C_{ell}/(2*pi)] in units of uK^2.\n');
  fprintf(fh, '# These spectra include the effect of gravitational lensing.\n');
  fprintf(fh, '# No tensor modes are included.\n');
  fprintf(fh, '# Note that the ordering of the spectra is different from the standard CAMB output:\n');
  fprintf(fh,'# Columns: l, TT, TE, EE, BB, TB, EB\n');
  for i = 1:1901
    fprintf(fh,'%3d   %1.6e   %1.6e   %1.6e   %1.6e   %1.6e   %1.6e  \n',ml.l(i), ml.Cs_l(i,1:6));
  end
  fclose(fh);



  filename = 'B2_3yr_camb_planck_withB_uK_20140314.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2014-03-14 \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# This file contains the LCDM power spectra that are derived from Planck (2013) parameters and\n');
  fprintf(fh,'# used for data vs. model comparisons in the pipeline and in the paper. These spectra are not fits\n');
  fprintf(fh,'# to the BICEP data. The spectra are in [ell*(ell+1)*C_{ell}/(2*pi)] in units of uK^2.\n');
  fprintf(fh, '# These spectra do not include the effect of gravitational lensing.\n');
  fprintf(fh, '# r=0.1 tensor modes are included in the BB spectrum only.\n');
  fprintf(fh, '# Note that the ordering of the spectra is different from the standard CAMB output:\n');
  fprintf(fh,'# Columns: l, TT, TE, EE, BB, TB, EB\n');
  for i = 1:1501
    fprintf(fh,'%3d   %1.6e   %1.6e   %1.6e   %1.6e   %1.6e   %1.6e  \n',im.l(i), im.Cs_l(i,1:6));
  end
  fclose(fh);

  filename = 'B2_3yr_cl_expected_lensed_20140314.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2014-03-14 \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# This file contains the expected bandpowers for BICEP2 associated to the LCDM power spectra that are derived from Planck (2013) parameters and\n');
  fprintf(fh,'# used for data vs. model comparisons in the pipeline and in the paper. These spectra are not fits\n');
  fprintf(fh,'# to the BICEP data. The spectra are in [ell*(ell+1)*C_{ell}/(2*pi)] in units of uK^2.\n');
  fprintf(fh, '# These spectra include the effect of gravitational lensing.\n');
  fprintf(fh, '# No tensor modes are included.\n');
  fprintf(fh, '# These expected bandpowers were computed by multiplying the power spectra by the bandpower window functions.\n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, TB, EB\n');
  r = calc_expvals(r, ml, bpwf);
  for i = ellbins
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,'%3d   %1.1f   %3d   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e \n',lmin, r.lc(i), lmax, r.expv(i,:));
  end
  fclose(fh);

  filename = 'B2_3yr_cl_expected_withB_20140314.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2014-03-14 \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# This file contains the expected bandpowers for BICEP2 associated to the LCDM power spectra that are derived from Planck (2013) parameters and\n');
  fprintf(fh,'# used for data vs. model comparisons in the pipeline and in the paper. These spectra are not fits\n');
  fprintf(fh,'# to the BICEP data. The spectra are in [ell*(ell+1)*C_{ell}/(2*pi)] in units of uK^2.\n');
  fprintf(fh, '# These spectra do not include the effect of gravitational lensing.\n');
  fprintf(fh, '# r=0.1 tensor modes are included in the BB spectrum only.\n');
  fprintf(fh, '# These expected bandpowers were computed by multiplying the power spectra by the bandpower window functions.\n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, TB, EB\n');
  r = calc_expvals(r, im, bpwf);
  for i = ellbins
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,'%3d   %1.1f   %3d   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e \n',lmin, r.lc(i), lmax, r.expv(i,:));
  end
  fclose(fh);


return

function make_likelihood_bperrors
  [r,im,ml, bpwf]=get_b2xb2;
  ellbins = 2:10;
  for iBin = 2:10
    like = hl_single_bin_likelihood(r, iBin, 0, 4, 1600, 0);
    sigma_here = calc_sigma2(like.likelihood_bins, like.likelihood);
    sigma_plus(iBin) = sigma_here.PlusSigma;
    sigma_minus(iBin) = sigma_here.MinusSigma;
    max_like(iBin) = sigma_here.max;
  end

line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
plot(im.l,ml.Cs_l(:,4),'r');
% plot B2xB2
h1=errorbar2(r(1).lc(:,4),r(1).real(:,4),std(r(1).simr(:,4,:),[],3),'k.');
h2 = errorbar2(r(1).lc(ellbins,4) + 5, max_like(ellbins), ...
  sigma_minus(ellbins), sigma_plus(ellbins), 'b.');
set(h1(2),'MarkerSize',12);
hold off
xlim([0,200]); ylim([-.005,.03]);
%ptitle('BB');
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
legend([h1(2),h2(2)],{'Traditional Symmetric Error Bar', 'Likelihood 68% Interval'},'Location','northwest');
legend boxoff

axis square

keyboard

return

%%%%%%%%%%%%%%%%%%%%%
function make_hlproductfiles
  [r,im,ml, bpwf]=get_b2xb2;
  ellbins = 2:10;
  %Default offdiagonal # in bpcm for B2 is 1 -- IDB 2014-02-26
  p = hl_create_data_products(r, ellbins, 'TEB', 1); 

  %nan out T information
  p.C_fl(1,:,:) = nan;
  p.C_fl(:,1,:) = nan;  
  p.C_l_hat(1,:,:) = nan;
  p.C_l_hat(:,1,:) = nan;  
  p.N_l(1,:,:) = nan;
  p.N_l(:,1,:) = nan;  

  format shortE
  filename = 'B2_3yr_likelihood_bandpowers_20140314.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2014-03-14 \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# BICEP2 actual and simulated bandpowers necessary for Hamimeche-Lewis bandpower likelihood approximation.\n');
  fprintf(fh,'# See Section 9.1 of Barkats et al. (2014) for details.\n');
  fprintf(fh, '# Note that a separate file containing the likelihood for the tensor-to-scalar ratio, computed without this approximation, is available (B2_3yr_rlikelihood_20140314.txt).\n');
  fprintf(fh, '#\n');
  fprintf(fh, '# Bandpowers are organized into one matrix per ell bin.\n');
  fprintf(fh, '# Each matrix is symmetric 3x3:\n');
  fprintf(fh, '#         | TT  TE  TB |\n');
  fprintf(fh, '#         | TE  EE  EB |\n');
  fprintf(fh, '#         | TB  EB  BB |\n');
  fprintf(fh, '# However, for this release bandpowers TT, TE, and TB are not available because our constrained T simulations do not include appropriate sample variance. See Section 5.1.1 of BICEP2 2014 I for more details.\n');
  fprintf(fh, '# Note also that our polarization angle calibration removes any polarization rotation from the EB spectrum.\n');
  fprintf(fh, '# In all cases, bandpowers represent ell*(ell+1)C_{ell}/(2*pi).\n');
  fprintf(fh, '# Units are uK^2 CMB.\n\n');

  fprintf(fh, '# 1.\n# Fiducial model bandpowers D^f\n');
  fprintf(fh, '# Calculated as the ensemble average of signal+noise simulations.\n');
  fprintf(fh, '# Noise bias is included.\n');
  for iBin = 1:length(ellbins)
    fprintf(fh, ['\n# Bin ' num2str((iBin)) ' (ell~' sprintf('%1.1f', r.lc(ellbins(iBin))) ')\n']);
    fprintf(fh, '   %1.4e   %1.4e   %1.4e\n', (p.C_fl(:,:,iBin)));
  end

  fprintf(fh, '\n# 2.\n');
  fprintf(fh, '# Data bandpowers \\hat{D}\n');
  fprintf(fh, '# Real data bandpowers from BICEP2 three-year result.\n');
  fprintf(fh, '# These differ from bandpowers displayed in Figure 2 of BICEP2 2014 I, because noise bias is included here.\n');
  for iBin = 1:length(ellbins)
    fprintf(fh, ['\n# Bin ' num2str((iBin)) ' (ell~' sprintf('%1.1f', r.lc(ellbins(iBin))) ')\n']);
    fprintf(fh, '   %1.4e   %1.4e   %1.4e\n', (p.C_l_hat(:,:,iBin)));
  end

  fprintf(fh, '\n# 3.\n'); 
  fprintf(fh, '# Noise bandpowers N, including instrumental noise and E->B leakage.\n');
  fprintf(fh, '# Calculated from the ensemble average of noise-only simulations (and signal-only simulations for E->B leakage).\n');
  fprintf(fh, '# Subtracting these bandpowers from the real data bandpowers listed above will recover the BICEP2 three-year bandpowers from Figure 2 of BICEP2 2014 I.\n');
  for iBin = 1:length(ellbins)
    fprintf(fh, ['\n# Bin ' num2str((iBin)) ' (ell~' sprintf('%1.1f', r.lc(ellbins(iBin))) ')\n']);
    fprintf(fh, '   %1.4e   %1.4e   %1.4e\n', (p.N_l(:,:,iBin)));
  end

  fclose(fh);


  filename = 'B2_3yr_bpcm_no-sysuncer_20140314.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);
  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2014-03-14 \n');
  fprintf(fh,'# \n');
  fprintf(fh,['# Bandpower covariance matrix M_{cc''}\n']);
  fprintf(fh,'# Covariance for bandpowers expressed as ell*(ell+1)C_{ell}/(2*pi).\n');
  fprintf(fh,'# Units are uK^4 CMB.\n');
  fprintf(fh,'# Statistical uncertainties only. Does not include the systematic uncertainty from absolute gain and beam width calibration.\n');
  fprintf(fh, '# These calibration uncertainties are included in the direct likelihood method used in Section 11.1 of BICEP2 2014 I, but they have a negligible impact.\n');
  fprintf(fh,'#\n');
  fprintf(fh,'# Symmetric 54x54 matrix. \n');
  fprintf(fh,'# Ordering of the columns/rows is (TT, EE, BB, TE, EB, TB) for the first ell bin, then (TT, EE, BB, TE, EB, TB) for the second ell bin, etc.\n');
  fprintf(fh, '# However, for this data release we do not include appropriate sample variance for TT, TE, and TB because the simulations constrain T to match the observed sky. You should not use these spectra for constraining cosmology.\n\n');
  fprintf(fh, '%5.3e  %5.3e  %5.3e  %5.3e  %5.3e  %5.3e\n', p.M_f);
  fclose(fh);

return

%%%%%%%%%%%%%%%%%%%%%
function make_bpwftable
  [r,im,ml, bpwf]=get_b2xb2;
%  keyboard %test for bpwf

  mkdir('windows');
  for i=2:10
    filename = sprintf('B2_3yr_bpwf_bin%d_20140314.txt',i-1);
    fh = fopen(['windows/' filename],'w') ;
    paper_print_generic_header(fh);
    fprintf(fh,'# File: ');
    fprintf(fh, filename);
    fprintf(fh, ' \n');
    fprintf(fh,'# Date: 2014-03-14 \n');
    fprintf(fh,'# \n');
    fprintf(fh,'# BICEP2 bandpower window functions for bin #%d (ell~%5.1f)\n',i-1, r.lc(i)) ;
    fprintf(fh,'# See Section 6.3 of BICEP2 2014 I.\n');
    fprintf(fh,'# \n');
    fprintf(fh,'# Columns: ');
    fprintf(fh,'ell   TT->TT     TE->TE     EE->EE     BB->BB \n');
    for j =  1:size(bpwf.l,2)
      fprintf(fh,'%3d   %1.2e   %1.2e   %1.2e   %1.2e\n',bpwf.l(j),bpwf.Cs_l(j,i,1:4));
    end
    fclose(fh);
  end
return


%%%%%%%%%%%%%%%%%%%%%
function make_powspecrestable

  for i=1:6
    chibins{i}=[2:10]; % use all plotted bins
  end
  [r,im,ml]=get_b2xb2;
  r=calc_chi(r,chibins,[],1);

  filename = 'B2_3yr_bandpowers_20140314.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);

  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2014-03-14 \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# BICEP2 bandpowers \n');
  fprintf(fh,'# This text file contains the BICEP2 TT, TE, EE, BB, TB, and EB bandpowers [ell*(ell+1)*C_{ell}/(2*pi)] and uncertainties. \n');
  fprintf(fh,'# They correspond to Figure 2 of BICEP2 2014 I.\n');
  fprintf(fh,'# Uncertainties are statistical only, the standard deviation of the constrained lensed-ΛCDM+noise simulations. \n');
  fprintf(fh,'# Uncertainties are calculated as the square root of diagonal elements of the bandpower covariance matrix. \n');
  fprintf(fh,'# The nature of the simulations constrains T to match the observed sky. Thus TT, TE, and TB uncertainties do not include appropriate sample variance.\n');
  fprintf(fh, '# Sample variance for a tensor BB signal is not included either.\n');
  fprintf(fh,'# The calibration procedure uses TB and EB to constrain the polarization angle. Thus TB and EB cannot be used to measure astrophysical polarization rotation.\n');
  fprintf(fh,'# The units are uK^2. \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, TB, EB, dTT, dTE, dEE, dBB, dTB, dEB \n');

  for i = chibins{1}
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,['%3d  %3.1f  %3d   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   ' ...
    '%1.2e \n'],lmin, r.lc(i), lmax,r.real(i,:),r.derr(i,:));
  end

  fclose(fh);

  filename = 'B2_3yr_bandpowers_withr_20140619.txt';
  fh = fopen([filename],'w') ;
  paper_print_generic_header(fh);

  fprintf(fh,'# File: ');
  fprintf(fh, filename);
  fprintf(fh, ' \n');
  fprintf(fh,'# Date: 2014-06-19 \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# BICEP2 bandpowers \n');
  fprintf(fh,'# This text file contains the BICEP2 TT, TE, EE, BB, TB, and EB bandpowers [ell*(ell+1)*C_{ell}/(2*pi)] and uncertainties. \n');
  fprintf(fh,'# Uncertainties are statistical only, the standard deviation of the constrained lensed-ΛCDM+noise+r=0.2 simulations. \n');
  fprintf(fh,'# Uncertainties are calculated as the square root of diagonal elements of the bandpower covariance matrix. \n');
  fprintf(fh,'# The nature of the simulations constrains T to match the observed sky. Thus TT, TE, and TB uncertainties do not include appropriate sample variance.\n');
  fprintf(fh, '# For BB (and EB) the sample variance of an r=0.2 tensor signal is included.\n');
  fprintf(fh, '# Thus, the BB bandpower uncertainties correspond to Figure 10 (left) of BICEP2 2014 I.\n');
  fprintf(fh,'# The calibration procedure uses TB and EB to constrain the polarization angle. Thus TB and EB cannot be used to measure astrophysical polarization rotation.\n');
  fprintf(fh,'# These values are for plotting purposes only. For cosmological parameter analysis we provide a bandpower likelihood at http://bicepkeck.org/. The likelihood is also available in cosmological parameter fitting software (e.g. CosmoMC).\n');
  fprintf(fh,'# The units are uK^2. \n');
  fprintf(fh,'# \n');
  fprintf(fh,'# Columns: lmin, lcenter, lmax, TT, TE, EE, BB, TB, EB, dTT, dTE, dEE, dBB, dTB, dEB \n');

  for i = chibins{1}
    lmin = r.l(i) - 35/2 +1;
    lmax = r.l(i) + 35/2;
    fprintf(fh,['%3d  %3.1f  %3d   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   %1.2e   ' ...
    '%1.2e \n'],lmin, r.lc(i), lmax,r.real(i,:), std(r.simr(i,:,:), [], 3)  );
  end
  fclose(fh);


return

%%%%%%%%%%%%%%%%%%%%%
function make_powspecres

for i=1:6
  chibins{i}=[2:10]; % use all plotted bins
end

% For TT we use legancy 1450 points... this is a blatant kludge but
% who cares about TT?
load final/1450x1350/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_overrx
r=r(1);
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN; r=calc_chi(r,chibins,[],1);
rt=r;

% get the real points
[rs,im,ml,dum,dum,mt]=get_b2xb2;
rs=calc_chi(rs,chibins,[],1);

% get the jackknife spectra - replace with 0751 when ready
%load final/1450x1350/real_a_filtp3_weight3_gs_dp1102_jack3_real_a_filtp3_weight3_gs_dp1102_jack31_pureB_overrx.mat
%r=r(1);
load final/0751/real_a_filtp3_weight3_gs_dp1102_jack3_pureB_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN; r=calc_chi(r,chibins,[],1);
rj=r;

clf; setwinsize(gcf,1000,600)

ms=10; % marker size

subplot(4,3,1); a1=gca;
% plot the least important things first
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,1),'r');
plot(im.l,im.Cs_l(:,1)+2*mt(:,2),'r--');
plot(im.l,2*mt(:,2),'r--');
h=errorbar2(rs.lc(:,1),rs.real(:,1),rt.derr(:,1),'k.');
set(h(2),'MarkerSize',ms); set(h(1),'Color',[0.7,0.7,0.7]);
hold off
xlim([0,330]); ylim([-1000,7000]);
%ptitle(sprintf('TT - \\chi^2 PTE = %.2f',rt.ptes(1)));
ptitle(sprintf('TT'));
ylabel('l(l+1)C_l/2\pi [\muK^2]')
subplot(4,3,4); a2=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rj.lc(:,1),rj.real(:,1),rj.derr(:,1),'b.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-1,1]);
ptitle(sprintf('TT jack - \\chi^2 PTE = %.2f',rj.ptes(1)),0.03,0.1);
make_axis_pair(a1,a2);

subplot(4,3,2); a1=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,2),'r');
plot(im.l,im.Cs_l(:,2)+2*mt(:,5),'r--');
plot(im.l,2*mt(:,5),'r--');
h=errorbar2(rs.lc(:,2),rs.real(:,2),rs.derr(:,2),'k.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-70,160]);
ptitle(sprintf('TE - \\chi^2 PTE = %.2f',rs.ptes(2)));
subplot(4,3,5); a2=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rj.lc(:,2),rj.real(:,2),rj.derr(:,2),'b.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); %ylim([-70,160]);
ptitle(sprintf('TE jack - \\chi^2 PTE = %.2f',rj.ptes(2)));
make_axis_pair(a1,a2);

subplot(4,3,3); a1=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,3),'r');
plot(im.l,im.Cs_l(:,3)+2*mt(:,3),'r--');
plot(im.l,2*mt(:,3),'r--');
h=errorbar2(rs.lc(:,3),rs.real(:,3),rs.derr(:,3),'k.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-1,16]);
ptitle(sprintf('EE - \\chi^2 PTE = %.2f',rs.ptes(3)));
subplot(4,3,6); a2=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rj.lc(:,3),rj.real(:,3),rj.derr(:,3),'b.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-.01,.01]);
ptitle(sprintf('EE jack - \\chi^2 PTE = %.2f',rj.ptes(3)));
make_axis_pair(a1,a2);

subplot(4,3,[7,10]); a1=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
%plot(rs.lc(:,4),mean(rs.noi(:,4,:),3),'k:');
plot(im.l,im.Cs_l(:,4)*2,'r--');
plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
plot(ml.l,ml.Cs_l(:,4),'r');
h=errorbar2(rs.lc(:,4),rs.real(:,4),rs.derr(:,4),'k.');
set(h(2),'MarkerSize',ms);
xlim([0,330]); ylim([-.015,.055]);
ptitle(sprintf('BB - \\chi^2 PTE = 1.3\\times10^{-7}',rs.ptes(4)));
h=errorbar2(rj.lc(:,4),rj.real(:,4),rj.derr(:,4),'b.');
set(h(2),'MarkerSize',ms);
hold off
ptitle(sprintf('BB jack - \\chi^2 PTE = %.2f',rj.ptes(4)),0.03,0.05);

subplot(4,3,8); a1=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,5),'r');
h=errorbar2(rs.lc(:,2),rs.real(:,5),rs.derr(:,5),'k.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-5,5]);
ptitle(sprintf('TB - \\chi^2 PTE = %.2f',rs.ptes(5)));
subplot(4,3,11); a2=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rj.lc(:,2),rj.real(:,5),rj.derr(:,5),'b.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); %ylim([-5,5]);
ptitle(sprintf('TB jack - \\chi^2 PTE = %.2f',rj.ptes(5)));
make_axis_pair(a1,a2);

subplot(4,3,9); a1=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,6),'r');
h=errorbar2(rs.lc(:,3),rs.real(:,6),rs.derr(:,6),'k.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-.1,.1]);
ptitle(sprintf('EB - \\chi^2 PTE = %.2f',rs.ptes(6)));
subplot(4,3,12); a2=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rj.lc(:,3),rj.real(:,6),rj.derr(:,6),'b.');
set(h(2),'MarkerSize',ms);
hold off
xlim([0,330]); ylim([-.01,.01]);
ptitle(sprintf('EB jack - \\chi^2 PTE = %.2f',rj.ptes(6)));
make_axis_pair(a1,a2);

xlabel('Multipole')
keyboard
print -depsc2 paper_plots/powspecres.eps
fix_lines('paper_plots/powspecres.eps')
!epstopdf paper_plots/powspecres.eps

return

%%%%%%%%
function make_axis_pair(a1,a2)
p1=get(a1,'Position');
p2=get(a2,'Position');
g=p1(2)-(p2(2)+p2(4));
g=g*0.7;
set(a1,'XtickLabel',[]);
set(a1,'Position',[p1(1),p1(2)-g,p1(3),p1(4)+g]);
return

%%%%%%%%%%%%%%%%%%%%%
function make_bbdevs()

% get the data
[r,im,ml]=get_b2xb2;
l=r.lc(:,4);

%load final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat
%r=get_bpcov(r); r=weighted_ellbins(r,bpwf); l=r.lc(:,4); l(1)=NaN;

for i=1:6
  chibins{i}=2:10;
end

clf; setwinsize(gcf,300,200)
r=calc_devs(r,chibins,1);

% taken from reduc_final_chi2
devs=sort(r.sdevs,3);
ns=size(r.sdevs,3);
% the levels in sorted bandpowers closest to -2,1,0,1,2 sigma
ncdf = normcdf([-2:2],0,1);
t=[floor(ns*ncdf(ncdf<0.5)),ceil(ns*ncdf(ncdf>=0.5))];

plot(l,squeeze(devs(:,4,t(3))),'b');
hold on;
plot(l,squeeze(devs(:,4,t([2,4]))),'r');
plot(l,squeeze(devs(:,4,t([1,5]))),'g');
h1=plot(l,r.rdevs(:,4),'k.');
set(h1,'MarkerSize',12);
hold off
xlim([40,330]); ylim([-3,7]);
xlabel('Multipole'); ylabel('BB deviation from lensed-\LambdaCDM (\sigma)');
grid

print -depsc2 paper_plots/bbdevs.eps
fix_lines('paper_plots/bbdevs.eps')
!epstopdf paper_plots/bbdevs.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_ebplot

ellrng=[50,120];
projfile='matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat';

load maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0
map=make_map(ac,m,coaddopt);
maps=cal_coadd_maps(map,get_ukpervolt);
maps=make_ebmap(m,maps,[],ellrng,projfile);

% load maps/0751/real_a_filtp3_weight3_gs_dp1102_jack3
% map=make_map(ac,m,coaddopt);
% map=jackknife_map(map);
% mapj=cal_coadd_maps(map,get_ukpervolt);
% mapj=make_ebmap(m,mapj,[],ellrng,projfile);

% use sim instead of jackknife
load maps/0751/0017_a_filtp3_weight3_gs_dp1100_jack0
map=make_map(ac,m,coaddopt);
mapj=cal_coadd_maps(map,get_ukpervolt);
mapj=make_ebmap(m,mapj,[],ellrng,projfile);

fig=figure;
clf;
dim=get_fourpaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% color range
ce=2.4;
cb=2.4/6.4;

% vector length scale factor
ve=0.6;
vb=0.6*6.4;

% E

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,maps.E,'iau');
caxis([-ce,ce]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,maps.EQ,maps.EU,maps.Qvar,maps.Uvar,ve,[],3,1)
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
title('BICEP2: E signal');

ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapj.E,'iau');
caxis([-ce,ce]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,mapj.EQ,mapj.EU,mapj.Qvar,mapj.Uvar,ve,[],3,1)
set(ax2,'FontSize',7,'FontWeight','normal');
set(ax2,'Xticklabel',[],'Yticklabel',[]);
% title('E jackknife');
title('Simulation: E from lensed-{\Lambda}CDM+noise')

% B

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,maps.B,'iau');
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,maps.BQ,maps.BU,maps.Qvar,maps.Uvar,vb,[],3,1)
set(ax3,'FontSize',7,'FontWeight','normal');
title('BICEP2: B signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapj.B,'iau');
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,mapj.BQ,mapj.BU,mapj.Qvar,mapj.Uvar,vb,[],3,1)
set(ax4,'FontSize',7,'FontWeight','normal');
set(ax4,'Yticklabel',[]);
% title('B jackknife');
title('Simulation: B from lensed-{\Lambda}CDM+noise')

% colorbars

ax5 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax5,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-ce,ce,1000),repmat(linspace(-ce,ce,1000)',1,10));
colormap(colormap_lint)
freezeColors;
set(ax5,'Xtick',[]);
set(ax5,'YDir','normal','YAxisLocation','right','YTick',[-1.8 0 1.8]);

ax6 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax6,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-cb,cb,1000),repmat(linspace(-cb,cb,1000)',1,10));
colormap(colormap_lint)
freezeColors;
set(ax6,'Xtick',[]);
set(ax6,'YDir','normal','YAxisLocation','right','YTick',[-0.3 0 0.3]);

% colorbar units

ax7 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax7, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% this works way better than direct print to pdf
print -depsc2 paper_plots/eb_maps.eps
fix_lines('paper_plots/eb_maps.eps')
!epstopdf paper_plots/eb_maps.eps

return

%%%%%%%%%%%%%%%%%%%%%
% used by make_ebplot above
function dim=get_fourpaneldims(m)
% Parameters for six-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 2 * dim.maph + dim.wide + 2 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 2 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y2 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return

%%%%%%%%%%%%%%%%%%%%%
function make_b2xb1

% get the standard b2xb2 points
[rb2,im,ml,dum,dum,mt]=get_b2xb2;

% get b2xb1
load final/0751x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix.mat

clf; setwinsize(gcf,400,400)

subplot_grid(2,1,1);
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(ml.l,ml.Cs_l(:,3),'r');
plot(ml.l,ml.Cs_l(:,3)+2*mt(:,3),'r--');
h1=errorbar2(rb2(1).lc(:,3),rb2(1).real(:,3),std(rb2(1).sim(:,3,:),[],3),'k.');
set(h1(2),'MarkerSize',12);
%plot(rb2(1).lc(:,4),mean(rb2(1).sim(:,3,:),3),'k');
h2=errorbar2(rb2(1).lc(:,3)-4,r(4).real(:,3),std(r(4).sim(:,3,:),[],3),'kx');
set(h2(2),'Color',[.3,.3,.3]); set(h2(1),'Color',[.3,.3,.3]);
%plot(rb2(1).lc(:,4),mean(r(4).sim(:,3,:),3),'k');
h3=errorbar2(rb2(1).lc(:,3)+4,r(5).real(:,3),std(r(5).sim(:,3,:),[],3),'k*');
set(h3(2),'Color',[.3,.3,.3]); set(h3(1),'Color',[.3,.3,.3]);
%plot(rb2(1).lc(:,4),mean(r(5).sim(:,3,:),3),'k');
hold off
xlim([0,200]); ylim([-.1,1.3]);
ptitle('EE');
legend([h1(2),h2(2),h3(2)],{'B2xB2','B2xB1_{100}','B2xB1_{150}'},'Location','west');
legend boxoff
ylabel('l(l+1)C_l^{EE}/2\pi [\muK^2]')
subplot_grid2(2,1,1);

subplot_grid(2,1,2);
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
plot(im.l,ml.Cs_l(:,4),'r');
h1=errorbar2(rb2(1).lc(:,4),rb2(1).real(:,4),std(rb2(1).sim(:,4,:),[],3),'k.');
set(h1(2),'MarkerSize',12);
%plot(rb2(1).lc(:,4),mean(rb2(1).sim(:,4,:),3),'k');
h2=errorbar2(rb2(1).lc(:,4)-4,r(4).real(:,4),std(r(4).sim(:,4,:),[],3),'kx');
set(h2(2),'Color',[.3,.3,.3]); set(h2(1),'Color',[.3,.3,.3]);
%plot(rb2(1).lc(:,4),mean(r(4).sim(:,4,:),3),'k');
h3=errorbar2(rb2(1).lc(:,4)+4,r(5).real(:,4),std(r(5).sim(:,4,:),[],3),'k*');
set(h3(2),'Color',[.3,.3,.3]); set(h3(1),'Color',[.3,.3,.3]);
%plot(rb2(1).lc(:,4),mean(r(5).sim(:,4,:),3),'k');
hold off
xlim([0,200]); ylim([-.01,.055]);
ptitle('BB');
ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
xlabel('Multipole')
subplot_grid2(2,1,2);

print -depsc2 paper_plots/b2xb1100.eps
fix_lines('paper_plots/b2xb1100.eps')
!epstopdf paper_plots/b2xb1100.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%
function make_beammapsimsplot

% Load beammap sim spectra and appropriate supfac
dp={'0000','0100','1100','1110','1101','1111'};

% Get the beam map sim algorithmic floor from noiseless constructed Planck sky dp1111
% beam map sim
floor=load('/n/bicepfs1/users/clwong/aps/2027/0002_a_filtp3_weight3_gs_dp1111_jack0_pureB.mat');
load('final/1450x1350/real_a_filtp3_weight3_gs_dp1101_jack0_real_a_filtp3_weight3_gs_dp1101_jack01_pureB_overrx.mat');
rwf1101=r(1).rwf;
load('final/1450x1350/real_a_filtp3_weight3_gs_dp1111_jack0_real_a_filtp3_weight3_gs_dp1111_jack01_pureB_overrx.mat');
rwf1111=r(1).rwf;
floor.aps.Cs_l=floor.aps.Cs_l.*rwf1111;


for k=1:numel(dp)

  load(sprintf('final/1450x1350/real_a_filtp3_weight3_gs_dp%s_jack0_real_a_filtp3_weight3_gs_dp%s_jack01_pureB_overrx.mat',dp{k},dp{k}));
  x=load(sprintf('aps/2244/0002_a_filtp3_weight3_gs_dp%s_jack0_pureB.mat',dp{k}));
  aps(k).Cs_l=x.aps.Cs_l.*r(1).rwf;
  aps(k).l=x.aps.l;
  
  % Remove bias
  dpb=dp{k};
  if strcmp(dpb,'0100') | strcmp(dpb,'1110');
    dpb='1100'; % This is dumb, but it makes no difference.
  end
  %dpb = '1101'; % always just remove the same dp1101 bias.
  load(sprintf('~csheehy/bicep2_analysis/bias_for_chinlin/beammap_bias_and_sigma_dp%s',dpb));
  aps(k).Cs_l(:,4)=aps(k).Cs_l(:,4)-bias;

  % Remove beam map floor
  aps(k).Cs_l(:,4)=aps(k).Cs_l(:,4)-floor.aps.Cs_l(:,4);
  
end

% get the standard b2xb2 points and models
[r,im,ml,dum,rf,dum,rscaling]=get_b2xb2;

clf; setwinsize(gcf,400,600)

% Plot input model
%semilogy(im.l,im.Cs_l(:,4)*2,'r--');
h1=semilogy(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
hold on
%plot(ml.l,ml.Cs_l(:,4),'r');

xlim([0,330]); ylim([1e-7,1]);

l=aps(1).l; l(1)=NaN;

lw=get_lw;
h2=semilogy(l,aps(1).Cs_l(:,4),'g','LineWidth',lw);
h3=semilogy(l,aps(2).Cs_l(:,4),'c','LineWidth',lw);
h4=semilogy(l,aps(3).Cs_l(:,4),'m','LineWidth',lw);
h5=semilogy(l,aps(4).Cs_l(:,4),'b--','LineWidth',lw);
h6=semilogy(l,aps(5).Cs_l(:,4),'k','LineWidth',lw);
%h7=semilogy(l,aps(6).Cs_l(:,4),'Color',[.08,.95,.08],'LineStyle','-');

hold off

xlabel('Multipole');
ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]');

h=legend([h1,h2,h3,h4,h5,h6],'lensed-\LambdaCDM+r=0.2','no deprojection','dp','dp+dg','dp+dg+bw','dp+dg+ellip.','Location','NorthWest');
legend boxoff

print -depsc2 paper_plots/beammapsims.eps
fix_lines('paper_plots/beammapsims.eps')
!epstopdf paper_plots/beammapsims.eps

% get the standard r likelihood
rfropt.rscaling=rscaling;
rfropt.rvals=0.1:.0002:0.3;
[r1,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

% put in undeproj resid debias and do it again
rfropt.fgmod=aps(5);
[r2,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

% print the delta
disp(sprintf('change in max like r value when debias undeproj resid=%f',r1.rmaxlike-r2.rmaxlike));

return

%%%%%%%%%%%%%%%%%%%%%%%%
function make_foreplot()

models={'r=0.2 + lensing', 'BSS', 'LSA', 'FDS', 'PSM', 'PSM MAMD Pred 17.5','PSM Sim', 'PSM MAMD Sim 17.5','PSM  Fauvet Pred 15', 'DDM1 1deg', 'DDM1', 'DDM2_1deg', 'DDM2'}
%these are: r=0.2 +lensing, BSS, LSA, FDS, PSM Pred MAMD 15% intri_pol, PSM Pred MAMD 17.5% intri_pol,  PSM Sim MAMD 15% intri_pol, PSM Sim MAMD 17.5% intri_pol, PSM fauvet, DDM1_1deg, DDM1, DDM2_1deg, DDM2 
colors={[1,0,0], [.9,.6,0], [0,.6,.5],[.8,.6,.7], [0,.45,.7],[.35,.7,.9], [.3,.05,.9],[.35 .7 .9],[.3,.08,.8],[.8 .8 .8],[.3,.3,.9],[.8 .2 .2],[.2,.3,.3]};

%of these models, select the ones we want to see
selected_models=[2,3,4,5,11];

simple_colors={'c','y','g','r','m','b'};
for i=1:length(selected_models)
  colors{selected_models(i)}=simple_colors{i};
end

%grab the models spectra
aps=get_jet_models(selected_models);

% get real data points
[r,im,ml]=get_b2xb2;
l=r.lc(:,4);

clf; setwinsize(gcf,400,300)
%subplot(7,1,[1 5]);a1=gca;
% Plot input model
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
hinp=plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'--','color',colors{1});
xlim([0,330]);ylim([-6e-3,2e-2]);

% plot b2 3yr
h1=errorbar2(l(1:6),r(1).real(1:6,4),std(r(1).sim(1:6,4,:),[],3),'k.');
set(h1(2),'MarkerSize',12);

%ytics
arr=[-5d-3:5d-3:2d-2];
for i=1:length(arr);fields{i}=num2str(arr(i), '%g');end
set(gca, 'YTick', arr, 'YTickLabel', fields);

lw=get_lw;

%cross spectra
for sel=selected_models
 
  if(0) %plot band of errors for cross spec
    seb = shadedErrorBar(l(2:end),aps(sel).Cs_l(2:end,4),std(aps(sel).sim(2:end,4,:),[],3), {'color',colors{sel}})
    h(sel)=seb.mainLine;
  else  %just plot line for cross spec
    h(sel)=plot(l(2:end),aps(sel).Cs_l(2:end,4),'Color',colors{sel},'LineWidth',lw);
  end

  % plot auto dust models
  if(1)
    if(sel~=11)
      plot(l(2:end),aps(sel).auto(2:end,4),'--','Color',colors{sel},'LineWidth',lw);
    else
      plot(l(2:end),aps(sel).auto(2:end,4)*0.05^2/0.05^2,'--','Color',colors{sel},'LineWidth',lw);
    end
  end
end
hold off

legend([hinp,h(selected_models)],{'lens+r=0.2',models{selected_models}},'Location','NE');
legend boxoff

% labels
ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]');
pos=get(gca, 'Position');
set(gca,'Position',[pos(1)+.05 pos(2) pos(3) pos(4)])
ptitle('Dust',0.1,0.9);


%now make sub figure showing cross spec and bands
if(0)
 subplot(7,1,[6 7]);a2=gca;
 line([0,330],[0,0],'LineStyle',':','color','k'); box on
 hold on
 for sel=selected_models
  shadedErrorBar(l(2:end),aps(sel).Cs_l(2:end,4),std(aps(sel).sim(2:end,4,:),[],3), {'color',colors{sel}, 'Visible', 'on'})
 end
 hold off
 arr=[-2.5d-3:2.5d-3:2.5d-3];
 for i=1:length(arr);fields{i}=num2str(arr(i), '%g');end
 set(gca, 'YTick', arr, 'YTickLabel', fields);
 ylim([-3d-3,3d-3]);
 make_axis_pair(a1,a2);
 pos=get(gca, 'Position');
 set(gca,'Position',[pos(1)+.05 pos(2) pos(3) pos(4)])
 make_axis_pair(a1,a2);
end

%final axis stuff
xlim([0,330]);
box on
xlabel('Multipole');

%print
print -depsc2 paper_plots/foregroundproj.eps
fix_lines('paper_plots/foregroundproj.eps')
!epstopdf paper_plots/foregroundproj.eps

return

%%%%%%%%%%
function aps=get_jet_models(n)

%load dust aps
for k=n
 
  load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat');
  x=load(['/n/bicepfs1/bicep2/pipeline/aps/0751x0704/real_a_filtp3_weight3_gs_dp1100_jack0_final_xxx8_allcmb_dust' num2str(k, '%.2d') '_filtp3_weight3_gs_dp1100_jack0_final_matrix.mat']);
  y=load(['/n/bicepfs1/bicep2/pipeline/aps/0751x0704/xxx7_a_filtp3_weight3_gs_dp1100_jack0_final_xxx8_allcmb_dust' num2str(k, '%.2d') '_filtp3_weight3_gs_dp1100_jack0_final_matrix.mat']);
  z=load(['/n/bicepfs1/bicep2/pipeline/aps/0704/xxx8_allcmb_dust' num2str(k, '%.2d') '_filtp3_weight3_gs_dp1100_jack0_final_matrix.mat']);
  
  %multiply by sergis beta=1.6 fudge factor: .746 in cross and .746^2 in auto
  if ismember(k,[10,11,12,13])
  x.aps(3).Cs_l(:,1:6)=x.aps(3).Cs_l(:,1:6).*0.746;
  y.aps(3).Cs_l(:,1:6,:)=y.aps(3).Cs_l(:,1:6,:).*0.746;
  z.aps(1).Cs_l(:,1:6,:)=z.aps(1).Cs_l(:,1:6,:).*0.746^2;
  end
  
  %correct for sup factor
  aps(k).Cs_l=x.aps(3).Cs_l(:,1:6).*r(1).rwf;
  aps(k).sim=bsxfun(@times, r(1).rwf, y.aps(3).Cs_l(:,1:6,:));
  aps(k).auto=bsxfun(@times, r(1).rwf, z.aps(1).Cs_l(:,1:6,:));
  
  if ismember(k,[1,10,12])
    % for redigitized planck, we will manually apply a one degree beam correction
    % see http://arxiv.org/abs/astro-ph/9705188v2 eqn 3.

    %setup beam finctions
    beamsize=1;
    fwhm=beamsize*pi/180;
    sig=fwhm/sqrt(8*log(2));
    sigl=1./sig; 
    beamcor1=exp(-.5*(r(1).l.*(r(1).l+1))/(sigl.^2));
    
    beamsize=31.22/60;
    fwhm=beamsize*pi/180;
    sig=fwhm/sqrt(8*log(2));
    sigl=1./sig; 
    beamcor2=exp(-.5*(r(1).l.*(r(1).l+1))/(sigl.^2));
    
    beamcor=beamcor2./beamcor1;    
   
    aps(k).Cs_l=bsxfun(@times, beamcor, x.aps(3).Cs_l);
    aps(k).sim=bsxfun(@times, beamcor, y.aps(3).Cs_l(:,1:6,:));
    aps(k).auto=bsxfun(@times, beamcor.^2, z.aps(1).Cs_l(:,1:6,:));    
  end
  
  % select bins to show    
  if k==13
   aps(k).Cs_l(7:end,:)=NaN;
   aps(k).sim(7:end,:)=NaN;
   aps(k).auto(7:end,:)=NaN;
  end

end
return

%%%%%%%%%%%%%%%%%%%%%
function make_foremap

ellrng=[50,120];
projfile='matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat';

load maps/0704/xxx8_allcmb_dust05_filtp3_weight3_gs_dp1100_jack0_final.mat
map=make_ebmap(m,map,[],ellrng,projfile);

fig=figure;
clf;
dim=get_onepaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% color range
cb=1.6/4;

% B

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,map.B,'iau');
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,map.BQ,map.BU,map.Qvar,map.Uvar,1,[],3,0)
set(ax1,'FontSize',7,'FontWeight','normal');
% title('PSM predicted B');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% colorbars

ax2 = axes('Position',[dim.x2,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax2,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-cb,cb,1000),repmat(linspace(-cb,cb,1000)',1,10));
colormap(colormap_lint)
freezeColors;
set(ax2,'Xtick',[]);
set(ax2,'YDir','normal','YAxisLocation','right','YTick',[-0.75*cb 0 0.75*cb]);

% colorbar units

ax3 = axes('Position', [(dim.wide+dim.mapw+dim.thin+dim.cbar) / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax3, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% this works way better than direct print to pdf
print -depsc2 paper_plots/foremap.eps
fix_lines('paper_plots/foremap.eps')
!epstopdf paper_plots/foremap.eps

return

%%%%%%%%%%%%%%%%%%%%%
% used by make_foremap above
function dim=get_onepaneldims(m)
% Parameters for six-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 1 * dim.wide - 1 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 1 * dim.maph + dim.wide + 1 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2 (colorbar).
dim.y1 = dim.wide / dim.H; % Bottom edge of row 1.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return

%%%%%%%%%%%%%%%%%%%%%
function make_b2_vs_world

includeSampleVariance = 1;

% get real data points
[r,im,ml]=get_b2xb2;

% Use new/old lensing curve
if(0)
  d1 = load('aux_data/official_cl/camb_planck2013_r0p1_ext_lensedcls.dat')
  d2 = load('aux_data/official_cl/camb_planck2013_r0p1_ext_tenscls.dat')
  lensing_curve=d1(1:2899,4);
  tensor_curve=d2(1:2899,4)*2;
else
  lensing_curve=ml.Cs_l(:,4);
  tensor_curve=2*im.Cs_l(:,4);
end

% add lensing+tensors, making sure they are evaluated at the same ell
total_curve=lensing_curve+tensor_curve;

clf;
setwinsize(gcf,400,300)

loglog(im.l,lensing_curve,'r');
hold on
loglog(im.l,tensor_curve,'r--');
loglog(im.l,total_curve,'r--');

% Legend location
text_x=[12,35];
text_y=logspace(log10(5e0),log10(5e1),5);

if includeSampleVariance
  h=errorbar2(r.lc(2:10,4),r.real(2:10,4),std(squeeze(r(1).simr(2:10,4,:))')','k.');
else
  h=errorbar2(r.lc(2:10,4),r.real(2:10,4),r.derr(2:10,4),'k.');
end

h=herrorbar2(r.lc(2:10,4),r.real(2:10,4),r.lc(2:10,4)-r.ll(2:10,4),r.lh(2:10,4)-r.lc(2:10,4),'k.');
h=plot(r.lc(2:10,4),r.real(2:10,4),'k.','MarkerSize',12);
text(text_x(1),text_y(5),'BICEP2','Color','k')
%set(gca,'FontSize',10,'FontWeight','normal');

ms=4;
other_data=load('other_experiments/bicep1.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(4),'BICEP1','Color',[0.5,0.5,0.5])
other_data=load('other_experiments/boomerang.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(4),'Boomerang','Color',[1,0,1])
other_data=load('other_experiments/capmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(1),'CAPMAP','Color',[0.5,0,0.5])
other_data=load('other_experiments/cbi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,0]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(5),'CBI','Color',[0,1,0])
other_data=load('other_experiments/dasi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(3),'DASI','Color',[0,1,1])
other_data=load('other_experiments/quad.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,0]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(3),'QUAD','Color',[1,0,0])
other_data=load('other_experiments/quiet40.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,1]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(2),'QUIET-Q','Color',[0.5,0.5,1])
other_data=load('other_experiments/quiet90.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(1),'QUIET-W','Color',[0,0,1])
other_data=load('other_experiments/wmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,1/3,0]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(2),'WMAP','Color',[1,1/3,0])

% include the lensing measurements
if(0)
  % polarbear measurment of lensing
  other_data=load('other_experiments/polarbear.txt');
  ul=3;
  meas=[1,2,4];
  halfbw = (other_data(2,1)-other_data(1,1))/2;
  halfbw = repmat(halfbw,4,1);
  h=errorbar2(other_data(meas,1),other_data(meas,2),other_data(meas,3),'bs');
  set(h(2),'MarkerSize',ms);
  h=herrorbar2(other_data(meas,1),other_data(meas,2),halfbw(meas),halfbw(meas),'bs');
  set(h(2),'MarkerSize',ms);

  h=plot(other_data(meas,1),other_data(meas,2),'bs','MarkerSize',ms,'MarkerFaceColor','b');
  h=herrorbar2(other_data(ul,1),other_data(ul,4),halfbw(ul),halfbw(ul),'bv');
  set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');

  % SPT lensing in cross correlation with the CIB
  other_data=load('other_experiments/SptBBX.txt');
  p = other_data(:,2)/1e4.*(other_data(:,1)+1)/2/pi
  pu= other_data(:,3)/1e4.*(other_data(:,1)+1)/2/pi
  pl= other_data(:,4)/1e4.*(other_data(:,1)+1)/2/pi

  le = p-pl;
  ue = pu-p;
  le(p-le<0)=p(p-le<0)-1e-10;
  h=errorbar2(other_data(:,1),p,le,ue,'^')
  set(h(2),'MarkerSize',0.01);
  set(h(1),'Color',[0,0.7,0]);
  plot(other_data(:,1),p,'^','color',[0,0.7,0],'MarkerFaceColor',[0,0.7,0],'MarkerSize',ms)
  plot(other_data(:,1),pl,'^','color',[0,0.7,0],'MarkerFaceColor',[0,0.7,0],'MarkerSize',ms/2)
  plot(other_data(:,1),pu,'^','color',[0,0.7,0],'MarkerFaceColor',[0,0.7,0],'MarkerSize',ms/2)
  %% done
  xlim([10,3000]);
else
  xlim([10,2000]);
end

% zoomed in version
if(0)
  text_y = linspace(0.95,0.8,3)
  text_x = linspace(0.05,0.25,2)
  text(text_x(1),text_y(1),'BICEP1','Color',[0.5,0.5,0.5],'Units','normalized')
  text(text_x(2),text_y(1),'QUIET-Q','Color',[0.5,0.5,1],'Units','normalized')
  text(text_x(1),text_y(2),'WMAP','Color',[1,1/3,0],'Units','normalized')
  text(text_x(2),text_y(2),'Polarbear','Color',[0,0,1],'Units','normalized')
  text(text_x(1),text_y(3),'SPT x-corr','Color',[0,0.7,0],'Units','normalized')
  text(text_x(2),text_y(3),'BICEP2','Color','k','Units','normalized')
  set(gca,'yscale','lin')
  ylim([-0.02,0.16]);
  text(6e1,5e-3,'r=0.2','Rotation',10)
  text(380,0.032,'lensing','Rotation',55) 
else
  ylim([1e-3,1e2]);
  text(2e1,3e-3,'r=0.2','Rotation',32)
  text(6e1,2e-3,'lensing','Rotation',35)
end

if includeSampleVariance
  %text(0.02,0.25,{'BICEP2 uncertainties','include r=0.2','sample variance'},'Units','normalized','FontSize',8)
end 


hold off
xlabel('Multipole')
ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')


%  keyboard

print -depsc2 paper_plots/b2_and_previous_limits.eps
fix_lines('paper_plots/b2_and_previous_limits.eps')
!epstopdf paper_plots/b2_and_previous_limits.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_rconstraint()

%%%%%%%%%%%%%%%%%%%%
% first panel the bandpowers

% get the real points
[r,im,ml,dum,rf,dum,rscaling]=get_b2xb2;

clf; setwinsize(gcf,800,250);

subplot_grid(1,3,1)

line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
plot(im.l,ml.Cs_l(:,4),'r');
% plot B2xB2
h1=errorbar2(r(1).lc(:,4),r(1).real(:,4),std(r(1).simr(:,4,:),[],3),'k.');
set(h1(2),'MarkerSize',12);
hold off
xlim([0,200]); ylim([-.005,.03]);
%ptitle('BB');
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
axis square

% compute the r limits using mostly default options

% fine scaling on r:
rfropt.rvals=0:.001:0.75;
%rfropt.chibins=get_chibins; rfropt.chibins{4}=3:6;
%  keyboard
% main
rfropt.rscaling=rscaling;
[r,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

% with s+n+r=0.2
rfropt.fiducialr=0.2;
[rp2,rfroptp2] = reduc_final_rlim_direct(rf,rfropt,0);

%%%%%%%%%%%%%%%%%%%%
% second panel the likelihood function
subplot_grid(1,3,2)
box on; hold on

% plot a vertical line at the ML:
plot([r.rmaxlike r.rmaxlike],[0 interp1(r.rvals,r.likeBB,r.rmaxlike)],'k');
%text(r.rmaxlike*1.05,interp1(r.rvals,r.likeBB,r.rmaxlike)*1.01,'ML','color','k');

% plot the 95% confidence level:
%  c(2)=plot([r.rconf95 r.rconf95],[0 interp1(r.rvals,rlike,r.rconf95)],'r','LineWidth',2);
%  text(r.rconf95*1.05,interp1(r.rvals,rlike,r.rconf95)*1.01,'95%CL','color','r');

% plot the 68% confidence interval:
plot([r.rlo68 r.rlo68],[0 r.rcont68],'k--');
plot([r.rhi68 r.rhi68],[0 r.rcont68],'k--');

% plot the likelihood curve on top:
c(1)=plot(r.rvals,r.likeBB,'k');

set(gca,'YTick',[],'XTick',-10:0.1:10)
xlim([0 0.6])
ylim([0 r.maxlike*1.1])
xlabel('Tensor-to-scalar ratio r'); 
ylabel('Likelihood');
axis square
yl=ylim;
text(0.3,yl(2)*0.8,sprintf('r = %.2f^{+%.2f}_{-%.2f}',r.rmaxlike,r.rhi68-r.rmaxlike,r.rmaxlike-r.rlo68));

% numbers to quote
lz = interp1(r.rvals,r.likeBB,0)
lr=lz/r.maxlike;
[chi2,pte,sig]=likeratio2sigmas(lr);
disp(sprintf('L(zero)/L(max)=%.1e -> chi2=%.1f -> PTE=%.1e -> %.1fsigma',lr,chi2,pte,sig'));

keyboard

%%%%%%%%%%%%%%%%%%%%
% third panel the maxlike hood distribution from sims
subplot_grid(1,3,3)
box on; hold on
% binning here is tricky since the real value of 0.2
% wants to lie on a bin boundary which looks odd
% we could center the first bin on zero. The direct
% method does not give values r<0, which makes this
% a bit funny. Set xlim to start from 0 - effectively
% this makes the first bin half as wide.
binsml=-0.0125:.025:0.5125;

% do two hists
n=histc([r.smaxlike],binsml);
np2=histc([rp2.smaxlike],binsml);
stairs(binsml,n,'Color',[0,.45,.7])
stairs(binsml,np2,'Color',[1,.32,0])
set(gca,'YLim',[0 max(n)*1.1]);
xlabel('Maximum likelihood r'); 
ylabel('Entries');
xlim([0,0.5])
% xlim([min(binsml) max(binsml)])

% line for the real data
plot([r.rmaxlike r.rmaxlike],ylim,'k');

% decoration
tx=0.55; ty=0.9; tsx=0.37; tsy=-0.07
text(tx,ty,['Data r = ' sprintf('%1.2f',r.rmaxlike)],'Units','normalized','HorizontalAlignment','left','VerticalAlignment','middle','color','k')
text(tx,ty+tsy,['S+N Sims r=0.0'],'Units','normalized','HorizontalAlignment','left','VerticalAlignment','middle','color',[0,.45,.7])
text(tx,ty+2*tsy,['S+N Sims r=0.2'],'Units','normalized','HorizontalAlignment','left','VerticalAlignment','middle','color',[1,.32,.0])
axis square

return

% print

print -depsc2 paper_plots/rconstraint.eps
fix_lines('paper_plots/rconstraint.eps')
!epstopdf paper_plots/rconstraint.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%
function make_rcons_allfg()

selected_models=[11]; % this is DDM1

% real data - including file name
[dum,dum,dum,dum,rf,dum,rscaling]=get_b2xb2;

% get fg models
aps=get_jet_models(selected_models);

clear rfropt;
clear rs
% fine scaling on r:
rfropt.rvals=-0.35:.04:0.45;
ps=0:0.02:0.2;

% exclude the first sience bin:
sbin=3;
bins=6;
chibins{1}=sbin:bins; % TT
chibins{2}=sbin:bins; % TE
chibins{3}=sbin:bins; % EE
chibins{4}=sbin:bins; % BB
chibins{5}=sbin:bins; % TB
chibins{6}=sbin:bins; % EB
chibins{7}=chibins{2};
chibins{8}=chibins{5};
chibins{9}=chibins{6};

rfropt.chibins=chibins;
rfropt.method='hl'
rfropt.dust = aps(11).auto;
rfropt.dustp0 = 0.05;
rfropt.calc_type='rvspfrac';
rfropt.lvals=ps;
[r,rfropt] = reduc_final_rlim(rf,rfropt,0);

% make the plot
clf; setwinsize(gcf,230,230)

%xl='r'; yl='lensing scale factor';
xl='r'; yl='Squared Polarization Fraction DDM1';
    
%subplot(1,3,1);
mv=maxmd(r.likeBB);
[c,h]=contourf(r.rvals,r.lvals.^2,r.likeBB',mv*exp(-([1,2]).^2/2));
hold
[dum,ind]=(max(r.likeBB));
plot(r.rvals(ind),r.lvals.^2,'r--')
box on
line([0,0],[0,0.2],'LineStyle','-','color','k');
xlabel(xl); ylabel(yl);
axis square
colormap(cool);
caxis([0 1.5]);
ch = get(h,'children');
set(ch(1),'FaceColor',[0 0 1]);
set(ch(1),'EdgeColor',[0 0 1]);
set(ch(2),'FaceColor',[0 1 1]);
set(ch(2),'EdgeColor',[0 0 1]);
set(ch(2),'LineWidth',0.45);

p =polyfit(r.rvals(ind),r.lvals.^2,1)
plot(r.rvals,(polyval(p,r.rvals)),'r-')
%  
%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  clear rfropt;
%  rfropt.method='chi2'
%  rfropt.addxfac=0
%  
%  for ii=1:length(ps)
%    p=ps(ii)
%    rfropt.fgmod=aps(11);
%    % use the auto spectrum:
%    rfropt.fgmod.Cs_l=rfropt.fgmod.auto*(p/0.05)^2;
%    [r,rfropt] = reduc_final_rlim(rf,rfropt,0);
%    rs(ii)=r
%  end
%  
%  figure()
%  hold
%  for ii=1:length(ps)
%    rml(ii)=rs(ii).rmaxliker;
%    if ii>1 & ii<length(ps)-1
%      errorbar2(ps(ii),rs(ii).rmaxliker,rs(ii).rmaxliker-rs(ii).rlo68,rs(ii).rhi68-rs(ii).rmaxliker,'bo')
%    end
%  end
%  plot(ps(2:end-2),rml(2:end-2),'b')
%  xlabel('Polarization Fraction in DDM1')
%  ylabel('r')
%  box
%  line([-0.1,0.3],[0,0],'Color','k')
%  xlim([-0.005,0.205])
%  
%  figure()
%  [r  ,im ,ml ,dum,rf,dum,rscaling]=get_b2xb2;
%  
%  line([0,330],[0,0],'LineStyle',':','color','k'); box on
%  hold on
%  plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
%  plot(im.l,ml.Cs_l(:,4),'r');
%  % plot B2xB2
%  h1=errorbar2(r(1).lc(:,4),r(1).real(:,4),std(r(1).sim(:,4,:),[],3),'k.');
%  rfropt.fgmod=aps(11);
%  rfropt.fgmod.Cs_l=rfropt.fgmod.auto*(0.125/0.05)^2;
%  plot(r(1).lc(:,4),rfropt.fgmod.Cs_l(:,4),'k--')
%  pl(1)=plot(r(1).lc(chibins{4},4),rfropt.fgmod.Cs_l(chibins{4},4),'k')
%  rfropt.fgmod=aps(11);
%  rfropt.fgmod.Cs_l=rfropt.fgmod.auto*(0.15/0.05)^2;
%  plot(r(1).lc(:,4),rfropt.fgmod.Cs_l(:,4),'b--')
%  pl(2)=plot(r(1).lc(chibins{4},4),rfropt.fgmod.Cs_l(chibins{4},4),'b')
%  legend(pl,'12.5%','15%')
%  
%  set(h1(2),'MarkerSize',12);
%  hold off
%  xlim([0,200]); ylim([-.005,.03]);
%  %ptitle('BB');
%  xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
%  axis square
%  title('abs power')
%  
%  figure()
%  
%  line([0,330],[0,0],'LineStyle',':','color','k'); box on
%  hold on
%  plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
%  plot(im.l,ml.Cs_l(:,4),'r');
%  % plot B2xB2
%  rfropt.fgmod=aps(11);
%  rfropt.fgmod.Cs_l=rfropt.fgmod.auto*(0.125/0.05)^2;
%  h1=errorbar2(r(1).lc(:,4),r(1).real(:,4)-rfropt.fgmod.Cs_l(:,4),std(r(1).sim(:,4,:),[],3),'k.');
%  rfropt.fgmod=aps(11);
%  rfropt.fgmod.Cs_l=rfropt.fgmod.auto*(0.15/0.05)^2;
%  h2=errorbar2(r(1).lc(:,4),r(1).real(:,4)-rfropt.fgmod.Cs_l(:,4),std(r(1).sim(:,4,:),[],3),'b.');
%  legend([h1(1),h2(1)],'12.5%','15%')
%  
%  set(h1(2),'MarkerSize',12);
%  set(h2(2),'MarkerSize',12);
%  hold off
%  xlim([0,200]); ylim([-.015,.03]);
%  %ptitle('BB');
%  xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
%  axis square
%  title('subtracted')

return


%%%%%%%%%%%%%%%%%%%%%%%%
function make_rcons_fgdebias()

% should match foreplot above
selected_models=[2,3,4,5,11];
colors={[1,0,0], [.9,.6,0], [0,.6,.5],[.8,.6,.7], [0,.45,.7],[.35,.7,.9], [.3,.05,.9],[.35 .7 .9],[.3,.08,.8]};

simple_colors={'c','y','g','r','m','b'};
for i=1:length(selected_models)
  colors{selected_models(i)}=simple_colors{i};
end

% real data - including file name
[dum,dum,dum,dum,rf,dum,rscaling]=get_b2xb2;

% get fg models
aps=get_jet_models(selected_models);

% fine scaling on r:
rfropt.rvals=0:.001:0.75;

% get base likelihood curve
rfropt.rscaling=rscaling;
[r,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

% with foreground models debiased
for i=selected_models
  i
  rfropt.fgmod=aps(i);
  [rx(i),rfroptx(i)] = reduc_final_rlim_direct(rf,rfropt,0);
  
  % copy auto spec into place and do it again
  rfropt.fgmod.Cs_l=rfropt.fgmod.auto;
  [ra(i),rfropta(i)] = reduc_final_rlim_direct(rf,rfropt,0);  
end

clf; setwinsize(gcf,300,300);
box on; hold on

% plot the foreground model debiased lines
for i=selected_models % autos
  h1=plot(r.rvals,ra(i).likeBB*r.maxlike/ra(i).maxlike,'--','color',colors{i});
end
for i=selected_models % crosses
  h2=plot(r.rvals,rx(i).likeBB*r.maxlike/rx(i).maxlike,'color',colors{i});
end

% plot the base likelihood curve on top:
h3=plot(r.rvals,r.likeBB,'k');

legend([h1,h2,h3],{'auto subtracted','cross subtracted','base result'})
legend boxoff

hold off

set(gca,'YTick',[],'XTick',-10:0.1:10)
xlim([0 0.6])
ylim([0 r.maxlike*1.1])
xlabel('Tensor-to-scalar ratio r'); 
ylabel('Likelihood');
axis square

rr=[ra(selected_models).rmaxlike];
disp(sprintf('auto: min/max rmaxlike %f %f',min(rr),max(rr)));
rr=[rx(selected_models).rmaxlike];
disp(sprintf('cross: min/max rmaxlike %f %f',min(rr),max(rr)));

disp(sprintf('DDM1 auto r = %.2f^{+%.2f}_{-%.2f}',ra(11).rmaxlike,ra(11).rhi68-ra(11).rmaxlike,ra(11).rmaxlike-ra(11).rlo68));

lz = interp1(ra(11).rvals,ra(11).likeBB,0)
lr=lz/ra(11).maxlike;
[chi2,pte,sig]=likeratio2sigmas(lr);
disp(sprintf('DDM1 auto L(zero)/L(max)=%.1e -> chi2=%.1f -> PTE=%.1e -> %.1fsigma',lr,chi2,pte,sig'));

for i=selected_models
  disp(['Model ',num2str(i),' fit chisquare auto: ',num2str(ra(i).rchisq(4)),' cross: ',num2str(rx(i).rchisq(4))])
end

print -depsc2 paper_plots/rcons_fgdebias.eps
fix_lines('paper_plots/rcons_fgdebias.eps')
!epstopdf paper_plots/rcons_fgdebias.eps

%  keyboard

return


%%%%%%%%%%%%%%%%%%%%%%%%
function make_syncdustmix()
  beta_dust = 1.6
  beta_sync = -3.0
  paper_beta_eff = -1.55
  r_nominal = 0.2
  out_uncorrelated = combine_sync_dust(0:.001:1, 0, beta_sync, beta_dust);
  dust150_unc = interp1(out_uncorrelated.beta, out_uncorrelated.dust150, ...
    paper_beta_eff)
  sync150_unc = interp1(out_uncorrelated.beta, out_uncorrelated.sync150, ...
    paper_beta_eff)
  dust100_unc = interp1(out_uncorrelated.beta, out_uncorrelated.dust100, ...
    paper_beta_eff);
  sync100_unc = interp1(out_uncorrelated.beta, out_uncorrelated.sync100, ...
    paper_beta_eff);
  sync_fraction_at_cross_unc = sqrt(sync150_unc*sync100_unc) / ...
    (sqrt(sync150_unc*sync100_unc) + sqrt(dust150_unc*dust100_unc))
  sync_150_r_unc = sync150_unc * r_nominal
  sigma_unc = interp1(out_uncorrelated.beta, out_uncorrelated.sigma, ...
    paper_beta_eff)


return

%%%%%%%%%%%%%%%%%%%%%%%%
function make_syncplot()

% real data - including file name
[r,im,ml,dum,rf,dum,rscaling]=get_b2xb2;
l=r.lc(:,4);

%Get Jamie T WMAP products
%http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20140223_WMAPKBand/
b2_wmap = load('/n/bicepfs1/bicep2/pipeline/aps/0751x0704/real_a_filtp3_weight3_gs_dp1100_jack0_xxx8_allcmb_wmapKband_filtp3_weight3_gs_dp1100_jack0_matrix.mat');
b2_wmap_noise = load('/n/bicepfs1/bicep2/pipeline/aps/0751x0704/xxxl_a_filtp3_weight3_gs_dp1100_jack0_xxx8_allcmb_wmapKband_noise_0001_filtp3_weight3_gs_dp1100_jack0_matrix.mat');
wmap_b1 = load('/n/bicepfs1/bicep2/pipeline/aps/0704x1651/xxx8_allcmb_wmapKband_filtp3_weight3_gs_dp1100_jack0_real_a_filtp3_weight3_gs_dp1000_jack0_matrix.mat');
wmap_b1_noise = load('/n/bicepfs1/bicep2/pipeline/aps/0704x1651/xxx8_allcmb_wmapKband_noise_0001_filtp3_weight3_gs_dp1100_jack0_real_a_filtp3_weight3_gs_dp1000_jack0_matrix.mat');
%wmap_b1.aps(4) is WMAP x B1_100 (based on comparing to figure in posting above)
nSim = size(b2_wmap_noise.aps(1).Cs_l, 3);


% get sergi WMAP spectrum for beta=-3.3
load /n/home13/srh/install/bicep_install/current/aps_paper/1900/alm_synchro/reobserved_synchro/individual_aps/xxx8_allcmb_synchro_sergi_model1_filtp3_weight3_gs_dp1100_jack0_final_matrix.mat

% correct for filter/beam
aps.Cs_l=aps.Cs_l.*r.rwf;
b2_wmap.aps(2).Cs_l = b2_wmap.aps(2).Cs_l .*r.rwf;
%WARNING: this drops the last 3 spectra from the cross spectra
b2_wmap.aps(3).Cs_l = b2_wmap.aps(3).Cs_l(:,1:6) .*r.rwf; %Cross spectrum is filtered like auto, so apply full filter/beam correction

wmap_b1_supfac = load('/n/bicepfs1/bicep2/pipeline/final/0751x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix.mat');
%WMAP x B1 is filtered like B2 x B1 so apply B2 x B1 suppression factor
wmap_b1.aps(4) = apply_supfac(wmap_b1.aps(4), wmap_b1_supfac.supfac(4)); 
wmap_b1_noise.aps(4) = apply_supfac(wmap_b1_noise.aps(4), wmap_b1_supfac.supfac(4)); 

for iSim = 1:nSim
  b2_wmap_noise.aps(2).Cs_l(:,:,iSim) = b2_wmap_noise.aps(2).Cs_l(:,:,iSim) .*r.rwf;
  b2_wmap_noise.aps(3).Cs_l(:,1:6,iSim) = b2_wmap_noise.aps(3).Cs_l(:,1:6,iSim) .*r.rwf;
end

%Scale K band to B2 observing frequency
scale_wmap_b2_33 = T_CMB_ratio_synch(23, 149.8, -3.3); %B2 center freq from instrument paper
scale_wmap_b2_30 = T_CMB_ratio_synch(23, 149.8, -3.0); 
scale_wmap_b1_33 = T_CMB_ratio_synch(23, 96.0, -3.3);
scale_wmap_b1_30 = T_CMB_ratio_synch(23, 96.0, -3.0);
scale_b1_b2_33 = T_CMB_ratio_synch(96.0, 149.8, -3.3);
scale_b1_b2_30 = T_CMB_ratio_synch(96.0, 149.8, -3.0);

b2_wmap.aps(2).Cs_l = b2_wmap.aps(2).Cs_l  .* (scale_wmap_b2_33) .^2; 
b2_wmap_noise.aps(2).Cs_l = b2_wmap_noise.aps(2).Cs_l  .* (scale_wmap_b2_33) .^2; 
b2_wmap.aps(3).Cs_l = b2_wmap.aps(3).Cs_l  .* (scale_wmap_b2_33); %Only WMAP part of Cross spectrum needs to be scaled so only 1 power of the frequency scaling
b2_wmap_noise.aps(3).Cs_l = b2_wmap_noise.aps(3).Cs_l  .* (scale_wmap_b2_33); 
wmap_b1.aps(4).Cs_l = wmap_b1.aps(4).Cs_l .* scale_wmap_b1_33;
wmap_b1_noise.aps(4).Cs_l = wmap_b1_noise.aps(4).Cs_l  .* scale_wmap_b1_33;
%From Takahashi et al., B1_100 is 96.0 GHz center, 0.93 deg FWHM
  %Center frequency from Bierman is within 0.5 GHz of that

%Deconvolve WMAP K-band beam
%Approximation using Gaussian
%Approx beam correction from http://bmode.caltech.edu/~spuder/analysis_logbook/analysis/20140221_synchrotron/png/plot_aps_ddm_synchro.m
sg_k = .88 / sqrt( 2. * pi ) * pi / 180 ;
b_ell_k = exp( -.5 * ( l .* ( l + 1 ) ) * sg_k^2 ) ;
bm_crr_wmap = 1 ./ b_ell_k * ones(1,6);
bm_crr_wmap_cross = 1 ./ b_ell_k * ones(1,9);

sg_b1 = 0.93 / 2.3548 * pi / 180;
b_ell_b1 =  exp( -.5 * ( l .* ( l + 1 ) ) * sg_b1^2 ) ;
bm_crr_b1 = 1 ./ b_ell_b1 * ones(1,6);
bm_crr_b1_cross = 1 ./ b_ell_b1 * ones(1,9);

b2_wmap.aps(2).Cs_l = b2_wmap.aps(2).Cs_l .* bm_crr_wmap .^ 2;
b2_wmap.aps(3).Cs_l = b2_wmap.aps(3).Cs_l .* bm_crr_wmap; %Only WMAP part of cross spectrum has a K band beam, so only 1 power of the beam correction
wmap_b1.aps(4).Cs_l = wmap_b1.aps(4).Cs_l .* bm_crr_wmap_cross;
for iSim = 1:nSim
  b2_wmap_noise.aps(2).Cs_l(:,:,iSim) = b2_wmap_noise.aps(2).Cs_l(:,:,iSim) .* bm_crr_wmap .^ 2;
  b2_wmap_noise.aps(3).Cs_l(:,:,iSim) = b2_wmap_noise.aps(3).Cs_l(:,:,iSim) .* bm_crr_wmap_cross;
  wmap_b1_noise.aps(4).Cs_l(:,:,iSim) = wmap_b1_noise.aps(4).Cs_l(:,:,iSim)  .* bm_crr_wmap_cross ; 
end

%The next part of the code is Colin's calculation. A lot of this is redundant with Immanuel's calculation above. These can be merged and simplified, but that work is deferred as we try to get the referee response quickly -- IDB 2014-05-15
  
% Load Jamie Tolan's aps for B2 x WMAP-K.
aps_real = load(['aps/0751x0704/' ...
                 'real_a_filtp3_weight3_gs_dp1100_jack0_xxx8_allcmb_' ...
                 'wmapKband_filtp3_weight3_gs_dp1100_jack0_matrix']);
auto.real = aps_real.aps(2).Cs_l(:,4);
cross.real = aps_real.aps(3).Cs_l(:,4);
aps_sim = load(['aps/0751x0704/xxxl_a_filtp3_weight3_gs_dp1100_jack0_' ...
                'xxx8_allcmb_wmapKband_noise_0001_filtp3_weight3_gs_' ...
                'dp1100_jack0_matrix']);
auto.sim = squeeze(aps_sim.aps(2).Cs_l(:,4,:));
cross.sim = squeeze(aps_sim.aps(3).Cs_l(:,4,:));

% Divide by WMAP B_ell (cross) or B_ell^2 (auto).
wmap_sigma = 0.88 / sqrt(2 * pi) * pi / 180;
wmap_Bl = exp(-0.5 * l .* (l + 1) * wmap_sigma^2);
auto.real = auto.real ./ wmap_Bl.^2;
auto.sim = auto.sim ./ repmat(wmap_Bl.^2, 1, size(auto.sim, 2));
cross.real = cross.real ./ wmap_Bl;
cross.sim = cross.sim ./ repmat(wmap_Bl, 1, size(cross.sim, 2));

% Divide by B2 filter/beam suppression factor.
auto.real = auto.real .* r.rwf(:,4);
auto.sim = auto.sim .* repmat(r.rwf(:,4), 1, size(auto.sim, 2));
cross.real = cross.real .* r.rwf(:,4);
cross.sim = cross.sim .* repmat(r.rwf(:,4), 1, size(cross.sim, 2));

% Scaling from 23 to 149.8 GHz.
% Apply this factor once to cross-spectra, twice to auto-spectra.
beta = -3.3; % Hard-coded to beta = -3.3
x1 = 23 / 56.75; % (h * nu) / (kB * T_CMB)
x2 = 149.8 / 56.75;
scale = ((exp(x2) - 1) / (exp(x1) - 1))^2 * (exp(x1) / exp(x2)) * ...
    (x2 / x1)^(beta - 2.);
auto.real = auto.real * scale^2;
auto.sim = auto.sim * scale^2;
cross.real = cross.real * scale;
cross.sim = cross.sim * scale;

auto.debias = mean(auto.sim, 2);
auto.err = std(auto.sim, 0, 2);

clf; setwinsize(gcf,400,300)
%subplot(7,1,[1 5]);a1=gca;
% Plot input model
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
hinp=plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
xlim([0,330]);
ylim([-2.2e-3,2.2e-3]);

% plot b2 3yr
h1=errorbar2(l(1:6),r(1).real(1:6,4),std(r(1).sim(1:6,4,:),[],3),'k.');
set(h1(2),'MarkerSize',12);

% ytics
arr=[-0.002,-0.001,0,0.001,0.002];
for i=1:length(arr);fields{i}=num2str(arr(i), '%g');end
set(gca, 'YTick', arr, 'YTickLabel', fields);

lw=get_lw;
% Plot WMAPK x BICEP2 cross-spectrum with error bar derived from
% WMAP noise sims.
cross_err = std(cross.sim, 0, 2);
hcross = errorbar2(l(2:end)-2, cross.real(2:end), cross_err(2:end), 'bx');
%set(hcross(2), 'MarkerSize', 12);

% Plot WMAPK auto-spectrum without error bars (upper limit on
% synchrotron contribution). Dashed line to match auto-spectra from
% dust models figure.
hauto = plot(l(2:end), auto.real(2:end), 'color', 'm', ...
             'linestyle', '--', 'linewidth', lw);

% debiased auto
hauto_debiased = errorbar2( l(2:end)+2, auto.real(2:end) - auto.debias(2:end), ...
  auto.err(2:end), 'm+');
%set(hauto_debiased, 'MarkerSize', 12);
%set(hauto_debiased(2), 'linewidth', lw);

% labels
ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]');
xlabel('Multipole');
pos=get(gca, 'Position');
set(gca,'Position',[pos(1)+.05 pos(2) pos(3) pos(4)])
ptitle('Synchrotron',0.1,0.9);

legend([hinp,hcross(2),hauto_debiased(2),hauto],{'lens+r=0.2','B2xW_K','W_KxW_K', ...
                    'W_KxW_K no debias'},'location','sw');
legend boxoff

% No legend?

drawnow

print -depsc2 paper_plots/syncproj.eps
fix_lines('paper_plots/syncproj.eps')
!epstopdf paper_plots/syncproj.eps

disp('Continue to get effective r values');
keyboard

%This plot is just for checking Immanuel's calculation -- IDB 2014-05-15
hold off
l1 = plot(l, b2_wmap.aps(2).Cs_l(:,4) ./ scale_wmap_b2_33 .^2, 'r');
hold on
for iSim = 1:nSim
  l2 = plot(l, b2_wmap_noise.aps(2).Cs_l(:,4,iSim) ./ scale_wmap_b2_33 .^2, 'Color', [0.5 0.5 0.5]);
end
l1 = plot(l, b2_wmap.aps(2).Cs_l(:,4) ./ scale_wmap_b2_33 .^2, 'r');
l3 = errorbar2( l(2:end),  auto.debias(2:end) ./ scale_wmap_b2_33 .^2, auto.err(2:end) ./ scale_wmap_b2_33 .^2, 'b--');
%ylim([-5, 20] .* 1e-3);
xlim([0, 350]);
ylim([0, 250]);
xlabel('ell');
ylabel('D_{ell} ({\mu}K^2)');
%title('BB Synchrotron WMAP Extrapolation to 150 GHz, \beta = -3.3');
title('BB WMAP Reobserved, 23 GHz');
legend([l1 l2 l3(2)], 'WMAP Real data', 'WMAP Noise Simulations', 'WMAP Noise Debias and Error');
%(auto.debias - mean(b2_wmap_noise.aps(2).Cs_l(:,4,:), 3)) ./ auto.debias

% get the standard r likelihood
rfropt.rscaling=rscaling;
rfropt.rvals=0.15:.0002:0.25;
[r1,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

% put in sync spec as foreground to debias and do it again
rfropt.fgmod=aps;
[r2,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

%WMAP auto, beta = -3.3
rfropt.fgmod = b2_wmap.aps(2);
[r3,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

%With noise debias
rfropt.fgmod = b2_wmap.aps(2);
rfropt.fgmod.Cs_l(:,4) = rfropt.fgmod.Cs_l(:,4) - auto.debias;
[r9,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

%WMAP x B2, beta = -3.3
rfropt.fgmod = b2_wmap.aps(3);
[r4,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);
%Noise sims
r_wmap_b2_sim = ones(1, nSim);
r_wmap_b1_sim = ones(1, nSim);
r_wmap_sim = zeros(1, nSim);

do_uncer = 0 %Disable sim calculation because it takes hours
if ~do_uncer
  disp('WARNING: Not calculating uncertainty. Change do_uncer = 0 to 1 in the code to calculate uncertainty. Note that this calculation takes 2~4 hours.');
end
if do_uncer
for iSim = 1:nSim
  tic
  aps_sim = b2_wmap.aps(3);
  aps_sim.Cs_l = b2_wmap_noise.aps(3).Cs_l(:,1:6,iSim);
  rfropt.fgmod = aps_sim;
  rfropt.rvals=0.15:.0002:0.25; %Reset r range for WMAP x BICEP2
  [r5,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);
  r_wmap_b2_sim(iSim) = r1.rmaxlike-r5.rmaxlike;

  %WMAP auto sims
  aps_sim = b2_wmap.aps(2);
  aps_sim.Cs_l = b2_wmap_noise.aps(2).Cs_l(:,1:6,iSim);
  rfropt.fgmod = aps_sim;
  [r8,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);
  r_wmap_sim(iSim) = r1.rmaxlike - r8.rmaxlike;

  aps_sim = wmap_b1.aps(4);
  aps_sim.Cs_l = wmap_b1_noise.aps(4).Cs_l(:,1:6,iSim);
  rfropt.fgmod = aps_sim;
  rfropt.rvals=0:.002:0.4; %WMAP x BICEP1 has a wider range of r in sim
  [r7,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);
  r_wmap_b1_sim(iSim) = r1.rmaxlike-r7.rmaxlike;
  toc
end
end

rfropt.rvals=0.15:.0002:0.25;  %Reset r range for WMAP x BICEP2

%WMAP x B1, beta = -3.3, at 96 GHz
rfropt.fgmod = wmap_b1.aps(4);
[r6,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

r_wmap_noise_bias = mean(r_wmap_sim);
r_wmap_std = std(r_wmap_sim);

% print the delta
if ~do_uncer
  disp('WARNING: Not calculating uncertainty. Change do_uncer = 0 to 1 in the code to calculate uncertainty. Note that this calculation takes 2~4 hours.');
end
%disp(sprintf('change in max like r value when removing sync pred=%f',r1.rmaxlike-r2.rmaxlike)); %This was Sergi's prediction corresponding to r < 0.003 as paper was originally submitted.
disp(sprintf('Change in max like r value when removing WMAP K-band auto extrapolation (Beta=-3.3) =%f',r1.rmaxlike-r3.rmaxlike));
disp(sprintf('Change in max like r value when removing WMAP K-band auto extrapolation (Beta=-3.0) =%f', (r1.rmaxlike-r3.rmaxlike) .* ( scale_wmap_b2_30 / scale_wmap_b2_33).^2 ));
disp(sprintf('Effective r for WMAP K-band auto extrapolation (Beta = -3.3), noise debiased = %f +/- %f', r1.rmaxlike-r9.rmaxlike, r_wmap_std));
disp(sprintf('Effective r for WMAP K-band auto extrapolation (Beta = -3.0), noise debiased = %f +/- %f', (r1.rmaxlike-r9.rmaxlike ) .* ( scale_wmap_b2_30 / scale_wmap_b2_33).^2, r_wmap_std  .* ( scale_wmap_b2_30 / scale_wmap_b2_33).^2 ));
disp(sprintf('Change in max like r value when removing WMAP K-band x BICEP2 extrapolation (Beta=-3.3) =%f +/- %f',r1.rmaxlike-r4.rmaxlike, std(r_wmap_b2_sim)  ));
disp(sprintf('Change in max like r value when removing WMAP K-band x BICEP2 extrapolation (Beta=-3.0) =%f +/- %f', (r1.rmaxlike-r4.rmaxlike) .* ( scale_wmap_b2_30 / scale_wmap_b2_33)  , std(r_wmap_b2_sim) .* ( scale_wmap_b2_30 / scale_wmap_b2_33)   ));
disp(sprintf('Change in max like r value when removing WMAP K-band x BICEP1 extrapolation to 96 GHz (Beta=-3.3) =%f +/- %f',r1.rmaxlike-r6.rmaxlike, std(r_wmap_b1_sim)  ));
disp(sprintf('Change in max like r value when removing WMAP K-band x BICEP1 extrapolation to 96 GHz (Beta=-3.0) =%f +/- %f', (r1.rmaxlike-r6.rmaxlike) .* ( scale_wmap_b1_30 / scale_wmap_b1_33), std(r_wmap_b1_sim) .* ( scale_wmap_b1_30 / scale_wmap_b1_33) ));
disp(sprintf('Change in max like r value when removing WMAP K-band x BICEP1 extrapolation to 96 x 150 GHz (Beta=-3.3) =%f +/- %f', (r1.rmaxlike-r6.rmaxlike) .* scale_b1_b2_33 , std(r_wmap_b1_sim) .* scale_b1_b2_33  ));
disp(sprintf('Change in max like r value when removing WMAP K-band x BICEP1 extrapolation to 150 GHz (Beta=-3.3) =%f +/- %f', (r1.rmaxlike-r6.rmaxlike) .* scale_b1_b2_33 .^2 , std(r_wmap_b1_sim) .* scale_b1_b2_33 .^2 ));
disp(sprintf('Change in max like r value when removing WMAP K-band x BICEP1 extrapolation to 96 x 150 GHz (Beta=-3.0) =%f +/- %f', (r1.rmaxlike-r6.rmaxlike) .* ( scale_wmap_b1_30 / scale_wmap_b1_33) .* scale_b1_b2_30, std(r_wmap_b1_sim) .* ( scale_wmap_b1_30 / scale_wmap_b1_33) .* scale_b1_b2_30 ));
disp(sprintf('Change in max like r value when removing WMAP K-band x BICEP1 extrapolation to 150 GHz (Beta=-3.0) =%f +/- %f', (r1.rmaxlike-r6.rmaxlike) .* ( scale_wmap_b1_30 / scale_wmap_b1_33) .* scale_b1_b2_30 .^ 2, std(r_wmap_b1_sim) .* ( scale_wmap_b1_30 / scale_wmap_b1_33) .* scale_b1_b2_30 .^ 2));

return

%%%%%%%%%%%%%%%%%%%%%%%%
function make_ps_rmod()
% compute the change in r constraint when pnt src model is debiased

% real data - including file name
[r,dum,dum,dum,rf,dum,rscaling]=get_b2xb2;

% get the standard r likelihood
rfropt.rscaling=rscaling;
rfropt.rvals=0.1:.0002:0.3;
[r1,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

% load the ps sim files
for i=1:10
  load(sprintf('aps/1610/real_a_filtp3_weight3_gs_dp1102_jack0_ps%02d_matrix.mat',i));
  ps.Cs_l(:,:,i)=aps.Cs_l;
end
ps.l=aps.l;
% take the mean
ps.Cs_l=mean(ps.Cs_l,3);
%load aps/1610/real_a_filtp3_weight3_gs_dp1102_jack0_ps_matrix.mat
%ps=aps;

% put in ps spec as foreground to debias and do it again
rfropt.fgmod=ps;
[r3,rfropt] = reduc_final_rlim_direct(rf,rfropt,0);

% print the delta
disp(sprintf('change in max like r value when removing ps pred=%f',r1.rmaxlike-r3.rmaxlike));

return

%%%%%%%%%%%%%%%%%%%%%%%%
function make_jacktab()

chibins1=get_chibins;
for i=1:6
  chibins2{i}=2:10;
end

jackstr='123456789abcde';


% 300 rlz unconstrained
%rf='final/1450x1350/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack05_pureB_overrx.mat'
% 200 rlz constrained
%rf='final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack05_pureB_overrx.mat'

% 300 rlz constrained
rf='final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_pureB_directbpwf';

for j=jackstr
    
  n=findstr(rf,'jack');
  rf(n+4)=sprintf('%s',j);
  load(sprintf('%s',rf));
  r=r(1);
  
  % set expectation value to mean of s+n sims
  r.expv=mean(r.sim,3);
  
  r=get_bpcov(r);
  
  k=strfind(jackstr,j);
  
  ra{k,1}=calc_chi(r,chibins1);
  ra{k,1}=calc_devs(ra{k,1},chibins1);
  ra{k,1}(1).jacktype=j;
  
  ra{k,2}=calc_chi(r,chibins2);
  ra{k,2}=calc_devs(ra{k,2},chibins2);
  ra{k,2}(1).jacktype=j;
end

fp=fopen('paper_plots/ptetab.tex','w');

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
fprintf(fp,'\\\\ \n');
%s=2:6; % include TE,EE,BB,TB,EB
s=[3,4,6]; % include EE,BB,EB only as B13
for j=1:size(ra,1)
  js=get_jackdef(ra{j}.jacktype);
  fprintf(fp,'\\sidehead{%s} \n',js);
  for i=s
    % replace zero values with PTEt
    if(ra{j,1}.ptes(i)==0)
      ra{j,1}.ptes(i)=ra{j,1}.ptet(i); disp(sprintf('chi2 1 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,2}.ptes(i)==0)
      ra{j,2}.ptes(i)=ra{j,2}.ptet(i); disp(sprintf('chi2 2 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,1}.ptes_sumdev(i)==0)
      ra{j,1}.ptes_sumdev(i)=ra{j,1}.ptet_sumdev(i); disp(sprintf('chi1 1 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,2}.ptes_sumdev(i)==0)
      ra{j,2}.ptes_sumdev(i)=ra{j,2}.ptet_sumdev(i); disp(sprintf('chi1 2 rep zero j=%d, i=%d',j,i));
    end
  
    fprintf(fp,'%s & %5.3f & %5.3f & %5.3f & %5.3f \\\\ \n',lab{i},...
            ra{j,1}.ptes(i),ra{j,2}.ptes(i),ra{j,1}.ptes_sumdev(i),ra{j,2}.ptes_sumdev(i));
  end
  ptes1(j,:)=ra{j,1}.ptes(s);
  ptes2(j,:)=ra{j,2}.ptes(s);
  ptes1_sumdev(j,:)=ra{j,1}.ptes_sumdev(s);
  ptes2_sumdev(j,:)=ra{j,2}.ptes_sumdev(s);
end

fclose(fp);

% make pte dist figure
clf; setwinsize(gcf,400,400);
yl=6;
subplot(2,2,1); hfill(ptes1,20,0,1); title('Bandpowers 1-5 \chi^2'); ylim([0,yl]);
%hfill(ptes1(:,2),20,0,1,[],'Sr--');
subplot(2,2,2); hfill(ptes2,20,0,1); title('Bandpowers 1-9 \chi^2'); ylim([0,yl]);
%hfill(ptes2(:,2),20,0,1,[],'Sr--');
subplot(2,2,3); hfill(ptes1_sumdev,20,0,1); title('Bandpowers 1-5 \chi'); ylim([0,yl]); 
%hfill(ptes1_sumdev(:,2),20,0,1,[],'Sr--');
subplot(2,2,4); hfill(ptes2_sumdev,20,0,1); title('Bandpowers 1-9 \chi'); ylim([0,yl]);
%hfill(ptes2_sumdev(:,2),20,0,1,[],'Sr--');

print -depsc2 paper_plots/ptedist.eps
fix_lines('paper_plots/ptedist.eps')
!epstopdf paper_plots/ptedist.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%
function make_rvsnt()

% call external prog to prepare the data
[dum,dum,dum, dum, filename]=get_b2xb2
rfropt.calc_type='rvsnt';
[r,rfropt]=reduc_final_rlim(filename,rfropt,0);

% make the plot
clf; setwinsize(gcf,800,300)

xl='r_{0.009}'; yl='n_T';

subplot(1,3,1);
mv=maxmd(r.likeBB);
contour(r.rvals,r.lvals,r.likeBB',mv*exp(-([1,2]).^2/2));
grid
hold on; plot(r.rmaxliker,r.rmaxlikel,'+'); hold off
xlabel(xl); ylabel(yl);
axis square

subplot(1,3,2);
rlike=sum(r.likeBB,2);
rlike=rlike./sum(rlike);
plot(r.rvals,rlike,'k');
xlim([min(r.rvals),max(r.rvals)])
hold on
plot([r.rlo68 r.rlo68],[0 r.rcont68],'b--');
plot([r.rhi68 r.rhi68],[0 r.rcont68],'b--');
hold off
xlabel(xl); ylabel('likelihood');
axis square

subplot(1,3,3);
llike=sum(r.likeBB,1);
llike=llike./sum(llike);
plot(r.lvals,llike,'k');
xlim([min(r.lvals),max(r.lvals)])
hold on
plot([r.llo68 r.llo68],[0 r.lcont68],'b--');
plot([r.lhi68 r.lhi68],[0 r.lcont68],'b--');
hold off
xlabel(yl); ylabel('likelihood');
axis square

print -depsc2 paper_plots/rvsnt.eps
fix_lines('paper_plots/rvsnt.eps')
!epstopdf paper_plots/rvsnt.eps

%  keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%%
function make_rvsl()

% call external prog to prepare the data
[dum,dum,dum, dum, filename]=get_b2xb2
rfropt.calc_type='rvsl';
%rfropt.rvals=0:0.0025:0.4; rfropt.lvals=-0.5:0.025:3.5;
rfropt.rvals=0:0.025:0.4; rfropt.lvals=-0.5:0.25:3.5;
rfropt.chibins{4}=2:10;
[r,rfropt]=reduc_final_rlim(filename,rfropt,0);

% make the plot
clf; setwinsize(gcf,230,230)

%xl='r'; yl='lensing scale factor';
xl='r'; yl='lensing scale factor A_L';
    
%subplot(1,3,1);
mv=maxmd(r.likeBB);
%contour(r.rvals,r.lvals,r.likeBB',mv*exp(-([1,2]).^2/2));
%contour(r.rvals,r.lvals,r.likeBB',mv*exp(-([1,2]).^2/2)), shading flat;
[c,h]=contourf(r.rvals,r.lvals,r.likeBB',mv*exp(-([1,2]).^2/2));
line([0,0.4],[0.95,0.95],'LineStyle',':','color','k'); box on
line([0,0.4],[1.05,1.05],'LineStyle',':','color','k');
%grid
%hold on; plot(r.rmaxliker,r.rmaxlikel,'+'); hold off
xlabel(xl); ylabel(yl);
axis square
xlim([0,0.4]);
colormap(cool);
caxis([0 1.5]);
ch = get(h,'children');
set(ch(1),'FaceColor',[0 0 1]);
set(ch(1),'EdgeColor',[0 0 1]);
set(ch(2),'FaceColor',[0 1 1]);
%set(ch(2),'FaceAlpha',0.4); % Doesn't render nicely with transparency...
set(ch(2),'EdgeColor',[0 0 1]);
set(ch(2),'LineWidth',0.45);
%set(ch(2),'EdgeAlpha',0.9);

% find the max index
llike=sum(r.likeBB,1);
[mv,j]=max(llike);

% find the zero index
i=find(r.lvals==0);
% find the likelihood ratio
lr=llike(i)/llike(j);
[chi2,pte,sig]=likeratio2sigmas(lr);
disp(sprintf('L(zero)/L(max)=%.1e -> chi2=%.1f -> PTE=%.1e -> %.1fsigma',lr,chi2,pte,sig'));

% find the unit index
i=find(r.lvals==1);
% find the likelihood ratio
lr=llike(i)/llike(j);
[chi2,pte,sig]=likeratio2sigmas(lr);
disp(sprintf('L(unity)/L(max)=%.1e -> chi2=%.1f -> PTE=%.1e -> %.1fsigma',lr,chi2,pte,sig'));

print -depsc2 paper_plots/rvsl.eps
fix_lines('paper_plots/rvsl.eps')
!epstopdf paper_plots/rvsl.eps

keyboard

return

%%%%%%%%%%%%%%%%%%%%%
function make_speccomp

% get B2xK12
load final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_matrix_overrx.mat
rk12=r;
% get B2xK13
load final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_b_filtp3_weight3_gs_dp1102_jack01_pureB_matrix_overrx.mat
rk13=r;

% combine as if K12 and K13 had been added
rck(1)=rk12(1); rck(1).w1=[]; rck(1).w2=[];
rck(2)=comb_spec(rk12(2),rk13(2)); % combination of auto spectra - not used
rck(3)=comb_spec(rk12(3),rk13(3)); % combination of cross spectra - used

% get B2xB1
load final/0751x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix.mat
% combine as if B1 100&150GHz maps had been added
rc(1)=r(1); rc(1).w1=[]; rc(1).w2=[];
rc(2)=comb_spec(r(2),r(3)); % combination of B1 auto spectra - not used
rc(3)=comb_spec(r(4),r(5)); % combination of B2xB1 cross spectra - used

% get the real points
[r,im,ml]=get_b2xb2;

clf; setwinsize(gcf,400,300)

line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
plot(im.l,ml.Cs_l(:,4),'r');
% plot B2xB2
h1=errorbar2(r(1).lc(:,4),r(1).real(:,4),std(r(1).sim(:,4,:),[],3),'k.');
set(h1(2),'MarkerSize',12);
%plot(r(1).lc(:,4),mean(r(1).sim(:,4,:),3),'k');
% plot B2xB1c
h2=errorbar2(r(1).lc(2:6,4)+4,rc(3).real(2:6,4),std(rc(3).sim(2:6,4,:),[],3),'kx');
set(h2(2),'Color',[.3,.3,.3]); set(h2(1),'Color',[.3,.3,.3]);
%plot(r(1).lc(:,4),mean(rc(3).sim(:,4,:),3),'k');
% plot B2xK
h3=errorbar2(r(1).lc(:,4)-4,rck(3).real(:,4),std(rck(3).sim(:,4,:),[],3),'k*');
set(h3(2),'Color',[.7,.7,.7]); set(h3(1),'Color',[.7,.7,.7]);
%plot(r(1).lc(:,4),mean(rk13(3).sim(:,4,:),3),'k');
h=legend([h1(2),h2(2),h3(2)],{'B2xB2','B2xB1c','B2xKeck (preliminary)'},'Location','nw');
legend boxoff
hold off
xlim([0,330]); ylim([-.01,.055]);
%ptitle('BB');
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')

print -depsc2 paper_plots/speccomp.eps
fix_lines('paper_plots/speccomp.eps')
!epstopdf paper_plots/speccomp.eps

%  keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_syslevels

% get supfac
load final/1450x1350/real_a_filtp3_weight3_gs_dp1101_jack0_real_a_filtp3_weight3_gs_dp1101_jack01_pureB_overrx.mat
rwf1101=r(1).rwf;

load final/1450x1350/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_overrx.mat
rwf1102=r(1).rwf;

load final/1450x1350/real_a_filtp3_weight3_gs_dp0000_jack0_real_a_filtp3_weight3_gs_dp0000_jack01_pureB_overrx.mat
rwf0000=r(1).rwf;

% Beams + relgain. Just load in the empirical distribution we include in the sims. Since we
% subtract the mean, use the std as an upper limit. This is 1 sigma upper limit.
load('~csheehy/bicep2_analysis/beammap_leakage_spectra_1000rlz.mat');
mm.Cs_l=mean(aps.Cs_l,3);
aps.Cs_l=std(aps.Cs_l,[],3);
sys{1}=aps;

% Thermal. Plot as measured leakage.
load(sprintf('~csheehy/bicep2_analysis/aps_bicepfs1/2203/real_a_filtp3_weight3_gs_dp1101_jack0_pureB.mat'));
aps.Cs_l=aps.Cs_l.*rwf1101;
sys{2}=aps;

% Pol uniform rotation from EB sum bpdev failure, plot as upper limit. This is ~2 sigma
% upper limit.
load('~csheehy/bicep2_analysis/uniform_polrot_spectrum.mat');
sys{3}.Cs_l=zeros(size(aps.Cs_l));
sys{3}.Cs_l(:,4)=bb;

% Plot 0.2 deg random chi error.
x1=load('~csheehy/bicep2_analysis/aps_bicepfs1/2246/0012_a_filtp3_weight3_gs_dp1101_jack0_pureB');
%x1=load('~csheehy/bicep2_analysis/aps/2247/0012_a_filtp3_weight3_gs_dp1101_jack0_pureB');
x2=load('~csheehy/bicep2_analysis/aps_bicepfs1/2245/0012_a_filtp3_weight3_gs_dp1101_jack0_pureB');
sys{4}.Cs_l=abs((x1.aps.Cs_l-x2.aps.Cs_l).*rwf1101);

% Plot measured little buddy leakage. Plot as measured.
load('~csheehy/bicep2_analysis/aps_bicepfs1/2186/0012_a_filtp3_weight3_gs_dp1101_jack0_pureB');
sys{5}.Cs_l=aps.Cs_l.*rwf1101;

% Plot detector time constant mismatch. Plot as measured.
load('~csheehy/bicep2_analysis/aps_bicepfs1/2239/0002_a_filtp3_weight3_gs_dp1101_jack0_pureB');
sys{6}.Cs_l=aps.Cs_l.*rwf1101;

% Crosstalk. Plot as measured.
load('/n/bicepfs1/users/sstokes/aps/1324/0252_a_filtp3_weight3_gs_dp11100000_jack0_pureB_matrix.mat');
load /n/bicepfs1/bicep2/pipeline/final/0750/real_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat
rwf1100matrix=r(1).rwf;
sys{7}.Cs_l=aps.Cs_l.*rwf1100matrix;

% Main beam
sys{8}=mm;

% Gain variation E->B
aps1=load('/n/panlfs2/bicep/csheehy/bicep2_analysis/aps/0751/xxx2_gainEtoB_nogainvar_filtp3_weight3_gs_dp1100_jack0_matrix.mat');
aps2=load('/n/panlfs2/bicep/csheehy/bicep2_analysis/aps/0751/xxx2_gainEtoB_gainvar_filtp3_weight3_gs_dp1100_jack0_matrix.mat');
nrlz=size(aps1.aps.Cs_l,3);
difb=squeeze(aps2.aps.Cs_l-aps1.aps.Cs_l).*repmat(rwf0000,[1,1,nrlz]);
sys{9}.Cs_l=mean(abs(difb),3);

% get the standard b2xb2 points and models
[r,im,ml]=get_b2xb2;


% Plot

clf; setwinsize(gcf,400,600)

% Plot input model
%semilogy(im.l,im.Cs_l(:,4)*2,'r--');
hinp=semilogy(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
hold on
%plot(ml.l,ml.Cs_l(:,4),'r');

l=aps.l; l(1)=NaN;
lw=get_lw;
h1=semilogy(l,sys{1}.Cs_l(:,4),'--k','LineWidth',lw);

hold on
h2=semilogy(l,sys{2}.Cs_l(:,4),'--r','LineWidth',lw);

h3=semilogy(l,sys{3}.Cs_l(:,4),'--m','LineWidth',lw);

h4=semilogy(l,sys{4}.Cs_l(:,4),'--g','LineWidth',lw);

h5=semilogy(l,sys{5}.Cs_l(:,4),'c','LineWidth',lw);

h6=semilogy(l,sys{6}.Cs_l(:,4),'--','Color',[.7,.3,0],'LineWidth',lw);

h7=semilogy(l,sys{7}.Cs_l(:,4),'Color',[.7,.3,.7],'LineWidth',lw);

h8=semilogy(l,std(r.noi(:,4,:),[],3),':k');

h9=semilogy(l,sys{9}.Cs_l(:,4),'--c','LineWidth',lw);

xlim([0,330]); ylim([1e-7,1]);
xlabel('Multipole');
ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]');

hleg=legend([hinp,h8,h1,h2,h3],'lensed-\LambdaCDM+r=0.2','BICEP2 stat. uncer.',...
            'extended beam','thermal','sys pol error','Location','NorthWest');
legend boxoff
set(hleg,'FontSize',9);
pos1=get(hleg,'Position');

hax=axes('Position',get(gca,'Position'),'Visible','off');
hleg=legend(hax,[h4,h9,h5,h6,h7],'rand pol error','gain E\rightarrowB','ghosted beams','transfer functions',...
            'crosstalk','Location','NorthEast');
set(hleg,'FontSize',9);
legend boxoff

% cannot get the proper legend position to work
%  global dopres;
%  if dopres
%    pos2=get(hleg,'Position');
%    pos2(2)=pos1(2)+0.01;
%    set(hleg,'Position',pos2);
%  end

% print the r-levels for all
Brp1=mean(r.sig(:,4,:),3);
Bstd=std(r.sim(:,4,:),[],3);
w=Brp1./Bstd.^2;
rhobins=2:6;

syst={'extended beam','thermal','sys pol error','rand pol error','ghosted beams',...
      'transfer functions','crosstalk','main beam','gain EtoB','statistical noise'};

for k=1:10
  
  if k==10
    bb=std(r.noi(:,4,:),[],3);
  else
    bb=sys{k}.Cs_l(:,4);
  end

  Bcontam=sum(bb(rhobins).*w(rhobins))/sum(w(rhobins));
  Brp1sum=sum(Brp1(rhobins).*w(rhobins))/sum(w(rhobins));
  rho=0.1*Bcontam/Brp1sum;

 disp(sprintf('r=%0.2d for %s',rho,syst{k}));

end

set(gcf,'PaperPositionMode','auto')
print -depsc2 paper_plots/systlevels.eps
fix_lines('paper_plots/systlevels.eps')
!epstopdf paper_plots/systlevels.eps

%  keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_colorconstraint

% I had been loading a data file with likelihoods and constraints...
%load('/Users/Jeff/matlab/BICEP2/0751x1651/colorconstraint_marg.mat');
%load('/n/home04/jpf/biceppipe/bicep2_analysis/paper_plots/colorconstraint_marg.mat');
%load('/n/home04/jpf/biceppipe/bicep2_analysis/paper_plots/colorconstraint_marg_09Mar.mat');
% ... but we get the same answer profiling, which is fast enough to
% run inline!

out = color_constraint_b1xb2(); % defaults are what we want
outNL = color_constraint_b1xb2('sub_lcdm',0); % turn off lensing subtraction

% move out variables we need
n = out.n;
L = exp(out.mlike);
L = L/max(L);
MPE = out.MPE;
int68B = out.int68B;
n_cmb = out.n_cmb;
Lnl = exp(outNL.mlike);
Lnl = Lnl/max(Lnl);


% Plot
clf; setwinsize(gcf,250,250)
axis([-4,4,0,1.1]); box on; axis square
hold on;
%set(gca,'YTick',[]);

% plot sync, CMB and dust lines
n_sync=-3;
n_dust=1.5; % Adjusted from 1.75 in initial preprint

% Vertical lines indicating expected beta for sync, CMB, dust.
Ls=interp1(n,L,n_sync);
Lc=interp1(n,L,n_cmb);
Ld=interp1(n,L,n_dust);
plot([n_sync,n_sync],[0,Ls],'b'); 
text(n_sync,Ls+0.1,'Sync','rotation',90, 'VerticalAlignment', 'baseline');
plot([n_cmb,n_cmb],[0,Lc],'b'); 
text(n_cmb,Lc+0.1,'CMB','rotation',90);
plot([n_dust,n_dust],[0,Ld],'b'); 
text(n_dust-0.05,Ld+0.05,'Dust','rotation',90);

% Print statistics to terminal.
disp('Lensing subtracted:')
[chi2,pte,sig] = likeratio2sigmas(Lc);
disp(['CMB:  PTE ' num2str(pte,'%3.3f') ' (' num2str(sqrt(chi2),'%3.2f') ' sigma, lr = ' num2str(Lc) ')']);
[chi2,pte,sig] = likeratio2sigmas(Ld);
disp(['Dust:  PTE ' num2str(pte,'%3.3f') ' (' num2str(sqrt(chi2),'%3.2f') ' sigma, lr = ' num2str(Ld) ')']);
[chi2,pte,sig] = likeratio2sigmas(Ls);
disp(['Sync:  PTE ' num2str(pte,'%3.3f') ' (' num2str(sqrt(chi2),'%3.2f') ' sigma, lr = ' num2str(Ls) ')']);

disp('Lensing not subtracted:')
Lsnl=interp1(outNL.n, Lnl,n_sync);
Lcnl=interp1(outNL.n, Lnl,n_cmb);
Ldnl=interp1(outNL.n, Lnl,n_dust);
[chi2,pte,sig] = likeratio2sigmas(Lcnl);
disp(['CMB:  PTE ' num2str(pte,'%3.3f') ' (' num2str(sqrt(chi2),'%3.2f') ' sigma, lr = ' num2str(Lcnl) ')']);
[chi2,pte,sig] = likeratio2sigmas(Ldnl);
disp(['Dust:  PTE ' num2str(pte,'%3.3f') ' (' num2str(sqrt(chi2),'%3.2f') ' sigma, lr = ' num2str(Ldnl) ')']);
[chi2,pte,sig] = likeratio2sigmas(Lsnl);
disp(['Sync:  PTE ' num2str(pte,'%3.3f') ' (' num2str(sqrt(chi2),'%3.2f') ' sigma, lr = ' num2str(Lsnl) ')']);

% Plot ML/68 vert lines for lensing subtracted likelihood.
plot([MPE,MPE],[0,interp1(n,L,MPE)],'k'); 
plot([int68B(1),int68B(1)],[0,interp1(n,L,int68B(1))],'k--');
plot([int68B(2),int68B(2)],[0,interp1(n,L,int68B(2))],'k--');

% Plot ML/68 vert lines for non-lensing subtracted likelihood.
plot(outNL.MPE * [1 1], [0, interp1(outNL.n, Lnl, outNL.MPE)], ...
     'Color', [0 0.5 0]);
plot(outNL.int68B(1) * [1 1], [0, interp1(outNL.n, Lnl, outNL.int68B(1))], ...
     '--', 'Color', [0 0.5 0]);
plot(outNL.int68B(2) * [1 1], [0, interp1(outNL.n, Lnl, outNL.int68B(2))], ...
     '--', 'Color', [0 0.5 0]);

% plot likelihood curve
plot(n,L,'k-', 'LineWidth', 1);

% plot likelihood curve NOT subtracting lensing
plot(n, Lnl, 'Color', [0 0.5 0], 'LineWidth', 1);

xlabel('Spectral index (\beta)');
ylabel('Likelihood');

%hleg=legend([h1,h2,h3],{'CMB','68% CL','95% CL'},'Location','NorthEast');
%legend boxoff

% Print one-sigma interval for beta.
sigH = int68B(2) - MPE;
sigL = MPE - int68B(1);
text(3.8,0.77,sprintf('\\beta_{excess} = %.2f^{+%.2f}_{-%.2f}',...
                     MPE,sigH,sigL), ...
     'HorizontalAlignment', 'right');

% Print one-sigma interval for beta without lensing
% subtraction.
sigH_NL = outNL.int68B(2) - outNL.MPE;
sigL_NL = outNL.MPE - outNL.int68B(1);
text(3.8, 0.62, sprintf('\\beta_{total} = %.2f^{+%.2f}_{-%.2f}', ...
                        outNL.MPE, sigH_NL, sigL_NL), ...
     'Color', [0 0.5 0], 'HorizontalAlignment', 'right');

%set(gcf, 'PaperPosition', [0 0 3.11 3], 'PaperUnits', 'inch');
print -depsc2 paper_plots/specindcons.eps
fix_lines('paper_plots/specindcons.eps')
!epstopdf paper_plots/specindcons.eps

%keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%
function make_supfac()

% get supfac and measured bins
[r,im,ml,bpwf]=get_b2xb2;

% get measured bpwf before normalization if preferred
%load('aps/0706/xxxx_allcmb_filtp3_weight3_gs_dp1100_jack0_matrix_directbpwf.mat')

% get the pixel window
pw=load('/n/panlfs/bicep/bicep2/pipeline/matrixdata/pixel_window_bicep_lmax500.mat')
 
% get bin edges
[be,nn]=get_bins('bicep_norm');

% get beam window function
bf= fitsread('aux_data/beams/beamfile_20130222_sum.fits', 'bintable');
beamf=bf{1}.^2; 

if(0)  % can check against this
  beamsize=31.22/60;
  fwhm=beamsize*pi/180;
  sig=fwhm/sqrt(8*log(2));
  sigl=1./sig; 
  beamcor=exp(-.5*(r(1).l.*(r(1).l+1))/(sigl.^2)).^2;
end

beamvals=interp1(1:length(beamf),beamf, r.lc(2:end,4));
pwvals=interp1(1:length(pw.wl.w_avg),pw.wl.w_avg.^2, r.lc(2:end,4));
filt=1./(beamvals.*r.rwf(2:end,4).*pwvals);

if(1)
  %colors={'r', 'g', 'm',[.6,.4,.6], [0,.3,0],[.5,0,.6],[.9,.3,0],[0,.4,.6],'c', 'k',[.8 .8 .8],[.3,.3,.9],[.8 .2 .2],[.2,.3,.3]};
  colors={'k','b','r','g','b','r','g','b','r','g'};
  
  % make plot
  clf 
  setwinsize(gcf,400,250)
  hold on
  %plot(pw.wl.w_avg.^2,'--k')
  %plot(beamf,'c')
  %plot(r.lc(2:end,4),filt,'m')
  plot(r.lc(2:end,4),1./r.rwf(2:end,4),'k')
  %plot(r.l,beamcor)

  for i=2:10
    plot(r.lc(i,4),1./r.rwf(i,4), '.','Color',colors{i},'MarkerSize',12) 
    plot(bpwf.l,5*bpwf.Cs_l(:,i,4), 'color', colors{i})
  end
  hold off
  ylim([0,1]);xlim([0,330])
  ylabel('Suppression Factor');
  xlabel('Multipole')
end

box off
%h=legend({'supression factor'},'Location','Northwest')
%a=get(h,'position'); a(2)=a(2)-0.1; set(h,'position',a);
%legend boxoff

haxes1 = get(gca,'Position');
haxes2 = axes('Position',haxes1,...
              'XAxisLocation','top',...
              'YAxisLocation','right',...
              'Color','none');
set(haxes2,'xtick',[],'ytick',[.2,.4,.6,.8,1],'yticklabel',[.2,.4,.6,.8,1]/5)

ylabel('BPWF')

print -depsc2 paper_plots/supfac.eps
%fix_lines('paper_plots/supfac.eps')
!epstopdf paper_plots/supfac.eps

return


%%%%%%%%%%%%%%%%%%%%%%%
function make_fieldoutline()

load maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0.mat

map=make_map(ac,m);
w=1./(map.Qvar+map.Uvar);

w(isnan(w))=0;
w=w/max(max(w));

ww=linspace(0,1,10000);

[xx,yy]=meshgrid(m.x_tic,m.y_tic);
yc=(m.hy+m.ly)/2;
sqdeg=0.25^2*cosd(yy-yc);

for k=1:numel(ww)
  keepind=w>ww(k);
  s(k)=sum(sqdeg(keepind));
end

target=373;
[dum,kind]=min(abs(s-target));
wval=ww(kind);

% Get perimeter points
wbin=w;
wbin(w<wval)=0;
wperim=bwperim(wbin);
raperim=xx(find(wperim));
decperim=yy(find(wperim));


%%%%%%%%%%%%%%
% Write data products

% Reorder to go around in a circle
ra0=0;dec0=-57;
[th,r]=cart2pol(raperim-ra0,decperim-dec0);
[dum,ind]=sort(th);

% To gal coords
[lperim,bperim]=euler(raperim,decperim,1,0);

raperim=raperim(ind);
decperim=decperim(ind);
lperim=lperim(ind);
bperim=bperim(ind);

fname='B2_3yr_373sqdeg_field_20140509.txt';
fid=fopen(fname,'w');
paper_print_generic_header(fid);
fprintf(fid,'# File: %s\n',fname);
fprintf(fid,'# Date: %s\n',datestr(now,'yyyy-mm-dd'));
fprintf(fid,'#\n');
fprintf(fid,'# BICEP2 field outline.\n');
fprintf(fid,'# The four columns are right ascension, declination, galactic\n');
fprintf(fid,'# longitude, and galactic latitude in degrees.\n');
fprintf(fid,'#\n');
fprintf(fid,'# The field outline is the polarization weight map contour enclosing\n');
fprintf(fid,'# 373 square degrees.\n');
fprintf(fid,'#\n');
fprintf(fid,'# Columns: RA, Dec., l, b\n');
fclose(fid);
dlmwrite(fname,[raperim,decperim,lperim,bperim],'precision','%0.2f','-append');

return


%%%%%%%%%%%%%%%%%%%%
function make_fts_asciifile

% FTS data file is inside aux_data on bicepfs1, but it's
% not actually part of the BICEP2 aux_data in CVS.
load('/n/bicepfs1/bicep2/bicep2_aux_data/fts/maxFTS.mat');

freq = FTS(1).freq/1e9;

% get indices of rgls
[p,ind]=get_array_info('20120510');

% collect all the rgl spectra into one array
rglspectra = zeros(412,317);
for ii = 1:206
    rglspectra(ii,:) = FTS(ind.rgla(ii)).spectra(1,:);
    rglspectra(206+ii,:) = FTS(ind.rglb(ii)).spectra(1,:);
end

% bad spectra:
indbad = [8 20 241 359 403 406];

% get rid of bad spectra
rglspectra(indbad,:) = NaN;


% take the average spectra over all rgls
avgspectra = nanmean(rglspectra);

avgspectra = avgspectra / max(avgspectra); %* opE;

[yr mo dy]=datevec(now);
fname=sprintf('B2_frequency_spectrum_%04d%02d%02d.txt',yr,mo,dy);

f=fopen(fname,'wt');
fprintf(f,'# BICEP2 2014 Data Release\n');
fprintf(f,'# The BICEP2 Collaboration, BICEP2 2014 II: EXPERIMENT AND THREE-YEAR DATA SET\n');
fprintf(f,'# http://bicepkeck.org/\n');
fprintf(f,'# \n');
fprintf(f,'# File: %s\n',fname);
fprintf(f,'# Date: %04d-%02d-%02d\n',yr,mo,dy);
fprintf(f,'# \n');
fprintf(f,'# BICEP2 150 GHz band frequency spectrum as in BICEP2 II Fig. 11.\n');
fprintf(f,'# The two columns are frequency in GHz and spectral response.\n');
fprintf(f,'# \n');
fprintf(f,'# Spectrum is given for an approximate point source and should be\n');
fprintf(f,'# divided by frequency^2 for use with an extended source.  It is\n');
fprintf(f,'# given in power units and peak-normalized.  The spectrum has not been\n');
fprintf(f,'# corrected for the frequency^2 spectrum of the Rayleigh-Jeans source.\n');
fprintf(f,'# \n');
fprintf(f,'# Negative values below 10^-3 are at the noise level of the\n');
fprintf(f,'# Fourier-transform spectroscopy measurement.\n');
fprintf(f,'# \n');
fprintf(f,'# Columns: f, r(f)\n');
for i=1:length(freq)
  fprintf(f,'%.2f,%.5f\n',freq(i),avgspectra(i));
end
fclose(f);

return


%%%%%%%%%%%%%%%%%%%%
function r = T_CMB_ratio_synch(f1, f2, beta)
%f1, f2 in GHz
%beta - spectral index
%r = ratio of synchrotron map amplitude in T_CMB
  x1 = f1 ./ 56.75;
  x2 = f2 ./ 56.75;
  r = ((x2 ./ x1) .^ (beta - 2)) .* exp(x1-x2) .* ((exp(x2)-1)/(exp(x1)-1)).^2;
return


%%%%%%%%%%%%%%%%%%%%%
% B2 B-mode map, minus axes and labels
% Copied from make_ebplot, then modified.
function make_prl_cover

ellrng=[50,120];
projfile='matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat';

load maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0
map=make_map(ac,m,coaddopt);
maps=cal_coadd_maps(map,get_ukpervolt);
maps=make_ebmap(m,maps,[],ellrng,projfile);

fig=figure;
clf;
dim=get_fourpaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.mapw dim.maph], 'PaperUnits', 'inches');
set(fig, 'Color', [1 1 1]);

% color range
cb=2.4/6.4;

% vector length scale factor
vb=0.6*6.4;

% B

ax3=axes('Position',[0,0,1,1]);
plot_map(m,maps.B,'iau');
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,maps.BQ,maps.BU,maps.Qvar,maps.Uvar,vb,[],3,1);
axis off;

% Delete any words in the figure.
h = get(gca, 'Children');
h1 = h(find(strcmp(get(h, 'type'), 'text')));
for x=h1
  delete(x);
end

% Delete the single longest polarization line -- this is the one
% that was drawn just for scale.
h = get(gca, 'Children');
h1 = h(find(strcmp(get(h, 'type'), 'line')));
dx = cell2mat(get(h1, 'XData'));
dy = cell2mat(get(h1, 'YData'));
len2 = (dx(:,1) - dx(:,2)).^2 + (dy(:,1) - dy(:,2)).^2;
delete(h1(find(len2 == max(len2))));

% this works way better than direct print to pdf
print -depsc2 paper_plots/prl_cover.eps
fix_lines('paper_plots/prl_cover.eps')
!epstopdf paper_plots/prl_cover.eps

return
