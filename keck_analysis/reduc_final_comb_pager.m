function reduc_final_comb_pager(rf)
%
% make .html files to provide pager reduc_final_comb
%  reduc_final_comb_pager('sim005_filtp3_weight2_jack0')
%  
system('rm -Rf reduc_final_comb_pager')
system('mkdir reduc_final_comb_pager')

make1(rf, 200);
make1(rf, 500);

return

% make pages
function make1(rf, lmax)

reduc_final_comb(rf,lmax,1)
 
l={'TT','TE','EE','BB','TB','EB'};

%write out gifs
for j=1:length(l)

fh=fopen(sprintf('reduc_final_comb_pager/%s_comb_%s_%i.html',rf,l{j},lmax),'w');
fprintf(fh,'<a href="../">up</a> ------ ');


% write spec types
fprintf(fh,'');
for i=1:length(l)
  if(i~=j)
    fprintf(fh,'<a href="%s_comb_%s_%i.html">%s</a>, ',rf,l{i},lmax,l{i});
  else
    fprintf(fh,'%s, ',l{i});
  end
end
fprintf(fh,'\n');

% Choose set
fprintf(fh,'------ ');
switch lmax
 case 200
  fprintf(fh,'<a href="%s_comb_%s_500.html">lmax=500</a>',rf,l{j});
 case 500
  fprintf(fh,'<a href="%s_comb_%s_200.html">lmax=200</a>',rf,l{j});
end

% include the figure
fprintf(fh,'<p><img src="%s_comb_%s_%i.gif"></a>\n',rf,l{j},lmax);

fclose(fh);


end
return
