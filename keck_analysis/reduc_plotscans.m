function reduc_plotscans(tag,out,rxin,cuts)
% reduc_plotscans(tag,out,rx,cuts)
%
% Make scan plots for web post
%
% tag = single tag
%
% out = 0, print to screen (default)
%     = 1, write plot to reducplots/ 
%
% rx = single value, vector, or cell array of either specifying which receivers to plot
%      NOTE for BICEP3, p.rx set to p.mce for plotting to get per-mce plots 
%
% cuts = string or cell array of strings
%         - none: no cuts
%         - noise: cuts noisy channels (default)
%         - round1: applies round 1 cuts
%         - round2: applies round 2 cuts
%
% e.g. tags=get_tags('cmb2010')
%      reduc_plotscans(tags{1},1,{0,1,2,[0,1,2]});
% 

[p,ind]=get_array_info(tag);

exptn=get_experiment_name();

if strcmp(exptn,'bicep3')
  p.rcvr=p.rx; % save copy of real p.rx for cuts
  p.rx=p.mce; % treat mces as separate rx's to get per-mce plots
  rxexts='mce'; % name files as 'mce'
  rxextsc='Mce'; % name plots as 'Mce'
else
  rxexts='rx';
  rxextsc='Rx';
end

if(~exist('out','var'))
  out=0;
end

if ~exist('rxin','var')
  rxin=[];
end
if isempty(rxin)
  rx=unique(p.rx);
  for i=1:numel(rx)
    rxin{i}=rx(i);
  end
  if(i>1)
    % make per-frequency lists:
    freqs = unique(p.band(ind.la));
    for ff=1:length(freqs)
      % Encapsulate list in cell so that even if only 1 receiver is at a
      % given frequency, we can later discriminate this from the per-receiver
      % plotting.
      rxin{end+1} = {unique(p.rx(ind.(sprintf('l%03da', freqs(ff)))))};
    end
    rxin{end+1}=rx; % this should be plotted last because this is the one used to
                    % check for missing status in reduc_processnewtags_driver.
  end
end
if(~iscell(rxin))
  rxin={rxin};
end
if ~exist('cuts','var')
  cuts='noise';
end
if ischar(cuts)
  cuts={cuts};
end

%close all 
if(ishandle(1))
  set(0,'CurrentFigure',1);
else
  figure(1);
end

if(out==1)
  close(1);
  set(0,'DefaultFigureVisible','off');
else
  figure(1);
end

% size figure appropriately for screen
setwinsize(gcf,1100,575);
drawnow


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get calibrated tod and setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load(['data/real/',tag(1:6),'/',tag,'_tod']);

% Drop pathological half-scans
fs.sf=fs.sf(fs.sf>0); fs.ef=fs.ef(fs.ef>0);
fs.s =fs.s (fs.s>0);  fs.e =fs.e (fs.e>0);

% mask the non-scan data
s.sf=[fs.sf;en.sf]; s.ef=[fs.ef;en.ef];
mapind=make_mapind(d,s);
d.mce0.data.fb(~mapind,:)=NaN;

% Cut out any half-scans with glitches
d.mce0.data.fb=cut_incomp_chanblk(d.mce0.data.fb,fs,ind.e);

% sum/diff pairs
d=sumdiff_pairs(d,p,fs,ind.a,ind.b);
d=sumdiff_pairs(d,p,en,ind.a,ind.b);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the following is for calculating weights and PSDs which can use a
% different filter type than for plotting

% remove p3 from per-channel and sum/diff timestreams for weights
wfilt='p3';
dd=filter_scans(d,fs,wfilt,ind.e);

% calculate covariance and correlation matrices
mapind=make_mapind(d,fs);
corrarray=corrcoef(dd.mce0.data.fb(mapind,:));
covarray=cov(dd.mce0.data.fb(mapind,:));

% ground subtract for weight calculation and for NET calculation
dd=ground_subtract(dd,fs,ind.l);

% calculate standard deviations of half scans
sd=scan_std(dd,fs); % sum/diff standard deviations

% PSD for NETs
NETfilt=wfilt;
[psdNET,f]=mean_psd(dd,fs);

clear dd;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the following is for plotting

% remove p0 from per-channel and sum/diff timestreams for plotting
plotfilt='p0';
d=filter_scans(d,fs,plotfilt,ind.e);
d=filter_scans(d,en,plotfilt,ind.e);

% pair diff PSDs for plotting
[psd,f]=mean_psd(d,fs);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% median sd for noise cut below and for front to back plot order
sdmed=nanmedian(sd,1); % median over half scans

% store default plot colors
defcm=get(gcf,'DefaultAxesColorOrder');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% colormap for plotting
indtmp=strip_ind(ind,find(p.rx==0));
Nchan=numel(indtmp.e);
cmap=jet(Nchan);
cmap(indtmp.b)=cmap(indtmp.a);
cmap=repmat(cmap,[5,1]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% what channels should we plot? Define a cut mask.

for ci=1:numel(cuts)

  cm=true(size(d.mce0.data.fb));

  % clear out cut message buffer at beginning of each cut type
  cutmsgbuf = {};

  if strcmp(exptn,'bicep3')
    p.rx=p.rcvr; % switch back to real p.rx for cut calcs 
    noisefrac=1.5; % noise cut threshhold smaller for B3 
  else
    noisefrac=2;
  end

  switch cuts{ci}
    
   case 'noise'
    % Cut channels with std too high for plotting purposes; BICEP2 seems to not need this
    rx=unique(p.rx);
    for i=1:numel(rx)
      indrx=strip_ind(ind,find(p.rx==rx(i)));
      cutthresh=noisefrac*nanmedian(sdmed(indrx.rgl));
      cutind=find(sdmed>cutthresh);
      cutind=intersect(cutind,indrx.e);
      cm(:,cutind)=false;
    end
    cutext='';
    
   case 'round1'
    load(sprintf('data/real/%s/%s_cutparams.mat',tag(1:6),tag));
    cut1=get_default_round1_cuts;
    c1=eval_round1_cuts(cp,cut1,p,ind);
    [c1.overall,cutmsg]=combine_cuts(c1,ind);
    for i=1:length(fs.sf)
      cm(fs.sf(i):fs.ef(i),c1.overall(i,:)==0)=false;
    end
    cutext='_round1';

    % store cut messages for later
    cutmsgbuf = [cutmsgbuf {''} {'overall round1 cuts'} cutmsg];

   case 'round2'
    cm1=cm;
    cm2=cm;
    
    % round 1 cut mask
    load(sprintf('data/real/%s/%s_cutparams.mat',tag(1:6),tag));
    cut1=get_default_round1_cuts;
    c1=eval_round1_cuts(cp,cut1,p,ind);
    c1.overall=combine_cuts(c1,ind);
    for i=1:length(fs.sf)
      cm1(fs.sf(i):fs.ef(i),c1.overall(i,:)==0)=false;
    end

    % round 2 cut mask
    cut2=get_default_round2_cuts([],tag(1:4));
    c2=eval_round2_cuts(cp,c1,cut2,p,ind);
    [c2.overall,cutmsg]=combine_cuts(c2,ind);
    cm2(:,~c2.overall)=false;

    % combine round 1 and round 2
    cm=cm1 & cm2;
    
    clear cm1 cm2
    
    cutext='_round2';
    
    % save cut messages
    cutmsgbuf = [cutmsgbuf {''} {'overall round2 cuts'} cutmsg];

   case 'none'
    % plot everything
    cutext='_none';
    
   otherwise
    % error
    error('That is not a defined cut.');
  end
  
  % cut entire pairs
  cm0=cm(:,ind.a) & cm(:,ind.b);
  cm(:,ind.a)=cm0;
  cm(:,ind.b)=cm0;
  clear cm0;
  
  if strcmp(exptn,'bicep3')
    p.rx=p.mce; % switch back to p.mce before plotting to get per-mce plots
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % loop over each combinations of receivers
  for j=1:length(rxin)

    % Per-frequency plots are boxed inside a cell (to distinguish single-rx
    % frequency plotting from the rx plotting).
    if iscell(rxin{j})
      rx = rxin{j}{1};
      allfreq = true;

    % Otherwise, plot as per-receiver/MCE or all receivers/MCEs
    else
      rx = rxin{j};
      allfreq = false;
    end

    % only plot receivers that are actually present in the data
    rx=intersect(rx,p.rx);
    
    % massage ind to only include the requested rx
    indrx=strip_ind(ind,find(ismember(p.rx,rx)));

    % get a masked version of the az offset
    ma=d.pointing.hor.az;
    ma(~mapind)=NaN;

    % Determine the frequency of this/these receivers. If the frequencies are
    % mixed, label as 'multi'.
    rxfreq = unique(p.band(indrx.l));
    
    if (length(unique(rxfreq)) > 1)
      freq = 'multi';
    else
      freq = sprintf('%03d', rxfreq(1));
    end

    [dum,sortind]=sort(sdmed(indrx.rgla),'descend');
    sumorderind=indrx.rgla(sortind);
    diforderind=indrx.rglb(sortind);

    % file name extension for multiple receivers
    if( numel(unique(p.rx)) > numel(rx) )
      if( allfreq )
        rxext = sprintf('_all%s', freq);
      else
        rxext=['_',rxexts,int2str(intersect(rx,p.rx))];
        rxext=rxext(rxext~=' ');
      end
    else
      rxext='';
    end

    % Re-run the cuts after stripping ind to get specific cuts for this
    % plotting choice.
    if ~isempty(rxext)
      switch cuts{ci}
        case 'round1'
          [dum,cutmsg]=combine_cuts(c1,indrx);
          cutmsgbuf = [cutmsgbuf {''} {[rxext(2:end) ' round1 cuts']} cutmsg];

        case 'round2'
          [dum,cutmsg]=combine_cuts(c2,indrx);
          cutmsgbuf = [cutmsgbuf {''} {[rxext(2:end) ' round2 cuts']} cutmsg];
      end
    end

    % loop over scanset blocks
    for i=1:length(fsb.s)
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % plot scans
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
      
      % ELNODS
      
      % find the preceding el nod
      x=fsb.t(i)-en.t; x(x<0)=NaN; [dummy,k]=nanmin(x);
      t0=en.t(1);
      
      sf=en.sf(k); ef=en.ef(k);
      t=(d.t(sf:ef)-t0)*24;
      xl=([max(min(t),0),max(t)]);
      
      % el motion
      subplot(3,7,1)
      set(gcf,'DefaultAxesColorOrder',defcm)
      plot(t,d.pointing.hor.el(sf:ef),'g');
      axis tight; xlim(xl);
      title(sprintf('elnod %d',k));
      ylabel('el')

      % pair sum
      subplot(3,7,8)
      set(gcf,'DefaultAxesColorOrder',cmap(sumorderind,:))
      c=cm(sf:ef,sumorderind);
      plot(t,d.mce0.data.fb(sf:ef,sumorderind).*c./c);
      axis tight; xlim(xl); ylim([-100,100]);
      title(sprintf('%sGHz',freq))

      % pair diff
      subplot(3,7,15)
      set(gcf,'DefaultAxesColorOrder',cmap(sumorderind,:));
      c=cm(sf:ef,diforderind);
      plot(t,d.mce0.data.fb(sf:ef,diforderind).*c./c);
      axis tight; xlim(xl); ylim([-100,100]);
      title(sprintf('%sGHz',freq))
      
      % FIELD SCANS
      
      % find field scan start/stop
      sf=fsb.sf(i); ef=fsb.ef(i);
      t=(d.t(sf:ef)-t0)*24;
      xl=([max(min(t),0),max(t)]);

      % azimuth
      subplot(3,7,[2:6])
      set(gcf,'DefaultAxesColorOrder',defcm)
      plot(t,d.pointing.hor.az(sf:ef),'b')
      hold on
      plot(t,ma(sf:ef),'r');
      hold off
      axis tight; xlim(xl);
      ylabel('az')

      % pair sum
      subplot(3,7,[9:13])
      set(gcf,'DefaultAxesColorOrder',cmap(sumorderind,:))
      c=cm(sf:ef,sumorderind);
      plot(t,d.mce0.data.fb(sf:ef,sumorderind).*c./c);
      xlim(xl); ylim([-20,20]);
      title([plotfilt,' filt; pair-sum'])

      % Helpful key to 5 noisiest pairs that are plotted
      sdplotted=nanstd(d.mce0.data.fb(sf:ef,sumorderind).*c./c);
      [dummy,tmpind]=sort(sdplotted,'descend');
      chanlist=sumorderind(tmpind);
      chanlist=chanlist(~isnan(dummy));
      if length(chanlist)>=5
        chanlist=chanlist(1:5);
      else
        chanlist=[];
      end
    
      chanlabel='5 noisiest plotted pair-sum (mce/gcpA): ';
      for ii=1:numel(chanlist)
        chanlabel=[chanlabel, sprintf(' %d/%d',p.mce(chanlist(ii)),p.gcp(chanlist(ii)))];
      end
      text(0.02,0.1, chanlabel, 'FontSize',6,...
           'horizontalalignment','left','Units','normalized','BackgroundColor',[0.8,0.8,0.8]);
      
      % pair diff
      subplot(3,7,[16:20])
      set(gcf,'DefaultAxesColorOrder',cmap(sumorderind,:))
      c=cm(sf:ef,diforderind);
      plot(t,d.mce0.data.fb(sf:ef,diforderind).*c./c);
      xlim(xl); ylim([-20,20]);
      title([plotfilt,' filt; pair-diff'])

      % plot trailing elnod if present
      x=fsb.t(i)-en.t; x(x>0)=NaN; [dummy,k]=nanmax(x);
      
      if(~isnan(dummy))
        sf=en.sf(k); ef=en.ef(k);
        t=(d.t(sf:ef)-t0)*24;
        xl=([max(min(t),0),max(t)]);
        
        % el motion
        subplot(3,7,7)
        set(gcf,'DefaultAxesColorOrder',defcm)
        plot(t,d.pointing.hor.el(sf:ef),'g');
        axis tight; xlim(xl);
        title(sprintf('elnod %d',k));
        ylabel('el')
        
        % individual channels
        subplot(3,7,14)
        set(gcf,'DefaultAxesColorOrder',cmap(sumorderind,:))
        c=cm(sf:ef,sumorderind);
        plot(t,d.mce0.data.fb(sf:ef,sumorderind).*c./c);
        axis tight; xlim(xl); ylim([-100,100]);
        title(sprintf('%sGHz',freq))
        
        % pair diff
        subplot(3,7,21)
        set(gcf,'DefaultAxesColorOrder',cmap(sumorderind,:))
        c=cm(sf:ef,diforderind);
        plot(t,d.mce0.data.fb(sf:ef,diforderind).*c./c);
        axis tight; xlim(xl); ylim([-100,100]);
        title(sprintf('%sGHz',freq))
      end
      
      % super title
      gtitle(sprintf('Run %s, scan block %d; %s: %s',...
                     strrep(tag,'_','\_'),i,rxextsc,num2str(intersect(p.rx,rx))));
      axes('Position',[0,0,1,1],'Visible','off');
      
      % subtitle
      c=any(cm,1);
      text (0.5,.03,sprintf('%d of %d rgl pairs have no data plotted after cuts ''%s''',...
                            numel(find(~c(indrx.rgla))),numel(indrx.rgla),cuts{ci}),...
            'Units','Normalized','FontSize',6,'HorizontalAlignment','center');      
      

      % plot
      if(out==1)
        fext='';
        saveplot(tag,i,rxext,fext,cutext)
      else
        pause
      end
      
      
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % plot elnod statsistic histograms
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
      clf
      
      % cut mask
      c=any(cm,1);
      c=c(indrx.rgl);
      
      % all elnod gains
      subplot(2,3,1)
      data=mean(en.g(:,indrx.rgl,2),1);
      data=data.*c./c;
      xtit='gain (ADU/airmass)';
      plottit='rgl elnod relgains (lead/trail mean)';    
      plothist(data,-1e3,1e4,xtit,plottit);
      
      % lead / trail elnod relgain difference
      subplot(2,3,2)
      data=en.g(2,indrx.rgl,2)-en.g(1,indrx.rgl,2);
      data=data.*c./c;
      xtit='\Deltagain (ADU/airmass)';
      plottit='trail minus lead rgl elnod relgains';    
      plothist(data,-1000,1000,xtit,plottit);
      
      % goodness of fit
      subplot(2,3,3)
      data=log10(mean(en.g(:,indrx.rgl,3)));
      data=data.*c./c;
      % get rid of non finite data from weird chi-squareds
      data=data(isfinite(data)); 
      xtit='log_{10}(\chi^2)';
      plottit='log_{10} of \chi^2 goodness of fit';    
      plothist(data,0,5,xtit,plottit);
      
      % elnod vs gcp
      subplot(2,3,4:6,'Visible','off')
      xlabel('gcp channel number of A pair member','Visible','on');
      ylabel('gain (ADU/airmass)','Visible','on');
      title('rgl elnod relgains (lead/trail mean)','Visible','on');
      c=any(cm,1);
      data=mean(en.g(:,:,2),1).*c./c;
      plotvsgcp(data,rx,p,ind,[0,1.5e4],0,1);
      %title('rgl elnod relgains (lead/trail mean)','Visible','on')
      
      % super title
      gtitle(sprintf('Elnod statistics; Run %s, scan block %d; %s: %s',...
                     strrep(tag,'_','\_'),i,rxextsc,num2str(intersect(p.rx,rx))),0.98);
      
      % plot
      if(out==1)
        fext='_elnod';
        saveplot(tag,i,rxext,fext,cutext)
      else
        pause
      end
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % pair sum/diff stddev statistics
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
      clf
      
      c0=cm(fs.sf,:);
      
      % pair sum weights
      subplot(2,2,1)
      data=1./sd(:,indrx.rgla).^2;
      c=c0(:,indrx.rgla);
      data=data.*c./c;
      xtit=sprintf('1/%s variance',wfilt);plottit='rgl pair sum weights';
      plothist(data(isfinite(data)),0,2,xtit,plottit)
      
      % pair diff weights
      subplot(2,2,2)
      data=1./sd(:,indrx.rglb).^2;
      c=c0(:,indrx.rglb);
      data=data.*c./c;
      xtit=sprintf('1/%s variance',wfilt);plottit='rgl pair diff weights';
      plothist(data(isfinite(data)),0,50,xtit,plottit)
      
      % stddev vs. channel plot title and labels
      subplot(2,2,3:4,'Visible','off');
      xlabel('gcp channel number of A pair member','Visible','on');
      ylabel('w','Visible','on');
      tit1='rgl pair diff weights avg. over halfscans';
      tit2=sprintf(' (s/d->%s->gs->\\Sigma\\sigma^{-2}_{hs}/N_{hs})',wfilt);
      tit3=' (dashed lines denote tile boundaries)';
      title([tit1,tit2,tit3],'Visible','on');
      sdd=sd.*c0./c0;
      data=nanmean(1./sdd.^2,1);
      plotvsgcp(data,rx,p,ind,[0,40],0);
      
      gtitle(sprintf('stddev statistics; Run %s, scan block %d; %s: %s',...
                     strrep(tag,'_','\_'),i,rxextsc,num2str(intersect(p.rx,rx))),0.98);
      
      if(out==1)
        fext='_stddev';
        saveplot(tag,i,rxext,fext,cutext)
      else
        pause
      end
      
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % per channel NETs
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
      clf
      
      % median array elnod response in ADU per airmass
      eng=nanmean(nanmedian(en.g(:,ind.rgl,2),2),1);
      
      % noise is mean in .1-1 Hz band, averaged over halfscans
      % (theoretically, psd is times 2 because we have a single sided psd here then divided
      % by 2 to go from ADU^2/Hz to ADU^2-s, which is necessary according to Justus)  
      flo=.1; fhi=1;
      [dum,floind]=min(abs(f-flo));
      [dum,fhiind]=min(abs(f-fhi));
      n=sqrt( mean(psdNET(floind:fhiind,:),1) );
      
      % copy pair diff noise into A pair
      n(ind.a)=n(ind.b);
      
      % effective individual channel noise is sqrt(2) greater than pair diff noise
      % (remember, sumdiff_pairs averages)
      n=n*sqrt(2);
      
      % ratio of elnod response to noise (ADU_signal/airmass)/ADU_noise = SNR/airmass
      ratio=eng./n;
      
      % zenith temperature (Kevlin_cmb/airmass)
      ztmp=12.7;
      
      % SNR/Kelvin_cmb
      SNR=ratio/ztmp;
      
      % per-channel NET = 1/(SNR/Kevlin_cmb)*1e6 uK/K (uK sqrt(s))
      NET=(1./SNR)*1e6;
      
      % calculate per-pair NET and put in A and B pair
      NET(ind.b)=1./sqrt(1./NET(ind.a).^2+1./NET(ind.b).^2);
      NET(ind.a)=NET(ind.b);
      
      % mask NETs
      c=any(cm,1);
      NET=NET.*c./c;
      
      % plot per pair net histogram
      subplot(2,2,1)
      data=NET(indrx.rglb);
      xtit='NET (\muK_{CMB} s^{1/2})';plottit='per-pair NET';
      plothist(data(isfinite(data)),150,1100,xtit,plottit);
      set(gca,'XTick',0:150:1100);
      
      % calculate median NET and plot a line and text
      yl=ylim();
      medNET=nanmedian(NET(indrx.rglb));
      line([medNET,medNET],[yl(1),yl(2)]);
      text(medNET*1.03,yl(2)*.9,sprintf('median = %0.0f',medNET),'Color','b');
      
      % calculate array NET as median per-pair NET / sqrt(Npairs) and add as text
      npairs=numel(intersect(find(c),indrx.rglb));
      arrayNET=medNET/sqrt(npairs);
      text(medNET*1.03,yl(2)*.6,...
           sprintf('N_{pairs} = %0.0f',npairs),'Color','b'); 
      text(medNET*1.03,yl(2)*.75,...
           sprintf('NET_{med}/N_{pair}^{1/2} = %0.0f',arrayNET),'Color','b');
      
      % calculate array NET correctly
      arrayNET=1/sqrt(nansum(1./NET(indrx.rglb).^2));
      text(medNET*1.03,yl(2)*.45,...
           sprintf('(\\SigmaNET_i^{-2})^{-1/2}=%0.0f',arrayNET),'Color','b');
      
      % plot per-pair NET vs. p3 pair-diff weight mean over halfscans
      subplot(2,2,2)
      w=nanmean(1./sdd.^2,1);
      col={'b','r','g','c','m'};
      clear legstr
      for k=1:numel(rx)
        doind=ind.rglb(p.rx(ind.rglb)==rx(k));
        xdata=NET(doind); ydata=w(doind);
        % get rid of weird negative values so loglog doesn't spew error messages
        posind=xdata>0&ydata>0;	
        loglog(xdata(posind),ydata(posind),'.','Color',col{k});hold on;
        legstr{k}=sprintf('%s%d',rxextsc,rx(k));
      end
      hold off
      if(numel(rx)>1)
        legend(legstr,'Location','NorthEast');
      end
      xlim([100,2000]);ylim([1e-1,1e2]);
      ylabel('w');xlabel('NET (\muK_{CMB} s^{1/2})');
      title(sprintf('per-pair NET vs. weight (s/d->%s->gs->\\Sigma\\sigma^{-2}_{hs}/N_{hs})',wfilt));
      grid on; set(gca,'XMinorGrid','on');
      
      % per pair NET plotted vs. gcp number
      subplot(2,2,3:4,'Visible','off');
      % get rid of negative data to avoid error messages
      NET(NET<0)=NaN;
      % x label
      xlabel('gcp channel number of A pair member','Visible','on');
      ylabel('NET (\muK_{CMB} s^{1/2})','Visible','on');
      % really long title so break it up
      tit1='per-pair NETs';
      tit2=sprintf(' (T_{zenith}=%0.1f K_{cmb})',ztmp);
      tit3=sprintf(' ( s/d->%s->gs-\\SigmaPSD_{hs}/N_{hs}->\\Sigma(PSD_{%0.1f<f<%0.1f})/N_f )',NETfilt,flo,fhi);
      tit4=' (dashed lines denote tile boundaries)';
      title([tit1,tit2,tit3,tit4],'Visible','on');
      plotvsgcp(NET,rx,p,ind,[100,2000],1);
      
      % super title
      gtitle(sprintf('NETs; Run %s, scan block %d; %s: %s',...
                     strrep(tag,'_','\_'),i,rxextsc,num2str(intersect(p.rx,rx))),0.98);
      
      % plot
      if(out==1)
        fext='_NET';
        saveplot(tag,i,rxext,fext,cutext)
      else
        pause
      end
      
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % plot pair diff PSDs
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
      clf

      xl=[.05,10];
      yl=[1e-3,10];
      
      clear ydata_dif
      clear ydata_sum
      clear ydatamed_dif
      clear ydatamed_sum
      
      %c=repmat(all(cm,1),[size(psd,1),1]); %this cuts channels that have any half-scans cut
      c=repmat(any(cm,1),[size(psd,1),1]); %this leaves channels that have at least one half-scan
      psdc=psd.*c./c;
      
      % calculate PSD mean/median over channels on a per receiver basis
      for k=1:numel(rx)
        % by receiver
        ind0=strip_ind(ind,find(p.rx==rx(k)));
        % mean / median over pairs
        ydata_dif(:,k)=nanmean(psdc(:,ind0.rglb),2);
        ydatamed_dif(:,k)=nanmedian(psdc(:,ind0.rglb),2);
        ydata_sum(:,k)=nanmean(psdc(:,ind0.rgla),2);
        ydatamed_sum(:,k)=nanmedian(psdc(:,ind0.rgla),2);
        % construct legend
        legstr{k}=sprintf('%s%d mean',rxextsc,rx(k));
        legstrmed{k}=sprintf('%s%d median',rxextsc,rx(k));      
      end
      % if more than one rx on plot also calculate PSD mean/median over all rx's
      if(numel(rx)>1)
        k=k+1;
        ydata_dif(:,k)=nanmean(psdc(:,indrx.rglb),2);
        ydatamed_dif(:,k)=nanmedian(psdc(:,indrx.rglb),2);
        ydata_sum(:,k)=nanmean(psdc(:,indrx.rgla),2);
        ydatamed_sum(:,k)=nanmedian(psdc(:,indrx.rgla),2);
        
        legstr{k}=[rxextsc ' {',num2str(rvec(rx)),'} mean'];
        legstrmed{k}=[rxextsc ' {',num2str(rvec(rx)),'} median'];    
      end
      
      % Can only set default color order before first plot, so if we want to plot the
      % mean/median lines in a different color order than the underlying PSDs we must plot
      % at the same time 
      ydata_dif=[ydata_dif,psdc(:,indrx.rglb)];
      ydata_sum=[ydata_sum,psdc(:,indrx.rgla)];
      
      % plot pair diff
      subplot(1,2,1)
      set(gcf,'DefaultAxesColorOrder',[lines(k);autumn(numel(indrx.rglb))]);
      loglog(f,ydata_dif);
      % plot mean/median a second time with thicker lines
      hold on; h=loglog(f,ydata_dif(:,1:k),'LineWidth',3);
      hmed=loglog(f,ydatamed_dif,'--','LineWidth',2);    
      legend([h;hmed],{legstr{1:end},legstrmed{1:end}},'Location','NorthEast');
      xlim(xl);ylim(yl);
      title(sprintf('%d rgl pair diff PSDs',numel(intersect(indrx.rglb,find(~isnan(psdc(1,:)))))));
      grid on;
      
      % plot pair sum
      subplot(1,2,2)
      set(gcf,'DefaultAxesColorOrder',[lines(k);autumn(numel(indrx.rgla))]);
      loglog(f,ydata_sum);
      % plot mean/median a second time with thicker lines
      hold on; h=loglog(f,ydata_sum(:,1:k),'LineWidth',3);
      hmed=loglog(f,ydatamed_sum,'--','LineWidth',2);    
      legend([h;hmed],{legstr{1:end},legstrmed{1:end}},'Location','NorthEast');
      xlim(xl);ylim(yl);
      xlabel('f (Hz)');ylabel('PSD (ADU^2/Hz)');
      title(sprintf('%d rgl pair sum PSDs',numel(intersect(indrx.rgla,find(~isnan(psdc(1,:)))))));
      grid on;
      
      % super title
      gtitle(sprintf('PSDs (s/d->%s->\\SigmaPSD_{hs}/N_{hs}); Run %s, scan block %d; %s: %s',...
                     plotfilt,strrep(tag,'_','\_'),i,rxextsc,num2str(intersect(p.rx,rx))),0.97);
      
      % plot
      if(out==1)
        fext='_PSD';
        saveplot(tag,i,rxext,fext,cutext)
      else
        pause
      end
      
      
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % plot correlation / covariance matrix
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
      clf
      
      c0 = any(cm,1);
      c = bsxfun(@and, c0, c0');
      csum=c(indrx.la,indrx.la);
      cdif=c(indrx.lb,indrx.lb);
      
      % correlation pair sum:
      subplot(2,2,1)
      corrsum=corrarray(indrx.la,indrx.la);
      corrsum(isnan(corrsum))=-1.1;
      corrsum=corrsum.*csum./csum;
      cmapc=jet(length(corrsum));
      cmapc(1,:)=[1,1,1];
      imagesc(corrsum);
      caxis([-1.1 1])
      title('Correlation, pair sum')
      axis square
      colormap(cmapc)
      colorbar
      
      % correlation pair diff:
      subplot(2,2,2)
      corrdif=corrarray(indrx.lb,indrx.lb);
      corrdif(isnan(corrdif))=-1.1;
      corrdif=corrdif.*cdif./cdif;
      imagesc(corrdif);
      caxis([-.2 .2])
      title('Correlation, pair diff')
      axis square   
      colormap(cmapc)
      colorbar
      
      % covariance pair sum:
      subplot(2,2,3)
      covsum=covarray(indrx.la,indrx.la);
      covsum(isnan(covsum))=min(min(covsum))-1;
      covsum=covsum.*csum./csum;
      imagesc(covsum);
      caxis([-1 1])
      title('Covariance, pair sum')
      axis square
      colormap(cmapc)
      colorbar
      
      % covariance pair diff:
      subplot(2,2,4)
      covdif=covarray(indrx.lb,indrx.lb);
      covdif(isnan(covdif))=min(min(covdif))-1;
      covdif=covdif.*cdif./cdif;
      imagesc(covdif);
      caxis([-.2 .2])
      title('Covariance, pair diff')
      axis square
      colormap(cmapc)
      colorbar
      
      % super title
      gtitle(sprintf('Run %s, scan block %d; %s: %s',...
                     strrep(tag,'_','\_'),i,rxextsc,num2str(intersect(p.rx,rx))),0.98);
      
      % plot
      if(out==1)
        fext='_cov';
        saveplot(tag,i,rxext,fext,cutext)
      else
        pause
      end
      
      
    end % scan block
  end % rx combo

  switch cuts{ci}
    case {'round1','round2'}
      printcutmsg(tag, cutmsgbuf, cutext);
  end
end % cuts


if(out==1)
  close(gcf)
end
set(0,'DefaultFigureVisible','on');

return






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function saveplot(tag,i,rxext,fext,cutext)

setwinsize(gcf,1400,600);

if ~exist(sprintf('reducplots/%s/',tag(1:6)),'dir')
  system(['mkdir reducplots/' num2str(tag(1:6)) '/'])
  system(['chmod g+w reducplots/' num2str(tag(1:6)) '/'])
end

try
  mkpng(sprintf('reducplots/%s/%s_%03d%s%s%s.png',tag(1:6),tag,i,rxext,fext,cutext),1);
catch
  fname = sprintf('reducplots/%s/%s_%03d%s%s%s.png',tag(1:6),tag,i,rxext,fext,cutext);
  tempfname = strrep(fname,'.png','.tmp.png');
  system(['rm -f ' fname]);
  system(['rm -f ' tempfname]);
  mkpng(sprintf('reducplots/%s/%s_%03d%s%s%s.png',tag(1:6),tag,i,rxext,fext,cutext),1);
end
  
setpermissions(sprintf('reducplots/%s/%s_%03d%s%s%s.png',tag(1:6),tag,i,rxext,fext,cutext));

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plothist(data,xl,xh,xtit,plottit)

nbins=60;
if ~isempty(data)
  [bc,n]=hfill(data,nbins,xl,xh);
  bar(bc,n,'k','EdgeColor','none','BarWidth',1);xlim([xl,xh]);
end
ylabel('N');xlabel(xtit);title(plottit);
grid on;

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotvsgcp(data,rx,p,ind,yl,logplot,plotA)

if(~exist('plotA','var'))
  plotA=0;
end

exptn=get_experiment_name();
% data that is off the plot (high/low) is set to (max/min) ylim
data(data>yl(2))=yl(2);
data(data<yl(1))=yl(1);

% assume mce=rx, even in case of bicep3; parent code does translation
mce = rx;
 
pos=get(gca,'Position');
pos(3)=pos(3)/numel(mce);
for k=1:numel(mce)
  axes('position',pos);
  
  % cut index array by receiver
  indrx=strip_ind(ind,find(p.mce==mce(k)));

  
  % plot all B-pair quantities (and A-pair if requested) vs. A-pair gcp number
  if(~logplot)
    plot(p.gcp(indrx.rgla),data(indrx.rglb),'.k');
    hold on;
    if(plotA)
      plot(p.gcp(indrx.rgla),data(indrx.rgla),'+k');
    end
    hold off;
  else
    semilogy(p.gcp(indrx.rgla),data(indrx.rglb),'.k');
    hold on; 
    if(plotA)
      semilogy(p.gcp(indrx.rgla),data(indrx.rgla),'+k');
    end
    hold off;
  end

  if(k==numel(mce))
    if(plotA)
      legend('A','B','Location','NorthEast');
    end
  end
 
  if strcmp(exptn,'bicep3')
    nchan = max(p.gcp)+1;
    xlim([0,nchan]);ylim(yl);
    grid

    %tick marks and dash lines
    % 5 tiles/mce
    xt=0:(nchan/5/2):nchan;
    set(gca,'XTick',xt);
    
    xt=0:nchan/5:nchan;
    x0=[xt;xt]; 
    y0=zeros(size(x0)); y0(1,:)=yl(1); y0(2,:)=yl(2);
    line(x0,y0,'Color','k','LineStyle','--');

    % label plot by mce 
    text(0.1,0.9,sprintf('mce%d',mce(k)),...
        'Units','Normalized','FontSize',10,'HorizontalAlignment','center',...
        'BackgroundColor',[0.7,0.7,0.7],'EdgeColor','k','Color','k','FontWeight','bold');

  else
    % tick marks at tile boundaries
    xt=0:(132/2):528;
    set(gca,'XTick',xt);

    xlim([0,528]);ylim(yl);
    grid 
  
    % dashed lines at tile boundaries
    xt=0:132:528;
    x0=[xt;xt]; 
    y0=zeros(size(x0)); y0(1,:)=yl(1); y0(2,:)=yl(2);
    line(x0,y0,'Color','k','LineStyle','--');
  
    % label plot by receiver
    text(0.1,0.9,sprintf('Rx %d',rx(k)),...
        'Units','Normalized','FontSize',10,'HorizontalAlignment','center',...
        'BackgroundColor',[0.7,0.7,0.7],'EdgeColor','k','Color','k','FontWeight','bold');
  end  

  % inhibit y tick labels 
  if(k>1)
    set(gca,'YTickLabel','') 
  end
  
  % do the next plot to the right
  pos(1)=pos(1)+pos(3);
  
  
end

return



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function printcutmsg(tag,cutmsg,cutext)

cutmsgdir=['reducplots/' num2str(tag(1:6)) '/cutmessages'];
fname=sprintf('%s%s.txt',tag,cutext);

if ~exist(cutmsgdir,'dir')
  cmd=sprintf('mkdir %s',cutmsgdir);
  system(cmd);
  setpermissions(cutmsgdir,'ug+rwx')
end

fullfname=fullfile(cutmsgdir,fname);
fid=fopen(fullfname,'w');

if fid==-1
  system(['rm -f ' fullfname])
  fid=fopen(fullfname,'w');
end

for i=1:numel(cutmsg)
  fprintf(fid,cutmsg{i});
  fprintf(fid,'\n');
end

fclose(fid);

setpermissions(fullfname);

return

