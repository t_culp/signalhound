function reduc_plotcomap_pager(mapname,apsopt,fixc,pad,realimag,dotemplates,doTQ,doHJ,doSL,doCross,doSub,om,lmax)
% reduc_plotcomap_pager(mapname,apsopt,fixc,pad,realimag,dotemplates,doTQ,doHJ,doSL,doCross,doSub,om,lmax)
%
% Build the standard "maps pager"
% 
%  apsopt - passed to aps_prepare_maps
%  fixc - fix color scale
%  pad - pad maps when making 2d aps
%  realimag - plot real/imag parts of 2d aps on left/right
%  dotemplates - plot the deproj templates
%  doTQ - plot the TQ and TU 2d aps
%  doHJ - plot half-jacks
%  doSL - plot the aps2d in log space (as well as linear)
%  doCross - plot the cross spectra 2d aps
%
%  doSub: '' (default),'psub','gsub','dsub', to plot regular maps, polyfiltered, ground subtracted
%         or deprojected signal components
%  
%  om: just make the missing plots
%
%  lmax: max l of speclin plots (default 500)
%
% e.g. reduc_plotcomap_pager({'1205/real_a_filtp3_weight3_gs_jack0.mat',...
%                            '1205/0011_a_filtp3_weight3_gs_jack0.mat',...
%                            '1205/0012_a_filtp3_weight3_gs_jack0.mat',...
%                            '1205/0013_a_filtp3_weight3_gs_jack0.mat'});

if(~exist('apsopt','var'))
  apsopt=[];
end

apsopt = get_default_apsopt(apsopt);

if ~exist('fixc','var') || isempty(fixc)
  fixc=true;
end
if ~exist('pad','var') || isempty(pad)
  pad=true;
end
if ~exist('realimag','var') || isempty(realimag)
  realimag=true;
end
if  ~exist('doTQ','var') || isempty(doTQ)
  doTQ=false;
end
if  ~exist('doHJ','var') || isempty(doHJ)
  doHJ=false;
end
if ~iscell(mapname)
  mapname={mapname};
end
if ~exist('dotemplates','var') || isempty(dotemplates)
  dotemplates=false;
end
if ~exist('doSL','var')
  doSL=false;
end
if ~exist('doCross','var') || isempty(doCross)
  doCross=false; % not currently working!
end
if ~exist('doSub','var') || isempty(doSub)
  doSub='';
end
if ~exist('om','var') || isempty(om)
  om=false;
end
if ~exist('lmax','var') || isempty(lmax)
  lmax=500;
end
% miss-use the apsopt to pass in designations for the plots
if(~isfield(apsopt,'mapname'))
  apsopt.mapname=[];
end

% fix color scale
if fixc
  % T/Q/U map color axes
  c{1,1}=[-165,165];
  c{2,1}=[-4,4];
  c{3,1}=[-4,4];
  c{4,1}=[-0.3,0.3]; %B
  c{5,1}=[-1.5,1.5]; %E
  c{6,1}=[-0.05,0.05]*3150; %T ground sub
  c{7,1}=[-0.5,0.5]*3150; %T poly sub
  c{8,1}=[-0.001,0.001]*3150; %Q&U ground sub
  c{9,1}=[-0.01,0.01]*3150; %Q&U poly sub
  c{10,1}=[-2000,2000]*3150; %Q&U deproj sub
  
  
  % speclin color axes
  caps{1,1}=[0,5]; % T
  caps{2,1}=[0,2e-4]; % Q
  caps{3,1}=[0,2e-4]; % U
  caps{4,1}=[0,2e-4]; % E
  caps{5,1}=[0,5e-5]; % B_normal
  caps{6,1}=[-25,25]*1e-3; % TE
  caps{7,1}=[-25,25]*1e-3; % TB_normal
  caps{8,1}=[-25,25]*1e-5; % EB_normal
  caps{9,1}=caps{5}; % B_kendrick
  caps{10,1}=caps{7}; % TB_kendrick
  caps{11,1}=caps{8}; % EB_kendrick
  caps{12,1}=caps{5}; % B_matrix
  caps{13,1}=caps{7}; % TB_matrix
  caps{14,1}=caps{8}; % EB_matrix
  % inter receiver cross spectra
  caps{15,1}=[-5,5]; % T
  caps{16,1}=[-1e-4,1e-4]; % Q
  caps{17,1}=[-1e-4,1e-4]; % U
  caps{18,1}=[-1e-4,1e-4]; % E
  caps{19,1}=[-2.5e-5,2.5e-5]; % B_normal
  caps{20,1}=caps{19}; % B_kendrick
  % subtracted signals
  caps{21,1}=caps{1,1}; % T
  caps{22,1}=caps{2,1}; % Q 
  caps{23,1}=caps{3,1}; % U
  caps{24,1}=[0,200]; % dsub
  
else
  c=[];
  caps=[];
end

if ~exist('reduc_plotcomap_pager','dir')
  system('mkdir reduc_plotcomap_pager');
end

set(0, 'DefaultFigureVisible', 'off')
setwinsize(gcf,900,700);

% this is the version which concatenates maps along the first dimension
% and uses the string replacement pattern
% it also works if just one map is handed in
% first fetch the maps according to the wildcard:
[s,d]=system_safe(sprintf('/bin/ls -1 maps/%s',mapname{1}));
% for some bizarre reason d now contains non-printing nonsense
d=strip_nonsense(d);
maps1=strread(d,'%s');
nmaps=length(maps1);

% this makes it backward compatible. If second part in mapname is not
% a cell, make it a cell: this indicates that just two maps are being read.
if length(mapname)>1 & ~iscell(mapname{2})
  mapname={mapname{1},{mapname{2:end}}};
end

% expand the hostdirs in the case just one hostdir is handed in:
if size(apsopt.hostdir,2)==1
  for jj = 2:length(mapname)
    apsopt.hostdir{end+1}=apsopt.hostdir{1};
  end
end

% loop through these maps...:
for ii=1:nmaps
  % get the file name for the maps which are going to be crossed
  clear cmaps;
  cmaps{1} = maps1{ii};

  % do string replacement to create additional filenames:
  % each cell array in mapname is one more filename.
  % in the cell array has the string replacement pattern:
  % the first string is replaced with the second string, 
  % the third with the forth and so on.
  % {'B2map*',{'B2','Keck','jack0','jack01'},{'B2','B1'},...}.
  % loop over the cells in mapname
  for jj = 2:length(mapname)
    % the initial map name is going to be tweaked to produce the other map names:
    map_next = cmaps{1};
    % loop through the replacement in key/values manner
    for kk = 1:2:length(mapname{jj})-1
      map_next = regexprep(map_next,mapname{jj}{kk},mapname{jj}{kk+1},'once');
    end
    cmaps{end+1} = map_next;
  end
  
  % prepare the maps:
  [mapstruct,apsopt,err] = aps_prepare_maps(cmaps,apsopt);
  
  % one of the maps to be crossed was missing
  if ~isnumeric(err) || err==1
    if isnumeric(err)
      error(['Error load map: ',cmaps{:}])
    else
      rethrow(err)
    end
  end
  
  % replace T->Tgsub etc if requested:
  mapstruct.map=replace_to_sub(mapstruct.map,doSub);
  
  % some fuzz needed for the titles of the figures
  for jj=1:length(cmaps)
    fn{jj} = strrep(cmaps{jj},'maps/','');
  end
  fn{end} = strrep(fn{end},'/',[doSub,'/']);
  if apsopt.commonmask
    fn{end}=strrep(fn{end},'.mat','_cm.mat');
  end
  
  use_base(fn,mapstruct,apsopt,c,caps,pad,realimag,dotemplates,doTQ,doHJ,doSL,doCross,doSub,om,lmax)
end

return

%%%%%%%%%%%%%%%%%%%%%%%
function [cj_out,cjaps_out]=use_base(fn,mapstruct,apsopt,cj_in,cjaps_in,pad,realimag,dotemplates,doTQ,doHJ,doSL,doCross,doSub,om,lmax)

% get the co-added maps
map = mapstruct.map;
m = mapstruct.m;
coaddopt=mapstruct.coaddopt;

% if the map is real, get rid of coaddopt.c as it slows things way down
if isfield(coaddopt,'c')
  coaddopt=rmfield(coaddopt,'c');
end

switch coaddopt.jacktype
 case 'f'
  do_cross=0;
 otherwise
  do_cross=1;
end

% form Pvar - the mean of Qvar and Uvar
% this is only used in calc_map_deptharea below - can it be gotten
% rid of? Seems Baroque
for i=1:size(map,1)
  map(i).Pvar=(map(i).Qvar+map(i).Uvar)/2;
end

% check to see if it is coadded by rx
if isempty(apsopt.mapname)
  switch numel(map)
   case 1
    rxlist={0};
   case 2
    if strmatch(fn{1}(1:2),'30') % SPT is 3000's
      rxlist={'spt95','spt150'};
    elseif length(findstr(strcat(fn{:}),'_c_'))==1
      rxlist={'Keck100','Keck150'};
    else
      rxlist={'Bicep2','Keck'};
    end
   case 5
    rxlist={'Rx0','Rx1','Rx2','Rx3','Rx4'};
   case 6
    rxlist={'B2','Rx0','Rx1','Rx2','Rx3','Rx4'};
   case 7
    rxlist={'B2','Rx0','Rx1','Rx2','Rx3','Rx4','Keck'};
  end
else
  rxlist = apsopt.mapname;
end

doPure = ~isempty(apsopt.purifmatname) | isfield(apsopt,'purifmat')

%colorscale for the maps:
if ~isempty(cj_in)
  c1=cj_in{1,1};
  c2=cj_in{2,1};
  c3=cj_in{3,1};
  fcs=true;
else
  c1=[];
  c2=[];
  c3=[];
  fcs=false;
end


% plot the maps
for i=1:size(map,1)
  [saT.s,saT.a]=calc_map_deptharea(m,map(i).T,map(i).Tw,map(i).Tvar);
  cj_out{1,i}=plot_and_save(m,map(i).T,c1,fn,i,fcs,om,'T','map',rxlist{i},saT);
  
  [saQ.s,saQ.a]=calc_map_deptharea(m,map(i).Q,map(i).Pw,map(i).Pvar);
  [saU.s,saU.a]=calc_map_deptharea(m,map(i).U,map(i).Pw,map(i).Pvar);  
  % combine Q and U PRL style:
  saQ.ac = (saQ.a+saU.a)./2;
  saQ.sc = sqrt(2)*(saQ.s^(-2)+saU.s.^(-2)).^-.5;
  saU.ac = saQ.ac; saU.sc = saQ.sc;
 
  cj_out{2,i}=plot_and_save(m,map(i).Q,c2,fn,i,fcs,om,'Q','map',rxlist{i},saQ);  
  cj_out{3,i}=plot_and_save(m,map(i).U,c3,fn,i,fcs,om,'U','map',rxlist{i},saU);
end

% plot the apodized map
for i=1:size(map,1)
  plot_and_save(m,map(i).T.*map(i).Tw./max(map(i).Tw(:)),c1,fn,i,fcs,om,'T','apmap',rxlist{i});
  plot_and_save(m,map(i).Q.*map(i).Pw./max(map(i).Pw(:)),c2,fn,i,fcs,om,'Q','apmap',rxlist{i});
  plot_and_save(m,map(i).U.*map(i).Pw./max(map(i).Pw(:)),c3,fn,i,fcs,om,'U','apmap',rxlist{i});
end

% plot the noise maps
for i=1:size(map,1)
  plot_and_save(m,map(i).Tvar,[],fn,i,fcs,om,'T','noi',rxlist{i});
  plot_and_save(m,map(i).Qvar,[],fn,i,fcs,om,'Q','noi',rxlist{i});
  plot_and_save(m,map(i).Uvar,[],fn,i,fcs,om,'U','noi',rxlist{i});
end

% make E and B maps
map_normal=make_ebmap(m,map,'normal');
map_kendrick=make_ebmap(m,map,'kendrick');
if doPure
  % load the purification matrix according to apsopt.purifmatname,
  % the logic in the function avoids reloading duplicate matrices.
  apsopt = aps_load_purifmat(apsopt);
  map_matrix=make_ebmap(m,map,'normal',[],apsopt.purifmat);
end

% gen the 2d aps
[ad,aps2d_normal]=make_aps2d(m,map_normal,'normal',pad,realimag);
[ad,aps2d_kendrick]=make_aps2d(m,map_kendrick,'kendrick',pad,realimag);
if doPure
  [ad,aps2d_matrix]=make_aps2d(m,map_matrix,'normal',pad,realimag);
end
if doHJ
  [adh,aps2d_halfjack]=make_aps2d_halfjack(m,map,nonjackmap,pad,realimag);
end

% store for later
ad.pad=pad;
ad.realimag=realimag;
adh.pad=pad;
adh.realimag=realimag;

% plot the aps2d lin space
for i=1:size(map,1)
  if ~isempty(cjaps_in)
    c1=cjaps_in{1,1};
    c2=cjaps_in{2,1};
    c3=cjaps_in{3,1};
    c4=cjaps_in{4,1};
    c5=cjaps_in{5,1};
    c6=cjaps_in{6,1};
    c7=cjaps_in{7,1};
    c8=cjaps_in{8,1};
    c9=cjaps_in{9,1};
    c10=cjaps_in{10,1};
    c11=cjaps_in{11,1};
    c12=cjaps_in{12,1};
    c13=cjaps_in{13,1};
    c14=cjaps_in{14,1};
    c15=cjaps_in{15,1};
    c16=cjaps_in{16,1};
    c17=cjaps_in{17,1};
    c18=cjaps_in{18,1};
    c19=cjaps_in{19,1};
    c20=cjaps_in{20,1};
    c21=cjaps_in{21,1};
    c22=cjaps_in{22,1};
    c23=cjaps_in{23,1};
    c24=cjaps_in{24,1};
    fcs=true;
  else
    c1=[];
    c2=[];
    c3=[];
    c4=[];
    c5=[];
    c6=[];
    c7=[];
    c8=[];
    c9=[];
    c10=[];
    c11=[];
    c12=[];
    c13=[];
    c14=[];
    c15=[];
    c16=[];    
    c17=[];
    c18=[];
    c19=[];
    c20=[];
    c21=[];
    c22=[];
    c23=[];
    c24=[];
    fcs=false;
  end
  
  ad.lmax = lmax;
  
  cjaps_out{1,i}=plot_and_save(ad,aps2d_normal(i).T,c1,fn,i,fcs,om,'T','speclin',rxlist{i});
  cjaps_out{2,i}=plot_and_save(ad,aps2d_normal(i).Q,c2,fn,i,fcs,om,'Q','speclin',rxlist{i});
  cjaps_out{3,i}=plot_and_save(ad,aps2d_normal(i).U,c3,fn,i,fcs,om,'U','speclin',rxlist{i});
  cjaps_out{4,i}=plot_and_save(ad,aps2d_normal(i).E,c4,fn,i,fcs,om,'E','speclin',rxlist{i});
  cjaps_out{5,i}=plot_and_save(ad,aps2d_normal(i).B,c5,fn,i,fcs,om,'B','speclin',rxlist{i});
  cjaps_out{6,i}=plot_and_save(ad,aps2d_normal(i).TE,c6,fn,i,fcs,om,'TE','speclin',rxlist{i});
  cjaps_out{7,i}=plot_and_save(ad,aps2d_normal(i).TB,c7,fn,i,fcs,om,'TB','speclin',rxlist{i});
  cjaps_out{8,i}=plot_and_save(ad,aps2d_normal(i).EB,c8,fn,i,fcs,om,'EB','speclin',rxlist{i});
  cjaps_out{9,i}=plot_and_save(ad,aps2d_kendrick(i).B,c9,fn,i,fcs,om,'B_kendrick','speclin',rxlist{i});
  cjaps_out{10,i}=plot_and_save(ad,aps2d_kendrick(i).TB,c10,fn,i,fcs,om,'TB_kendrick','speclin',rxlist{i});
  cjaps_out{11,i}=plot_and_save(ad,aps2d_kendrick(i).EB,c11,fn,i,fcs,om,'EB_kendrick','speclin',rxlist{i});
  if doPure
    cjaps_out{12,i}=plot_and_save(ad,aps2d_matrix(i).B,c12,fn,i,fcs,om,'B_matrix','speclin',rxlist{i});
    cjaps_out{13,i}=plot_and_save(ad,aps2d_matrix(i).TB,c13,fn,i,fcs,om,'TB_matrix','speclin',rxlist{i});
    cjaps_out{14,i}=plot_and_save(ad,aps2d_matrix(i).EB,c14,fn,i,fcs,om,'EB_matrix','speclin',rxlist{i});
  end
  if doTQ
    plot_and_save(ad,aps2d_normal(i).TQ,c6,fn,i,fcs,om,'TQ','speclin',rxlist{i});
    plot_and_save(ad,aps2d_normal(i).TU,c6,fn,i,fcs,om,'TU','speclin',rxlist{i});
    if doHJ
      plot_and_save(adh,aps2d_halfjack(i).TQ,c6,fn,i,fcs,om,'TQhj','speclin',rxlist{i});
      plot_and_save(adh,aps2d_halfjack(i).TU,c6,fn,i,fcs,om,'TUhj','speclin',rxlist{i});
    end
  end
  if doHJ
    plot_and_save(adh,aps2d_halfjack(i).TE,c6,fn,i,fcs,om,'TEhj','speclin',rxlist{i});
    plot_and_save(adh,aps2d_halfjack(i).TB,c7,fn,i,fcs,om,'TBhj','speclin',rxlist{i});
    plot_and_save(adh,aps2d_halfjack(i).EB,c8,fn,i,fcs,om,'EBhj','speclin',rxlist{i});
  end
end

if doCross & length(aps2d_normal)>1
  % do the cross spectra: they start in the 2daps
  % after the auto spectra:
  xspectramask = size(map_normal,1)+1:length(aps2d_normal)
  i=size(map_normal,1);
  for j=1:size(map_normal,1)-1
    for c=j+1:size(map_normal,1)
      i=i+1
      % hard code here that we do not look at inter Keck cross spectra:
      if(i>11) continue; end
      plot_and_save(ad,aps2d_normal(i).T,c15,fn,i,fcs,om,'T','speclin',[rxlist{j},'x',rxlist{c}]);
      plot_and_save(ad,aps2d_normal(i).Q,c16,fn,i,fcs,om,'Q','speclin',[rxlist{j},'x',rxlist{c}]);
      plot_and_save(ad,aps2d_normal(i).U,c17,fn,i,fcs,om,'U','speclin',[rxlist{j},'x',rxlist{c}]);
      plot_and_save(ad,aps2d_normal(i).E,c18,fn,i,fcs,om,'E','speclin',[rxlist{j},'x',rxlist{c}]);
      plot_and_save(ad,aps2d_normal(i).B,c19,fn,i,fcs,om,'B','speclin',[rxlist{j},'x',rxlist{c}]);
      plot_and_save(ad,aps2d_normal(i).TE,c6,fn,i,fcs,om,'TE','speclin',[rxlist{j},'x',rxlist{c}]);
      plot_and_save(ad,aps2d_normal(i).TB,c7,fn,i,fcs,om,'TB','speclin',[rxlist{j},'x',rxlist{c}]);
      plot_and_save(ad,aps2d_normal(i).EB,c8,fn,i,fcs,om,'EB','speclin',[rxlist{j},'x',rxlist{c}]);
      plot_and_save(ad,aps2d_kendrick(i).B,c20,fn,i,fcs,om,'B_kendrick','speclin',[rxlist{j},'x',rxlist{c}]);
      plot_and_save(ad,aps2d_kendrick(i).TB,c10,fn,i,fcs,om,'TB_kendrick','speclin',[rxlist{j},'x',rxlist{c}]);
      plot_and_save(ad,aps2d_kendrick(i).EB,c11,fn,i,fcs,om,'EB_kendrick','speclin',[rxlist{j},'x',rxlist{c}]);
      if doPure
        plot_and_save(ad,aps2d_matrix(i).B,c20,fn,i,fcs,om,'B_matrix','speclin',[rxlist{j},'x',rxlist{c}]);
        plot_and_save(ad,aps2d_matrix(i).BT,c10,fn,i,fcs,om,'TB_matrix','speclin',[rxlist{j},'x',rxlist{c}]);
        plot_and_save(ad,aps2d_matrix(i).BE,c11,fn,i,fcs,om,'EB_matrix','speclin',[rxlist{j},'x',rxlist{c}]);
      end
      % do the alternate ones:
      plot_and_save(ad,aps2d_normal(i).T,c15,fn,i,fcs,om,'T','speclin',[rxlist{c},'x',rxlist{j}]);
      plot_and_save(ad,aps2d_normal(i).Q,c16,fn,i,fcs,om,'Q','speclin',[rxlist{c},'x',rxlist{j}]);
      plot_and_save(ad,aps2d_normal(i).U,c17,fn,i,fcs,om,'U','speclin',[rxlist{c},'x',rxlist{j}]);
      plot_and_save(ad,aps2d_normal(i).E,c18,fn,i,fcs,om,'E','speclin',[rxlist{c},'x',rxlist{j}]);
      plot_and_save(ad,aps2d_normal(i).B,c19,fn,i,fcs,om,'B','speclin',[rxlist{c},'x',rxlist{j}]);
      plot_and_save(ad,aps2d_normal(i).ET,c6,fn,i,fcs,om,'TE','speclin',[rxlist{c},'x',rxlist{j}]);
      plot_and_save(ad,aps2d_normal(i).BT,c7,fn,i,fcs,om,'TB','speclin',[rxlist{c},'x',rxlist{j}]);
      plot_and_save(ad,aps2d_normal(i).BE,c8,fn,i,fcs,om,'EB','speclin',[rxlist{c},'x',rxlist{j}]);
      plot_and_save(ad,aps2d_kendrick(i).B,c20,fn,i,fcs,om,'B_kendrick','speclin',[rxlist{c},'x',rxlist{j}]);
      plot_and_save(ad,aps2d_kendrick(i).BT,c10,fn,i,fcs,om,'TB_kendrick','speclin',[rxlist{c},'x',rxlist{j}]);
      plot_and_save(ad,aps2d_kendrick(i).BE,c11,fn,i,fcs,om,'EB_kendrick','speclin',[rxlist{c},'x',rxlist{j}]);
      if doPure
        plot_and_save(ad,aps2d_matrix(i).B,c20,fn,i,fcs,om,'B_matrix','speclin',[rxlist{c},'x',rxlist{j}]);
        plot_and_save(ad,aps2d_matrix(i).BT,c10,fn,i,fcs,om,'TB_matrix','speclin',[rxlist{c},'x',rxlist{j}]);
        plot_and_save(ad,aps2d_matrix(i).BE,c11,fn,i,fcs,om,'EB_matrix','speclin',[rxlist{c},'x',rxlist{j}]);
      end
      if doTQ
        plot_and_save(ad,aps2d_normal(i).TU,c6,fn,i,fcs,om,'TU','speclin',[rxlist{j},'x',rxlist{c}]);
        plot_and_save(ad,aps2d_normal(i).TQ,c7,fn,i,fcs,om,'TQ','speclin',[rxlist{j},'x',rxlist{c}]);
        % do the alternate ones:
        plot_and_save(ad,aps2d_normal(i).UT,c6,fn,i,fcs,om,'TU','speclin',[rxlist{c},'x',rxlist{j}]);
        plot_and_save(ad,aps2d_normal(i).QT,c7,fn,i,fcs,om,'TQ','speclin',[rxlist{c},'x',rxlist{j}]);
      end
    end
  end
end

% plot the aps2d log space
if doSL
  for i=1:size(map_normal,1)
    plot_and_save(ad,aps2d_normal(i).T,[],fn,i,fcs,om,'T','speclog',rxlist{i});
    plot_and_save(ad,aps2d_normal(i).Q,[],fn,i,fcs,om,'Q','speclog',rxlist{i});
    plot_and_save(ad,aps2d_normal(i).U,[],fn,i,fcs,om,'U','speclog',rxlist{i});
    plot_and_save(ad,aps2d_normal(i).E,[],fn,i,fcs,om,'E','speclog',rxlist{i});
    plot_and_save(ad,aps2d_normal(i).B,[],fn,i,fcs,om,'B','speclog',rxlist{i});
    plot_and_save(ad,abs(aps2d_normal(i).TE),[],fn,i,fcs,om,'TE','speclog',rxlist{i});
    plot_and_save(ad,abs(aps2d_normal(i).TB),[],fn,i,fcs,om,'TB','speclog',rxlist{i});
    plot_and_save(ad,abs(aps2d_normal(i).EB),[],fn,i,fcs,om,'EB','speclog',rxlist{i});
    plot_and_save(ad,abs(aps2d_kendrick(i).B),[],fn,i,fcs,om,'B_kendrick','speclog',rxlist{i});
    plot_and_save(ad,abs(aps2d_kendrick(i).TB),[],fn,i,fcs,om,'TB_kendrick','speclog',rxlist{i});
    plot_and_save(ad,abs(aps2d_kendrick(i).EB),[],fn,i,fcs,om,'EB_kendrick','speclog',rxlist{i});
    if doPure
      plot_and_save(ad,abs(aps2d_matrix(i).B),[],fn,i,fcs,om,'B_matrix','speclog',rxlist{i});
      plot_and_save(ad,abs(aps2d_matrix(i).TB),[],fn,i,fcs,om,'TB_matrix','speclog',rxlist{i});
      plot_and_save(ad,abs(aps2d_matrix(i).EB),[],fn,i,fcs,om,'EB_matrix','speclog',rxlist{i});
    end
    if doTQ
      plot_and_save(ad,abs(aps2d_normal(i).TU),c6,fn,i,fcs,om,'TU','speclog',rxlist{i});
      plot_and_save(ad,abs(aps2d_normal(i).TQ),c7,fn,i,fcs,om,'TQ','speclog',rxlist{i});
      if doHJ
        plot_and_save(ad,abs(aps2d_halfjack(i).TU),c6,fn,i,fcs,om,'TUhj','speclog',rxlist{i});
        plot_and_save(ad,abs(aps2d_halfjack(i).TQ),c7,fn,i,fcs,om,'TQhj','speclog',rxlist{i});
      end
    end
    if doHJ
      plot_and_save(ad,abs(aps2d_halfjack(i).TE),c6,fn,i,fcs,om,'TEhj','speclog',rxlist{i});
      plot_and_save(ad,abs(aps2d_halfjack(i).TB),c7,fn,i,fcs,om,'TBhj','speclog',rxlist{i});
      plot_and_save(ad,abs(aps2d_halfjack(i).EB),c8,fn,i,fcs,om,'EBhj','speclog',rxlist{i});
    end
  end
end

% plot E and B maps
for i=1:size(map_normal,1)
  if ~isempty(cj_in)
    c1=cj_in{4,1};
    c2=cj_in{5,1};
    c3=cj_in{2,1};
    c4=cj_in{3,1};
  else
    c1=[];
    c2=[];
  end

  cj_out{4,i}=plot_and_save(m,map_normal(i).E,c2,fn,i,fcs,om,'E','apmap',rxlist{i});
  cj_out{5,i}=plot_and_save(m,map_normal(i).B,c1,fn,i,fcs,om,'B','apmap',rxlist{i});

  plot_and_save(m,map_kendrick(i).E,c2,fn,i,fcs,om,'E_kendrick','apmap',rxlist{i});
  plot_and_save(m,map_kendrick(i).B,c1,fn,i,fcs,om,'B_kendrick','apmap',rxlist{i});

  if doPure
    plot_and_save(m,map_matrix(i).E,c2,fn,i,fcs,om,'E_matrix','apmap',rxlist{i});
    plot_and_save(m,map_matrix(i).B,c1,fn,i,fcs,om,'B_matrix','apmap',rxlist{i});
  end
    
  % plot the pol mask under E/B slots
  plot_and_save(m,1./map_normal(i).Pw,[],fn,i,fcs,om,'E','noi',rxlist{i});
  plot_and_save(m,1./map_normal(i).Pw,[],fn,i,fcs,om,'B','noi',rxlist{i});
  
end

% plot the deprojected templates
if dotemplates
  for i=1:size(map,1)
    if isfield(map(i),'Qd')
      for k=1:numel(map(i).Qd)
        [sa.s,sa.a]=calc_map_deptharea(m,map(i).Qd{k},map(i).Pw);
        plot_and_save(m,map(i).Qd{k},[],fn,i,fcs,om,['Qd',num2str(k)],'map',rxlist{i},sa);
        [sa.s,sa.a]=calc_map_deptharea(m,map(i).Ud{k},map(i).Pw);
        plot_and_save(m,map(i).Ud{k},[],fn,i,fcs,om,['Ud',num2str(k)],'map',rxlist{i},sa);
      end
    end
  end
end

% plot the aps2d log space for cross spectra
%plot_and_save(ad,abs(aps2d_normal(3).T),[],fn,3,'T','speclog',rxlist{i});
%plot_and_save(ad,abs(aps2d_normal(3).Q),[],fn,3,'Q','speclog',rxlist{i});
%plot_and_save(ad,abs(aps2d_normal(3).U),[],fn,3,'U','speclog',rxlist{i});
%plot_and_save(ad,abs(aps2d_normal(3).E),[],fn,3,'E','speclog',rxlist{i});
%plot_and_save(ad,abs(aps2d_normal(3).B),[],fn,3,'B','speclog',rxlist{i});
%plot_and_save(ad,abs(aps2d_normal(3).TE),[],fn,3,'TE','speclog',rxlist{i});
%plot_and_save(ad,abs(aps2d_normal(3).TB),[],fn,3,'TB','speclog',rxlist{i});
%plot_and_save(ad,abs(aps2d_normal(3).EB),[],fn,3,'EB','speclog',rxlist{i});

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function c_out=plot_and_save(m,map,c,fn,f,fcs,om,tqu,mapspec,rx,sa)

clf

switch length(fn)
  case 1
   tit = fn{1};
   fn = fn{1};
 case 2
  tit = [fn{1},' x ',fn{2}];
  [d1,f1,e1]=fileparts(fn{1});
  [d2,f2,e2]=fileparts(fn{2});  
  fn=sprintf('%sx%s/%s_%s.mat',d1,d2,f1,f2);
 case 3
  tit = [fn{1},' x ',fn{2},' x ',fn{3}];
  [d1,f1,e1]=fileparts(fn{1});
  [d2,f2,e2]=fileparts(fn{2});  
  [d3,f3,e3]=fileparts(fn{3});  
  fn=sprintf('%sx%sx%s/%s_%s_%s.mat',d1,d2,d3,f1,f2,f3);
end

if ~exist('rx','var')
  rx=0;
end

fn=rvec(fn);

[fdir,fn0]=fileparts(fn);

if any(strfind(mapspec,'spec')) && m.lmax~=500
  lmax=num2str(m.lmax);
else
  lmax='';
end

if rx
  fname=sprintf('%s_%s_%s%s',fn0,tqu,mapspec,lmax);
  fname = [fname,'_',rx,'.png'];
else
  fname=sprintf('%s_%s_%s%s.png',fn0,tqu,mapspec,lmax);
end
fname=fullfile('reduc_plotcomap_pager',fdir,fname);
fname=strrep(fname,'/maps','');
if fcs
  fname=strrep(fname,'.png','_fcs.png');
end


fname

if exist(fname,'file') & om
  c_out=[];
  return
end

if issparse(map)
  map=full(map);
end

tit = strrep(tit,'/maps/','');
tit = strrep(tit,'./','');

% if colorstretch not specified base on noise level
if(isempty(c))
  center=size(map)/2;
  lox=center(1)-30;
  hix=center(1)+30;
  loy=center(2)-20;
  hiy=center(2)+20;
  c1=min(min(map(lox:hix,loy:hiy)));
  c2=max(max(map(lox:hix,loy:hiy)));
  c1=-(abs(c1)+abs(c2))/2;c2=-c1;
else
  c1=c(1);
  c2=c(2);
end
if ~isfinite(c1); c1=0; end
if ~isfinite(c2); c2=1; end

switch mapspec
 case 'map'
  colormap jet;
  %if any(strfind(tqu,'T'))
  %  colormap hot
  %elseif any(strfind(tqu,'Q')) || any(strfind(tqu,'U'))
  %  colormap gray
  %end
  plot_map(m,map,'iau')
  caxis([c1,c2]); colorbar
  c_out=caxis;
  if ~isfield(sa,'sc')
    title({sprintf('%s - map',strrep(tit,'_','\_'));sprintf('weighted rms %.3f uK-deg over area %.1f sq deg',sa.s,sa.a)});
  else
    title({sprintf('%s - map',strrep(tit,'_','\_'));sprintf('weighted rms %.3f uK-deg over area %.1f sq deg, QU combined: %.3f uK-deg, %.1f sq deg',sa.s,sa.a,sa.sc,sa.ac)});
  end

 case 'noi'
  colormap hot;
    plot_map(m,1./map,'iau')
    center=size(map)/2;
    lox=center(1)-30;
    hix=center(1)+30;
    loy=center(2)-20;
    hiy=center(2)+20;
    c1=min(min(1./map(lox:hix,loy:hiy)));
    c2=max(max(1./map(lox:hix,loy:hiy)));
    if ~isfinite(c2); c2=1; end
    caxis([0,c2]); colorbar
    title({sprintf('%s - 1/varmap',strrep(tit,'_','\_'));sprintf('min noise %.2f uK',min(sqrt(map(:))))});
 
 case 'apmap'
  colormap jet;
  %if any(strfind(tqu,'T'))
  %  colormap hot
  %elseif any(strfind(tqu,'Q')) || any(strfind(tqu,'U'))
  %  colormap gray
  %elseif any(strfind(tqu,'E')) || any(strfind(tqu,'B'))
  %  colormap(colormap_lint)
  %end
    plot_map(m,map,'iau')
    caxis([c1,c2]); colorbar
    c_out=caxis;
    if any(strfind(tqu,'E')) || any(strfind(tqu,'B'))
      title({strrep(tit,'_','\_');'apodized map (filtered)'});
    else
      title({strrep(tit,'_','\_');'apodized map'});
    end
  
  
    
 case 'speclin'
   colormap gray;
    xtic=m.u_val{1}*2*pi;
    ytic=m.u_val{2}*2*pi;    
    imagesc(xtic,ytic,map); axis image
    xlim([-m.lmax,m.lmax]); ylim([-m.lmax,m.lmax]);
    bullseye;
    center=[size(map,1)/2, size(map,2)/2];
    lox=center(1)-50;
    hix=center(1)+50;
    loy=center(2)-50;
    hiy=center(2)+50;
    if isempty(c)
      c1=min(min(map(lox:hix,loy:hiy)));
      c2=max(max(map(lox:hix,loy:hiy)));
    end
    if ~isfinite(c1); c1=0; end
    if ~isfinite(c2); c2=1; end
    caxis([c1,c2]);
    colorbar
    c_out=caxis;
    title({strrep(tit,'_','\_');'square of ft (linear uK^2 - no l^2 scaling)'});
    
    if(m.realimag)
      text(0.01,0.97,'Imag','Units','normalized','HorizontalAlignment','left','Fontsize',14,'color','r');
      text(0.99,0.97,'Real','Units','normalized','HorizontalAlignment','right','Fontsize',14,'color','r');
    end
    

 case 'speclog'
  colormap gray;
    xtic=m.u_val{1}*2*pi;
    ytic=m.u_val{2}*2*pi;    
    imagesc(xtic,ytic,log10(map)); axis image
    xlim([-500,500]); ylim([-500,500]);
    bullseye;
    center=[size(map,1)/2, size(map,2)/2];
    lox=center(1)-30;
    hix=center(1)+30;
    loy=center(2)-30;
    hiy=center(2)+30;
    if isempty(c)
      c1=min(min(log10(map(lox:hix,loy:hiy))));
      c2=max(max(log10(map(lox:hix,loy:hiy))));
      if isfinite(c1) && isfinite(c2)
        caxis([c1+2,c2]);
      end
    end
    colorbar
    c_out=caxis;
    title({strrep(tit,'_','\_'); 'square of ft (log10 uK^2 - no l^2 scaling)'});
    
    if(m.realimag)
      text(0.01,0.97,'Imag','Units','normalized','HorizontalAlignment','left','Fontsize',14,'color','r');
      text(0.99,0.97,'Real','Units','normalized','HorizontalAlignment','right','Fontsize',14,'color','r');
    end
    
 case 'specm'
  colormap gray;
    xtic=m.u_val{1}*2*pi;
    ytic=m.u_val{2}*2*pi;    
    imagesc(xtic,ytic,log10(map)); axis image
    xlim([-500,500]); ylim([-500,500]);
    bullseye;
    caxis([low,high]);
    colorbar
    title({strrep(tit,'_','\_');'spec with l^2 scaling (log10 uK^2)'});
    
    if(m.realimag)
      text(0.01,0.97,'Imag','Units','normalized','HorizontalAlignment','left','Fontsize',14,'color','r');
      text(0.99,0.97,'Real','Units','normalized','HorizontalAlignment','right','Fontsize',14,'color','r');
    end
    
end

% shave off .mat
fdir=fileparts(fname);
if ~exist(fdir,'dir')
  mkdir(fdir);
end
mkpng(fname);

return

%%%%%%%%%%%%%%%%%%%
function bullseye()
xl=xlim;
hold on
%r=1;n=35; be=20:n:581;  be=[linspace(0,be(1),r+1),be(2:end)];
[be,n]=get_bins('bicep_norm');
[x,y]=circle(0,0,be,50,1);
plot(x,y,'r','linewidth',0.2);
%x=sind(10)*700;
%line([-x,x],[-500,500]); line([x,-x],[-500,500]);
hold off
return

%%%%%%%%%%%%%%%%%%%
function map = replace_to_sub(map,doSub)
if ischar(doSub)
  for ii = 1:size(map,1);
    for jj = 1:size(map,2);
      switch doSub
        case 'psub'
          map(ii,jj).T=map(ii,jj).Tpsub;
          map(ii,jj).Q=map(ii,jj).Qpsub;
          map(ii,jj).U=map(ii,jj).Upsub;
        case 'gsub'
          map(ii,jj).T=map(ii,jj).Tgsub;
          map(ii,jj).Q=map(ii,jj).Qgsub;
          map(ii,jj).U=map(ii,jj).Ugsub;
        case 'dsub'
          map(ii,jj).Q=map(ii,jj).Qdsub;
          map(ii,jj).U=map(ii,jj).Udsub;
      end
    end
  end
end
return
