function reduc_simtraj(expt)
% reduc_simtraj(expt)
%
% Generate a trajectory file to allow sim of bicep/SPUD/SPTpol
%
% Output:
%   scratch/(basedate+dk_index)_tod.mat
%    which contains simulated structures 'd' and 'fs'.
%
% e.g.) for 'bicep', the output is scratch/10010[1-4]_tod.mat
%       where [1-4] is for each dk angles [-45, 0, 135, 180].

switch expt
  case 'bicep'
    trajopt.scanthrow=65;
    trajopt.scanrate=1;
    trajopt.timestep=0.05; % 10ms for QUaD
    trajopt.nreps=23;
    trajopt.elsteptint=3600;
    trajopt.elsteps=55:0.25:59.75;
    trajopt.dks=[-45, 0, 135, 180];
    trajopt.basedate=100100;
  case 'spud'
    trajopt.scanthrow=50;
    trajopt.scanrate=0.09;
    trajopt.timestep=0.5; % 10ms for QUaD
    trajopt.nreps=3;
    trajopt.elsteptint=3600;
    %trajopt.elsteps=55:0.25:59.75;
    trajopt.elsteps=-2.5:0.25:2.25;
    trajopt.dks=[-45, 0, 135, 180]+2;
    trajopt.basedate=200100;    
    %trajopt.basedate=200104;
end

azoff=-trajopt.scanthrow/2:trajopt.scanrate:trajopt.scanthrow/2;
azoff=[NaN*ones(1,3),azoff,NaN*ones(1,3)];
azoff=[azoff,fliplr(azoff)];
azoff=repmat(azoff,[1,trajopt.nreps]);

% pad az to 1 hour
n=length(azoff);
azoff=[azoff,NaN*ones(1,trajopt.elsteptint-n)];

t=0:length(azoff)-1;
plot(t,azoff);

ra=t*15/3600+azoff-7.5*n/3600;

el=rvec(ones(size(azoff))'*trajopt.elsteps);

azbase=-rvec(ones(size(azoff))'*[0:15:15*(length(trajopt.elsteps)-1)]);

az=repmat(azoff,[1,length(trajopt.elsteps)])+azbase;
azoff=repmat(azoff,[1,length(trajopt.elsteps)]);
ra=repmat(ra,[1,length(trajopt.elsteps)]);

t=0:1:length(az)-1;
plotyy(t,az,t,el)

% fast time
tf=-1+trajopt.timestep:trajopt.timestep:t(end);

d.tracker.actual(:,1)=az;
d.tracker.actual(:,2)=el;

d.t=t';
d.tf=tf';
d.az=interp1(t,az,tf)';
d.azoff=interp1(t,azoff,tf)';
d.el=interp1(t,el,tf)';
d.ra=interp1(t,ra,tf)';
d.dec=-d.el;
d.tracker.source=repmat('spud1',[length(d.t),1]);
d.weather.air_temp=NaN*ones(size(d.t));

fs=find_blk(~isnan(az),1/trajopt.timestep);

% because of interp above last value of d.azoff is NaN which causes
% problem in filter_scans
fs.ef=fs.ef-1;

plotyy(d.tf,d.ra,d.tf,d.dec)

for i=1:length(trajopt.dks)
  d.tracker.horiz_off(:,3)=ones(size(d.t))*trajopt.dks(i);
  save(sprintf('scratch/%06d_tod',trajopt.basedate+i),'d','fs');
end

return
