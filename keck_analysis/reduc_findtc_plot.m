function reduc_findtc_plot(file)
% function reduc_findtc_plot(file)
%
% plot results from findtc
%
% e.g.
% reduc_findtc_plot('timeconst/050714_deconvdata_dual')
% reduc_findtc_plot('timeconst/060909_deconvdata_dual')

load(file);

% set bad channels to NaN
for i=1:size(tc1,1)
  for j=1:size(tc1,2)
    if(tc1(i,j,1)==0)
      tc1(i,j,:)=NaN;
      tc2(i,j,:)=NaN;
      tcs(i,j,:)=NaN;
    end
  end
end

% if tc2 fit has failed to reduce chi2 from tc1 value set fraction to
% zero and 2nd tc to NaN
for i=1:size(tc1,1)
  for j=1:size(tc1,2)
    if(tc2(i,j,4)>tc1(i,j,2))
      tc2(i,j,1)=tc1(i,j,1);
      tc2(i,j,2)=NaN;
      tc2(i,j,3)=0;
    end
  end
end

figure(1)
subplot(2,2,1); plot(tc1(:,:,1)','.-'); grid
subplot(2,2,2); plot(tc1(:,:,2)','.-'); grid
subplot(2,2,3); plot(tc1(:,:,3)','.-'); grid

figure(2)
subplot(2,2,1); plot(tc2(:,:,1)','.-'); grid
subplot(2,2,2); plot(tc2(:,:,2)','.-'); grid
subplot(2,2,3); plot(tc2(:,:,3)','.-'); grid
subplot(2,2,4); plot(tc2(:,:,4)','.-'); grid

% in the cases where the deck angles disagree as to whether a 2nd tc
% is needed we might hope that the weight of the 2nd tc is low...
[[1:62]' tc2(1,:,2)' tc2(1,:,3)' tc2(2,:,2)' tc2(2,:,3)']

% in cases where the two deck angles do not agree that a 2nd tc is
% needed we revert to single tc values
for i=1:size(tc1,2)
  if(sum(isnan(tc2(:,i,2)))==1)
    tc2(:,i,1)=tc1(1:2,i,1);
    tc2(:,i,2)=NaN;
    tc2(:,i,3)=0;
  end
end

[[1:62]' tc2(1,:,1)' tc2(1,:,2)' tc2(2,:,3)']

% write parameter file
fh=fopen(sprintf('%s.txt',file),'w');
fprintf(fh,'DATE,%s\n',tag(1:6));
fprintf(fh,'BODY\n');
fprintf(fh,'        chan,     tc1,          tc2,        weight\n');
fprintf(fh,'Units,  (null),   seconds,      seconds,    (null)\n');
fprintf(fh,'Format, string,   float,        float,      float\n');
for i=ind.l
  fprintf(fh,'       %8s,  %10.9f, %10.6f, %10.4f\n',p.channel_name{i},tc2(1,i,1:3));
end
fclose(fh);

return

p=ParameterRead('timeconst/041229_labtc_fit.txt')
i=p.tau2a>p.tau2b;
p.ratio(i)=1./p.ratio(i);
t=p.tau2b(i);
p.tau2b(i)=p.tau2a(i);
p.tau2a(i)=t;
p.w=1./(1+p.ratio);

%i=p.ratio>100;

% pulls out the ones where second tc really matters
i=p.ratio>p.tau2b*15;
i=p.ratio==Inf

p.ratio(i)=NaN;
p.tau2b(i)=NaN;
p.w(i)=NaN;

for i=1:length(p.tau2a)
  disp(sprintf('%2d %s  %5.3f  %5.3f  %5.3f',i,p.name{i},p.tau2a(i),p.tau2b(i),p.w(i)));
end

plot(p.tau2b,p.ratio,'+')
x=0:0.01:1.4;
hold on
plot(x,x*15,'r')
hold off
