% reduc_make_tag_browser(tag_csv_file,stats_csv_file,file_dir,file_url,farmstuff,tags)
%
% Generate the HTML pages for the tag browser.  All arguments
% may be omitted, and reasonable defaults will be used.
function reduc_make_tag_browser(tag_csv_file,stats_csv_file,file_dir,file_url,farmstuff,tags)
if nargin<1 || isempty(tag_csv_file)
  tag_csv_file=fullfile('aux_data','tag_list.csv');
end
if nargin<2 || isempty(stats_csv_file)
  stats_csv_file=fullfile('aux_data','tag_stats.csv');
end
if nargin<3 || isempty(file_dir)
  file_dir=fullfile('reducplots');
end
if ~exist(file_dir,'dir')
  mkdir(file_dir);
  fileattrib(file_dir,'+w','g');
end
if nargin<4 || isempty(file_url)
  file_url=fullfile('.');
end
if nargin<5 || isempty(farmstuff)
  farmstuff=0;
end
if nargin<6 || isempty(tags)
  tags=[];
end

if farmstuff
  QUEUE = 'serial_requeue';
  MEM = 1500; % 500MB was not enough to get Matlab started since changes to farmit.
  LICENSE = 'bicepfs1:25'; % 1500 slots -> 1500/25 = 60 jobs
  MAXTIME = 60;
end

[pt,kt]=ParameterRead(tag_csv_file);
[ps,ks]=ParameterRead(stats_csv_file);

% Are we BICEP2, BICEP3 or Keck?
experiment=get_experiment_name;

% Index and help pages are in the browser subdir:
index_dir = fullfile(file_dir,'browser');
if ~exist(index_dir,'dir')
  mkdir(index_dir);
  fileattrib(file_dir,'+w','g');
end

% Reducplots help files
get_help_pages(index_dir);

% Top-level index page
write_main_index(index_dir,file_url,pt.tag{end});

% Better left pane with hierarchical list
write_spiffy_tag_index(pt,ps,fullfile(index_dir,'tag_index.html'),false);
write_spiffy_tag_index(pt,ps,fullfile(index_dir,'full_index.html'),true);

% Individual HTML files for each tag
% do all tags?
if isempty(tags)
  tags=pt.tag;
% do all tags from a particular date?
elseif ischar(tags)
  tags=pt.tag(datenum(pt.tstart,'dd-mmm-yyyy:HH:MM:SS')>=utc2datenum(tags));
end
rel_url=file_url;
if rel_url(1)=='.'
  rel_url=[rel_url '/../../'];
end
if farmstuff
  %farm out the tags in chunks of 400
  n=400;
  for ii=1:n:length(tags)
    tmp_tags=tags(ii:min(ii+n-1,length(tags)));
    [farmfile,jobname] = farmfilejobname('BICEP', ...
        'reduc_make_tag_browser', tmp_tags{1});
    farmit(farmfile,...
        ['tag_html_driver(''' tag_csv_file ''',''' stats_csv_file ...
            ''', tmp_tags,''' file_dir ''',''' rel_url ''',''' ...
            experiment ''');'], ...
        'jobname',jobname, 'queue',QUEUE, 'mem',MEM, 'maxtime',MAXTIME, ...
        'license',LICENSE, 'account','bicepdata_group', ...
        'var',{'tmp_tags'},'overwrite',true);
  end

else
  %if not farmed
  for ii=length(tags):-1:1
    write_tag_html(pt,ps,ks,tags{ii},file_dir,rel_url,experiment)
  end
end

return



function write_main_index(file_dir,file_url,default_tag);
  % Top-level index page
  h=fopen(fullfile(file_dir,'index.html'),'wt');
  fprintf(h,'<html>\n\n');
  fprintf(h,'<head><title>Scanset browser</title></head>\n\n');
  write_main_index_javascript(h,file_url);
  fprintf(h,'<frameset noresize="noresize" cols="260,*">\n\n');
  fprintf(h,'<frame src="tag_index.html#left_top">\n');
  fprintf(h,'<frame src="../%s/browser/%s.html" name="tagpage">\n\n',default_tag(1:6),default_tag);
  fprintf(h,'</frameset>\n\n</html>\n\n');
  fclose(h);
  setpermissions(fullfile(file_dir,'index.html'));

  return


function write_main_index_javascript(h,file_url)

fprintf(h,'<SCRIPT LANGUAGE="JavaScript">\n');
fprintf(h,'<!--\n');
% Information to select the particular plot of interest:
% tag, plot type, rx
fprintf(h,'scanset_tag="";\n');
fprintf(h,'reduc_plot_type="scans";\n');
fprintf(h,'reduc_plot_rx="";\n');
fprintf(h,'reduc_plot_cut="";\n');
% Parse this out of URL if provided, to allow direct links to
% desired plot.
fprintf(h,'query_string=window.location.search.substring(1);\n');
fprintf(h,'query_words=query_string.split("&");\n');
fprintf(h,'if(query_words.length>=1){\n');
fprintf(h,'  scanset_tag=query_words[0];\n');
fprintf(h,'}\n');
fprintf(h,'if(query_words.length>=2){\n');
fprintf(h,'  if(query_words[1].length>0){\n');
fprintf(h,'    reduc_plot_type=query_words[1];\n');
fprintf(h,'  }\n');
fprintf(h,'}\n');
fprintf(h,'if(query_words.length>=3){\n');
fprintf(h,'  if(query_words[2].length>0){\n');
fprintf(h,'    reduc_plot_rx=query_words[2];\n');
fprintf(h,'  }\n');
fprintf(h,'}\n');
fprintf(h,'if(query_words.length>=4){\n');
fprintf(h,'  if(query_words[3].length>0){\n');
fprintf(h,'    reduc_plot_cut=query_words[3];\n');
fprintf(h,'  }\n');
fprintf(h,'}\n');
fprintf(h,'reduc_plot_tagdate='''';\n');
fprintf(h,'reduc_plot_phase='''';\n');
fprintf(h,'reduc_plot_scanset='''';\n');
fprintf(h,'reduc_plot_dk='''';\n');
fprintf(h,'reduc_plot_url='''';\n');
rel_url='../';
fprintf(h,'reduc_plot_prefix=''%s'';\n',rel_url);
% For collapsible boxes: "" means do display,
% "none" means don't display.  Tag stats and info
% are shown by default, plot help is hidden by default.
fprintf(h,'taginfo_disp="";\n');
fprintf(h,'tagstats_disp="";\n');
fprintf(h,'plothelp_disp="none";\n');

% Construct the file name for the current reducplot view.
% This is getting complicated.  Deal with it in parts.
fprintf(h,'function plupdate(){\n');
fprintf(h,' url_base=reduc_plot_prefix+"/";\n');

% Put together tag name at appropriate granularity.
fprintf(h,' switch (reduc_plot_type)\n');
fprintf(h,' {\n');
% Per-scanset plots.
fprintf(h,'  case "scans":\n');
fprintf(h,'  case "stddev":\n');
fprintf(h,'  case "elnod":\n');
fprintf(h,'  case "PSD":\n');
fprintf(h,'  case "NET":\n');
fprintf(h,'  case "cov":\n');
fprintf(h,'    tag_base=reduc_plot_tagdate\n');
fprintf(h,'      +reduc_plot_phase+reduc_plot_scanset\n');
fprintf(h,'      +"_"+reduc_plot_dk;\n');
fprintf(h,'    break;\n');
% Per-schedule plots.
fprintf(h,'  case "cal":\n');
fprintf(h,'  case "snow":\n');
fprintf(h,'    tag_base=reduc_plot_tagdate;\n');
fprintf(h,'    break;\n');
% Per-phase plots.
fprintf(h,'  case "light":\n');
fprintf(h,'  case "dark_sd":\n');
fprintf(h,'    tag_base=reduc_plot_tagdate\n');
fprintf(h,'      +reduc_plot_phase+"_"+reduc_plot_dk;\n');
fprintf(h,'    break;\n');
fprintf(h,' }\n');

% Put together file name.
fprintf(h,' switch (reduc_plot_type)\n');
fprintf(h,' {\n');
fprintf(h,'   case "scans":\n');
fprintf(h,'     reduc_plot_url=url_base+tag_base+"_001"+reduc_plot_rx+reduc_plot_cut+".png";\n');
fprintf(h,'     break;\n');
fprintf(h,'   case "stddev":\n');
fprintf(h,'   case "elnod":\n');
fprintf(h,'   case "PSD":\n');
fprintf(h,'   case "NET":\n');
fprintf(h,'   case "cov":\n');
fprintf(h,'     reduc_plot_url=url_base+tag_base+"_001"+reduc_plot_rx+"_"+reduc_plot_type+reduc_plot_cut+".png";\n');
fprintf(h,'     break;\n');
fprintf(h,'   case "cal":\n');
fprintf(h,'   case "snow":\n');
fprintf(h,'     reduc_plot_url=url_base+reduc_plot_type+"_"+tag_base+reduc_plot_rx+".png";\n');
fprintf(h,'     break;\n');
fprintf(h,'   case "light":\n');
fprintf(h,'     reduc_plot_url=url_base+"perphase_"+tag_base+"_001"+reduc_plot_rx+".png";\n');
fprintf(h,'     break;\n');
fprintf(h,'   case "dark_sd":\n');
fprintf(h,'     reduc_plot_url=url_base+"perphase_"+tag_base+"_002"+reduc_plot_rx+".png";\n');
fprintf(h,'     break;\n');
fprintf(h,' }\n');
fprintf(h,' tagpage.document["reduc"].src=reduc_plot_url;\n');
fprintf(h,' if(plothelp_disp!="none"){\n');
fprintf(h,'   tagpage.document.getElementById(''plothelp_obj'').src="../../browser/help_"+reduc_plot_type+".html";\n');
fprintf(h,' }\n');
fprintf(h,'}\n');

fprintf(h,'function set_reduc_plot_type(plot_type){\n');
fprintf(h,'  reduc_plot_type=plot_type;\n');
fprintf(h,'  plupdate();\n');
fprintf(h,'}\n');

fprintf(h,'function set_reduc_plot_rx(rx_num){\n');
fprintf(h,'  reduc_plot_rx=rx_num;\n');
fprintf(h,'  plupdate();\n');
fprintf(h,'}\n');

fprintf(h,'function set_reduc_plot_cut(cut){\n');
fprintf(h,'  reduc_plot_cut=cut;\n');
fprintf(h,'  plupdate();\n');
fprintf(h,'}\n');

fprintf(h,'function set_reduc_plot_tag(tagdate,phase,scanset,dk){\n');
fprintf(h,'  reduc_plot_tagdate=tagdate;\n');
fprintf(h,'  reduc_plot_phase=phase;\n');
fprintf(h,'  reduc_plot_scanset=scanset;\n');
fprintf(h,'  reduc_plot_dk=dk;\n');
fprintf(h,'  plupdate();\n');
fprintf(h,'}\n');

fprintf(h,'function toggle_taginfo(){\n');
fprintf(h,'  if(taginfo_disp==""){\n');
fprintf(h,'    taginfo_disp="none";\n');
fprintf(h,'  } else {\n');
fprintf(h,'    taginfo_disp="";\n');
fprintf(h,'  }\n');
fprintf(h,'}\n');

fprintf(h,'function toggle_tagstats(){\n');
fprintf(h,'  if(tagstats_disp==""){\n');
fprintf(h,'    tagstats_disp="none";\n');
fprintf(h,'  } else {\n');
fprintf(h,'    tagstats_disp="";\n');
fprintf(h,'  }\n');
fprintf(h,'}\n');

fprintf(h,'function toggle_plothelp(){\n');
fprintf(h,'  if(plothelp_disp==""){\n');
fprintf(h,'    plothelp_disp="none";\n');
fprintf(h,'  } else {\n');
fprintf(h,'    plothelp_disp="";\n');
fprintf(h,'  }\n');
fprintf(h,'}\n');

fprintf(h,'//-->\n</SCRIPT>\n\n');

return


function write_spiffy_tag_index(pt,ps,html_file,do_show_all)

% Identify cryo phases
is_cryo=zeros(size(pt.tag));
for i=1:numel(pt.tag)
  is_cryo(i)=any(strfind(lower(pt.sch{i}),'cryo'));
end

% Remove cryo phases (often "A") and x scansets (per-phase cals) if requested
if ~do_show_all
  for i=1:numel(pt.tag)
    rmind(i)=any(strfind(pt.tag{i},'x')) | is_cryo(i);
  end
  pt=structcut(pt,~rmind);
  is_cryo=is_cryo(~rmind);
end

h=fopen(html_file,'wt');
fprintf(h,'<html>\n\n');
fprintf(h,'<body bgcolor="#d0d0d0">\n\n');
fprintf(h,'<SCRIPT LANGUAGE="JavaScript">\n');
fprintf(h,'<!--\n');
fprintf(h,'if(parent.scanset_tag!=""){\n');
fprintf(h,'  on_month=parent.scanset_tag;\n');
fprintf(h,'  on_month=on_month.substring(0,6);\n');
fprintf(h,'  parent.tagpage.location="../"+on_month+"/browser/"+parent.scanset_tag+".html";\n');
fprintf(h,'}\n//-->\n</SCRIPT>\n\n');
tagz=pt.tag;
% Try to build a 'tag base' that's the same for all scansets taken during
% the same schedule.  This is more complicated than it sounds (and more
% complicated than it should be)!  Construct out of YYMMDD_dk###.  This
% could still fail, since we can have several false starts on the same day.
% However, in that case, our whole tag structure fails!  So good enough.
for ii=1:length(pt.tag)
  tmpbase=tagz{ii}(1:8);
  tmpdk=pt.dk{ii};
  % For cryo service phase, spoof the start date and dk angle
  % to go along with the next observing schedule
  if is_cryo(ii)
    if (ii+1)<=length(pt.ph) && ~is_cryo(ii+1)
      % check that delta t is not too large
      delta_t=datenum(pt.tstart{ii+1},'dd-mmm-yyyy:HH:MM:SS') ...
             -datenum(pt.tend{ii},'dd-mmm-yyyy:HH:MM:SS');
      if (delta_t>0) && (delta_t<6/24)
        if strcmp(pt.ph{ii+1},'X')
          tmpdk='';
        else
          tmpdk=pt.dk{ii+1};
        end
        tmpbase=tagz{ii+1}(1:8);
      end
    end
  elseif strcmp(pt.ph{ii},'X')
    tmpdk='';
  end
  base{ii}=[tmpbase '_dk' tmpdk];
  start_time{ii}=pt.tstart{ii}; % ,'yy-mmm-dd:HH:MM:SS');
end

% Preserve chronological order.  In case several schedules started
% on the same day, alpha order is not necessarily correct.
[unique_base,ii]=unique(base);
try
start_time=[datenum({start_time{ii}},'dd-mmm-yyyy:HH:MM:SS')];
catch
%keyboard
end
% start_time=[start_time(ii)];
[start_time,ii]=sort(start_time);
unique_base={unique_base{ii}};
% Top index with links to each year+month
unique_month=unique_base;
unique_year=unique_base;
for ii=1:length(unique_base)
  unique_month{ii}=unique_base{ii}(1:6);
  unique_year{ii}=unique_year{ii}(1:4);
end
unique_month=unique(unique_month);
unique_year=unique(unique_year);
fprintf(h,'<a name="left_top">\n');
fprintf(h,'Go to:\n\n');
fprintf(h, '<pre>\n');
fprintf(h,'<b><font color="green">Year:</font> <font color="blue">month</font></b>\n\n');
for ii=length(unique_year):-1:1
  fprintf(h,'<font color="green">%s:</font>',unique_year{ii});
  line_length=0;
  for jj=1:length(unique_month)
    if strncmp(unique_year{ii},unique_month{jj},4)
      if (line_length>=6)
        fprintf(h,'\n     ');
        line_length=0;
      end
      fprintf(h,' <a href="#%s">%s</a>',unique_month{jj},unique_month{jj}(5:6));
      line_length=line_length+1;
    end
  end
  fprintf(h,'\n');
end
fprintf(h,'\n<hr>\n');
fprintf(h,'<font color="green">');
fprintf(h,'<b>NATO phonetic alphabet</b>\n');
fprintf(h,' A:<font color="blue">Alfa</font>\n');
fprintf(h,' B:<font color="blue">Bravo</font>\n');
fprintf(h,' C:<font color="blue">Charlie</font>\n');
fprintf(h,' D:<font color="blue">Delta</font>\n');
fprintf(h,' E:<font color="blue">Echo</font>\n');
fprintf(h,' F:<font color="blue">Foxtrot</font>\n');
fprintf(h,'</font>');
fprintf(h,'\n<hr>\n');
if do_show_all
  fprintf(h,'<a href="tag_index.html">Hide cryo, phase cals</a>\n\n');
else
  fprintf(h,'<a href="full_index.html">Show cryo, phase cals</a>\n\n');
end
fprintf(h,'\n<hr><hr>\n');
fprintf(h,'<b><font color="green">Schedule start, dk=###</font></b>\n');
fprintf(h,'<b><font color="green">Ph:</font> <font color="blue">scansets</font></b>\n');
last_month='';
for ii=length(unique_base):-1:1
  on_base=unique_base{ii};
  on_month=on_base(1:6);
  if ~strcmp(on_month,last_month)
    last_month=on_month;
    fprintf(h,'\n\n<hr><a name="%s" />',on_month);
    fprintf(h,'<font color="green"><b>%s-%s</b></font>',on_month(1:4),on_month(5:6));
    fprintf(h,' <a href="#left_top"><font color="green"><b>(top)</b></font></a>\n');
  end
  cc=strcmp(base,on_base);
  base_tagz={tagz{cc}};
  base_idx=find(cc);
  for jj=1:length(base_idx)
    if strcmp(pt.ph{base_idx(jj)},'X')
      base_dk='';
      break
    end
    base_dk=pt.dk{base_idx(jj)};
    if ~isempty(base_dk) && ~strcmp(base_dk,'0')
      break
    end
  end
  fprintf(h,'\n\n<font color="green">%s', on_base(1:8));
  if ~isempty(base_dk)
    fprintf(h,', dk=%s', num2str(str2num(base_dk),'%3d'));
  end
  fprintf(h,'</font>');
  last_phase='';
  for jj=1:length(base_tagz)
    idx=base_idx(jj);
    % Don't include per-phase cals
    % if strcmp(pt.type{idx},'perphasecals')
    %   continue
    % end
    on_phase=pt.ph{idx};
    if ~strcmp(last_phase,on_phase)
      fprintf(h,'\n<font color="green">%s:</font> ', on_phase);
    end
    last_phase=on_phase;
    if is_cryo(idx)
      fprintf(h,'<a href="../%s/browser/%s.html" target="tagpage"><font color="blue">Cryo service</font></a> ', ...
        on_month,pt.tag{idx});
    elseif strcmp(on_phase,'X')
      fprintf(h,'<a href="../%s/browser/%s.html" target="tagpage"><font color="red">%s</font></a> ', ...
        on_month,pt.tag{idx},pt.set{idx});
    elseif strcmp(pt.set{idx},'0')
      fprintf(h,'<a href="../%s/browser/%s.html" target="tagpage"><font color="blue">%s</font></a> ', ...
        on_month,pt.tag{idx},'x');
    else
      fprintf(h,'<a href="../%s/browser/%s.html" target="tagpage"><font color="blue">%s</font></a> ', ...
        on_month,pt.tag{idx},pt.set{idx});
    end
  end
end
fprintf(h,'</pre>\n\n');
fprintf(h,'</body>\n\n');
fprintf(h,'</html>\n\n');
fclose(h);
setpermissions(html_file);

return



function get_help_pages(file_dir)

pipelinedir=fileparts(mfilename('fullpath'));
if exist(fullfile(pipelinedir,'docs','reducplots'),'dir')
  system(['cp ' fullfile(pipelinedir,'docs','reducplots','help_*.html') '  ' file_dir]);
end

return

