function ac=make_template(matrix,hmap,ac,m, sdir, j)

% get the healpix map
if ischar(hmap)
  hmap=read_fits_map(hmap);
end

% If using a WMAP map, it is in units of mK. Convert to uK.

if ~strcmp(hmap.units{1},'uK')
  hmap.map=hmap.map*1e3;
end

%get Temperature fields
mapT=[hmap.map(:,1)',hmap.map(:,4)',hmap.map(:,7)',hmap.map(:,10)',hmap.map(:,16)',hmap.map(:,13)'];


for i=1:length(matrix(sdir).pair(j).wcd)
  %operate deproj templates matrix
  wcd=matrix(sdir).pair(j).wcd{i}*mapT';
  wsd=matrix(sdir).pair(j).wsd{i}*mapT';
  %turn into map
  [m, wcdm]=vect2map(m,wcd);
  [m, wsdm]=vect2map(m,wsd);
  %store as sparse
  ac(j,sdir).wcd{i}=sparse(wcdm);
  ac(j,sdir).wsd{i}=sparse(wsdm);
end

return

