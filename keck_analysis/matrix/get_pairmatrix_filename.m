function filename=get_pairmatrix_filename(tag, mapopt)

fname=char(get_pairmap_filename(tag,mapopt)); 
fname=fname(min(strfind(fname, '/')+1):end); %remove direcotry path
fname=strtok(fname, '.'); %remove .mat

if isfield(mapopt, 'siginterp')
  filename=['matrixdata/pairmatrices/' fname '_' mapopt.sigmaptype '_' ...
            mapopt.siginterp '.mat'];
else
  filename=['matrixdata/pairmatrices/' fname '_' mapopt.sigmaptype '.mat'];
end


return
