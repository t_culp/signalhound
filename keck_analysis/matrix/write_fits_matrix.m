function write_fits_matrix(fname,matrix)
% write_fits_matrix(fname,matrix)
%
% Write a sparse matrix to a fits file.
%
% Modified from healpix map writer write_fits_map.m
%
% e.g. write_fits_matrix('test_reob_matrix.fits',matrix.matrixc.TTTQTU)

% Fill out info field
info.PrimaryData.Keywords={
    'SIMPLE'     'T'                               ''
    'BITPIX'     [                 32]             ''
    'NAXIS'      [                  0]             ''
    'EXTEND'     'T'                               ''
    'END'                           ''             ''
};

% Generate header keywords
xhdr=get_xhdr(matrix);
info.BinaryTable.Keywords=xhdr;

% find row,col,val triplets
[dat{1},dat{2},dat{3}]=find(matrix);

% Assemble a cell array to pass to fitswrite_bintable
%dat{1}=r;
%dat{2}=c;
%dat{3}=v;

% Write fits map
fitswrite_bintable(dat,info,fname);

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xhdr=get_xhdr(matrix)

nfields=3;
unit='test';
npix=nnz(matrix);

xhdr={
    'XTENSION'    'BINTABLE'          ' binary table extension                         '
    'BITPIX'      [             8]    ' 8-bit bytes                                    '
    'NAXIS'       [             2]    ' 2-dimensional binary table                     '
    'NAXIS1'      [     4*nfields]    ' width of table in bytes                        '
    'NAXIS2'      [          npix]    ' number of rows in table                        '
    'PCOUNT'      [             0]    ' size of special data area                      '
    'GCOUNT'      [             1]    ' one data group (required keyword)              '
    'TFIELDS'     [       nfields]    ' number of fields in each row                   '
    'POLCCONV'    'IAU'               ' note BICEP uses IAU                            '
    'NROWS'       [size(matrix,1)]    ' number of rows                                 '
    'NCOLS'       [size(matrix,2)]    ' number of cols                                 '
     };

for k=1:nfields

  % we would prefer to write row/col as integers (code J) but this
  % is way slower
  switch k
    case 1
      unit='numbers';
      form='E';
      type='ROW';
    case 2
      unit='numbers';
      form='E';
      type='COLUMN';
    otherwise
      unit='uK';
      form='E';
      type='VALUE';
  end

  ks=num2str(k);

  xhdr_add={
      ['TTYPE' ks]     type                ' map                                            '
      ['TFORM' ks]     form                ' data format of field: 4-byte REAL              '
      ['TUNIT' ks]     unit                ' map unit                                       '
      };

  xhdr=[xhdr;xhdr_add];

end

xhdr_add = {'END'         ''                  ''};
xhdr=[xhdr;xhdr_add];

return
