function reduc_coaddmatrices(tags,coaddopt)
% reduc_coaddmatrices(tags,coaddopt)
%
% coadd map making is now a three step process:
%   - reduc_matrix accumulates tod into map and matrix for each pair for one
%   or more tags
%   - reduc_coaddmatrices coadds over pairs and tag-groups and makes
%   real maps and coadded matrices
%   - reduc_map2bicep makes sim maps using coaddded matrices.
%
% loop over tag-groups:
%   read in pairmaps, tag_options, matrices
%   accumulate over tags and pairs to make all jackknifes
% finalize maps, matrices
%
% jacktype is jack knife split types (2nd map dim)
% - note default is to gen all if coaddtype=0 or 1, gen only 0 if coaddtpye>1
%   0 = no jack knife - accumulate single 100/150GHz maps
%   >0 means accumulate >1 maps splitting data various ways:
%   1 = deck split, which is really az range split
%   2 = Forward, Backward scans
%   3 = 1st half of tag list versus 2nd half
%   4 = "Instrument Q","Instrument U"
%   5 = Phases B/C/E vs. F/H/I
%   6 = mux column split (even vs. odd)
%   7 = alternative deck split
%   8 = mux row split (0-16 vs. 17-33)
%
% coaddtype controls coadding over pairs (1st map dim)
%   0 = coadd over pairs (default)
%   1 = make ind rx T,Q,U maps
%   2 = make ind pair T,Q,U maps
%   3 = make ind pair sum/diff maps
%   4 = make ind pair T,Q,U maps
%   5 = coadd over first 10 pairs
%
%
% deproj is four element array of zeros and/or ones specifying which modes to deproject
%   deproj(1) = relgain
%   deproj(2) = A/B offsets
%   deproj(3) = beamwidth
%   deproj(4) = ellipticity
%
%   or deproj is false, and no deprojection is done and nothing is appended to filename
%   (default)
%
% deproj_timescale = 'by_scanset'
%                    'by_phase' (default)
%                    'by_raster'
%
%
% Following options dont actually do anything in this prog except
% control which pairmap files are read in:
%
% sernum specifies a serial number for sims
%
%   filt is string specifying half scan filtering method
%
%   weight is half scan weighting in co-add
%     0 = uniform (no weighting)
%     1 = weight by reciprocal of half scan variance pre-filtering
%     2 = weight by reciprocal of half scan variance post-filtering
%     3 = weight by reciprocal of scan set variance post-pairdiff&filtering
%
%   gs is a flag indicating that real data is to be ground subtracted. do the same on the
%   sims (default =  0)
%
%   proj is projection used for map 'radec' 'ortho' 'arc', or 'tan'
%
% chflags - a structure of additional files which get_array_info
%           should read from the aux_data area and apply cuts based on the
%           parameters found there. For instance if we have beams files we could
%           do:
%           chflags.filebase={'beams/beams_dummy'}
%           chflags.par_name={'fwhm_maj'}
%           chflags.low=.4
%           chflags.high=.6
%
%
% cut is a structure of "second round cut" values
%
% tagsublist is a two line cell array. For each element in the
% first row the corresponding second row element gives the tag
% which should be subsituted. This allows to generate only a tag
% subset for signal only sims but to coadd them across all tags
% thus acquiring the correct weighting etc.
%
% e.g.
%      tags=get_tags('cmb2011');
%      clear coaddopt; coaddopt.sernum='0001real';
%      reduc_coaddmatrices(tags,coaddopt)

if(~exist('tags','var'))
  tags=[];
end
if(isempty(tags))
  tags=get_tags;
end

if(~isfield(coaddopt,'sernum'))
   error('No serial number (coaddopt.sernum) specified! Cannot proceed!')
end

% Need this before get_default_coaddopt to get the correct cut parameters
if ~isfield(coaddopt,'tags')
  coaddopt.tags = tags;
end

coaddopt=get_default_coaddopt(coaddopt);


% output coaddopt for the record
coaddopt


if(~strcmp(coaddopt.jacktype, '0'))
  error('must have jacktype 0 for current version of coaddmatrices')
else
  %since we can only do jack zero for matrix coadd, label it as such
  coaddopt.jackname{1}='none';
end

if(coaddopt.coaddtype~=0)
  error('must have coaddtype 0 for current version of coaddmatrices')
end

% read pairmaps for each phase or phase-group and coadd
% for each requested jackknife
tic
[ac,matrixc,m,coaddopt]=accumulate(tags,coaddopt);
toc

% record the list of tags whose pairmaps were coadded
coaddopt.tags=tags;

% option to remove the cut stucture which is
% usefull for repetetive realizations of signflip noise
if isfield(coaddopt,'c') & ~coaddopt.save_cuts
  coaddopt = rmfield(coaddopt,'c');
end

% this helps reduce file size
if isfield(coaddopt,'c')
  coaddopt.c.cp=structcat(1,coaddopt.c.cp);
  coaddopt.c.c1=structcat(1,coaddopt.c.c1);
  coaddopt.c.c2=structcat(1,coaddopt.c.c2);
end

% "backup" the coaddopt structure
coaddopt_bu=coaddopt;


% write a separate file for each requested jackknife
for i=1:length(coaddopt.jacktype)

  % adapt coaddopt to this jack type
  coaddopt=coaddopt_bu;
  coaddopt.jacktype=coaddopt.jacktype(i);
  coaddopt.jackname=coaddopt.jackname{i};

  % save matrix & ac
  % always save 'm' last so we change check for corruptness faster
  matrixname=get_matrix_filename(coaddopt);
  saveandtest(matrixname,'matrixc','ac','coaddopt','m', '-v7.3')
  setpermissions(matrixname);

end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [acc,matrixcc, m,coaddopt]=accumulate(tags,coaddopt)

% read each file of data
for i=1:length(tags)
  % read in the ac file
  if(~isfield(coaddopt,'tagsublist'))
    fname=get_pairmatrix_filename(tags{i},coaddopt);
    fprintf(1,'loading matrix from %s\n',fname);
  else
    % lookup which tag we are in sublist (normally tags and
    % coaddopt.tagsublist will be the same length but we might want
    % the latter to be a superset of the former so do it this way
    % for greater generality)
    thistag=strmatch(cvec(tags{i}), cvec(coaddopt.tagsublist(1,:)));
    % read in the tag which is to be substituted, which must be a weight0
    % pairmatrix.
    fname=get_pairmatrix_filename(coaddopt.tagsublist{2,thistag},...
        setfield(coaddopt, 'weight',0));
    fprintf(1,'loading substitute matrix from %s\n',fname);
  end

  tic
  % Don't bother with figuring out which variables to load if we're going to
  % load everything anyway.
  if ~isfield(coaddopt,'pair_subset') || ~any(coaddopt.pair_subset)
    pairmatrix = load(fname);

  else
    % First get the list of variables in the mat file. Since we explode every
    % variable in reduc_matrix(), 'explodeinfo' will contain a complete
    % variable listing.
    meta = load(fname, 'explodeinfo');
    varnames = {};

    % Produce the listing of expected fields exactly as implode_struct() does.
    for ii=1:length(meta.explodeinfo.name)
      fullname = meta.explodeinfo.name{ii};
      membname = strrep(fullname, '.', '_');
      numelems = prod(meta.explodeinfo.size{ii});

      manglenames = arrayfun(@(i) sprintf('%s_%05d', membname, i), ...
          1:numelems, 'UniformOutput', false);

      varnames = {varnames{:} manglenames{:}};
    end

    % From the variables already listed, filter out particular entries to
    % leave only the requested pair subset. Right now, I see this solution to
    % be relatively general without over-complicating the matter, but note
    % that this filtering needs to be manually kept up-to-date with the save
    % procedure in reduc_matrix.m.
    keepvars = {'explodeinfo'};
    for ii=1:length(varnames)
      nn = length(varnames{ii});

      % If it's an exploded entry we want to subset, keep it only if it's in
      % the pair_subset list.
      %
      % Magic constants:
      %    3 = length('ac_')
      %    8 = length('ac_00001')
      %    7 = length('matrix_')
      %   23 = length('matrix_00001_pair_00001')
      %
      % Checking for length 23 allows us to distinguish 'matrix_00001' from
      % 'matrix_00001_pair_00001' without resorting to regular expressions,
      % which should help with performance.
      if (  (nn==8  && strncmp(varnames{ii},'ac_',3)) ...
         || (nn==23 && strncmp(varnames{ii},'matrix_',7)) )

        varnum = str2num(varnames{ii}(end-4:end)); % Explosion uses 5 digits
        if any(varnum == coaddopt.pair_subset)
          keepvars{end+1} = varnames{ii};
        end

      % Keep everything else
      else
        keepvars{end+1} = varnames{ii};
      end
    end

    pairmatrix = load(fname, keepvars{:});
  end

  % Implode the entries if needed
  pairmatrix = implode_struct(pairmatrix);

  % Finally lift the structure members to local scope.
  fields = fieldnames(pairmatrix);
  for ii=1:length(fields)
    eval(sprintf('%s = pairmatrix.%s;', fields{ii}, fields{ii}));
  end
  clear pairmatrix

  toc

  % Get the set of good channels for this day - use chflags structure
  % to control the set of used channels.
  % This is in effect an additional "second round cut" - note that this also
  % effects some of the cuts proper because some depend on the set of
  % good channels to determine what fraction of potentially good
  % channels are passing.
  [p,ind]=get_array_info(tags{i},[],[],[],[],coaddopt.chflags);

  % Free up memory.
  % - Don't need anything except matrix(:).pair
  % - Don't need matrix(:).pair(:).pointm and .weights
  % - Don't care about non-light detectors
  [dum,la]=intersect(ind.a,ind.la);
  for sdir=length(matrix)
    matrix2(sdir).pair(la)=rmfield(matrix(sdir).pair(la),{'pointm','weights'});
  end
  matrix = matrix2;
  clear matrix2;

  [day,phase,scanset,dk]=parse_tag(tags{i});

  %sorta a hack (JET)
  mapopt.tags={mapopt.tag};

  % unpack the ac structure
  ac=acunpack(ac);


  % store the mapopt/ind - this results in very big size so only do it for
  % first scanset
  if(i==1)
    coaddopt.mapopt{i}=mapopt;
    coaddopt.ind(i)=ind;
    coaddopt.p(i)=p;
  end


%---------------------------------------------------------%

  % DO "SECOND ROUND" CUTS

  % Load real data and make corrections to substitution data if necessary.
  if isfield(coaddopt, 'tagsublist') && ~isempty(coaddopt.tagsublist)
    % First verify that pairmatrix was generated correctly.
    if (~isfield(mapopt,'flavor_subset') || ~mapopt.flavor_subset)
      error(['Cannot correctly reweight pairmatrix created with' ...
             'mapopt.flavor_subset not set or set to false.'])
    end

    % Get data from the corresponding real pairmaps
    coaddoptreal=coaddopt;
    coaddoptreal.sernum=sprintf('%s%s',coaddopt.realpairmapset,'real');
    if isfield(coaddopt,'realpairmapopt')
      fld=fieldnames(coaddopt.realpairmapopt);
      for k=1:numel(fld)
        coaddoptreal.(fld{k}) = coaddopt.realpairmapopt.(fld{k});
      end
    end
    filename=get_pairmap_filename(tags{i},coaddoptreal);
    fprintf(1, 'loading real mapopt from %s...\n',filename);
    x=load(filename,'mapopt');

    % First set cuts as appropriate
    mapopt.c=x.mapopt.c;

    % Then for a tag substitution, reweight substituted ac and matrix with
    % weights and variances from real pairmap.
    disp('tag substitution in effect: doing reweighting...')
    tic
    ac=redo_weights(ac,x.mapopt.hs,ind,'ac');
    matrix=redo_weights(matrix,x.mapopt.hs,ind,'matrix');
    toc

    % copy in the half-scan stats which now apply (these are only used for
    % diagnostics but might as well)
    mapopt.hs = x.mapopt.hs;

    clear x
  end

  % evaluate the second round cuts
  [c,mapopt.c.cp]=eval_round2_cuts(mapopt.c.cp,mapopt.c.c,coaddopt.cut,p,ind);

  % combine the cuts to get final channel selection
  c.overall=combine_cuts(c,ind);

  % convert good pair list from get_array_info above into a cut for
  % later visualization
  c.rgl=false(1,c.nch); c.rgl(ind.rgl)=true;

  % delete all flagged sums/diffs from the relevant fields of the ind
  % structure - this applies the cut
  ind.rgl=intersect(ind.rgl,find(c.overall));
  ind.rgla=intersect(ind.rgla,find(c.overall));
  ind.rglb=intersect(ind.rglb,find(c.overall));

  % compress the cutparams and and per scanset cut masks - impossible
  % to store per scanset resolution info over 1000's of scansets
  mapopt.c.cp=compress_perhs(mapopt.c.cp);
  mapopt.c.c=compress_perhs(mapopt.c.c);

  %if we are not sim store the cut parameters and masks for later
  % reference. (There is little point in storing this info for sims as
  % for a given daughter set it will be identical to that for the real
  % maps.)
  if(~isfield(mapopt,'simopt'))
    coaddopt.c.cp(i)=mapopt.c.cp;
    coaddopt.c.c1(i)=mapopt.c.c;
    coaddopt.c.c2(i)=c;
  end

%---------------------------------------------------------%

 % RECORD POST CUT DIAGNOSTIC QUANTITIES

  % take maximum weight over the scanset
  coaddopt.hsmax(i).w=nanmax(mapopt.hs.w);
  % the maximum deviation - max(max-value-in-half-scan/std-of-half-scan)
  coaddopt.hsmax(i).d=nanmax(mapopt.hs.m./mapopt.hs.s);

  % mask to only the data which passed all cuts and is going into the maps
  k=setdiff(ind.e,ind.rgl);
  coaddopt.hsmax(i).w(k)=NaN;
  coaddopt.hsmax(i).d(k)=NaN;

  % Also accumulate histogram of scanset/channel weights
  % accumulated over good sum and diff channels
  [bcs,ns]=hfill(mapopt.hs.w(:,ind.rgla),100,0,25);
  [bcd,nd]=hfill(mapopt.hs.w(:,ind.rglb),100,0,100);
  if(~isfield(coaddopt,'whist'))
    coaddopt.whist.sum.bc=bcs; coaddopt.whist.sum.n=ns;
    coaddopt.whist.dif.bc=bcd; coaddopt.whist.dif.n=nd;
  else
    coaddopt.whist.sum.n=coaddopt.whist.sum.n+ns;
    coaddopt.whist.dif.n=coaddopt.whist.dif.n+nd;
  end

  % coadd the histograms of "deviation" (tod/std) over good sum and
  % diff channels
  if(~isfield(coaddopt,'devhist'))
    coaddopt.devhist.sum.bc=mapopt.devhist.bc;
    coaddopt.devhist.dif.bc=mapopt.devhist.bc;
    coaddopt.devhist.sum.n=sum(mapopt.devhist.n(ind.rgla,:),1);
    coaddopt.devhist.dif.n=sum(mapopt.devhist.n(ind.rglb,:),1);
  else
    coaddopt.devhist.sum.n=coaddopt.devhist.sum.n+sum(mapopt.devhist.n(ind.rgla,:),1);
    coaddopt.devhist.dif.n=coaddopt.devhist.dif.n+sum(mapopt.devhist.n(ind.rglb,:),1);

   end

%---------------------------------------------------------%

  % SETUP FOR THE CO-ADD

  % Make sure to choose a representative element that is within the
  % pair_subset list, otherwise this structure is unlikely to be the correct
  % shape. This is complicated by needing to make sure it's a light detector.
  if isfield(coaddopt, 'pair_subset')
    dum = intersect(ind.a(coaddopt.pair_subset), ind.la);
    repidx = find(ind.a == dum(1));

  % Otherwise just use the first as representative
  else
    repidx = 1;
  end

  % make all-zero ac and matrix.pair structures - these are used in two places
  if ~exist('acz','var')
    acz = zerodup(ac(repidx));
  end
  if ~exist('matrixcz','var')
    matrixcz = zerodup(matrix(1).pair(repidx));
  end


  %---------------------------------------------------------%
  %only coadd for this subset of pairs:
  if isfield(coaddopt, 'pair_subset') && any(coaddopt.pair_subset)
    display(['pair_subset ' num2str(min(coaddopt.pair_subset)) '-' ...
             num2str(max(coaddopt.pair_subset))])

    %la
    [dum,j]=intersect(ind.a, ind.la);
    dumm=intersect(j,coaddopt.pair_subset);
    ind.la=ind.a(dumm);
    %rgla
    [dum,j, jj]=intersect(ind.a, ind.rgla);
    dumm=intersect(j,coaddopt.pair_subset);
    ind.rgla=ind.a(dumm);

    %make a temp
    ac_temp=repmat(acz,size(ac));
    matrix_temp.pair=repmat(matrixcz, size(matrix.pair));
    matrix_temp=repmat(matrix_temp, size(matrix));

    [dum,j]=intersect(ind.a, ind.rgla);
    for k=j
      for sdir=1:length(matrix)
        ac_temp(k,sdir)=ac(k,sdir);
        matrix_temp(sdir).pair(k)=matrix(sdir).pair(k);
      end
    end

    clear ac;clear matrix;
    ac=ac_temp;matrix=matrix_temp;
    clear ac_temp;clear matrix_temp;
  end

  %---------------------------------------------------------%


  %DEPROJECTION

  % ACCUMULATE TAGS UP TO DEPROJECTION TIMESCALE

  if(~exist('acp'))
    % if we are first (or only) tag of phase make an all zero acp
    acp=repmat(acz,size(ac));
    matrixcp=repmat(matrixcz,size(ac));
  end

  % add this scanset to any previous using only good pairs for this
  % specific scanset - its highly dynamic due to cuts
  %! THIS IS WHERE RGLs are applied!

  %at this point we need to coadd matrix over pairs and lr,
  %or else we will run out of memory!
  %this isn't going to work if we ever want scandir matrices

  [dum,j]=intersect(ind.a,ind.rgla);
  for k=j
    for sdir=1:length(matrix)
      acp(k,sdir)=addac(acp(k,sdir),ac(k,sdir));
      matrixcp(k,sdir)=addac(matrixcp(k,sdir),matrix(sdir).pair(k));
    end
  end
  clear matrix

  % there are several things which may trigger us to deproject and
  % coadd into jackknifes
  deproj=false;

  % if we have been requested to do per scanset deprojection then
  % force termination of accumulation
  if any(coaddopt.deproj) && strcmp(coaddopt.deproj_timescale, 'by_scanset')
    deproj=true;
  end

  if(i==length(tags))
    % if this is the last tag we need to finish up
    deproj=true;
  else
    % look up the phase of the next tag
    [dum,next_phase]=parse_tag(tags{i+1});

    % if next tag starts a new phase
    if any(coaddopt.deproj) && ~strcmp(phase,next_phase)
      switch coaddopt.deproj_timescale
        case 'by_phase'
          % deproj upon completion of each phase
          deproj=true;
        case 'by_raster'
          % deproj on completion of raster
          % (phase G will get lumped into HI raster)
          % NB: this makes phase jack become BC vs EFGHI
          if(any(phase=='CFI'))
            deproj=true;
          end
      end
    end
  end

  if(~deproj)
    % skip to next tag <-- NB SKIPS ALL CODE BELOW!
    continue
  end

  disp('done accumulating tags...')

  % move phase accumulated ac structure to ac to avoid changing all
  % the code below
  ac=acp;
  matrixc=matrixcp;

  % clear acp to signal that next loop iteration will start a fresh
  % accumulation
  clear acp;clear matrixcp;

  %setup final coadd
  matrixcc=repmat(matrixcz,[1,1]);
  acc=repmat(acz,[1,1]);

  % scale each deprojection mode by the regression coefficient
  if any(coaddopt.deproj)
    disp('doing regression & coadd...')
    tic
    if(isfield(ac,'wcd'))
      % do the regression on all possible pairs since the used set
      % varies by scanset
      [dum,j]=intersect(ind.a,ind.la);

      for k=j
        % note that we are allowing here seperate regressor scalings for
        % forward and backward scans - this may not be what we
        % would want
        % note that if we have mapopt.do_scanjack=0, then the above ...
        % statement is no longer true and we differ from normal
        % pipeline

        if ~rem(k,20)
          display([num2str(k) ' of ' num2str(max(j))])
        end

        for sdir=1:size(ac,2)
          %do regression
          [ac(k,sdir),matrix_temp]=regress_matrix(ac(k,sdir),coaddopt,m);
          %do deproj
          depro=matrix_temp.deproj*[matrixc(k,sdir).awcgfcha_awcgfsha', ...
                                    matrixc(k,sdir).awsgfcha_awsgfsha']';
          matrixc(k,sdir).awcgfcha_awcgfsha=depro(1:end/2,:);
          matrixc(k,sdir).awsgfcha_awsgfsha=depro(end/2+1:end,:);

          %coadd
          acc=addac(acc,ac(k,sdir));
          matrixcc=addac(matrixcc,matrixc(k,sdir));
        end
      end
    end
    toc
  else %deprojection not true
    disp('doing coadd...')
    % add the matrixcp and acp, if we do not deproj.
    [dum,j]=intersect(ind.a,ind.la);
    for k=j
      for sdir=1:size(ac,2)
        acc=addac(acc,ac(k,sdir));
        matrixcc=addac(matrixcc,matrixc(k,sdir));
      end
    end
  end %deproj on/off
end %tags

return

