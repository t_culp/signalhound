function wl=calc_pixel_window(map,lmax)
% function wl=calc_pixel_window(map,lmax)
%
% Calculate average window spectrum of pixels in map
% See: http://healpix.jpl.nasa.gov/html/intronode14.htm
%
% Inputs:
%  map:  the map, as per get_map_defn
%  lmax: maximum ell to which to calculate window
%
% Outputs:
%  wl.l:     ell coordinate for spectra (0:lmax)
%  wl.w:     pixel window for each constant-latitude row in map
%  wl.w_avg: average window function over all rows in map
% 
% integral may be incorrect for large pixel size or high lmax
% (ie pixels larger than a few degrees, lmax above about 2000)

% TODO this is pretty slow
% choosing the mid-point in lat gives the same result as average for high-l
% but has extra wiggles at low l that need to be "averaged over"

t_total = tic;

if (ischar(map))
  disp(['Calculating pixel window function: ' map]);
  map = get_map_defn(map);
else
  disp('Calculating pixel window function');
end

N_points = 1000;  % this needs to be increased for very large pixels/high lmax
wl.l = (0:lmax)';
wl.w = zeros(lmax+1,map.ny);

% window size depends only on delta_phi, so all pixels in a row are the same
dphi = pi/180*(map.hx - map.lx)/map.nx;

tenpercent = tic;
for ii=1:map.ny
  if(mod(ii,round(map.ny/10))==0)
    disp(['...ii = ' num2str(ii) '/' num2str(map.ny)]);
    toc(tenpercent)
    tenpercent = tic;
  end

  zmin = cosd(90 - map.y_tic(ii) + map.pixsize/2);
  zmax = cosd(90 - map.y_tic(ii) - map.pixsize/2);
  zspace = (zmax - zmin)/(N_points);
  z = zmin+zspace/2:zspace:zmax-zspace/2;

  for ll=0:lmax  % done ll=0 above
    Pl = legendre(ll,z,'sch'); % size = [ll+1, length(z)]
    theint = trapz(Pl,2) * zspace; % integrate over z, for each mm in Pl

    mm = (0:ll)';
    temp = (2 - 2*cos(mm*dphi))./(mm.^2*dphi^2*(zmax-zmin)^2);
    temp(1) = 1; % apply correct limit for m=0, which was 0/0
    wl.w(ll+1,ii) = sqrt(sum((theint.^2) .* temp));
  end

end

wl.w_avg = sqrt(sum(wl.w.^2,2)/map.ny);

toc(t_total)

