function matrix_driver(task,rx)
% function matrix_driver(task,rx)
%
% Generates a reobserving matrix.
%
% INPUTS
%     task        Determines the operation mode of the driver script. The two
%                 valid options are
%
%                   1. 'generate' - Create farm files and places them on disk
%                   2. 'submit'   - Runs a loop over babysitjobs() to submit
%                                   the job files.
%
%                 Both options can be set in a cell array in which case the
%                 driver will perform both tasks in the same call.
%
%     rx          Array of receiver numbers to submit. Useful for submitting
%                 with several instances when job submission time is the
%                 bottleneck. Defaults to [0,1,2,3,4].
%


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % GENERAL

  % Serial number for the matrices to produce, as an integer and daughter ID
  nbase = 1704;
  daughter = 'cmb2014';
  % If true, produce pairmatrices for only the tag flavor subset and coadd
  % the entire season using tag substitutions and reweighting.
  do_reweight = true;
  % The serial number which has the real pairmaps produced with the standard
  % pipeline. Might match nbase, but probably typically will be a different
  % string.
  reweight_sernum = '1351real';
  %reweight_sernum = sprintf('%04dreal',nbase);
  reweight_daughter = 'd';

  % The tag flavor subset list needs to be calculated by using an existing
  % coadded real map. This tells which one to use.
  reweight_srcmap = sprintf(...
      'maps/%s/%s_%s_filtp3_weight3_gs_dp1100_jack01.mat', ...
      reweight_sernum(1:4), reweight_sernum(5:8), reweight_daughter);

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % TAGS
  
  tag_name='cmb2014'
  tags = get_tags(tag_name, {'has_tod'});

  % For a reweighting coadd, get the tag substitution list and minimal tag
  % list by examining the real coadd.
  if do_reweight
    fprintf(1,'getting minimum tag list from %s...\n', reweight_srcmap);
    x = load(reweight_srcmap,'coaddopt');
    [tagsublist,mintags] = get_tag_sublist(x.coaddopt);
    clear x;

  % Otherwise, the minimal tag list is just the tag list.
  else
    mintags = tags;
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % MAPOPTS
  mapopt.sernum     = sprintf('%04dreal', nbase);
  mapopt.deproj_map = {...
    'input_maps/planck/planck_derivs_nopix/HFI_SkyMap_100_2048_R1.10_nominal_nside0512_nlmax1280_bKuber100.fits';...
    'input_maps/planck/planck_derivs_nopix/synfast_deproj_143_nominal_B2.fits'...
  };
  mapopt.sigmaptype = 'healmap';
  mapopt.siginterp  = 'healpixnearest';
  mapopt.type       = 'bicep';
  mapopt.covn       = 0;
  mapopt.beamcen    = 'obs';
  mapopt.chi        = 'obs';
  mapopt.epsilon    = 'obs';
  mapopt.gs         = 1;
  mapopt.filt       = 'p3';
  %mapopt.diffpoint = 'obs';
  mapopt.diffpoint  = 'ideal';
  mapopt.deproj     = 1;
  mapopt.do_scanjack= 0;
  if do_reweight
    mapopt.weight = 0;
    mapopt.flavor_subset = true;
  end

  % The actual contents of these healpix maps don't actually matter; all we
  % need is the pixelization scheme to agree with any input sky you'll want to
  % multiply against the reobserving matrix.
  mapopt.sigmapfilename = {...
    'input_maps/camb_planck2013_r0/map_unlens_n0512_r0001_sKuber100_cPl100_dPl100.fits',...
    'input_maps/camb_planck2013_r0/map_unlens_n0512_r0001_sB2bbns_cPl143_dNoNoi.fits'...
  };
  mapopt = get_default_mapopt(mapopt);

  pairfarmopt.dofarm  = 1;
  pairfarmopt.submit  = 0; % Leave at 0 if using driver loops below
  % Use default mem and maxtime options

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % COADDOPTS

  % Base the coaddopt on the mapopt, but *EXPLICITLY* remove the cut structure
  % so that the correct defaults for coaddopt are loaded. Included are any
  % other fields which may have different definitions between mapopt and
  % coaddopt.
  coaddopt            = rmfield(mapopt,{'cut','realpairmapset'});

  coaddopt.sernum     = sprintf('%04dreal', nbase);
  coaddopt.daughter   = daughter;
  coaddopt.jacktype   = '0';
  coaddopt.coaddtype  = 0;
  coaddopt.deproj     = [1,1,0,0];
  % Comment out this line for BICEP2 since chflags weren't yet defined.
  coaddopt.chflags    = get_default_chflags([], tags{1}(1:4));
  % Need another few options for a reweighting coadd
  if do_reweight
    % Tell where real pairmaps exist
    coaddopt.realpairmapset = reweight_sernum(1:4);
    % Specify the substitution list
    coaddopt.tagsublist     = tagsublist;
    % Inherited weight0 from mapopt, so override to weight3
    coaddopt.weight         = 3;
  end

  coaddfarmopt.dofarm  = 1;
  coaddfarmopt.split   = 16; % pair subsets in groups of 16
  coaddfarmopt.submit  = 0; % Leave at 0 if using driver loops below
  % Use default mem and maxtime options

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ~exist('task','var') || isempty(task)
    task = {'generate'};
  end
  if ~iscell(task)
    task = {task};
  end

  if ~exist('rx','var') || isempty(rx)
    rx = [0,1,2,3,4];
  end

  dogenerate = false;
  dosubmit   = false;
  if any(ismember('generate', task))
    dogenerate = true;
  end
  if any(ismember('submit', task))
    dosubmit = true;
  end


  % Generate all the pairmatrix farmfiles on disk. This only needs to be done
  % once for all later loop executions.
  if dogenerate
    farm_makepairmatrix(mintags, mapopt, pairfarmopt);
  end

  % Run a busy-wait loop here in the driver and only use babysitjobs to
  % resubmit farmfiles so that we can try to run all stages as available.
  %
  % Emulate a C-style "do { } while(condition);" loop to guarantee this
  % runs at least once.
  loopnum = 1;
  maxpairjobs = 300;
  pairdelta = 10;
  maxcoaddpairjobs = 1000;
  coaddpairdelta = 25;
  missingcoadds = {};
  while ( length(missingcoadds) > 0 | loopnum == 1 )
    % Theoretically, a large number of pair matrix coadds can run in
    % parallel across the cluster since the operations are CPU bound. Just
    % be careful with how many are spawned initially to minimize the impact
    % of the simultaneous startup I/O burden.
    wildcard = sprintf('%s/*makepairmatrix*.mat', mapopt.sernum(1:4));

    % Step up to the full amount in increments, choosing the number of jobs
    % in incremental jumps.
    if dosubmit
      numpairjobs = min(maxpairjobs, loopnum * pairdelta);
      babysitjobs(wildcard, 'resubmit', [], false, numpairjobs);
    end

    % Then figure out which coadd steps can be run.
    if dogenerate
      missingcoadds = farm_coaddmatrices(tags, coaddopt, coaddfarmopt);
      if length(missingcoadds)==0
        break
      end
    end

    if dosubmit
      % First spawn up to a relatively small number of pair coadds.
      numcoaddpairjobs = min(maxcoaddpairjobs, loopnum * coaddpairdelta);

      % Create a general pattern which selects the correct folder and receivers
      rxpattern = ['[' sprintf('%i',rx) ']'];
      genpattern = sprintf('%s/*%%s*%s*.mat', ...
          coaddopt.sernum(1:4), rxpattern);

      % The default time and memory requirements are set pretty low in
      % farm_coaddmatrices() to avoid over-requesting for many jobs which are
      % fast due to cuts sparsifying the data heavily. For complete data,
      % though, we need more time and memory, so be relatively generous when
      % adding time and memory on resubmits.
      wildcard = sprintf(genpattern, 'coadd_pairsubset');
      babysitjobs(wildcard,'resubmit', [], false, numcoaddpairjobs, ...
        'mem','+8000', 'maxtime','+60');

      % We can run a higher number of coadds into rx's since they are a lot
      % less I/O heavy. This step will likely tend to be job-exhausted,
      % though, waiting on the previous step to complete.
      %
      % Like the previous, be somewhat generous in giving more mem/time.
      wildcard = sprintf(genpattern, 'coadd_phaserx');
      babysitjobs(wildcard,'resubmit', [], false, numcoaddpairjobs, ...
        'mem','+4000', 'maxtime','+30');

      % Let the month coadds run unempeded since there can't be too many of
      % them.
      wildcard = sprintf(genpattern, 'coadd_monthrx');
      babysitjobs(wildcard,'resubmit', [], false, [], ...
        'mem','+1500', 'maxtime','+60');

      % Same for the final whole-rx coadd.
      wildcard = sprintf(genpattern, 'coadd_wholerx');
      babysitjobs(wildcard,'resubmit', [], false, [], ...
        'mem','+2000', 'maxtime','+30');

      % Finally, coadd to individual frequencies.
      wildcard = sprintf('%s/*coadd_freq*.mat', coaddopt.sernum(1:4));
      babysitjobs(wildcard,'resubmit', [], false, [], ...
        'mem','+2500', 'maxtime','+20');
    end

    % Signal end of first iteration of "do { } while (condition);"
    loopnum = loopnum + 1;
    
    % Now wait 10 minutes before going again.
    disp('Pausing for 2 minutes.')
    pause(60*2);
    missingcoadds = listfiles(sprintf('farmfiles/%04d/*.mat',nbase));
  end

  %we can finalize the matrix, so that it gives T,Q,U maps (instead of "ac")
  if(0)
    finalize_matrix(matrix_name, dofarm);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function finalize_matrix(matrix_name,dofarm)

  if ~exist('dofarm','var') || isempty(dofarm)
    dofarm = true;
  end

  if dofarm
    % limit to 200 jobs
    LICENSE='bicepfs1:5'; % 1500 / 300 = 5
    QUEUE='general,serial_requeue';
    mem=80000;
    farmit('farmfiles/', ...
      'make_matrix(matrix_name);', ...
      'license',LICENSE, 'queue',QUEUE, 'mem',mem, ...
      'var',{'matrix_name'});
  else
    make_matrix(matrix_name);
  end

end

