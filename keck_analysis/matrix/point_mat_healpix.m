function pointhm=point_mat_healpix(ind, j, d, p, mapind, hmap, mapopt)

%Creates a matrix, (pointm.a), of dimensions mapind x npixels
%(npixels is the number of pixels in your output map), that takes a
%vector of tod data and bins it into a map vector.
%
%This is matrix version of gen_sig
%
%INPUTS:
%    ind: detector channel for this matrix
%    d: tod data   (ie from reduc_initial)
%    p: focal plane structure
%    mapind: locations in d with map data
%    hmap:healpix map (used to determine npixels and location)
%    mapopt:  Contains:
%             diffpoint: whether to map a and b seperately
%             interptype: what type of interpolation.
%                (either 'taylor' or 'healpixnearest')
%
%OUTPUTS:
%    pointhm.a: output matrix
%    pointhm.dphi: matrix with derivatives of phi position
%    pointhm.detheta: matrix with derivatives of theta position
%
%if mapopt.diffpoint='obs', them output pointm(1)= for A detector
%                                       pointm(2)= for B detector
%e.g.
%load('data/real/201107/20110704C01_dk293_tod.mat')
%mapind=make_mapind(d,fs);
%mapopt.diffpoint='obs';
%mapopt.siginterp='healpixnearst';
%[p,ind]=get_array_info('20110704C01_dk293', [],[],[],mapopt.diffpoint);
%hmap=read_fits_map('input_maps/camb_wmap5yr_noB/map_cmb_n0512_r0001_s31p00.fits');
%pointhm=point_mat_healpix(ind, 3, d, p, mapind, hmap, mapopt)
%
%
%
%JET (20120423)
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%display('point_mat_healpix...')
%tic
switch mapopt.diffpoint
  case 'ideal'
    ind=ind.a(j);
  case 'obs'
    ind=[ind.a(j), ind.b(j)];
end

%loop over both (or one) ind
for j=1:length(ind)
  i=ind(j);

  x0=d.pointing.cel.ra(mapind);
  y0=d.pointing.cel.dec(mapind);
  dk=d.pointing.cel.dk(mapind);

  % find ra/dec trajectory for this feed
  [y,x]=reckon(y0,x0,p.r(i),p.theta(i)-90-dk);

  %NOT necessay?
  %if(m.lx>0)
  %  x(x<0)=x(x<0)+360;
  %end

  %Make pointing matrix
  pointhm_temp=make_pointing_matrix_healpix(x,y,hmap, mapopt.siginterp);

  %fill in
  pointhm(j).a=pointhm_temp.a;
  if(strcmp(mapopt.siginterp, 'taylor'))
    pointhm(j).dphi=pointhm_temp.dphi;
    pointhm(j).dtheta=pointhm_temp.dtheta;
  end

  % calc the pol angle for the detector
  th=p.theta(i)-dk;
  thref=p.chi_thetaref(i)-dk;
  alpha=chi2alpha(x0,y0,p.r(i),th,p.chi_mean(i),thref);

  % Ganga alpha_h
  ap=(alpha+p.beta(i))*pi/180;

  % take sin and cos
  pointhm(j).c=cos(2*ap); pointhm(j).s=sin(2*ap);

end  %loop over j

%toc
return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function pointhm=make_pointing_matrix_healpix(ra,dec,hmap, interptype)


%BASED LARGERLY ON healpix_interp.m

%  (ra,dec)   are timestream coordintes of dector pair
%   hmap        the map structure
%interptype   'healpixnearest' or 'taylor'

%makes a pointing matrix (mapind X numel(hmap.pixel) X #pairs) that conatins
%    pointing info for each pair.


%set up pointing matix
pointhm.a=sparse(length(ra),length(hmap.pixel));

% deg to rad
theta=cvec((90-dec)*pi/180);
phi=cvec(ra*pi/180);

% NaN values make healpix code fragile -- replace with a sensible value
badpix=~isfinite(theta)|~isfinite(phi);
theta(badpix)=0;
phi(badpix)=0;

% Find nearest pixel to each requested position
switch hmap.ordering
 case 'RING'
   pix=ang2pix_ring(hmap.nside,theta,phi);
 case 'NESTED'
   pix=ang2pix_nest(hmap.nside,theta,phi);
 otherwise
   error('healpix map must have RING or NESTED ordering')
end

% if needed find actual coords of those healpix pixels
if(strcmp(interptype,'taylor'))
  switch hmap.ordering
    case 'RING'
      [theta_p,phi_p]=pix2ang_ring(hmap.nside,pix);
    case 'NESTED'
      [theta_p,phi_p]=pix2ang_nest(hmap.nside,pix);
  end
end

% find pixel indices in hmap
[dum,hmaploc]=ismember(pix,hmap.pixel);

if any(hmaploc==0)
  % Identify any pixel locations which weren't found in the healpix map. Then
  % fixup the row-column indices into the sparse matrix to skip over these.
  locmask = hmaploc~=0;
  maploc = find(locmask);
  hmaploc = hmaploc(locmask);
else
  % Everything matched up, so there's an entry in every row of the pointing
  % matrix.
  maploc = 1:length(ra);
end

switch interptype
  case 'taylor'
    %distance in theta and phi of desired interpolation locations from nearest pixel center
    dtheta=theta-theta_p;
    dphi=(phi-phi_p);
    dphi(dphi<-1)=dphi(dphi<-1)+2*pi;
    dphi=dphi.*sin(theta);

    %save dphi and dtheta vectors for interpolation later
    pointhm.dphi=dphi;
    pointhm.dtheta=dtheta;

  case 'healpixnearest'
    % Nothing else special to be done.

  otherwise
    error('not a valid interptype')
end

%put a 1 wherever we are pointing
pointhm.a=sparse(maploc, hmaploc, 1, length(ra), length(hmap.pixel));
%should be able to make above smaller by using logical... true(1,length(ra))

return

