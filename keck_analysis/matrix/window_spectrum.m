function wspec=window_spectrum(spec,map,beam,lmax,hpix)
% function wspec=window_spectrum(spec,map,beam,lmax=300,hpix=0)
%
% Apply pixel and beam windowing to a power spectrum
%
% Inputs:
%  spec : input spectrum (filename, or from load_cmbfast)
%  map  : the map (name string per get_map_defn, or name/object of healpix map)
%         if not present, don't do pixel windowing
%  beam : FWHM in arcmin, or 'B2bbns' for B_l file
%         if not present or zero, don't do beam windowing
%  lmax : l up to which to compute window functions
%         Only used by pixel windows, which are set to zero for higher l
%  hpix : include and set to true is using a healpix map

% TODO maybe this should go somewhere else?
pixel_path = 'data/pixel_windows/';

if (ischar(spec))
  spec = load_cmbfast(spec);
end
wspec = spec;

if ~exist('beam','var')
  beam = [];
end

if (exist('map','var') && ~isempty(map))
  % do pixel window
  if (exist('hpix','var') && hpix) % using healpix
    [rv hpdir] = system('echo -n $HEALPIX');
    if (ischar(map))
      map=read_fits_map(map);
    end
    nside = num2str(map.nside,'%04d');
    pixfile = [hpdir '/data/pixel_window_n' nside '.fits'];
    disp(['Using pixel window: ' pixfile]);

    pixel_window = fitsread(pixfile,'bintable');
    pixel_window = pixel_window{1}.^2;

  else % not using healpix
    nfiles = dir([pixel_path, 'pix_window_', map, '_l*.mat']);
    found_match = 0;
    if (~isempty(nfiles))
      % check for suitable high lmax
      numind = 14 + length(map);
      for ii=1:length(nfiles)
        flmax = str2num(nfiles(ii).name(numind:numind+4));
        if (flmax >= lmax)
          found_match = 1;
          load([pixel_path, nfiles(ii).name]);
          disp(['loading pixel windows: ', pixel_path, nfiles(ii).name]);
          pixel_window(lmax+1:end) = 0; % if flmax is higher, zero some
          break;
        end
      end
    end
    if (~found_match)
      % no exisiting windows, calculate new one
      pixel_window = calc_pixel_window(map, lmax);
      pixel_window = pixel_window.w_avg.^2;
      fname = [pixel_path, 'pix_window_', map, '_l', ...
                 num2str(lmax,'%04d'), '.mat'];
      disp(['saving new window function: ', fname]);
      save(fname,'pixel_window');
    end

  end % healpix

  wspec.W_p2 = zeros(length(wspec.l),1);
  if (length(wspec.l) < length(pixel_window))
    wspec.W_p2(:) = pixel_window(1:length(wspec.l));
  else
    wspec.W_p2(1:length(pixel_window)) = pixel_window(:);
  end

else
  disp('not applying pixel windowing');
  wspec.W_p2 = ones(size(wspec.l));
end

if isempty(beam)
  disp('not applying beam windowing')
  wspec.W_b2 = ones(size(wspec.l));

elseif isnumeric(beam)
  if isnumeric(beam) && beam > 0
    % convert from fwhm(arcmin) to rms(rad)
    b_theta = beam*pi/180/60/sqrt(8*log(2));
    % calculate the (approximate) beam window function
    wspec.W_b2 = exp(-0.5*b_theta^2*wspec.l.*(wspec.l+1)).^2;
  else
    disp('not applying beam windowing')
    wspec.W_b2 = ones(size(wspec.l));
  end

elseif ischar(beam)
  [ell,Bl] = get_bl(beam);

  % If the beam function has fewer specified modes than the spectrum, trucate
  % the input spectrum.
  if length(ell) < length(wspec.l)
    ll = length(ell);
    fprintf(1,'Truncating input spectrum from %i to %i modes.\n', ...
        length(wspec.l), ll);
    wspec.l    = wspec.l(1:ll);
    wspec.W_p2 = wspec.W_p2(1:ll);
    wspec.Cs_l = wspec.Cs_l(1:ll,:);
    wspec.C_l  = wspec.C_l(1:ll,:);
  end

  wspec.W_b2=Bl(1:length(wspec.l)).^2;
  clear dum
end

for ii=1:size(wspec.C_l,2)
  wspec.Cs_l(:,ii) = wspec.Cs_l(:,ii) .* wspec.W_b2 .* wspec.W_p2;
  wspec.C_l(:,ii) = wspec.C_l(:,ii) .* wspec.W_b2 .* wspec.W_p2;
end

return
