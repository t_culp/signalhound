function plot_matrix_eigenmodes(eigmatrix,queb,modes,savefigs)
% plot_matrix_eigenmodes(eigmatrix,queb,modes,savefigs)
%
% Plots the given modes for the E and B pure subspaces coming from the
% solution to the generalized eigenvalue problem. The eigenvectors are reshaped
% to Q and U, and then transformed to E and B maps. The magnitudes are shown on
% a colorscale, and the corresponding polarization vectors are plotted on top.
%
% INPUTS
%   eigmatrix    Either the file path to the eigenvalue problem solution, or
%                a struct containing all of a solution's file's contents.
%
%   queb         If 'qu', plots the Q and U maps for the specified modes,
%                otherwise for 'eb' plots E and B maps.
%
%   modes        A list of the modes to plot. Defaults to 'end-[0,1,4,9,49]'
%                to show the 1st, 2nd, 5th, 10th, and 50th B eigenmodes.
%
%   savefigs     Defaults to 'eps'. If not false, then the plots are saved to
%                disk within matrix_plots/ in either 'png' or 'eps' format.
%
% EXAMPLE
%   plot_matrix_eigenmodes(['matrixdata/c_t/1706/healpix_red_spectrum_' ...
%       'lmax700_beamKuber100rev1_reob1351_d_100GHz_QQQUUU_eigen.mat']);
%

  if ~exist('queb','var') || isempty(queb)
    queb = 'qu';
  end
  if ~exist('savefigs','var') || isempty(savefigs)
    savefigs = 'eps';
  end

  % Only load from disk if we haven't been passed an already-loaded structure
  if ~isstruct(eigmatrix)
    filename = eigmatrix;
    fprintf(1,'Loading the eigensystem matrix %s...\n', filename)
    eigmatrix = load(filename);
  else
    filename = eigmatrix.file;
  end

  % Load relevant variables into local namespace.
  b = eigmatrix.b; obs_pixels = eigmatrix.obs_pixels; m = eigmatrix.m;
  if isfield(eigmatrix, 'covmatopt')
    reob = eigmatrix.covmatopt(1).reob;
  else
    reob = eigmatrix.reob;
  end

  if ~exist('modes','var') || isempty(modes)
    modes = length(obs_pixels) - [0, 1, 4, 9, 49];
  end

  % 0704-era matrices didn't store the lmax with the reobservation
  % information, so choose lmax=700 as a default and warn about this fact.
  if ~isfield(eigmatrix, 'covmatopt')
    lmax = 700;
    warning('plot_matrix_eigenmodes:unknownLmax', ['The maximum ell used' ...
        ' in the production of this matrix was not stored. Assuming' ...
        ' lmax=700.']);
  else
    lmax = eigmatrix.covmatopt(1).lmax;
  end

  % Select just the modes we actually want
  modesqu = b(:,modes);
  clear x b;

  % Normalize the vectors, just because it makes the scales look nicer
  modesqu = bsxfun(@rdivide, modesqu, sqrt(sum(modesqu.^2)));

  % Then to create E/B out of the Q/U that expand_vect will give us, we
  % need to retrieve some information from the reobserving matrix.
  reobmap = load(reob.file, 'map');
  for ii=1:size(modesqu,2)
    % Turn the mode eigenvector back into a map
    maptmp = expand_vect(m, modesqu(:,ii), obs_pixels);
    % Then inject the necessary var information to produce E/B
    maptmp.Tvar = reobmap.map.Tvar;
    maptmp.Qvar = reobmap.map.Qvar;
    maptmp.Uvar = reobmap.map.Uvar;

    % Then turn into E/B maps.
    modemap(ii) = make_ebmap(m, maptmp, 'normal', [0,lmax]);
  end

  % Plot a couple eigenvector modes
  [dim,ax] = setup_mode_plot(m, length(modes));

  % Use different colormaps for the types to make it visually clear. Jet for
  % Q/U and Lint for E/B.
  if strcmp(queb,'eb')
    colormap(colormap_lint());
    mapf = {'E','B'};
  else
    colormap('jet');
    mapf = {'Q','U'};
  end

  for ii=1:length(modemap)
    set(gcf, 'currentaxes', ax(ii,1));
    % Plot the eigenmode directly
    imagescnan(m.x_tic, m.y_tic, modemap(ii).(mapf{1}));

    if strcmp(queb,'eb')
      % Then for generating the quiver plots, we need to smooth the map or
      % else the noise fluctuations destroy any visual pattern. (This is also
      % justifiable since plot_pol() will only sample one out of every 3
      % pixels to create the quiver plot.)
      fakemap.Q    = modemap(ii).EQ;
      fakemap.U    = modemap(ii).EU;
      fakemap.Qvar = modemap(ii).Qvar;
      fakemap.Uvar = modemap(ii).Uvar;
      fakemap.T    = fakemap.Q;
      fakemap.Tvar = fakemap.Qvar;
      fakemap = smooth_maps(m, fakemap);
      fakemap = smooth_varmaps(m, fakemap);
      % Determine a scale that will show the pattern nicely by normalizing
      % everything so that the most intense regions are nearly unity. We
      % can't choose an absolute scale since these modes aren't absolutely
      % calibrated into uK_cmb.
      scale = 1/prctile(abs([modemap(ii).E(:); modemap(ii).B(:)]), 98);
      hold on
      plot_pol(m, ...
          fakemap.Q,    fakemap.U, ...
          fakemap.Qvar, fakemap.Uvar, ...
          scale, [], 3, false);
      hold off
    end

    % Make sure the x-y directions are shown as is standard
    axis('xy')
    set(gca, 'xdir','reverse');
    % Explicitly set the axis ticks to avoid clashes between plots
    set(gca, 'xtick', [-50,-25,0,25,50])
    set(gca, 'ytick', [-65,-60,-55,-50])
    % Remove x-axis tick labels from everything except the bottom plots since
    % the rest are on the same scale and can share these labels.
    if ii==1
      title([mapf{1} ' modes'])
    end
    if ii~=length(modemap)
      set(gca, 'xticklabel', {});
    else
      xlabel('RA [deg]')
    end
    % E-modes are in the left column, so label every plot
    ylabel('Dec [deg]')
    % Collect color scale so that each half of the mode can be made
    % consistent.
    clims(1,:) = caxis();


    % Now do all the same again, but for B-modes in the right column...
    set(gcf, 'currentaxes', ax(ii,2));
    imagescnan(m.x_tic, m.y_tic, modemap(ii).(mapf{2}));
    if strcmp(queb, 'eb')
      fakemap.Q    = modemap(ii).BQ;
      fakemap.U    = modemap(ii).BU;
      fakemap.Qvar = modemap(ii).Qvar;
      fakemap.Uvar = modemap(ii).Uvar;
      fakemap.T    = fakemap.Q;
      fakemap.Tvar = fakemap.Qvar;
      fakemap = smooth_maps(m, fakemap);
      fakemap = smooth_varmaps(m, fakemap);
      scale = 1/prctile(abs([modemap(ii).E(:); modemap(ii).B(:)]), 98);
      hold on
      plot_pol(m, ...
          fakemap.Q,    fakemap.U, ...
          fakemap.Qvar, fakemap.Uvar, ...
          scale, [], 3, false);
      hold off
    end

    axis('xy')
    set(gca, 'xdir','reverse', 'ytick',[]);
    set(gca, 'yaxislocation','right')
    set(gca, 'xtick', [-50,-25,0,25,50])
    set(gca, 'ytick', [-65,-60,-55,-50])
    set(gca, 'yticklabel', {});
    if ii==1
      title([mapf{2} ' modes'])
    end
    if ii~=length(modemap)
      set(gca, 'xticklabel', {});
    else
      xlabel('RA [deg]')
    end
    ylabel(sprintf('Eigenmode %i', modes(ii)))
    clims(2,:) = caxis();


    % Keep the necessary extremes, and then center both color scales around
    % zero.
    clims = [min(clims(:,1)),max(clims(:,2))];
    clims = clims - mean(clims);

    set(gcf, 'currentaxes', ax(ii,1));
    caxis(clims);
    % Empirically, it looks like this function does something so that printing
    % to EPS/PDF has the proper white background in NaN regions that is shown
    % on screen. Without it, the NaN backgrounds always turn black, no matter
    % what arguments are given to imagescnan().
    freezeColors();

    set(gcf, 'currentaxes', ax(ii,2));
    caxis(clims)
    freezeColors();
  end

  % Set global title using information we should have available.
  [dirname,fname,ext] = fileparts(filename);
  if isfield(eigmatrix, 'covmatopt')
    sernum = eigmatrix.covmatopt.sernum;
    [dum,reobfile,dum] = fileparts(eigmatrix.covmatopt(1).reob.file);

  else
    % For pre-covmatopt data products, extract info given the file name.
    if ~isempty(filename)
      [dum,sernum,dum] = fileparts(dirname);
    else
      warning('plot_matrix_eigenmodes:unknownReobs', ['The covariance ' ...
        'given does not contain enough identifying information. Passing ' ...
        'its filename instead of a struct may be more meaningful.'])
      sernum = 'XXXX';
    end

    % Then get which reobservation was used
    [dum,reobfile,dum] = fileparts(eigmatrix.reob.file);
  end

  h = gtitle({sprintf('Eigenmodes for covariance %s reobserved with', ...
      sernum), reobfile}, dim.y1(2)+(dim.wide+dim.med)/dim.H, 'none');
  set(h, 'verticalalignment','bottom', 'fontsize', 10);

  if ischar(savefigs)
    outfname = sprintf('matrix_plots/eigenmodes_%s_%s_%s', queb, sernum, ...
        fname);
    switch savefigs
      case 'eps'
        % From pieces of print_fig():
        set(gcf,'PaperPositionMode','auto');
        set(gcf, 'Renderer', 'painters')
        set(gcf, 'RendererMode', 'manual')
        print(gcf, '-depsc2', '-painters', outfname);
        fix_lines([outfname '.eps'])

      case 'png'
        mkpng(outfname);
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  function [dim,ax]=setup_mode_plot(m,nmodes)
  % [dim,ax]=setup_map_plot(m,nmodes)
  %
  % Helper function which sets up axes and plot size to match a single plotted
  % map.
  %

    % Generic parameters for plot panel layouts:
    dim.thin = 0.05; % Small gap, in inches.
    dim.med  = 0.24; % Medium gap, in inches.
    dim.wide = 0.40; % Wide gap, in inches.
    dim.cbar = 0.15; % Width of color bar, in inches.
    % Full-figure width in inches
    dim.W = 10;

    % E-modes in left column, B-modes in right column.

    % Calculate the width of each plot given how many plots we need to fit
    % and the padding required between each.
    dim.subw = (dim.W - 2*dim.wide-dim.med - dim.thin) / 2;
    % Then make each plot the correct proportional size given the field
    % dimensions.
    dim.subh = dim.subw * m.ydos / m.xdos;
    % Now calculate the height of the figure in total
    dim.H = 4*dim.wide+2*dim.med + nmodes*dim.subh + (nmodes-1)*dim.thin;

    % Two columns:
    dim.x1 = dim.wide+dim.med + [0, dim.subw];
    dim.x2 = dim.x1(2) + dim.thin + [0, dim.subw];
    % Normalize to be fractions of the figure size.
    dim.x1 = dim.x1 / dim.W;
    dim.x2 = dim.x2 / dim.W;

    % Now build the row positions, remembering that the positions start at the
    % bottom of the figure but we will work in matrix-like numbering starting
    % from the top.
    for ii=1:nmodes
      % Field names
      y = sprintf('y%i', ii);
      % Invert to count from top of figure instead of bottom
      jj = nmodes - ii + 1;
      % Set positions in inches.
      dim.(y) = 2*dim.wide+(jj-1)*(dim.subh+dim.thin) + [0, dim.subh];
      % Normalize the row calculations to be fractions of the figure size
      dim.(y) = dim.(y) ./ dim.H;
    end

    % Now actually generate the figure
    fig = figure();
    % Make the figure the right size, both on screen and in print
    set(fig, 'Units','inches');
    p = get(fig, 'Position');
    set(fig, 'Position', [p(1), p(2)+p(4)-dim.H, dim.W, dim.H])
    set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');
    % Put axes on the page
    clf();
    for ii=1:nmodes
      % Field names
      y = sprintf('y%i', ii);

      ax(ii,1) = axes('Position',[dim.x1(1),dim.(y)(1),diff(dim.x1),diff(dim.(y))]);
      ax(ii,2) = axes('Position',[dim.x2(1),dim.(y)(1),diff(dim.x2),diff(dim.(y))]);
    end
  end
end

