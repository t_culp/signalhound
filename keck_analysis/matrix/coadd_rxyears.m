function coadd_rxyears(fnames, sernum, daughter, ukpervolt)    
%
%eg
%fnames={'matrix/matrices/0704/real_cmb2010_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrix/matrices/0704/real_cmb2011_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrix/matrices/0704/real_cmb2012_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat'}
%fnames={'matrixdata/matrices/0704/real_cmb201208-201209_rx0_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrixdata/matrices/0704/real_cmb201208-201209_rx1_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrixdata/matrices/0704/real_cmb201208-201209_rx2_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrixdata/matrices/0704/real_cmb201208-201209_rx3_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrixdata/matrices/0704/real_cmb201208-201209_rx4_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat'}
%sernum='0704real';
%daughter='cmb201208-201209_rxall'
%coadd_rxyears(fnames, sernum, daughter)


%fnames={'matrixdata/matrices/0705/real_cmb201308-201309_rx0_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrixdata/matrices/0705/real_cmb201308-201309_rx1_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrixdata/matrices/0705/real_cmb201308-201309_rx2_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrixdata/matrices/0705/real_cmb201308-201309_rx3_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrixdata/matrices/0705/real_cmb201308-201309_rx4_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat'}
%sernum='0705real';
%daughter='cmb201308-201309_rxall'
%coadd_rxyears(fnames, sernum, daughter)

%fnames={'matrixdata/matrices/0704/real_cmb201208-201209_rxall_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrixdata/matrices/0705/real_cmb201308-201309_rxall_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat'}
%sernum='0706real';
%daughter='cmb_subset2012_2013_rxall'
%coadd_rxyears(fnames, sernum, daughter)


%fnames={'/n/bicepfs2/bicep2/pipeline/matrixdata/matrices/0704/real_allcmb_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat','matrixdata/matrices/0706/real_cmb_subset2012_2013_rxall_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest.mat'}
%sernum='0707real';
%daughter='cmb_b2_and_k2012_2013'
%ukperv=[3150,3400];  %is this right for keck?
%coadd_rxyears(fnames, sernum, daughter, ukperv)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%coadd phasematrices
do_farm=true;
cmd='reduc_coadd_coadds(fnames,sernum,daughter,0,ukpervolt);';


if(do_farm)
  
  %setup for farm
  [s username]=system('whoami');
  username=strtrim(username);
  QUEUE='general,serial_requeue';
  mem=70000;  %make sure you know how much mem and time this will take
  maxtime=720; %minutes 
  
  farmit('farmfiles',{cmd},'queue',QUEUE,'mem', mem, 'maxtime', maxtime, 'var',{'fnames', 'sernum', 'daughter', 'ukpervolt'});

else

  eval(cmd)

end


return
