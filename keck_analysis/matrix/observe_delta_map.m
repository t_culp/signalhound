function observe_delta_map(deltaopt)
% function map_name = observe_delta_map(deltaopt)
% creates a stack of observed  maps "maps/delta/sernum/XXYYYE" or "XXYYYB"
% which have a delta peak at the given multipole deltaopt.l.
% "XX" is the user number
% "YYY" is the ell number
% "E" means TT=TE=EE=1 and "B" means TT=TB=BB=1
%
% deltaopt.spec     = 'E' - delta function in TT,TE and EE
%                   = 'B' - delta function in TT and BB
% deltaopt.l        = e.g. 80 - the ell number at which the stack is generated
% deltaopt.rlzs     = e.g. [0:100] - these realization are produced
%                     is passed to run_synfast where it will use the random seed list
% deltaopt.nlmax    = use 1499 - passed to gen_delta_map
% deltaopt.beam     = e.g. {get_bl_filename('B2bbns'),'B2bbns'} 
%                     can also be numeric to specify fwhm
% deltaopt.matrixname = filename of reob matrix to use
% deltaopt.sernum   = e.g. '2801' - a place to save the maps
%
% called from reduc_makedeltamaps


%grab inputs from deltaopt
spec=deltaopt.spec;
l=deltaopt.l;
rlzs=deltaopt.rlzs;
nlmax=deltaopt.nlmax;,
beam=deltaopt.beam;
sernum=deltaopt.sernum;

%nside must be 512 for reob matrix
nside = 512;
 
%-------------------------%
for r=1:length(rlzs)      
  rlz=rlzs(r);
  
  %make a healpix map
  map_name{r} = gen_delta_map(spec,l,rlz,nside,nlmax,beam);
  
  %if this is first realization, load reob matrix
  if(r==1)
    %load reob matrix once now
    display(['Loading ',deltaopt.matrix_name])
    tic
    obs_matrix=load(deltaopt.matrix_name);
    toc
    %get the coaddopt & m
    coaddopt=obs_matrix.coaddopt;
    m=obs_matrix.m;
  end
  
  %reobserve all this maps and place in stack
  simtype='0'; %this doesn't matter
  cmap=reduc_observemap(obs_matrix,map_name{r},simtype,0,0);
  
  % if the obs matrix was not finalized, convert ac into map:
  if isfield(cmap,'wcs')
    cmap = make_map(cmap,m,coaddopt);
  end
  % and save it. This is going to be an size=rlzx1 - so the jackknife dimension
  % is used for the stack.
  map(r)=cmap;
  
  %rm the healpix map 
  system_safe(['rm ' map_name{r}]);
end  %rlz

clear obs_matrix;

%thow out cuts for sims since they are big
if isfield(coaddopt, 'c')
  coaddopt=rmfield(coaddopt, 'c');
  coaddopt=rmfield(coaddopt, 'hsmax');
end

%and save stack
stackname=gen_deltastack_name(coaddopt, l, spec, sernum);
saveandtest(stackname, 'map', 'map_name', 'coaddopt','m');
setpermissions(stackname);

return
