function missingct=farm_covmattheory(covmatopt,farmopt)
% missingct=farm_covmattheory(covmatopt,farmopt)
%
% Driver which intelligently runs reduc_covmattheory() over pixel subsets
% before combining the pieces to create a full covariance matrix.
% 
% INPUTS
%   covmatopt    Structure of information used in creating and processing the
%                theory covariance matrices. See the doc string in
%                reduc_covmattheory() for more information.
% 
%   farmopt     Structure of options which control the farm jobs which are
%               produced.
% 
%               .dofarm     Defaults to true. If false, then run interactively
%                           (useful for testing).
% 
%               .maxtime    List of maximum times (in minutes) to request for
%                           each job, based on the stage. Defaults to
%                           [1440,1440] for the pixel-subset and combine
%                           stages, respectively.
% 
%               .mem        List of maximum memory (in megabytes) to request
%                           for each job, based on the stage. Defaults to
%                           [70000,40000] for the pixel-subset and combine
%                           stages, respectively.
% 
%               .queue      Queue to submit job to. Defaults to using both
%                           'general,serial_requeue'.
% 
%               .submit     Defaults to false. If false, then the farmfile is
%                           placed on disk but not submitted to the queue. This
%                           is particularly useful if using babysitsims().
% 
% OUTPUTS
% EXAMPLE
% 

  if ~exist('farmopt','var') || isempty(farmopt)
    farmopt = struct();
  end

  if ~isfield(farmopt,'dofarm') || isempty(farmopt.dofarm)
    farmopt.dofarm = true;
  end
  if ~isfield(farmopt,'maxtime') || isempty(farmopt.maxtime)
    farmopt.maxtime = [1440,1440];
  end
  if ~isfield(farmopt,'mem') || isempty(farmopt.mem)
    farmopt.mem = [70000,40000];
  end
  if ~isfield(farmopt,'queue') || isempty(farmopt.queue)
    farmopt.queue = 'general,serial_requeue';
  end
  if ~isfield(farmopt,'submit') || isempty(farmopt.submit)
    farmopt.submit = false;
  end

  covmatopt = get_default_covmatopt(covmatopt);

  % Validate the reobservation parameters if reobservations are being done
  if isstruct(covmatopt.reob)
    % It is an error to have covmatopt.reob be a struct and not point to a
    % valid reobserving matrix.
    if ~isfield(covmatopt.reob,'file') || isempty(covmatopt.reob.file) ...
        || ~exist_file(covmatopt.reob.file)
      error('farm_covmattheory:InvalidReobs', ['covmatopt.reob.file ' ...
          'must point to valid, finalized reobserving matrix, otherwise ' ...
          'disable reobservations with covmatopt.reob = false'])
    end
    % Define fields automatically if necessary
    if ~isfield(covmatopt.reob,'field') || isempty(covmatopt.reob.field)
      covmatopt.reob.field = {'TT','TQ','TU','QQ','QU','UU'};
    end
  end

  missingct = {};

  % Calculate pixel centers locations
  if strfind(covmatopt.map,'.fits')
    [lat,lon] = pixel_centers([], covmatopt.map);
    maptype = 'healpix';
  else
    [lat,lon] = pixel_centers(covmatopt.map, []);
    maptype = covmatopt.map;
  end
  % Then immediately put into structure form, either splitting based on
  % constant-latitude pixel subsets or a single group if no subsets are
  % requested.
  [lat,lon] = pixel_subset(lat, lon, covmatopt.subsets);
  % For diagnostics, also record the type of lat/lon points
  lat.map = maptype;
  lon.map = maptype;

  % Window the spectrum appropriately
  if ischar(covmatopt.spec)
    specfile = covmatopt.spec;
    covmatopt.spec = window_spectrum(specfile, [], covmatopt.beam, ...
        covmatopt.lmax);
    % Also store the originating file in the spectrum structure
    covmatopt.spec.file = specfile;
  end

  % Ensure the farmfile directory exists
  if ~exist(['farmfiles/' covmatopt.sernum(1:4)], 'dir')
    system_safe(['mkdir -p farmfiles/' covmatopt.sernum(1:4)]);
  end

  ct = {};

  % lat.subset will be a cell of length 1 with all pixels if subsetting is
  % disabled, so no need to handle anything specially.
  for ii=1:length(lat.subset)
    % Set the proper pixel subsets
    sublat = lat;
    sublon = lon;
    sublat.subset = lat.subset{ii};
    sublon.subset = lon.subset{ii};

    % Update the covmatopt for this subset
    subcovmatopt = covmatopt;
    subcovmatopt.subset = sublat.subset(1);

    % Then use sub-function to handle all fields
    [newmissing,newct] = farm_overfields(sublat, sublon, subcovmatopt, ...
        farmopt);

    missingct = [missingct newmissing];
    ct = [ct newct];
  end % loop over lat.subset

  % If all the subsets have not been generated, then return now.
  if ~isempty(missingct)
    return
  end

  % Get babysitsims-compatible farmfile and jobnames
  ctname = get_ct_filename(covmatopt);
  [dum,farmid,dum] = fileparts(ctname);
  [farmfile,jobname] = farmfilejobname(covmatopt.sernum, ...
      'combine_ct', farmid);

  % Next, check the filesystem to see if the completed data product
  % already exists on disk
  if exist_file(ctname)
    fprintf(1,'Done. Covariance theory matrix exists: %s\n', ctname)
    return
  end
  % At this point, we know the data product is missing, so add it to the
  % list of missing matrices.
  missingct = [missingct {ctname}];

  % If we've gotten to this point, then we can combine over all the subsets
  if farmopt.dofarm
    farmit(farmfile, ...
        'combine_ct(covmatopt);', ...
        'jobname',jobname, ...
        'maxtime',farmopt.maxtime(2), ...
        'mem',farmopt.mem(2), ...
        'queue',farmopt.queue, ...
        'submit',farmopt.submit, ...
        'overwrite','skip', ...
        'var',{'covmatopt'});
  else
    combine_ct(covmatopt);
  end
end

function [missingct,ct]=farm_overfields(lat,lon,covmatopt,farmopt)
%function [missingct,ct]=farm_overfields(lat,lon,covmatopt,farmopt)
%
%Generates farm files for all fields specified in the given covmatopt.
%

  % Initialize outputs
  missingct = {};
  ct = {};

  if isstruct(covmatopt.reob)
    for jj=1:length(covmatopt.reob.field)
      subcovmatopt{jj} = covmatopt;
      subcovmatopt{jj}.reob.field = covmatopt.reob.field{jj};
    end
  else
    subcovmatopt{1} = covmatopt;
  end

  for jj=1:length(subcovmatopt)
    covmatopt = subcovmatopt{jj};
    % Get babysitsims-compatible farmfile and jobnames
    ctname = get_ct_filename(covmatopt);
    [dum,farmid,dum] = fileparts(ctname);
    [farmfile,jobname] = farmfilejobname(covmatopt.sernum, ...
        'covmattheory', farmid);

    % Next, check the filesystem to see if the completed data product
    % already exists on disk
    ct{jj} = ctname;
    if exist_file(ctname)
      continue
    end
    % At this point, we know the data product is missing, so add it to the
    % list of missing matrices.
    missingct = [missingct {ctname}];

    % Either submit the appropriate options to farmit or execute the command
    if farmopt.dofarm
      farmit(farmfile, ...
          'reduc_covmattheory(lat,lon,covmatopt);', ...
          'jobname',jobname, ...
          'maxtime',farmopt.maxtime(1), ...
          'mem',farmopt.mem(1), ...
          'queue',farmopt.queue, ...
          'submit',farmopt.submit, ...
          'overwrite','skip', ...
          'var',{'lat','lon','covmatopt'});
    else
      reduc_covmattheory(lat, lon, covmatopt);
    end
  end % loop over subcovmatopt
end
