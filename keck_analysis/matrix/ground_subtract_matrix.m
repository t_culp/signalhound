function gsm=ground_subtract_matrix(d,fs,ind,flavsubset)
%function gsm=ground_substract_matrix(d,fs,ind,flavsubset)
%
%Creates a matrix. (gsm), of dimensions mapind x mapind which performs
%ground subtraction, on a timestream, (d). Ground subtaction subtracts the 
%mean signal of a scanset a particular az position, for a particular
%scan direction. It is really a comb filter.
%
%
%INPUTS:
%    d: tod data   (ie from reduc_initial)
%    fs: scan sturcture 
%    ind: channel to construct matrix for. It is necessary to
%         construct a different matrix for each channels timestream in
%         order to have the mean ignore NaNs.
%    flavsubset: true if creating a flavor subset matrix, otherwise false (e.g.
%            mapopt.flavor_subset)
%
%OUTPUTS:
%    gsm: ground subtraction matrix
%
%
%e.g.
%load('data/real/20110730E01_dk113_tod.mat')
%gsm=ground_subtract_matrix(d,fs,3);
%
%
%JET (201200525)
%this version assumes all halfscans are same length (as original ...
%    ground_subtract.m does) and subtracts a mean signal taken across
%    all the halfscans going the same direction.
% 
%   This version doed not handle interpolation across azz points with
%   many NaNs, which normal gs does do.


%input is single scanset only (unlike orignal ground_subtract)

%could add functionality to output d, a filtered timestream


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%disp('ground_subtract_matrix...');
%tic


%make some things
mapind=make_mapind(d,fs);
tod=d.mce0.data.fb(mapind,ind);
nan_tod=isnan(tod);
tod_length=length(tod);

%exit if there is no data for this channel and this isn't a substitution
%matrix
if (~flavsubset && ~nansum(d.mce0.data.fb(mapind,ind)))
  display('all data NAN for this channel')
  gsm=speye(tod_length);
  return
end

% construct Denis' damn fs.src
if(~isfield(fs,'src'))
  for i=1:length(fs.s)
    fs.src{i,1}=d.antenna0.tracker.source(fs.s(i),:);
  end
end


%find halfscan starting points in mapind (rather than tod) for each halfscan
index=1;

for i=1:length(fs.sf)
  n=fs.ef(i)-fs.sf(i)+1;
  fsm.sf(i)=index;
  fsm.ef(i)=index+n-1;

  if(fs.inc(i) ==1)
    fsm.inc(index:index+n-1)=1;
  else
    fsm.inc(index:index+n-1)=0;
  end  
  index=index+n;
end

%find out if the source is az-fixed.
if isempty(strfind(fs.src{1},'CMB4')) && ...
   isempty(strfind(fs.src{1},'CMB5')) && ...
   isempty(strfind(fs.src{1},'AZCIRC'))
  disp('not scanning a CMB az fixed region, no GS possible')
  gsm=speye(tod_length) % gcm is identity
else    

  %number of data points in halfscan    
  nhalfscan=fsm.ef(1)-fsm.sf(1)+1;

  %set a sparse identity for this halfscan
  id=sparse(1:nhalfscan,1:nhalfscan,1);

  %gs is a comb filter
  hsgs=repmat(id, 1,length(fsm.sf));

  %but it ignores places that are nan in tod (for non-flavor tags)
  if ~flavsubset
    hsgs=bsxfun(@times, hsgs, ~nan_tod');
  end

  %make a left and right filter
  hsgs_l=bsxfun(@times, hsgs, fsm.inc);
  hsgs_r=bsxfun(@times, hsgs,~fsm.inc);


  %construct mean taking matrix 
  %find number of filled elements in each row
  nl=sum(hsgs_l,2);
  nr=sum(hsgs_r,2);

  %normalize by sum
  hsgs_l=bsxfun(@times, hsgs_l,1./nl);
  hsgs_r=bsxfun(@times, hsgs_r,1./nr);


  %setup output vectors
  elements=sum(~fs.inc)*length(find(hsgs_r))+sum(fs.inc)*length(find(hsgs_l));
  gsm_v.az=zeros(1,elements);
  gsm_v.locs=zeros(1,elements);
  gsm_v.num=zeros(1,elements);

  v_ind=1;
  %loop over halfscans
  for i=1:length(fs.sf)
    s=fsm.sf(i); e=fsm.ef(i);

    %check they are really all same length
    if(e-s+1 ~= nhalfscan)
      display('halfscan lengths not all identical')
      keyboard
    end  

    %only for this halfscans dir
    if(fs.inc(i)==1)
      hsgs_i=hsgs_l;
    else
      hsgs_i=hsgs_r;
    end  

    %shift for this hs's position
    hsgs_i=circshift(hsgs_i, s-1);

    %get vectors
    [v.az, v.locs, v.num]=find(hsgs_i);

    %shift az by nhalfscan
    v.az=v.az+nhalfscan*(i-1);

    %fill into gsm for cumlative all  halfscans
    gsm_v.az(v_ind:v_ind-1+length(v.az))=v.az;
    gsm_v.locs(v_ind:v_ind-1+length(v.az))=v.locs;
    gsm_v.num(v_ind:v_ind-1+length(v.az))=v.num;

    v_ind=v_ind+length(v.az);
  end %loop over half scans

  %create sparse matrix
  gsm=sparse(gsm_v.az,gsm_v.locs,gsm_v.num, tod_length, tod_length); 

  %make matrix which subtracts mean at this az & scan dir from each point.
  gsm=speye(tod_length)-gsm;

  %toc
end  %actually doing gs


%plot to test
if(0)

  %MAtRIX GS
  nogs=d.mce0.data.fb(mapind,ind);
  j=isnan(nogs);
  nogs(j)=0;
  mat_gs=gsm*nogs;

  %Without subtraction:
  nogs=d.mce0.data.fb(mapind,ind)-nanmean(d.mce0.data.fb(mapind,ind));

  %NORMAL GS
  nor_gs=ground_subtract(d,fs,ind);
  nor_gs=nor_gs.mce0.data.fb(mapind,ind);

  clf
  setwinsize(gcf, 900,400)

  npoints=1000;

  subplot(2,1,1)
  hold on 
  plot(nogs(1:npoints))
  plot(mat_gs(1:npoints),'.r')
  plot(nor_gs(1:npoints),'g')
  legend('no gs', 'matrix gs' , 'normal gs')
  subplot(2,1,2)
  plot(mat_gs(1:npoints)-nor_gs(1:npoints))
  title('difference of matrix and normal')

%  print_fig('../eb_mat/ground_sub_comp_new')  

end

return

