function missing=farm_coaddmatrices(tags,coaddopt,farmopt,timeout)
%farm_coaddmatrices(tags,coaddopt,farmopt,timeout)
%
%Wrapper utility around reduc_coaddmatrices and reduc_coadd_coadds which
%intelligently calls farmit for each task as required. It also knows how to
%determine which pairmatrices have been produced---allowing the coadds to
%proceed---from those which are missing (and therefore does not attempt to
%farm the coadd).
%
%INPUTS
%  tags        Tags of phases for which coadded pairmatrices should be
%              produced.
%
%  coaddopt    coaddopt structure which specified options used to produce
%              each coadd. See reduc_coaddmatrices for more info.
%
%  farmopt     Structure of options which control the farm jobs which are
%              produced.
%
%              .dofarm     Defaults to true. If false, then run interactively
%                          (useful for testing).
%
%              .maxtime    List of maximum time (in minutes) to request for
%                          jobs as a function of coadd stage. The list should
%                          be given in the order corresponding to:
%
%                            [ COADDSTAGE_FREQMATRIX, COADDSTAGE_RXMATRIX, ...
%                              COADDSTAGE_MONTHRX, COADDSTAGE_PHASERX, ...
%                              COADDSTAGE_PAIRSUBSET ]
%
%                          Defaults to [120, 120,  60, 30, 30] for BICEP2.
%                          Defaults to [180, 180, 120, 30, 30] for Keck.
%
%              .mem        List of maximum memory (in megabytes) to request
%                          for jobs as a function of coadd stage. The list
%                          should be given in the order corresponding to:
%
%                            [ COADDSTAGE_FREQMATRIX, COADDSTAGE_RXMATRIX, ...
%                              COADDSTAGE_MONTHRX, COADDSTAGE_PHASERX, ...
%                              COADDSTAGE_PAIRSUBSET ]
%
%                          Defaults to [40000,28000,28000,26000,16000] with no deproj.
%                          Defaults to [80000,80000,60000,30000,20000] with deproj.
%
%              .split      If non-zero, then the coadd of pairmatrices is
%                          divided into .split number of pair subsets.
%                          Defaults to 0 if no deprojection, 16 with
%                          deprojection enabled.
%
%              .queue      Queue to submit job to. Defaults to using both
%                          'itc_cluster,serial_requeue'.
%
%              .submit     Defaults to false. If false, then the farmfile is
%                          placed on disk but not submitted to the queue. This
%                          is particularly useful if using babysitjobs().
%
%  timeout     Defaults to 1800 seconds (30 minutes). If non-zero, the function
%              will abort recursion through the dependency tree if the timeout
%              is reached. This is useful for very large tag lists with split
%              submissions which can take too long to cycle through the entire
%              tag list in a reasonable time period.
%
%OUTPUTS
%  missing     A list of all missing data products (input, intermediate, and
%              final). See get_pairmatrix_filename() and get_matrix_filename()
%              for more information on the file naming possibilities.
%
%              All coadds have successfully finished when this list is empty.
%
%EXAMPLE
%
%  tags = get_tags('cmb2014',{'has_tod,'has_cuts'});
%
%  % ...
%  % Run farm_makepairmatrix
%  % ...
%
%  coaddopt          = rmfield(mapopt, 'cut');
%  coaddopt.jacktype = '0';
%  coaddopt.deproj   = [1 1 0 0];
%  coaddopt.chflags  = get_default_chflags([], tags{1}(1:4));
%  coaddopt = get_default_coaddopt(coaddopt);
%
%  farmopt.dofarm = true;
%  farmopt.split = 16;
%  farmopt.submit = false;
%
%  missing = farm_coaddmatrices(tags, coaddopt, farmopt);
%  while ~isempty(missing)
%
%    % Keep the queue full with 50 pair-subset coadds
%    babysitjobs('9999/*coadd_pairsubset*.mat', 'resubmit', [], [], 50);
%    % Also allow up to 200 per-phase, per-rx coadds over coadds
%    babysitjobs('9999/*coadd_phaserx*.mat', 'resubmit', [], [], 200);
%    % Allow all per-month, per-rx coadds since there aren't many
%    babysitjobs('9999/*coadd_monthrx*.mat', 'resubmit');
%
%    % Wait 10 minutes before trying again
%    pause(10 * 60);
%
%    % Redefine the missing list
%    missing = farm_coaddmatrices(tags, coaddopt, farmopt);
%
%  end
%

  % Common default values
  default_queue = 'itc_cluster,serial_requeue'; % SLURM queues

  % Depending on deprojection options, request different memory requirements
  if isfield(coaddopt,'deproj') && any(coaddopt.deproj)
    default_mem   = [80000,80000,60000,30000,20000]; % MBytes
    default_split = 16;
  else
    default_mem   = [40000,28000,28000,26000,16000]; % MBytes
    default_split = 0;
  end

  % Other default values are experiment dependent
  switch get_experiment_name
    case {'bicep2'}
      default_maxtime = [120,120, 60,30,30];  % minutes
    case {'keck'}
      default_maxtime = [180,180,120,30,30];  % minutes
  end


  %%%% Process/verify all the inputs %%%%


  % Set default farming options
  if ~exist('farmopt','var') || isempty(farmopt)
    farmopt = struct();
  end
  if ~isfield(farmopt,'dofarm') || isempty(farmopt.dofarm)
    farmopt.dofarm = true;
  end
  if ~isfield(farmopt,'maxtime') || isempty(farmopt.maxtime)
    farmopt.maxtime = default_maxtime;
  end
  if ~isfield(farmopt,'mem') || isempty(farmopt.mem)
    farmopt.mem = default_mem;
  end
  if ~isfield(farmopt,'queue') || isempty(farmopt.queue)
    farmopt.queue = default_queue;
  end
  if ~isfield(farmopt,'split') || isempty(farmopt.split)
    farmopt.split = default_split;
  end
  if ~isfield(farmopt,'submit') || isempty(farmopt.submit)
    farmopt.submit = false;
  end

  % Default to a 30 minute timeout.
  if ~exist('timeout','var') || isempty(timeout)
    timeout = 30 * 60; % in seconds
  end

  % Fill in coaddopt with defaults if not already set. The tag list needs to
  % be set first or else the cut parameters will be wrong.
  coaddopt.tags = tags;
  coaddopt = get_default_coaddopt(coaddopt);


  %%%% Setup the execution environment %%%%


  % Make sure there's a directory to put the farmfiles into
  system_safe(sprintf('mkdir -p %s', ...
      fullfile('farmfiles',coaddopt.sernum(1:4))));

  %%%% Coadd stages %%%%

  % Use a recursive helper function to do all the hard work of determining if
  % dependencies have been satisfied, if a farmfile already exists on disk,
  % or if the data product has already been produced. This allows us to
  % essentially implement a depth-first traversal of the dependency graph.

  COADDSTAGE_FREQMATRIX = 1;

  % Start the recursion at the top case: fully-coadded matrices to the
  % receiver level.
  try
    [missing,dum] = coadd_recurse(COADDSTAGE_FREQMATRIX, ...
        tags, coaddopt, farmopt, timeout);
  catch ex
    if strcmp(ex.identifier,'matrix:farm_coaddmatrices:Timeout')
      disp('farm_coaddmatrices: Timeout reached.')
      % Need to assign an output to not throw another error.
      missing = true;
    else
      rethrow(ex)
    end
  end
end

function [allmissing,matrix]=coadd_recurse(stage,varargin)
%[allmissing,matrix]=coadd_recurse(stage,varargin)
%
%Recursive function which walks the dependency tree for coadding matrices and
%produces farmfiles as needed/able to.
%
%NOTE that calling into this at an arbitrary stage is likely to fail! There
%is internal state which needs to be setup before everything will work
%correctly. This means the only **supported** entry point is to the
%COADDSTAGE_FREQMATRIX stage.
%
%INPUTS
%  stage       Identifier for the stage of the coadd process (equivalent to
%              a particular depth in the dependency tree graph). Valid
%              options are:
%
%                COADDSTAGE_FREQMATRIX = 1
%                    Coadds multiple receivers into single-frequency matrices.
%
%                COADDSTAGE_RXMATRIX   = 2
%                    Coadds all months together to form the matrix for a
%                    receiver over all tags.
%
%                COADDSTAGE_MONTHRX    = 3
%                    Coadds per-phase matrices into month-long coadds, simply
%                    as a parallelization stage on a per-rx basis.
%
%                COADDSTAGE_PHASERX    = 4
%                    Coadds pair-subset matrices over a phase into a single
%                    matrix spanning the entire receiver for the phase.
%
%                    In the case of a single receiver, this stage will coadd
%                    directly from the pairmatrices by utilizing the
%                    COADDSTAGE_PAIRSUBSET stage internally but with all
%                    pairs selected (and no filename suffix attached).
%
%                COADDSTAGE_PAIRSUBSET = 5
%                    Coadds to pair-subsets from the pairmatrices. See
%                    farm_makepairmatrices for more information on producing
%                    the pairmatrices.
%
%  varargin    Inputs vary depending on the active coadd stage:
%
%                COADDSTAGE_FREQMATRIX
%                  varargin{1} -> tags        All tags to consider
%                  varargin{2} -> coaddopt    Total coaddopt
%                  varargin{3} -> farmopt     General farming options
%                  varargin{4} -> timeout     Recursion timeout to enforce
%
%                COADDSTAGE_RXMATRIX
%                  varargin{1} -> coaddopt    Total coaddopt
%
%                COADDSTAGE_MONTHRX
%                  varargin{1} -> month       Single month to coadd for an rx
%                  varargin{2} -> tags        Month's corresponding tags
%                  varargin{3} -> coaddopt    Options for this month and rx
%
%                COADDSTAGE_PHASERX
%                  varargin{1} -> phase       Single phase to coadd for an rx
%                  varargin{2} -> tags        Phase's corresponding tags
%                  varargin{3} -> coaddopt    Options for this phase and rx
%
%                COADDSTAGE_PAIRMATRICES
%                  varargin{1} -> phase       Single phase to coadd
%                  varargin{2} -> tags        Phase's corresponding tags
%                  varargin{3} -> coaddopt    Options for this phase and pair
%                                             subset.
%                  varargin{4} -> coaddstyle  String to differentiate the
%                                             pair-subset coadds
%                                             ('coadd_pairsubset') from the
%                                             pairmatrix coadds
%                                             ('coadd_phaserx').
%
%OUTPUTS
%  allmissing    The total list of all dependencies for the current stage and
%                all children stages. Note that some files may be repeated due
%                to a dependency of a single input on multiple outputs (e.g.
%                for the pair-subset coadds depending on pairmatrices).
%
%  matrix        The matrix file(s) the stage is responsible for producing.
%


  COADDSTAGE_FREQMATRIX = 1;
  COADDSTAGE_RXMATRIX   = 2;
  COADDSTAGE_MONTHRX    = 3;
  COADDSTAGE_PHASERX    = 4;
  COADDSTAGE_PAIRSUBSET = 5;

  % Keep a few things in persistent memory for convenience
  persistent origdaughter;
  persistent farmopt;
  persistent months;
  persistent p;
  persistent ind;
  persistent rxs
  persistent ismultirx;
  persistent pairsubsets;
  persistent timeout;
  persistent tichandle;

  allmissing = {};
  matrix     = {};

  % It's assumed that if one persistent variable is initialized (non-empty),
  % then all have been initialized.
  if stage == COADDSTAGE_FREQMATRIX
    % Setup the persistent contents first
    tags     = varargin{1};
    farmopt  = varargin{3};
    timeout  = varargin{4};
    if isfield(varargin{2}, 'daughter')
      origdaughter = varargin{2}.daughter;
    else
      origdaughter = [];
    end

    % Setup the phase and month lists since they'll be needed later anyway
    phases = taglist_to_phaselist(varargin{1});
    months = phase2month(phases);

    % Decide whether there are multiple receivers and get the unique listing
    [p,ind] = get_array_info(varargin{1}{1});
    rxs = unique(p.rx);
    ismultirx = (length(rxs)~=1);

    % Generate the pair subset list just once since that should be common
    % across all phases. Do this carefully so that a pair subset group never
    % belongs to two different receivers.
    if farmopt.split > 0
      % Create per-rx pair-subset lists
      for rx=rxs'
        pairsubsets.(['rx' num2str(rx)]) = {};
      end

      rxlist = p.rx(ind.a);
      ii = 0;
      % Consume the detector and rx list until all elements are claimed
      while ii < length(ind.a)
        % Assume farmopt.split does not span bigger than an entire receiver and
        % claim any tail elements to a single group if there are less than the
        % split size remaining elements
        if length(ind.a)-ii <= farmopt.split
          endidx = length(ind.a);

        % If the span is over the same rx, then do nothing fancy.
        elseif rxlist(ii+1) == rxlist(ii+farmopt.split)
          endidx = ii + farmopt.split;

        % Otherwise, do a bit more more computational work to find the end point
        else
          endidx = ii + find(rxlist(ii+1)==rxlist(ii:(ii+farmopt.split)),1,'last');
        end

        % Save the detector subset list. The pair numbers are in terms of
        % indices into the ind.a list.
        field = sprintf('rx%1d', p.rx(ind.a(ii+1)));
        pairsubsets.(field){end+1} = (ii+1):endidx;

        % Update counter
        ii = endidx;
      end

      clear rxlist endidx
    end

    % Start a timeout counter
    tichandle = tic();
  end

  % Check for timeout at the beginning of each invocation.
  runtime = toc(tichandle);
  if timeout~=0 && runtime >= timeout
    % Need to abort due to a timeout being enforced
    throw(MException('matrix:farm_coaddmatrices:Timeout', ...
        sprintf('Timeout of %d minutes reached.', timeout/60)))
  end

  switch stage


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case COADDSTAGE_FREQMATRIX
      % The two commented arguments are already set when initializing the
      % persistent variables above.

      % tags     = varargin{1};
      coaddopt = varargin{2};
      % farmopt  = varargin{3};

      % Most of the work is done by the COADDSTAGE_RXMATRIX stage.
      [allmissing,rxmatrices] = coadd_recurse(COADDSTAGE_RXMATRIX, coaddopt);

      % Only do more processing if there are multiple receivers to combine
      % into a single file.
      if ismultirx
        % Find the set of unique frequencies, using only light detector
        % indices since darks can be labeled as 0GHz.
        freqs = unique(p.band(ind.la));
        % Now map frequencies to receivers.
        rxfreq = arrayfun( @(rx) unique(p.band(p.rx==rx & p.band~=0 ...
            & ~isnan(p.band))), rxs);

        % The ukpervolt can be specified singly or for each receiver. Handle
        % both cases.
        allukpv = get_ukpervolt(tags{1});
        % Checking for 1 is enough since ismultirx==true iff length(rxs)>1
        if length(allukpv)==1
          % Replicate to same size as the receiver array
          allukpv = repmat(allukpv, size(rxs));
        elseif length(allukpv)~=length(rxs)
          error('Incompatible ukpervolt and number of receivers.')
        end
        
        matrix = cell(size(freqs));

        % Generate each frequency coadd if necessary
        for ii=1:length(freqs)
          freq = freqs(ii);
          % frx are the receivers at this frequency
          frx = rxs(rxfreq == freq);

          matrixdeps = rxmatrices(1+frx);

          % If this frequency's matrix already exists, skip rest of loop
          freqcoaddopt = coaddopt;
          freqcoaddopt.daughter = sprintf('%dGHz', freq);
          if ~isempty(origdaughter)
            freqcoaddopt.daughter = [origdaughter '_' freqcoaddopt.daughter];
          end

          matrix{ii} = get_matrix_filename(freqcoaddopt);
          if exist_file(matrix{ii})
            fprintf(1,'Matrix %s already exists. Done.\n', matrix{ii})
            continue
          end
          % Past here, we know the matrix is still missing so add it to the
          % list.
          allmissing = [allmissing matrix(ii)];

          % Generate unique identifier for the job
          [matdir,matfile] = fileparts(matrix{ii});
          [farmfile,jobname] = farmfilejobname(...
              freqcoaddopt.sernum, 'coadd_freq', matfile);

          % Can't continue if all of the per-rx files don't exist
          if ~all(cellfun(@exist_file, matrixdeps))
            continue
          end

          % Finally, everything is in order to produce a frequency-coadded
          % matrix.
          if farmopt.dofarm
            sernum = freqcoaddopt.sernum;
            daughter = freqcoaddopt.daughter;

            % Calibrate each receiver into temperature units for the final
            % coadd.
            ukpv = allukpv(1+frx);
            % Finally, farm out a job!
            farmit(farmfile, ...
                ['reduc_coadd_coadds(matrixdeps,sernum,daughter,false,ukpv)'], ...
                'jobname',jobname, ...
                'maxtime',farmopt.maxtime(COADDSTAGE_FREQMATRIX), ...
                'mem',farmopt.mem(COADDSTAGE_FREQMATRIX), ...
                'queue',farmopt.queue, ...
                'submit',farmopt.submit, ...
                'overwrite','skip', ...
                'var',{'matrixdeps','sernum','daughter','ukpv'});
          else
            reduc_coadd_coadds(matrixdeps, sernum, daughter, false, ukpv);
          end

        end
      end

      % When this stage complete, reset the persistent variables so that a
      % subsequent call will recalculate them all from scratch.
      months = [];
      farmopt = [];
      p = [];
      ind = [];
      rxs = [];
      ismultirx = [];
      pairsubsets = [];
      timeout = [];
      tichandle = [];



    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case COADDSTAGE_RXMATRIX
      coaddopt = varargin{1};

      % If there are multiple rx's, we want to coadd over just a single rx at
      % a time.
      if ismultirx
        coaddopts = repmat(coaddopt, size(rxs));
        for ii=1:length(rxs)
          coaddopts(ii).rx = num2str(rxs(ii));
        end

      % Otherwise just alias the coaddopt into the "array" for the next bit
      else
        coaddopts(1) = coaddopt;
      end

      % Check presence of matrix for each coaddopt
      matrix = cell(size(coaddopts));
      for ii=1:length(coaddopts)
        matrixdeps = {};

        % Skip everything else if the final matrix already exists.
        matrix{ii} = get_matrix_filename(coaddopts(ii));
        if exist_file(matrix{ii})
          if ~ismultirx
            fprintf(1,'Matrix %s already exists. Done.\n', matrix{ii})
          end
          continue;
        else
          % Otherwise make sure the matrix is added to the dependency list
          allmissing = [allmissing matrix(ii)];
        end
        
        % We can bail out really early if we already know that a farmfile has
        % been produced for this job.
        [matdir,matfile] = fileparts(matrix{ii});
        [farmfile,jobname] = farmfilejobname(...
            coaddopts(ii).sernum, 'coadd_wholerx', matfile);

        % Then skip any further processing if we've already laid down a
        % farmfile for this job. Do this check explicitly since checking here
        % is less costly than recursing and checking the dependency tree.
        if exist_file(farmfile)
          continue
        end

        allexist = true;
        % Check to see if each per-month rx coadd has been completed.
        for mo=1:length(months.name)
          % Recursively call the next stage to check for existence and
          % any missing data products
          [newmissing,newdep] = coadd_recurse(COADDSTAGE_MONTHRX, ...
              months.name{mo}, months.tags{mo}, coaddopts(ii));

          matrixdeps = [matrixdeps {newdep}];

          % If there's a missing list returned, then set the all-OK flag
          % to false and append to the list of missing data products.
          if ~isempty(newmissing)
            allexist = false;
            allmissing = [allmissing newmissing];
          end
        end % months

        % If everything we need exists, then we can just farm out the coadd
        % job.
        if allexist
          sernum = coaddopts(ii).sernum;
          daughter = coaddopts(ii).daughter;

          if farmopt.dofarm
            % Finally, farm out a job!
            farmit(farmfile, ...
                ['reduc_coadd_coadds(matrixdeps,sernum,daughter)'], ...
                'jobname',jobname, ...
                'maxtime',farmopt.maxtime(COADDSTAGE_RXMATRIX), ...
                'mem',farmopt.mem(COADDSTAGE_RXMATRIX), ...
                'queue',farmopt.queue, ...
                'submit',farmopt.submit, ...
                'var',{'matrixdeps','sernum','daughter'});
          else
            reduc_coadd_coadds(matrixdeps, sernum, daughter);
          end
        end

      end % coaddopts



    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case COADDSTAGE_MONTHRX
      month    = varargin{1};
      tags     = varargin{2};
      coaddopt = varargin{3};

      % First use the phase name
      coaddopt.daughter = month;
      % Then append the original coaddopt.daughter if there was one
      if ~isempty(origdaughter)
        coaddopt.daughter = [coaddopt.daughter '_' origdaughter];
      end

      % If the matrix has already been generated, bail out now.
      matrix = get_matrix_filename(coaddopt);
      % We can bail out really early if we already know that a farmfile has
      % been produced for this job.
      [matdir,matfile] = fileparts(matrix);
      [farmfile,jobname] = farmfilejobname(...
          coaddopt.sernum, 'coadd_monthrx', matfile);

      if exist_file(matrix)
        return
      % Otherwise, add ourself to the missing list
      else
        allmissing = [allmissing {matrix}];

        % Then skip any further processing if we've already laid down a
        % farmfile for this job. Do this check explicitly since checking here
        % is less costly than recursing and checking the dependency tree.
        if exist_file(farmfile)
          return
        end
      end


      matrixdeps = {};
      allexist = true;

      % If the per-month, per-rx matrix doesn't exist yet, then check for the
      % presence of its dependencies.
      ph = taglist_to_phaselist(tags);
      for ii=1:length(ph.name)

        [newmissing,newdep] = coadd_recurse(COADDSTAGE_PHASERX, ...
            ph.name{ii}, ph.tags{ii}, coaddopt);

        % Keep the dependency list
        matrixdeps = [matrixdeps {newdep}];

        % Flag missing dependencies
        if ~isempty(newmissing)
          allmissing = [allmissing newmissing];
          allexist = false;
        end
      end

      % If we have all pair-subset coadds we need, then coadd over coadds.
      if allexist
        sernum = coaddopt.sernum;
        daughter = coaddopt.daughter;

        if farmopt.dofarm
          % Finally, farm out a job!
          farmit(farmfile, ...
              ['reduc_coadd_coadds(matrixdeps,sernum,daughter,true)'], ...
              'jobname',jobname, ...
              'maxtime',farmopt.maxtime(COADDSTAGE_MONTHRX), ...
              'mem',farmopt.mem(COADDSTAGE_MONTHRX), ...
              'queue',farmopt.queue, ...
              'submit',farmopt.submit, ...
              'var',{'matrixdeps','sernum','daughter'});
        else
          reduc_coadd_coadds(matrixdeps, sernum, daughter);
        end
      end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case COADDSTAGE_PHASERX
      phase    = varargin{1};
      tags     = varargin{2};
      coaddopt = varargin{3};

      % First use the phase name
      coaddopt.daughter = phase;
      % Then append the original coaddopt.daughter if there was one
      if ~isempty(origdaughter)
        coaddopt.daughter = [coaddopt.daughter '_' origdaughter];
      end

      % If the matrix has already been generated, bail out now.
      matrix = get_matrix_filename(coaddopt);
      % We can bail out really early if we already know that a farmfile has
      % been produced for this job.
      [matdir,matfile] = fileparts(matrix);
      [farmfile,jobname] = farmfilejobname(...
          coaddopt.sernum, 'coadd_phaserx', matfile);

      if exist_file(matrix)
        return
      % Otherwise, add ourself to the missing list
      else
        allmissing = [allmissing {matrix}];

        % Then skip any further processing if we've already laid down a
        % farmfile for this job. Do this check explicitly since checking here
        % is less costly than recursing and checking the dependency tree.
        if exist_file(farmfile)
          return
        end
      end


      matrixdeps = {};
      allexist = true;

      % If the per-phase, per-rx matrix doesn't exist yet, then check for the
      % presence of its dependencies. Either we need to coadd from per-phase,
      % pair-subset coadds or directly from the pair matrices.
      if farmopt.split > 0

        % Handle both the multirx
        if isfield(coaddopt,'rx')
          rxname = ['rx' coaddopt.rx];
        else
          rxname = 'rx0';
        end

        % In the case of splits, check for all the pair-subset coadds.
        for ii=1:length(pairsubsets.(rxname))

          subsetcoaddopt = coaddopt;
          % Choose the subsets
          subsetcoaddopt.pair_subset = pairsubsets.(rxname){ii};

          [newmissing,newdep] = coadd_recurse(COADDSTAGE_PAIRSUBSET, ...
              phase, tags, subsetcoaddopt, 'coadd_pairsubset');

          % Keep the dependency list
          matrixdeps = [matrixdeps {newdep}];

          % Flag missing dependencies
          if ~isempty(newmissing)
            allmissing = [allmissing newmissing];
            allexist = false;
          end
        end

        % If we have all pair-subset coadds we need, then coadd over coadds.
        if allexist
          sernum = coaddopt.sernum;
          daughter = coaddopt.daughter;

          if farmopt.dofarm
            % Finally, farm out a job!
            farmit(farmfile, ...
                ['reduc_coadd_coadds(matrixdeps,sernum,daughter,true)'], ...
                'jobname',jobname, ...
                'maxtime',farmopt.maxtime(COADDSTAGE_PHASERX), ...
                'mem',farmopt.mem(COADDSTAGE_PHASERX), ...
                'queue',farmopt.queue, ...
                'submit',farmopt.submit, ...
                'var',{'matrixdeps','sernum','daughter'});
          else
            reduc_coadd_coadds(matrixdeps, sernum, daughter);
          end
        end

      % Otherwise, coadd directly from pairmatrices. Reuse the pair-subset
      % coadd stage since it does nothing that knows about the pair-subsets
      % and can just blindly coadd a whole phase.
      else
        % Add this warning since I *think* this will break as everything is
        % arranged right now. The use of per-rx pair-subset lists and keeping
        % coaddopt.rx set (as a way to get reduc_coadd_coadds() to know about
        % the per-rx coadds at all later stages) will probably not work in
        % this case. Because it's doubtful this code path will ever be
        % exercised since it's infeasibly computationally expensive, it's
        % probably a reasonable solution.
        if ismultirx
          disp('This is an untested code path, and the multi-level coadd stages might not work properly.')
          choice = input('Continue? [y/n] ', 's');
          if ~strcmp(choice, 'y')
            error('Aborting.')
          end
        end

        % coaddstyle 'coadd_phaserx' is chosen to match this stage's
        % identifier since they produce the same level of output.
        [newmissing,newdep] = coadd_recurse(COADDSTAGE_PAIRSUBSET, ...
            phase, tags, coaddopt, 'coadd_phaserx');

        if ~isempty(newmissing)
          allmissing = [allmissing newmissing];
        end
      end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case COADDSTAGE_PAIRSUBSET
      phase      = varargin{1};
      tags       = varargin{2};
      coaddopt   = varargin{3};
      coaddstyle = varargin{4};

      % Don't modify the original daughter like at every other stage because
      % the pair-subset coadds explicitly include a pairsXXXXtoXXXX suffix
      % that is given by get_matrix_filename().

      % If the matrix has already been generated, bail out now.
      matrix = get_matrix_filename(coaddopt);
      % We can bail out really early if we already know that a farmfile has
      % been produced for this job.
      [matdir,matfile] = fileparts(matrix);
      [farmfile,jobname] = farmfilejobname(...
          coaddopt.sernum, coaddstyle, matfile);

      if exist_file(matrix)
        return  
      % Otherwise, add ourselves to the missing list
      else
        allmissing = [allmissing {matrix}];

        % Then skip any further processing if we've already laid down a
        % farmfile for this job. Do this check explicitly since checking here
        % is less costly than checking the dependency tree.
        if exist_file(farmfile)
          return
        end
      end

      allexist = true;

      % Pair-subset coadds come from coadding subsets of pixels over all
      % tags within a single phase. Make sure the pairmatrices exist that
      % we need.
      pairmatrix = cell(size(tags));
      for ii=1:length(tags)
        % Get the file name, making sure to make an appropriate substitution
        % if we're doing a reweighted coadd with a tag flavor substitution
        % list.
        if isfield(coaddopt, 'tagsublist') && ~isempty(coaddopt.tagsublist)
          % Lookup which tag we are in sublist (normally tags and
          % coaddopt.tagsublist will be the same length but we might want
          % the latter to be a superset of the former so do it this way
          % for greater generality)
          thistag=find(strncmp(tags{ii},coaddopt.tagsublist(1,:),17), 1);

          pairtag = coaddopt.tagsublist{2,thistag};
          pairopt = setfield(coaddopt, 'weight', 0);
        else
          pairtag = tags{ii};
          pairopt = coaddopt;
        end
        pairmatrix{ii} = get_pairmatrix_filename(pairtag, pairopt);

        % Check for existence on disk
        if ~exist_file(pairmatrix{ii})
          allexist = false;
          % Add to the missing list if necessary
          allmissing = [allmissing pairmatrix(ii)];
        end
      end

      % If we have all the pairmatrices we need, coadd over them
      if allexist
        if farmopt.dofarm
          % Finally, farm out a job!
          farmit(farmfile, ...
              'reduc_coaddmatrices(tags,coaddopt)', ...
              'jobname',jobname, ...
              'maxtime',farmopt.maxtime(COADDSTAGE_PAIRSUBSET), ...
              'mem',farmopt.mem(COADDSTAGE_PAIRSUBSET), ...
              'queue',farmopt.queue, ...
              'submit',farmopt.submit, ...
              'var',{'tags','coaddopt'});
        else
          reduc_coaddmatrices(tags, coaddopt);
        end
      end

    % End cases
  end

end
