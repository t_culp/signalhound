function matrix_ct_driver(dset)
% matrix_ct_driver(dset)
%
% Drives the processes to create reobserved covariances and solve the
% generalized eigenvalue problem to create a purification matrix.
%
% INPUTS
%     dset    Data set ('BK14', 'BK15', etc) to make covariances for.
%

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % GENERAL

  % Each reobserving matrix needs to know which beam and map to use. The
  % reobservations and covariance matrices which are built depend on the
  % number of entries which exist in this structure array.
  %
  % .file  = Path to matrix to use during reobservation. Must be finalized.
  % .field = Fields to reobserve ('TT','QU', etc).
  % .beam  = Beam to apply to spectra. Will be copied to covmatopt.beam and
  %          removed from covmatopt.reob when creating job descriptions.
  % .map   = Map pixelization to use. Will be copied to covmatopt.map and
  %          removed from covmatopt.reob when creating job descriptions.
  reobs = struct();

  switch dset
    case 'BK14'
      reobs(1).file = 'matrixdata/matrices/1351/real_d_100GHz_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat';
      reobs(1).beam = 'Kuber100rev1';
      reobs(1).map  = 'input_maps/camb_planck2013_r0/map_unlens_n0512_r0001_sKuber100_cPl100_dPl100.fits';

      reobs(2).file = 'matrixdata/matrices/1459/real_aabd_150GHz_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat';
      reobs(2).beam = 'B2bbns';
      reobs(2).map  = 'input_maps/camb_planck2013_r0/map_unlens_n0512_r0001_sB2bbns_cPl143_dNoNoi.fits';

    case 'BK15'
      reobs(1).file = 'matrixdata/matrices/1459/real_aabde_100GHz_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat';
      reobs(1).beam = 'Kuber100rev1';
      reobs(1).map  = 'input_maps/camb_planck2013_r0/map_unlens_n0512_r0001_sKuber100_cPl100_dPl100.fits';

      reobs(2).file = 'matrixdata/matrices/1459/real_aabde_150GHz_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat';
      reobs(2).beam = 'B2bbns';
      reobs(2).map  = 'input_maps/camb_planck2013_r0/map_unlens_n0512_r0001_sB2bbns_cPl143_dNoNoi.fits';
      
      reobs(3).file = 'matrixdata/matrices/1459/real_aabde_220GHz_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat';
      reobs(3).beam = 'Kuber220';
      reobs(3).map  = 'input_maps/camb_planck2013_r0/map_unlens_n0512_r0001_sKuber220_cPl217_dPl217.fits';
  end

  % Settings which are common to all reobservations can be set simultaneously
  [reobs(:).field] = deal({'QQ','QU','UU'});

  % Spectra to reobserve. See settings below.
  specs = {};

  covmatopt.sernum = '1459real';
  covmatopt.lmax = 700;
  covmatopt.subsets = true;

  farmopt.dofarm = true;
  farmopt.submit = false;

  % To produce a purification matrix, the first two red specta are required.
  % The latter two Planck spectra are only used for ...
  specs{end+1} = 'aux_data/official_cl/red_spectrum_EnoB.fits';
  specs{end+1} = 'aux_data/official_cl/red_spectrum_BnoE.fits';
  %specs{end+1} = 'aux_data/official_cl/camb_planck2013_r0.fits';
  %specs{end+1} = 'aux_data/official_cl/camb_planck2013_r0p1_noE.fits';

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  % Operate on each requested reobservation
  for rr=1:length(reobs)
    covmatopt.reob = rmfield(reobs(rr), {'beam','map'});
    covmatopt.beam = reobs(rr).beam;
    covmatopt.map = reobs(rr).map;

    % First, make a reobserved covariance matrix for each spectrum.
    for ii=1:length(specs)
      covmatopt.spec = specs{ii};

      missingcov{ii} = farm_covmattheory(covmatopt, farmopt);
    end

    % The eigenvalue problem should only be solved for the EnoB and BnoE
    % spectra, so identify if those exist.
    Espec = find(cellfun(@(s) ~isempty(strfind(s,'_EnoB')), specs));
    Bspec = find(cellfun(@(s) ~isempty(strfind(s,'_BnoE')), specs));
    % Only proceed if there are no missing covariances for these spectra
    if isempty(missingcov{Espec}) && isempty(missingcov{Bspec})
      Ename = get_ct_filename(setfield(covmatopt, 'spec',specs{Espec}));
      Bname = get_ct_filename(setfield(covmatopt, 'spec',specs{Bspec}));
      eigname = farm_makeeigen(farmopt, covmatopt, Ename, Bname);

      % Since we have an eigenvalue problem, we can also make sure that the
      % corresponding projection matrices have also been produced.
      if exist_file(eigname)
        projname = farm_makeprojection(farmopt, covmatopt, eigname);
      end
    end

    %-----------%

    %Make cholesky decomposition from E and B spectra
    if(0)
      fname=get_ct_name(map,specs{1},lmax, beam, [], [], reob);
      cov2chol_driver(fname, do_farm)
      fname=get_ct_name(map,specs{2},lmax, beam, [], [], reob);
      cov2chol_driver(fname, do_farm)
    end

    %make sims from a chol decomp cov
    if(0)
     fname=get_ct_name(map,specs{1},lmax, beam, [], [], reob);
     fname=[fname '_chol'];
     chol2sim_driver(fname,[1:100], do_farm)
     fname=get_ct_name(map,specs{2},lmax, beam, [], [], reob);
     fname=[fname '_chol'];
     chol2sim_driver(fname,[1:100], do_farm)
    end
  end

  %-----------%

  return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function combine_ct_driver(map, spec, lmax, beam, reob, do_farm)

if do_farm

  switch do_farm
    case 'general'
      QUEUE='general';
      maxtime=1440; %minutes
    case 'serial_requeue'
      QUEUE='serial_requeue';
      maxtime=1440; %minutes
    otherwise
     error('choose recommended queue (general, serial_requeue or normal_serial), or set do_farm=false')
  end

  mem=40000;

  farmdir=farmdir();

  farmit(farmdir,'combine_ct(map, spec, lmax, beam, reob);', 'queue',QUEUE, 'mem',mem, 'maxtime',maxtime, 'var',{'map','spec','lmax','beam','reob'});
else
  combine_ct(map, spec, lmax, beam, reob);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function cov2chol_driver(fname, do_farm)

if do_farm

  switch do_farm
    case 'general'
      QUEUE='general';
      maxtime=4320; %minutes
    case 'serial_requeue'
      QUEUE='serial_requeue';
      maxtime=1440; %minutes
    otherwise
     error('choose recommended queue (general or serial_requeue), or set do_farm=false')
  end

  mem=80000;

  farmit(farmdir(),'cov2chol(fname);', 'queue',QUEUE, 'mem',mem, 'maxtime',maxtime, 'var',{'fname'});
else
  cov2chol(fname);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function fname=farm_makeeigen(farmopt, covmatopt, Ename, Bname)

  if ~isfield(farmopt, 'queue')
    farmopt.queue = 'general';
  end
  if ~isfield(farmopt, 'mem')
    farmopt.mem = 80000;
  end
  if ~isfield(farmopt, 'maxtime')
    farmopt.maxtime = 2 * 24 * 60; % 2 days in minutes
  end
  if ~isfield(farmopt, 'submit')
    farmopt.submit = false;
  end
  if ~isfield(farmopt, 'dofarm')
    farmopt.dofarm = true;
  end

  % Reproduce the naming scheme used in make_eigen(). This should probably
  % eventually be made to live in get_ct_filename() somehow.
  fname=strtok(Ename, '.');
  if strfind(fname, 'E')
    root=[fname(1:strfind(fname, 'E')-2)];
    leaf=[fname(strfind(fname, 'E')+4:end)];
    fname=[root leaf '_eigen.mat'];
  else
    fname=[fname '_eigen.mat'];
  end

  if exist_file(fname)
    fprintf(1,'Done. Eigensystem matrix already exists: %s\n', fname);
    return
  end

  [dum,ename,dum] = fileparts(fname);
  [farmfile,jobname] = farmfilejobname(covmatopt.sernum, ename);

  if farmopt.dofarm
    farmit(farmfile, 'make_eigen(Ename,Bname);', ...
      'jobname', jobname, ...
      'queue', farmopt.queue, ...
      'mem', farmopt.mem, ...
      'maxtime', farmopt.maxtime, ...
      'submit', farmopt.submit, ...
      'overwrite', 'skip', ...
      'var',{'Ename','Bname'});
  else
    make_eigen(Ename, Bname);
  end

  return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function fname=farm_makeprojection(farmopt, covmatopt, eigname)

  if ~isfield(farmopt, 'queue')
    farmopt.queue = 'general';
  end
  if ~isfield(farmopt, 'mem')
    farmopt.mem = 60000;
  end
  if ~isfield(farmopt, 'maxtime')
    farmopt.maxtime = 1 * 24 * 60; % 1 day in minutes
  end
  if ~isfield(farmopt, 'submit')
    farmopt.submit = false;
  end
  if ~isfield(farmopt, 'dofarm')
    farmopt.dofarm = true;
  end

  % Reproduce the naming scheme used in make_projection(). This should
  % probably eventually be made to live in get_ct_filename() somehow.
  fname=strtok(eigname, '.');
  if strfind(fname, '_eigen')
    fname=fname(1:strfind(fname, '_eigen'));
  end
  fname=[fname 'proj.mat'];

  if exist_file(fname)
    fprintf(1,'Done. Projection matrix already exist: %s\n', fname);
    return
  end

  [dum,projname,dum] = fileparts(fname);
  [farmfile,jobname] = farmfilejobname(covmatopt.sernum, projname);

  if farmopt.dofarm
    farmit(farmfile, 'make_projection(eigname);', ...
      'jobname', jobname, ...
      'queue', farmopt.queue, ...
      'mem', farmopt.mem, ...
      'maxtime', farmopt.maxtime, ...
      'submit', farmopt.submit, ...
      'overwrite', 'skip', ...
      'var',{'eigname'});
  else
    make_projection(eigname);
  end

  return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function chol2sim_driver(fname, sims, do_farm)

if do_farm

  switch do_farm
    case 'general'
      QUEUE='general';
    case 'serial_requeue'
      QUEUE='serial_requeue';
    otherwise
     error('choose recommended queue (general, serial_requeue), or set do_farm=false')
  end

  mem=25000;

  %maximum time it should reasonably take
  maxtime=120; %minutes

  farmdir=farmdir();

  farmit(farmdir,'chol2sim(fname,sims);', 'queue',QUEUE, 'mem',mem, 'maxtime',maxtime, 'var',{'fname', 'sims'});
else
  chol2sim(fname, sims);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function farmdir=farmdir()

farmdir='farmfiles/c_t';

if(~exist(farmdir, 'dir'))
  system(['mkdir ' farmdir]);
end

return
