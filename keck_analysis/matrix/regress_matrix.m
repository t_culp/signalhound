function [ac,matrix]=regress_matrix(ac,coaddopt,m)

%eg
  % [ff,matrix]=regress_matrix(ac(1),coaddopt,m)
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Expand out the modes we are deprojecting
mapdp=coaddopt.mapopt{1}.deproj;
if isscalar(mapdp)
  % Standard modes of a gaussian
  dp=[coaddopt.deproj(1), ... % relgain
      coaddopt.deproj(2), ... % diff. point x
      coaddopt.deproj(2), ... % diff. point y
      coaddopt.deproj(3), ... % beamwidth
      coaddopt.deproj(4), ... % ellip. plus
      coaddopt.deproj(4)];    % ellip. cross
else
  % arbitrarily specified templates..probably doesn't work

  % Check to see that the number of modes specified in coaddopt matches the number of
  % modes accumulated in reduc_makepairmaps
  if iscell(mapdp)
    ntemp=0;
    for k=1:numel(mapdp)
      ntemp=ntemp+numel(mapdp{k});
    end
  else
    ntemp=numel(mapdp);
  end
  if ntemp~=numel(coaddopt.deproj)
    error(['the number of elements of coaddopt.deproj must equal the ' ...
           'number of templates accumulated at pairmap stage and ' ...
           'specified in mapopt.deproj']);
  end

  dp=coaddopt.deproj;
end

ntemplates=numel(dp);
nfit=sum(dp);

%for matrix, use map2vect...
[m,wcz]=map2vect(m,ac.wcz);
wcz=sparse(wcz);
[m,wsz]=map2vect(m,ac.wsz);
wsz=sparse(wsz);
[m,wcc]=map2vect(m,ac.wcc);
wcc=sparse(wcc);
[m,wss]=map2vect(m,ac.wss);
wss=sparse(wss);

for i=1:length(ac.wcd)
  [m,wcd{i}]=map2vect(m,ac.wcd{i});
  wcd{i}=sparse(wcd{i});
  [m,wsd{i}]=map2vect(m,ac.wsd{i});
  wsd{i}=sparse(wsd{i});
end

% reduce maps to vectors of non-zero pixels; make sure to use only wcc or
% wss for counting since real data zeros (or NaNs) in the original
% representative tag are wrong for the current coadd when doing
% tag-substitution reweighted coadds.
pixind=find(wcc~=0 & isfinite(wcc))'; %'
mlength=length(wcc);

if isempty(pixind)
  matrix.deproj=sparse(1:mlength*2,1:mlength*2, zeros(mlength*2,1));
  return
end

% pre-allocate regressor for memory efficiency
X=zeros(numel(pixind)*2,nfit);

% Construct regressor
xi=1;
for i=1:numel(dp)
  if dp(i)
    X0=[wcd{i}(pixind), wsd{i}(pixind)];
    X(:,xi)=X0;
    xi=xi+1;
  end
end

% Weighted linear regression%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

v=[wcc(pixind)';wss(pixind)'];
w=1./v;

%matrix version of Weighted linear regression
wm=sparse(1:length(w),1:length(w),w);
dm=inv(X'*wm*X)*X'*wm;

%make a sparse Identity matrix filled for only obs pixelswh [c,s].
filled=[pixind; pixind+length(wcc)];
iden=sparse(filled,filled, ones(length(filled),1), mlength*2, mlength*2);
matrix.deproj=iden;

%construct matrix for both c and s
% scale each deprojection mode by the regression coefficient and
% subtract
%Deprojection=(I-Template*dm)*ac
ii=1;
for i=1:numel(dp)
  if dp(i)
    deprom=sparse(ones(1,length(pixind)*2),[pixind,pixind+mlength],dm(ii,:), 1,mlength*2);
    matrix.dwd{i}=[wcd{i}, wsd{i}]'*deprom; %'
    matrix.deproj=matrix.deproj-matrix.dwd{i};
    ii=ii+1;
  end
end

%input pairmap as a vector [ac.wcz, ac.wsz]
vac=[wcz, wsz]'; %'

%operate on pairmaps (ac)
vac=matrix.deproj*vac;
[m,ac.wcz]=vect2map(m, vac(1:end/2));
[m,ac.wsz]=vect2map(m, vac(end/2+1:end));

return

