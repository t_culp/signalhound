function covmatopt=get_default_covmatopt(covmatopt)
% covmatopt=get_default_covmatopt(covmatopt)
%
% Creates/fills structure with default covmatopt options.
%
% INPUTS
%   covmatopt    A covmatopt strcture. If empty, it is created.
%
% OUTPUTS
%   covmatopt    Structure with default options filled.
%

  if ~exist('covmatopt','var') || isempty(covmatopt)
    covmatopt = struct();
  end

  if ~isfield(covmatopt,'sernum') || isempty(covmatopt.sernum)
    error('get_default_covmatopt:DefaultSernum', ...
        'sernum must be provided')
  end

  if ~isfield(covmatopt,'map')
    covmatopt.map = 'bicep';
  end

  if ~isfield(covmatopt,'spec')
    covmatopt.spec = 'aux_data/official_cl/camb_planck2013_r0.fits';
  end

  if ~isfield(covmatopt,'lmax')
    covmatopt.lmax = 700;
  end

  if ~isfield(covmatopt,'beam')
    covmatopt.beam = [];
  end

  if ~isfield(covmatopt,'reob')
    covmatopt.reob = false;
  end

  if ~isfield(covmatopt,'subsets')
    covmatopt.subsets = false;
  end
end
