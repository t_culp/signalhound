function cov2chol(fname)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Creates a cholesky decomposition based on a covariance matrix
%
% INPUTS   : 
%  fname   : name of cov mat file (from combine_ct.m)
%
%
% Note: You'll probably need to farm this out for 'bicep' resolution
% cov matrices (see ct_driver.m)
%
% eg
%fname='matrixdata/c_t/0704/healpix_red_spectrum_EnoB_lmax700_beamB2bbns_reob0704.mat'
%cov2chol(fname)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%trim off .mat
fname=strtok(fname, '.');

%check to see if we've already made chol decomp
if(exist([fname '_chol.mat']))
  disp([fname '_chol.mat already exists, skipping']) 
else
  disp(['loading ' fname])
  load([fname '.mat']) % loads 'rcr', 'reob', 'm'

  %make rcr symmetric
  %(they are slightly non-symettric from numerical precision)
  issym=@(x) all(all(tril(x)==triu(x).'));


  if (~issym(rcr))
    mdif=max(max((rcr-rcr')./rcr));
    disp('RCR is not symmteric, maximumally non symetric element is')
    disp(['fractionally ' num2str(mdif) ' from sym...'])
    disp('forcing symmetric by averaging across diag')
    rcr=(rcr+rcr')./2;
  end


  %find where the actual map really is
  % we don't want to waste memory on empty rows of the matrix
  ss=sum(rcr,2);
  not_zero=ss~=0;
  obs_pixels=find(not_zero);

  %take only observed pixels
  rcr=rcr(obs_pixels,obs_pixels);

  %find chol
  tic;
  [CC, p]=chol(rcr);
  if p==0 %p is pos definite

    toc
    %save
    %kuldge if m is not variable
    if ~exist('m', 'var')
      load(reob.file, 'm')
    end
    saveandtest([fname '_chol'], 'CC', 'obs_pixels', 'reob', 'm')
    setpermissions([fname '_chol.mat']);

  else  %C is not pos def...remove neg eigen vals to make it

    toc

    %find eigenvalues
    tic
    display('rcr not pos def, computing eigenvalues, removing negatives')
    display('computing eigenvals...')
    [vects, vals]=eig(rcr);
    toc

    %remove neg eigenvals & vects
    val=diag(vals);
    disp(['removing ' num2str(numel(find(val<=0))) ' negative eigenvalues'])
    CC=vects(:,find(val>0))';
    nm=sqrt(val(find(val>0)));
    CC=bsxfun(@times,CC,nm);

    %replace neg eigen val with min pos
    %valsn=diag(vals);val=valsn;
    %disp(['replacing ' num2str(numel(find(valsn<=0))) ' negative eigenvalues'])
    %valsn(find(valsn<=0))=min(valsn(find(valsn>0)));
    %valsnm=sparse(1:length(vects), 1:length(vects), valsn);

    %reform C
    %Cr=vects*valsnm*vects';

    %do chol
    %tic
    %disp('finding cholesky decomp')
    %CC=cholcov(Cr);
    %toc

    %save
    %kuldge if m is not variable
    if ~exist('m', 'var')
      load(reob.file, 'm')
    end
    saveandtest([fname '_chol'], 'CC', 'obs_pixels', 'vects', 'val', 'reob', 'm')
    setpermissions([fname '_chol.mat']);

  end
  disp('cholesky decomp complete')
end


%This is how you can generate sims based on Chol 
%rr=randn(size(CC,1),1);
%sim=rr'*CC;

return
