function name=get_ct_filename(covmatopt)
% name=get_ct_filename(covmatopt)
%
% Return the path + filename for a theory covariance matrix.
% 
% INPUTS
%   covmatopt    Structure of information used in creating and processing the
%                theory covariance matrices. See the doc string in
%                reduc_covmattheory() for more information.
% 
% OUTPUTS
%   name         The file path for the given covariance matrix (relative to
%                the analysis directory).
%

  % Ensure the destination directory exists
  ctdir = fullfile('matrixdata/c_t/', covmatopt.sernum(1:4));
  if ~isdir(ctdir)
    system_safe(['mkdir -p ' ctdir]);
  end

  maptype = covmatopt.map;
  % Healpix maps are all generically described as 'healpix'.
  if strfind(maptype,'.fits')
    maptype = 'healpix';
  end

  % Record input spectrum as given by file name
  if ischar(covmatopt.spec)
    [dum,specname,dum] = fileparts(covmatopt.spec);
  elseif isstruct(covmatopt.spec)
    [dum,specname,dum] = fileparts(covmatopt.spec.file);
  else
    error('get_ct_filename:InvalidData', ...
        'Invalid data in covmatopt.spec');
  end

  % If present, record the beam which is applied
  if isfield(covmatopt,'beam') && ~isempty(covmatopt.beam)
    % Turn a numeric beam into string.
    if isnumeric(covmatopt.beam)
      beamstr = sprintf('_beam%0.2f', covmatopt.beam);
      dotidx = strfind(beamstr, '.');
      beamstr(dotidx) = 'p';

    % Assume strings are recognized beam names
    elseif ischar(covmatopt.beam)
      beamstr = ['_beam' covmatopt.beam];

    % Anything else is an error
    else
      error('get_ct_filename:InvalidData', ...
          'Invalid data in covmatopt.beam');
    end
  else
    beamstr = '';
  end

  % Record the reobservation matrix information, if any
  if isfield(covmatopt,'reob') && isstruct(covmatopt.reob)

    % Will probably always be this case
    if ischar(covmatopt.reob.file)
      % Extract sernum and daughter from matrix filename as identifying
      % information.
      [matdir,matfile,dum] = fileparts(covmatopt.reob.file);
      [dum,sernum,dum] = fileparts(matdir);
      % Remove simulation, e.g. 'real_', from beginning of file name.
      matfile = matfile(6:end);
      % Then grab the daughter by recognizing it comes before '_filt'
      daughter = matfile(1:max(strfind(matfile,'_filt'))-1);
      % Use sernum and daughter to identify the reobservation
      reobsstr = sprintf('_reob%s_%s', sernum, daughter);

    % Just in case, allow matrix structure to be given as data, and use
    % information from the coaddopt.
    elseif isstruct(covmatopt.reob.file) ...
        && isfield(covmatopt.reob.file, 'coaddopt')

      % Use sernum and daughter from the coaddopt to identify the
      % reobservation
      reobsstr = sprintf('_reob%s_%s', ...
          covmatopt.reob.file.coaddopt.sernum(1:4), ...
          covmatopt.reob.file.coaddopt.daughter);

    % Otherwise, emit error
    else
      error('get_ct_filename:InvalidData', ...
          'Invalid data in covmatopt.reob.file');
    end

    % Identify the field which has been reobserved uniquely
    if isfield(covmatopt.reob, 'field')
      if ~iscell(covmatopt.reob.field)
        reobsfield = ['_' covmatopt.reob.field];
      else
        % Ensure that naming is consistent despite how reob.field might be
        % ordered. This is a bit ugly, but it seems to be easiest way to
        % accomplish T < Q < U ordering since Q < T < U in ASCII ordering.
        order = {'TT','TQ','TU','QQ','QU','UU'};
        [tf,idx] = ismember(covmatopt.reob.field, order);
        idx = sort(idx);
        reobsfield = ['_' sprintf('%s', order{idx})];
      end
    else
      reobsfield = '';
    end
  else
    reobsstr = '_reobnone';
    reobsfield = '';
  end

  % Latitude subset as indexed from output of pixel_subset().
  %
  % Note: The strange (.subset || ~islogical(.subset)) line accomplishes
  %       having .subset identify the selected latitude, including lat = 0.
  %       The logical test will only be evaluated when .subset==0, but if
  %       it's numeric rather than a logical 0/false, then it's taken to be
  %       a latitude. NB! This means setting .subset=false has a different
  %       meaning than .subset=0.
  if isfield(covmatopt,'subset') && ~isempty(covmatopt.subset) ...
      && (covmatopt.subset || ~islogical(covmatopt.subset))
    latstr = sprintf('%+09.4f', -covmatopt.subset);
    % Get rid of the plus sign. (Forced in format string so that positive and
    % negative numbers have same number of digits.)
    if strcmp(latstr(1),'+')
      latstr = latstr(2:end);
    else
      % Use 'n' for negative. Because of negation in sprintf, this will only
      % happen if we somehow get northern hemisphere data.
      latstr(1) = 'n';
    end
    % Replace the decimal point with 'p'
    latstr(strfind(latstr,'.')) = 'p';
    % Then create file name part
    subsetstr = ['_subset' latstr];
  else
    subsetstr = '';
  end

  name = fullfile(ctdir, sprintf('%s_%s_lmax%i%s%s%s%s.mat', ...
      maptype, specname, covmatopt.lmax, beamstr, reobsstr, reobsfield, ...
      subsetstr));

end
