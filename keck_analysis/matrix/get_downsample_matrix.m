function [D,m]=get_downsample_matrix(m)
% [D,m]=get_downsample_matrix(m)
%
% Generates a pointing matrix which maps pixels from a given map gridding
% to one which has half as many pixels in each dimension (i.e. the pixel
% size has 4 times the area).
%
% INPUTS
%   m    The standard map structure from get_map_defn()
%
% OUTPUTS
%   D    A sparse matrix which when multiplied by a map vector (in the
%        style of map2vect()) sums each 2x2 grid of pixels into the
%        coarser pixel grid.
%
%   m    The map structure with values modified to reflect the pixelization
%        of a map after operating with D.
%
% EXAMPLE
%   load('maps/1351/real_d_filtp3_weight3_gs_dp1100_jack01.mat');
%   ac = cal_coadd_ac(ac, get_ukpervolt('2014'), coaddopt);
%   map = make_map(ac, m, coaddopt);
%   [m,T] = map2vect(m, map(1).T);
%   [D,m2] = get_downsample_matrix(m);
%
%   % Operate and divide by 4 to do average over 4 pixels
%   T2 = D * T.' ./ 4; % ...map2vect() gives row vector...
%
%   [m2,T2] = vect2map(m2, T2);
%   plot_map(m2, T2); caxis([-150,150])
%

  % Define the downgraded dimensions
  dx = m.nx/2;
  dy = m.ny/2;

  % subx and suby are the "sub-grided"/original grid. We want to identify
  % a 2x2 block of pixels in this sub-grid with a single pixel in the
  % downgraded map, so there should be two side-by-side pixels (when
  % traversing in a single dimension) which map to the same downgraded
  % map pixel.
  subx = zeros(1, m.nx);
  suby = zeros(1, m.ny);
  subx([2*(1:dx)-1]) = 1:dx;
  subx([2*(1:dx)])   = 1:dx;
  suby([2*(1:dy)-1]) = 1:dy;
  suby([2*(1:dy)])   = 1:dy;
  % Then to make the entire 2D mapping, just mesh the vectors we've
  % constructed.
  [subx,suby] = meshgrid(subx,suby);

  % Then to identify each pixel uniquely, we can calculate the linear index
  % into a downgraded-map-size matrix.
  %
  % Despite Matlab being a column-major order language, the way map2vect()
  % was written, it unwinds the data in a row-major scheme. Therefore, we
  % need to also create the sub-pixel indexing in a row-major manner *and*
  % unwind it in row-major order.
  sublin = reshape([subx + dx*(suby-1)]', 1, m.nx*m.ny);

  % Preallocate or else this will be painfully slow
  D = zeros(dx*dy, length(sublin));
  % Now just run through the matrix row-by-row and identify the 4 pixels
  % which have the same pixel index from the sub-grid.
  for ii=1:size(D,1)
    D(ii,:) = double(sublin == ii);
  end

  % Because D is largely sparse (4/(m.nx*m.ny) fraction filled), any
  % following matrix multiplication is much faster using sparse matrix
  % multiplication.
  D = sparse(D);

  % Now modify m to corresond to what this downgrading will produce
  m.nx = m.nx / 2;
  m.ny = m.ny / 2;
  m.pixsize = m.pixsize * 2;
  % Exactly as get_map_defn() defines these:
  sx=(m.hx-m.lx)/m.nx;
  m.x_tic=m.lx+sx/2:sx:m.hx-sx/2;
  sy=(m.hy-m.ly)/m.ny;
  m.y_tic=m.ly+sy/2:sy:m.hy-sy/2;
  % Remove these if they exist since they're not right anymore and they're
  % things various matrix functions add just for convenience.
  if isfield(m,'ind')
    m = rmfield(m,'ind');
  end
end
