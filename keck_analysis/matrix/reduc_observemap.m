function [mapout, mapin]=reduc_observemap(matrix_name,sigmapfilename,simtype,finalize,save_output,daughter,sernum,ukpervolt)
% Takes a reobserving matrix and applies it to an input map
% 
% 
% INPUTS:
% matrix_name       : filename of reob matrix (string)
%                     OR structure constaining loaded reob matrix
% sigmapfilename    : filename of input map (string)
%                     if sigmapfilename is a cell of input map
%                     filenames, will create output maps for all
%                     element of cell
%                     OR preloaded healpix map structure
% simtype           : type of sim : 2=LCDM, no lensing, no diffpoint
%                                   4=r=0.1 tensors with E forced to zero
%                                   5=LCDM, with lensing, no diffpoint
%                                   8=other tensor spectrum
% 
% OPTIONAL INPUTS:
% finalize          : true, then form Q and U
%                   : false, then leave as ac and save
%                   : (default:false)
% save_output       : save maps (finalized matrix) to disk
%                   : (default:true)
% daughter          : changes daughter name of map. Must be same length
%                     as sigmapfilename
% sernum            : sernum to save the maps under. If less than 8 characters
%                     (after conversion from number to string), then the string
%                     is padded with '0' on the left.
% ukpervolt         : ukpervolt to use if coaddopt.ukpervolt does not exist and
%                     matrix has not already been calibrated (as according to
%                     coaddopt.ukpv_applied).
%
% OUTPUTS:
% map  : the reobserved map
% mapin: the input map pixelized with same pixelization
%
% eg
% matrix_name='matrixdata/matrices/0704/real_allcmb_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat';
% sigmapfilename={'input_maps/camb_planck2013_r0/map_unlens_n0512_r0001_sB2bbns_constrained_dNoNoi.fits'};
% reduc_observemap(matrix_name,sigmapfilename,2);
%
% matrix_name='matrixdata/matrices/0704/real_allcmb_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat';
% sigmapfilename={'/n/bicepfs2/b2planck/pipeline/input_maps/PSM178_Planck/psm178_fg_353_full_map_nside0512_nlmax1280_bB2bbns_0338.fits'};
% reduc_observemap(matrix_name,sigmapfilename,8,0,1,'psm_178_fg_353', '0755001');

if ~iscell(sigmapfilename);
  sigmapfilename={sigmapfilename};
end

if ~exist('simtype', 'var') | isempty(simtype)
  error('no simtype specified')
end

if ischar(simtype)
  simtype=str2num(simtype);
end

if ~exist('finalize', 'var') | isempty(finalize)
  finalize=false;
end

if ~exist('save_output', 'var') | isempty(save_output)
  save_output=true;
end

if ~exist('daughter', 'var') | isempty(daughter)
  daughter=cell(1,length(sigmapfilename));
end

if ~iscell(daughter)
  daughter={daughter};
end

if ~exist('sernum', 'var')
  sernum=cell(1,length(sigmapfilename));
end
if ~iscell(sernum)
  sernum={sernum};
end
for ii=1:length(sernum)
  if ~isempty(sernum{ii}) && isnumeric(sernum{ii})
    sernum{ii} = num2str(sernum{ii});
  end
  if ~isempty(sernum{ii}) && length(sernum{ii}) < 8
    sernum{ii} = [sernum{ii} sprintf('%0*i', 8-length(sernum{ii}), 0)];
  end
end

%---------------------------------%

%load matrix
if~isstruct(matrix_name)
  fprintf(1,'loading %s...\n', matrix_name);
  tic
  load(matrix_name)
  toc
else
  matrixc=matrix_name.matrixc;
  coaddopt=matrix_name.coaddopt;
end

%thow out cuts for sims since they are big
if isfield(coaddopt, 'c')
  coaddopt=rmfield(coaddopt, 'c');
  coaddopt=rmfield(coaddopt, 'hsmax');
end

%if m doesnt exist, get from coaddopt
if ~exist('m', 'var')
  disp('taking m from coaddopt')
  m=get_map_defn(coaddopt.type);
end

% To enforce consistency, it is an error to not have the correct ukpervolt
% either stored in the coaddopt or use a matrix which has already been
% calibrated if the ukpervolt argument is not given.
%
% This is required since the matrix contains two types of data: part is
% unit-agnostic and will spit out temperature given a temperature input sky,
% the other part comes from the real data (namely weights and variances).
if (~isfield(coaddopt, 'ukpervolt') || isempty(coaddopt.ukpervolt)) ...
    && ~isfield(coaddopt, 'ukpv_applied')
  if ~exist('ukpervolt','var') || isempty(ukpervolt)
    error('reduc_observemap:MissingCalibration',['ukpervolt is mandatory ' ...
        'when neither coaddopt.ukpervolt nor coaddopt.ukpv_applied are set']);
  else
    coaddopt.ukpervolt = ukpervolt;
  end
end

%grab input daughter
indaughter=coaddopt.daughter;

%grab input sernum if needed
emptysernum = cellfun(@isempty,sernum);
if any(emptysernum)
  sernum{emptysernum} = coaddopt.sernum;
end

for i=1:length(sigmapfilename)

  if isempty(sigmapfilename{i})
    continue; %skip if cell is empty
  end

  %get name of this sim x
  coaddopt.sigmapfilename=sigmapfilename{i};
  coaddopt.sernum=[sernum{i}(1:7) num2str(simtype)];
  coaddopt.observematrix=matrix_name;

  %if daughter is filled, add it to name
  coaddopt.daughter=indaughter;
  if(~isempty(daughter{i}))
    coaddopt.daughter=[coaddopt.daughter '_' daughter{i}];
  end

  % Convert a coaddopt.rx to a daughter suffix since traditional pipeline
  % coaddopts don't have this field.
  if isfield(coaddopt, 'rx') && ~isempty(coaddopt.rx)
    coaddopt.daughter = [coaddopt.daughter '_rx' coaddopt.rx];
    coaddopt = rmfield(coaddopt, 'rx');
  end

  %create the input map vector
  [mapv,mapin]=make_mapv(coaddopt,m);

  %operate matrix
  ac=make_map_matrix(matrixc,m,coaddopt,mapv);

  %check if matrix was finalized
  if(isfield(ac, 'T')) %we used a finalized matrix
    if ~finalize
      disp('You have used a finalized matrix, so the output map has to be finalized')
      finalize=true;  %override input
    end
  end

  if finalize %give map and save map

    if(isfield(ac, 'T')) %we used a finalized matrix
      map=ac;
    else
      map=make_map(ac, m, coaddopt);
      if ~isfield(coaddopt,'ukpv_applied')
        map=cal_coadd_maps(map, coaddopt.ukpervolt);
      end
    end

    if(save_output)
      fname=get_map_filename(coaddopt);
      fname=strtok(fname, '.');
      fname=[fname '_final.mat'];
      dirname=fname(1:max(strfind(fname, '/')));
      if ~exist(dirname, 'dir')
        system(['mkdir ' dirname]);
      end
      disp(['saving ' fname])
      saveandtest(fname, 'map','mapin','m','coaddopt','-v7.3');
      setpermissions(fname);
    end

  else %give ac and save ac

    if(save_output)
      fname=get_map_filename(coaddopt);
      dirname=fname(1:max(strfind(fname, '/')));
      if ~exist(dirname, 'dir')
        system(['mkdir ' dirname]);
      end
      disp(['saving ' fname])
      saveandtest(fname, 'ac','mapin','m','coaddopt','-v7.3');
      setpermissions(fname);
    end
    map=ac;
  end

  %store for output
  mapout{i}=map;
  mapin_temp{i}=mapin;
end

%set output variable to mapin
mapin=mapin_temp;

%if there is only one map, don't return a cell
if length(sigmapfilename)==1
  mapout=mapout{1};
  mapin=mapin{1};
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mapout=make_map_matrix(matrix, m, coaddopt, mapv)

for j=1:numel(matrix)
  %this needs to be fixed for coaddtypes with multiple maps

  if(isfield(matrix, 'awgfha')) %this is a pre finalized matrix

    %Make wieghting from var and weight vectors
    [m,mapout(j).wsum]=vect2map(m,full(matrix(j).awa));
    [m,mapout(j).wwv]=vect2map(m,full(matrix(j).awwva));
    [m,mapout(j).sitime]=vect2map(m,full(matrix(j).ata));
    [m,mapout(j).ditime]=vect2map(m,full(matrix(j).apa));
    [m,mapout(j).w]=vect2map(m,full(matrix(j).awdiffa));
    [m,mapout(j).wcc]=vect2map(m, full(matrix(j).awcca));
    [m,mapout(j).wcs]=vect2map(m, full(matrix(j).awcsa));
    [m,mapout(j).wss]=vect2map(m, full(matrix(j).awssa));
    [m,mapout(j).wwccv]=vect2map(m, full(matrix(j).awwccva));
    [m,mapout(j).wwcsv]=vect2map(m, full(matrix(j).awwcsva));
    [m,mapout(j).wwssv]=vect2map(m, full(matrix(j).awwssva));

    %these don't work because we don't have wmax in matrix:
    %[m,mapout(j).wdiff]=vect2map(m,full(matrix(j).awdiffa));
    %[m,mapout(j).Twitime]=vect2map(m, full(matrix(j).awta));
    %[m,mapout(j).Pwitime]=vect2map(m, full(matrix(j).awpa));


    %if(any(size(matrix.awgfha) ~= size(matrix.awcgfcha_awcgfsha)))
    if(strcmp(coaddopt.diffpoint, 'ideal'))
      %operate
      tic
      mapoutv.wz=matrix(j).awgfha*[mapv.T]';
      mapoutv.wcz=matrix(j).awcgfcha_awcgfsha*[mapv.Q,mapv.U]';
      mapoutv.wsz=matrix(j).awsgfcha_awsgfsha*[mapv.Q,mapv.U]';
      toc
    else
      %operate
      tic
      mapoutv.wz=matrix(j).awgfha*[mapv.T,mapv.Q,mapv.U]';
      mapoutv.wcz=matrix(j).awcgfcha_awcgfsha*[mapv.T,mapv.Q,mapv.U]';
      mapoutv.wsz=matrix(j).awsgfcha_awsgfsha*[mapv.T,mapv.Q,mapv.U]';
      toc
    end

    %--------------------------

    %back to 100x236
    [m,mapout(j).wz]=vect2map(m,mapoutv.wz);
    [m,mapout(j).wcz]=vect2map(m,mapoutv.wcz);
    [m,mapout(j).wsz]=vect2map(m,mapoutv.wsz);



  else %we already have a finalized matrix

    [m,mapout(j).Tvar]=vect2map(m, full(matrix(j).Tvar));
    [m,mapout(j).Titime]=vect2map(m, full(matrix(j).Titime));
    [m,mapout(j).Qvar]=vect2map(m, full(matrix(j).Qvar));
    [m,mapout(j).Uvar]=vect2map(m, full(matrix(j).Uvar));
    [m,mapout(j).QUcovar]=vect2map(m, full(matrix(j).QUcovar));
    [m,mapout(j).Pitime]=vect2map(m, full(matrix(j).Pitime));

    tic
    mapoutv.T=matrix(j).TTTQTU*[mapv.T,mapv.Q,mapv.U]';
    mapoutv.Q=matrix(j).QTQQQU*[mapv.T,mapv.Q,mapv.U]';
    mapoutv.U=matrix(j).UTUQUU*[mapv.T,mapv.Q,mapv.U]';
    toc
    %--------------------------

    %back to 100x236
    [m,mapout(j).T]=vect2map(m,mapoutv.T);
    [m,mapout(j).Q]=vect2map(m,mapoutv.Q);
    [m,mapout(j).U]=vect2map(m,mapoutv.U);

    %set un obs regions to NaN
    mapout(j).T(isnan(mapout(j).Tvar))=NaN;
    mapout(j).Q(isnan(mapout(j).Qvar))=NaN;
    mapout(j).U(isnan(mapout(j).Uvar))=NaN;

  end %check on awgfha

end  %jack

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [mapv,mapin]=make_mapv(coaddopt,m)


%%%%%%%%%%%%%%%%%%%%%%%%%%
%      CORRMAP           %
%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(coaddopt.sigmaptype, 'corrmap'))

  %first make the bicep field from the corrmap
  %load the sim map
  load(coaddopt.sigmapfilename);

  % find center of field
  map.racen=mean(m.x_tic); map.deccen=mean(m.y_tic);

  % convert from flat sky map to ra/dec map
  % - this is an approximation only valid for small maps!
  map.y_tic=map.ad.t_val_deg+map.deccen;
  map.x_tic=map.racen+map.ad.t_val_deg/cos(map.deccen*pi/180);

  % now interp sim map to the bin centers of the ra/dec grid as used in
  % reduc_makecomap - the only point of doing this is to strip the
  % area down to the minimum necessary to speed up conv2 and
  % interp2 later
  m.T=interp2(map.x_tic,map.y_tic,map.T,m.x_tic',m.y_tic);
  m.Q=interp2(map.x_tic,map.y_tic,map.Q,m.x_tic',m.y_tic);
  m.U=interp2(map.x_tic,map.y_tic,map.U,m.x_tic',m.y_tic);
  % overwrite sim map with re-interpolated version
  mapin=m;


  %turn map into vector
  [m,mapv.T]=map2vect(m,mapin.T);
  [m,mapv.Q]=map2vect(m,mapin.Q);
  [m,mapv.U]=map2vect(m,mapin.U);

end %corrmap inputs

%%%%%%%%%%%%%%%%%%%%%%%%%%
%      HEALMAP           %
%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(coaddopt.sigmaptype, 'healmap'))
  
  if(ischar(coaddopt.sigmapfilename))
    %load healpix map
    hmap=read_fits_map(coaddopt.sigmapfilename);
  else
    % allow for direct input of healpix map (rather than load from file)
    hmap=coaddopt.sigmapfilename;
  end
    
  %If polarization convention is Healpix change to IAU
  if(strcmp(hmap.polcconv,'COSMO')|strcmp(hmap.polcconv,'UNKNOWN'))
    hmap.map(:,3)=-hmap.map(:,3);
  end
    
  %construct ra,dec array for map directly from healpix (for comparison)
  [ra,dec]=meshgrid(m.x_tic,m.y_tic);
  [m,rav]=map2vect(m,ra);
  [m,decv]=map2vect(m,dec);

  %Construct vectors of healpix maps
  switch coaddopt.siginterp
    case 'taylor'
      mapv.T=[hmap.map(:,1)',hmap.map(:,4)',hmap.map(:,7)',hmap.map(:,10)',hmap.map(:,16)',hmap.map(:,13)'];
      mapv.Q=[hmap.map(:,2)',hmap.map(:,5)',hmap.map(:,8)',hmap.map(:,11)',hmap.map(:,17)',hmap.map(:,14)'];
      mapv.U=[hmap.map(:,3)',hmap.map(:,6)',hmap.map(:,9)',hmap.map(:,12)',hmap.map(:,18)',hmap.map(:,15)'];

      %interpolate directly for comparison
      mapvd=healpix_interp(hmap,rav',decv','taylor');

    case 'healpixnearest'
      mapv.T=hmap.map(:,1)';
      mapv.Q=hmap.map(:,2)';
      mapv.U=hmap.map(:,3)';

      %interpolate directly for comparison
      mapvd=healpix_interp(hmap,rav',decv','healpixnearest');

    otherwise
      error('not a valid interptype')
  end %cases of interp

  %make ref input into map
  [m,mapin.T]=vect2map(m,mapvd(:,1));
  [m,mapin.Q]=vect2map(m,mapvd(:,2));
  [m,mapin.U]=vect2map(m,mapvd(:,3));


end %healpix inputs


%apply ukpervolt (necessary for deproj)
if isfield(coaddopt,'ukpervolt') && ~isfield(coaddopt,'ukpv_applied')
  mapv.T=mapv.T./coaddopt.ukpervolt;
  mapv.Q=mapv.Q./coaddopt.ukpervolt;
  mapv.U=mapv.U./coaddopt.ukpervolt;
end
  
return

