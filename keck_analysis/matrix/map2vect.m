%based on map_vectorize, but includes all pixels.
function [m,mapv]=map2vect(m, map)
%optional: if data is included, will fill in map vect with data and
%    outputs as map_vect

if(~exist('map', 'var'))
  map=[];
  mapv=[];
end

[mx, my]=meshgrid(1:m.nx, 1:m.ny);
mxx=reshape(mx,1,m.nx*m.ny);
myy=reshape(my,1,m.nx*m.ny);
m.ind=sub2ind(size(map),myy, mxx);

if(~isempty(map))
  mapv=reshape(map',1,m.nx*m.ny); %'
end

return
