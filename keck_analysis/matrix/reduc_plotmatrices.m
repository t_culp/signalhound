function plot_matrix_diagnostics(sernum,types,interactive,savefigs)
% plot_matrix_diagnostics(sernum,types,interactive,savefigs)
%
% Generates various diagnostic plots which examine matrix-pipeline products
% to help with ensuring data quality.
%
% INPUTS
%   sernum         The first four digits of a serial number to identify the
%                  set of data products to search for and possibly examine.
%
%   types          Defaults to all options list below.A string or cell array
%                  which identifies which diagnostic plots to generate.
%                  Options are:
%
%                    observing      Plots the middle-map pixel for the
%                                   observing matrix TT, QQ, QU, and UU
%                                   components after reshaping the column
%                                   vector to map shape.
%
%                    covariance     Inspects the E-no-B and B-no-E reobserved
%                                   covariances by plotting the pixel-pixel
%                                   covariance for a map-center pixel after
%                                   reshaping to map shape.
%
%                    eigensystem    Examines the solution to the generalized
%                                   eigenproblem and plots the eigenvalues
%                                   and eigenvectors.
%
%                    eigenmodes     Examines the solution to the generalized
%                                   eigenproblem and plots the 1st, 2nd, 5th,
%                                   10th, & 50th eigenmodes for both E and B
%                                   in map form.
%
%   interactive    Defaults to true. If false, all valid matrices found
%                  belonging to the given serial number are examined,
%                  otherwise the user is queried as to whether a particular
%                  matrix should be examined or not.
%
%   savefigs       Defaults to true. If false, plots are only shown on screen.
%                  If true, then plots are saved using print_fig() in the
%                  matrix_plots/ subdirectory of the current working path.
%
% EXAMPLES
%
%   plot_matrix_diagnostics('1351')
%

  % Make sure we're given a serial number since that's required.
  if ~exist('sernum','var') || isempty(sernum)
    error('plot_matrix_diagnostics:sernum', 'sernum is required');
  end
  if isnumeric(sernum)
    sernum = sprintf('%04i', sernum);
  end

  % Setup default plot types
  if ~exist('types','var') || isempty(types)
    types = {...
      'observing'...     % Observing matrix
      'covariance',...   % Reobserved covariances
      'eigensystem',...  % Eigenvalues and eigenvectors
      'eigenmodes',...   % Sample E and B eigenmodes
    };
  end

  % Default to interactive plotting
  if ~exist('interactive','var') || isempty(interactive)
    interactive = true;
  end

  % Default to saving figures to disk
  if ~exist('savefigs','var') || isempty(savefigs)
    savefigs = true;
  end
  % Also create the output directory if necessary
  if savefigs && ~exist('matrix_plots','dir')
    system_safe('mkdir -p matrix_plots');
  end

  % First, generate a list of all matrices which should be analyzed for the
  % given plot types that have been requested.
  pairbase = sprintf('matrixdata/pairmatrices/%s', sernum);
  matbase = sprintf('matrixdata/matrices/%s', sernum);
  covbase = sprintf('matrixdata/c_t/%s', sernum);

  obsmats = {};
  if any(ismember(types, {'observing'}))
    % Look for the finalized observing matrices. These already have the
    % ukpervolt baked in, so its easier to do an automated analysis with
    % them.
    obsmats = listfiles([matbase '/real_*_final.mat']);

    % Filter on the list if interactive
    if interactive
      toquery = obsmats;
      obsmats = {};
      for ii=1:length(toquery)
        fprintf(1, 'Examine observing matrix in %s? ', toquery{ii});
        ss = lower(strtrim(input('','s')));
        if strcmp(ss,'y') || strcmp(ss,'yes')
          obsmats{end+1} = toquery{ii};
        end
      end
    end
  end

  covmats = {};
  if any(ismember(types, {'covariance'}))
    % First, try to identify the newer-style coadded covariances.
    covmats = listfiles([covbase '/*_{TT,}{QQ,}{QU,}{UU,}.mat']);
    % If that list is empty, then fall back on trying to find matrices which
    % match the older-style naming scheme where the fields weren't included
    % in the final, coadded file name.
    if isempty(covmats)
      covmats = listfiles([covbase '/*_reob????.mat']);
    end

    % Filter on the list if interactive
    if interactive
      toquery = covmats;
      covmats = {};
      for ii=1:length(toquery)
        fprintf(1, 'Examine covariance in %s? ', toquery{ii});
        ss = lower(strtrim(input('','s')));
        if strcmp(ss,'y') || strcmp(ss,'yes')
          covmats{end+1} = toquery{ii};
        end
      end
    end
  end

  eigenmats = {};
  if any(ismember(types, {'eigensystem','eigenmodes'}))
    % The eigenvalue problem solutions all append _eigen to the name, so
    % they're easy to find.
    eigenmats = listfiles([covbase '/*_eigen.mat']);

    % Filter on the list if interactive
    if interactive
      toquery = eigenmats;
      eigenmats = {};
      for ii=1:length(toquery)
        fprintf(1, 'Examine eigensystem in %s? ', toquery{ii});
        ss = lower(strtrim(input('','s')));
        if strcmp(ss,'y') || strcmp(ss,'yes')
          eigenmats{end+1} = toquery{ii};
        end
      end
    end
  end

  % Now run the analyses

  for ii=1:length(obsmats)
    % Avoid having to reload the matrix multiple times for analyses if more
    % than one type is requested.
    fprintf(1,'Loading the observing matrix %s...\n', obsmats{ii})
    x = load(obsmats{ii});
    x.file = obsmats{ii};

    if ismember('observing', types)
      plot_matrix_observing(x, [], savefigs);
    end

    % Clear x since we can't load the next file while this one is still
    % sitting in memory without causing issues with consuming too much.
    clear x;
  end

  for ii=1:length(covmats)
    % Avoid having to reload the matrix multiple times for analyses if more
    % than one type is requested.
    fprintf(1,'Loading the covariance matrix %s...\n', covmats{ii})
    x = load(covmats{ii});
    x.file = covmats{ii};

    if ismember('covariance', types)
      plot_matrix_covariance(x, savefigs);
    end

    % Clear x since we can't load the next file while this one is still
    % sitting in memory without causing issues with consuming too much.
    clear x;
  end

  for ii=1:length(eigenmats)
    % Avoid having to reload the matrix multiple times for analyses if more
    % than one type is requested.
    fprintf(1,'Loading the eigensystem matrix %s...\n', eigenmats{ii})
    x = load(eigenmats{ii});
    x.file = eigenmats{ii};

    if ismember('eigensystem', types)
      plot_matrix_eigensystem(x, savefigs);
    end
    if ismember('eignemodes', types)
      plot_matrix_eigenmodes(x, [], savefigs);
    end

    % Clear x since we can't load the next file while this one is still
    % sitting in memory without causing issues with consuming too much.
    clear x;
  end

end

