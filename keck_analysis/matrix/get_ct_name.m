function name=get_ct_name(map,spec,lmax,beam,subsets,i,reob)
%function name=get_ct_name(map,spec,lmax,beam,subsets,i,reob)
%
%Deprecated. See get_ct_filename() instead.
%

%default subsets is false
if ~exist('subsets','var') || isempty(subsets)
  covmatopt.subset = false;
else
  covmatopt.subset = subsets;
end

%default reob doesn't exist
if ~exist('reob','var')
  covmatopt.reob = [];
else
  covmatopt.reob = reob;
end

%put in a reob sernum dir
if isfield(reob,'file') && ~isempty(reob.file)
  if isfield(reob,'sernum') %avoid loading coaddopt
    covmatopt.sernum = reob.sernum;
  else
    load(reob.file, 'coaddopt')
    covmatopt.sernum = coaddopt.sernum(1:4);
  end
else
  covmatopt.sernum = '0000';
end

covmatopt.map = map;
covmatopt.spec = spec;
covmatopt.lmax = lmax;
covmatopt.beam = beam;

if subsets
  covmatopt.subset = i;
end

name = get_ct_filename(covmatopt);

