function mname=gen_deltastack_name(coaddopt, l, spec, sernum)

%get mapname
mname=get_map_filename(coaddopt);

%make sure dir exists
if(~exist(['maps/' sernum], 'dir'))
  system_safe(['mkdir maps/' sernum])
end  

%make name of stack
nn=max(strfind(mname, '/'));
mname(nn-4:nn-1)=sernum;
mname(nn+1:nn+4)=[num2str(l, '%.3d') spec];

return
