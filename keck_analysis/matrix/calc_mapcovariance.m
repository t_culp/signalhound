function [ct dat]=calc_mapcovariance(map,specfile,lmax,beam,dat,subsets,do_farm,reob)
%function [ct dat]=calc_mapcovariance(map,spec,lmax=800,beam=31[,dat])
%
% calculate the theoretical covariance matrix for a map geometry and spectrum
%
% Inputs:
%  map:   Map definition string, as per get_map_defn
%
%  spec:  The power spectrum, as returned by load_cmbfast
%         or string path to .fits file
%
%  lmax:  Largest value of l to consider (default 800)
%
%  beam:  Beam width in arcminutes at which to smooth (default 31)
%
%  dat:   Structure of pre-computed data from a previous call (optional)
%         Reusing data will save computation time in consecutive calls
%         When absent, all the data is recomputed based on the 'map' input
%         NB: when present, 'map' is ignored and taken to be the
%         same as the call in which 'dat' was generated
%         When using 'dat', 'lmax' can't be larger than in the previous call
% 
%  subsets: if true, will calculate cov mat in pixel subsets
%         : if false, will calculate entire ct (default)
%
%  do_farm: if set to queue name, will farm creation of ct
%           ('serial_requeue', 'general', 'normal_serial', etc)
%         : if false, make in serial (default)
%
%  reob   : structure with fields 'file', and 'field'
%           if file of reob matrix, will reobserve c_t and save rcr
%           for 'field'. eg reob.file='blah.fits';reob.field='TT'
%         : if false, do nothing (default)
%
% Outputs:
%  ct:   The 3*Npix by 3*Npix covariance matrix, with block-repreentation:
%              [TT,  TQ,  TU; ...
%               TQ', QQ,  QU; ...
%               TU', QU', UU];
%        Within each block, ordering is according to map2vect
%  dat:  Structure of precomputed data, to speed up future calls
%        NB: can only be reused on the same map type and not for larger lmax
%
% Example:
%  >> [ct dat] = calc_mapcovariance('bicepcoarse','spectrum.fits',800,31)
%  >> ct2 = calc_mapcovariance('','spectrum2.fits',800,31,dat)
%
%  >>map='/n/bicepfs1/bicep2/pipeline/input_maps/camb_planck2013_r0/map_unlens_n0512_r0001_sB2bbns_dNoNoi.fits';
%  >>spec='input_spectra/camb_planck2013_r0.fits';
%  >>reob.file='matrixdata/matrices/0701/real_subset2010b2_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat';
%  >>calc_mapcovariance(map,spec, 700, 31, [],subsets, 1, reob);
%
% TODO may be useful to have a low-memory mode that doesn't use 'dat' and
%      calculates on the fly rather than in advance
% TODO may want to use pixel-windowing, to get effective averaging over pixels

if (~exist('lmax','var') || isempty(lmax))
  lmax = 700;
end

if (~exist('beam','var') || isempty(beam))
  beam = 'B2bbns';
end

if (~exist('subsets','var')) || isempty(subsets)
  subsets = false;
end

if (~exist('do_farm','var')) || isempty(do_farm)
  do_farm = false;
end

if ~exist('reob','var') || isempty(reob)
  reob = false;
end

if ~isstruct(reob) && reob
  error('must set reob.file to reob matrix filename for reob')
end

if isfield(reob, 'file') && ~isfield(reob,'field')
  reob.field={'TT','TQ', 'TU','QQ','QU','UU'};
end


%window spectrum
spec = window_spectrum(specfile, [], beam, lmax);
spec.file=specfile;
spec.beam=beam;

%find pixel locations
if strfind(map,'.fits');
  [lat lon] = pixel_centers([],map);
  mapname='healpix';
  if isfield(reob, 'file')
    fprintf(1, 'loading coaddopt from %s...\n', reob.file);
    tic; load(reob.file, 'coaddopt'); toc
    sernum=coaddopt.sernum(1:4);
    reob.sernum=sernum;

    switch coaddopt.type
      case 'bicep'
        switch do_farm
          case 'general'
            QUEUE='general';
            mem=70000;
          case 'serial_requeue'
            QUEUE='serial_requeue';
            mem=70000;
          case 'normal_serial'
            QUEUE='normal_serial';
            mem=65000;
          case false
            %do nothing
          otherwise
            error('choose recommended queue (general or serial_requeue), or set do_farm=false')
        end
      case 'bicepcoarse'
        switch do_farm
          case 'serial_requeue'
            QUEUE='serial_requeue';
            mem=36000;
          case 'short_serial'
            QUEUE='short_serial';
            mem=36000;
          case false
            %do nothing
          otherwise
           error('choose recommended queue (serial_requeue), or set do_farm=false')
        end
    end

  elseif do_farm
    mem=30000;
    QUEUE='short_serial';
  end
else
  [lat lon] = pixel_centers(map);
  mapname=map;
  mem=10000;
end

%maximum time it should reasonably take
maxtime=1440; %minutes  

%if requested, find subset of all pixels
[lat lon] = pixel_subset(lat, lon, subsets);
lat.map=mapname;lon.map=mapname;

%if there are subsets, loop over each one
for i=1:length(lat.subset)
  lat_temp=lat;lat_temp.subset=lat.subset{i};lat_temp.i=i;
  lon_temp=lon;lon_temp.subset=lon.subset{i};lon_temp.i=i;

  if (~exist('dat','var') || isempty(dat)) || subsets; %never use dat for subsets
    if (isfield(reob, 'file')) %we are reobserving cov
      for j=1:length(reob.field); %for a particular filed (eg TT,TQ,...)
        reob_in{j}.file=reob.file;
        reob_in{j}.field=reob.field{j};
        reob_in{j}.sernum=reob.sernum;
        reob_in{j}.fname=get_ct_name(lat_temp.map, spec.file, lmax, ...
            spec.beam, subsets, lat_temp.subset(1), reob_in{j});
      end   

    else %fill these in with dummies for non reob case
      reob_in{1}.file=false;
      reob_in{1}.field=false;
      reob_in{1}.sernum=reob.sernum;
      reob_in{1}.fname=get_ct_name(lat_temp.map, spec.file, lmax, ...
          spec.beam, subsets, lat_temp.subset(1));
    end

    for j=1:length(reob_in)
      reob_submit=reob_in{j};

      % Skip if the final data product already exists.
      if exist_file(reob_submit.fname)
        fprintf(1, 'Reobservation %s already exists.\n', ...
            reob_submit.fname);
        continue;
      end

      [dum,fbase,dum] = fileparts(reob_submit.fname);
      [farmfile,jobname] = farmfilejobname(coaddopt.sernum, ...
          'covmattheory', fbase);

      % Also avoid resubmitting (or recreating a farmfile) if the farmfile
      % already exists on disk.
      if exist_file(farmfile)
        fprintf(1,'Job for reobservation of %s_%s already exists.\n', ...
            farmfile, sernum, coaddopt.daughter);
        continue
      end

      if do_farm
        farmit(farmfile, ...
          'reduc_covmattheory(lat_temp,lon_temp,spec,lmax,[],reob_submit);', ...
          'jobname', jobname, ...
          'queue', QUEUE, ...
          'mem', mem, ...
          'maxtime', maxtime, ...
          'submit', false, ...
          'var',{'lat_temp','lon_temp', 'spec', 'lmax', 'reob_submit'});
      else %don't farm
        [ct dat] = reduc_covmattheory(lat_temp, lon_temp, spec, lmax, [], reob_submit); 
      end
    end %loop over reob_in
  else %use dat
    [ct dat] = reduc_covmattheory([], [], spec, lmax, dat); 
  end

end %loop over subsets 

