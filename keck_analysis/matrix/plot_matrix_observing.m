function plot_matrix_observing(obsmatrix,ukpv,savefigs)
% plot_matrix_observing(obsmatrix,ukpv,savefigs)
%
% Plots the center pixel of the BICEP map region from the given observing
% matrix in map form for easy visual comparison.
%
% INPUTS
%   obsmatrix    Either the file path to an observing matrix, or a struct
%                containing all of a finalized obsering matrix file's
%                contents. If the matrix is unfinalized, then ukpv must be
%                specified.
%
%   ukpv         If obsmatrix is unfinalized, the ukpervolt conversion factor
%                must be given. Otherwise for finalized matrices, ignored.
%
%   savefigs     Defaults to 'eps'. If not false, then the plots are saved to
%                disk within matrix_plots/ in either 'png' or 'eps' format.
%
% EXAMPLE
%   plot_obs_matrix(['matrixdata/matrices/1351/real_d_100GHz_filtp3_' ...
%       'weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat']);
%

  if ~exist('savefigs','var') || isempty(savefigs)
    savefigs = 'eps';
  end

  if ~isstruct(obsmatrix)
    filename = obsmatrix;
    fprintf(1,'Loading the observing matrix %s...\n', filename)
    tic();
    obsmatrix = load(filename);
    toc()
  else
    if isfield(obsmatrix,'file')
      filename = obsmatrix.file;
    else
      filename = '';
    end
  end

  % Load relevant variables into local namespace.
  %
  % Lift the map description for convenience.
  m = obsmatrix.m;
  coaddopt = obsmatrix.coaddopt;

  % Finalize the matrix, if necessary.
  if ~isfield(obsmatrix.matrixc, 'TTTQTU')
    if ~exist('ukpv','var') || isempty(ukpv)
      error('plot_matrix_observing:ukpv', ...
        'ukpv must be provided for unfinalized matrices');
    end

    % Applies ukpervolt and makes a finalized matrix.
    obsmatrix.matrixc = make_matrix(obsmatrix.matrixc, coaddopt, m, ...
        false, ukpv);
  end

  % Matrix, too. Do this after finalization to make sure we don't have two
  % different copies of the matrix in memory since Matlab is a copy-on-write
  % language.
  matrixc = obsmatrix.matrixc;

  % Then load the reference HEALPix map that matches the pixelization format
  % the matrix was built for.
  hmap = read_fits_map(obsmatrix.coaddopt.sigmapfilename{1});

  % Note that although Matlab arrays are column-major ordering, the way
  % map2vect() and vect2map() were written, they unravel the map in row-major
  % order.
  %
  % Given the size of the map, we can calculate the central pixel index.
  midpix = m.nx*(m.ny/2) + m.nx/2;
  npix = m.nx * m.ny;

  % Find the pixel nearest to the center of the map in HEALPix coordinates
  lat = m.v_y_tic(midpix);
  lon = m.v_x_tic(midpix);
  lat = deg2rad(90 - lat);
  if lon < 0
    lon = deg2rad(lon + 360);
  else
    lon = deg2rad(lon);
  end
  % Now find which cut-sky pixel index corresponds to this HEALPix pixel
  hmidpix = find(hmap.pixel == ang2pix_ring(hmap.nside, lat, lon));
  nhpix = numel(hmap.pixel);

  [dim,ax] = setup_obs_maps(m);

  % Plot a representative pixel from the middle of the map as:
  %
  %   TT,QU
  %   QQ,UU

  % Calculate the top, center position for the inner titles.
  tx = (m.lx + m.hx) / 2;
  ty = m.hy - 0.05*(m.hy-m.ly);

  % Make maps for the middle pixel of each field.
  [m,TT] = vect2map(m, matrixc.TTTQTU(:,0*nhpix+hmidpix));
  TT(TT==0) = NaN;
  [m,QU] = vect2map(m, matrixc.QTQQQU(:,2*nhpix+hmidpix));
  QU(QU==0) = NaN;
  [m,QQ] = vect2map(m, matrixc.QTQQQU(:,1*nhpix+hmidpix));
  QQ(QQ==0) = NaN;
  [m,UU] = vect2map(m, matrixc.UTUQUU(:,2*nhpix+hmidpix));
  UU(UU==0) = NaN;

  % Use QQ and UU to determine the color scale.
  scale = floor(log10(nanstd([QQ(:);UU(:)])));

  % TT:
  set(gcf(), 'currentaxes', ax(1,1));
  plot_map(m, TT); colormap('gray');
  caxis([-1,1].*10^scale); freezeColors();
  xlabel(''); set(gca(), 'xticklabel',{});
  text(tx, ty, 'TT', ...
    'horizontalalignment','center', ...
    'verticalalignment', 'top', ...
    'fontsize',12);

  % QU:
  set(gcf(), 'currentaxes', ax(1,2));
  plot_map(m, QU); colormap('gray');
  caxis([-1,1].*10^scale); freezeColors();
  xlabel(''); set(gca(), 'xticklabel',{});
  ylabel(''); set(gca(), 'yticklabel',{});
  text(tx, ty, 'QU', ...
    'horizontalalignment','center', ...
    'verticalalignment', 'top', ...
    'fontsize',12);

  % QQ:
  set(gcf(), 'currentaxes', ax(2,1));
  plot_map(m, QQ); colormap('gray');
  caxis([-1,1].*10^scale); freezeColors();
  text(tx, ty, 'QQ', ...
    'horizontalalignment','center', ...
    'verticalalignment', 'top', ...
    'fontsize',12);

  % UU:
  set(gcf(), 'currentaxes', ax(2,2));
  plot_map(m, UU); colormap('gray');
  caxis([-1,1].*10^scale); freezeColors();
  ylabel(''); set(gca(), 'yticklabel',{})
  text(tx, ty, 'UU', ...
    'horizontalalignment','center', ...
    'verticalalignment', 'top', ...
    'fontsize',12);

  % Fill in the colorbar.
  set(gcf, 'currentaxes', ax(1,3));
  cgrad = linspace(-1, 1, 100);
  cimg = repmat(cgrad', 1, 10);
  cimg(end,:) = NaN;
  imagescnan(1:10, cgrad, cimg);
  colormap('gray'); freezeColors();
  set(gca,'xtick',[]);
  set(gca,'ydir','normal','yaxislocation','right');
  set(gca,'ytick', [-1,-0.5,0,0.5,1])
  h = ylabel(sprintf('10^{%d} \\mu{}K',scale));

  % Set global title using information we should have available.
  [dirname,fname,ext] = fileparts(filename);
  sernum = coaddopt.sernum;

  h = gtitle({sprintf('Observation matrix %s', ...
      sernum), fname}, dim.y1(2)+dim.wide/dim.H, 'none');
  set(h,'verticalalignment','bottom', 'fontsize', 10);


  if ischar(savefigs)
    outfname = sprintf('matrix_plots/observation_%s_%s', sernum, fname);
    switch savefigs
      case 'eps'
        % From pieces of print_fig():
        set(gcf,'PaperPositionMode','auto');
        set(gcf, 'Renderer', 'painters')
        set(gcf, 'RendererMode', 'manual')
        print(gcf, '-depsc2', '-painters', outfname);
        fix_lines([outfname '.eps'])

      case 'png'
        mkpng(outfname);
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function [dim,ax]=setup_obs_maps(m)
  %
  % Helper function which just sets up the figure size and axes positions for
  % best plotting for the pixel-sourcing maps for different "field" types.
  %

    % Generic parameters for plot panel layouts:
    dim.thin = 0.05; % Small gap, in inches.
    dim.med  = 0.24; % Medium gap, in inches.
    dim.wide = 0.40; % Wide gap, in inches.
    dim.cbar = 0.15; % Width of color bar, in inches.
    % Full-figure width in inches
    dim.W = 10;
    % Calculate the width of each plot given how many plots we need to fit
    % and the padding required between each.
    dim.subw = (dim.W - 2*dim.wide-2*dim.med - 3*dim.thin - dim.cbar) / 2;
    % Then make each plot the correct proportional size given the field
    % dimensions.
    dim.subh = dim.subw * m.ydos / m.xdos;
    % Now calculate the height of the figure in total
    dim.H = 3*dim.wide+2*dim.med + 2*dim.subh + dim.thin;

    % Now build the column and row positions, remembering that the positions
    % start at the bottom of the figure but we will work in matrix-like
    % numbering starting from the top.
    for ii=1:2
      % Field names
      x = sprintf('x%i', ii);
      y = sprintf('y%i', ii);
      % Invert to count from top of figure instead of bottom
      jj = 2 - ii + 1;

      dim.(x) = dim.wide+dim.med+(ii-1)*(dim.subw+dim.thin) + [0, dim.subw];
      dim.(y) = dim.wide+dim.med+(jj-1)*(dim.subh+dim.thin) + [0, dim.subh];

      % Normalize the row/column calculations to be fractions of the figure
      % size
      dim.(x) = dim.(x) ./ dim.W;
      dim.(y) = dim.(y) ./ dim.H;
    end
    % Add a colorbar that spans both rows
    dim.x3 = dim.x2(2)*dim.W + dim.thin + [0, dim.cbar];
    dim.x3 = dim.x3 ./ dim.W;

    % Now actually generate the figure
    fig = figure();
    % Make the figure the right size, both on screen and in print
    set(fig, 'Units','inches');
    p = get(fig, 'Position');
    set(fig, 'Position', [p(1), p(2)+p(4)-dim.H, dim.W, dim.H])
    set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

    clf();
    ax = nan(2,3);
    % Build the normal grid
    for jj=1:2
      for ii=1:2
        x = sprintf('x%i', ii);
        y = sprintf('y%i', jj);

        ax(jj,ii) = axes('Position', [dim.(x)(1), dim.(y)(1), ...
            diff(dim.(x)), diff(dim.(y))]);
      end
    end
    % Then add the colorbar
    ax(1,3) = axes('Position', [dim.x3(1), dim.y2(1), diff(dim.x3), ...
        dim.y1(2)-dim.y2(1)]);
    ax(2,3) = ax(1,3);
  end
end


