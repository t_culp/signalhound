function plot_matrix_covariance(covmatrix,savefigs)
% plot_matrix_covariance(covmatrix,savefigs)
%
% Plots the center pixel of the reobserved region from the given covariance
% matrix in map form for easy visual comparison. The number of panels will
% vary based on the choice of fields which have been reobserved, but they
% will be shown in matrix of maps going from from top-left to lower-right
% in T -> Q -> U order.
%
% INPUTS
%   covmatrix    Either the file path to a reobserved covariance, or a struct
%                containing all of a covariance file's contents.
%
%   savefigs     Defaults to 'eps'. If not false, then the plots are saved to
%                disk within matrix_plots/ in either 'png' or 'eps' format.
%
% EXAMPLE
%   plot_matrix_covariance(['matrixdata/c_t/1706/healpix_red_spectrum_' ...
%       'EnoB_lmax700_beamKuber100rev1_reob1351_d_100GHz_QQQUUU_eigen.mat']);
%

  if ~exist('savefigs','var') || isempty(savefigs)
    savefigs = 'eps';
  end

  if ~isstruct(covmatrix)
    filename = covmatrix;
    fprintf(1,'Loading the covariance matrix %s...\n', filename)
    covmatrix = load(filename);
  else
    if isfield(covmatrix,'file')
      filename = covmatrix.file;
    else
      filename = '';
    end
  end

  % Load relevant variables into local namespace. Need to do this carefully
  % since various possiblities exist.

  % Before covmatopt existed, reob was stored directly in the file, but we
  % still need to handle this case (i.e. B2/0704 and K12+13/0706 matrices are
  % in this format, so they can't be forgotten).
  if isfield(covmatrix, 'reob')
    reob = covmatrix.reob;
  % Later data products instead store more information in the covmatopt.
  else
    reob = covmatrix.covmatopt.reob;
  end

  % Lift the map description for convenience.
  m = covmatrix.m;

  % Three types of covariance files are saved to disk:
  %   1. Single-field latitude subset covariance.
  %   2. Single-field covariance over all latitudes.
  %   3. Fully coadded covariance over all relevant fields.
  % Try to identify each of these types up-front.
  if isfield(covmatrix, 'rcr')
    % Case 1 - Extract the field from the struct and make it a directly
    % accessible matrix.
    if isstruct(covmatrix.rcr)
      latsubset = true;
      rcr = covmatrix.rcr.(reob.field);

    % Case 3 - Otherwise, rcr is a fully coadded matrix.
    else
      latsubset = false;
      rcr = covmatrix.rcr;
    end
  end
  % Case 2 - The intermediate product during coaddition is named rcrt instead.
  if isfield(covmatrix, 'rcrt')
    latsubset = false;
    rcr = covmatrix.rcrt.(reob.field);
  end

  % For a full matrix (Case 3), plot all fields
  if ~latsubset
    % Determine the number of unique types of fields in this matrix by looking
    % at the first letter of each "cross" field used in the covariance. Make
    % sure to keep the fields in T->Q->U relative ordering.
    fields1 = unique(cellfun(@(s) s(1), reob.field,'uniformoutput',false));
    ascii = {'T','Q','U'};
    fields1 = ascii(ismember(ascii,fields1));
    % Then count the number of fields.
    nfields = numel(fields1);

    % For compatibility with the other cases, we enumerate the fields on
    % each axis independently.
    fields2 = fields1;

  % The other cases should only contain a single field to show.
  else
    nfields = 1;
    % Also correctly label which field is being shown
    fields1 = {reob.field(1)};
    fields2 = {reob.field(2)};
  end
  
  % Note that although Matlab arrays are column-major ordering, the way
  % map2vect() and vect2map() were written, they unravel the map in row-major
  % order.
  if ~latsubset
    % Given the size of the map, we can calculate the central pixel index.
    pixind = m.nx*(m.ny/2) + m.nx/2;
  else
    % For a latitude subset, the middle of the field is unlikely to be
    % observed. Instead find anything that corresponds to a point near the
    % middle in RA, and then just choose one that's isn't unfilled.

    % All pixel indices for a line top to bottom through the middle of the
    % field.
    pixind = (0:m.ny-1)*m.nx + m.nx/2;
    % Get a list of the pixel locations which are non-zero in rcr
    valind = find(rcr);
    % The intersection is a valid pixel to choose.
    pixind = intersect(pixind, valind);
  end
  npix = m.nx * m.ny;

  [dim,ax] = setup_cov_maps(nfields,m);

  clim = [];
  % Consider all possible fields
  for jj=1:nfields
    for ii=jj:nfields
      % Immediately check to see if we actually have information about this
      % particular combination of fields. If we don't, just skip this loop
      % iteration.
      if ~ismember([fields1{jj} fields1{ii}], reob.field)
        fprintf(1,'Field %s not present in covariance matrix. Skipping.\n',...
            [fields1{jj} fields1{ii}]);
        % Make this axis invisible since we won't be plotting anything on it.
        set(axes(ax(jj,ii)), 'visible','off')
        continue
      end

      % The reobserved covariance matrix is a concatenation of the fields
      % into a large matrix. To look at a specific piece like we want to,
      % we need to extract the correct piece.
      %
      % First, skip whole blocks to get to the correct "column".
      mtxind = pixind + (ii-1)*npix;
      % Then grab that col-vector across the entire covariance matrix.
      mapv = rcr(:,mtxind);
      % Finally, grab only the correct portion of this vector for the
      % given field we want.
      mapv = mapv((jj-1)*npix + [1:npix]);

      % Now get the "map" by reshaping this vector
      [dum,map] = vect2map(m,mapv);

      % Finally, plot it!
      set(gcf, 'currentaxes', ax(jj,ii));
      imagesc(m.x_tic, m.y_tic, map);

      % Make sure the x-y directions are shown as is standard
      axis('xy')
      set(gca, 'xdir','reverse');
      % Explicitly set the axis ticks to avoid clashes between plots
      set(gca, 'xtick', [-50,-25,0,25,50])
      set(gca, 'ytick', [-65,-60,-55,-50])

      % The "diagonal" plots carry the axis labels, so remove the tick labels
      % from everything else.
      if (ii~=jj)
        set(gca,'xticklabel',{});
        set(gca,'yticklabel',{});
      else
        xlabel('RA [deg]');
        ylabel('Dec [deg]');
      end

      % Store color axis information about each as we go
      clim(end+1,:) = caxis();
    end
  end

  % From the accumulated color axis information, pick a relative scale that
  % works for all fields simultaneously.

  % Make sure all lower limits are negative, or at worst, 0.
  clim(clim(:,1)>0,1) = 0;
  % Then favor large (absolute) values to make sure we don't saturate them
  % too badly.
  clim2 = [-1,1] .* sqrt(mean(clim.^2,1));
  % No go back and set the color axis for all the plots
  colormap('jet')
  for jj=1:nfields
    for ii=jj:nfields
      if ~isnan(ax(jj,ii))
        set(gcf, 'currentaxes', ax(jj,ii));
        caxis(clim2)
        freezeColors();
      end
    end
  end

  % Add annotations to label which fields are which
  for ii=1:nfields
    % For text above the top row of plots, choose the correct axis and then
    % calculate the correct position in terms of the coordinate axes.
    set(gcf, 'currentaxes', ax(1,ii));
    oldunits = get(gca, 'units');
    set(gca, 'units','inches')
    p = get(gca, 'position');
    xlims = xlim(); ylims = ylim();
    xsc = diff(xlims) / p(3);
    ysc = diff(ylims) / p(4);
    text(mean(xlims), ylims(2) + dim.thin*ysc, fields2{ii}, ...
      'horizontalalignment','center', ...
      'verticalalignment', 'bottom', ...
      'fontsize',12);
    set(gca, 'units',oldunits);

    % Then on the right size, do the same
    set(gcf, 'currentaxes', ax(ii,nfields));
    oldunits = get(gca, 'units');
    set(gca, 'units','inches')
    p = get(gca, 'position');
    xlims = xlim(); ylims = ylim();
    xsc = diff(xlims) / p(3);
    ysc = diff(ylims) / p(4);
    % Note that we reverse the side we'd normally consider since the axes
    % have also been reversed.
    text(xlims(1)-dim.thin*xsc, mean(ylims), fields1{ii}, ...
      'horizontalalignment','left', ...
      'verticalalignment', 'middle', ...
      'fontsize',12);
    set(gca, 'units',oldunits);
  end

  % Set global title using information we should have available.
  [dirname,fname,ext] = fileparts(filename);
  if isfield(covmatrix, 'covmatopt')
    sernum = covmatrix.covmatopt.sernum;

  else
    % For pre-covmatopt data products, extract info given the file name.
    if ~isempty(filename)
      [dum,sernum,dum] = fileparts(dirname);
    else
      warning('plot_matrix_covariance:unknownReobs', ['The covariance ' ...
        'given does not contain enough identifying information. Passing ' ...
        'its filename instead of a struct may be more meaningful.'])
      sernum = 'XXXX';
    end
  end

  h = gtitle({sprintf('Covariance %s reobserved with', ...
      sernum), fname}, dim.y1(2)+dim.wide/dim.H, 'none');
  set(h,'verticalalignment','bottom', 'fontsize', 10);


  if ischar(savefigs)
    outfname = sprintf('matrix_plots/covariance_%s_%s', sernum, fname);
    switch savefigs
      case 'eps'
        % From pieces of print_fig():
        set(gcf,'PaperPositionMode','auto');
        set(gcf, 'Renderer', 'painters')
        set(gcf, 'RendererMode', 'manual')
        print(gcf, '-depsc2', '-painters', outfname);
        fix_lines([outfname '.eps'])

      case 'png'
        mkpng(outfname);
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function [dim,ax]=setup_cov_maps(nfields,m)
  %
  % Helper function which just sets up the figure size and axes positions for
  % best plotting for the pixel-pixel covariance plots.
  %

    % Generic parameters for plot panel layouts:
    dim.thin = 0.05; % Small gap, in inches.
    dim.med  = 0.24; % Medium gap, in inches.
    dim.wide = 0.40; % Wide gap, in inches.
    dim.cbar = 0.15; % Width of color bar, in inches.
    % Full-figure width in inches
    dim.W = 10;
    % Calculate the width of each plot given how many plots we need to fit
    % and the padding required between each.
    dim.subw = (dim.W - dim.wide-2*dim.med - nfields*dim.thin) / nfields;
    % Then make each plot the correct proportional size given the field
    % dimensions.
    dim.subh = dim.subw * m.ydos / m.xdos;
    % Now calculate the height of the figure in total
    dim.H = 3*dim.wide+2*dim.med + nfields*dim.subh + (nfields-1)*dim.thin;

    % Now build the column and row positions, remembering that the positions
    % start at the bottom of the figure but we will work in matrix-like
    % numbering starting from the top.
    for ii=1:nfields
      % Field names
      x = sprintf('x%i', ii);
      y = sprintf('y%i', ii);
      % Invert to count from top of figure instead of bottom
      jj = nfields - ii + 1;

      dim.(x) = dim.wide+dim.med+(ii-1)*(dim.subw+dim.thin) + [0, dim.subw];
      dim.(y) = dim.wide+dim.med+(jj-1)*(dim.subh+dim.thin) + [0, dim.subh];

      % Normalize the row/column calculations to be fractions of the figure
      % size
      dim.(x) = dim.(x) ./ dim.W;
      dim.(y) = dim.(y) ./ dim.H;
    end

    % Now actually generate the figure
    fig = figure();
    % Make the figure the right size, both on screen and in print
    set(fig, 'Units','inches');
    p = get(fig, 'Position');
    set(fig, 'Position', [p(1), p(2)+p(4)-dim.H, dim.W, dim.H])
    set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

    % Put axes on the page. Only make the upper triangle since the other
    % entries are symmetric.
    clf();
    ax = NaN * zeros(nfields);
    for jj=1:nfields
      for ii=jj:nfields
        % Field names
        x = sprintf('x%i', ii);
        y = sprintf('y%i', jj);

        ax(jj,ii) = axes('Position', [dim.(x)(1), dim.(y)(1), ...
            diff(dim.(x)), diff(dim.(y))]);
      end
    end
  end
end


