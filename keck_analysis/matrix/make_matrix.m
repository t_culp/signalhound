function matrixc=make_matrix(matrixc,coaddopt,m,save_output,ukpervolt)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Creates a finalized matrix that goes from [TQU]->[TQU]. Also applies ukpervolt
%if the matrix has not already been calibrated.
%
%INPUTS:
%
%  matrixc     : filename of matrix or matrix structure
%  coaddopt    : usual coaddopt, not needed if you input 
%                a filename for matrixc
%  m           : the map stucture...basically optional
%  save_output : true, will save output file
%  ukpervolt   : ukpervolt to use if coaddopt.ukpervolt does not exist and
%                matrix has not already been calibrated (as according to
%                coaddopt.ukpv_applied).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist('save_output', 'var') | isempty(save_output)
  save_output=true;
end

%---------------------------------%

%load matrix if it is a string
if ischar(matrixc)
  fprintf('loading matrix %s...\n', matrixc);
  tic; load(matrixc); toc
elseif(~exist('coaddopt','var'))
  error('must input a coaddopt since you gave make_matrix a matrix stucture instead of a filename')
end

%remove cuts from coaddopts...
%temporary fix because of bug in reduc_coadd_coadds for 0703 & 0702
if (regexp(coaddopt.sernum, '0703')) | (regexp(coaddopt.sernum, '0702'))
  coaddopt=rmfield(coaddopt, 'c');
  coaddopt=rmfield(coaddopt, 'hsmax');
end

%get m, if needed (shouldn't need to)
if ~exist('m', 'var')
  disp('taking m from coaddopt')
  m=get_map_defn(coaddopt.type);
end

% To enforce consistency, it is an error to not have the correct ukpervolt
% either stored in the coaddopt or use a matrix which has already been
% calibrated if the ukpervolt argument is not given.
%
% This is required since the matrix contains two types of data: part is
% unit-agnostic and will spit out temperature given a temperature input sky,
% the other part comes from the real data (namely weights and variances) which
% must be calibrated.
if (~isfield(coaddopt, 'ukpervolt') || isempty(coaddopt.ukpervolt)) ...
    && ~isfield(coaddopt, 'ukpv_applied')
  if ~exist('ukpervolt','var') || isempty(ukpervolt)
    error('make_matrix:MissingCalibration', ['ukpervolt is mandatory ' ...
        'when neither coaddopt.ukpervolt nor coaddopt.ukpv_applied are set']);
  else
    coaddopt.ukpervolt = ukpervolt;
  end
end

if ~iscell(matrixc)
  % usual matrix
  matrixc=make_matrix_iteration(matrixc,m,coaddopt);
elseif length(matrixc)==1
  % usual matrix mistakenly is a cell
  matrixc=make_matrix_iteration(matrixc{1},m,coaddopt);
else
  display('trouble be a brewin')
end


%if we have an ac, make the map
if (exist('ac', 'var'))
  map=make_map(ac,m,coaddopt);
  if ~isfield(coaddopt, 'ukpv_applied')
    map=cal_coadd_maps(map,coaddopt.ukpervolt);
  end
else
  map=[];
end


%save
if(save_output)
  mname=get_matrix_filename(coaddopt);
  mname=strtok(mname, '.');
  mname=[mname '_final.mat'];
  fprintf('saving finalized matrix to %s...\n', mname);
  tic; saveandtest(mname, 'matrixc','coaddopt','m','map','-v7.3'); toc
  setpermissions(mname);
end


return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function matrix_out=make_matrix_iteration(matrix,m,coaddopt)

% Calibrate the matrix if it hasn't already been done. Then the remaining
% steps can be calibration-agnostic.
if ~isfield(coaddopt,'ukpv_applied')
  fprintf(1, 'Calibrating matrix with ukpervolt %i...\n', coaddopt.ukpervolt);
  matrix = cal_coadd_ac(matrix, coaddopt.ukpervolt, coaddopt);
else
  fprintf(1, 'Matrix already calibrated.\n');
end

%-----------------------------------------%
%              2X2 matrix inversion       %
%-----------------------------------------%
tic
for j=1:length(matrix)

  %do diff to Q,U 2x2 matrix operation as per reduc_coadpairmaps

  %store things for later
  awa=matrix(j).awa;
  awam=sparse(1:length(awa),1:length(awa),1./awa);
  awgfha=awam*matrix(j).awgfha;
  Tvar=matrix(j).awwva./matrix(j).awa.^2;
  rmfield(matrix(j), 'awgfha');
  ata=matrix(j).ata;
  apa=matrix(j).apa;

  %normalize such that sum weight is 1
  awdiffam=sparse(1:length(awa),1:length(awa),1./matrix(j).awdiffa);

  awssa=matrix(j).awssa./matrix(j).awdiffa;
  awcca=matrix(j).awcca./matrix(j).awdiffa;
  awcsa=matrix(j).awcsa./matrix(j).awdiffa;

  awwccva=matrix(j).awwccva./matrix(j).awdiffa.^2;
  awwssva=matrix(j).awwssva./matrix(j).awdiffa.^2;
  awwcsva=matrix(j).awwcsva./matrix(j).awdiffa.^2;

  %calc T 2x2 matrix per pixel
  x=awcca; y=awssa; zz=awcsa;
  acn=1./(x.*y-zz.^2);

  %remove regions where only precision limits have prevented matrix
  %from being singular
  acn(abs(acn)>1d3)=0; %seems we can use same # as normal pipline

  %set zeros and Inf to NaN..surprisingly this seems to be necessary at this
  %point
  acn(find(acn==0))=NaN;
  acn(isinf(acn))=NaN;

  %simple matrix inverse of [x,z;z,y] = [a,b;b,c] - Ken T
  e=acn.*y; f=acn.*-zz; g=acn.*x;

  %set NANs to zero...this is a little scary
  %But it vastly increases sparsity of final matrices
  e(isnan(e))=0;
  f(isnan(f))=0;
  g(isnan(g))=0;

  ee=sparse(1:length(e),1:length(e), e);
  ff=sparse(1:length(f),1:length(f), f);
  gg=sparse(1:length(g),1:length(g), g);

  %operate
  %long form 2x2 * 2x1 product
  pqawgfPa=awdiffam*(ee*matrix(j).awcgfcha_awcgfsha ...
            +ff*matrix(j).awsgfcha_awsgfsha);
  puawgfPa=awdiffam*(ff*matrix(j).awcgfcha_awcgfsha ...
            +gg*matrix(j).awsgfcha_awsgfsha);
  clear matrix;

  %remove zeros so that final mask reflects the obsevreve pixels 
  %found by having non-singular matrix
  setz=isinf(1./e);
  e(setz)=NaN;
  f(setz)=NaN;
  g(setz)=NaN;

  %calc Q/U var maps according to Ken recipe
  Qvar=e.*e.*awwccva+f.*f.*awwssva+2*e.*f.*awwcsva;
  Uvar=f.*f.*awwccva+g.*g.*awwssva+2*f.*g.*awwcsva;
  QUcovar=e.*f.*awwccva+e.*g.*awwcsva+f.*f.*awwcsva+f.*g.*awwssva;


  %stuff in matrix
  matrix_out(j).w=awa;
  matrix_out(j).Tvar=Tvar;
  matrix_out(j).Titime=ata;
  matrix_out(j).Qvar=Qvar;
  matrix_out(j).Uvar=Uvar;
  matrix_out(j).QUcovar=QUcovar;
  matrix_out(j).Pitime=apa;


  %make one block diagonal matrix of awgfha,pqawgfPa and puawgfPa

  %TT TQ TU%
  %QT QQ QU%
  %UT UQ UU% 

  if(strcmp(coaddopt.diffpoint, 'obs'))
    matrix_out(j).TTTQTU=awgfha;clear awgfha
    matrix_out(j).QTQQQU=pqawgfPa;clear pqawgfPa
    matrix_out(j).UTUQUU=puawgfPa;clear puawgfPa
  else
    empties=sparse(size(awgfha,1),size(awgfha,2));
    matrix_out(j).TTTQTU=[awgfha, empties,empties];clear awgfha
    matrix_out(j).QTQQQU=[empties, pqawgfPa];clear pqawgfPa
    matrix_out(j).UTUQUU=[empties,puawgfPa];clear puawgfPa
  end

end   

toc

return
