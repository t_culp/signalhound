function [filtm, d]=filter_scans_matrix(d, fs, order)
%
%Creates matrix, (filtm), of dimensions mapind x mapind which performs
%polynomial filtering of order , (order), on a timestream, (d).
%
%This is matrix version of filter_scans.m
%
%INPUTS:
%    d: tod data   (ie from reduc_initial)
%    fs: scan sturcture 
%    order: polynomial order to subtract
%
%OUTPUTS:
%    filtm: filtering matrix
%    d: filtered timestream (optional) 
%
%e.g.
%load('data/201107030E01_dk113_tod.mat')
%filtm=filter_scans_matrix(d,fs,3);
%OR
%[filtm,d]=filter_scans_matrix(d,fs,3)
%
%JET (20120215)
%20120301: Now outputs matrix, or matrix and data.
%20120320: Also, fits on az instead of sample number.
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

display('filter_scans_matrix...')
tic

% preallocate sparse components for speed
prennz = sum(arrayfun(@(s,e) (e-s+1)^2, fs.sf, fs.ef));
indi = zeros(1, prennz);
indj = zeros(1, prennz);
pm_out = zeros(1, prennz);

%--------------------------
index=1;
index2=1;
for i=1:length(fs.sf)
  s=fs.sf(i); e=fs.ef(i);
  %construct nth order poly (as spec. in order) for this halfscan

  %length of halfscan
  n=e-s+1;  %add one for entire thing

  %make polynomial sub matrix
  pm=poly_matrix(d.azoff(s:e), order);

  %uber fast method..build indices, fill sparse later
  [indjj,indii]=meshgrid([index:index+n-1],[index:index+n-1]);
  indi(index2:(index2+n*n-1)) = indii(:);
  indj(index2:(index2+n*n-1)) = indjj(:);
  pm_out(index2:(index2+n*n-1)) = pm(:);

  % Advance index by n to track row/col offset in matrix and index2 by n*n to
  % track linear index through packed arrays
  index=index+n;
  index2=index2+n*n;

  %only apply filtering if requested
  if nargout>1
    %project out nth order poly from this halfscan

    %060130: need double typecast or precision not sufficient when
    % scans have DC offset added in
    v=double(d.mce0.data.fb(s:e,:));

    v_out=pm*v;
    d.mce0.data.fb(s:e,:)=v_out;
  end

end

%uber fast filling of sparse matrix
filtm=sparse(indj, indi, pm_out);
filtm=filtm';   %for reasons i cant see, but know to be true
%------------------------

toc
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [pm]=poly_matrix(x,order)
%
%Example: pm=poly_matrix(d.azoff(fs.sf(1):fs.ef(1), 3);
%

order=order+1;   %you need one more data point that order,as pointed out by clk 2/17/10.

a = ones(length(x), order);
for j=2:order
  a(:,j) = a(:,j-1) .* x;
end

%Left inverse of vandermone: (A^TA)^-1A^T
ai=inv(a'*a)*a';

%make matrix
pm=eye(length(x))-ai'*a';  %'

return

