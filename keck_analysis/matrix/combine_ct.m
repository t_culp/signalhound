function combine_ct(covmatopt,clean_ct)
% combine_ct(covmatopt,clean_ct)
%
% Combines row subsets of theory cov matrix into full matrix
%
% INPUTS
%   covmatopt    Options for final covariance matrix.
%
%   clean_ct     Optional, defaults to false. If true, row-subset matrices
%                are deleted after combining.
%

if ~exist('clean_ct','var') || isempty(clean_ct)
  clean_ct = false;
end

% Get the final matrix name
covmatopt = get_default_covmatopt(covmatopt);
fullcovname = get_ct_filename(covmatopt);

% Return if the file already exists
if exist_file(fullcovname)
  fprintf(1, 'Covariance matrix %s already exists. Exiting.\n', fullcovname);
  return;
end

% Find pixel locations and create subsets
if strfind(covmatopt.map, '.fits')
  [lat lon] = pixel_centers([], covmatopt.map);
else
  [lat lon] = pixel_centers(map);
end
[lat lon] = pixel_subset(lat, lon, covmatopt.subsets);

% Get the fields we care about (TT, QQ, etc)
if isfield(covmatopt.reob, 'file')
  for j=1:length(covmatopt.reob.field)
    reob_in{j} = covmatopt.reob;
    reob_in{j}.field = covmatopt.reob.field{j};
  end
else %fill these in with dummies for non reob case
  reob_in{1}.file=false;
  reob_in{1}.field=false;
end

% First, combine subsets into full blocks.
for j=1:length(reob_in)
  covmatoptfield = covmatopt;
  covmatoptfield.reob = reob_in{j};
  field = reob_in{j}.field;

  % Check whether the field matrix exists or not.
  fieldcovname = get_ct_filename(covmatoptfield);
  if exist_file(fieldcovname)
    continue;
  end

  for i=1:length(lat.subset)
    fname{i} = get_ct_filename(setfield(covmatoptfield, 'subset', ...
      lat.subset{i}(1)));

    disp(['Loading ' fname{i}])
    load(fname{i}, 'rcr', 'm');

    % Preallocate
    if i==1
      rcrt=[];
      rcrt.(field) = zeros(size(rcr.(field)));
    end

    % Add/combine
    rcrt.(field) = rcrt.(field) + rcr.(field);
  end

  % Kuldge if m is not variable
  if ~exist('m', 'var')
    load(covmatopt.reob.file, 'm')
  end

  % To save the correct covmatopt under the correct name, need to enter into
  % a struct and save that.
  tosave.rcrt = rcrt;
  tosave.covmatopt = covmatoptfield;
  tosave.m = m;
  disp(['Saving ' fieldcovname])
  saveandtest(fieldcovname, '-v7.3', '-struct', 'tosave')
  setpermissions(fieldcovname);
  clear tosave

  % Saving was successful, so clean up the subsets if requested.
  if clean_ct
    disp('Cleaning up row subset matrices...')
    for ii=1:length(fname)
      system_safe(['rm -fr ' fname{ii}]);
    end
  end
end

% Now, all fields exist. Combine into one matrix.
clear rcr

for j=1:length(reob_in)
  covmatoptfield = covmatopt;
  covmatoptfield.reob = reob_in{j};

  %load
  fname = get_ct_filename(covmatoptfield);
  disp(['Loading ' fname])
  load(fname, 'rcrt', 'm');

  %fill in to full cov
  nn = size(rcrt.(covmatoptfield.reob.field));
  switch covmatoptfield.reob.field
    case 'QQ'
      rcr(1:nn(1),1:nn(2))=rcrt.QQ;
    case 'QU'
      rcr(1:nn(1),nn(2)+1:2*nn(2))=rcrt.QU;
      rcr(nn(1)+1:2*nn(1),1:nn(2))=rcrt.QU';
    case 'UU'
      rcr(nn(1)+1:2*nn(1),nn(2)+1:2*nn(2))=rcrt.UU;
  end

end

%kuldge if m is not variable
if ~exist('m', 'var')
  load(covmatopt.reob.file, 'm')
end

%save the all the blocks in on matrix
disp(['Saving ' fullcovname])
saveandtest(fullcovname, '-v7.3', 'rcr', 'covmatopt', 'm')
setpermissions(fullcovname);

return
