function ac=tod2pairmap(ind, j, matrix, matrix_temp, d, m, ac, sdir)

% in B2 partial loadcurves are recorded at a different rate - Walt has
% provided code which comes up with the correct numbers
[sampratio,samprate]=get_sampratio_rate(d);

A=matrix(sdir).pair(j).pointm.a;
tod_length=length(matrix(sdir).filtm);

%get pair indices
js=ind.a(j); %a (sum) indices
jd=ind.b(j); %b (diff) indices

%%%%%%%%%%%%%%%%%%%%
%TEMPERATURE
%%%%%%%%%%%%%%%%%%%%

%get the temp combined matrix'
awgf=matrix_temp.pair.awgf;


%set up tod
z=d.mce0.data.fb(matrix(sdir).mapind,js);

%need to set tod NaNs to zero
z(isnan(z))=0;

%process the data
wz=awgf*z;  %should make sparse?


%Titime
titime=sparse(1:tod_length, 1:tod_length, matrix(sdir).pair(j).weights.wsum>0);
titime=titime./samprate;
sitime=diag(A'*titime*A);


%------------------
%make into map'
[m, ac(j,sdir).wz(:, :)]=vect2map(m,wz);
[m, ac(j,sdir).wsum(:, :)]=vect2map(m,matrix(sdir).pair(j).awa);
[m, ac(j,sdir).sitime(:, :)]=vect2map(m,sitime);
[m, ac(j,sdir).wwv(:, :)]=vect2map(m,matrix(sdir).pair(j).awwva);
%------------------------------------------%


%%%%%%%%%%%%%%%%%%%%
%POLARIZATION
%%%%%%%%%%%%%%%%%%%%

%get the previously combined matrices
awcgf=matrix_temp.pair.awcgf;
awsgf=matrix_temp.pair.awsgf;

%'set up tod
z=d.mce0.data.fb(matrix(sdir).mapind,jd);  %use b index-> diff

%need to set tod NaNs to zero..otherwise one NaN makes whole
%pairmap NaN.
z(isnan(z))=0;

%process the data into wcz, and wsz
wcz=awcgf*z;
wsz=awsgf*z;


if(0)
  %matrix for the wdiff maps
  %we usually dont need this and it makes everything quite a bit slower
  wdiff=matrix(sdir).pair(j).weights.wdiff;
  wdiff=sparse(1:tod_length, 1:tod_length,wdiff);
  awdiffgf=A'*wdiff*matrix_temp.gsm*matrix(sdir).filtm;
  wzdiff=awdiffgf*z;
  [m, ac(j,sdir).wzdiff(:, :)]=vect2map(m,wzdiff);
end


%ditime
pitime=sparse(1:tod_length, 1:tod_length, matrix(sdir).pair(j).weights.wdiff>0);
pitime=pitime./samprate;
ditime=diag(A'*pitime*A);

%----------------------------
%make into map'

[m, ac(j,sdir).wcz(:, :)]=vect2map(m,wcz);
[m, ac(j,sdir).wsz(:, :)]=vect2map(m,wsz);
[m, ac(j,sdir).w(:, :)]=vect2map(m,matrix(sdir).pair(j).awdiffa);
[m, ac(j,sdir).wcc(:, :)]=vect2map(m,matrix(sdir).pair(j).awcca);
[m, ac(j,sdir).wss(:, :)]=vect2map(m,matrix(sdir).pair(j).awssa);
[m, ac(j,sdir).wcs(:, :)]=vect2map(m,matrix(sdir).pair(j).awcsa);
[m, ac(j,sdir).wwccv(:, :)]=vect2map(m,matrix(sdir).pair(j).awwccva);
[m, ac(j,sdir).wwssv(:, :)]=vect2map(m,matrix(sdir).pair(j).awwssva);
[m, ac(j,sdir).wwcsv(:, :)]=vect2map(m,matrix(sdir).pair(j).awwcsva);
[m, ac(j,sdir).ditime(:, :)]=vect2map(m,ditime);

%----------------------------
%convert everything to sparse
ac(j,sdir)=sparsify(ac(j,sdir));

return

