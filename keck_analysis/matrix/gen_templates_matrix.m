function Tm = gen_templates_matrix(pointhm,d,p,ch,mapind)

%    we'd need such a function if we what to be able to input an ...
%    arbitrary T map to use as the deproj template

%this only makes relgain and diffpointing deproj..for now

%eg:

%tag='20120725E02_dk248';
%hmap='input_maps/temp/map_cmb_r0001_n0512_s31p22_signalpnoise_Vband.fits'
%load(['data/real/201207/' tag  '_tod.mat'])
%[p,ind]=get_array_info(tag);
%mapind=make_mapind(d,fs);
%pointhm=point_mat_healpix(ind.a(1), d, p, mapind,hmap,'taylor');
%Tm = gen_templates_matrix(pointhm,d,p,ind.a(1),mapind);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Cut down to only mapped points
ra = d.pointing.cel.ra(mapind);
dec = d.pointing.cel.dec(mapind);
dk = d.pointing.cel.dk(mapind);

[y,x]=reckon(dec,ra,p.r(ch),repmat(p.theta(ch),size(dk))-dk-90);

x = cvec(x);
y = cvec(y);

% keep theta and phi around for later use
theta=cvec((90-y)*pi/180);
phi=cvec(x*pi/180);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Template 1: temperature map for monopole

%number of filled timestream indecies
tod_length=length(find(mapind));

%expand into diagonal
ha=pointhm.a;
dphi=sparse(1:tod_length, 1:tod_length,pointhm.dphi);
dtheta=sparse(1:tod_length, 1:tod_length,pointhm.dtheta);


%fill interp matrix
HA=[(ha),(dtheta*ha),(dphi*ha),(0.5*dtheta.^2*ha),(0.5*dphi.^2*ha),...
    (dphi*dtheta*ha)];

%REFERENCE: 
% map_vector=[T, dTdth, dTdph, dTdth2, dTdph2, dTdthdph]

%OR,
% T_p=hmap.map(loc,1);
% dTdth=hmap.map(loc,4);
% dTdph=hmap.map(loc,7);
% dTdth2=hmap.map(loc,10);
% dTdph2=hmap.map(loc,16);
% dTdthdph=hmap.map(loc,13);

%Taylor interp is
%    map(:,1) = T_p + cvec(dTdth.*dtheta + dTdph.*dphi + 
%               0.5*(dTdth2.*dtheta.^2 + dTdph2.*dphi.^2 + 2*dtheta.*dphi.*dTdthdph));


Tm{1}=HA;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Templates 2-3: differential pointing

%make a empty placeholding matrix
zz = spalloc(size(ha,1),size(ha,2),0);

% Trick healpix_interp into moving the 1st derivs around on the sky using the
% second derivs
%dmap=hmap;
%dmap=rmfield(dmap,'map');
%dmap.map(:,1)=hmap.map(:,4); % dTdth -> T
%dmap.map(:,4)=hmap.map(:,10); % dTdth2 -> dTdth
%dmap.map(:,7)=hmap.map(:,13); %  dTdthdph -> dTdph
%dTdth=healpix_interp(dmap,x,y,'taylor1T');

dTdth=[(zz), (ha), (zz), (dtheta*ha), (zz), (dphi*ha)]; 

%dmap.map(:,1)=hmap.map(:,7); % dTdph -> T
%dmap.map(:,7)=hmap.map(:,16); % dTdph2 -> dTdph
%dmap.map(:,4)=hmap.map(:,13); %  dTdphdth -> dTdth
%dTdph=healpix_interp(dmap,x,y,'taylor1T');

dTdph=[(zz), (zz), (ha), (zz), (dphi*ha), (dtheta*ha)];

% dT/dphi in a synfast map is really dT/dphi/sin(theta), so we'll multiply by
% sin(theta) up front to put it in terms of coordinate phi

%put on diagonal
stheta=sparse(1:length(theta), 1:length(theta),sin(theta));

%mult
dTdph=stheta*dTdph;

% Using the chain rule, we'll construct the templates using d^2T/dx^2 and
% d^T/dy^2 for beamwidth and ellipticity. We'll need the second derivative
% maps, which will have to be nearest neighbor interpolated for now. Trick
% healpix_interp into returning the two second derivatives and one cross
% derivative.
%dmap=hmap;
%dmap=rmfield(dmap,'map');
%dmap.map(:,1)=hmap.map(:,10); % dTdth2 -> T
%dmap.map(:,2)=hmap.map(:,13); % dTdthdph -> Q
%dmap.map(:,3)=hmap.map(:,16); %  dTdph2 -> U
%derivs=healpix_interp(dmap,x,y,'healpixnearest');

%the above is a little confusing...just follow:
% map_vector=[T, dTdth, dTdph, dTdth2, dTdph2, dTdthdph]

d2Tdth2 = [(zz), (zz), (zz), (ha), (zz), (zz)];

d2Tdthdph = [(zz), (zz), (zz), (zz), (zz), (ha)];

d2Tdph2 = [(zz), (zz), (zz), (zz), (ha), (zz)];


% Again, put phi derivatives in terms of coordinate phi
d2Tdth2 = d2Tdth2;
d2Tdthdph = stheta*d2Tdthdph;
d2Tdph2 = stheta.^2*d2Tdph2;


% Derivatives expressed in the healpix (theta,phi) coordinate system need to
% be expressed in the focal plane (x,y) coordinate system. This requires the
% chain rule. The parital derivatives of theta(x,y) and phi(x,y) are needed
% and could be calculated analytically, but we will determine them numerically
% by displacing x,y by a small amount and seeing how theta and phi vary. Here
% we displace by a small amount and then later scale the derivatives up to one
% degree.

% When calculating the numerical derivatives of theta,phi w.r.t. x,y, offset
% the beam by this many degrees
du=1e-3;

% First displace the beam centroids by +x, calculate dtheta and dphi
pd=displace_beamcen(p,du,0,ch);
[dthdx_1,dphdx_1,theta_plusx,phi_plusx]=calc_num_deriv(pd,theta,phi,dk,ra,dec,ch);

% Now displace the beam centroids by -x, calculate dtheta and dphi
pd=displace_beamcen(p,-du,0,ch);
[dthdx_2,dphdx_2,theta_minx,phi_minx]=calc_num_deriv(pd,theta,phi,dk,ra,dec,ch);
dthdx_2=-dthdx_2;
dphdx_2=-dphdx_2;

% Take the average of the derivative to the left and deriv to the right
dthdx=(dthdx_1+dthdx_2)/2;
dphdx=(dphdx_1+dphdx_2)/2;

% Now do the same for y
pd=displace_beamcen(p,0,du,ch);
[dthdy_1,dphdy_1,theta_plusy,phi_plusy]=calc_num_deriv(pd,theta,phi,dk,ra,dec,ch);

pd=displace_beamcen(p,0,-du,ch);
[dthdy_2,dphdy_2,theta_miny,phi_miny]=calc_num_deriv(pd,theta,phi,dk,ra,dec,ch);
dthdy_2=-dthdy_2;
dphdy_2=-dphdy_2;

% Take the average of the derivative above and below
dthdy=(dthdy_1+dthdy_2)/2;
dphdy=(dphdy_1+dphdy_2)/2;

%put on diag:
n=length(dthdx);
dthdx=sparse(1:n, 1:n,dthdx);
dphdx=sparse(1:n, 1:n,dphdx);
dthdy=sparse(1:n, 1:n,dthdy);
dphdy=sparse(1:n, 1:n,dphdy);

% Apply chain rule for first derivatives in two dimensions
% dT/dx
Tm{2} = dthdx*dTdth + dphdx*dTdph;
%dT/dy
Tm{3} = dthdy*dTdth + dphdy*dTdph;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Template 4: beamwidth

% The fit coefficient for beamwidth shouldn't depend on focal plane
% orientation, so we really don't need to convert to d^2T/dx^2 and
% d^2/dy^2, but we'll do it anyways because we need it for ellipticity below

d2thdx2 = dthdx_1-dthdx_2;
d2phdx2 = dphdx_1-dphdx_2;

d2thdy2 = dthdy_1-dthdy_2;
d2phdy2 = dphdy_1-dphdy_2;

% For cross derivatives (needed for ellipticity) we first calculate dtheta/dx
% and dphi/dx with zero y displacement (already done above) and then calculate
% dtheta/dx with with a non zero y-displacement (done below)

% offset trajectory at +y displacement
theta0=theta_plusy;
phi0=phi_plusy;

% dtheta/dx and dphi/dx at +y displacement
pd=displace_beamcen(p,du,du,ch);
[dthdx_1,dphdx_1]=calc_num_deriv(pd,theta0,phi0,dk,ra,dec,ch);

pd=displace_beamcen(p,-du,du,ch);
[dthdx_2,dphdx_2]=calc_num_deriv(pd,theta0,phi0,dk,ra,dec,ch);
dthdx_2=-dthdx_2;
dphdx_2=-dphdx_2;

dthdx_plusy=(dthdx_1+dthdx_2)/2;
dphdx_plusy=(dphdx_1+dphdx_2)/2;

% dtheta/dx and dphi/dx at -y displacement
theta0=theta_miny;
phi0=phi_miny;

pd=displace_beamcen(p,du,-du,ch);
[dthdx_1,dphdx_1]=calc_num_deriv(pd,theta0,phi0,dk,ra,dec,ch);

pd=displace_beamcen(p,-du,-du,ch);
[dthdx_2,dphdx_2]=calc_num_deriv(pd,theta0,phi0,dk,ra,dec,ch);
dthdx_2=-dthdx_2;
dphdx_2=-dphdx_2;

dthdx_miny=(dthdx_1+dthdx_2)/(2);
dphdx_miny=(dphdx_1+dphdx_2)/(2);

% How much did dtheta/dx and dphi/dx change from -y to +y displacement? This is the
% cross derivative. 
d2thdxdy = (dthdx_plusy - dthdx_miny)/2;
d2phdxdy = (dphdx_plusy - dphdx_miny)/2;

clear dthdx_1 dthdx_2 dphdx_1 dphdx_2 dthdx_plusy dphdx_plusy dthdx_miny dphdx_miny theta0 phi0 theta_plusy theta_miny

%put in diag
d2thdx2=sparse(1:n, 1:n,d2thdx2);
d2phdx2=sparse(1:n, 1:n,d2phdx2);
d2thdy2=sparse(1:n, 1:n,d2thdy2);
d2phdy2=sparse(1:n, 1:n,d2phdy2);
d2phdxdy=sparse(1:n, 1:n,d2phdxdy);
d2thdxdy=sparse(1:n, 1:n,d2thdxdy);

d2Tdx2 = d2thdx2*dTdth + d2phdx2*dTdph + (dthdx).^2*d2Tdth2 ...
         + (dphdx).^2*d2Tdph2 + (2*dthdx.*dphdx)*d2Tdthdph;

d2Tdy2 = d2thdy2*dTdth + d2phdy2*dTdph + (dthdy).^2*d2Tdth2 ...
         + (dphdy).^2*d2Tdph2 + (2*dthdy.*dphdy)*d2Tdthdph;

d2Tdxdy = d2thdxdy*dTdth + d2phdxdy*dTdph + dthdx.*dthdy*d2Tdth2 ...
          + dphdx.*dphdy*d2Tdph2 + (dthdx.*dphdy + dphdx.*dthdy)*d2Tdthdph;

% Differential beamwidth
Tm{4} = d2Tdx2 + d2Tdy2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Templates 5-6: ellipticity

% plus - d^2T/dx^2 - d^2T/dy^2
Tm{5} = d2Tdx2 - d2Tdy2;

% cross - 2(d^2T/dxdy)
Tm{6} = 2*d2Tdxdy;

%%%%%%%%%%%%%%%%%%%%%%%

% Return if we don't want more templates
Tm=finalizeTm(Tm,du);

return



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Tm=finalizeTm(Tm,du)

% Scale to 1 degree offsets

% First derivatives
Tm{2}=Tm{2}/du;
Tm{3}=Tm{3}/du;

% Second derivatives
Tm{4}=Tm{4}/du^2;
Tm{5}=Tm{5}/du^2;
Tm{6}=Tm{6}/du^2;
return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [dthdu,dphdu,theta_out,phi_out]=calc_num_deriv(pd,theta,phi,dk,ra,dec,ch)

% find ra/dec trajectory for the displaced channels
[y,x]=reckon(dec,ra,pd.r(ch),repmat(pd.theta(ch),size(dk))-dk-90);

theta_out = cvec((90-y)*pi/180);
phi_out = cvec(x*pi/180);

dthdu=theta_out - theta;
dphdu=phi_out - phi;

return
