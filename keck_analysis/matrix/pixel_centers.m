function [lat lon]=pixel_centers(map,hmap)
% function [lat lon]=pixel_centers(map[,hmap])
%
% Create vectors of the pixel-center coordinates for a map
%
% 'map' should contain a map definition
%   either a struct returned from, or a string to pass to, get_map_defn
% 
% To use healpix pixelization, supply a healpix map 'hmap'
%   either a struct returned from, or string to pass to, read_fits_map
% Specify an empty 'map' ('' or []) to use all pixels in 'hmap'
% If both 'map' and 'hmap' are specified, choose 'hmap' pixels that lie
%   within the bounds of 'map'
%
% Eg:
% [lat lon] = pixel_centers('bicep');
% [lat lon] = pixel_centers('','path/to/healpix_map.fits');

if (~exist('hmap','var') || isempty(hmap))
  % no healpix
  if (ischar(map))
    map = get_map_defn(map);
  end
  [mlon mlat] = meshgrid(map.x_tic, map.y_tic);
  [dum lat] = map2vect(map,mlat);
  [dum lon] = map2vect(map,mlon);
  return

else
  % healpix

  if(ischar(hmap))
    hmap=read_fits_map(hmap);
  end

  % get pixel center coordinates
  switch hmap.ordering
   case 'RING'
    [lat,lon]=pix2ang_ring(hmap.nside,hmap.pixel);
   case 'NESTED'
    [lat,lon]=pix2ang_nest(hmap.nside,hmap.pixel);
  end
  % convert to latitude and logitude in degrees
  lat = 90 - rad2deg(lat);
  lon = rad2deg(lon) - 180;

  if(~isempty(map))
    if(ischar(map))
      map=get_map_defn(map);
    end
    lat_range=((lat>map.ly)&(lat<map.hy));
    lon_range=((lon>map.lx)&(lon<map.hx));
    hmapind=lat_range&lon_range;
    lat=lat(hmapind);
    lon=lon(hmapind);
  end

  return

end


