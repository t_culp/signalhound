function C=make_noise_covariance_block(ac,m,coaddopt)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 20120708 GPT
%
% If reduc_coaddpairmaps has been called with coaddopt.do_covariance~=0,
% then it saves the outer product of the weighted deviations.  This
% function converts them into blocks of the noise covariance matrix in a
% way similar to make_map.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~isfield(coaddopt,'do_covariance') || coaddopt.do_covariance==0
  error('You must set coaddopt.do_covariance in reduc_coaddpairmaps.')
end

% Convert the weights into long vectors.
[m,wsum]=map2vect(m,ac.wsum);
[m,w]=map2vect(m,ac.w);

switch coaddopt.do_covariance
  case 1
    % TT block
    C.TT=ac.wdzwdz./(wsum'*wsum);
  case 2
    % TQ and TU blocks
    [ev,fv,gv]=do_Ken_recipe(ac,coaddopt.m);
    C.TQ=(bsxfun(@times,ev,ac.wdzwdcz)+bsxfun(@times,fv,ac.wdzwdsz))./(wsum'*w);
    C.TU=(bsxfun(@times,fv,ac.wdzwdcz)+bsxfun(@times,gv,ac.wdzwdsz))./(wsum'*w);
  case 3
    % QQ, QU, and UU blocks
    [ev,fv,gv]=do_Ken_recipe(ac,coaddopt.m);
    C.QQ=((ev'*ev).*ac.wdczwdcz+(ev'*fv).*ac.wdczwdsz+(fv'*ev).*ac.wdszwdcz ...
         +(fv'*fv).*ac.wdszwdsz)./(w'*w);
    C.QU=((ev'*fv).*ac.wdczwdcz+(ev'*gv).*ac.wdczwdsz+(fv'*fv).*ac.wdszwdcz ...
         +(fv'*gv).*ac.wdszwdsz)./(w'*w);
    C.UU=((fv'*fv).*ac.wdczwdcz+(fv'*gv).*ac.wdczwdsz+(gv'*fv).*ac.wdszwdcz ...
         +(gv'*gv).*ac.wdszwdsz)./(w'*w);
  case 4
    % all blocks
    [ev,fv,gv]=do_Ken_recipe(ac,coaddopt.m);
    C.TT=ac.wdzwdz./(wsum'*wsum);
    C.TQ=(bsxfun(@times,ev,ac.wdzwdcz)+bsxfun(@times,fv,ac.wdzwdsz))./(wsum'*w);
    C.TU=(bsxfun(@times,fv,ac.wdzwdcz)+bsxfun(@times,gv,ac.wdzwdsz))./(wsum'*w);
    C.QQ=((ev'*ev).*ac.wdczwdcz+(ev'*fv).*ac.wdczwdsz+(fv'*ev).*ac.wdszwdcz ...
          +(fv'*fv).*ac.wdszwdsz)./(w'*w);
    C.QU=((ev'*fv).*ac.wdczwdcz+(ev'*gv).*ac.wdczwdsz+(fv'*fv).*ac.wdszwdcz ...
          +(fv'*gv).*ac.wdszwdsz)./(w'*w);
    C.UU=((fv'*fv).*ac.wdczwdcz+(fv'*gv).*ac.wdczwdsz+(gv'*fv).*ac.wdszwdcz ...
          +(gv'*gv).*ac.wdszwdsz)./(w'*w);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ev,fv,gv]=do_Ken_recipe(ac,m)

% This calculates the e/f/g terms needed in the "Ken recipe" used in the
% standard make_map on polarization data.

% normalize such that sum weight is 1
% calc T 2x2 matrix per pixel
x=ac.wcc./ac.w;
y=ac.wss./ac.w;
z=ac.wcs./ac.w;

n=1./(x.*y-z.^2);
% remove regions where only precision limits have prevented matrix
% from being singular
n(abs(n)>1e3)=NaN;   %used to be 10^12
% simple matrix inverse of [x,z;z,y] = [a,b;b,c] - Ken T
ek=n.*y; fk=n.*-z; gk=n.*x;

% Convert the output to long (row) vectors.
[m,ev]=map2vect(m,ek);
[m,fv]=map2vect(m,fk);
[m,gv]=map2vect(m,gk);

return
