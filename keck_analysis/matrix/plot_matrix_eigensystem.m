function plot_matrix_eigensystem(eigmatrix,cutoff,savefigs)
% plot_matrix_eigensystem(eigmatrix,cutoff,savefigs)
%
% Generates a two-panel plot that graphically shows the results of solving
% the generalized eigenvalue problem. The top panel shows the eigenvalues
% (in order) on a semilog-y plot. Below it are plotted the corresponding
% eigenvectors (as columns) with the color axis showing the magnitude of the
% components.
%
% Annotations are provided to show the boundaries of the modes kept for the
% E and B purifying matrices (the cut-off is taken from make_projection()),
% and the regions (Pure-E, Pure-B, and Ambiguous) are identified.
%
% INPUTS
%   eigmatrix    Either the file path to the eigenvalue problem solution, or
%                a struct containing all of a solution's file's contents.
%
%   cutoff       Cut-offs as in make_projection(). Defaults to 'pixby4'.
%
%   savefigs     Defaults to 'eps'. If not false, then the plots are saved to
%                disk within matrix_plots/ in either 'png' or 'eps' format.
%
% EXAMPLE
%   plot_matrix_eigensystem(['matrixdata/c_t/1706/healpix_red_spectrum_' ...
%       'lmax700_beamKuber100rev1_reob1351_d_100GHz_QQQUUU.mat']);
%

  if ~exist('cutoff','var') || isempty(cutoff)
    cutoff = 'pixby4';
  end
  if ~exist('savefigs','var') || isempty(savefigs)
    savefigs = 'eps';
  end

  % Only load from disk if we haven't been passed an already-loaded structure
  if ~isstruct(eigmatrix)
    filename = eigmatrix;
    fprintf(1,'Loading the eigensystem matrix %s...\n', filename)
    eigmatrix = load(filename);
  else
    filename = eigmatrix.file;
  end

  % Load relevant variables into local namespace.
  b = eigmatrix.b; l_b = eigmatrix.l_b; obs_pixels = eigmatrix.obs_pixels;

  disp('Plotting eigensystem diagnostics...')
  [dim,ax11,ax12,ax21] = setup_eigsystem_plot();

  % Plot the eigenvectors as is
  set(gcf, 'currentaxes', ax11);
  % Even at 8 inches across at 300dpi, no more than 2400 pixels can be shown
  % so downsample the image to that kind of level for faster and smaller
  % plotting. (This also helps with "printing" the image to files later.)
  xx = round(linspace(1,size(b,1),2400));
  % For better visualization, bring every column to the unity stddev level.
  bsc = b(xx,xx);
  bsc = bsxfun(@rdivide, bsc, std(bsc,1));
  imagesc(xx,xx,bsc); clear xx;
  colormap('jet')
  % Capture 2 standard deviations in the color scale
  clims = [-2,2];
  caxis(clims);
  % Remove ticks since they don't really help since the numbering is arbitrary
  % to a person
  set(ax11, 'xtick',[]);
  set(ax11, 'ytick',[]);
  title(sprintf('Eigenvalues/vectors (above/below) for %d observed pixels', ...
    length(obs_pixels)), 'verticalalignment','middle')

  % Manually construct the corresponding colorbar so that we can completely
  % control the size and position of everything
  set(gcf, 'currentaxes', ax12);
  cgrad = linspace(clims(1),clims(2), 1000);
  imagesc(1:10, cgrad, repmat(cgrad',1,10));
  set(ax12,'xtick',[]);
  set(ax12,'ydir','normal','yaxislocation','right');
  set(ax12,'ytick', [clims(1):clims(2)])
  ylabel('eigenvectors normalized to unit stddev')

  % Plot the eigenvalues
  set(gcf, 'currentaxes', ax21);
  semilogy(l_b)
  axis('tight')
  % Again, remove x-ticks since they mean nothing to the viewer
  set(ax21, 'xtick', []);
  % Determine all the relevant powers of 10 spanning the eigenvalue range
  yticks = round(log10(ylim) + [-0.5,0.5]);
  yticks = 10.^(yticks(1):yticks(2));
  % Set the ticks explicitly, and then ask that the major grid is on and
  % minor ticks be shown
  set(ax21, 'ytick',yticks, ...
    'yminortick','on', ...
    'ygrid','on', ...
    'yminorgrid','off');

  if ischar(cutoff)
    switch cutoff
      case 'pixby4'
        nmodes=floor(length(obs_pixels)/4);
    end
  else
    % Find position of eigenvalue nearest the given cutoff value
    [dum,nmodes]=min(abs(l_b - cutoff));
  end

  % nmodes1 is the upper extent of the E-modes, and nmodes2 is the lower
  % extent of the B-modes.
  nmodes1 = floor(nmodes);
  nmodes2 = length(obs_pixels) - nmodes1;

  % Add vertical lines to the eigenvalue plot in these positions...
  set(gcf, 'currentaxes', ax11);
  hold on
  ylims = ylim();
  plot(nmodes1*[1 1], ylims, 'k-');
  plot(nmodes2*[1 1], ylims, 'k-');
  hold off

  % ...and the same for the eigenvectors
  set(gcf, 'currentaxes', ax21)
  hold on
  ylims = ylim();
  plot(nmodes1*[1 1], ylims, 'k:');
  plot(nmodes2*[1 1], ylims, 'k:');
  hold off

  % Then also annotate which side is which in the eigenvalue plot
  text(nmodes1/2, 1, 'E-modes', ...
    'fontsize',12, ...
    'horizontalalignment','center', ...
    'verticalalignment','bottom');
  text(length(obs_pixels)/2, 1, 'Ambiguous modes', ...
    'fontsize',12, ...
    'horizontalalignment','center', ...
    'verticalalignment','bottom');
  text(nmodes2 + nmodes1/2, 1, 'B-modes', ...
    'fontsize',12, ...
    'horizontalalignment','center', ...
    'verticalalignment','top');

  % Set global title using information we should have available.
  [dirname,fname,ext] = fileparts(filename);
  if isfield(eigmatrix, 'covmatopt')
    sernum = eigmatrix.covmatopt.sernum;
    [dum,reobfile,dum] = fileparts(eigmatrix.covmatopt(1).reob.file);

  else
    % For pre-covmatopt data products, extract info given the file name.
    if ~isempty(filename)
      [dum,sernum,dum] = fileparts(dirname);
    else
      warning('plot_matrix_eigensystem:unknownReobs', ['The covariance ' ...
        'given does not contain enough identifying information. Passing ' ...
        'its filename instead of a struct may be more meaningful.'])
      sernum = 'XXXX';
    end

    % Then get which reobservation was used
    [dum,reobfile,dum] = fileparts(eigmatrix.reob.file);
  end

  h = gtitle({sprintf('Eigensystem for covariance %s reobserved with', ...
      sernum), reobfile}, dim.y2(2)+dim.thin/dim.H, 'none');
  set(h,'verticalalignment','bottom', 'fontsize', 10);

  if ischar(savefigs)
    outfname = sprintf('matrix_plots/eigensystem_%s_%s', sernum, fname);
    switch savefigs
      case 'eps'
        % From pieces of print_fig():
        set(gcf,'PaperPositionMode','auto');
        set(gcf, 'Renderer', 'painters')
        set(gcf, 'RendererMode', 'manual')
        print(gcf, '-depsc2', '-painters', outfname);
        fix_lines([outfname '.eps'])

      case 'png'
        mkpng(outfname);
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  function [dim,ax11,ax12,ax21]=setup_eigsystem_plot()
  % [dim,ax11,ax12,ax21]=setup_eigsystem_plot()
  %
  % Helper function which just sets up the figure size and axes positions for
  % best plotting for the eigensystem plots.
  %

    % Generic parameters for plot panel layouts:
    dim.thin = 0.05; % Small gap, in inches.
    dim.med  = 0.24; % Medium gap, in inches.
    dim.wide = 0.40; % Wide gap, in inches.
    dim.cbar = 0.15; % Width of color bar, in inches.
    % Full-figure width in inches
    dim.W = 7.5;
    % Boundaries of columns 1 and 2
    dim.x1 = [dim.wide, dim.W-dim.cbar-dim.thin-dim.wide-dim.med];
    dim.x2 = [dim.x1(2)+dim.thin, dim.W-dim.wide-dim.med];
    % Force row 1 to be square
    dim.y1 = dim.med + [0, diff(dim.x1)];
    % Make row 2 half as high as row 1
    dim.y2 = dim.y1(2)+dim.med + [0, 0.5*diff(dim.x1)];
    % Now calculate the height of the figure in total
    dim.H = dim.y2(2) + dim.wide+dim.med;
    % Normalize the row/column calculations to be fractions of the figure size
    dim.x1 = dim.x1 ./ dim.W;
    dim.x2 = dim.x2 ./ dim.W;
    dim.y1 = dim.y1 ./ dim.H;
    dim.y2 = dim.y2 ./ dim.H;
    % Now actually generate the figure
    fig = figure();
    % Make the figure the right size, both on screen and in print
    set(fig, 'Units','inches');
    p = get(fig, 'Position');
    set(fig, 'Position', [p(1), p(2)+p(4)-dim.H, dim.W, dim.H])
    set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');
    % Put axes on the page
    clf();
    ax11 = axes('Position',[dim.x1(1),dim.y1(1),diff(dim.x1),diff(dim.y1)]);
    ax12 = axes('Position',[dim.x2(1),dim.y1(1),diff(dim.x2),diff(dim.y1)]);
    ax21 = axes('Position',[dim.x1(1),dim.y2(1),diff(dim.x1),diff(dim.y2)]);
  end
end


