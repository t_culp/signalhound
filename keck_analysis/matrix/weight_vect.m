function weight=weight_vect(i,k, d, p, mapind, m, subset)

%Creates a vector, (weight), of dimensions 1 x mapind, which contains
%weights, variances, and detector orientations.
%
%
%INPUTS:
%    i: indices of pair sum (eg: ind.a(j))
%    k: indices of pair diff (eg: ind.b(j))
%    d: tod data   (ie from reduc_initial)
%    p: focal plane structure
%    mapind: locations in d with map data
%    m: structure of desired output map
%    subset: true if creating a flavor subset matrix, otherwise false (ie
%            mapopt.flavor_subset)
%
%
%OUTPUTS:
%    weight.wsum: vector of pair sum weights
%    weight.vsum: vector of pair diff weights
%    weight.wdiff: vector of pair sum variance
%    weight.vdiff: vector of pair diff variance
%    weight.c: vector of cosine of detector angle
%    weight.s: vector of sine of detector angle
%
%
%e.g.
%load('data/201107030E01_dk113_tod.mat')
%[p,ind]=get_array_info('201107030E01_dk113');
%mapind=make_mapind(d,fs);
%m=get_map_defn('bicep');
%weight=weight_vect(ind.a(2),ind.b(2), d, p, mapind, m);
%
%
%JET (201203010)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%display('weights...')
%tic

% xaxis of map can be ra or az
switch m.binning
  case 'az'
    %disp('binning in az/dec')
    x0=d.pointing.hor.az(mapind);
  case 'ra'
    x0=d.pointing.cel.ra(mapind);
end
y0=d.pointing.cel.dec(mapind);

% calc the pol angle for the pair
dk=d.pointing.cel.dk(mapind);
th=p.theta(k)-dk;
thref=p.chi_thetaref(k)-dk;
alpha=chi2alpha(x0,y0,p.r(k),th,p.chi_mean(k),thref);

% Ganga alpha_h+beta_h
ap=(alpha+p.beta(k))*pi/180;

%take sin and cos
c=cos(2*ap); s=sin(2*ap);

% data for this channel
z=double(d.mce0.data.fb(mapind,i));
wsum=d.w(mapind,i);
vsum=d.v(mapind,i);
%c and s have already been mapinded

% Only propagate data NaNs to weight/var if not creating a flavor subset.
if ~subset
  % screen out periods where this channel lost to glitch, or cut by applycuts
  dnan=isnan(z);
  wsum(dnan)=NaN;
  vsum(dnan)=NaN;
end

%Not sure if below is kosher, but need to remove nans before matrix multiply
wsum(isnan(wsum))=0;
vsum(isnan(vsum))=0;

%%%%%%%%%%%%
%Now diff

% data for this channel
zdiff=double(d.mce0.data.fb(mapind,k));
wdiff=d.w(mapind,k);
vdiff=d.v(mapind,k);

% Only propagate data NaNs to weight/var if not creating a flavor subset.
if ~subset
  % screen out periods where this channel lost to glitch
  dnan=isnan(zdiff);
  wdiff(dnan)=NaN;
  vdiff(dnan)=NaN;
end

%Not sure if below is kosher, but need to remove nans before matrix multiply
wdiff(isnan(wdiff))=0;
vdiff(isnan(vdiff))=0;


%%%%%%%%%%%%%

%store output
weight.wsum=wsum;
weight.vsum=vsum;
weight.c=c;
weight.s=s;
weight.wdiff=wdiff;
weight.vdiff=vdiff;


%could make smaller by just storing "a" instead of cos(a) & sin(a) ?

%toc
return
