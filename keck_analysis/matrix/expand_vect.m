function map=expand_vect(m,v_in,obs_pixels);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Expands v_in into a full [Q,U] map based on obs_pixels
%
% Inputs:
%
%    m          : pixel information
%    v_in       : input map vector of obs_pixels
%    obs_pixels : locations of obs_pixels in full map
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%place v_in into full vector
%according to obs_pixels
v_out=zeros(2*m.nx*m.ny,1);
v_out(obs_pixels)=v_in;

%replace seros with NaNs
ff=find(v_out==0);
v_out(ff)=NaN;

%transform vector to map
[m, map.Q]=vect2map(m,v_out(1:end/2));
[m, map.U]=vect2map(m,v_out(end/2+1:end));

return
