function reduc_matrix(tags,mapopt)
%
% tags = cell array of tags. A separate pairmap is made for each tag.

%   read in cal tod
%   filter half-scans in requested manner
%   take sum/diff of A/B pairs
%   for each pair generate "ac" sum matrices from which coadd maps are
%   formed
%   for each pair generate "matrix" sum matrices from which coaddmatrices are
%   formed
%
% options mapopt specify:
%
% type is string specifying what sky area and pixel size to use
%   value is passed through to get_map_defn
%
% proj is projection used for map 'radec' 'ortho' 'arc', or 'tan' (default 'radec')
%
% binning is a flag that indicated whether to bin in standard ra/dec
% (default = 'ra') coordinates or in az/dec coordinates (mapopt.binning = 'az')
%
% filt is string specifying half scan filtering method - see
% filter_scans comments. Most common:
%   n = no filtering
%   pX = polynominal filter where X is order - use p0 for mean subtract
%
% weight is half scan weighting
%   0 = uniform (no weighting)
%   1 = weight by reciprocal of half scan variance pre-filtering
%   2 = weight by reciprocal of half scan variance post-pairdiff&filtering
%   3 = weight by reciprocal of scan set variance post-pairdiff&filtering
%
% gs is a flag indicating that data is to be ground subtracted
%
% resrelgain is a flag that indicates wheather to do or not the secondary
%  residual relgain corrction using atmosphere  default=0
%
% beamcen is 'ideal', 'obs' or 'assim'
%
% polpar is 'ideal', 'obs' or 'assim'
%
% beamwid and abgain are relevant only when making sim
%
% diffpoint is only relevant when making sim
% Note: this should make no difference since the bolometer coords are
% averaged when taking the sum and difference
%
% sernum is a mandatory option specifying the serial number of the pairmaps
%   to be generated. If generating pairmaps from real data, sernum should be
%   of the form 'NNNNreal', where NNNN is the usual user-specific integer.
%   The tod's used will be drawn from data/real/.
%   If generating pairmaps from simulated data, sernum=NNNNYYYX (not a
%   string) where YYY is the realization number and X is the sim type. In
%   this case the tod's used will be drawn from data/NNNNYYYX/

% pairmapset specifies pairmap set of real data from which the weights are
%   derived.  This is now necessary since Clem removed the parallel
%   processing of real data along with sim data.  NNNNreal is the default
%   source
%
% realpairmapset specifies pairmap set of real data from which the cuts
% and weights should be drawn when mapping sims. sernum with the run
% number replaced by 'real' is the
% default and should basically never need to be changed.
%
% When mapping sims the weights and variance are read from the
% files written when analyzing real data with the same
% filter/weight/gs options. In the case of signal-only sims these are used
% for everything. In the case of sims containing noise the half-scan
% variance is recomputed from the sim tod - so that we can check the
% resulting noise maps match the real ones.
%
% acsparse - default 1, save ac structure as sparse matrices. This is much preferable
%            to acpacking because the unpack operation makes coadding over many many
%            tags take longer than 24 hours to complete.
%
% acpack - default 0, beware turning this on if you're planning on a season's worth of
%          data
% mapopt.cut is sub-structure of cut parameters as passed eval_cuts
% eval_round1_cuts - only per halfscan cuts are now applied at
% this stage.
%
% usepsm = 1 means use point source mask when poly subtract
%        = 0 turn it off
%
% e.g.
%      mapopt.sernum='0703real'
%      reduc_matrix({'20100216H01_dk130'},mapopt)

if~iscell(tags)
  tags={tags};
end

if(~exist('mapopt','var'))
  mapopt=[];
end

if(~isfield(mapopt,'sigmaptype') || isempty(mapopt.sigmaptype))
  mapopt.sigmaptype='healmap';
end

if(~isfield(mapopt,'siginterp') || isempty(mapopt.siginterp))
  mapopt.siginterp='healpixnearest';
end

if(~isfield(mapopt,'covn') || isempty(mapopt.covn))
  mapopt.covn=false;
end

if(~isfield(mapopt,'diffpoint') || isempty(mapopt.diffpoint))
  mapopt.diffpoint='ideal';
end

if(~isfield(mapopt,'do_scanjack') || isempty(mapopt.do_scanjack))
  mapopt.do_scanjack=false;
end

if(~isfield(mapopt,'sigmapfilename') || isempty(mapopt.sigmapfilename))
  mapopt.sigmapfilename={'input_maps/camb_planck2013_r0/map_unlens_n0512_r0001_sB2bbns_constrained_dNoNoi.fits'};
end

if (~isfield(mapopt,'flavor_subset') || isempty(mapopt.flavor_subset))
  mapopt.flavor_subset = false;
end

% Get default mapping options for ones that are not specified
mapopt=get_default_mapopt(mapopt);

% Serial number must be specified
if(~isfield(mapopt,'sernum'))
  error('No serial number (mapopt.sernum) specified! Cannot proceed!')
end

if (mapopt.weight==0 && ~mapopt.flavor_subset)
  warning('Possible mistake using weight0 without flavor_subset')
elseif (mapopt.flavor_subset && mapopt.weight~=0)
  error('Flavor-subset reweighting requires weight0')
end

% Generate the necessary subdirs
if ~isdir(['matrixdata/pairmatrices/' mapopt.sernum(1:4) '/'])
  mkdir(['matrixdata/pairmatrices/' mapopt.sernum(1:4) '/']);
end
if ~isdir(['matrixdata/pairmatrices/'  mapopt.sernum(1:4) '/' ...
           mapopt.sernum(5:end) '/'])
  mkdir(['matrixdata/pairmatrices/'  mapopt.sernum(1:4) '/' ...
         mapopt.sernum(5:end) '/']);
end

% fetch map area/pixel size
m=get_map_defn(mapopt.type,[],mapopt.proj);
m.binning=mapopt.binning;

% this prog now loops over tags making a separate pairmap for each
for i=1:length(tags)

  % tags is a cell array of tag strings
  tag=tags{i}

  % read each day of data, process and accumulate into ac structures for
  % each requested jackknife
  [ac,m,matrix,mapopt]=accumulate(tag,m,mapopt);

  %make filename
  filename=get_pairmatrix_filename(tag,mapopt)

  % Write out matrix, exploding every entry so that 'explodeinfo' will contain
  % every variable name entry, letting us skip the expensive whos() call we'd
  % otherwise need to get a complete variable listing. We do the exploding,
  % though, to allow loading subsets of ac and matrix.pair, both of which are
  % very large and increase read times greatly.
  S = explode_setfield([], 'mapopt', mapopt);
  S = explode_setfield(S, 'm', m);
  S = explode_setfield(S, 'ac', ac);
  S = explode_setfield(S, 'matrix(?).pair', matrix.pair);
  S = explode_setfield(S, 'matrix', rmfield(matrix, 'pair'));

  saveandtest(filename,'-v7.3','-struct', 'S');
  setpermissions(filename);

  clear S;

end %tags loop

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ac,m,matrix,mapopt]=accumulate(tag,m,mapopt)
  tic

  % read in tod
  if logical(strfind(mapopt.sernum,'real'))
    if(~isfield(mapopt,'altcal'))
      filename=['data/real/' tag(1:6) '/' tag '_tod.mat'];
    else
      filename=['data/real/' tag(1:6) '/' tag '_tod_altcal.mat'];
    end
  else
    filename=['scratch/' mapopt.sernum(1:4) '/' mapopt.sernum(5:end) '/' tag '_tod.mat'];
  end
  fprintf(1, '\nloading tod from %s...\n',filename);
  load(filename);
  mapopt.filename=filename;


  mapopt.tag=tag;

  % remove large register blocks not needed here to save memory
  d.mce0.data.err=[];
  d.mce0.data.raw=[];
  d.mce0.data.numfj=[];
  d.antenna0.hk.fast_voltage=[];

  % read psm - point source mask
  psmfilename=sprintf('pntsrcmask/%s_pntsrcmask',tag);
  if(exist([psmfilename,'.mat'],'file')) && (mapopt.usepsm ==1)
    fprintf('using point source mask %s\n',psmfilename)
    load(psmfilename);
  end

  toc % time to read tag

  tic
  % if no psm exists or we have been told not to use it set it null
  if(~exist('psm','var') || mapopt.usepsm==0)
    psm=false(size(d.mce0.data.fb));
  end

  % Get array info and index arrays
  [p,ind]=get_array_info(tag,mapopt.beamcen,mapopt.chi, [], [], [], mapopt.epsilon);

  % Modify detector pol angle due to waveplates for this date/time
  p=do_hwp_rot(d.t(1),p);

  % if we are real record p info in mapopt for the record - this might
  % be useful for diagnostics on the pointing/polpar etc if we ever
  % decide to vary that as a function of time - on the hand it is quite big
  if(~exist('simopt','var'))
    mapopt.p=p;
  end

  % calculate the cuts etc

  % round1 cuts are unnecessary in flavor matrices since the cuts will come
  % from the real pairmaps during coadd
  if ~mapopt.flavor_subset
    % calculate the cutparams
    cp=calc_cutparams(d,fs,en,lc,p,ind,dg);
    % evaluate the cuts
    [c, cp]=eval_round1_cuts(cp,mapopt.cut,p,ind);
    % combine to final mask array (with stat printout)
    c.overall=combine_cuts(c,ind);
    % record this info for re-use when pairmapping sims and when
    % coadding real and sim
    mapopt.c.cp=cp; % cut parameters
    mapopt.c.c=c;   % cuts (logical masks)

    % apply the cuts - nan out the bad half-scan/channels
    d=apply_cuts(d,fs,c.overall);
  end

  % Can weight by anything we want
  % This rarely used option weights by reciprocal ofthe pre-sum/diff,
  % pre-filter half scan variance.
  if(mapopt.weight==1 && ~exist('simopt','var'))  % pre filter rvar
    % calc var from d as normal
    v=scan_std(d,fs).^2;
    % weight sum/diff equally using sum of un-diff vars
    v(:,ind.la)=v(:,ind.la)+v(:,ind.lb); % sum A and B var
    v(:,ind.lb)=v(:,ind.la);             % copy sum to B
    w=1./v;                              % conv to weight
  end

  % take sum/diff of A/B pairs
  % store sum in A, diff in B
  %For matrix, This is just to get NANs in right place
  [d,p]=sumdiff_pairs(d,p,fs,ind.a,ind.b);

  % do secondary residual relgain correction using atmosphere
  % (doesnt this just mean we should have used the atmosphere in the
  % first place?)
  if(mapopt.resrelgain == 1)
    %d=remove_relgain2(d,fs,ind.la);
    error('reduc_matrix not able to handle mapopt.resrelgain=1')
  end

  % We want to filter timestream before calculating the weights. Actual
  % filter happens in matrix as well, so create a dummy tod here. Skip
  % if we know we don't need correct variance to be calculated.
  if ~mapopt.flavor_subset
    dd=d;

      % filter half scans...only for weight calc
    if(~iscell(mapopt.filt))
      % filter sum/diff the same
      dd=filter_scans(dd,fs,mapopt.filt,ind.e,psm,mapopt.filttype);
    else
      % filter sum/diff differently
      dd=filter_scans(dd,fs,mapopt.filt{1},ind.a,psm,mapopt.filttype);
      dd=filter_scans(dd,fs,mapopt.filt{2},ind.b,psm,mapopt.filttype);
    end

    % ground subtraction on light channels...only for weight calc
    if(mapopt.gs==1)
      dd=ground_subtract(dd,fs,ind.l,psm);
    end
  end

  % calc alpha_horn and delta alpha_horn values per pair as specified by Ken Ganga
  for k=1:length(ind.a)
    a=ind.a(k); b=ind.b(k);
    p.chi_mean([a,b],1)=mean(p.chi([a,b]))-45;
    p.delalpha([a,b],1)=-(p.chi(b)-p.chi(a)-90);
  end

  % calc r and beta parameters for diff data as specified by Ken Ganga
  p.gamma=(1-p.epsilon)./(1+p.epsilon);
  for k=1:length(ind.a)
    a=ind.a(k); b=ind.b(k);
    p.rr(b,1)=0.5*sqrt((p.gamma(a)+p.gamma(b))^2*cos(p.delalpha(a)*pi/180)^2+...
                      (p.gamma(a)-p.gamma(b))^2*sin(p.delalpha(a)*pi/180)^2);
    p.beta(b,1)=0.5*atan2((p.gamma(a)-p.gamma(b))*sin(p.delalpha(a)*pi/180),...
                      (p.gamma(a)+p.gamma(b))*cos(p.delalpha(a)*pi/180))*180/pi;
  end
  % not sure how to handle r' and beta' in original Ganga doc so ignored...

  % Again, only compute if variance is needed.
  if ~mapopt.flavor_subset
    % apply correction for imperfect polarization efficiency
    % this is in effect an additional cal applied only to difference data
    for k=ind.lb
      dd.mce0.data.fb(:,k)=dd.mce0.data.fb(:,k)/p.rr(k);
    end
  end

  % take variance post filter, sum/diff, efficiency correction -
  % resulting variance maps should reflect pixel scatter in
  % corresponding signal maps

  
  % For flavor-subset tags, the variance should be uniform initially.
  % The reweighting step will create real data-like variances.
  if mapopt.flavor_subset
    v = ones(length(fs.s),length(ind.e));

  % For real data we calc var from d. The variance is used to construct
  % expected noise maps regardless of how weighting is being done.
  elseif(~exist('simopt','var'))
    v=scan_std(dd,fs).^2;

  else
    % for noise only sim we also calc var from d - the
    % resulting variance maps need to be indistinguishable from the
    % real ones or the noise model is not working.
    % (for signal only sims v remains the one from real data read in
    % above.)
    if(~strcmp(simopt.noise,'none'))
      v=scan_std(dd,fs).^2;
    end
  end

  % option 0 - weight uniformly
  if(mapopt.weight==0 && ~exist('simopt','var'))
    w=ones(size(v));
  end

  % option 2 - weight by post filter sum/diff half scan 1/var
  if(mapopt.weight==2 && ~exist('simopt','var'))
    w=1./v;
  end

  % option 3 - same as 2 except weight is calculated over all
  % half-scans. This much reduces the sample variance issue which JK
  % is obsessed with but places stringent requirements on noise
  % stationarity which only a small fraction of pair-sum data probably
  % meets. However since the noise model will not work for such
  % non-stationary data possibly that objection is moot.
  if(mapopt.weight==3 && ~exist('simopt','var'))
    mapind=make_mapind(dd,fs);
    w=1./nanvar(dd.mce0.data.fb(mapind,:));
    % expand to usual size
    w=repmat(w,[length(fs.sf),1]);
  end

  % Catch infinite weights - this might legitimately happen although
  % the associated data had better also be NaN...
  w(~isfinite(w))=NaN;

  %remove dummy tod
  clear dd

  % Record half-scan stats of the data as actually binned into map for
  % diagnostics. Note that this has full cut masking applied as
  % opposed to the similar values in cut-parameters structure cp.
  mapopt.hs.s=sqrt(v);
  mapopt.hs.w=w;
  % also calculate and record the max value occuring in each scan
  % - values large compared to std should not occur
  mapopt.hs.m=scan_max(d,fs);

  % expand out var and weight values over each half scan
  d.v=exp_scan_val(d,fs,v,false);
  d.w=exp_scan_val(d,fs,w,false);

  % histogram the "deviation" (tod/std) for each channel
  mapind=make_mapind(d,fs);
  for j=ind.e
    [bc,n]=hfill(d.mce0.data.fb(mapind,j)./sqrt(d.v(mapind,j)),100,-10,10);
    mapopt.devhist.bc=bc;
    mapopt.devhist.n(j,:)=n;
  end

  toc % time to setup data


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %MATRIX
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %MOST ABOVE WAS FROM reduc_makepairmaps, with certain timestream
  %    operations modified (ie filter_scans, ground_sub) merely
  %    for the sake of faster runtime, since the timestreams DATA
  %    isnt actually used below (but weights, NANS, etc are).
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  % in B2 partial loadcurves are recorded at a different rate - Walt has
  % provided code which comes up with the correct numbers
  [sampratio,samprate]=get_sampratio_rate(d);

  %Make and store mapind
  matrix.mapind=make_mapind(d,fs);
  tod_length=length(find(matrix.mapind));


  %%%%%%%%%%%%%%%%%%%%
  %Poly Filter
  %%%%%%%%%%%%%%%%%%%%

  %Make poly filter matrix
  %we might not want to put this in matrix since we don't need it to
  %    be saved
  if(strcmp(mapopt.filt, 'n'))
    matrix.filtm=sparse(1:tod_length,1:tod_length,1);
  end
  if(strcmp(mapopt.filt, 'p3'))
    matrix.filtm=filter_scans_matrix(d, fs, 3);
  end
  if(strcmp(mapopt.filt, 'p0'))
    matrix.filtm=filter_scans_matrix(d, fs, 0);
  end
  F=matrix.filtm;

  %Next, do a little setup before looping over pairs:

  %make maps of x and y ra dec
  [coordx, coordy]=meshgrid(m.x_tic,m.y_tic);

  %turn ra, dec maps into vect
  [m,m.v_x_tic]=map2vect(m,coordx);
  [m,m.v_y_tic]=map2vect(m,coordy);

  %Make ind lookup table
  [m,m.ind_lookup]=vect2map(m,m.ind);

  %setup cov noise
  if(mapopt.covn==1)
    matrix.covn_sum=sparse(length(m.v_x_tic),length(m.v_x_tic));
    matrix.covn_diff=sparse(length(m.v_x_tic),length(m.v_x_tic));
  end

  %if necessary, load healmap. Do this now, once, so we don't do it each pair
  if(strcmp(mapopt.sigmaptype, 'healmap'))
    if ~iscell(mapopt.sigmapfilename)
      mapopt.sigmapfilename={mapopt.sigmapfilename};

    elseif size(mapopt.sigmapfilename, 1) ~= 1
      warning(['mapopt.sigmapfilename is expected to be 1-by-F cell ' ...
               'array where F is the number of frequencies. Ignoring ' ...
               'anything beyond the first element in dimension 1.'])
    end

    % pre-allocate cell array of appropriate size
    all_hmap=cell(1, size(mapopt.sigmapfilename,2));

    % load every map before continuing
    for ii=1:size(mapopt.sigmapfilename,2)
      all_hmap{ii}=read_fits_map(mapopt.sigmapfilename{1,ii});
    end
  end %of healpix option

  if(isfield(mapopt, 'deproj_map'))
    if ~iscell(mapopt.deproj_map)
      mapopt.deproj_map = {mapopt.deproj_map};

    elseif size(mapopt.deproj_map, 2) ~= 1
      %TODO: Make this warning unnecessary in the future.
      warning(['A different map for each deprojection type is currently '...
               'not implemented for the matrix pipeline. mapopt.deproj_map '...
               'is expected to be F-by-1 cell array where F is the number '...
               'of frequencies. Ignoring anything beyond the first element '...
               'in dimension 2.'])
    end

    all_template_hmap=cell(size(mapopt.deproj_map,1), 1);

    for ii=1:size(mapopt.deproj_map,1)
      all_template_hmap{ii}=read_fits_map(mapopt.deproj_map{ii,1});
    end
  end %of deproj option

  %setup ac
  ac=setup_ac(m, ind);

  %get differential pointing p
  if(strcmp(mapopt.diffpoint,'obs'))
    [p_diffpoint,ind_trash]=get_array_info(tag, mapopt.beamcen, mapopt.chi, ...
      [], mapopt.diffpoint, [], mapopt.epsilon);
  end

  % Enumerate the possible frequencies
  freqs = unique(p.band(ind.la));

  %---%LOOP OVER PAIRS%---%
  %fill in pointm, weights, covn for each pair
  for j=1:length(ind.a)

    % With a known shape for matrix, preallocate for the rest of the
    % iterations.
    if j == 2

      % Get the empty pairs and expand
      zpairs = zerodup(matrix.pair);
      zpairs = repmat(zpairs, [1, length(ind.a)]);
      
      % Then copy in the first iteration's data
      zpairs(1) = matrix(1).pair(1);
      matrix(1).pair = zpairs;
      if (mapopt.do_scanjack)
        zpairs(1) = matrix(2).pair(1);
        matrix(2).pair = zpairs;
      end

      clear zpairs
    end

    tic

    % Determine the correct signal and deprojection maps to use for the
    % rest of this pixel's procesing

    freqidx = find(freqs == p.band(ind.a(j)));
    % For dark detectors with band==0, just use the first map listed
    % as is done in gen_templates.
    if isempty(freqidx)
      freqidx = 1;
    end

    % Pick either the only map given (i.e. numel(...)==1) or the appropriate
    % frequency (freqidx case).
    hmap = all_hmap{ min(freqidx,numel(all_hmap)) };
    template_hmap = all_template_hmap{ min(freqidx,numel(all_template_hmap)) };

    %%%%%%%%%%%%%%%%%%%%
    %GROUND SUBTRACTION
    %%%%%%%%%%%%%%%%%%%%
    % Make ground subtraction matrix. It is different for every channel only
    % if gs is on.
    if mapopt.gs==1
      matrix_temp.gsm = ground_subtract_matrix(d, fs, ind.a(j), ...
          mapopt.flavor_subset);
    else
      matrix_temp.gsm = speye(tod_length);
    end
    G=matrix_temp.gsm;

    %%%%%%%%%%%%%%%%%%%%
    %POINTING
    %%%%%%%%%%%%%%%%%%%%
    %get lhs pointing each pair (this is always ideal)
    mapopt_temp.diffpoint='ideal';
    pointm=point_mat(ind,j,d,p, mapind, m, mapopt_temp);

    %if we want to use healpix input maps, we need different rhs pointing matrix
    %we also want a different rhs for diffpoint
    switch mapopt.diffpoint
      case 'obs'
        if(strcmp(mapopt.sigmaptype, 'healmap'))
          pointhm=point_mat_healpix(ind,j,d,p_diffpoint,mapind,hmap,mapopt);
        else
          pointhm=point_mat(ind,j,d,p_diffpoint, mapind, m, mapopt);
        end
      case 'ideal'
        if(strcmp(mapopt.sigmaptype, 'healmap'))
          pointhm=point_mat_healpix(ind,j,d,p,mapind,hmap,mapopt);
        else
          pointhm=pointm; %lhs is same as rhs
        end
    end

    %%%%%%%%%%%%%%%%%%%%
    %DEPROJ TEMPATES
    %%%%%%%%%%%%%%%%%%%%
    %only for a (use common centers...not p_diffpoint)
    if any(mapopt.deproj)
      mapopt_temp=mapopt;
      mapopt_temp.siginterp='taylor';
      mapopt_temp.diffpoint='ideal';
      pointhm_temp=point_mat_healpix(ind,j,d,p,mapind,template_hmap,mapopt_temp);
      matrix_temp.pair.Tm=gen_templates_matrix(pointhm_temp,d,p,ind.a(j),mapind);
    end

    %%%%%%%%%%%%%%%%%%%%
    %WEIGHTS
    %%%%%%%%%%%%%%%%%%%%
    %make weights for pair sum and diff
    weight=weight_vect(ind.a(j), ind.b(j), d, p, mapind, m, ...
        mapopt.flavor_subset);

    %------------------------------------%
    %        CONSTRUCT MATRICES          %
    %------------------------------------%

    %SCAN JACK
    %separate into scan dir
    %you dont need to cut the filter and GS matrices since cutting the
    %weighting does this already

    if(j==1)
      if (mapopt.do_scanjack)
        sdirs=1:2;
        % expand 2nd dim for forward/backward scan directions
        matrix(2)=matrix(1);
        ac(1,2)=ac(1,1);
      else
        sdirs=1;
      end
    end

    for sdir=sdirs

      % Only do this calculation on the first pair since it's invariant over
      % the rest of the pairs.
      if (j == 1)
        if(mapopt.do_scanjack)
          scind=make_mapind(d,structcut(fs,fs.inc==(sdir-1)));
        else
          scind1=make_mapind(d,structcut(fs,fs.inc==0));
          scind2=make_mapind(d,structcut(fs,fs.inc==1));
          scind=scind1+scind2;
        end

        scind=scind(mapind);

        %save index for later
        matrix(sdir).scind=scind;
      end

      % Pull in correct scind (in matrix form for easy multiplication)
      scind=sparse(1:tod_length, 1:tod_length, matrix(sdir).scind);

      matrix(sdir).pair(j).weights.wsum=scind*weight.wsum;
      matrix(sdir).pair(j).weights.vsum=scind*weight.vsum;
      matrix(sdir).pair(j).weights.c=scind*weight.c;
      matrix(sdir).pair(j).weights.s=scind*weight.s;
      matrix(sdir).pair(j).weights.wdiff=scind*weight.wdiff;
      matrix(sdir).pair(j).weights.vdiff=scind*weight.vdiff;

      %Make full matrices here...for every pair

      %%%%%%%%%%%%%%%%
      %TEMPERATURE
      %%%%%%%%%%%%%%%%%%%%

      %expand weight vectors of this pair into diagonal matrices
      wsum=sparse(1:tod_length, 1:tod_length,matrix(sdir).pair(j).weights.wsum);
      wdiff=sparse(1:tod_length, 1:tod_length,matrix(sdir).pair(j).weights.wdiff);
      vsum=sparse(1:tod_length, 1:tod_length,matrix(sdir).pair(j).weights.vsum);

      %left pointing matrix for this pair
      matrix(sdir).pair(j).pointm.a=scind*pointm.a;
      A=matrix(sdir).pair(j).pointm.a;

      for jj=1:length(pointhm)
        %right pointing matrix for this pair
        if(strcmp(mapopt.sigmaptype, 'healmap'))
          switch mapopt.siginterp
            case 'taylor'
              %expand into diagonal
              dphi=sparse(1:tod_length, 1:tod_length,scind*pointhm(jj).dphi);
              dtheta=sparse(1:tod_length, 1:tod_length,scind*pointhm(jj).dtheta);

              %healpix pointing for this pair
              ha=scind*pointhm(jj).a;

              %fill interp matrix
              HA(jj).a=[(ha),(dtheta*ha),(dphi*ha),(0.5*dtheta.^2*ha),(0.5*dphi.^2*ha),(dphi*dtheta*ha)];

            case 'healpixnearest'
              HA(jj).a=scind*pointhm(jj).a;

            otherwise
              error('not a valid interptype')
          end
        end

        if(strcmp(mapopt.sigmaptype, 'corrmap'))
          HA(jj).a=scind*pointhm(jj).a;
        end
      end


      %make a temporary matrix so we only have to do this
      %multiplication once for matrix and pairmaps...for speed
      %don't try to make GF first...it is slower
      matrix_temp.pair.awgf=A'*wsum*G*F;


      %wdiff is just w times a constant..this is faster
      a=unique(diag(wsum));b=unique(diag(wdiff));
      if(length(a)==2 && length(b)==2)
        ratio=max(b)/max(a);
        matrix_temp.pair.awdiffgf=matrix_temp.pair.awgf*ratio;
      else
        matrix_temp.pair.awdiffgf=A'*wdiff*G*F;
      end

      %AWGFHA*...Filters, weights the map vector -> bicep
      if(strcmp(mapopt.diffpoint, 'ideal'))
        matrix(sdir).pair(j).awgfha=matrix_temp.pair.awgf*HA(1).a;
      else
        sa=sparse(1:tod_length, 1:tod_length,scind*pointhm(1).s);
        sb=sparse(1:tod_length, 1:tod_length,scind*pointhm(2).s);
        ca=sparse(1:tod_length, 1:tod_length,scind*pointhm(1).c);
        cb=sparse(1:tod_length, 1:tod_length,scind*pointhm(2).c);
        ta=[HA(1).a,ca*HA(1).a,sa*HA(1).a];
        tb=[HA(2).a,cb*HA(2).a,sb*HA(2).a];
        HA_s=.5*(ta+tb);
        HA_d=.5*(ta-tb);
        matrix(sdir).pair(j).awgfha=matrix_temp.pair.awgf*HA_s;
      end


      %AwA*...makes weighting map
      matrix(sdir).pair(j).awa=diag(A'*wsum*A);

      %AwwvA*...makes Tvar map
      matrix(sdir).pair(j).awwva=diag(A'*wsum*wsum*vsum*A);

      %ATA*....makes Titime
      %only for scans  where weight hasn't been zeroed
      titime=sparse(1:tod_length, 1:tod_length,matrix(sdir).pair(j).weights.wsum>0);
      titime=titime./samprate;
      matrix(sdir).pair(j).ata=diag(A'*titime*A);
      twitime=titime*wsum;
      matrix(sdir).pair(j).awta=diag(A'*twitime*A);

      %%%%%%%%%%%%%%%%
      %POLARIZATION
      %%%%%%%%%%%%%%%%%%%%

      %AWSSA & AWCCA & AWCSA

      %construct cos and sins
      wdiff=matrix(sdir).pair(j).weights.wdiff;
      vdiff=matrix(sdir).pair(j).weights.vdiff;
      c=matrix(sdir).pair(j).weights.c;
      s=matrix(sdir).pair(j).weights.s;
      wc=matrix(sdir).pair(j).weights.wdiff.*c;
      ws=matrix(sdir).pair(j).weights.wdiff.*s;
      wcc=wc.*matrix(sdir).pair(j).weights.c;
      wss=ws.*matrix(sdir).pair(j).weights.s;
      wcs=wc.*matrix(sdir).pair(j).weights.s;
      wwccv=wdiff.*wcc.*vdiff;
      wwssv=wdiff.*wss.*vdiff;
      wwcsv=wdiff.*wcs.*vdiff;


      %expand into diagonal matrices
      wdiff=sparse(1:tod_length, 1:tod_length,wdiff);
      vdiff=sparse(1:tod_length, 1:tod_length,vdiff);
      c=sparse(1:tod_length, 1:tod_length,c);
      s=sparse(1:tod_length, 1:tod_length,s);
      wc=sparse(1:tod_length, 1:tod_length,wc);
      ws=sparse(1:tod_length, 1:tod_length,ws);
      wss=sparse(1:tod_length, 1:tod_length, wss);
      wcc=sparse(1:tod_length, 1:tod_length, wcc);
      wcs=sparse(1:tod_length, 1:tod_length, wcs);
      wwssv=sparse(1:tod_length, 1:tod_length, wwssv);
      wwccv=sparse(1:tod_length, 1:tod_length, wwccv);
      wwcsv=sparse(1:tod_length, 1:tod_length, wwcsv);

      %turn into matrix to be inverted after coadd step...otherwise
      %  we are singular...use in reduc_coaddmatrices

      %matrix mult
      matrix(sdir).pair(j).awssa=diag(A'*wss*A);
      matrix(sdir).pair(j).awcca=diag(A'*wcc*A);
      matrix(sdir).pair(j).awcsa=diag(A'*wcs*A);
      matrix(sdir).pair(j).awdiffa=diag(A'*wdiff*A);

      matrix(sdir).pair(j).awwssva=diag(A'*wwssv*A);
      matrix(sdir).pair(j).awwccva=diag(A'*wwccv*A);
      matrix(sdir).pair(j).awwcsva=diag(A'*wwcsv*A);


      %'AWCGFCHA & AWSGFCHA & AWCGFSHA & AWSGFSHA
      %Next, need a Matrix that goes from maps to diff
      %  timestream(this is [c*HA,s*HA])
      %and then one that goes from input maps to wcz, wsz
      %  (this is awcgfcha_awcgfsha and awsgfcha_awsgfsha)

      %make a temporary matrix so we only have to do this
      %multiplication once for matrix and pairmaps...for speed
      matrix_temp.pair.awcgf=A'*wc*G*F;
      matrix_temp.pair.awsgf=A'*ws*G*F;

      % apply correction for imperfect polarization efficiency
      % this is in effect an additional cal applied only to
      % difference data
      matrix_temp.pair.awcgf=matrix_temp.pair.awcgf/p.rr(ind.b(j));
      matrix_temp.pair.awsgf=matrix_temp.pair.awsgf/p.rr(ind.b(j));

      %matrix mult
      if(strcmp(mapopt.diffpoint, 'ideal'))
        awcgfcha=matrix_temp.pair.awcgf*c*HA(1).a;
        awsgfsha=matrix_temp.pair.awsgf*s*HA(1).a;
        awsgfcha=matrix_temp.pair.awsgf*c*HA(1).a;
        awcgfsha=matrix_temp.pair.awcgf*s*HA(1).a;

        %this matrix makes wcz and wsz:
        awcgfcha_awcgfsha=[awcgfcha, awcgfsha];
        awsgfcha_awsgfsha=[awsgfcha, awsgfsha];
      else
        awcgfcha_awcgfsha=matrix_temp.pair.awcgf*HA_d;
        awsgfcha_awsgfsha=matrix_temp.pair.awsgf*HA_d;
      end

      %store
      matrix(sdir).pair(j).awcgfcha_awcgfsha=awcgfcha_awcgfsha;
      matrix(sdir).pair(j).awsgfcha_awsgfsha=awsgfcha_awsgfsha;


      %APA*....makes Pitime
      %only for scans  where weight hasnt been zeroed
      pitime=sparse(1:tod_length, 1:tod_length, matrix(sdir).pair(j).weights.wdiff>0);
      pitime=pitime./samprate;
      matrix(sdir).pair(j).apa=diag(A'*pitime*A);
      pwitime=pitime*wdiff;
      matrix(sdir).pair(j).awpa=diag(A'*pwitime*A);

      %deprojection
      %process the deproj templates
      if any(mapopt.deproj)

        % Make sure ac has the wsd and wcd fields since setup_ac() doesn't add
        % these (can't assume deprojection is enabled). Needed to make sure
        % make_template() call below can merge the structs properly.
        if j==1 && sdir==1 && ~isfield(ac, 'wsd')
          ac(1,1).wsd = [];
          ac(1,1).wcd = [];
        end

        for tm=1:length(matrix_temp.pair.Tm)
          matrix_temp.pair.wcd{tm}=matrix_temp.pair.awcgf*matrix_temp.pair.Tm{tm};
          matrix_temp.pair.wsd{tm}=matrix_temp.pair.awsgf*matrix_temp.pair.Tm{tm};
        end

        %make template.m applies Tm to healpix map so we can make ac
        %of deprojection templates...we have to use the real sky (planck)
        % template map in order for deprojection to be a linear process.
        %
        % Explicitly choose sdir=1 and j=1 in call since matrix_temp is not a
        % full matrix, and give only a piece of ac to accomodate.
        ac(j,sdir)=make_template(matrix_temp,template_hmap,ac(j,sdir),m,1,1);
      end

      %%%%%%%%%%%%%%%%%%%%
      %NOISE COVARIANCE
      %%%%%%%%%%%%%%%%%%%%

      %---------------------------------------
      if(mapopt.covn==1)

        %make covaraint noise matrix
        covnm_sum=cov_noise_matrix(d,fs,ind.a(j));
        covnm_diff=cov_noise_matrix(d,fs,ind.b(j));

        keyboard
        %Note that covn is only made for rgl
        %multiply and coadd
        matrix.covn_sum=matrix.covn_sum+A'*w*G*F*covnm_sum*(A'*w*G*F)';
        matrix.covn_diff=matrix.covn_diff+A'*wdiff*G*F*covnm_diff*(A'*wdiff*G*F)';
        %diff should be Q/U
        clear covnm_sum; clear covnm_diff;
      end  %of covn option
      %---------------------------------------



      %%%%%%%%%%%%%%%%
      %REAL PAIRMAPS
      %%%%%%%%%%%%%%%%%%%%

      %---------------------------------------
      %process real data tod with the same matrices to form the usual pairmaps:
      ac=tod2pairmap(ind, j, matrix, matrix_temp, d, m, ac, sdir);
      %---------------------------------------

      %clear variables
      clear A; clear w, clear v; clear wdiff; clear vdiff; clear HA;
      clear c; clear s;

    end %of scan dir loop

    display([num2str(j) ' of ' num2str(length(ind.a)) ' pairs completed'])
    toc

  end  %of pair loop

  %could remove filtm from matrix structure to save space.

  return

