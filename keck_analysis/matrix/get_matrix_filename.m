function fn=get_matrix_filename(coaddopt)

if isfield(coaddopt, 'rx')
  coaddopt.daughter=[coaddopt.daughter '_rx' coaddopt.rx];
end

% get the filename saving the maps
fname=get_map_filename(coaddopt);
if ~iscell(fname)
  fname={fname};
  wascell=0;
else
  wascell=1;
end


%construct matrix filename
for i=1:length(fname)
  [coadddir ffname]=fileparts(fname{i});
  [dum, serdir]=fileparts(coadddir);
  ffname=strtok(ffname, '.');

  %if we have pair_subset
  if(isfield(coaddopt, 'pair_subset')&& any(coaddopt.pair_subset))
    pair_subset=['_pairs' num2str(min(coaddopt.pair_subset),'%.4d') 'to' ...
                 num2str(max(coaddopt.pair_subset),'%.4d')];
    if isfield(coaddopt, 'rx')
      pair_dir=[coaddopt.daughter(1:end-4) '/'];
    else
      pair_dir=[coaddopt.daughter '/'];
    end
  else
    pair_subset=[];
    pair_dir=[];
  end

  %write healmap specifics
  if isfield(coaddopt, 'siginterp')
    fn{i}=['matrixdata/matrices/' serdir '/' pair_dir ffname '_' ...
           coaddopt.sigmaptype '_' coaddopt.siginterp pair_subset '.mat'];
  else
    fn{i}=['matrixdata/matrices/' serdir '/' pair_dir ffname '_' ...
           coaddopt.sigmaptype pair_subset '.mat'];
  end
end

% make sure output sub-directories exist
if ~isdir(['matrixdata/matrices/' serdir])
  mkdir(['matrixdata/matrices/' serdir])
end

if ~isdir(['matrixdata/matrices/' serdir '/' pair_dir])
  mkdir(['matrixdata/matrices/' serdir '/' pair_dir])
end

if ~wascell
  fn=fn{1};
end

return

