function [pb,pe]=make_projection(fname,cutoff)
% [pb,pe]=make_projection(fname)
%
% Creates a projection matrix based on eigen modes
%
% INPUTS   :
%  fname   : name of eigenvalue mat file (from make_eigen.m)
%
% OUPUTS   :
%   pb     : a projection matrix made from the largest pureB
%            eigenmodes
%
%   pe     : a projection matrix made from the largest pureE
%            eigenmodes
%
%
% Note: You'll probably need to farm this out for 'bicep' resolution
% cov matrices (see ct_driver.m)
%
% eg
% fname='matrixdata/c_t/0704/healpix_red_spectrum_EnoB_lmax700_beamB2bbns_reob0704_eigen.mat'
% pb=make_projection(fname)
%

tic

if ~exist('cutoff','var') || isempty(cutoff)
  cutoff = 'pixby4';
end

%get solved eigen
disp(['loading ' fname])
load(fname);
disp(['forming projection matrix...'])

% For immediate compatibility, lift a copy of covmatopt.reob to reob if this
% is a new-style matrix. In the future, other code should be made aware of
% using covmatopt.reob instead.
if exist('covmatopt','var')
  reob = covmatopt.reob;
end

%get var masks
%store for later so when calculating aps
%we don't get messed up by smoothing , etc.
load(reob.file, 'map');
reob.Qvar=map.Qvar;
reob.Uvar=map.Uvar;

%we'll use 1/4 the number of obs pixels
%as the number of eigenmodes to include
if ischar(cutoff)
  switch cutoff
    case 'pixby4'
      nmodes=floor(length(obs_pixels)/4);
  end
else
  % Find position of eigenvalue nearest the given cutoff value
  [dum,nmodes]=min(abs(l_b - cutoff));
end

%normalize
norm=sqrt(sum(b.^2,1));
normed=bsxfun(@times, b,1./norm);
clear b;

%select large eigvalue modes... these are B
b_norm=normed(:,end-nmodes+1:end);

%make projection
pb=b_norm*b_norm';
clear b_norm;

%select small eigvalue modes... these are E
e_norm=normed(:,1:nmodes);

%make projection
pe=e_norm*e_norm';
clear e_norm;clear normed

%save
fname=strtok(fname, '.');
if strfind(fname, '_eigen')
  fname=fname(1:strfind(fname, '_eigen'));
end
fname=[fname 'proj.mat'];
disp(['saving ' fname]);

% Store everything in a option structure instead. This needs to be cleaned
% up a bit going forward to take this an input, but for now, just construct
% the format we can use later.
projmatopt.sernum = covmatopt(1).sernum;
projmatopt.cutoff = cutoff;
projmatopt.obs_pixels = obs_pixels;
projmatopt.nmodes = nmodes;
projmatopt.reob = reob;
if exist('covmatopt','var')
  projmatopt.covmatopt = covmatopt;
end

saveandtest(fname, '-v7.3', 'm','projmatopt','pb','pe')
setpermissions(fname);
toc
return
