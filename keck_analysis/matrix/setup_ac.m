function ac=setup_ac(m,ind)

% make accumulation arrays for pair sum
ac(1).wsum=sparse(zeros(m.ny,m.nx));    % pair-sum map weights
ac(1).wz=sparse(zeros(m.ny,m.nx));
ac(1).wwv=sparse(zeros(m.ny,m.nx));

ac(1).sitime=sparse(zeros(m.ny,m.nx));  % nominal int time
ac(1).switime=sparse(zeros(m.ny,m.nx)); % weighted int time
ac(1).swmax=sparse(zeros(m.ny,m.nx));   % maximum weight

% make accumulation arrays for pair diff
ac(1).w=sparse(zeros(m.ny,m.nx));
ac(1).wzdiff=sparse(zeros(m.ny,m.nx));
ac(1).wcz=sparse(zeros(m.ny,m.nx));
ac(1).wsz=sparse(zeros(m.ny,m.nx));

ac(1).wcc=sparse(zeros(m.ny,m.nx));
ac(1).wss=sparse(zeros(m.ny,m.nx));
ac(1).wcs=sparse(zeros(m.ny,m.nx));
ac(1).wwccv=sparse(zeros(m.ny,m.nx));
ac(1).wwssv=sparse(zeros(m.ny,m.nx));
ac(1).wwcsv=sparse(zeros(m.ny,m.nx));

ac(1).ditime=sparse(zeros(m.ny,m.nx));
ac(1).dwitime=sparse(zeros(m.ny,m.nx));
ac(1).dwmax=sparse(zeros(m.ny,m.nx));

% expand 1st dim to number of pairs
for j=1:length(ind.a)
  ac(j,:)=ac(1,:);
end

return
