function filename=prep_fullsky_map(prep_opt)
% filename=prep_fullsky_map(prep_opt)
%  
% This functions is designed to be used to prep a full sky map for
% use with the Reob matrix (reduc_map2bicep)
% this driver:
% 1.Finds alms using anafast
% 2.Rotates Coords with alteralms (if necessary)
%   and Beam smooths
% 3.regenerates cut sky maps with synfast
% 
% INPUTS:
%   prep_opt.in_file  :  name for file containing input map
%           .in_beam  : (0 default - nothing deconvolved) beam of input map 
%                       to be deconvolved in arcmin, or b_l.fits file
%           .in_mask  : ('' default = no mask) apply this fits mask prior to extracting the alms
%           .in_coord : (1 default) assume that the in_file needs to be converted from G to C
%  
%   prep_opt.out_beams: ('B2bbns' default) size of out_beams to convol 
%                       output with (if >1 entries, should be a cell)
%                       can be '...fits' file containing Bls
%           .out_nside: (default 512) healpix nside of output maps 
%           .out_nlmax: (Default is 2.5x out_nside) max l to include.
%                       careful when changing this from its default: too
%                       high an out_nlmax causes problems.
%           .out_roll : (none default) roll off the alms of the output map smoothly
%                       with a cosine taper in the l-range roll(1)->roll(2), e.g. roll=[350,400]
%           .out_cut  : (1 default) cuts the outputmaps to our patch, or
%                       0 - returns full sky, or
%                       cut.theta1='-70.', etc. (see below) - define a box to return
%  
%   prep_opt.dry_run   :  (0 default) only return the output filename, do nothing else
%  
% e.g.:
%  
% the deprojection map for K2014_95:
%   prep_opt.in_file = 'input_maps/planck/planck_maps/HFI_SkyMap_100_2048_R1.10_nominal.fits';
%   prep_opt.in_beam = 'input_maps/planck/planck_beam/b_l_100GHz.fits';
%   prep_opt.out_beams= 'Kuber100rev1';
%   fn = prep_fullsky_map(prep_opt);
%   system_safe(['mv ',fn,' input_maps/planck/planck_derivs_nopix'])
%  
% the WMAP9 kband map - preparation for reobservation:
%   prep_opt.in_file = '/n/bicepfs1/bicep2/pipeline/input_maps/wmap9/wmap_band_iqumap_r9_9yr_K_v5.fits'
%   prep_opt.in_beam = '/n/bicepfs1/bicep2/pipeline/input_maps/wmap9/bl/bl_k_wmap_9yr.fits'
%   prep_opt.in_mask = '/n/bicepfs1/bicep2/pipeline/input_maps/wmap9/masks/wmap_point_source_catalog_mask_r9_9yr_v5.fits'
%   prep_opt.out_roll = [375,425];
%   prep_opt.out_nlmax = 450;
%   prep_fullsky_map(prep_opt)
%  
%  NOTES on synfast, anafast and alteralm:
%  a summary shuffeling thourgh the online healpix documentation and our mods to synfast:
%  
%  anafast: the simplest of the three progs. Just transits a map into alm - does not apply
%           or undo any pixel window functions or beam smoothings. Has the option to apply a mask.
%  
%  alteralm: Tweaks a set of alm. 
%            * Deconvolves and convolves with a beam. 
%            * Rotates the coordinate system between Galactic and Celestial. 
%            * The alms file usually comes with an NSide entry in its header coming from 
%              the maps they were created from. alteralms will always undo the corresponding 
%              pixel window function from the alms. However, it will then apply a pixel window 
%             function corresponding to the nsmax (nsmax_out) argument given. If nsmax=0 is set
%             no final pixel window function is applied.
%  
%  synfast: Makes healpix maps and random alms
%           * if alms are handed in, it makes those into a map
%           * a random seed can be used to make random alms and the corresponding map
%           * a beam smoothing can be applied to the map and alms
%           * alms and map always match one another (smoothing). However, the NSide of the
%             the map must not necessarily be high enough to resolve the highest l (lmax) of the
%             underlying alms
%           * synfast usually applies the pixel window function corresponding to the NSide of the
%             created map. (read on...)
%           * The application of the beam smoothing and the pwf can be switch off
%             using "apply_windows = f". You cannot switch off the pwf alone. 
%             The beam smoothing can effectively be switched off using beam=0 (beam_file=0).
%           * We have changed the source code of synfast so that we have the option
%             to not apply any pixel window function using windowfile = none but still
%             apply beam smoothing.             

% fills in defaults and does some basic checking and massaging:
prep_opt = get_default_prepopt(prep_opt)

% check if the fits file is compressed:
if(strcmp(prep_opt.in_file(end-2:end),'.gz'))
  % and shave off the ending:
  prep_opt.in_file=prep_opt.in_file(1:end-3);
  isgz = true;
end

% generate filename of output
filename = get_filename(prep_opt);

% finish if dry_run
if prep_opt.dry_run  
  return
end

% if input filename is compressed decompress while keeping
% original file and remove the decompressed version at the end
if(exist('isgz','var'))
  disp('decompressing input file');
  prep_opt.in_file_tmp=strrep(prep_opt.in_file,'.fits',['_',gen_stamp,'.fits']);
  system_safe(sprintf('gunzip -c %s.gz > %s',prep_opt.in_file,prep_opt.in_file_tmp));
else
  prep_opt.in_file_tmp=prep_opt.in_file;
end

% run a basic check: the out_nlmax should not be higher than the maxium ell
% that can be extracted from the input map:
hmap_props=get_hmap_properties(prep_opt.in_file_tmp);
if (prep_opt.out_nlmax > hmap_props.nside*2.5)
  error(['out_nlmax=',num2str(prep_opt.out_nlmax),' exceeds 2.5*nside=',num2str(hmap_props.nside*2.5),' of input map'])
end

% make sure temp directory exists
direc=strfind(filename, '/');
if(~isempty(direc))
  if ~exist(filename(1:max(direc)),'dir')
    system_safe(['mkdir ' filename(1:max(direc))]);
  end
  tempdir=[filename(1:max(direc)) '/temp/'];
else
  if ~exist(filename,'dir')
    system_safe(['mkdir ' filename]);
  end
  tempdir=[filename '/temp/'];
end

if ~exist(tempdir,'dir')
  system_safe(['mkdir ' tempdir]);
end

%check if input map has wonky units
prep_opt.in_file_tmp_2=[tempdir '/map_',gen_stamp,'.fits'];
tweak_units(prep_opt.in_file_tmp, prep_opt.in_file_tmp_2);

% prepare a smooth roll-off of the alms if requested (beyond beam b_l)
% out_roll will be passed to synfast as final beam smoothing
% parameter - if '0' there is no additional smoothing
out_roll = '0';
if ~isempty(prep_opt.out_roll)
  out_roll=[tempdir '/b_l_roll_',gen_stamp,'.fits'];
  make_rolloff_file(out_roll,prep_opt.out_roll)
end

%-------------------------------------------
%1.)find alms from map
%-------------------------------------------
tmp_alms=[tempdir '/alms_',gen_stamp,'.fits'];
% since nside should either stay the same or be downgraded
% use the final nlmax to start with
run_anafast(prep_opt.in_file_tmp_2, tmp_alms, prep_opt.out_nlmax,[],[],prep_opt.in_mask);

beam=prep_opt.out_beams{1};
for i=1:length(prep_opt.out_beams)
  % replace the previous beam with the current one:
  filename=strrep(filename,beam,prep_opt.out_beams{i});

  beam=prep_opt.out_beams{i};
  if strcmp(beam,'none')
    beam = 0;
  else
    beam = get_bl_filename(beam);
  end

  %-------------------------------------------
  %2.)rotate coords with alteralms, correct for in_beam and pixel window
  %-------------------------------------------
  tmp_alms_rot=[tempdir '/alms_rot_',gen_stamp,'.fits'];
  
  % alteralm per default removes the input's pixel window function. The re-application
  % of the pwf is avoided by setting the last argument to 0:
  run_alteralm(tmp_alms, tmp_alms_rot,prep_opt.in_beam,beam,prep_opt.in_coord,0);
  
  %-------------------------------------------
  %3.)make map from alms
  %------------------------------------------- 
  % avoid having two job filling the same file
  filename_tmp=strrep(filename,'.fits',['_',gen_stamp,'.fits']);
  run_synfast([],[],tmp_alms_rot,prep_opt.out_nside,filename_tmp,[],prep_opt.out_cut,out_roll,prep_opt.out_nlmax);
  system_safe(['mv ',filename_tmp,' ',filename]);
end

%remove temp files we made
if isempty(whos('global','synfast_debug'))
  system_safe(sprintf('rm %s %s %s',tmp_alms,tmp_alms_rot,prep_opt.in_file_tmp_2));
  if ~strcmp(out_roll,'0')
    system_safe(sprintf('rm %s ',out_roll));
  end
end
return
%-------------------------------------------
%All code below is just to remove Q,U if there weren't in input map, ...
%    but were created by synfast
%-------------------------------------------

%if we only had a T map, set Q/U enties in map to zero
%(We have to make them in the run_synfast step because stefan's cut ...
%    sky synfast doesn't work with them turned off)

% get keywords for original file
info=fitsinfo(prep_opt.in_file_tmp);
xhdr=info.BinaryTable.Keywords;

% Old method of determining presence of Q/U wasn't working for
% Planck files so change to...

% look for TTYPE
pol=false;
for i=1:length(xhdr)
  if(strncmp(xhdr{i,1},'TTYPE',5))
    % look for Q as first letter
    if(strncmp(xhdr{i,2},'Q',1))
      pol=true;
    end
    %some planck maps have "column" rather than Q...dont remove
    %Q/U from these either
    if(strncmp(xhdr{i,2},'column02',1))
      pol=true;
    end
  end
end

if ~pol % if we don't have input polarization, zero out these fields in map
  warning('Zeroing Q/U in output file');

  in_map=fitsread(filename, 'bintable');
  in_info=fitsinfo(filename);
  out_map=in_map;

  qumaps=[3,4,6,7,9,10,12,13,15,16,18,19];
  for ii=qumaps
    out_map{ii}=zeros(size(in_map{ii}));
  end

  % write outs
  % avoid having two jobs filling the same file
  filename_tmp=strrep(filename,'.fits',['_',gen_stamp,'.fits']);
  fitswrite_bintable(out_map,in_info,filename_tmp)
  system_safe(['mv ',filename_tmp,' ',filename]);

end  %of pol false case

% remove decompressed file (compressed remains)
if(exist('isgz','var'))
  system_safe(sprintf('rm %s',prep_opt.in_file_tmp));
  system_safe(sprintf('rm %s',prep_opt.in_file_tmp_2));
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function filename = get_filename(prep_opt)
  filename=prep_opt.in_file(1:max(strfind(prep_opt.in_file, '.')-1));
  filename=[filename  '_nside' sprintf('%04d',prep_opt.out_nside) '_nlmax' sprintf('%04d',prep_opt.out_nlmax) '_b' prep_opt.out_beams{1} '.fits'];
  if ~isempty(prep_opt.in_mask)
    filename = strrep(filename,'_nside','_mskd_nside');
  end
  if ~isempty(prep_opt.out_roll)
    filename = strrep(filename,'.fits',sprintf('_r%1dt%1d.fits',prep_opt.out_roll(1),prep_opt.out_roll(2)));
  end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tweak_units(file_in, file_out)

%adjusts the units of header so that alteralms won't do something ...
%    screwy

ii=fitsinfo(file_in);

unit=get_unit(ii.BinaryTable(1).Keywords);

%this will assume K units even if only TUNIT1 or TUNIT2 is K.  But should it be
%possible that one will say K_CMB and the other will say uK_CMB?
switch unit
  case 'K_CMB'
    disp('WARNING: converting K_CMB to uK, this may cause problems')
    data=fitsread(file_in, 'BinTable');
    for j=1:length(data)
      %sometimes there are crazy high pixel values in map that
      %blow up when we convert to uK from K... this was true in
      %PSM FFP8 143Ghz map
      bad_ind{j}=find(abs(data{j})>1d15);
      if ~isempty(bad_ind{j})
        data{j}(bad_ind{j})=median(data{j}(:));
      end
      %convert from K to uK
      data{j}=data{j}*1d6;
      % fitswrite_bintable requires column vector data, but some FITS files
      % may write data as matrix for each field. Do as read_fits_map does and
      % convert to column vectors before writing.
      if size(data{j},1) ~= numel(data{j})
        data{j} = cvec(data{j}');
      end
    end
    ii.BinaryTable.Keywords=tweak_unit(ii.BinaryTable.Keywords);
    fitswrite_bintable(data,ii,file_out);
  case 'mK,thermodynamic'
    disp('WARNING: converting mK to uK, this may cause problems')
    data=fitsread(file_in, 'BinTable');
    for j=1:length(data)
      data{j}=data{j}*1d3;
      % fitswrite_bintable requires column vector data, but some FITS files
      % may write data as matrix for each field. Do as read_fits_map does and
      % convert to column vectors before writing.
      if size(data{j},1) ~= numel(data{j})
        data{j} = cvec(data{j}');
      end
    end
    ii.BinaryTable(1).Keywords=tweak_unit(ii.BinaryTable(1).Keywords);
    ii.BinaryTable = ii.BinaryTable(1);
    fitswrite_bintable(data,ii,file_out);
  otherwise
    system_safe(['ln -s $(readlink -m ' file_in ') ' file_out]);
end 


return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function unit = get_unit(hdr)
for ii =1:size(hdr,1)
  if any(strfind(hdr{ii,1},'TUNIT2')) | any(strfind(hdr{ii,1},'TUNIT1')) 
    unit=hdr{ii,2};
    return
  end
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function hdr = tweak_unit(hdr)
for ii =1:size(hdr,1)
  if any(strfind(hdr{ii,2},'K_CMB')) 
    hdr{ii,2}='uK';
  end
  if any(strfind(hdr{ii,2},'mK,thermodynamic')) 
    hdr{ii,2}='uK';
  end
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function prep_opt=get_default_prepopt(prep_opt)
if(~isfield(prep_opt,'in_file'))
  error('An input file must be specified')
end
if(~isfield(prep_opt,'in_beam'))
  prep_opt.in_beam = 0;
end
if(~isfield(prep_opt,'in_mask'))
  prep_opt.in_mask = '';
end
if(~isfield(prep_opt,'in_coord'))
  prep_opt.in_coord = 1;
end

if(~isfield(prep_opt,'out_beams'))
  prep_opt.out_beams={'B2bbns'};
end
if(isnumeric(prep_opt.out_beams))
  error('beams must be string or cell of strings')
end
if(~iscell(prep_opt.out_beams))
  prep_opt.out_beams={prep_opt.out_beams};
end

if(~isfield(prep_opt,'out_nside'))
  prep_opt.out_nside=512;
end
if(~isnumeric(prep_opt.out_nside))
  prep_opt.out_nside=str2num(prep_opt.out_nside);
end

if(~isfield(prep_opt,'out_nlmax'))
  prep_opt.out_nlmax=2.5*prep_opt.out_nside;
end                                     
if(~isnumeric(prep_opt.out_nlmax))
  prep_opt.out_nlmax=str2num(prep_opt.out_nlmax);
end

if(~isfield(prep_opt,'out_roll'))
  prep_opt.out_roll =[];
end

if(~isfield(prep_opt,'out_cut'))
  prep_opt.out_cut=1;
end
if(isnumeric(prep_opt.out_cut))
  switch prep_opt.out_cut
    case 1
      prep_opt=rmfield(prep_opt,'out_cut'); % avoid err mess
      prep_opt.out_cut.theta1='-70.';
      prep_opt.out_cut.theta2='-45.';
      prep_opt.out_cut.phi1='-55.';
      prep_opt.out_cut.phi2='55.';
    case 0
      prep_opt.out_cut=[];
  end
end

if(~isfield(prep_opt,'dry_run'))
  prep_opt.dry_run=0;
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_rolloff_file(tmpfilename,roll)
  % get a proxy fits file:
  ff = 'aux_data/beams/beamfile_20130222_sum.fits';
  fi = fitsinfo(ff);
  bl = fitsread(ff,'Bintable');
  % tukeywin is a cosine roll but double sided
  win =tukeywin(roll(2)*2,(roll(2)-roll(1))/roll(2));
  % cut it down
  win = win(length(win)/2:end); 
  % fill with zeros to typical length:
  win(end+1:length(bl{1}))=0;
  % write out
  fitswrite_bintable({win},fi, tmpfilename)
return
