function [ct dat]=calc_mapcovariance_band(map,snum,lmin,lmax,beam,dat)
%function [ct dat]=calc_mapcovariance_band(map,snum,lmin,lmax,beam=31[,dat])
%
% Calculate the theoretical covariance matrix derivative wrt a flat bandpower
% Equivalent to covariance of a spectrum for which l*(l+1)/2/pi*C_l is flat and
% is equal to 1/bandwidth between lmin and lmax, and zero elsewhere
%
% Inputs:
%  map:   Map definition string, as per get_map_defn
%  snum:  The number of the particular spectrum to use
%         Follows load_cmbfast convention: 1=TT, 2=TE, 3=EE, 4=BB
%  lmin:  Lower limit of l band
%  lmax:  Upper limit of l band
%  beam:  Beam width in arcminutes at which to smooth (default 31)
%  dat:   Structure of pre-computed data from a previous call (optional)
%         Reusing data will save computation time in consecutive calls
%         When absent, all the data is recomputed based on the 'map' input
%         NB: when present, 'map' is ignored and taken to be the
%         same as the call in which 'dat' was generated
%         When using 'dat', 'lmax' can't be larger than in the previous call
%
%         This means to use dat, you should work from high-l down to low-l
% 
% Outputs:
%  ct:   The 3*Npix by 3*Npix covariance matrix, with block-repreentation:
%              [TT,  TQ,  TU; ...
%               TQ', QQ,  QU; ...
%               TU', QU', UU];
%        Within each block, ordering is according to map2vect
%  dat:  Structure of precomputed data, to speed up future calls
%        NB: can only be reused on the same map type and not for larger lmax
%
% Example:
%  >> [ct dat] = reduc_calcmapcovariance_band('bicepcoarse',1,301,335,31)
%  >> ct2 = reduc_calcmapcovariance('',1,266,300,31,dat)

% TODO may be useful to have flag to calculate only polarization sub-matrix
% TODO may be useful to have a low-memory mode that doesn't use 'dat' and
%      calculates on the fly rather than in advance
% TODO may want to use pixel-windowing, to get effective averaging over pixels

if (~exist('beam','var') || isempty(beam))
  beam = 31;
end

[lat lon] = pixel_centers(map);

% spectrum is zero, except between [lmin, lmax] (lmax-lmin+1 points)
% where it has value 2*pi/l/(l+1)/(lmax-lmin+1)
% NB: first index is l=0, so index = l+1
spec.l = (0:lmax)';
spec.Cs_l = zeros(length(spec.l),9);
spec.Cs_l(lmin+1:lmax+1,snum) = 1/(lmax-lmin+1);
spec.C_l = zeros(length(spec.l),9);
spec.C_l(:,snum) = spec.Cs_l(:,snum)*2*pi./spec.l./(spec.l+1);
spec.C_l(1,snum) = 0; % correct NaN from divide by zero

spec = window_spectrum(spec, [], beam, lmax);

if (~exist('dat','var') || isempty(dat))
  [cts dat] = covmat_theory(lat, lon, spec, lmax); 
else
  cts = covmat_theory([], [], spec, lmax, dat); 
end

% convert cts from sub-matrix form to ct in full-matrix form
ct = [cts.TT,  cts.TQ,  cts.TU; ...
      cts.TQ', cts.QQ,  cts.QU; ...
      cts.TU', cts.QU', cts.UU];

