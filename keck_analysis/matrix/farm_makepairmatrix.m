function farm_makepairmatrix(tags,mapopt,farmopt)
%function farm_makepairmatrix(tags,mapopt,farmopt)
%
%Wrapper utility around reduc_matrix which intelligently calls farmit
%for each task as required.
%
%INPUTS
%  tags        Tags for which a pairmatrix should be produced.
%
%  mapopt      mapopt structure which specifies options used to produce
%              each pairmatrix. See reduc_matrix for more info.
%
%  farmopt     Structure of options which control the farm jobs which are
%              produced.
%
%              .dofarm     Defaults to true. If false, then run interactively
%                          (useful for testing).
%
%              .maxtime    Maximum time to request for job. Defaults to 300
%                          minutes for BICEP2 and 900 minutes for Keck.
%
%              .mem        Maximum memory to request for job. Defaults to
%                          20GB for BICEP2 and 40GB for Keck.
%
%              .queue      Queue to submit job to. Defaults to using both
%                          'general,serial_requeue'.
%
%              .submit     Defaults to false. If false, then the farmfile is
%                          placed on disk but not submitted to the queue. This
%                          is particularly useful if using babysitjobs().
%
%OUTPUTS
%
%EXAMPLES
%
%  tags = get_tags('cmb2014',{'has_tod','has_cuts'});
%
%  mapopt.sernum = '9999real';
%  mapopt = get_default_mapopt(mapopt);
%
%  farmopt.mem = 35000;
%  farmopt.maxtime = 900;
%
%  farm_makepairmatrix(tags, mapopt, coaddopt, farmopt);
%  % Keep the queue full with 100 jobs, checked very 10 minutes.
%  babysitjobs(9999, 'wait10', [], [], 100);
%

  % Common default values
  default_queue = 'general'; % SLURM queues

  % Other default values are experiment dependent
  switch get_experiment_name
    case {'bicep2'}
      default_maxtime = 300;   % minutes
      default_mem     = 20000; % MBytes
    case {'keck'}
      default_maxtime = 1000;  % minutes
      default_mem     = 30000; % MBytes
  end


  %%%% Process/verify all the inputs %%%%


  % Set default farming options
  if ~exist('farmopt','var')
    farmopt = struct();
  end
  if ~isfield(farmopt,'dofarm') || isempty(farmopt.dofarm)
    farmopt.dofarm = true;
  end
  if ~isfield(farmopt,'maxtime') || isempty(farmopt.maxtime)
    farmopt.maxtime = default_maxtime;
  end
  if ~isfield(farmopt,'mem') || isempty(farmopt.mem)
    farmopt.mem = default_mem;
  end
  if ~isfield(farmopt,'queue') || isempty(farmopt.queue)
    farmopt.queue = default_queue;
  end
  if ~isfield(farmopt,'submit') || isempty(farmopt.submit)
    farmopt.submit = false;
  end

  % Fill in mapopt with defaults if not already set
  mapopt = get_default_mapopt(mapopt);


  %%%% Setup the execution environment %%%%


  % Make sure there's a directory to put the farmfiles into
  system_safe(sprintf('mkdir -p %s', fullfile('farmfiles',mapopt.sernum(1:4))));


  %%%% Process each phase and submit jobs as necessary %%%%


  for ii=1:numel(tags)
    tagz = tags{ii};
    [p,ind] = get_array_info(tagz);

    % Get babysitjobs-compatible farmfile and jobnames
    pairname = get_pairmatrix_filename(tagz, mapopt);
    [dum,farmid,dum] = fileparts(pairname);
    [farmfile,jobname] = farmfilejobname(mapopt.sernum, ...
        'makepairmatrix', farmid);

    % Next, check the filesystem to see if the completed data product
    % already exists on disk
    if exist_file(pairname)
      fprintf(1,'pairmatrix %s already exists\n', pairname);
      continue
    end

    % Either submit the appropriate options to farmit or execute the command
    if farmopt.dofarm
      farmit(farmfile, ...
          'reduc_matrix(tagz,mapopt)', ...
          'jobname',jobname, ...
          'maxtime',farmopt.maxtime, ...
          'mem',farmopt.mem, ...
          'queue',farmopt.queue, ...
          'submit',farmopt.submit, ...
          'overwrite','skip', ...
          'var',{'tagz','mapopt'});
    else
      reduc_matrix(tagz, mapopt);
    end

  end % ii over numel(tags)

  return
end
