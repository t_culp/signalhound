function [lat_out, lon_out]=pixel_subset(lat, lon, subsets)
%
%Purpose: to return a subset of the full map so that the covariance
%    matrix can be calculated over it

if (~exist('subsets','var')) || isempty(subsets)
  subsets= false;
end

%make sure we have correct row/col
lat=reshape(lat, 1,length(lat));
lon=reshape(lon, 1,length(lon));

%this is the whole map
lat_out.a=lat;
lon_out.a=lon;

if subsets
  %this is all map rows with constant lat
  rows=unique(lat);
  for i=1:length(rows)
    a=find(lat==rows(i));
    lat_out.subset{i}=lat(a);
    lon_out.subset{i}=lon(a);  
  end
else
  lat_out.subset{1}=lat;
  lon_out.subset{1}=lon; 
end

return