function pointm=point_mat(ind,j,d, p, mapind, m, mapopt)

%Creates a matrix, (pointm.a), of dimensions mapind x npixels
%(npixels is the number of pixels in your output map), that takes a
%vector of tod data and bins it into a map vector.
%
%This is matrix version of hfill2.m
%
%INPUTS:
%    ind: detector channel for this matrix
%    d: tod data   (ie from reduc_initial)
%    p: focal plane structure
%    mapind: locations in d with map data
%    m: structure of desired output map
%
%OUTPUTS:
%    pointm.a: output matrix
%
%if mapopt.diffpoint='obs', them output pointm(1)= for A detector
%                                       pointm(2)= for B detector
%e.g.
%load('data/real/201107/20110704C01_dk293_tod.mat')
%mapind=make_mapind(d,fs);
%m=get_map_defn('bicep');
%mapopt.diffpoint='obs';
%[p,ind]=get_array_info('20110704C01_dk293', [],[],[],mapopt.diffpoint);
%pointm=point_mat(ind, 3, d, p, mapind, m, mapopt);
%
%
%JET (20120301)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%display('point_mat...')
%tic
switch mapopt.diffpoint
  case 'ideal'
    ind=ind.a(j);
  case 'obs'
    ind=[ind.a(j), ind.b(j)];
end

%in case we haven't formed the loopup table
if(~isfield(m, 'ind_lookup')  )
  [coordx, coordy]=meshgrid(m.x_tic,m.y_tic);
  [m,m.v_x_tic]=map2vect(m,coordx);
  [m,m.ind_lookup]=vect2map(m,m.ind);
end


%loop over both (or one) ind
for j=1:length(ind)
  i=ind(j);

  % xaxis of map can be ra or az
  switch m.binning
    case 'az'
      %disp('binning in az/dec')
      x0=d.pointing.hor.az(mapind);
    case 'ra'
      x0=d.pointing.cel.ra(mapind);
  end
  y0=d.pointing.cel.dec(mapind); dk=d.pointing.cel.dk(mapind);


  % find ra/dec trajectory for this feed
  [y,x]=reckon(y0,x0,p.r(i),p.theta(i)-90-dk);
  if(m.lx>0)
    x(x<0)=x(x<0)+360;
  end


  % alternate projection
  switch m.proj
    case 'ortho'
      [x,y]=radec_to_sinproj(x,y,m.racen,m.deccen);
    case 'tan'
      [x,y]=radec_to_tanproj(x,y,m.racen,m.deccen);
    case 'arc'
      [x,y]=radec_to_arcproj(x,y,m.racen,m.deccen);
    case 'radec'
      % do nothing
  end


   %Make pointing matrix
   pointm(j).a=make_pointing_matrix(x,y,m);


   % calc the pol angle for the detector
   th=p.theta(i)-dk;
   thref=p.chi_thetaref(i)-dk;
   alpha=chi2alpha(x0,y0,p.r(i),th,p.chi_mean(i),thref);

   % Ganga alpha_h
   ap=(alpha+p.beta(i))*pi/180;

   % take sin and cos
   pointm(j).c=cos(2*ap); pointm(j).s=sin(2*ap);

end  %loop over j

%toc
return



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function pointm=make_pointing_matrix(x,y,m)
%  (x,y)   are timestream coordintes of dector pair
%   m        the map structure

%makes a pointing matrix (mapind X nx*ny X #pairs) that conatins
% pointing info for each pair.

%m.y_tic-y
disty=abs(bsxfun(@minus, m.y_tic, y));
%m.x_tic-x
distx=abs(bsxfun(@minus, m.x_tic, x));

%minimize
%might need to worry about
%having two locations
locx=bsxfun(@eq, distx', min(distx'));
locy=bsxfun(@eq, disty', min(disty'));

[ix,jx]=find(locx);
[iy,jy]=find(locy);

%lookup locations
loc_temp=sub2ind(size(m.ind_lookup),iy,ix);
loc=m.ind_lookup(loc_temp);

%put a 1 wherever we are pointing
pointm=sparse([1:length(x)],loc,ones(1,length(x)),length(x),m.nx*m.ny);

%should be able to make above smaller by using logical

%removing pointings that are not in our map
%otherwise, if a scan goes off the edge of the map
%the pointing matrix fills in at the edges...that's bad!
out = x>m.hx | x<m.lx | y<m.ly | y>m.hy;
in_bounds=sparse(1:length(x),1:length(x),~out);

%remove samples that are off map
pointm=in_bounds*pointm;

return

