function month=phase2month(ph)

%creates a list of monthly tags, phases and names

for i=1:length(ph.name)
  phname=ph.name{i};
  monthname{i}=phname(1:6);
end

[u,a,c]=unique(monthname);

for i=1:length(u)
  ind=find(c==i);
  month.name{i}=u{i};
  month.ph{i}=ph.name(ind);
  month.tags{i}=[ph.tags{ind}];
end
  
return