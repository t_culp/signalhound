function [m, map]=vect2map(m,mapv)
%vect2map
%Inverse of map2vect

if(~exist('mapv', 'var'))
  map=[];
  mapv=[];
end

if(~isempty(mapv))
  map=reshape(mapv,m.nx, m.ny)';
end

return

