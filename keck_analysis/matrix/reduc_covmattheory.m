function [cij dat]=reduc_covmattheory(lat,lon,covmatopt,dat)
% [cij dat]=reduc_covmattheory(lat,lon,covmatopt,dat)
%
% Calculate theoretical covariance between a set of pixels in a map. Mostly
% follows astro-ph/0012120v3 Appendix A.
% 
% INPUTS
%   lat,lon  The coordinates (in degrees) of the pixel centers. These can be
%            obtained with the pixel_centers().
% 
%   dat      Optional. Structure of pre-computed data from a previous call.
%            When absent, all the data is recomputed based on lat/lon.
% 
%            NB! When present, lat and lon are ignored and taken to be the
%                same as the call in which 'dat' was generated, and 'lmax'
%                can't be larger than in the previous call.
% 
%   reob     Defaults to false, in which case no extra processing occurs.
%            If not false, then must be a structure describing how the
%            covariance matrix is to be reobserved. The structure must
%            contain the following fields:
% 
%              .file     Path to the reobserving matrix.
% 
%              .field    The covariance term to reobserve. Valid options are
%                        'TT', 'TQ', 'TU', 'QQ', 'QU', and 'UU'.
% 
% OUTPUTS
%   cij      Structure of computed pixel covariance information. If reob is
%            false, then all fields described below will exist, otherwise
%            when reobserved, only the field given in reob.field will be
%            returned.
% 
%              .TT .TQ .TU    The Npix by Npix covariance matrices, where
%              .QQ .QU .UU    Npix = length(lat).
% 
%              .tt_z .tq_z    Angular separation spectrum (as a function of
%              .qq_z .uu_z    dat.u_z).
% 
%            The full 3Npix by 3Npix matrix can be constructed as:
%               [cij.TT,  cij.TQ,  cij.TU; ...
%                cij.TQ', cij.QQ,  cij.QU; ...
%                cij.TU', cij.QU', cij.UU];
% 
%   dat       Structure of precomputed data, to speed up future calls.
% 
%             NB! The data can only be reused on the same map (including the
%             same lat/lon).
% 
% EXAMPLE
%

if ~exist('dat','var')
  dat = [];
end

if ~isstruct(lat)
  [lat,lon] = pixel_subset(lat, lon, covmatopt.subsets);
end

covmatopt = get_default_covmatopt(covmatopt);

% Time the entire operation
t_tot = tic;

% Load spectra (if necessary), and clean/truncate before use
if ischar(covmatopt.spec)
  disp(['Loading spectra: ' covmatopt.spec])
  specfile = covmatopt.spec;
  covmatopt.spec = window_spectrum(specfile, [], covmatopt.beam, ...
      covmatopt.lmax);
  covmatopt.spec.file = specfile;
else
  disp(['Using spectrum: ' covmatopt.spec.file])
end
covmatopt.spec.C_l(isnan(covmatopt.spec.C_l)) = 0;
lmax = covmatopt.lmax;
cll = covmatopt.spec.l(1:lmax+1);  % NB: first element is 0
ctt = covmatopt.spec.C_l(1:lmax+1,1);
cte = covmatopt.spec.C_l(1:lmax+1,2);
cee = covmatopt.spec.C_l(1:lmax+1,3);
cbb = covmatopt.spec.C_l(1:lmax+1,4);

% Compute data (angles and legendre functions) if not supplied
if isempty(dat)
  dat.a = covmat_angles(lat, lon);
  % decompose z into unique list and array of indexes
  sz = size(dat.a.z);
  f1=find(dat.a.z)';  %use z to avoid zeros
  [ii,jj,ss]=find(dat.a.z);
  [dat.u_z dum dat.a.z] = unique(dat.a.z(f1));
  clear dum;
  dat.l= calc_legendre(dat.u_z, lmax, true);
end

% Calculate unrotated covariances, as functions of z
% Note: this loop works much faster than a matrix-form computation like:
%   cij.tt=sum(diag((2*cll+1)/(4*pi).*ctt) * dat.l.P);
disp('summing unrotated covariances');
tic
cij.tt_z=zeros(size(dat.l.z));
cij.tq_z=zeros(size(dat.l.z));
cij.qq_z=zeros(size(dat.l.z));
cij.uu_z=zeros(size(dat.l.z));
for i=1:length(cll)
  nrml = ((2*cll(i)+1)/(4*pi));
  cij.tt_z = cij.tt_z + nrml * dat.l.P(:,i)*ctt(i);
  cij.tq_z = cij.tq_z - nrml * dat.l.F10(:,i)*cte(i);
  cij.qq_z = cij.qq_z + nrml * ...
          (dat.l.F12(:,i)*cee(i)-dat.l.F22(:,i)*cbb(i));
  cij.uu_z = cij.uu_z + nrml * ...
          (dat.l.F12(:,i)*cbb(i)-dat.l.F22(:,i)*cee(i));
end
toc % summing unrotated covariances

% Rotate the polarization to global reference frame
disp('rotating and constructing full covariance matrix');
tic
alpha = 2*dat.a.alpha(f1) * pi/180;
c2a = cos(alpha)'; % Make column to match shape of cij.*_z below
s2a = sin(alpha)';
clear alpha;

% Construct transpose
alphat = dat.a.alphat'; % Tranpose to use same indices as non-transpose
alphat = 2*alphat(f1) * pi/180;
c2at = cos(alphat)';
s2at = sin(alphat)';
clear alphat;

% Construct full matrix in blocks
cij.TT = sparse(ii,jj, cij.tt_z(dat.a.z), sz(1),sz(2));
cij.TQ = sparse(ii,jj, c2at.*cij.tq_z(dat.a.z), sz(1),sz(2));
cij.TU = sparse(ii,jj, -s2at.*cij.tq_z(dat.a.z), sz(1),sz(2));
cij.QQ = sparse(ii,jj, c2at.*c2a.*cij.qq_z(dat.a.z) ...
            + s2at.*s2a.*cij.uu_z(dat.a.z), sz(1),sz(2));
cij.UU = sparse(ii,jj, s2at.*s2a.*cij.qq_z(dat.a.z) ...
            + c2at.*c2a.*cij.uu_z(dat.a.z), sz(1),sz(2));
cij.QU = sparse(ii,jj, -s2at.*c2a.*cij.qq_z(dat.a.z) ...
            + c2at.*s2a.*cij.uu_z(dat.a.z), sz(1),sz(2));

% Calculate the transpose entries
if ~islogical(covmatopt.subsets) || ~covmatopt.subsets
  cij.QT = sparse(ii,jj, c2a.*cij.tq_z(dat.a.z), sz(1),sz(2));
  cij.UT = sparse(ii,jj, -s2a.*cij.tq_z(dat.a.z), sz(1),sz(2));
  cij.UQ = sparse(ii,jj, -s2a.*c2at.*cij.qq_z(dat.a.z) ...
            + c2a.*s2at.*cij.uu_z(dat.a.z), sz(1),sz(2));
else
  cij.QT = cij.TQ';
  cij.UT = cij.TU';
  cij.UQ = cij.QU';
end

clear s2a s2at c2a c2at ii jj ss;
toc % rotating and constructing full covariance matrix

% Get out file name
fname = get_ct_filename(covmatopt);

% If we have specified a reobserving matrix, apply it to ct
if isstruct(covmatopt.reob)
  disp('Reobserving covariance...')
  % Free up unneeded memory
  clear dat;

  % Reobserve the covariance and replace cij with rcr.
  rcr=multiply_rcr(covmatopt, cij);
  cij=rcr;

else
  disp(['Saving ' fname])
  % need to save with '-v7.3' to save large cij matrix.
  saveandtest(fname, '-v7.3', 'covmatopt', 'lat', 'lon', 'cij');
  setpermissions(fname);
end

disp('Computation of covariance complete');
toc(t_tot);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function angles=covmat_angles(lat, lon)
% function angles=covmat_angles(lat, lon)
%
% calculate angles for covariance matrix, between pixels at [lat lon]
%
% Outputs, each an array of size (# of pixels) by (# of pixels):
%   angles.z     : (cosine of) angular separation between pixels
%   angles.alpha : azimuth of pixels with respect to each compute

% other angles using matlab's builtin distance
display('computing distance and angle between pixels...')
tic

% Figure out if we are a subset
issubset = (length(lat.a) ~= length(lat.subset));

%find which indecies of matrix we are doing
c=find(ismember(lat.a,lat.subset));

% preallocate
dis = zeros(length(lat.subset),length(lat.a));
alpha = zeros(length(lat.subset),length(lat.a));
alphat = zeros(length(lat.a),length(lat.subset));

% distance() faster than explicitly coding the method in
% astro-ph/0012120v3
for i=1:length(lat.subset)
  [dis(i,:), alpha(i,:)]=distance(lat.subset(i), lon.subset(i), lat.a, lon.a);
end

% Here is where we want to calulate the angles of other pairs (so we have
% c2a' and c2a). This is necessary when we are doing subset matrices and
% don't want to calculate the whole thing.
if issubset
  for i=1:length(lat.a)
    [dis_dum, alphat(i,:)]=distance(lat.a(i), lon.a(i), lat.subset, lon.subset);
    clear dis_dum;
  end
else
  alphat=alpha;
end

% Remove points that have basically the same distance to within pixel
% precision. This makes the legendre calculation tractable for healpix
% maps. We maintain precision to 10^-4 degrees
dis=dis*1d4;
dis=round(dis)/1d4;

% Find absolute value of dot product.
z=cosd(dis);
clear dis;

% set azimuth of antipodes explicitly to zero
antipodes = (abs(z+1) < 1e-13);
display(['correcting for ' num2str(sum(sum(antipodes)),'%d') ' antipodal azimuths']);
alpha(antipodes) = 0;
alphat(antipodes) = 0;
clear antipodes;

% Here we change the sign of alpha and alphat so we go from Healpix to IAU
% convention of U. See last paragraph of pg 15 in Tegmarks paper.
alpha=-alpha;
alphat=-alphat;


if issubset
  % Setup so we can effiently use sparse
  s1=size(z,1);
  s2=size(z,2);
  z=reshape(z,1,s1*s2);
  alpha=reshape(alpha,1,s1*s2);
  alphat=reshape(alphat,1,s1*s2);

  % Put in sparse
  [x1,y1]=meshgrid([1:s2],c);
  [x2,y2]=meshgrid(c,[1:s2]);

  c1=reshape(y1,1,s1*s2);
  c2=reshape(y2,1,s1*s2);
  i1=reshape(x1,1,s1*s2);
  i2=reshape(x2,1,s1*s2);

  angles.z=sparse(c1,i1,z,size(lat.a,2),size(lat.a,2));
  angles.alpha=sparse(c1,i1,alpha,size(lat.a,2),size(lat.a,2));
  angles.alphat=sparse(c2,i2,alphat,size(lat.a,2),size(lat.a,2));
  clear i1 c1 i2 c2;

% We have a full matrix
else
  angles.z=z;
  angles.alpha=alpha;
  angles.alphat=alpha;
end
clear z alpha alphat;

toc
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function rcr = multiply_rcr(covmatopt, cij)
% Concatenate unobserved theory fields to produce full matrix.
tic
disp('concat unobserved covariance...')
ct = [cij.TT, cij.TQ, cij.TU;
      cij.QT, cij.QQ, cij.QU;
      cij.UT, cij.UQ, cij.UU];
% Input structure no longer needed
clear cij;
toc

%get reob matrix
tic
disp(['Loading ' covmatopt.reob.file])
load(covmatopt.reob.file);
sernum=coaddopt.sernum(1:4);
clear coaddopt;
toc

%fill in to rcr matrix
tic
disp(['making rcr for ' covmatopt.reob.field])

%make inverse var weight matrices, so that double float precision on matrix
%multplication isn't dominated by poorly observed regions. This
%would lead to rcr begin non-symmetric.
% This is where the assumed apodization mask is "baked" in - could
% be anyhting one wanted...
wT=sparse(1:length(matrixc.Tvar),1:length(matrixc.Tvar),1./matrixc.Tvar);
wQ=sparse(1:length(matrixc.Qvar),1:length(matrixc.Qvar),1./matrixc.Qvar);
wU=sparse(1:length(matrixc.Uvar),1:length(matrixc.Uvar),1./matrixc.Uvar);

switch covmatopt.reob.field
  case 'TT'
     rcr.TT=(wT*matrixc.TTTQTU)*ct*(wT*matrixc.TTTQTU)';
   case 'TQ'
     rcr.TQ=(wT*matrixc.TTTQTU)*ct*(wQ*matrixc.QTQQQU)';
   case 'TU'
     rcr.TU=(wT*matrixc.TTTQTU)*ct*(wU*matrixc.UTUQUU)';
   case 'QQ'
     rcr.QQ=(wQ*matrixc.QTQQQU)*ct*(wQ*matrixc.QTQQQU)';
   case 'QU'
     rcr.QU=(wQ*matrixc.QTQQQU)*ct*(wU*matrixc.UTUQUU)';
   case 'UU'
     rcr.UU=(wU*matrixc.UTUQUU)*ct*(wU*matrixc.UTUQUU)';
end
toc

tic
fname = get_ct_filename(covmatopt);
disp(['Saving ' fname])
saveandtest(fname, '-v7.3', 'covmatopt','rcr','m');
setpermissions(fname);
toc
end
