function [b,l_b]=make_eigen(Ename,Bname)
% [b,l_b]=make_eigen(Ename,Bname)
%
% Solves eigen problem from two (E & B) covariance matrices.
%
% INPUTS   :
% Ename    :  filename of covariance matrix to use as E
% Bname    :  filename of covariance matrix to use as B
%
% OUPUTS   :
%  b       : the generalized eigenvetors
%            (ordered from pureE (small l_b) to pureB (large l_b).
% l_b      : the eigenvalues
%
%
% Note: You'll probably need to farm this out for 'bicep' resolution
% cov matrices (see ct_driver.m)
%
% e.g.
% Ename='matrixdata/c_t/0704/healpix_red_spectrum_EnoB_lmax700_beamB2bbns_reob0704.mat'
% Bname='matrixdata/c_t/0704/healpix_red_spectrum_BnoE_lmax700_beamB2bbns_reob0704.mat'
% [b,l_b]=make_eigen(Ename,Bname)
%

%E cov matrix
disp(['loading ' Ename])
E = load(Ename);
%B cov matrix
disp(['loading ' Bname])
B = load(Bname);

% Need to support covariances before and after addition of covmatopt.
if isfield(E, 'covmatopt')
  covmatopt(1) = E.covmatopt;
  covmatopt(2) = B.covmatopt;
else
  covmatopt(1).reob = E.reob;
  covmatopt(2).reob = B.reob;
end
% The reobserved covariances existed in both versions, so no check needed.
rcrE = E.rcr;
rcrB = B.rcr;
% Load the map definition into the local scope
if isfield(E, 'm')
  m = E.m;
end

clear E B

%make rcrE and rcrB symmetric
%(they are slightly non-symettric from numerical precision)
issym=@(x) all(all(tril(x)==triu(x).'));

if (~issym(rcrB))
  mdif=max(max((rcrB-rcrB')./rcrB));
  disp('rcrB is not symmteric, maximumally non symetric element is')
  disp(['fractionally ' num2str(mdif) ' from sym...'])
  disp('forcing symmetric by averaging across diag')
  rcrB=(rcrB+rcrB')./2;
end

if (~issym(rcrE))
  mdif=max(max((rcrE-rcrE')./rcrE));
  disp('rcrE is not symmteric, maximumally non symetric element is')
  disp(['fractionally ' num2str(mdif) ' from sym...'])
  disp('forcing symmetric by averaging across diag')
  rcrE=(rcrE+rcrE')./2;
end


%find where the actual map really is
% we don't want to waste memory on empty rows of the matrix
ss=sum(rcrE,2);
not_zero=ss~=0;
obs_pixels=find(not_zero);

%take only observed pixelss
rcrE=rcrE(obs_pixels,obs_pixels);
rcrB=rcrB(obs_pixels,obs_pixels);

%create regularizing vector
scale=(mean(diag(rcrE))+mean(diag(rcrB)))./2;
Nm=sparse(1:length(rcrE),1:length(rcrE), 1)*scale./1d2; %this might work


%solve eigenvalue problem
tic
disp(['solving generalized eigenvalue using eig()'])
[b,l_b1]=eig(rcrB+Nm, rcrE+Nm);
toc

%take the diag
l_b=diag(l_b1);

%clear some mem
clear l_b1;clear rcrB;

%grab the m from reob.file if we dont have it
if ~exist('m', 'var')
  load(reob.file, 'm');
end

%save
Ename=strtok(Ename, '.');
if strfind(Ename, 'E')
  root=[Ename(1:strfind(Ename, 'E')-2)];
  leaf=[Ename(strfind(Ename, 'E')+4:end)];
  fname=[root leaf '_eigen.mat'];
else
  fname=[Ename '_eigen.mat'];
end
disp(['saving ' fname]);
saveandtest(fname, '-v7.3', 'b', 'l_b', 'obs_pixels', 'covmatopt', 'm');
setpermissions(fname);

return
