function ac=sparsify(ac)

fnames=fieldnames(ac);
for f=1:length(fnames)
  if iscell(getfield(ac,fnames{f}));
     cc=getfield(ac,fnames{f});
    for c=1:length(cc)
     cc_new{c}=sparse(cc{c});
     end %cell elements
     ac=setfield(ac,fnames{f},cc_new);
   else
     ac=setfield(ac,fnames{f},sparse(getfield(ac,fnames{f})));
   end %cell if
end %fnames loop

return
