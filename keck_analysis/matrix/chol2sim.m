function maps=chol2sim(fname, sims)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Uses at cholecky decomp cov matrix to create sims
% (you can make a cholesky decomp of a cov matrix with cov2chol)
%
% INPUTS :
% sims   : integer array of sims you'd like (ie [31:37])
% fname  : filename of cholesky decomp covariance (string)
% 
%  eg
% fname='matrixdata/c_t/0704/healpix_camb_planck2013_r0_lmax700_beamB2bbns_reob0704_chol.mat'
% maps=chol2sim(fname,1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load(fname, 'CC', 'reob', 'obs_pixels')

%use our usual seeds
seeds=get_seedlist();

%load reob matrix for Var maps and coaddopts
load(reob.file, 'map', 'coaddopt', 'm')

%thow out cuts for sims since they are big
if isfield(coaddopt, 'c')
  coaddopt=rmfield(coaddopt, 'c');
  coaddopt=rmfield(coaddopt, 'hsmax');
end

%get sernum
sernum=coaddopt.sernum(1:4);

for i=sims

  %make filename
  beg=max(strfind(reob.file, '/'))+5;
  last=strfind(reob.file, '.')-1;
  outname=[sernum '/' num2str(i, '%.3d') '1' reob.file(beg:last)];

  %append cov information
  beg=max(strfind(fname, '/'))+1;
  last=strfind(fname, 'reob')-2;
  outname=[outname '_' fname(beg:last)];

  %create a sim
  randn('seed', seeds(i))
  rr=randn(size(CC,1),1);
  sim=rr'*CC;

  %expand into 2d map
  simmap=expand_vect(m,sim, obs_pixels);

  %adjust by appropriate Var maps
  if regexp(reob.file, 'final')
    if ~isfield(coaddopt, 'ukpervolt')
      disp('Using default ukpervolt')
      ukpervolt=get_ukpervolt();
    else 
      ukpervolt=coaddopt.ukpervolt;
    end
    map.Q=simmap.Q.*map.Qvar./ukpervolt;
    map.U=simmap.U.*map.Uvar./ukpervolt;
  else
    map.Q=simmap.Q.*map.Qvar;
    map.U=simmap.U.*map.Uvar;
  end

  %save this sim  
  disp(['saving maps/' outname])
  saveandtest(['maps/' outname], 'map', 'm', 'coaddopt')
  setpermissions(['maps/' outname]);

  %store for output
  maps{i}=map;

end  

