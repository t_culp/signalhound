function reduc_processnewtags_perphase(tags,farmstuff,serial_requeue,more_mem,cpfilestobicepfs1)
% reduc_processnewtags_perphase(tags,farmstuff,serial_queue,more_mem,cpfilestobicepfs1)
% 
% coadds and plots per-phase maps for reducplots
%
% If input tag list are phase tags ('20110516E_dk???') then make pairmaps from
% all existing TODs.  If input tag list is a regular tag list
% ('20110516E01_dk???') then use exactly the tags requested.
%
% input: tags           = list of tags to process
%        farmstuff      = 0/1 whether to farm jobs or not
%                         (On spud, set to max number of jobs to run at once)
%        serial_requeue = 0 (default) use queue 'general'
%                       = 1 use queue 'serial_requeue'
%                         (Matlab doesn't know BLCR checkpointing, so might be
%                         willing to wait for 'general' queue if likely to be
%                         preempted.)
%        more_mem       = 0 (default) request 8 GB of memory
%                       = 1 request 16 GB of memory
%
% ex. tags=get_tags('cmb2011');
%     ph=taglist_to_phaselist(tags);
%     reduc_processnewtags_perphase(ph.name,1);

if nargin == 0
    error('Not enough input arguments. Please specify tag list.')
end

if nargin < 2
    farmstuff=0;
end

if ~exist('serial_requeue','var')
  serial_requeue=[];
end
if isempty(serial_requeue)
  serial_requeue=0;
end

if ~exist('more_mem','var')
  more_mem=[];
end
if isempty(more_mem)
  more_mem=0;
end

if ~exist('cpfilestobicepfs1','var')
  cpfilestobicepfs1=[];
end

% record the scansets that fail
errorfile='./reducplots_failed.txt';
if exist(errorfile); movefile(errorfile,[errorfile '.tmp']); end

if farmstuff
  LICENSE = 'bicepfs1:17'; % 1500 slots -> 1500/17 = 88 jobs
  MAXTIME = 1440;

  if serial_requeue
    QUEUE='serial_requeue';
  else
    QUEUE='general';
  end
  if more_mem
    MEM=16000;
  else
    MEM=8000;
  end
end

% Phase tags or regular tags?
[dum,dum,scanset,dum]=parse_tag(tags);

if isempty(scanset) | all(isnan(scanset))
  % phase tags
  ph.name=tags;

  % form tag list to cover all data that is present in the requested phases
  ri=get_run_info;
  [riday,riphase]=parse_tag(ri.tag);

  for i=1:length(ph.name);
    [day,phase]=parse_tag(ph.name{i});
    tagind=find(riday==day & strcmp(riphase,phase));
    tagz=ri.tag(tagind);
    type=ri.type(tagind);
    tagz=tagz(strncmpi(type,'gal',3) | strncmpi(type,'cmb',3));
    
    % don't make map using pairmaps that don't exist
    clear tagind
    for j=1:numel(tagz)
      tagind(j)=~isempty(dir(['pairmaps/0000/real/',tagz{j},'_*.mat']));
    end
    tagz=tagz(tagind);
    ph.tags{i}=tagz;
  end
else
  % regular tags
  ph=taglist_to_phaselist(tags);
end

% cut out phases with no tags
hastags=[];
for i=1:numel(ph.name)
  if ~isempty(ph.tags{i})
    hastags=[hastags,i];
  end
end
ph=structcut(ph,hastags);


for i=1:length(ph.name);

  disp(ph.name{i})
  tagz=ph.tags{i};

  if farmstuff

    % Control number of jobs running at once on SPUD where there is no queueing
    % architechture 
    pid=myprocesses('farmit\(');
    while numel(pid)>=farmstuff
      pid=myprocesses('farmit\(');  
      pause(1);
    end

    [farmfile,jobname] = farmfilejobname('BICEP', 'reduc_batch2', ...
        ph.name{i});
    farmit(farmfile, 'reduc_batch2(tagz,1,cpfilestobicepfs1);', ...
           'jobname',jobname, 'queue',QUEUE, 'mem',MEM, ...
           'maxtime',MAXTIME,'license',LICENSE, 'account','bicepdata_group', ...
           'var',{'tagz','cpfilestobicepfs1'},'overwrite','skip');

  else
    try
      reduc_batch2(tagz,1,cpfilestobicepfs1);
    catch exception
      disp([ ph.name{i} ' failed']);
      fid=fopen(errorfile,'a');
      fprintf(fid,'%s\t%s\n',ph.name{i},exception.message)
      fclose(fid)
    end

  end

end

