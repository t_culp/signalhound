function reduc_pntsrcmask(tags,src)
% reduc_pntsrcmask(tags,src)
%
% Construct a logical mask the same size as d.lockin.adcData which
% indicates when each channel was close to any of the point sources in
% structure src
%
% See documentation for get_mask for definition of src structure
%
% e.g.
%     tags=get_tags('gal2011');
%     src.name='galplane'; src.r=1.5;
%     reduc_pntsrcmask(tags,src);

for f=1:length(tags)
  tag=tags{f}
  
  load(sprintf('data/real/%s/%s_tod',tag(1:6),tag));

  % Get array info
  [p,ind]=get_array_info(tag);
  
  % Get deck angle
  dk=d.pointing.cel.dk;  

  % Initialize mask from experiment-appropriate detector
  % timestream registers.
  if isfield(d.antenna0,'bolo')&&~isempty(d.antenna0.bolo.mag)
    % BICEP1
    psm=false(size(d.antenna0.bolo.mag));
  elseif isfield(d,'mce0')&&~isempty(d.mce0.data.fb)
    % BICEP2/Keck
    psm=false(size(d.mce0.data.fb));
  else
    error('reduc_pntsrc cannot find detector time streams in register map.');
  end;
  mapind=make_mapind(d,fs);

  % for each channel
  % make sure channel list is a row vector
  ind.gl=(ind.gl(:))';
  for j=ind.gl
    %[i j]
    % find ra/dec trajectory for this feed for entire set of scans
    [y,x]=reckon(d.pointing.cel.dec(mapind),d.pointing.cel.ra(mapind),p.r(j),p.theta(j)-90-dk(mapind));  
    x(x<0)=x(x<0)+360;
    mp=get_mask(x,y,src);
    psm(mapind,j)=mp;
  end

  save(sprintf('pntsrcmask/%s_pntsrcmask',tag),'psm');
end

return
