function reduc_final_datarel_newdat(rf,of)
% reduc_final_datarel_newdat(rf)
%
% Write "newdat" format data file as included with COSMOMC for B03 and
% CBI
%
% If arg "of" exists will be used as name of output file - otherwise
% same name as input used
%
% e.g.
% reduc_final_datarel_newdat('0303real_pairmap_cut1_filtp3_weight3_gs_jack0_debias')

if(~exist('of'))
  of=rf;
end

% directory to output to
od='newdat/';

% load the data set
load(sprintf('final/%s',rf));

% get standard set of chibins
chibins=get_chibins(10);
cb=chibins{1};
n=length(cb);

% ordering of newdat is TT EE BB EB TE TB
% Matlab pipeline uses TT TE EE BB TB EB
% to go from newdat order to my order we do
ro=[1,3,4,6,2,5];

% get the bandpower covariance from sims
r=get_bpcov(r);

% strip off diags in corr mats
for i=1:length(r.corr)
  r.corr{i}=triu(tril(r.corr{i},2),-2);
end

% make the all-with-all bp covariance matrix (for the wanted output bins)
c=r.sim(cb,ro,:); % here we translate to newdat spectral ordering
x=size(c);
c=reshape(c,[x(1)*x(2),x(3)]);
bpcm=cov(c');
% zero off diags
bpcm=zero_offdiags(2,6,bpcm);

% open the output file
fh=fopen(sprintf('%s/%s.newdat',od,of),'w');

% first line is base name of associated files in "windows" subdir 
fprintf(fh,'%s_\n',of);

% second line is how many bins of each pol type in
% the order TT EE BB EB TE TB
% newdat format allows for EB and TB and Chiang et al and Brown et al
% include them
fprintf(fh,'%d %d %d %d %d %d\n',n,n,n,n,n,n);

% next section is band selection - select them all
fprintf(fh,'BAND_SELECTION\n');
fprintf(fh,'0 0\n');
fprintf(fh,'1 %d\n',n);
fprintf(fh,'1 %d\n',n);
fprintf(fh,'0 0\n');
fprintf(fh,'0 0\n');
fprintf(fh,'0 0\n');

% abs calibration uncer
% calon/off(1/0), recal value (in temp), cal error (in power)
fprintf(fh,'0 1.00 0.056\n')

% beam uncer
% beamerr on/off(1/0),  beam FWHM (arcmin), beam err (arcmin)
fprintf(fh,'0 45.0 0.8\n');

% likelihood option line liketype
%  0 == treat all bands gaussian
%  1 == treat all bands offset lognormals (using offsets provided)
%  2 == treat according to flag in last column of bandpower (0== 
% gaussian, 1==offset lognormal)
fprintf(fh,'2\n');

ss={'TT','TE','EE','BB','TB','EB'};
for c=1:length(ro)
  
  % map from newdat order to our order
  i=ro(c);
  
  % 'TT', 'EE' etc. flag telling the code which pol type is listed
  fprintf(fh,'%s\n',ss{i});

  for j=1:n
    b=cb(j);
    
    switch i
      case {1,3,4}
	z=1;
      case 2
	z=0;
    end
    
    % calc the x factor
    
    % for auto spectra this should just be the mean of the noise only
    % sims
    %x=mean(r.noisim(b,i,:),3);
    
    % when we are using cross spectra (frequency or temporal) this is
    % no longer right - but we can calc from the signal sims - see:
    % http://find.spa.umn.edu/~quad/logbook/20080530/bp_uncer.pdf
    x=mean(r.sig(b,i,:),3)*std(r.noi(b,i,:),[],3)/std(r.sig(b,i,:),[],3);
    
    % list of band power info:
    % band num per type, C_b, dC_b+,dC_b-, noise offsets, lmin, lmax,
    % likeflag
    % HARD CODE 17.5 IN NEXT LINE NEEDS FIXING 
    fprintf(fh,'%2d %8.2e %8.2e %8.2e %8.2e %8.2f %8.2f %1d\n',...
	j,r.real(b,i),r.derr(b,i),r.derr(b,i),x,r.l(b)-17.5,r.l(b)+17.5,z);
  end
  
  % Contaldi says:
  % dump of block diagonal normalized covariance matrix for that 
  % particular pol type, dimension must be the same as list immediately 
  % above. This is included purely for the practical purpose of reading off 
  % bin-to-bin correlations without having to dig them out of the full 
  % covariance matrix. It is not used by cosmomc and I guess I should make 
  % this optional at some point but this information is always very handy 
  % for people to see.
  for j=1:n
    b1=cb(j);
    for k=1:n
      b2=cb(k);
      fprintf(fh,'%6.3f ',r.corr{i}(b1,b2));
    end
    fprintf(fh,'\n');
  end
end

% write the bpcm
for i=1:length(bpcm)
  for j=1:length(bpcm)
    fprintf(fh,'%.3e ',bpcm(i,j));
  end
  fprintf(fh,'\n');
end

fclose(fh);

% write the bpwf window files
% squawk if the bpwf wasn't saved in the final/XX.mat file
if ~exist('bpwf')
  disp('##########################')
  disp(['bpwf was not saved in ' rf])
  disp(['load bwpf, save it to ' rf ' and try again!!!!'])
  disp('##########################')
  return
end

% no silly scaling needed for newdat format!
% bpwf.wl=bpwf.Cs_l;
% actually, yes a silly scaling is needed.  Cosmomc expects windows
% in w_l^b/l format, where Cs_l = (l+.5)/(l+1)*(w_l^b/l)
% this is just a small change in the normalization of the bpwf's
for k=1:size(bpwf.Cs_l,2)
for l=1:size(bpwf.Cs_l,3)
bpwf.wl(:,k,l)=bpwf.Cs_l(:,k,l).*((bpwf.l'+1)./(bpwf.l'+.5));
end
end

keyboard

% Contaldi complains that small values cause trouble on readin
ind=abs(bpwf.wl)<1e-20;
bpwf.wl(ind)=0;

% write out the bpwfs - cosmomc requires one file per bandpower, per spectrum
f=0;

% for each spectum
for c=1:length(ro)
  
  % map from newdat order to our order
  i=ro(c);
  
  % for each used band
  for j=cb
  
    % file number steps through continuously
    f=f+1;
    fh=fopen(sprintf('%s/windows/%s_%d',od,of,f),'w');
    b=cb(i);
  
    switch i
      case 1 % TT
	for l=1:length(bpwf.l)
	  fprintf(fh,'%4d %11.4e %11.4e %11.4e %11.4e\n',bpwf.l(l),bpwf.wl(l,j,1),0,0,0);
	end
	
      case 2 % TE
	for l=1:length(bpwf.l)
	  fprintf(fh,'%4d %11.4e %11.4e %11.4e %11.4e\n',bpwf.l(l),0,bpwf.wl(l,j,2),0,0);
	end

      case 3 % EE
	for l=1:length(bpwf.l)
	  fprintf(fh,'%4d %11.4e %11.4e %11.4e %11.4e\n',bpwf.l(l),0,0,bpwf.wl(l,j,3),bpwf.wl(l,j,6));
	end

      case 4 % BB
	for l=1:length(bpwf.l)
	  fprintf(fh,'%4d %11.4e %11.4e %11.4e %11.4e\n',bpwf.l(l),0,0,bpwf.wl(l,j,5),bpwf.wl(l,j,4));
	end
	
      case {5,6} % TB or EB
	for l=1:length(bpwf.l)
	  fprintf(fh,'%4d %11.4e %11.4e %11.4e %11.4e\n',bpwf.l(l),0,0,0,0);
	end

    end
  
    fclose(fh);
    
  end
end

if(0)
% now write simple output file
fh=fopen(sprintf('%s/BICEP_May09.dat',ob),'w');

sprintf('%s/BICEP_May08.dat',ob)

fprintf(fh,' BICEP May 2009 data as described in Chiang el al astro-ph/\n');
fprintf(fh,'\n');
fprintf(fh,' Columns are:\n');
fprintf(fh,' 1) band center ell\n');
fprintf(fh,' 2) bandpower value l(l+1)C_l/2pi in uK^2\n');
fprintf(fh,' 3) bandpower uncertainty\n');
fprintf(fh,' 4) bandpower expectation value under LCDM\n');
fprintf(fh,'\n');
fprintf(fh,' Say something about BB debiasing process\n');
fprintf(fh,'\n');

for j=1:4
  fprintf(fh,'%s\n',ss{j});
  for i=1:n
    b=cb(i);
    fprintf(fh,'%4.0f %8.2f %8.2f %8.2f\n',r.l(b),r.real(b,j),r.derr(b,j),r.expv(b,j));
  end
  fprintf(fh,'\n');
end

fclose(fh);
end

return

function bpcmp=zero_offdiags(nod,nspec,bpcm)
% bpcmp=zero_offdiags(nod,nspec,bpcm)
%
% Force offdiags to zero in multispectrum band power covariance matrix
%
% nod = "n off diags" to keep
% nspec = "number of spectra" in bpcm
%
% Note that keeps some inter-spectra covariances for selected
% secondaries - assuming  newdat spectral ordering (TT/EE/BB/EB/TE/TB)

n=length(bpcm)/nspec;

bpcmp=zeros(size(bpcm));

for i=0:(nspec-1)
  for j=0:(nspec-1)
    % pull out the submatrix
    x=bpcm(i*n+1:(i+1)*n,j*n+1:(j+1)*n);
    
    y=zeros(size(x));

    % only select secondary off diags
      
    % keep intra spectra main + n
    if(j==i)
      y=triu(tril(x,nod),-nod);
    end
    
    % these are tuned to be like Chiang et al data release - not sure
    % why they are as they are
    
    % keep TT/TE, EE/TE, EB/TB main + 1 only
    if((i==0 & j==4)|(i==4 & j==0) | (i==1 & j==4)|(i==4 & j==1) | (i==3 & j==5)|(i==5 & j==3))
      y=triu(tril(x,1),-1);
    end

    % keep TT/EE main only
    if((i==0 & j==1) | (i==1 & j==0))
      y=triu(tril(x,0),0);
    end
    
    % re-insert sub matrix
    bpcmp(i*n+1:(i+1)*n,j*n+1:(j+1)*n)=y;
  end
end

return
