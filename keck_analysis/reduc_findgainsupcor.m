function reduc_findgainsupcor2(tagz)
% reduc_findgainsupcor2(tagz)
%
% find the correction factor to compensate for loading gain supression
%
% re-work to get the correction to apply post elnod corr in reduc_applycal
%
% reduc_findgainsupcor(get_tags)
% reduc_findgainsupcor(setdiff(get_tags('2006use'),{'060630','060705','060707'}))

% needed because cs is an internal matlab function name
cs=1;

for z=1:length(tagz)
  tagz{z}
  
  % get the cal info
  load(['data/',tagz{z},'_calval']);
  
  % average for/back elnod gains
  if(rem(length(en.s),2)~=0)
    i=true(size(en.s)); i(end)=0;
    en=structcut(en,i);
    warning('elnods not paired - removing last');
  end
  g=reshape(en.g,[2,size(en.g,1)/2,size(en.g,2)]);
  t=reshape(en.t,[2,length(en.t)/2]);
  en.g=squeeze(mean(g)); en.t=mean(t)';
  
  cra(z)=cr;
  rca(z)=rc;
  ena(z)=en;
  csa(z)=cs;
  fsa(z)=fs; 
end

cr=structcat(1,cra);
rc=structcat(1,rca);
en=structcat(1,ena);
cs=structcat(1,csa);
fs=structcat(1,fsa);

% plot set of channels valid for start day
[p,ind]=get_array_info(tagz{1});

% convert from mjd to matlab datenum
do=datenum(2005,5,17)-53507;

% find mutual xlim
allt=[cr.a.t+do;cr.e.t+do;rc.t+do;en.t+do;cs.gr.t+do];
xl=[min(allt),max(allt)];

% last elnod of a run sometimes gets dropped leading to more calsrc
% than elnod
% find closest calsrc to each elnod and just use those hereafter
en.cs=interp1(cs.gr.t,cs.gr.par(:,:,1),en.t,'nearest');

% when calsrc amp is negative invert
i=en.cs<0;
en.cs(i)=-en.cs(i);

% plot the raw time series
figure(1)
clf; setwinsize(gcf,1000,1000);
set(gcf,'DefaultAxesColorOrder',[copper(length(ind.gl150));cool(length(ind.gl100))]);
subplot(2,1,1)
plot(en.t+do,en.cs(:,ind.gl),'.','MarkerSize',0.1);
ylabel('calsrc rot sinusoid amplitude (V)')
axis tight;
xlim(xl); xaxis_time(gca);
ylim([0,1.25]);
subplot(2,1,2)
plot(en.t+do,en.g(:,ind.gl),'.','MarkerSize',0.1);
ylabel('elnod (V/airmass)')
axis tight;
xlim(xl); xaxis_time(gca);
ylim([-100,0]);
gtitle('raw calsrc and elnod timeseries')

% plot the raw correlations
figure(2)
clf; setwinsize(gcf,1000,1000);
for i=ind.gl100
  subplot(8,8,i)
  plot(en.g(:,i),en.cs(:,i),'b.','MarkerSize',0.1);
  axis tight
  xlim([-60,-20]); ylim([0.2,1.3]);
  title(p.channel_name{i});
end
for i=ind.gl150
  subplot(8,8,i)
  plot(en.g(:,i),en.cs(:,i),'r.','MarkerSize',0.1);
  axis tight
  xlim([-60,-20]); ylim([0.2,1.3]);
  title(p.channel_name{i});
end
gtitle('raw calsrc vs elnod')

% channels have different gains and calsrc illumination is non-uniform
% normalize each calsrc timeseries to unit mean
cm=mean(en.cs,1);
cn=en.cs./repmat(cm,[size(en.cs,1),1]);

% plot correlation plots now
figure(3)
clf; setwinsize(gcf,1000,1000);
for i=ind.gl100
  subplot(8,8,i)
  plot(mean(en.g(:,ind.rgl100),2),cn(:,i),'b.','MarkerSize',0.1);
  axis tight
  xlim([-40,-20]); ylim([0.9,1.1])
  title(p.channel_name{i});
end
for i=ind.gl150
  subplot(8,8,i)
  plot(mean(en.g(:,ind.rgl150),2),cn(:,i),'r.','MarkerSize',0.1);
  axis tight
  xlim([-60,-20]); ylim([0.8,1.2])
  title(p.channel_name{i});
end
gtitle('norm calsrc vs elnod')

% we will apply gain sup corr in reduc_applycal after el nod rel gain
% calibration
% but I think we can regress each channel here against anything we
% like so long as we use the same quantity when calc gain corr in
% reduc_applycal - we are just seeking an empirical gain corr factor -
% it doesn't matter what the mechanism is - we just make the
% hypothesis that calsrc is actually stable and apparent change in it's
% amplitude is due to detector gain variation

% what is the best x axis?

% could be el nod val for each channel
%x=en.g;

% could be the mean el nod gain within each freq group
x(:,ind.gl100)=repmat(mean(en.g(:,ind.rgl100),2),[1,length(ind.gl100)]);
x(:,ind.gl150)=repmat(mean(en.g(:,ind.rgl150),2),[1,length(ind.gl150)]);

% could be the mean el nod gain at 150GHz
%x(:,ind.gl)=repmat(mean(en.g(:,ind.rgl150),2),[1,length(ind.gl)]);

% what is best y axis?

% could be norm cal src val for each channel
%y=cn;

% could be the mean cs val within each freq group
y(:,ind.gl100)=repmat(mean(cn(:,ind.rgl100),2),[1,length(ind.gl100)]);
y(:,ind.gl150)=repmat(mean(cn(:,ind.rgl150),2),[1,length(ind.gl150)]);

% settle on mean of elnod and calsrc within each freq band for x and y
% - this is conceptually clean and works just about as well as
% anything else tried

% for each channel
for i=ind.gl
  % regress
  X=[ones(size(x(:,i),1),1) x(:,i)];
  b(:,i)=regress(y(:,i),X);
  % correct
  sf=X*b(:,i);
  cc(:,i)=en.cs(:,i)./sf;
end

% plot uncorrected/corrected timeseries
figure(4)
clf; setwinsize(gcf,1000,1000);
set(gcf,'DefaultAxesColorOrder',[copper(length(ind.gl150));cool(length(ind.gl100))]);
subplot(2,1,1)
plot(en.t+do,en.cs(:,ind.gl),'.','MarkerSize',0.1);
ylabel('raw (V)')
axis tight;
xlim(xl); xaxis_time(gca);
ylim([0,1.25]);
subplot(2,1,2)
plot(en.t+do,cc(:,ind.gl),'.','MarkerSize',0.1);
ylabel('corrected')
axis tight;
xlim(xl); xaxis_time(gca);
ylim([0,1.25]);
gtitle('raw and corrected calsrc timeseries')

ffraw=std(en.cs)./mean(en.cs);
ffcor=std(cc)./mean(cc);

% plot distribution of fractional amplitude fluctuation
figure(5)
clf; setwinsize(gcf,600,400);
subplot(2,2,1); hfill(ffraw(ind.gl100),100,0,0.06); xlabel('100GHz before')
subplot(2,2,3); hfill(ffcor(ind.gl100),100,0,0.06); xlabel('100GHz after')
subplot(2,2,2); hfill(ffraw(ind.gl150),100,0,0.06); xlabel('150GHz before')
subplot(2,2,4); hfill(ffcor(ind.gl150),100,0,0.06); xlabel('150GHz after')
gtitle('distributions of calsrc fractional fluctuation')

disp(sprintf('sf100=%.3e + %.3e * men100',b(:,ind.gl100(1))));
disp(sprintf('sf150=%.3e + %.3e * men150',b(:,ind.gl150(1))));

[mean(ffcor(ind.gl100)) mean(ffcor(ind.gl150))]

return
