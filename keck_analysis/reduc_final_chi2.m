function reduc_final_chi2(rf,rfcopt,lmax,pl)
% reduc_final_chi2(rf,rfcopt,lmax,pl)
%
% Calc chisq of spectra
%
% reduc_final_chi2('sim005_filtp3_weight2_jack0',0,200)
%
% rf = filename to read
%
% rfcopt.dia      = (0) diagonalize the bandpowers (default no)
%       .doblind  = (0) zero out bb/tb/eb spectra
%       .pl_cross = ([1,2]) select two maps to display in the two auto columns
%                   the corresponding cross spectrum is selected automatically
%                   to be displayed in the 3rd and 4th (alternative cross) column
%                   3-entry array like [1,2,9] allows to select the columns freely
%       .diffspec = (0) difference the spectra (also see simtype option)
%       .simtype  = ('sim'),'simr','simd',etc - which signal sim to use
%                   for plotting, calculating the statistics and 
%                   *also the spectral jack*
%       .chibins  = overwrite default standard chibins with your choice
%       .mapname  = a string identifier, for instance {'BK14_95','BK14_150'}
%  
% lmax            = lmax of plots, also selects the default chibins to be
%                   get_chibins() for l<=300 and get_chibins(10) for l>300
%
% pl - always plots to screen - if pl=2 also makes png images
% 

if(~exist('rfcopt','var'))
  rfcopt=[];
end
if(~isfield(rfcopt,'dia'))
  rfcopt.dia=0;
end
if(~isfield(rfcopt,'doblind'))
  rfcopt.doblind=false;
end
if(~isfield(rfcopt,'pl_cross'))
  rfcopt.pl_cross=[1,2];
end
if(~isfield(rfcopt,'diffspec'))
  rfcopt.diffspec=false;
end
if(~isfield(rfcopt,'simtype'))
  rfcopt.simtype='sim';
end
% keep rfcopt.simr for backwards compatibility,
% use rfcopt.simtype='simr' instead.
if(isfield(rfcopt,'simr') && rfcopt.simr==1)
  rfcopt.simtype='simr';
end
if(~isfield(rfcopt,'mapname'))
  rfcopt.mapname={};
end
if(~exist('lmax','var')) || isempty(lmax)
  lmax=500;
end
if(~isfield(rfcopt,'chibins'))
  % get standard set of chibins
  if lmax>300
    rfcopt.chibins=get_chibins(10);
  else
    rfcopt.chibins=get_chibins();
  end
end
if(~exist('pl','var')) || isempty(pl)
  pl=1;
end

rf

% find jacktype
n=strfind(rf,'jack');
jacktype=rf(n(1)+4);

% load the data set
load(sprintf('final/%s',rf));

if(~strcmp(rfcopt.simtype,'sim'))
  % if requested replace the lensed-LCDM+noise sims with
  % lensed-LCDM+noise+r=0.2
  for i=1:length(r)
    r(i).sim=r(i).(rfcopt.simtype);
  end

  % add simr to the end of rf to identify it 
  % as compared to the normal sims
  rf=[rf,'_',rfcopt.simtype];
end

% pl_cross marks two of the na auto spectra, fetch the corresponding cross
% spectrum nc_i among the N=na+nc=na+(na-1)*na/2 spectra in the files:
N=size(r,1);
na = -0.5+sqrt(2*N+0.25);

% just do the same loop as during the creating of the xspectra
% first: expand out the mapname to the cross spectrum
nc_c=na+1;
if na>1
  for j=1:na-1
    for c=j+1:na
      rfcopt.mapname{nc_c}=[rfcopt.mapname{j},'x',rfcopt.mapname{c}];
      nc_c=nc_c+1;
    end
  end
end

% find the proper third index which points to the cross spectrum:
% do this only if the pl_cross does not already have three columns
% if it is handed with three columns arbitrary combinations can be plotted (special)
if size(rfcopt.pl_cross,2)~=3
  nc_c=na+1;
  if na>1
    for j=1:na-1
      for c=j+1:na
        % use here the min and max functions. In this way
        % one can swap the order of the colums that are plotted
        % by handing in for instance pl_cross = [2,1] instead of [1,2].
        if j==min(rfcopt.pl_cross(1:2)) && c==max(rfcopt.pl_cross(1:2))
          nc_i=nc_c;
        end
        nc_c=nc_c+1;
      end
    end
    % That are the three columns to be plotted:
    rfcopt.pl_cross=[rfcopt.pl_cross,nc_i];
  else
    rfcopt.pl_cross=[1];
  end
end

% fetch the proper rx assignment:
if numel(rfcopt.pl_cross)==1
  filename = {''};
else
  filename = {rfcopt.mapname{rfcopt.pl_cross(1)},rfcopt.mapname{rfcopt.pl_cross(2)},rfcopt.mapname{rfcopt.pl_cross(3)}};
end

% by default column names match the filename
mapname=filename;
% gets modified below for spectrum jack

% set crosses to minus for the experiment jack
if strcmp(jacktype,'f')
  for ii=1:length(mapname)
    mapname{ii}=strrep(mapname{ii},'x','-');
  end
end

% "Blind" B-mode real data for jack0 only
if jacktype=='0' && rfcopt.doblind
  for i=1:length(r)
    % these are the alternative cross spectra
    if size(r(i).real,2)==9
      r(i).real(:,[4:6,8,9])=NaN;
    else
      r(i).real(:,4:6)=NaN;
    end
  end
end

% difference the spectra if wanted to assess compatibility
if(rfcopt.diffspec)
  % just copy over the content of pl_cross into a,b,c to have the following lines
  % short. For B2,Keck,B2xKeck a=1,b=2 and c=3, c is always the cross spectrum:
  a=rfcopt.pl_cross(1); b=rfcopt.pl_cross(2); c=rfcopt.pl_cross(3);
  % do not replace the sim field with simr per default but use whatever was selected
  % with the rfcopt.simtype field.
  r=specjack(r,a,b,c); 
  
  % compose column names of plot
  mapname={[mapname{1},'-',mapname{3}],[mapname{1},'-',mapname{2}],[mapname{3},'-',mapname{2}]},
  
  % make comparable to "experiment" jack and others which divide
  % map by 2 - divide power by 4
  for i=1:length(r)
    r(i).real=r(i).real/4;
    r(i).sim=r(i).sim/4;
    r(i).noi=r(i).noi/4;
  end
  
  % flag as "spectral" jackknife
  opt.coadd.jacktype='s';
  jackn=strfind(rf,'jack')+4;
  if(all(rf(jackn)~='0'))
    error('Asked for spectral difference on non-jack0 spectra');
  end
  rf=strrep(rf,'jack0','jacks');
  
  % zero the model
  inpmod.Cs_l=zeros(size(inpmod.Cs_l));
end

% strip down so all sims being compared have same number of realizations
if(0)
  n=150;
  for i=1:length(r);
    r(i).sim=r(i).sim(:,:,1:n);
  end
end
disp(sprintf('%i realizations being considered',size(r(1).sim,3)))

% calc cov and derr
r=get_bpcov(r);

% diagonalize the bandpowers if wanted
if(rfcopt.dia==1)
  r=diag_bandpow(r,2);
end

if jacktype=='0'
  jtag='Signal';
elseif isempty(jacktype)
  jtag='experiment jackknife';
  jacktype='f';
else
  jtag=get_jackdef(jacktype);
end
if iscell(jtag)
  jtag=jtag{1};
end

% only open and resize windows that need it (for speed)
wantfigs=[1,2,3,4,5,6];
openfigs=findall(0,'type','figure');
winsize=[1260,840];
for j=wantfigs
  if ~ismember(j,openfigs)
    % create figure
    figure(j);
    % put them in the upper left because I don't like the way they span my dual monitor
    set(gcf,'Position',[0,winsize(2),winsize])
  else
    pos=get(j,'Position');
    sz=pos(3:4);
    if ~all(sz==winsize)
      setwinsize(j,winsize(1),winsize(2))
    end
  end
end

% clear all figures
for j=wantfigs
  set(0,'CurrentFigure',j)
  clf
end

% plot the spectra
if(length(r)>1)
  for j=1:length(rfcopt.pl_cross)
    set(0,'CurrentFigure',1);
    plot_spec(j,r(rfcopt.pl_cross(j)),inpmod,mapname{j},opt,lmax,rfcopt.chibins,0);
    set(0,'CurrentFigure',6);
    plot_spec(j,r(rfcopt.pl_cross(j)),inpmod,mapname{j},opt,lmax,rfcopt.chibins,1);
  end
else
  set(0,'CurrentFigure',1);
  plot_spec(1,r(1),inpmod,'150GHz',opt,lmax,rfcopt.chibins,0);
  set(0,'CurrentFigure',6);
  plot_spec(1,r(1),inpmod,'150GHz',opt,lmax,rfcopt.chibins,1);
end
set(0,'CurrentFigure',1);
gtitle(sprintf('%s spectra %s',jtag,rf),0.98,'none');
set(0,'CurrentFigure',6);
gtitle(sprintf('%s spectra %s',jtag,rf),0.98,'none');


% Plot the contributions to the error bars.
plot_errors(5,r,opt,lmax,sprintf('%s errors for %s',jtag,rf),rfcopt.pl_cross,mapname)

% Plot devs versus LCDM
if(jacktype=='0' && length(r)==1 && isfield(r,'expv'))
  
  r=calc_devs(r,rfcopt.chibins); 
  r=calc_chi(r,rfcopt.chibins); % (this places chibins into r)
  
  plot_devs(2,r,1,lmax,'',1,'[...]=LCDM',rfcopt.pl_cross,mapname)

  plot_chi(3,r,1,'',1,'[...]=LCDM',rfcopt.pl_cross,mapname);
  
  % plot mean of band power deviations for data and sims
  plot_hdevs(4,r,1,lmax,'',1,'[...]=LCDM',rfcopt.pl_cross,mapname)

  colstart=2;
else
  colstart=1;
end

% Plot devs versus null model
if length(r)==1

  % Set expectation value to zero
  for j=1:length(r)
    r(j).expv=zeros(size(r(j).real));
  end
  
  % Plot devs versus null model
  r=calc_devs(r,rfcopt.chibins);
  plot_devs(2,r,jacktype,lmax,sprintf('%s (d-m)/e where m is [...] for %s',jtag,rf),colstart, ...
            '[...]=null model',rfcopt.pl_cross,mapname)
  
  % plot chisq
  r=calc_chi(r,rfcopt.chibins);
  plot_chi(3,r,jacktype,sprintf('%s \\chi^2 versus [...] for %s',jtag,rf),colstart, ...
           '[...]=null model',rfcopt.pl_cross,mapname);
  
  % plot sum devs
  plot_hdevs(4,r,jacktype,lmax,sprintf('%s sum((d-m)/e) where m [...] for %s',jtag,rf),colstart, ...
             '[...]=null model',rfcopt.pl_cross,mapname)

  
  colstart=colstart+1;
  
end

% set expectation value to mean of s+n sims
for j=1:length(r)
  r(j).expv=nanmean(r(j).sim,3);
end

% Plot devs versus mean of s+n sims
r=calc_devs(r,rfcopt.chibins);
plot_devs(2,r,jacktype,lmax,sprintf('%s (d-m)/e where m is [...] for %s',jtag,rf),colstart, ...
          '[...]=mean of s+n sims',rfcopt.pl_cross,mapname)

% plot chisq
r=calc_chi(r,rfcopt.chibins);
plot_chi(3,r,jacktype,sprintf('%s \\chi^2 versus [...] for %s',jtag,rf),colstart, ...
          '[...]=mean of s+n sims',rfcopt.pl_cross,mapname)

% plot sum devs
plot_hdevs(4,r,jacktype,lmax,sprintf('%s sum((d-m)/e) where m is [...] for %s',jtag,rf),colstart, ...
          '[...]=mean of s+n sims',rfcopt.pl_cross,mapname)

% write out pngs
if(pl==2)
  for pp=1:6
    set(0,'CurrentFigure',pp);
    if strcmp(filename{1},'')
      printname=sprintf('final/%s_chi2_%i_%i.png',rf,lmax,pp);
    else
      printname=sprintf('final/%s_chi2_%i_%i_%sx%s.png',rf,lmax,pp,filename{1},filename{2})
    end
    mkpng(printname,1);
  end
end   

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_devs(fig,r,ymax,lmax,gt,col,colhead,pl_cross,mapname)

set(0,'CurrentFigure',fig);

if(length(r)>1)
  for j=1:length(pl_cross)
    plot_devs_sub(j,r(pl_cross(j)),[mapname{j} ', ' colhead],ymax,lmax);
  end
else
  plot_devs_sub(col,r(1),colhead,ymax,lmax);
end

gtitle(gt,0.98,'none');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_devs_sub(n,r,freq,ymax,lmax)

% sort the sim bandpowers lowest to highest
devs=sort(r.sdevs,3);
ns=size(r.sdevs,3);
% the levels in sorted bandpowers closest to -2,1,0,1,2 sigma
%t=round(ns*normcdf([-2:2],0,1));
ncdf = normcdf([-2:2],0,1);
t=[floor(ns*ncdf(ncdf<0.5)),ceil(ns*ncdf(ncdf>=0.5))];
% when plotting very few realizations t(1)=0, so the selection below
% will fail. Set this sigma contour to t(0)=1 instead.
t(t==0)=1;
%  t=ceil(ns*normcdf([-2:2],0,1));
lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];
for i=1:size(r.sdevs,2)
  z=(j(i)-1)*4+n+k(i);
  subplot_grid(6,4,z);

  plot(r.l,squeeze(squeeze(r.sdevs(:,i,:))),'color',[0.7,0.7,0.7],'LineWidth',0.5);
 
  hold on;
  plot(r.l,squeeze(devs(:,i,t(3))),'b','LineWidth',1);
  plot(r.l,squeeze(devs(:,i,t([2,4]))),'r','LineWidth',1);
  plot(r.l,squeeze(devs(:,i,t([1,5]))),'g','LineWidth',1);
  plot(r.l,r.rdevs(:,i),'ko-','MarkerFaceColor','k','LineWidth',2);
  plot(r.l(r.chibins{i}),r.rdevs(r.chibins{i},i),'ko-','MarkerFaceColor','r','LineWidth',2);

  text(10,6,sprintf(' sum of bpdevs =%4.2f',r.rsumdev(i)));
  
  hold off;
  %axis tight
  xlim([0,lmax]);

  if(ymax==0 && i==1)
    yl=-12; yu=15;
  else
    yl=-12; yu=12;
  end
  ylim([yl,yu]);
  
  if n == 1
    ylabel(lab{i});
  end
  
  if(i==1)
    title(strrep(freq,'_','\_'));
  end
  grid

  subplot_grid2(6,4,z);
end

return



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_hdevs(fig,r,jack,lmax,gt,col,colhead,pl_cross,mapname)

set(0,'CurrentFigure',fig);

if(length(r)>1)
  for j=1:length(pl_cross)
    plot_hdevs_sub(j,r(pl_cross(j)),[mapname{j} ', ' colhead],jack,lmax);
  end
else
  plot_hdevs_sub(col,r(1),colhead,jack,lmax);
end

gtitle(gt,0.98,'none');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_hdevs_sub(n,r,freq,jack,lmax)

[dum freqjknum]=get_jackdef();

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
xl={[-20,20],[-20,20],[-20,20],[-20,20],[-20,20],[-20,20],[-20,10],[-20,20],[-20,20]};
xlj={[-20,20],[-20,20],[-20,20],[-20,20],[-20,20],[-20,20],[-20,20],[-20,20],[-20,20]};
xl6={[-20,20],[-20,20],[-20,20],[-20,20],[-20,20],[-20,20],[-20,20],[-20,20],[-20,20]};

j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];
for i=1:size(r.sdevs,2)
  z=(j(i)-1)*4+n+k(i);
  subplot(6,4,z)
  
  if(jack==0)
    x=xl{i};
  elseif (jack==freqjknum)
    x=xl6{i};
   else
    x=xlj{i};
  end
    
  rdev=r.rsumdev(i);
  sdev=r.ssumdev(i,:);

  x(1)=min([x(1),min(sdev),1.05*rdev]);
  x(2)=max([x(2),max(sdev),1.05*rdev]);
  
  [bc,m]=hfill(sdev,100,x(1),x(2));
  try
    hplot(bc,m);
  catch 
    ylim([0,1])
  end 
  xlim([x(1),x(2)])
  line([rdev,rdev],[1e-99,100],'color','r')
  hold on
  % plot gaussian
  bw=(x(2)-x(1))/100;
  xp=linspace(x(1),x(2),1000);
  plot(xp,bw*length(sdev)*normpdf(xp,nanmean(sdev),nanstd(sdev)),'g');
  hold off

  ylabel(lab{i})
  if(i==1)
    title(strrep(freq,'_','\_'));
  end
  
  hnull([],'S');
  text(6,8.5,sprintf('sumdevs=%.2f',rdev));
  text(6,7,sprintf('PTEs=%#.3g',r.ptes_sumdev(i)));
  text(6,5.5,sprintf('PTEt=%#.3g',r.ptet_sumdev(i)));
  %text(6,4,sprintf('(m=%.2f \\sigma=%.2f)',p(2),p(3)));

end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_chi(fig,r,jacktype,gt,col,colhead,pl_cross,mapname)

set(0,'CurrentFigure',fig);
if(length(r)>1)
  for j=1:length(pl_cross)
    plot_chi_sub(j,r(pl_cross(j)),[mapname{j} ', ' colhead],jacktype);
  end
else
  plot_chi_sub(col,r(1),colhead,jacktype);
end

gtitle(gt,.98,'none');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_chi_sub(n,r,freq,jacktype)

[dum freqjknum]=get_jackdef();

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
xuv=[100,100,100,100,100,100,100,100,100];
xuv6=[100,100,100,100,100,100,100,100,100];

j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];
for i=1:size(r.sdevs,2)
  z=(j(i)-1)*4+n+k(i);

  subplot(6,4,z);

  x=r.rchisq(i);
  y=r.schisq(i,:);
  
  if(jacktype=='0')
    xu=xuv(i);
  elseif(jacktype==freqjknum)
    xu=xuv6(i);
  else
    xu=100;
  end
  xu=max([xu,max(y),1.05*max(x)]);

  [bcen,bcon]=hfill(y,100,0,xu);
  bw=xu/100;
  xp=linspace(0,xu,1000);
  plot(xp,bw*length(y)*chi2pdf(xp,length(r.chibins{i})),'g');

  hold on;
  try
    hplot(bcen,bcon);
  catch
    ylim([0,1])
  end
  hold off
  
  line([x,x],ylim,'Color','r');
  
  ylabel(lab{i})

  if(i==1)
    title(strrep(freq,'_','\_'));
  end
  
  hnull([],'S');
  text(5,7.5,sprintf('\\chi^2=%.1f',x));
  text(5,6.0,sprintf('PTEs=%#.3g',r.ptes(i)));
  text(5,4.5,sprintf('PTEt=%#.3g',r.ptet(i)));
  
  if(isfield(r,'rchisqa'))
    % plot the overall chi2
    subplot(7,4,24+n);
  
    x=r.rchisqa;
    y=r.schisqa;
  
    if(jacktype=='0')
      xu=1000;
    elseif(jacktype==freqjknum)
      xu=xuv6(i);
    else
      xu=100;
    end
   
    [bcen,bcon]=hfill(y,100,0,xu);
    bw=xu/100;
    xp=linspace(0,xu,1000);
    plot(xp,bw*length(y)*chi2pdf(xp,r.chisqa_ndf),'g');
  
    hold on;
    try
      hplot(bcen,bcon);
    catch
      ylim([0,1])
    end
    hold off
  
    line([x,x],ylim,'Color','r');

    ylabel('TT/TE/EE/BB together')
    
    hnull([],'S');
    text(6,7.5,sprintf('PTE theory=%.3f',r.pteat));
    text(6,6,sprintf('PTE sim     =%.3f',r.pteas));
    text(6,4.5,sprintf('\\chi^2 real   =%.1f',x));
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_spec(n,c,inpmod,freq,opt,lmax,chibins,freeyaxis)

if(opt.coadd.jacktype=='0' || strcmp(opt.aps.howtojack,'none'))
  % ylim so we can see the signal spectra
  if (lmax == 500)
    yrange=[-500,8000;-150,150;-1,25;-.1,.5;-20,20;-1,1];
  elseif (lmax == 200)
    yrange=[-500,6000;-60,40;-.5,1.5;-.05,.05;-6,6;-.5,.5];
  end
else
  if (lmax == 500)
    yrange=[-5,5;-.2,.2;-.05,.05;-.05,.05;-.2,.2;-.05,.05];
  elseif (lmax == 200)
    yrange=[-5,5;-.1,.1;-.02,.02;-.02,.02;-.1,.1;-.02,.02];
  end
end

% copy TE,TB,EB into ET,BT,BE
yrange([7,8,9],:)=yrange([2,5,6],:);

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];
for i=1:size(c.real,2)
  z=(j(i)-1)*4+n+k(i);
  subplot_grid(6,4,z);
  % plot s+n sims in light gray
  plot(c.l,squeeze(squeeze(c.sim(:,i,:))),'color',[0.7,0.7,0.7],'LineWidth',0.5);
  % plot mean of s+n sims in dark gray
  hold on;
  plot(c.l,squeeze(nanmean(c.sim(:,i,:),3)),'color',[0.46,0.47,0.46],'LineWidth',2);
  %if (opt.coadd.jacktype=='0')
  %  % plot signal only sims in dark gray
  %  plot(c.l,squeeze(squeeze(c.sig(:,i,:))),'color',[0.46,0.47,0.46]);
  %end 
  errorbar2(c.l,c.real(:,i),c.derr(:,i),'b.');
  plot(c.l(chibins{i}),c.real(chibins{i},i),'ro','MarkerFaceColor','r','MarkerSize',4);
  plot(inpmod.l,inpmod.Cs_l(:,i),'r','LineWidth',2);
  if isfield(c,'expv')
    %plot(c.l, c.expv(:,i),'rx')
  end
  hold off;
  if freeyaxis
    sbins = 2:12;
    if lmax<=200
      sbins = 2:7;
    end
    mmU = max(c.real(sbins,i) + c.derr(sbins,i));
    mmL = min(c.real(sbins,i) - c.derr(sbins,i));
    if mmL>0
      mmL=-max(c.derr(sbins,i)); 
    end
    mmU = mmU+0.05*abs(mmU);
    mmL = mmL-0.05*abs(mmL);
    try
      ylim([mmL,mmU]);
    catch
      ylim(yrange(i,:));
    end
  else
    ylim(yrange(i,:));
  end
  xlim([0,lmax]);
  line(xlim,[0,0],'Color','k','LineStyle',':');
  if(n==1||n==4)
    ylabel(lab{i})
  end   
  if(i==1)
    title(strrep(freq,'_','\_'));
  end
  subplot_grid2(6,4,z);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_errors(fig,r,opt,lmax,gt,pl_cross,mapname)

set(0,'CurrentFigure',fig);

if(length(r)>1)
  for j=1:length(pl_cross)
    plot_errors_sub(j,r(pl_cross(j)),mapname{j},opt,lmax);
  end
else
  plot_errors_sub(1,r(1),mapname{1},opt,lmax);
end

gtitle(gt,0.98,'none');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_errors_sub(n,r,freq,opt,lmax)

if(opt.coadd.jacktype=='0' || strcmp(opt.aps.howtojack,'none'))
  % ylim so we can see them
  if (lmax == 500)
    yrange=[0,1000; 0,20; 0,1; 0,0.1; 0,5; 0,.2];
  elseif (lmax == 200)
    yrange=[0,1000; 0,10; 0,0.2; 0,0.02; 0,2; 0,.04];
  end
else
  if (lmax == 500)
    yrange=[0,5;0,.2;0,.05;0,.05;0,.2;0,.05];
  elseif (lmax == 200)
    yrange=[0,5;0,.1;0,.02;0,.02;0,.1;0,.02];
    %yrange=[0,15;0,.5;0,.02;0,.005;0,.5;0,.02];
  end
end

% copy TE,TB,EB into ET,BT,BE
yrange([7,8,9],:)=yrange([2,5,6],:);

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};

j=[1:6,2,5,6];
k=[zeros(1,6),1,1,1];
for i=1:size(r.derr,2)
  z=(j(i)-1)*4+n+k(i);
  subplot(6,4,z)
  % plot the fluctuation of the noise and sig+noise sims
  plot(r.l,nanstd(r.noi(:,i,:),[],3),'b');
  hold on
  plot(r.l,nanstd(r.sim(:,i,:),[],3),'r');
  hold off
  xlim([0,lmax]);
  ylim(yrange(i,:));
  
  ylabel(lab{i})
  if(i==1)
    title(strrep(freq,'_','\_'));
  end
  grid

end

return
