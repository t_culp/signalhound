function [q,u]=derotate_qu(q,u,m)
% De-rotate a Q and U map in a projection other than RA/DEC so that Q and
% U are referenced to "up" instead of north
%
% [q,u]=derotate_qu(q,u,m)
%

[xg0,yg0]=meshgrid(m.x_tic,m.y_tic);
dx=0; dy=m.pixsize/10;

switch m.proj
 case 'arc'
  [ra,dec]=arcproj_to_radec(xg0,yg0,m.racen,m.deccen);
  ra=ra+dx; dec=dec+dy;
  [xg1,yg1]=radec_to_arcproj(ra,dec,m.racen,m.deccen);
  
 case 'ortho'
  [ra,dec]=sinproj_to_radec(xg0,yg0,m.racen,m.deccen);
  ra=ra+dx; dec=dec+dy;
  [xg1,yg1]=radec_to_sinproj(ra,dec,m.racen,m.deccen);
  
 case 'tan'
  [ra,dec]=tanproj_to_radec(xg0,yg0,m.racen,m.deccen);
  ra=ra+dx; dec=dec+dy;
  [xg1,yg1]=radec_to_tanproj(ra,dec,m.racen,m.deccen);
  
 case 'azeq'
  [ra,dec]=azeq_to_radec(xg0,yg0,m.racen,m.deccen);
  ra=ra+dx; dec=dec+dy;
  [xg1,yg1]=radec_to_azeq(ra,dec,m.racen,m.deccen);  

 case 'radec'
  xg1=xg0;
  yg1=yg0;

end

theta=atan2(xg1-xg0,yg1-yg0);

% apparently complex requires inputs to be full matrices
if issparse(q)
  q=full(q);
  u=full(u);
end

L=complex(q,u);
L=L.*(exp(complex(zeros(size(theta)),2*theta)));

q=real(L);
u=imag(L);

return