function [Eft,Bft]=qu2eb_pure(ad,Q,U,win,convention,appsf)
% [Eft,Bft]=qu2eb_pure(ad,Q,U,win,convention,appsf)
%
% window is the q and u window function
% convention is either 'healpix' or 'iau'
%
% This is Chris' implementation of Kendrick Smith's algorithm from
% this paper:
% http://arxiv.org/abs/astro-ph/0511629
%
% Now assumes that the Q/U variance maps are well smoothed before
% inputing to this function.  Using 0.5 deg smoothing, with 
% removing 5x pixels on the edge should be adequate.

if(~exist('pure_b'))
  pure_b='no';
end
if(~exist('convention'))
  convention='iau'
end
if(~exist('appsf'))
  appsf=true;
end

[u,v]=meshgrid(ad.u_val{1},ad.u_val{2});
ellgrid=2*pi*ad.u_r;

Q(isnan(Q))=0;
U(isnan(U))=0;
win(isnan(win))=0;

% Get E spectrum
Qft=i2f(ad,Q.*win);
Uft=i2f(ad,U.*win);

% Normalize the Q/U Fourier transforms
if(appsf)
  sf=prod(ad.N_pix)/nansum(rvec(win.^2));
else
  sf=1;
end
Qft=Qft*sqrt(sf);
Uft=Uft*sqrt(sf);

% rotate Q/U into E
switch convention
  case 'iau'
    % pol angle measured from N towards E
    chi=-atan2(v,u)+pi/2;
  case 'healpix'
    % pol angle measured from N towards W
    chi=atan2(v,u)-pi/2;
end
Eft=Qft.*cos(2*chi) + Uft.*sin(2*chi);

% Apply Kendrick's no-leak estimator for B
% Get the various derivatives of the window 
window_arr=calc_window_derivs(win,ad);

% This is (I think!) the definition that the pure-B papers assume and so
%I must use them lest I be forced to learn more math than I'd like
chi=atan2(v,u);
U=-U;

% make lots of ffts of Q/U times window and its derivs
% FFT(Q*W)
fftarr(:,:,1)=i2f(ad,Q.*window_arr(:,:,1));
% FFT(U*W)
fftarr(:,:,2)=i2f(ad,U.*window_arr(:,:,1));
% FFT(Q*dW/dx)
fftarr(:,:,3)=i2f(ad,Q.*window_arr(:,:,2));
% FFT(Q*dW/dy)
fftarr(:,:,4)=i2f(ad,Q.*window_arr(:,:,3));
% FFT(U*dW/dy)
fftarr(:,:,5)=i2f(ad,U.*window_arr(:,:,3));
% FFT(U*dW/dx)
fftarr(:,:,6)=i2f(ad,U.*window_arr(:,:,2));
% FFT(Q*d2W/dx/dy + U*(d2W/dy2 - d2W/dx2))
fftarr(:,:,7)=i2f(ad,2*Q.*window_arr(:,:,6) + ...
    U.*(window_arr(:,:,5)-window_arr(:,:,4)));

% The pure-B papers assume the forward FFT whereas the rest of our power
% spectrum estimation code uses the inverse FFT. The difference between the two is a
% flip in the sign of the imaginary component.
fftarr=complex(real(fftarr),-imag(fftarr));

% add them with the right prefactors to get pure B estimate
sarg=sin(chi);
sarg2=sin(2*chi);
carg=cos(chi);
carg2=cos(2*chi);

% don't divide by zero
ellgrid(ellgrid==0)=1e10;

Bft=-sarg2.*fftarr(:,:,1)+carg2.*fftarr(:,:,2)-...
    2*complex(0,1)./ellgrid.*...
    (sarg.*fftarr(:,:,3)+carg.*fftarr(:,:,4)+...
     sarg.*fftarr(:,:,5)-carg.*fftarr(:,:,6))+...
     1./ellgrid.^2.*fftarr(:,:,7);

% Because T and E are calculated with the inverse FFT, we must also return the B
% FFT as if it were calculated using the inverse FFT. Therefore, flip back the
% sign of the imaginary component. BB remains the same but the B correlations will now
% be calculated correctly. (Alternately, we could have computed the FFTs above with f2i
% instead of i2f.) 
% Note: I have validated the stationarity of the real and imaginary parts of Bft at high ell
% (the part not affected by pure-B estimation) as compared to what is returned by the
% normal non-pure-B estimator. However, the signs are opposite. Thus I will NOT
% flip the sign of imag(Bft) but WILL flip the sign of real(Bft).
% This should  give the correct Bft to compute all auto and cross spectra.
Bft=complex(-real(Bft),imag(Bft));

% Normalize the B map
Bft=Bft*sqrt(sf);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function result=calc_window_derivs(w,ad)

dx=ad.del_t(2);
dy=ad.del_t(1);

%dx=0.25*pi/180;
%dy=dx;

dwdx1=w-circshift(w,[0,1]);
dwdx2=circshift(w,[0,-1])-w;
dwdx_temp=(dwdx1+dwdx2)/2;
dwdx=(dwdx1/dx+dwdx2/dx)/2;

dwdy1=w-circshift(w,[1,0]);
dwdy2=circshift(w,[-1,0])-w;
dwdy_temp=(dwdy1+dwdy2)/2;
dwdy=(dwdy1./dy+dwdy2./dy)/2;

d2wdx1=dwdx_temp-circshift(dwdx_temp,[0,1]);
d2wdx2=circshift(dwdx_temp,[0,-1])-dwdx_temp;
d2wdx=(d2wdx1./dx+d2wdx2./dx)/2;

d2wdy1=dwdy_temp-circshift(dwdy_temp,[1,0]);
d2wdy2=circshift(dwdy_temp,[-1,0])-dwdy_temp;
d2wdy=(d2wdy1./dy+d2wdy2./dy)/2;

d2dxdy1=dwdy_temp-circshift(dwdy_temp,[0,1]);
d2dxdy2=circshift(dwdy_temp,[0,-1])-dwdy_temp;
d2dxdy=(d2dxdy1./dx+d2dxdy2./dx)/2;

result(:,:,1)=w;
result(:,:,2)=dwdx;
result(:,:,3)=dwdy;
result(:,:,4)=d2wdx;
result(:,:,5)=d2wdy;
result(:,:,6)=d2dxdy;

return
