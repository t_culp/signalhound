function reduc_examine_pntsrc(file,n,ukpervolt)
% examine_pntsrc(file)
%
% Look at the point sources in a coadd map
%
% e.g.
% reduc_examine_pntsrc('0751/real_a_filtp3_weight3_gs_dp1102_jack0')

if(~exist('n','var'))
  n=[];
end

if(~exist('ukpervolt','var'))
  ukpervolt=get_ukpervolt;
end

if(isempty(n))
  n=1;
end

load(sprintf('maps/%s',file))
if ~exist('map','var') && exist('ac','var')
  map=make_map(ac,m,coaddopt);
end
file=strrep(file,'_','\_');

% cal the maps
map=cal_coadd_maps(map,ukpervolt);

% Get PMNS catalog
%load('~/data/mat/pmns')
%pmns=structcut(pmns,pmns.s>0.3);

% Get the Planck early release catalog
[s.cname,s.name,s.s,s.se,s.ra,s.dec]=textread('aux_data/planck_erc_143.txt','%s %s %f %f %f %f');
s.ra(s.ra>180)=s.ra(s.ra>180)-360;

% jackknife the maps
if(n==3)
  map=jackknife_map(map);
  n=1;
end

% allow to fit pntsrc in one half of a jack
map=map(:,n);

% zoom in on strong source
if(0)
  src=structcut(src,[1]);
  %src.ra=src.ra-7.5;
  figure(1)
  clf; setwinsize(gcf,1100,200*length(src.ra));
  %colormap jet
  for i=1:length(src.ra)
    zoom_map2(m,map,src.ra(i),src.dec(i),src.name{i},i,pmns,length(src.ra))
  end
end

figure(1); clf; setwinsize(gcf,1200,800);
plot_map(m,map(1).T); title('150GHz T map');
caxis([-150,150]); colorbar; colormap gray
hold on; plot(s.ra,s.dec,'bo','MarkerSize',10); hold off

figure(2); clf; setwinsize(gcf,1200,800);
plot_map(m,map(1).Q); title('150GHz Q map');
caxis([-4,4]); colorbar; colormap gray
hold on; plot(s.ra,s.dec,'bo','MarkerSize',10); hold off

figure(3); clf; setwinsize(gcf,1200,800);
plot_map(m,map(1).U); title('150GHz U map');
caxis([-4,4]); colorbar; colormap gray
hold on; plot(s.ra,s.dec,'bo','MarkerSize',10); hold off



return

src=structcut(src,[1:6]);

figure(3); clf; setwinsize(gcf,800,200*length(src.ra));
colormap jet
for i=1:length(src.ra)
  zoom_map(m,map,src.ra(i),src.dec(i),src.name{i},i,pmns,length(src.ra))
end

figure(4); clf; setwinsize(gcf,1100,200*length(src.ra));
colormap jet
for i=1:length(src.ra)
  zoom_map2(m,map,src.ra(i),src.dec(i),src.name{i},i,pmns,length(src.ra))
end

src=structcut(src,[1]);

figure(5); clf; setwinsize(gcf,800,250);
colormap jet
for i=1:length(src.ra)
  zoom_map(m,map,src.ra(i),src.dec(i),src.name{i},i,pmns,length(src.ra))
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function zoom_map2(m,map,ra,dec,name,row,pmns,nsrc)

zoom_map2_sub(m,map(1),ra,dec,'100GHz',(row-1)*6+1,pmns,nsrc)
zoom_map2_sub(m,map(2),ra,dec,'150GHz',(row-1)*6+4,pmns,nsrc)
subplot(nsrc,6,(row-1)*6+1);
ylabel(name);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function zoom_map2_sub(m,map,ra,dec,name,n,pmns,nsrc)

s=15;

[d,i]=min(abs(m.x_tic-ra));
[d,j]=min(abs(m.y_tic-dec));
x=m.x_tic(i-s:i+s);
y=m.y_tic(j-s:j+s);

t=map.T(j-s:j+s,i-s:i+s);
q=map.Q(j-s:j+s,i-s:i+s);
u=map.U(j-s:j+s,i-s:i+s);

% plot the T map
subplot(nsrc,6,n)
imagesc(x,y,t);
axis xy; axis square; set(gca,'XDir','reverse');
title(sprintf('%s T %.0f %.0f',name,caxis));
hold on; plot(pmns.ra,pmns.dec,'b+'); hold off
hold on; plot(ra,dec,'k+'); hold off
caxis([-250,250])

% plot the Q map
subplot(nsrc,6,n+1)
imagesc(x,y,q);
axis xy; axis square; set(gca,'XDir','reverse');
title(sprintf('%s Q %.0f %.0f',name,caxis));
hold on; plot(pmns.ra,pmns.dec,'b+'); hold off
hold on; plot(ra,dec,'k+'); hold off
caxis([-250,250])

% plot the U map
subplot(nsrc,6,n+2)
imagesc(x,y,u);
axis xy; axis square; set(gca,'XDir','reverse');
title(sprintf('%s U %.0f %.0f',name,caxis));
hold on; plot(pmns.ra,pmns.dec,'b+'); hold off
hold on; plot(ra,dec,'k+'); hold off
caxis([-250,250])

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function zoom_map(m,map,ra,dec,name,row,pmns,nsrc)

zoom_map_sub(m,map(1).T,ra,dec,'100GHz',(row-1)*4+1,pmns,nsrc)
zoom_map_sub(m,map(2).T,ra,dec,'150GHz',(row-1)*4+3,pmns,nsrc)
subplot(nsrc,4,(row-1)*4+1);
ylabel(name);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function zoom_map_sub(m,t,ra,dec,name,n,pmns,nsrc)

s=15;

[d,i]=min(abs(m.x_tic-ra));
[d,j]=min(abs(m.y_tic-dec));
x=m.x_tic(i-s:i+s);
y=m.y_tic(j-s:j+s);
t=t(j-s:j+s,i-s:i+s);

par(1)=NaN;
mv=t(s+1,s+1);
if(~isnan(mv))
  % fit elliptical gaussian model
  p=[-s:s]*(y(2)-y(1));
  [xg,yg]=meshgrid(p,p);
  par=[mv,0,0,0.04,0.04,pi/2];
  
  % remove NaNs from fit consideration
  i=~isnan(t);
  [par,dum,dum,stat]=lsqnonlin(@(x) t(i)-egauss2(x,xg(i),yg(i)),par);
  mod=egauss2(par,xg,yg);  
end  

% plot the source
subplot(nsrc,4,n)
imagesc(x,y,t);
axis xy; axis square; set(gca,'XDir','reverse');
title(sprintf('%s h=%.3f',name,par(1)));
hold on; plot(pmns.ra,pmns.dec,'b+'); hold off
hold on; plot(ra,dec,'k+'); hold off

if(~isnan(mv))
  subplot(nsrc,4,n+1)
  imagesc(p,p,t-mod);
  axis xy; axis square; set(gca,'XDir','reverse');
  r=max(rvec(abs(t-mod)));
  title(sprintf('r/h=%.3f \\sigma_x=%.3f \\sigma_y=%.3f',r/par(1),par([4:5])));
end

return
