function paper_plots_bk14(pl,dopr)
% paper_plots_bk14(pl,dopr)
%
% This file should know how to make all of the plots and quoted
% numbers

global dopres;
if exist('dopr','var') & dopr
  set(0,'defaultlinelinewidth',1.0);
  set(0,'DefaultAxesFontSize',14);
  %set(0,'defaulttextfontsize',14);
  dopres = 1;
else
  dopres = 0;
end

if(any(pl==1))
  make_mapplot1;
end

if(any(pl==2))
  make_mapplot2;
end

if(any(pl==3))
  make_specjack_150;
end

if(any(pl==4))
  make_powspecres_95;
end

if(any(pl==5))
  make_powspec_95x150;
end

if(any(pl==6))
  make_jacktab;
end

% selected spectra
if(any(pl==7))
  make_powspec_BKxExt;
end

if(any(pl==8))
  like_baseline;
end

if(any(pl==9))
  like_evolution;
end

if(any(pl==10))
  like_variation;
end

if(any(pl==11))
  like_validation;
end

if(any(pl==12))
  make_noilev;
end

if(any(pl==13))
  make_mapplot0prl;
end

if(any(pl==14))
  % Also makes bandpowers file for data release.
  make_powspec_all;
end

if(any(pl==15))
  make_devs_hist;
end

if(any(pl==16))
  comp_perbin;
end

if(any(pl==17))
  like_evolmain;
end

if(any(pl==18))
  find_mlmodel;
end

if(any(pl==19))
  make_bk_vs_world;
end

if(any(pl==20))
  comp_perbin_datarelease;  
end

if(any(pl==21))
  rlik_datarelease;
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [c1,c2,c3,c4,c5]=get_colors()
c1=[.6,0,0]; c2=[.6,.6,0]; c3=[0,.5,0]; c4=[0,.6,.6]; c5=[0,0,1];
return

%%%%%%%%%%%%%%%%%%%%%
% used by make_mapplot
function dim=get_sixpaneldims(m)
% Parameters for six-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 3 * dim.maph + dim.wide + 3 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 2 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 2 * dim.maph + 2 * dim.med) / dim.H; % Bottom edge of row 1.
dim.y2 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y3 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.
return

%%%%%%%%%%%%%%%%%%%%%
% used by make_mapplot
function dim=get_fourpaneldims(m)
% Parameters for four-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 2 * dim.maph + dim.wide + 2 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 2 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 1.
dim.y2 = dim.wide / dim.H; % Bottom edge of row 2.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.
return

%%%%%%%%
function make_axis_pair(a1,a2)
p1=get(a1,'Position');
p2=get(a2,'Position');
g=p1(2)-(p2(2)+p2(4));
g=g*0.7;
set(a1,'XtickLabel',[]);
set(a1,'Position',[p1(1),p1(2)-g,p1(3),p1(4)+g]);
return

%%%%%%%%%%%%%%%%%%%%%
function make_mapplot0

% plot the 95GHz and 150 GHz Q/U maps

% get the BK14 maps
load maps/1459/real_aabd_filtp3_weight3_gs_dp1102_jack0.mat
maps=make_map(ac,m,coaddopt);

fig=figure(1);
clf;

dim=get_fourpaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% Q
ax3=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,maps(1).Q,'iau'); xlabel(''); ylabel('');
caxis([-4,4]);
colormap gray
freezeColors
set(ax3,'FontSize',7,'FontWeight','normal');
set(ax3,'Xticklabel',[]);
title('95 GHz Q');

ax4=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,maps(2).Q,'iau'); xlabel(''); ylabel('');
caxis([-4,4]);
colormap gray
freezeColors
set(ax4,'FontSize',7,'FontWeight','normal');
set(ax4,'Xticklabel',[],'Yticklabel',[]);
title('150 GHz Q');

% U
ax5=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,maps(1).U,'iau'); xlabel(''); ylabel('');
caxis([-4,4]);
colormap gray
freezeColors
set(ax5,'FontSize',7,'FontWeight','normal');
title('95 GHz U');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

ax6=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,maps(2).U,'iau'); xlabel(''); ylabel('');
caxis([-4,4]);
colormap gray
freezeColors
set(ax6,'FontSize',7,'FontWeight','normal');
set(ax6,'Yticklabel',[]);
title('150 GHz U');

% colorbars
ax8 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax8,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax8,'Xtick',[]);
set(ax8,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

ax9 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax9,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax9,'Xtick',[]);
set(ax9,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

% colorbar units
ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% this works way better than direct print to pdf
print -depsc2 paper_plots/qu_maps.eps
fix_lines('paper_plots/qu_maps.eps')
!epstopdf paper_plots/qu_maps.eps

return

%%%%%%%%%%%%%%%%%%%%%
% used by make_mapplot
function dim=get_twopaneldims(m)
% Parameters for four-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 1 * dim.maph + 2 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 2 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 1.
dim.y2 = dim.wide / dim.H; % Bottom edge of row 2.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.
return

%%%%%%%%%%%%%%%%%%%%%
function make_mapplot0prl

% plot only Q maps

% get the BK14 maps
load maps/1459/real_aabd_filtp3_weight3_gs_dp1102_jack0.mat
maps=make_map(ac,m,coaddopt);

fig=figure(1);
clf;

dim=get_fourpaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% Q
ax3=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,maps(1).Q,'iau'); xlabel(''); ylabel('');
caxis([-4,4]);
colormap gray
freezeColors
set(ax3,'FontSize',7,'FontWeight','normal');
set(ax3,'Xticklabel',[]);
title('95 GHz Q');

ax4=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,maps(2).Q,'iau'); xlabel(''); ylabel('');
caxis([-4,4]);
colormap gray
freezeColors
set(ax4,'FontSize',7,'FontWeight','normal');
set(ax4,'Xticklabel',[],'Yticklabel',[]);
title('150 GHz Q');

% colorbars
ax8 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax8,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax8,'Xtick',[]);
set(ax8,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

% colorbar units
ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
%text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
%     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% this works way better than direct print to pdf
print -depsc2 paper_plots/qu_maps_prl.eps
fix_lines('paper_plots/qu_maps_prl.eps')
!epstopdf paper_plots/qu_maps_prl.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_mapplot1

% plot the 95GHz real and noise maps

% get the BK14 maps
load maps/1459/real_aabd_filtp3_weight3_gs_dp1102_jack0.mat
maps=make_map(ac,m,coaddopt);

load maps/1459/0016_aabd_filtp3_weight3_gs_dp1100_jack0.mat
mapn=make_map(ac,m,coaddopt);

fig=figure(1);
clf;

dim=get_sixpaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% T

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,maps(1).T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
title('95 GHz T signal');

ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapn(1).T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax2,'FontSize',7,'FontWeight','normal');
set(ax2,'Xticklabel',[],'Yticklabel',[]);
title('95 GHz T noise');

% Q

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,maps(1).Q,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax3,'FontSize',7,'FontWeight','normal');
set(ax3,'Xticklabel',[]);
title('95 GHz Q signal');

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapn(1).Q,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax4,'FontSize',7,'FontWeight','normal');
set(ax4,'Xticklabel',[],'Yticklabel',[]);
title('95 GHz Q noise');

% U

ax5=axes('Position',[dim.x1,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,maps(1).U,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax5,'FontSize',7,'FontWeight','normal');
title('95 GHz U signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

ax6=axes('Position',[dim.x2,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,mapn(1).U,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax6,'FontSize',7,'FontWeight','normal');
set(ax6,'Yticklabel',[]);
title('95 GHz U noise');

% colorbars

ax7 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax7,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-150,150,1000),repmat(linspace(-150,150,1000)',1,10));
colormap hot;
freezeColors;
set(ax7,'Xtick',[]);
set(ax7,'YDir','normal','YAxisLocation','right','YTick',[-100 0 100]);

ax8 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax8,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax8,'Xtick',[]);
set(ax8,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

ax9 = axes('Position',[dim.x3,dim.y3,dim.cbar/dim.W,dim.ymap]);
set(ax9,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax9,'Xtick',[]);
set(ax9,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

% colorbar units

ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y3 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% this works way better than direct print to pdf
print -depsc2 paper_plots/tqu_maps_95.eps
fix_lines('paper_plots/tqu_maps_95.eps')
!epstopdf paper_plots/tqu_maps_95.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_mapplot2

% plot the 150GHz real and noise maps

% get the BK14 maps
load maps/1459/real_aabd_filtp3_weight3_gs_dp1102_jack0.mat
maps=make_map(ac,m,coaddopt);

load maps/1459/0016_aabd_filtp3_weight3_gs_dp1100_jack0.mat
mapn=make_map(ac,m,coaddopt);

fig=figure(1);
clf;

dim=get_sixpaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% T

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,maps(2).T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
title('150 GHz T signal');

ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapn(2).T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax2,'FontSize',7,'FontWeight','normal');
set(ax2,'Xticklabel',[],'Yticklabel',[]);
title('150 GHz T noise');

% Q

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,maps(2).Q,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax3,'FontSize',7,'FontWeight','normal');
set(ax3,'Xticklabel',[]);
title('150 GHz Q signal');

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapn(2).Q,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax4,'FontSize',7,'FontWeight','normal');
set(ax4,'Xticklabel',[],'Yticklabel',[]);
title('150 GHz Q noise');

% U

ax5=axes('Position',[dim.x1,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,maps(2).U,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax5,'FontSize',7,'FontWeight','normal');
title('150 GHz U signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

ax6=axes('Position',[dim.x2,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,mapn(2).U,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax6,'FontSize',7,'FontWeight','normal');
set(ax6,'Yticklabel',[]);
title('150 GHz U noise');

% colorbars

ax7 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax7,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-150,150,1000),repmat(linspace(-150,150,1000)',1,10));
colormap hot;
freezeColors;
set(ax7,'Xtick',[]);
set(ax7,'YDir','normal','YAxisLocation','right','YTick',[-100 0 100]);

ax8 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax8,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax8,'Xtick',[]);
set(ax8,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

ax9 = axes('Position',[dim.x3,dim.y3,dim.cbar/dim.W,dim.ymap]);
set(ax9,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax9,'Xtick',[]);
set(ax9,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

% colorbar units

ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y3 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% this works way better than direct print to pdf
print -depsc2 paper_plots/tqu_maps_150.eps
fix_lines('paper_plots/tqu_maps_150.eps')
!epstopdf paper_plots/tqu_maps_150.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_specjack_150
% make a figure similar to PRL2 fig8 comparing BK13_150 and BK14_150

% TODO: should we not:
% a) Use the BK13xP full points as plotted in BKP fig2 from file final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
% b) Can't use simd for comparison as doesn't exist in BK13 files...

% load the relevant data
BK13 = load('final/1456/real_aab_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat');
BK13.r=weighted_ellbins(BK13.r,BK13.bpwf);
BK14 = load('final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');
BK14.r=weighted_ellbins(BK14.r,BK14.bpwf);
K14  = load('final/1351/real_d_filtp3_weight3_gs_dp1102_jack01_pureB_matrix_overfreq.mat');
K14.r =weighted_ellbins(K14.r,K14.bpwf);

% to display the lensing spectrum
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');

% nan out the first ell bin:
BK13.r.lc(1,:)=NaN;
for ii=1:length(BK14.r)
  BK14.r(ii).lc(1,:)=NaN;
end

% copy the individual r structures into a new one to call the specjack
% they don't all come with the same fields - throw out the non-overlapping
% ones:
r(1) = rmfield(BK13.r,'doc');
r(2) = rmfield(BK14.r(2),{'doc','d','simd'});
% trick specjack to effectively only use two sets of spectra:
r = specjack(r,1,2,2,'simr');
% this is the differences we want:
r=r(1);

% also take diff of BK13 and K14
r2(1) = rmfield(BK13.r,'doc');
r2(2) = rmfield(K14.r(2),{'doc','d','simd'});
r2 = specjack(r2,1,2,2,'simr');
r2=r2(1);

% do the factor 4 even though there is no experiment jack here:
r.real=r.real/4; r.sim=r.sim/4; r.noi=r.noi/4; 
% compare to the mean expected from sims
r.expv=mean(r.sim,3);
r=get_bpcov(r);

r2.real=r2.real/4; r2.sim=r2.sim/4; r2.noi=r2.noi/4; 
r2.expv=mean(r2.sim,3);
r2=get_bpcov(r2);

% the weighted ell bins differ slightly between the two maps - 
% use the average:
r.l=mean([BK13.r.lc(:,4),BK14.r(2).lc(:,4)],2);

% chi and chi2 for the two ell bin ranges
r11=calc_chi(r,get_chibins());  r11=calc_devs(r11,get_chibins());
r12=calc_chi(r,get_chibins(10)); r12=calc_devs(r12,get_chibins(10));

r21=calc_chi(r2,get_chibins());  r21=calc_devs(r21,get_chibins());
r22=calc_chi(r2,get_chibins(10)); r22=calc_devs(r22,get_chibins(10));

%%%%%%%%%% plotting
clf; setwinsize(gcf,400,600)

% First panel - spectra
subplot_grid(2,1,1);
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on

% the lensing line:
plot(ml.l,ml.Cs_l(:,4),'k');

% plot BK13
errorbar(BK14.r(2).lc(:,4)-4,BK13.r.real(:,4),std(BK13.r.sim(:,4,:),[],3),'kx');
h1=errorbar2(BK14.r(2).lc(:,4)-4,BK13.r.real(:,4),std(BK13.r.simr(:,4,:),[],3),'kx');
set(h1(2),'Color',[.3,.3,.3]); set(h1(1),'Color',[.3,.3,.3]);

% plot K14
errorbar(BK14.r(2).lc(:,4)+0,K14.r(2).real(:,4),std(K14.r(2).sim(:,4,:),[],3),'b*');
h2=errorbar2(BK14.r(2).lc(:,4)+0,K14.r(2).real(:,4),std(K14.r(2).simr(:,4,:),[],3),'b*');

% plot BK14
errorbar(BK14.r(2).lc(:,4)+4,BK14.r(2).real(:,4),std(BK14.r(2).sim(:,4,:),[],3),'k.');
h3=errorbar2(BK14.r(2).lc(:,4)+4,BK14.r(2).real(:,4),std(BK14.r(2).simr(:,4,:),[],3),'k.');
set(h3(2),'MarkerSize',12);

legend([h1(2),h2(2),h3(2)],{'BK13_{150}','K2014_{150}','BK14_{150}'},'Location','nw');
legend boxoff
hold off
xlim([0,330]); ylim([-.01,.055]);
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
subplot_grid2(2,1,1);

% Second panel - spectral differences
subplot_grid(2,1,2);
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on

h1=errorbar2(BK14.r(2).lc(:,4)-2,r2(1).real(:,4),std(r2(1).sim(:,4,:),[],3),'b*');

h2=errorbar2(BK14.r(2).lc(:,4)+2,r(1).real(:,4),std(r(1).sim(:,4,:),[],3),'k.');
set(h2(2),'MarkerSize',12);

legend([h1(2),h2(2)],{'BK13_{150} - K2014_{150}','BK13_{150} - BK14_{150}'},'Location','NW');
legend boxoff
hold off
xlim([0 330]); ylim([-0.013, 0.007]); %ylim([-0.0041, 0.0021]);
yticks=-0.01:0.005:0.005; set(gca,'ytick',yticks); set(gca,'YTickLabel',num2str(yticks'))
xlabel('Multipole'); h=ylabel('\DeltaD_l^{BB}/4 [\muK^2]');
% force the y-axis label in a bit
set(h,'position',[-35,-0.001,1]);

% the braces:
drawbrace([r(1).l(6)+5,-0.005],[r(1).l(2)-5,-0.005], 5,'color','k')
drawbrace([r(1).l(10)+5,-0.009],[r(1).l(2)-5,-0.009], 5,'color','k')
text(mean([r(1).l(2),r(1).l(6)]),-0.0070,sprintf('PTE(\\chi_{1-5})=%.2f, PTE(\\chi^2_{1-5})=%.2f',r21.ptes_sumdev(4),r21.ptes(4)),'HorizontalAlignment','center','color','b')
text(mean([r(1).l(2),r(1).l(6)]),-0.0085,sprintf('PTE(\\chi_{1-5})=%.2f, PTE(\\chi^2_{1-5})=%.2f',r11.ptes_sumdev(4),r11.ptes(4)),'HorizontalAlignment','center')
text(mean([r(1).l(2),r(1).l(10)]),-0.0110,sprintf('PTE(\\chi_{1-9})=%.2f,PTE(\\chi^2_{1-9})=%.2f',r22.ptes_sumdev(4),r22.ptes(4)),'HorizontalAlignment','center','color','b')
text(mean([r(1).l(2),r(1).l(10)]),-0.0125,sprintf('PTE(\\chi_{1-9})=%.2f,PTE(\\chi^2_{1-9})=%.2f',r12.ptes_sumdev(4),r12.ptes(4)),'HorizontalAlignment','center')

subplot_grid2(2,1,2);
print -depsc2 paper_plots/specjack_150.eps
fix_lines('paper_plots/specjack_150.eps')
!epstopdf paper_plots/specjack_150.eps
system_safe(['convert -density 150 paper_plots/specjack_150.pdf paper_plots/specjack_150.png'])

return


%%%%%%%%%%%%%%%%%%%%%
function make_powspecres_95

% to display the lensing spectrum - note we are still computing
% chi2 against the mean of sims which is the old spectrum
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');

% in constrast to B-I and BK-V papers use only 5 bins
for i=1:6
  chibins{i}=[2:6];
end

% load the BK14 data set
load('final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');
r=get_bpcov(r);
r = weighted_ellbins(r,bpwf);
% we here only care about 95:
rs=r(1);
rs.lc([1,7:17],:)=NaN;
rs=calc_chi(rs,chibins,[],1);
im = inpmod;

% For TT variance we use legancy 1450 points... this is a blatant kludge but
% who cares about TT? The dominating sample variance portion for TT should even 
% be good for the 95s anyways.
load('final/1450x1350/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_overrx')
r=r(1);
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN; r=calc_chi(r,chibins,[],1);
rt=r;

% get the jackknife spectra - we are going to show the dk jack:
% c.f. http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20150218_finalpager_K14/
load('final/1351/real_d_filtp3_weight3_gs_dp1102_jack11_pureB_overfreq.mat');
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r=r(1); r.lc(1,:)=NaN; r=calc_chi(r,chibins,[],1);
rj=r;

% fig 4 of K13 was 1000x600
clf; setwinsize(gcf,1000,600)

xm=200;

ms=5; % marker size
ms2=10; % for the . markers 
f = 0.5 % fade out of T involving error bars

subplot(4,3,1); a1=gca;
% plot the least important things first
line([0,xm],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,1),'k');
h=errorbar2(rs.lc(:,1),rs.real(:,1),rt.derr(:,1),'k.');
set(h(2),'MarkerSize',ms2); set(h(1),'Color',[f,f,f]);
hold off
xlim([0,xm]); ylim([-1000,7000]);
%ptitle(sprintf('TT - \\chi^2 PTE = %.2f',rt.ptes(1)));
ptitle(sprintf('TT'));
ylabel('l(l+1)C_l/2\pi [\muK^2]')
subplot(4,3,4); a2=gca;
line([0,xm],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rs.lc(:,1),rj.real(:,1)-rj.expv(:,1),rj.derr(:,1),'b.');
set(h(2),'MarkerSize',ms2);
hold off
xlim([0,xm]); ylim([-1.8,0.5]);
%  ptitle(sprintf('TT jack - \\chi^2 PTE = %.2f',rj.ptes(1)),0.03,0.1);
ptitle(sprintf('TT jack - \\chi^2 PTE = %.2f',rj.ptes(1)));
make_axis_pair(a1,a2);

subplot(4,3,2); a1=gca;
line([0,xm],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,2),'k');
h=errorbar2(rs.lc(:,2),rs.real(:,2),rs.derr(:,2),'k.');
set(h(2),'MarkerSize',ms2);set(h(1),'Color',[f,f,f]);
hold off
xlim([0,xm]); ylim([-70,70]);
%  ptitle(sprintf('TE - \\chi^2 PTE = %.2f',rs.ptes(2)));
ptitle('TE');
subplot(4,3,5); a2=gca;
line([0,xm],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rs.lc(:,2),rj.real(:,2)-rj.expv(:,2),rj.derr(:,2),'b.');
set(h(2),'MarkerSize',ms2);
hold off
xlim([0,xm]); ylim([-0.1,0.1]);
%set(a2,'ytick',[-0.15,0,0.15]);
ptitle(sprintf('TE jack - \\chi^2 PTE = %.2f',rj.ptes(2)));
make_axis_pair(a1,a2);

subplot(4,3,3); a1=gca;
line([0,xm],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,3),'k');
h=errorbar2(rs.lc(:,3),rs.real(:,3),rs.derr(:,3),'k.');
set(h(2),'MarkerSize',ms2);
hold off
xlim([0,xm]); ylim([-0.1,1.3]);
ptitle(sprintf('EE - \\chi^2 PTE = %.2f',rs.ptes(3)));
subplot(4,3,6); a2=gca;
line([0,xm],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rs.lc(:,3),rj.real(:,3)-rj.expv(:,3),rj.derr(:,3),'b.');
set(h(2),'MarkerSize',ms2);
hold off
xlim([0,xm]); ylim([-.02,.02]);
ptitle(sprintf('EE jack - \\chi^2 PTE = %.2f',rj.ptes(3)));
make_axis_pair(a1,a2);

subplot(4,3,[7,10]); a1=gca;
line([0,xm],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(ml.l,ml.Cs_l(:,4),'k');
h=errorbar2(rs.lc(:,4),rs.real(:,4),rs.derr(:,4),'k.');
set(h(2),'MarkerSize',ms2);
xlim([0,xm]); ylim([-.01,.03]);
ptitle(sprintf('BB - \\chi^2 PTE = %.2f',rs.ptes(4)),0.03,0.95);
h=errorbar2(rs.lc(:,4)+4,rj.real(:,4)-rj.expv(:,4),rj.derr(:,4),'b.');
set(h(2),'MarkerSize',ms2);
hold off
ptitle(sprintf('BB jack - \\chi^2 PTE = %.2f',rj.ptes(4)),0.03,0.05);

subplot(4,3,8); a1=gca;
line([0,xm],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,5),'k');
h=errorbar2(rs.lc(:,2),rs.real(:,5),rs.derr(:,5),'k.');
set(h(2),'MarkerSize',ms2);set(h(1),'Color',[f,f,f]);
hold off
xlim([0,xm]); ylim([-5,5]);
%  ptitle(sprintf('TB - \\chi^2 PTE = %.2f',rs.ptes(5)));
ptitle('TB');
subplot(4,3,11); a2=gca;
line([0,xm],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rs.lc(:,2),rj.real(:,5)-rj.expv(:,5),rj.derr(:,5),'b.');
set(h(2),'MarkerSize',ms2);
hold off
xlim([0,xm]); ylim([-0.07,0.07]);
ptitle(sprintf('TB jack - \\chi^2 PTE = %.2f',rj.ptes(5)));
make_axis_pair(a1,a2);

subplot(4,3,9); a1=gca;
line([0,xm],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,6),'k');
h=errorbar2(rs.lc(:,3),rs.real(:,6),rs.derr(:,6),'k.');
set(h(2),'MarkerSize',ms2);
hold off
xlim([0,xm]); ylim([-.07,.07]);
ptitle(sprintf('EB - \\chi^2 PTE = %.2f',rs.ptes(6)));
subplot(4,3,12); a2=gca;
line([0,xm],[0,0],'LineStyle',':','color','k'); box on
hold on
h=errorbar2(rs.lc(:,3),rj.real(:,6)-rj.expv(:,6),rj.derr(:,6),'b.');
set(h(2),'MarkerSize',ms2);
hold off
xlim([0,xm]); ylim([-.015,.015]);
ptitle(sprintf('EB jack - \\chi^2 PTE = %.2f',rj.ptes(6)));
make_axis_pair(a1,a2);

xlabel('Multipole')

print -depsc2 paper_plots/powspecres_95.eps
fix_lines('paper_plots/powspecres_95.eps')
!epstopdf paper_plots/powspecres_95.eps
system_safe(['convert -density 150 paper_plots/powspecres_95.pdf paper_plots/powspecres_95.png'])

return

%%%%%%%%%%%%%%%%%%%%%
function make_powspec_95x150

% get updated lensing spectrum
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');

% load the BK14 data set
load('final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');
r=r(1:12); % to speed up
r=get_bpcov(r); % to get derr
r=weighted_ellbins(r,bpwf);
for ii=1:length(r)
  r(ii).lc([1,11:17],:)=NaN;
end

% calc the chi2 for 5 bandpowers
for i=1:9
  chibins{i}=[2:6];
end
r=calc_chi(r,chibins,[],1);
r=calc_devs(r,chibins,1);

% fig2 in BKP is 1000 wide
clf; setwinsize(gcf,1000,350)

ms=5; % marker size
ms2=10;
f=0.5;
xlims=[0,330];

% the position of the markers:
% use the 150 GHz weighted ones:
l95  = r(2).lc-5;
lX   = r(2).lc;
l150 = r(2).lc+5;

[c1,c2,c3]=get_colors();

% construct the lensed-LCDM+dust model as "theory curves"
% TODO: get Victor to check this
% DONE: Colin says it looks good
likedata.opt.fields = {'E'};
likedata.opt.expt = {'BK14_95','BK14_150'};
likedata = like_read_bandpass(likedata);
likedata = like_read_theory(likedata);
% this is old best lensed-LCDM+dust only value
param = [0, 1, 0, 3.75, -3.3, -0.6, 1.59, -0.42, 1, 2, 0, 19.6];
[lfine, rms] = model_rms(param, likedata);
lfine=lfine(30:end); rms=rms(30:end,:,:); % avoid distracting low ell bump
d95    =rms(:,1,1).^2+rms(:,1,3).^2;
d95x150=rms(:,1,1).^2+rms(:,1,3).*rms(:,2,3);
d150   =rms(:,1,1).^2+rms(:,2,3).^2;

subplot_grid(1,2,1); a3=gca;
%line([0,330],[0,0],'LineStyle',':','color','k'); box on
semilogy(lfine,d95,'--','Color',c1);
hold on
plot(lfine,d95x150,'--','Color',c2);
plot(lfine,d150,'--','Color',c3);
% plot lensing model last so on top
plot(ml.l,ml.Cs_l(:,3),'k');
h1=errorbar2( l95(:,3),r(1).real(:,3),r(1).derr(:,3),'mx');
h2=errorbar2(  lX(:,3),r(12).real(:,3),r(12).derr(:,3),'b*');
h3=errorbar2(l150(:,3),r(2).real(:,3),r(2).derr(:,3),'k.');
set(h1(2),'MarkerSize',ms);set(h2(2),'MarkerSize',ms);set(h3(2),'MarkerSize',ms2);
set(h1(1),'Color',c1);set(h1(2),'Color',c1);
set(h2(1),'Color',c2);set(h2(2),'Color',c2);
set(h3(1),'Color',c3);set(h3(2),'Color',c3);
xlim(xlims); ylim([0.1,30]);
ptitle('EE',0.01,0.7);
ylabel('l(l+1)C_l/2\pi [\muK^2]')
l{1}=sprintf('BK14_{95}xBK14_{95} - \\chi^2=%.1f, \\chi=%.1f',r(1).rchisq(3),r(1).rsumdev(3));
l{2}=sprintf('BK14_{95}xBK14_{150} - \\chi^2=%.1f, \\chi=%.1f',r(12).rchisq(3),r(12).rsumdev(3));
l{3}=sprintf('BK14_{150}xBK14_{150} - \\chi^2=%.1f, \\chi=%.1f',r(2).rchisq(3),r(2).rsumdev(3));
legend([h1(2),h2(2),h3(2)],l,'location','nw')
legend boxoff
%legend([h1(2),h2(2),h3(2)],{'95x95','150x150','95x150'},'location','se')
%subplot_grid2(1,2,1)

% construct the lensed-LCDM+dust model as "theory curves"
% TODO: get Victor to check this
% DONE: Colin says it looks good
likedata.opt.fields = {'B'};
likedata.opt.expt = {'BK14_95','BK14_150'};
likedata = like_read_bandpass(likedata);
likedata = like_read_theory(likedata);
[lfine, rms] = model_rms(param, likedata);
lfine=lfine(30:end); rms=rms(30:end,:,:);
d95    =rms(:,1,1).^2+rms(:,1,3).^2;
d95x150=rms(:,1,1).^2+rms(:,1,3).*rms(:,2,3);
d150   =rms(:,1,1).^2+rms(:,2,3).^2;

subplot_grid(1,2,2); a4=gca;
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(ml.l,ml.Cs_l(:,4),'k');
plot(lfine,d95,'--','Color',c1);
plot(lfine,d95x150,'--','Color',c2);
plot(lfine,d150,'--','Color',c3);
h1=errorbar2( l95(:,4),r(1).real(:,4),r(1).derr(:,4),'mx');
h2=errorbar2(  lX(:,4),r(12).real(:,4),r(12).derr(:,4),'b*');
h3=errorbar2(l150(:,4),r(2).real(:,4),r(2).derr(:,4),'k.');
set(h1(2),'MarkerSize',ms);set(h2(2),'MarkerSize',ms);set(h3(2),'MarkerSize',ms2);
set(h1(1),'Color',c1);set(h1(2),'Color',c1);
set(h2(1),'Color',c2);set(h2(2),'Color',c2);
set(h3(1),'Color',c3);set(h3(2),'Color',c3);
xlim(xlims); ylim([-.005,.055]);
ptitle('BB',0.01,0.95);
xlabel('Multipole')
l{1}=sprintf('BK14_{95}xBK14_{95} - \\chi^2=%.1f, \\chi=%.1f',r(1).rchisq(4),r(1).rsumdev(4));
l{2}=sprintf('BK14_{95}xBK14_{150} - \\chi^2=%.1f, \\chi=%.1f',r(12).rchisq(4),r(12).rsumdev(4));
l{3}=sprintf('BK14_{150}xBK14_{150} - \\chi^2=%.1f, \\chi=%.1f',r(2).rchisq(4),r(2).rsumdev(4));
legend([h1(2),h2(2),h3(2)],l,'location','nw')
legend boxoff
subplot_grid2(1,2,2)

print -depsc2 paper_plots/powspec_95x150.eps
fix_lines('paper_plots/powspec_95x150.eps')
!epstopdf paper_plots/powspec_95x150.eps
system_safe(['convert -density 150 paper_plots/powspec_95x150.pdf paper_plots/powspec_95x150.png'])

return


%%%%%%%%%%%%%%%%%%%%%%%%
function make_jacktab()

chibins1=get_chibins;
for i=1:6
  chibins2{i}=2:10;
end

jackstr='123456789abcde';

rf = ('final/1351/real_d_filtp3_weight3_gs_dp1102_jack01_pureB_overfreq.mat');

for j=jackstr
    
  n=findstr(rf,'jack');
  rf(n+4)=sprintf('%s',j);
  load(sprintf('%s',rf));
  r=r(1);
  
  % set expectation value to mean of s+n sims
  r.expv=mean(r.sim,3);
  
  r=get_bpcov(r);
  
  k=strfind(jackstr,j);
  
  ra{k,1}=calc_chi(r,chibins1);
  ra{k,1}=calc_devs(ra{k,1},chibins1);
  ra{k,1}(1).jacktype=j;
  
  ra{k,2}=calc_chi(r,chibins2);
  ra{k,2}=calc_devs(ra{k,2},chibins2);
  ra{k,2}(1).jacktype=j;
end

fp=fopen('paper_plots/ptetab_95.tex','w');

lab={'TT','TE','EE','BB','TB','EB','ET','BT','BE'};
fprintf(fp,'\\\\ \n');
%s=2:6; % include TE,EE,BB,TB,EB
s=[3,4,6]; % include EE,BB,EB only as B13
for j=1:size(ra,1)
  js=get_jackdef(ra{j}.jacktype);
%    fprintf(fp,'\\sidehead{%s} \n',js);
  fprintf(fp,'\\multicolumn{5}{l}{%s} \\\\ \n',js);
  for i=s
    % replace zero values with PTEt
    if(ra{j,1}.ptes(i)==0)
      ra{j,1}.ptes(i)=ra{j,1}.ptet(i); disp(sprintf('chi2 1 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,2}.ptes(i)==0)
      ra{j,2}.ptes(i)=ra{j,2}.ptet(i); disp(sprintf('chi2 2 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,1}.ptes_sumdev(i)==0)
      ra{j,1}.ptes_sumdev(i)=ra{j,1}.ptet_sumdev(i); disp(sprintf('chi1 1 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,2}.ptes_sumdev(i)==0)
      ra{j,2}.ptes_sumdev(i)=ra{j,2}.ptet_sumdev(i); disp(sprintf('chi1 2 rep zero j=%d, i=%d',j,i));
    end
    if(ra{j,1}.ptes_sumdev(i)==1)
      ra{j,1}.ptes_sumdev(i)=ra{j,1}.ptet_sumdev(i); disp(sprintf('chi1 1 rep one j=%d, i=%d',j,i));
    end
    if(ra{j,2}.ptes_sumdev(i)==1)
      ra{j,2}.ptes_sumdev(i)=ra{j,2}.ptet_sumdev(i); disp(sprintf('chi1 2 rep one j=%d, i=%d',j,i));
    end
  
    fprintf(fp,'%s & %5.3f & %5.3f & %5.3f & %5.3f \\\\ \n',lab{i},...
            ra{j,1}.ptes(i),ra{j,2}.ptes(i),ra{j,1}.ptes_sumdev(i),ra{j,2}.ptes_sumdev(i));
  end
  ptes1(j,:)=ra{j,1}.ptes(s);
  ptes2(j,:)=ra{j,2}.ptes(s);
  ptes1_sumdev(j,:)=ra{j,1}.ptes_sumdev(s);
  ptes2_sumdev(j,:)=ra{j,2}.ptes_sumdev(s);
end

fclose(fp);

% make pte dist figure
% in B12 and BK13 this was 400x400 with full col width
% this time make it 300x300 with 0.7 col width
figure(3)
clf; setwinsize(gcf,300,300);
yl=10;
subplot(2,2,1); hfill(ptes1,20,0,1); title('Bandpowers 1-5 \chi^2'); ylim([0,yl]);
%hfill(ptes1(:,2),20,0,1,[],'Sr--');
subplot(2,2,2); hfill(ptes2,20,0,1); title('Bandpowers 1-9 \chi^2'); ylim([0,yl]);
%hfill(ptes2(:,2),20,0,1,[],'Sr--');
subplot(2,2,3); hfill(ptes1_sumdev,20,0,1); title('Bandpowers 1-5 \chi'); ylim([0,yl]); 
%hfill(ptes1_sumdev(:,2),20,0,1,[],'Sr--');
subplot(2,2,4); hfill(ptes2_sumdev,20,0,1); title('Bandpowers 1-9 \chi'); ylim([0,yl]);
%hfill(ptes2_sumdev(:,2),20,0,1,[],'Sr--');

print -depsc2 paper_plots/ptedist_95.eps
fix_lines('paper_plots/ptedist_95.eps')
!epstopdf paper_plots/ptedist_95.eps
system_safe(['convert -density 150 paper_plots/ptedist_95.pdf paper_plots/ptedist_95.png'])

return

%%%%%%%%%%%%%%%%%%%%%
function make_powspec_all

% load the BK14xExt points
load('final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');
r=get_bpcov(r); % provides derr
r=weighted_ellbins(r,bpwf);
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');

% construct the lensed-LCDM+dust/sync models as "theory curves"
likedata.opt.fields = {'B'};
likedata.opt.sync_freq=23;
likedata.opt.expt = {'BK14_95','BK14_150','W023','P030','W033','P044','P070','P100','P143','P217','P353'};
likedata = like_read_bandpass(likedata);
likedata = like_read_theory(likedata);
param = [0, 1, 3.8, 4.2, -3.1, -0.6, 1.6, -0.4, 1, 2, 0, 19.6];
[lfine, rms] = model_rms(param, likedata);
lfine=lfine(30:end); rms=rms(30:end,:,:); % avoid distracting low ell bump
clear likedata

% get the ordering
k=1;
for i=1:10
  for j=i+1:11
    ii(k)=i; jj(k)=j;
    k=k+1;
  end
end
ii=[1:11,ii]; jj=[1:11,jj];

close(gcf);
setwinsize(gcf,800,1000); clf;
% can't make the window bigger in pixels so force text smaller -
% also need the -r0 switch to print below to get wisywig
set(gcf,'DefaultAxesFontSize', 8);
set(gcf,'DefaultTextFontSize', 8);

% make pretty labels in native order
s=make_labels();

for i=1:length(r)
  subplot_grid(11,6,i);
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  plot(ml.l,ml.Cs_l(:,4),'k');
  plot(lfine,rms(:,1,1).^2+rms(:,ii(i),2).*rms(:,jj(i),2),'r--');
  plot(lfine,rms(:,1,1).^2+rms(:,ii(i),3).*rms(:,jj(i),3),'b--');
  h1=errorbar2(r(i).lc(2:6,4),r(i).real(2:6,4),r(i).derr(2:6,4),'k.');
  set(h1(2),'MarkerSize',10);
  xlim([0,200]);
  if(i<=60)
    set(gca,'XtickLabel',[])
  end
  %drawnow; subplot_grid2(11,6,i,0,1);
  % subplot_grid2 doesn't work well - do this instead
  yl=ylim;
  if(yl(1)==0)
    ylim([-diff(yl)*0.05,yl(2)*1.05]);
  else
    ylim(yl*1.1);
  end
  switch i
   case 1
    ylim([-0.01,0.03]);
   case 2
    yl=ylim;
    ylim([yl(1),0.03]);
   case 3
    ylim([-20,30]);
   case 8
    ylim([-0.5,0.5]);
   case 10
    ylim([-0.5,1.5]);
   case 13
    ylim([-0.2,1]);
   case 14
    ylim([-0.3,0.3]);
   case 15
    ylim([-0.5,0.5]);
   case 17
    ylim([-0.3,0.3]);
   case 22
    ylim([-0.3,0.3]);
   case 24
    ylim([-0.3,0.3]);
   case 32
    ylim([-5,20]);
   case 33
    ylim([-5,20]);
   case 34
    ylim([-5,10]);
   case 35
    ylim([-2,2]);
   case 37
    ylim([-3,3]);
   case 38
    ylim([-10,10]);
   case 44
    ylim([-3,3]);
   case {46,47,51}
    ylim([-10,10]);
   case 52
    ylim([-5,5]);
   case 53
    ylim([-3,3]);
   case 54
    ylim([-1,1]);
   case 55
    ylim([-2,2]);
   case 58
    ylim([-2,2]);
    
    
  end
  ptitle(s{i},[],0.8);
end
xlabel('Multipole')

% turn off -r0 switch leads to ugly lines
%print -r0 -depsc2 paper_plots/powspec_all.eps
print -depsc2 paper_plots/powspec_all.eps
fix_lines('paper_plots/powspec_all.eps')
!epstopdf paper_plots/powspec_all.eps

% Write data release file.
powspec_all_datarelease(r, s);

return

function s=make_labels()
b={'BK_{95}','BK_{150}','W_{23}','P_{30}','W_{33}','P_{44}','P_{70}','P_{100}','P_{143}','P_{217}','P_{353}'};
for i=1:11
  s{i}=sprintf('%sx%s',b{i},b{i});
end
k=12;
for i=1:11
  for j=(i+1):11
    s{k}=sprintf('%sx%s',b{i},b{j});
    k=k+1;
  end
end
return

%%%%%%%%%%%%%%%%%%%%%
function make_devs_hist

% get the normalized devs of the real data versus the ML model
likeopt.fields = {'B'};
likeopt.l=2:10; likeopt.sync_freq=23;
likedata = get_likedata('BK14', likeopt);
% ML model
mod=[0.026, 1, 1.4, 4.1, -3.1, -0.56, 1.60, -0.19, 2, 2, 0, 19.6];
% get bpcm and expv
[ell,cmp]=model_rms(mod,likedata); likedata.bpcm=scale_bpcm(cmp,likedata);
likedata.expv=like_getexpvals(mod,likedata);
% find the devs
e=sqrt(diag(likedata.bpcm));
e=reshape(e,size(likedata.real'))';
d=cvec(likedata.real);
m=cvec(likedata.expv);
e=cvec(e);
rdevs=(d-m)./e;

% get the normalized devs of the simd sims
% load the BK14xExt points
load('final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');
% copy simd into sim so that everything is relative to lcdm+dust model
for i=1:length(r)
  r(i).sim=r(i).simd;
end
% get devs - "1" switch means take expv as mean of sim
r=calc_devs(r,get_chibins,1);
for i=1:length(r)
  sdevs(i,:,:)=r(i).sdevs(2:10,4,:);
end

setwinsize(gcf,300,300); clf;
[bc,nd]=hfill(rdevs,50,-5,5);
[bc,ns]=hfill(sdevs,50,-5,5);
hplot(bc,nd,'L');
hold on
plot(bc,sum(nd)*ns/sum(ns),'r');
g=gauss([50,0,1],bc); g=sum(nd)*g/sum(g);
plot(bc,g,'g');
ylim([0.01,100])
xlabel('sdevs'); ylabel('N per 0.2 wide bin');

disp(' ');
disp(sprintf('maximum norm dev = %.2f sigma',max(abs(rdevs))));

print -depsc2 paper_plots/devs_all_hist.eps
fix_lines('paper_plots/devs_all_hist.eps')
!epstopdf paper_plots/devs_all_hist.eps

return

%%%%%%%%%%%%%%%%%%%%%
% NOT USED ANYMORE
function make_devs_all

% load the BK14xExt points
load('final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');
r=weighted_ellbins(r,bpwf);

% copy simd into sim so that everything is relative to lcdm+dust model
for i=1:length(r)
  r(i).sim=r(i).simd;
end

% chibins needed for calc_devs - not used here though
for i=1:9
  chibins{i}=[2:10];
end
% get devs - "1" switch means take expv as mean of sim
r=calc_devs(r,chibins,1);

close(1); figure(1);
setwinsize(gcf,800,1000); clf;
% can't make the window bigger in pixels so force text smaller -
% also need the -r0 switch to print below to get wisywig
set(gcf,'DefaultAxesFontSize', 7);
set(gcf,'DefaultTextFontSize', 7);

% make pretty labels in native order
l=make_labels();

for i=1:length(r)
  subplot_grid(11,6,i);
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  plot(r(i).lc(2:10),r(i).rdevs(2:10,4),'.')
  d(i,:)=r(i).rdevs(2:10,4);
  s(i,:,:)=r(i).sdevs(2:10,4,:);
  xlim([0,330]); ylim([-5,5]);
  ptitle(l{i},[],0.8);
  if(i<=60)
    set(gca,'XtickLabel',[])
  end
end
xlabel('Multipole')

print -depsc2 paper_plots/devs_all.eps
fix_lines('paper_plots/devs_all.eps')
!epstopdf paper_plots/devs_all.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_powspec_BKxExt

% load the BK14xExt points
load('final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');

% calc the chi2 for 5 bandpowers
for i=1:9
  chibins{i}=[2:6]; % use the plotted bins
end
r=calc_chi(r,chibins,[],1);
r=calc_devs(r,chibins,1);

% the weighted bin centers
for ii=1:length(r)
  r(ii).lc(1,:)=NaN;
  r(ii).lc(7:end,:)=NaN;
end

% fig 3 in BKP was 400 wide
clf; setwinsize(gcf,400,500);
ms=5; % marker size
ms2=10;
f=0.5;

f1={'BK_{95}','BK_{150}','W_{23}','P_{30}','W_{33}','P_{44}','P_{70}','P_{100}','P_{143}','P_{217}','P_{353}'};

% construct the lensed-LCDM+dust model as "theory curves"
% TODO: get Victor to check this
% DONE: Colin says it looks good
likedata.opt.fields = {'B'};
likedata.opt.expt = {'BK14_95','BK14_150','W023','P030','W033','P044','P070','P100','P143','P217','P353'};
likedata = like_read_bandpass(likedata);
likedata = like_read_theory(likedata);
% this is old best lensed-LCDM+dust only value
param = [0, 1, 0, 3.75, -3.3, -0.6, 1.59, -0.42, 1, 2, 0, 19.6];
[lfine, rms] = model_rms(param, likedata);
lfine=lfine(30:end); rms=rms(30:end,:,:);

make_powspec_BKxExt_sub(1,3,r,ml,f1,ms,ms2,lfine,rms);
make_powspec_BKxExt_sub(2,4,r,ml,f1,ms,ms2,lfine,rms);
make_powspec_BKxExt_sub(3,5,r,ml,f1,ms,ms2,lfine,rms);
make_powspec_BKxExt_sub(4,10,r,ml,f1,ms,ms2,lfine,rms);
make_powspec_BKxExt_sub(5,11,r,ml,f1,ms,ms2,lfine,rms);

subplot_grid(3,2,6);
j1=aps_getxind(length(r),3,11);
j2=aps_getxind(length(r),4,11);
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(ml.l,ml.Cs_l(:,4),'k');
h1=errorbar2(r(2).lc(:,4)-5,r(j1).real(:,4),r(j1).derr(:,4),'r.');
h2=errorbar2(r(2).lc(:,4),r(j2).real(:,4),r(j2).derr(:,4),'b.');
set(h1(2),'MarkerSize',ms2); set(h2(2),'MarkerSize',ms2);
hold off
xlim([0,200]); ylim([-15,15]); set(gca,'YTick',[-10:10:10]);
xlabel('Multipole')
ptitle(sprintf('W_{23}xP_{353}, \\chi^2=%.1f, \\chi=%.1f',r(j1).rchisq(4),r(j1).rsumdev(4)),[],0.85,'color','r','fontsize',8)
ptitle(sprintf('P_{30}xP_{353}, \\chi^2=%.1f, \\chi=%.1f',r(j2).rchisq(4),r(j2).rsumdev(4)),[],0.05,'color','b','fontsize',8)

print -depsc2 paper_plots/powspec_BKxExt.eps
fix_lines('paper_plots/powspec_BKxExt.eps')
!epstopdf paper_plots/powspec_BKxExt.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_powspec_BKxExt_sub(i,j,r,ml,f1,ms,ms2,lfine,rms)
[c1,c2,c3]=get_colors();

subplot_grid(3,2,i);
j1=aps_getxind(length(r),1,j);
j2=aps_getxind(length(r),2,j);
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(ml.l,ml.Cs_l(:,4),'k');
plot(lfine,rms(:,1,1).^2+rms(:,1,3).*rms(:,j,3),'--','color',c1);
plot(lfine,rms(:,1,1).^2+rms(:,2,3).*rms(:,j,3),'--','color',c3);
h1=errorbar2(r(2).lc(:,4)-5,r(j1).real(:,4),r(j1).derr(:,4),'mx');
h2=errorbar2(r(2).lc(:,4),r(j2).real(:,4),r(j2).derr(:,4),'k.');
set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms2);
set(h1(1),'Color',c1);set(h1(2),'Color',c1);
set(h2(1),'Color',c3);set(h2(2),'Color',c3);

hold off
xlim([0,200]);
switch j
 case 10
  ylim([-0.1,0.1]); set(gca,'YTick',[-0.05:0.05:0.05]);
 otherwise
  ylim([-0.5,0.5]); set(gca,'YTick',[-0.4:0.4:0.4]);
end
if(j<11)
  set(gca,'XtickLabel',[])
end

%subplot_grid2(3,2,i);
if(j<7)
  ptitle(sprintf('%sxBK14_{95}, \\chi^2=%.1f, \\chi=%.1f',f1{j},r(j1).rchisq(4),r(j1).rsumdev(4)),[],0.85,'color',c1,'fontsize',8)
  ptitle(sprintf('%sxBK14_{150}, \\chi^2=%.1f, \\chi=%.1f',f1{j},r(j2).rchisq(4),r(j2).rsumdev(4)),[],0.05,'color',c3,'fontsize',8)
else
  ptitle(sprintf('BK14_{95}x%s, \\chi^2=%.1f, \\chi=%.1f',f1{j},r(j1).rchisq(4),r(j1).rsumdev(4)),[],0.85,'color',c1,'fontsize',8)
  ptitle(sprintf('BK14_{150}x%s, \\chi^2=%.1f, \\chi=%.1f',f1{j},r(j2).rchisq(4),r(j2).rsumdev(4)),[],0.05,'color',c3,'fontsize',8)
end
  
return


%%%%%%%%%%%%%%%%%%%%%%%%
% used by like_baseline below
function dat = tridata(i)
% function to load in the data for the baseline triangle plot
% location of processed chains for BK14_{95+150} + Ext Data
fpath = '/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/';
folder{1} = 'BK14_baseline';
fname{1} = strcat(fpath,'BK14_baseline/');

% load r vs Ad 
dat.l_rd = load(strcat(fname{i}, folder{i}, '_2D_BBdust_r'));
dat.r = load(strcat(fname{i}, folder{i}, '_2D_BBdust_r_x'));
dat.d = load(strcat(fname{i}, folder{i}, '_2D_BBdust_r_y'));

% normalize and find the location of the contours
dat.l_rd = dat.l_rd / max(dat.l_rd(:));
dat.ls_rd = sort(dat.l_rd(:), 1, 'descend');
dat.lc_rd = cumsum(dat.ls_rd) / sum(dat.ls_rd);
dat.imax_rd = min(find(dat.lc_rd > 0.99));
dat.l68_rd = interp1(dat.lc_rd(1:dat.imax_rd), dat.ls_rd(1:dat.imax_rd), 0.68);
dat.l95_rd = interp1(dat.lc_rd(1:dat.imax_rd), dat.ls_rd(1:dat.imax_rd), 0.95);

% load As vs Ad
dat.l_sd = load(strcat(fname{i}, folder{i}, '_2D_BBsync_BBdust'));
dat.s = load(strcat(fname{i}, folder{i}, '_2D_BBsync_BBdust_x'));
dat.d1 = load(strcat(fname{i}, folder{i}, '_2D_BBsync_BBdust_y'));

% normalize and find the location of the contours
dat.l_sd = dat.l_sd / max(dat.l_sd(:));
dat.ls_sd = sort(dat.l_sd(:), 1, 'descend');
dat.lc_sd = cumsum(dat.ls_sd) / sum(dat.ls_sd);
dat.imax_sd = min(find(dat.lc_sd > 0.99));
dat.l68_sd = interp1(dat.lc_sd(1:dat.imax_sd), dat.ls_sd(1:dat.imax_sd), 0.68);
dat.l95_sd = interp1(dat.lc_sd(1:dat.imax_sd), dat.ls_sd(1:dat.imax_sd), 0.95);

% load r vs As
dat.l_sr = load(strcat(fname{i}, folder{i}, '_2D_BBsync_r'));
dat.s1 = load(strcat(fname{i}, folder{i}, '_2D_BBsync_r_x'));
dat.r1 = load(strcat(fname{i}, folder{i}, '_2D_BBsync_r_y'));

% load r vs As
dat.l_sr = load(strcat(fname{i}, folder{i}, '_2D_BBsync_r'));
dat.s1 = load(strcat(fname{i}, folder{i}, '_2D_BBsync_r_x'));
dat.r1 = load(strcat(fname{i}, folder{i}, '_2D_BBsync_r_y'));

% normalize and find the location of the contours
dat.l_sr = dat.l_sr / max(dat.l_sr(:));
dat.ls_sr = sort(dat.l_sr(:), 1, 'descend');
dat.lc_sr = cumsum(dat.ls_sr) / sum(dat.ls_sr);
dat.imax_sr = min(find(dat.lc_sr > 0.99));
dat.l68_sr = interp1(dat.lc_sr(1:dat.imax_sr), dat.ls_sr(1:dat.imax_sr), 0.68);
dat.l95_sr = interp1(dat.lc_sr(1:dat.imax_sr), dat.ls_sr(1:dat.imax_sr), 0.95);

% Load the 1D likelihoods on r, Ad and As
dat.l_r = load(strcat(fname{i}, folder{i}, '_p_r.dat'));
dat.l_d = load(strcat(fname{i}, folder{i}, '_p_BBdust.dat'));
dat.l_s = load(strcat(fname{i}, folder{i}, '_p_BBsync.dat'));

return

%%%%%%%%%%%%%%%%%%%%%%%%
function like_baseline
% make baseline triangle plot

figure(3)
clf; setwinsize(gcf,600,600);

% load data for BK13 baseline result
load('/n/home08/pryke/bicep2_analysis/b2p_plots/likevar_dust.mat')

% load data for BK14 baseline result
dt1 = tridata(1);

% r vs Ad contours
ax1 = axes('Position', [0.09 0.39 0.3 0.3]);
hold all;
y=sum(lc1.l,3); contourfrac(lc1.r,lc1.A_d,y,[0.68,0.95],'r','LineWidth',1);
contour(dt1.r, dt1.d, dt1.l_rd, [dt1.l95_rd, dt1.l68_rd], 'k', 'LineWidth', 2);
set(ax1, 'Box', 'on');
set(ax1, 'XLim', [0 0.22], 'XTick', [0:0.04:0.2], 'XTickLabel', '', ...
           'XTickMode', 'manual', 'XTickLabelMode', 'manual');
set(ax1, 'YLim', [0 8.5], 'YTick', [0:1.5:7.5]);
ylabel('A_d @ l=80 & 353GHz [\muK^2]');

% As vs Ad contours
ax2 = axes('Position', [0.39 0.09 0.3 0.3]);
hold all;
contour(dt1.s, dt1.d1, dt1.l_sd, [dt1.l95_sd, dt1.l68_sd], 'k', 'LineWidth', 2);
set(ax2, 'Box', 'on');
set(ax2, 'XLim', [0 11], 'XTick', [0:2:10]);
set(ax2, 'YLim', [0 8.5], 'YTick', [0:1.5:7.5], 'YTickLabel', '', ...
           'YTickMode', 'manual', 'YTickLabelMode', 'manual');
xlabel('A_d @ l=80 & 353GHz [\muK^2]');

% r vs As contours
ax3 = axes('Position', [0.09 0.09 0.3 0.3]);
hold all;
contour(dt1.s1, dt1.r1, dt1.l_sr, [dt1.l95_sr, dt1.l68_sr], 'k','LineWidth', 2);
set(ax3, 'Box', 'on');
set(ax3, 'XLim', [0 0.22], 'XTick', [0:0.04:0.2]);
set(ax3, 'YLim', [0 8.5], 'YTick', [0:1.5:7.5]);
ylabel('A_{sync} @ l=80 & 23GHz [\muK^2]');
xlabel('r')

% 1D lik on r
ax4 = axes('Position', [0.09 0.69 0.3 0.3]);
hold all;
y=sum(sum(lc1.l,1),3); plot(lc1.r,y./max(y),'r');
plot(dt1.l_r(:,1), dt1.l_r(:,2), 'k', 'LineWidth', 2);
set(ax4, 'Box', 'on');
set(ax4, 'XLim', [0 0.22], 'XTick', [0:0.04:0.2], 'XTickLabel', '', ...
           'XTickMode', 'manual', 'XTickLabelMode', 'manual');
set(ax4, 'YLim', [0 10.5/10], 'YTick', [0:0.2:1]);
ylabel('L/L_{peak}');
legend('BKP baseline', 'BK14 baseline')
legend boxoff

% print the baseline r-values and limit to copy to paper
disp(' '); disp('r-values and 95% limit:')
cons1d_stats(dt1.l_r(:,1),dt1.l_r(:,2));

% 1D lik on Ad
ax5 = axes('Position', [0.39 0.39 0.3 0.3]);
hold all;
y=sum(sum(lc1.l,2),3); plot(lc1.A_d,y./max(y),'r');
plot(dt1.l_d(:,1), dt1.l_d(:,2), 'k', 'LineWidth', 2);
set(ax5, 'Box', 'on');
set(ax5, 'XLim', [0 11], 'XTick', [0:2:10], 'XTickLabel', '', ...
           'XTickMode', 'manual', 'XTickLabelMode', 'manual');
set(ax5, 'YLim', [0 8.5/7.5], 'YTick', [0.2:0.2:1], 'YTickLabel', '', ...
           'YTickMode', 'manual', 'YTickLabelMode', 'manual');

% print the baseline Ad values to copy to paper
disp(' '); disp('A_d values and significance:')
cons1d_stats(dt1.l_d(:,1),dt1.l_d(:,2));

% 1D lik on As
ax6 = axes('Position', [0.69 0.09 0.3 0.3]);
hold all;
plot(dt1.l_s(:,1), dt1.l_s(:,2), 'k', 'LineWidth', 2);
set(ax6, 'Box', 'on');
set(ax6, 'XLim', [0 10], 'XTick', [0:2:10]);
set(ax6, 'YLim', [0 8.5/7.5], 'YTick', [0.2:0.2:1], 'YTickLabel', '', ...
           'YTickMode', 'manual', 'YTickLabelMode', 'manual');
xlabel('A_{sync} @ l=80 & 23GHz [\muK^2]');

% print the baseline As limit to copy to paper
disp(' '); disp('A_s 95% limit:')
cons1d_stats(dt1.l_s(:,1),dt1.l_s(:,2));

% plot 1D constraints on the parameters not shown in the
% triangle plot
% Start with beta_dust
fpath = '/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/';
fname = strcat(fpath,'BK14_baseline/BK14_baseline_p_BBbetadust.dat');
bdust = textread(fname, '', 'commentstyle', 'shell');
ax7 = axes('Position', [0.45 0.85 0.14 0.14]);
plot(bdust(:,1), bdust(:,2) / max(bdust(:,2)), 'k', 'LineWidth', 2);
% plot prior
hold all;
x = [1.15:0.01:2.15];
plot(x, exp(-1 * (x - 1.59).^2 / (2 * 0.11^2)), 'r--');
% axes
xlabel('\beta_{d}');
set(ax7, 'XLim', [1 2], 'XTick', [1:0.5:2]);
set(ax7, 'YLim', [0 10.5/10], 'YTick', [0,1]);
%set(ax7, 'YTickLabel', '');
%ylabel('L/L_{peak}');

% beta_sync
fname = strcat(fpath,'BK14_baseline/BK14_baseline_p_BBbetasync.dat');
bsync = textread(fname, '', 'commentstyle', 'shell');
ax8 = axes('Position', [0.65 0.85 0.14 0.14]);
plot(bsync(:,1), bsync(:,2) / max(bsync(:,2)), 'k', 'LineWidth', 2);
% plot prior
hold all;
x = [-4.2:0.01:-1.9];
plot(x, exp(-1 * (x + 3.1).^2 / (2 * 0.3^2)), 'r--');
% axes
xlabel('\beta_{s}');
set(ax8, 'XLim', [-4.4 -1.8], 'XTick', [-4:1:-2]);
set(ax8, 'YLim', [0 10.5/10], 'YTick', [0,1]);
%set(ax8, 'YTickLabel', '');

% epsilon
fname = strcat(fpath,'BK14_baseline/BK14_baseline_p_BBdustsynccorr.dat');
epsilon = textread(fname, '', 'commentstyle', 'shell');
ax9 = axes('Position', [0.85 0.85 0.14 0.14]);
plot(epsilon(:,1), epsilon(:,2) / max(epsilon(:,2)), 'k', 'LineWidth', 2);
% plot prior
hold all;
x = [-0.1, 0, 0, 1, 1, 1.1];
y = [0, 0, 1, 1, 0, 0];
plot(x, y, 'r--');
% axes
xlabel('\epsilon');
set(ax9, 'XLim', [-0.1 1.1], 'XTick', [0:0.5:1]);
set(ax9, 'YLim', [0 10.5/10], 'YTick', [0,1]);
%set(ax9, 'YTickLabel', '');

% alpha_dust
fname = strcat(fpath,'BK14_baseline/BK14_baseline_p_BBalphadust.dat');
adust = textread(fname, '', 'commentstyle', 'shell');
ax10 = axes('Position', [0.85 0.65 0.14 0.14]);
plot(adust(:,1), adust(:,2) / max(adust(:,2)), 'k', 'LineWidth', 2);
% plot prior
hold all
x = [-1.1, -1, -1, 0, 0, 0.1];
y = [0, 0, 1, 1, 0, 0];
plot(x, y, 'r--');
% axes
xlabel('\alpha_{d}');
xlim([-1.05 0.05]);
set(ax10, 'XLim', [-1.1 0.1], 'XTick', [-1:0.5:0]);
set(ax10, 'YLim', [0 10.5/10], 'YTick', [0,1]);
%set(ax10, 'YTickLabel', '');

% alpha_sync
fname = strcat(fpath,'BK14_baseline/BK14_baseline_p_BBalphasync.dat');
async = textread(fname, '', 'commentstyle', 'shell');
ax11 = axes('Position', [0.85 0.45 0.14 0.14]);
plot(async(:,1), async(:,2) / max(async(:,2)), 'k', 'LineWidth', 2);
% plot prior
hold all
x = [-1.1, -1, -1, 0, 0, 0.1];
y = [0, 0, 1, 1, 0, 0];
plot(x, y, 'r--');
% axes
xlabel('\alpha_{s}');
set(ax11, 'XLim', [-1.1 0.1], 'XTick', [-1:0.5:0]);
set(ax11, 'YLim', [0 10.5/10], 'YTick', [0,1]);
%set(ax11, 'YTickLabel', '');

% Print figure.
print -depsc paper_plots/baseline_plot.eps 
fix_lines('paper_plots/baseline_plot.eps');
!epstopdf paper_plots/baseline_plot.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%
function like_evolmain
% plot for main body of paper showing evolution

% BKP baseline
load('/n/home08/pryke/bicep2_analysis/b2p_plots/likevar_dust.mat')
% BK13 with model changes
bk13_150=load('/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/BK14_bk13baseline/BK14_bk13baseline_p_r.dat');
% BK14_150
bk14_150=load('/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/BK14_bk150change/BK14_bk150change_p_r.dat');
% BK14 baseline
bk14=load('/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/BK14_baseline/BK14_baseline_p_r.dat');
% BK14 baseline no beta prior
bk14nbp=load('/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/BK14_nobetadprior/BK14_nobetadprior_p_r.dat');

% fig 7 of BKP is 300x300 and 0.7\columnwidth
clf; setwinsize(gcf,300,300);

y=sum(sum(lc1.l,1),3); plot(lc1.r,y./max(y),'r');
hold on
plot(bk13_150(:,1),bk13_150(:,2),'g');
plot(bk14_150(:,1),bk14_150(:,2),'b');
plot(bk14(:,1),bk14(:,2),'k', 'LineWidth', 2);
plot(bk14nbp(:,1),bk14nbp(:,2),'m');
hold off

ylabel('L/L_{peak}');
xlabel('r');
set(gca, 'XLim', [0 0.22], 'XTick', [0:0.04:0.2]);
set(gca, 'YLim', [0 10.5/10], 'YTick', [0.2:0.2:1]);
legend({'BKP baseline','+ model changes','BK14_{150}','BK14 baseline','no \beta_d prior'})
legend boxoff

% calc r limits for no beta prior case
disp(' '); disp('no beta prior r-values and 95% limit:')
l_r=load('/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/BK14_nobetadprior/BK14_nobetadprior_p_r.dat');
cons1d_stats(l_r(:,1),l_r(:,2))

% look at beta_d constraint
l_bd=load('/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/BK14_nobetadprior/BK14_nobetadprior_p_BBbetadust.dat');
disp(' '); disp('no beta prior beta curve numbers:')
cons1d_stats(l_bd(:,1),l_bd(:,2))

print -depsc2 paper_plots/evolmain.eps
fix_lines('paper_plots/evolmain.eps');
!epstopdf paper_plots/evolmain.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%
function like_evolution
% Plots the historical evolution plot

figure(1)
% fig 6 in BKP was 1000x250 and 0.9\textwidth
clf; setwinsize(gcf,1000,250);


% Fetch the files from the right location.
% For the BKP Fig6 result, look in the plot directory for b2p
fname{1} = '/n/home08/pryke/bicep2_analysis/b2p_plots/likevar_dust.mat';
% For the BKP Fig8 result, look in the plot directory for b2p 
fname{2} = '/n/home08/pryke/bicep2_analysis/b2p_plots/sync_dust.mat';
% For further developments, look in the CosmoMC outputs folder
fpath = '/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/';
% File location corresponding to the run in which we go from 5 to 9 bins 
folder{3} = 'BK14_bkpanl_5to9';
fname{3} = strcat(fpath,folder{3});
% File location corresponding to the run in which we change yr-split to autp
folder{4} = 'BK14_bkpanl_planckauto';
fname{4} = strcat(fpath,folder{4});
% File location corresponding to the run in which we add WMAP
folder{5} = 'BK14_bkpanl_wmap';
fname{5} = strcat(fpath,folder{5});
% File location corresponding to the run in which we add beta_sync prior
folder{6} = 'BK14_bkp_betasprior';
fname{6} = strcat(fpath,folder{6});
% File location corresponding to the run in which we free up dust/sync correlation
folder{7} = 'BK14_bkp_corr';
fname{7} = strcat(fpath,folder{7});
% File location corresponding to the run in which we free up alpha_dust and
% alpha_sync
folder{8} = 'BK14_bk13baseline';
fname{8} = strcat(fpath,folder{8});
% File location corresponding to the run in which we change from BK13_150 to BK14_150
folder{9} = 'BK14_bk150change';
fname{9} = strcat(fpath,folder{9});
% File location corresponding to the run in which we add BK14_95
folder{10} = 'BK14_baseline';
fname{10} = strcat(fpath,folder{10});

% Colors for each line
color = {'r', 'r', 'b', 'c', 'm', [1,0.7,0], 'g', 'b', 'k', 'k'};
ltype = {'-', '--', '-', '-', '-', '-', '-', '--', '--', '-'};
for i = 1:length(color)
  % Load 1D likelihoods
  if ((i == 1) | (i ==2))
    load(fname{i});
    y=summd(lc1.l,2); y1 = summd(lc1.l,1); y2 = summd(lc1.l,4);
  else
    l_r = load(strcat(fname{i},'/', folder{i}, '_p_r.dat'));
    l_d = load(strcat(fname{i}, '/', folder{i}, '_p_BBdust.dat'));
    l_s = load(strcat(fname{i}, '/', folder{i}, '_p_BBsync.dat'));
  end

  % Plot all 1D likelihoods for r
  ax1 = subplot(1,3,1);
  hold all;
  if ((i == 1) | (i == 2))
    plot(lc1.r,y./max(y), 'Color', color{i}, 'LineStyle', ltype{i});
  elseif i == 10
    plot(l_r(:,1), l_r(:,2), 'Color', color{i}, 'LineStyle', ltype{i}, 'LineWidth', 2);
  else
    plot(l_r(:,1), l_r(:,2), 'Color', color{i}, 'LineStyle', ltype{i});
  end
  set(ax1, 'Box', 'on');
  set(ax1, 'XLim', [0 0.22], 'XTick', [0:0.04:0.2]);
  set(ax1, 'YLim', [0 1.05], 'YTick', [0:0.2:1]);
  ylabel('L/L_{peak}');
  xlabel('r');

  % Plot all 1D likelihoods for Ad
  ax2 = subplot(1,3,2);
  hold all;
  if ((i == 1) | (i == 2))
    plot(lc1.A_d,y1./max(y1), 'Color', color{i}, 'LineStyle', ltype{i});
  elseif i == 10
    plot(l_d(:,1), l_d(:,2), 'Color', color{i}, 'LineStyle', ltype{i}, 'LineWidth', 2);
  else
    plot(l_d(:,1), l_d(:,2), 'Color', color{i}, 'LineStyle', ltype{i});
  end
  set(ax2, 'Box', 'on');
  set(ax2, 'XLim', [0 11], 'XTick', [0:2:10]);
  set(ax2, 'YLim', [0 1.05], 'YTick', [0:0.2:1]);
  xlabel('A_d @ l=80 & 353 GHz [\muK^2]');

  % Plot all 1D likelihoods for As
  ax3 = subplot(1,3,3);
  hold all;
  switch i
   case {1,2}
    x=lc1.A_s*freq_scaling([23,1],-3.3,[],150).^2;
    plot(x,y2./max(y2), 'Color', color{i}, 'LineStyle', ltype{i});
   case 10
    plot(l_s(:,1), l_s(:,2), 'Color', color{i}, 'LineStyle', ltype{i}, 'LineWidth', 2);
   otherwise
    plot(l_s(:,1), l_s(:,2), 'Color', color{i}, 'LineStyle', ltype{i});
  end
  set(ax3, 'Box', 'on');
  set(ax3, 'XLim', [0 40], 'XTick', [0:10:40]);
  set(ax3, 'YLim', [0 1.05], 'YTick', [0:0.2:1]);
  xlabel('A_{sync} @ l=80 & 23 GHz [\muK^2]');

  if i == 10
    ax3 = subplot(1,3,3);
    h = legend({'BKP baseline', 'BKP with sync', '+ 9 bandpowers', '+ Planck auto', '+ WMAP 23/33', '+ \beta_{sync} prior change', '+ \epsilon prior change', '+ \alpha_{d} & \alpha_{s} prior change', '+ BK14_{150}', '+ BK14_{95}'}, 'Position',[0.76, 0.55, 0.13, 0.15]);
    set(h,'PlotBoxAspectRatioMode','manual');
    set(h,'PlotBoxAspectRatio',[1 0.82 1]);
    legend boxoff
  end
end

print -depsc2 paper_plots/evolution_plot.eps
fix_lines('paper_plots/evolution_plot.eps');
!epstopdf paper_plots/evolution_plot.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%
function like_variation
% make plot comparing r-cons when varying model
figure(2)
% fig 6 in BKP was 1000x250 and 0.9\textwidth
clf; setwinsize(gcf,1000,250);
set(gcf, 'PaperPosition', [0 0 12 3]);

% Fetch the files from the right location.
fpath = '/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/';

% baseline
folder{1} = 'BK14_baseline';
fname{1} = strcat(fpath,folder{1});
% no beta_dust prior
folder{2} = 'BK14_nobetadprior';
fname{2} = strcat(fpath,folder{2});
% beta_sync tophat
folder{3} = 'BK14_nobetasprior';
fname{3} = strcat(fpath,folder{3});
% no Planck LFI
folder{4} = 'BK14_noPlanckLFI';
fname{4} = strcat(fpath,folder{4});
% no WMAP
folder{5} = 'BK14_noWMAP';
fname{5} = strcat(fpath,folder{5});
% dust/sync corr = 0.2
folder{6} = 'BK14_fixcorr';
fname{6} = strcat(fpath,folder{6});
% dust/sync corr = 0.2
% and alpha_dust = -0.42 and alpha_sync = -0.6
folder{7} = 'BK14_fixcorralpha';
fname{7} = strcat(fpath,folder{7});
% dust/sync corr = 0.2
% and fix alpha_sync = -0.6, alpha_dust = -0.42
% and include EE and EB, with EE/BB=2
folder{8} = 'BK14_bestinfo';
fname{8} = strcat(fpath,folder{8});

% Colors for each line
color = {'k', 'c', 'g', 'm', [1,0.7,0], 'r', 'b', 'k'};
ltype = {'-', '-', '-', '-', '-', '--', '-', '--'};
for i = 1:8
  % Load 1D likelihoods
  l_r = load(strcat(fname{i},'/', folder{i}, '_p_r.dat'));
  l_d = load(strcat(fname{i}, '/', folder{i}, '_p_BBdust.dat'));
  l_s = load(strcat(fname{i}, '/', folder{i}, '_p_BBsync.dat'));
  
  % Plot all 1D likelihoods for r
  ax1 = subplot(1,3,1);
  hold all;
  if i == 1
    plot(l_r(:,1), l_r(:,2), 'Color', color{i}, 'LineStyle', ltype{i}, 'LineWidth', 2);
  else
    plot(l_r(:,1), l_r(:,2), 'Color', color{i}, 'LineStyle', ltype{i});
  end
  set(ax1, 'Box', 'on');
  set(ax1, 'XLim', [0 0.22], 'XTick', [0:0.04:0.2]);
  set(ax1, 'YLim', [0 1.05], 'YTick', [0:0.2:1]);
  ylabel('L/L_{peak}');
  xlabel('r');

  % Plot all 1D likelihoods for Ad
  ax2 = subplot(1,3,2);
  hold all;
  if i == 1
    plot(l_d(:,1), l_d(:,2), 'Color', color{i}, 'LineStyle', ltype{i}, 'LineWidth', 2);
  else
    plot(l_d(:,1), l_d(:,2), 'Color', color{i}, 'LineStyle', ltype{i});
  end
  set(ax2, 'Box', 'on');
  set(ax2, 'XLim', [0 11], 'XTick', [0:2:10]);
  set(ax2, 'YLim', [0 1.05], 'YTick', [0:0.2:1]);
  xlabel('A_d @ l=80 & 353 GHz [\muK^2]');

  % Plot all 1D likelihoods for As
  ax3 = subplot(1,3,3);
  hold all;
  if i == 1
    plot(l_s(:,1), l_s(:,2), 'Color', color{i}, 'LineStyle', ltype{i}, 'LineWidth', 2);
  else
    plot(l_s(:,1), l_s(:,2), 'Color', color{i}, 'LineStyle', ltype{i});
  end
  set(ax3, 'Box', 'on');
  set(ax3, 'XLim', [0 40], 'XTick', [0:10:40]);
  set(ax3, 'YLim', [0 1.05], 'YTick', [0:0.2:1]);
  xlabel('A_{sync} @ l=80 & 23 GHz [\muK^2]');

  if i == 8
    ax3 = subplot(1,3,3);
    h = legend({'Baseline Analysis', 'no \beta_d prior', '-4<\beta_s<-2', ...
                'no Planck LFI', 'no WMAP', '\epsilon=0.2', '+ fix \alpha', ...
                '+ inc. EE (EE/BB=2)'}, 'Position',[0.76, 0.55, 0.13, 0.15]);
    set(h,'PlotBoxAspectRatioMode','manual');
    set(h,'PlotBoxAspectRatio',[1 0.83 1]);
    legend boxoff
  end
end

% Print numbers for paper.
% 1. For no beta_dust prior, print Gaussian approximation to beta_d posterior.
bd = load([fname{2} '/' folder{2} '_p_BBbetadust.dat']);
imax = find(bd(:,2) == max(bd(:,2)));
bdmax = bd(imax,1);
bdlow = interp1(bd(1:imax,2), bd(1:imax,1), exp(-0.5));
bdhigh = interp1(bd(imax:end,2), bd(imax:end,1), exp(-0.5));
sigma = mean([bdmax - bdlow, bdhigh - bdmax]);
disp(sprintf(['No beta_dust prior: beta_d posterior is approx. Gaussian with ' ...
              'mean = %0.2f, sigma = %0.2f'], bdmax, sigma));
% 2. For no beta_dust prior, repeat statistics for r.
r = load([fname{2} '/' folder{2} '_p_r.dat']);
cons1d_stats(r(:,1), r(:,2));

print -depsc2 paper_plots/variation_plot.eps
fix_lines('paper_plots/variation_plot.eps');
!epstopdf paper_plots/variation_plot.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%
function like_validation
% Validation plot. Plot histograms of recovered ML values
%a = load('/n/bicepfs2/users/vbuza/2015_09/24/ml_files/ml_bk14.mat');
a = load('/n/bicepfs2/users/vbuza/2015_10/21/ml_files_oldlens/ml_bk14_6D_pivot23_v1.mat');
% mask out badly behaved realizations
mask_5D = [188, 310, 379, 472, 490, 499];
mask_6D = [83, 142, 253, 256, 376, 485, 490];
a.ml(mask_6D,:) = NaN;

figure(2)
% fig 6 in BKP was 1000x250 and 0.9\textwidth
clf; setwinsize(gcf,800,200);

% r_ML
subplot(1,3,1)
hfill(a.ml(:,1),30,-0.1,0.1);
rm = nanmean(a.ml(:,1));
ri = 0;
ylim([0,80]);
hold on; plot([ri ri], ylim, 'k'); plot([rm rm], ylim, 'r');
xlabel('r_{ML}')

disp(' ');
disp(sprintf('sigma(r)=%.3f',nanstd(a.ml(:,1))));

% Ad_ML
subplot(1,3,2)
hfill(a.ml(:,3),30,0,8);
dm = nanmean(a.ml(:,3));
di = 3.75;
ylim([0,80]);
hold on; plot([di di], ylim, 'k'); plot([dm dm], ylim, 'r');
xlabel('A_{d,ML}')

% As_ML
subplot(1,3,3)
%hfill(a.ml(:,2),40,-1.2e-4,1.2e-4);
hfill(a.ml(:,2), 30, -3, 3);
sm = nanmean(a.ml(:,2));
si = 0;
ylim([0,80]);
hold on; plot([si si], ylim, 'k'); plot([sm sm], ylim, 'r');
xlabel('A_{sync,ML}')
%set(gca, 'XLim', [-1.2e-4 1.2e-4], 'XTick', [-1e-4:5e-5:1e-4]);
set(gca, 'XLim', [-3 3], 'XTick', [-3:1.5:3]);

%print figure
print -depsc paper_plots/validation_plot.eps
fix_lines('paper_plots/validation_plot.eps');
!epstopdf paper_plots/validation_plot.eps

return

%%%%%%%%%%%%%%%%%%%%%
function find_mlmodel
% search for maximum likelihood models with and without priors
% Clem thinks it would be better to examine the COSMOMC chain

likeopt.l = [2:10]; likeopt.sync_freq=23;
likedata = get_likedata('BK14', likeopt);
% Precalculate H-L likelihood terms
p0 = [0, 1, 0, 3.6, -3.0, -0.6, 1.59, -0.42, 1, 1, 0, 19.6];
C_fl = like_getexpvals(p0, likedata);
[ell, rms] = model_rms(p0, likedata);
bpcm = scale_bpcm(rms, likedata);
prep = hamimeche_lewis_prepare(ivecp((C_fl + likedata.N_l)'), bpcm);
opt = optimset('MaxIter',1e4,'MaxFunEvals',1e4,'TolX',1e-10,'TolFun',1e-10,'Display','iter');

% without priors
if(0)
  minfunc = @(x) -1*like_hl(like_getexpvals([x(1), 1, x(2:7), 1, 2, x(8), 19.6], likedata), likedata.real, likedata.N_l, prep);
  x0 = [p0(1) p0(3:8) p0(11)];
  [x,L,ev] = fminsearch(minfunc,x0,opt);
  ML = x;
end

% with priors
% what Victor sent
%minfunc = @(x) -1*like_hl(like_getexpvals([x(1),1,x(2:7),1,2,x(8),19.6],likedata),likedata.real,likedata.N_l,prep)+0.5*(x(6)-1.59)^2/0.11^2+0.5*(x(4)+3.1)^2/0.3^2+((x(5)+0.5)/0.5)^12+((x(7)+0.5)/0.5)^12+((x(8)-0.5)/0.5)^12;
% this works and delivers lower epsilon
minfunc = @(x) -1*like_hl(like_getexpvals([x(1),1,x(2:7),1,2,x(8),19.6],likedata),likedata.real,likedata.N_l,prep)+0.5*(x(6)-1.59)^2/0.11^2+0.5*(x(4)+3.1)^2/0.3^2+30*((x(5)+0.5)/0.5)^1000+30*((x(7)+0.5)/0.5)^1000+30*((x(8)-0.5)/0.5)^1000+30*((x(2)-5)/5)^1000;
% now pivot is 23GHz A_s x(2) wants to go a little negative - add
% prior to prevent this

x0 = [p0(1) p0(3:8) p0(11)];
[x,L,ev] = fminsearch(minfunc,x0,opt);
% goes to:
% 0.0260    1.4403    4.0575   -3.0549   -0.5640    1.6041   -0.1901    0.0047
MP = x

if(0)
% Use alternative minimizer (MINUIT from HEP)
% Interestingly this does not require A_s to be limited at zero to
% converge to the same place as fminsearch
freepar.free=ones(1,8);
% set bounds on parameters - really only on alphas and epsilon
% since the range is so wide on the others (and there are priors on
% beta_s and beta_d inside like_hl_matmin)
freepar.lb=[-10,-10,-10,-10,-1,-10,-1,0];
freepar.ub=[+10,+10,+10,+10, 0,+10, 0,1];
[x,xuncer,L,stat,cov]=matmin('like_hl_matmin',x0,freepar,'dummy',likedata,prep);
% goes to:
% 0.0260    1.4500    4.0570   -3.0537   -0.5492    1.6041   -0.1895    0.0000

%simd = extract_fgsims(likedata.opt.finalfile, likedata.opt,'simd');
%likedata.real=simd(:,:,1);
%[x,xuncer,L,stat,cov]=matmin('like_hl_matmin',x0,freepar,'dummy',likedata,prep);

end

% order of ML/MP is {r, As, Ad, \beta_s, \alpha_s, \beta_d, \alpha_d, \epsilon}

% compute chi2 of MP model
param=[x(1),1,x(2:7),1,2,x(8),19.6];
likedata.dof=length(cvec(likedata.real))-4;
% computes model values and chi2
likedata=compute_modelcomps(likedata,param);
disp(' ');
disp(sprintf('For ML model chi2=%.1f - for %d dof pte=%.3f',likedata.chi2,likedata.dof,likedata.pte));

% now compute chi2 vals of lensed-LCDM+dust sims to compare to this
% compute distribution of values from lensed-LCDM+dust sims
likeopt.l = [2:10]; likeopt.sync_freq=23;
%likeopt.fields = {'B'};
likedata = get_likedata('BK14', likeopt);
% Switch back to old lensing theory spectrum, to match sims.
likedata.opt.camb_lensed = 'camb_planck2013_r0_lensing.fits';
likedata = like_read_theory(likedata);
% Now we can set A_lens to 1 in model params, like usual.
mod = [0, 1, 0, 3.75, -3.3, -0.6, 1.59, -0.42, 1, 2, 0, 19.6];
[ell,cmp]=model_rms(mod,likedata);
bpcm=scale_bpcm(cmp,likedata); % This bpcm includes all sims.
icm = inv(bpcm);
likedata.expv=like_getexpvals(mod,likedata);
m = cvec(likedata.expv');
% Get simd realizations.
simd = extract_fgsims(likedata.opt.finalfile, likedata.opt, 'simd');
% Loop over realizations.
for ii=1:size(simd,3)
  ii
  d = cvec(simd(:,:,ii)');
  chi2s(ii) = (d - m)' * icm * (d - m); % Using same bpcm each time.
  likealt = likedata;
  likealt.opt.dropsim = ii;
  likealt = like_read_simset(likealt.opt.apsfile, likealt);
  bpcm_alt = scale_bpcm(cmp, likealt);
  chi2sp(ii) = (d - m)' * inv(bpcm_alt) * (d - m);
end
[bc,n]=hfill(chi2s,30,400,900); hplot(bc,n); grid
hfill(chi2sp,30,400,900,[],'Sg');
hold on;
t = chi2pdf(bc, numel(m));
plot(bc, numel(chi2s) * t / sum(t), 'r');
line([chi2r,chi2r],ylim,'color','m');
hold off;
legend({'bpcm from sims cleaned','same exclude each','chi2pdf','real val'})
ylim([0,100])

disp(' ');
disp(sprintf('fraction of chi2sp>chi2r = %.3f',sum(chi2sp>chi2r)/length(chi2sp)));

keyboard

return

%%%%%%%%%%%%%%%%%%%%%
function make_noilev
% update the plot summarizing noise levels adding wmap etc

% get the data
load('final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');

% can see this in opt.finalopt.mapname
f=[95,150,23,30,33,44,70,100,143,217,353];

figure(1); clf;
setwinsize(gcf,400,300);
xlim([0,370]); ylim([1e-4,1e1]);
set(gca,'yscale','log'); box on
hold on

% plot dust/sync as fine spaced curves
ff=linspace(1,400,100);
for i=1:numel(ff)
  % TODO: check these new 23GHz pivot vals are good
  sc(i)=3.8*freq_scaling([ff(i),1],-3.1,[],23)^2;
  dcu(i)=(4.3+1.2)*freq_scaling([ff(i),1],1.6,19.6,353)^2;
  dcl(i)=(4.3-1.0)*freq_scaling([ff(i),1],1.6,19.6,353)^2;
  dc(i)=(4.3+0.0)*freq_scaling([ff(i),1],1.6,19.6,353)^2;
end
% plot sync shaded region
h=patch([ff,ff(end),ff(1)],[sc,1e-9,1e-9],[1,0.7,0.7],'EdgeColor','none');
% plot dust shaded region
patch([ff,fliplr(ff)],[dcu,fliplr(dcl)],[0.7,0.7,1],'EdgeColor','none');

% find nominal gmean freqs
for i=1:9
  fgm95(i)=gmean(f(1),f(i+2));
  fgm150(i)=gmean(f(2),f(i+2));
end

% manually tune to push "points onto curves"
% this is necessary because the dust model is not a true power law
% (and neither is the sync model in the CMB units being used here)
% in practice only push the 95x353 and 150x353 points
fgm95(9)=fgm95(9)+40;
fgm150(9)=fgm150(9)+20;
if(0)
  % figure out tuning above
  for i=1:numel(f)
    sc(i)=0.0003*freq_scaling([f(i),1],-3.3,[],150)^2;
    dc(i)=3.3*freq_scaling([f(i),1],1.59,19.6,353)^2;
  end
  for i=1:9
    plot(fgm95(i),gmean(dc(1),dc(i+2)),'rx');
    plot(fgm150(i),gmean(dc(2),dc(i+2)),'gx');
    plot(fgm95(i),gmean(sc(1),sc(i+2)),'r.');
    plot(fgm150(i),gmean(sc(2),sc(i+2)),'g.');
  end
end

% llcdm and r are same values at all ell
xl=xlim;
llcdm=mean(r(1).sim(3,4,:),3);
rp2=mean(r(1).simr(3,4,:),3)-llcdm;
line(xl,[llcdm,llcdm],'color','k');
text(260,1e-3,'lensed-LCDM','color','k');
line(xl,[rp2,rp2]/4,'color','k','linestyle','--');
text(260,4e-3,'r=0.05','color','k');

[c1,c2,c3,c4,c5]=get_colors();

% plot the noise bandpower uncertainties in BB ell=70 bin
bkn95=std(r(1).noi(3,4,:),[],3);
bkn150=std(r(2).noi(3,4,:),[],3);
j=aps_getxind(length(r),1,2);
bkn95x150=std(r(j).noi(3,4,:),[],3);
plot(f(1),bkn95,'.','color',c1,'MarkerSize',15);
plot(gmean(f(1),f(2)),bkn95x150,'x','color',c2,'MarkerSize',10);
plot(f(2),bkn150,'.','color',c3,'MarkerSize',15);

for i=1:9
  % get WMAP/Planck autos
  j=aps_getxind(length(r),i+2,i+2);
  a(i)=std(r(j).noi(3,4,:),[],3);
  % get BKxExt
  j=aps_getxind(length(r),1,i+2);
  c95(i)=std(r(j).noi(3,4,:),[],3);
  j=aps_getxind(length(r),2,i+2);
  c150(i)=std(r(j).noi(3,4,:),[],3);
end
% plot ext auto noise uncer curve
plot(f(3:11),a,'k.:','MarkerSize',7);

% plot regular cross spectral noise curve
plot(fgm95,c95,'x:','color',c1);
plot(fgm150,c150,'x:','color',c3);

if(0)
  % make up a BK15 220x220 point as 10x better than Planck 220 (see
  % email Subj "220 rocks!" 10/2/2014
  plot(220,a(8)/10,'.','color',c5,'MarkerSize',15);
  text(220+10,a(8)/10,'220x220','color',c5)
end

% do text at end to make sure on top of lines
text(42,2e0,'Sync upper limit','Rotation',-72);
text(250,0.17,'Dust','Rotation',35);
text(f(1)+10,bkn95,'95x95','color',c1)
text(gmean(f(1),f(2))+10,bkn95x150,'95x150','color',c2)
text(f(2)+10,bkn150,'150x150','color',c3)
text(100,0.13,'Ext noise uncer.','Rotation',0);
text(fgm95(end)+10,c95(end),'95xExt','color',c1);
text(fgm150(end)+10,c150(end),'150xExt','color',c3);

hold off

xlabel('Nominal band center [GHz]');
ylabel('BB in l\sim80 bandpower l(l+1)C_l/2\pi [\muK^2]')

% stop axis ticks from being obscured
set(gca,'Layer','top');

print -depsc2 paper_plots/noilev.eps
fix_lines('paper_plots/noilev.eps')
!epstopdf paper_plots/noilev.eps

return


function [cmb, dust, sync] = get_comp_perbin
% Get results from per ell bin component separation analysis.

% Get 1D marginalized likelihoods for CMB, dust, and sync.
% For each likelihood, calculate 68% and 95% HPD interval.
distdir = '/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_perbin/real';
for ii=1:9
  % CMB component.
  distfile = fullfile(distdir, sprintf('BK14_bin%i_p_CMBflat_BB.dat', ii));
  d = load(distfile);
  [cmb.int68(ii,:), cmb.max(ii)] = hpd_interval(d(:,1), d(:,2), 0.68);
  [cmb.int95(ii,:)] = hpd_interval(d(:,1), d(:,2), 0.95);
  % Dust component.
  distfile = fullfile(distdir, sprintf('BK14_bin%i_p_BBdust.dat', ii));
  d = load(distfile);
  [dust.int68(ii,:), dust.max(ii)] = hpd_interval(d(:,1), d(:,2), 0.68);
  [dust.int95(ii,:)] = hpd_interval(d(:,1), d(:,2), 0.95);
  % Sync component.
  distfile = fullfile(distdir, sprintf('BK14_bin%i_p_BBsync.dat', ii));
  d = load(distfile);
  [sync.int68(ii,:), sync.max(ii)] = hpd_interval(d(:,1), d(:,2), 0.68);
  [sync.int95(ii,:)] = hpd_interval(d(:,1), d(:,2), 0.95);  
end

return


%%%%%%%%%%%%%%%%%%%%%
function comp_perbin
% Make figure showing CMB, dust, and sync components in each ell bin. 
%
% CMB component doesn't distinguish between tensor vs lensing.
% Dust and sync amplitudes are defined at 150 GHz. 
% Analysis marginalizes over beta_dust, beta_sync, dust-sync correlation.
%   Prior(beta_dust) = Gaussian, mean=1.59, sigma=0.11
%   Prior(beta_sync) = Gaussian, mean=-3.1, sigma=0.3
% Analysis operates on one ell bin at a time (no correlation between bins).
% Error bars are 68% highest posterior density intervals derived with flat 
% non-negative (i.e. physical) priors applied to CMB, dust, and sync power.
%
% ini files: /n/bicepfs2/keck/pipeline/likelihood/ini/BK14_perbin
% chains: /n/bicepfs2/keck/pipeline/likelihood/chains/BK14_perbin
% dists: /n/bicepfs2/keck/pipeline/likelihood/dists/BK14_perbin

% Plot colors for CMB, dust, sync.
clr = {[0, 0, 0], [0, 0, 1], [1, 0, 0]};
% Offset in ell for CMB, dust, sync.
ofs = [0, 5, -5];

% Get actual ell bin centers.
load('/n/bicepfs1/bicep2/pipeline/final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');
r = weighted_ellbins(r, bpwf);
l = r(2).lc(2:10, 4); % Use BK14_150 BB bin centers.

% Get theory spectra.
likedata.opt.fields = {'B'};
likedata.opt.expt = {'dummy150'};
likedata.bandpass{1} = [150., 1];
likedata = like_read_theory(likedata);
% Tensor r=0.1
param = [0.1, 0, 0, 0, -3.1, -0.6, 1.59, -0.42, 1, 1, 0, 19.6];
[lfine, rms] = model_rms(param, likedata); 
r0p1 = rms(:,1,1).^2;
% Lensing
param = [0, 1, 0, 0, -3.1, -0.6, 1.59, -0.42, 1, 1, 0, 19.6];
[lfine, rms] = model_rms(param, likedata);
lens = rms(:,1,1).^2;
% Dust
% Adust = 4.X uK^2 (@ 353 GHz), beta_dust = 1.6, alpha_dust = -0.4
% Tdust = 19.6 K
param = [0, 0, 0, 4.3, -3.1, -0.6, 1.6, -0.4, 1, 1, 0, 19.6];
[lfine, rms] = model_rms(param, likedata);
dust = rms(:,1,3).^2;
% Sync (based on BKP upper limit)
% Async = 3e-4 uK^2 (@ 150 GHz), beta_sync = -3.1, alpha_sync = -0.6
% Note that confusion over beta_sync = -3.1 (BK14) vs -3.3 (BKP) doesn't 
% matter because we are plotting at the pivot frequency (150 GHz).
%param = [0, 0, 6e-4, 0, -3.1, -0.6, 1.59, -0.42, 1, 1, 0, 19.6];
%[lfine, rms] = model_rms(param, likedata);
%sync = rms(:,1,2).^2;

% % Get 1D marginalized likelihoods for CMB, dust, and sync.
[comp{1}, comp{2}, comp{3}] = get_comp_perbin();

% Set up figure.
clf;
setwinsize(gcf, 400, 300);
xlim([0, 330]); 
ylim([0, 0.05]);
xlabel('Multipole');
ylabel('l(l+1)C_l/2\pi [\muK^2] @ 150 GHz');
box on;
hold on;

% Plot theory spectra.
%plot(lfine(30:end), sync(30:end), 'Color', clr{3});
plot(lfine(30:end), dust(30:end), 'Color', clr{2});
plot(lfine, lens, 'Color', clr{1});
plot(lfine, r0p1/2+lens, '--', 'Color', clr{1});

% For each ell bin:
%   * Plot 68% interval as an error bar.
%   * Plot ML point if it is greater than zero.
%   * If the 68% interval hits zero, also plot 95% UL as a down arrow.
for ii=1:9
  for jj=1:3
    % Plot 68% interval.
    h1(jj)=plot((l(ii) + ofs(jj)) * [1 1], comp{jj}.int68(ii,:), ...
         'Color', clr{jj});
    % ML point, if > 0
    if comp{jj}.max(ii) > 0
      h2(jj)=plot(l(ii) + ofs(jj), comp{jj}.max(ii), '.', ...
           'Color', clr{jj}, 'MarkerSize', 10, 'LineWidth', 2);
    end
    % 95% upper limit, if 68% interval touches zero.
    if comp{jj}.int68(ii,1) == 0
      h3(jj)=plot((l(ii) + ofs(jj)), comp{jj}.int95(ii,2), 'v', ...
           'Color', clr{jj}, 'MarkerSize', 3.5, 'MarkerFaceColor', clr{jj});
    end
  end
end

legend([h2(1),h2(2),h3(3)],{'CMB','Dust','Sync'},'Location','nw');
legend boxoff

% Print figure.
%set(gcf, 'PaperPosition', [0 0 6 3], 'PaperUnits', 'inches');
print -depsc2 paper_plots/comp_perbin.eps
fix_lines('paper_plots/comp_perbin.eps')
!epstopdf paper_plots/comp_perbin.eps

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function comp_perbin_datarelease
% Write an ascii file containing ML points, 68% intervals, and 95%
% intervals for CMB, dust, and sync components in each of the nine
% science bins. This is for people who want to reproduce Figure 7.

% Get actual ell bin centers.
load('/n/bicepfs1/bicep2/pipeline/final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');
r = weighted_ellbins(r, bpwf);
lc = r(2).lc(2:10, 4); % Use BK14_150 BB bin centers.
ll = r(2).ll(2:10, 4);
lh = r(2).lh(2:10, 4);

% % Get 1D marginalized likelihoods for CMB, dust, and sync.
[comp{1}, comp{2}, comp{3}] = get_comp_perbin();

% Open file to write.
dstr = datestr(now, 'yyyymmdd');
dstr2 = datestr(now, 'yyyy-mm-dd');
fname = ['BK14_components_' dstr '.txt'];
fid = fopen(['paper_plots/' fname], 'w');

% Header information.
fprintf(fid, '# BICEP2/Keck Array October 2015 Data Products\n');
fprintf(fid, ['# BICEP2/Keck Array VI: Improved Constraints On Cosmology and ' ...
              'Foregrounds When Adding 95 GHz Data From Keck Array\n']);
fprintf(fid, '# http://bicepkeck.org/\n');
fprintf(fid, '#\n');
fprintf(fid, '# File: %s\n', fname);
fprintf(fid, '# Date: %s\n', dstr2);
fprintf(fid, '#\n');
fprintf(fid, ['# This file contains the per-ell bin spectral ' ...
              'decomposition found in Figure 7 of BICEP2/Keck Array VI.\n']);
fprintf(fid, ['# For the 150 GHz amplitude of CMB, dust, and synchrotron ' ...
              'components, we provide the most probable\n']);
fprintf(fid, ['# value in each bin, as well as 68%% and 95%% credible ' ...
              'intervals, all derived from 1D marginalized posteriors.\n']);
fprintf(fid, ['# We have additionally marginalized over the spectral ' ...
              'indices of the dust and synchrotron components,\n']);
fprintf(fid, ['# as well as possible dust-sync spatial correlation. ' ...
              'See BICEP2/Keck Array VI for more details.\n']);
fprintf(fid, '#\n');
fprintf(fid, ['# Points in this file should be used for display ' ...
              'purposes only. Component amplitudes in each ell bin\n']);
fprintf(fid, '# are highly correlated and correlations exist between ell bins as well.\n');
fprintf(fid, '#\n');
fprintf(fid, ['# Columns: lmin, lcenter, lmax, CMB max, CMB 68%% ' ...
              'low, CMB 68%% high, CMB 95%% low, CMB 95%% high,\n']);
fprintf(fid, ['#          dust max, dust 68%% low, dust 68%% high, ' ...
              'dust 95%% low, dust 95%% high, sync max, sync 68%% low, \n']);
fprintf(fid, '#          sync 68%% high, sync 95%% low, sync 95%% high\n');

% Print data.
for ii=1:9
  str1 = sprintf('%3i  %5.1f  %3i  ', ll(ii), lc(ii), lh(ii));
  str2 = sprintf('%9.2e  %9.2e  %9.2e  %9.2e  %9.2e  ', comp{1}.max(ii), ...
                 comp{1}.int68(ii,1), comp{1}.int68(ii,2), ...
                 comp{1}.int95(ii,1), comp{1}.int95(ii,2));
  str3 = sprintf('%9.2e  %9.2e  %9.2e  %9.2e  %9.2e  ', comp{2}.max(ii), ...
                 comp{2}.int68(ii,1), comp{2}.int68(ii,2), ...
                 comp{2}.int95(ii,1), comp{2}.int95(ii,2));
  str4 = sprintf('%9.2e  %9.2e  %9.2e  %9.2e  %9.2e\n', comp{3}.max(ii), ...
                 comp{3}.int68(ii,1), comp{3}.int68(ii,2), ...
                 comp{3}.int95(ii,1), comp{3}.int95(ii,2));
  fprintf(fid, [str1 str2 str3 str4]);
end
fclose(fid);

return %% END of comp_perbin_datarelease


function make_bk_vs_world

% Get actual ell bin centers.
load('/n/bicepfs1/bicep2/pipeline/final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');
r = weighted_ellbins(r, bpwf);
lc = r(2).lc(2:10, 4); % Use BK14_150 BB bin centers.
ll = r(1).ll(2:10,4);
lh = r(1).lh(2:10,4);

% get updated lens spectrum
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');
% this lens spec doesn't go high enough
ml.Cs_l(1900:end,4)=NaN;

% Get tensor BB spectrum.
tens = load_cmbfast('aux_data/official_cl/camb_planck2013_r0p1.fits');

% Legend location
text_x=[12,35,55];
text_y=logspace(log10(3e0),log10(5e1),6);

clf;
setwinsize(gcf,400,300)

loglog(ml.l,ml.Cs_l(:,4),'r');
hold on
text(2e1,2e-4,'lensing','Rotation',32);
xlim([10,2000]); ylim([1e-4,1e2]);

% Plot theory spectra
plot(tens.l, 0.5*tens.Cs_l(:,4), 'r--');
plot(tens.l, 0.5*tens.Cs_l(:,4) + ml.Cs_l(:,4), 'r:');
text(1.6e1, 6e-4, 'r=0.05', 'Rotation', 28);
plot(tens.l, 0.1*tens.Cs_l(:,4), 'r--');
text(3.8e1, 1.6e-4, 'r=0.01', 'Rotation', 25);

% Plot CMB-only bandpowers.
% Color for CMB-only bandpowers (black)
cc = [0 0 0];
% Add CMB component spectrum
[cmb, dust, sync] = get_comp_perbin();
% Select ell bins that should be plotted as upper limits
% (all points that are <1 sigma above zero).
ulbin = (cmb.int68(:,1) == 0);
% Plot points.
h = errorbar2(lc(~ulbin), cmb.max(~ulbin)', cmb.max(~ulbin)' - cmb.int68(~ulbin,1), ...
              cmb.int68(~ulbin,2) - cmb.max(~ulbin)', '.');
set(h, 'Color', cc);
herrorbar2(lc(~ulbin), cmb.max(~ulbin)', lc(~ulbin) - ll(~ulbin), ...
           lh(~ulbin) - lc(~ulbin), '.', cc);
plot(lc(~ulbin), cmb.max(~ulbin)', '.', 'Color', cc, 'MarkerSize', 12);
% Plot upper limits.
herrorbar2(lc(ulbin), cmb.int95(ulbin,2), lc(ulbin) - ll(ulbin), ...
           lh(ulbin) - lc(ulbin), 'v', cc);
% Label.
text(1.1e1, 7e-3, 'BK14', 'Color', cc);
text(1.1e1, 4e-3, 'CMB component', 'Color', cc);

% Plot other experiments
ms=4; % marker size

% DASI
other_data=load('other_experiments/dasi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,1/3,0]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(5),'DASI','Color',[1,1/3,0])
% CBI
other_data=load('other_experiments/cbi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,1]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(4),'CBI','Color',[0,1,1])
% CAPMAP
other_data=load('other_experiments/capmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(3),'CAPMAP','Color',[0.5,0,0.5])
% BOOMERANG
other_data=load('other_experiments/boomerang.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(2),'Boomerang','Color',[1,0,1])
% WMAP
other_data=load('other_experiments/wmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,0]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(1),'WMAP','Color',[0,1,0])
% QUAD
other_data=load('other_experiments/quad.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,0]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(5),'QUAD','Color',[1,0,0])
% BICEP1
other_data=load('other_experiments/bicep1.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(4),'BICEP1','Color',[0.5,0.5,0.5])
% QUIET-40
other_data=load('other_experiments/quiet40.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(3),'QUIET-Q','Color',[0.5,0.5,1])
% QUIET-90
other_data=load('other_experiments/quiet90.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(2),'QUIET-W','Color',[0,0,1])

% polarbear measurement of lensing
other_data=load('other_experiments/polarbear.txt');
ul=3;
meas=[1,2,4];
halfbw = (other_data(2,1)-other_data(1,1))/2;
halfbw = repmat(halfbw,4,1);
h=errorbar2(other_data(meas,1),other_data(meas,2),other_data(meas,3),'gs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','g');
h=herrorbar2(other_data(meas,1),other_data(meas,2),halfbw(meas),halfbw(meas),'gs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','g');
h=herrorbar2(other_data(ul,1),other_data(ul,4),halfbw(ul),halfbw(ul),'gv');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','g');
text(6e2, 2.3e-2,'Polarbear','Color',[0,1,0])

% sptpol http://arxiv.org/pdf/1503.02315.pdf tab1
other_data=load('other_experiments/sptpol.txt');
other_data(:,2)=other_data(:,2).*other_data(:,1)/(2*pi);
other_data(:,3)=other_data(:,3).*other_data(:,1)/(2*pi);
ul=[4,5];
meas=[1,2,3];
halfbw = (other_data(2,1)-other_data(1,1))/2;
halfbw = repmat(halfbw,5,1);
h=errorbar2(other_data(meas,1),other_data(meas,2),other_data(meas,3),'bs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');
h=herrorbar2(other_data(meas,1),other_data(meas,2),halfbw(meas),halfbw(meas),'bs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');
% decide to make upper limit just +2xerrorbar
h=herrorbar2(other_data(ul,1),other_data(ul,3)*2,halfbw(ul),halfbw(ul),'bv');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');
text(6e2, 1.3e-2, 'SPTpol','Color',[0,0,1])

% ACTPol, http://arxiv.org/pdf/1405.5524 table 3
other_data = load('other_experiments/actpol.txt');
% Only plot points 1,2 (3+4 are off the rhs of plot)
ul = [1,2];
% Calculate 95% upper limit by assuming Gaussian bandpower pdf
for ii=1:numel(ul)
  x = [0:0.01:5];
  y = exp(-0.5 * (x - other_data(ul(ii),4)).^2 / other_data(ul(ii),5)^2);
  cdf = cumsum(y) / sum(y);
  imax = min(find(cdf == 1));
  ulval(ul(ii)) = interp1(cdf(1:imax), x(1:imax), 0.95);
end
h=herrorbar2(other_data(ul,2), ulval(ul), other_data(ul,2) - other_data(ul,1), ...
             other_data(ul,3) - other_data(ul,2), 'v', [0.8,0.6,0]);
set(h(2),'MarkerSize',ms,'MarkerFaceColor',[0.8,0.6,0]);
text(6e2, 7.34e-3, 'ACTpol','Color',[0.8,0.6,0])

hold off

xlabel('Multipole')
ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')

set(gcf, 'PaperPosition', [0 0 6 4.5]);
print -depsc2 paper_plots/bk14_vs_world.eps
fix_lines('paper_plots/bk14_vs_world.eps')
!epstopdf paper_plots/bk14_vs_world.eps

return %% END of bk14_vs_world


function rlik_datarelease
% Make text file containing 1D likelihood for r.

% Load 1D r likelihood from baseline CosmoMC result.
r = load(['/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_paper/BK14_baseline/' ...
          'BK14_baseline_p_r.dat']);

% Open file to write.
dstr = datestr(now, 'yyyymmdd');
dstr2 = datestr(now, 'yyyy-mm-dd');
fname = ['BK14_r_likelihood_' dstr '.txt'];
fid = fopen(['paper_plots/' fname], 'w');

% Header information.
fprintf(fid, '# BICEP2/Keck Array October 2015 Data Products\n');
fprintf(fid, ['# BICEP2/Keck Array VI: Improved Constraints On Cosmology and ' ...
              'Foregrounds When Adding 95 GHz Data From Keck Array\n']);
fprintf(fid, '# http://bicepkeck.org/\n');
fprintf(fid, '#\n');
fprintf(fid, '# File: %s\n', fname);
fprintf(fid, '# Date: %s\n', dstr2);
fprintf(fid, '#\n');
fprintf(fid, ['# This text file contains the tabulated likelihood for the tensor-to-scalar ' ...
              'ratio, r, derived from BICEP2 and Keck Array maps, as well as external maps ' ...
              'from WMAP and Planck.\n']);
fprintf(fid, ['# This corresponds to the black curve in the upper left panel of Figure 4 ' ...
              'from BK-VI.\n']);
fprintf(fid, '#\n');
fprintf(fid, '# Columns: r, Likelihood(r)\n');

% Print data.
for ii=1:size(r,1)
  fprintf(fid, '%9.5f  %10.4e\n', r(ii,1), r(ii,2));
end
fclose(fid);

return %% END of rlik_datarelease


function powspec_all_datarelease(r, s)
% Make text file containing all BB spectra with errorbars, as in
% Figure 14 (make_powspec_all).

% Open file to write.
dstr = datestr(now, 'yyyymmdd');
dstr2 = datestr(now, 'yyyy-mm-dd');
fname = ['BK14_bandpowers_' dstr '.txt'];
fid = fopen(['paper_plots/' fname], 'w');

% Header information.
fprintf(fid, '# BICEP2/Keck Array October 2015 Data Products\n');
fprintf(fid, '# BICEP2/Keck Array VI: Improved Constraints On Cosmology and Foregrounds When Adding 95 GHz Data From Keck Array\n');
fprintf(fid, '# http://bicepkeck.org/\n');
fprintf(fid, '#\n');
fprintf(fid, '# File: %s\n', fname);
fprintf(fid, '# Date: %s\n', dstr2);
fprintf(fid, '#\n');
fprintf(fid, '# This file contains BB bandpowers from the BK14 analysis: a total of 11 auto and 55 cross spectra between \n');
fprintf(fid, '# BICEP2/Keck maps at 95 and 150 GHz, WMAP maps at 23 and 33 GHz, and Planck maps at 30, 44, 70, 100, 143, \n');
fprintf(fid, '# 217, and 353 GHz. These correspond to the points shown in Figure 14 of BK-VI (except bandpowers 1-9 are \n'); 
fprintf(fid, '# included here). The error bars are standard deviations of lensed-LCDM+noise simulations and hence contain \n');
fprintf(fid, '# no sample variance on any additional signal component. \n');
fprintf(fid, '#\n');
fprintf(fid, '# Bandpowers are specified as ell*(ell+1)*C_{ell}/(2*pi) in units of uK_{CMB}^2.\n');
fprintf(fid, '#\n');
fprintf(fid, '# Column 1: bin number\n');
fprintf(fid, '# Column 2: ell\n');
fprintf(fid, '# Columns 3-%03i: BB bandpower and error bar with the following order for the spectra:\n', 2+2*numel(r));
for ii=1:6:numel(s)
  fprintf(fid, '#                %s %s %s %s %s %s\n', s{ii}, s{ii+1}, ...
          s{ii+2}, s{ii+3}, s{ii+4}, s{ii+5});
end
fprintf(fid, '# For example, column 3 is the BK_95 x BK14_95 bandpower, column 4 is the error bar for that bandpower, \n');
fprintf(fid, '# column 5 is the BK_150 x BK_150 bandpower, and so on.\n');
fprintf(fid, '#\n');

% Print data.
lbin = [2:10];
for ii=1:numel(lbin)
  fprintf(fid, '%i  %5.1f', ii, r(2).lc(lbin(ii),4));
  for jj=1:numel(r)
    fprintf(fid, '  %11.4e  %10.4e', r(jj).real(lbin(ii),4), r(jj).derr(lbin(ii),4));
  end
  fprintf(fid, '\n');
end
fclose(fid);

return %% END of powspec_all_datarelease

