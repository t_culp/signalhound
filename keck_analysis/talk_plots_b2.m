function talk_plots_b2(pl)

set(0,'defaultlinelinewidth',1.0);
set(0,'DefaultAxesFontSize',14);
set(0,'defaulttextfontsize',14);

% make Jackknife explanation plot
if(any(pl==1))
  make_jackplot;
end

if(any(pl==2))
  make_eb_sequence;
end

if(any(pl==3))
  make_pol_over_pol;
end

if(any(pl==4))
  make_pol_over_t;
end

if(any(pl==5))
  make_skyproj;
end

if(any(pl==6))
  plot_eb_leakage;
end

if(any(pl==7))
  purification_steps();
end

if(any(pl==8))
  make_b2_vs_world;
end

if(any(pl==9))
  make_EEvsSims;
end

if(any(pl==10))
  make_rconstraint_cont;
end
 
if(any(pl==11))
  plot_bb_ambloss;
end

if(any(pl==12))
  make_EE_cont;
end

if(any(pl==13))
  make_bpdevs_panel;
end

if(any(pl==14))
  plot_bandpowerdev;
end

if(any(pl==15))
    make_EE_colorconstraint;
end

if(any(pl==16))
  make_BBvsSims;
end

if(any(pl==17))
  plot_beammapsims;
end

if(any(pl==18))
  make_jackplotV2;
end

if(any(pl==19))
  make_2Daps;
end

if(any(pl==20))
  make_BBpoweronly;
end

if(any(pl==21))
  make_mapOutlook2014;
end

if(any(pl==22))
  make_mapdepth_b2tk14;
end

if(any(pl==23))
  make_total_polarizations;
end

return

function dim=get_onepaneldims(m)
% Parameters for six-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 1 * dim.wide - 1 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 1 * dim.maph + dim.wide + 1 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2 (colorbar).
dim.y1 = dim.wide / dim.H; % Bottom edge of row 1.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return

function dim=get_twopaneldims(m)
% Parameters for six-panel layout.
dim.W = 4.8; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 1.05; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 2 * dim.maph + dim.wide + 2 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + 1 * dim.mapw + 1 * dim.thin) / dim.W; % Left edge of column 2 (colorbar).
dim.y1 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y2 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return

function [r,im,ml, bpwf, filename,mt,rscaling,jackname,mapname,projfile]=get_b2xb2
% Single function fetches official B2xB2 spectra and models for
% plotting etc - one point switch!

filename = '0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf_rbc.mat';
jackname = '';
mapname  = 'maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0.mat';
projfile = '';
load(['final/',filename])
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN;
im=inpmod;
rscaling='';
mt=[];

% get the lensing model to compare to lensing sims
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing.fits');

return

%%%%%%%%%%%%%%%%%%%%%%%%
function [r,im,ml, bpwf,filename,mt,rscaling,jackname,mapname,projfile]=get_k14
% Single function fetches official B2xB2 spectra and models for
% plotting etc - one point switch!
%  filename = 'final/0751x1353/real_a_fixesub_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp11022_jack01_real_b_filtp3_weight3_gs_dp11022_jack01_matrix_overrx_overall_directbpwf_debias_comb_lensfix.mat';
%  filename = 'final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_real_b_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall_directbpwf_debias_comb.mat';
% switch to map combine:
filename = '';
jackname = '';
mapname  = 'maps/1351/real_d_filtp3_weight3_gs_dp1102_jack01.mat';
projfile = '';
r=[];
im=[];
ml=[];
mt=[];
bpwf=[];
rscaling = '';

return

%%%%%%%%%%%%%%%%%%%%%%%%
function [r,im,ml, bpwf,filename,mt,rscaling,jackname,mapname,projfile]=get_k1213
% Single function fetches official B2xB2 spectra and models for
% plotting etc - one point switch!
%  filename = 'final/0751x1353/real_a_fixesub_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp11022_jack01_real_b_filtp3_weight3_gs_dp11022_jack01_matrix_overrx_overall_directbpwf_debias_comb_lensfix.mat';
%  filename = 'final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_real_b_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall_directbpwf_debias_comb.mat';
% switch to map combine:
filename = '';
jackname = '';
mapname  = 'maps/1351/real_ab_filtp3_weight3_gs_dp1102_jack01.mat';
projfile = '';
r=[];
im=[];
ml=[];
mt=[];
bpwf=[];
rscaling = '';

return

%%%%%%%%%%%%%%%%%%%%%%%%
function [r,im,ml, bpwf,filename,mt,rscaling,jackname,mapname,projfile]=get_b2k_comb
% Single function fetches official B2xB2 spectra and models for
% plotting etc - one point switch!
%  filename = 'final/0751x1353/real_a_fixesub_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp11022_jack01_real_b_filtp3_weight3_gs_dp11022_jack01_matrix_overrx_overall_directbpwf_debias_comb_lensfix.mat';
%  filename = 'final/0751x1351/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_real_b_filtp3_weight3_gs_dp1102_jack01_matrix_overrx_overall_directbpwf_debias_comb.mat';
% switch to map combine:
filename = 'final/1456/real_aab_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat';
jackname = '';
mapname  = 'maps/1456/real_aab_filtp3_weight3_gs_dp1102_jack0.mat';
projfile = '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat';
load(filename)
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN;
im=inpmod;

% get the lensing model to compare to lensing sims
%  ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing_lensfix.fits');
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');
%  ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing.fits');

% get the tensor only model, mainly to plot the EE tensor only
% this is currently not a fits file so be care full about the data columns
% 
mt=load('aux_data/official_cl/camb_planck2013_r0p1_tenscls.dat');
% extend it to the same l range as the other inputs. Note quite sure
% why it is not to start with. However, checked that tensor B is the same as in im
mt = interp1(mt(:,1),mt,ml.l);

%  rscaling = 'aps/0751x1351/xxx4_a_dp1100_jack0_xxx5_a_dp1100_jack0_xxx6_a_dp1100_jack0_xxx4_ab_dp1100_jack01_xxx5_ab_dp1100_jack01_xxx6_ab_dp1100_jack01_matrix_overrx_comb_lensfix.mat';
%  rscaling = 'aps/0751x1351/xxx4_a_dp1100_jack0_xxx5_a_dp1100_jack0_xxx6_a_dp1100_jack0_xxx4_ab_dp1100_jack01_xxx5_ab_dp1100_jack01_xxx6_ab_dp1100_jack01_matrix_overrx_comb.mat';
rscaling = '';

return

function make_jackplot() 
  
  maps = {...
    'maps/1450/real_a_filtp3_weight3_gs_dp0000_jack1.mat',...
    'maps/1450/real_a_filtp3_weight3_gs_dp0000_jack0.mat',...
    'maps/1450/real_a_filtp3_weight3_gs_dp1100_jack1.mat',...
    'maps/1450/real_a_filtp3_weight3_gs_dp1100_jack0.mat'};
    
  savename={...
    'dp0000_jack1',...
    'dp0000_jack0',...
    'dp1100_jack1',...
    'dp1100_jack0'}
  
  names = {...
    'Deck split w/o deprojection',...
    'map w/o deprojection',...
    'Deck split w/ differential pointing deprojection',...
    'map w/ differential pointing deprojection'}
  
  for ii=1:4
    load(maps{ii})
    map=make_map(ac,m,coaddopt);

    if size(map,2)
      map=map(1);
    end
    map=jackknife_map(map);
    map=cal_coadd_maps(map,get_ukpervolt);

    dim=get_twopaneldims(m);
    
    fig=figure;
    clf; 
    set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

    fs = 8;
    % first Q
    ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
    plot_map(m,map.Q,'iau');
    caxis([-4,4]);
    colormap gray
    freezeColors
    set(ax1,'FontSize',fs,'FontWeight','normal');
    set(ax1,'Xticklabel',[],'Xtick',[-50,0,50]);
    title(['Q ',names{ii}],'FontSize',fs);
      
    % second U
    ax4=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
    plot_map(m,map.U,'iau');
    caxis([-4,4]);
    colormap gray
    freezeColors
    set(ax4,'FontSize',fs,'FontWeight','normal');
    set(ax4,'Xtick',[-50,0,50]);
    title(['U ',names{ii}],'FontSize',fs);
    xlabel('Right ascension [deg.]');
    ylabel('Declination [deg.]');
    
    % colorbar
    ax2 = axes('Position',[dim.x2,dim.y1,dim.cbar/dim.W,dim.ymap]);
    set(ax2,'FontSize',fs,'FontWeight','normal');
    imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
    colormap gray;
    freezeColors;
    set(ax2,'Xtick',[]);
    set(ax2,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);
    
    ax5 = axes('Position',[dim.x2,dim.y2,dim.cbar/dim.W,dim.ymap]);
    set(ax5,'FontSize',fs,'FontWeight','normal');
    imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
    colormap gray;
    freezeColors;
    set(ax5,'Xtick',[]);
    set(ax5,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

    % colorbar units
    ax3 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
                'Visible', 'off');
    set(ax3, 'FontSize', 7, 'FontWeight', 'normal');
    text(-0.1, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 10, ...
        'FontWeight', 'normal', 'HorizontalAlignment', 'center');
        
    ax6 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
                'Visible', 'off');
    set(ax6, 'FontSize', 7, 'FontWeight', 'normal');
    text(-0.1, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 10, ...
        'FontWeight', 'normal', 'HorizontalAlignment', 'center');

    print('-depsc2', ['paper_plots/jack_maps_',savename{ii},'.eps'])
    fix_lines(['paper_plots/jack_maps_',savename{ii},'.eps'])
    system_safe(['epstopdf paper_plots/jack_maps_',savename{ii},'.eps'])
    system_safe(['convert -density 200 paper_plots/jack_maps_',savename{ii},'.pdf paper_plots/jack_maps_',savename{ii},'.png'])    
    
  end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function make_mapdepth_b2tk14

% plots the 2-panel livetime, instantaneous sens, and cum map depth

% load the instantaneous sensitivity
a=load(['/n/bicepfs2/keck/pipeline/data/real/k12_net_perscanset.mat']);
b=load(['/n/bicepfs2/keck/pipeline/data/real/k13_net_perscanset.mat']);
c=load(['/n/bicepfs2/keck/pipeline/data/real/k14d_net_perscanset.mat']);
%concatenate
cmbtags={a.cmbtags{:} b.cmbtags{:} c.cmbtags{:}};
net=[a.net' b.net' c.net'];
net100=c.net100;

%find the time
for ii=1:length(cmbtags)
  ttime(ii)=datenum(cmbtags{ii}(1:8),'yyyymmdd');
end
for ii=1:length(c.cmbtags)
  t100time(ii)=datenum(c.cmbtags{ii}(1:8),'yyyymmdd');
end

% make years
for yr=2010:2015
  yeartime(yr-2009)=datenum(yr,0,0);
end

%average the NET over ~10
net(isinf(net))=NaN; net100(isinf(net100))=NaN;
for ii=1:floor(length(net)*.1)
  indx=(ii-1)*10+1:ii*10;
  net_pp(ii)=nanmean(net(indx).^(-2))^(-.5);
  ptime(ii)=ttime(indx(1));
end
for ii=1:floor(length(net100)*.1)
  indx=(ii-1)*10+1:ii*10;
  net_pp100(ii)=nanmean(net100(indx).^(-2))^(-.5);
  p100time(ii)=t100time(indx(1));
end

%same with b2....
b2=load('~/keckpipe/20140220_b2detparamplots/bicep2_net_perscanset.mat');
b2.net(isinf(b2.net))=NaN;
for ii=1:length(b2.cmbtags)
  b2ttime(ii)=datenum(b2.cmbtags{ii}(1:8),'yyyymmdd');
end
for ii=1:floor(length(b2.net)*.1)
  indx=(ii-1)*10+1:ii*10;
  b2_net_pp(ii)=nanmean(b2.net(indx).^(-2))^(-.5);
  b2ptime(ii)=b2ttime(indx(1));
end

%%%%%%%%%%%%%%%%%%%%
% Load the cumulative map depth
load(['/n/panlfs2/bicep/keck/pipeline/maps/1325/b2tk14_d_cum_depth.mat']);
% convert from phase to time
for ii=1:length(ph.name)
  phtime(ii)=datenum(ph.name{ii}(1:8),'yyyymmdd');
end
% total depth is inverse sum of the Q and U?
depth=sqrt(2)*(Qd.^(-2)+Ud.^(-2)).^-.5;
depth100=sqrt(2)*(Qd100.^(-2)+Ud100.^(-2)).^-.5;
% area is the average of Q and U
area=(Qa+Ua)./2;
area100=(Qa100+Ua100)./2;
% total map depth is depth x area
sens=depth./sqrt(area)/sqrt(2);
sens100=depth100./sqrt(area100)/sqrt(2);

%%%%%%%%%%%%%%%%%%%%

% Set up some axes now so we can do fiddly things with them later
clf;
h=gcf;
setwinsize(gcf,800,350);
set(gcf,'DefaultAxesFontSize',11)
set(0,'defaultlinelinewidth',1.0);
xticks=[datenum(2010,7,0),datenum(2011,7,0),datenum(2012,7,0),datenum(2013,7,0),datenum(2014,7,0)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%inst sens
subplot_grid(2,1,1,0,1)
plot(b2ptime,b2_net_pp,'.','color',[0,.5,0]); hold on;
plot(ptime,net_pp,'.','color',[.2353,.7020,.4431])
plot(p100time,net_pp100,'.','color',[.698,0.13,.13]);
datetick('x'); legend('Bicep2','Keck 150 GHz','Keck 95 GHz','Location','SW');
% mark the years
for ii=1:length(yeartime)
  plot([yeartime(ii),yeartime(ii)],[0 100],'--k');
end
xlim([yeartime(1) yeartime(end)]);
set(gca,'XTick',xticks);
set(gca,'Ytick',[0,10,20,30,40]);
datetick('x','YYYY','keeplimits','keepticks');
ylabel('NET [\muK sqrt s]');
ylim([0 40]);
subplot_grid2(2,1,1,0,1)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%cum depth
subplot_grid(2,1,2,0,1)
semilogy(phtime,depth*1e3,'.','color',[0,.5,0]); hold on
semilogy(phtime(800:end),depth(800:end)*1e3,'.','color',[.2353,.7020,.4431]);
semilogy(phtime(end-length(depth100)+1:end),depth100*1e3,'.','color',[.698,0.13,.13])
legend('Bicep2','Bicep2 + Keck 150 GHz','Keck 95 GHz','Location','SW')
for ii=1:length(yeartime)
  plot([yeartime(ii),yeartime(ii)],[10 1000],'--k');
end
xlim([yeartime(1) yeartime(end)]);
set(gca,'XTick',xticks);
datetick('x','YYYY','keeplimits','keepticks');
ylim([30 5e2]);
ylabel('Map Depth [nK deg]');
xlabel('Time')

% this works way better than direct print to pdf
set(gcf,'PaperPositionMode','auto')
print -depsc2 paper_plots/b2tk14_depth_sens.eps
!epstopdf paper_plots/b2tk14_depth_sens.eps
system_safe(['convert -density 200 paper_plots/b2tk14_depth_sens.pdf paper_plots/b2tk14_depth_sens.png'])

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_eb_sequence()

ellrng=[50,120];
projfile='matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat';

load maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0 ac m
map=make_map(ac,m);
map=cal_coadd_maps(map,get_ukpervolt);
map=make_tqu_apmap(m,map,[],ellrng);
map=make_ebmap(m,map,[],ellrng,projfile);

% vector length scale factor
ve=0.6;
vb=0.6*6.4;

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_pol(m,map.Qap,map.Uap,map.Qvar,map.Uvar,ve,[],3,1)
title('BICEP2 total polarization signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
print -depsc2 total_pol.eps
!epstopdf total_pol.eps
system_safe(['convert -density 200 total_pol.pdf total_pol.png'])

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_pol(m,map.EQ,map.EU,map.Qvar,map.Uvar,ve,[],3,1)
title('BICEP2 E-mode signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
print -depsc2 e_only.eps
!epstopdf e_only.eps
system_safe(['convert -density 200 e_only.pdf e_only.png'])

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_pol(m,map.BQ,map.BU,map.Qvar,map.Uvar,ve,[],3,1)
title('BICEP2 B-mode signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
print -depsc2 b_only.eps
!epstopdf b_only.eps
system_safe(['convert -density 200 b_only.pdf b_only.png'])

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_pol(m,map.BQ,map.BU,map.Qvar,map.Uvar,vb,[],3,1)
title('BICEP2 B-mode signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
print -depsc2 b_only_zoom.eps
!epstopdf b_only_zoom.eps
system_safe(['convert -density 200 b_only_zoom.pdf b_only_zoom.png'])

% cropped maps below

upsample_factor=2;
[m2,map2.Qap]=upsample_map(m,map.Qap,upsample_factor);
[m2,map2.Uap]=upsample_map(m,map.Uap,upsample_factor);
[m2,map2.B]=upsample_map(m,map.B,upsample_factor);
[m2,map2.BQ]=upsample_map(m,map.BQ,upsample_factor);
[m2,map2.BU]=upsample_map(m,map.BU,upsample_factor);
[m2,map2.Qvar]=upsample_map(m,map.Qvar,upsample_factor);
[m2,map2.Uvar]=upsample_map(m,map.Uvar,upsample_factor);

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_pol(m2,map2.Qap,map2.Uap,map2.Qvar,map2.Uvar,ve,[],3*(2*upsample_factor-1),0,[-30.1,30.1,-65.9,-49.1])
title('BICEP2 total polarization signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
print -depsc2 total_pol_rect.eps
!epstopdf total_pol_rect.eps
system_safe(['convert -density 200 total_pol_rect.pdf total_pol_rect.png'])

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_pol(m2,map2.BQ,map2.BU,map2.Qvar,map2.Uvar,ve,[],3*(2*upsample_factor-1),0,[-30.1,30.1,-65.9,-49.1])
title('BICEP2 B-mode signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
print -depsc2 b_only_rect.eps
!epstopdf b_only_rect.eps
system_safe(['convert -density 200 b_only_rect.pdf b_only_rect.png'])

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_pol(m2,map2.BQ,map2.BU,map2.Qvar,map2.Uvar,vb,[],3*(2*upsample_factor-1),0,[-30.1,30.1,-65.9,-49.1])
title('BICEP2 B-mode signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
print -depsc2 b_only_zoom_rect.eps
!epstopdf b_only_zoom_rect.eps
system_safe(['convert -density 200 b_only_zoom_rect.pdf b_only_zoom_rect.png'])

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function make_pol_over_pol

ellrng=[50,120];
projfile='matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat';

load maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0 ac m
map=make_map(ac,m);
map=cal_coadd_maps(map,get_ukpervolt);
map=make_ebmap(m,map,[],ellrng,projfile);

upsample_factor=2;
[m2,map2.E]=upsample_map(m,map.E,upsample_factor);
[m2,map2.EQ]=upsample_map(m,map.EQ,upsample_factor);
[m2,map2.EU]=upsample_map(m,map.EU,upsample_factor);
[m2,map2.B]=upsample_map(m,map.B,upsample_factor);
[m2,map2.BQ]=upsample_map(m,map.BQ,upsample_factor);
[m2,map2.BU]=upsample_map(m,map.BU,upsample_factor);
[m2,map2.Qvar]=upsample_map(m,map.Qvar,upsample_factor);
[m2,map2.Uvar]=upsample_map(m,map.Uvar,upsample_factor);

% color range
ce=2.4;
cb=2.4/6.4;

% vector length scale factor
ve=0.6;
vb=0.6*6.4;

% E

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_map(m2,nan(size(map2.E)),'iau');
caxis([-ce,ce]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m2,nan(size(map2.EQ)),nan(size(map2.EU)),map2.Qvar,map2.Uvar,ve,[],3*(2*upsample_factor-1),1)
plot_map(m2,map2.E,'iau');
caxis([-ce,ce]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m2,map2.EQ,map2.EU,map2.Qvar,map2.Uvar,ve,[],3*(2*upsample_factor-1),1)
title('BICEP2 E-mode signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
set(fig,'Renderer','painters')
print -depsc2 e_over_e.eps
!epstopdf e_over_e.eps
system_safe(['convert -density 200 e_over_e.pdf e_over_e.png'])

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_map(m2,nan(size(map2.E)),'iau',[-30.1,30.1,-65.9,-49.1]);
caxis([-ce,ce]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m2,nan(size(map2.EQ)),nan(size(map2.EU)),map2.Qvar,map2.Uvar,ve,[],3*(2*upsample_factor-1),1,[-30.1,30.1,-65.9,-49.1])
plot_map(m2,map2.E,'iau',[-30.1,30.1,-65.9,-49.1]);
caxis([-ce,ce]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m2,map2.EQ,map2.EU,map2.Qvar,map2.Uvar,ve,[],3*(2*upsample_factor-1),0,[-30.1,30.1,-65.9,-49.1])
title('BICEP2 E-mode signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
set(fig,'Renderer','painters')
print -depsc2 e_over_e_rect.eps
!epstopdf e_over_e_rect.eps
system_safe(['convert -density 200 e_over_e_rect.pdf e_over_e_rect.png'])

% B

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_map(m2,nan(size(map2.B)),'iau');
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m2,nan(size(map2.BQ)),nan(size(map2.BU)),map2.Qvar,map2.Uvar,vb,[],3*(2*upsample_factor-1),1)
plot_map(m2,map2.B,'iau');
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m2,map2.BQ,map2.BU,map2.Qvar,map2.Uvar,vb,[],3*(2*upsample_factor-1),1)
title('BICEP2 B-mode signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
set(fig,'Renderer','painters')
print -depsc2 b_over_b.eps
!epstopdf b_over_b.eps
system_safe(['convert -density 200 b_over_b.pdf b_over_b.png'])

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_map(m2,nan(size(map2.B)),'iau',[-30.1,30.1,-65.9,-49.1]);
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m2,nan(size(map2.BQ)),nan(size(map2.BU)),map2.Qvar,map2.Uvar,vb,[],3*(2*upsample_factor-1),1,[-30.1,30.1,-65.9,-49.1])
plot_map(m2,map2.B,'iau',[-30.1,30.1,-65.9,-49.1]);
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m2,map2.BQ,map2.BU,map2.Qvar,map2.Uvar,vb,[],3*(2*upsample_factor-1),0,[-30.1,30.1,-65.9,-49.1])
title('BICEP2 B-mode signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
set(fig,'Renderer','painters')
print -depsc2 b_over_b_rect.eps
!epstopdf b_over_b_rect.eps
system_safe(['convert -density 200 b_over_b_rect.pdf b_over_b_rect.png'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function make_pol_over_t

load maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0 ac m
map=make_map(ac,m);
map=cal_coadd_maps(map,get_ukpervolt);

ellrng=[80,150];
mapp=make_tqu_apmap(m,map,[],ellrng);

% less aggressive filter for temperature
ellrng=[80,180];
mapt=make_tqu_apmap(m,map,[],ellrng);

win=1./mapp.Qvar; win=win./prctile(win(:),90);
pol_std=sqrt(mapp.Qvar+mapp.Uvar);
mapt.Tap(pol_std>2*nanmedian(pol_std(:)))=nan;

upsample_factor=2;
[m2,map2.Tap]=upsample_map(m,mapt.Tap,upsample_factor);
[m2,map2.win]=upsample_map(m,win,upsample_factor);
[m2,map2.Qap]=upsample_map(m,mapp.Qap,upsample_factor);
[m2,map2.Uap]=upsample_map(m,mapp.Uap,upsample_factor);
[m2,map2.Qvar]=upsample_map(m,mapp.Qvar,upsample_factor);
[m2,map2.Uvar]=upsample_map(m,mapp.Uvar,upsample_factor);

% vector length scale factor
vp=0.6;

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_map(m2,nan(size(map2.Tap)),'iau');
caxis([-150,150]);
colormap hot
freezeColors
hold on
plot_pol(m2,nan(size(map2.Qap)),nan(size(map2.Uap)),map2.Qvar,map2.Uvar,vp,1.4,3*(2*upsample_factor-1),1)
plot_map(m2,map2.Tap./map2.win,'iau');
caxis([-150,150]);
colormap hot
freezeColors
hold on
plot_pol(m2,map2.Qap./map2.win,map2.Uap./map2.win,map2.Qvar,map2.Uvar,vp,1.4,3*(2*upsample_factor-1),1)
title('BICEP2 temperature and polarization signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
set(fig,'Renderer','painters')
print -depsc2 pol_over_t.eps
!epstopdf pol_over_t.eps
system_safe(['convert -density 200 pol_over_t.pdf pol_over_t.png'])

% undo crop
mapt=make_tqu_apmap(m,map,[],ellrng);
[m2,map2.Tap]=upsample_map(m,mapt.Tap,upsample_factor);

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_map(m2,nan(size(map2.Tap)),'iau');
caxis([-150,150]);
colormap hot
freezeColors
hold on
plot_pol(m2,nan(size(map2.Qap)),nan(size(map2.Uap)),map2.Qvar,map2.Uvar,vp,[],3*(2*upsample_factor-1),1)
plot_map(m2,map2.Tap,'iau');
caxis([-150,150]);
colormap hot
freezeColors
hold on
plot_pol(m2,map2.Qap,map2.Uap,map2.Qvar,map2.Uvar,vp,[],3*(2*upsample_factor-1),1)
title('BICEP2 temperature and polarization signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% this works way better than direct print to pdf
set(fig,'Renderer','painters')
print -depsc2 pol_over_tap.eps
!epstopdf pol_over_tap.eps
system_safe(['convert -density 200 pol_over_tap.pdf pol_over_tap.png'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function make_skyproj

load maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0 ac m
map=make_map(ac,m);
map=cal_coadd_maps(map,get_ukpervolt);

[m2,map2.T]=upsample_map(m,map.T,2);

fig=figure;
clf;
setwinsize(gcf,800,600)

plot_map(m2,map2.T,'iau')
caxis([-150,150]);
colormap hot
freezeColors
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');
hold on

% code inspired by outdated fp_radec function
ra=0;
dec=-58;
dk=68;

[p,ind]=get_array_info;

p=rotarray(p,dk);

alpha=chi2alpha(ra,dec,p.r,p.theta,p.chi,p.chi_thetaref);
[decb,rab]=reckon(dec,ra,p.r,p.theta+90);

roff=p.fwhm_maj/(2*sqrt(2*log(2)));
xo=roff.*sin(alpha*pi/180)./cos(decb*pi/180);
yo=roff.*cos(alpha*pi/180);

xl=[rab-xo,rab+xo]';
yl=[decb-yo,decb+yo]';

plot(rab,decb,'ko')

% this works way better than direct print to pdf
print -depsc2 skyproj.eps
!epstopdf skyproj.eps
system_safe(['convert -density 200 skyproj.pdf skyproj.png'])

return



%%%%%%%%%%%%%%%

function plot_eb_leakage()

for nonoise=[0,1]

%get aps & suppression factors
if(nonoise)
x=load('aps/0751/xxx2_a_filtp3_weight3_gs_dp1100_jack0.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_in=squeeze(aps.Cs_l(:,4,:));
x=load('aps/0751/xxx2_a_filtp3_weight3_gs_dp1100_jack0_pureB.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_pureB_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_in_pureB=squeeze(aps.Cs_l(:,4,:));
x=load('aps/0751/xxx2_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_b=squeeze(aps.Cs_l(:,4,:));
else
  x=load('aps/0751/xxx7_a_filtp3_weight3_gs_dp1100_jack0.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_in=squeeze(aps.Cs_l(:,4,:));
x=load('aps/0751/xxx7_a_filtp3_weight3_gs_dp1100_jack0_pureB.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_pureB_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_in_pureB=squeeze(aps.Cs_l(:,4,:));
x=load('aps/0751/xxx7_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_b=squeeze(aps.Cs_l(:,4,:));
end

[r,im,ml]=get_b2xb2;

%subtract mean
matrixb=bsxfun(@minus, Cs_l_b, mean(Cs_l_b,2));
smithb=bsxfun(@minus, Cs_l_in_pureB, mean(Cs_l_in_pureB,2));
normalb=bsxfun(@minus, Cs_l_in, mean(Cs_l_in,2));

%find 95% highest


clf
setwinsize(gcf,500,350)

%BB
h1=plot(r.l(2:end), normalb(2:end,1:200), 'b', 'linewidth', 1);
hold on
h2=plot(r.l(2:end), smithb(2:end, 1:200), 'color', [.7,.5,0], 'linewidth', 1);
h3=plot(r.l(2:end), matrixb(2:end, 1:200), 'k', 'linewidth', 1);

%add a nominal BB spec
hinp=plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'--r', 'linewidth', 2);
hold off


for i=1:30;arr(i)=-1d-1+2d-2*i;fields{i}=num2str(arr(i), '%g');end
set(gca, 'YTick', arr, 'YTickLabel', fields);

xlabel('Multipole')
ylabel('l(l+1)C^{BB}_l/2\pi [\muK^2]')
xlim([30,330])
ylim([-4d-2,8d-2])

legend([hinp,h1(1),h2(1),h3(1)],{'lens+r=0.2','normal', 'Smith', 'matrix'}, 'location', 'northwest')

legend boxoff

if(nonoise)
print_fig('paper_plots/plot_eb_leakage3')
else
print_fig('paper_plots/plot_eb_leakage3_withnoise')
end

end

return

%%%%%%%%%%%%%%%

function plot_bb_ambloss()

%get suppression factor
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat');


%get aps
x=load('aps/0751/xxx4_a_filtp3_weight3_gs_dp1100_jack0.mat')
%aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
aps.Cs_l=x.aps.Cs_l(:,1:6,:);
Cs_l_in=squeeze(aps.Cs_l(:,4,:));
x=load('aps/0751/xxx4_a_filtp3_weight3_gs_dp1100_jack0_pureB.mat')
%aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
aps.Cs_l=x.aps.Cs_l(:,1:6,:);
Cs_l_in_pureB=squeeze(aps.Cs_l(:,4,:));
x=load('aps/0751/xxx4_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat')
%aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
aps.Cs_l=x.aps.Cs_l(:,1:6,:);
Cs_l_b=squeeze(aps.Cs_l(:,4,:));






clf
setwinsize(gcf,500,350)


%BB
h1=semilogy(r.l, Cs_l_in, 'b');
hold on
h2=semilogy(r.l, Cs_l_in_pureB, 'color', [.7, .5, 0]);
h3=semilogy(r.l, Cs_l_b, 'k');

%add a nominal BB spec
[r,im,ml]=get_b2xb2;
hinp=semilogy(im.l,im.Cs_l(:,4),'--r');
hold off


for i=1:8;arr(i)=1d-7*10^i;fields{i}=num2str(arr(i), '%g');end
set(gca, 'YTick', arr, 'YTickLabel', fields);

xlabel('Multipole')
ylabel('l(l+1)C^{BB}_l/2\pi [\muK^2]')
xlim([0,500])
ylim([2d-7,2d-2])

legend([hinp,h1(1),h2(1),h3(1)],{'Input: r=0.1','normal', 'Smith', 'matrix'}, 'location', 'southeast')

legend boxoff

print_fig('paper_plots/plot_bb_ambloss')

return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function purification_steps()



%signal only sim with E only, no lensing
map_raw='maps/0751/0012_a_filtp3_weight3_gs_dp1100_jack0.mat'
mapE='maps/0751/0012_a_filtp3_weight3_gs_dp1100_jack0_projE.mat'
mapB='maps/0751/0012_a_filtp3_weight3_gs_dp1100_jack0_projB.mat'
ellrng=[50,120];
lims(1).csE=[-2.4,2.4];lims(1).csB=[-2.4/6.4,2.4/6.4];
lims(2).csE=[-2.4,2.4];lims(2).csB=[-1d-2,1d-2];
lims(1).vsE=1/1.7;lims(1).vsB=1/.3;
lims(2).vsE=1/1.7;lims(2).vsB=50;
setup_maps(map_raw, mapE, mapB, lims,ellrng,'paper_plots/purification_steps_Emodes_filtered')


close all
%the real B2
map_raw='maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0.mat'
mapE='maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0_projE.mat'
mapB='maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0_projB.mat'  
ellrng=[50,120];
clear lims;
lims(1).csE=[-2.4,2.4];lims(1).csB=[-2.4/6.4,2.4/6.4];
lims(1).vsE=1/1.7;lims(1).vsB=1/.3;
setup_maps(map_raw, mapE, mapB, lims,ellrng,'paper_plots/purification_steps_B2_filtered')
  
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function setup_maps(map_raw, mapE, mapB, lims, ellrng, outname)

if ~exist('ellrng','var') || isempty(ellrng)
  ellrng=[0,400];
end


load(map_raw)
map=make_map(ac, m, coaddopt);
clear coaddopt;
map_raw=cal_coadd_maps(map, get_ukpervolt);
load(mapE, 'map')
mapE=cal_coadd_maps(map, get_ukpervolt);
load(mapB, 'map')
mapB=cal_coadd_maps(map, get_ukpervolt);


%find E and B maps
map_raw=make_ebmap(m,map_raw,[],ellrng);
map_kendrick=make_ebmap(m,map_raw,'kendrick', ellrng);
map_pure=make_ebmap(m,mapE,[],ellrng);
mapB=make_ebmap(m,mapB,[],ellrng);

%substitute Bpurified map into pure E map...this is confusing ...
%otherwise because there leakage from E->B in makeing B map
map_pure.B=mapB.B;
map_pure.BQ=mapB.BQ;
map_pure.BU=mapB.BU;


plot_twopanel(map_raw,m, lims(1))
print_fig([outname '_normal_map_EB'])

plot_twopanel(map_kendrick,m, lims(1))
print_fig([outname '_smith_map_EB'])

plot_twopanel(map_pure,m, lims(1))
print_fig([outname '_matrix_map_EB'])

if length(lims)>1
plot_twopanel(map_pure,m, lims(2))
print_fig([outname '_matrix_map_EB_zoom'])
end



return

%%%%%%%%%%%%%

function plot_twopanel(map,m,lims)


%font size
fs=10;

figure
clf
setwinsize(gcf, 480,370)


colormap(colormap_lint)
titletex={'E', 'B'};


dim=get_twopaneldims(m);
set(gcf, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,map.E,'iau');
set(ax1,'FontSize',fs,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
set(ax1,'XTick',[-50 0 50]);
caxis(lims.csE)
hold on
plot_pol(m,map.EQ,map.EU,map.Qvar,map.Uvar,lims.vsE,[],3,1)
hold off
title(titletex{1});
freezeColors;

ax2=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,map.B,'iau');
set(ax2,'FontSize',fs,'FontWeight','normal');
set(ax2,'XTick',[-50 0 50]);
title(titletex{2});
caxis(lims.csB)
hold on
plot_pol(m,map.BQ,map.BU,map.Qvar,map.Uvar,lims.vsB,[],3,1)
hold off
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');
freezeColors;

% colorbars
cas=lims.csE;
ax3 = axes('Position',[dim.x2,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax3,'FontSize',fs,'FontWeight','normal');
imagesc(1:10,linspace(cas(1),cas(2),1000),repmat(linspace(cas(1),cas(2),1000)',1,10));
freezeColors;
set(ax3,'Xtick',[]);
marks=[cas(1)+(cas(2)-cas(1))./8,cas(2)-(cas(2)-cas(1))./8];
set(ax3,'YDir','normal','YAxisLocation','right','YTick',[marks(1),0,marks(2)]);
set(ax3,'YtickLabel', {num2str(marks(1), '%.1g'), '0',num2str(marks(2), '%.1g')});

cas=lims.csB;
ax4 = axes('Position',[dim.x2,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax4,'FontSize',fs,'FontWeight','normal');
imagesc(1:10,linspace(cas(1),cas(2),1000),repmat(linspace(cas(1),cas(2),1000)',1,10));
freezeColors;
set(ax4,'Xtick',[]);
marks=[cas(1)+(cas(2)-cas(1))./8,cas(2)-(cas(2)-cas(1))./8];
set(ax4,'YDir','normal','YAxisLocation','right','YTick',[marks(1),0,marks(2)]);
set(ax4,'YtickLabel', {num2str(marks(1), '%.1g'), '0',num2str(marks(2), '%.1g')});

% colorbar units

ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', fs, 'FontWeight', 'normal');
text(-0.1, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', fs, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(-0.1, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', fs, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

return


%%%%%%%%%%%%%%%%%%%%%
function make_b2_vs_world
set(0,'defaulttextfontsize',12);
% get real data points
[r,im,ml]=get_b2xb2;

showb2=0;

clf;
setwinsize(gcf,400,300)

d1 = load('aux_data/official_cl/camb_planck2013_r0p1_ext_lensedcls.dat')
d2 = load('aux_data/official_cl/camb_planck2013_r0p1_ext_tenscls.dat')

lensing_curve=d1(1:2899,4);
loglog(d1(1:2899,1),lensing_curve,'r-');
hold on


if ~showb2
  rss = [0.01,0.1,1]
else
  rss = 0.2
end

for rs=rss
  % CAMB lensing curve.
  tensor_curve=d2(1:2899,4)*rs/0.1;
  % add lensing+tensors, making sure they are evaluated at the same ell
  total_curve=lensing_curve+tensor_curve;
  loglog(d2(1:2899,1),tensor_curve,'r--');
  if showb2
    loglog(d2(1:2899,1),total_curve,'r--');
  end
end
  
% Legend location
%  text_x=[13,20];
%  text_y=logspace(log10(5e0),log10(5e1),5);
text_y = linspace(0.75,0.97,5)
text_x = linspace(0.05,0.25,2)

if showb2
  h=errorbar2(r.lc(2:10,4),r.real(2:10,4),r.derr(2:10,4),'k.');   
  h=herrorbar2(r.lc(2:10,4),r.real(2:10,4),r.lc(2:10,4)-r.ll(2:10,4),r.lh(2:10,4)-r.lc(2:10,4),'k.');
  h=plot(r.lc(2:10,4),r.real(2:10,4),'k.','MarkerSize',12);
  text(text_x(1),text_y(1),'BICEP2','Color','k','Units','normalized')
end

ms=4;
other_data=load('other_experiments/bicep1.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(1),'BICEP1','Color',[0.5,0.5,0.5],'Units','normalized')
other_data=load('other_experiments/boomerang.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(3),'Boomerang','Color',[1,0,1],'Units','normalized')

other_data=load('other_experiments/capmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(5),'CAPMAP','Color',[0.5,0,0.5],'Units','normalized')
other_data=load('other_experiments/cbi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,0]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(4),'CBI','Color',[0,1,0],'Units','normalized')

other_data=load('other_experiments/dasi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(2),'DASI','Color',[0,1,1],'Units','normalized')
other_data=load('other_experiments/quad.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,0]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(2),'QUAD','Color',[1,0,0],'Units','normalized')
other_data=load('other_experiments/quiet40.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,1]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(3),'QUIET-Q','Color',[0.5,0.5,1],'Units','normalized')
other_data=load('other_experiments/quiet90.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(4),'QUIET-W','Color',[0,0,1],'Units','normalized')
other_data=load('other_experiments/wmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,1/3,0]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(5),'WMAP','Color',[1,1/3,0],'Units','normalized')

%  % polarbear measurment of lensing
other_data=load('other_experiments/polarbear.txt');
ul=3;
meas=[1,2,4];
halfbw = (other_data(2,1)-other_data(1,1))/2;
halfbw = repmat(halfbw,4,1);
h=errorbar2(other_data(meas,1),other_data(meas,2),other_data(meas,3),'bs');
set(h(2),'MarkerSize',ms);
h=herrorbar2(other_data(meas,1),other_data(meas,2),halfbw(meas),halfbw(meas),'bs');
set(h(2),'MarkerSize',ms);

h=plot(other_data(meas,1),other_data(meas,2),'bs','MarkerSize',ms,'MarkerFaceColor','b');
h=herrorbar2(other_data(ul,1),other_data(ul,4),halfbw(ul),halfbw(ul),'bv');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');

% SPT lensing in cross correlation with the CIB
other_data=load('other_experiments/SptBBX.txt');
p = other_data(:,2)/1e4.*(other_data(:,1)+1)/2/pi
pu= other_data(:,3)/1e4.*(other_data(:,1)+1)/2/pi
pl= other_data(:,4)/1e4.*(other_data(:,1)+1)/2/pi

le = p-pl;
ue = pu-p;
le(p-le<0)=p(p-le<0)-1e-10;
h=errorbar2(other_data(:,1),p,le,ue,'^')
set(h(2),'MarkerSize',0.01);
set(h(1),'Color',[0,0.7,0]);
plot(other_data(:,1),p,'^','color',[0,0.7,0],'MarkerFaceColor',[0,0.7,0],'MarkerSize',ms)
plot(other_data(:,1),pl,'^','color',[0,0.7,0],'MarkerFaceColor',[0,0.7,0],'MarkerSize',ms/2)
plot(other_data(:,1),pu,'^','color',[0,0.7,0],'MarkerFaceColor',[0,0.7,0],'MarkerSize',ms/2)

ylim([1e-4,10e1]);
if showb2
  ylim([1e-3,5e1])
end
xlabel('Multipole')
ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')


hold off
%  text(2e1,3e-3,'r=0.2','Rotation',32)
%  text(6e1,2e-3,'lensing','Rotation',35)
set(gca,'ytick',[1e-4,1e-3,1e-2,1e-1,1e0,1e1,1e2]);
%  set(gca,'xscale','lin')
%  xlim([10,500]); 
xlim([10,3000]);
%  keyboard
print -depsc2 paper_plots/previous_limits.eps
fix_lines('paper_plots/previous_limits.eps')
!epstopdf paper_plots/previous_limits.eps
system_safe(['convert -density 300 paper_plots/previous_limits.pdf paper_plots/previous_limits.png'])

return



function make_EEvsSims

  
  % get the standard b2xb2 points
  [rb2,im,ml]=get_b2xb2;
  
  % get b2xb1
  load final/0751x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix.mat

  
  for cc=1:2
    clf; setwinsize(gcf,400,200)
    line([0,330],[0,0],'LineStyle',':','color','k'); box on
    hold on
    pl = plot(rb2(1).lc(:,3),squeeze(rb2(1).sim(:,3,1)));
    
    plot(ml.l,ml.Cs_l(:,3),'r');
    h1=errorbar2(rb2(1).lc(:,3),rb2(1).real(:,3),std(rb2(1).sim(:,3,:),[],3),'k.');
    set(h1(2),'MarkerSize',12);    

    hold off
    xlim([0,200]); ylim([-.1,1.3]);
    ptitle('EE');
    legend([h1(2),pl(1)],{'B2xB2',['Sim 1']},'Location','west');
    legend boxoff
    ylabel('l(l+1)C_l^{EE}/2\pi [\muK^2]')
    xlabel('Multipole')

    print('-depsc2',['paper_plots/b2xb1100vsSim_1.eps'])
    fix_lines(['paper_plots/b2xb1100vsSim_1.eps'])
    system_safe(['epstopdf paper_plots/b2xb1100vsSim_1.eps'])
    system_safe(['convert -density 200 paper_plots/b2xb1100vsSim_1.pdf paper_plots/b2xb1100vsSim_1.png'])
  end  
   
  clf; setwinsize(gcf,400,200)
  
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  pl = plot(rb2(1).lc(:,3),squeeze(rb2(1).sim(:,3,1:50)));
  
  plot(ml.l,ml.Cs_l(:,3),'r');
  h1=errorbar2(rb2(1).lc(:,3),rb2(1).real(:,3),std(rb2(1).sim(:,3,:),[],3),'k.');
  set(h1(2),'MarkerSize',12);    

  hold off
  xlim([0,200]); ylim([-.1,1.3]);
  ptitle('EE');
  legend([h1(2),pl(1)],{'B2xB2',['Sims 1-50']},'Location','west');
  legend boxoff
  ylabel('l(l+1)C_l^{EE}/2\pi [\muK^2]')
  xlabel('Multipole')

  print('-depsc2',['paper_plots/b2xb1100vsSim_50.eps'])
  fix_lines(['paper_plots/b2xb1100vsSim_50.eps'])
  system_safe(['epstopdf paper_plots/b2xb1100vsSim_50.eps'])
  system_safe(['convert -density 200 paper_plots/b2xb1100vsSim_50.pdf paper_plots/b2xb1100vsSim_50.png'])

  clf; setwinsize(gcf,400,200)
  
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  pl = plot(rb2(1).lc(:,3),squeeze(rb2(1).sim(:,3,1:499)));
  
  plot(ml.l,ml.Cs_l(:,3),'r');
  h1=errorbar2(rb2(1).lc(:,3),rb2(1).real(:,3),std(rb2(1).sim(:,3,:),[],3),'k.');
  set(h1(2),'MarkerSize',12);    

  hold off
  xlim([0,200]); ylim([-.1,1.3]);
  ptitle('EE');
  legend([h1(2),pl(1)],{'B2xB2',['Sims 1-500']},'Location','west');
  legend boxoff
  ylabel('l(l+1)C_l^{EE}/2\pi [\muK^2]')
  xlabel('Multipole')

  print('-depsc2',['paper_plots/b2xb1100vsSim_all.eps'])
  fix_lines(['paper_plots/b2xb1100vsSim_all.eps'])
  system_safe(['epstopdf paper_plots/b2xb1100vsSim_all.eps'])
  system_safe(['convert -density 200 paper_plots/b2xb1100vsSim_all.pdf paper_plots/b2xb1100vsSim_all.png'])

  
%    for sn=[1,1:100]
%      clf; setwinsize(gcf,400,200)
%      
%      line([0,330],[0,0],'LineStyle',':','color','k'); box on
%      hold on
%      plot(ml.l,ml.Cs_l(:,3),'r');
%      h1=errorbar2(rb2(1).lc(:,3),rb2(1).real(:,3),std(rb2(1).sim(:,3,:),[],3),'k.');
%      set(h1(2),'MarkerSize',12);
%      
%      pl = plot(rb2(1).lc(:,3),rb2(1).sim(:,3,sn),'b');
%      
%      hold off
%      xlim([0,200]); ylim([-.1,1.3]);
%      ptitle('EE');
%      legend([h1(2),pl],{'B2xB2',['Sim ',num2str(sn)]},'Location','west');
%      legend boxoff
%      ylabel('l(l+1)C_l^{EE}/2\pi [\muK^2]')
%      xlabel('Multipole')
%  
%      print('-depsc2',['paper_plots/b2xb1100vsSim_',num2str(100+sn),'.eps'])
%      fix_lines(['paper_plots/b2xb1100vsSim_',num2str(100+sn),'.eps'])
%      system_safe(['epstopdf paper_plots/b2xb1100vsSim_',num2str(100+sn),'.eps'])
%      system_safe(['convert -density 200 paper_plots/b2xb1100vsSim_',num2str(100+sn),'.pdf paper_plots/b2xb1100vsSim_',num2str(100+sn),'.png'])
%    end
  
return

function make_rconstraint_cont()

%%%%%%%%%%%%%%%%%%%%
% first panel the bandpowers

% get the real points
[r,im,ml,dum,rf]=get_b2xb2;

clf; setwinsize(gcf,1000,800);

subplot_grid(2,3,1)

line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
plot(im.l,ml.Cs_l(:,4),'r');
% plot B2xB2
h1=errorbar2(r(1).lc(:,4),r(1).real(:,4),std(r(1).simr(:,4,:),[],3),'k.');
set(h1(2),'MarkerSize',12);
hold off
xlim([0,200]); ylim([-.005,.03]);
%ptitle('BB');
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
axis square

cest_sim = squeeze(r(1).simr(:,4,:));
% evaluate the bandpower distributions terms of chi2 dists:
se = sqrt( var(cest_sim') ./ (mean(cest_sim')+r.db(:,4)' )/2 );
ke = var(cest_sim')/2./se.^4;
db = -r.db(:,4)';

fitfun = @(p,x) chi2pdf((x-p(:,3))./p(:,2).^2,p(:,1))./p(:,2).^2.*p(:,4)
fitcdf = @(p,x) chi2cdf((x-p(:,3))./p(:,2).^2,p(:,1))./p(:,2).^2.*p(:,4)
fitcdfinv = @(p,x) chi2inv(x,p(:,1)).*p(:,2).^2+p(:,3);

nbins = 30;
nsig = 5;
xlo = mean(cest_sim')-nsig*std(cest_sim');
xhi = mean(cest_sim')+nsig*std(cest_sim');
dlo = zeros(17,1);
dhi = zeros(17,1);
l68 = zeros(17,1);
h68 = zeros(17,1);
p16 = nan(17,1);
p84 = nan(17,1);
p98 = nan(17,1);
p50 = nan(17,1);
p2 = nan(17,1);
mll = nan(17,1);

for ii=1:16
  bin=ii+1;
  
  [x,n]=hfill(cest_sim(bin,:),nbins,xlo(bin),xhi(bin));
  % for the chisquare to match entries in a histogram
  % apply a norm: number of entries/bin density:
  bd = length(x)/(xhi(bin)-xlo(bin));
  pc0=[ke(bin),se(bin),db(bin),1];
  pc0(end)=length(cest_sim')/bd;

  xx=linspace(xlo(bin),xhi(bin),1000);
  [cont68,lo68,hi68]=find_68pnts(fitfun(pc0,xx),xx);

  minfun = @(p) abs(fitcdf(pc0,p)/fitcdf(pc0,100)-normcdf(-1,0,1))
  p16(bin) = fminsearch(minfun,0,optimset('MaxIter',1000))
  minfun = @(p) abs(fitcdf(pc0,p)/fitcdf(pc0,100)-normcdf(1,0,1))
  p84(bin) = fminsearch(minfun,0,optimset('MaxIter',1000))
  
  minfun = @(p) abs(fitcdf(pc0,p)/fitcdf(pc0,100)-normcdf(-2,0,1))
  p2(bin) = fminsearch(minfun,0,optimset('MaxIter',1000))
  minfun = @(p) abs(fitcdf(pc0,p)/fitcdf(pc0,100)-normcdf(2,0,1))
  p98(bin) = fminsearch(minfun,0,optimset('MaxIter',1000))
  
  minfun = @(p) abs(fitcdf(pc0,p)/fitcdf(pc0,100)-0.5)
  p50(bin) = fminsearch(minfun,0,optimset('MaxIter',1000))
  
  if ii<6
    subplot_grid(2,3,1+ii)
    hplot(x,n)
    xlim([xlo(bin),xhi(bin)])
    hold
    
    plot(xx,fitfun(pc0,xx),'r--') 
    plot([r(1).real(bin,4),r(1).real(bin,4)],[0,max(n)],'k')
    plot([lo68 lo68],[0 fitfun(pc0,hi68)],'g-');
    plot([hi68 hi68],[0 fitfun(pc0,hi68)],'g-');
    
    plot([p16(bin) p16(bin)],[0 fitfun(pc0,p16(bin))],'m-');
    plot([p84(bin) p84(bin)],[0 fitfun(pc0,p84(bin))],'m-');
  
    title(['bandpower distribution bin ',num2str(bin)]);
    xlabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
    ylabel('entries');
    axis square
  end
  
  [dum,ind] = max(fitfun(pc0,xx));
  mll(bin) = xx(ind)
  dhi(bin)=hi68-xx(ind);
  dlo(bin)=xx(ind)-lo68;
  l68(bin)=lo68;
  h68(bin)=hi68;
   
end

meansim= mean(cest_sim,2)

figure

clf; setwinsize(gcf,600,500);
seb1 = shadedErrorBar(r(1).lc(:,4),meansim,[p98-meansim,meansim-p2]',{'color','b', 'Visible', 'off'})
set(seb1.edge,'Color',get(seb1.patch,'FaceColor'))
hold
seb = shadedErrorBar(r(1).lc(:,4),meansim,[p84-meansim,meansim-p16]',{'color','b', 'Visible', 'off'})
set(seb.patch,'FaceColor',[0.7,0.7,1])
set(seb.edge,'Color',get(seb.patch,'FaceColor'))

pll = plot(im.l,ml.Cs_l(:,4),'r-');
%  plm = plot(im.l,im.Cs_l(:,4)*2,'r--');
plm = plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
%  pl50 = plot(r(1).lc(:,4),p50,'-','Color',[0.5,0.5,1]);

pl = plot(r(1).lc(:,4),r(1).real(:,4),'k.','MarkerSize',16);
plot(r(1).lc(7:end,4),r(1).real(7:end,4),'.','MarkerSize',16,'color',[0.4,0.4,0.4]);

xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
xlim([0,330]); ylim([-.01,.055]);
axis square
set(gca,'Layer','top')
line([0,330],[0,0],'LineStyle',':','color','k');
box on
legend([pl,pll,plm,seb.patch,seb1.patch],'B2 data','lensed LCDM','lensed LCDM + best fit r','sims: 16-84% interval','...   2-98% interval','Location','NorthWest')
legend boxoff
% print
keyboard
print -depsc2 paper_plots/rconstraint_band.eps
fix_lines('paper_plots/rconstraint_band.eps')
!epstopdf paper_plots/rconstraint_band.eps
system_safe(['convert -density 300 paper_plots/rconstraint_band.pdf paper_plots/rconstraint_band.png'])

return


function make_EE_cont()

%%%%%%%%%%%%%%%%%%%%
% first panel the bandpowers

% get the real points
[r,im,ml,dum,rf]=get_b2xb2;

clf; setwinsize(gcf,1000,800);

subplot_grid(2,3,1)

line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,ml.Cs_l(:,3),'r');
% plot B2xB2
h1=errorbar2(r(1).lc(:,3),r(1).real(:,3),std(r(1).simr(:,3,:),[],3),'k.');
set(h1(2),'MarkerSize',12);
hold off
xlim([0,200]);
%ptitle('BB');
xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
axis square

cest_sim = squeeze(r(1).simr(:,3,:));
% evaluate the bandpower distributions terms of chi2 dists:
se = sqrt( var(cest_sim') ./ (mean(cest_sim')+r.db(:,3)' )/2 );
ke = var(cest_sim')/2./se.^4;
db = -r.db(:,3)';

fitfun = @(p,x) chi2pdf((x-p(:,3))./p(:,2).^2,p(:,1))./p(:,2).^2.*p(:,4)
fitcdf = @(p,x) chi2cdf((x-p(:,3))./p(:,2).^2,p(:,1))./p(:,2).^2.*p(:,4)
fitcdfinv = @(p,x) chi2inv(x,p(:,1)).*p(:,2).^2+p(:,3);

nbins = 30;
nsig = 5;
xlo = mean(cest_sim')-nsig*std(cest_sim');
xhi = mean(cest_sim')+nsig*std(cest_sim');
dlo = zeros(17,1);
dhi = zeros(17,1);
l68 = zeros(17,1);
h68 = zeros(17,1);
p16 = nan(17,1);
p84 = nan(17,1);
p98 = nan(17,1);
p50 = nan(17,1);
p2 = nan(17,1);
mll = nan(17,1);

for ii=1:16
  bin=ii+1;
  
  [x,n]=hfill(cest_sim(bin,:),nbins,xlo(bin),xhi(bin));
  % for the chisquare to match entries in a histogram
  % apply a norm: number of entries/bin density:
  bd = length(x)/(xhi(bin)-xlo(bin));
  pc0=[ke(bin),se(bin),db(bin),1];
  pc0(end)=length(cest_sim')/bd;

  xx=linspace(xlo(bin),xhi(bin),1000);
%    [cont68,lo68,hi68]=find_68pnts(fitfun(pc0,xx),xx);

  minfun = @(p) abs(fitcdf(pc0,p)/fitcdf(pc0,100)-normcdf(-1,0,1))
  p16(bin) = fminsearch(minfun,(xhi(bin)-xlo(bin))/2,optimset('MaxIter',1000))
  
  minfun = @(p) abs(fitcdf(pc0,p)/fitcdf(pc0,100)-normcdf(1,0,1))
  p84(bin) = fminsearch(minfun,(xhi(bin)-xlo(bin))/2,optimset('MaxIter',1000))
  
  minfun = @(p) abs(fitcdf(pc0,p)/fitcdf(pc0,100)-normcdf(-2,0,1))
  p2(bin) = fminsearch(minfun,(xhi(bin)-xlo(bin))/2,optimset('MaxIter',1000))
  minfun = @(p) abs(fitcdf(pc0,p)/fitcdf(pc0,100)-normcdf(2,0,1))
  p98(bin) = fminsearch(minfun,(xhi(bin)-xlo(bin))/2,optimset('MaxIter',1000))
  
  minfun = @(p) abs(fitcdf(pc0,p)/fitcdf(pc0,100)-0.5)
  p50(bin) = fminsearch(minfun,(xhi(bin)-xlo(bin))/2,optimset('MaxIter',1000))
  
  if ii<6
    subplot_grid(2,3,1+ii)
    hplot(x,n)
    xlim([xlo(bin),xhi(bin)])
    hold
    
    plot(xx,fitfun(pc0,xx),'r--') 
    plot([r(1).real(bin,3),r(1).real(bin,3)],[0,max(n)],'k')
%      plot([lo68 lo68],[0 fitfun(pc0,hi68)],'g-');
%      plot([hi68 hi68],[0 fitfun(pc0,hi68)],'g-');
    
    plot([p16(bin) p16(bin)],[0 fitfun(pc0,p16(bin))],'m-');
    plot([p84(bin) p84(bin)],[0 fitfun(pc0,p84(bin))],'m-');
  
    title(['bandpower distribution bin ',num2str(bin)]);
    xlabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
    ylabel('entries');
    axis square
  end
%    keyboard
  
  [dum,ind] = max(fitfun(pc0,xx));
  mll(bin) = xx(ind)
%    dhi(bin)=hi68-xx(ind);
%    dlo(bin)=xx(ind)-lo68;
%    l68(bin)=lo68;
%    h68(bin)=hi68;
   
end

meansim= mean(cest_sim,2)

figure

clf; setwinsize(gcf,600,500);
seb1 = shadedErrorBar(r(1).lc(:,3),meansim,[p98-meansim,meansim-p2]',{'color','b', 'Visible', 'off'})
set(seb1.edge,'Color',get(seb1.patch,'FaceColor'))
hold
seb = shadedErrorBar(r(1).lc(:,3),meansim,[p84-meansim,meansim-p16]',{'color','b', 'Visible', 'off'})
set(seb.patch,'FaceColor',[0.7,0.7,1])
set(seb.edge,'Color',get(seb.patch,'FaceColor'))

pll = plot(im.l,ml.Cs_l(:,3),'r-');
%  plm = plot(im.l,im.Cs_l(:,3)*2,'r--');
%  plm = plot(im.l,im.Cs_l(:,3)*2+ml.Cs_l(:,3),'r--');
%  pl50 = plot(r(1).lc(:,3),p50,'-','Color',[0.5,0.5,1]);

pl = plot(r(1).lc(:,3),r(1).real(:,3),'k.','MarkerSize',16);

xlabel('Multipole'); ylabel('l(l+1)C_l^{EE}/2\pi [\muK^2]')
xlim([0,200]); ylim([-.1,1.3]);
axis square
set(gca,'Layer','top')
line([0,330],[0,0],'LineStyle',':','color','k');
box on
legend([pl,pll,seb.patch,seb1.patch],'B2 data','LCDM','sims: 16-84% interval','...   2-98% interval','Location','NorthWest')
legend boxoff

% print
print -depsc2 paper_plots/EE_band.eps
fix_lines('paper_plots/EE_band.eps')
!epstopdf paper_plots/EE_band.eps
system_safe(['convert -density 300 paper_plots/EE_band.pdf paper_plots/EE_band.png'])

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ripped from reduc_final_chi2
function plot_devs_sub(r,i)
  % sort the sim bandpowers lowest to highest
  devs=sort(r.sdevs,3);
  ns=size(r.sdevs,3);
  % the levels in sorted bandpowers closest to -2,1,0,1,2 sigma
  ncdf = normcdf([-2:2],0,1);
  t=[floor(ns*ncdf(ncdf<0.5)),ceil(ns*ncdf(ncdf>=0.5))];

  plot(r.lc(:,i),squeeze(squeeze(r.sdevs(:,i,:))),'color',[0.7,0.7,0.7],'LineWidth',0.5);

  plot(r.lc(:,i),squeeze(devs(:,i,t(3))),'b','LineWidth',1);
  plot(r.lc(:,i),squeeze(devs(:,i,t([2,4]))),'r','LineWidth',1);
  plot(r.lc(:,i),squeeze(devs(:,i,t([1,5]))),'g','LineWidth',1);
  plot(r.lc(r.chibins{i},i),r.rdevs(r.chibins{i},i),'k.','MarkerSize',16);

return

function make_bpdevs_panel()
  
  for i=1:6
    chibins{i}=[2:10]; % use all plotted bins
  end

  % For TT we use legancy 1450 points... this is a blatant kludge but
  % who cares about TT?
  load final/1450x1350/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_overrx
  r=r(1);
  r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN; r=calc_chi(r,chibins,[],1);
  rt=r;

  % get the real points
  [r,im,ml,dum,rf]=get_b2xb2;
  r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN; r=calc_chi(r,chibins,[],1);
  
  % TB binning = TE binning
  r.lc(:,5)=r.lc(:,2);
  
  r=calc_devs(r,chibins);
  rt=calc_devs(rt,chibins);
  
  clf; setwinsize(gcf,1000,600)

  subplot(2,3,1); a1=gca;
  % plot the least important things first
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  plot_devs_sub(rt,1)
  hold off
  xlim([0,330]); ylim([-7,7]);
  ptitle(sprintf('TT'));
  ylabel('Bandpower deviation')

  subplot(2,3,2); a1=gca;
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  plot_devs_sub(r,2)
  hold off
  xlim([0,330]); ylim([-7,7]);
  ptitle(sprintf('TE'));

  subplot(2,3,3); a1=gca;
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  plot_devs_sub(r,3)
  hold off
  xlim([0,330]); ylim([-7,7]);
  ptitle(sprintf('EE'));

  subplot(2,3,4); a1=gca;
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  plot_devs_sub(r,4)
  hold off
  xlim([0,330]); ylim([-7,7]);
  ptitle(sprintf('BB'));

  subplot(2,3,5); a1=gca;
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  plot_devs_sub(r,5)
  hold off
  xlim([0,330]); ylim([-7,7]);
  ptitle(sprintf('TB'));
  
  subplot(2,3,6); a1=gca;
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  plot_devs_sub(r,6)
  hold off
  xlim([0,330]); ylim([-7,7]);
  ptitle(sprintf('EB'));
  
  xlabel('Multipole')
  keyboard

  print -depsc2 paper_plots/bpdevs.eps
  fix_lines('paper_plots/bpdevs.eps')
  !epstopdf paper_plots/bpdevs.eps
  system_safe(['convert -density 300 paper_plots/bpdevs.pdf paper_plots/bpdevs.png'])

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function plot_bandpowerdev()


jacks={'0','1','2', '3', '4', '5','6', '7', '8','9','a','b','c','d','e'}
jacknames={'none', 'dk', 'scan dir', '1st/2nd', 'tile', 'az', 'mux col','alt dk','mux row', 'tile/dk', 'FP inner/outer', 'tile top/bottom', 'tile inner/outer','moon up/down', 'best/worst'}

specs={3,4,6}

close all
for s=1:length(specs)
  fig(s)=figure;
  setwinsize(gcf, 1500,600)
end

for i=1:length(jacks)
  jacktype=jacks{i};
  jackname=jacknames{i}
  
  if strcmp(jacktype, '0')
    final_name=['final/0751/real_a_filtp3_weight3_gs_dp1102_jack' jacktype '_matrix_directbpwf.mat'];
  else
    final_name=['final/0751/real_a_filtp3_weight3_gs_dp1102_jack' jacktype '_pureB_directbpwf.mat'];
  end

  load(final_name);
  
  % set expectation value to mean of s+n sims
  for j=1:length(r)
    r(j).expv=mean(r(j).sim,3);
  end
  
  %lmax to plot
  lmax=325;
  
  % get standard set of chibins
  rfcopt.chibins=get_chibins();

  % calc cov and derr
  r=get_bpcov(r);

  % Plot devs versus null model
  r=calc_devs(r,rfcopt.chibins);

  for s=1:length(specs)
  set(0,'CurrentFigure',fig(s));
  subplot_grid(3,5,i);
  plot_devs(specs{s},r,jacktype,jackname,lmax)
  if i==11
    xlabel('Multipole')
  end
  subplot_grid2(3,5,i);	
  end

  
end
for s=1:length(specs)
  switch specs{s}
    case 3
      print_fig('paper_plots/bandpower_dev_EE',fig(s))
    case 4
      print_fig('paper_plots/bandpower_dev_BB',fig(s))
    case 6
      print_fig('paper_plots/bandpower_dev_EB',fig(s))
  end
end


return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_devs(spec,r,jacktype,jackname,lmax)

% sort the sim bandpowers lowest to highest
devs=sort(r.sdevs,3);
ns=size(r.sdevs,3);
% the levels in sorted bandpowers closest to -2,1,0,1,2 sigma
ncdf = normcdf([-2:2],0,1);
t=[floor(ns*ncdf(ncdf<0.5)),ceil(ns*ncdf(ncdf>=0.5))];


i=spec;
  
plot([0,1000], [0,0], ':k','LineWidth',1.5)

hold on;
plot(r.l(2:end),squeeze(squeeze(r.sdevs(2:end,i,:))),'color',[0.7,0.7,0.7],'LineWidth',0.5);
plot(r.l(2:end),squeeze(devs(2:end,i,t(3))),'b','LineWidth',1);
plot(r.l(2:end),squeeze(devs(2:end,i,t([2,4]))),'r','LineWidth',1);
plot(r.l(2:end),squeeze(devs(2:end,i,t([1,5]))),'g','LineWidth',1);
plot(r.l(2:end),r.rdevs(2:end,i),'ko-','MarkerFaceColor','k','LineWidth',2);
hold off

set(gca, 'ytick', [-5,0,5])
set(gca, 'yticklabel', [-5,0,5])
set(gca, 'xtick', [0,100,200,300])
set(gca, 'xticklabel', [0,100,200,300])

xlim([0,lmax]);
ylim([-9,9])


ylabel(jackname)
  
return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_beammapsims

% jack0 -> 0
% FP inner/outer -> 'a'
% tile top/bottom -> 'b'
% tile inner/outer -> 'c'

% Let's just plot... FP/inner outer BB

% Load beammap sim spectrum
dpp={'1100','1101'};

clear aps
clear aps0
clear r
clear rm

jt='a';

for k=1:numel(dpp)
  
  dp=dpp{k}; 
  
  % Store aps and sigma from jacks
  x=load(sprintf('/n/bicepfs1/bicep2/pipeline/aps/2244/0002_a_filtp3_weight3_gs_dp%s_jack%s_pureB.mat',dp,jt));
  y=load(sprintf('/n/home06/csheehy/bicep2_analysis/bias_for_chinlin/beammap_bias_and_sigma_cutmeanwithrg_dp%s_jack%s',dp,jt));
  x.aps.sigma=y.sigma;
  x.aps.bias=y.bias;
  aps(k)=x.aps;
  
  x=load(sprintf('final/1450x1350/real_a_filtp3_weight3_gs_dp%s_jack%s_real_a_filtp3_weight3_gs_dp%s_jack%s1_pureB_overrx.mat',dp,jt,dp,jt));
  r(k)=x.r(1);
  
  % Apply supfac
  aps(k).Cs_l=aps(k).Cs_l.*r(k).rwf;
  
  % Remove bias and store uncertainty
  aps(k).Cs_l=aps(k).Cs_l-y.bias;
    
end

% get jack0 sims
jt='0';

for k=1:numel(dpp)
  
  dp=dpp{k}; 
  
  % Store aps and sigma from jacks
  x=load(sprintf('/n/bicepfs1/bicep2/pipeline/aps/2244/0002_a_filtp3_weight3_gs_dp%s_jack%s_pureB.mat',dp,jt));
  y=load(sprintf('/n/home06/csheehy/bicep2_analysis/bias_for_chinlin/beammap_bias_and_sigma_cutmeanwithrg_dp%s_jack%s',dp,jt));
  x.aps.sigma=y.sigma;
  x.aps.bias=y.bias;
  aps0(k)=x.aps;
  
  x=load(sprintf('final/1450x1350/real_a_filtp3_weight3_gs_dp%s_jack%s_real_a_filtp3_weight3_gs_dp%s_jack%s1_pureB_overrx.mat',dp,jt,dp,jt));

  % Apply supfac
  aps0(k).Cs_l=aps0(k).Cs_l.*x.r(1).rwf;
  
  % Remove bias and store uncertainty
  aps0(k).Cs_l=aps0(k).Cs_l-y.bias;
    
end


% Get input model
inprp1=load_cmbfast('aux_data/official_cl/camb_planck2013_r0p1.fits');
inplens=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing.fits');


l=aps(1).l;
clf; setwinsize(gcf,500,600)

subplot(2,1,1)
semilogy(l(2:end),aps0(1).Cs_l(2:end,4),'-','Color',[0,0.7,0]);
hold on
semilogy(inprp1.l,2*inprp1.Cs_l(:,4),'--r');
semilogy(inprp1.l,2*inprp1.Cs_l(:,4)+inplens.Cs_l(:,4),'r');
xlim([0,325]);
hold off
ylim([1e-5,.1]);
set(gca,'YTick',[1e-5,1e-3,1e-1]);
ylabel('l(l+1)C_l/2\pi [\muK^2]');
title('BB')

subplot(2,1,2)
plot(l(2:end),aps(1).Cs_l(2:end,4),'-','Color',[0,0.7,0]);
hold on
h=errorbar2(l(2:end),r(1).real(2:end,4),std(r(1).sim(2:end,4,:),[],3),'k.');
set(h(2),'MarkerSize',12)
xlim([0,325]);
hold off
ylim([-.01,.05]);
ylabel('l(l+1)C_l/2\pi [\muK^2]');
xlabel('Multipole')
title('BB jackknife')

% print
print -depsc2 paper_plots/beammapsim_talk.eps
fix_lines('paper_plots/beammapsim_talk.eps')
!epstopdf paper_plots/beammapsim_talk.eps
system_safe(['convert -density 300 paper_plots/beammapsim_talk.pdf paper_plots/beammapsim_talk.png'])

clf;
subplot(2,1,1)
semilogy(l(2:end),aps0(2).Cs_l(2:end,4),'-','Color',[0,0.7,0]);
hold on
semilogy(inprp1.l,2*inprp1.Cs_l(:,4),'--r');
semilogy(inprp1.l,2*inprp1.Cs_l(:,4)+inplens.Cs_l(:,4),'r');
xlim([0,325]);
hold off
ylim([1e-5,.1]);
set(gca,'YTick',[1e-5,1e-3,1e-1]);
ylabel('l(l+1)C_l/2\pi [\muK^2]');
title('BB');

subplot(2,1,2)
plot(l(2:end),aps(2).Cs_l(2:end,4),'-','Color',[0,0.7,0]);
hold on
h=errorbar2(l(2:end),r(2).real(2:end,4),std(r(2).sim(2:end,4,:),[],3),'k.');
set(h(2),'MarkerSize',12)
xlim([0,325]);
hold off
ylim([-.01,.05]);
ylabel('l(l+1)C_l/2\pi [\muK^2]');
xlabel('Multipole');
title('BB jackknife')

% print
print -depsc2 paper_plots/beammapsim_talk2.eps
fix_lines('paper_plots/beammapsim_talk2.eps')
!epstopdf paper_plots/beammapsim_talk2.eps
system_safe(['convert -density 300 paper_plots/beammapsim_talk2.pdf paper_plots/beammapsim_talk2.png'])

return



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function make_EE_colorconstraint()

% I had been loading a data file with likelihoods and constraints...
%load('/Users/Jeff/matlab/BICEP2/0751x1651/colorconstraint_marg.mat');
%load('/n/home04/jpf/biceppipe/bicep2_analysis/paper_plots/colorconstraint_marg.mat');
%load('/n/home04/jpf/biceppipe/bicep2_analysis/paper_plots/colorconstraint_marg_09Mar.mat');
% ... but we get the same answer profiling, which is fast enough to
% run inline!


%  out = color_constraint_b1xb2('EE',1,'n',-3.5:0.01:2);
out = color_constraint_b1xb2('EE',1,'sub_lcdm',0);

% move out variables we need
n = out.n;
L = exp(out.mlike);
L = L/max(L);
MPE = out.MPE;
int68B = out.int68B;
n_cmb = out.n_cmb;

% Plot
clf; setwinsize(gcf,250,250)
axis([-4,4,0,1.1]); box on; axis square
hold on;
%set(gca,'YTick',[]);

% plot sync, CMB and dust lines
n_sync=-3;
n_dust=1.50;

Ls=interp1(n,L,n_sync);
Lc=interp1(n,L,n_cmb);
Ld=interp1(n,L,n_dust);
%plot([n_sync,n_sync],[0,Ls],'b'); text(n_sync+0.05,Ls+0.07,'Sync','rotation',90);
line([n_sync,n_sync],[Ls+0.07,Ls],'Color','b'); text(n_sync,Ls+0.1,'Sync','rotation',90);
plot([n_cmb,n_cmb],[0,Lc],'b'); text(n_cmb-0.5,0.85,'CMB','rotation',90);
%plot([n_dust,n_dust],[0,Ld],'b'); text(n_dust+0.05,Ld+0.07,'Dust','rotation',90);
line([n_dust,n_dust],[Ld+0.07,Ld],'Color','b'); text(n_dust,Ld+0.1,'Dust','rotation',90);

[chi2,pte,sig] = likeratio2sigmas(Lc);
disp(['CMB:  PTE ' num2str(pte,'%3.3f') ' (' num2str(sqrt(chi2),'%3.2f') ' sigma)']);
[chi2,pte,sig] = likeratio2sigmas(Ld);
disp(['Dust:  PTE ' num2str(pte,'%3.3f') ' (' num2str(sqrt(chi2),'%3.2f') ' sigma)']);
[chi2,pte,sig] = likeratio2sigmas(Ls);
disp(['Sync:  PTE ' num2str(pte,'%3.3f') ' (' num2str(sqrt(chi2),'%3.2f') ' sigma)']);

% plot ML/68/95 vert lines
plot([MPE,MPE],[0,interp1(n,L,MPE)],'k'); 
plot([int68B(1),int68B(1)],[0,interp1(n,L,int68B(1))],'k--');
plot([int68B(2),int68B(2)],[0,interp1(n,L,int68B(2))],'k--');

% plot likelihood curve
plot(n,L,'-','color',[0,0.5,0]);

xlabel('Spectral index (\beta)');
ylabel('Likelihood');

%hleg=legend([h1,h2,h3],{'CMB','68% CL','95% CL'},'Location','NorthEast');
%legend boxoff
sigH = int68B(2) - MPE;
sigL = MPE - int68B(1);
text(0,0.8,sprintf('\\beta = %.2f^{+%.2f}_{-%.2f}',MPE,sigH,sigL));

print -depsc2 paper_plots/specindconsEE.eps
fix_lines('paper_plots/specindconsEE.eps')
!epstopdf paper_plots/specindconsEE.eps


return

function make_BBvsSims

  
  % get the standard b2xb2 points
  [rb2,im,ml]=get_b2xb2;
  
  % get b2xb1
%    load final/0751x1651/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1000_jack0_pureB_matrix.mat

  gc = 0.8;
  lw=0.5;
  
  clf; setwinsize(gcf,500,300)
  
  pl = plot(rb2(1).lc(:,4),squeeze(rb2(1).simr(:,4,1:end)),'Color',[gc,gc,gc],'LineWidth',lw);
  hold on
  
  pll = plot(im.l,ml.Cs_l(:,4),'r-');
  plm = plot(im.l,im.Cs_l(:,4)*2,'r--');
  plm = plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');

  
  plot(ml.l,ml.Cs_l(:,4),'r');
  h1=errorbar2(rb2(1).lc(:,4),rb2(1).real(:,4),std(rb2(1).simr(:,4,:),[],3),'k.');
  h2=errorbar2(rb2(1).lc(7:end,4),rb2(1).real(7:end,4),std(rb2(1).simr(7:end,4,:),[],3),'.');
  set(h1(2),'MarkerSize',12);
  set(h2(2),'MarkerSize',12);
  set(h2(2),'Color',[0.4,0.4,0.4]);
  set(h2(1),'Color',[0.4,0.4,0.4]);

  line([0,330],[0,0],'LineStyle',':','color','k'); box on

  hold off
  legend([h1(2),pl(1)],{'B2xB2',['Sims']},'Location','northwest');
  legend boxoff

  xlabel('Multipole'); ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')
  xlim([0,330]); ylim([-.01,.055]);
  axis square
  set(gca,'Layer','top')

  print('-depsc2',['paper_plots/BBvsSims.eps'])
  fix_lines(['paper_plots/BBvsSims.eps'])
  system_safe(['epstopdf paper_plots/BBvsSims.eps'])
  system_safe(['convert -density 300 paper_plots/BBvsSims.pdf paper_plots/BBvsSims.png'])

  
  
return

function make_jackplotV2() 
  
  maps = {...
    'maps/1450/real_a_filtp3_weight3_gs_dp0000_jack1.mat',...
    'maps/1450/real_a_filtp3_weight3_gs_dp1100_jack1.mat',...
    'maps/1450/real_a_filtp3_weight3_gs_dp0000_jack0.mat',...
    'maps/1450/real_a_filtp3_weight3_gs_dp1100_jack0.mat'};
    
  savename={...
    'dp0000_jack1',...
    'dp0000_jack1s1',...
    'dp0000_jack1s2',...
    'dp1100_jack1',...
    'dp1100_jack1s1',...
    'dp1100_jack1s2',...
    'dp0000_jack0',...
    'dp1100_jack0'}
  
  names = {...
    'jack w/o deprojection',...
    'split half A w/o deprojection',...
    'split half B w/o deprojection',...
    'jack w/ deprojection',...
    'split half A w/ deprojection',...
    'split half B w/ deprojection',...  
    'map w/o deprojection',...
    'map w/ deprojection'}
    
  fig=figure;  
  kk=1;
  for ii=1:4
    load(maps{ii})
    map=make_map(ac,m,coaddopt);
    
    jjs=1;
    if size(map,2)==2
      jjs=1:3;
    end

    cmap=map;      
    for jj =jjs
      if jj==2
        cmap=map(1);
      elseif jj==3
        cmap=map(2);
      end
      
      cmap=jackknife_map(cmap);
      cmap=cal_coadd_maps(cmap,get_ukpervolt);
      
%        cmap=make_ebmap(m,cmap,[],[0,1000]);

      dim=get_onepaneldims(m);
      
      clf; 
      set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

      fs = 8;
      % first Q
      plot_map(m,cmap.Q,'iau');
      caxis([-4,4]);
      colormap gray
      freezeColors
      set(gca,'Xticklabel',[],'Xtick',[]);
      set(gca,'Yticklabel',[],'Ytick',[]);
      set(gca,'visible','off')
      text(47,-46,['Q ',names{kk}],'FontSize',fs);

          
      print('-depsc2', ['paper_plots/jack_maps_',savename{kk},'.eps'])
      fix_lines(['paper_plots/jack_maps_',savename{kk},'.eps'])
%        system_safe(['epstopdf --nocompress paper_plots/jack_maps_',savename{kk},'.eps'])
      system_safe(['convert -density 300  paper_plots/jack_maps_',savename{kk},'.eps paper_plots/jack_maps_',savename{kk},'.png'])    
      system_safe(['convert -density 300  -bordercolor white -border 1x1 -matte -fill none -fuzz 7% -draw ''matte 1,1 floodfill'' -shave 1x1 paper_plots/jack_maps_',savename{kk},'.png paper_plots/jack_maps_',savename{kk},'.png'])    
      kk=kk+1;
%        keyboard
    end
  end
  
return

function make_2Daps
  
  purifmatname='matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat';
  [r,im,ml]=get_b2xb2;
  load maps/0751/real_a_filtp3_weight3_gs_dp1102_jack0 ac m
  maps{1}=make_map(ac,m);
  maps{1}=cal_coadd_maps(maps{1},get_ukpervolt);
  maps{1}=make_ebmap(m,maps{1},'normal',[],purifmatname);
  
  load maps/0751/0017_a_filtp3_weight3_gs_dp1100_jack0 ac m
  maps{2}=make_map(ac,m);
  maps{2}=cal_coadd_maps(maps{2},get_ukpervolt);
  maps{2}=make_ebmap(m,maps{2},'normal',[],purifmatname);
  specstr = {'TT','TE','EE','BB','TB','EB'};
  setwinsize(gcf, 510,415)
  for ii = 1:length(maps)
    map = maps{ii};   
    realimag=0;
    pad=1;
    [ad,aps2d,map]=make_aps2d(m,map,'normal',pad,realimag);
    map.Pw=1./((map.Qvar+map.Uvar)/2);
    map.Pw(isnan(map.Pw))=0;
    mft=i2f(ad,map.Pw);
    mft=real(mft.*conj(mft));
    
    xtic=ad.u_val{1}*2*pi;
    ytic=ad.u_val{2}*2*pi;
    [xx,yy]=meshgrid(xtic,ytic);
    dist = sqrt(xx.^2 + yy.^2);
      
    colormap gray;

    for jj=[3,4]
      clf
      switch jj
       case 4
        caps2d = aps2d.B*1e5;
        ca = [0,5];
       case 3
        caps2d = aps2d.E*1e4;
        ca = [0,13];
      end

      % fill in the ft of the mask:
      [dum,xind] = min(abs(xtic+300));
      [dum,yind] = min(abs(ytic-300));
      [dum,xind2] = min(abs(xtic));
      [dum,yind2] = min(abs(ytic));
      wx = round(50 / (xtic(2)-xtic(1)));
      wy = round(50 / (ytic(2)-ytic(1)));
      cmft = mft(xind2-wx:xind2+wx,yind2-wy:yind2+wy);
      caps2d(xind-wx:xind+wx,yind-wy:yind+wy)=cmft/max(cmft(:))*ca(2)*1.1;
      
      imagesc(xtic,ytic,caps2d); axis image
      hold
      
      % box around the mask:
      rectangle('Position',[250,-350,100,100],'EdgeColor','y')
      text(253,-250,'mask','HorizontalAlignment','left','VerticalAlignment','top','FontSize',10,'color','y')
      
      
      xlim([-350,350]); ylim([-350,350]);
      [be,n]=get_bins('bicep_norm');
      be(1)=nan;
      be(12:end)=nan;
      [x,y]=circle(0,0,be,50,1);
      plot(x,y,'r:','linewidth',0.65);
      set(gca,'ydir','normal')
      cb = colorbar();
      cblab = get(cb,'ylabel')
      caxis(ca);      
      switch jj
       case 4
        set(cblab,'String', 'C_l^{BB} x 10^5 [\muK^2]');   
        set(cb,'ytick',0:5);   
       case 3
        set(cblab,'String', 'C_l^{EE} x 10^4 [\muK^2]');   
      end
      set(cblab,'position',get(cblab,'position')+[1.2,0,0])
      % text(0.01,0.97,'Imag','Units','normalized','HorizontalAlignment','left','Fontsize',14,'color','r');
      % text(0.99,0.97,'Real','Units','normalized','HorizontalAlignment','right','Fontsize',14,'color','r');
      set(gca,'TickDir','out')
      xlabel('$\ell_x$','interpreter','latex','fontsize',16)
      ylabel('$\ell_y$','interpreter','latex','fontsize',16)
      

      appendum = [num2str(ii),specstr{jj}];
      print('-depsc2',['paper_plots/2Dspec_',appendum,'.eps'])
      fix_lines(['paper_plots/2Dspec_',appendum,'.eps'])
      system_safe(['epstopdf paper_plots/2Dspec_',appendum,'.eps'])
      system_safe(['convert -density 300 paper_plots/2Dspec_',appendum,'.pdf paper_plots/2Dspec_',appendum,'.png'])
    end
  end
  
return

function make_BBpoweronly
  
  for i=1:6
    chibins{i}=[2:10]; % use all plotted bins
  end

  % get the real points
  [r,im,ml]=get_b2xb2;
  r=calc_chi(r,chibins,[],1);

  clf; setwinsize(gcf,300,300)

  ms=10; % marker size
  line([0,330],[0,0],'LineStyle',':','color','k'); box on
  hold on
  
  plot(im.l,im.Cs_l(:,4)*2+ml.Cs_l(:,4),'r--');
  plot(ml.l,ml.Cs_l(:,4),'r');
  h=errorbar2(r(1).lc(:,4),r(1).real(:,4),std(r(1).simr(:,4,:),[],3),'k.');
  
  set(h(2),'MarkerSize',ms);
  xlim([0,330]); ylim([-.015,.055]);
  hold off
  xlabel('Multipole')
  ylabel('l(l+1)C_l/2\pi [\muK^2]')
  axis square;

  print('-depsc2',['paper_plots/BBpowerspec.eps'])
  fix_lines(['paper_plots/BBpowerspec.eps'])
  system_safe(['epstopdf paper_plots/BBpowerspec.eps'])
  system_safe(['convert -density 300 paper_plots/BBpowerspec.pdf paper_plots/BBpowerspec.png'])

return

function make_mapOutlook2014()
  [dum,dum,dum,dum,dum,dum,dum,dum,mapname{1},dum]=get_b2xb2;
  [dum,dum,dum,dum,dum,dum,dum,dum,mapname{2},dum]=get_k1213;
  [dum,dum,dum,dum,dum,dum,dum,dum,mapname{3},dum]=get_b2k_comb;
  [dum,dum,dum,dum,dum,dum,dum,dum,mapname{4},dum]=get_k14;

  ellrng=[50,120];
  upsample_factor=2;
  for ii=1:4
    load(mapname{ii})
    % bicep2 and Keck2014 are uncalibrated
    % this needs to be run from a keck_analysis folder
    if (ii==1)
      ac=cal_coadd_ac(ac,3150,coaddopt);
    end
    if (ii==2)
      ac=coadd_ac_overrx(ac);
    end
    if (ii==4)
      ac=cal_coadd_ac(ac,get_ukpervolt('2014'),coaddopt);
      ac=coadd_ac_overfreq(ac,coaddopt);
      % select keck95 GHz
      ac = ac(1,:); 
    end
    map{ii} = make_map(ac,m,coaddopt);
    map{ii} = make_ebmap(m,map{ii},[],ellrng);
%      [m,map{ii}.E]=upsample_map(m,map{ii}.E,upsample_factor);
  end
  
  titles = {'BICEP2','Keck12+13','BICEP2 + Keck12+13','Keck14 95 GHz'};
  fns = {'B2','K','B2K','K1495'};
  % color range
  ce=2.4;
  % vector length scale factor
  ve=0.6;
  keyboard
  
  for ii=1:4
    fig=figure;
    set(fig,'visible','off')
    setwinsize(gcf,800,600)
   
    plot_map(m,nan(size(map{ii}.E)),'iau');
    caxis([-ce,ce]);
    colormap(colormap_lint)
    freezeColors
    hold on
    plot_pol(m,nan(size(map{ii}.EQ)),nan(size(map{ii}.EU)),map{ii}.Qvar,map{ii}.Uvar,ve,[],3,1); 
    plot_map(m,map{ii}.E,'iau');
    caxis([-ce,ce]);
    colormap(colormap_lint)
    freezeColors
    hold on
    plot_pol(m,map{ii}.EQ,map{ii}.EU,map{ii}.Qvar,map{ii}.Uvar,ve,[],3,1)
    title([titles{ii},' E-mode signal']);
    xlabel('Right ascension [deg.]');
    ylabel('Declination [deg.]');

    % this works way better than direct print to pdf
    set(fig,'Renderer','painters')
    print('-depsc2',['paper_plots/MapComp_E_',fns{ii},'.eps'])
    system_safe(['epstopdf --nocompress paper_plots/MapComp_E_',fns{ii},'.eps'])
    system_safe(['convert -density 200 -colorspace rgb paper_plots/MapComp_E_',fns{ii},'.eps paper_plots/MapComp_E_',fns{ii},'.png'])
  end
    
  dim=get_twopaneldims(m)
  fig2=figure;
  set(fig2,'visible','off')
  set(fig2, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');
  for ii=1:4
    clf
    ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
    plot_map(m,map{ii}.Q,'iau');
    caxis([-4,4]);
    colormap gray
    freezeColors
    set(ax1,'FontSize',7,'FontWeight','normal');
    set(ax1,'Xticklabel',[]);
    title([titles{ii},' Q signal']);
    
    ax2=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
    plot_map(m,map{ii}.U,'iau');
    caxis([-4,4]);
    colormap gray
    freezeColors
    set(ax2,'FontSize',7,'FontWeight','normal');
    set(ax2,'Xticklabel',[]);
    title([titles{ii},' U signal']);
    
    xlabel('Right ascension [deg.]');
    ylabel('Declination [deg.]');
    
    set(fig2,'Renderer','painters')
    print('-depsc2',['paper_plots/MapComp_QU_',fns{ii},'.eps'])
    system_safe(['epstopdf paper_plots/MapComp_QU_',fns{ii},'.eps'])
    system_safe(['convert -density 200 paper_plots/MapComp_QU_',fns{ii},'.pdf paper_plots/MapComp_QU_',fns{ii},'.png'])
    
  end
  
return

function make_total_polarizations
  keyboard
  [dum,dum,dum,dum,dum,dum,dum,dum,mapname{1},dum]=get_b2xb2;
  [dum,dum,dum,dum,dum,dum,dum,dum,mapname{2},dum]=get_k1213;
  [dum,dum,dum,dum,dum,dum,dum,dum,mapname{3},dum]=get_b2k_comb;
  [dum,dum,dum,dum,dum,dum,dum,dum,mapname{4},dum]=get_k14;
  
  mapname{5} = 'maps/1457/real_aab_filtp3_weight3_gs_dp1100_jack0.mat';
  
  ellrng=[50,120];
  ce=1.6; % 2.4
    
  for ii=1:5
    load(mapname{ii})
    % bicep2 and Keck2014 are uncalibrated
    % this needs to be run from a keck_analysis folder
    if (ii==1)
      ac=cal_coadd_ac(ac,3150,coaddopt);
    end
    if (ii==2)
      ac=coadd_ac_overrx(ac);
    end
    if (ii==4)
      ac=cal_coadd_ac(ac,get_ukpervolt('2014'),coaddopt);
      ac=coadd_ac_overfreq(ac,coaddopt);
      % select keck95 GHz
      ac = ac(1,:); 
    end
    if (ii==5)
      ac = ac(end);
    end
    map{ii} = make_map(ac,m,coaddopt);
    map{ii}=make_tqu_apmap(m,map{ii},[],ellrng);

  end
  
  titles = {'BICEP2','Keck12+13','BICEP2 + Keck12+13','Keck14 95 GHz','P353'};
  fns = {'B2','K','B2K','K1495','P353'};
  cm = colormap_lint();
  cm = cm(64:end,:);
  % vector length scale factor
  ve=0.6;
  
  for ii=1:5
    fig=figure(1); clf; setwinsize(gcf,800,600);
    set(fig,'visible','off')
    
    cve = ve;
    cce = ce;
    if (ii==5) cve = ve/6; end;
    if (ii==5) cce = ce*6; end;

    plot_map(m,sqrt(map{ii}.Qap.^2 + map{ii}.Uap.^2),'iau');
    caxis([0,cce]);
    colormap(cm)
    freezeColors
    hold on
    
    plot_pol(m,map{ii}.Qap,map{ii}.Uap,map{ii}.Qvar,map{ii}.Uvar,cve,[],3,1)

    caxis([0,cce]);
    colormap(cm )
    cb = colorbar;
    set(get(cb,'ylabel'),'String', 'sqrt(Polarized Intenstity) [\muK]');
    
    title([titles{ii},' total polarization signal']);
    xlabel('Right ascension [deg.]');
    ylabel('Declination [deg.]');
    print('-depsc2',['paper_plots/MapComp_TotPol_',fns{ii},'.eps'])
    system_safe(['convert -density 200 -colorspace rgb paper_plots/MapComp_TotPol_',fns{ii},'.eps paper_plots/MapComp_TotPol_',fns{ii},'.png'])
  end  
    


return
