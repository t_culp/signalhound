function reduc_makespec(tags,filttype,nset,gs)
% reduc_makespec(tags,filttype,nset,gs)
%
% make noise spectra for a days data
%
% reduc_makespec('050820','p0')
% reduc_makespec('B20060730_cmb11_1B_D_000',{'s3','d3'},100) % BICEP (d for dif)

if(~exist('filttype','var'))
  filttype=[];
end
if(~exist('nset','var'))
  nset=[];
end
if(~exist('gs','var'))
  gs=[];
end

% allow for single or loop over tags
if(~iscell(tags))
  tag{1}=tags;
  tags=tag;
end

if(isempty(filttype))
  filttype='p3';
end

if(isempty(nset))
  nset=100; 
end
if(isempty(gs))
  gs=0;
end

for i=1:length(tags)
  tag=tags{i};
  tic
  % Get array info (only ind used)
  [p,ind]=get_array_info(tag);
  
  % read in tod
  load(strcat('scratch/',tag,'_tod'));
  
  % sum/dif the pairs to odd/even channels of all light channels
  [d,p]=sumdiff_pairs(d,p,fs,ind.la); 
  
  % gapfill light channels
  d=gapfill(d,fs,ind.gl,d.scancheck_v1>1);   
   % if requested ground subtract the data
  if(gs==1)
    % do ground subtraction
    d=ground_subtract(d,fs,ind.l);
  end
  
  if(isempty(strfind(d.scan.src{1},'CMB4'))) 	% galpl 1:16
    qw=fs.set ~= 1;
    fs=structcut(fs,qw);
    d.scancheck_v1 = d.scancheck_v1(:,find(qw));
  end
  
  if(isempty(strfind(d.scan.src{end},'CMB4'))) %galpl 1017:1032
    qw = fs.set ~= max(fs.set);
    fs=structcut(fs,qw);
    d.scancheck_v1 = d.scancheck_v1(:,find(qw));
  end

  % Ignore scancheck bit 0 (elnod relgain flag) for non-lights:
  d.scancheck_v1(setdiff(1:144,ind.l),:)=d.scancheck_v1(setdiff(1:144,ind.l),:)-1;
    
  % filter half scans
  d=filter_scans(d,fs,filttype{1},ind.la);
  d=filter_scans(d,fs,filttype{2},ind.lb);
 
  %skip half-scans with multi channel glitch
  scancheck=false(size(d.scancheck_v1));
  skipscans=find(sum(d.scancheck_v1(ind.gl,:) > 1,1) > 24);
  disp(sprintf('%i halfscans discarded from %i total',size(skipscans,2), size(scancheck,2)))
  if size(skipscans,2) > 500 
    continue;
  end
  scancheck(:,skipscans)=1;
 
  % get the spectra
  spec=scan_ft(d,fs,nset,ind,scancheck)
 
  % save the spectra 
  dirsave='./noispec_tmp/noispec_v03/';
  if(gs~=1)
    save(sprintf('%s%s_noispec_filt%s%s_nset%02d',dirsave,tag,filttype{1},filttype{2},nset),'spec','fs');
  else
    save(sprintf('%s%s_noispec_filt%s%s_nset%02d_gs',dirsave,tag,filttype{1},filttype{2},nset),'spec','fs');
  end
  toc
end

return

