function reduc_make_tag_list(log_dir,tag_file,stats_file,t1,t2)
% reduc_make_tag_list(log_dir,tag_file,stats_file,t1,t2)
%
% Create or update the CSV file identifying
% all data "tags".  Each tag is the basic
% chunk of observing data, equal to a scan set
% for BICEP2, BICEP3 and the Keck Array.
%
% log_dir is the directory with the GCP
% run logs, usually a symlink 'log'.
%
% tag_file is the CSV file to be first read,
% and then re-written incorporating any new
% scan sets from the GCP logs.  Usually
% 'aux_data/tag_list.csv'.
%
% stats_file is the CSV file to save tag live-time
% statistics.  These are calculated from the
% same pass through the log files.
%
% e.g.
% reduc_make_tag_list()
% reduc_make_tag_list([],[],[],'2011-jun-1','2011-jul-1')
%
% t1 and t2 are optional start and end times
% of the log files to read, in the format
% '2010-jan-1' or '2010-jan-1:10:01:35', etc.
%
% if t1 is not specified t1 is 1 day before last time specified
% in tag file; if t2 is not specified t2 is today


% Default options if needed
if (nargin<1)||isempty(log_dir)
  log_dir='log';
end
if ~exist(log_dir,'dir')
  error(['Log file directory ' log_dir ' not found.']);
end
if (nargin<2)||isempty(tag_file)
  tag_file='aux_data/tag_list.csv';
end
if (nargin<3)||isempty(stats_file)
  stats_file='aux_data/tag_stats.csv';
end
if (nargin<4)||isempty(t1)
  t1='';
end
if (nargin<5)||isempty(t2)
  t2='';
end

% Read current tag list file, if
% it already exists.
if exist(tag_file,'file')
  [p k]=ParameterRead(tag_file);
  % trim trailing whitespace on comment field, which would
  % otherwise grow without bound because of oddities in
  % ParameterRead / ParameterWrite.
  p.comment=strtrim(p.comment);
else
  p=[];
  p.tag={};
  k.fields={};
end

if(strcmp(t1,'') & ~isempty(p))
  t1=datenum(p.tstart{end},'dd-mmm-yyyy:HH:MM:SS');
  t1=datestr(t1,'yyyy-mmm-dd');
end

if(strcmp(t2,''))
  t2=datestr(now,'yyyy-mmm-dd');
end

% Parse the GCP run logs for all
% scan sets.
[C F SCH]=parse_run_log(log_dir,t1,t2);

% Generate tag names for all scan sets
% parsed from run logs.
% Format is yymmddPNN_dk###
% yymmdd=date of schedule start
% PP=phase letter code A-I
% NN=scanset within phase
%    (0 corresponds to phase cals)
% ###=deck angle in degrees
for ii=1:length(C)
  if C(ii).ch<0
    tmp=[C(ii).t0 C(ii).ph 'x' num2str(abs(C(ii).ch),'%.1d')];
  else
    tmp=[C(ii).t0 C(ii).ph num2str(C(ii).ch,'%.2d')];
  end
  if ~isempty(C(ii).dk)
    tmp=[tmp '_dk' num2str(round(C(ii).dk),'%.3d')];
  end
  C(ii).tag=tmp;
end

% Generate "A" phase tag names for cryo service.
% These can't be put through the pipeline, but it's
% useful to have a label for them.

% This needs to be a little bit clever.  We don't
% want only the fridge cycle, but also the star
% pointing, etc.  But if there's no fridge cycle,
% as when the schedule is restarted after a crash,
% we don't want an "A" phase.  Here's the logic to
% use:
%   - identify fridge cycles
%   - create logical "A" phases
%   - use cycle start and start times
%   - identify end of last schedule before cycle,
%     start of next schedule after cycle.
%   - if within 6 hr of cycle, use these boundaries
%     as the phase "A" start / end times.

C_cyc(1).t1=NaN;
C_cyc(1).t2=NaN;
C_cyc(1:length(F))=C_cyc(1);
tSCH=NaN*zeros(length(SCH),2);
for j=1:length(SCH)
  tSCH(j,:)=SCH(j).t;
end
j0=0;
for ii=1:length(F)
  t1=F(ii).t(1);
  t2=F(ii).t(2);
  % Find any neighboring CMB schedule start/end
  dt1=t1-tSCH(:,2);
  dt2=tSCH(:,1)-t2;
  jdx1=find(dt1>0 & dt1<6/24);
  jdx2=find(dt2>0 & dt2<6/24);
  for jj=length(jdx1):-1:1
    [schpath schname schext]=fileparts(SCH(jdx1(jj)).schedule);
    if ~strcmp(schext,'.sch')
      continue
    end
    if strncmp(schname,'9_CMB',5) || strncmp(schname,'9_kCMB',6)
      t1=SCH(jdx1(jj)).t(2);
      break
    end
  end
  for jj=1:length(jdx2)
    [schpath schname schext]=fileparts(SCH(jdx2(jj)).schedule);
    if ~strcmp(schext,'.sch')
      continue
    end
    if strncmp(schname,'9_CMB',5) || strncmp(schname,'9_kCMB',6)
      t2=SCH(jdx2(jj)).t(1);
      break
    end
  end

  % If cycle is still in progress at end of currently available
  % log files, don't create the 'phase' yet.
  if isnan(t2)
    continue
  end

  % Merge with prev. fridge cycle entry
  % This is needed in case of multiple cycle stop/starts.
  if (j0>0) && (t1<=datenum(C_cyc(j0).t2,'dd-mmm-yyyy:HH:MM:SS'))
    tmp_t1=min(t1,datenum(C_cyc(j0).t1,'dd-mmm-yyyy:HH:MM:SS'));
    tmp_t2=max(t2,datenum(C_cyc(j0).t2,'dd-mmm-yyyy:HH:MM:SS'));
    C_cyc(j0).t1=datestr(tmp_t1,'dd-mmm-yyyy:HH:MM:SS');
    C_cyc(j0).t2=datestr(tmp_t2,'dd-mmm-yyyy:HH:MM:SS');
    C_cyc(j0).livetime(1)=(tmp_t2-tmp_t1)*24*60;
    C_cyc(j0).livetime(5)=C_cyc(j0).livetime(5)+(F(ii).t(2)-F(ii).t(1))*24*60;
  % Or start a new entry
  else
    j0=j0+1;
    C_cyc(j0).t1=datestr(t1,'dd-mmm-yyyy:HH:MM:SS');
    C_cyc(j0).t2=datestr(t2,'dd-mmm-yyyy:HH:MM:SS');
    C_cyc(j0).sch='Cryo service';
    C_cyc(j0).t0=datestr(F(ii).t(1),'yyyymmdd');
    C_cyc(j0).scset='';
    if isempty(F(ii).ph)
      C_cyc(j0).ph='A';
    else
      C_cyc(j0).ph=F(ii).ph;
    end
    C_cyc(j0).ch=0;
    C_cyc(j0).sc='';
    C_cyc(j0).nsc=0;
    C_cyc(j0).dk=0;
    C_cyc(j0).elofs=0;
    C_cyc(j0).tag=[C_cyc(j0).t0 C_cyc(j0).ph];
    C_cyc(j0).livetime=zeros(1,7);
    C_cyc(j0).livetime(1)=(t2-t1)*24*60;
    C_cyc(j0).livetime(5)=(F(ii).t(2)-F(ii).t(1))*24*60;
    C_cyc(j0).nmark=[1,0,0,0,1,0,0;1,0,0,0,1,0,0];  % One fridge cycle, nothing else
  end
end
if j0>0
  if length(C)>0
    C=[C,C_cyc(1:j0)];
  else
    C=[C_cyc(1:j0)];
  end
end

% Decide which tags represent complete scansets
for ii=1:length(C)
  isgood=all(C(ii).nmark(:,1)==1) ...   % One total start / stop
      && all(C(ii).nmark(:,2)==1) ...   % One set of field scans
      && all(C(ii).nmark(:,3)==2) ...   % Two el nods
      && all(C(ii).nmark(:,4)==2);      % Two partial load curves
  if isgood
    C(ii).good='1';
  else
    C(ii).good='0';
  end
end

% Fields to be included in new output list
% (can be expanded to include new fields
% by adding them here)
% Specify experiment name in the comment
expt=get_experiment_name();
switch expt
  case 'bicep3'
    experiment='BICEP3';
  case 'keck'
    experiment='Keck';
  case 'bicep2'
    experiment='BICEP2';
end
k_new.comments={[experiment ' complete tag list'],...
  ['Updated ' datestr(now)]};
k_new.fields ={'TAG',   'TSTART','TEND',  'SCH',   'PH',    'SET',   'DK',    'ELOFS', 'SCAN',  'TYPE',  'NSCAN', 'GOOD',  'COMMENT'};
k_new.units  ={'-',     '-',     '-',     '-',     '-',     '-',     'deg',   'deg',   '-',     '-',     '-',     '-',        '-'};
k_new.formats={'string','string','string','string','string','string','string','string','string','string','string','string','string'};

% Make CSV file entries for newly parsed tags
p_new=[];
for ii=1:length(C)
  p_new.TAG    {ii}=C(ii).tag;
  p_new.TSTART {ii}=C(ii).t1;
  p_new.TEND   {ii}=C(ii).t2;
  p_new.SCH    {ii}=C(ii).sch;
  p_new.PH     {ii}=C(ii).ph;
  p_new.SET    {ii}=num2str(max(0,C(ii).ch));
  p_new.TYPE   {ii}=C(ii).scset;
  p_new.DK     {ii}=num2str(C(ii).dk);
  p_new.ELOFS  {ii}=num2str(C(ii).elofs);
  p_new.SCAN   {ii}=C(ii).sc;
  p_new.NSCAN  {ii}=num2str(C(ii).nsc);
  p_new.GOOD   {ii}=C(ii).good;
  p_new.COMMENT{ii}='""';
end
newtags=p_new.TAG;

% Merge in entries already existing from CSV file
old_fields=k.fields;
new_fields=setdiff(k_new.fields,old_fields);
for ii=1:length(p.tag)
  % try to match up with an entry in the new tag list
  matched=false;
  if p.tag{ii}(9)=='X'
    jj=find(strncmp(p.tag{ii},p_new.TAG,8));
  else
    jj=find(strcmp(p.tag{ii},p_new.TAG));
  end
  for kk=1:length(jj)
    if datenum(p_new.TSTART{jj(kk)},'dd-mmm-yyyy:HH:MM:SS')<datenum(p.tend{ii},'dd-mmm-yyyy:HH:MM:SS') ...
    && datenum(p_new.TEND{jj(kk)},'dd-mmm-yyyy:HH:MM:SS')>datenum(p.tstart{ii},'dd-mmm-yyyy:HH:MM:SS')
      matched=true;
      newpos=jj(kk);
      break;
    end
  end
  % otherwise, append
  if ~matched
    newpos=length(p_new.TAG)+1;
    for onfield=1:length(new_fields)
      p_new.(upper(new_fields{onfield})){newpos}='';
    end
  end
  % and copy fields
  for onfield=1:length(old_fields)
    p_new.(upper(old_fields{onfield})){newpos}=p.(old_fields{onfield}){ii};
  end
end

% Sort all tags by start time
tstart=datenum(p_new.TSTART,'dd-mmm-yyyy:HH:MM:SS');
tend=datenum(p_new.TEND,'dd-mmm-yyyy:HH:MM:SS');
p_new_tmp=p_new;
[tstart,sortidx]=sort(tstart);
fl=fieldnames(p_new);
for onfield=1:length(fl)
  p_new_tmp.(fl{onfield})=[p_new.(fl{onfield})(sortidx)];
end
p_new=p_new_tmp;
clear p_new_tmp;

% Rename tags with duplicate names
[tmp,tmpi,tagindex]=unique(p_new.TAG);
for ii=1:max(tagindex)
  if sum(tagindex==ii)>1
    % rename all but the last one (or it may rename all but the first one in case the reduction was run in two different runs)
    jj=find(tagindex==ii);
    for kk=1:(length(jj)-1)
      tmptag=p_new.TAG{jj(kk)};
      setno=1;
      tmptag(9:11)=['X' num2str(setno,'%.2d')];
      while sum(strcmp(p_new.TAG,tmptag))>0
        setno=setno+1;
        tmptag(9:11)=['X' num2str(setno,'%.2d')];
      end
      if sortidx(jj(kk))<=length(C)
        p_new.TAG{jj(kk)}=tmptag;
        % Modify the tag in the C structure as well, so it goes to the right
        % place in tag_stats
        C(sortidx(jj(kk))).tag=tmptag;
      end
    end
  end
end

% Keep at most one cryo service tag before the first real
% observing tag
first_obs=Inf;
for ii=1:length(p_new.TAG)
  if ~isempty(p_new.PH{ii}) && ~strcmp(p_new.PH{ii},'A')
    first_obs=ii;
    break;
  end
end
if ~isinf(first_obs) && (first_obs>2)
  fl=fieldnames(p_new);
  for onfield=1:length(fl)
    if iscell(p_new.(fl{onfield}))
      p_new.(fl{onfield})={p_new.(fl{onfield}){(first_obs-1):end}};
    else
      p_new.(fl{onfield})=[p_new.(fl{onfield})((first_obs-1):end)];
    end
  end
end

% Write out new CSV file
ParameterWrite(tag_file,p_new,k_new);
setpermissions(tag_file);

% Update tag stats CSV file with live time stats already tabulated
reduc_calc_stats(C,stats_file);

return
