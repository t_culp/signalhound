function reduc_farm_calplots(tags,farmstuff,cpfilestobicepfs1)
% reduc_farm_calplots(tags,farmstuff)
% 
% inputs: tags: Tag list to be processed
%         farmstuff: Set to 1 to farm generation of cal plots
%                    On spud, set to maximum number of jobs to run at once

% return if tags is emtpy
if(isempty(tags))
  return
end

if nargin == 0
    error('Not enough input arguments')
end

if nargin < 2
    farmstuff = 0;
end

% Farming options
MEM = 8000;
LICENSE = 'bicepfs1:25'; % 1500 slots -> 1500/25 = 60 jobs
MAXTIME = 25;

% File for error messages
errorfile='reducplots_failed.txt';

% Organize tag list according to day:
days = parse_tag(tags);
days = unique(days);
for i = 1:length(days)
    ri = get_run_info(num2str(days(i)));
    tagz{i} = ri.tag(strncmpi(ri.type,'cmb',3) | strncmpi(ri.type,'gal',3));
end

% get rid of TODs that don't exist / have no calvals
good_sched=true(size(tagz));
for i=1:numel(tagz)
  tags_temp=tagz{i};
  keepind=false(size(tags_temp));
  for j=1:numel(tags_temp)
    keepind(j)=logical(exist(sprintf('data/real/%s/%s_calval.mat',tags_temp{j}(1:6),tags_temp{j}),'file'));
  end
  tags_temp=tags_temp(keepind);
  tagz{i}=tags_temp;
  good_sched(i)=~isempty(tags_temp);
end
tagz={tagz{good_sched}};

% Process reduc_plotcals
[p,ind]=get_array_info(tags{1});
rx=unique(p.rx);
for i=1:numel(rx)
  rxdo{i}=rx(i);
end
if(numel(rx)>1)
  rxdo{i+1}=rx;
end

for k=1:numel(rxdo)
  rxdok = rxdo{k};
  for i = length(tagz):-1:1
    daytags = tagz{i};
    switch size(rxdok,1)
     case 1
      % make a string that contains the single receiver
      rxstr = sprintf('_rx%i',rxdok);
     otherwise
      % make a string that contains all receivers in the current rxdo
      rxstr = sprintf('_multi%s',strrep(num2str(rxdok'),' ',''));
    end
    if farmstuff
      % Control number of jobs running at once on SPUD where there is no queueing
      % architechture 
      pid=myprocesses('farmit\(');
      while numel(pid)>=farmstuff
        pid=myprocesses('farmit\(');  
        pause(1);
      end

      [farmfile,jobname] = farmfilejobname('BICEP', 'reduc_plotcals', ...
          [daytags{1},rxstr]);
      farmit(farmfile, 'reduc_plotcals(daytags,1,rxdok,cpfilestobicepfs1);',...
             'jobname',jobname, 'mem',MEM, ...
             'license',LICENSE, 'account','bicepdata_group', ...
             'var',{'daytags','rxdok','cpfilestobicepfs1'},...
             'overwrite','skip','maxtime',MAXTIME);
    else
      try
        reduc_plotcals(daytags,1,rxdok,cpfilestobicepfs1);
      catch exception
        disp([ daytags{1} 'et al failed']);
        fid=fopen(errorfile,'a');
        fprintf(fid,'%s\t%s\n',daytags{1},exception.message)
        fclose(fid)
      end
    end
  end
end


% track snow accumulation on BICEP2's window from LFB cam
if strcmp(get_experiment_name(), 'bicep2')
  for i = length(tagz):-1:1
    daytags = tagz{i};
    if farmstuff
      % Control number of jobs running at once on SPUD where there is no queueing
      % architechture 
      pid=myprocesses('farmit\(');
      while numel(pid)>=farmstuff
        pid=myprocesses('farmit\(');  
        pause(1);
      end
      farmit('farmfiles',['plot_lfbsnow(daytags,0,1);'],...
             'jobname','reduc_lfbsnow', 'mem',MEM, ...
             'license',LICENSE, 'account','bicepdata_group', ...
             'var',{'daytags'},'overwrite','skip');
    else
      try
        plot_lfbsnow(daytags,0,1);
      catch exception
        disp([ tagz{i}{1} 'et al failed']);
%         fid=fopen(errorfile,'a');
%         fprintf(fid,'%s\t%s\n',tagz{i}{1},exception.message)
%         fclose(fid)
      end
    end
  end
end

return
