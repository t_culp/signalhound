function reduc_processnewtags(tags,farmstuff,more_mem,onlyreducplots,onlypairmaps,cpfilestobicepfs1)
% reduc_processnewtags(tags,farmstuff,more_mem,onlyreducplots,onlypairmaps,cpfilestobicepfs1)
% 
% run tags through reduc_initial, reduc_applycal, reduc_plotscans and
% reduc_makepairmaps for specified tags
%
% input: tags           = list of tags to process
%        farmstuff      = 0/1 whether to farm jobs or not
%                         (On spud, equals max number of jobs to run at once)
%        more_mem       = 0 (default) request 8 GB of memory
%                       = 1 requested 16 GB of memory 
%        onlyreducplots = 0 (default) reduc_intial, applycal, cutparams,
%                         plotscans, pairmaps
%                       = 1 only plotscans
%        onlypairmaps   = 0 (default) reduc_intial, applycal, cutparams,
%                         plotscans, pairmaps
%                       = 1 only reduc_makepairmaps
%        cpfilestobicepfs1 = [] (default) doesn't do anything beyond
%                          = '/n/bicepfs1/keck/pipeline/reducplots' copies
%                            reducplots here

if nargin == 0
  error('Not enough input arguments. Please specify tag list.')
end

if nargin < 2
  farmstuff=0;
end

if ~exist('more_mem','var')
  more_mem=[];
end
if isempty(more_mem)
  more_mem=0;
end

if ~exist('onlyreducplots','var')
  onlyreducplots=0;
end

if ~exist('onlypairmaps','var')
  onlypairmaps=0;
end

if ~exist('cpfilestobicepfs1','var')
  cpfilestobicepfs1=[];
end

% record the scansets that fail
errorfile='./reducplots_failed.txt';
if exist(errorfile); movefile(errorfile,[errorfile '.tmp']); end

if farmstuff
  % serial_requeue isn't passed along from reduc_processnewtags_driver, so
  % assume serial_requeue=1 here.
  QUEUE = 'serial_requeue';
  LICENSE = 'bicepfs1:17'; % 1500 slots -> 1500/20 = 88 jobs
  MAXTIME = 1440; % in minutes

  if more_mem
    MEM=27000;
  else
    MEM=8000;
  end
end

for i=1:length(tags);
 disp(tags{i})
  if farmstuff
  % Control number of jobs running at once on SPUD where there is no queueing
  % architechture
    pid=myprocesses('farmit\(');
    while numel(pid)>=farmstuff
      pid=myprocesses('farmit\(');
      pause(1);
    end
    [farmfile,jobname] = farmfilejobname('BICEP', 'reduc_batch1', ...
        tags{i});
    farmit(farmfile, ...
           ['reduc_batch1({''' tags{i} '''},1,' num2str(onlyreducplots) ',' ...
             num2str(onlypairmaps) ',''' cpfilestobicepfs1 ''');'], ...
           'jobname',jobname, 'queue',QUEUE, 'account','bicepdata_group', ...
           'mem',MEM, 'maxtime',MAXTIME,'license',LICENSE,'overwrite',true);
  else
    try
      reduc_batch1(tags(i),1,onlyreducplots,onlypairmaps,cpfilestobicepfs1);
    catch exception
      disp([ tags{i} ' failed']);
      fid=fopen(errorfile,'a');
      fprintf(fid,'%s\t%s\n',tags{i},exception.message)
      fclose(fid)
    end

  end

end

