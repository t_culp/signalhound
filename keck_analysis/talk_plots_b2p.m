function talk_plots_b2p(pl)
% talk_plots_b2_b2p(pl)
%
% Make additional plots for presentations

set(0,'defaultlinelinewidth',1.0);
set(0,'DefaultAxesFontSize',14);

if(any(pl==1))
  % make BK TQU signal and jack maps
  make_maps_bk;
end

if(any(pl==2))
  % make P353 raw TQU maps
  make_maps_p353(1);
end

if(any(pl==3))
  % make P353 ap TQU maps
  make_maps_p353(2);
end

if(any(pl==4))
  % make P353 filt/ap TQU maps
  make_maps_p353(3);
end

if(any(pl==5))
  % make BK vs P353 TQU maps
  make_maps_bkvp(1);
end

if(any(pl==6))
  % make sim BK vs P353 TQU maps
  make_maps_bkvp(2);
end

if(any(pl==7))
  % make fig2 in B2P paper with heavy lines
  paper_plots_b2p(2,1);
end

if(any(pl==8))
  % make BB BKxBK and BKxP353 zoom in
  make_specplot1_zoom;
end

if(any(pl==9))
  % Look at all freqs
  make_specplot2(1);
end
if(any(pl==10))
  % Look at all freqs
  make_specplot2(2);
end

% plot the spectra versus multicomp model
if(any(pl==11))
  % sequence of 2 to illustrate adding EE
  make_multifid(1); % fiducial analysis
  make_multifid(2); % +EE
end
if(any(pl==12))
  % sequence of 2 to illustrate high BKx30 EE is not due to sync
  make_multifid(3); % w 30+sync
  make_multifid(4); % w 30+sync+EE
end
if(any(pl==13))
  % sequence of 3 to illustrate sync corr
  make_multifid(5); % w 30+sync+EE+100%corr
end

if(any(pl==14))
  make_decorrplot;
end

if(any(pl==15))
  make_synthmap;
end

if(any(pl==16))
  make_EBcomp;
end

if any(pl == 17)
  make_perbin_comp;
end

if(any(pl==18))
  make_bk_vs_world;
end

if(any(pl==19))
  make_wmap_sync;
end

if(any(pl==99))
  % make improvement plot
  make_future
end

return

function dim=get_onepaneldims(m)
% Parameters for six-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 1 * dim.wide - 1 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 1 * dim.maph + dim.wide + 1 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2 (colorbar).
dim.y1 = dim.wide / dim.H; % Bottom edge of row 1.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return


function dim=get_twopaneldims(m)
% Parameters for six-panel layout.
dim.W = 4.8; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 1.05; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 2 * dim.maph + dim.wide + 2 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + 1 * dim.mapw + 1 * dim.thin) / dim.W; % Left edge of column 2 (colorbar).
dim.y1 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y2 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.
return

%%%%%%%%%%%%%%%%%%%%%
% used by make_ebplot above
function dim=get_fourpaneldims(m)
% Parameters for six-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 2 * dim.maph + dim.wide + 2 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 2 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y2 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return


%%%%%%%%%%%%%%%%%%%%%
function make_maps_bk

% make a plot of the BK maps in style of fig1 of B2 PRL and fig3 of
% the Keck paper
load maps/1456/real_aab_filtp3_weight3_gs_dp1102_jack0
map=make_map(ac,m,coaddopt);
%maps=cal_coadd_maps(map,get_ukpervolt);
maps=map;

load maps/1456/real_aab_filtp3_weight3_gs_dp1102_jack3
map=make_map(ac,m,coaddopt);
map=jackknife_map(map);
%mapj=cal_coadd_maps(map,get_ukpervolt);
mapj=map;

figure(1); clf;
dim=get_3x2dims(m);
set(gcf, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% T

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,maps.T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
title('T signal');

ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapj.T,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax2,'FontSize',7,'FontWeight','normal');
set(ax2,'Xticklabel',[],'Yticklabel',[]);
title('T jackknife');

% Q

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,maps.Q,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax3,'FontSize',7,'FontWeight','normal');
set(ax3,'Xticklabel',[]);
title('Q signal');

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapj.Q,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax4,'FontSize',7,'FontWeight','normal');
set(ax4,'Xticklabel',[],'Yticklabel',[]);
title('Q jackknife');

% U

ax5=axes('Position',[dim.x1,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,maps.U,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax5,'FontSize',7,'FontWeight','normal');
title('U signal');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

ax6=axes('Position',[dim.x2,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,mapj.U,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax6,'FontSize',7,'FontWeight','normal');
set(ax6,'Yticklabel',[]);
title('U jackknife');

% colorbars

ax7 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax7,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-150,150,1000),repmat(linspace(-150,150,1000)',1,10));
colormap hot;
freezeColors;
set(ax7,'Xtick',[]);
set(ax7,'YDir','normal','YAxisLocation','right','YTick',[-100 0 100]);

ax8 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax8,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax8,'Xtick',[]);
set(ax8,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

ax9 = axes('Position',[dim.x3,dim.y3,dim.cbar/dim.W,dim.ymap]);
set(ax9,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax9,'Xtick',[]);
set(ax9,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

% colorbar units

ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y3 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

print -depsc2 b2p_plots/tqu_maps_bk.eps
fix_lines('b2p_plots/tqu_maps_bk.eps')
% epstopdf does unstable image compression - convert directly to png to avoid
system_safe('convert -density 300 -colorspace rgb b2p_plots/tqu_maps_bk.eps b2p_plots/tqu_maps_bk.png')

return

%%%%%%%%%%%%%%%%%%%%%
function make_maps_p353(i)
% plot the P353 TQU maps with: i=1 nothing, i=2 ap, i=3 ap+filt

% get observed map
%load maps/1613/real_a_filtp3_weight3_gs_dp1100_jack0.mat
load maps/1457/real_aab_filtp3_weight3_gs_dp1100_jack0.mat
map=make_map(ac,m,coaddopt);
%map=cal_coadd_maps(map,get_ukpervolt);
% use 353GHz map
mapo=map(end);

% make the apodization masks
maskt=1./mapo.Tvar; maskt=maskt./prctile(maskt(:),97);
maskp=1./((mapo.Qvar+mapo.Uvar)/2);
maskp=maskp./prctile(maskp(:),97);

% apply mask
mapo.T=mapo.T.*maskt; mapo.Q=mapo.Q.*maskp; mapo.U=mapo.U.*maskp; 

% extract Planck map in our field
map=healpix_to_longlat(coaddopt.reob_map_fname{7},m.x_tic,m.y_tic);
mapr.T=map(:,:,1); mapr.Q=map(:,:,2); mapr.U=map(:,:,3);

% copy before further processing
mapi=mapr;

% mask to non nan region
mapi.T(isnan(mapo.T))=NaN; mapi.Q(isnan(mapo.T))=NaN; mapi.U(isnan(mapo.T))=NaN;

% remove the weighted mean in the obs region
meant=nansum(rvec(mapi.T.*maskt))/nansum(maskt(:));
mapi.T=mapi.T-meant;
meanq=nansum(rvec(mapi.Q.*maskp))/nansum(maskp(:));
mapi.Q=mapi.Q-meanq;
meanu=nansum(rvec(mapi.U.*maskp))/nansum(maskp(:));
mapi.U=mapi.U-meanu;

% apply mask
mapi.T=mapi.T.*maskt; mapi.Q=mapi.Q.*maskp; mapi.U=mapi.U.*maskp; 

% caxis range
switch i
 case 1
  map=mapr;
  tr=round(([-150,150]+meant)/5)*5;
  qr=round(([-40,40]+meanq)/5)*5; ur=round(([-40,40]+meanu)/5)*5;
 case 2
  map=mapi;
  tr=[-150,150]; qr=[-40,40]; ur=[-40,40];
 case 3
  map=mapo;
  tr=[-150,150]; qr=[-40,40]; ur=[-40,40];
end

fname=sprintf('b2p_plots/tqu_maps_p353_%1d',i);
plot_tqu(m,map,tr,qr,ur,fname);

return

%%%%%%%%%%%%%%%%%%%%%
function make_maps_bkvp(rs)

% make a 6 panel plot of BK apodized vs P353 filtered/apodized
% the Keck paper

% get the BK map
switch rs
 case 1
  load maps/1456/real_aab_filtp3_weight3_gs_dp1102_jack0
  map=make_map(ac,m,coaddopt);
  mapc=map;
  % get P353 map
  load maps/1457/real_aab_filtp3_weight3_gs_dp1100_jack0
  map=make_map(ac,m,coaddopt);
  % use 353GHz map
  mapp=map(end);
  fname1='b2p_plots/tqu_maps_bkvp_1';
  fname2='b2p_plots/tqu_maps_bkvp_2';
 case 2
  load maps/1456/0026_aab_filtp3_weight3_gs_dp1100_jack0
  map=make_map(ac,m,coaddopt);
  mapc=map;
  % get P353 map
  load maps/1457/0026_aab_filtp3_weight3_gs_dp1100_jack0
  map=make_map(ac,m,coaddopt);
  % use 353GHz map
  mapp=map(end);
  fname1='b2p_plots/tqu_maps_bkvp_1s';
  fname2='b2p_plots/tqu_maps_bkvp_2s';
end

% make the apodization masks
maskt=1./mapc.Tvar; maskt=maskt./prctile(maskt(:),97);
maskp=1./((mapc.Qvar+mapc.Uvar)/2);
maskp=maskp./prctile(maskp(:),97);

mapc.T=mapc.T.*maskt; mapc.Q=mapc.Q.*maskp; mapc.U=mapc.U.*maskp; 
mapp.T=mapp.T.*maskt; mapp.Q=mapp.Q.*maskp; mapp.U=mapp.U.*maskp; 

plot_tqu(m,mapc,[-150,150],[-4,4],[-4,4],fname1);
drawnow
plot_tqu(m,mapp,[-150,150],[-40,40],[-40,40],fname2);

return

%%%%%%%%%%%%%%%%%%%%%
function plot_tqu(m,map,tr,qr,ur,fname)

figure(1); clf;
dim=get_3x1dims(m);
set(gcf, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% T

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,map.T,'iau');
caxis(tr);
ta=caxis;
colormap hot
freezeColors
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
title('T');

% Q

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,map.Q,'iau');
caxis(qr);
qa=caxis;
colormap gray
freezeColors
set(ax3,'FontSize',7,'FontWeight','normal');
set(ax3,'Xticklabel',[]);
title('Q');

% U

ax5=axes('Position',[dim.x1,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,map.U,'iau');
caxis(ur);
ua=caxis;
colormap gray
freezeColors
set(ax5,'FontSize',7,'FontWeight','normal');
title('U');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% colorbars

ax7 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax7,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(ta(1),ta(2),1000),repmat(linspace(ta(1),ta(2),1000)',1,10));
colormap hot;
freezeColors;
set(ax7,'Xtick',[]);
set(ax7,'YDir','normal','YAxisLocation','right','YTick',[ta(1) mean(ta) ta(2)]);

ax8 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax8,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(qa(1),qa(2),1000),repmat(linspace(qa(1),qa(2),1000)',1,10));
colormap gray;
freezeColors;
set(ax8,'Xtick',[]);
set(ax8,'YDir','normal','YAxisLocation','right','YTick',[qa(1) mean(qa) qa(2)]);

ax9 = axes('Position',[dim.x3,dim.y3,dim.cbar/dim.W,dim.ymap]);
set(ax9,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(ua(1),ua(2),1000),repmat(linspace(ua(1),ua(2),1000)',1,10));
colormap gray;
freezeColors;
set(ax9,'Xtick',[]);
set(ax9,'YDir','normal','YAxisLocation','right','YTick',[ua(1) mean(ua) ua(2)]);

% colorbar units

ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', 7, 'FontWeight', 'normal');
text(0.6, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.6, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.6, dim.y3 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% matlab seems to behave strangely here - whole image is bitmap
% rather than just the maps themselves...
print('-depsc2',sprintf('%s.eps',fname));
fix_lines(sprintf('%s.eps',fname));
% epstopdf does unstable image compression - convert directly to png to avoid
system_safe(sprintf('convert -density 300 -colorspace rgb %s.eps %s.png',fname,fname));

return

function dummy

figure(1); clf;
dim=get_3x2dims(m);
set(gcf, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

% T

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapc.T.*maskt,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
title('T');

ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapp.T.*maskp,'iau');
caxis([-150,150]);
colormap hot
freezeColors
set(ax2,'FontSize',7,'FontWeight','normal');
set(ax2,'Xticklabel',[],'Yticklabel',[]);
title('T');

% Q

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapc.Q.*maskp,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax3,'FontSize',7,'FontWeight','normal');
set(ax3,'Xticklabel',[]);
title('Q');

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapp.Q.*maskp,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax4,'FontSize',7,'FontWeight','normal');
set(ax4,'Xticklabel',[],'Yticklabel',[]);
title('Q');

% U

ax5=axes('Position',[dim.x1,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,mapc.U.*maskp,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax5,'FontSize',7,'FontWeight','normal');
title('U');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

ax6=axes('Position',[dim.x2,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,mapp.U.*maskp,'iau');
caxis([-4,4]);
colormap gray
freezeColors
set(ax6,'FontSize',7,'FontWeight','normal');
set(ax6,'Yticklabel',[]);
title('U');

% colorbars

ax7 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax7,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-150,150,1000),repmat(linspace(-150,150,1000)',1,10));
colormap hot;
freezeColors;
set(ax7,'Xtick',[]);
set(ax7,'YDir','normal','YAxisLocation','right','YTick',[-100 0 100]);

ax8 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax8,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax8,'Xtick',[]);
set(ax8,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

ax9 = axes('Position',[dim.x3,dim.y3,dim.cbar/dim.W,dim.ymap]);
set(ax9,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-4,4,1000),repmat(linspace(-4,4,1000)',1,10));
colormap gray;
freezeColors;
set(ax9,'Xtick',[]);
set(ax9,'YDir','normal','YAxisLocation','right','YTick',[-3 0 3]);

% colorbar units

ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y3 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

print -depsc2 b2p_plots/tqu_maps_bkvp.eps
fix_lines('b2p_plots/tqu_maps_bkvp.eps')
% epstopdf does unstable image compression - convert directly to png to avoid
system_safe('convert -density 300 -colorspace rgb b2p_plots/tqu_maps_bkvp.eps b2p_plots/tqu_maps_bkvp.png')

return

%%%%%%%%%%%%%%%%%%%%%
function dim=get_3x2dims(m)
% Parameters for 3 high 2 wide layout
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 3 * dim.maph + dim.wide + 3 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 2 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 2 * dim.maph + 2 * dim.med) / dim.H; % Bottom edge of row 1.
dim.y2 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y3 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

%%%%%%%%%%%%%%%%%%%%%
function dim=get_3x1dims(m)
% Parameters for 3 high 1 wide layout
dim.W = 4; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 1 * dim.thin - dim.cbar); % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 3 * dim.maph + dim.wide + 3 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
%dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 1 * dim.mapw + 1 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 2 * dim.maph + 2 * dim.med) / dim.H; % Bottom edge of row 1.
dim.y2 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y3 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_specplot1_zoom

for i=1:9
  chibins{i}=[2:10]; % use all plotted bins
end

% For TT get errorbars from legacy unconstrained sims - we will
% also use these for B2xP and PxP errorbars since noise is negligible
load final/1450x1350/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1102_jack01_pureB_overrx
r=r(1); r=get_bpcov(r);
rt=r;

% get the B2xP full points
% NB: the B2xB2 ones in here lack the residual beam correction and
% hence are not quite the same as the PRL ones in the highest 3
% bandpower plotted
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rf=r;
im=inpmod;

% So get the B2xB2 exactly as plotted in the PRL
load final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf_rbc.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rb2prl=r;

% get the KxP full points
load final/1351x1614/real_ab_filtp3_weight3_gs_dp1102_jack0_real_ab_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rk=r;

% get the CxP full points
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rc=r;

ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');

% the weighted bin centers
lc=r(1).lc; lc(1,:)=NaN;

% get the PxP splitxsplit points
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack3_real_a_filtp3_weight3_gs_dp1100_jack3_pureB_matrix_nonej_directbpwf.mat
r=get_bpcov(r);
rs1=r;
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack4_real_a_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_nonej_directbpwf.mat
r=get_bpcov(r);
rs2=r;
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack5_real_a_filtp3_weight3_gs_dp1100_jack5_pureB_matrix_nonej_directbpwf.mat
r=get_bpcov(r);
rs3=r;

% strip down to the elements we need
i=aps_getxind(length(rf),1,8);
rf=rf([1,i]);
rk=rk([1,i]);
rc=rc([1,i]);
rs1=rs1(end);
rs2=rs2(end);
rs3=rs3(end);

% calc the chi2 for 9 bandpowers
for i=1:9
  chibins{i}=[2:10]; % use all plotted bins
end
rc=calc_chi(rc,chibins,[],1); rc=calc_devs(rc,chibins,1);
rs2=calc_chi(rs2,chibins,[],1); rs2=calc_devs(rs2,chibins,1);

clf; setwinsize(gcf,1000,400);
ms=5; % marker size
ms2=10;
f=0.5;

subplot(1,2,1)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,ml.Cs_l(:,4),'r');
%h1=errorbar2(lc(:,4)-5,rf(1).real(:,4),rf(1).derr(:,4),'b*');
% plot points exactly as in PRL - for ell<200 nearly identical
h1=errorbar2(lc(:,4)-5,rb2prl.real(:,4),rb2prl.derr(:,4),'b*');
h2=errorbar2(lc(:,4)+5,rk(1).real(:,4),rk(1).derr(:,4),'rx');
h3=errorbar2(lc(:,4)+0,rc(1).real(:,4),rc(1).derr(:,4),'k.');
set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
set(h3(2),'MarkerSize',ms2);
set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
title('150x150'); xlabel('Multipole');
xlim([0,330]); ylim([-0.03,0.04]);
ylabel('BB l(l+1)C_l/2\pi [\muK^2]')
yl=ylim;
legend([h3(2),h1(2),h2(2)],{'BKxBK','BxB','KxK'},'Location','NW');
legend boxoff
ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rc(1).rchisq(4),rc(1).rsumdev(4)),0.03,0.1)

sf=24.63; % from Ganga post 8/14

subplot(1,2,2)
line([0,330],[0,0],'LineStyle',':','color','k'); box on
hold on
plot(im.l,ml.Cs_l(:,4),'r--');
h1=errorbar2(lc(:,4)-5,rf(2).real(:,4),rf(2).derr(:,4),'b*');
h2=errorbar2(lc(:,4)+5,rk(2).real(:,4),rk(2).derr(:,4),'rx');
h3=errorbar2(lc(:,4)+0,rc(2).real(:,4),rc(2).derr(:,4),'k.');
set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
set(h3(2),'MarkerSize',ms2);
set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
hold off
title('150x353'); xlabel('Multipole');
xlim([0,330]); ylim(yl*sf); box off
legend([h3(2),h1(2),h2(2)],{'BKxP353','BxP353','KxP353'},'Location','NW');
legend boxoff
axes('Position',get(gca,'Position'),'XAxisLocation','top','YAxisLocation','right','Color','none');
ylim(yl);
y=ylabel('scaled'); set(y,'position',get(y,'position')-[0.07,0,0]);
set(gca,'XTick',[])
ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rc(2).rchisq(4),rc(2).rsumdev(4)),0.03,0.1)

print -depsc2 b2p_plots/spectra_bb.eps
fix_lines('b2p_plots/spectra_bb.eps')
!convert -density 300 b2p_plots/spectra_bb.eps b2p_plots/spectra_bb.png

keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_specplot2(eebb)

% get the B2xP points
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rf=r;
im=inpmod;

% Get KxP points
load final/1351x1614/real_ab_filtp3_weight3_gs_dp1102_jack0_real_ab_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rk=r;

% get the CxP points
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat
r=get_bpcov(r); r=weighted_ellbins(r,bpwf);
rc=r;

ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');

% strip down to used spectra
n=[9,10,11,12,13,1,14,15];
rf=rf(n); rk=rk(n); rc=rc(n);

% calc the chi2 for 5 bandpowers
for i=1:9
  chibins{i}=[2:6]; % use the plotted bins
end
rc=calc_chi(rc,chibins,[],1);
rc=calc_devs(rc,chibins,1);

% the weighted bin centers
lc=r(1).lc; lc(1,:)=NaN;

figure(1); clf; setwinsize(gcf,1200,550);

ms=5; % marker size
ms2=10;
f=0.5;

f1={'P30','P44','P70','P100','P143','BK150','P217','P353'};
byl=[-0.1,0.2;-0.1,0.2;-0.1,0.2;-0.01,0.05;-0.01,0.05;-0.01,0.05;-0.02,0.3;-0.02,0.3];
for i=1:8
  switch eebb
   case 1
    subplot(2,4,i);
    line([0,330],[0,0],'LineStyle',':','color','k'); box on
    hold on
    plot(im.l,im.Cs_l(:,3),'r');
    h1=errorbar2(lc(:,3)-5,rf(i).real(:,3),rf(i).derr(:,3),'b*');
    h2=errorbar2(lc(:,3)+5,rk(i).real(:,3),rk(i).derr(:,3),'rx');
    h3=errorbar2(lc(:,3)+0,rc(i).real(:,3),rc(i).derr(:,3),'k.');
    set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
    set(h3(2),'MarkerSize',ms2);
    set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
    set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
    hold off
    xlim([0,200]); ylim([-0.1,2]); axis square
    title(sprintf('BK150x%s',f1{i}));
    ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rc(i).rchisq(3),rc(i).rsumdev(3)))
    if(i==1)
      [l,h]=legend([h3(2),h1(2),h2(2)],{'BKxP','BxP','KxP'},'Location','W');
      legend boxoff
      set(l,'Position',[0.1239 0.7540 0.0673 0.1187]);
      p=get(h(1),'position'); p(1)=p(1)-.13; set(h(1),'position',p);
      p=get(h(2),'position'); p(1)=p(1)-.13; set(h(2),'position',p);
      p=get(h(3),'position'); p(1)=p(1)-.13; set(h(3),'position',p);   
    end
    if(any(i==[1,5]))
      h=ylabel('EE l(l+1)C_l/2\pi [\muK^2]');
      p=get(h,'position'); p(1)=-40; set(h,'position',p);
    end
   case 2
    subplot(2,4,i);
    line([0,330],[0,0],'LineStyle',':','color','k'); box on
    hold on
    plot(im.l,ml.Cs_l(:,4),'r');
    h1=errorbar2(lc(:,4)-5,rf(i).real(:,4),rf(i).derr(:,4),'b*');
    h2=errorbar2(lc(:,4)+5,rk(i).real(:,4),rk(i).derr(:,4),'rx');
    h3=errorbar2(lc(:,4)+0,rc(i).real(:,4),rc(i).derr(:,4),'k.');
    set(h1(2),'MarkerSize',ms); set(h2(2),'MarkerSize',ms);
    set(h3(2),'MarkerSize',ms2);
    set(h1(2),'Color',[f,f,1]); set(h1(1),'Color',[f,f,1]);
    set(h2(2),'Color',[1,f,f]); set(h2(1),'Color',[1,f,f]);
    hold off
    xlim([0,200]);
    ylim(byl(i,:)); axis square
    title(sprintf('BK150x%s',f1{i}));
    ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',rc(i).rchisq(4),rc(i).rsumdev(4)))
    if(i==1)
      [l,h]=legend([h3(2),h1(2),h2(2)],{'BKxP','BxP','KxP'},'Location','W');
      legend boxoff
      set(l,'Position',[0.1239 0.7540 0.0673 0.1187]);
      p=get(h(1),'position'); p(1)=p(1)-.13; set(h(1),'position',p);
      p=get(h(2),'position'); p(1)=p(1)-.13; set(h(2),'position',p);
      p=get(h(3),'position'); p(1)=p(1)-.13; set(h(3),'position',p);   
    end
    if(any(i==[1,5]))
      h=ylabel('BB l(l+1)C_l/2\pi [\muK^2]');
      p=get(h,'position'); p(1)=-40; set(h,'position',p);
    end
  end
end
xlabel('Multipole')

switch eebb
 case 1
  print -depsc2 b2p_plots/spectra2ee.eps
  fix_lines('b2p_plots/spectra2ee.eps')
  !convert -density 300 b2p_plots/spectra2ee.eps b2p_plots/spectra2ee.png
 case 2
  print -depsc2 b2p_plots/spectra2bb.eps
  fix_lines('b2p_plots/spectra2bb.eps')
  !convert -density 300 b2p_plots/spectra2bb.eps b2p_plots/spectra2bb.png
end

return

%%%%%%%%%%%%%%%
function make_multifid(sw)

% run the analysis
switch sw
 case 1
  % fiducial
  [lo,l]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,0,1);
  make_multifid_plot(sw,lo,l);
 case 2
  % inc EE
  [lo,l]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,2,1);
  make_multifid_plot(sw,lo,l);
 case 3
  % inc 30 while force sync to upper limit val
  [lo,l]=dynchi2_grid_analysis(5,2:6,1,[],1,33,1,0,1,0,[0,0.0003,0.00031],0);
  make_multifid_plot(sw,lo,l);
 case 4
  % same but also inc EE
  [lo,l]=dynchi2_grid_analysis(5,2:6,1,[],1,33,1,2,1,0,[0,0.0003,0.00031],0);
  make_multifid_plot(sw,lo,l);
 case 5
  % inc 30 while force sync to upper limit val
  [lo,l]=dynchi2_grid_analysis(5,2:6,1,[],1,33,1,0,1,0,[0,0.0003,0.00031],0);
  % now force dust/sync corr to 1 and compute model comps
  lo.mlmod(11)=1; lo=compute_modelcomps(lo,lo.mlmod);
  % plot it and record yaxes (this one needs biggest ranges)
  yl=make_multifid_plot(6,lo,l);
  % dust/sync corr back to 0 and plot
  lo.mlmod(11)=0; lo=compute_modelcomps(lo,lo.mlmod);
  make_multifid_plot(5,lo,l,yl);
  % corr and flat spec
  lo.mlmod(11)=1; lo.mlmod(5)=-2.5; lo=compute_modelcomps(lo,lo.mlmod);
  make_multifid_plot(7,lo,l,yl);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ylo=make_multifid_plot(sw,lo,l,yli)

n=length(lo.expt);
lo.expt(1).name='BK';
A_s=lo.mlmod(3)>0;

figure(1); clf; setwinsize(gcf,900,900);

% write params of ML model
switch length(lo.fields)
 case 1
  subplot(n,n,n);
 case 2
  subplot(n+1,n+1,1);
end
title('Model:')
box on
text(0.1,0.8,sprintf('r=%.2f',lo.mlmod(1)),'fontsize',14);
text(0.1,0.65,sprintf('A_d=%.1f, \\beta_d=%.2f',lo.mlmod([4,7])),'fontsize',14);
if(A_s)
  text(0.1,0.50,sprintf('A_s=%.4f, \\beta_s=%.1f',lo.mlmod([3,5])),'fontsize',14);
  text(0.1,0.35,sprintf('s/d corr=%.1f',lo.mlmod(11)),'fontsize',14);
end
text(0.1,0.20,sprintf('\\chi^2/dof=%.1f/%d',lo.chi2,lo.dof),'fontsize',14);
h=text(0.1,0.05,sprintf('PTE=%.2f',lo.pte),'fontsize',14);
set(gca, 'XTick', []); set(gca, 'YTick', []);

incee=length(lo.fields)==2;

% plot each spectrum
for f=1:length(lo.fields) 
  for i=1:n
    for j=i:n
      if(~incee)
        subplot(n,n,i+(j-1)*n);
      else
        switch f
         case 1
          subplot(n+1,n+1,j+1+(i-1)*(n+1));
         case 2
          subplot(n+1,n+1,i+(j)*(n+1));
        end
      end        
      k=likeopt_getxind(lo,i,j,f,f);
      %[i j k]
      d=lo.real(:,k);
      m=lo.expv(:,k);
      e=lo.e(:,k);
      ell=lo.lval;
      hold on; box on
      if(incee&f==1)
        % EE
        plot(ell,lo.expv(:,k)-lo.expvs(:,k),'m');
        plot(ell,lo.expvl(:,k),'r');
        if(A_s)
          % sync alone
          plot(ell,lo.expvs(:,k)-lo.expvl(:,k),'c');
          % boost due to sync/dust corr
          if(lo.mlmod(11)>0)
            plot(ell,lo.expvsd(:,k)-lo.expvs(:,k)-lo.expvd(:,k)+lo.expvl(:,k),'k');
          end
        end
        title(sprintf('%sx%s EE',lo.expt(i).name,lo.expt(j).name));
      else
        % BB
        h4=plot(ell,lo.expvd(:,k),'m');
        h3=plot(ell,lo.expvr(:,k),'g');
        h2=plot(ell,lo.expvl(:,k),'r');
        if(A_s)
          h5=plot(ell,lo.expvs(:,k),'c');
          if(lo.mlmod(11)>0)
            h6=plot(ell,lo.expvsd(:,k)-lo.expvd(:,k)-lo.expvs(:,k),'k');
          end
        end
        title(sprintf('%sx%s BB',lo.expt(i).name,lo.expt(j).name));
      end
      h1=plot(ell,m,'b--');
      plot(ell,m+e,'b:'); plot(ell,m-e,'b:');
      h0=plot(ell,d,'k*');%,'MarkerSize',20);
      hold off
      if(i==1&(~incee|f==2))
        ylabel('l(l+1)C_l/2\pi [\muK^2]');
      end
      if(j==n&(~incee|f==2))
        xlabel('Multipole');
      end
      axis tight; %xlim([ell(1)-20,ell(end)+20]);
      axis square
      xlim([0,200]);
      line(xlim,[0,0],'LineStyle',':','color','k');
      yl=ylim;
      if(yl(1)>=0)
        yl(1)=-yl(2)*0.05;
        yl(2)=yl(2)*1.05;
      else
        dy=diff(yl)*0.05;
        yl(1)=yl(1)-dy; yl(2)=yl(2)+dy;
      end
      ylim(yl);
      ylo(f,i,j,:)=yl; % record y axis limits
      if(exist('yli'))
        ylim(yli(f,i,j,:)); % restore y axis limits
      end
      ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',...
                     sum(((d-m)./e).^2),sum(((d-m)./e))));
    end
  end
end

if(~A_s)
  h=legend([h0,h1,h2,h3,h4],{'Real data','Model total w. 1\sigma range','Lensed-LCDM','tensors (r)','dust'},'location','bestoutside');
else
  h=legend([h0,h1,h2,h3,h4,h5],{'Real data','Model total w. 1\sigma range','Lensed-LCDM','tensors (r)','dust','sync'},'location','bestoutside');
  if(lo.mlmod(11)>0)
    h=legend([h0,h1,h2,h3,h4,h5,h6],{'Real data','Model total w. 1\sigma range','Lensed-LCDM','tensors (r)','dust','sync','sync&dust-sync-dust'},'location','bestoutside');
  end
end
  
if(~incee)
  if(sw==1)
    set(h,'position',[0.6603,0.4273,0.2462,0.1792]);
  else
    set(h,'position',[0.6536,0.5406,0.2462,0.1792]);
  end
end

print('-depsc2',sprintf('b2p_plots/multifid_%d.eps',sw))
fix_lines(sprintf('b2p_plots/multifid_%d.eps',sw))
system(sprintf('convert -density 300 b2p_plots/multifid_%d.eps b2p_plots/multifid_%d.png',sw,sw));

return

%%%%%%%%%%%%%%%
function make_decorrplot

% Load B2

% 1353 -> Keck
% 1614 -> Keck reobs Planck

% 0751 -> B2
% 1613 -> B2 reobs Planck

% jack 3 -> year split
% jack 4 -> det set split
% jack 5 -> half ring split

% 1 -> B2

% 2 -> Pl 30
% 3 -> Pl 44
% 4 -> Pl 70
% 5 -> Pl 100
% 6 -> Pl 143
% 7 -> Pl 217
% 8 -> Pl 353

% 9  -> Pl 30a
% 10 -> Pl 30b

% 11 -> Pl 44a
% 12 -> Pl 44b

% 13 -> Pl 70a
% 14 -> Pl 70b

% 15 -> Pl 100a
% 16 -> Pl 100b

% 17 -> Pl 143a
% 18 -> Pl 143b

% 19 -> Pl 217a
% 20 -> Pl 217b

% 21 -> Pl 353a
% 22 -> Pl 353b

% Load year split final file
doload = 1;

if doload
  %% B2
  %load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1100_jack0_real_a_filtp3_weight3_gs_dp1100_jack3_pureB_matrix_directbpwf

  % B2K
  load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack3_pureB_matrix_directbpwf

  % Input model
  inp=load_cmbfast('aux_data/official_cl/camb_planck2013_r0.fits');
  
end

% Get indices
clear i
i(1)=1; % B2/K/B2K
i(2)=6; % Planck 143 FFP
i(3)=aps_getxind(r,i(1),i(2)); % B2xPlanck 143 FFP
i(4)=aps_getxind(r,17,18); % Planck 143 halfa x Planck 143 halfb

% Plot spectra with noise error bars
l=r(1).l(2:end);
sym={'.k','.b','.m','.r'};
dl=[0,-2,2,-4,4];
xl=[0,325];

spec=[1,3];
tit={'l(l+1)C_l/2\pi [\muK^2]','l(l+1)C_l/2\pi [\muK^2]'};

figure(1)
clf
setwinsize(gcf,700,400)

for j=1:2 % TT,EE

  subplot_grid(2,2,j*2-1,0,1);
  specind=spec(j);
  titlab=tit{j};
  
  clear h
  kk=1;


  plot(inp.l,inp.Cs_l(:,specind),'r','LineWidth',1);
  hold on
  
  for k=[1,4,3] % B2,P,B2xP
    x=l+dl(k);
    y=r(i(k)).real(2:end,specind);
    err=std(r(i(k)).noi(2:end,specind,:),[],3);

    h=errorbar2(x,y,err,sym{k});
    set(h(2),'MarkerSize',8);
    set(gca,'FontSize',12);
    set(gca,'LineWidth',1);
    hh(kk)=h(2);
    kk=kk+1;
    hold on;
  end
  plot([0,500],[0,0],':k');
  hold off;
  ylabel(titlab);
  xlim(xl);
  xlabel('Multipole');
  
  if j==1
    ylim([-1000,7000]);
    h=legend(hh,'BKxBK','PxP','BKxP','Location','NorthWest');
    set(h,'FontSize',10);
    pos=get(h,'Position');
    pos(1)=pos(1)-.03;
    pos(2)=pos(2)+.01;
    set(h,'Position',pos);
    legend boxoff
  end

  
  subplot_grid2(2,2,j*2-1,0,1);
  
  % Decorr plot
  subplot_grid(2,2,j*2,0,1);

  b2xb2r=r(i(1)).real(2:end,specind);
  plxplr=r(i(4)).real(2:end,specind);
  b2xplr=r(i(3)).real(2:end,specind);
  
  b2xb2sim=r(i(1)).sim(2:end,specind,:);
  plxplsim=r(i(4)).sim(2:end,specind,:);
  b2xplsim=r(i(3)).sim(2:end,specind,:);

  yr=b2xplr./sqrt(abs(plxplr.*b2xb2r));
  ysim=b2xplsim./sqrt(abs(plxplsim.*b2xb2sim));
  yerr=std(ysim,[],3);
  h=errorbar2(x,yr,yerr,'.k');
  set(gca,'FontSize',12);
  set(gca,'LineWidth',1);

  set(h(2),'MarkerSize',8);
  xlim([0,500]);
  hold on
  plot([0,500],[1,1],':k');
  hold off

  if j==1
    ylim([.95,1.05]);
    h=legend(h(2),'(BKxP) / [(BKxBK)(PxP)]^{(1/2)}','Location','NorthWest');
    set(h,'FontSize',10);
    pos=get(h,'Position');
    pos(1)=pos(1)-.03;
    pos(2)=pos(2)+.01;
    set(h,'Position',pos);
    legend boxoff
  else
    ylim([0,2]);
  end
  xlabel('Multipole');

  ybar=yr(l<500).*(1./yerr(l<500).^2)/sum(1./yerr(l<500).^2);
  
  
  yy=yr(l<500);
  yw=1./yerr(l<500).^2;
  ybar=sum(yy.*yw)/sum(yw);
  ysig=sqrt(1/sum(yw));
  
  disp(sprintf('weighted mean, %0.5f +/- %0.5f',ybar,ysig));
  
  subplot_grid2(2,2,j*2,0,1);  
  
end


set(gcf,'PaperPositionMode','auto')
print -depsc2 b2p_plots/b2p_decorr.eps
fix_lines('b2p_plots/b2p_decorr.eps')
!epstopdf b2p_plots/b2p_decorr.eps


return

%%%%%%%%%%
function make_synthmap

bk=load('maps/1456/real_aab_filtp3_weight3_gs_dp1102_jack0.mat');
p=load('maps/1457/real_aab_filtp3_weight3_gs_dp1100_jack0.mat');
m=bk.m;
mapbk=make_map(bk.ac,m);
mapp=make_map(cal_coadd_ac(p.ac(7),0.0394589677),m);
projfile='/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat';
[dum,mapbk]=do_projection(mapbk,projfile);
mapbk.Q=full(mapbk.Q);
mapbk.U=full(mapbk.U);

ez_i2f=@(z)fftshift(ifft2(fftshift(z)));
ez_f2i=@(z)ifftshift(fft2(ifftshift(z)));
ppart=@(a,b,c,d)0.5*(sign(a)+sign(c)).*sqrt(abs(a.*c))+1i*0.5*(sign(b)+sign(d)).*sqrt(abs(b.*d));
npart=@(a,b,c,d)0.5*(sign(a)-sign(c)).*sqrt(abs(a.*c))+1i*0.5*(sign(b)-sign(d)).*sqrt(abs(b.*d));

% fake map structure to pass to make_ebmap
map_corr.Qvar=sqrt((mapbk.Qvar+mapbk.Uvar).*(mapp.Qvar+mapp.Uvar))/2;
map_corr.Uvar=sqrt((mapbk.Qvar+mapbk.Uvar).*(mapp.Qvar+mapp.Uvar))/2;
map_anti.Qvar=sqrt((mapbk.Qvar+mapbk.Uvar).*(mapp.Qvar+mapp.Uvar))/2;
map_anti.Uvar=sqrt((mapbk.Qvar+mapbk.Uvar).*(mapp.Qvar+mapp.Uvar))/2;

x=mapbk.Q./(mapbk.Qvar+mapbk.Uvar); x(isnan(x))=0;
y=mapp.Q./(mapp.Qvar+mapp.Uvar); y(isnan(y))=0;
xr=real(ez_i2f(x)); xi=imag(ez_i2f(x)); yr=real(ez_i2f(y)); yi=imag(ez_i2f(y));
map_corr.Q=real(ez_f2i(ppart(xr,xi,yr,yi))).*sqrt((mapbk.Qvar+mapbk.Uvar).*(mapp.Qvar+mapp.Uvar));
map_anti.Q=real(ez_f2i(npart(xr,xi,yr,yi))).*sqrt((mapbk.Qvar+mapbk.Uvar).*(mapp.Qvar+mapp.Uvar));

x=mapbk.U./(mapbk.Qvar+mapbk.Uvar); x(isnan(x))=0;
y=mapp.U./(mapp.Qvar+mapp.Uvar); y(isnan(y))=0;
xr=real(ez_i2f(x)); xi=imag(ez_i2f(x)); yr=real(ez_i2f(y)); yi=imag(ez_i2f(y));
map_corr.U=real(ez_f2i(ppart(xr,xi,yr,yi))).*sqrt((mapbk.Qvar+mapbk.Uvar).*(mapp.Qvar+mapp.Uvar));
map_anti.U=real(ez_f2i(npart(xr,xi,yr,yi))).*sqrt((mapbk.Qvar+mapbk.Uvar).*(mapp.Qvar+mapp.Uvar));

map_corr=make_ebmap(m,map_corr,'normal',[50,120]);
map_anti=make_ebmap(m,map_anti,'normal',[50,120]);

% plot

dim=get_twopaneldims(m);

fig=figure;
clf; 
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

fs=8;
cb=2.4/6.4;

% first correlated B
ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,map_corr.B,'iau');
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
set(ax1,'FontSize',fs,'FontWeight','normal');
set(ax1,'Xticklabel',[],'Xtick',[-50,0,50]);
title(['BK x Planck 353 correlated B'],'FontSize',fs);

% second anticorrelated B
ax4=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,map_anti.B,'iau');
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
set(ax4,'FontSize',fs,'FontWeight','normal');
set(ax4,'Xtick',[-50,0,50]);
title(['BK x Planck 353 anticorrelated B'],'FontSize',fs);
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% colorbar
ax2 = axes('Position',[dim.x2,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax2,'FontSize',fs,'FontWeight','normal');
imagesc(1:10,linspace(-cb,cb,1000),repmat(linspace(-cb,cb,1000)',1,10));
colormap(colormap_lint)
freezeColors;
set(ax2,'Xtick',[]);
set(ax2,'YDir','normal','YAxisLocation','right','YTick',[-0.3 0 0.3]);

ax5 = axes('Position',[dim.x2,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax5,'FontSize',fs,'FontWeight','normal');
imagesc(1:10,linspace(-cb,cb,1000),repmat(linspace(-cb,cb,1000)',1,10));
colormap(colormap_lint)
freezeColors;
set(ax5,'Xtick',[]);
set(ax5,'YDir','normal','YAxisLocation','right','YTick',[-0.3 0 0.3]);

% colorbar units
ax3 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax3, 'FontSize', 7, 'FontWeight', 'normal');
text(-0.1, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 10, ...
    'FontWeight', 'normal', 'HorizontalAlignment', 'center');

ax6 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax6, 'FontSize', 7, 'FontWeight', 'normal');
text(-0.1, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 10, ...
    'FontWeight', 'normal', 'HorizontalAlignment', 'center');

set(gcf,'PaperPositionMode','auto')
print -depsc2 b2p_plots/b2p_synthmaps.eps
fix_lines('b2p_plots/b2p_synthmaps.eps')
!epstopdf b2p_plots/b2p_synthmaps.eps 

return 

%%%%%%%%%%%%%%%%%%%%%%
function make_EBcomp()
keyboard
% v1 = 0 : all the same color stretch
% v1 = 1 : individual color stretches
% v1 = 2 : individual color stretches + noise realization
v1 = 2;

pm = '/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat';
projfile = load(pm);
projfile2 = {projfile};

load maps/1456/real_aab_filtp3_weight3_gs_dp1100_jack0.mat
mapBK=make_map(ac,m,coaddopt);

load maps/1457/real_aab_filtp3_weight3_gs_dp1100_jack0.mat
mapP=make_map(ac,m,coaddopt);

mapP=mapP(end);

ellrng=[50,120];
mapBK=make_ebmap(m,mapBK,[],ellrng,projfile2);
mapP=make_ebmap(m,mapP,[],ellrng,projfile2);

% vector length scale factor
sfe = nanstd(mapBK.E(:))/nanstd(mapP.E(:));
ve=0.6;
vep = ve/nanstd(mapP.E(:))*nanstd(mapBK.E(:));

mapPr = mapP;
mapBKr = mapBK;

if v1==2
  load maps/1456/0026_aab_filtp3_weight3_gs_dp1100_jack0.mat
  mapBK=make_map(ac,m,coaddopt);

  load maps/1457/0026_aab_filtp3_weight3_gs_dp1100_jack0.mat
  mapP=make_map(ac,m,coaddopt);
  
  mapP=mapP(end);
  
  mapBK=make_ebmap(m,mapBK,[],ellrng,projfile2);
  mapP=make_ebmap(m,mapP,[],ellrng,projfile2);
  
  

end

dim=get_twopaneldims(m);
set(0,'DefaultTextFontSize',6);
set(0,'defaultlinelinewidth',0.5);
fig=figure(1);
clf;
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

sl=1;
% color range
ce=2.4;
cb=ce/6.4;


vbp = vep;
vb=ve*6.4;
ctickse = [-1.8 0 1.8]
cticksb = [-0.3 0 0.3]
cticksp = [-11 0 11]

if (v1==1)
  cb=ce;
  vb=ve;
  sl=0;
  sfe=1;
  cticksb=ctickse;
  cticksp=ctickse;
end

% E BK
ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapBK.E,'iau');
caxis([-ce,ce]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,mapBK.EQ,mapBK.EU,mapBK.Qvar,mapBK.Uvar,ve,[],3,sl)
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[]);
title('BK E');

% B BK
ax2=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapBK.B,'iau');
caxis([-cb,cb]);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,mapBK.BQ,mapBK.BU,mapBK.Qvar,mapBK.Uvar,vb,[],3,sl)
set(ax2,'FontSize',7,'FontWeight','normal');
title('BK B');
xlabel('Right ascension [deg.]');
ylabel('Declination [deg.]');

% colorbars
ax3 = axes('Position',[dim.x2,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax3,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-ce,ce,1000),repmat(linspace(-ce,ce,1000)',1,10));
colormap(colormap_lint)
freezeColors;
set(ax3,'Xtick',[]);
set(ax3,'YDir','normal','YAxisLocation','right','YTick',ctickse);

ax4 = axes('Position',[dim.x2,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax4,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-cb,cb,1000),repmat(linspace(-cb,cb,1000)',1,10));
colormap(colormap_lint)
freezeColors;
set(ax4,'Xtick',[]);
set(ax4,'YDir','normal','YAxisLocation','right','YTick',cticksb);

% colorbar units
ax5 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1],'Visible', 'off');
set(ax5, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% this works way better than direct print to pdf
print -depsc2 b2p_plots/eb_maps.eps
%  fix_lines('b2p_plots/eb_maps.eps')
system_safe(['convert -density 300 -colorspace rgb b2p_plots/eb_maps.eps b2p_plots/eb_maps_bk_',num2str(v1),'.png'])
%  system_safe('scp b2p_plots/spectra_BB.png fliescher@spudws4.spa.umn.edu:~/keck_analysis/b2p_plots')

fig=figure(1);
clf;
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,mapP.E,'iau');
caxis([-ce,ce]/sfe);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,mapP.EQ,mapP.EU,mapP.Qvar,mapP.Uvar,vep,[],3,sl)
set(ax1,'FontSize',7,'FontWeight','normal');
set(ax1,'Xticklabel',[],'Yticklabel',[]);
title('P353 E');

ax2=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapP.B,'iau');
caxis([-ce,ce]/sfe);
colormap(colormap_lint)
freezeColors
hold on
plot_pol(m,mapP.BQ,mapP.BU,mapP.Qvar,mapP.Uvar,vbp,[],3,sl)
set(ax2,'FontSize',7,'FontWeight','normal');
title('P353 B');
set(ax2,'Yticklabel',[]);
xlabel(' ');
ylabel(' ');


% colorbars
ax3 = axes('Position',[dim.x2,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax3,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-ce/sfe,ce/sfe,1000),repmat(linspace(-ce/sfe,ce/sfe,1000)',1,10));
colormap(colormap_lint)
freezeColors;
set(ax3,'Xtick',[]);
set(ax3,'YDir','normal','YAxisLocation','right','YTick',cticksp);

ax4 = axes('Position',[dim.x2,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax4,'FontSize',7,'FontWeight','normal');
imagesc(1:10,linspace(-ce/sfe,ce/sfe,1000),repmat(linspace(-ce/sfe,ce/sfe,1000)',1,10));
colormap(colormap_lint)
freezeColors;
set(ax4,'Xtick',[]);
set(ax4,'YDir','normal','YAxisLocation','right','YTick',cticksp);

% colorbar units
ax5 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1],'Visible', 'off');
set(ax5, 'FontSize', 7, 'FontWeight', 'normal');
text(0.5, dim.y1 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');
text(0.5, dim.y2 + dim.ymap / 2, '{\mu}K', 'Rotation', 90, 'FontSize', 7, ...
     'FontWeight', 'normal', 'HorizontalAlignment', 'center');

% this works way better than direct print to pdf
print -depsc2 b2p_plots/eb_maps.eps
%  fix_lines('b2p_plots/eb_maps.eps')
system_safe(['convert -density 300 -colorspace rgb b2p_plots/eb_maps.eps b2p_plots/eb_maps_P_',num2str(v1),'.png'])

return

%%%%%%%%%%%%%%%%%%%%%
function make_bk_vs_world

% A couple of options here for which data to display:
% * Raw bandpower option:
%   0 = Don't plot raw bandpowers
%   1 = Plot BK raw bandpowers (default)
rawopt = 1;
% * CMB-only bandpowers
%   0 = Don't plot CMB-only bandpowers
%   1 = Plot CMB-only bandpowers from cleaning analysis
%   2 = Plot CMB-only bandpowers from per-bin component separation (default)
cmbopt = 1;

% get the BK points - we use r=0.2 simr field as for the posted
% BK_bandpowers_sample_20150129.txt file
load final/1456x1457/real_aab_filtp3_weight3_gs_dp1102_jack0_real_aab_filtp3_weight3_gs_dp1100_jack0_real_aab_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf.mat
r=get_bpcov(r); 
r=weighted_ellbins(r,bpwf);

% get updated lens spectrum
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');
% this lens spec doesn't go high enough
ml.Cs_l(1900:end,4)=NaN;

% get the dust only model at 150GHz
param =  [0, 0, 0, 3.75, 0, 0, 1.6, -0.4, 0, 2, 0, 19.6, 0, 0];
likedata.opt.expt{1} = 'B2_150';
likedata.opt.fields = {'T', 'E', 'B'};
likedata = like_read_theory(likedata);
likedata = like_read_bandpass(likedata);
% fetch the model from the multicomponent framework
[ell, cmp] = model_rms(param, likedata);

% Legend location
text_x=[12,35,55];
text_y=logspace(log10(3e0),log10(5e1),6);

clf;
setwinsize(gcf,400,300)

loglog(ml.l,ml.Cs_l(:,4),'r');
hold on
%xlim([0,330]); ylim([-0.03,0.04]);
xlim([10,3000]); ylim([1e-3,1e2]);

plot(ell,cmp(:,3,3).^2,'r--');
plot(ell,ml.Cs_l(:,4)+cmp(:,3,3).^2,'r--');
text(1.1e1,2.5e-2,'Dust at 150GHz','Rotation',-10)
text(6e1,2e-3,'lensing','Rotation',35)

% Plot raw bandpowers.
% Shift horizontal position left by 2% to avoid overlap with
% CMB-only points.
if rawopt
  lc = 0.98 * r(1).lc(2:10,4);
  h = errorbar2(lc, r(1).real(2:10,4), std(r(1).simr(2:10,4,:),[],3), 'k.');
  h = herrorbar2(lc, r(1).real(2:10,4), lc - r(1).ll(2:10,4), ...
                 r(1).lh(2:10,4) - lc, 'k.');
  h = plot(lc, r(1).real(2:10,4), 'k.', 'MarkerSize', 12);
end

% Plot CMB-only bandpowers.
% Color for CMB-only bandpowers (dark grey).
cc = [0.5 0.5 0.5];
% Shift horizontal position right by 2% to avoid overlap with raw bandpowers.
lc = 1.02 * r(1).lc(:,4);
if (cmbopt == 1)
  % Add Cambridge-style "cleaned" spectrum
  % Calculate cleaned spectrum and error bars.
  clear likedata;
  likeopt.l = [2:10];
  likeopt.fields = {'B'};
  likeopt.Planck_datasplit = 2;
  likeopt.Planck_noisebias = 1;
  likedata = get_likedata('BKP', likeopt);
  likeopt = likeopt_select(likeopt, [1,5], [], []);
  alpha = freq_scaling(likedata.bandpass{1}, 1.59, 19.6, 353) / ...
          freq_scaling(likedata.bandpass{2}, 1.59, 19.6, 353);
  % Cleaned spectrum.
  clspec = nan([17,1]);
  clspec(2:10) = (likedata.real(:,1) - alpha * likedata.real(:,3)) / (1 - alpha);
  % Error bars for cleaned spectrum using dust fiducial model.
  pfl = [0, 1, 0, 3.6, -3, -0.6, 1.59, -0.42, 1, 1, 0, 19.6];
  [ell, rms] = model_rms(pfl, likedata);
  bpcm0 = scale_bpcm(rms, likedata);
  n_spec = 3;
  for i=1:numel(likedata.opt.l)
    for j=1:numel(likedata.opt.l)
      bpcm(i,j) = (bpcm0((i-1)*n_spec+1,(j-1)*n_spec+1) + ...
                   alpha^2 * bpcm0((i-1)*n_spec+3,(j-1)*n_spec+3) - ...
                   alpha * bpcm0((i-1)*n_spec+1,(j-1)*n_spec+3) - ...
                   alpha * bpcm0((i-1)*n_spec+3,(j-1)*n_spec+1)) / ...
          (1 - alpha)^2;
    end
  end
  clerr = nan([17,1]);
  clerr(2:10) = sqrt(diag(bpcm));
  % Select ell bins that should be plotted as upper limits
  % (all points that are <1 sigma above zero).
  ulbin = (clspec < clerr);
  % Plot points.
  h = errorbar2(lc(~ulbin), clspec(~ulbin), clerr(~ulbin), clerr(~ulbin), '.');
  set(h, 'Color', cc);
  herrorbar2(lc(~ulbin), clspec(~ulbin), lc(~ulbin) - r(1).ll(~ulbin,4), ...
             r(1).lh(~ulbin,4) - lc(~ulbin), '.', cc);
  plot(lc(~ulbin), clspec(~ulbin), '.', 'Color', cc, 'MarkerSize', 12);
  % Plot upper limits.
  herrorbar2(lc(ulbin), clspec(ulbin) + 2 * clerr(ulbin), lc(ulbin) - r(1).ll(ulbin,4), ...
             r(1).lh(ulbin,4) - lc(ulbin), 'v', cc);
elseif (cmbopt == 2)
  % Add CMB component spectrum
  [cmb, dust] = get_perbin_comp();
  % Select ell bins that should be plotted as upper limits
  % (all points that are <1 sigma above zero).
  ulbin = (cmb.sig1(1,:) == 0);
  % Plot points.
  h = errorbar2(lc(~ulbin), cmb.ml(~ulbin), cmb.ml(~ulbin) - cmb.sig1(1,~ulbin), ...
                cmb.sig1(2,~ulbin) - cmb.ml(~ulbin), '.');
  set(h, 'Color', cc);
  herrorbar2(lc(~ulbin), cmb.ml(~ulbin), lc(~ulbin) - r(1).ll(~ulbin,4), ...
             r(1).lh(~ulbin,4) - lc(~ulbin), '.', cc);
  plot(lc(~ulbin), cmb.ml(~ulbin), '.', 'Color', cc, 'MarkerSize', 12);
  % Plot upper limits.
  herrorbar2(lc(ulbin), cmb.sig2(2,ulbin), lc(ulbin) - r(1).ll(ulbin,4), ...
             r(1).lh(ulbin,4) - lc(ulbin), 'v', cc);
end

t = text(text_x(1),text_y(6),'BK','Color','k');
if cmbopt > 0
  text(15, text_y(6), ' [CMB-only]', 'Color', cc);
end

ms=4;
other_data=load('other_experiments/bicep1.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(5),'BICEP1','Color',[0.5,0.5,0.5])
other_data=load('other_experiments/boomerang.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(5),'Boomerang','Color',[1,0,1])
other_data=load('other_experiments/capmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(2),'CAPMAP','Color',[0.5,0,0.5])
other_data=load('other_experiments/cbi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,0]);
set(h(2),'MarkerSize',ms);
text(text_x(3),text_y(6),'CBI','Color',[0,1,0])
other_data=load('other_experiments/dasi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(4),'DASI','Color',[0,1,1])
other_data=load('other_experiments/quad.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,0]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(4),'QUAD','Color',[1,0,0])
other_data=load('other_experiments/quiet40.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,1]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(3),'QUIET-Q','Color',[0.5,0.5,1])
other_data=load('other_experiments/quiet90.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(2),'QUIET-W','Color',[0,0,1])
other_data=load('other_experiments/wmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,1/3,0]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(3),'WMAP','Color',[1,1/3,0])

% polarbear measurement of lensing
other_data=load('other_experiments/polarbear.txt');
ul=3;
meas=[1,2,4];
halfbw = (other_data(2,1)-other_data(1,1))/2;
halfbw = repmat(halfbw,4,1);
h=errorbar2(other_data(meas,1),other_data(meas,2),other_data(meas,3),'bs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');
h=herrorbar2(other_data(meas,1),other_data(meas,2),halfbw(meas),halfbw(meas),'bs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');
h=herrorbar2(other_data(ul,1),other_data(ul,4),halfbw(ul),halfbw(ul),'bv');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');
text(text_x(1),text_y(1),'Polarbear','Color',[0,0,1])
text(text_x(1),text_y(1),'Polarbear','Color',[0,0,1])

% sptpol http://arxiv.org/pdf/1503.02315.pdf tab1
other_data=load('other_experiments/sptpol.txt');
other_data(:,2)=other_data(:,2).*other_data(:,1)/(2*pi);
other_data(:,3)=other_data(:,3).*other_data(:,1)/(2*pi);
ul=[4,5];
meas=[1,2,3];
halfbw = (other_data(2,1)-other_data(1,1))/2;
halfbw = repmat(halfbw,5,1);
h=errorbar2(other_data(meas,1),other_data(meas,2),other_data(meas,3),'gs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','g');
h=herrorbar2(other_data(meas,1),other_data(meas,2),halfbw(meas),halfbw(meas),'gs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','g');
% decide to make upper limit just +2xerrorbar
h=herrorbar2(other_data(ul,1),other_data(ul,3)*2,halfbw(ul),halfbw(ul),'gv');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','g');
text(text_x(2),text_y(1),'SPTpol','Color',[0,1,0])

hold off

xlabel('Multipole')
ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')

keyboard

print -depsc2 b2p_plots/bk_vs_world.eps
fix_lines('b2p_plots/bk_vs_world.eps')
!convert -density 300 b2p_plots/bk_vs_world.eps b2p_plots/bk_vs_world.png

return

%%%%%%%%%%%%%%%
function make_wmap_sync

% undo
set(0,'defaultlinelinewidth','remove');
set(0,'DefaultAxesFontSize','remove');

ll=-3.6; ul=-2.5;

fn='input_maps/wmap/wmap_mcmc_fs_synch_spec_index_9yr_v5.fits'

hmap=read_fits_map(fn);

close all

subplot(2,1,1);
mollview(fn,2,[]); caxis([ll,ul]);
fid=fopen('B2_3yr_373sqdeg_field_20140509.txt');
ol=textscan(fid,'%f %f %f %f','commentstyle','#','Delimiter',',');
fclose(fid);
plotm(ol{4},ol{3},'.k');

% get BK mask
load maps/1456/real_aab_filtp3_weight3_gs_dp1102_jack0
map=make_map(ac,m,coaddopt);
fm=1./((map.Qvar+map.Uvar)/2);
bm=1.0*~isnan(fm); bm(bm==0)=NaN;

% get wmap in our region
wssi=healpix_to_longlat(fn,m.x_tic,m.y_tic,'G',false);

map=wssi(:,:,2).*bm; % mean over chain

%set(h,'position',[0.8079    0.8080    0.1425    0.0846]);

subplot(2,2,3);
plot_map(m,map.*bm); caxis([ll,ul]);
h=colorbar('horiz')
title('Zoom on BK patch');
xlabel('RA'); ylabel('Dec');

subplot(2,2,4);
[bc,j]=hfill(map,100,ll,ul,fm);
hplot(bc,j./sum(j),'b');
[bc,k]=hfill(hmap.map(:,2),100,ll,ul);
hplot(bc,k./sum(k),'Sr');
title(sprintf('weighted mean in BK patch=%.2f',nansum(cvec(map).*fm(:)./nansum(fm(:)))));
axis square

h=legend({'BK patch','full sky'});
legend boxoff

gtitle('WMAP9 Synchrotron Spectral Index Map');

print -depsc2 b2p_plots/wmap_sync.eps
fix_lines('b2p_plots/wmap_sync.eps')
!convert -density 300 b2p_plots/wmap_sync.eps b2p_plots/wmap_sync.png

return

%%%%%%%%%%%%%%%
function make_future

% fiducial analysis
[loc,lc]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,0,1);

% find model which centers on this
amod=[0.05,1,0,3.2,-3.3,-0.6,1.60,-0.42,1,2,0,19.6];
[loc,lc]=dynchi2_grid_analysis(3,2:6,1,[],1,34,1,0,1);

return

%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate CMB and dust spectra from 3D likelihoods at each ell bin.
function [cmb, dust] = get_perbin_comp()

% Directory containing precomputed likelihoods.
dir = '/n/home05/cbischoff/work/2015_02/20150227_multicomp/output/';

% Allocate results so that these arrays will have length=17 (but
% nan values for ell bins that weren't calculated).
cmb.ml = nan([1,17]);
cmb.sig1 = nan([2,17]);
cmb.sig2 = nan([2,17]);
dust.ml = nan([1,17]);
dust.sig1 = nan([2,17]);
dust.sig2 = nan([2,17]);

% Loop over ell bins.
for i=2:10
  % Load 3D log-likelihood.
  a = load(fullfile(dir, sprintf('like_bin%02i.mat', i)));

  % Marginalize over beta.
  Pbeta = zeros(size(a.logl));
  for j=1:size(Pbeta,3)
    Pbeta(:,:,j) = exp(-0.5 * (a.bdust(j) - 1.59)^2 / 0.11^2);
  end
  lik3 = exp(a.logl);
  lik2 = sum(lik3 .* Pbeta, 3);

  % Marginalize over dust to get CMB likelihood.
  lik_cmb = sum(lik2, 2);
  % Get ML point, 1 and 2-sigma intervals.
  [cmb.sig1(:,i), cmb.ml(i)] = hpd_interval(a.cmb, lik_cmb, 0.68);
  cmb.sig2(:,i) = hpd_interval(a.cmb, lik_cmb, 0.95);

  % Marginalize over CMB to get dust likelihood.
  lik_dust = sum(lik2, 1)';
  % Get ML point, 1 and 2-sigma intervals.
  [dust.sig1(:,i), dust.ml(i)] = hpd_interval(a.Adust, lik_dust, 0.68);
  dust.sig2(:,i) = hpd_interval(a.Adust, lik_dust, 0.95);
end

function make_perbin_comp()
% Plots CMB and dust spectra derived from pre-calculated 3D likelihoods.
% Likelihood dimensions are:
%   * CMB amplitude, in uK^2
%   * Dust amplitude, in uK^2 at 150 GHz
%   * Dust spectral index (beta)

% Get CMB and dust spectra, with one and two sigma intervals.
[cmb, dust] = get_perbin_comp();

% Get actual bin centers.
a = load(['final/1456x1457/' ...
          'real_aab_filtp3_weight3_gs_dp1102_jack0_' ...
          'real_aab_filtp3_weight3_gs_dp1100_jack0_' ...
          'pureB_matrix_directbpwf']);
r = weighted_ellbins(a.r(1), a.bpwf(1));
l = r.lc(:,4);

% ell bins to plot
bin = [2:10];

% Set up figure.
figure();
clf;
setwinsize(gcf, 900, 450);
set(gcf, 'PaperPosition', [0 0 9 4.5]);
hold all;
% Get lensing and dust theory spectra.
% BKP best-fit dust is 3.3 uK^2 at 353 GHz -- scale this to 150 GHz for figure.
lens = load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');
dust_bf = 3.3 * freq_scaling([150,1], 1.59, 19.6, 353)^2 * (lens.l / 80).^(-0.42);
l3 = plot(lens.l, lens.Cs_l(:,4), 'Color', [0 0.5 0]);
l4 = plot(lens.l, dust_bf, 'b');
for i=1:numel(bin)
  plot((l(bin(i)) - 3) * [1 1], cmb.sig1(:,bin(i)), 'Color', [0 0.5 0]);
  l1 = plot([l(bin(i)) - 3], [cmb.ml(bin(i))], 'x', 'Color', [0 0.5 0], 'MarkerSize', 10);
  plot((l(bin(i)) + 3) * [1 1], dust.sig1(:,bin(i)), 'b');
  l2 = plot([l(bin(i)) + 3], [dust.ml(bin(i))], 'bx', 'MarkerSize', 10);

  % Plot 2 sigma upper limit only if 1 sigma interval includes zero.
  if cmb.sig1(1,bin(i)) == 0
    plot(l(bin(i)) + [-6, 0], cmb.sig2(2,bin(i)) * [1 1], 'Color', [0 0.5 0]);
    plot(l(bin(i)) + [-5,-3,-1], cmb.sig2(2,bin(i)) + [0, -0.002, 0], ...
         'Color', [0 0.5 0]);
  end
  if dust.sig1(1,bin(i)) == 0
    plot(l(bin(i)) + [0, 6], dust.sig2(2,bin(i)) * [1 1], 'b');
    plot(l(bin(i)) + [1,3,5], dust.sig2(2,bin(i)) + [0, -0.002, 0], 'b');
  end
end
% Axes, labels, legend.
xlim([0 350]);
ylim([0 0.06]);
xlabel('ell');
ylabel('D_{l,150} [\muK^2]');
legend([l1, l2, l3, l4], {'CMB component', 'dust component', ...
                    'standard \LambdaCDM lensing', 'BKP best-fit dust'});
% Print figure.
print -depsc2 b2p_plots/comp_spectra.eps
system_safe(['convert -density 300 -colorspace rgb b2p_plots/comp_spectra.eps ' ...
             '-resize 900 b2p_plots/comp_spectra.png']);

return
