function reduc_plotcals(tagz,do_save,rx,cpfilestobicepfs1)
% reduc_plotcals(tagz,do_save,rx)
%
% make plots of cal quantities over run
%
% reduc_plotcals({'055OB0517'})
% reduc_plotcals(get_tags('bicepcmb2006',2),2)
%
% t=get_tags('bicepcmb2008',2); reduc_plotcals(t(141:141))
%
% if do_save is 1, save under a default file name.
% if do_save is a string, save under that name.
%
% only plot receivers specified by rx (can be a vector)


if(~exist('rx','var'))
  rx=[];
end
if(isempty(rx))
  rx=[0,1,2,3,4];
end

% don't plot to screen if writing a png file
if(do_save==1 || ischar(do_save)); 
  set(0,'DefaultFigureVisible','off');
end

%if saving to panlfs
if(~exist('cpfilestobicepfs1','var'))
  cpfilestobicepfs1=[];
end


% needed because cs is an internal matlab function name
cs=1;

% decide what range of time / schedule we're plotting over.
% This will give the fullest tag part shared between all
% tags to be plotted.  For example, it might be 20100424,
% or 20100424E, or 20100424E01, or 201004.
show_boundaries=[];
if ~isempty(do_save)
  if iscell(tagz)
    % Find part of tag that is common between all entries
    tag_com=tagz{1};
    for j=2:length(tagz)
      while ~strncmpi(tagz{j},tag_com,length(tag_com))
        tag_com=tag_com(1:(end-1));
      end
    end
    if length(tag_com)==4      % year; show month numbers
      show_boundaries=[5 6];
    elseif length(tag_com)==6  % month; show schedule boundaries
      show_boundaries=[7 8];
    elseif length(tag_com)==8  % schedule; show phase boundaries
      show_boundaries=[9 9];
    end
  else
    tag_com=tagz;
  end
end

% Make sure tags are sorted in chronological order
for z=1:length(tagz)
  tmp=get_run_info(tagz{z});
  t0(z)=datenum(tmp.tstart{1},'dd-mmm-yyyy:HH:MM:SS');
end
[t0 idx]=sort(t0);
tagz={tagz{idx}};

% if ~isempty(show_boundaries)
%   disp(['Common tag is ' tag_com ', will show boundaries of tag chars ' num2str(show_boundaries(1)) '-' num2str(show_boundaries(2))]);
% end
boundary_list=[];
boundary_names={};
last_tag='';

for z=1:length(tagz)
  disp(tagz{z})
  
  load(['data/real/',tagz{z}(1:6),'/',tagz{z},'_calval']);

  if ~isempty(show_boundaries) && (isempty(last_tag) || ~strncmp(last_tag,tagz{z},show_boundaries(2)))
    boundary_list(end+1)=en.t(1);
    boundary_names{end+1}=tagz{z}(show_boundaries(1):show_boundaries(2));
    last_tag=tagz{z};
  end

  ena(z)=en;
  try
  if size(lc.g,3)<4
    lc.g(:,:,4)=NaN;
  end
  lca(z)=lc;
  catch
   % idiocy...
   for j=fieldnames(lc)'
     lca(z).(j{1})=lc.(j{1});
   end
  end
  fsa(z)=fs; 
end

en=structcat(1,ena);
lc=structcat(1,lca);
fs=structcat(1,fsa);

% plot set of channels valid for start day
[p,ind]=get_array_info(tagz{1});

% strip down rx array to contain only receivers that are actually present
keepind=ismember(rx,p.rx);
rx=rx(keepind);

% cut out indices not requested by rx
ind=strip_ind(ind,find(ismember(p.rx,rx)));

% convert from mjd to matlab datenum
do=datenum(2005,5,17)-53507;

% find mutual xlim
allt=[en.t+do;fs.t+do];
xl=[min(allt(allt>=allt(1))),max(allt)];

%close all
% figure(1);
clf
setwinsize(gcf,1400,600);
defcm=get(gcf,'DefaultAxesColorOrder');

set(gcf,'DefaultAxesColorOrder',jet(length(ind.rgl)));
set(gcf,'DefaultLineLineWidth',0.2);
set(gcf,'DefaultLineMarkerSize',6);

NUMPANES=7;

subplot(NUMPANES,1,1)
plot(en.t+do,en.g(:,ind.rgl,2),'.');
hold on
plot(en.t+do,nanmedian(en.g(:,ind.rgl,2),2),'k-','linewidth',2);
ylabel({'elnod';'(V/airmass)'})
axis tight; xlim(xl);
xaxis_time(gca);
% yl=ylim; ylim([0,yl(2)]);
% ylim([2,8]);
ylim([0,2e4]);
% set(gca,'YScale','log');
draw_boundaries(boundary_list+do,boundary_names);

set(gcf,'DefaultAxesColorOrder',jet(length(ind.rgla)));

% JAB 20101013, let's track mismatch of relgains
subplot(NUMPANES,1,2)
bovera=(en.g(:,ind.rglb,2)./en.g(:,ind.rgla,2));
delta_bovera=(bovera(2:2:size(bovera,1),:)-bovera(1:2:size(bovera,1)-1,:))./(bovera(1:2:size(bovera,1)-1,:));
tt=en.t+do; tt=mean([tt(2:2:size(bovera,1)),tt(1:2:size(bovera,1)-1)],2);
plot(tt,delta_bovera,'.');
hold on
plot(tt,nanmedian(delta_bovera,2),'k-','linewidth',2);
ylabel({'delta elnod';'\Delta(B/A)/(B/A)'})
axis tight; xlim(xl);
xaxis_time(gca);
% yl=ylim; ylim([0,yl(2)]);
% ylim([2,8]);
ylim([-.05,.05]);
% set(gca,'YScale','log');
set(gca,'YMinorGrid','on')
draw_boundaries(boundary_list+do,boundary_names);

set(gcf,'DefaultAxesColorOrder',jet(length(ind.rgl)));

subplot_grid(NUMPANES,1,3);
plot(fs.t+do,fs.std(:,ind.rgl),'.');
hold on
plot(fs.t+do,nanmedian(fs.std(:,ind.rgl),2),'k-','linewidth',2);
axis tight; xlim(xl);
xaxis_time(gca);
ylabel({'field scan';'std lights'});
ylim([0,20])
draw_boundaries(boundary_list+do,boundary_names);

set(gcf,'DefaultAxesColorOrder',jet(length(ind.gd)));
  
subplot(NUMPANES,1,4)
plot(fs.t+do,fs.std(:,ind.gd),'.');
hold on
plot(fs.t+do,nanmedian(fs.std(:,ind.gd),2),'k-','linewidth',2);
axis tight; xlim(xl);
xaxis_time(gca);
ylabel({'field scan';'std darks'});
ylim([0,8])
draw_boundaries(boundary_list+do,boundary_names);

set(gcf,'DefaultAxesColorOrder',jet(length(ind.rgl)));

subplot_grid(NUMPANES,1,5);
if 0
  if isfield(lc,'g')
    plot(lc.t+do,lc.g(:,ind.rgl,2)*1e12,'.');
  end
  axis tight; xlim(xl); xaxis_time(gca);
  ylim([0,30]);
  ylabel('Pj / pW');
  draw_boundaries(boundary_list+do,boundary_names);
else
  if isfield(lc,'g')
    tmp=lc.g(:,ind.rgl,2);
    for j=1:size(tmp,2)
      tmp(:,j)=tmp(:,j)/nanmedian(tmp(:,j)) - 1;
    end
    plot(lc.t+do,tmp,'.');
    hold on
    plot(lc.t+do,nanmedian(tmp,2),'k-','linewidth',2);
  end
  axis tight; xlim(xl); xaxis_time(gca);
  ylim([-.1 .1]);
  ylabel('\DeltaPj/Pj');
  set(gca,'YMinorGrid','on')
  draw_boundaries(boundary_list+do,boundary_names);
end

subplot(NUMPANES,1,6)
if 0
  if isfield(lc,'g')
    plot(lc.t+do,lc.g(:,ind.rgl,3)*1e3,'.');
  end
  axis tight; xlim(xl); xaxis_time(gca);
  ylim([0,200]);
  ylabel('Rn / m\Omega');
  draw_boundaries(boundary_list+do,boundary_names);
else
  if isfield(lc,'g')
    tmp=lc.g(:,ind.rgl,3);
    for j=1:size(tmp,2)
      tmp(:,j)=tmp(:,j)/nanmedian(tmp(:,j)) - 1;
    end
    plot(lc.t+do,tmp,'.');
    hold on
    plot(lc.t+do,nanmedian(tmp,2),'k-','linewidth',2);
  end
  axis tight; xlim(xl); xaxis_time(gca);
  ylim([-.02 .02]);
  set(gca,'YMinorGrid','on')
  ylabel('\DeltaRn/Rn');
  draw_boundaries(boundary_list+do,boundary_names);
end

subplot(NUMPANES,1,7)
if 0
  if isfield(lc,'g') && size(lc.g,3)>=4
    plot(lc.t+do,lc.g(:,ind.rgl,4)./lc.g(:,ind.rgl,3),'.');
  end
  axis tight; xlim(xl); xaxis_time(gca);
  ylim([0,1]);
  ylabel('Rs/Rn');
  draw_boundaries(boundary_list+do,boundary_names);
else
  tmp=NaN*zeros(size(lc.g,1),1);
  if isfield(lc,'g') && size(lc.g,3)>=4
    for j=1:size(lc.g,1)
      tmp(j)=nanmean(lc.g(j,ind.rgl,4)./lc.g(j,ind.rgl,3)>=0.95) * 100;
      if all(isnan(lc.g(j,ind.rgl,4)))
        tmp(j)=NaN;
      end
    end
    plot(lc.t+do,nanmedian(tmp,2),'k+-','linewidth',2);
  end
  axis tight; xlim(xl); xaxis_time(gca);
  ylim([0 100]);
  set(gca,'YMinorGrid','on')
  ylabel('% normal');
  draw_boundaries(boundary_list+do,boundary_names);
end

title_tags=str_prot(tagz);

if(iscell(tagz))
  gtitle(sprintf('all cal quantities %s to %s; Rx: %s',title_tags{1},title_tags{end},num2str(rx)));
else
  gtitle(sprintf('all cal quantities %s; Rx: %s',title_tags,num2str(rx)));
end

% if requesting all receivers don't save an _rx0 extension
if(numel(rx)==numel(unique(p.rx)))
  rxext='';
else
  rxext=sprintf('_rx%d',rx);
end

if ~isempty(do_save)
  if isnumeric(do_save) && do_save==1
    mkpng(sprintf('reducplots/%s/cal_%s%s.png',tag_com(1:6),tag_com,rxext),1);
    setpermissions(sprintf('reducplots/%s/cal_%s%s.png',tag_com(1:6),tag_com,rxext));

    %copy them to bicepfs1 if requested
    if ~isempty(cpfilestobicepfs1)
      copyfile(sprintf('reducplots/%s/cal_%s%s.png',tag_com(1:6),tag_com,rxext),cpfilestobicepfs1);
    end
  elseif ischar(do_save)
    mkpng(do_save,1);
    setpermissions(do_save);
  end
end

% don't plot to screen if writing a png file
if(do_save==1 || ischar(do_save)); 
  set(0,'DefaultFigureVisible','on');
end
set(gcf,'DefaultLineLineWidth',0.5)
set(gcf,'DefaultLineMarkerSize',6)


return

% Escape _ characters for titles, etc.
function t_out=str_prot(t_in)
t_out=strrep(t_in,'_','\_');
return

function draw_boundaries(t,lab)
axax=axis;
for j=1:length(t)
  line(t(j)*[1 1], [axax(3) axax(4)], 'linestyle', '--', 'color', 'k');
  if ~isempty(lab{j})
    x0=t(j);
    if j<length(t)
      x1=t(j+1);
    else
      x1=axax(2);
    end
    text((x0+x1)/2, 0.9*axax(4)+0.1*axax(3), lab{j}, ...
      'fontsize', 12);
  end
end
