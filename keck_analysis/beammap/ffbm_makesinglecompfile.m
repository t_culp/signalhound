function ffbm_makesinglecompfile(compopt)
% function ffbm_makesinglecompfile(compopt)
%
% Function to save all of the component maps we want to use into a single
% file (instead of having to load up 50+ maps).  The file can then be
% plugged into plotting or rotation code and resaved.  Keep all the maps
% (can apply cuts later)
%
% INPUTS (should all be sent in with compopt)
%
%   expt:          'keck','b3'
%   year:          keck: 2014, 2015
%                  b3:   2015
%
% OPTIONAL INPUTS 
%
%   mapcoord:        'azel_ap' (default), 'raw' (used to get map filename)
%   demodtype:       'square' (default), 'choplet'
%   suffix:          optional string to add to map name, '' default
%   bmdir:           directory from which to load beammaps
%                    (default beammaps/maps)
%   mapstoload:      vector of beam map run numbers to look at, i.e. 1:10
%                    (default is everything in bmrunlist.csv)
%   componentdir:    directory in which to save output map struct
%                    (default beammaps/maps_component)
%   componentfile:   name of file to save in componentdir
%                    (default ffbm_year_allcomp)

% Parse compopt
expt = compopt.expt;
year = compopt.year;
if ~isfield(compopt,'mapcoord');
  mapcoord = 'azel_ap';
else
  mapcoord = compopt.mapcoord;
end
if ~isfield(compopt,'demodtype');
  demodtype = 'square';
else
  demodtype = compopt.demodtype;
end
if ~isfield(compopt,'suffix')
  suffix = '';
else
  suffix = compopt.suffix;
end
if ~isfield(compopt,'bmdir')
  bmdir = 'beammaps/maps';
else
  bmdir = compopt.bmdir;
end
% Which maps do we want?
% Get beam map run info
bm = get_bm_info(year);
if ~isfield(compopt,'mapstoload')
  compopt.mapstoload = bm.number;
else
  bm = structcut(bm,compopt.mapstoload);
end
if ~isfield(compopt,'componentdir')
  compopt.componentdir = 'beammaps/maps_component';
  componentdir = compopt.componentdir;
else
  componentdir = compopt.componentdir;
end
if ~isfield(compopt,'componentfile')
  compopt.componentfile = ['ffbm_' num2str(year) '_allcomp'...
	suffix];
  componentfile = compopt.componentfile;
else
  componentfile = compopt.componentfile;
end

[p ind] = get_array_info([num2str(year) '0201']);

% Load up each beam map.  Only use windowed map!
for ii = 1:length(bm.number)

  filename = bm.filename{ii};
  disp(['Loading map ' num2str(ii) ': ' filename]);
  wmapname = [bmdir,'/mapwin_',filename,'_',demodtype,...
	'_',mapcoord,suffix];
  w = load(wmapname);
  w = w.bm;
  
  % Save the maps in a larger array
  component{ii} = w.map;
  fit{ii} = w.A;
  %x_bin{ii} = w.x_bin;
  %y_bin{ii} = w.y_bin;
  az_ap{ii} = w.x_bin;
  el_ap{ii} = w.y_bin;

  % Here the maps have 
  %   dimension 1 = increasing apparent el
  %   dimension 2 = increasing apparent az
  % So to get e.g. a normal beam map plot, we would do:
  %   imagesc(x_bin,y_bin,map); set(gca,'ydir','normal');
  % Which places increasing apparent az along the bottom going right
  % and increasing apparent el on the left going up
  % Note that this plot is REVERSED PARITY from x'/y'!!!
  
end

% Rearrange so we're in a more logical structure: for each detector, have an
% array of each map taken and the fit values
for ii = 1:length(p.gcp)
  for jj = 1:length(component)
    map{ii}.component{jj} = component{jj}(:,:,ii);
    map{ii}.fit{jj} = fit{jj}(:,ii);
  end
end

% Prep output struct
% For some reason, ad doesn't play nicely with our pixel spacings
% Hope to god that the windowed map dimensions are identical across all
% components 
% Here the ad struct is a generalized (i.e. axis-shifted) axis onto which
% the individual component maps will be moved.  It has the same
% dimensions as the az_ap and el_ap arrays, where
%   az_ap = increasing apparent az
%   el_ap = increasing apparent el
% so we here define
%   ad.t_val_deg{1} = increasing apparent az
%   ad.t_val_deg{2} = increasing apparent el
% such that up to a constant, 
%   imagesc(ad.t_val_deg{1},ad.t_val_deg{2},map);
%   set(gca,'ydir','normal')
% renders the same parity and orientation as above

field_size_x = range(w.x_bin) + (w.x_bin(2) - w.x_bin(1));
field_size_y = range(w.y_bin) + (w.y_bin(2) - w.y_bin(1));
comp.ad = calc_ad2([field_size_x field_size_y],...
    [length(w.x_bin) length(w.y_bin)]);
comp.map = map;
comp.bm = bm;
%comp.x_bin = x_bin;
%comp.y_bin = y_bin;
comp.az_ap = az_ap;
comp.el_ap = el_ap;
comp.compopt = compopt;

savename = [componentdir '/' componentfile];
save(savename,'comp','-v7.3');

disp(['Saved single component file: ' savename]);

return