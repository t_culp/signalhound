function rpsdata = rps_fit_angle(rpsdata)

% Loop over channels.
nchan = length(rpsdata.channel);
for i=1:nchan
  % Three parameter fit, for now.
  [amp ph amperr pherr dc] = sinefit([0:1/6:2]', ...
      rpsdata.beam{i}.param(:,1), 1);
  
  % Store fit parameters.
  rpsdata.fit{i}.amp = amp;
  rpsdata.fit{i}.ph = ph;
  rpsdata.fit{i}.amperr = amperr;
  rpsdata.fit{i}.pherr = pherr;
  rpsdata.fit{i}.dc = dc;
  
  % Store model.
  rpsdata.fit{i}.model(:,1) = [0:0.01:2*pi];
  rpsdata.fit{i}.model(:,2) = amp * sin(2 * [0:0.01:2*pi] + ph) + dc;
end
