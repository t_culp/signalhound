function bmsim_combine_K2012_K2013()
%  bmsim_combine_K2012_K2013()
%  this makes the beam map sims for the 1351/????_ab_ maps (K13) 

% CLW's individual year maps corresponding to
% the real maps 1351 dp1102
for j='0'
  mapK2012 = (['2086/0002_a_filtp3_weight3_gs_dp1102_jack',j,'1.mat']);
  mapK2013 = (['2086/0002_b_filtp3_weight3_gs_dp1102_jack',j,'1.mat']);
  mapK13   = (['2086/0002_ab_filtp3_weight3_gs_dp1102_jack',j,'.mat']);
  load_and_combine(mapK2012,mapK2013,mapK13)
  %farmit('farmfiles/','load_and_combine(mapK2012,mapK2013,mapK13)','func',{@load_and_combine,@minimize_coaddopt},'var',{'mapK2012','mapK2013','mapK13'},'queue',queue,'mem',10000,'maxtime',15,'submit',0)    

end

%babysitjobs('farmfiles/comb/*','wait5')
      
return


function load_and_combine(mapK2012,mapK2013,mapK13)
%  this needs to be adjusted year to year
  mapK2012
  mapK2013
  mapK13
  
  mK2012  = load(['maps/',mapK2012]);
  mK2013  = load(['maps/',mapK2013]);
  
  % make sure you got the right ukpervolt for that year here:
  mK2012.coaddopt.ukpv_applied = get_ukpervolt('2012');
  mK2012.ac=cal_coadd_ac(mK2012.ac,mK2012.coaddopt.ukpv_applied);
  mK2012.ac=coadd_ac_overfreq(mK2012.ac,mK2012.coaddopt);
  
  mK2013.coaddopt.ukpv_applied = get_ukpervolt('2013');
  mK2013.ac=cal_coadd_ac(mK2013.ac,mK2013.coaddopt.ukpv_applied);
  mK2013.ac=coadd_ac_overfreq(mK2013.ac,mK2013.coaddopt);
  
  ac = struct_merge(mK2012.ac,mK2013.ac);
  ac = coadd_ac_overrx(ac);
  
  mK2012.coaddopt = minimize_coaddopt(mK2012.coaddopt);
  mK2013.coaddopt = minimize_coaddopt(mK2013.coaddopt);  
  
  mK2012.coaddopt.mapname = mapK2012;
  mK2013.coaddopt.mapname = mapK2013;
  
  coaddopt={mK2012.coaddopt;mK2013.coaddopt};
  m = mK2013.m;
  
  saveandtest(['maps/',mapK13],'ac','coaddopt','m','-v7.3');
return

function coaddopt = minimize_coaddopt(coaddopt)
  try coaddopt = rmfield(coaddopt,'b'); end
  try coaddopt = rmfield(coaddopt,'bi'); end
  try coaddopt = rmfield(coaddopt,'bw'); end
  try coaddopt = rmfield(coaddopt,'hsmax'); end
  try coaddopt = rmfield(coaddopt,'whist'); end
  try coaddopt = rmfield(coaddopt,'devhist'); end
  try coaddopt = rmfield(coaddopt,'traj'); end
  try coaddopt = rmfield(coaddopt,'c'); end
  try coaddopt = rmfield(coaddopt,'jackmask'); end
return
