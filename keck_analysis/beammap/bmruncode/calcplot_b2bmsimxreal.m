function calcplot_b2bmsimxreal(simnums,calcopt)
% function calcplot_b2bmsimxreal(simnums,calcopt)
% Calculate and plot beam map sim aps, maps pagers, and final spectra
%
% INPUTS
%   
%   simnums: struct specifying the sim numbers to calc/plot 
%              simser, simrlz, simdau, realser, realrlz, realdau
%   calcopt: struct specifying spectrum calculation option and what to do
%              calcaps: 0 (default), 1 - generate aps
%              mappager: 0 (default), 1 - generate maps pager
%              plotaps: 0 (default), 1 - generate aps spectrum plots
%              est: 'pureB' (default), 'matrix'
%              autocross: 'auto' (default), 'cross'

% Parse input options
if isempty(simnums)
  simser = '2564';
  simrlz = '0002';
  simdau = 'a';
  realser = '1450';
  realrlz = 'real';
  realdau = 'a';
else
  simser = simnums.simser;
  simrlz = simnums.simrlz;
  simdau = simnums.simdau;
  realser = simnums.realser;
  realrlz = simnums.realrlz;
  realdau = simnums.realdau;
end
  
if isempty(calcopt)
  calcaps = 0;
  mappager = 0;
  plotaps = 0;
  est = 'pureB';
  autocross = 'auto';
else
  calcaps = calcopt.calcaps;
  mappager = calcopt.mappager;
  plotaps = calcopt.plotaps;
  est = calcopt.est;
  autocross = calcopt.autocross;
end

% General setup

% BICEP2 matrix estimator
xb12 = ['/n/panlfs2/bicep/bicep2/pipeline/matrixdata/c_t/0704/' ...
        'healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat']; 

% apsopt template
switch autocross
  case 'auto'
    load('aps/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix');
  case 'cross'
    load(['aps/0751x1351/' ...
          'real_a_filtp3_weight3_gs_dp1102_jack0_'...
          'real_a_filtp3_weight3_gs_dp1102_jack01_matrix_overrx']);
end
apsopt = rmfield(apsopt,'purifmatname');
apsopt.makebpwf = 0;

switch est
  case 'matrix'
    apsopt.purifmatname = xb12;
  case 'pureB'
    apsopt.pure_b = 'kendrick';
end

apsopt.overrideukpervolt = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate aps

if calcaps
  switch autocross
    case 'auto'
      apsopt.ukpervolt = {get_ukpervolt()};
      apsopt.polrot = [0];
      mapn = {[simser '/' simrlz '_' simdau '_filtp3_weight3_gs_dp*_jack*.mat']};
      reduc_makeaps(mapn,apsopt);
    case 'cross'
      apsopt.ukpervolt = {get_ukpervolt(),get_ukpervolt()};
      apsopt.polrot = [0,-1.1];
      switch est
        case 'matrix'
          apsopt.purifmatname = {xb12,xb12};
      end
      apsopt.commonmask = 2;
      mapn = {[simser '/' simrlz '_' simdau '_filtp3_weight3_gs_dp*_jack*.mat'],...
              [simser '/' simrlz '_' simdau],[realser '/' realrlz '_' realdau]};
      reduc_makeaps(mapn,apsopt);
  end
  
end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make maps pager

mapn = {[simser '/' simrlz '_' simdau '_filtp3_weight3_gs_dp*_jack*.mat']};
apsopt.mapname = {simser};
apsopt.polrot = 0;
apsopt.ukpervolt = {get_ukpervolt()};

if mappager  % free, then fixed color scale
  reduc_plotcomap_pager(mapn,apsopt,0,[],[],[],[],[],[],0,[],1); 
  reduc_plotcomap_pager(mapn,apsopt,1,[],[],[],[],[],[],0,[],1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot aps

nbase = simser;
nbase2 = [simser 'x' realser];

% Deprojections - all made for auto/cross
depj{1} = '0000'; depj{2} = '0100'; depj{3} = '1100'; depj{4} = '1110';
depj{5} = '1101'; depj{6} = '1111'; depj{7} = '1102';

% Same for jacks
jack{1} = '0'; jack{2} = '1'; jack{3} = '2'; jack{4} = '3';
jack{5} = '4'; jack{6} = '5'; jack{7} = '6'; jack{8} = '7';
jack{9} = '8'; jack{10} = '9'; jack{11} = 'a'; jack{12} = 'b';
jack{13} = 'c'; jack{14} = 'd'; jack{15} = 'e'; 

lmax{1} = '200'; lmax{2} = '500';

if plotaps

  set(0,'defaultlinelinewidth',1.0);
  set(0,'DefaultAxesFontSize',14);
  
  figure(1); clf; 
  setwinsize(gcf,1000,600);
  set(gcf,'Visible','off');
  
  for dd = 1:length(depj)

    % Load up the appropriate supfac - in principle this should have
    % been done for all jacks too (so it would go below), but we only
    % have jack0, so to save time load it here
    switch est
      case 'pureB'
        % See paper_plots_b2_respap14, subfunc make_beammapsimsplot
        final = load(['final/1450x1350/real_a_filtp3_weight3_gs_dp'...
                      depj{dd} '_jack0_real_a_filtp3_weight3_gs_dp'...
                      depj{dd} '_jack01_pureB_overrx.mat']);
      case 'matrix'
        % See paper_plots_b2_respap14, subfunc get_b2xb2
        % This supfac was only made for dp1100/1102!  Just use 1102 
        final = load(['final/0751/real_a_filtp3_weight3_gs_dp1102_'...
                      'jack0_matrix_directbpwf_rbc.mat']);
    end

    for jj = 1:length(jack)
        
      % Load in aps, apply supfac, prepare for plotting fuction
      switch autocross
        case 'auto'
          aps = load(['aps/' simser '/' simrlz '_' simdau ...
                      '_filtp3_weight3_gs_dp' depj{dd} '_jack' ...
                      jack{jj} '_' est '.mat']); 
          % pureB supfac loaded above has 3 fields (B2XKeck), choose first
          % Matrix only has 1 so final.supfac(1) is fine
          aps = apply_supfac(aps.aps,final.supfac(1));
          leg = nbase;
        case 'cross'
          aps = load(['aps/' simser 'x' realser '/' simrlz '_' simdau ...
                      '_filtp3_weight3_gs_dp' depj{dd} '_jack' ...
                      jack{jj} '_' realrlz '_' realdau ...
                      '_filtp3_weight3_gs_dp' depj{dd} '_jack' ...
                      jack{jj} '_' est '_cm_overrx.mat']);
          % aps(3) contains alt crosses which cause apply_supfac to fail
          % zero them out
          aps.aps(3).Cs_l = aps.aps(3).Cs_l(:,1:6);
          % pureB supfac loaded above is B2, Keck, B2xKeck - so for our
          % BM sim result we actually want B2, B2, B2 for all fields!
          % For matrix there is only 1, and this extends final.supfac to
          % auto and cross!
          final.supfac(2) = final.supfac(1);
          final.supfac(3) = final.supfac(1);
          aps = apply_supfac(aps.aps,final.supfac); % Apply to all 3
          % Do we want to plot the real spectra too?  If not, reduce
          % dimensionality of aps 
          % [sim auto, real auto, sim x real] ->
          % [sim auto, sim x real]
          aps = aps([1 3]); 
          leg = {nbase,nbase2};
      end
    
      % Loop over plot ranges
      for ll = 1:length(lmax)
      
        % Plot all 6 spectra
        subplot(2,3,1)
        plot_bmspec(aps,leg,'TT',lmax{ll},'150',jack{jj},depj{dd})
        subplot(2,3,2)
        plot_bmspec(aps,leg,'TE',lmax{ll},'150',jack{jj},depj{dd})
        subplot(2,3,3)
        plot_bmspec(aps,leg,'EE',lmax{ll},'150',jack{jj},depj{dd})
        subplot(2,3,4)
        plot_bmspec(aps,leg,'BB',lmax{ll},'150',jack{jj},depj{dd})
        subplot(2,3,5)
        plot_bmspec(aps,leg,'TB',lmax{ll},'150',jack{jj},depj{dd})
        subplot(2,3,6)
        plot_bmspec(aps,leg,'EB',lmax{ll},'150',jack{jj},depj{dd})
        

        % Even if we just have auto, save in subfolder nbase x real cause
        % we'll populate  it eventually...
        mkdir(['plots/' nbase2]);
        savename = ['plots/' nbase2 '/' simrlz '_' simdau '_filtp3_weight3' ...
                    '_gs_dp' depj{dd} '_jack' jack{jj} '_' est '_' lmax{ll} ...
                    '.eps'];
        print('-depsc2',savename);
        clf()
        
      end % plot range
    end % jack
  end % depj

end

return