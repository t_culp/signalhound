function bmsim_combine_BK13_K2014()
%  bmsim_combine_BK13_K2014()
%  this makes the beam map sims for the 1459/????_aabd_ maps (BK14)

%queue='serial_requeue,itc_cluster';

%deprojsreal={'1100','1102'};
deprojsreal={'1102'};
deprojsreal2={'110200'};
jacks = get_default_coaddopt();
jacks = '0';

% this the latest addition to the set: K2014
%nbase1 = 1351
%daughter1 = 'd'
nbase1 = 2544
daughter1 = 'a'

% this is the BK13, the previous map deepest combination
%nbaseP = 1456
nbaseP = 2549
daughterP = 'aab'

% this is the BK14 = BK13 + K2014
%nbase2 = 1459
nbase2 = 2549
daughter2 = 'aabd'

%  real
for deproj = deprojsreal
  for j=jacks
    
    mBK = [num2str(nbaseP),'/0002_',daughterP,'_filtp3_weight3_gs_dp',deproj{:},'_jack',j,'.mat']
    mK  = [num2str(nbase1),'/0001_',daughter1,'_filtp3_weight3_gs_dp',deprojsreal2{:},'_jack',j,'1.mat']
    mS  = [num2str(nbase2),'/0001_',daughter2,'_filtp3_weight3_gs_dp',deproj{:},'_jack',j,'.mat']
    %if ~exist(['maps/',mS],'file')
      %farmit('farmfiles/coaddcoadd/','load_and_combine(mBK,mK,mS)','func',{@load_and_combine,@minimize_coaddopt},'var',{'mBK','mK','mS'},'queue',queue,'mem',10000,'maxtime',15,'submit',0)    
      load_and_combine(mBK,mK,mS);
    %end
  end
end
    
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function load_and_combine(mBK,mK,mS)
%  this needs to be adjusted year to year
  mBK
  mK
  mS

  mapBK  = load(['maps/',mBK]);
  mapK   = load(['maps/',mK]);

  % make sure you got the right ukpervolt for that year and apply it:
  mapK.coaddopt.ukpv_applied = get_ukpervolt('2014');
  mapK.ac=cal_coadd_ac(mapK.ac,mapK.coaddopt.ukpv_applied);
  mapK.ac=coadd_ac_overfreq(mapK.ac,mapK.coaddopt);
  
  mapK.ac = rmfield(mapK.ac,'wsd');  
  mapK.ac = rmfield(mapK.ac,'wcd');
  actemp=struct_merge(mapK.ac,mapBK.ac);
  ac(1,1) = actemp(1);
  % this is handpicking the right freqs to make the deepest map
  % at each frequency:
  ac(2,1)=coadd_ac_overrx(actemp([2,3]));

  mapBK.coaddopt = minimize_coaddopt(mapBK.coaddopt);
  mapK.coaddopt  = minimize_coaddopt(mapK.coaddopt);
  
  mapK.coaddopt.mapname = mK;
  mapBK.coaddopt.mapname = mBK;
  
  % Above line kills all information in coaddopt since it's a 3-element cell
  % Below allows us to make the aps!
  mapBK.coaddopt.coaddtype = 0;
  mapBK.coaddopt.jacktype = 0;
  
  coaddopt={mapBK.coaddopt;mapK.coaddopt};
  m = mapBK.m;
  
  saveandtest(['maps/',mS],'ac','coaddopt','m','-v7.3');
return

function coaddopt = minimize_coaddopt(coaddopt)
  try coaddopt = rmfield(coaddopt,'b'); end
  try coaddopt = rmfield(coaddopt,'bi'); end
  try coaddopt = rmfield(coaddopt,'bw'); end
  try coaddopt = rmfield(coaddopt,'hsmax'); end
  try coaddopt = rmfield(coaddopt,'whist'); end
  try coaddopt = rmfield(coaddopt,'devhist'); end
  try coaddopt = rmfield(coaddopt,'traj'); end
  try coaddopt = rmfield(coaddopt,'c'); end
  try coaddopt = rmfield(coaddopt,'jackmask'); end
return
