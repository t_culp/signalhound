function composite_runall_keck_2014(makesingle,unrotplots,maskmaps,...
    maskplots,rotmaps,rotplots,compmaps,compplots,splitmaps)
% composite_runall_keck_2014(makesingle,unrotplots,maskmaps,...
%                            maskplots,rotmaps,rotplots,compmaps,...
%                            compplots,splitmaps)
%
% Wrapper function to go from windowed/fitted maps and cuts to final
% composite beam maps suitable for beam map sims.  
%
% Before running this: 
%   1. ALL windowed maps in the bmrunlist should be have been made with
%      bm_makemap (the fits should all exist in the files)
%   2. ffbm_makecuts should have been run to generate the automated cuts
%   3. ffbm_makehandcuts should have been run to add visually bad maps to
%      the cut struct
%
% Note that here we're using all default options

compopt.expt = 'keck';
compopt.year = 2014;

% Gather all windowed map files from beammaps/maps into a single file
%  -> beammaps/maps_component/ffbm_2014_allcomp.mat
if makesingle
  ffbm_makesinglecompfile(compopt);
end

% Plot unrotated maps - thumbnail is total number of maps for
% this detector
compopt.hascomposite = 0;
compopt.plotdir = 'plots/keck_2014_compmaps_unrotated'; 
bmopt.expt = 'keck';
bmopt.rxNum = 'all';
bmopt.filename = '20140201';
bmopt.plotdir = compopt.plotdir;
bmopt.t1 = '';
bmopt.t2 = '';
bmopt.author = 'KSK';
bmopt.bmnum = '';
bmopt.sched = '';
bmopt.run = 'k2014';

if unrotplots
  ffbm_plotCompMaps(compopt);
  make_bmhtml(bmopt);
end

% Mask the maps - take unrotated
%   beammaps/maps_component/ffbm_2014_allcomp.mat ->
%   beammaps/maps_masked/ffbm_2014_allcomp_masked.mat
if maskmaps
  ffbm_maskground(compopt);
end

% Plot masked maps
compopt.plotdir = 'plots/keck_2014_compmaps_masked';
compopt.componentdir = 'beammaps/maps_masked';
compopt.componentfile = 'ffbm_2014_allcomp_masked';
bmopt.plotdir = compopt.plotdir;
if maskplots
  ffbm_plotCompMaps(compopt);
  make_bmhtml(bmopt);
end

% Rotate maps - take masked
%   beammaps/maps_masked/ffbm_2014_allcomp_masked.mat ->
%   beammaps/maps_rotated/ffbm_2014_allcomp_rotated.mat
if rotmaps
  ffbm_rotatemaps(compopt);
end

% Plot rotated maps
compopt.plotdir = 'plots/keck_2014_compmaps_rotated';
compopt.componentdir = 'beammaps/maps_rotated';
compopt.componentfile = 'ffbm_2014_allcomp_rotated';
bmopt.plotdir = compopt.plotdir;
if rotplots
  ffbm_plotCompMaps(compopt);
  make_bmhtml(bmopt);
end

% Composite maps - take rotated
%   beammaps/maps_rotated/ffbm_2014_allcomp_rotated.mat ->
%   beammaps/maps_composite/ffbm_2014_all_onlycommonab.mat
compopt.onlycommon_ab = 1;
compopt.compositefile = 'ffbm_2014_all_onlycommonab';
if compmaps
  ffbm_compositemaps(compopt);
end

% Plot composite maps
compopt.plotdir = 'plots/keck_2014_composites';
compopt.hascomposite = 1;
bmopt.plotdir = compopt.plotdir;
if compplots
  ffbm_plotCompMaps(compopt);
  %make_bmhtml(bmopt);
end

% Make split/sum/diff maps
compopt.makesplits = 1;
if splitmaps
  ffbm_compositemaps(compopt);
end

return




