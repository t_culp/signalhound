function runsim_2563(doreal,dorealcoadd,rlz,submit,daughter)
% runsim_2563(doreal,dorealcoadd,rlz,submit,daughter)
%
% Sim 2563: BICEP2 beam map sim matching 2063_b - this was the beam map
% sim used to obtain the uncertainty on the beam map sims in the final
% BICEP2 result - see 20140203_compositebm, 20140220_beammap_jackknife
% Uses a median-filtered full (4 degree) beam map
%
% Daughter 'a': all maps
%          'b': additional channel flags
% 
% To generate the final result:
% >> runsim_2563(1,0,0,1,'b'); % Wait for pairmaps to finish
% >> runsim_2563(0,1,0,1,'b'); % Wait for round1 coadds to finish
% >> runsim_2563(0,1,0,1,'b'); % Wait for round2 coadds to finish
% All maps should now be done - next run calcplot_b2bmsimxreal.m

nbase = 2563;

% Defaults: do nothing at all
if(~exist('doreal','var'))
  doreal = 0;
end
if(~exist('dorealcoadd','var'))
  dorealcoadd = 0;
end
if(~exist('rlz','var'))
  rlz = 0;
end
if(~exist('submit','var'))
  submit = true;
end
if(~exist('daughter','var'))
  daughter = 'b';
end

% Memory usage
memreal = 10000;
memcoadd = 4000;
memcoaddcoadd = 4000;
memcoaddcoadddp = 20000;
maxtime = 400;
maxtime_real = 400;
if dorealcoadd 
  maxtime = 12*60;
end
%if doreal 
%  maxtime = 190;
%end

% Pick appropriate queues
display('Using SLURM')
slurm_queue = 'general,serial_requeue,itc_cluster';
queue_sim = slurm_queue;
queue_coadd = slurm_queue;
queue_coadd_coadd = slurm_queue;
queue_other = slurm_queue;

% Global options
om = 1; % onlymissing option
ntpg = 4; % NTagsPerJob

% Sim options
if strcmp(get_experiment_name(),'keck')
  rlzc = 3; % rlzchunksize for runsim
end
tagc = 1; % tagchunksize for runsim

% Coadd options
js = 0; % option for FarmJacksSeparately for coadd
farmdps = 1;

% Which deprojection we want to do:
deprojs = {[0,0,0,0],
           [0,1,0,0],
           [1,1,0,0],
           [1,1,1,0],
           [1,1,0,1],
           [1,1,1,1],
           [1,1,0,2]}; 
rlz

% WHAT TAGS DO I WANT
realfile = 'maps/1450/real_a_filtp3_weight3_gs_dp0000_jack0.mat';
x = load(realfile);
tags = x.coaddopt.tags;
[tagsublist,mtl] = get_tag_sublist(x.coaddopt);
clear x;

clear simopt
simopt.rlz = rlz;
simopt.sernum = [num2str(nbase) sprintf('%03d',simopt.rlz) '1'];
simopt.siginterp = 'linear'; % for bm sims
%simopt.curveskyrotbeam = 1; % To compare to CLW sims let's not turn these on yet
%simopt.curveskyrescale = 1; 
simopt.beamcen = 'obs';
simopt.diffpoint = 'ideal'; % Since the beam maps include this
simopt.chi = 'obs';
simopt.epsilon = 'obs';

% From my stuff:
simopt.noise = 'none';
simopt.sig = 'nopol';
simopt.sigmaptype = 'healmap';
simopt.maketod = false;
simopt.interpix = 0.1;

simopt.ukpervolt = get_ukpervolt();

simopt.coord = 'C';
%simopt.force_ab_int_from_common_pix = false; % What is this?
simopt.update = 1;

clear mapopt

mapopt.beamcen = 'obs';
mapopt.chi = 'obs';
mapopt.epsilon = 'obs';
mapopt.acpack = 0;
mapopt.gs = 1;
mapopt.filt = 'p3';
mapopt.deproj = true;
mapopt.update = true;
%mapopt.curveskyrotbeam = 1; 
%mapopt.curveskyrescale = 1; 

mapopt.realpairmapset = 'pairmaps/1450/real';

clear coaddopt

chflags = get_default_chflags();
switch daughter
  case 'b'
    chflags.filebase{9} = 'fp_data/fp_data_tocut';
    chflags.par_name{9} = 'pair41';
    chflags.low(9) = -1;
    chflags.high(9) = 0.5;
end

coaddopt.chflags = chflags;
coaddopt.daughter = daughter;
coaddopt.deproj_timescale = 'by_phase';
coaddopt.filt = 'p3';
coaddopt.gs = 1;
coaddopt.jacktype = '0123456789abcde';
coaddopt.realpairmapset = '1450';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Let's work (?!)

if doreal 
  % We call it "real" but need to treat it like a sim - only difference is input maps
  
  % The weird map cooked up by god knows who
  sigmapfilename = ['input_maps/planck/planck_maps/' ...
                    'HFI_SkyMap_143_2048_R1.10_nominal_l600_00p00_cel_uk.fits'];
  mapopt.deproj_map = ['input_maps/planck/planck_derivs_nopix/' ...
                      'synfast_deproj_143_nominal_B2.fits'];
  mapopt.residbeam.beammap = ['beammaps/maps_composite/uberchopper_all_med_v4.mat'];
  mapopt.residbeam.skymap = {['input_maps/planck/planck_maps/' ...
                      'HFI_SkyMap_143_2048_R1.10_nominal_l600_00p00_cel_uk.fits']};
  mapopt.deproj = [1:6];%,9,10];
 
  simopt.mapopt = mapopt;
  simopt.beammapfilename = 'beammaps/maps_composite/clw/uberchopper_all_med_v4.mat';
  if ~exist(['simrunfiles/' num2str(nbase) '_' daughter '_dithers.mat'],'file')
    par_make_simrunfiles(tags,simopt,nbase,daughter);
  end
  
  mkdir(['pairmaps/' num2str(nbase)]);
  system(['ln -s /n/panlfs2/bicep/bicep2/pipeline/pairmaps/1450/real pairmaps/' ...
	num2str(nbase) '/real'])
  
  type = 1;
  runsim(nbase,daughter,mapopt,'nopol','none',type,sigmapfilename,rlz,mtl,...
      true,[],om,800,0,[],1,queue_sim,0,memreal,tagc,0,maxtime,submit);
 
end

if dorealcoadd
  
  for kk = 1
    kk
    
    coaddopt.sernum = sprintf('%04dxxx%01d',nbase,kk);
    coaddopt.save_cuts = false;
    coaddopt.tagsublist = tagsublist;

    if farmdps
      for jj = 1:size(deprojs,1)
	%coaddopt.deproj = deprojs(jj,:);
	coaddopt.deproj = deprojs{jj};
	farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
	    'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
	    'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadddp,...
	    'SplitSubmission',20,'UseCompiled',0,...
	    'maxtime',maxtime,'submit',submit);
      end
    else
      coaddopt.deproj = deprojs;
      farm_coaddpairmaps(tags,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
	  'FarmJobs',1,'JobLimit',800,'FarmJacksSeparately',js,...
	  'Queue',queue_coadd_coadd,'MemRequire',memcoaddcoadddp,...
	  'SplitSubmission',20,'UseCompiled',0,...
	  'maxtime',maxtime,'submit',submit);
    end
  end % simtypes
  
end % dorealcoadd

return
