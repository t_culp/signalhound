function bmsim_combine_B12_K13()
%  bmsim_combine_B12_K13()
%  this makes the beam map sims for the 1456/????_aab_ maps (BK13) 

% CLW's B2 map corresponding to sim 0751
% and coadded Keck map corresponding to 1351_ab
for j='0'
  mapB12 = (['2063/0002_b_filtp3_weight3_gs_dp1102_jack',j,'.mat']);
  mapK13 = (['2086/0002_ab_filtp3_weight3_gs_dp1102_jack',j,'.mat']);
  mapBK13= (['2549/0002_aab_filtp3_weight3_gs_dp1102_jack',j,'.mat']);
  load_and_combine(mapB12,mapK13,mapBK13)
  %farmit('farmfiles/coaddcoadd/','load_and_combine(mapK13,mapB12,mapK13)','func',{@load_and_combine,@minimize_coaddopt},'var',{'mapK13','mapB12','mapK13'},'queue',queue,'mem',10000,'maxtime',15,'submit',0)    
end
      
return


function load_and_combine(mapB12,mapK13,mapBK13)
%  this needs to be adjusted year to year
  mapB12
  mapK13
  mapBK13
  
  mB12  = load(['maps/',mapB12]);
  mK13  = load(['maps/',mapK13]);
    
  % make sure you got the right ukpervolt for that year here:
  mB12.coaddopt.ukpv_applied = 3150;
  mB12.ac=cal_coadd_ac(mB12.ac,mB12.coaddopt.ukpv_applied);
  
  % for K13 nothing needs to be done...
  %
  
  ac = struct_merge(mB12.ac,mK13.ac);
  ac = coadd_ac_overrx(ac);
  
  mB12.coaddopt = minimize_coaddopt(mB12.coaddopt);
  
  mB12.coaddopt.mapname = mapB12;
  
  coaddopt={mB12.coaddopt,mK13.coaddopt{:}};
  m = mK13.m;
  
  saveandtest(['maps/',mapBK13],'ac','coaddopt','m','-v7.3');
return

function coaddopt = minimize_coaddopt(coaddopt)
  try coaddopt = rmfield(coaddopt,'b'); end
  try coaddopt = rmfield(coaddopt,'bi'); end
  try coaddopt = rmfield(coaddopt,'bw'); end
  try coaddopt = rmfield(coaddopt,'hsmax'); end
  try coaddopt = rmfield(coaddopt,'whist'); end
  try coaddopt = rmfield(coaddopt,'devhist'); end
  try coaddopt = rmfield(coaddopt,'traj'); end
  try coaddopt = rmfield(coaddopt,'c'); end
  try coaddopt = rmfield(coaddopt,'jackmask'); end
return
