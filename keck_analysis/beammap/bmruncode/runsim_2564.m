function runsim_2564(doreal,dosim,docoadd)
% function runsim_2564(doreal,dosim,docoadd)
% 
% Script to produce the core BICEP2 beam map sim result (CDS sim 2244)
% but updated for current pipeline.  See 20160607_b2bmsim_maps post for
% more details.
%
% r<1.2 mean filtered beam maps + B(l) + 
% current best ukpv normalization. Like 2238 but cut down to r<1.2.

% setup options
dotags = 1;
rlz = 0;
submit = 0;

% what do we do?
%doreal=0
%dosim=0
%docoadd=1

% global options
om = 1;  % supplies 'OnlyMissing' option for both runsim and coadds

% real options
nptgr = 1; % NTagsPerJob
qrl = 'general,serial_requeue,itc_cluster'; % real pairmaps queue

% simulation options
rlzc = 1; % rlzchunksize for runsim
tagc = 1; % tagchunksize for runsim
qr = 'general,serial_requeue,itc_cluster'; % queue for runsim jobs

% coadd options
dpts = 'by_phase'; % deprojection timescale for coadd
js = 1; % option for FarmJacksSeparately for coadd
maxtime = 400;
q = 'general,serial_requeue,itc_cluster'; % queue for coadd jobs

rlz;

nm = get_experiment_name;

if dotags

  if strcmp(nm,'bicep2')
    % Get B2 tag subset tags
    x = load('maps/2152/0011_a_filtp3_weight3_gs_dp0000_jack0.mat');
  else
    x = load('maps/2153/0012_a_filtp3_weight3_gs_dp0000_jack01.mat');
  end

  tags = x.coaddopt.tags;
  
end

for nbase = 2564
  
  clear simopt
  sigmapfilename = 'input_maps/planck/planck_maps/HFI_SkyMap_143_2048_R1.10_nominal_l600_00p00_cel_uk.fits';
  
  simopt.siginterp = 'linear';
  simopt.beamcen = 'obs';
  simopt.coord = 'C';
  simopt.diffpoint = 'ideal';
  simopt.epsilon = 'ideal';
  simopt.beammapfilename = 'beammaps/maps_composite/cds/uberchopper_mean_no41_cut_blplusbestukpvnorm.mat';
  simopt.interpix = 0.1;
  
  daughter = 'a';
  
  clear mapopt
  mapopt.beamcen = 'obs';
  mapopt.gs = 1;
  mapopt.filt = 'p3';
  mapopt.deproj = true;
  mapopt.epsilon = 'ideal';
  if strcmp(nm,'bicep2')
    mapopt.realpairmapset = 'pairmaps/1450/real';
  else
    mapopt.realpairmapset = 'pairmaps/1350/real';
  end
  
  if doreal
    mapopt.sernum=sprintf('%04dreal',nbase);
    mapopt.deproj_map='input_maps/planck_derivs/HFI_SkyMap_143_0512_R1.10_nominal_31p22.fits';
    farm_makepairmaps(tags,mapopt,'JobLimit',600,'NTagsPerJob',nptgr,'FarmJobs',1, ...
                      'OnlyMissing',om,'Queue',qrl);
  end

  if dosim
    if ~exist(['simrunfiles/' num2str(nbase) '_' daughter '_dithers.mat'],'file')
      par_make_simrunfiles(tags,simopt,nbase,daughter);
    end
    mapopt.deproj_map='input_maps/planck_derivs/HFI_SkyMap_143_0512_R1.10_nominal_31p22.fits';

    % This is from regular runsim scripts
    %      runsim(nbase,daughter,mapopt,'nopol','none',type,sigmapfilename,rlz,mtl,...
    %  true,[],om,800,0,[],1,queue_sim,0,memreal,tagc,0,maxtime,submit);
    runsim(nbase,daughter,mapopt,'nopol','none',2,sigmapfilename,rlz, ...
           tags,0,rlzc,om,600,0,[],1,qr,[],[],tagc,0,400,submit);    
    % Note here subsim = 0 (a la CDS)
  end
  
  if docoadd
    
    clear coaddopt
    
    for k=2
      
      coaddopt.sernum=sprintf('%04dxxx%01d',nbase,k);
      
      chflags=get_default_chflags;
      coaddopt.chflags=chflags;
      coaddopt.daughter='a';
      
      if strcmp(nm,'bicep2');
        coaddopt.realpairmapset='1450';
        coaddopt.coaddtype=0;
      else
        coaddopt.realpairmapset='1350';
        coaddopt.coaddtype=1;
      end
      
      % DISABLE ROUND2 CUTS - for tag subset sim
      
      %load('/n/home06/csheehy/bicep2_analysis/cutstruct_for_round2_off');
      cut.elnod_fracdel = Inf;
      cut.elnod_ab_ba = Inf;
      cut.elnod_nancount = Inf;
      cut.elnod_mean = [-Inf Inf];
      cut.elnod_median = [-Inf Inf];
      cut.elnod_gof = Inf;
      cut.elnod_chisq_dif = Inf;
      cut.rtes_frac = [-Inf Inf];
      cut.rnorm = [0 Inf];
      cut.pjoule = [0 Inf];
      cut.fp_cor = Inf;
      cut.skeness_dif = Inf;
      cut.scanset_std = Inf;
      cut.fb_wn_sd_p0 = Inf;
      cut.fb_1f_sd_p0 = Inf;
      cut.num_fj = Inf;
      cut.num_destep = Inf;
      cut.max_fj_gap = 0;
      cut.stationarity_ab = [-Inf Inf];
      cut.stationarity_dif = [-Inf Inf];
      cut.tfpu_mean = [-Inf Inf];
      cut.tfpu_std = Inf;
      cut.enc_az_diff = Inf;
      cut.az_range = Inf;
      cut.passfrac_halfscan = 0;
      cut.passfrac_scanset = 0;

      coaddopt.cut=cut;
      
      mapopt.sernum=sprintf('%04d0002',nbase);
      ind=has_pairmap(tags,mapopt);
      tags0=tags(ind);
      
      % No deprojection
      coaddopt.deproj=[0,0,0,0];
      farm_coaddpairmaps(tags0,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                         'FarmJobs',1,'JobLimit',600,'FarmJacksSeparately',js,'Queue',q,...
                         'MemRequire',12000,'maxtime',maxtime);

      % deproject dp
      coaddopt.deproj=[0,1,0,0];
      farm_coaddpairmaps(tags0,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                         'FarmJobs',1,'JobLimit',600,'FarmJacksSeparately',js,'Queue',q,...
                         'MemRequire',12000,'maxtime',maxtime);
      
      % deproject dp+rg
      coaddopt.deproj=[1,1,0,0];
      farm_coaddpairmaps(tags0,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                         'FarmJobs',1,'JobLimit',600,'FarmJacksSeparately',js,'Queue',q,...
                         'MemRequire',12000,'maxtime',maxtime);
      
      % deproject dp+rg+bw
      coaddopt.deproj=[1,1,1,0];
      farm_coaddpairmaps(tags0,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                         'FarmJobs',1,'JobLimit',600,'FarmJacksSeparately',js,'Queue',q,...
                         'MemRequire',12000,'maxtime',maxtime);
      
      % deproject dp+rg+ellip
      coaddopt.deproj=[1,1,0,1];
      farm_coaddpairmaps(tags0,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                         'FarmJobs',1,'JobLimit',600,'FarmJacksSeparately',js,'Queue',q,...
                         'MemRequire',12000,'maxtime',maxtime);

      % deproject dp+rg+ellip
      coaddopt.deproj=[1,1,0,2];
      farm_coaddpairmaps(tags0,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                         'FarmJobs',1,'JobLimit',600,'FarmJacksSeparately',js,'Queue',q,...
                         'MemRequire',12000,'maxtime',maxtime);

      % deproject all
      coaddopt.deproj=[1,1,1,1];
      farm_coaddpairmaps(tags0,coaddopt,'OnlyMissing',om,'Realizations',rlz,...
                         'FarmJobs',1,'JobLimit',600,'FarmJacksSeparately',js,'Queue',q,...
                         'MemRequire',12000,'maxtime',maxtime);
      
    end
  end
end


return


