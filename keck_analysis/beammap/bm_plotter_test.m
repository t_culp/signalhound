function bm_plotter(bmopt)
% bm_plotter(bmopt)
%
% Function to plot standard far field beam maps.
% Makes small thumbnails (from windowed map) and large plots with fits (from
% full and windowed maps)
%
% INPUTS (should all be passed in with bmopt)
%
%   expt:      'b2','keck','b3'
%   rxNum:     For Keck: rx number, array of rx, or 'all' (default)
%   mapcoord:  'azel_ap' (default),'raw' (used to get map filename)
%   demodtype:   'square' (default), 'choplet'
%   bmdir:     directory from which to load beammaps, beammaps/maps default
%   suffix:    optional string to add to map name, '' default
%   filename:  string with timestamp of map file (i.e. '20150101_000000' or
%              p = get_bm_info(year); filename = p.filename{bmnum};)
%   plotdir:   directory in which to save images.  Will create if doesn't
%              exist, default plots/filename
%
% OPTIONAL
%
%   clim_sum:  color limits for normal maps, default is expt-dependent
%   clim_dif:  color limits for difference maps, default [-0.1 0.1]
%   dosmall:   1 (default) to make small maps, 0 to skip
%   dofull:    1 (default) to make full maps, 0 to skip
%   fullaxis:  limits for full map, i.e. [-20 20 -13 23].  Defaults is
%              max/min of the fullmap, which may extend annoyingly far. 

% Parse bmopt and set defaults
expt = bmopt.expt;
if ~isfield(bmopt,'rxNum')
  switch expt
    case 'keck'
      rxNum = 0:4;
  end
else
  if strcmp(bmopt.rxNum,'all')
    rxNum = 0:4;
  else
    rxNum = bmopt.rxNum;
  end
end
if ~isfield(bmopt,'mapcoord')
  mapcoord = 'azel_ap';
else
  mapcoord = bmopt.mapcoord;
end
if ~isfield(bmopt,'demodtype')
  demodtype = 'square';
else
  demodtype = bmopt.demodtype;
end
if ~isfield(bmopt,'bmdir')
  bmdir = 'beammaps/maps';
else
  bmdir = bmopt.bmdir;
end
if ~isfield(bmopt,'suffix')
  suffix = '';
else
  suffix = bmopt.suffix;
end
filename = bmopt.filename;
if ~isfield(bmopt,'plotdir')
  plotdir = ['plots/' filename];
else
  plotdir = bmopt.plotdir;
end
if ~isfield(bmopt,'dosmall')
  dosmall = 1;
else
  dosmall = bmopt.dosmall;
end
if ~isfield(bmopt,'dolarge')
  dolarge = 1;
else
  dolarge = bmopt.dolarge;
end
% If empty, call subfunction in plot loop to figure out clim_sum
% We save to a different name here because it can change in the loop
if ~isfield(bmopt,'clim_sum')
  clim_sum_in = [];
else
  clim_sum_in = bmopt.clim_sum;
end
if ~isfield(bmopt,'clim_dif')
  clim_dif = [-0.1 0.1];
else
  clim_dif = bmopt.clim_dif;
end
if ~isfield(bmopt,'fullaxis')
  fullaxis = [];
else
  fullaxis = bmopt.fullaxis;
end

% Load up saved maps
fmapname = [bmdir,'/map_',filename,'_',demodtype,...
      '_',mapcoord,suffix];
f = load(fmapname);
f = f.bm;
wmapname = [bmdir,'/mapwin_',filename,'_',demodtype,...
      '_',mapcoord,suffix];
w = load(wmapname);
w = w.bm;

% Make directories
switch expt
  case {'b2','b3'}
    mkdir(plotdir);
    mkdir([plotdir '/small']);
    mkdir([plotdir '/full']);
  case 'keck'
    for ii = 1:length(rxNum)
      mkdir([plotdir '_rx' num2str(rxNum(ii))]);
      mkdir([plotdir '_rx' num2str(rxNum(ii)) '/small']);
      mkdir([plotdir '_rx' num2str(rxNum(ii)) '/full']);
    end
end

[pp ind] = get_array_info(filename(1:8));

switch expt
  case {'b2','b3'}
    %f.dk = f.dk + 90 + 180;
    %f.dk = double(f.dk);
    dk = f.dk + pp.drumangle;
  case 'keck'
    cutind = ismember(pp.rx,rxNum);
    pp = structcut(pp,cutind);
    ind = make_ind(pp);
    % Cut down more stuff for each rx
    f.map = f.map(:,:,cutind);
    w.map = w.map(:,:,cutind);
    w.A = w.A(:,cutind);
    %f.dk = 90 + (f.dk + pp.drumangle(1));  % CHANGE for individual dets
    dk = 90 + (f.dk + pp.drumangle);
end 

% Gaussian fit from bm_makemap
F = w.A;

% Small plots (thumbnails)
if dosmall

  fh = figure(1);
  set(fh,'Position',[10 10 670 500],'Visible','off') % For cropping to work 
  clf
  colormap('jet')
  
  for ii = 1:length(ind.la)
    if mod(ii,10) == 0
      disp([filename sprintf(': Small map, rx%i, %i/%i',pp.rx(ind.la(ii)),...
	  ii,length(ind.la))]);
    end
    
    ro = int2str(pp.det_row(ind.la(ii)));
    co = int2str(pp.det_col(ind.la(ii)));
    ti = int2str(pp.tile(ind.la(ii)));
    
    A = imrotate(w.map(:,:,ind.la(ii)),dk(ind.la(ii)),'bicubic');
    B = imrotate(w.map(:,:,ind.lb(ii)),dk(ind.la(ii)),'bicubic');
    
    switch expt
      case {'b2','b3'}
	subplotdir = plotdir;
      case {'keck'}
	subplotdir = [plotdir '_rx' num2str(pp.rx(ind.la(ii)))];
    end
    
    % Get clim_sum
    if isempty(clim_sum_in)
      clim_sum = get_clim_sum(expt,pp,ind,ii,filename);
    else
      clim_sum = clim_sum_in;
    end
    
    % Plot and save A pol
    imagesc(w.x_bin,w.y_bin,A)
    prettify_small(clim_sum);
    img_str = [subplotdir '/small/det_row_' ro '_col_' co ...
	  '_tile_' ti '_A_sm'];
    print('-dpng', img_str);
    
    % Plot and save B pol
    imagesc(w.x_bin,w.y_bin,B)
    prettify_small(clim_sum);
    img_str = [subplotdir '/small/det_row_' ro '_col_' co ...
	  '_tile_' ti '_B_sm'];
    print('-dpng', img_str);
    
    % Plot and save A/|A|-B/|B|
    imagesc(w.x_bin,w.y_bin,A./w.A(1,ind.la(ii)) - B./w.A(1,ind.lb(ii)))
    prettify_small(clim_dif);
    img_str = [subplotdir '/small/det_row_' ro '_col_' co ...
	  '_tile_' ti '_dif_sm'];
    print('-dpng', img_str);
    
    % Pause for a split second to let the files appear
    pause(0.5)
    
    % Crop
    system(['mogrify -crop ''720x720+253+67'' ' subplotdir '/small/det_row_' ro '_col_' co '_tile_' ti '_*']);
    
    % Shrink
    system(['mogrify -resize ''7%x7%'' ' subplotdir '/small/det_row_' ro '_col_' co '_tile_' ti '_*']);
    
  end % over detectors
end % dosmall

if dolarge
  fh = figure(1);
  clf

  x_win = w.x_bin - mean(w.x_bin);
  y_win = w.y_bin - mean(w.y_bin);

  % For log plots
  f.map(f.map < 0) = 0;

  % Create full plots
  set(fh,'Position',[0 0 1600 800],'visible','off')

  for ii = 1:length(ind.la)
    if mod(ii,10) == 0
      disp([filename sprintf(': Full map, rx%i, %i/%i',pp.rx(ind.la(ii)),...
	  ii,length(ind.la))]);
    end
    clf
    
    ro = int2str(pp.det_row(ind.la(ii)));
    co = int2str(pp.det_col(ind.la(ii)));
    ti = int2str(pp.tile(ind.la(ii)));
    gcpA = int2str(pp.gcp(ind.la(ii)));
    gcpB = int2str(pp.gcp(ind.lb(ii)));
    
    A = f.map(:,:,ind.la(ii));
    B = f.map(:,:,ind.lb(ii));
    ZA = gauss2d(F(:,ind.la(ii)),w.x_bin,w.y_bin,'std');
    ZB = gauss2d(F(:,ind.lb(ii)),w.x_bin,w.y_bin,'std');
    WA = w.map(:,:,ind.la(ii));
    WB = w.map(:,:,ind.lb(ii));
    
    % Map amplitudes
    mA = w.A(1,ind.la(ii));
    mB = w.A(1,ind.lb(ii));
    
    switch expt
      case {'b2','b3'}
	subplotdir = plotdir;
      case {'keck'}
	subplotdir = [plotdir '_rx' num2str(pp.rx(ind.la(ii)))];
    end  

    % Get clim_sum
    if isempty(clim_sum_in)
      clim_sum = get_clim_sum(expt,pp,ind,ii,filename);
    else
      clim_sum = clim_sum_in;
    end
      
    % Plot and save A pol
    % full map
    subplot(3,4,[1 11])
    imagesc(f.x_bin,f.y_bin,log10(A))
    title(['Det row ' ro ', col ' co ', tile ' ti ',  GCP ' gcpA ...
	  ', pol A, log10 scale'])
    prettify_large_large([log10(clim_sum(2))-3 log10(clim_sum(2))],fullaxis);
    % Windowed map 
    subplot(3,4,4)
    imagesc(x_win,y_win,WA)
    title('Main beam')
    prettify_large(clim_sum) 
    % Beam fit
    subplot(3,4,8)
    imagesc(x_win,y_win,ZA)
    title('Fit')
    prettify_large(clim_sum) 
    % Residual
    subplot(3,4,12)
    imagesc(x_win,y_win,(WA - ZA)./mA)  
    title('Residual')
    
    prettify_large(clim_dif) 
    img_str = [subplotdir '/full/det_row_' ro '_col_' co '_tile_' ti '_A'];
    mkpng(img_str);

    % Plot and save B pol
    % Full map
    subplot(3,4,[1 11])
    imagesc(f.x_bin,f.y_bin,log10(B))
    title(['Det row ' ro ', col ' co ', tile ' ti ',  GCP ' gcpB ...
	  ', pol B, log10 scale'])
    prettify_large_large([log10(clim_sum(2))-3 log10(clim_sum(2))],fullaxis);  
    % Windowed map 
    subplot(3,4,4)
    imagesc(x_win,y_win,WB)
    title('Main beam')
    prettify_large(clim_sum)
    % Beam fit
    subplot(3,4,8)
    imagesc(x_win,y_win,ZB)
    title('Fit')
    prettify_large(clim_sum)
    % Residual
    subplot(3,4,12)
    imagesc(x_win,y_win,(WB - ZB)./mB)  
    title('Residual')
    prettify_large(clim_dif)
    
    img_str = [subplotdir '/full/det_row_' ro '_col_' co '_tile_' ti '_B'];
    mkpng(img_str);

    % Plot and save A/|A|-B/|B|  
    % Full map
    subplot(3,4,[1 11])
    imagesc(f.x_bin,f.y_bin,A./mA - B./mB)
    title(['Det row ' ro ', col ' co ', tile ' ti ',  GCP ' gcpB ...
	  ', A-B'])
    prettify_large_large(clim_dif,fullaxis);
    % Windowed map 
    subplot(3,4,4)
    imagesc(x_win,y_win,WA./mA - WB./mB)
    title('Main beam')
    prettify_large(clim_dif)
    % Beam fit
    subplot(3,4,8)
    imagesc(x_win,y_win,ZA./mA - ZB./mB)
    title('Fit')
    prettify_large(clim_dif)
    % Residual
    subplot(3,4,12)
    imagesc(x_win,y_win,(WA - ZA)./mA - (WB - ZB)./mB)
    title('Residual')
    prettify_large([-.02 .02])
  
    img_str = [subplotdir '/full/det_row_' ro '_col_' co '_tile_' ti '_dif'];
    mkpng(img_str);
    
  end
end
close all;

return % Main function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function clim_sum = get_clim_sum(expt,pp,ind,ii,filename)
% Color axes are unfortunately experiment and frequency-specific, so choose
% them here

bmtime = date2mjd(str2num(filename(1:4)),str2num(filename(5:6)),...
    str2num(filename(7:8)),str2num(filename(10:11)),...
    str2num(filename(12:13)),str2num(filename(14:15)));

switch expt
  case 'b2'
    clim_sum = [0 300];
  case 'keck'
    % Before 2016 we used the 18" chopper
    if bmtime < date2mjd(2016)
      switch pp.band(ind.la(ii))
	case {100,220}
	  clim_sum = [0 200];
	case 150
	  clim_sum = [0 300];
      end
    else
      clim_sum = [0 800];
    end
  case 'b3'
    % Before 2015-03-17 we had the 24" and after the 18"
    % For 2016 24" 
    if bmtime < date2mjd(2015,03,17,00,00,00)
      clim_sum = [0 200];
    else
      clim_sum = [0 100];
    end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function prettify_small(cax)

caxis(cax)
axis equal
set(gca,'xtick',[],'ytick',[])
set(gca,'YDir','normal')

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function prettify_large_large(cax,fullaxis)

set(gca,'YDir','normal')
colorbar('location','eastoutside')
caxis(cax)
axis equal
if ~isempty(fullaxis)
  axis(fullaxis)
else
  axis tight
end
ylabel('Degrees')

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function prettify_large(cax)

set(gca,'YDir','normal')
colorbar('location','eastoutside')
caxis(cax)
axis equal
axis tight
ylabel('Degrees')

return
