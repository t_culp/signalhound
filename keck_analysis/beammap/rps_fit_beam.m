function [rpsparam, bestfit] = rps_fit_beam(rpstod, rpsopt, fit_plot)
% rpsparam = rps_fit_beam(rpstod, rpsopt, fit_plot)
%
% Fit demodulated TOD to elliptical Gaussian beam model.
%
% The fit model is given by:
%   model = A * exp([rx - x0; ry - y0] * C^-1 * [rx - x0, ry - y0]) + B
% where C^-1 is an inverse covariance matrix for the elliptical Gaussian:
%   C^-1 = |         sigma_x^2,        e_xy * sigma_x * sigma_y |^-1
%          | e_xy * sigma_x * sigma_y,         sigma_y^2        |
%
% The coordinates used for the fit, (rx, ry), are the Lambert azimuthal
% equal-area projection calculated from the (r, theta) coordinates of the
% source relative to each detector.
%   rx = 2 * sin(r / 2) * cos(theta) * (180 / pi)
%   ry = 2 * sin(r / 2) * sin(theta) * (180 / pi)
%
% Model fitting is accomplished by using fminsearch to minimize chi^2. The 
% chi^2 statistic is unweighted and unnormalized, so it does not provide a 
% measure of goodness-of-fit.
%
% Full list of fit parameters, in the order returned, is:
%   rpsparam.fit.param(1): A         Amplitude
%   rpsparam.fit.param(2): x0        Beam center rx position
%   rpsparam.fit.param(3): y0        Beam center ry position
%   rpsparam.fit.param(4): sigma_x   Beam width in rx direction
%   rpsparam.fit.param(5): sigma_y   Beam width in ry direction
%   rpsparam.fit.param(6): e_xy      Beam x-y correlation coefficient
%   rpsparam.fit.param(7): B         Background offset level
%
% [Arguments]
%   rpstod    Data structure containing demodulated TOD and pointing 
%             coordinates. See rps_read.m.
%   rpsopt    Data structure controlling options for RPS analysis.
%   fit_plot  Set this optional argument to produce a map of the beam 
%             (from data) with the best fit beam indicated. If you set this 
%             argument to a string like 'myplot.eps' or 'myplot.png', then
%             the figure will be saved to the desired file name and format 
%             (only eps and png supported). Your filename can also include
%             '[CH]', which will be replaced by the matlab channel index 
%             for each plot, or '[DATE]', which will be replaced by the 
%             date/time for the start of the scan.
%
%   Fields for rpsopt data structure:
%     rpsopt.demod_type  Select which demodulated timestream to fit. 
%                        Defaults to 'cos'. Other options are 'sin' or
%                        'quad'.
%     rpsopt.beamwidth   Value to use as the initial guess for the beamwidth.
%                        This option does not actually constrain the fit 
%                        value of beam width. Default is 0.22, which is
%                        appropriate for 150 GHz. Use 0.33 for 100 GHz.
%     rpsopt.window_x    Specify a range of the rx coordinate to use for 
%                        fitting. Data outside this range will be excluded.
%                        Defaults to no windowing, but this can be useful if
%                        junk in the data is screwing up the fit.
%     rpsopt.window_y    Specify a range of the ry coordinate to use for 
%                        fitting. Data outside this range will be excluded.
%                        Defaults to no windowing.
%
% [Returns]
%   rpsparam  Data structure containing best fit parameters and fit 
%             diagnostics.
%   bestfit   Cell array containing time-ordered data derived from
%             the best fit model for each detector.
%
%   Fields for rpsparam data structure:
%     rpsparam.scan    Scan data structure copied from rpstod input.
%     rpsparam.ch      Channel list copied from rpstod input.
%     rpsparam.p       Detector parameter data structure for selected 
%                      channels only, copied from rpstod input.
%     rpsparam.rpsopt  Record of the rpsopt argument.
%     rpsparam.fit     Array of rpsparam.fit data structures.
%
%   Fields for rpsparam.fit data structure:
%     rpsparam.fit.param     Array of best fit model parameters, in
%                            the order listed above.
%     rpsparam.fit.fval      Chi^2 value for the best fit model.
%     rpsparam.fit.flag      Flag indicating fit status. See documentation 
%                            for fminsearch.
%     rpsparam.fit.psi       Psi coordinate for the tod sample closest to 
%                            the beam peak location.
%     rpsparam.fit.psi_dist  Offset in degrees of the tod sample used to 
%                            measure psi from the beam peak location.

% Last update: 2014-02-07 CAB

% Fill out rpsopt data structure with default parameters, if necessary.
if ~exist('rpsopt', 'var')
  rpsopt = [];
end
% Default to fitting cosine demodulated timestream.
if ~isfield(rpsopt, 'demod_type')
  rpsopt.demod_type = 'cos';
end
% Default to beam width appropriate for 150 GHz.
if ~isfield(rpsopt, 'beamwidth')
  rpsopt.beamwidth = 0.22;
end
% Default to using no window.
if ~isfield(rpsopt, 'window_x')
  rpsopt.window_x = [];
end
if ~isfield(rpsopt, 'window_y')
  rpsopt.window_y = [];
end

% Calculate Lambert azimuthal equal-area projection.
% Apply conversion so rx, ry have units of degrees.
rx = 2 * sind(rpstod.r / 2) .* cosd(rpstod.theta) * 180 / pi;
ry = 2 * sind(rpstod.r / 2) .* sind(rpstod.theta) * 180 / pi;

% Copy values to output data structure.
rpsparam.scan = rpstod.scan;
rpsparam.ch = rpstod.ch;
rpsparam.p = rpstod.p;
rpsparam.rpsopt = rpsopt;

% Fit parameters:
%   param(1) = peak amplitude
%   param(2) = x center
%   param(3) = y center
%   param(4) = sigma-x
%   param(5) = sigma-y
%   param(6) = x-y correlation coefficient
%   param(7) = background level

% Loop over detectors.
nchan = length(rpstod.ch);
for i=1:nchan
  % Create a window around the source position.
  % If no window is specified, then keep all data.
  if ~isempty(rpsopt.window_x)
    window_x = (rx(:,i) > rpsopt.window_x(1)) & ...
        (rx(:,i) < rpsopt.window_x(2));
  else
    window_x = true(size(rx(:,i)));
  end
  if ~isempty(rpsopt.window_y)
    window_y = (ry(:,i) > rpsopt.window_y(1)) & ...
        (ry(:,i) < rpsopt.window_y(2));
  else
    window_y = true(size(ry(:,i)));
  end
  window = window_x & window_y;
  % Apply window.
  x = rx(window,i);
  y = ry(window,i);
  % Also, choose which timestream to fit.
  if strcmp(rpsopt.demod_type, 'sin')
    % Sine demodulation.
    z = rpstod.todsin(window,i);
  elseif strcmp(rpsopt.demod_type, 'quad')
    % Quadrature sum of sine and cosine.
    z = sqrt(rpstod.todcos(window,i).^2 + rpstod.todsin(window,i).^2);
  else
    % Cosine demodulation (default).
    z = rpstod.todcos(window,i);
  end

  % Guess initial parameters.
  amp = max(abs(z));
  indpeak = find(abs(z) == amp);
  indpeak = indpeak(1);
  guess = [z(indpeak), x(indpeak), y(indpeak), ...
           rpsopt.beamwidth, rpsopt.beamwidth, 0, 0];
  
  % Fit data.
  [param, fval, flag] = fminsearch(...
    @(param) rps_beam_chi2(param, x, y, z), guess);
  
  % Record fit to rpsfit structure.
  rpsparam.fit(i).param = param;
  rpsparam.fit(i).fval = fval;
  rpsparam.fit(i).flag = flag;
  
  % Record best-fit model.
  bestfit{i} = rps_beam_model(param, rx(:,i), ry(:,i));

  % Find psi value closest to the beam center.
  distance = sqrt((rx(:,i) - param(2)).^2 + (ry(:,i) - param(3)).^2);
  rpsparam.fit(i).psi = rpstod.psi(distance == min(distance), i);
  rpsparam.fit(i).psi_dist = min(distance);

  % Optional fit plot.
  if ~isempty(fit_plot)
    % Default file name.
    plot_prefix = 'rps_beam_[CH]_[DATE]';
  
    % Plot formats: 
    %   0: eps (default)
    %   1: png
    plot_format = 0;
    
    % Can specify prefix and/or format by passing a string to the
    % fit_plot argument.
    if isstr(fit_plot)
      % Is .eps specified?
      tokens = regexp(fit_plot, '(.*).eps', 'tokens');
      if ~isempty(tokens)
        plot_format = 0; % eps
        plot_prefix = tokens{1}{1};
      end
      
      % Is .png specified?
      tokens = regexp(fit_plot, '(.*).png', 'tokens');
      if ~isempty(tokens)
        plot_format = 1; % png
        plot_prefix = tokens{1}{1};
      end
    end

    % Substitude in channel number and date to plot prefix.
    plot_prefix = strrep(plot_prefix, '[CH]', ...
                         sprintf('%04i', rpsparam.ch(i)));
    plot_prefix = strrep(plot_prefix, '[DATE]', ...
                         mjd2datestr(rpsparam.scan.t1, 'yyyymmdd_HHMMSS'));

    % Make beam map.
    range = 4; % Covers [-range, range] for rx, ry.
    binsize = 0.2; % Size of square pixels.
    xbin = [-range:binsize:range];
    ybin = xbin;
    if strcmp(rpsopt.demod_type, 'sin')
      beam_map = grid_map(rx(:,i), ry(:,i), rpstod.todsin(:,i), xbin, ybin);
    elseif strcmp(rpsopt.demod_type, 'quad')
      beam_map = grid_map(rx(:,i), ry(:,2), sqrt(rpstod.todcos(:,i).^2 + ...
                                                 rpstod.todsin(:,i).^2), ...
                          xbin, ybin);
    else
      beam_map = grid_map(rx(:,i), ry(:,i), rpstod.todcos(:,i), xbin, ybin);
    end

    % Set up figure.
    fig = figure('Visible', 'off');
    set(fig, 'Position', [0 0 800 600], 'PaperPosition', [0 0 8 6]);

    % Draw beam map.
    colormap gray;
    imagesc(xbin, ybin, beam_map);
    set(gca, 'YDir', 'normal');
    axis square;
    cb = colorbar();

    % If we are windowing the data, draw the window.
    if ~isempty(rpsopt.window_x) & ~isempty(rpsopt.window_y)
      rt = rectangle('Position', [rpsopt.window_x(1), rpsopt.window_y(1), ...
                          diff(rpsopt.window_x), diff(rpsopt.window_y)]);
      set(rt, 'EdgeColor', [1 0 0]);
    end

    % Draw elliptical 1-sigma contour for best fit beam.
    % 1. Solve quadratic equation for major/minor axes.
    sigma_maj_2 = (param(4)^2 + param(5)^2 + ...
                   sqrt((param(4)^2 + param(5)^2)^2 - 4 * ...
                        (1 - param(6)^2) * param(4)^2 * param(5)^2)) / 2;
    sigma_maj = sqrt(sigma_maj_2);
    sigma_min_2 = (param(4)^2 + param(5)^2 - ...
                   sqrt((param(4)^2 + param(5)^2)^2 - 4 * ...
                        (1 - param(6)^2) * param(4)^2 * param(5)^2)) / 2;
    sigma_min = sqrt(sigma_min_2);
    % 2. Find the rotation angle of the ellipse (in radians!).
    phi = 0.5 * asin(2 * param(6) * param(4) * param(5) / ...
                     (sigma_maj_2 - sigma_min_2));
    cp = cos(phi);
    sp = sin(phi);
    % 3. Draw ellipse.
    hold all;
    rad = [0:0.01:2*pi];
    plot(param(2) + cp * sigma_min * cos(rad) - sp * sigma_maj * sin(rad), ...
         param(3) + sp * sigma_min * cos(rad) + cp * sigma_maj * sin(rad), ...
         'r');
    % 4. Draw semi-major and semi-minor axes.
    plot(param(2) + cp * [-1, 1] * sigma_min, ...
         param(3) + sp * [-1, 1] * sigma_min, 'r');
    plot(param(2) - sp * [-1, 1] * sigma_maj, ...
         param(3) + cp * [-1, 1] * sigma_maj, 'r');
         
    % Labels.
    xlabel('r_x');
    ylabel('r_y');
    ylabel(cb, 'ADU');
    % Print fit parameters as title.
    title(sprintf('[%0.3f, %0.3f, %0.3f, %0.3f, %0.3f, %0.3f, %0.3f]', ...
                  param));

    % Print figure.
    if plot_format == 0
      figname = [plot_prefix '.eps'];
      print(figname, fig, '-depsc');
    elseif plot_format == 1
      figname = [plot_prefix '.png'];
      print(figname, fig, '-dpng');
    end
    close(fig);
  end
end


% Subfunction: rps_beam_model
% ---
% Calculates the elliptical Gaussian model for vector of parameters, p, and 
% coordinates (x, y).
function model = rps_beam_model(p, x, y)

% Offset coordinates.
dx = x - p(2);
dy = y - p(3);
% Covariance matrix for beam ellipticity.
cov = [p(4)^2, p(6) * p(4) * p(5); ...
  p(6) * p(4) * p(5), p(5)^2];
icov = inv(cov);
% Elliptical Gaussian model.
model = p(7) + p(1) * exp(-0.5 * ...
  (dx.^2 * icov(1,1) + dy.^2 * icov(2,2) + 2 * dx .* dy * icov(1,2)));


% Subfunction: rps_beam_chi2
% ---
% Calls rps_beam_model with parameters p and coordinates (x, y). Then 
% calculates the chi^2 statistic for this model from data z.
% This chi^2 statistic is unweighted and unnormalized! Does not provide a 
% measure of the model goodness-of-fit.
function chi2 = rps_beam_chi2(p, x, y, z)

% Get model for these parameter values.
model = rps_beam_model(p, x, y);
% Calculate chi^2.
chi2 = nansum((model - z).^2);
