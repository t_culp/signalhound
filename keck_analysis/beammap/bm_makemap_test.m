function bm_makemap(bmopt)
% bm_makemap(bmopt)
% 
% Function to take the demodulated beam map timestreams (output of
% bm_demod), bin them into maps, and perform a 2D Gaussian fit if requested.
% 
% Note that maps are binned into pixels 2*el_res (deg)
%
% INPUTS (should all be passed in with bmopt)
%
%   t1:          start time (UTC), e.g. '2014-Mar-08:12:00:00'
%   t2:          stop time (UTC)
%   expt:        'b2','keck','b3'
%   run:         BICEP2 runs: 'r5','r8'
%                Keck runs:   'k0r4', 'k0r4t2' (tile 2 only), highbay
%                             'k083' = k7 run 2013-10, highbay
%                             'k201?' = Keck ffbm @ Pole (12,13,14,15,16)
%                             'k201?fsl' = Keck sidelobe @ Pole (13,14)
%                BICEP3 runs: 'hb3r5' = B3 run 5, highbay
%                             'b3r6' = B3 run 6 @ Pole, 2015-02
%                             'b3r8' = B3 run 8 @ Pole, 2016-02
%   rxNum:       0 default, or specific Keck rx number, or 'all'
%   demodtype:   'square' (default), 'choplet'
%   demoddir:    directory in which to save demodulated data (normally
%                beammaps/demod)
%
%         ABOVE ARGUMENTS WERE USED IN BM_DEMOD.  BELOW FOR MAKEMAP
%
%   el_res:      Resolution of elevation steps (degrees), 0.05 default
%   source_az:   source type CENTER azimuth position (degrees), 0 default
%   source_el:   source type CENTER el position (degrees), 0 default
%   rot:         dk angle for a high bay measurement
%   component:   Specify demodulation component to map. options are:
%                'cos':  cos demodulation, default
%                'sin':  sin demodulation)
%                'comp': complex: re((cos+i*sin)*exp(i*theta*(pi/180))) - 
%                        send in theta or defaults to 0, i.e. cos
%                'quad': quadrature sum, sqrt( cos.^2 + sin.^2 )
%                'phase':arctan(sin./cos)
%                'raw':  raw fb, no demodulation
%   theta:       angle by which to rotate complex map, 0 default (degrees)
%   mapcoord:    Option for az/el
%                'azel_ap': apparent az/el per det, behind mirror (default)
%                'raw': just use boresight
%   maptype:     Options for binning:
%                'full' (default),'window','adaptivewindow'
%   winsize:     If windowing map, radius from beam center to include
%                Default 2 deg (good for viewing thumbnail plots)
%                Should go larger if you want good low-ell B_l profiles
%   dofit:       1: Perform fit (only fits r<2 deg)
%                0: No fit (default)
%   bmdir:       directory in which to save beammaps, beammaps/maps default
%   suffix:      optional string to add to map name (default '')

% Parse bmopt - these mattered for bm_demod, defaults should be the same!
t1 = bmopt.t1;
t2 = bmopt.t2;
expt = bmopt.expt;
run = bmopt.run;
if ~isfield(bmopt,'rxNum');
  rxNum = 0;
else
  rxNum = bmopt.rxNum;
end
if ~isfield(bmopt,'demodtype')
  demodtype = 'square';
else
  demodtype = bmopt.demodtype;
end
if ~isfield(bmopt,'demoddir')
  demoddir = 'beammaps/demod';
else
  demoddir = bmopt.demoddir;
end

% These matter for makemap
if ~isfield(bmopt,'el_res')
  el_res = 0.05;
else
  el_res = bmopt.el_res;
end
if ~isfield(bmopt,'source_az')
  source_az = 0;
else
  source_az = bmopt.source_az;
end
if ~isfield(bmopt,'source_el')
  source_el = 0;
else
  source_el = bmopt.source_el;
end
if ~isfield(bmopt,'rot')
  rot = [];
else
  rot = bmopt.rot;
end
if ~isfield(bmopt,'component')
  component = 'cos';
else
  component = bmopt.component;
end
if ~isfield(bmopt,'theta')
  theta = 0;
else
  theta = bmopt.theta;
end
if ~isfield(bmopt,'mapcoord')
  mapcoord = 'azel_ap';
else
  mapcoord = bmopt.mapcoord;
end
if ~isfield(bmopt,'maptype')
  maptype = 'full';
else
  maptype = bmopt.maptype;
end
if ~isfield(bmopt,'winsize')
  winsize = 2;
else
  winsize = bmopt.winsize;
end
if ~isfield(bmopt,'dofit')
  dofit = 0;
else
  dofit = bmopt.dofit;
end
if ~isfield(bmopt,'bmdir')
  bmdir = 'beammaps/maps';
else
  bmdir = bmopt.bmdir;
end
if ~isfield(bmopt,'suffix')
  suffix = '';
else
  suffix = bmopt.suffix;
end
if ~isfield(bmopt,'update')
  update = 0;
else
  update = 1;
end
if ~isfield(bmopt,'demodshift')
  demodshift = [];
else
  demodshift = bmopt.demodshift;
end

% Get the files:
files = list_arc_files('arc/',t1,t2);
n_files = length(files);

% If there is a user-input demod shift, make sure the demodded arcfile has
% it in the filename
if ~isempty(demodshift)
  demodstr = ['_shift' int2str(demodshift)];
else
  demodstr = [];
end

switch expt
  case {'b2','b3'}
    kk = 0;
    while ~exist([demoddir,'/bm_',files{kk+1}(1,1:15),...
	    '_',demodtype,demodstr,'.mat'],'file')
      kk = kk + 1;
    end
  case 'keck'
    switch rxNum
      case 'all'
	kk = 0;
	while ~exist([demoddir,'/bm_',files{kk+1}(1,1:15),...
		'_',demodtype,demodstr,'.mat'],'file')
	  kk = kk + 1;
	end
      otherwise
	kk = 0;
	while ~exist([demoddir,'/bm_',files{kk+1}(1,1:15),...
		'_rx',int2str(rxNum),'_',demodtype,demodstr,'.mat'],'file')
	  kk = kk + 1;
	end
    end
end

% Choose the first arcfile as the beammap name
filename = files{kk+1}(1,1:15);

% if update, check if map file already exists
if update == 1
  if strcmp(maptype,'full')
    name = 'map';
  else
    name = 'mapwin';
  end
  flstr = [bmdir,'/',name,'_',filename,'_',demodtype,'_',mapcoord,suffix,demodstr,'.mat']
  if exist_file(flstr) 
    disp(sprintf('Map %s already exists, skipping... ',flstr));
    return
  else
    disp(sprintf('Map %s does not exist, proceeding... ',flstr));
  end
end

% Concatenate demodulated files
disp('### Concatenating demodulated files')
tic
for fl = 1:n_files
  disp(files{fl}(1,1:15))
  % Does the file exist?  Check for both B2/B3 and Keck style
  if exist([demoddir,'/bm_',files{fl}(1,1:15),...
	  '_',demodtype,demodstr,'.mat'],'file') | exist([demoddir,...
	  '/bm_',files{fl}(1,1:15),'_rx',int2str(rxNum),'_',...
	  demodtype,demodstr,'.mat'],'file')
    switch expt
      case {'b2','b3'}
	e = load([demoddir,'/bm_',files{fl}(1,1:15),...
	      '_',demodtype,demodstr,'.mat']);
      case 'keck'
	switch rxNum
	  case 'all'
	    e = load([demoddir,'/bm_',files{fl}(1,1:15),...
		  '_',demodtype,demodstr,'.mat']);
	  otherwise
	    e = load([demoddir,'/bm_',files{fl}(1,1:15),...
		  '_rx',int2str(rxNum),'_',demodtype,demodstr,'.mat']);
	end
    end
    % Kill non-important fields to save memory
    switch component
      case 'sin'
	e = rmfield(e.d,{'cos','fb'});
      case 'cos'
	e = rmfield(e.d,{'sin','fb'});
      case {'comp','quad','phase'}
	e = rmfield(e.d,'fb');  
      case 'raw'
	e = rmfield(e.d,{'sin','cos'});
    end
    d(fl) = e;
  end
end
toc

clear e
d = structcat(1,d);
d.pointing = structcat(1,d.pointing);
dk = nanmean(d.pointing.hor.dk);

% Get a total amplitude signal and discard the rest
switch component
  case 'sin' 
    d.amp = d.sin;
    d = rmfield(d,'sin');
  case 'cos'
    d.amp = d.cos;
    d = rmfield(d,'cos');
  case 'quad'
    d.amp = sqrt(d.cos.^2 + d.sin.^2);
    d = rmfield(d,{'cos','sin'});
  case 'comp'
    d.amp = real(complex(d.cos,d.sin).*exp(1i*theta*(pi/180)));
    d = rmfield(d,{'cos','sin'});
  case 'phase'
    d.amp = atan2(d.sin,d.cos);
    d = rmfield(d,{'cos','sin'});
  case 'raw'
    d.amp = d.fb;
    d = rmfield(d,'fb');
end

% I'm leaving this in here as a lesson for everyone.  For the love of god
% let's never use the variable name "thing" ever again!!!
%
% CLW cut on azimuth.  
%switch run
%  case {'k2014'}
%    thing = d.pos(:,1) > 10;
%    d = structcut(d,thing);
%end

% Get the nominal pixel centers
switch expt
  case 'b2'
    [p,k] = ParameterRead('aux_data/beams/beams.csv');
    [pp ind] = get_array_info(filename(1:8),'obs');
  case 'keck'
    switch run 
      case {'k2012','k2013','k2013fsl','k2014','k2014fsl','k2015'} % At Pole 
 	% [pp ind] = get_array_info('20120201','obs');
 	[pp ind] = get_array_info(filename(1:8),'obs');
 	switch rxNum
          case 'all'
	  otherwise
	    cutind = (pp.rx == rxNum);
	    pp = structcut(pp,cutind);
	    ind = make_ind(pp);
	end
	%p.theta = p.theta - pp.drumangle;
	%p.drumangle = pp.drumangle;
	mount.drum_angle = mean(pp.drumangle);
      case 'k2016'
      	[pp ind] = get_array_info(20160201);
	mount.drum_angle = mean(pp.drumangle);
      case {'k0r4','k0r4t2','h083'} % high bay
	[tmp,k] = ParameterRead(sprintf(...
	    'aux_data/beams/beams_cmb_4param_20120324.csv',rxNum));
	[pp ind] = get_array_info('20120130');
	cutind = (pp.rx == rxNum); % Make sure this is rx0...
	pp = structcut(pp,cutind);
	ind = make_ind(pp);
	pp.theta = pp.theta + pp.drumangle;
    end
  case 'b3'
    [pp ind] = get_array_info(filename(1:8)); 
    % ADD 'obs' tag once we have beam centers!!!
end

tic

az_ap = [];
el_ap = [];

disp('### Getting apparent map coordinates...');
switch mapcoord
  case 'azel_ap'
    % Find the apparent az and el of each pixel centroid:
    switch expt
      case 'b2'
	[az_ap el_ap] = projection_3d(d.pos(:,1),d.pos(:,2),-dk,...
	    d.com(:,1),d.com(:,2),-dk_com,pp,ind,strcmp(type,'moon'),...
	    0,experiment);    
      case 'keck'
	switch run
          case {'k2012','k2013','k2013fsl','k2014','k2014fsl','k2015','k2016'}         
	    [az_ap, el_ap, pa_out] = ...
		keck_beam_map_pointing(d.pointing.hor.az,...
		d.pointing.hor.el,d.pointing.hor.dk,mount,...
		[],[],pp,'Legacy');
	  case {'k0r4','k0r4t2','h083'} % Use legacy mode for high bay
	    dk = rot;
	    dks = ones(length(d.pos(:,3)),1);
	    dks = dks*rot;
	    d.pos(:,2) = 90 + d.pos(:,2);
	    [mount mirror source] = get_mount_params(run);
	    [az_ap, el_ap, pa_out] = ...
		keck_beam_map_pointing(d.pos(:,1),d.pos(:,2),...
		dks,mount,mirror,source,pp);
	    az_ap(az_ap < 90) = az_ap(az_ap < 90) + 360;	
	end
      case 'b3'
	switch run
	  case {'hb3r5'} 
	    dk = rot; 
	    dks = ones(length(d.pos(:,3)),1);
	    dks = dks*rot;
	    d.pos(:,2) = 90 + d.pos(:,2);
	    [mount mirror source] = get_mount_params(run);
	    [az_ap, el_ap, pa_out] = ...
		keck_beam_map_pointing(d.pos(:,1),d.pos(:,2),...
		dks,mount,mirror,source,pp,'Legacy');
	    az_ap(az_ap < 90) = az_ap(az_ap < 90) + 360;
	  case {'b3r6','b3r8'} % At Pole
	    [mount mirror source] = get_mount_params(run);
	    [az_ap, el_ap, pa_out] = ...
		keck_beam_map_pointing(d.pointing.hor.az,...
		d.pointing.hor.el,d.pointing.hor.dk,mount,...
		mirror,source,pp,'Legacy');
	    az_ap(az_ap > 90) = az_ap(az_ap > 90) - 360;
	    az_ap(az_ap < -270) = az_ap(az_ap < -270) + 360;
	end
    end % experiment
    az_coord = az_ap;
    el_coord = el_ap;
  case 'raw'
    az_coord = repmat(d.pointing.hor.az,1,size(d.amp,2)); 
    el_coord = repmat(d.pointing.hor.el,1,size(d.amp,2)); 
end % mapcoord
toc

%%%%%%%%%%%%%%%%
% MAKE THE MAP
disp('### Making the map...');
switch maptype
  case 'full'
    [map x_bin y_bin] = create_map(d,pp,ind,el_res,maptype,...
	source_az,source_el,az_coord,el_coord,run,winsize);
  case 'window'
    % Make the map with whatever window size you specified
    [map x_bin y_bin] = create_map(d,pp,ind,el_res,maptype,...
	source_az,source_el,az_coord,el_coord,run,winsize);
    % If this is large, there could be crap which will make the fitting
    % fail - so also make a smaller (r<2) deg windowed map to fit
    [mapcut x_bincut y_bincut] = create_map(d,pp,ind,el_res,maptype,...
	source_az,source_el,az_coord,el_coord,run,2);
    % Do the fit
    A = NaN(7,size(map,3));
    if dofit
      disp('### Fitting the windowed map...');
      [pf A] = fit_beamparams(x_bincut,y_bincut,mapcut,pp);
    end
  case 'adaptivewindow'
    % Make a large-ish map (4deg) around the nominal source location
    % These should be NaN-interped
    [maplarge x_bin y_bin] = create_map(d,pp,ind,el_res,maptype,...
	source_az,source_el,az_coord,el_coord,run,4);
    % We want a separate window for each PAIR, not detector
    % (otherwise we can't plot the diff beam nicely)
    % For each detector, fit the beam to get a new source location, 
    % preconditioned with guess - this should obviate the need (above) for a
    % smaller, 2 degree window
    map = NaN(size(maplarge));
    A = NaN(7,size(map,3));
    disp('### Fitting and adaptively windowing the map (fancy!)');
    for ii = 1:length(ind.la)
      guess = get_fit_guess(expt,filename(1:8),pp.band(ind.la(ii)));
      guess(2) = source_az;
      guess(3) = source_el;
      fit_A = normfit2d(x_bin,y_bin,map(:,:,ind.la(ii)),'A0',guess);
      fit_B = normfit2d(x_bin,y_bin,map(:,:,ind.lb(ii)),'A0',guess);
      
      % Now make a new (specified) windowed map centered at the mean of A/B
      if ~isnan(fit_A(1)) & ~isnan(fit_B(1))
	x_bin_mean = (-winsize : el_res*2 : winsize) + (fit_A(2)+fit_B(2))/2;
	y_bin_mean = (-winsize : el_res*2 : winsize) + (fit_A(3)+fit_B(3))/2;
	map(:,:,ind.la(ii)) = single(grid_map(...
	    az_coord(:,ind.la(ii)),el_coord(:,ind.la(ii)),...
	    d.amp(:,ind.la(ii)),x_bin_mean,y_bin_mean));
	map(:,:,ind.lb(ii)) = single(grid_map(...
	    az_coord(:,ind.lb(ii)),el_coord(:,ind.lb(ii)),...
	    d.amp(:,ind.lb(ii)),x_bin_mean,y_bin_mean));
	% And save the bins too
	x_bin{ind.la(ii)} = x_bin_mean;
	x_bin{ind.lb(ii)} = x_bin_mean;
	y_bin{ind.la(ii)} = y_bin_mean;
	y_bin{ind.lb(ii)} = y_bin_mean;
      end
      A(:,ind.la(ii)) = fit_A;
      A(:,ind.lb(ii)) = fit_B;
      pf = pp;
    end

end

%%%%%%%%%%%%%%%%
toc

% For rx3's weird column in 2012 data
%if rxNum == 3 && strcmp(t1(1:4),'2012')
%  for ii = 34:66
%    map(:,:,ii) = -map(:,:,ii);
%  end
%end

% Prep output struct
bm.map = map;
bm.x_bin = x_bin;
bm.y_bin = y_bin;
bm.source_az = source_az;
bm.source_el = source_el;
bm.dk = dk;
bm.bmopt = bmopt;
bm.pp = pp;
bm.ind = ind;
if dofit
  bm.A = A;
  bm.p = pf;
else
  bm.p = pp;
end

switch maptype
 case 'full'
    disp('### Saving the full map...');
    switch expt
      case 'keck'
	switch rxNum
	  case 'all'
	    savename = [bmdir,'/map_',filename,'_',demodtype,...
		  '_',mapcoord,suffix,demodstr];
	  otherwise
	    savename = [bmdir,'/map_',filename,'_rx',...
		  int2str(rxNum),'_',demodtype,'_',mapcoord,suffix,demodstr];
	end % rxNum
      case {'b2','b3'}
	savename = [bmdir,'/map_',filename,'_',demodtype,...
	      '_',mapcoord,suffix,demodstr];
    end % expt
    save(savename,'bm','-v7.3');
    disp(sprintf('Saved full map %s',savename));
  
  case {'window','adaptivewindow'}

    disp('### Saving the windowed map...');
    switch expt
      case 'keck'
	switch rxNum
	  case 'all'
	    savename = [bmdir,'/mapwin_',filename,'_',demodtype,...
		  '_',mapcoord,suffix,demodstr];
	    save(savename,'bm','-v7.3')
	  otherwise
	    savename = [bmdir,'/mapwin_',filename,'_rx',...
		  int2str(rxNum),'_',demodtype,'_',mapcoord,suffix,demodstr];
	    save(savename,'bm','-v7.3')
	end % rxNum
      case {'b2','b3'}
	savename = [bmdir,'/mapwin_',filename,'_',demodtype,...
	      '_',mapcoord,suffix,demodstr];
	save(savename,'bm','-v7.3')
    end % expt
    disp(sprintf('Saved windowed map %s',savename));
end % maptype
system(['chmod g+w ' savename '.mat']);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [map x_bin y_bin] = create_map(d,p,ind,el_res,maptype,...
    source_az,source_el,az_coord,el_coord,run,winsize)

% Choose az/el bins
switch maptype
  case 'full'
    [az_coord,el_coord] = remove_outliers(az_coord,el_coord,ind);
    x_bin = min(min(az_coord(:,ind.l))) : el_res*2 : ...
	max(max(az_coord(:,ind.l)));
    y_bin = min(min(el_coord(:,ind.l))) : el_res*2 : ...
	max(max(el_coord(:,ind.l)));    
  case {'window','adaptivewindow'} % 
    % If fitting, window just around the main beam:
    switch run
      case 'hb3r5'
	x_bin = 215 : el_res*2 : 221;
	y_bin = -9 : el_res*2 : -1;
      otherwise
	x_bin = (-winsize : el_res*2 : winsize) + source_az;
	y_bin = (-winsize : el_res*2 : winsize) + source_el;
    end
    %x_bin=(-1.5:el_res*2:1.5)+source_az;
    %y_bin=(-1.5:el_res*2:1.5)+source_el;    
end

map = zeros(length(y_bin),length(x_bin),length(p.r));

for ii = 1:size(d.amp,2); %1:length(p.r)
  % Generate x and y map coordinates
  x = az_coord(:,ii);
  y = el_coord(:,ii);

  map(:,:,ii) = single(grid_map(x,y,d.amp(:,ii),x_bin,y_bin));
  
  A = map(:,:,ii);
  
  if ~isempty(A(~isnan(A))) 
    switch maptype
      case {'window','adaptivewindow'}
	map(:,:,ii) = nan_interp(A);
    end
  end   
end

%insist the map be positive definite
%map(map<0)=0;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [pf A] = fit_beamparams(x_bin,y_bin,map,p)

% Turn off all warnings which swamp the output file
warning('off','all') 

pf = p;

A = NaN(7,size(map,3));

for ii = 1:size(map,3)
  thismap = map(:,:,ii);
  % if map is all NaN, skip the fitting
  if numel(find(isnan(thismap))) == numel(thismap)
    continue
  else
    A(:,ii) = normfit2d(x_bin,y_bin,thismap);
  end
end

pf.theta(pf.theta > 360) = pf.theta(pf.theta > 360) - 360;
pf.theta(pf.theta < 0) = pf.theta(pf.theta < 0) + 360;

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [az el] = remove_outliers(az, el, ind)
% Simple function to exclude outliers from az and el coordinates
% This can be removed once the skipped samples are excluded in bm_demod
% This assumes outliers are common to all the channels
% so we choose 1st ind.l channel

ch = ind.l(1);
q = find(abs(az(:,ch) - median(az(:,ch))) > 5*std(az(:,ch)) | ...
    abs(el(:,ch) - median(el(:,ch))) > 5*std(el(:,ch)));
disp(q)
if ~isempty(q)
  az(q,:) = NaN;
  el(q,:) = NaN;
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function guess = get_fit_guess(expt,date,band)
% The fit guess is experiment, chopper, and frequency-dependent!
% Choose amplitude and beamwidth here

guess = [200,0,0,0.2,0.2,0,0];

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [mount mirror source] = get_mount_params(run)

switch run
  case {'k0r4','k0r4t2','h083'}
    mount.aperture_offr = 0;
    mount.aperture_offz = 0.6;
    mount.dk_offx = 0;
    mount.dk_offy = 0;
    mount.drum_angle = 0;
    mount.el_tilt = 0;
    mount.el_offz = 0;
    mount.el_offx = 0;
    mount.az_tilt_ha = 0;
    mount.az_tilt_lat = 0;
    mirror.height = 0.7;
    mirror.tilt = 45;    
    mirror.roll = 0;
    source.distance = 70;  
  case 'hb3r5'
    mount.aperture_offr = 0;
    mount.aperture_offz = 0.997; % Aperture to el axis: 39.255"
    mount.dk_offx = 0;
    mount.dk_offy = 0;
    mount.drum_angle = 0;
    mount.el_tilt = 0;
    mount.el_offz = 0;
    mount.el_offx = 0;
    mount.az_tilt_ha = 0;
    mount.az_tilt_lat = 0;
    mirror.height = 0.954;       % Aperture to mirror: 37.561"
    mirror.tilt = 45;     
    mirror.roll = 0;
    source.distance = 70;
  case {'b3r6','b3r8'}
    mirror.tilt = 45;
    mirror.roll = 0;
    mirror.height = 0.954;       % Aperture to mirror: 37.561"
    mount.aperture_offr = 0;
    mount.aperture_offz = 0.997; % Aperture to el axis: 39.255"
    mount.dk_offx = 0;
    mount.dk_offy = 0;
    mount.drum_angle = 0;
    mount.el_tilt = 0;
    mount.el_offz = 0;
    mount.el_offx = 0;
    mount.az_tilt_ha = 0;
    mount.az_tilt_lat = 0;
    source.distance = 210;
end


return
