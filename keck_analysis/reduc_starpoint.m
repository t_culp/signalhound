function reduc_starpoint(pmfile,recalc,t1,t2,makeplots,grouptime,schedduration,nstars)
% reduc_starpoint(pointingmodelfile,recalc,t1,t2,makeplots,grouptime,schedduration,nstars)
%
% pointingmodelfile specifies filename of CVS file located in aux_data/pointingmodels/
%        default('pointingmodel_complete_v1.csv')
% recalc = 0 do not re-fit starpointing data if already present in file (default)
%        = 1 fit all available starpointing data in date ranges
% t1,t2: date ranges in which to look for startpointing schedules (default all dates)
%        have form '20110101' or '20110101 HH:MM:SS'
% makeplots: if 1 (default), save plots and html in directory starpoint_plots/
% schedduration specifies minimum required length of schedule in minutes (default 10)
% nstars specifies minimum number of acceptable stars used in fit (default 25)
%
% schedules beginning less than the number of minutes specified by grouptime will be
% treated as one run (default grouptime=0)
%
% ex. reduc_starpoint;
% ex. reduc_starpoint('pointingmodel_optical_v1.csv');
% ex. reduc_starpoint('pointingmodel_optical_v1.csv',1,'20110623','20110627');

if(~exist('pmfile','var'));
  pmfile=[];
end
if(isempty(pmfile))
  pmfile='pointingmodel_complete_v1.csv';
end
pmfile=['aux_data/pointingmodels/',pmfile];

if(~exist('recalc','var'))
  recalc=[];
end
if(isempty(recalc))
  recalc=0;
end

if(~exist('t1','var'))
  t1=[];
end
if(isempty(t1))  
  % way in the past
  t1='19900101 00:00:00';
end

if(~exist('t2','var'))
  t2=[];
end
if(isempty(t2))
  % way in the future
  t2='20500101 00:00:00';
end

if(~exist('makeplots','var'))
  makeplots=[];
end
if(isempty(makeplots))
  makeplots=1;
end

if(~exist('grouptime','var'))
  grouptime=[];
end
if(isempty(grouptime))
  grouptime=0;
end

if(~exist('schedduration','var'))
  schedduration=[];
end
if(isempty(schedduration))
  schedduration=10;
end

if(~exist('nstars','var'))
  nstars=[];
end
if(isempty(nstars))
  nstars=25;
end

% search GCP run logs to find starpoitning schedules
[sched,t]=search_run_log('starpoint',t1,t2,1);

% exit if there are no new starpoitning data
if(isempty(sched))
  disp('no starpointing data in date range');
  return
end

% only keep starpointings that last longer than xx minutes
tstart=datenum(t(1,:),'yymmdd HH:MM:SS');
tend=datenum(t(2,:),'yymmdd HH:MM:SS');
dt=(tend-tstart)*24*60;
keepind=dt>schedduration;
if(isempty(keepind))
  % exit if there are no new starpoitning schedules >xx minutes
  disp(sprintf('no starpointing schedule in date range lasting longer than %d minutes',schedduration));
  return
end
tstart=tstart(keepind);
tend=tend(keepind);
t=t(:,keepind);

% if no recalculation is requested only reduce starpointings that don't already exist
% get the existing pointing model parameters
[p_old,k_old]=ParameterRead(pmfile);
if(~recalc)
  keepind=[];
  for i=1:numel(t(1,:))
    if(~any(strncmp(p_old.meas_name,t{1,i},15)))
      keepind=[keepind,i];
    end
  end
  tstart=tstart(keepind);
  tend=tend(keepind);
  t=t(:,keepind);
end

% exit if there are no new starpoitning schedules 
if(isempty(keepind))
  disp(sprintf('no new starpointing schedules to process',schedduration));
  return
end

% group together schedules separated by a small amount
tsep=(tstart(2:end)-tend(1:end-1))*24*60;
ind=tsep<grouptime;
tstart_tmp=tstart(1);
t_tmp=t(1,1);
j=1;
for i=1:numel(ind)
  if(~ind(i))
    tend_tmp(j)=tend(i);
    t_tmp(2,j)=t(2,i);

    tstart_tmp(j+1)=tstart(i+1);
    t_tmp(1,j+1)=t(1,i+1);    

    j=j+1;
  end
end
tend_tmp(j)=tend(end);
t_tmp(2,j)=t(2,end);

tstart=tstart_tmp;
tend=tend_tmp;
t=t_tmp;

% put start/stop times into GCP format times for writing to CSV file
p_new.meas_name=t(1,:)';

% calculate mjd
for i=1:numel(tstart)
  year=str2num(datestr(tstart(i),'yyyy'));
  month=str2num(datestr(tstart(i),'mm'));  
  day=str2num(datestr(tstart(i),'dd'));
  hour=str2num(datestr(tstart(i),'HH'));  
  minute=str2num(datestr(tstart(i),'MM'));  
  second=str2num(datestr(tstart(i),'SS'));    
  p_new.mjd(i,1)=date2mjd(year,month,day,hour,minute,second);
end

% put start/stop times into format suitable for optpoint
tstart=cellstr(datestr(tstart,'yyyy-mmm-dd:HH:MM:SS'));
tend=cellstr(datestr(tend,'yyyy-mmm-dd:HH:MM:SS'));

% exit if all the star pointings already exist and no re-calculation is requested
for i=1:numel(p_new.meas_name)
  match(i)=any(strncmp(p_old.meas_name,p_new.meas_name{i},15));
end
if(all(match) & ~recalc)
  disp('no new starpointings found in date range and no re-calculation requested');
  return
end

% reduce the data
% fn must be in order of fieldnames of fit params in fitp
disp(sprintf('reducing %d starpointing runs',numel(tstart)));
fn=fieldnames(p_old);
fn=fn(3:end);
for i=1:numel(tstart)
  % close all windows if printing figures
  if makeplots
    close all
    FigVis=get(0,'DefaultFigureVisible');
    set(0,'DefaultFigureVisible','off');
  end
  
  % fit and make plots
  [p,perr,rms,n]=optpoint(tstart{i},tend{i},1);
  
  % fit params
  if(~isempty(p))
    fitp=[p,perr,rms,n];
  else
    fitp=NaN(1,numel(fn));
  end
  
  for j=1:numel(fn)
    p_new=setfield(p_new,{1},fn{j},{i,1},fitp(j));
  end

  figs=findall(0,'type','figure');
  if makeplots & ~isempty(figs)
    % save figures
    plotdir='starpoint_plots';
    plottit1=datestr(datenum(tstart{i},'yyyy-mmm-dd:HH:MM:SS'),'yymmddHHMMSS');
    set(0,'CurrentFigure',1);
    mkpng([plotdir '/' plottit1 '_1.png']);
    setpermissions([plotdir '/' plottit1 '_1.png'])
    set(0,'CurrentFigure',2);
    mkpng([plotdir '/' plottit1 '_2.png']);
    setpermissions([plotdir '/' plottit1 '_2.png'])
    
    if ismember(3,figs)
      % there was an outlier
      set(0,'CurrentFigure',3);
    else
      % make a dummy plot
      figure(3);
      set(gcf,'Position',get(1,'Position'));
      plot(0)
      text(1,0,'No outliers');
    end
    mkpng([plotdir '/' plottit1 '_3.png']);
    setpermissions([plotdir '/' plottit1 '_3.png']);
  end
  
end

% cut out fits with too few stars
p_new=structcut(p_new,p_new.numpts>=nstars);

% cut out NaN values
p_new=structcut(p_new,~isnan(p_new.flex_cos));

% now merge old and new starpointings
nreplace=0;
nadd=0;
p_out=p_old;
for i=1:numel(p_new.mjd)
  % data to replace with newly reduced data
  replaceind=find(strncmp(p_old.meas_name,p_new.meas_name{i},15));

  if(~isempty(replaceind) & recalc)
    % replace existing best fit parameters if they exist with new ones if requested
    replaceind=replaceind(1);
    p_out.meas_name{replaceind}=p_new.meas_name{i};
    
    fn=fieldnames(p_out);
    fn=fn(2:end);
    for j=1:numel(fn)
      newval=getfield(p_new,{1},fn{j},{i,1});
      p_out=setfield(p_out,{1},fn{j},{replaceind,1},newval);
    end
    nreplace=nreplace+1;
  
  elseif(~isempty(replaceind) & ~recalc)
    % don't do anything
  end
  
  if(isempty(replaceind))
    % tack on new best fit params; it doesn't matter at this point if recalculation has
    % been requested
    p_out.meas_name={p_out.meas_name{:}, p_new.meas_name{i}}';
    
    fn=fieldnames(p_out);
    fn=fn(2:end);
    for j=1:numel(fn)
      % concatenate old and new values and put back into old values
      oldval=getfield(p_out,fn{j});
      newval=getfield(p_new,{1},fn{j},{i,1});

      oldval=cat(1,oldval,newval);
      p_out=setfield(p_out,fn{j},oldval);
    end
    nadd=nadd+1;
  end
end

% Order values from oldest to newest
[dum,ind]=sort(p_out.mjd);
fn=fieldnames(p_out);
for i=1:numel(fn)
  val=getfield(p_out,fn{i});
  p_out=setfield(p_out,fn{i},val(ind));
end

% update k
k_out=k_old;
k_out.modified=datestr(now);

% prompt before saving
disp('...')
disp(sprintf('replacing %d starpointing runs',nreplace));
disp(sprintf('adding %d starpointing runs',nadd));
disp('...');

dosave=[];
while(~(strcmp(dosave,'y') | strcmp(dosave,'n')))
  dosave=input(sprintf('save file %s? (y/n)',pmfile),'s');
end

% Write out new CSV file
if(strcmp(dosave,'y'))
  ParameterWrite(pmfile,p_out,k_out);
  setpermissions(pmfile);
end

%  make starpoint plots and html
if makeplots && ~isempty(figs)
  close all
  plot_starpoint(pmfile,plotdir);
  close all
  set(0,'DefaultFigureVisible',FigVis);
end

return
