function proposal_plots_15(pl,dopr)
% proposal_plots_15(pl,dopr)
%
% This file should know how to make all of the plots and quoted
% numbers

global dopres;
if exist('dopr','var') & dopr
  set(0,'defaultlinelinewidth',1.0);
  set(0,'DefaultAxesFontSize',14);
  %set(0,'defaulttextfontsize',14);
  dopres = 1;
else
  dopres = 0;
end

if(any(pl==1))
  make_bk_vs_world;
end

if(any(pl==2))
  make_noilev;
end

if(any(pl==3))
  make_surveyweight;
end

if(any(pl==4))
  make_surveyweight2;
end

if(any(pl==5))
  make_atmos;
end

if(any(pl==6))
  make_sensproj;
end

if(any(pl==7))
  make_fpplots;
end

if(any(pl==8))
  make_b3a_fp;
end

if(any(pl==9))
  make_2015maps;
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%
function make_2015maps

% make E-mode maps from 2015 data
load maps/1351/real_e_filtp3_weight3_gs_dp1102_jack01.mat
ac=cal_coadd_ac(ac,get_ukpervolt('20150601'),coaddopt);
ac=coadd_ac_overfreq(ac,coaddopt);
map=make_map(ac,m,coaddopt);
map=make_ebmap(m,map);

% fussed around to make very similar to the composite figure Kovac
% made using plots taken from maps pager

figure(1); clf
setwinsize(gcf,450,470);
subplot_grid(3,1,1);
plot_map(m,map(1).E); caxis([-1.2,1.2]);
xlabel([]);
h1=gca; h2=colorbar;
subplot_grid2(3,1,1);
freezeColors;
text(40,-65,{'Keck 2015','95 GHz'},'HorizontalAlignment','center','fontsize',12);
p1=get(h1,'position'); p2=get(h2,'position');
p2(3)=p2(3)/2; p2(1)=p1(1)+p1(3)+0.01;
set(h2,'position',p2); set(h1,'position',p1);

subplot_grid(3,1,2);
plot_map(m,map(2).E); caxis([-1.3,1.3]);
xlabel([]);
h1=gca; h2=colorbar;
subplot_grid2(3,1,2);
freezeColors;
text(40,-65,{'Keck 2015','150 GHz'},'HorizontalAlignment','center','fontsize',12);
p1=get(h1,'position'); p2=get(h2,'position');
p2(3)=p2(3)/2; p2(1)=p1(1)+p1(3)+0.01;
set(h2,'position',p2); set(h1,'position',p1);

subplot_grid(3,1,3);
plot_map(m,map(3).E); caxis([-1.6,1.6]);
h1=gca; h2=colorbar;
subplot_grid2(3,1,3);
freezeColors;
text(40,-65,{'Keck 2015','220 GHz'},'HorizontalAlignment','center','fontsize',12);
p1=get(h1,'position'); p2=get(h2,'position');
p2(3)=p2(3)/2; p2(1)=p1(1)+p1(3)+0.01;
set(h2,'position',p2); set(h1,'position',p1);

print -depsc2 proposal_plots/2015maps.eps

% this has drawn text but fuzzy map - nocompress switch does not fix
%!epstopdf --nocompress proposal_plots/2015maps.eps

% this has non-fuzzy map but bitmapped text
!convert -density 300 -colorspace rgb proposal_plots/2015maps.eps proposal_plots/2015maps.png

keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%%%
function make_bk_vs_world

% Get actual ell bin centers.
load('/n/bicepfs1/bicep2/pipeline/final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');
r = weighted_ellbins(r, bpwf);
lc = r(2).lc(2:10, 4); % Use BK14_150 BB bin centers.
ll = r(1).ll(2:10,4);
lh = r(1).lh(2:10,4);

% get updated lens spectrum
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_rev1_lensing.fits');
% this lens spec doesn't go high enough
ml.Cs_l(1900:end,4)=NaN;

% Get tensor BB spectrum.
tens = load_cmbfast('aux_data/official_cl/camb_planck2013_r0p1.fits');

% Legend location
text_x=[12,35,55];
text_y=logspace(log10(3e0),log10(5e1),6);

clf;
setwinsize(gcf,400,300)

loglog(ml.l,ml.Cs_l(:,4),'r');
hold on
loglog(ml.l,ml.Cs_l(:,4)/5,'r-.');
xlim([10,2000]); ylim([1e-4,1e2]);

% Plot theory spectra
plot(tens.l, 0.5*tens.Cs_l(:,4), 'r--');
plot(tens.l, 0.5*tens.Cs_l(:,4) + ml.Cs_l(:,4), 'r:');
plot(tens.l, 0.1*tens.Cs_l(:,4), 'r--');

text(4e1,7.3e-4,'lensing','Rotation',32);
text(4e1,1.5e-4,'lensing/5','Rotation',32);
text(2e2, 9e-4, 'r=0.05', 'Rotation', 0);
text(2e2, 2e-4, 'r=0.01', 'Rotation', 0);

% Plot CMB-only bandpowers.
% Color for CMB-only bandpowers (black)
cc = [0 0 0];
% Add CMB component spectrum
[cmb, dust, sync] = get_comp_perbin();
% Select ell bins that should be plotted as upper limits
% (all points that are <1 sigma above zero).
ulbin = (cmb.int68(:,1) == 0);
% Plot points.
h = errorbar2(lc(~ulbin), cmb.max(~ulbin)', cmb.max(~ulbin)' - cmb.int68(~ulbin,1), ...
              cmb.int68(~ulbin,2) - cmb.max(~ulbin)', '.');
set(h, 'Color', cc);
herrorbar2(lc(~ulbin), cmb.max(~ulbin)', lc(~ulbin) - ll(~ulbin), ...
           lh(~ulbin) - lc(~ulbin), '.', cc);
plot(lc(~ulbin), cmb.max(~ulbin)', '.', 'Color', cc, 'MarkerSize', 12);
% Plot upper limits.
herrorbar2(lc(ulbin), cmb.int95(ulbin,2), lc(ulbin) - ll(ulbin), ...
           lh(ulbin) - lc(ulbin), 'v', cc);
% Label.
text(1.5e1, 6e-3, {'BK14','CMB component'}, 'Color', cc);
%text(1.1e1, 4e-3, 'CMB component', 'Color', cc);

% Plot other experiments
ms=4; % marker size

% DASI
other_data=load('other_experiments/dasi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,1/3,0]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(5),'DASI','Color',[1,1/3,0])
% CBI
other_data=load('other_experiments/cbi.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,1]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(4),'CBI','Color',[0,1,1])
% CAPMAP
other_data=load('other_experiments/capmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(3),'CAPMAP','Color',[0.5,0,0.5])
% BOOMERANG
other_data=load('other_experiments/boomerang.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(2),'Boomerang','Color',[1,0,1])
% WMAP
other_data=load('other_experiments/wmap.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,1,0]);
set(h(2),'MarkerSize',ms);
text(text_x(1),text_y(1),'WMAP','Color',[0,1,0])
% QUAD
other_data=load('other_experiments/quad.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[1,0,0]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(5),'QUAD','Color',[1,0,0])
% BICEP1
other_data=load('other_experiments/bicep1.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,0.5]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(4),'BICEP1','Color',[0.5,0.5,0.5])
% QUIET-40
other_data=load('other_experiments/quiet40.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0.5,0.5,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(3),'QUIET-Q','Color',[0.5,0.5,1])
% QUIET-90
other_data=load('other_experiments/quiet90.txt');
h=herrorbar2(other_data(:,1),other_data(:,4),other_data(:,2),other_data(:,3),'v',[0,0,1]);
set(h(2),'MarkerSize',ms);
text(text_x(2),text_y(2),'QUIET-W','Color',[0,0,1])

% polarbear measurement of lensing
other_data=load('other_experiments/polarbear.txt');
ul=3;
meas=[1,2,4];
halfbw = (other_data(2,1)-other_data(1,1))/2;
halfbw = repmat(halfbw,4,1);
h=errorbar2(other_data(meas,1),other_data(meas,2),other_data(meas,3),'gs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','g');
h=herrorbar2(other_data(meas,1),other_data(meas,2),halfbw(meas),halfbw(meas),'gs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','g');
h=herrorbar2(other_data(ul,1),other_data(ul,4),halfbw(ul),halfbw(ul),'gv');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','g');

% sptpol http://arxiv.org/pdf/1503.02315.pdf tab1
other_data=load('other_experiments/sptpol.txt');
other_data(:,2)=other_data(:,2).*other_data(:,1)/(2*pi);
other_data(:,3)=other_data(:,3).*other_data(:,1)/(2*pi);
ul=[4,5];
meas=[1,2,3];
halfbw = (other_data(2,1)-other_data(1,1))/2;
halfbw = repmat(halfbw,5,1);
h=errorbar2(other_data(meas,1),other_data(meas,2),other_data(meas,3),'bs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');
h=herrorbar2(other_data(meas,1),other_data(meas,2),halfbw(meas),halfbw(meas),'bs');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');
% decide to make upper limit just +2xerrorbar
h=herrorbar2(other_data(ul,1),other_data(ul,3)*2,halfbw(ul),halfbw(ul),'bv');
set(h(2),'MarkerSize',ms,'MarkerFaceColor','b');

% ACTPol, http://arxiv.org/pdf/1405.5524 table 3
other_data = load('other_experiments/actpol.txt');
% Only plot points 1,2 (3+4 are off the rhs of plot)
ul = [1,2];
% Calculate 95% upper limit by assuming Gaussian bandpower pdf
for ii=1:numel(ul)
  x = [0:0.01:5];
  y = exp(-0.5 * (x - other_data(ul(ii),4)).^2 / other_data(ul(ii),5)^2);
  cdf = cumsum(y) / sum(y);
  imax = min(find(cdf == 1));
  ulval(ul(ii)) = interp1(cdf(1:imax), x(1:imax), 0.95);
end
h=herrorbar2(other_data(ul,2), ulval(ul), other_data(ul,2) - other_data(ul,1), ...
             other_data(ul,3) - other_data(ul,2), 'v', [0.8,0.6,0]);
set(h(2),'MarkerSize',ms,'MarkerFaceColor',[0.8,0.6,0]);

text(8e2, 10^-2, 'ACTpol','Color',[0.8,0.6,0])
text(8e2, 10^-2.3,'Polarbear','Color',[0,1,0])
text(8e2, 10^-2.6, 'SPTpol','Color',[0,0,1])

hold off

xlabel('Multipole')
ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]')

set(gcf, 'PaperPosition', [0 0 6 4.5]);
print -depsc2 proposal_plots/bk14_vs_world.eps
fix_lines('proposal_plots/bk14_vs_world.eps')
!epstopdf proposal_plots/bk14_vs_world.eps

return %% END of bk14_vs_world

function [cmb, dust, sync] = get_comp_perbin
% Get results from per ell bin component separation analysis.

% Get 1D marginalized likelihoods for CMB, dust, and sync.
% For each likelihood, calculate 68% and 95% HPD interval.
distdir = '/n/bicepfs2/keck/pipeline/likelihood/dists/BK14_perbin/real';
for ii=1:9
  % CMB component.
  distfile = fullfile(distdir, sprintf('BK14_bin%i_p_CMBflat_BB.dat', ii));
  d = load(distfile);
  [cmb.int68(ii,:), cmb.max(ii)] = hpd_interval(d(:,1), d(:,2), 0.68);
  [cmb.int95(ii,:)] = hpd_interval(d(:,1), d(:,2), 0.95);
  % Dust component.
  distfile = fullfile(distdir, sprintf('BK14_bin%i_p_BBdust.dat', ii));
  d = load(distfile);
  [dust.int68(ii,:), dust.max(ii)] = hpd_interval(d(:,1), d(:,2), 0.68);
  [dust.int95(ii,:)] = hpd_interval(d(:,1), d(:,2), 0.95);
  % Sync component.
  distfile = fullfile(distdir, sprintf('BK14_bin%i_p_BBsync.dat', ii));
  d = load(distfile);
  [sync.int68(ii,:), sync.max(ii)] = hpd_interval(d(:,1), d(:,2), 0.68);
  [sync.int95(ii,:)] = hpd_interval(d(:,1), d(:,2), 0.95);  
end

return

%%%%%%%%%%%%%%%%%%%%%
function make_noilev
% update the plot summarizing noise levels adding wmap etc

% get the data
load('final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat');

% can see this in opt.finalopt.mapname
f=[95,150,23,30,33,44,70,100,143,217,353];

figure(1); clf;
setwinsize(gcf,400,300);
xlim([0,370]); ylim([1e-4,1e1]);
set(gca,'yscale','log'); box on
hold on

% plot dust/sync as fine spaced curves
ff=linspace(1,400,100);
for i=1:numel(ff)
  % TODO: check these new 23GHz pivot vals are good
  sc(i)=3.8*freq_scaling([ff(i),1],-3.1,[],23)^2;
  dcu(i)=(4.3+1.2)*freq_scaling([ff(i),1],1.6,19.6,353)^2;
  dcl(i)=(4.3-1.0)*freq_scaling([ff(i),1],1.6,19.6,353)^2;
  dc(i)=(4.3+0.0)*freq_scaling([ff(i),1],1.6,19.6,353)^2;
end
% plot sync shaded region
h=patch([ff,ff(end),ff(1)],[sc,1e-9,1e-9],[1,0.7,0.7],'EdgeColor','none');
% plot dust shaded region
patch([ff,fliplr(ff)],[dcu,fliplr(dcl)],[0.7,0.7,1],'EdgeColor','none');

% find nominal gmean freqs
for i=1:9
  fgm95(i)=gmean(f(1),f(i+2));
  fgm150(i)=gmean(f(2),f(i+2));
end

% manually tune to push "points onto curves"
% this is necessary because the dust model is not a true power law
% (and neither is the sync model in the CMB units being used here)
% in practice only push the 95x353 and 150x353 points
fgm95(9)=fgm95(9)+40;
fgm150(9)=fgm150(9)+20;
if(0)
  % figure out tuning above
  for i=1:numel(f)
    sc(i)=0.0003*freq_scaling([f(i),1],-3.3,[],150)^2;
    dc(i)=3.3*freq_scaling([f(i),1],1.59,19.6,353)^2;
  end
  for i=1:9
    plot(fgm95(i),gmean(dc(1),dc(i+2)),'rx');
    plot(fgm150(i),gmean(dc(2),dc(i+2)),'gx');
    plot(fgm95(i),gmean(sc(1),sc(i+2)),'r.');
    plot(fgm150(i),gmean(sc(2),sc(i+2)),'g.');
  end
end

% llcdm and r are same values at all ell
xl=xlim;
llcdm=mean(r(1).sim(3,4,:),3);
rp2=mean(r(1).simr(3,4,:),3)-llcdm;
line(xl,[llcdm,llcdm],'color','k');
h=text(260,llcdm,'lensed-LCDM','color','k');
set(h,'backgroundcolor','w','margin',0.1,'VerticalAlignment','middle');
line(xl,[rp2,rp2]/4,'color','k','linestyle','--');
line(xl,[rp2,rp2]/20,'color','k','linestyle','--');
h=text(325,rp2/4,'r=0.05','color','k');
set(h,'backgroundcolor','w','margin',0.1,'VerticalAlignment','middle');
h=text(325,rp2/20,'r=0.01','color','k');
set(h,'backgroundcolor','w','margin',0.1,'VerticalAlignment','middle');

[c35,c95,c150,c220,c270]=get_colors2();

% plot the noise bandpower uncertainties in BB ell=70 bin
bkn95=std(r(1).noi(3,4,:),[],3);
bkn150=std(r(2).noi(3,4,:),[],3);
j=aps_getxind(length(r),1,2);
bkn95x150=std(r(j).noi(3,4,:),[],3);
plot(f(1),bkn95,'.','color','b','MarkerSize',15);
%plot(gmean(f(1),f(2)),bkn95x150,'x','color','b','MarkerSize',10);
plot(f(2),bkn150,'.','color','b','MarkerSize',15);

for i=1:9
  % get WMAP/Planck autos
  j=aps_getxind(length(r),i+2,i+2);
  a(i)=std(r(j).noi(3,4,:),[],3);
  % get BKxExt
  j=aps_getxind(length(r),1,i+2);
  cx95(i)=std(r(j).noi(3,4,:),[],3);
  j=aps_getxind(length(r),2,i+2);
  cx150(i)=std(r(j).noi(3,4,:),[],3);
end
% plot ext auto noise uncer curve
plot(f(3:11),a,'k.:','MarkerSize',7);
plot(f(3),a(1),'k.','MarkerSize',15);
plot(f(11),a(end),'k.','MarkerSize',15);

% plot regular cross spectral noise curve
%plot(fgm95,cx95,'x:','color',c95);
%plot(fgm150,cx150,'x:','color',c150);
plot(fgm95(1),cx95(1),'x:','color','b','MarkerSize',10);
plot(fgm150(end),cx150(end),'x:','color','b','MarkerSize',10);

if(1)
  % make up a BK15 220x220 point 
  % 220 noise in 20151021_keck pager is 29.88 uK-arcmin but this is
  % not full season - estimate 27 full season
  % Therefore multiply bkn150 by (27/3)^2
  bkn220=bkn150*(27/3)^2;
  plot(220,bkn220,'.','color','r','MarkerSize',15);
  text(220+10,bkn220,'220x220 prelim.','color','r');
  % take the 150x220 noise as the gmean
  % note this does not check out for P143 and BK150 -
  % gmean(bkn150,a(7)) is not equal to c150(7)
  bkn150x220=gmean(bkn150,bkn220);
  plot(gmean(150,220),bkn150x220,'x','color','r','MarkerSize',10);
  text(gmean(150,220)+10,bkn150x220,'150x220 prelim.','color','r');
end

% do text at end to make sure on top of lines
text(20,0.3,'Sync upper limit','Rotation',-72);
text(158,0.011,'Dust Level','Rotation',38);
text(f(1)+10,bkn95,'95x95','color','b')
%text(gmean(f(1),f(2))+10,bkn95x150,'95x150','color','b')
text(f(2)+10,bkn150,'150x150 Noise','color','b')
text(100,0.15,'WMAP/Planck autos','Rotation',0);
%text(fgm95(end)+10,cx95(end),'95xExt','color',c95);
%text(fgm150(end)+10,cx150(end),'150xExt','color',c150);
text(fgm95(1)+10,cx95(1),'95xW23','color','b');
text(fgm150(end)+10,cx150(end),'150xP353','color','b');

text(f(3)-20,a(1)*1.4,'W23xW23');
text(f(11)-33,a(end)*1.4,'P353xP353');

hold off

xlabel('Nominal band center [GHz]');
ylabel('BB in l\sim80 bandpower l(l+1)C_l/2\pi [\muK^2]')

% stop axis ticks from being obscured
set(gca,'Layer','top');

print -depsc2 proposal_plots/noilev.eps
fix_lines('proposal_plots/noilev.eps')
!epstopdf proposal_plots/noilev.eps

return

%%%%%%%%%%%%%%%%%%%%%
function make_surveyweight

% try to make a plot showing the buildup of "survey weight" over
% the data taking periods of experiments and the delay before
% publication of the results

% See for notes:
% http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20150602_the_comp

% POLARBEAR 1403.2369 sec 5.2.4 says 6 uK arcmin in RA23. Table 1
% Sec 3 says obs taken between June 2012 and June 2013.
% says RA23 has effective area 8.8 sq deg. There are two other
% fields with eff area 7.0 and 8.7 sq deg.
% Sec 5.2.4 says the number of "daily maps" is similar over the 3
% fields.

% B2 paper says 87 nK deg over 380 sq deg

% ACTpol 1405.5524 sec 3.1 gives areas of 4 patches as
% 73+70+70+63=276 (central region only) and says obs from 13/9/11
% to 13/12/14.
% T map white noise is given as 16.2, 17, 13.2, and 11.2 uK arcmin
% all to be multiplied by sqrt(2) to get Q/U noise

% K paper says 74 nK deg over 390 sq deg
% and 57 nK deg over 400 sq deg

% SPTpol 1503.02315 sec3 says that for nominal 100d field the "white
% noise" levels are 17/9 uK arcmin at 95/150GHz
% sec3 says 100d obs taken between apr12 and apr13 - then switched
% to 100d

% Planck from 1502.01582

pb.ot(1)=2012+5/12;
pb.ot(2)=2013+6/12;
pb.pd=2014+2/12;
[ts,sw]=maprmsarea_to_sensweight(6*1e3/60,8.8+7.0+8.7);
pb.sw=sw;

b2.ot(1)=2010+3/12; % assume begin apr-1
b2.ot(2)=2012+10/12; % assume end nov-1
b2.pd=2014+2.5/12;
[ts,sw]=maprmsarea_to_sensweight(87,380);
b2.sw=sw;

ap.ot(1)=2013+8.5/12;
ap.ot(2)=2013+11.5/12;
ap.pd=2014+5/12;
[ts,sw]=maprmsarea_to_sensweight(sqrt(2)*14*1e3/60,276);
ap.sw=sw;

k13.ot(1)=2012+3/12; % first year not used
k13.ot(2)=2013+10/12;
k13.pd=2015+1/12;
[ts,sw]=maprmsarea_to_sensweight(74,390);
k13.sw=sw;

% SPTpol 100deg
sptpol.ot(1)=2012+3/12; % says 2012 april-oct
sptpol.ot(2)=2013+4/12; % says 2013 april
sptpol.pd=2015+3/12;
[ts,sw95]=maprmsarea_to_sensweight(17*1e3/60,100);
[ts,sw150]=maprmsarea_to_sensweight(9*1e3/60,100);
sptpol.sw=sw95+sw150;

% Planck
pl.ot(1)=2009+7.5/12; % mid Aug
pl.ot(2)=2012+0.5/12; % mid Jan
pl.pd=2015+1/12;
pl.sw=125e3/1e6;

% no simple receipe for WMAP - see Chris post 20140714_wmap_Kband

bk13=add_ex(b2,k13);

bk14.ot(1)=2013+10/12; % continue BK line
bk14.ot(2)=2014+10/12;
bk14.pd=2015+10/12;
[ts,sw95]=maprmsarea_to_sensweight(127,375);
[ts,sw150]=maprmsarea_to_sensweight(50,395);
bk14.sw0=bk13.sw(end);
bk14.sw=sw95+sw150;

figure(1); clf
setwinsize(gcf,550,400);

h(1)=plot_expt(pl,'c');
h(2)=plot_expt(sptpol,'m');
h(3)=plot_expt(pb,'g');
h(4)=plot_expt(ap,'r');

h(5)=plot_expt(k13,'b');
h(6)=plot_expt(bk13,'b');
h(7)=plot_expt(bk14,'b');
h(8)=plot_expt(b2,'b');

grid on

legend(h,{'Planck','SPTpol','POLARBEAR','ACTpol','BICEP/Keck'},'Location','NW');

% label our papers
text(b2.pd+0.1,b2.sw*1e6-1000,'BK-I');
text(k13.pd+0.1,k13.sw*1e6-1000,'BK-V');
text(bk13.pd+0.1,bk13.sw(end)*1e6-1000,'BKP');
text(bk14.pd+0.1,bk14.sw*1e6-1000,'BK-VI');

% add annotation
drawbrace([2012.25,1e4],[2013.7,14e4],7,'color',[0.5,0.5,0.5]);
drawbrace([2013.90,15e4],[2015,15e4],7,'color',[0.5,0.5,0.5]);
text(2012.1,3e4,'Data taking','rotation',50);
text(2014,18e4,'Analysis delay');

xlabel('Time (year)');
ylabel('Survey weight (\muK^{-2})');

xlim([2010,2016.4]); ylim([0,.4e6]);

% shift the year labels to mid-year
x=get(gca,'xticklabel');
x(end,:)=' ';
set(gca,'xticklabel',[repmat('                       ',size(x,1),1),x])

% write the time limit on the plot
h=text(2014,3.8e5,'Results on arxiv as of Feb 22 2016');
set(h,'backgroundcolor','w','margin',0.1,'HorizontalAlignment','center','fontsize',12);

print -depsc2 proposal_plots/sens_comp1.eps
fix_lines('proposal_plots/sens_comp1.eps')
!epstopdf proposal_plots/sens_comp1.eps

return

%%%%%%%%%%%%%%%%%%%%%%
function h=plot_expt(ex,c)

if(~isfield(ex,'sw0'))
  ex.sw0=0;
end

ex.sw=ex.sw*1e6; ex.sw0=ex.sw0*1e6;

if(numel(ex.sw)==1)
  h=plot(ex.ot,[ex.sw0,ex.sw],c);
else
  h=plot(ex.ot,ex.sw,c);
end
hold on
h2=plot([ex.ot(end),ex.pd],[ex.sw(end),ex.sw(end)],'--','color',[0.5,0.5,0.5]);
plot(ex.pd,ex.sw(end),'x','color',c);

set(h,'linewidth',2.0);
%set(h2,'linewidth',2.0);

return

%%%%%%%%%%%%%%%%%%%%%%
function ex=add_ex(ex1,ex2)

ts=min([ex1.ot,ex2.ot]);
te=max([ex1.ot,ex2.ot]);

tc=linspace(ts,te,1000);

sw1=interp1(ex1.ot,[0,ex1.sw],tc,[],ex1.sw);
sw2=interp1(ex2.ot,[0,ex2.sw],tc,[],0);

ex.ot=tc;
ex.sw=sw1+sw2;
if(isfield(ex1,'pd'))
  ex.pd=max([ex1.pd,ex2.pd]);
end
return

%%%%%%%%%%%%%%%%%%%%%
function make_surveyweight2

% alternate version including projection forward

b2.ot(1)=2010+3/12; % assume begin apr-1
b2.ot(2)=2012+10/12; % assume end nov-1
[ts,b2.sw]=maprmsarea_to_sensweight(87,380);

k.ot(1)=2012+3/12; % first year not used
k.ot(2)=2013+10/12;
[ts,k.sw]=maprmsarea_to_sensweight(74,390);

bk=add_ex(b2,k);

% SPTpol 100 deg sq survey
sp100.ot(1)=2012+3/12; % says 2012 april-oct
sp100.ot(2)=2013+4/12; % says 2013 april
[ts,sw95]=maprmsarea_to_sensweight(17*1e3/60,100);
[ts,sw150]=maprmsarea_to_sensweight(9*1e3/60,100);
sp100.sw=sw95+sw150;

pb.ot(1)=2012+4/12;
pb.ot(2)=2013+6/12;
[ts,pb.sw]=maprmsarea_to_sensweight(6*1e3/60,8.8+7.0+8.7);

ap.ot(1)=2013+8.5/12;
ap.ot(2)=2013+11.5/12;
[ts,ap.sw]=maprmsarea_to_sensweight(sqrt(2)*14*1e3/60,276);


figure(2); clf
setwinsize(gcf,550,400);

h(1)=plot(pb.ot,[0,1e6*pb.sw],'g');
hold on
h(2)=plot(ap.ot,[0,1e6*ap.sw],'r');
h(3)=plot(sp100.ot,[0,1e6*sp100.sw],'b');
h(4)=plot(bk.ot,1e6*bk.sw,'k');

% add points for published results
%plot(b2.ot(2),b2.sw,'ko');
%plot(bk.ot(end),1e6*bk.sw(end),'ko');
%plot(pb.ot(2),1e6*pb.sw,'ro');
%plot(ap.ot(2),1e6*ap.sw,'go');
%plot(sp100.ot(2),1e6*sp100.sw,'bo');

% Keating email implies about 3x the data by end 2015
plot([pb.ot(2),2016],1e6*[pb.sw,pb.sw*3],'g--');

% Niemack says same deriv up to end 2014 and then 100k by end of 2015
plot([ap.ot(2),2015],1e6*[ap.sw,.03],'r--')
plot([2015,2016],1e6*[0.03,0.1],'r--')

% continue the line to Henning vals from spreadsheet for 2015
plot([sp100.ot(2),2016],1e6*[sp100.sw,0.068],'b--');

% add in 2014
plot([bk.ot(end),2015],1e6*[bk.sw(end),bk.sw(end)+.105],'k--');

% add in 2015
plot([2015,2016],1e6*[bk.sw(end)+.105,bk.sw(end)+2*.105],'k--');

%hold off

legend(h,{'POLARBEAR','ACTpol','SPTpol','BK'},'Location','NW');

xlabel('Time [years]');
ylabel('Survey weight [\muK^{-2}]');

xlim([2010,2015.99]); ylim([0,.5e6]);

grid

print -depsc2 proposal_plots/sens_comp2.eps
!convert -density 300 proposal_plots/sens_comp2.eps proposal_plots/sens_comp2.png

return

%%%%%%%%%%%%%%%%%%%%%
function make_atmos

% get Grant files as shown in post
% http://bicep.rc.fas.harvard.edu/bkcmb/analysis_logbook/analysis/20131025_am_validity
am_dir='~/am/from_grant/20131025_post/am_files_no_summer';
am_files=dir([am_dir,'/*.out']);

for jj=1:length(am_files)
  am_file=am_files(jj).name;
  am=importdata(fullfile(am_dir,am_file));
  t(:,jj)=am(:,2);
end
f=am(:,1);

% get Chile curve
c=load('~/am/Chajnantor_edit_32p5_1100_270.out');

% get bandpasses as used in multicomp analysis - and hardcoded in
% multicomp/like_read_bandpass!
[x,y,z]=textread('aux_data/bandpass/B2_frequency_spectrum_20141216.txt',...
                 '%f,%f,%f','commentstyle','shell');
b150=[x,y,z];
[x,y,z]=textread('aux_data/bandpass/K95_frequency_spectrum_20150309.txt',...
                 '%f,%f,%f','commentstyle','shell');
b95=[x,y,z];
[x,y,z]=textread('aux_data/bandpass/K220_frequency_spectrum_20160120.txt',...
                 '%f,%f,%f','commentstyle','shell');
b220=[x,y,z];

% make up others
b35(:,1)=25:45; b35(:,2)=tukeywin(size(b35,1),0.75);
%b220(:,1)=185:255; b220(:,2)=tukeywin(size(b220,1),0.75);
b270(:,1)=230:310; b270(:,2)=tukeywin(size(b270,1),0.75);

figure(1); clf
setwinsize(gcf,750,200);
subplot(1,3,[1,2]);

[c35,c95,c150,c220,c270]=get_colors2();

plot(f,prctile(t,[10,90],2),'color',[0.7,0.7,0.7]);
hold on
plot(f,median(t,2),'k');
plot(b35(:,1),b35(:,2),'--','color',c35);
plot(b95(:,1),b95(:,3),'color',c95);
plot(b150(:,1),b150(:,3),'color',c150);
plot(b220(:,1),b220(:,2),'color',c220);
plot(b270(:,1),b270(:,2),'--','color',c270);
hold off

a1=gca;

xlim([0,400]); ylim([0,1]);
xlabel('Observing frequency (GHz)'); ylabel('Transmittance');

% plot some timestream psd

% get the tod with recal applied
tag='20150604B01_dk248';
load(['data/real/',tag(1:6),'/',tag,'_tod']);
[p,ind]=get_array_info(tag);

% get the precalc cutparams - this is just a speedup - ought to get
% exact same thing if we recomputed
load(sprintf('data/real/%s/%s_cutparams.mat',tag(1:6),tag));

% apply round1 cuts to timestream
cut1=get_default_round1_cuts;
c1=eval_round1_cuts(cp,cut1,p,ind);
c1.overall=combine_cuts(c1,ind);
d=apply_cuts(d,fs,c1.overall);

% apply abscal to the tod
ukpv=get_ukpervolt(tag)
d.mce0.data.fb=d.mce0.data.fb.*repmat(ukpv(p.rx+1),[size(d.mce0.data.fb,1),1]);

% sumdiff and p0 filter
d=sumdiff_pairs(d,p,fs,ind.a,ind.b);
d=filter_scans(d,fs,'p0',ind.e);

% take the psd
[psd,fpsd]=mean_psd(d,fs);

psd=psd*2;

% get round2 cuts which are a pair selection
cut2=get_default_round2_cuts([],tag(1:4));
c2=eval_round2_cuts(cp,c1,cut2,p,ind);
[c2.overall,cutmsg]=combine_cuts(c2,ind);
ind.rgl100a=intersect(ind.rgl100a,find(c2.overall));
ind.rgl150a=intersect(ind.rgl150a,find(c2.overall));
ind.rgl220a=intersect(ind.rgl220a,find(c2.overall));
ind.rgl100b=intersect(ind.rgl100b,find(c2.overall));
ind.rgl150b=intersect(ind.rgl150b,find(c2.overall));
ind.rgl220b=intersect(ind.rgl220b,find(c2.overall));

subplot(1,3,3);

% plot
h(1)=loglog(fpsd,sqrt(median(psd(:,ind.rgl100b)')),'color',c95)
hold on
h(2)=loglog(fpsd,sqrt(median(psd(:,ind.rgl150b)')),'color',c150)
h(3)=loglog(fpsd,sqrt(median(psd(:,ind.rgl220b)')),'color',c220)
loglog(fpsd,sqrt(median(psd(:,ind.rgl100a)')),'--','color',c95)
loglog(fpsd,sqrt(median(psd(:,ind.rgl150a)')),'--','color',c150)
loglog(fpsd,sqrt(median(psd(:,ind.rgl220a)')),'--','color',c220)
plot([0.1,0.1],[1e2,1e5],'k--');
plot([1,1],[1e2,1e5],'k--');
hold off
xlim([0.09,4]); ylim([100,100000])
legend(h,{'95 GHz','150 GHz','220 GHz'});
legend boxoff
xlabel('Timestream frequency [Hz]');
ylabel('Noise [\muK \surds]');

a2=gca;

% manual fixup to overcome Matlab crapness!
p1=get(a1,'position'); p2=get(a2,'position'); 
p1([2,4])=[0.17,0.75]; set(a1,'position',p1);

print -depsc2 proposal_plots/atmos.eps
fix_lines('proposal_plots/atmos.eps')
!epstopdf proposal_plots/atmos.eps

keyboard

return

%%%%%%%%%%%%%%%%%%%%%
function ww = depth_weight(year)

% function used by make_sensproj.
% returns the weight and map depth at each frequency as a function of year

% Starting point: BK14 survey weights
w35_14 = 0;
w95_14 = 48000;
w150_14 = 316000;
w220_14 = 0;
w250_14 = 0;
w270_14 = 0;

% Resolution at which the projections have been calculated
rez = 21;
% Choose year. Calculate the exact number of rx-yrs in each band in each 
% experiment (Keck, B3, B3A).
switch year
  case 2013
    rez = 21;
    rxk = -[linspace(2,2,rez); linspace(10.1,3,rez)];
    nuk = [95 * ones(1,rez); 150 * ones(1,rez)];
    rxb3 = [];
    nub3 = [];
    rxb3a = [];
    nub3a = [];
  case 2014
    rxk = -[linspace(2,0,rez); linspace(3,0,rez)];
    nuk = [95 * ones(1,rez); 150 * ones(1,rez)];
    rxb3 = [];
    nub3 = [];
    rxb3a = [];
    nub3a = [];
  case 2015
    rxk = [linspace(0,2,rez); linspace(0,1,rez); linspace(0,2,rez)];
    nuk = [95 * ones(1,rez); 150 * ones(1,rez); 220 * ones(1,rez)];
    rxb3 = [];
    nub3 = [];
    rxb3a = [];
    nub3a = [];
  case 2016
    rxk = [linspace(2,2,rez); linspace(1,2,rez); linspace(2,6,rez)];
    nuk = [95 * ones(1,rez); 150 * ones(1,rez); 220 * ones(1,rez)];
    rxb3 = [linspace(0,9,rez)];
    nub3 = [95 * ones(1,rez)];
    rxb3a = [];
    nub3a = [];
  case 2017
    rxk = [linspace(2,2,rez); linspace(2,3,rez); linspace(6,8,rez); linspace(0,2,rez)];
    nuk = [95 * ones(1,rez); 150 * ones(1,rez); 220 * ones(1,rez); 270 * ones(1,rez)];
    rxb3 = [linspace(9,18,rez)];
    nub3 = [95 * ones(1,rez)];
    rxb3a = [];
    nub3a = [];
  case 2018
    rxk = [linspace(2,2,rez); linspace(3,3,rez); linspace(8,8,rez); linspace(2,5,rez)];
    nuk = [95 * ones(1,rez); 150 * ones(1,rez); 220 * ones(1,rez); 270 * ones(1,rez)];
    rxb3 = [linspace(18,27,rez)];
    nub3 = [95 * ones(1,rez)];
    rxb3a = [linspace(0,1,rez)];
    nub3a = [35 * ones(1,rez)];
  case 2019
    rxk = [linspace(2,2,rez); linspace(3,3,rez); linspace(8,8,rez); linspace(5,6,rez)];
    nuk = [95 * ones(1,rez); 150 * ones(1,rez); 220 * ones(1,rez); 270 * ones(1,rez)];
    rxb3 = [linspace(27,36,rez)];
    nub3 = [95 * ones(1,rez)];
    rxb3a = [linspace(1,2,rez); linspace(0,1,rez); linspace(0,1,rez)];
    nub3a = [35 * ones(1,rez); 95 * ones(1,rez); 150 * ones(1,rez)];
case 2020
    rxk = [linspace(2,2,rez); linspace(3,3,rez); linspace(8,8,rez); linspace(6,6,rez)];
    nuk = [95 * ones(1,rez); 150 * ones(1,rez); 220 * ones(1,rez); 270 * ones(1,rez)];
    rxb3 = [linspace(36,45,rez)];
    nub3 = [95 * ones(1,rez)];
    rxb3a = [linspace(2,3,rez); linspace(1,2,rez); linspace(1,2,rez); linspace(0,0.5,rez); linspace(0,0.5,rez)];
    nub3a = [35 * ones(1,rez); 95 * ones(1,rez); 150 * ones(1,rez); 220 * ones(1,rez); 270 * ones(1,rez)];
  case 2021
    rxk = [linspace(2,2,rez); linspace(3,3,rez); linspace(8,8,rez); linspace(6,6,rez)];
    nuk = [95 * ones(1,rez); 150 * ones(1,rez); 220 * ones(1,rez); 270 * ones(1,rez)];
    rxb3 = [linspace(45,54,rez)];
    nub3 = [95 * ones(1,rez)];
    rxb3a = [linspace(3,4,rez); linspace(2,3,rez); linspace(2,3,rez); linspace(0.5,1,rez); linspace(0.5,1,rez)];
    nub3a = [35 * ones(1,rez); 95 * ones(1,rez); 150 * ones(1,rez); 220 * ones(1,rez); 270 * ones(1,rez)];
end

% Ideal NET's for B3-Array (i.e. a reality factor has not been applied for yield
% and inevitable less than ideal performance) in muK_CMB * rts
net35 = 13.68;
net95 = 4.89;
net150 = 3.55;
net220 = 6.11;
net250 = 5.51;
net270 = 9.58;
% Ideal NET's for Keck:
net95_keck = 287.6/sqrt(288);
net150_keck = 313.1/sqrt(512);
net220_keck = 837.7/sqrt(512);

% Add all the weight from different experiments in each band. 
for i = 1:rez
  % Keck first
  add_keck = [nuk(:,i), rxk(:,i)];
  % B3 second
  if isempty(rxb3)
    add_b3 = [];
    w95_b3 = 0;
  else
    add_b3 = [nub3(:,i), rxb3(:,i)];
    if any(add_b3(:,1)==95)
      w95_b3 = add_b3(find(add_b3(:,1)==95),2) .* 24000;
    end
  end
  % Finally, B3A
  if isempty(rxb3a)
    add_b3a = [];
    w35_b3a = 0;
    w95_b3a = 0;
    w150_b3a = 0;
    w220_b3a = 0;
    w270_b3a = 0;
  else
    add_b3a = [nub3a(:,i), rxb3a(:,i)];
    if any(add_b3a(:,1)==35)
      w35_b3a = add_b3a(find(add_b3a(:,1)==35),2) * 24000 * net95_keck^2/net35^2;
    else
      w35_b3a = 0;
    end
    if any(add_b3a(:,1)==95)
      w95_b3a = add_b3a(find(add_b3a(:,1)==95),2) * 24000 * net95_keck^2/net95^2;
    else
      w95_b3a = 0;
    end
    if any(add_b3a(:,1)==150)
      w150_b3a = add_b3a(find(add_b3a(:,1)==150),2) * 30000 * net150_keck^2/net150^2;
    else
      w150_b3a = 0;
    end
    if any(add_b3a(:,1)==220)
      w220_b3a = add_b3a(find(add_b3a(:,1)==220),2) * 2000 * net220_keck^2/net220^2;
    else
      w220_b3a = 0;
    end
    if any(add_b3a(:,1)==270)
      w270_b3a = add_b3a(find(add_b3a(:,1)==270),2) * 2000 * net220_keck^2/net270^2;
    else
      w270_b3a = 0;
    end

  end

  % Collect all the weight at 35
  w35 = w35_14 + w35_b3a;

  % Collect all the weight at 95
  if any(add_keck(:,1)==95)
    w95_keck = add_keck(find(add_keck(:,1)==95),2) .* 24000;
  else
    w95_keck = 0;
  end
  w95 = w95_14 + w95_keck + w95_b3 + w95_b3a;

  % Collect all the weight at 150
  if any(add_keck(:,1)==150)
    w150_keck = add_keck(find(add_keck(:,1)==150),2) .* 20000;
  else
    w150_keck = 0;
  end
  w150 = w150_14 + w150_keck + w150_b3a;

  % Collect all the weight at 220
  if any(add_keck(:,1)==220)
    w220_keck = add_keck(find(add_keck(:,1)==220),2) .* 2000;
  else
    w220_keck = 0;
  end
  w220 = w220_14 + w220_keck + w220_b3a;

  % Collect all the weight at 270
  if any(add_keck(:,1)==270)
    w270_keck = add_keck(find(add_keck(:,1)==270),2) .* 2000 .* (7.05/11.06)^2;
  else
    w270_keck = 0;
  end
  w270 = w270_14 + w270_keck + w270_b3a;

  % Given the resolution, form the weight array
  w35_tot(:,i) = w35;
  w95_tot(:,i) = w95;
  w150_tot(:,i) = w150;
  w220_tot(:,i) = w220;
  w270_tot(:,i) = w270;
end

% Collect all the weight information
ww = [w35_tot; w95_tot; w150_tot; w220_tot; w270_tot];
% Conversion factor from weight to map depth; calculated from published numbers.
gamma = 2.89*10^6;
% Calcualte map depth
nn = sqrt(gamma)./sqrt([ww]);

return

%%%%%%%%%%%%%%%%%%%%%
function [c35,c95,c150,c220,c270]=get_colors2()
c35=[1,0.4,0.4];
c95=[0.8,0.0,0];
c150=[0,0.5,0];
c220=[0.4,0.4,1];
c270=[0,0,0.6];
return
%%%%%%%%%%%%%%%%%%%%%
function cm=blend_colors(c1,c2,N)
z = repmat(cvec(linspace(0,1,N)), 1, 3);
a1 = bsxfun(@times, 1-z, c1.^2);
a2 = bsxfun(@times, z, c2.^2);
cm = sqrt(a1 + a2);
return
%%%%%%%%%%%%%%%%%%%%%
function cm=get_grad2(co)
cm = blend_colors(co, [1,1,1], 10);
return
%%%%%%%%%%%%%%%%%%%%%
function cm=get_2col_grad2(co1,co2)
N=10;
ct=zeros(N,3);
cm = [
  blend_colors([1,1,1], co1,N);
  blend_colors(co1, co2,N);
  blend_colors(co2, [1,1,1],N);
  ];
return

%ct(:,1) = co1(1) + (1-co1(1))*linspace(0,1,N);
%ct(:,2) = co1(2) + (1-co1(2))*linspace(0,1,N);
%ct(:,3) = co1(3) + (1-co1(3))*linspace(0,1,N);
%cm(:,1) = ct(end:-1:1,1);
%cm(:,2) = ct(end:-1:1,2);
%cm(:,3) = ct(end:-1:1,3);
%ct(:,1) = co1(1) + (co2(1)-co1(1))*linspace(0,1,N);
%ct(:,2) = co1(2) + (co2(1)-co1(2))*linspace(0,1,N);
%ct(:,3) = co1(3) + (co2(1)-co1(3))*linspace(0,1,N);
%cm(:,1) = [cm(:,1);ct(:,1)];
%cm(:,2) = [cm(:,2);ct(:,2)];
%cm(:,3) = [cm(:,3);ct(:,3)];
%ct(:,1) = co2(1) + (1-co2(1))*linspace(0,1,N);
%ct(:,2) = co2(2) + (1-co2(2))*linspace(0,1,N);
%ct(:,3) = co2(3) + (1-co2(3))*linspace(0,1,N);
%cm(:,1) = [cm(:,1);ct(:,1)];
%cm(:,2) = [cm(:,1);ct(:,1)];
%cm(:,3) = [cm(:,1);ct(:,1)];
%return
%%%%%%%%%%%%%%%%%%%%%
function make_sensproj

figure(1); clf
setwinsize(gcf,700,500);

% plot lines to illustrate program
subplot_grid(5,1,1);

% define the colors once
[c35,c95,c150,c220,c270]=get_colors2();

% define vert posn of bars
k1=[10,10]; k2=[9,9]; k3=[8,8]; k4=[7,7]; k5=[6,6];
b3=[3.8,3.8];

x=linspace(k5(1)-0.5,k1(1)+0.5,4);
ba1=[x(4),x(4)];
ba2=[x(3),x(3)];
ba3=[x(2),x(2)];
ba4=[x(1),x(1)];

% Keck slot 1
plot([2012,2017],k1,'Color',c150,'linewidth',2);
hold on
% Keck slot 2+3
plot([2012,2014],k2,'Color',c150,'linewidth',2);
plot([2012,2014],k3,'Color',c150,'linewidth',2);
plot([2014,2017],k2,'Color',c220,'linewidth',2);
plot([2014,2017],k3,'Color',c220,'linewidth',2);
% Keck slot 4+5
plot([2012,2013],k4,'Color',c150,'linewidth',2);
plot([2012,2013],k5,'Color',c150,'linewidth',2);
plot([2013,2015],k4,'Color',c95,'linewidth',2);
plot([2013,2015],k5,'Color',c95,'linewidth',2);
plot([2015,2016],k4,'Color',c220,'linewidth',2);
plot([2015,2016],k5,'Color',c220,'linewidth',2);
plot([2016,2017],k4,'Color',c270,'linewidth',2);
plot([2016,2017],k5,'Color',c270,'linewidth',2);

% B3
plot([2014,2015],b3,'Color',c95,'linewidth',4);
plot([2015,2021],b3,'Color',c95,'linewidth',12);

% BA slot 1
plot([2017,2021],ba1,'Color',c35,'linewidth',12);
% BA slot 2
plot([2017,2018],ba2,'Color',c270,'linewidth',2);
plot([2018,2021],ba2,'Color',c95,'linewidth',12);
% BA slot 3
plot([2017,2018],ba3,'Color',c270,'linewidth',2);
plot([2018,2021],ba3,'Color',c150,'linewidth',12);
% BA slot 4
plot([2017,2019],ba4,'Color',c270,'linewidth',2);
%plot([2019,2021],ba4,'Color',c220,'linewidth',12);
%plot([2019,2021],ba4,'--','Color',c270,'linewidth',12);
plot([2019,2021],ba4+0.4,'Color',c220,'linewidth',6);
plot([2019,2021],ba4-0.4,'Color',c270,'linewidth',6);

% arrows
y=12.2;
plot([2012,2013.2],[y,y],'k','linewidth',1);
plot([2014.8,2016],[y,y],'k','linewidth',1);
plot([2015.9,2016,2015.9],[y+.5,y,y-.5],'k','linewidth',1);
text(2014,y,'funded operations','HorizontalAlignment','center');
plot([2016,2016.5],[y,y],'k','linewidth',1);
plot([2018.5,2021],[y,y],'k','linewidth',1);
plot([2016.1,2016,2016.1],[y+.5,y,y-.5],'k','linewidth',1);
plot([2020.9,2021,2020.9],[y+.5,y,y-.5],'k','linewidth',1);
text(2017.5,y,'proposed operations','HorizontalAlignment','center');

h=text(2014.5,k3(1),'Keck Array','HorizontalAlignment','center');
set(h,'backgroundcolor','w'); set(h,'margin',0.1);
h=text(2019,mean([ba2(1),ba3(1)]),'BICEP Array','HorizontalAlignment','center');
set(h,'backgroundcolor','w'); set(h,'margin',0.2);
h=text(2017.5,b3(1),'BICEP3','HorizontalAlignment','center');
set(h,'backgroundcolor','w'); set(h,'margin',0.1);


hold off
xlim([2012,2021]); ylim([2.5,13]);
subplot_grid2(5,1,1);
grid
set(gca,'YTick',[]);
set(gca,'TickLength',[0,0]);

makegridsolidfaint()

text(2013.3,14,'Stage 2','HorizontalAlignment','center','fontsize',14,'fontweight','bold');
text(2018.3,14,'Stage 3','HorizontalAlignment','center','fontsize',14,'fontweight','bold');

% Time resolution for which the projections have been pre-calculated
rez = 21;
% Get the time mapping and the years of operation
time = [2012:1/(rez-1):2021];
year = [2013:2021];

% map depth vs time
subplot_grid(5,1,[2,3]);

% get weight, transform into map-depth, and plot
for i = 1:length(year)
  ww = depth_weight(year(i));
  gamma = 2.89*10^6;
  nn = sqrt(gamma)./sqrt([ww]);
  hold on;
  h(3)=plot(time(rez*(i-1)-i+2:(rez-1)*i+1),nn(3,:),'Color',c150,'LineWidth',2);
  h(4)=plot(time(rez*(i-1)-i+2:(rez-1)*i+1),nn(4,:),'Color',c220,'LineWidth',2);
  h(5)=plot(time(rez*(i-1)-i+2:(rez-1)*i+1),nn(5,:),'Color',c270,'LineWidth',2);
  h(1)=plot(time(rez*(i-1)-i+2:(rez-1)*i+1),nn(1,:),'Color',c35,'LineWidth',2);
  h(2)=plot(time(rez*(i-1)-i+2:(rez-1)*i+1),nn(2,:),'Color',c95','LineWidth',2);
end

xlim([2012,2021]); ylim([1,100]);
set(gca,'XTick',year,'XtickLabel',[])
ylabel('Map Sensitivity [\muK-arcmin]');
set(gca,'yscale','log')
box on
grid on

hold all;
% Plot the published 2013 (150) and 2014 (95, 150) points
plot(2013,3.4,'kx','MarkerSize',8,'LineWidth',2)
plot(2014,3.0,'kx','MarkerSize',8,'LineWidth',2)
plot(2014,7.62,'kx','MarkerSize',8,'LineWidth',2)

% Plot a point for 220 based on 2015 prelim maps
plot(2014.85,30.06,'kx','MarkerSize',8,'LineWidth',2,'color',[.5,.5,.5])

text(2013.2,3.95,'150 GHz')
text(2014.4,8,'95 GHz')
text(2015.2,28,'220 GHz')
text(2017.6,36.3,'270 GHz')
text(2018.9,7.5,'35 GHz')

makegridsolidfaint()

%subplot_grid2(5,1,[2,3]);

% sigma(r) vs time
subplot_grid(5,1,[4,5]);

% load data for Fisher projections calculated with first term only
f1 = load('/n/bicepfs2/users/vbuza/2016_01/17/sigr_full_f1.mat');

% Temporary rescaling to get the BK14 cross to lie on top of the Fisher line
fac_temp = [0.024./f1.sr_store(2,41)];

% Plot
for ii = 1:5
  hold all;
  if ii == 1
    % A_L = 1.0 case
    plot(f1.sr_store(1,:),fac_temp.*f1.sr_store(2,:),'k','LineWidth',2,'color',[.5,.5,.5])
  elseif ii == 2
    % A_L = 0.5 case
    plot(f1.sr_store(1,:),fac_temp.*f1.sr_store(3,:),'k','LineWidth',2,'color',[.5,.5,.5])
  elseif ii == 3
    % A_L = 0.2 case
    plot(f1.sr_store(1,:),fac_temp.*f1.sr_store(4,:),'k','LineWidth',2,'color',[.5,.5,.5])
  elseif ii == 4
    % Knox raw sensitivity
    plot(f1.tqk,fac_temp.*f1.sqk,':r','LineWidth',2)
  % Optional line
  %elseif ii == 5
    % Knox marginalized
    %plot(f1.tqkm, fac_temp .* f1.sqkm, 'r-.', 'LineWidth',2)
  end
end

hold all;
% Plot BK14, BK13 (statistic derived in BK14 if it were applied to BK13; note
% BK13 is not BKP, BK13 includes same bands as BK14, and full autos for Planck.)
% and BK20, derived from Gaussian sims
plot([2013 2013], [0.032 0.032], 'kx', 'MarkerSize', 8, 'LineWidth', 2)
plot([2014 2014], [0.024 0.024], 'kx', 'MarkerSize', 8, 'LineWidth', 2)
%plot([2020 2020], [0.007 0.007], 'x','Color',[0.4,0.4,0.4], 'MarkerSize', 8, 'LineWidth', 2)

set(gca,'yscale','log')
ylabel('\sigma(r)');
grid on
box on
xlim([2012,2021]); ylim([3e-4,0.05]);

% shift the year labels to mid-year
x=get(gca,'xticklabel');
x(1:end-1,:)=x(2:end,:); x(end,:)=' ';
set(gca,'xticklabel',[repmat('                    ',size(x,1),1),x])

h=text(2020.3,0.0080,'A_L=1.0');
h=text(2020.3,0.0044,'A_L=0.5');
h=text(2020.3,0.0025,'A_L=0.2');
h=text(2019,0.0009,'Knox raw sensitivity');

subplot_grid2(5,1,[4,5]);

makegridsolidfaint()

print -depsc2 proposal_plots/bk_projections.eps
fix_lines('proposal_plots/bk_projections.eps')
!epstopdf proposal_plots/bk_projections.eps

return

%%%%%%%%%%%%%%%%%%%%
function makegridsolidfaint()
% serious nonsense to get a solid faint grid
set(gca,'gridlinestyle','-'); set(gca,'minorgridlinestyle','-')
set(gca,'xcolor',[.8,.8,.8]); set(gca,'ycolor',[.8,.8,.8])
Caxes=copyobj(gca,gcf);
set(Caxes,'color','none','xcolor','k','xgrid','off','ycolor','k','ygrid','off');
return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function make_fpplots

expt=get_experiment_name;
clear find_file_by_date;
[c35,c95,c150,c220,c270]=get_colors2();
switch expt
  case 'keck'
    [p ind]=get_array_info('20151201');
    lng=[-8:.05:8];
    [xx yy]=meshgrid(lng,lng);
    fwhm=[43,21,43,21,30]./60;
    for(i=1:5)
      % For each rx, rotate to drumangle 0 and get detector coordinates
      slc=[];
      slc=(p.rx==(i-1)&strcmp(p.pol,'A'));
      r=p.r(slc);
      th=p.theta(slc);
      alph=p.alpha(slc);
      sig=fwhm(i)/(2.3548);
      drm = unique(p.drumangle(slc));
      frq = unique(nonzeros(p.band(slc)));
      th=th+drm;
      th=deg2rad(th);
      [x,y]=pol2cart(th,r);
      z=zeros(size(xx));
      for(j=1:length(x))
        if(~isnan(x(j)))
          param=[.1,x(j),y(j),sig,sig,0,0];
          z=z+egauss2(param,xx,yy);
        end

      end

      figure(1);
      setwinsize(gcf,150,150);
      imagesc(lng,lng,z);
      set(gca,'PlotBoxAspectRatio',[1,1,1])
      set(gca,'ytick',[]);
      xlabel('Degrees on sky');
      set(gca,'FontSize',12);
      %axis equal;
      
      switch frq
        case 100
          colormap(get_grad2(c95));
        case 150
          colormap(get_grad2(c150));
        case 220
          colormap(get_grad2(c220));
      end

      print(sprintf('proposal_plots/keck_rx%i_fp.eps',i-1));
    end
  case 'bicep3'
    [p ind]=get_array_info('20151201');
    slc=strcmp(p.pol,'A');
    slc(ind.d)=1;
    r=p.r(slc);
    th=p.theta(slc);
    drm = unique(p.drumangle);
    frq = unique(nonzeros(p.band));
    th=th+drm;
    th=deg2rad(th);
    [x,y]=pol2cart(th,r);
    lng=[-14:.1:14];
    [xx yy]=meshgrid(lng,lng);
    z=zeros(size(xx));
    sig=(24/(60*2.3548));
    for(j=1:length(x))
      if(~isnan(x(j)))
        param=[.5,x(j),y(j),sig,sig,0,0];
        z=z+egauss2(param,xx,yy);
      end

    end

    figure(1);
    imagesc(lng,lng,z);
    axis equal;
    set(gca,'ytick',[]);
    set(gca,'Xtick',[-10,-5,0,5,10])
    xlabel('Degrees on sky');
    set(gca,'FontSize',12);
    setwinsize(gcf,250,250);

    colormap(get_grad2(c95));

    print 'proposal_plots/bicep3_fp.eps';
    !epstopdf proposal_plots/bicep3_fp.eps
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function make_b3a_fp

tiwd=87.89;
Npixs=[4,12,18];
[c35,c95,c150,c220,c270]=get_colors2();
lbls=['35GHZ';'95GHZ';'15GHZ'];
fwhm=[68,24,15];
for(f=1:3)
  pxsz=tiwd/Npixs(f);
  Npix=Npixs(f);
  gap=15;
  ctrgap=Npix*pxsz+gap;
  pxprti=Npix^2;
  sig=fwhm(f)/(60*2.3548);
  x=[];
  y=[];
  ctrx=[0:ctrgap:3*ctrgap];
  ctrx=ctrx-2*(pxsz*Npix)-1.5*gap+pxsz*Npix/2;
  ctry=ctrx;

  % For central 4 tiles
  for(v=1:2)
    for(t=1:4)
      rx= ctrx(t)+Npix/2*pxsz;
      lx= ctrx(t)-Npix/2*pxsz;
      uy= ctry(v+1)+Npix/2*pxsz;
      ly= ctry(v+1)-Npix/2*pxsz;


      for(i=1:Npix)
        for(j=1:Npix)
          x(i+Npix*(j-1)+(t-1)*pxprti+(v-1)*4*pxprti)=lx+pxsz/2+pxsz*(mod(i,Npix));
          y(i+Npix*(j-1)+(t-1)*pxprti+(v-1)*4*pxprti)=ly+pxsz/2+pxsz*(mod(j,Npix));
        end
      end
    end
  end

  idx=length(x);

  %For top two tiles
  for(t=1:2)
    rx=ctrx(t+1)+Npix/2*pxsz;
    lx=ctrx(t+1)-Npix/2*pxsz;
    uy=ctry(4)+Npix/2*pxsz;
    ly=ctry(4)-Npix/2*pxsz;

    for(i=1:Npix)
      for(j=1:Npix)
        x(idx+i+Npix*(j-1)+(t-1)*pxprti)=lx+pxsz/2+pxsz*(mod(i,Npix));
        y(idx+i+Npix*(j-1)+(t-1)*pxprti)=ly+pxsz/2+pxsz*(mod(j,Npix));
      end
    end
  end

  idx=length(x);

  %For bottom two tiles
  for(t=1:2)
    rx=ctrx(t+1)+Npix/2*pxsz;
    lx=ctrx(t+1)-Npix/2*pxsz;
    uy=ctry(1)+Npix/2*pxsz;
    ly=ctry(1)-Npix/2*pxsz;

    for(i=1:Npix)
      for(j=1:Npix)
        x(idx+i+Npix*(j-1)+(t-1)*pxprti)=lx+pxsz/2+pxsz*(mod(i,Npix));
        y(idx+i+Npix*(j-1)+(t-1)*pxprti)=ly+pxsz/2+pxsz*(mod(j,Npix));
      end
    end
  end

  %Project onto sky using plate scale 15.328 mm/deg
  x=x/15.328;
  y=y/15.328;

  [r,theta]=cart2pol(x,y);
  theta=deg2rad(theta);
  scl=[-14:.1:14];
  [xx yy]=meshgrid(scl,scl);
  z=zeros(size(xx));
  for(i=1:length(x))
    param=[.5,x(i),y(i),sig,sig,0,0];
    z=z+egauss2(param,xx,yy);
  end
  figure(1);
  imagesc(scl,scl,z);
  set(gca,'ytick',[]);
  set(gca,'xtick',[-10,-5,0,5,10]);
  set(gca,'fontsize',12);
  xlabel('Degrees on sky')
  setwinsize(gcf,250,250);

  switch f
    case 1
      colormap(get_grad2(c35));
      print 'proposal_plots/b3a_35GHZ_fp.eps';
      !epstopdf proposal_plots/b3a_35GHZ_fp.eps
    case 2
      colormap(get_grad2(c95));
      print 'proposal_plots/b3a_95GHZ_fp.eps';
      !epstopdf proposal_plots/b3a_95GHZ_fp.eps
    case 3
      colormap(get_grad2(c150));
      print 'proposal_plots/b3a_150GHZ_fp.eps';
      !epstopdf proposal_plots/b3a_150GHZ_fp.eps
  end
end

% For Mixed 220/270 Reciever

Npix=28;
gap=15;
pxsz=tiwd/Npix;
sig20=11/(60*2.3548);
sig70=9/(60*2.3548);
tguess=tiwd+gap;
lx=-tiwd/2-gap/4+pxsz/2;
ly=lx;
xt=repmat([1:Npix],Npix,1); 
yt=repmat([1:Npix],1,Npix);
yt=(yt)*pxsz+ly;
xt=(xt)*pxsz+lx;
[r c]=size(xt);
xt=reshape(xt,1,prod(size(xt)));
scale=[-tguess/2:.5:tguess/2];
[xx,yy]=meshgrid(scale,scale);
z=zeros(size(xx));
for(i=1:length(xt))
  param=[.6,xt(i),yt(i),sig20*15.328,sig20*15.328,0,0];
  z=z+egauss2(param,xx,yy);
end
z=z/max(z(:));
figure(2)
clf;
ons=ones(size(z));
zrs=zeros(size(ons));
im20(:,:,1)=ons+.6*(z-1);
im20(:,:,2)=ons+.6*(z-1);
im20(:,:,3)=ons;
im70(:,:,1)=z;
im70(:,:,2)=z;
im70(:,:,3)=ons+.4*(z-1);
blank20(:,:,1)=ons*.4;
blank20(:,:,2)=ons*.4;
blank20(:,:,3)=ons;
blank70(:,:,1)=zrs;
blank70(:,:,2)=zrs;
blank70(:,:,3)=.6*ons;
r1=cat(2,blank70,im20,im70,blank20);
r2=cat(2,im20,im70,im20,im70);
r3=cat(2,im70,im20,im70,im20);
r4=cat(2,blank20,im70,im20,blank70);

image(4*scale/15.238,4*scale/15.238,cat(1,r1,r2,r3,r4));
set(gca,'Ytick',[]);
set(gca,'fontsize',12);
xlabel('Degrees on sky')
setwinsize(gcf,250,250);

print 'proposal_plots/b3a_220.eps';
!epstopdf proposal_plots/b3a_220.eps
return




