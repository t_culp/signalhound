function examine(cmd,varargin)
% EXAMINE  Make daily examiner plots and HTML pages.
%
%  Depending on the arguments, this function can
%  make daily examiner plots for observing periods,
%  cryo service periods, or all observing and cryo
%  operations over a span of time.  It can also
%  write the HTML index pages that organize the
%  plots.
%
%  The default output directory is ./daily_examiner_plots.
%
%  Usage examples:
%
%    examine('all plots');      % Make examiner plots
%                               % for all obs schedules
%                               % and fridge cycles; save
%                               % in ./daily_examiner_plots/.
%
%    examine('all plots',LOG_DIR,OUT_DIR);
%                               % Same as above, but find
%                               % GCP logs in LOG_DIR and
%                               % save output in OUT_DIR.
%
%    examine('all plots',[],[], ...
%        datenum(2010,05,01),datenum(2010,05,31));
%                               % Make plots only for the
%                               % month of May, 2010.
%
%    examine('html');           % Write HTML index and nav
%                               % pages for existing plots.
%

if nargin<1 || isempty(cmd)
  cmd='all plots';
end

switch(cmd)
  case 'html',
    make_examiner_html(varargin{:});
    return
  case 'all plots',
    examine_all_plots(varargin{:});
    return
  case 'one cycle',
    examine_one_cycle(varargin{:});
    return
  case 'one run',
    examine_one_run(varargin{:});
    return
end
return


function examine_one_run(t0,t1,out_dir)
if nargin<3 || isempty(out_dir)
  out_dir='daily_examiner_plots';
end
[D,S]=define_examiner_plots(t0);
h2=make_examiner_plots(D,S,'arc',t0,t1,30/60/60/24);
disp('Done making plots, now to save them.');
for jj=1:length(h2)
  disp(['Saving plot #' num2str(jj) ', h=' num2str(h2(jj))]);
  figure(h2(jj));
  disp(['File name will be ' fullfile(out_dir,['daily_examiner_' datestr(t0,'yyyymmddHHMMSS') '_page' num2str(jj)]) ]);
  mkpng(fullfile(out_dir,['daily_examiner_' datestr(t0,'yyyymmddHHMMSS') '_page' num2str(jj)]),1);
  % print(h2(jj),'-dpng',fullfile(out_dir,['daily_examiner_' datestr(last_t1,'yyyymmddHHMMSS') '_page' num2str(jj)]));
end


function examine_one_cycle(t0,t1,out_dir)
h1=make_cycle_plot('arc',t0-1/24,t1+1/24);
for jj=1:length(h1)
  figure(h1(jj));
  mkpng(fullfile(out_dir,['daily_examiner_' datestr(t0,'yyyymmddHHMMSS') '_page' num2str(jj)]),1);
end

return


function [D S]=define_examiner_plots(t0)
expt=get_experiment_name;
if (nargin<1) || isempty(t0)
  [D S]=eval(['def_examiner_' expt '([])']);
else
  [D S]=eval(['def_examiner_' expt '(t0)']);
end
return


function H=get_about_filename()
expt=get_experiment_name;
examiner_dir=fileparts(mfilename('fullpath'));
html_dir=fullfile(examiner_dir,'..','docs','examiner');
H=fullfile(html_dir,[expt '_about.html']);
return


function examine_all_plots(log_dir,out_dir,t0,t1)
if nargin<1 || isempty(log_dir)
  log_dir='log';
end
if nargin<2 || isempty(out_dir)
  out_dir='daily_examiner_plots';
end
if nargin<3 || isempty(t0)
  t0=datenum(2010,1,1);
elseif ischar(t0)
  t0=datenum(t0);
end
if nargin<4 || isempty(t1)
  t1=datenum(3000,1,1);
elseif ischar(t1)
  t1=datenum(t1);
end

disp ('Looking through log files.');
[C F]=parse_run_log(log_dir,datestr(t0-4,'yyyy-mmm-dd:HH:MM:SS'),datestr(t1,'yyyy-mmm-dd:HH:MM:SS'));
disp (['Found ' num2str(length(F)) ' cycles.  Making plots.']);

ttlist=[F(:).t];
ttlist=mod_ttlist(ttlist);
t0list=ttlist(1:2:end);
t1list=ttlist(2:2:end);
if t1list(end)<t1
  % t0list=[t0list(:); t1];          % make last schedule plot through end time t1
  t0list=[t0list(:); t1list(end)+3]; % make last schedule plot through start time + 3 days
  t1list=[t1list(:); NaN];
end
last_t1=NaN;
for ii=1:length(t0list)
  if t0list(ii)<t0 || t0list(ii)>t1
    last_t1=t1list(ii);
    continue
  end
  if ~isnan(t1list(ii))
    % Make cycle plots for a max of 10 hrs
    tmpt1=t1list(ii);
    tmpt1=min(tmpt1,t0list(ii)+10/24);
    disp(['t0=' datestr(t0list(ii)) ', t1=' datestr(tmpt1)]);
    h1=make_cycle_plot('arc',t0list(ii)-1/24,tmpt1+1/24);
    for jj=1:length(h1)
      figure(h1(jj));
      mkpng(fullfile(out_dir,['daily_examiner_' datestr(t0list(ii),'yyyymmddHHMMSS') '_page' num2str(jj)]),1);
    end
  end;
  close all
  if ~isnan(last_t1)
    [D,S]=define_examiner_plots(last_t1);
    h2=make_examiner_plots(D,S,'arc',last_t1,t0list(ii),1/60/24);
    for jj=1:length(h2)
      figure(h2(jj));
      mkpng(fullfile(out_dir,['daily_examiner_' datestr(last_t1,'yyyymmddHHMMSS') '_page' num2str(jj)]),1);
      % print(h2(jj),'-dpng',fullfile(out_dir,['daily_examiner_' datestr(last_t1,'yyyymmddHHMMSS') '_page' num2str(jj)]));
    end
  end
  close all
  last_t1=t1list(ii);
end

return



function make_examiner_html(logdir,plotdir,htmldir,reldir)
if nargin<1 || isempty(logdir)
  logdir='log';
end
if nargin<2 || isempty(plotdir)
  plotdir='daily_examiner_plots';
end
if nargin<3 || isempty(htmldir)
  htmldir=plotdir;
end
if nargin<4 || isempty(reldir)
  reldir='';
end
[C F S]=parse_run_log(logdir);
disp (['Found ' num2str(length(F)) ' cycles.  Making plots.']);
ttlist=[F(:).t];
ttlist=mod_ttlist(ttlist);
t0list=ttlist(1:2:end);
t1list=ttlist(2:2:end);
t0list=[t0list(:); datenum(now)+1];
t1list=[t1list(:); NaN];
last_t1=NaN;
[D setup]=define_examiner_plots([]);

% List each cycle and in-between
page_list=[];
for ii=1:length(t0list)
  if ~isnan(last_t1)
    if exist(fullfile(plotdir,['daily_examiner_' datestr(last_t1,'yyyymmddHHMMSS') '_page1.png']))
      page_list(end+1).name=datestr(last_t1,'yyyymmddHHMMSS');
      page_list(end).str=datestr(last_t1,'yyyy-mm-dd HH:MM:SS');
      page_list(end).cyc=0;
    end
  end
  if ~isnan(t1list(ii))
    if exist(fullfile(plotdir,['daily_examiner_' datestr(t0list(ii),'yyyymmddHHMMSS') '_page1.png']))
      disp(['t0=' datestr(t0list(ii)) ', t1=' datestr(t1list(ii))]);
      page_list(end+1).name=datestr(t0list(ii),'yyyymmddHHMMSS');
      page_list(end).str=datestr(t0list(ii),'yyyy-mm-dd HH:MM:SS');
      page_list(end).cyc=1;
    end
  end
  last_t1=t1list(ii);
end

% Each individual html page
for ii=1:length(page_list)
  h=fopen(fullfile(htmldir,[page_list(ii).name '.html']),'wt');
  fprintf(h,'<html>\n\n<body>\n\n');
  % fprintf(h,'    <center><font size="+2">The Tri-Daily Examiner</font></center>\n');
  fprintf(h,'    <center><img src="the_daily_examiner.png" alt="%s"></center>\n', setup.title);
  if page_list(ii).cyc==1
    fprintf(h,'    <center><h3>Fridge cycle</h3></center>\n');
  else
    fprintf(h,'    <center><h3>Observing data</h3></center>\n');
  end
  fprintf(h,'    <center><h3>');
  if ii>1
    fprintf(h,'[<a href="%s.html">prev</a>]',page_list(ii-1).name);
  else
    fprintf(h,'<b>[prev]</b>');
  end
  fprintf(h,' [%s] ',page_list(ii).str);
  if ii<length(page_list)
    fprintf(h,'[<a href="%s.html">next</a>]',page_list(ii+1).name);
  else
    fprintf(h,'<b>[next]</b>');
  end
  fprintf(h,'</center>\n');
  fprintf(h,'<center><h3>');
  if page_list(ii).cyc==1
    type_tag='cycle';
  else
    type_tag='obs';
  end
  ii_prev=NaN;
  for jj=(ii-1):-1:1
    if page_list(jj).cyc==page_list(ii).cyc
      ii_prev=jj;
      break;
    end
  end
  if ~isnan(ii_prev)
    fprintf(h,'[<a href="%s.html">prev %s</a>]',page_list(ii_prev).name,type_tag);
  else
    fprintf(h,'[prev %s]',type_tag);
  end
  fprintf(h,' ');
  ii_next=NaN;
  for jj=(ii+1):length(page_list)
    if page_list(jj).cyc==page_list(ii).cyc
      ii_next=jj;
      break;
    end
  end
  if ~isnan(ii_next)
    fprintf(h,'[<a href="%s.html">next %s</a>]',page_list(ii_next).name,type_tag);
  else
    fprintf(h,'[next %s]',type_tag);
  end
  fprintf(h,'</center>\n');
  fprintf(h,'<table border="0" cellspacing="0" cellpadding="0">\n');
  fprintf(h,'<tr>\n');
  plotlist=dir(fullfile(plotdir,['daily_examiner_' page_list(ii).name '_page*.png']));
  for jj=1:length(plotlist)
    fprintf(h,'  <td>\n');
    fprintf(h,'    <a href="%s" target="_blank">',plotlist(jj).name);
    fprintf(h,'<img src="%s">\n',plotlist(jj).name);
    fprintf(h,'  </td>\n');
  end
  fprintf(h,'</tr>\n');
  fprintf(h,'</table>\n\n</body>\n\n</html>\n\n');
  fclose(h);
end

% Left pane with list of cycles and examiner pages
h=fopen(fullfile(htmldir,'plot_index.html'),'wt');
fprintf(h,'<html>\n\n');
fprintf(h,'<body bgcolor="#d0d0d0">\n\n');
fprintf(h,'Go to:\n');
fprintf(h,'<pre>\n');
fprintf(h,'<b><a href="%s" target="plotpage">About examiner</a></b>\n\n','about.html');
fprintf(h,'<b><font color="blue">Obs. data</font></b>\n');
fprintf(h,'&nbsp;&nbsp;&nbsp;&nbsp;<b><font color="green">Fridge cycle</font></b>\n\n');
for ii=length(page_list):-1:1
  if page_list(ii).cyc==1
    fprintf(h,'&nbsp;&nbsp;&nbsp;&nbsp;');
    fprintf(h,'<a href="%s.html" target="plotpage"><font color="green">%s</font></a>\n',page_list(ii).name,page_list(ii).str);
  else
    fprintf(h,'<a href="%s.html" target="plotpage"><font color="blue">%s</font></a>\n',page_list(ii).name,page_list(ii).str);
  end
end
fprintf(h,'</pre>\n\n');
fprintf(h,'</body>\n\n');
fprintf(h,'</html>\n\n');
fclose(h);

% Top-level index page
h=fopen(fullfile(htmldir,'index.html'),'wt');
fprintf(h,'<html>\n\n');
fprintf(h,'<frameset noresize="noresize" cols="200,*">\n\n');
fprintf(h,'<frame src="plot_index.html">\n');
fprintf(h,'<frame src="%s.html" name="plotpage">\n\n','about');
fprintf(h,'</frameset>\n\n</html>\n\n');
fclose(h);

% Copy over 'about' page
aboutfname=get_about_filename();
copyfile(aboutfname,fullfile(htmldir,'about.html'));

return



function h=make_cycle_plot(arcdir,t0,t1)
if isnumeric(t0)
  t0=datestr(t0,'yyyy-mmm-dd:HH:MM:SS');
end
if isnumeric(t1)
  t1=datestr(t1,'yyyy-mmm-dd:HH:MM:SS');
end
[D,S]=define_examiner_plots(utc2datenum(t0));
try
  d=load_arc(arcdir,t0,t1,...
    {'antenna0.time',...
     'antenna0.hk*',...
     'antenna0.cycle*'});
catch mexcept
  h=[];
  % Exit on ctrl-C from user.  Otherwise, continue.
  if strfind(mexcept.message,'Exiting at user request')
    rethrow(mexcept);
  end
  return
end
if isempty(d.antenna0.time.utcfast)
  h=[];
  return
end
tf=utc2datenum(d.antenna0.time.utcfast);
ts=utc2datenum(d.antenna0.time.utcslow);
ccf=(tf>=datenum(1900,1,1));
ccs=(ts>=datenum(1900,1,1));
rx_try_list = {'', '0', '1', '2','3','4'};
for jrx=1:length(rx_try_list)
  hk=['hk' rx_try_list{jrx}];
  cycle=['cycle' rx_try_list{jrx}];
  if ~isfield(d.antenna0,hk)
    continue
  end

  fignum=figure;
  set(fignum,'DefaultAxesFontSize',10);
  set(fignum,'DefaultTextFontSize',10);
  subplot(4,1,1,'align');
  % title(['Pump temperatures']);
  if ~isempty(rx_try_list{jrx})
    title(['Fridge cycle ' t0 ', rx' rx_try_list{jrx}]);
  else
    title(['Fridge cycle ' t0]);
  end
  hold on;
  plot(ts(ccs),d.antenna0.(hk).slow_temp(ccs,0+1),'r-');
  plot(ts(ccs),d.antenna0.(hk).slow_temp(ccs,2+1),'g-');
  grid on;
  ylabel('T / K');
  xlim([min(ts(ccs)) max(ts(ccs))+1/24]);
  datetick('x','keeplimits');
  legend('He4 pump','He3 pump');
  subplot(4,1,2,'align');
  % title(['HS temperatures']);
  hold on;
  plot(ts(ccs),d.antenna0.(hk).slow_temp(ccs,4+1),'r-');
  plot(ts(ccs),d.antenna0.(hk).slow_temp(ccs,6+1),'g-');
  grid on;
  ylabel('T / K');
  legend('He4 HS','He3 HS');
  xlim([min(ts(ccs)) max(ts(ccs))+1/24]);
  datetick('x','keeplimits');
  subplot(4,1,3,'align');
  % title(['Condenser temperatures']);
  hold on;
  plot(ts(ccs),d.antenna0.(hk).slow_temp(ccs,12+1),'r-');
  plot(ts(ccs),d.antenna0.(hk).slow_temp(ccs,14+1),'g-');
  cgood7=ccs&(d.antenna0.(cycle).state(:,1)==7)&(d.antenna0.(hk).slow_temp(:,14+1)>=0.1);
  [min_evap min_evap_idx]=min(d.antenna0.(hk).slow_temp(cgood7,14+1));
  tmp_time=ts(cgood7);
  plot(tmp_time(min_evap_idx),min_evap,'k+','linewidth',2);
  text(0.1,0.85,['min(evap)=' num2str(min_evap) ' K'],...
    'units','normalized');
  grid on;
  ylabel('T / K');
  legend('He4 cond','He4 evap');
  xlim([min(ts(ccs)) max(ts(ccs))+1/24]);
  datetick('x','keeplimits');
  subplot(4,1,4,'align');
  % title(['Cycle state']);
  hold on;
  plot(ts(ccs),d.antenna0.(cycle).state(ccs,1),'r-');
  grid on;
  ylabel('State');
  legend('State');
  xlabel('UTC time');
  xlim([min(ts(ccs)) max(ts(ccs))+1/24]);
  datetick('x','keeplimits');
  if nargout>=1
    if ~exist('h','var')
      h=[];
    end
    h=[h; fignum];
  end
  setwinsize(fignum,S.size(1),S.size(2));
  disp(['Position of figure ' num2str(fignum) ' : ']);
  get(fignum,'Position')
end
return;



function h=make_examiner_plots(D,S,arcdir,t0,t1,dt)
if isnumeric(t0)
  t0=datestr(t0,'yyyy-mmm-dd:HH:MM:SS');
end
if isnumeric(t1)
  t1=datestr(t1,'yyyy-mmm-dd:HH:MM:SS');
end

warnstate=warning('off','MATLAB:interp1:NaNinY');

disp(['Listing arc files from ' t0 ' to ' t1 '.']);
fl=list_arc_files(arcdir,t0,t1);
for iFile=1:length(fl)
  D=accumulate_examiner_data(D,fullfile(arcdir,fl{iFile}),t0,t1,dt);
end
disp(['Done accumulating.  Will make plots.']);
h=[];
def_colrs='bgrkcmy';
for iFig=1:length(D)
  h(iFig)=figure;
  set(h(iFig),'Visible','off');
  set(h(iFig),'DefaultAxesFontSize',10);
  set(h(iFig),'DefaultTextFontSize',10);
  setwinsize(h(iFig),S.size(1),S.size(2));
  for iPlot=1:length(D{iFig})
    subplot(length(D{iFig}),1,iPlot,'align');
    if ~isfield(D{iFig}(iPlot),'t') || isempty(D{iFig}(iPlot).t)
      continue
    end
    if ~isfield(D{iFig}(iPlot),'ss') || isempty(D{iFig}(iPlot).ss)
      ss=zeros(size(D{iFig}(iPlot).t));
    else
      ss_tmp=D{iFig}(iPlot).ss;
      cc=~isnan(ss_tmp) & ...
        ([false; isnan(ss_tmp(1:(end-1)))] | ss_tmp<[0; ss_tmp(1:(end-1))]);
      ss=cumsum(cc);
      ss(isnan(ss_tmp))=NaN;
      ss=ss-nanmin(ss);
    end
    hold on;
    legleg={};
    for iReg=1:length(D{iFig}(iPlot).regs)
      if ~isfield(D{iFig}(iPlot).regs(iReg),'style') || isempty(D{iFig}(iPlot).regs(iReg).style)
        if size(D{iFig}(iPlot).regs(iReg).val,2) == 1
          if size(D{iFig}(iPlot).regs)==1
            plotstyle = [def_colrs(mod(iReg-1,length(def_colrs))+1) '-'];
          else
            plotstyle = [def_colrs(mod(iReg-1,length(def_colrs))+1) '-'];
          end
        else
          plotstyle='.';
        end
      else
        plotstyle=D{iFig}(iPlot).regs(iReg).style;
      end
      if isfield(D{iFig}(iPlot).regs(iReg),'ofs') && ~isempty(D{iFig}(iPlot).regs(iReg).ofs)
        D{iFig}(iPlot).regs(iReg).val=D{iFig}(iPlot).regs(iReg).val-D{iFig}(iPlot).regs(iReg).ofs;
      end
      if isfield(D{iFig}(iPlot).regs(iReg),'scale') && ~isempty(D{iFig}(iPlot).regs(iReg).scale)
        D{iFig}(iPlot).regs(iReg).val=D{iFig}(iPlot).regs(iReg).val*D{iFig}(iPlot).regs(iReg).scale;
      end
      if isfield(D{iFig}(iPlot).regs(iReg),'ofs_ps') && ~isempty(D{iFig}(iPlot).regs(iReg).ofs_ps)
        D{iFig}(iPlot).regs(iReg).val=D{iFig}(iPlot).regs(iReg).val-D{iFig}(iPlot).regs(iReg).ofs_ps;
      end
      if isfield(D{iFig}(iPlot).regs(iReg),'legend') && ~isempty(D{iFig}(iPlot).regs(iReg).legend)
        legleg{iReg}=D{iFig}(iPlot).regs(iReg).legend;
      end
      if isfield(D{iFig}(iPlot).regs(iReg),'on_source') && ~isempty(D{iFig}(iPlot).regs(iReg).on_source) && D{iFig}(iPlot).regs(iReg).on_source==1
        D{iFig}(iPlot).regs(iReg).val(isnan(ss),:)=NaN;
      end
      if isfield(D{iFig}(iPlot).regs(iReg),'dc_sub') && ~isempty(D{iFig}(iPlot).regs(iReg).dc_sub) && D{iFig}(iPlot).regs(iReg).dc_sub==1
        D{iFig}(iPlot).regs(iReg).old_val=D{iFig}(iPlot).regs(iReg).val;
        for iCh=1:size(D{iFig}(iPlot).regs(iReg).val,2)
          tmp=D{iFig}(iPlot).regs(iReg).val(:,iCh);
          cc=(tmp~=0)&~isnan(tmp)&~isinf(tmp);
          if isfield(D{iFig}(iPlot).regs(iReg),'on_source') && ~isempty(D{iFig}(iPlot).regs(iReg).on_source) && D{iFig}(iPlot).regs(iReg).on_source==1
            for onscan=nanmin(ss):nanmax(ss)
              try
              tmp(ss==onscan)=tmp(ss==onscan)-nanmedian(tmp(cc & (ss==onscan)));
              catch
		disp('catch 2')
                keyboard
              end
            end
            D{iFig}(iPlot).regs(iReg).val(:,iCh)=tmp;
          else
            D{iFig}(iPlot).regs(iReg).val(:,iCh)=tmp-nanmedian(tmp(cc));
          end
          D{iFig}(iPlot).regs(iReg).val(~cc,iCh)=NaN;
        end
      end
      try
      % insert NaNs when gaps are more than 3 minutes
      xx=D{iFig}(iPlot).t(:);
      igap=find(diff(xx)>3/60/24);
      yy=D{iFig}(iPlot).regs(iReg).val;
      for j=length(igap):-1:1
        gg=igap(j);
        xx=[xx(1:gg); NaN; xx((gg+1):end)];
        yy=[yy(1:gg,:); NaN*ones(1,size(yy,2)); yy((gg+1):end,:)];
      end
      % plot(D{iFig}(iPlot).t,D{iFig}(iPlot).regs(iReg).val,plotstyle,'MarkerSize',1);
      plot(xx,yy,plotstyle,'MarkerSize',1);
      catch
	disp('catch 3')
      keyboard
      end
    end
    if isfield(D{iFig}(iPlot),'title') && ~isempty(D{iFig}(iPlot).title)
      title(D{iFig}(iPlot).title);
    end
    % xlim([min(D{iFig}(iPlot).t) max(D{iFig}(iPlot).t)]);
    if ischar(t0)
      xlim([utc2datenum(t0), utc2datenum(t1)]);
    else
      xlim([t0 t1]);
    end
    datetick('x','keeplimits');
    grid on;
    if isfield(D{iFig}(iPlot),'ylabel') && ~isempty(D{iFig}(iPlot).ylabel)
      ylabel(D{iFig}(iPlot).ylabel);
    end
    if isfield(D{iFig}(iPlot),'ylim') && ~isempty(D{iFig}(iPlot).ylim)
      ylim(D{iFig}(iPlot).ylim);
    end
    if isfield(D{iFig}(iPlot),'ytick') && ~isempty(D{iFig}(iPlot).ytick)
      set(gca,'YTick',D{iFig}(iPlot).ytick);
    end
    if ~isempty(legleg)
      for iReg=1:length(D{iFig}(iPlot).regs)
        if iReg>length(legleg) || isempty(legleg{iReg})
          legleg{iReg}='';
        end
      end
      legend(legleg{:});
    end
    if isfield(D{iFig}(iPlot),'title') && ~isempty(D{iFig}(iPlot).title)
      title(gca,D{iFig}(iPlot).title);
    end
  end
end
warning(warnstate.state,'MATLAB:interp1:NaNinY');

return



function D_out=accumulate_examiner_data(D_in,arcdir,t0,t1,dt)
D_out=D_in;
if(nargin<3)||isempty(t0)
  t0='1000-Jan-1:00:00:00';
end
if(nargin<4)||isempty(t1)
  t1='3000-Jan-1:00:00:00';
end
if(nargin<5)||isempty(dt)
  dt=1/60/24;			% 1 minute ticks
end
if isnumeric(t0)
  t0=datestr(t0,'yyyy-mmm-dd:HH:MM:SS');
end
if isnumeric(t1)
  t1=datestr(t1,'yyyy-mmm-dd:HH:MM:SS');
end
disp(['Accumulating ' arcdir ' : ' t0 ' - ' t1]);
reglist=list_regs(D_in);
try
  d=load_arc(arcdir,t0,t1,reglist);
catch mexcept
  % Exit on ctrl-C from user.  Otherwise, continue.
  if strfind(mexcept.message,'Exiting at user request')
    rethrow(mexcept);
  end
  return
end
if isempty(d) || ~isfield(d,'antenna0') ...
 || size(d.antenna0.time.utcslow,1) < 2
  return
end
tf=utc2datenum(d.antenna0.time.utcfast);
ts=utc2datenum(d.antenna0.time.utcslow);
if isempty(ts)
  return
end
cf=(tf>datenum(1900,1,1)) & (tf<datenum(3000,1,1));
tt=(min(tf(cf))):dt:(max(tf(cf)));
scanreps=double(d.antenna0.tracker.scan_rep);
scanstates=double(bitand(d.array.frame.features,2^1)>0);
ss=scanreps;
ss(scanstates==0)=NaN;
for iFig=1:length(D_out)
  for iPlot=1:length(D_out{iFig})
    for iReg=1:length(D_out{iFig}(iPlot).regs)
      % Parse register name
      tmp=D_out{iFig}(iPlot).regs(iReg).name;
      [regmap tmp]=strtok(tmp,'.');
      if tmp(1)=='.'
        tmp(1)='';
      end
      [regbrd tmp]=strtok(tmp,'.');
      if tmp(1)=='.'
        tmp(1)='';
      end
      [regblk tmp]=strtok(tmp,'[');
      tmp=strtrim(tmp);
      ch=[];
      if ~isempty(tmp)
        if tmp(1)=='['
          tmp(1)='';
        end
        if tmp(end)==']'
          tmp(end)='';
        end
        if ~isempty(tmp)
          ch=1+str2num(tmp);
        end
      end
      % Get register values and time coordinate
      try
        yy=d.(regmap).(regbrd).(regblk);
      catch
        yy=[];
      end
      if ~isempty(ch) && ~isempty(yy)
        yy=yy(:,ch);
      end
      if size(yy,1)==length(tf)
        xx=tf;
      else
        xx=ts;
      end
      if isfield(D_out{iFig}(iPlot).regs(iReg),'chan') && ~isempty(D_out{iFig}(iPlot).regs(iReg).chan)
        if ~isempty(yy)
          yy=yy(:,D_out{iFig}(iPlot).regs(iReg).chan);
        end
      end
      cc=(xx>datenum(1900,1,1)) & ([1; diff(xx)]>0);
      while ~issorted(xx(cc)) || any(diff(xx(cc))==0)
        last_max=-Inf;
        for ii=1:length(xx)
          if xx(ii)>last_max
            last_max=xx(ii);
          else
            cc(ii)=false;
          end
        end
      end
      if isfield(D_out{iFig}(iPlot).regs(iReg),'bit')
        yy=double(bitand(yy,2^D_out{iFig}(iPlot).regs(iReg).bit)>0);
        yy=yy/(1+0.5*(iReg-1)/length(D_out{iFig}(iPlot).regs));
      end
      if isfield(D_out{iFig}(iPlot).regs(iReg),'utc') && (D_out{iFig}(iPlot).regs(iReg).utc)
        yy=utc2datenum(yy);
      end
      if isfield(D_out{iFig}(iPlot).regs(iReg),'delta') && (D_out{iFig}(iPlot).regs(iReg).delta)
        yy(~cc) = NaN;
        yy=[0; diff(yy)];
      end
      combine='';
      if isfield(D_out{iFig}(iPlot).regs(iReg),'combine')
        combine=D_out{iFig}(iPlot).regs(iReg).combine;
      end
      if isempty(combine)
        combine='nearest';
      end
      if ~isempty(yy)
        switch(lower(combine))
          case 'nearest',
            yy=interp1(xx(cc),double(yy(cc,:)),tt(:),'nearest');
          case {'max','min','bitor','union','or'},
            yy=interp1x(xx(cc),double(yy(cc,:)),tt(:),combine);
        end
      end
      if ~isfield(D_out{iFig}(iPlot).regs(iReg),'val') || isempty(D_out{iFig}(iPlot).regs(iReg).val)
        if isempty(yy)
          yy=NaN*ones(length(tt),1);
        end
        D_out{iFig}(iPlot).regs(iReg).val=yy;
      else
        try
        if isempty(yy)
          tmp=D_out{iFig}(iPlot).regs(iReg).val;
          nch=max(1,size(tmp,2));
          tmp=[tmp; NaN*ones(length(tt),nch)];
          D_out{iFig}(iPlot).regs(iReg).val=tmp;
        else
          tmp=D_out{iFig}(iPlot).regs(iReg).val;
          if size(tmp,2)<size(yy,2)
            tmp=[tmp, NaN*ones(size(tmp,1),size(yy,2)-size(tmp,2))];
          elseif size(tmp,2)>size(yy,2)
            yy=[yy, NaN*ones(size(yy,1),size(tmp,2)-size(yy,2))];
          end
          tmp=[tmp; yy];
          D_out{iFig}(iPlot).regs(iReg).val=tmp;
        end
        catch
	disp('catch 1')
        keyboard
        end
      end
    end
    if ~isfield(D_out{iFig}(iPlot),'t') || isempty(D_out{iFig}(iPlot).t)
      D_out{iFig}(iPlot).t=tt;
    else
      D_out{iFig}(iPlot).t=[D_out{iFig}(iPlot).t tt];
    end
    xx=ts;
    cc=(xx>datenum(1900,1,1)) & ([1; diff(xx)]>0);
    while ~issorted(xx(cc)) || any(diff(xx(cc))==0)
      last_max=-Inf;
      for ii=1:length(xx)
        if xx(ii)>last_max
          last_max=xx(ii);
        else
          cc(ii)=false;
        end
      end
    end
    yy=interp1(xx(cc),double(ss(cc)),tt(:),'nearest');
    if ~isfield(D_out{iFig}(iPlot),'ss') || isempty(D_out{iFig}(iPlot).ss)
      D_out{iFig}(iPlot).ss=yy;
    else
      D_out{iFig}(iPlot).ss=[D_out{iFig}(iPlot).ss(:); yy];
    end
  end
end

return

% If there are multiple short fridge cycle and observing
% schedules found in rapid succession, string them all
% together.  This happens with restarts.
function ttlist=mod_ttlist(ttlist_in)
ttlist=ttlist_in;
c1=false(size(ttlist));
c2=c1;
c1(3:2:end)=true;
c2(2:2:(end-2))=true;
cshort=(ttlist(c1)-ttlist(c2) < 1/24);
cc=true(size(ttlist));
cc(c1)=~cshort;
cc(c2)=~cshort;
ttlist=ttlist(cc);

return

% Make a list of all the registers we need to load for
% examiner plots of observing data
function reglist=list_regs(D)
reglist={};
for i=1:length(D)
  for j=1:length(D{i})
    for k=1:length(D{i}(j).regs)
      % Current register specification
      reg=D{i}(j).regs(k).name;
      % Ignore whitespace, channel selection -- just get reg name
      ll=find(reg=='(' | reg=='[',1,'first');
      if ~isempty(ll)
        reg(ll:end)='';
      end
      reg=strtrim(reg);
      reglist=[reglist, {reg}];
    end
  end
end
reglist=unique([reglist, {'antenna0.time','array','antenna0.tracker.scan_rep'}]);

return

