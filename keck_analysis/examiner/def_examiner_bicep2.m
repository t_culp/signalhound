function [D S]=def_examiner_bicep2(t0)
% DEF_EXAMINER_BICEP2  Set up daily examiner plots
%   for BICEP2.
%
% [D S]=def_examiner_bicep2(T0)
%
% T0 is a Matlab datenum indicating the starting
% time of interest.  This is used only to get the
% appropriate good channel list with get_array_info.
%
% D is a cell array of plot settings.  D{1}
% defines the first figure of examiner plots, D{2}
% the second pane, etc.  Each figure is a vector
% with entries for the various axes: D{1}(1) is
% the top axis in the first figure, D{1}(2) is the
% second axis in the same figure.
%
% See below for examples of how to set up the
% plots.
%
% S is a structure with settings that apply to
% the examiner plots and HTML in general: such as
% page title.

disp ('Setting up examiner for BICEP2.');

S.title='The Tri-Daily Examiner';
S.size=[800 1000];

disp ('Setting up plots for BICEP2.');

% First pane of plots

% Read pixel info from CSV file in aux data
%[ppix,kpix]=ParameterRead('aux_data/fp_data/fp_data_bicep2.csv');
%[ppix,kpix]=ParameterRead('aux_data/fp_data/fp_data_bicep2_20100105.csv');
if (nargin<1) || isempty(t0)
  [ppix,ind]=get_array_info();
else
  [ppix,ind]=get_array_info(datestr(t0,'yyyymmdd'));
end
good_light_pix=strcmp(ppix.type,'L') & (ppix.flag==0 | isnan(ppix.flag));
good_dark_pix=strcmp(ppix.type,'D') & (ppix.flag==0 | isnan(ppix.flag));
good_dark_squid=strcmp(ppix.type,'0') & (ppix.flag==4096);

D{1}(1).regs(1).name='mce0.data.fb';
D{1}(1).regs(1).chan=good_light_pix(1:528);
D{1}(1).regs(1).dc_sub=1;
D{1}(1).regs(1).on_source=1;
D{1}(1).ylim=[-100 100];
D{1}(1).ylabel='light TESs / fbu';

D{1}(2).regs(1).name='mce0.data.fb';
D{1}(2).regs(1).chan=good_dark_pix(1:528);
D{1}(2).regs(1).dc_sub=1;
D{1}(2).regs(1).on_source=1;
D{1}(2).ylim=[-100 100];
D{1}(2).ylabel='dark TESs / fbu';

do_feature_bits=[0 1 3 14];
titl='';
for j=1:length(do_feature_bits)
  onbit=do_feature_bits(j);
  D{1}(3).regs(j).name='array.frame.features';
  D{1}(3).regs(j).bit=onbit;
  switch (onbit)
    case    0, leg='good dat';
    case    1, leg='on src';
    case    2, leg='cycle';
    case    3, leg='el nod';
    case    6, leg='ld crv';
    case    7, leg='sky dip';
    case   14, leg='ld crv';
    otherwise, leg=['bit ' num2str(onbit)];
  end
  % D{1}(3).regs(j).legend=leg;
  titl=[titl ' ' leg];
end
D{1}(3).ylim=[-0.1 1.1];
D{1}(3).ylabel='Feature bits';

D{1}(4).regs(1).name='antenna0.tracker.actual[0]';
D{1}(4).regs(1).scale=180/pi;
% D{1}(4).regs(1).legend='az';
D{1}(4).regs(2).name='antenna0.tracker.actual[1]';
D{1}(4).regs(2).scale=180/pi * 100;
% D{1}(4).regs(2).legend='(el-56^o)\times100';
D{1}(4).regs(2).ofs= 56 / (180/pi);
D{1}(4).regs(3).name='antenna0.tracker.actual[2]';
D{1}(4).regs(3).scale=180/pi;
% D{1}(4).regs(3).legend='dk';
D{1}(4).ylabel='degrees';
D{1}(4).ylim=[-200 400];
D{1}(4).ytick=[-180 0 180 360];
D{1}(4).title='AZ (bl), (EL-56^o)\times100 (gr), DK (red)';

D{1}(5).regs(1).name='antenna0.pmac.mtr_mean_err[0]';
D{1}(5).regs(1).scale=360/2304000 / 100;
D{1}(5).regs(2).name='antenna0.pmac.mtr_mean_err[1]';
D{1}(5).regs(2).scale=360/2304000;
D{1}(5).regs(3).name='antenna0.pmac.mtr_mean_err[2]';
D{1}(5).regs(3).scale=360/574400;
D{1}(5).ylim=[-5 5];
D{1}(5).ylabel='mean err / deg';
D{1}(5).title='AZ/100 (bl), EL (gr), DK (red)';


% Second pane of plots

D{2}(1).regs(1).name='antenna0.hk.fast_temp';
D{2}(1).ylabel='NTDs + fast temps / K';
D{2}(1).ylim=[0.25 0.35];

D{2}(2).regs(1).name='antenna0.hk.tcm_heater';
D{2}(2).regs(2).name='antenna0.pmac.aux_input[0]';
D{2}(2).regs(2).scale=1/10000;
D{2}(2).regs(2).ofs_ps=-4;
D{2}(2).ylabel='TCM htr / V';
D{2}(2).ylim=[0 5];

D{2}(3).regs(1).name='mce0.syncBox.sync_status';
D{2}(3).regs(1).combine='union';
D{2}(3).ylabel='sync status';
D{2}(3).ylim=[0 8];

% D{2}(3).regs(1).name='antenna0.tipper.tatm';
% D{2}(3).ylabel='Tatm / Kelvin';
% D{2}(3).ylim=[150 250];

D{2}(4).regs(1).name='antenna0.time.utcfast';
D{2}(4).regs(1).delta=true;
D{2}(4).regs(1).on_source=1;
D{2}(4).regs(1).utc=true;
D{2}(4).regs(1).combine='max';
D{2}(4).regs(1).scale=24*3600*1000;
D{2}(4).ylabel='\Deltat / ms';
D{2}(4).ylim=[0 100];

% D{2}(4).regs(1).name='antenna0.tipper.tau';
% D{2}(4).ylabel='tau-350';
% D{2}(4).ylim=[0 3];

D{2}(5).regs(1).name='antenna0.weather.windSpeed';
D{2}(5).regs(1).scale=1.94384449;
D{2}(5).ylabel='wnd spd / kts';
D{2}(5).ylim=[0 20];

D{2}(6).regs(1).name='antenna0.weather.windDirection';
D{2}(6).ylabel='wnd dir / deg';
D{2}(6).ylim=[0 360];
D{2}(6).ytick=[0 90 180 270 360];

return

