function [D S]=def_examiner_keck(t0)
% DEF_EXAMINER_KECK  Set up daily examiner plots
%   for the Keck Array.
%
% [D S]=def_examiner_keck(T0)
%
% T0 is a Matlab datenum indicating the starting
% time of interest.  This is used only to get the
% appropriate good channel list with get_array_info.
%
% D is a cell array of plot settings.  D{1}
% defines the first figure of examiner plots, D{2}
% the second pane, etc.  Each figure is a vector
% with entries for the various axes: D{1}(1) is
% the top axis in the first figure, D{1}(2) is the
% second axis in the same figure.
%
% See below for examples of how to set up the
% plots.
%
% S is a structure with settings that apply to
% the examiner plots and HTML in general: such as
% page title.

disp ('Setting up examiner for the Keck Array.');

S.title='The Bi-Daily Examiner';
S.size=[800 1000];


disp ('Setting up plots for the Keck Array.');

% First pane of plots.  Mount, weather, control, etc.

do_feature_bits=[0 1 3 14];
titl='';
for j=1:length(do_feature_bits)
  onbit=do_feature_bits(j);
  D{1}(1).regs(j).name='array.frame.features';
  D{1}(1).regs(j).bit=onbit;
  switch (onbit)
    case    0, leg='good dat';
    case    1, leg='on src';
    case    2, leg='cycle';
    case    3, leg='el nod';
    case    6, leg='ld crv';
    case    7, leg='sky dip';
    case   14, leg='ld crv';
    otherwise, leg=['bit ' num2str(onbit)];
  end
  % D{1}(1).regs(j).legend=leg;
  titl=[titl ' ' leg];
end
D{1}(1).ylim=[-0.1 1.1];
D{1}(1).ylabel='Feature bits';
D{1}(1).title='Control, mount, weather';

D{1}(2).regs(1).name='antenna0.tracker.actual[0]';
D{1}(2).regs(1).scale=180/pi;
% D{1}(2).regs(1).legend='az';
D{1}(2).regs(2).name='antenna0.tracker.actual[1]';
D{1}(2).regs(2).scale=180/pi * 100;
% D{1}(2).regs(2).legend='(el-56^o)\times100';
D{1}(2).regs(2).ofs= 56 / (180/pi);
D{1}(2).regs(3).name='antenna0.tracker.actual[2]';
D{1}(2).regs(3).scale=180/pi;
% D{1}(2).regs(3).legend='dk';
D{1}(2).ylabel='degrees';
D{1}(2).ylim=[-400 400];
D{1}(2).ytick=[-360 -180 0 180 360];
D{1}(2).title='AZ (blu), (EL-56^o)\times100 (gr), DK (red)';

D{1}(3).regs(1).name='antenna0.pmac.fast_az_err[0]';
D{1}(3).regs(1).scale=3600*360/337.64e6 / 10;
D{1}(3).regs(2).name='antenna0.pmac.fast_el_err[0]';
D{1}(3).regs(2).scale=3600*360/3.6e6;
D{1}(3).regs(3).name='antenna0.pmac.fast_dk_err[0]';
D{1}(3).regs(3).scale=3600*360/50.8e6;
D{1}(3).ylim=[-15 15];
D{1}(3).ylabel='fast err / arcsec';
D{1}(3).title='AZ/10 (bl), EL (gr), DK (red)';

D{1}(4).regs(1).name='antenna0.pmac.enc_az_diff[0]';
D{1}(4).regs(1).scale=360/337638296;
D{1}(4).regs(2).name='antenna0.pmac.enc_az_diff[1]';
D{1}(4).regs(2).scale=360/337638296;
D{1}(4).regs(3).name='antenna0.pmac.enc_az_diff[2]';
D{1}(4).regs(3).scale=360/337638296;
D{1}(4).regs(4).name='antenna0.pmac.enc_az_diff[3]';
D{1}(4).regs(4).scale=360/337638296;
D{1}(4).regs(5).name='antenna0.pmac.enc_az_diff[4]';
D{1}(4).regs(5).scale=360/337638296;
D{1}(4).ylabel='az enc diff / deg';
D{1}(4).ylim=[-100 100] * 1e-3;

D{1}(5).regs(1).name='antenna0.weather.airTemperature';
D{1}(5).ylabel='air temp / C';
D{1}(5).ylim=[-100 0];

D{1}(6).regs(1).name='antenna0.weather.windSpeed';
D{1}(6).regs(1).scale=1.94384449;
D{1}(6).ylabel='wnd spd / kts';
D{1}(6).ylim=[0 20];

D{1}(7).regs(1).name='antenna0.weather.windDirection';
D{1}(7).ylabel='wnd dir / deg';
D{1}(7).ylim=[0 360];
D{1}(7).ytick=[0 90 180 270 360];


% Remaining plots per-rx.

% Read pixel info from CSV file in aux data
%[ppix,kpix]=ParameterRead('aux_data/fp_data/fp_data_bicep2.csv');
%[ppix,kpix]=ParameterRead('aux_data/fp_data/fp_data_bicep2_20100105.csv');
if (nargin<1) || isempty(t0)
  [ppix,ind]=get_array_info('20120303');
else
  [ppix,ind]=get_array_info(datestr(t0,'yyyymmdd'));
end
good_light_pix=strcmp(ppix.type,'L') & (ppix.flag==0 | isnan(ppix.flag));
good_dark_pix=strcmp(ppix.type,'D') & (ppix.flag==0 | isnan(ppix.flag));
good_dark_squid=strcmp(ppix.type,'0') & (ppix.flag==4096);

for i=0:4

  % Light and dark TES time streams
  D{i+2}(1).regs(1).name=['mce' num2str(i) '.data.fb'];
  D{i+2}(1).regs(1).chan=good_light_pix((1:528) + 528*i);
  D{i+2}(1).regs(1).dc_sub=1;
  D{i+2}(1).regs(1).on_source=1;
  D{i+2}(1).ylim=[-100 100];
  D{i+2}(1).ylabel=['rx' num2str(i) ' light TESs / fbu'];
  D{i+2}(1).title=['RX' num2str(i)];

  D{i+2}(2).regs(1).name=['mce' num2str(i) '.data.fb'];
  D{i+2}(2).regs(1).chan=good_dark_pix((1:528) + 528*i);
  D{i+2}(2).regs(1).dc_sub=1;
  D{i+2}(2).regs(1).on_source=1;
  D{i+2}(2).ylim=[-100 100];
  D{i+2}(2).ylabel=['rx' num2str(i) ' dark TESs / fbu'];
  
  % HK
  D{i+2}(3).regs(1).name=['antenna0.hk' num2str(i) '.ntd_temp'];
  D{i+2}(3).regs(2).name=['antenna0.hk' num2str(i) '.fast_temp'];
  D{i+2}(3).ylabel=['rx' num2str(i) ' NTDs + fast/ K'];
  D{i+2}(3).ylim=[0.25 0.35];

  D{i+2}(4).regs(1).name=['antenna0.hk' num2str(i) '.tcm_heater'];
  D{i+2}(4).ylabel=['rx' num2str(i) ' TCM htr / V'];
  D{i+2}(4).ylim=[0 5];

  D{i+2}(5).regs(1).name=['antenna0.hk' num2str(i) '.slow_temp[37]'];
  D{i+2}(5).ylabel=['rx' num2str(i) ' 4K plate / K'];
  D{i+2}(5).ylim=[3 5];
end

return

