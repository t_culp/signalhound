function simset_final_comp_pager(sim1, sim2)
% simset_pager()
%
% Make plots and html pager for webpage
% simset_final_comp_pager('sim005_filtp3_weight2_jack0.mat','sim004_filtp3_weight2_jack0.mat')
  
system('rm -rf simset_final_comp_pager')
system('mkdir simset_final_comp_pager')

lmax=[200,500]

for jk=0:6
  for k=1:2
    lm=lmax(k)
    compspec(sim1,sim2,jk, lm)
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%
function compspec(sim1,sim2,jk,lmax)

  jackn1=strfind(sim1,'jack');
  jackn2=strfind(sim2,'jack');

  cut1=strfind(sim1, '_');
  cut2=strfind(sim2, '_');

  s1=strrep(sim1, 'jack0',sprintf('jack%1d',jk));
  s2=strrep(sim2, 'jack0',sprintf('jack%1d',jk));

reduc_final_comp(s1,s2, lmax);
unix('mv *.gif ./simset_final_comp_pager/')

tit=sprintf('%s_%s',s1(1:cut1(1)-1),s2(1:cut2(1)-1));

for i=1:3
  mkhtml(jk,i,tit, lmax)
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mkhtml(jk,fig,s, lmax)

pl={'spec','errorbar','errorratio'};
  
fh=fopen(sprintf('simset_final_comp_pager/%s_jack%1d_%s_%01d.html',s,jk,pl{fig},lmax),'w');

fprintf(fh,'<a href="../">up</a> ------ ');

% write jack types
jk1={'signal','dk jack','scan jack','season jack','fp jack', 'el_coverage','freq jack'};
for i=1:length(jk1)
  if(i-1~=jk)
    fprintf(fh,'<a href="%s_jack%1d_%s_%01d.html">%s</a>, ',s,i-1,pl{fig},lmax,jk1{i});
  else
    fprintf(fh,'%s, ',jk1{i});
  end
end
fprintf(fh,'------ ');
fprintf(fh,'\n');

%write spec, errorbar,....
for i=1:3
  if(i==fig)
    fprintf(fh,'%s, ',pl{i});
  else
    fprintf(fh,'<a href="%s_jack%1d_%s_%01d.html">%s</a>, ',s,jk,pl{i},lmax,pl{i});
     end
end
fprintf(fh,'\n');


% Choose lmax
fprintf(fh,'------ ');
switch lmax
 case 200
  fprintf(fh,'<a href="%s_jack%1d_%s_500.html">lmax=500</a>',s,jk,pl{fig});
 case 500
  fprintf(fh,'<a href="%s_jack%1d_%s_200.html">lmax=200</a>',s,jk,pl{fig});
end

% include the figure
fprintf(fh,'<p><img src="%s_jack%1d_%s_%01d.gif">\n',s,jk,pl{fig}, lmax);

fclose(fh);

return
