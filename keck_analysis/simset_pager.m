function simset_pager()
% simset_pager()
%
% Make plots and html pager for webpage

system('rm -rf simset_pager')
system('mkdir simset_pager')

for jk=0:6
  % make real plots
  compspec(jk,1)

  % make sim plots (2)noise-only, (3)signal only, (4)signal + noise
  for type=2:4
    compspec(jk,type);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%
function compspec(jk,type)

if(type==1)
  ts1='';
  gs1='';
  ts2='';
  gs2='';
elseif( type == 3)
  ts1=sprintf('xxx%1d',type-1);
  ts2=sprintf('xxx%1d',type+2);
  gs1='';
  gs2='_gs'
else
  ts1=sprintf('xxx%1d',type-1);
  ts2=sprintf('xxx%1d',type+2);
  gs1='';
  gs2=''
end

filts1={'p3','n','p3','p3'}; filts2={'p3','n','p3','p3'};

s1=sprintf('%s_filt%s_weight2_jack%1d%s',ts1,filts1{type},jk,gs1);
s2=sprintf('%s_filt%s_weight2_jack%1d%s',ts2,filts2{type},jk,gs2);

reduc_compsimset(sprintf('sim004%s',s1),sprintf('sim005%s',s2));

if(type==1)
  for i=1:2
    mkhtml(jk,type,ts1,gs1,i,s1)
  end
else
  for i=1:6
    mkhtml(jk,type,ts1,gs1,i,s1)
  end
end  

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mkhtml(jk,type,ts,fd,fig,s)

figure(fig); printfig(fig,sprintf('simset_pager/%1d_%s.gif',fig,s),'pp',1400);

fh=fopen(sprintf('simset_pager/%1d_%s.html',fig,s),'w');

fprintf(fh,'<a href="..">up</a> ------ ');

% write jack types
jk1={'signal','dk jack','scan jack','season jack','fp jack','el coverage','freq jack'};
filts={'p3','n','p3','p3'};
for i=1:length(jk1)
  if(i-1~=jk)
    fprintf(fh,'<a href="%1d_%s_filt%s_weight2_jack%1d%s.html">%s</a>, ',fig,ts,filts{type},i-1,fd,jk1{i});
  else
    fprintf(fh,'%s, ',jk1{i});
  end
end
fprintf(fh,'------ ');
fprintf(fh,'\n');

% write data nsim, ssim, snsim
ty={'data','nsim','ssim','snsim'};
tss={'','xxx1','xxx2','xxx3'};
fds={'','','',''};
for i=1:length(ty)
  if(i==type)
    fprintf(fh,'%s, ',ty{i});
  else
    if(i==1&fig>2) % if sending to data and current fig>2 we redirect to fig1
      fprintf(fh,'<strike>%s</strike>, ',ty{i});
    else
      fprintf(fh,'<a href="%1d_%s_filt%s_weight2_jack%1d%s.html">%s</a>, ',fig,tss{i},filts{i},jk,fds{i},ty{i});
    end
  end
end
fprintf(fh,'------ ');
fprintf(fh,'\n');

% write mean, rmean etc
pl={'mean','rmean','std','rstd','reals','covmat'};
for i=1:6
  if(i==fig)
    fprintf(fh,'%s, ',pl{i});
  else
    if(type==1&i>2)
      fprintf(fh,'<strike>%s</strike>, ',pl{i});
    else
      fprintf(fh,'<a href="%1d_%s_filt%s_weight2_jack%1d%s.html">%s</a>, ',i,ts,filts{type},jk,fd,pl{i});
    end
  end
end
fprintf(fh,'\n');

% include the figure
fprintf(fh,'<p><img src="%1d_%s.gif">\n',fig,s);

fclose(fh);

return
