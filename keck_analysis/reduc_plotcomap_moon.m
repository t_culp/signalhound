function reduc_plotcomap_moon(mapfile,ukpervolt)
% reduc_plotcomap_moon(mapfile,ukpervolt)
%
% Plot the coadded moon maps to check reduc_makecomap works
%
% e.g. reduc_plotcomap_moon('maps/moon050816_filtn_weight0_jack0')
%      reduc_plotcomap_moon('maps/cena060607_filtm_weight2_jack0')

load(mapfile)
if ~exist('map','var') && exist('ac','var')
    map=make_map(ac,m,coaddopt);
end

if(~exist('ukpervolt','var'))
  ukpervolt=[500e3,450e3]/2;
end
ukpervolt=ukpervolt/1e3;
% cal the maps
map=cal_coadd_maps(map,ukpervolt);

figure(1); clf
setwinsize(gcf,1000,550);

plot_iqu(m,map(1),0,{'100GHz T','100GHz Q','100GHz U'});
plot_iqu(m,map(2),3,{'150GHz T','150GHz Q','150GHz U'});

figure(2); clf
%setwinsize(gcf,300,300);
setwinsize(gcf,800,330);
subplot(1,2,1); plot_ip(m,map(1)); title('100GHz');
subplot(1,2,2); plot_ip(m,map(2)); title('150GHz');

return

function plot_ip(m,map)
%m.y_tic=m.y_tic-mean(m.y_tic);
%m.x_tic=m.x_tic+0.05;
%map.T=map.T(3:23,2:22);
%map.Q=map.Q(3:23,2:22);
%map.U=map.U(3:23,2:22);
%m.x_tic=m.x_tic(2:22); m.y_tic=m.y_tic(3:23);
imagesc(m.x_tic,m.y_tic,map.T);
axis xy; set(gca,'XDir','reverse'); set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
[t,r]=qu2linpol(map.Q,map.U);
%t=t+pi/2; % to make B field direction
[u,v]=pol2cart(t+pi/2,r); % pi/2 because t measured +y to -x
[x,y]=meshgrid(m.x_tic,m.y_tic);
hold on;
%i=r>1e-3; x=x(i); y=y(i); u=u(i); v=v(i);
quiver(x,y,+u,+v,0.35,'r.');
quiver(x,y,-u,-v,0.35,'r.');
hold off;
%zoom(2)
%title('QUaD moon map at 150GHz')
n=5;
r(:,1:n)=0; r(:,end-n:end)=0; r(1:n,:)=0; r(end-n:end,:)=0; 
[mv,ind]=maxmd(r); 
xlabel(sprintf('max P =%.1f mK, T at that loc = %.1f mK (P/T = %.1f percent)',...
    mv,map.T(ind(1),ind(2)),100*mv/map.T(ind(1),ind(2))));
colorbar
hold on
plot(m.x_tic(ind(2)),m.y_tic(ind(1)),'go')
hold off
return

function plot_iqu(m,map,n,tit)
subplot(2,3,1+n)
imagesc(m.x_tic,m.y_tic,map.T);
axis xy; set(gca,'XDir','reverse'); set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%caxis([-40,40])
colorbar
title(tit{1});
[t,r]=qu2linpol(map.Q,map.U);
x=m.x_tic; y=m.y_tic;
[u,v]=pol2cart(t+pi/2,r); % pi/2 because t measured +y to -x
hold on;
%quiver(x,y,+u,+v,0.35,'r.');
%quiver(x,y,-u,-v,0.35,'r.');
hold off;

c=max(abs([map.Q(:);map.U(:)]));

subplot(2,3,2+n)
imagesc(m.x_tic,m.y_tic,map.Q);
axis xy; set(gca,'XDir','reverse'); set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
caxis([-c,c]); colorbar
title(tit{2})
subplot(2,3,3+n)
imagesc(m.x_tic,m.y_tic,map.U);
axis xy; set(gca,'XDir','reverse'); set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
caxis([-c,c]); colorbar
title(tit{3})

%map.P=pyth(map.Q,map.U);
%subplot(2,4,4+n)
%imagesc(m.x_tic,m.y_tic,map.P./map.T);
%axis xy; set(gca,'XDir','reverse'); set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
%caxis([-c,c]); colorbar

return
