function paper_plots_b2_matrix(pl, dopr)
% paper_plots_b2_matrix14(pl)
%
% Make plots for B2 2015 matrix paper
%
% This file should know how to make all of the plots and quoted numbers
% for the upcoming paper allowing us to:
% 1) maintain a common "look and feel"
% 2) anyone can tweak any plot rather then having to refer to its
% "keeper"
%
% Some notes:
%
% - It's probably going to be necessary to always run this script
% interactively as batch running seems to produce mysteriously
% different plots.
% eps->pdf is by far the best for a paper being infinitely rescalable
% - if it really won't work then pdflatex also eats png - jpg is
% *crap*
global dopres;
if exist('dopr','var') & dopr
  set(0,'defaultlinelinewidth',1.0);
  set(0,'DefaultAxesFontSize',14);
%    set(0,'defaulttextfontsize',14);
  dopres = 1;
else
  dopres = 0;
end

%make sure we have the directory where all figures are written
if ~exist('~/matrix_paper', 'dir')
  system_safe('mkdir ~/matrix_paper')
end  

if ~exist('~/matrix_paper/figs', 'dir')
  system_safe('mkdir ~/matrix_paper/figs')
end  

%--------------
%swtiches below for all the different figures

% make suppresion factor plot
if(any(pl==1))
  plot_supfac;
end

% make eb leakage with noise plot
if(any(pl==2))
  plot_eb_leakage_debiased;
end

% make eb leakage with out noise plot
if(any(pl==3))
  plot_eb_leakage;
end

% make bb purified plot
if(any(pl==4))
plot_bb_ambloss()
end

% make covariance map plots
if(any(pl==5))
plot_ct()
end

%make comparison of constrained
if(any(pl==6))
compare_constrained()
end

%make comparison of reob vs standard
if(any(pl==7))
reob_apscomp()
end

%plot map of row of observing matrix
if(any(pl==8))
reobserving_psf()
end

%plot filter leakage maps
if(any(pl==9))
filter_leakage()
end

%plot cleaned maps
if(any(pl==10))
proj_maps_6panel()
end

%plot eigenmode maps
if(any(pl==11))
plot_eigenmodes()
end

%plot difference of matrix and standard pipeline
if (any(pl==12))
 reob_mapcomp()
end

%plot individual pieces of the observation matrix
if (any(pl==13))
 plot_reob()
end

%plot eigenvalues
if (any(pl==14))
 plot_eigenvals()
end

%plot direct bpwf
if (any(pl==15))
 plot_directbpwf()
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_directbpwf()

load('/n/bicepfs3/bicep2/pipeline/aps/0706/xxxx_allcmb_filtp3_weight3_gs_dp1100_jack0_matrix_directbpwf.mat')


set(0,'defaultAxesFontSize',14);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',14);

 
 %setup y axis  
 for i=1:4;arr(i)=1d-9*100^i;fields{i}=num2str(arr(i), '%g');end
   
clf 
setwinsize(gcf, 1400,800)
subplot(3,2,1)
semilogy(bpwf.l, bpwf.Cs_l(:,2:17,1), 'linewidth', 2)
ylim([1d-9,1d-1])
grid on
ylabel('\it{l(l+1)C^{TT}_l/2\pi} [\muK^2]');
title('TT \rightarrow TT')
set(gca, 'YTick', arr, 'YTickLabel', fields);
subplot(3,2,2)
semilogy(bpwf.l, bpwf.Cs_l(:,2:17,2), 'linewidth', 2)
ylim([1d-9,1d-1])
grid on
ylabel('\it{l(l+1)C^{TE}_l/2\pi} [\muK^2]');
title('TE \rightarrow TE')
set(gca, 'YTick', arr, 'YTickLabel', fields);
subplot(3,2,3)
semilogy(bpwf.l, bpwf.Cs_l(:,2:17,3), 'linewidth', 2)
ylim([1d-9,1d-1])
grid on
ylabel('\it{l(l+1)C^{EE}_l/2\pi} [\muK^2]');
title('EE  \rightarrow  EE')
set(gca, 'YTick', arr, 'YTickLabel', fields);
subplot(3,2,4)
semilogy(bpwf.l, bpwf.Cs_l(:,2:17,4), 'linewidth', 2)
ylim([1d-9,1d-1])
grid on
ylabel('\it{l(l+1)C^{BB}_l/2\pi} [\muK^2]');
title('BB  \rightarrow  BB')
set(gca, 'YTick', arr, 'YTickLabel', fields);
subplot(3,2,5)
semilogy(bpwf.l, bpwf.Cs_l(:,2:17,5), 'linewidth', 2)
ylim([1d-9,1d-1])
grid on
ylabel('\it{l(l+1)C^{BB}_l/2\pi} [\muK^2]');
title('EE  \rightarrow  BB')
xlabel('Multipole, \it{l}');
set(gca, 'YTick', arr, 'YTickLabel', fields);
subplot(3,2,6)
semilogy(bpwf.l, bpwf.Cs_l(:,2:17,6), 'linewidth', 2)
ylim([1d-9,1d-1])
grid on
ylabel('\it{l(l+1)C^{EE}_l/2\pi} [\muK^2]');
title('BB  \rightarrow  EE')
xlabel('Multipole, \it{l}');
set(gca, 'YTick', arr, 'YTickLabel', fields);

 set(findall(gcf,'type','text'),'FontSize',14,'fontWeight','bold', 'Fontname', 'new century schoolbook')
 
 print_fig('~/matrix_paper/figs/directbpwf_log')

 
 
 return



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_eigenvals()

 load('/n/bicepfs3/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_eigen.mat', 'l_b', 'obs_pixels', 'm') 
  nmodes=floor(length(obs_pixels)/4);
  
clf
set(0,'defaultAxesFontSize',14);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',14);
setwinsize(gcf,800,600)


semilogy(l_b, 'k','linewidth', 2)
hold on
semilogy([nmodes,nmodes], [1d-10, 1d10], '--r','linewidth', 2) 
semilogy([length(l_b)-nmodes,length(l_b)-nmodes], [1d-10, 1d10], '--r','linewidth', 2) 
hold off
for i=1:10;arr(i)=1d-5*10^i;fields{i}=num2str(arr(i));end
set(gca, 'YTick', arr, 'YTickLabel', fields);

arr=[0:5000:length(obs_pixels)];
for i=1:length(arr);fields{i}=num2str(arr(i));end
set(gca, 'XTick', arr, 'XTickLabel', fields);

xlabel('Eigenmode sorted by eigenvalue')
ylabel('Eigenvalue')
ylim([min(l_b), max(l_b)])
xlim([0, length(l_b)])

text(.2d4, 1.4d2, 'Emodes')
text(1.3d4, 1.4d2, 'Ambiguous')
text(2.6d4, 1.4d2, 'Bmodes')

set(findall(gcf,'type','text'),'FontSize',18,'fontWeight','bold', 'Fontname', 'new century schoolbook')
grid on

print_fig('~/matrix_paper/figs/plot_eigenvals')

keyboard;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_reob()

load('/n/bicepfs3/bicep2/pipeline/matrixdata/pairmatrices/0704/real/20110803H09_dk113_filtp3_weight3_gs_dp_healmap_healpixnearest.mat')
clf
ss=size(matrix.filtm);
setwinsize(gcf, ss(1)./100,ss(2)./100)
ha = tight_subplot(1, 1, [0,0], [0,0], [0,0])
axes(ha(1))
set(0,'defaultAxesFontSize',20);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',20);
spy(matrix.filtm, 'k')
ylabel('n_t');xlabel('n_t')
set(gca,'Ytick', [], 'YTickLabel',[])
set(gca,'Xtick', [], 'XTickLabel',[])
set(findall(gcf,'type','text'),'FontSize',20,'fontWeight','bold') 
xlabel([]);ylabel([]);
print('-dpng', '~/matrix_paper/figs/poly_filter')
clf
setwinsize(gcf, 400,400)
ha = tight_subplot(1, 1, [0,0], [0,0], [0,0])
spy(matrix.filtm, 'k.')
xlim([1.825d4,2.145d4])
ylim([1.825d4,2.145d4])
set(gca,'Ytick', [], 'YTickLabel',[])
set(gca,'Xtick', [], 'XTickLabel',[])
set(gca,'PlotBoxAspectRatio',[1,1,1])
print('-dpng', '~/matrix_paper/figs/poly_filter_zoom')


clf
ss=size(matrix.pair(1).awa);
setwinsize(gcf, ss(1)./100,ss(1)./100)
ha = tight_subplot(1, 1, [0,0], [0,0], [0,0])
axes(ha(1))
set(0,'defaultAxesFontSize',20);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',20);
w=sparse(1:23600,1:23600, 1:23600);
spy(w, 'k')
ylabel('n_t');xlabel('n_t')
set(gca,'Ytick', [], 'YTickLabel',[])
set(gca,'Xtick', [], 'XTickLabel',[])
set(findall(gcf,'type','text'),'FontSize',20,'fontWeight','bold') 
xlabel([]);ylabel([]);
print('-dpng', '~/matrix_paper/figs/weight_vector_full')


clf
ss=size(matrix.pair(1).pointm.a);
setwinsize(gcf, ss(1)./100,ss(2)./100)
ha = tight_subplot(1, 1, [0,0], [0,0], [0,0])
axes(ha(1))
set(0,'defaultAxesFontSize',20);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',20);
spy(matrix.pair(1).pointm.a', 'k.')
%xlabel('n_t');
%ylabel([char(241) '_p'], 'interpreter', 'tex')
set(gca,'Ytick', [], 'YTickLabel',[])
set(gca,'Xtick', [], 'XTickLabel',[])
xlabel([]);ylabel([]);
%set(findall(gcf,'type','text'),'FontSize',20,'fontWeight','bold') 
print('-dpng', '~/matrix_paper/figs/pointm_matrix')
clf
setwinsize(gcf, 400,400)
ha = tight_subplot(1, 1, [0,0], [0,0], [0,0])
spy(matrix.pair(1).pointm.a', 'k.')
xlim([3.75d4,4.25d4])
ylim([1.45d4,1.6d4])
set(gca,'Ytick', [], 'YTickLabel',[])
set(gca,'Xtick', [], 'XTickLabel',[])
set(gca,'PlotBoxAspectRatio',[1,1,1])
print('-dpng', '~/matrix_paper/figs/pointm_matrix_zoom')



clf
load('/n/home01/jetolan/thesis/ground_subtraction_matrix.mat')
ss=size(matrix_temp.gsm)
setwinsize(gcf, ss(1)./100,ss(2)./100)
ha = tight_subplot(1, 1, [0,0], [0,0], [0,0])
axes(ha(1))
set(0,'defaultAxesFontSize',20);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',20);
spy(matrix_temp.gsm, 'k.')
set(gca,'Ytick', [], 'YTickLabel',[])
set(gca,'Xtick', [], 'XTickLabel',[])
%ylabel('n_t');xlabel('n_t')
xlabel([]);ylabel([]);
%set(findall(gcf,'type','text'),'FontSize',20,'fontWeight','bold') 
print('-dpng', '~/matrix_paper/figs/ground_subtract')
clf
setwinsize(gcf, 400,400)
ha = tight_subplot(1, 1, [0,0], [0,0], [0,0])
spy(matrix_temp.gsm, 'k.')
xlim([1.8d4,2.2d4])
ylim([1.8d4,2.2d4])
set(gca,'Ytick', [], 'YTickLabel',[])
set(gca,'Xtick', [], 'XTickLabel',[])
set(gca,'PlotBoxAspectRatio',[1,1,1])
print('-dpng', '~/matrix_paper/figs/ground_subtract_zoom')


clf
load('/n/home01/jetolan/thesis/pointhm_matrix2.mat')
ss=size(pointhm.a)
setwinsize(gcf, ss(2)./100,ss(1)./100)
ha = tight_subplot(1, 1, [0,0], [0,0], [0,0])
axes(ha(1))
set(0,'defaultAxesFontSize',20);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',20);
spy(pointhm.a, 'k.')
set(gca,'Ytick', [], 'YTickLabel',[])
set(gca,'Xtick', [], 'XTickLabel',[])
%xlabel('n_p');ylabel('n_t')
xlabel([]);ylabel([]);
%set(findall(gcf,'type','text'),'FontSize',20,'fontWeight','bold') 
print('-dpng', '~/matrix_paper/figs/pointhm_matrix')
clf
setwinsize(gcf, 400,400)
ha = tight_subplot(1, 1, [0,0], [0,0], [0,0])
spy(pointhm.a, 'k.')
xlim([5.46d4,5.62d4])
ylim([1.2d4,1.5d4])
set(gca,'Ytick', [], 'YTickLabel',[])
set(gca,'Xtick', [], 'XTickLabel',[])
set(gca,'PlotBoxAspectRatio',[1,1,1])
print('-dpng', '~/matrix_paper/figs/pointhm_matrix_zoom')


%%%%%%%%%%%%%%%%%%%%%%%%
%Deprojection

load('/n/home01/jetolan/thesis/deproj_matrix.mat')

%all T,Q,U

clf
ss=size(matrix_temp.deproj)
fill=find(diag(matrix_temp.deproj));
t=sparse(fill(1:end/2), fill(1:end/2), fill(1:end/2), ss(1)/2,ss(2)/2);
empties=sparse(ss(1)/2,ss(2)/2);
matr=[t,empties, empties; empties, matrix_temp.deproj(1:end/2,:);empties, matrix_temp.deproj(1:end/2,:)]; 

ss=size(matr);
setwinsize(gcf, ss(2)./100,ss(1)./100)
ha = tight_subplot(1, 1, [0,0], [0,0], [0,0])
axes(ha(1))
spy(matr, 'k.')
set(gca,'Ytick', [ss(1)/3,ss(1)*2/3], 'YTickLabel',[])
set(gca,'Xtick', [ss(2)/3,ss(2)*2/3], 'XTickLabel',[])
xlabel([]);ylabel([]);
print('-dpng', '~/matrix_paper/figs/deproj_matrix_all')


%just Q,U
clf
matr=matrix_temp.deproj;
 
ss=size(matr);
setwinsize(gcf, ss(2)./100,ss(1)./100)
ha = tight_subplot(1, 1, [0,0], [0,0], [0,0])
axes(ha(1))
spy(matr, 'k.')
set(gca,'Ytick', [ss(1)/2], 'YTickLabel',[])
set(gca,'Xtick', [ss(2)/2], 'XTickLabel',[])
xlabel([]);ylabel([]);
print('-dpng', '~/matrix_paper/figs/deproj_matrix')


return




%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_eb_leakage()


%get suppression factors
%load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat');


%get aps & suppression factors
x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx2_a_filtp3_weight3_gs_dp1100_jack0.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_in=squeeze(aps.Cs_l(:,4,:));
x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx2_a_filtp3_weight3_gs_dp1100_jack0_pureB.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_pureB_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_in_pureB=squeeze(aps.Cs_l(:,4,:));
x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx2_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_b=squeeze(aps.Cs_l(:,4,:));


%colors
c3=[0,0,0];
c2=[.4,.5,.5];
c1=[.4,.8,.2];

r=weighted_ellbins(r,bpwf);

clf
set(0,'defaultAxesFontSize',16);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',16);
setwinsize(gcf,600,350)


%BB
h1=semilogy(r.lc(2:15,4), Cs_l_in(2:15,:), 'color', c2);
hold on
h2=semilogy(r.lc(2:15,4), Cs_l_in_pureB(2:15,:), 'color', c1);
h3=semilogy(r.lc(2:15,4), Cs_l_b(2:15,:), 'color', c3);

%add a nominal BB spec
[r,im,ml]=get_b2xb2;
hinp=semilogy(im.l,im.Cs_l(:,4)*1+ml.Cs_l(:,4),'--r', 'linewidth', 2);
hold off


for i=1:8;arr(i)=1d-7*10^i;fields{i}=num2str(arr(i), '%g');end
set(gca, 'YTick', arr, 'YTickLabel', fields);


xlabel('Multipole ,  \it{l}')
ylabel(['\it{l(l+1)C^{BB}_l / 2\pi} [\muK^2]'])
xlim([30,450])
ylim([4d-7,8d-1])

legend([hinp,h1(1),h2(1),h3(1)],{'lens+r=0.1','normal', 'Smith', 'matrix'}, 'location', 'southeast')

legend boxoff
grid on

%add r limits from 
%http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20140327_ebleakage_rlim2/
bin=3;
 text(r.l(bin)+5, mean(Cs_l_in(bin,4,:))+.07, ['normal: r < 0.17'], 'color', c2)
 text(r.l(bin)+5, mean(Cs_l_in_pureB(bin,4,:))-.005, ['Smith: r < 0.074'], 'color', c1)
 text(r.l(bin)+5, mean(Cs_l_b(bin,4,:))-5d-6, ['matrix: r < 8.3x10^{-5}'], 'color', c3)

  set(findall(gcf,'type','text'),'FontSize',16,'fontWeight','bold', ...
      'Fontname', 'new century schoolbook')
  

print_fig('~/matrix_paper/figs/plot_eb_leakage')



keyboard
return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%

function plot_bb_ambloss()

set(0,'defaultlinelinewidth',1.0);
set(0,'DefaultAxesFontSize',16);
set(0,'defaulttextfontsize',16);


%get suppression factor
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat');


%get aps
x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx4_a_filtp3_weight3_gs_dp1100_jack0.mat')
%aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
aps.Cs_l=x.aps.Cs_l(:,1:6,:);
Cs_l_in=squeeze(aps.Cs_l(:,4,:));
x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx4_a_filtp3_weight3_gs_dp1100_jack0_pureB.mat')
%aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
aps.Cs_l=x.aps.Cs_l(:,1:6,:);
Cs_l_in_pureB=squeeze(aps.Cs_l(:,4,:));
x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx4_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat')
%aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
aps.Cs_l=x.aps.Cs_l(:,1:6,:);
Cs_l_b=squeeze(aps.Cs_l(:,4,:));

%colors
c3=[0,0,0];
c2=[.4,.5,.5];
c1=[.4,.8,.2];

clf
set(0,'defaultAxesFontSize',16);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',16);
setwinsize(gcf,600,350)

r=weighted_ellbins(r,bpwf);

%BB
h1=semilogy(r.lc(2:15,4), Cs_l_in(2:15,:), 'color', c2);
hold on
h2=semilogy(r.lc(2:15,4), Cs_l_in_pureB(2:15,:), 'color', c1);
h3=semilogy(r.lc(2:15,4), Cs_l_b(2:15,:), 'color', c3);

%add a nominal BB spec
[r,im,ml]=get_b2xb2;
hinp=semilogy(im.l,im.Cs_l(:,4),'--r', 'linewidth', 2);
hold off


for i=1:8;arr(i)=1d-7*10^i;fields{i}=num2str(arr(i), '%g');end
set(gca, 'YTick', arr, 'YTickLabel', fields);

xlabel('Multipole ,  \it{l}')
ylabel(['\it{l(l+1)C^{BB}_l / 2\pi} [\muK^2]'])
xlim([30,450])
ylim([2d-7,2d-2])

legend([hinp,h1(1),h2(1),h3(1)],{'Input: r=0.1','normal', 'Smith', 'matrix'}, 'location', 'southeast')

legend boxoff
grid on

  set(findall(gcf,'type','text'),'FontSize',16,'fontWeight','bold', ...
      'Fontname', 'new century schoolbook')

print_fig('~/matrix_paper/figs/plot_bb_ambloss')



%now do ndof..according to formula from reduc_final_bb_pte

normal=2*mean(Cs_l_in,2).^2./var(Cs_l_in,[],2);
pureb=2*mean(Cs_l_in_pureB,2).^2./var(Cs_l_in_pureB,[],2);
matrix=2*mean(Cs_l_b,2).^2./var(Cs_l_b,[],2);

ndof=[r.l(2:12), normal(2:12), pureb(2:12), matrix(2:12)];

columnLabels = {'Bin Center, \textit{l}', 'Normal', 'Smith', 'Matrix'};

matrix2latex(ndof,'~/matrix_paper/figs/bb_ambloss_ndof.tex', 'columnLabels', columnLabels, 'format', '%.1f')


figure
setwinsize(gcf, 600,350)
plot(r.l, pureb./normal,'color',c1, 'linewidth', 2)
hold on
plot(r.l, matrix./normal,'color',c3, 'linewidth', 2)
plot([0,1000], [1,1], '--k', 'linewidth', 2)
hold off
legend('Smith/normal', 'matrix/normal')
xlabel('Multipole ,  \it{l}')
ylabel(['Degrees of Freedom Ratio'])
  set(findall(gcf,'type','text'),'FontSize',16,'fontWeight','bold', ...
      'Fontname', 'new century schoolbook')
legend boxoff
xlim([0,330])
ylim([.5,1.5])

arr=[.6:.2:1.4]
for i=1:length(arr);fields{i}=num2str(arr(i), '%g');end
set(gca, 'YTick', arr, 'YTickLabel', fields);

print_fig('~/matrix_paper/figs/plot_bb_ambloss_dof')

keyboard
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_supfac()

%get measured bpwf
%load('/n/bicepfs1/bicep2/pipeline/aps/0706/xxxx_allcmb_filtp3_weight3_gs_dp1100_jack0_matrix_directbpwf.mat')

%after normalization
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_directbpwf.mat');

%get supfac and measured bins
 [r,im,ml]=get_b2xb2;

%get the pixel window
a=fitsread('~jetolan/biceppipe/bicep2_pixel_window.fits', 'bintable');
pw.wl.w_avg=a{1};

%get bin edges
 [be,nn]=get_bins('bicep_norm')

 %get beam window function
bf= fitsread('aux_data/beams/beamfile_20130222_sum.fits', 'bintable');
beamf=bf{1}.^2; 

if(1)%can check against this
   beamsize=31.22/60;
   fwhm=beamsize*pi/180;
   sig=fwhm/sqrt(8*log(2));
   sigl=1./sig; 
   beamcor=exp(-.5*(r(1).l.*(r(1).l+1))/(sigl.^2)).^2;
end
  

beamvals=interp1(1:length(beamf),beamf, r.lc(2:end,4));
pwvals=interp1(1:length(pw.wl.w_avg),pw.wl.w_avg.^2, r.lc(2:end,4));
filt=1./(beamvals.*r.rwf(2:end,4).*pwvals);

if(1)
  colors={'r', 'g', 'm',[.6,.4,.6], [0,.3,0],[.5,0,.6],[.9,.3,0],[0,.4,.6],'c', 'k',[.8 .8 .8],[.3,.3,.9],[.8 .2 .2],[.2,.3,.3]};
  
%make plot
fs=18
set(0,'defaultAxesFontSize',fs);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',fs);
clf 
setwinsize(gcf, 600,300)
hold on
plot(pw.wl.w_avg.^2, '--k','linewidth', 2)
plot(beamf, ':k', 'linewidth', 2)
plot(r.lc(2:end,4), filt, '-.k')
plot(r.lc(2:end,4), 1./r.rwf(2:end,4), 'k','linewidth', 2)
%plot(r.lc(2:end,4), 1./r.rwf(2:end,4), 'ok')


%plot(r.l,beamcor)
for i=2:10
plot(r.lc(i,4), 1./r.rwf(i,4), 'o', 'MarkerEdgeColor', colors{i}, ...
    'MarkerFaceColor', colors{i}) 
plot(bpwf.l, bpwf.Cs_l(:,i,4)*20./r.rwf(i,4), 'color', colors{i},'linewidth', 2)
end
hold off
ylim([0,1]);xlim([0,350])
ylabel('Suppression Factor');
xlabel('Multipole, \it{l}')
end
set(findall(gcf,'type','text'),'FontSize',fs,'fontWeight','bold', ...
      'Fontname', 'new century schoolbook')
box on
legend('map', 'beam', 'filter', 'total')

haxes1 = get(gca,'Position');
haxes2 = axes('Position',haxes1,...
              'XAxisLocation','top',...
              'YAxisLocation','right',...
              'Color','none');
set(haxes2, 'xtick', [], 'ytick', [.2,.4,.6, .8], 'yticklabel',[.2,.4,.6, .8]/20)

ylabel('BPWF')
	

print_fig('~/matrix_paper/figs/plot_supfac')


columnLabels = {'bin number','nominal L binedge', 'nominal bincenter', 'nominal U binedge','actual L binedge', 'actual bincenter', 'actual U binedge',};

for i=1:10;
  n.ll(i)=be(i);
  n.lh(i)=be(i+1);
  n.lc(i)=r.l(i);
end
cols=[[2:10]', n.ll(2:10)',n.lc(2:10)',n.lh(2:10)',r.ll(2:10,4),r.lc(2:10,4), r.lh(2:10,4)];
matrix2latex(cols,'~/matrix_paper/figs/plot_supfac.tex' , 'columnLabels', columnLabels, 'format', '%.1f')

keyboard
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function plot_eb_leakage_debiased()

%colors
c3=[0,0,0];
c2=[.4,.5,.5];
c1=[.4,.8,.2];

for nonoise=[0,1]

%get aps & suppression factors
if(nonoise)
x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx2_a_filtp3_weight3_gs_dp1100_jack0.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_in=squeeze(aps.Cs_l(:,4,:));
x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx2_a_filtp3_weight3_gs_dp1100_jack0_pureB.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_pureB_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_in_pureB=squeeze(aps.Cs_l(:,4,:));
x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx2_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_b=squeeze(aps.Cs_l(:,4,:));
else
  x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx7_a_filtp3_weight3_gs_dp1100_jack0.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_in=squeeze(aps.Cs_l(:,4,:));
x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx7_a_filtp3_weight3_gs_dp1100_jack0_pureB.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_pureB_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_in_pureB=squeeze(aps.Cs_l(:,4,:));
x=load('/n/bicepfs1/bicep2/pipeline/aps/0751/xxx7_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat')
load('/n/bicepfs1/bicep2/pipeline/final/0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf.mat');
aps.Cs_l=bsxfun(@times, r(1).rwf, x.aps.Cs_l(:,1:6,:));
Cs_l_b=squeeze(aps.Cs_l(:,4,:));
end

[r,im,ml]=get_b2xb2;

%subtract mean
matrixb=bsxfun(@minus, Cs_l_b, mean(Cs_l_b,2));
smithb=bsxfun(@minus, Cs_l_in_pureB, mean(Cs_l_in_pureB,2));
normalb=bsxfun(@minus, Cs_l_in, mean(Cs_l_in,2));

%find 95% highest


clf
setwinsize(gcf,600,350)

r=weighted_ellbins(r,bpwf);

%BB
h1=plot(r.lc(2:end,4), normalb(2:end,1:200), 'color',c2, 'linewidth', 1);
hold on
h2=plot(r.lc(2:end,4), smithb(2:end, 1:200), 'color', c1, 'linewidth', 1);
h3=plot(r.lc(2:end,4), matrixb(2:end, 1:200), 'color', c3, 'linewidth', 1);

%add a nominal BB spec
hinp=plot(im.l,im.Cs_l(:,4)*1+ml.Cs_l(:,4),'--r', 'linewidth', 2);
hold off


for i=1:30;arr(i)=-1d-1+2d-2*i;fields{i}=num2str(arr(i), '%g');end
set(gca, 'YTick', arr, 'YTickLabel', fields);


xlabel('Multipole ,  \it{l}')
ylabel(['\it{l(l+1)C^{BB}_l / 2\pi} [\muK^2]'])
xlim([30,330])
ylim([-4d-2,8d-2])
if nonoise
legend([hinp,h1(1),h2(1),h3(1)],{'lens+r=0.1','normal', ...
    'Smith', 'matrix'}, 'location', 'northwest')
else
legend([hinp,h1(1),h2(1),h3(1)],{'lens+r=0.1','normal: r<0.20', ...
    'Smith: r<0.11', 'matrix: r<0.06'}, 'location', 'northwest')
end

legend boxoff

 set(findall(gcf,'type','text'),'FontSize',16,'fontWeight','bold', ...
      'Fontname', 'new century schoolbook')

if(nonoise)
print_fig('~/matrix_paper/figs/plot_eb_leakage_debiased_nonoise')
else
print_fig('~/matrix_paper/figs/plot_eb_leakage_debiased')
end
 
  end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_ct()

%load the files saved in first part of ~jetolan/thesis/plot_ct.m
names={'EnoB', 'BnoE','r0', 'r0p1_noE'}
tnames={ '1/\it{l}^2 E','1/\it{l}^2 B','\it{\LambdaCDM}', '\it{r = 0.2}'}

%grab Qvar and Uvar
proj_file='matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rxa_proj.mat';
load(proj_file, 'reob')

ellrng=[0,800];

for i=1:4
load(['~jetolan/biceppipe/ct_plot/map2_' names{i}])
map{i}.Qvar=reob.Qvar; map{i}.Uvar=reob.Uvar;
map{i}.Q=Q; map{i}.U=U;
mapEB(i)=make_ebmap(m,map{i},[],ellrng);
end


%plot maps
lims{1}.c=[-2d20,2d20];lims{1}.v=[1d-20];
lims{2}.c=[-2d20,2d20];lims{2}.v=[1d-20];
lims{3}.c=[-1d9,1d9];lims{3}.v=[3d-9];
lims{4}.c=[-8d6,8d6];lims{4}.v=[2d-7];
plot_fourpanel(mapEB,m, lims, tnames)



print_fig('~/matrix_paper/figs/plot_ct',[],[],1)

keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function compare_constrained()

  if(1)
      %get specs
       load('/n/bicepfs1/bicep2/pipeline/final/1450x1350/real_a_filtp3_weight3_gs_dp1100_jack0_real_a_filtp3_weight3_gs_dp1100_jack01_pureB.mat')
       load('/n/bicepfs1/bicep2/pipeline/aps/0751x1351/xxx2_a_filtp3_weight3_gs_dp1100_jack0_0012_a_filtp3_weight3_gs_dp1100_jack01_pureB_overrx.mat')

      cons.Cs_l=bsxfun(@times,aps(1).Cs_l,r(1).rwf);
      cons.l=aps(1).l;
      
      load('/n/bicepfs1/bicep2/pipeline/aps/1450x1350/xxx2_a_filtp3_weight3_gs_dp1100_jack0_0012_a_filtp3_weight3_gs_dp1100_jack01_pureB_overrx.mat')
      
      
      uncons.Cs_l=bsxfun(@times,aps(1).Cs_l,r(1).rwf);
      uncons.l=aps(1).l;
      
      name='LCDM'
      plot_specs(cons, uncons, name)
    end
    
keyboard    
return      

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function reob_apscomp()


%load two aps corresponding to maps shown
mat=load('/n/bicepfs1/bicep2/pipeline/aps/0750/0062_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat')
nor=load('/n/bicepfs1/users/jetolan/aps/0751/0062_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat')
 
matr=load('/n/bicepfs1/bicep2/pipeline/aps/0750/0064_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat')
norr=load('/n/bicepfs1/users/jetolan/aps/0751/0064_a_filtp3_weight3_gs_dp1100_jack0_matrix.mat')

mat.aps.Cs_l(:,4)=matr.aps.Cs_l(:,4);
nor.aps.Cs_l(:,4)=norr.aps.Cs_l(:,4);

%get suppression factor
filename = '0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf_rbc.mat';
load(['final/',filename])
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN;

%apply the suppresion factor
nor.Cs_l=bsxfun(@times,nor.aps.Cs_l,r(1).rwf);
mat.Cs_l=bsxfun(@times,mat.aps.Cs_l,r(1).rwf);
nor.lc=r.lc;
mat.lc=r.lc;

%plot the aps
plot_specs_apscomp(mat,nor, inpmod)


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function reobserving_psf()


reob.file='/n/bicepfs2/keck/pipeline/matrixdata/matrices/0704/real_cmb201208-201209_rxall_filtp3_weight3_gs_dp1100_jack0_healmap_healpixnearest_final.mat';
load(reob.file, 'matrixc', 'm');

vect=matrixc.QTQQQU(:,159490);
[m,mm]=vect2map(m,vect);
Q=full(mm);
Q(mm==0)=NaN;

vect=matrixc.UTUQUU(:,159490);
[m,mm]=vect2map(m,vect);
U=full(mm);
U(mm==0)=NaN;


clf
colormap gray
setwinsize(gcf, 600,450)
fs=16;
set(0,'defaultAxesFontSize',fs);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',fs);

ha=tight_subplot(2,1,[.1 .03],[.1 .1],[.1 .15])
axes(ha(1))
freezeColors
Q(isnan(Q))=1d5;
imagesc(m.x_tic,m.y_tic, asinh(Q)); 
title('Q', 'fontangle', 'italic')
caxis([-4,4])
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
set(gca, 'xticklabel',[])
set(gca, 'yticklabel',[])
set(gca,'ydir', 'normal')
set(gca,'XDir','reverse');
ylim([-61 -53]);


axes(ha(2))
freezeColors
U(isnan(U))=1d5;
imagesc(m.x_tic,m.y_tic, asinh(U)); 
caxis([-4,4])
set(gca,'ydir', 'normal')
set(gca,'XDir','reverse');
title('U', 'fontangle', 'italic')
%set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
ylabel('Declination [deg]')
xlabel('Right ascension [deg]')
ylim([-61 -53]);

% Create colorbar
c=colorbar('Location', 'East');
set(gca, 'CLim', [-4,4])
y=get(c, 'Position');
y(4)=y(4).*2.45
y(2)=y(2).*1;
y(1)=y(1).*1.08
set(c, 'Position',y, 'YAxisLocation', 'right') 
ylabel(c, '\bf{asinh(\muK)}', 'FontWeight', 'bold')
set(findall(gcf,'type','text'),'FontSize',fs,'fontWeight','bold', 'Fontname', 'new century schoolbook') 
print_fig('~/matrix_paper/figs/reobserving_psf')

keyboard


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function filter_leakage()
%leakage maps
  
  opt{1}.gs='';opt{1}.filt='n';opt{1}.dp='0000';
  opt{2}.gs='';opt{2}.filt='p3';opt{2}.dp='0000';
  opt{3}.gs='_gs';opt{3}.filt='n';opt{3}.dp='0000';
  opt{4}.gs='';opt{4}.filt='n';opt{4}.dp='1100';

  
  lims{1}.c=[-1,1];lims{1}.v=[2d0];
  lims{2}.c=[-4d-1,4d-1];lims{2}.v=[3d0];
  lims{3}.c=[-4d-1,4d-1];lims{3}.v=[3d0];
  lims{4}.c=[-8d-1,8d-1];lims{4}.v=[2.5d0];
  
  set(0, 'DefaultFigureVisible', 'off')
  setwinsize(gcf, 1025,500)
  clf
  fs=14;
  nancolor=[.94,.94,.94]
  set(0,'defaultAxesFontSize',fs);
  set(0,'defaultAxesFontWeight','b');
  set(0,'defaulttextfontsize',12);
  tnames={'Projection + Apodization', 'Third order polynomial filter', 'Scan-synchronous subtraction', ...
      'Deprojection'}
  m=get_map_defn('bicep')
  dim=get_fourpaneldims(m);
  set(gcf, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');
  
  for i=1:4
     
    switch i
      case 1
	ax=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);	
      case 2
	ax=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
      case 3
	ax=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
      case 4
	ax=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
    end
    
	mapname=['/n/bicepfs1/bicep2/pipeline/maps/0780/0012_a_filt' opt{i}.filt '_weight3' opt{i}.gs '_dp' opt{i}.dp  '_jack0.mat'];
	load([ mapname ])
	map=make_map(ac, m, coaddopt);
	map=cal_coadd_maps(map, get_ukpervolt);
        
	ellrng=[20,150];
	mapEB(i)=make_ebmap(m,map,[],ellrng);
	
	if i>1
	  mapEB(i).B=mapEB(i).B-mapEB(1).B;
	  mapEB(i).BU=mapEB(i).BU-mapEB(1).BU;
	  mapEB(i).BQ=mapEB(i).BQ-mapEB(1).BQ;
	end
	
	colormap(colormap_lint)
	plot_map(m,mapEB(i).B,'iau',[], nancolor);
	set(ax,'FontSize',fs,'FontWeight','bold');
	
	set(ax,'XTick',[-50 0 50]);
	set(ax,'YTick',[-65 -60 -55 -50]);
	if i==3
	  set(ax,'XTickLabel',[-50 0 50]);
	  set(ax,'YTickLabel',[-65 -60 -55 -50]);
	  xlabel('Right Ascension [deg]','Fontname', 'new century schoolbook')
	  ylabel('Declination [deg]','Fontname', 'new century schoolbook')	  
	elseif i==4
	 set(ax,'Yticklabel',[]);
	  set(ax,'Xticklabel',[-50 0 50]);
	else
	  set(ax,'Yticklabel',[]);
	  set(ax,'Xticklabel',[]);
	end
	caxis(lims{i}.c)
	hold on
	plot_pol(m,mapEB(i).BQ,mapEB(i).BU,map(1).Qvar,map(1).Uvar,lims{i}.v,[],3,1)
	hold off
	title(tnames{i},'FontSize',fs, 'fontWeight','bold', 'Fontname', 'new century schoolbook');
	freezeColors
      end

      %set(findall(gcf,'type','text'),'FontSize',fs, 'fontWeight','bold', 'Fontname', 'new century schoolbook')

      print_fig('~/matrix_paper/figs/filter_leakage_maps', [], [],1)

set(0, 'DefaultFigureVisible', 'on')
keyboard
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function proj_maps_6panel()

%load and plot maps created in ~jetolan/thesis/proj_maps_6panel.m
set(0, 'DefaultFigureVisible', 'off')
map_raw='/n/bicepfs1/bicep2/pipeline/maps/0751/0012_a_filtp3_weight3_gs_dp1100_jack0.mat'
mapE='/n/bicepfs1/bicep2/pipeline/maps/0751/0012_a_filtp3_weight3_gs_dp1100_jack0_projE.mat'
mapB='/n/bicepfs1/bicep2/pipeline/maps/0751/0012_a_filtp3_weight3_gs_dp1100_jack0_projB.mat'
lims{1}.c=[-6d0,6d0];lims{1}.v=[2d-1];
lims{2}.c=[-6d0,6d0];lims{2}.v=[2d-1];
lims{3}.c=[-4d-1,4d-1];lims{3}.v=[3.5d0];
lims{1}.ct=[-5d0,0,5d0];lims{2}.ct=[-5d0,0,5d0];lims{3}.ct=[-.3,0,.3];
lims{1}.ellrng=[20,300];
[m,map_raw, mapE, mapB]=setup_maps(map_raw, mapE, mapB,lims)




map_raw2='/n/bicepfs1/bicep2/pipeline/maps/0751/0013_a_filtp3_weight3_gs_dp1100_jack0.mat'
mapE2='/n/bicepfs1/bicep2/pipeline/maps/0751/0013_a_filtp3_weight3_gs_dp1100_jack0_projE.mat'
mapB2='/n/bicepfs1/bicep2/pipeline/maps/0751/0013_a_filtp3_weight3_gs_dp1100_jack0_projB.mat'
lims{1}.c=[-6d0,6d0];lims{1}.v=[2d-1];
lims{2}.c=[-6d0,6d0];lims{2}.v=[2d-1];
lims{3}.c=[-4d-1,4d-1];lims{3}.v=[3.5d0];
lims{1}.ct=[-5d0,0,5d0];lims{2}.ct=[-5d0,0,5d0];lims{3}.ct=[-.3,0,.3];
lims{1}.ellrng=[20,300];
[m,map_raw2, mapE2, mapB2]=setup_maps(map_raw2, mapE2, mapB2,lims)


sixpaneleb(m, map_raw, mapE, mapB,map_raw2, mapE2, mapB2, lims)
set(0, 'DefaultFigureVisible', 'on')
keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_eigenmodes()

if(0)
   load('matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_eigen.mat', 'b', 'obs_pixels', 'm')
  bb(:,1)=b(:,1);
  bb(:,2)=b(:,end);
  bb(:,3)=b(:,50);
  bb(:,4)=b(:,end-50);

 save('matrixdata/c_t/afew_eigenmodes.mat', 'bb')
 return
end


 load('/n/bicepfs3/bicep2/pipeline/matrixdata/c_t/afew_eigenmodes.mat') 
 load('/n/bicepfs3/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat', 'obs_pixels', 'm','reob') 

 if ~exist('ellrng','var') || isempty(ellrng)
   ellrng=[0,800];
 end


for i=1:4
map{i}=expand_vect(m,bb(:,i),obs_pixels);
map{i}.Qvar=reob.Qvar; map{i}.Uvar=reob.Uvar;
mapEB(i)=make_ebmap(m,map{i},[],ellrng);
end


%plot maps
lims{1}.c=[-3d-13,3d-13];lims{1}.v=[6d12];
lims{2}.c=[-6d-11,6d-11];lims{2}.v=[2d10];
lims{3}.c=[-1d-12,1d-12];lims{3}.v=[2d12];
lims{4}.c=[-1d-10,1d-10];lims{4}.v=[2d10];
plot_fourpanel_vectors(mapEB,m, lims)




print_fig('~/matrix_paper/figs/plot_eigenmodes',[],[],1)

keyboard



return

%%%%%%%%%%%%%

function reob_mapcomp()
set(0, 'DefaultFigureVisible', 'off')
load('/n/bicepfs1/bicep2/pipeline/maps/0751/0062_a_filtp3_weight3_gs_dp1100_jack0.mat')
map=make_map(ac, m, coaddopt);
map=cal_coadd_maps(map, get_ukpervolt);
mapn=map;


load('/n/bicepfs1/bicep2/pipeline/maps/0750/0062_a_filtp3_weight3_gs_dp1100_jack0.mat')
map=make_map(ac, m, coaddopt);
map=cal_coadd_maps(map, get_ukpervolt);

mapm=map;

w=mapn.Qvar./min(mapn.Qvar(:));
ww=mapm.Qvar./min(mapm.Qvar(:));


%plot colored background

clf
setwinsize(gcf,600,700)
nancolor=[.94 .94 .94];
fs=14
cas=[-4,4];
colormap gray
set(0,'defaultAxesFontSize',fs);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',fs);
set(0,'defaultlinelinewidth',1.0);
ha=tight_subplot(3,1,[0.05 .02],[.05 .05],[.1 .1]);

% Standard map

  axes(ha(1));
    plot_map(m,mapn.Q./w,'IAU',[], nancolor);
    set(gca,'FontSize',fs,'FontWeight','bold');
    set(gca,'Xticklabel',[]);
    set(gca,'XTick',[-50 0 50]);
    set(gca,'YTick',[-65 -60 -55 -50]);
    set(gca,'Yticklabel',[]);
    caxis([-3,3]);
    c=colorbar;
%    set(c, 'Ytick', lims{1}.ct)
    ylabel(c, '\bf{\muK}', 'FontWeight', 'bold','Fontname', 'new century schoolbook')
    title('Standard Pipeline','Fontname', 'new century schoolbook')

 % Matrix map
  axes(ha(2));
  plot_map(m,mapm.Q./ww,'IAU',[],nancolor)
    set(gca,'FontSize',fs,'FontWeight','bold');
    set(gca,'Xticklabel',[]);
    set(gca,'XTick',[-50 0 50]);
    set(gca,'YTick',[-65 -60 -55 -50]);
    set(gca,'Yticklabel',[]);
    caxis([-3,3]);
    c=colorbar;
    title('Observation Matrix','Fontname', 'new century schoolbook')
     ylabel(c, '\bf{\muK}', 'FontWeight', 'bold','Fontname', 'new century schoolbook') 
    
  % Diff map
  axes(ha(3));
  plot_map(m,(mapm.Q-mapn.Q)./w,'IAU',[],nancolor)
  set(gca,'FontSize',fs,'FontWeight','bold');
    set(gca,'Xticklabel',[-50 0 50]);
    set(gca,'XTick',[-50 0 50]);
    set(gca,'YTick',[-65 -60 -55 -50]);
    set(gca,'Yticklabel',[-65 -60 -55 -50]);
    caxis([-.5,.5])
    c=colorbar;
    title('Difference','Fontname', 'new century schoolbook')
    
 
%   set(c, 'Ytick', lims{3}.ct)
   ylabel(c, '\bf{\muK}', 'FontWeight', 'bold','Fontname', 'new century schoolbook')
   
   
  set(findall(gcf,'type','text'),'FontSize',fs,'fontWeight','bold') 
  
print_fig('~/matrix_paper/figs/reob_mapcomp_tall')
keyboard
set(0, 'DefaultFigureVisible', 'on')

keyboard
return



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%below are subfunctions for plotting routines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [m,map_raw, mapE, mapB]=setup_maps(map_raw, mapE, mapB,lims)

load(map_raw)
map=make_map(ac, m, coaddopt);
clear coaddopt;
map_raw=cal_coadd_maps(map, get_ukpervolt);
load(mapE, 'map')
mapE=cal_coadd_maps(map, get_ukpervolt);
load(mapB, 'map')
mapB=cal_coadd_maps(map, get_ukpervolt);

map_raw=make_ebmap(m,map_raw,[],lims{1}.ellrng);
mapE=make_ebmap(m,mapE,[],lims{1}.ellrng);
mapB=make_ebmap(m,mapB,[],lims{1}.ellrng);


return




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function sixpaneleb(m, map_raw, mapE, mapB, map_raw2, mapE2, mapB2, lims)

nancolor=[.94,.94,.94];
fs=14;

fig=figure;
clf;
 setwinsize(gcf, 1000,650)
dim=get_sixpaneldims(m);
set(fig, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');
colormap(colormap_lint)
set(0,'defaulttextfontsize',12);
% Total

ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,map_raw.E+map_raw.B,'iau',[], nancolor);
set(gca,'FontSize',fs,'FontWeight','bold');
set(gca,'Xticklabel',[]);
set(gca,'XTick',[-50 0 50]);
set(gca,'YTick',[-65 -60 -55 -50]);
set(gca,'Yticklabel',[]);
caxis(lims{1}.c)
Pvar=sqrt(map_raw.Qvar.^2+map_raw.Uvar.^2);
Pvar=Pvar./min(Pvar(:));
hold on
plot_pol(m,map_raw.Q./Pvar,map_raw.U./Pvar,map_raw.Qvar,map_raw.Uvar,lims{1}.v,[],3,1)
hold off
title('Scalars only','Fontname', 'new century schoolbook');


ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,map_raw2.E+map_raw2.B,'iau',[], nancolor);
set(gca,'FontSize',fs,'FontWeight','bold');
set(gca,'Xticklabel',[]);
set(gca,'XTick',[-50 0 50]);
set(gca,'YTick',[-65 -60 -55 -50]);
set(gca,'Yticklabel',[]);
caxis(lims{1}.c)
Pvar=sqrt(map_raw2.Qvar.^2+map_raw2.Uvar.^2);
Pvar=Pvar./min(Pvar(:));
hold on
plot_pol(m,map_raw2.Q./Pvar,map_raw2.U./Pvar,map_raw2.Qvar,map_raw2.Uvar,lims{1}.v,[],3,1)
hold off
title('Scalars + tensors (r=0.1)','Fontname', 'new century schoolbook');

% pure E

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);

plot_map(m,mapE.E,'iau',[], nancolor);
set(gca,'FontSize',fs,'FontWeight','bold');
set(gca,'Xticklabel',[]);
set(gca,'XTick',[-50 0 50]);
set(gca,'YTick',[-65 -60 -55 -50]);
set(gca,'Yticklabel',[]);
caxis(lims{2}.c)
hold on
plot_pol(m,mapE.EQ,mapE.EU,map_raw.Qvar,map_raw.Uvar,lims{2}.v,[],3,1)
hold off

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,mapE2.E,'iau',[], nancolor);
set(gca,'FontSize',fs,'FontWeight','bold');
set(gca,'Xticklabel',[]);
set(gca,'XTick',[-50 0 50]);
set(gca,'YTick',[-65 -60 -55 -50]);
set(gca,'Yticklabel',[]);
caxis(lims{2}.c)
hold on
plot_pol(m,mapE2.EQ,mapE2.EU,map_raw2.Qvar,map_raw2.Uvar,lims{2}.v,[],3,1)
hold off

% pure B

ax5=axes('Position',[dim.x1,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,mapB.B,'iau',[], nancolor);
set(gca,'FontSize',fs,'FontWeight','bold');
set(gca,'Xticklabel',[-50 0 50]);
set(gca,'XTick',[-50 0 50]);
set(gca,'Yticklabel',[-65 -60 -55 -50]);
set(gca,'YTick',[-65 -60 -55 -50]);
caxis(lims{3}.c)
xlabel('Right Ascension [deg]','Fontname', 'new century schoolbook')
ylabel('Declination [deg]','Fontname', 'new century schoolbook')
hold on
plot_pol(m,mapB.BQ,mapB.BU,map_raw.Qvar,map_raw.Uvar,lims{3}.v,[],3,1)
hold off


ax6=axes('Position',[dim.x2,dim.y3,dim.xmap,dim.ymap]);
plot_map(m,mapB2.B,'iau',[], nancolor);
set(gca,'FontSize',fs,'FontWeight','bold');
set(gca,'Xticklabel',[-50 0 50]);
set(gca,'XTick',[-50 0 50]);
set(gca,'Yticklabel',[]);
set(gca,'YTick',[-65 -60 -55 -50]);
caxis(lims{3}.c)
hold on
plot_pol(m,mapB2.BQ,mapB2.BU,map_raw2.Qvar,map_raw2.Uvar,lims{3}.v,[],3,1)
hold off


% colorbars

ax7 = axes('Position',[dim.x3,dim.y1,dim.cbar/dim.W,dim.ymap]);
set(ax7,'FontSize',fs,'FontWeight','bold','Fontname', 'new century schoolbook');
imagesc(1:10,linspace(lims{1}.c(1),lims{1}.c(2),1000),repmat(linspace(lims{1}.c(1),lims{1}.c(2),1000)',1,10));
freezeColors;
set(ax7,'Xtick',[]);
set(ax7,'YDir','normal','YAxisLocation','right','YTick',lims{1}.ct,'FontSize',fs,'fontWeight','bold');
set(ax7, 'XaxisLocation', 'top')
xlabel('\muK','FontWeight','bold','Fontname', 'new century schoolbook')

ax8 = axes('Position',[dim.x3,dim.y2,dim.cbar/dim.W,dim.ymap]);
set(ax8,'FontSize',fs,'FontWeight','bold','Fontname', 'new century schoolbook');
imagesc(1:10,linspace(lims{2}.c(1),lims{2}.c(2),1000),repmat(linspace(lims{2}.c(1),lims{2}.c(2),1000)',1,10));
freezeColors;
set(ax8,'Xtick',[]);
set(ax8,'YDir','normal','YAxisLocation','right','YTick',lims{2}.ct,'FontSize',fs,'fontWeight','bold');
set(ax8, 'XaxisLocation', 'top')
xlabel('\muK','FontWeight','bold','Fontname', 'new century schoolbook')


ax9 = axes('Position',[dim.x3,dim.y3,dim.cbar/dim.W,dim.ymap]);
set(ax9,'FontSize',fs,'FontWeight','bold','Fontname', 'new century schoolbook');
imagesc(1:10,linspace(lims{3}.c(1),lims{3}.c(2),1000),repmat(linspace(lims{3}.c(1),lims{3}.c(2),1000)',1,10));
freezeColors;
set(ax9,'Xtick',[]);
set(ax9,'YDir','normal','YAxisLocation','right','YTick',lims{3}.ct,'FontSize',fs,'fontWeight','bold');
set(ax9, 'XaxisLocation', 'top')
xlabel('\muK','FontWeight','bold','Fontname', 'new century schoolbook')

% colorbar units

ax10 = axes('Position', [1 - dim.wide / dim.W, 0, dim.wide / dim.W, 1], ...
            'Visible', 'off');
set(ax10, 'FontSize', fs, 'FontWeight', 'bold','Fontname', 'new century schoolbook');
text(0.5, dim.y1 + dim.ymap / 2, 'Total Polarization', 'Rotation', 90, 'FontSize', fs, ...
     'FontWeight', 'bold', 'HorizontalAlignment', 'center','Fontname', 'new century schoolbook');
text(0.5, dim.y2 + dim.ymap / 2, 'pure E', 'Rotation', 90, 'FontSize', fs, ...
     'FontWeight', 'bold', 'HorizontalAlignment', 'center','Fontname', 'new century schoolbook');
text(0.5, dim.y3 + dim.ymap / 2, 'pure B', 'Rotation', 90, 'FontSize', fs, ...
     'FontWeight', 'bold', 'HorizontalAlignment', 'center','Fontname', 'new century schoolbook');


 
% this works way better than direct print to pdf
print_fig('~/matrix_paper/figs/proj_maps_6panel', [], [],1)


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function plot_specs_apscomp(mat,nor, inpmod)
set(0, 'DefaultFigureVisible', 'off')
clf
setwinsize(gcf,600,550)
fs=16;
set(0,'defaultAxesFontSize',fs);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',fs);


diff.Cs_l(:,3)=(mat.Cs_l(:,3)-nor.Cs_l(:,3))./nor.Cs_l(:,3)*1;
diff.Cs_l(:,4)=(mat.Cs_l(:,4)-nor.Cs_l(:,4))./nor.Cs_l(:,4)*1;
diff.lc=mat.lc;

subplot(14,1,[1,2,3,4])
plot_specs_sub_apscomp(3, 'EE', mat,nor, inpmod)

subplot(14,1,[5,6])
h2=plot_specs_diff(3, 'EE', diff)
subplot(14,1,[8,9,10,11,12])
h1=plot_specs_sub_apscomp(4, 'BB', mat,nor, inpmod)
legend([h1(1),h1(2),h1(3),h2], 'Theory','Standard Pipeline', 'Observation Matrix', 'Fractional diff')
legend boxoff
subplot(14,1,[13,14])
plot_specs_diff(4, 'BB', diff)


set(findall(gcf,'type','text'),'FontSize',fs,'fontWeight','bold', 'Fontname', 'new century schoolbook')

print_fig(['~/matrix_paper/figs/reob_apscomp'])
set(0, 'DefaultFigureVisible', 'on')

keyboard

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function h=plot_specs_diff(ind, tit, diff)

thigh=.12;
tlow=-.12; 

%plot colors
red=[.8,0,0];
lred=[.8,.4,.4];
gray=[.4,.4,.4];
lgray=[.7,.7,.7];

h=plot(diff.lc(2:end,ind), diff.Cs_l(2:end,ind), 'k', 'LineWidth', 3)

%ylabel(['Fractional difference'])
xlim([0,350])
ylim([tlow,thigh])
grid on
xlabel('Multipole ,  \it{l}')


return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function h=plot_specs_sub_apscomp(ind, tit, mat, nor, inpmod)



if  ind==4
  unconsi=squeeze(nor.Cs_l(1:10,ind));
  thigh=max([max(unconsi(:))]);
  thigh=.008
  tlow=0; 
else
  unconsi=squeeze(nor.Cs_l(1:9,ind));
  thigh=max([max(unconsi(:))]);
  tlow=-.01
end

%plot colors
red=[.8,0,0];
lred=[.8,.4,.4];
gray=[.4,.4,.4];
lgray=[.7,.7,.7];


h(1)=plot(inpmod.l, inpmod.Cs_l(:,ind), '--' ,'color', lred)
hold on
h(2)=plot(nor.lc(2:end,ind), nor.Cs_l(2:end, ind), 'color',red, 'LineWidth', 5)
h(3)=plot(mat.lc(2:end,ind), mat.Cs_l(2:end,ind), 'color',gray, 'LineWidth', 5, 'LineStyle', '--')


hold off

ylabel(['\it{l(l+1)C^{' tit  '}_l / 2\pi}'])
xlim([0,350])
ylim([tlow,thigh])
grid on


if ind==4
  arr=[0:.002:.008];
  for i=1:length(arr);fields{i}=num2str(arr(i), '%g');end
set(gca, 'YTick', arr, 'YTickLabel', fields);

end

set(gca, 'xticklabel',[])
return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_specs(cons, uncons, name)
clf
setwinsize(gcf,1100,700)
set(0,'defaultAxesFontSize',14);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',14);

ha=tight_subplot(3,2,[0.01 .1],[.1 .05],[.1 .05]);
axes(ha(1))
plot_specs_sub(1, 'TT', cons, uncons)
axes(ha(2))
plot_specs_sub(2, 'TE', cons, uncons)
axes(ha(3))
plot_specs_sub(3, 'EE', cons, uncons)
axes(ha(4))
plot_specs_sub(4, 'BB', cons, uncons)
axes(ha(5))
plot_specs_sub(5, 'TB', cons, uncons)
axes(ha(6))
plot_specs_sub(6, 'EB', cons, uncons)


set(findall(gcf,'type','text'),'FontSize',14,'fontWeight','bold', 'Fontname', 'new century schoolbook')


print_fig(['~/matrix_paper/figs/compare_constrained_spec_' name])


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_specs_sub(ind, tit, cons, uncons)



if ind==3 || ind==4
  consi=squeeze(cons.Cs_l(1:7,ind,:));
  unconsi=squeeze(uncons.Cs_l(1:7,ind,:));  
else
  consi=squeeze(cons.Cs_l(1:10,ind,:));
  unconsi=squeeze(uncons.Cs_l(1:10,ind,:));
end
thigh=max([max(consi(:)),max(unconsi(:))]);
tlow=min([min(consi(:)),min(unconsi(:))]); 

%plot colors
red=[.8,0,0];
lred=[.8,.4,.4];
gray=[.4,.4,.4];
lgray=[.7,.7,.7];

cons.mean=mean(cons.Cs_l(:,ind,:),3);
uncons.mean=mean(uncons.Cs_l(:,ind,:),3);


plot(cons.l(2:end), cons.mean(2:end), '--','color',red, 'LineWidth', 3)
hold on
plot(uncons.l(2:end), uncons.mean(2:end), '--','color',gray, 'LineWidth', 3)
plot(uncons.l(2:end), squeeze(uncons.Cs_l(2:end,ind,:)), 'color',lgray)
plot(cons.l(2:end), squeeze(cons.Cs_l(2:end,ind,:)), 'color',lred)
plot(cons.l(2:end), cons.mean(2:end), '--','color',red, 'LineWidth', 3)
plot(uncons.l(2:end), uncons.mean(2:end), '--','color',gray, 'LineWidth', 3)
hold off

ylabel(['\it{l(l+1)C^{' tit  '}_l / 2\pi} [\muK^2]'])
xlim([0,350])
ylim([tlow,thigh])
grid on
if ind==2
  legend('constrained', 'unconstrained', 'Location', 'NorthWest')
end
if(ind==5)
  xlabel('Multipole ,  \it{l}')
else
  if~(ind==6)
    set(gca, 'xticklabel',[])
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_fourpanel(map,m,lims,tnames)

%font size
fs=16;

nancolor=[.94,.94,.94];

clf
set(0,'defaultAxesFontSize',fs);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',fs);
setwinsize(gcf, 600,350)

dim=get_fourpaneldims(m);
set(gcf, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

colormap(gray)
ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,map(1).Q,'iau',[], nancolor);
set(ax1,'FontSize',fs,'FontWeight','bold');
set(ax1,'Xticklabel',[]);
set(ax1,'XTick',[-50 0 50]);
set(ax1,'YTick',[-65 -60 -55 -50]);
set(ax1,'Yticklabel',[]);
caxis(lims{1}.c)
hold on
%plot([2.35,2.35], [-56.35,-60.35], 'k', 'linewidth', 2)
%text(2.45, -55.35, '+Q','FontSize',fs,'FontWeight','bold')
%plot_pol(m,map(1).EQ,map(1).EU,map(1).Qvar,map(1).Uvar,lims{1}.v,[],3,0)
hold off
title(tnames{1},'Fontname', 'new century schoolbook');
freezeColors

ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,map(2).Q,'iau',[], nancolor);
set(ax2,'FontSize',fs,'FontWeight','bold');
set(ax2,'Xticklabel',[]);
set(ax2,'XTick',[-50 0 50]);
set(ax2,'YTick',[-65 -60 -55 -50]);
set(ax2,'Yticklabel',[]);
caxis(lims{2}.c)
hold on
%plot([2.35,2.35], [-56.35,-60.35], 'k', 'linewidth', 2)
%text(2.45, -55.35, '+Q','FontSize',fs,'FontWeight','bold')
%plot_pol(m,map(2).BQ,map(2).BU,map(2).Qvar,map(2).Uvar,lims{2}.v,[],3,0)
hold off
title(tnames{2},'Fontname', 'new century schoolbook');
freezeColors



ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,map(3).Q,'iau',[], nancolor);
set(ax3,'FontSize',fs,'FontWeight','bold');
set(ax3,'XTick',[-50 0 50]);
set(ax3,'XTickLabel',[-50 0 50]);
set(ax3,'YTick',[-65 -60 -55 -50]);
set(ax3,'YTicklabel',[-65 -60 -55 -50]);
xlabel('Right Ascension [deg]')
ylabel('Declination [deg]')
caxis(lims{3}.c)
hold on
%plot([2.35,2.35], [-56.35,-60.35], 'k', 'linewidth', 2)
%text(2.45, -55.35, '+Q','FontSize',fs,'FontWeight','bold')
%plot_pol(m,map(3).EQ,map(3).EU,map(3).Qvar,map(3).Uvar,lims{3}.v,[],3,0)
hold off
title(tnames{3},'Fontname', 'new century schoolbook');
freezeColors

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,map(4).Q,'iau',[], nancolor);
caxis(lims{4}.c)
hold on
%plot([2.35,2.35], [-56.35,-60.35], 'k', 'linewidth', 2)
%text(2.45, -55.35, '+Q','FontSize',fs,'FontWeight','bold')
%plot_pol(m,map(4).BQ,map(4).BU,map(4).Qvar,map(4).Uvar,lims{4}.v,[],3,0)
hold off
title(tnames{4},'Fontname', 'new century schoolbook');
set(ax4,'FontSize',fs,'FontWeight','bold');
set(ax4,'XTick',[-50 0 50]);
set(ax4,'XTickLabel',[-50 0 50]);
set(ax4,'YTick',[-65 -60 -55 -50]);
set(ax4,'YTicklabel',[]);
freezeColors



set(findall(gcf,'type','text'),'FontSize',fs,'fontWeight','bold', 'Fontname', 'new century schoolbook')



return

%%%%%%%%%%%%%%%%%%%%


function plot_fourpanel_vectors(map,m,lims)

%input



%font size
fs=14;

nancolor=[.94,.94,.94];

clf
set(0,'defaultAxesFontSize',fs);
set(0,'defaultAxesFontWeight','b');
set(0,'defaulttextfontsize',fs);
set(0,'defaultlinelinewidth',1.0);
setwinsize(gcf, 1025,500)

dim=get_fourpaneldims(m);
set(gcf, 'PaperPosition', [0 0 dim.W dim.H], 'PaperUnits', 'inches');

colormap(colormap_lint)
ax1=axes('Position',[dim.x1,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,map(1).E,'iau',[], nancolor);
set(ax1,'FontSize',fs,'FontWeight','bold');
set(ax1,'Xticklabel',[]);
set(ax1,'XTick',[-50 0 50]);
set(ax1,'YTick',[-65 -60 -55 -50]);
set(ax1,'Yticklabel',[]);
caxis(lims{1}.c)
hold on
plot_pol(m,map(1).EQ,map(1).EU,map(1).Qvar,map(1).Uvar,lims{1}.v,[],3,0)
hold off
title('E', 'Fontname', 'new century schoolbook', 'Fontangle', 'italic');
freezeColors

ax2=axes('Position',[dim.x2,dim.y1,dim.xmap,dim.ymap]);
plot_map(m,map(2).B,'iau',[], nancolor);
set(ax2,'FontSize',fs,'FontWeight','bold');
set(ax2,'Xticklabel',[]);
set(ax2,'XTick',[-50 0 50]);
set(ax2,'YTick',[-65 -60 -55 -50]);
set(ax2,'Yticklabel',[]);
caxis(lims{2}.c)
hold on
plot_pol(m,map(2).BQ,map(2).BU,map(2).Qvar,map(2).Uvar,lims{2}.v,[],3,0)
hold off
title('B', 'Fontname', 'new century schoolbook', 'Fontangle', 'italic');
freezeColors

axesPosition=get(gca, 'Position');
hnew=axes('Position',axesPosition, 'color', 'none','xtick',[], 'ytick',[], 'yaxislocation', 'right', 'Box', 'off');
ylabel(hnew, '1st Eigenmodes', 'Fontname', 'new century schoolbook')
set(hnew,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])

ax3=axes('Position',[dim.x1,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,map(3).E,'iau',[], nancolor);
set(ax3,'FontSize',fs,'FontWeight','bold');
set(ax3,'XTick',[-50 0 50]);
set(ax3,'XTickLabel',[-50 0 50]);
set(ax3,'YTick',[-65 -60 -55 -50]);
set(ax3,'YTicklabel',[-65 -60 -55 -50]);
xlabel('Right Ascension [deg]', 'Fontname', 'new century schoolbook')
ylabel('Declination [deg]', 'Fontname', 'new century schoolbook')
caxis(lims{3}.c)
hold on
plot_pol(m,map(3).EQ,map(3).EU,map(3).Qvar,map(3).Uvar,lims{3}.v,[],3,0)
hold off
freezeColors

ax4=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap]);
plot_map(m,map(4).B,'iau',[], nancolor);
caxis(lims{4}.c)
hold on
plot_pol(m,map(4).BQ,map(4).BU,map(4).Qvar,map(4).Uvar,lims{4}.v,[],3,0)
hold off

set(ax4,'FontSize',fs,'FontWeight','bold');
set(ax4,'XTick',[-50 0 50]);
set(ax4,'XTickLabel',[-50 0 50]);
set(ax4,'YTick',[-65 -60 -55 -50]);
set(ax4,'YTicklabel',[]);
freezeColors

hnew=axes('Position',[dim.x2,dim.y2,dim.xmap,dim.ymap], 'color', 'none','xtick',[], 'ytick',[], 'yaxislocation', 'right', 'Box', 'off');
ylabel(hnew, '50th Eigenmodes', 'Fontname', 'new century schoolbook')
set(hnew,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])

set(findall(gcf,'type','text'),'FontSize',fs,'fontWeight','bold')



return

%%%%%%%%%%%%%%%%%%%%%
function ha = tight_subplot(Nh, Nw, gap, marg_h, marg_w)

% tight_subplot creates "subplot" axes with adjustable gaps and margins
%
% ha = tight_subplot(Nh, Nw, gap, marg_h, marg_w)
%
%   in:  Nh      number of axes in hight (vertical direction)
%        Nw      number of axes in width (horizontaldirection)
%        gap     gaps between the axes in normalized units (0...1)
%                   or [gap_h gap_w] for different gaps in height and width 
%        marg_h  margins in height in normalized units (0...1)
%                   or [lower upper] for different lower and upper margins 
%        marg_w  margins in width in normalized units (0...1)
%                   or [left right] for different left and right margins 
%
%  out:  ha     array of handles of the axes objects
%                   starting from upper left corner, going row-wise as in
%                   going row-wise as in
%
%  Example: ha = tight_subplot(3,2,[.01 .03],[.1 .01],[.01 .01])
%           for ii = 1:6; axes(ha(ii)); plot(randn(10,ii)); end
%           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')

% Pekka Kumpulainen 20.6.2010   @tut.fi
% Tampere University of Technology / Automation Science and Engineering


if nargin<3; gap = .02; end
if nargin<4 || isempty(marg_h); marg_h = .05; end
if nargin<5; marg_w = .05; end

if numel(gap)==1; 
    gap = [gap gap];
end
if numel(marg_w)==1; 
    marg_w = [marg_w marg_w];
end
if numel(marg_h)==1; 
    marg_h = [marg_h marg_h];
end

axh = (1-sum(marg_h)-(Nh-1)*gap(1))/Nh; 
axw = (1-sum(marg_w)-(Nw-1)*gap(2))/Nw;

py = 1-marg_h(2)-axh; 

ha = zeros(Nh*Nw,1);
ii = 0;
for ih = 1:Nh
    px = marg_w(1);
    
    for ix = 1:Nw
        ii = ii+1;
        ha(ii) = axes('Units','normalized', ...
            'Position',[px py axw axh], ...
            'XTickLabel','', ...
            'YTickLabel','');
        px = px+axw+gap(2);
    end
    py = py-axh-gap(1);
end


%%%%%%%%%%%%%%%%%%%%%

function dim=get_fourpaneldims(m)
% Parameters for six-panel layout.
dim.W = 7.3; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 2 * dim.maph + dim.wide + 2 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 2 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y2 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return

%%%%%%%%%%%%%%%%%%%%%

function dim=get_twopaneldims(m)
% Parameters for six-panel layout.
dim.W = 4.8; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.2; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 2 * dim.thin - dim.cbar) / 1.05; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 2 * dim.maph + dim.wide + 2 * dim.med; % Overall height, in inches.
dim.x1 = dim.wide / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + 1 * dim.mapw + 1 * dim.thin) / dim.W; % Left edge of column 2 (colorbar).
dim.y1 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y2 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dim=get_sixpaneldims(m)
% Parameters for six-panel layout.
dim.W = 10; % Overall width, in inches.
dim.thin = 0.05; % Small gap, in inches.
dim.med = 0.24; % Medium gap, in inches.
dim.wide = 0.4; % Wide gap, in inches.
dim.cbar = 0.15; % Width of color bar, in inches.
dim.mapw = (dim.W - 2 * dim.wide - 4 * dim.thin - dim.cbar) / 2; % Map width, in inches.
dim.maph = dim.mapw * m.ydos / m.xdos; % Map height, in inches.
dim.H = 3 * dim.maph + dim.wide + 3 * dim.med; % Overall height, in inches.
dim.x1 = ( dim.wide + 2 * dim.thin ) / dim.W; % Left edge of column 1.
dim.x2 = (dim.wide + dim.mapw + 3* dim.thin) / dim.W; % Left edge of column 2.
dim.x3 = (dim.wide + 2 * dim.mapw + 4 * dim.thin) / dim.W; % Left edge of column 3 (colorbar).
dim.y1 = (dim.wide + 2 * dim.maph + 2 * dim.med) / dim.H; % Bottom edge of row 1.
dim.y2 = (dim.wide + 1 * dim.maph + 1 * dim.med) / dim.H; % Bottom edge of row 2.
dim.y3 = dim.wide / dim.H; % Bottom edge of row 3.
dim.xmap = dim.mapw / dim.W; % Map width as fraction of image width.
dim.ymap = dim.maph / dim.H; % Map height as fraction of image height.

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [r,im,ml, bpwf, filename]=get_b2xb2
% Single function fetches official B2xB2 spectra and models for
% plotting etc - one point switch!

filename = '0751/real_a_filtp3_weight3_gs_dp1102_jack0_matrix_directbpwf_rbc.mat';
load(['final/',filename])
r=get_bpcov(r); r=weighted_ellbins(r,bpwf); r.lc(1,:)=NaN;
im=inpmod;

% get the lensing model to compare to lensing sims
ml=load_cmbfast('aux_data/official_cl/camb_planck2013_r0_lensing.fits');

return