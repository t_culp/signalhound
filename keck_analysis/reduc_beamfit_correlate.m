function reduc_beamfit_correlate(mapname,beamcorropt,do_continue)
% reduc_beamfit_correlate(mapname,beamcorropt,do_continue)
%
% Correlate our CMB maps against WMAP data. This
% uses standard coadded maps from the spuder pipeline,
% or separate A/B maps that have not been summed and
% differenced.
% 
% The coadded maps are identified by 'mapname'.  This
% may include wildcards, for example:
% reduc_beamfit_correlate('0513/real_dk*_filtp3_weight3_gs_jack24')
% It can also be a cell array of file names.
%
% The input maps can be either per-pair sum and difference
% maps (reduc_makepairmaps, coadd with coaddtype=3), or
% single-channel maps (reduc_makeabmaps, coadd with
% coaddtype=4).  If both types of maps exist, you can use
% beamcorropt.coaddtype to specify which you want.  The
% coadded maps should ordinarily be made with the 'bicepfine'
% map type.
%
% The 'beamcorropt' structure is analogous to mapopt
% and coaddopt.  Relevant fields are:
%
%   'hmapfile' is the healpix map file from which the WMAP
%      CMB data should be loaded.
%   'coord' is 'G' or 'C' to indicate that the healpix map
%      is in galactic or equatorial coordinates.
%   'nshift' and 'nsubsamp' allow the size and fineness
%      of the correlation window to be controlled.  By
%      default, nshift=4 and nsubsamp=5.  To get a coarser
%      fit that can handle larger offsets, increase nshif
%      and decrease nsubsamp.
%   'corrname' is an optional additional identifier to go
%      at the start of the output file name.  Useful for
%      doing multiple beam fits on the same maps, using
%      different WMAP bands, Planck, etc.
%
% The output will be beamfit correlation files, which
% can then be fed to reduc_beamfit_fit as input.  These
% are saved in the equivalent location under 'beamfit'
% instead of 'maps'.
%
% The correlation step is somewhat slow (hours) and needs
% to be run on a 'bigmem' machine, if you're using maps
% of the type 'bicepfine'.
%
% Since this function can take a long time to run, it
% saves its output every time it runs through one tile's
% worth of detectors.  The calculation can be resumed
% from the last save by setting the 'do_continue' flag:
% reduc_beamfit_correlate(beamcorropt,1).
%

if (nargin<1) || isempty(mapname)
  error(['Must specify at least one coadded map file to correlate.']);
end

if (nargin<2)
  beamcorropt = [];
end

% WMAP healpix map; by default, don't put anything for now.
% This gives us the chance later to see if we can get a
% healpix map name from simopt.
if ~isfield(beamcorropt,'hmapfile') || isempty(beamcorropt.hmapfile)
  beamcorropt.hmapfile = '';
end
if ~isfield(beamcorropt,'coord') || isempty(beamcorropt.coord)
  beamcorropt.coord = '';
end

% How many map pixels to shift by, and how finely to
% sub-sample?  These affect the size and resolution of
% the correlation matrices, and the running time.
DEF_NSHIFT = 4;
DEF_NSUBSAMP = 5;
if ~isfield(beamcorropt,'nshift')
  beamcorropt.nshift=DEF_NSHIFT;
end
if ~isfield(beamcorropt,'nsubsamp')
  beamcorropt.nsubsamp=DEF_NSUBSAMP;
end
if ~isfield(beamcorropt,'tile')
  beamcorropt.tile=[];
end
if (nargin<3) || isempty(do_continue)
  do_continue = false;
end

if ~iscell(mapname)
  mapname={mapname};
end

% Any additional identifier for output file names?
if ~isfield(beamcorropt,'corrname')
  beamcorropt.corrname='';
end

% loop over files and do correlation for each coadded mapfile
for i=1:length(mapname)
  [fpath fname fext]=fileparts(mapname{i});
  if isempty(fext)
    fext='.mat';
  end
  mapdir=fpath;
  d=dir(fullfile(mapdir,[fname fext]));
  if isempty(d)
    mapdir=fullfile('maps',fpath);
    d=dir(fullfile(mapdir,[fname fext]));
  end
  for j=1:length(d)
    brp_correlate_map(fullfile(mapdir,d(j).name), ...
      beamcorropt, do_continue, 'beamfit');
  end
end

return

%%%%
% Meat of the function goes here. Do correlations for a single
% coadded map file.
function brp_correlate_map (cmfile, beamcorropt, do_continue, outdir)

nshift=beamcorropt.nshift;
nsubsamp=beamcorropt.nsubsamp;
hmapfile=beamcorropt.hmapfile;

disp(['Loading coadded map file ' cmfile]);
cmap = load(cmfile);
if ~isfield(cmap,'map')
  disp('Coadded ac loaded.  Converting to map')
  %have to sparse it... reduces size by a factor of 10
  cmap.ac=sparse_ac(cmap.ac);
  %convert into map...
  cmap.map=make_map(cmap.ac,cmap.m,cmap.coaddopt);
  cmap.ac=[]; cmap.coaddopt.c=[]; %remove to save memory... 
  %should also sparse map
  cmap.map=sparse_ac(cmap.map);
end
[p,ind] = get_array_info(cmap.coaddopt.mapopt{1}.tag);

if isempty(hmapfile)
  if isfield(cmap.coaddopt.mapopt{1},'simopt') ...
    && isfield(cmap.coaddopt.mapopt{1}.simopt,'sigmapfilename')
    hmapfile = cmap.coaddopt.mapopt{1}.simopt.sigmapfilename;
    coord = cmap.coaddopt.mapopt{1}.simopt.coord;
  else
    hmapfile = '~justusb/code/smooth/wmap5yr/wmap_band_iqumap_r9_5yr_W_v3_bicep2ideal.fits';
    coord = 'G';
  end
end
beamcorropt.hmapfile=hmapfile;
if isfield(beamcorropt,'coord') && ~isempty(beamcorropt.coord)
  coord = beamcorropt.coord;
end
beamcorropt.coord=coord;

disp(['Loading and massaging WMAP map from healpix file ' hmapfile]);
[tmap,tmap0,tmap1ra,tmap1de]=prepare_wmap_maps(hmapfile,coord, ...
  nshift,nsubsamp,cmap.m);

if ~isfield(beamcorropt,'sernum') || isempty(beamcorropt.sernum)
  beamcorropt.sernum=cmap.coaddopt.sernum;
end
outfname=get_beamfit_filename(beamcorropt);
disp(['Output file name is ' outfname]);
% make sure there's a place to save
if ~exist(outdir,'dir')
  mkdir(outdir);
end
[fdir fname fext]=fileparts(outfname);
if ~exist(fdir,'dir')
  mkdir(fdir);
end

% If we have pair sum/diff maps...
if isfield(cmap.map,'Tsum')
  % How many pairs?
  npair = size(cmap.map,1);
% If we have separate A/B maps...
else
  % How many pairs?
  npair = length(ind.a);
end
% How many scan directions?
nscandirs = size(cmap.map,2);

% Initialize empty structures for output
xc = zeros(2*nshift*nsubsamp+1,2*nshift*nsubsamp+1,npair,nscandirs);
xca = xc;
xcb = xc;
diffcorr = zeros(npair,nscandirs,3);

% Get pair r and theta values
tmpind = make_ind(cmap.coaddopt.p(1));
feed_r = cmap.coaddopt.p(1).r(tmpind.a);
feed_th = cmap.coaddopt.p(1).theta(tmpind.a);
clear tmpind
dk = str2num(cmap.coaddopt.tags{1}((end-2):end));

% Fill in extra stuff to be saved
% These are useful for reckoning and plotting, but not actually used here.
dd = 1:(2*nshift*nsubsamp+1);
dd = dd - nshift*nsubsamp-1;
dd = dd / nsubsamp;
m.x_tic = dd * median(diff(cmap.m.x_tic));
m.y_tic = dd * median(diff(cmap.m.y_tic));
% Find schedule name of earliest tag
schdate = Inf;
for i=1:length(cmap.coaddopt.tags)
  schdate=min(schdate,parse_tag(cmap.coaddopt.tags{i}));
end
beamcorropt.schdate=schdate;
% Save beam positions, etc. used to make the maps
% NOTE - the [p,ind] in cmap.coaddopt is NOT the
% one used for mapmaking! That set is only for
% channel cuts in reduc_coaddpairmaps.
if isfield(cmap.coaddopt.mapopt{1},'p')
  beamcorropt.map_p = cmap.coaddopt.mapopt{1}.p(1);
  beamcorropt.map_ind = make_ind(beamcorropt.map_p);
else
  [beamcorropt.map_p,beamcorropt.map_ind] = ...
    get_array_info(cmap.coaddopt.mapopt{1}.tag, cmap.coaddopt.mapopt{1}.beamcen);
end

% Pick up where a partial run left off, if requested and if possible
if do_continue && exist(outfname,'file')
  tile=beamcorropt.tile;
  load(outfname);
  beamcorropt.tile=tile;
  if ~exist('done_list','var')
    disp(['Existing xcorr file already complete: ' outfname]);
  end
else
  done_list = false(nscandirs,npair);
end

% Loop over scan directions and pairs
disp(['Performing convolution.']);
for sdir=1:nscandirs
  for jdx=1:npair

    if done_list(sdir,jdx)
      continue
    end

    if ~isempty(beamcorropt.tile)
      if p.rx(ind.a(jdx))*4+p.tile(ind.a(jdx))~=beamcorropt.tile
        continue
      end
    end

    % Print out a friendly message occasionally to let you know we're still alive
    if 1 | mod(jdx,10)==1
      disp(['Correlate chan ' num2str(jdx) ', sdir=' num2str(sdir)]);
    end

    % If we have pair sum/diff maps
    if isfield(cmap.map,'Tsum')
      chans=jdx;
    % If we have A/B separate maps
    else
      % one map per chan, including dark squids
      if size(cmap.map,1)==max(ind.e)
        chans=[ind.a(jdx); ind.b(jdx)];
      % or not including dark squids
      else
        chans=[jdx; jdx+length(ind.a)];
      end
    end

    % Cross-correlations against pair sums
    xc(:,:,jdx,sdir) = cross_correlate(cmap.map(chans,sdir),'sum',tmap,nshift,nsubsamp);
    % Cross-correlations against kludged A maps
    xca(:,:,jdx,sdir) = cross_correlate(cmap.map(chans,sdir),'A',tmap,nshift,nsubsamp);
    % Cross-correlations against kludged B maps
    xcb(:,:,jdx,sdir) = cross_correlate(cmap.map(chans,sdir),'B',tmap,nshift,nsubsamp);

    % Regression of derivative maps against pair differences
    % Only do this for pair sum/difference map types
    diffcorr(jdx,sdir,1) = NaN;
    diffcorr(jdx,sdir,2) = NaN;
    diffcorr(jdx,sdir,3) = NaN;

    if isfield(cmap.map,'Tdif')
      try
        td = cmap.map(jdx,sdir).Tdif(:);
        tw = 1 ./ cmap.map(jdx,sdir).Tdifvar(:);
        [dra,dde] = reckon_ab_ofs_dir(feed_r(jdx),feed_th(jdx),cmap.m.y_tic,dk);
        nrep = length(cmap.m.x_tic);
        tmap1x = tmap1ra .* repmat(dra{1}',1,nrep) + tmap1de .* repmat(dde{1}',1,nrep);
        diffcorr(jdx,sdir,1) = regress(td .* tw,tmap1x(:) .* tw);
        tmap1y = tmap1ra .* repmat(dra{2}',1,nrep) + tmap1de .* repmat(dde{2}',1,nrep);
        diffcorr(jdx,sdir,2) = regress(td .* tw,tmap1y(:) .* tw);

        % Regression of pair sum against T map, for normalization
        ts = cmap.map(jdx,sdir).Tsum(:);
        tw = 1 ./ cmap.map(jdx,sdir).Tvar(:);
        diffcorr(jdx,sdir,3) = regress(ts .* tw, tmap0(:) .* tw);

      catch
        diffcorr(jdx,sdir,1) = NaN;
        diffcorr(jdx,sdir,2) = NaN;
        diffcorr(jdx,sdir,3) = NaN;
      end
    end

    done_list(sdir,jdx) = true;

    % Save every one tile's worth
    if mod(jdx,64)==0
      disp(['Saving partial output: sdir=' num2str(sdir) ', pair=' num2str(jdx) ', outfile=' outfname '.']);
      save(outfname,'beamcorropt','m','xc','xca','xcb','diffcorr','done_list');
    end
  end
end

% Save the output.
save(outfname,'beamcorropt','m','xc','xca','xcb','diffcorr','done_list');

return



% Helper function - reckon from offsets in fp_data X and Y
% to corresponding offsets in RA and DE.  These are used
% to perform a coordinate transform on the partial derivatives
% of the WMAP map before regressing against our pair difference
% maps.
% Note the dk here is in horizontal coordinates, while
% we're otherwise working with equatorial coordinates.
% Therefore, the reckon uses th+90-dk instead of th-90-dk.
function [dra, dde] = reckon_ab_ofs_dir(r,th,de,dk)

  % An arbitrary small offset, in degrees in fp_data coordinates
  DELTA = 1e-5;

  % Define my fp_data X and Y
  x0 = r .* cos(th * pi/180);
  y0 = r .* sin(th * pi/180);

  % Apply small offsets in fp_data coordinates
  r1 = sqrt((x0 + DELTA).^2 + (y0).^2);
  th1 = 180/pi * atan2 (y0, x0 + DELTA);
  r2 = sqrt((x0).^2 + (y0 + DELTA).^2);
  th2 = 180/pi * atan2 (y0 + DELTA, x0);

  % Find range of boresight elevations that will
  % give single-valued interpolation
  de0 = -90:0.1:90;
  [b0,a0] = reckon(de0,0 * de0,r,th+90-dk);
  [tmp imin] = min(b0);
  [tmp imax] = max(b0);
  de0 = de0(imin:imax);

  % Reckon from boresight pointing to map pixels
  [b0,a0] = reckon(de0,0 * de0,r,th+90-dk);
  [b1,a1] = reckon(de0,0 * de0,r1,th1+90-dk);
  [b2,a2] = reckon(de0,0 * de0,r2,th2+90-dk);

  % And interpolate for requested map pixel locations
  if 0
    dra{1} = interp1(de0,a1-a0,de) / DELTA;
    dra{2} = interp1(de0,a2-a0,de) / DELTA;
    dde{1} = interp1(de0,b1-b0,de) / DELTA;
    dde{2} = interp1(de0,b2-b0,de) / DELTA;
  else
    dra{1} = interp1(b0,a1-a0,de) / DELTA;
    dra{2} = interp1(b0,a2-a0,de) / DELTA;
    dde{1} = interp1(b0,b1-b0,de) / DELTA;
    dde{2} = interp1(b0,b2-b0,de) / DELTA;
  end

  return


% Helper function - load a WMAP map from healpix file,
% interpolate to sub-sampled BICEP2/Keck map coordinates,
% smooth out healpix pixelization artifacts,
% prepare derivative maps in RA and DE.
function [tmap,tmap0,tmap1ra,tmap1de]=prepare_wmap_maps(hmapfile,coord,nshift,nsubsamp,m)

  nshift = nshift;
  nsubsamp = nsubsamp;

  disp(['Loading healpix map ' hmapfile]);
  hmap = read_fits_map(hmapfile);
  disp(['Translating healpix map into simple lat/long structure, assuming ' coord ' input coordinates.']);

  % Calculate sub-sampled map pixel tics
  xticsize = median (diff (m.x_tic));
  yticsize = median (diff (m.y_tic));
  xt = min(m.x_tic) : (xticsize/nsubsamp) : (max(m.x_tic) + xticsize*nsubsamp/(nsubsamp+1));
  yt = min(m.y_tic) : (yticsize/nsubsamp) : (max(m.y_tic) + yticsize*nsubsamp/(nsubsamp+1));
  
  % approximate resolution of healpix map in degrees.
  npix = 12 * (hmap.nside)^2;
  hmapres = sqrt((4*pi)/npix) * 180/pi;

  % Sample WMAP data at our fine map coordinates
  lmap = healpix_to_longlat(hmap,xt,yt,coord);
  tmap = lmap(:,:,1);
  clear hmap lmap 

  % Smooth the healpix map to remove healpix pixelization artifacts.
  %  Dumdum method.

  % Note that x tic spacing is in degrees of RA,
  % *not* degrees of arc.  So decide smoothing window
  % size based on Y tic size, and use for both.

  fy = hmapres / yticsize * nsubsamp * 5;
  fy = floor((fy+1)/2)*2+1;
  fx = fy;

  tmapf = sgolayfilt(tmap, 2, fx, [], 2);
  tmapf = sgolayfilt(tmapf, 2, fy, [], 1);

  tmap = tmapf;
  clear tmapf;

  % Construct derivatives in RA and DE 
  D = floor((nsubsamp+1)/2);
  tmap1ra = (circshift(tmap,[0 D]) - circshift(tmap,[0 -D])) / (2*D*1000) / xticsize * nsubsamp;
  tmap1de = (circshift(tmap,[D 0]) - circshift(tmap,[-D 0])) / (2*D*1000) / yticsize * nsubsamp;
  tmap1ra = tmap1ra(1:nsubsamp:end,1:nsubsamp:end);
  tmap1de = tmap1de(1:nsubsamp:end,1:nsubsamp:end);
  tmap0   = tmap(1:nsubsamp:end,1:nsubsamp:end) / 1000;

  return


% Helper function - perform the map cross-correlation for one detector or pair sum.
function [xc,xnorm]=cross_correlate(map,type,tmap,nshift,nsubsamp)

    % We have standard pair sum/diff maps.
    % Need kludge to get separate A and B maps.
    if isfield(map,'Tsum')
      switch(lower(type))
        case {'','sum'}, z = map.Tsum;
        case {'a'}, z = map.Tsum + map.Tdif;
        case {'b'}, z = map.Tsum - map.Tdif;
        otherwise, error(['Unknown correlation type ' type '.']);
      end
      tvar = map.Tvar;

    % We have separate A/B maps.  They have
    % never been through pair sum/diff, and
    % can be used directly.
    else
      switch(lower(type))
        case {'','sum'}, z = (map(1).T + map(2).T) / 2; tvar = (map(1).Tvar + map(2).Tvar) / 2;
        case {'a'}, z = map(1).T; tvar = map(1).Tvar;
        case {'b'}, z = map(2).T; tvar = map(2).Tvar;
      end
    end

    % Simple cut on variance of map pixels
    cc = tvar(:) <= nanmedian(tvar(:));
    z(~cc) = NaN;

    % Construct map norms -- not actually used.  Useful in the case
    % that you want to do the correlation phase-by-phase and coadd
    % later, with cuts on norm to throw out crap data.
    norm1 = 1 / sqrt (nansum (nansum (z .* z)));
    norm2 = 1 / sqrt (nansum (nansum (tmap(cc).^2)));

    xc = zeros(2*nshift*nsubsamp+1,2*nshift*nsubsamp+1);

    % Loop over offsets in 2 dimensions.
    for s1=1:(2*nshift*nsubsamp+1)
      for s2=1:(2*nshift*nsubsamp+1)
        ofs1 = s1 - nshift*nsubsamp-1;
        sel1 = mod (ofs1, nsubsamp) + 1;
        shf1 = floor ((nsubsamp-ofs1-1) / nsubsamp);
        ofs2 = s2 - nshift*nsubsamp-1;
        sel2 = mod (ofs2, nsubsamp) + 1;
        shf2 = floor ((nsubsamp-ofs2-1) / nsubsamp);

        shiftmap = circshift(tmap(sel1:nsubsamp:end, sel2:nsubsamp:end), [shf1, shf2]);
        xc(s1,s2) = nansum(nansum(z .* shiftmap));
      end
    end

    % Calculate norm -- not currently used.
    xnorm = max (max (xc)) * norm1 * norm2;

    return

%Helper function - Sparse AC to avoid memory issues
function ac=sparse_ac(ac)

fns=fieldnames(ac);
for ii=1:size(ac,1)
  for jj=1:size(ac,2)
    for fn=fns'
      ac(ii,jj).(fn{1})=sparse(ac(ii,jj).(fn{1}));
    end
  end
end

return

