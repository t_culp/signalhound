function [rmssa,sa,outlierind]=plot_res(model,ide,obs,ttext,doplots)
% rmssa=plot_res(model,ide,obs,ttext,doplots)
%
% Plot the model residuals.

d2r=pi/180;

% Find the model values
mva=pointing_model(model,ide);

% Find the az/el residuals
res.az=obs.az-mva.az;
ind=res.az>180; res.az(ind)=res.az(ind)-360;
ind=res.az<-180; res.az(ind)=res.az(ind)+360;
ind=res.az>180; res.az(ind)=res.az(ind)-360;
ind=res.az<-180; res.az(ind)=res.az(ind)+360;

res.el=obs.el-mva.el;

% Find the space angles between observed and reference values
%sa=spaceangle(obs.az,obs.el,mva.az,mva.el,'deg');
%
% JMK: the above is WRONG when the encoder offsets are not small
%  Fixed this by subtracting encoder offsets from both obs and mva coords
%  coordinates, to make them close to ide coords.
sa=spaceangle(obs.az-model(10),obs.el-model(11),mva.az-model(10),mva.el-model(11),'deg');
%  Note that this is STILL not really a proper spaceangle, since
%  obs and mva coordinates give the angles of the mount axes, not angles
%  on the sphere.  Strictly, if we want the residual spaceangle we would
%  compare ide coords with the computed ideal topocentric coords given by
%  applying the inverse pointing model to the obs coords--but that seems
%  less direct than measuring residuals about the mount axes.

rmssa=rms(sa);
ind=sa>3*rmssa;
outlierind=find(ind);

if(~doplots)
  return
end

% Calc the cos(el) factor
ide.cosel=cos(ide.el*d2r);

setwinsize(gcf,600,600); clf

% Plot the sky positions hit
subplot(3,3,1)
polar(ide.az*d2r,90-ide.el,'+');
view(90,-90);
hold on; polar(ide.az(ind)*d2r,90-ide.el(ind),'ro'); hold off

subplot(3,3,4)
plot(ide.az,res.az.*ide.cosel,'+'); xlabel('az'); ylabel('az residual * cos(el)');
hold on; plot(ide.az(ind),res.az(ind).*ide.cosel(ind),'ro'); hold off
xlim([0,360]); grid
subplot(3,3,5);
plot(ide.el,res.az.*ide.cosel,'+'); xlabel('el'); ylabel('az residual * cos(el)');
hold on; plot(ide.el(ind),res.az(ind).*ide.cosel(ind),'ro'); hold off
xlim([0,90]); grid
subplot(3,3,6);
plot(ide.dk,res.az.*ide.cosel,'+'); xlabel('dk'); ylabel('az residual * cos(el)');
hold on; plot(ide.dk(ind),res.az(ind).*ide.cosel(ind),'ro'); hold off
xlim([-180,180]); grid

subplot(3,3,7)
plot(ide.az,res.el,'+'); xlabel('az'); ylabel('el residual'); %ylim([-0.01,0.02]);
hold on; plot(ide.az(ind),res.el(ind),'ro'); hold off
xlim([0,360]); grid
subplot(3,3,8)
plot(ide.el,res.el,'+'); xlabel('el'); ylabel('el residual'); %ylim([-0.01,0.02]);
hold on; plot(ide.el(ind),res.el(ind),'ro'); hold off
xlim([0,90]); grid
subplot(3,3,9)
plot(ide.dk,res.el,'+'); xlabel('dk'); ylabel('el residual'); %ylim([-0.01,0.02]);
hold on; plot(ide.dk(ind),res.el(ind),'ro'); hold off
xlim([-180,180]); grid

% Write the model parameters if available
if(exist('model','var'))
  subplot(3,3,3);
  modpar={'flex cos','flex sin','az tilt ha','az tilt lat',...
      'el tilt','collim x','collim y','collim mag','collim dir',...
      'az zero','el zero','num pts'};
  disp_vals=[model,length(obs.az)];
  for i=1:length(modpar)
    h=text(0.1,(10-i)*0.1,sprintf('%s = %f',modpar{i},disp_vals(i)));
    if(disp_vals(i)~=0)
      set(h,'Color','b');
    end
  end
  axis off
end

% Plot space angle histograms
subplot(3,3,2)
m=max(sa)*1.000001;
if isreal(sa) & any(isfinite(sa))
  hfill(sa,20,0,m,[],'r');
  hfill(sa(~ind),20,0,m,[],'bS');
  xlabel('space angle error');
  y=ylim; ylim([0,y(2)*1.1]);
  axes('Position',get(gca,'Position'),'Xlim',[0,1],'Ylim',[0,1],'Visible','off');
  text(0.1,0.9,sprintf('rms=%.1f arcsec',rms(sa(~ind))*60^2),'Color','b');
  if(sum(ind)>0)
    text(0.1,0.8,sprintf('rms=%.1f arcsec',rms(sa)*60^2),'Color','r');
  end
end

gtitle(ttext)

return
