function reduc_initial(tag,deglitch_switch,deconv_switch)
% reduc_initial(tag,deglitch_switch,deconv_switch)
%
% Reduce a complete day of data:
% - deconv & lowpass
% - deskip & deglitch
% - calc pointing
% - find cal periods
% - measure cals & save cal info
% - save tod
%
% e.g.: reduc_initial('20110308C03_dk068')

code_dir = pwd;

if(~exist('deglitch_switch','var'))
  deglitch_switch=[];
end
if(~exist('deconv_switch','var'))
  deconv_switch=[];
end

if(isempty(deglitch_switch))
  deglitch_switch=1;
end
if(isempty(deconv_switch))
  deconv_switch=1;
end

% presumably we always want to deskip
deskip_switch=1;

% get full day's data
d=read_run(tag);

% do a sanity check on the data.  
r = get_run_info(tag);

% Convert start and stop times to MJD:
t_comp = d.array.frame.utc(d.array.frame.utc~=0);
Vs = datevec(r.tstart{1},'dd-mmm-yyyy:HH:MM:SS');
mjd_start = date2mjd(Vs(1),Vs(2),Vs(3),Vs(4),Vs(5),Vs(6));
delta_start = abs(mjd_start - t_comp(1))*24*3600;

Ve = datevec(r.tend{1},'dd-mmm-yyyy:HH:MM:SS');
mjd_end = date2mjd(Ve(1),Ve(2),Ve(3),Ve(4),Ve(5),Ve(6));
delta_end = abs(mjd_end - t_comp(end))*24*3600;

% Check to see that the start and stop times of the tag agree with the
% start and stop of the timestream:
if delta_start > 10 || delta_end > 10
  error(['This tag appears to be incomplete.  The start and stop time of ' ...
    'the tag differs from the start and stop time of the timeseries by more '...
    'than 30 seconds'])
end

% Also make sure that the length of the trace is something reasonable:
if length(d.t) < 5E4
  error('This tag appears to be incomplete judging fron the length of the time trace')
end

% since all mce's are packed into mce0 by read_arc, we only need to check
% mce0:
if isempty(d.mce0.data.fb(d.mce0.data.fb~=0))
  error('No non-zero data to analyze - likely due to an MCE crash.')
end

% fast/slow ratio is assumed fixed during any period of data read in
% (requires gcp recompile to change - it's 20 in all B2/Keck data.
% NB: changing the gcp downsample ratio does not change the fast/slow
% ratio, it changes clock time interval between samples.)
sampratio=length(d.t)/length(d.ts);

% get nominal array info
[p,ind]=get_array_info(tag);

% deskip/deglitch/deconv only data marked for analysis
% don't do these operations to partial load curves (bit=2^14)
% in B2/Keck squid based readout the values jump around for 
% a number of reasons. assume that gcp downsample (e.g. samprate) 
% does not change during each such periods.
% tweak the deconv/data block to include a block of constant samprate
% to minimize invalid regions in elnods due to feature bit timing.
% this also prevents the introduction of a step in the elnods when
% steps are releveled in deglitch.  Use b for all deconv/deglitch/deskip
b=tweak_deconv_blocks(d);
if(isempty(b.s))
  error(['No valid data to analyze in tag ' tag]);
end

% DECONVOLVE
% deconvolution now done in the time domain, so NaNs in data are ok,
% but NaNd regions will grow by ~1/2 the kernel length during deconv.
% the deconvolution kernel includes the low pass filter and is FIR.
% to keep in-band ripple small the stop band is very wide, so it isn't
% an adequate low pass filter for downsampling... don't downsample!
if(deconv_switch==1)
  % fetch measured bolometer transfer functions
  % pass as d.tf so deglitch/deskip and can NaN based on kernel size
  d=get_xferfunc(d,b);
  d=deconv_scans(d,p,1);
  % Note: there will be a transient as the filter turns on / off.  This will
  % be taken care of in deconv_scans by the 'mask' option.  The masking length
  % is determined from the size of the FIR deconv kernel
end

% DESKIP & DEGLITCH
% the deconvolution kernel is FIR, so non-physical steps/deltas
% can be deconvolved across and masked based on the kernel size.

% DESKIP - HANDLE ANY SKIPPED SAMPLES FROM BBCPCI CARD
% These affect the antenna layer, and are usually single
% missing samples in antenna.*.  In some cases, there may
% be strings of skipped samples. Save ds structure of samples
% that were deskipped.
if(deskip_switch==1)
  [d,ds]=deskip_scans(d,b);
else
  ds=[];
end

% DEGLITCH - (Replaces glitch with NaN)
% In B2/Keck the primary cause of glitches is readout (squid jumps
% etc) which are non-physical, so the region around these should be
% NaNd. if the data are deconvolved, use the kernel size to determine
% the region size. always attempt to level steps due to flux jumps, 
% even if no numfj data is available. Save dg structure of masks 
% applied in deglitch.
if(deglitch_switch==1)
  % Pass everything to deglitcher (ind.e instead of ind.ge).
  % This allows destepping good channels with bad jumpy neighbors.
  [d,dg]=deglitch(d,b,ind.e,p);
else
  dg=[];
end

% calc these after lowpass...
% determine nominal az/el and az/el offset of center pixel at each
% time step (adds d.az_off,el_off and rawpoint.hor)
d=arcvar_to_azel(d);
%decide if the mirror is on by checking if the dk encode sign is negative
if mode(double(d.antenna0.tracker.encoder_sign(:,3))) == -1
  mirror=1;
else
  mirror=0;
end
% fetch the pointing model parameters
% dropped samples throw off mean(d.t)
pm=get_pointing_model(mean(d.t(d.t~=0)),mirror,d);
% apply the inverse pointing model to generate d.pointing.hor
d=invpointing_model(d,pm, mirror);


% do the transform from az/el to ra/dec 
%  - method and functions stolen from Cynthia bcn code point.h 
% (lat/lon stored in milli arcsec)
lat=median(double(d.antenna0.tracker.siteActual(:,2))/3.6e6);
lon=median(double(d.antenna0.tracker.siteActual(:,1))/3.6e6);
[d.pointing.cel.ra,d.pointing.cel.dec]=azel2radec(...
    d.pointing.hor.az,d.pointing.hor.el,d.t,lat,lon);
d.pointing.cel.dk=d.pointing.hor.dk-parallactic(...
    d.pointing.hor.az,d.pointing.hor.el,d.t,lat,lon);


% REDUCE CALS

% FIND PERIODS OF EACH TYPE
% find start,end of elnods
en=find_blk(bitand(d.array.frame.features,2^3),sampratio);
en=tweak_elnods(d,en,sampratio);
en.t=d.t(en.sf);

% find start,end of partial load curves
lc=find_blk(bitand(d.array.frame.features,2^14),sampratio);
lc.t=d.t(lc.sf);

% find start,end of field scan half-scans
fs=find_scans(d.antenna0.tracker.scan_off(:,1),bitand(d.array.frame.features,2^1),sampratio);
fs.t=d.t(fs.sf);

% reduce el nods
if(~isempty(en.s))
  [en.g,endat]=elnod(d,p,en,[ind.l,ind.d]);
else
  endat=[];
end

% reduce partial load curves
if(~isempty(lc.s))
  [lc, lcdat]=plc(d,p,lc,[ind.l,ind.d]);
  lcdat=rmfield(lcdat,'rdet');  %not saved since redundant with idet and vdet
  lcdat=rmfield(lcdat,'bias'); 
else
  lcdat=[];
end

% find std of field scans which is a useful diagnostic
fs.std=scan_std(d,fs);

% save cal values to disk without tod - allows for fast readin and
% plotting of values over large timespans
saveandtest(['data/real/',tag(1:6),'/',tag,'_calval'],'en','fs','lc','code_dir');
setpermissions(['data/real/',tag(1:6),'/',tag,'_calval.mat']);

% save cal data to disk (can be used to investigate miss fits)
saveandtest(['data/real/',tag(1:6),'/',tag,'_caldat'],'endat','lcdat','code_dir');
setpermissions(['data/real/',tag(1:6),'/',tag,'_caldat.mat']);

if(~isempty(fs.s))
  % get scan info
  fs=get_scan_info(d.antenna0.tracker.scan(fs.s,:),fs);
  
  % tweak sf and ef such that half-scan cover only specifed scan throw
  % and all have exact same length
  % the 1.13 is turnfrac parameter used by Cynthia in BICEP1 analysis
  fs=tweak_field_scans(d,fs,1.13);

  % Identify overlapping half-scans by noting if a half-scan start time
  % preceeds the end time for the previous half-scan. This is an irrecoverable
  % error that may happen post-tweaking due to dropped frames.
  overlaps = fs.ef(1:end-1) > fs.sf(2:end);
  if any(overlaps)
    error(['This tag seems to be corrupted, possibly due to dropped ' ...
        'frames. After tweaking the field scan blocks, at least one ' ...
        'half-scan now overlaps with a neighboring block.']);
  end
end

% make fsb (field-scan-blocks) structure
fsb=find_blk(bitand(d.array.frame.features,2^1),sampratio);
fsb.t=d.t(fsb.sf);

% Ensure that a spurious NaN (probably due to a dropped sample) does not
% stall identification of field sets. Simply choose the first non-NaN time
% index.
% 
% This simultaneously updates the time definitions to match the post-tweaked
% timing rather than continuing to use the original times.
for i=1:length(fs.sf)
  tt = d.t(fs.sf(i):fs.ef(i));
  tt = tt(~isnan(tt) & tt>0);
  fs.t(i) = tt(1);
end

% use fsb to fill in fs.set field Denis introduced
for i=1:length(fsb.s)
  fs.set(fs.t>d.t(fsb.sf(i))&fs.t<d.t(fsb.ef(i)),1)=i;
end

% d.array.frame.features goes away before the last scan of a block is
% actually finished. Fix this up by forcing block end to match the end
% of the last scan
for i=1:length(fsb.s)
  q=find(fs.set==i);
  fsb.e(i)=fs.e(q(end));
  fsb.ef(i)=fs.ef(q(end));
end

% save d structure to disk
matfile=['data/real/',tag(1:6),'/',tag,'_tod_uncal'];

Ndet=size(d.mce0.data.fb,2);
if Ndet<1000
  % BICEP2 save as the superior v7
  ver='-v7';
else
  % Keck save as the lame v7.3, necessary for variables larger than 2 GB
  ver='-v7.3';
end

saveandtest(matfile,'d','fs','en','lc','fsb','dg','ds','pm','code_dir',ver);
setpermissions([matfile '.mat']);

return
