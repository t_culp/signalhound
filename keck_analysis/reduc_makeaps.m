function reduc_makeaps(mapname,apsopt)
% reduc_makeaps(mapname,apsopt)
%
% Measure angular power spectra from coadd maps and write back to file
%
% if mapname is:
%      single name
%        - single file will be read
%      single name containing wildcard
%        - each matching file will be read and processed separately
%      single name containing wildcard + string replacement pattern
%        - will read the maps replace the following string in key value manner
%          the filename coming from the replacement will be used to form cross spectra
%          dimension 1 and auto&cross spectra will then be taken
%          amongst these. e.g. mapname={'B2map*','B2','Keck'}
%          to do all B2map1 x Keckmap1 etc..
%          the string replacement pattern can have more than 2 entries to generate
%          more complicated file names.
%      single name containing wildcard + one or more cell arrays of string replacement patterns.
%        - Each cell array holds a string replacement pattern to form a map that
%          goes into the cross. E.g.:
%          {'B2map*',{'B2','Keck'}} = {'B2map*','B2','Keck'} - as above
%          {'B2map*',{'B2','Keck'},{'B2','B1'}} - all cross spectra 
%                                                 combinations between b2,keck and b1
%
% apsopt. ... :
%
%  pure_b ='normal' (default) computes power spectrum as usual 
%         ='kendrick' will use Kendrick Smith pure B estimator
%
%  purifmatname = cell containing filenames of projection matrix. These will be
%  loaded once at run start and then applied to each map to make
%  special B-mode only maps. 
%  If you have more than one map, then the cells of purifimatname will
%  be used to purify the maps. The empty or non-existent
%  elements in purifimatname cell will use what is specified in
%  pure_b as the estimator.
%  If purifmatname is a simple string then the same purifmat will
%  be used for all maps  
%
%  ie:
%  apsopt.purifmatname={'matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat',[],'matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_Rx1_proj.mat'};
%
%  update = 0 (default) will re-make any aps which already exist.
%         = 1 only new aps will be made
%
%  howtojack = 'dim2' (default) diff along 2nd dim of map array
%            = 'none' does not jackknife and makes spectra from each map
%            = 'dim1' diff along 1st dim of map array
%
%  ukpervolt = define what ukpervolt to use for aps.
%            = {get_ukpervolt()} (default. only works with 1 map case)
%            = can also be 'none' if maps are in uK already
%              Needs to be in cell format with one entry per map - this value isn't given as a default.   
%              i.e. {3150,3400};
% 
%  noisemask is fourier plan noise masking. Need to specify a file
%  
%  hostdir: only to be used for cross spectra, allows to specify the path to the maps dir,
%            i.e. {'~/bicep2_analysis','~/keck_analysis'}
%
%  commonmask makes the Tw and Pw ap masks the same across all maps:
%          = false (0) means do nothing
%          = integer N means copy map(N) masks to all
%          = 'gmean' means copy geometric mean of all to each
%  
%  commonmasksel = allows to make a subselection of the maps
%                  from which the common mask is created from
%
%  coaddrx = 0 (default) - do nothing
%          = 1 coadds over rx (or rather, dim 1).
%          = 2 coadds over freqs  
% 
%  overall = 0 (default) - do nothing
%          = 1 coadds multiple maps together after abscalling them seperately
%          = {[1,2],3} will coadd maps 1 and 2 together 
%
%  save_coaddopts = 1 (default)
%                 = 0 don't save coaddopts for sim rlzs>1
%  
%  random_order = 1 (default) loop through the maps found with the wildcard input
%                 in random order
%
%  bintype = specifies the ell space binning (arg to get_bins)
%
%  makebpwf = 0 (default) do nothing
%           = 1 make the bandpower window function and save them
%
%  scalefac - each cell element of scalefac is applied to the
%  corresponding cell element of ac before they are added in
%  make_map (reduc_combcomap places noise and signal maps into cell
%  elements of ac for type7 and type9). Separate scalings are
%  allowed (required) for each map. So when coadding over rx:
%  apsopt.scalefac={[1,1],[1,1],[1,1]*sqrt(3)}
%  will scale up the B-modes to r=0.3 and when not coadding over rx:
%  apsopt.scalefac={[1,1,3,1,1,1],[1,1,1,1,1,1],[1,1,1,1,1,1]*sqrt(3)};
%  will also scale up the rx1 noise by factor 3
%
%  pure_e = 1 or 0 (default): makes purified Emodes in parallel to 
%           purified Bmodes.
%
% e.g.
%
% make aps for all noise only sims in batch sim12 whether or not they
% exist already
% reduc_makeaps('sim12???4_*_jack?.mat')
%
% fill in any missing aps in the first 200
% apsopt.update=1
% reduc_makeaps('sim12[0-1]??4_*_jack?.mat',apsopt)
%
% generate expt jack spec
% apsopt.howtojack='dim2'
% reduc_makeaps({'B2map','Keckmap',[],apsopt)
%
% generate per chan sum/diff/x spec
% reduc_makeaps('sim13_*_jack03_fd.mat')
%
% gen separate spectra for each jack split - used for abscal studies(?)
% apsopt.howtojack='none'
% reduc_makeaps('sim1200?8_*_jack[1-4].mat',apsopt)
%
% use f plane weighting
% apsopt.noisemask = 'sim13xxx4_filtn_weight2_jack0.mat'
% reduc_makeaps('sim130014_filtn_weight2_jack0.mat',apsopt)
%
% make auto/cross spectra between B2 and Keck-all (3 fold)
% apsopt.coaddrx=1;
% reduc_makeaps({'1450/real_a_filtp3_weight3_gs_dp1100_jack0.mat','1450','1350','.mat','1.mat'},apsopt)
%
% make auto/cross spectra between B2 and Keck per rx (21 fold)
% apsopt.coaddrx=0;
% reduc_makeaps({'1450/real_a_filtp3_weight3_gs_dp1111_jack0.mat','1450','1350','.mat','1.mat'},apsopt)

if(~exist('apsopt','var'))
  apsopt=[];
end

apsopt = get_default_apsopt(apsopt);
 
if ~iscell(mapname)
  mapname={mapname};
end

% kludge to allow to not keep recalc index pointers in
% calcspec below - make sure it's cleared here from any
% previous call to calcspec
clear global calcspec_ind

% this is the version which concatenates maps along the first dimension
% and uses the string replacement pattern
% it also works if just one map is handed in
% first fetch the maps according to the wildcard:
[s,d]=system_safe(sprintf('/bin/ls -1 maps/%s',mapname{1}));
% for some bizarre reason d now contains non-printing nonsense
d=strip_nonsense(d);
maps1=strread(d,'%s');
nmaps=length(maps1);

% process the maps in random order. Useful to farm out the same
% job serveral times to work through the input maps more rapidly.
% only makes sense with apsopt.update=1 though
if apsopt.random_order
  [err,rn] = system_safe('bash -c ''echo $RANDOM''');
  RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)+str2num(rn)));
  maps1 = maps1(randperm(nmaps));
end

% this makes it backward compatible. If second part in mapname is not
% a cell, make it a cell: this indicates that just two maps are being read.
if length(mapname)>1 & ~iscell(mapname{2})
  mapname={mapname{1},{mapname{2:end}}};
end

% expand the hostdirs in the case just one hostdir is handed in:
if size(apsopt.hostdir,2)==1
  for jj = 2:length(mapname)
    apsopt.hostdir{end+1}=apsopt.hostdir{1};
  end
end

% loop through these maps...:
is_first_pass = 1;
for ii=1:nmaps
  % get the file name for the maps which are going to be crossed
  clear cmaps;
  cmaps{1} = maps1{ii};

  % do string replacement to create additional filenames:
  % each cell array in mapname is one more filename.
  % in the cell array has the string replacement pattern:
  % the first string is replaced with the second string, 
  % the third with the forth and so on.
  % {'B2map*',{'B2','Keck','jack0','jack01'},{'B2','B1'},...}.
  % loop over the cells in mapname
  for jj = 2:length(mapname)
    % the initial map name is going to be tweaked to produce the other map names:
    map_next = cmaps{1};
    % loop through the replacement in key/values manner
    for kk = 1:2:length(mapname{jj})-1
      map_next = regexprep(map_next,mapname{jj}{kk},mapname{jj}{kk+1},'once');
    end
    cmaps{end+1} = map_next;
  end

  % check if the output file which would be created already exists
  apsfile=get_apsfilename(apsopt,cmaps);
  if apsopt.update && exist(apsfile,'file')
    disp(['Skipping over existing file: ' apsfile])
    continue
  end

  % prepare the maps:
  [mapstruct,apsopt,err] = aps_prepare_maps(cmaps,apsopt);

  % some verbosity
  apsopt
  
  % one of the maps to be crossed was missing
  if ~isnumeric(err) || err==1
    if isnumeric(err)
      warning(['Failed to load map: ',cmaps{:}])
    else
      warning(err.message)
    end
    continue; 
  end
  
  % load the purification matrix only once and
  % behind the check for the existance of the file
  % this allows to loop over existing aps file quickly
  % and only load the matrix if there are really gaps
  if is_first_pass
    % load the purification matrix according to apsopt.purifmatname,
    % the logic in the function avoids reloading duplicate
    % matrices.
    apsopt = aps_load_purifmat(apsopt);
    is_first_pass = 0;
  end
  
  % if requested make the corresponding bpwf and save them
  if(apsopt.makebpwf)
    bpwf=reduc_bpwf(mapstruct);
    saveandtest(strrep(apsfile,'.mat','_bpwf.mat'),'bpwf');
  end

  % finally make the auto and cross aps
  use_base(mapstruct,apsopt,apsfile)
end

% clear ind array
clear global calcspec_ind

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function use_base(mapstruct,apsopt,apsfile)

% get the co-added maps
map = mapstruct.map;
m = mapstruct.m;
coaddopt=mapstruct.coaddopt;

% if the map is real, get rid of coaddopt.c as it slows things way down
if isfield(coaddopt,'c')
  coaddopt=rmfield(coaddopt,'c');
end

switch coaddopt.jacktype
 case 'f'
  do_cross=0;
 otherwise
  do_cross=1;
end
  
% apply matrix purification to make Q/U maps containing only
% B-modes
if(isfield(apsopt,'purifmat'))
  for i=1:size(map,1)    
    if i <= size(apsopt.purifmat,2) & ~isempty(apsopt.purifmat{i}) 
      for j=1:size(map,2)
        display(['do projection ',num2str(i),' ',num2str(j)])
        [mapE,mapB]=do_projection(map(i,j),apsopt.purifmat{i}, apsopt.pure_e);
        map(i,j).QprojB=mapB.Q; 
        map(i,j).UprojB=mapB.U;
        if apsopt.pure_e
          map(i,j).QprojE=mapE.Q; 
          map(i,j).UprojE=mapE.U;
        end
      end
    end
  end
end

% pad the maps
if(1)
  disp('pad')
  p=2^nextpow2(max([m.nx,m.ny]));
  [m,map]=pad_map(m,map,p);
end

% calc axis data
ad=calc_ad2([m.xdos,m.ydos],[m.nx,m.ny]);

% prepare f plane weight mask
if(isfield(apsopt,'noisemask'))
  n=load(sprintf('aps2d/%s',apsopt.noisemask)); n=n.aps2d;
  
  % the noise sims tell us which modes are noisier than usual
  % within each annular bin while the signal sims tell us which
  % modes are filtered out within each annular bin
  % formally for TT w=s/(s+n) but we want to use a mask
  % constructed from TT to down-weight potentially contaminated
  % modes in other spectra that the noise model is failing to tell
  % use are contaminated there.
  % So we don't care about the relative strength of the 
  
  % the 2d noise spectra
  n100=n(1).Cs_l(:,:,1);
  n150=n(2).Cs_l(:,:,1);
  
  % kill the region where noise is small because it has been
  % poly filtered out
  [x,y]=meshgrid(n(1).ad.u_val{1}*2*pi);
  n100(abs(x)<120)=NaN;
  n150(abs(x)<120)=NaN;

  % weight masks are inverse noise
  w{1}=1./n100;
  w{2}=1./n150;
  w{3}=1./n150;
  %w{3}=min(w{1},w{2});
  
  if(0)
    % normalize to the max in each annulus
    % - this is not necessary for weighting but nice for
    % visualization
    % regular bins
    [be,n]=get_bins('bicep_norm')
    w{1}=norm_to_annular_max(be,n(1).ad.l_r,w{1});
    w{2}=norm_to_annular_max(be,n(2).ad.l_r,w{2});
    w{3}=norm_to_annular_max(be,n(2).ad.l_r,w{3});
      
    % be more agressive and kill outright dodgy modes
    %w{1}=w{1}>0.5;
    %w{2}=w{2}>0.5;
    %w{3}=w{3}>0.5;
    
    figure(1)
    setwinsize(gcf,1000,400);
    xtic=aps2d(1).ad.u_val{1}*2*pi;
    ytic=aps2d(1).ad.u_val{2}*2*pi;
    
    subplot(1,2,1); imagesc(xtic,ytic,w{1});
    xlim([-2500,2500]); ylim([-2500,2500]); axis square; colorbar
    bullseye; title('100 GHz TT noise mask')
    subplot(1,2,2); imagesc(xtic,ytic,w{2});
    xlim([-2500,2500]); ylim([-2500,2500]); axis square; colorbar
    bullseye; title('150 GHz TT noise mask')
    %subplot(1,3,3); imagesc(xtic,ytic,w{3});
    %xlim([-2500,2500]); ylim([-2500,2500]); colorbar
    drawnow
  end
else
  for j=1:size(map,1)
    w{j}=ones(size(map(1).Tw));
  end
  if(size(map,1)==2)
    w{3}=ones(size(map(1).Tw));
  end
end

% normally size over 2nd dim is 1 but we keep the option to make
% separate spectra for each jack split - used in abscal
for k=1:size(map,2)

  % Use coaddtype to determine how to treat the rows of the map array
  switch coaddopt.coaddtype      
   
   case {0,1,5,4,2} % normal, per rx, per tile, per channel
     % NB the code below now does all possible cross spectra -
     % it is unlikely we want this for per tile and per channel
     
     % calc spectra for each row of map array
     disp('calcspec')
     for j=1:size(map,1)

       % do TT
       mw=map(j,k).Tw;
       ft=calc_map_fts(map(j,k),ad,mw,1,apsopt.pure_b); % calc the weighted modes using map weight mw
       [aps(j,k).l,aps(j,k).Cs_l(:,1)]=calcspec(ad,ft.T,ft.T,w{j},apsopt.bintype);
       
       % do PP (EE, BB, EB)
       mw=map(j,k).Pw;
       ft=calc_map_fts(map(j,k),ad,mw,2,apsopt.pure_b);
       [aps(j,k).l,aps(j,k).Cs_l(:,3)]=calcspec(ad,ft.E,ft.E,w{j},apsopt.bintype);
       [aps(j,k).l,aps(j,k).Cs_l(:,4)]=calcspec(ad,ft.B,ft.B,w{j},apsopt.bintype);
       [aps(j,k).l,aps(j,k).Cs_l(:,6)]=calcspec(ad,ft.E,ft.B,w{j},apsopt.bintype);
       
       % do TP (TE, TB)
       mw=gmean(map(j,k).Tw,map(j,k).Pw);
       ft=calc_map_fts(map(j,k),ad,mw,[1,2],apsopt.pure_b);
       [aps(j,k).l,aps(j,k).Cs_l(:,2)]=calcspec(ad,ft.T,ft.E,w{j},apsopt.bintype);
       [aps(j,k).l,aps(j,k).Cs_l(:,5)]=calcspec(ad,ft.T,ft.B,w{j},apsopt.bintype);
     end
     
     if do_cross
      % append the inter row cross cross spectra in additional rows of aps array
      aps_offset = size(map,1);
      n = 1;
      for j=1:size(map,1)-1
        for c=j+1:size(map,1)
          
          % do TT
          % make a common mask as gmean of two masks
          mw=gmean(map(j,k).Tw,map(c,k).Tw); 
          % mult each map by common mask and ft
          ft1=calc_map_fts(map(j,k),ad,mw,1,apsopt.pure_b);
          ft2=calc_map_fts(map(c,k),ad,mw,1,apsopt.pure_b);
          % take the products of the two sets of modes and bin to 1d spectra
          [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,1)]=calcspec(ad,ft1.T,ft2.T,w{j},apsopt.bintype);
          
          % do PP
          mw=gmean(map(j,k).Pw,map(c,k).Pw);
          ft1=calc_map_fts(map(j,k),ad,mw,2,apsopt.pure_b);
          ft2=calc_map_fts(map(c,k),ad,mw,2,apsopt.pure_b);
          [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,3)]=calcspec(ad,ft1.E,ft2.E,w{j},apsopt.bintype);
          [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,4)]=calcspec(ad,ft1.B,ft2.B,w{j},apsopt.bintype);
          [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,6)]=calcspec(ad,ft1.E,ft2.B,w{j},apsopt.bintype);
          [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,9)]=calcspec(ad,ft1.B,ft2.E,w{j},apsopt.bintype);
          
          % do TP
          mw=gmean(map(j,k).Tw,map(c,k).Pw); 
          ft1=calc_map_fts(map(j,k),ad,mw,1,apsopt.pure_b);
          ft2=calc_map_fts(map(c,k),ad,mw,2,apsopt.pure_b);
          [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,2)]=calcspec(ad,ft1.T,ft2.E,w{j},apsopt.bintype);
          [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,5)]=calcspec(ad,ft1.T,ft2.B,w{j},apsopt.bintype);
          
          % do PT
          mw=gmean(map(j,k).Pw,map(c,k).Tw);
          ft1=calc_map_fts(map(j,k),ad,mw,2,apsopt.pure_b);
          ft2=calc_map_fts(map(c,k),ad,mw,1,apsopt.pure_b);
          [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,7)]=calcspec(ad,ft1.E,ft2.T,w{j},apsopt.bintype);
          [aps(aps_offset+n,k).l,aps(aps_offset+n,k).Cs_l(:,8)]=calcspec(ad,ft1.B,ft2.T,w{j},apsopt.bintype);
          
          % when doing the cross spectra save the weight for each of the cross partners for T E and B
          % NB what are these for? Are they now correct given
          % common mask change above?
          %aps(aps_offset+n,k).w(:,1)=[map(j,k).Tw,map(c,k).Tw];
          %aps(aps_offset+n,k).w(:,2)=[map(j,k).Tw,map(c,k).Ew];
          %aps(aps_offset+n,k).w(:,3)=[map(j,k).Ew,map(c,k).Ew];
          %aps(aps_offset+n,k).w(:,4)=[map(j,k).Bw,map(c,k).Bw];
          %aps(aps_offset+n,k).w(:,5)=[map(j,k).Tw,map(c,k).Bw];
          %aps(aps_offset+n,k).w(:,6)=[map(j,k).Ew,map(c,k).Bw];
          %aps(aps_offset+n,k).w(:,7)=[map(j,k).Ew,map(c,k).Tw];
          %aps(aps_offset+n,k).w(:,8)=[map(j,k).Bw,map(c,k).Tw];
          %aps(aps_offset+n,k).w(:,9)=[map(j,k).Bw,map(c,k).Ew];
          
          % increment to next slot
          n=n+1;
        end
      end
    end % do_cross
   
   case 3 % per pair sum/diff     
     disp('check this untested code!')
     for j=1:size(map,1)
       mw=map(j,k).Tw; 
       ft=calc_map_fts(map(j,k),ad,mw,3);
       [aps(j,k).l,aps(j,k).Cs_l(:,1)]=calcspec(ad,ft.Tsum,ft.Tsum,w{j},apsopt.bintype);
       
       mw=map(j,k).Pw; 
       ft=calc_map_fts(map(j,k),ad,mw,3);
       [aps(j,k).l,aps(j,k).Cs_l(:,2)]=calcspec(ad,ft.Tdif,ft.Tdif,w{j},apsopt.bintype);
       
       mw=gmean(map(j,k).Tw,map(j,k).Pw);
       ft=calc_map_fts(map(j,k),ad,mw,3);
       [aps(j,k).l,aps(j,k).Cs_l(:,3)]=calcspec(ad,ft.Tsum,ft.Tdif,w{j},apsopt.bintype);
     end      
  
  end %switch coaddopt.coaddtype

end % for k=1:size(map,2)

% put aps in a simset-specific subdir. Make sure it's there first:
apsdir=fileparts(apsfile);

% if output directory does not exist create
if ~isdir(apsdir)
  system(['mkdir ' fileparts(apsdir)])
  system(['mkdir ' apsdir])
end

% remove the massive purification matrix from apsopt before saving
if(isfield(apsopt,'purifmat'))
  apsopt=rmfield(apsopt,'purifmat'); 
end

% save the spectra
fname=apsfile;
[dirname,aname] = fileparts(fname);
if ~exist(dirname)
  mkdir(dirname)
end
disp(['Saving to file: ',fname]);
if apsopt.save_coaddopts | strcmp(aname(1:3),'001') | strcmp(aname(1:4),'real')
  saveandtest(fname,'aps','coaddopt','apsopt');
else
  saveandtest(fname,'aps','apsopt');
end
setpermissions(fname);

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y=norm_to_annular_max(be,l,x)

y=zeros(size(x));

for i=1:length(be)-1
  % find modes in annulus
  ind=l>be(i)&l<be(i+1);
      
  % divide modes in bin by median and enter into normalized array
  y(ind)=x(ind)/max(x(ind));
end

return


%%%%%%%%%%%%%%%%%%%
function bullseye()
xl=xlim;
hold on
[x,y]=circle(0,0,[10,500:500:xl(2)],100,1);
plot(x,y,'r');
%line(xl,[0,0],'Color','r');
%line([0,0],xl,'Color','r');
hold off

return
