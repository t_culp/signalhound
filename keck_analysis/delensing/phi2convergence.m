function phi2convergence(map_in,map_out,phimap_out,nside,lmax)
% phi2convergence(phi_mapname,kappa_mapname,phi_mapname_out,nside,lmax)
%
% Call anfast and synfast to get 1st derivs of phi map specified in phi_mapname, then
% create convergence (kappa) map from these and write back out to a fits file. The
% output map is simul_type=6, which contains T, Q, U and first and second
% derivatives. This is because the pipeline interpolation functions always expects the
% maps in this form, even though Q and U here are meaningless.
%
% If phi_mapname_out is specified, wirite the phi map to disk with its derivatives as a
% full sky simul_type=6.
%
% If kappa_mapname is empty, write the phi map as a cut sky simul_type=6 and no kappa
% map. This is much faster.
%
% Ex.:
%
% phi_map =     'input_maps/lensing_potentials/camb_planck2013_r0/phi_r0001.fits';
% kappa_map =   'input_maps/lensing_convergences/camb_planck2013_r0/kappa_r0001.fits';
% phi_map_out = 'input_maps/lensing_convergences/camb_planck2013_r0/phi_r0001_simul6.fits';
%
% phi2convergence(phi_map,kappa_map,phi_map_out,512,1024);
%

if ~exist('phimap_out') || isempty(phimap_out)
  phimap_out=[];
end

try
  h=get_hmap_properties(map_in);
end

if ~exist('nside') || isempty(nside)
  nside=h.nside;
end

if ~exist('lmax') || isempty(lmax)
  lmax=nside*2;
end

% Call anafast to get a_lms of phi map
stamp = gen_stamp();
tmp_alms = ['synfast_tmp/phialms_' stamp '.fits'];
if ~exist('phimap_out','var')
  tmp_map = ['synfast_tmp/phimap_' stamp '.fits'];
else
  tmp_map = phimap_out;
end
run_anafast(map_in,tmp_alms,lmax,1);

if isempty(map_out)
  cut.theta1=-70; cut.theta2=-45;cut.phi1=-55; cut.phi2=55;
else
  cut=[];
end

% Call synfast to get phi map with derivatives
run_synfast(0,[],tmp_alms,nside,tmp_map,[],cut,0,lmax,6);

% Return if the phi map is cut sky, as getting alms from a cut sky map is impossible
% with anafast
if ~isempty(cut)
  system_safe(['rm -f ' tmp_alms]);
  return
end

% Read in phi map plus its derivs and create convergence map
hmap=read_fits_map(tmp_map);

% Convergence is d^2 phi / d theta^2 + d^2 phi / d phi^2 (healpix longitude "phi" and
% lensing potential "phi" unfortunately share the same greek letter)
hmap_out.type={'CONVERGENCE'};
hmap_out.units={'UNKNOWN'};
hmap_out.coordsys=h.coordsys;
hmap_out.nside=nside;
hmap_out.ordering=hmap.ordering;
hmap_out.fwhm=0;
hmap_out.polcconv='UNKNOWN';
hmap_out.pixel=hmap.pixel;
hmap_out.map=-(hmap.map(:,10)+hmap.map(:,16))/2;

% Write map to disk
tmp_kappamap = ['synfast_tmp/kappamap_' stamp '.fits'];
write_fits_map(tmp_kappamap,hmap_out);

% Now get alms of the kappa map and write back with it and its first and second derivatives
tmp_kappaalms = ['synfast_tmp/kappaalms_' stamp '.fits'];
run_anafast(tmp_kappamap,tmp_kappaalms,lmax,1);

run_synfast(0,[],tmp_kappaalms,nside,map_out,[],0,lmax,6);

% Delete temporary files
system_safe(['rm -f ' tmp_alms]);

if ~exist('phimap_out','var')
  system_safe(['rm -f ' tmp_map]);
else
  system_safe(['mv ' tmp_map ' ' phimap_out]);
end

system_safe(['rm -f ' tmp_kappaalms]);
system_safe(['rm -f ' tmp_kappamap]);

return



