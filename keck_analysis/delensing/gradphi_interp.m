function [dphidphi,dphidtheta]=gradphi_interp(hmap,x,y)
% [dphidphi,dphidtheta]=gradphi_interp(hmap,ra,dec)
% Return grad phi on ra/dec pixel centers

% Trick healpix_interp into moving the 1st derivs around on the sky using the second
% derivs
dmap=hmap;
dmap=rmfield(dmap,'map');
dmap.map(:,1)=hmap.map(:,4); % dTdth -> T
dmap.map(:,4)=hmap.map(:,10); % dTdth2 -> dTdth
dmap.map(:,7)=hmap.map(:,13); %  dTdthdph -> dTdph
dphidtheta=healpix_interp(dmap,x(:),y(:),'taylor1T');

dmap.map(:,1)=hmap.map(:,7); % dTdph -> T
dmap.map(:,7)=hmap.map(:,16); % dTdph2 -> dTdph
dmap.map(:,4)=hmap.map(:,13); %  dTdphdth -> dTdth
dphidphi=healpix_interp(dmap,x(:),y(:),'taylor1T');

dphidtheta=reshape(dphidtheta,size(x));
dphidphi=reshape(dphidphi,size(x));

return

