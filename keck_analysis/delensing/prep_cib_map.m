function prep_cib_map(rlz,update,applywiener,indir,prefix)
% prep_cib_map(rlz,update,applywiener,indir,prefix)
% 
% rlz = sim realization
% update = 0 (default) only compute new cib map if it does not exist already
%        = 1 always compute new cib map
% applywiener = true (default) apply Wiener filter in the alm->phi step. 
%             = 1 do not apply
% indir = string of input cib map directory
% prefix = cib map filename prefix (default 'cib'), filename will be
%          sprintf('%s/%s_r%04d.fits',indir,prefix,rlz) 
%
% Outfile is always phi_from_cib_rxxxx.fits in a directory cib_withderivs/indir,
% regardless of prefix.

if ~exist('update','var') || isempty(update)
  update=false;
end

if ~exist('applywiener') || isempty(applywiener)
  applywiener=true;
end

if ~exist('indir') || isempty(indir)
  %indir  = '/n/bicepfs1/bicep2/pipeline/input_maps/cib/camb_planck2013_r0_20150701/';
  indir = 'input_maps/cib/camb_planck2013_r0_20160418_SN/';
end

if ~exist('prefix') || isempty(prefix)
  prefix='cib';
end

% Directories
outdir = strrep(indir,'cib','cib_withderivs');
almindir = strrep(indir,'input_maps','input_maps/alms');
almoutdir = strrep(outdir,'input_maps','input_maps/alms');

if ~exist(indir,'dir');
  system(['mkdir ' indir]);
end
if ~exist(outdir,'dir');
  system(['mkdir ' outdir]);
end
if ~exist(almindir,'dir');
  system(['mkdir ' almindir]);
end
if ~exist(almoutdir,'dir');
  system(['mkdir ' almoutdir]);
end

% Filenames
infile  = sprintf('%s_r%04d.fits',prefix,rlz);
outfile = sprintf('phi_from_cib_r%04d.fits',rlz);

% Full paths
map_in  = fullfile(indir,infile);
map_out = fullfile(outdir,outfile);
alm_in  = fullfile(almindir,infile);
alm_out = fullfile(almoutdir,outfile);

% Return if output map already exists
if ~update & exist(map_out,'file')
  disp(sprintf('%s already exists, returning',map_out));
  return
end

% Get input map properties
h=get_hmap_properties(map_in);
nside=h.nside;
lmax=nside*2;

% Call anafast to get a_lms of cib map
run_anafast(map_in,alm_in,lmax,1);

% CIB Wiener filter fits file (1/g to go from T-> phi where T = g phi)
phi_wiener = 'phimap_gain_plus_wiener_bl.fits';

% Synfast is absolutely lame when it comes to units. Someone should strangle the
% authors. There is simply no way to make it convert between alms and maps without
% attempting unit conversions. Therefore, this kludge is necessary. I am also quite
% confused over where the 1e12 comes from. 1e6 I could see (uK vs. K).
stamp=gen_stamp();
phi_wiener_temp=['synfast_tmp/' 'phimap_gain_plus_wiener_bl_temp_' stamp '.fits'];
x=fitsread(phi_wiener,'BinTable');
info=fitsinfo(phi_wiener);
if applywiener
  % Wiener + unit conversion
  x{1}=x{1}*1e12;
else
  % Just unit conversion (so lame)
  x{1}=ones(size(x{1})*1e12);
end
fitswrite_bintable(x,info,phi_wiener_temp);

% Write fullsky map, save alms, then delete map
cut=[];
map_out_fullsky=strrep(map_out,'.fits','_fullsky.fits');
run_synfast(0,[],alm_in,nside,map_out_fullsky,[],cut,phi_wiener_temp,lmax,1);
run_anafast(map_out_fullsky,alm_out,lmax,1);

% Write cut sky map with derivs
cut.theta1=-70; cut.theta2=-45;cut.phi1=-55; cut.phi2=55
run_synfast(0,[],alm_in,nside,map_out,[],cut,phi_wiener_temp,lmax,6);

% Remove temporary 
system(['rm -f ' phi_wiener_temp]);

return


