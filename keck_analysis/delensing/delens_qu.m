function map=delens_qu(m,map,gradphihmap,nrecurse,interptype)
% map=delens_qu(m,map,gradphihmap,nrecurse,interptype)
%
% m,map = BK map variabies
% gradphimap = healpix map variable (loaded in with read_fits_map) of simultype=6 synfast map
%              with phi in the T fields. Q/U and derivative map fields are not used.
% nrecurse = For perfect delensing, grad_phi should be evaluated at the unlensed pixel
%            centers. Therefore, the deflection field is calculated iteratively. First
%            calculate using grad_phi at the pixel centers, then again by interpolating
%            grad_phi to the deflected locations, etc. More than one recursion appears
%            to do very little, so default = 1.
% interptype = interpolation type used to interpolate the BK map to the deflected
%              locations. This does not effect the interpolatio of grad_phi, which,
%              being a healpix map, uses second order Taylor interpolation

if ~exist('nrecurse','var') || isempty(nrecurse)
  nrecurse=1;
end

if ~exist('interptype','var') || isempty(interptype)
  interptype='linear';
end

% RA/Dec
[x,y]=meshgrid(m.x_tic,m.y_tic);

dx=0;dy=0;

for k=1:(nrecurse+1)

  % Get grad phi
  [dphidphi,dphidtheta]=gradphi_interp(gradphihmap,x-dx,y-dy);

  % negative of deflection in RA
  dx=((dphidphi./cosd(y))*180/pi);

  % negative of deflection in Dec
  dy=-dphidtheta*180/pi;

end

% Interpolate to undo lensing
map.T=interp2(x,y,map.T,x-dx,y-dy,interptype);
map.Q=interp2(x,y,map.Q,x-dx,y-dy,interptype);
map.U=interp2(x,y,map.U,x-dx,y-dy,interptype);


return






