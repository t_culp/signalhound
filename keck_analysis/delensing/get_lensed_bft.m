function bft=get_lensed_bft(ad,E,kappaft,max_ell)
% bft = get_lensed_bft(ad,Eft,kappaft,max_ell)

lx0=ad.u_val{1}*2*pi;
ly0=ad.u_val{2}*2*pi;
[lxx,lyy]=meshgrid(lx0,ly0);

lx_res=lx0(2)-lx0(1);
ly_res=ly0(2)-ly0(1);

lp=ad.u_r*2*pi;

% Angle in the Fourier plane
chi=atan2(lyy,lxx);

% But the l=0 center of the Fourier plane isn't a center pixel unless the array has an
% odd number, so the flipping above uncenters. Center kappa w.r.t. E. (This doesn't
% matter! Since kappa is real, kappaft is symmetric and doing this makes no difference!)
k=flipkappa(kappaft,lp);

% Size of array
sz=size(E);
szx=sz(2);
szy=sz(1);

% Declare array
bft=zeros(size(E));

% What ells do we delens?
[kxx,kyy]=meshgrid(1:ad.N_pix(1),1:ad.N_pix(2));
doind=find(lp<=max_ell);

% Loop over every mode and calculate the lensing B-mode
for j=1:numel(doind)

  if mod(j,1000)==0
    disp(sprintf('mode %d of %d',j,numel(doind)));
  end
  
  kx=kxx(doind(j));
  ky=kyy(doind(j));
  
  % Calculate convolution weight mask
  lx=lxx(ky,kx);
  ly=lyy(ky,kx);
  
  dlx=lx-lxx;
  dly=ly-lyy;
  
  dchi=chi(ky,kx)-chi;
  
  lp_dot_dl = lxx.*dlx + lyy.*dly;
  abs_dl_sq=dlx.^2+dly.^2;
  sin2dchi=sin(2*dchi);
  
  W=(2*lp_dot_dl./abs_dl_sq).*sin2dchi;
  W(isnan(W))=0;
  
  %imagesc(lx0,ly0,W);colorbar;axis square;
  
  % Shift size in pixels
  shift_x=round(lx/lx_res);
  shift_y=round(ly/ly_res);
  
  sxk=max(1,1-shift_x);
  exk=min(szx,szx-shift_x);
  syk=max(1,1-shift_y);
  eyk=min(szy,szy-shift_y);
  
  sxwe=max(1,1+shift_x);
  exwe=min(szx,szx+shift_x);
  sywe=max(1,1+shift_y);
  eywe=min(szy,szy+shift_y);
  
  if 0
    if shift_x==-20 & shift_y==-20
      
      clf
      subplot(1,3,1)
      imagesc(lxx(1,sxwe:exwe),lyy(sywe:eywe,1),W(sywe:eywe,sxwe:exwe));
      axis square;xlabel('lx''');ylabel('ly''');title('W(l,l'')');colorbar;
      
      subplot(1,3,2)
      imagesc(lxx(1,sxwe:exwe),lyy(sywe:eywe,1),abs(E(sywe:eywe,sxwe:exwe)));
      axis square;xlabel('lx''');ylabel('ly''');title('abs[E(l'')]');colorbar;
      
      subplot(1,3,3)
      imagesc(lxx(1,sxk:exk),lyy(syk:eyk,1),abs(k(syk:eyk,sxk:exk)));
      axis square;xlabel('lx''');ylabel('ly''');title('abs[\kappa(l''-l)]');colorbar;
      
      colormap hot
      gtitle(sprintf('shifted Fourier planes and weighting for l=(lx,ly)=(%0.1f,%0.1f)',lx,ly));
      
      keyboard
    end
  end
  
  subarr=W(sywe:eywe,sxwe:exwe).*E(sywe:eywe,sxwe:exwe).*k(syk:eyk,sxk:exk);
  bft(ky,kx)=sum(subarr(:));

end

% Normalize
c=lx_res*ly_res/(2*pi)^2;
bft=c*bft;

%% This is an empirical kludge for now. Where is this coming from?
%bft=bft/2;

return


%%%%%%%%%%%%%%%%%%%%%%%
function k=flipkappa(k,lp)

% Convolution is shift-and-add with a flipped kernel
k=flipud(fliplr(k));

% Flipping is great, but now the two arrays are decentered for even element
% arrays. Recenter them.

% Flipped ell array
lp_flip=flipud(fliplr(lp));

% Zero point value of Fourier plane
lpmin=nanmin(lp(:));

% Center of original ell array
[yc,xc]=ind2sub(size(lp),find(lp==lpmin));

% Center of flipped ell array
[yc_flip,xc_flip]=ind2sub(size(lp),find(lp_flip==lpmin));

% Shift size
shift_x=xc_flip-xc;
shift_y=yc_flip-yc;

% De-shift
k=zeroshift(k,[-shift_y,-shift_x]);

return





