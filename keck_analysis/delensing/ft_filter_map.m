function map=ft_filter_map(map,m,lmax,beam,fpw,inv,lmin)
% map=ft_filter_map(map,m,lmax,beam,fpw,mapw,inv)
% 
% lmax = lmax or [lmin,lmax], if former, assume lmin=0

if numel(lmax)==1
  lrange=[0,lmax];
else
  lrange=lmax;
end

if ~exist('beam','var')
  beam=1;
end

if ~exist('fpw','var')
  fpw=1;
end

if ~exist('inv','var')
  inv=true;
end

ad=calc_ad2([m.xdos,m.ydos],[m.nx,m.ny]);

map.T=ft_filter_sub(map.T,ad,lrange,beam,fpw,inv);
if isfield(map,'Q')
  map.Q=ft_filter_sub(map.Q,ad,lrange,beam,fpw,inv);
  map.U=ft_filter_sub(map.U,ad,lrange,beam,fpw,inv);
end

return

%%%%%%%%%%%%%%%
function im_out=ft_filter_sub(im,ad,lrange,beam,fpw,inv)

im(~isfinite(im))=0;
lr=ad.u_r*2*pi;

% Compute FT
ft=i2f(ad,im);

% Beam
if ~isempty(beam)
  [dum,bl]=get_bl(beam,lr(:));
  bl=reshape(bl,size(im));
end

% Filter
if iscell(fpw)
  sflr=fpw{1}.u_r*2*pi;
  [x,ind]=unique(sflr(:));
  y=fpw{2}(ind);
  sf=reshape(interp1(x,y,lr(:)),size(im));
else
  sf=fpw;
end

% Invert the suppression (as opposed to apply it)
if inv
  bl=1./bl;
  sf=1./sf;
end

% Apply the filter
ft=ft.*bl.*sf;

% No NaNs.
ft(isnan(ft))=0;

% Truncate in ell
w=ones(size(lr));
w(lr>lrange(2))=0;
w(lr<lrange(1))=0;
w=conv2(w,ones(30,30)/30^2,'same');
ft=ft.*w;

% FT back
im_out=f2i(ad,ft);

return

