function [map_template,m]=gen_lt(rlz,type,sernum,cmbtype,phitype,dosave)
% function [map_template,m] = gen_lt(rlz,type,sernum,cmbtype,phitype,dosave)
% 
% Generate lensing template and optionally save 
%
% rlz = sim rlz
% type = 5,6,7 (CMB sim type, s, n or s+n)
% sernum = XXYY in maps/XXYY/ folder in which template is saved
% cmbtype = 'bk' latest BICEP/Keck 150 GHz s+n sim (default)
%         = 'ideal' unfiltered, noiseless sim input map
%         = 'sptpol' SPTpol s+n sim
%         = 'spt3G'  SPTpol s+n sim with noise component scaled down
%
% phitype = 'pl545' (default) constrained CIB realizations based on observed level of
%                   correlation of Pl545 map with Planck's phi map
%         = 'ideal' use simset's ideal phi map
%         = 'spt' (doesn't work yet)
%
% dosave = false (default)
%          true, save in maps/lt

if ~exist('cmbtype','var') || isempty(cmbtype)
  cmbtype = 'bk';
end
if ~exist('phitype','var') || isempty(phitype)
  phitype = 'pl545';
end
if ~exist('dosave','var') || isempty(dosave)
  dosave = false;
end

% Load CIB map

switch phitype  
 case 'pl545'
  % Constrained CIB realization
  hmap_phi=read_fits_map(sprintf('input_maps/cib_withderivs/camb_planck2013_r0_20150701/phi_from_cib_r%04d.fits',rlz));
 case 'ideal'
  % Ideal phi
  hmap_phi=read_fits_map(sprintf('input_maps/lensing_potentials_withderivs/camb_planck2013_r0/phi_r%04d_n0512_l1024.fits',rlz));
 case 'spt'
  % SPT
  error('SPT phi not yet implemented')
 otherwise
  error('not a valid phi choice')
end


% Load and set up CMB maps

switch cmbtype  
 
 case 'bk'
  % BICEP/Keck s+n sim
  load(sprintf('maps/1459/%03d%d_aabd_filtp3_weight3_gs_dp1100_jack0.mat',rlz,type));
  map_cmb = make_map(ac,m);
  map_cmb = map_cmb(2); % 150 GHz
  map_cmb=add_masks(m,map_cmb);
 
 case 'ideal'
  % 37 arcmin beam instead of BBNS because BBNS B(l) goes to zero beyond l=800 and it
  % just gets deconvolved anyways. We should change this to an unsmoothed map later
  hmap_cmb = read_fits_map(sprintf('input_maps/camb_planck2013_r0/map_lensed_n2048_r%04d_s00p00_cPl143_dNoNoi.fits',rlz));
  % The input CMB map is healpix format. Interpolate onto high res BK pixel centers.
  m=get_map_defn('bicepultrafine');
  [xx,yy]=meshgrid(m.x_tic,m.y_tic);
  zz=healpix_interp(hmap_cmb,xx(:),yy(:),'taylor');
  map_cmb.T=reshape(zz(:,1),size(xx));
  map_cmb.Q=reshape(zz(:,2),size(xx));
  map_cmb.U=-reshape(zz(:,3),size(xx)); % negative sign for healpix -> IAU
  map_cmb.Tw = ones(size(xx));
  map_cmb.Pw = ones(size(xx));
  
 case 'sptpol'
  % s+n lensed SPT sim
  load(sprintf('maps/3003/%03d%d_ltfull_filt6_weight1_dp0000_jack0.mat',rlz,type));
  map_cmb = map(2); % 150 GHz
  ind = map_cmb.Pw<1e-2 | ~isfinite(map_cmb.Pw);
  map_cmb.Pw(ind)=0;
  map_cmb.T(ind)=0;
  map_cmb.Q(ind)=0;
  map_cmb.U(ind)=0;
  map_cmb.Tw = map_cmb.Pw; % Tw not included in SPTs saved maps
  
 case 'spt3G'
  % signal only SPT sim
  load(sprintf('maps/3003/%03d5_ltfull_filt6_weight1_dp0000_jack0.mat',rlz));
  map_cmb = map;
  % noise only SPT sim
  load(sprintf('maps/3003/%03d6_ltfull_filt6_weight1_dp0000_jack0.mat',rlz));
  % scale noise and combine
  fac=10; % currently a completely fabricated number, dial in your favorite Stage X experiment
  map_cmb.Q = map_cmb.Q+map_n.Q/fac;
  map_cmb.U = map_cmb.Q+map_n.U/fac;

 otherwise
  error('not a valid cmb choice')
  
end


% Optionally deconvolve and/or Wiener filter CMB map

switch cmbtype
 
 case 'bk'
  load final/1459x1615/real_aabd_filtp3_weight3_gs_dp1102_jack0_real_aabd_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_cm_directbpwf.mat
  apss.Cs_l=r(2).sigl;apss.l=r(2).l;
  apsn.Cs_l=r(2).noi; apsn.l=r(2).l; % Wiener filter
  ap=true; % Apodize
  lpass=[]; % Default lowpass filter
  beam=[]; % Default beam deconvolution (BBNS)
 
 case {'sptpol','spt3G'}
  % Just deconvolve the approximate 1' beam. This is not quite right, but the Wiener
  % filter is close to 1 at l<1500 so this shouldn't be too suboptimal.
  apss=[]; apsn=[]; % No Wiener filter
  ap=true; % Apodize
  lpass='none'; % No lowpass filter
  %beam=1; % 1', which is approximate, but the correction at l=1000 is only 10% or so
  beam='none'; % The maps Jason gave us are not beam convolved. Ugh.
  
 case 'ideal'
  apss=[]; apsn=[]; % No Wiener filter
  ap = false; % Do not apodize
  lpass = 'none'; % No lowpass
  beam = 'none'; % (arbitrary) beam of input map, actually wish this were zero, empty
             % equals BBNS in deconv_map below

end

map_cmb = deconv_map(m,map_cmb,beam,lpass,apss,apsn,ap);

%%%%%%%%%%%%%%%%%%%%%%%
% Delens map.
% Using linear interpolation for now to keep things...well...linear. Interpolation
% errors result in a floor in delensed Cl / lensed Cl = 0.02 at el=100 in fully
% idealized maps, improving to .005 with cubic interpolation.
map_delens=delens_qu(m,map_cmb,hmap_phi,1,'linear');
%%%%%%%%%%%%%%%%%%%%%%%

% Lensing template
map_template=map_cmb;
map_template.T=map_cmb.T-map_delens.T;
map_template.Q=map_cmb.Q-map_delens.Q;
map_template.U=map_cmb.U-map_delens.U;

% Abbly B2 beam always and apply lowpass. Undo apodization.
deap=ap;
map_template=deconv_map(m,map_template,[],[600,650],[],[],0,deap,1);

% Interpolate back to B2 map grid
if m.pixsize ~= 0.25
  mhi=m;
  m=get_map_defn('bicep');
  map_template=interp_mapfields(m,[],mhi,map_template,{'T','Q','U','Pw','Tw'});
  % Some of these additional fields are needed to pass to functions like calc_map_fts, etc.
  map_template.QUcovar = zeros(size(map_template.Q));
  map_template.Tvar = 1./map_template.Tw;
  map_template.Qvar = 1./map_template.Pw;
  map_template.Uvar = 1./map_template.Pw;
  map_template.x_tic = m.x_tic;
  map_template.y_tic = m.y_tic;
end

% This appears to be necessesary for SPT pol because of weird values where Pw  = 0. I
% don't understand how that can happen given that we are first setting the Q/U maps to zero
% where Pw =0 upon loading.
map_template.T(map_template.Tw==0)=NaN;
map_template.Q(map_template.Pw==0)=NaN;
map_template.U(map_template.Pw==0)=NaN;

map_template.T(isinf(map_template.T))=NaN;
map_template.Q(isinf(map_template.Q))=NaN;
map_template.U(isinf(map_template.U))=NaN;


if dosave
  map=map_template;
  fname=sprintf('maps/%04d/%03d%d_lt_CMB%s_PHI%s.mat',sernum,rlz,type,cmbtype,phitype);
  save(fname,'map','m');
end

return
