function loglik = like_components(param, likeopt, type)
% loglik = like_components(param, likeopt, type)
%
% Calculate log(likelihood) separately for individual spectra.
%
% Given n_expt experiments and n_field CMB fields, the total number of unique 
% spectra is N = n_expt*n_field (n_expt*n_field + 1) / 2. This function will 
% return a vector of N log-likelihoods obtained by considering each of those 
% spectra one at a time. For the Hamimeche-Lewis likelihood, there is a 
% round-about process to evaluate log-likelihood for cross-spectra.
% 
% [Inputs]
%   param    Eleven element vector of model parameters. See like_getexpvals.
%   likeopt  Data structure for multicomponent analysis. It is important 
%            to run like_read_theory and like_read_data for this likeopt 
%            before passing it to like_components.
%   type     Type of the likelihood approximation to use, either 'chi2' 
%            or 'hl'.
%
% [Output]
%   loglik   Vector of N log(likelihood) values.

% Get number of experiments, CMB fields, ell bins.
n_ell = numel(likeopt.l);
n_expt = numel(likeopt.expt);
n_field = numel(likeopt.fields);

% Evaluate model at the specified parameter values.
expv = like_getexpvals(param, likeopt);

% Get (expt,field) ordering.
expt_field = expt_field_order(n_expt, n_field);
N = numel(expt_field);

% Loop over spectra.
for s=expt_field
  if strcmp(type, 'chi2')
    %% Chi^2 likelihood approximation

    % Select model and real data bandpowers for this spectrum.
    model = expv(:,s.i);
    real = likeopt.real(:,s.i);
    % Select subset of bandpower covariance matrix for this spectrum.
    select = [s.i:N:N*n_ell];
    bpcm = likeopt.bpcm(select,select);
    % Calculate inverse bandpower covariance matrix.
    ibpcm = inv(bpcm);
    % Calculate log likelihood by chi^2 approximation.
    loglik(s.i) = like_chi2(model, real, ibpcm);
  elseif strcmp(type, 'hl')
    %% Hamimeche-Lewis likelihood approximation

    % Is this an auto or cross-spectrum?
    if (s.e1 == s.e2) && (s.f1 == s.f2)
      % Auto-spectrum, just select the one spectrum and calculate
      % H-L likelihood as normal.
      model = expv(:,s.i);
      real = likeopt.real(:,s.i);
      N_l = likeopt.N_l(:,s.i);
      C_fl = likeopt.C_fl(:,s.i);
      % Select subset of bandpower covariance matrix for this spectrum.
      select = [s.i:N:N*n_ell];
      bpcm = likeopt.bpcm(select,select);
      % Pre-calculation for H-L likelihood.
      prep = hamimeche_lewis_prepare(ivecp((C_fl + N_l)'), bpcm);
      % Calculate log likelihood by H-L approximation.
      loglik(s.i) = like_hl(model, real, N_l, prep);
    else
      % Cross-spectrum
      % First, locate the corresponding auto-spectra.
      auto1 = find_auto_spectrum(expt_field, s.e1, s.f1);
      auto2 = find_auto_spectrum(expt_field, s.e2, s.f2);
      % Select bandpowers.
      model = expv(:,[auto1,auto2,s.i]);
      real = likeopt.real(:,[auto1,auto2,s.i]);
      N_l = likeopt.N_l(:,[auto1,auto2,s.i]);
      C_fl = likeopt.C_fl(:,[auto1,auto2,s.i]);
      % Select subset of the bandpower covariance matrix
      % corresponding to this spectrum and the two auto-spectra.
      select = repmat([auto1,auto2,s.i], 1, n_ell) + ...
               reshape(repmat(N * [0:(n_ell-1)], 3, 1), 1, 3 * n_ell);
      bpcm = likeopt.bpcm(select,select);
      % Pre-calculation for H-L likelihood.
      prep = hamimeche_lewis_prepare(ivecp((C_fl + N_l)'), bpcm);
      % Calculate log likelihood by H-L approximation.
      loglik(s.i) = like_hl(model, real, N_l, prep) - ...
          loglik(auto1) - loglik(auto2);
    end
  end
end

% Subfunction: find_auto_spectrum
% -------------------------------
% Searches through a list of unique CMB spectra (obtained from
% expt_field_order) and returns the index of the auto spectrum with
% experiment e and CMB field f. Returns 0 if that auto spectrum
% doesn't exist.
function auto_index = find_auto_spectrum(expt_field, e, f)

% Loop over (expt, field) combinations.
for s=expt_field
  % If this is the right auto spectrum, then return its index.
  if (s.e1 == e) && (s.f1 == f) && (s.e2 == e) && (s.f2 == f)
    auto_index = s.i;
    return
  end
end

% Didn't find the auto spectrum! Return 0.
auto_index = 0;

