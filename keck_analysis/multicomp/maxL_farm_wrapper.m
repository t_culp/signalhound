% Farm out jobs to compute maximum likelihood parameter values for
% sets of 100 sims. Current version computes H-L maximum
% likelihood values for Keck gaussian foreground sims for model 0.
% Change the filepath, filenames to do BICEP2/Keck foreground sims or
% my toy model sims for any model.
%
% Committed 2014-11-18 (KDA)

clear;
nsims=100;
%nsims=99; %only 499 sims, so last run is different

filepath='/n/bicepfs2/users/kalex/simfiles/fgsim/keck';

for n=1:4
  outfile=sprintf('farmfiles/k_%i.out',n);
  errfile=sprintf('farmfiles/k_%i.err',n);
  savefile=sprintf('%s/m0/like_%i.mat',filepath,n);
  loadfile=sprintf('%s/m0_prep.mat',filepath);
  simrange=[(n-1)*nsims+1, n*nsims];
%  simrange=[401, 499];
  cmd=sprintf('maxL_fgsim(''%s'',[%i %i],''%s'')', savefile, simrange(1),simrange(2),loadfile);

  farmit('farmfiles', cmd, 'outfile', outfile, 'errfile', errfile, 'mem', 2500);
end
