function likeopt = get_default_likeopt(likeopt)
% likeopt = get_default_likeopt(likeopt)
%
% Fills in the likeopt structure with defaults.  
% 
% Inputs:
%   likeopt:             Struct which is filled in here (optional)
%   likeopt.bands.names: cell array of experiments (optional) 
%                        e.g. likeopt.bands.names = {'B2_150','B2XB1_100'}
%
% Note that the order for fields 'bands' and 'spectra' change the 
% ordering of the data vectors and covariance matrices built in 
% like_buildmat.
%
% KSK 2014-05-24
% VB, CAB 2014-06-05

if(~exist('likeopt','var'))
  likeopt = [];
end

% Ell bins (choose from 1:17)
% Default to bins 2:6 (used for BICEP2 r constraint)
if(~isfield(likeopt,'l'))
  likeopt.l = 2:6;
end

% By default, use only one off-diagonal block in bpcm.
if ~isfield(likeopt, 'offdiag')
  likeopt.offdiag = 1;
end

% Fields -- could be {'T', 'E', 'B'}.
% H-L works naturally in terms of fields, so if you include 'E' and 'B', 
% then the relevant spectra are EE, BB, and EB. 
if ~isfield(likeopt, 'fields')
  likeopt.fields = {'B'};
end

% likeopt.expt is a list of the experiments that are included. 
if ~isfield(likeopt, 'expt')
  likeopt.expt(1).name = 'B2_150';
  likeopt.expt(1).freq = 149.8;
  likeopt.expt(2).name = 'B1_100';
  likeopt.expt(2).freq = 96;
end

% likeopt.mod contains parameters used to calculate model
if ~isfield(likeopt, 'mod')
  % Directory containing theory spectra from CAMB.
  likeopt.mod.camb_dir = '/n/bicepfs1/bicep2/bicep2_aux_data/official_cl';
  % File containing unlensed Cs_l.
  likeopt.mod.camb_unlensed = 'camb_planck2013_r0.fits';
  % File containing lensed Cs_l.
  likeopt.mod.camb_lensed = 'camb_planck2013_r0_lensing.fits';
  % File containing tensor Cs_l.
  likeopt.mod.camb_tensor = 'camb_planck2013_r0p1.fits';
  % Record r value used for tensor Cs_l.
  likeopt.mod.rfid = 0.1;
  
  % Also, set the ell value used as a pivot scale for foreground
  % power laws...
  likeopt.mod.fg_pivot = 80;
  % ...and the observing frequencies to use when defining foreground
  % power spectrum amplitude.
  likeopt.mod.sync_freq = 150;
  likeopt.mod.dust_freq = 353;
end
