% KDA 2014-07-28

clear;

likeopt = get_default_likeopt;
likeopt.fields = {'B'};         % BB only
%likeopt.expt = likeopt.expt(1); % BICEP2 only
likeopt.expt(3).name = 'WMAP7_K';
likeopt.expt(3).freq = 23;

% Get data products from final .mat files.
likeopt = like_read_data(likeopt);

% Read theory spectra from CAMB files.
likeopt = like_read_theory(likeopt);

% Grid evaluation over r and A_L -- eventually some sampler here
r_range = [0:0.02:0.4];
A_l_range = [0.5:0.05:2.5];
%dust_range = [0:1e-3:0.040];
%sync_range = [0:8e-6:5e-4];
%beta_range = [-4:0.5:4];
%beta_sync = [-3.0];
%corr_range = [0];
%beta_sync = [-3.3:0.3:-3.0];
%corr_range = [0:0.25:1];

% Other model parameters
% We will be varying the first two elements of this vector (r and A_L);
% other elements will remain fixed to these values.
%p = [0, 1, 0, 0, -2.92, -0.6, 1.6, -0.4, 1, 1, 0];
%p = [0, 1, 5.6e-5, 0, -3.3, -0.6, 1.6, -0.4, 1, 1, 0];
%p = [0, 1.0, 5.6e-5, 0, -1.45, -0.6, 1.5, -0.4, 1, 1, 0];
p = [0.2, 1, 0, 0, -3.3, -0.6, 1.6, -0.4, 1, 1, 0]; %tensors+lensing

% Uncomment this section if you'l like to set all the cross-experiment covariances that feature WMAP_K to zero.
%bpcm = likeopt.bpcm;
%b0 = eye(length(likeopt.bpcm) ,length(likeopt.bpcm));
%N = length(likeopt.l);
%for i = 1:N
%    for j = 1:N
%	b0(1+6*(i-1), 1+6*(j-1)) = 1;
%        b0(2+6*(i-1), 2+6*(j-1)) = 1;
%        b0(3+6*(i-1), 3+6*(j-1)) = 1;
%        b0(4+6*(i-1), 4+6*(j-1)) = 1;
%        b0(5+6*(i-1), 5+6*(j-1)) = 1;
%        b0(6+6*(i-1), 6+6*(j-1)) = 1;
%
%	b0(2+6*(i-1), 1+6*(j-1)) = 1;
%	b0(4+6*(i-1), 1+6*(j-1)) = 1;
%	b0(4+6*(i-1), 2+6*(j-1)) = 1;
%	b0(6+6*(i-1), 5+6*(j-1)) = 1;
%
%	b0(1+6*(i-1), 2+6*(j-1)) = 1;
%        b0(1+6*(i-1), 4+6*(j-1)) = 1;
%        b0(2+6*(i-1), 4+6*(j-1)) = 1;
%        b0(5+6*(i-1), 6+6*(j-1)) = 1;
%    end
%end
%b1 = tril(b0) + tril(b0,-1)';
%likeopt.bpcm = likeopt.bpcm .* b1;

% get 
%[likeopt.real,likeopt.bpcm] = bpcm_val(p,likeopt,1);

% For chi^2 likelihood, precompute inverse of bandpower covariance matrix.
bpcminv = inv(likeopt.bpcm);

% For H-L likelihood, precompute fiducial model and inverse bpcm.
% Need to add noise bias to fiducial model bandpowers and convert
% to symmetric matrix before passing to hamimeche_lewis_prep.
prep = hamimeche_lewis_prepare(ivecp((likeopt.C_fl + likeopt.N_l)'), ...
                               likeopt.bpcm);

for i = 1:length(r_range)
  for j = 1:length(A_l_range)
    % Calculate model expectation values.
    p(1) = r_range(i);    % r value
    p(2) = A_l_range(j);
%    p(3) = sync_range(i);
%    p(4) = dust_range(j); % dust amplitude
%    p(5) = beta_range(j);
    model = like_getexpvals(p, likeopt);

    % Calculate log(likelihood) from chi^2.
    logl_chi2(i,j) = like_chi2(model, likeopt.real, bpcminv);
    
    % Calculate log(likelihood) from H-L.
    % Here, the wrapper takes care of combining noise bias with
    % real data and model bandpowers.
    logl_hl(i,j) = like_hl(model, likeopt.real, likeopt.N_l, prep);

%  i
%  for j = 1:length(sync_range)
%    for k = 1:length(dust_range)
%      for m = 1:length(beta_sync)
%	for n = 1:length(corr_range)
%	  % Calculate model expectation values.
%    	  p(1) = r_range(i);    % r value
%	  p(3) = sync_range(j); % sync amplitude
%    	  p(4) = dust_range(k); % dust amplitude
%	  p(5) = beta_sync(m); % sync spectral index
%	  p(11) = corr_range(n); % dust/sync correlation
%    	  model = like_getexpvals(p, likeopt);
%
%    	  % Calculate log(likelihood) from chi^2.
%    	  logl_chi2(i,j,k,m,n) = like_chi2(model, likeopt.real, bpcminv);
%    	  %logl_chi2_cpts(i,j,k,m,n) = sum(like_components(p, likeopt, 'chi2'));
%
%    	  % Calculate log(likelihood) from H-L.
%    	  % Here, the wrapper takes care of combining noise bias with
%    	  % real data and model bandpowers.
%    	  logl_hl(i,j,k,m,n) = like_hl(model, likeopt.real, likeopt.N_l, prep);
%	  %logl_hl_cpts(i,j,k,m,n) = sum(like_components(p, likeopt, 'hl'));
%
%  	end
%      end
%    end

  end
end

% Peak normalize.
%logl_chi2_un = logl_chi2;
logl_chi2 = logl_chi2 - max(logl_chi2(:));
%logl_chi2_cpts_un = logl_chi2_cpts;
%logl_chi2_cpts = logl_chi2_cpts - max(logl_chi2_cpts(:));
%logl_hl_un = logl_hl;
logl_hl = logl_hl - max(logl_hl(:));

[temp,midx_chi2]= max(logl_chi2(:));
[temp,midx_hl]= max(logl_hl(:));
[xchi2,ychi2]=ind2sub(size(logl_chi2),midx_chi2);
[xhl,yhl]=ind2sub(size(logl_hl),midx_hl);

%logl_hl_cpts_un = logl_hl_cpts;
%logl_hl_cpts = logl_hl_cpts - max(logl_hl_cpts(:));

%toc

%save('~/code/work/data/5D_lik_b2b1wk_zero_b2b1wk_corrected.mat');


% Figures.

figure(4); clf;
%imagesc(r_range, A_l_range, 2*logl_chi2, [-20,0]);
contourf(r_range, A_l_range, exp(logl_chi2'), exp(-([1, 2]).^2/2));
set(gca, 'YDir', 'normal')
xlabel('r'); %synchrotron amplitude [uK_{CMB}^2]');
ylabel('Lensing Amplitude A_L'); %dust amplitude [uK_{CMB}^2]');
title('r vs A_L, 2*log(L), BICEP2 BB');
%colorbar();
hold on;
%plot([0 0.4], [1.05 1.05], 'k--');
%plot([0 0.4], [0.95 0.95], 'k--');
scatter(r_range(xchi2),A_l_range(ychi2),30,'k+');
%plot(logl_chi2(mod(midx,length(r_range)),floor(midx/length(r_range)),'k+'));
colormap cool
caxis([0 1.5]);

figure(2); clf;
%imagesc(r_range, A_l_range, 2*logl_hl, [-20,0]);
contourf(r_range, A_l_range, exp(logl_hl'), exp(-([1, 2]).^2/2));
set(gca, 'YDir', 'normal')
xlabel('r'); %synchrotron amplitude [uK_{CMB}^2]');
ylabel('Lensing Amplitude A_L'); %dust amplitude [uK_{CMB}^2]');
title('r vs A_L, 2*log(L), BICEP2 BB');
%colorbar();
hold on;
%plot([0 0.4], [1.05 1.05], 'k--');
%plot([0 0.4], [0.95 0.95], 'k--');
scatter(r_range(xhl),A_l_range(yhl),30,'k+');
colormap cool
caxis([0 1.5]);

% figure(1); clf;
% imagesc(dust_range, r_range, 2*logl_hl, [-10,0]);
% set(gca, 'YDir', 'normal')
% xlabel('dust amplitude [uK_{CMB}^2]');
% ylabel('r');
% title('Dust vs r, 2*log(L), BICEP2 BB');
% colorbar();

%figure(1); clf;
%plot(r_range,exp(logl_chi2));
%hold all;
%plot(r_range,exp(logl_hl));
%xlabel('r');
%ylabel('Likelihood');
%title('r likelihood, B2 X B1100 + B2150, A_l fixed at 1');
%legend('\chi^2','HL');