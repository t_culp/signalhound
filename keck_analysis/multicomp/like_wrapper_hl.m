% VB, CAB 2014-06-05
% VB 2014-06-06

clear;

tic

likeopt = get_default_likeopt;
likeopt.fields = {'B'};         % BB only
%likeopt.expt = likeopt.expt(1); % BICEP2 only
likeopt.l = 2:6;                % Use standard 5 ell bins.

% Get data products from final .mat files.
likeopt = like_read_data(likeopt);

% Read theory spectra from CAMB files.
likeopt = like_read_theory(likeopt);

% likeopt.real(:,3) = zeros(5,1); % setting WMAP_K auto bp to zero
% likeopt.real(:,3) = 2.*likeopt.real(:,3); % setting WMAP_K bp to twice their actual value
% likeopt.real(:,6) = zeros(5,1); %setting B2xWMAP_K bp to zero
% likeopt.real([1,2,4],6) = zeros(3,1); %just setting bands 1,2,4 in B2xWMAP_K to zero 

% Grid evaluation over r and A_L -- eventually some sampler here
r_range = [0:0.02:0.6];
% A_l_range = [0.5:0.05:2.5];
dust_range = [0:1e-3:0.040];
sync_range = [0:8e-6:5e-4];
beta_sync = [-3.0];
corr_range = [0];
%beta_sync = [-3.3:0.3:-3.0];
%corr_range = [0:0.25:1];

% Other model parameters -- set foregrounds to zero for this case.
% We will be varying the first two elements of this vector (r and A_L);
% other elements will remain fixed to these values.
%p = [0, 1, 0, 0, -3, 0.6, 1.6, 0.6, 1, 1, 0];
p = [0, 1, 0, 0, 0, -0.6, 1.5, -0.4, 1, 1, 0];

% Uncomment this section if you'l like to set all the cross-experiment covariances that feature WMAP_K to zero.
bpcm = likeopt.bpcm;
b0 = eye(length(likeopt.bpcm) ,length(likeopt.bpcm));
N = length(likeopt.l);
for i = 1:N
    for j = 1:N
	b0(1+6*(i-1), 1+6*(j-1)) = 1;
        b0(2+6*(i-1), 2+6*(j-1)) = 1;
        b0(3+6*(i-1), 3+6*(j-1)) = 1;
        b0(4+6*(i-1), 4+6*(j-1)) = 1;
        b0(5+6*(i-1), 5+6*(j-1)) = 1;
        b0(6+6*(i-1), 6+6*(j-1)) = 1;

	b0(2+6*(i-1), 1+6*(j-1)) = 1;
	b0(4+6*(i-1), 1+6*(j-1)) = 1;
	b0(4+6*(i-1), 2+6*(j-1)) = 1;
	b0(6+6*(i-1), 5+6*(j-1)) = 1;

	b0(1+6*(i-1), 2+6*(j-1)) = 1;
        b0(1+6*(i-1), 4+6*(j-1)) = 1;
        b0(2+6*(i-1), 4+6*(j-1)) = 1;
        b0(5+6*(i-1), 6+6*(j-1)) = 1;
    end
end
b1 = tril(b0) + tril(b0,-1)';
likeopt.bpcm = likeopt.bpcm .* b1;

% For chi^2 likelihood, precompute inverse of bandpower covariance matrix.
bpcminv = inv(likeopt.bpcm);

% For H-L likelihood, precompute fiducial model and inverse bpcm.
% Need to add noise bias to fiducial model bandpowers and convert
% to symmetric matrix before passing to hamimeche_lewis_prep.
prep = hamimeche_lewis_prepare(ivecp((likeopt.C_fl + likeopt.N_l)'), ...
                               likeopt.bpcm);

for i = 1:length(r_range)
  i
  for j = 1:length(sync_range)
    for k = 1:length(dust_range)
      for m = 1:length(beta_sync)
	for n = 1:length(corr_range)
	  % Calculate model expectation values.
    	  p(1) = r_range(i);    % r value
	  p(3) = sync_range(j); % sync amplitude
    	  p(4) = dust_range(k); % dust amplitude
	  p(5) = beta_sync(m); % sync spectral index
	  p(11) = corr_range(n); % dust/sync correlation
    	  model = like_getexpvals(p, likeopt);

    	  % Calculate log(likelihood) from chi^2.
    	  logl_chi2(i,j,k,m,n) = like_chi2(model, likeopt.real, bpcminv);
    	  %logl_chi2_cpts(i,j,k,m,n) = sum(like_components(p, likeopt, 'chi2'));

    	  % Calculate log(likelihood) from H-L.
    	  % Here, the wrapper takes care of combining noise bias with
    	  % real data and model bandpowers.
    	  logl_hl(i,j,k,m,n) = like_hl(model, likeopt.real, likeopt.N_l, prep);
	  %logl_hl_cpts(i,j,k,m,n) = sum(like_components(p, likeopt, 'hl'));

  	end
      end
    end
  end
end

% Peak normalize.
%logl_chi2_un = logl_chi2;
logl_chi2 = logl_chi2 - max(logl_chi2(:));
%logl_chi2_cpts_un = logl_chi2_cpts;
%logl_chi2_cpts = logl_chi2_cpts - max(logl_chi2_cpts(:));
%logl_hl_un = logl_hl;
logl_hl = logl_hl - max(logl_hl(:));
%logl_hl_cpts_un = logl_hl_cpts;
%logl_hl_cpts = logl_hl_cpts - max(logl_hl_cpts(:));

toc

save('~/code/work/data/5D_lik_b2b1wk_zero_b2b1wk_corrected.mat');

% Figure.
% figure(1); clf;
% imagesc(dust_range, r_range, 2*logl_hl, [-10,0]);
% set(gca, 'YDir', 'normal')
% xlabel('dust amplitude [uK_{CMB}^2]');
% ylabel('r');
% title('Dust vs r, 2*log(L), BICEP2 BB');
% colorbar();

%figure(1); clf;
%plot(r_range,exp(logLike));
%xlabel('r');
%ylabel('Likelihood');
%title('r likelihood, B2 X B1100 + B2150, A_l fixed at 1');
