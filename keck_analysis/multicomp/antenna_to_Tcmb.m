function factor = antenna_to_Tcmb(nu)
% factor = antenna_to_Tcmb(nu)
% 
% Calculates conversion from antenna temperature to thermodynamic
% temperature for the specified frequency. Returned value has units 
% of [antenna / thermodynamic], so if you have a value in antenna 
% temperature units, divide by this number to obtain thermodynamic 
% units.
%
% [Input]
%   nu      Observing frequency, in GHz.
%
% [Output]
%   factor  Conversion factor from antenna temperature to
%           thermodynamic temperature at frequency nu.

% Physical constants.
h = 6.626e-34; % J*s
kB = 1.381e-23; % J/K
Tcmb = 2.72548; % K

% Calculate conversion factor.
x = h * nu * 1e9 / (kB * Tcmb);
factor = x^2 * exp(x) / (exp(x) - 1)^2;
