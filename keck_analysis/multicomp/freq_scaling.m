function scale_fac = freq_scaling(bandpass, beta, temp, nu0)
% scale_fac = freq_scaling(bandpass, beta, [temp, [nu0]])
%
% [Input]
%   bandpass   Experiment bandpass. Should be an array with shape [N,2],
%              where bandpass(:,1) is a vector of frequencies in GHz, and 
%              bandpass(:,2) are the corresponding spectral responses.
%   beta       Power law exponent beta.
%   temp       Thermodynamic temperature (in Kelvin) for a greybody model. 
%              If not specified, then a power law will be used instead.
%   nu0        Optional argument. Frequency (in GHz) at which the foreground 
%              signal power is defined. The scale factor returned by this 
%              function converts a signal amplitude from that frequency to 
%              the specified bandpass.
% 
% [Output]
%   scale_fac  The appropriate scale factor to convert a foreground signal
%              amplitude from uK_CMB at frequency nu0 to uK_CMB for the
%              instrument bandpass provided.

% Greybody model only if temp is specified.
use_greybody = 0;
if nargin >= 3
  if ~isempty(temp)
    use_greybody = 1;
  end
end
% Default value for frequency that we are scaling *from*.
if nargin < 4
  nu0 = 150;
end
if isempty(nu0)
  nu0 = 150;
end

% Fundamental constants.
h = 6.626e-34;  % J*s
kB = 1.381e-23; % J/K
Tcmb = 2.72548; % K

% Power-law scaling function.
pl = @(v, beta) v.^(2+beta);
% Greybody scaling function.
gb = @(v, beta, T) v.^(3+beta) ./ (exp(h*v*10^9 / (kB*T)) - 1);
% Conversion factor for thermodynamic temperature.
cf = @(v) v.^4 .* exp(h*v*10^9 / (kB*Tcmb)) ./ ...
     (exp(h*v*10^9 / (kB*Tcmb)) - 1).^2;

if use_greybody
  if size(bandpass, 1) > 1
    % Calculate greybody scale factor for target bandpass.
    scale_fac = trapz(bandpass(:,1), ...
                      gb(bandpass(:,1), beta, temp) .* bandpass(:,2)) ./ ...
        trapz(bandpass(:,1), bandpass(:,2));
  else
    % Calculate greybody scale factor for single frequency.
    scale_fac = gb(bandpass(1,1), beta, temp);
  end
  scale_fac = scale_fac ./ gb(nu0, beta, temp);
else
  if size(bandpass, 1) > 1
    % Calculate power law scale factor for target bandpass.
    scale_fac = trapz(bandpass(:,1), ...
                      pl(bandpass(:,1), beta) .* bandpass(:,2)) ./ ...
        trapz(bandpass(:,1), bandpass(:,2));
  else
    % Calculate power law scale factor for single frequency.
    scale_fac = pl(bandpass(1,1), beta);
  end
  scale_fac = scale_fac ./ pl(nu0, beta);
end

if size(bandpass, 1) > 1
  % Calculate thermodynamic temperature conversion for target bandpass.
  conv_fac = trapz(bandpass(:,1), cf(bandpass(:,1)) .* bandpass(:,2)) ./ ...
      trapz(bandpass(:,1), bandpass(:,2));
else
  % Calculate thermodynamic temperature conversion for single frequency.
  conv_fac = cf(bandpass(1,1));
end
conv_fac = conv_fac ./ cf(nu0);

% Combine scaling model and thermodynamic conversion.
scale_fac = scale_fac / conv_fac;

