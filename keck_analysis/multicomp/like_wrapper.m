% VB, CAB 2014-06-05
% VB 2014-06-06
% VB 2014-06-13

clear;

likeopt = get_default_likeopt;
%likeopt.mod.camb_dir = '.';
likeopt.fields = {'B'};
%likeopt.expt = likeopt.expt(1);
%likeopt.l = 2:6;

% Make data products from simfiles
%like_makedataprod(likeopt);
likeopt = formingfull_bpcm(likeopt);

% Read theory spectra from CAMB files.
likeopt = like_read_theory(likeopt);

% Make grand data vector, bpcm
%lik = like_buildmat(likeopt);

% Grid evaluation over r and A_L -- eventually some sampler here
r_range = [-0.1:0.01:0.4];
% A_l_range = [0.5:0.05:2.5];
dust_range = [0:5e-4:0.030];
%corr_range = [-1:0.01:1];
% Other model parameters -- set foregrounds to zero for this case.
% We will be varying the first two elements of this vector (r and A_L);
% other elements will remain fixed to these values.
%p = [0, 1, 0, 0, -3, 0.6, 1.6, 0.6, 1, 1, 0];
p = [0, 1, 0, 0, -2.92, -0.6, 1.6, -0.4, 1, 1, 0.7];
%lik.A_l_range = 1;
logl_chi2 = zeros(length(r_range),length(dust_range));

% Constant covmat, so forget about prefactor and compute the inverse
% beforehand
bpcminv = inv(likeopt.bpcm);

for i = 1:length(r_range)
  for j = 1:length(dust_range)
    % Calculate model expectation values.
    p(1) = r_range(i);   % r value
    %p(2) = A_l_range(j); % lensing amplitude
    p(4) = dust_range(j);
    %p(11) = corr_range(j);
    model = like_getexpvals(p, likeopt);

    % Calculate -2*log(likelihood) from chi^2.
    logl_chi2(i,j) = like_chi2(model, likeopt.real, bpcminv);
  end
end

% Convert to log(likelihood) and peak normalize.
%logLike = -0.5 * logLike;
%logLike = logLike - max(logLike(:));
logl_chi2 = logl_chi2 - max(logl_chi2(:));

figure(1); clf;
imagesc(dust_range, r_range, 2*logl_chi2, [-10,0]);
set(gca,'YDir','normal')
xlabel('dust amplitude [uK_{CMB}^2]');
ylabel('r');
title('Dust vs r 2*log(L), B2 + B1\_100');
colorbar();

%figure(1); clf;
%plot(r_range,exp(logLike));
%xlabel('r');
%ylabel('Likelihood');
%title('r likelihood, B2 X B1100 + B2150, A_l fixed at 1');
