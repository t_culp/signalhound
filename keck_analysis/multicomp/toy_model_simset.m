function toy_model_simset(likeopt, prefix, nsim)
% toy_model_simset(likeopt)
%
% Mostly copied bpcm_val, but this is designed to generate fake aps
% and final files appropriate for reading with like_read_simset. Following
% standard pipeline practice, the aps file will contain tensor-only sims 
% corresponding to r=0.1, then lensed-LCDM sims, then noise-only sims for 
% each experiment, along with all of the cross-spectra between the sim 
% types. The final file is somewhat incomplete -- ignored some fields that 
% are not used by like_read_simset. The first simulation is assigned as
% "real data".
%
% Toy model calculates bandpowers from Gaussian random numbers with fixed 
% degrees-of-freedom for signal and noise (dof taken from B2 sig+noise sims).
% Includes ~10% correlations between adjacent ell bins. Noise levels 
% based on noise bias level for the input likeopt.
%
% [Inputs]
%   likeopt  likeopt data structure should specify the experiments to use
%            and their N_l values. Everything else in the likeopt structure
%            is ignored.
%   prefix   Filename prefix for the output aps and final files. Defaults 
%            to 'tm' (for toy model).
%   nsim     Number of simulations. Defaults to 1e4.
%
% [Output]
% Writes two files to the current working directory. 
%   'PREFIX_aps.mat'   (substitute the value of the prefix argument)
%                      Acts like a pipeline aps file.
%   'PREFIX_final.mat' Acts like a pipeline final bandpowers file.


% Optional arguments.
if nargin < 2
  prefix = 'tm';
end
if nargin < 3
  nsim = 1e4; % Note: 1e6 sims requires extra memory for MATLAB!
end

% Get number of ell bins, CMB fields, and experiments.
n_ell = numel(likeopt.l);
n_expt = numel(likeopt.expt); % number of experiments
n_field = numel(unique(likeopt.fields));
M = n_expt * n_field;
n_spec = M * (M + 1) / 2; % Number of distinct expt/field combinations.

% Degrees of freedom parameter for each ell bin
% http://bmode.caltech.edu/~spuder/analysis_logbook/analysis/20140205_sigmas/
dofs = [4, 16, 45, 87, 128, 134, 186, 292, 287, 332]; 
% Number of overlapping dof. Try to get ~10% correlation btw ell bins
dofs_corr = [1, 3, 7, 11, 13, 16, 24, 29, 31];

% Get bandpower expectation values for tensor-only r=0.1 model.
p_tensor = [0.1, 0, 0, 0, -3, -0.6, 1.6, -0.4, 1, 1, 0, 19.6];
expv(:,:,1) = like_getexpvals(p_tensor, likeopt);
% Get bandpower expectation values for lensed-LCMD model.
p_lcdm = [0, 1, 0, 0, -3, -0.6, 1.6, -0.4, 1, 1, 0, 19.6];
expv(:,:,2) = like_getexpvals(p_lcdm, likeopt);
% For the current version of like_read_simset, we have to assume that all 
% experiments observe a CMB-type signal identically. So instead of expv 
% for every possible spectra, we will just keep the expt1 auto-spectra. 
% Note: This code treats different CMB fields as independent, so T and E 
% will *not* have the proper correlation. But probably we aren't going to 
% be using this for T anyway...
expv = expv(:,1:n_field,:);
  
% Generate fake noise and signal realizations for each ell bin.
signal_overlap = [];
noise_overlap = [];
for bin=1:n_ell
  % Degrees of freedom for this ell bin.
  l = likeopt.l(bin);
  dof = dofs(l);

  % Signal realizations common to all experiments.
  % Assume that all experiments have the same expectation value for
  % the signal, which is necessary for the current version of
  % like_read_simset.
  clear fld_sig;
  for i=1:size(expv, 2)   % loop over CMB fields
    for j=1:size(expv, 3) % loop over tensor, LCDM signals
      fld_sig(:,:,i,j) = generate_gauss_vec(expv(bin,i,j), dof, nsim);
      
      % Signal correlation with last ell bin.
      if ~isempty(signal_overlap)
        last_dof = dofs(l-1);
        ncorr = size(signal_overlap, 1);
        fld_sig(1:ncorr,:,i,j) = signal_overlap(:,:,i,j) * ...
            sqrt(last_dof / dof) * sqrt(expv(bin,i,j) ./ expv(bin-1,i,j));
      end
    end
  end
  % Store overlapping modes for next ell bin.
  ncorr = dofs_corr(l);
  signal_overlap = fld_sig(dof-ncorr+1:dof,:,:,:);
  
  % Loop over experiments to generate noise realizations.
  clear fld_noise;
  for i=1:size(expv,2)
    for j=1:n_expt
      % Want N_l for autospec for each experiment -- this is correct
      % because HL ordering lists all autospecs first.
      fld_noise(:,:,i,j) = generate_gauss_vec(likeopt.N_l(bin,j), dof, nsim);

      % Noise correlation with last ell bin.
      if ~isempty(noise_overlap)
        last_dof = dofs(l-1);
        ncorr = size(noise_overlap, 1);
        fld_noise(1:ncorr,:,i,j) = noise_overlap(:,:,i,j) * ...
            sqrt(last_dof / dof) * ...
            sqrt(likeopt.N_l(bin,j) / likeopt.N_l(bin-1,j));
      end  
    end
  end
  % Store overlapping modes for next ell bin.
  ncorr = dofs_corr(l);
  noise_overlap = fld_noise(dof-ncorr+1:dof,:,:,:);

  % Compute all aps for this ell bin
  f = likeopt.fields;
  cntr = 1;
  % aps(1) = tensor x tensor
  aps(cntr).Cs_l(l,:,:) = make_aps(f, fld_sig(:,:,:,1));
  cntr = cntr + 1;
  % aps(2) = LCDM x LCDM
  aps(cntr).Cs_l(l,:,:) = make_aps(f, fld_sig(:,:,:,1));
  cntr = cntr + 1;
  % aps(3:n_expt+2) = noise auto for each experiment
  for i=1:n_expt
    aps(cntr).Cs_l(l,:,:) = make_aps(f, fld_noise(:,:,:,i));
    cntr = cntr + 1;
  end
  % Next, tensor x others.
  aps(cntr).Cs_l(l,:,:) = make_aps(f, fld_sig(:,:,:,1), fld_sig(:,:,:,2));
  cntr = cntr + 1;
  for i=1:n_expt
    aps(cntr).Cs_l(l,:,:) = make_aps(f, fld_sig(:,:,:,1), ...
                                     fld_noise(:,:,:,i));
    cntr = cntr + 1;
  end
  % Then, LCDM x noise.
  for i=1:n_expt
    aps(cntr).Cs_l(l,:,:) = make_aps(f, fld_sig(:,:,:,2), ...
                                     fld_noise(:,:,:,i));
    cntr = cntr + 1;
  end
  % Finally, noise x noise.
  for i=1:n_expt
    for j=i+1:n_expt
      aps(cntr).Cs_l(l,:,:) = make_aps(f, fld_noise(:,:,:,i), ...
                                       fld_noise(:,:,:,j));
      cntr = cntr + 1;
    end
  end

  % Calculate bandpowers, etc, for final file in this ell bin.
  % First, loop over autos.
  cntr = 1;
  for i=1:n_expt
    r(cntr).sig(l,:,:) = make_aps(f, fld_sig(:,:,:,1));
    r(cntr).sigl(l,:,:) = make_aps(f, fld_sig(:,:,:,2));
    r(cntr).noi(l,:,:) = make_aps(f, fld_noise(:,:,:,i));
    r(cntr).db(l,:) = mean(r(cntr).noi(l,:,:), 3);
    db = repmat(r(cntr).db(l,:), [1,1,nsim]);
    r(cntr).sim(l,:,:) = make_aps(f, fld_sig(:,:,:,2) + ...
                                  fld_noise(:,:,:,i)) - db;
    r(cntr).simr(l,:,:) = make_aps(f, sqrt(2) * fld_sig(:,:,:,1) + ...
                                   fld_sig(:,:,:,2) + ...
                                   fld_noise(:,:,:,i)) - db;
    cntr = cntr + 1;
  end
  % Then do all the crosses.
  for i=1:n_expt
    for j=i+1:n_expt
      r(cntr).sig(l,:,:) = ...
          make_aps(f, fld_sig(:,:,:,1), fld_sig(:,:,:,1));
      r(cntr).sigl(l,:,:) = ...
          make_aps(f, fld_sig(:,:,:,2), fld_sig(:,:,:,2));
      r(cntr).noi(l,:,:) = ...
          make_aps(f, fld_noise(:,:,:,i), fld_noise(:,:,:,j));
      r(cntr).db(l,:) = mean(r(cntr).noi(l,:,:), 3);
      db = repmat(r(cntr).db(l,:), [1,1,nsim]);
      r(cntr).sim(l,:,:) = ...
          make_aps(f, fld_sig(:,:,:,2) + fld_noise(:,:,:,i), ...
                   fld_sig(:,:,:,2) + fld_noise(:,:,:,j)) - db;
      r(cntr).simr(l,:,:) = ...
          make_aps(f, sqrt(2) * fld_sig(:,:,:,1) + fld_sig(:,:,:,2) + ...
                   fld_noise(:,:,:,i), ...
                   sqrt(2) * fld_sig(:,:,:,1) + fld_sig(:,:,:,2) + ...
                   fld_noise(:,:,:,j)) - db;
      cntr = cntr + 1;
    end
  end
end

% Fill everything out to the usual 17 ell bins.
for i=1:numel(aps)
  aps(i).l = [10, 2.5 + 35 * [1:16]]';
  aps(i).Cs_l(max(l)+1:17,:,:) = 0;
end
for i=1:numel(r)
  r(i).l = [10, 2.5 + 35 * [1:16]]';
  r(i).sim(max(l)+1:17,:,:) = 0;
  r(i).simr(max(l)+1:17,:,:) = 0;
  r(i).sigl(max(l)+1:17,:,:) = 0;
  r(i).sig(max(l)+1:17,:,:) = 0;
  r(i).noi(max(l)+1:17,:,:) = 0;
  r(i).db(max(l)+1:17,:,:) = 0;
end

% Add real and rwf fields for final bandpowers.
for i=1:numel(r)
  r(i).rwf = ones(17, size(r(i).sim, 2));
  r(i).real = squeeze(r(i).simr(:,:,1));
end

% Fake bandpower window functions.
for i=1:numel(r)
  bpwf(i).l = [2:600]';
  bpwf(i).Cs_l = zeros(numel(bpwf(i).l), size(r(i).sim, 1), ...
                       size(r(i).sim, 2));
  for j=1:numel(r(i).l)
    bpwf(i).Cs_l(ceil(r(i).l(j)),j,:) = 1;
  end
end

% Fake option structure.
for i=1:n_expt
  opt.finalopt.mapname{i} = likeopt.expt(i).name;
end

% Write files.
apsname = [prefix '_aps.mat'];
save(apsname, 'aps');
finalname = [prefix '_final.mat'];
save(finalname, 'r', 'bpwf', 'opt', '-v7.3');


%%%%%%%%%%%%%
% Helper function from Immanuel's code
function v = generate_gauss_vec(amplitude, dof, n_realization)
%Normalize so average auto spectrum = amplitude
%randn gives mean 0 and std 1
% dof * amplitude / dof = amplitude
v = randn(dof, n_realization) * sqrt(amplitude / dof);


function aps = make_aps(fields, fld1, fld2)
  
isauto = 0;
if nargin < 3
  isauto = 1;
  fld2 = fld1;
end

nsim = size(fld1, 2);
if isauto
  aps = zeros(1, 6, nsim);
else
  aps = zeros(1, 9, nsim);
end

iT = find(strcmp('T', fields));
iE = find(strcmp('E', fields));
iB = find(strcmp('B', fields));
% TT
if ~isempty(iT)
  aps(1,1,:) = sum(fld1(:,:,iT) .* fld2(:,:,iT), 1);
end
% TE
if ~isempty(iT) && ~isempty(iE)
  aps(1,2,:) = sum(fld1(:,:,iT) .* fld2(:,:,iE), 1);
end
% EE
if ~isempty(iE)
  aps(1,3,:) = sum(fld1(:,:,iE) .* fld2(:,:,iE), 1);
end
% BB
if ~isempty(iB)
  aps(1,4,:) = sum(fld1(:,:,iB) .* fld2(:,:,iB), 1);
end
% TB
if ~isempty(iT) && ~isempty(iB)
  aps(1,5,:) = sum(fld1(:,:,iT) .* fld2(:,:,iB), 1);
end
% EB
if ~isempty(iE) && ~isempty(iB)
  aps(1,6,:) = sum(fld1(:,:,iE) .* fld2(:,:,iB), 1);
end
if ~isauto
  % ET
  if ~isempty(iE) && ~isempty(iT)
    aps(1,7,:) = sum(fld1(:,:,iE) .* fld2(:,:,iT), 1);
  end
  % BT
  if ~isempty(iB) && ~isempty(iT)
    aps(1,8,:) = sum(fld1(:,:,iB) .* fld2(:,:,iT), 1);
  end
  % BE
  if ~isempty(iB) && ~isempty(iE)
    aps(1,9,:) = sum(fld1(:,:,iB) .* fld2(:,:,iE), 1);
  end
end

