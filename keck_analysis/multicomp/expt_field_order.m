function [order, snorder] = expt_field_order(n_expt, n_field, n_type)
% order = expt_field_order(n_expt, n_field)
%
% A common problem that comes up is to start with a list of experiments
% and CMB fields and then generate all the possible auto and
% cross-spectra following the Hamimeche-Lewis vecp ordering. This
% convenience function does that for arbitrary number of experiments 
% and CMB fields.
%
% [Input]
%   n_expt   Number of experiments, ex: 2 for {'B2', 'B1_100'}
%   n_field  Number of CMB fields, ex: 2 for {'E', 'B'}
%   n_type   Number of sim types, which can be 1 (default) or 2 to
%            include both signal and noise sims. This optional argument
%            does not affect the order output, but does determine the
%            snorder output. See output description and examples below.
%
% [Output]
%   order    An array with length N*(N+1)/2, where N = n_expt*n_field.
%            Each element in the array specifies an auto or cross
%            spectrum consisting of four elements: experiment 1 
%            (order.e1), field 1 (order.f1), experiment 2 (order.e2), 
%            and field 2 (order.f2). Each element of order also includes 
%            order.i, which just increments starting from one.
%   snorder  This output is only calculated for n_type = 2. Array with 
%            length N^2, where N = n_expt*n_field. Each element in the 
%            array represents a signal x noise cross spectrum consisting
%            of six elements: four of the elements are the same as
%            described above for the order output; the additional two are
%            snorder.t1 and snorder.t2, which specify whether the first 
%            and second halves of the cross spectrum are signal (1) or 
%            noise (2). Since these are all signal x noise, t1 and t2 
%            will always be different.
% 
% [Examples]
%   1. Example for two experiments, two fields.
%   >> order = expt_field_order(2, 2);
%   >> for s=order
%          expt1 = s.e1;
%          field1 = s.f1;
%          expt2 = s.e2;
%          field2 = s.f2;
%          %% do some calculation for this spectrum %%
%          result(s.i) = my_calculation(...);
%      end
%
%   2. Example for three experiments, one field, signal and noise sims.
%   >> [order, snorder] = expt_field_order(3, 1, 2);
%   >> for s=snorder
%          expt1 = s.e1;
%          field1 = s.f1;
%          type1 = s.t1; % 1 for signal, 2 for noise
%          expt2 = s.e2;
%          field2 = s.f2;
%          type2 = s.t2; % always opposite of s.t1
%          %% do some calculation %%
%          result(s.i) = my_calculation(...);
%      end

% Optional n_type argument defaults to value 1.
if nargin < 3
  n_type = 1;
end

% Combine experiments and CMB fields into a single array. Correct
% ordering is: (e1f1,..., e1fx, e2f1,..., e2fx,..., eyf1, ..., eyfx).
M = n_expt * n_field;
expt_field(:,1) = reshape(ones(n_field, 1) * [1:n_expt], 1, M);
expt_field(:,2) = reshape([1:n_field]' * ones(1, n_expt), 1, M);

% Now loop over all pairwise combinations of (expt, field). The
% native H-L ordering starts with M auto-correlations, then (M-1)
% lag-1 cross-correlations, (M-2) lag-2 cross-correlations, etc.
counter = 1;
for i=1:M         % (i-1) sets the lag of cross-correlation
  for j=1:(M-i+1) % j steps through the expt_field array
    order(counter).e1 = expt_field(j, 1);
    order(counter).f1 = expt_field(j, 2);
    order(counter).e2 = expt_field(j + i - 1, 1);
    order(counter).f2 = expt_field(j + i - 1, 2);

    order(counter).i = counter;

    % Increment counter.
    counter = counter + 1;
  end
end

% Calculate signal x noise combinations.
if n_type == 2
  % Number of experiment field combinations doubles when we include
  % signal and noise types.
  M = 2 * M;
  % Experiment (1) varies slowest, then field (2), then type (3).
  clear expt_field;
  expt_field(:,1) = reshape(ones(n_field * n_type, 1) * [1:n_expt], 1, M);
  expt_field(:,2) = reshape(repmat([1:n_field], n_type, n_expt), 1, M);
  expt_field(:,3) = reshape([1:n_type]' * ones(1, n_field * n_expt), 1, M);

  % Now loop over all pairwise combinations of (expt, field). The
  % native H-L ordering starts with M auto-correlations, then (M-1)
  % lag-1 cross-correlations, (M-2) lag-2 cross-correlations, etc.
  counter = 1;
  for i=1:M         % (i-1) sets the lag of cross-correlation
    for j=1:(M-i+1) % j steps through the expt_field array
      % Only keep terms that are products of signal and noise.
      if expt_field(j, 3) ~= expt_field(j + i - 1, 3)
        snorder(counter).e1 = expt_field(j, 1);
        snorder(counter).f1 = expt_field(j, 2);
        snorder(counter).t1 = expt_field(j, 3);
        snorder(counter).e2 = expt_field(j + i - 1, 1);
        snorder(counter).f2 = expt_field(j + i - 1, 2);
        snorder(counter).t2 = expt_field(j + i - 1, 3);

        snorder(counter).i = counter;
        
        % Increment counter.
        counter = counter + 1;
      end
    end
  end
else
  % If n_type ~= 2, just return a dummy value.
  snorder = [];
end
