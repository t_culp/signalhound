function likedata = scale_noi(noi_scl, likedata)
%
% Scales the noise in the noise x noise and signal x noise contributions to 
% the bandpower covariance matrix.
%
%  [Input]
%    noi_scl  Vector of map noise scalings; Applied once to a noise x signal term,
%	      and twice to a noise x noise term.
%    likedata  likedata data structure, including aps field with six 
%             covariance matrix components.
%
%  [Output]
%    likedata  likedata data structure, with the noise aps fields rescaled
%

% Get number of experiments, fields, spectra, ell bins.
n_expt = length(likedata.opt.expt);
n_field = length(likedata.opt.fields);
order = expt_field_order(n_expt, n_field);
n_spec = numel(order);
n_ell = numel(likedata.opt.l);

% For convenience, convert order from array of structs into a 2D array with 
% size [5, n_spec].
%   order(1,:) = expt 1
%   order(2,:) = field 1
%   order(3,:) = expt 2
%   order(4,:) = field 2
%   order(5,:) = index (1:n_spec) 
order = reshape(struct2array(order), 5, n_spec);
 
% Calculate the indexing in this array from bpcm order.
findex_1 = order(2,:) + (order(1,:) - 1) * n_field;
findex_2 = order(4,:) + (order(3,:) - 1) * n_field;

% Scale down the N_l's
Nl_matrix = repmat(noi_scl(findex_1) .* noi_scl(findex_2), n_ell, 1);
likedata.N_l = likedata.N_l ./ Nl_matrix;

% Repeat the indexing for all ell bins.
findex_1 = repmat(findex_1, 1, n_ell);
findex_2 = repmat(findex_2, 1, n_ell);

% Prepare noise scale factors using the indexing above
noiscl_1 = noi_scl(findex_1);
noiscl_2 = noi_scl(findex_2);

% Scale down the noise x noise contributions
noi_matrix = (noiscl_1 .* noiscl_2)' * (noiscl_1 .* noiscl_2);
likedata.bpcov.noi = likedata.bpcov.noi .* likedata.bpcov.mask_noi ./ noi_matrix;

% Scale term 3 contribution: Cov(S_i x N_j, S_k x N_l)
noi_matrix = noiscl_2' * noiscl_2;
likedata.bpcov.sn1 = likedata.bpcov.sn1 .* likedata.bpcov.mask_sn1 ./ noi_matrix;

% Scale Term 4 contribution: Cov(S_i x N_j, N_k x S_l)
noi_matrix = noiscl_2' * noiscl_1;
likedata.bpcov.sn2 = likedata.bpcov.sn2 .* likedata.bpcov.mask_sn2 ./ noi_matrix;

% Scale Term 5 contribution: Cov(N_i x S_j, S_k x N_l)
noi_matrix = noiscl_1' * noiscl_2;
likedata.bpcov.sn3 = likedata.bpcov.sn3 .* likedata.bpcov.mask_sn3 ./ noi_matrix;

% Scale Term 6 contribution: Cov(N_i x S_j, N_k x S_l)
noi_matrix = noiscl_1' * noiscl_1;
likedata.bpcov.sn4 = likedata.bpcov.sn4 .* likedata.bpcov.mask_sn4 ./ noi_matrix;

return
