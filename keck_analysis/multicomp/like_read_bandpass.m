function likedata = like_read_bandpass(likedata)
% likedata = like_read_bandpass(likedata)
%
% Get bandpass data for experiments listed in likedata.opt. Bandpasses are 
% stored in likedata.bandpass as a cell array with the same length as 
% likedata.opt.expt. Each entry in the cell array is a 2D array where the 
% first  row lists frequency values and the second row lists spectral 
% response.
%
% [Input]
%   likedata  Data structure for multicomponent analysis.
%
% [Output]
%   likedata  Data structure for multicomponent analysis, now with
%             bandpasses.
%
% == BICEP2 bandpass options ==
% By default, this function will fetch a BICEP2 bandpass (also used
% for Keck 150) that has been corrected for FTS coupling efficiency
% and uses the appropriate convention for freq_scaling.m (response
% to a source with uniform spectral radiance). However, you can set
% the likeopt.B2_old_bandpass field for backwards compatibility.
% Behavior:
%   likedata.opt.B2_old_bandpass undefined -- return 2014-12-16 bandpass
%   likedata.opt.B2_old_bandpass = 0 -- return 2014-12-16 bandpass
%   likedata.opt.B2_old_bandpass > 0 -- return 2014-06-26 bandpass
%                   (no throughput correction, response to R-J source)
%   likeopt.B2_old_bandpass < 0 -- return 2014-06-26 bandpass,
%                   but scaled by nu^x, where x is the value of
%                   likedata.opt.B2_old_bandpass.
% For more info, see posting from 2014 Dec 11 (B2 logbook).
%
% == Planck bandpass options ==
% By default, the bandpasses used for Planck 143, 217, and 353 GHz are 
% averaged over the polarization-sensitive bolometers only. If you set
% likedata.opt.Planck_full_bandpass = 1, then this function will retrieve 
% the bandpass file that is averaged over *all* bolometers. This option 
% is included for backwards compatibility with foreground sims.
%
% == Foreground sims ==
% For the B2P foreground sims (models 7, 12, 13), you should set
% likedata.opt.B2_old_bandpass = 1 and likeopt.Planck_full_bandpass = 1. As a 
% shortcut, can set likedata.opt.fgm_mode = 1, which will set both of those 
% switches to the right value. Remember that those sims were also generated
% with beta_dust = 1.60.

% Clear any existing likedata.bandpass field.
if isfield(likedata, 'bandpass')
  likedata = rmfield(likedata, 'bandpass');
end

% Setting fgm_mode=1 is the same as setting likeopt.B2_old_bandpass=1
% and likeopt.Planck_full_bandpass=1.
if isfield(likedata.opt, 'fgm_mode')
  if likedata.opt.fgm_mode > 0
    likedata.opt.B2_old_bandpass = 1;
    likedata.opt.Planck_full_bandpass = 1;
  end
end

% Default values of optional likeopt fields.
if ~isfield(likedata.opt, 'B2_old_bandpass')
  likedata.opt.B2_old_bandpass = 0;
end
if ~isfield(likedata.opt, 'Planck_full_bandpass')
  likedata.opt.Planck_full_bandpass = 0;
end

% Loop over experiments.
for ii=1:numel(likedata.opt.expt)
  % Switch on experiment name. Paths to all bandpass values are hard-coded
  % for odyssey.
  switch likedata.opt.expt{ii}
    case {'B2_150', 'Bicep2', 'B2', 'K', 'B2K', 'BK', 'BK14_150'}
      % Settings in likedata.opt control which version of B2
      % bandpass to use.
      if likedata.opt.B2_old_bandpass == 0
        % If flag == 0, get the new (correct) bandpass.
        likedata.bandpass{ii} = ...
            read_BK_bandpass('/n/bicepfs1/bicep2/bicep2_aux_data/bandpass', ...
                             'B2_frequency_spectrum_20141216.txt');
      else
        if likedata.opt.B2_old_bandpass > 0
          % If flag > 0, get unscaled version of old bandpass.
          likedata.bandpass{ii} = read_old_B2_bandpass();
        else
          % If flag < 0, get old bandpass by rescale by nu^flag.
          likedata.bandpass{ii} = ...
              read_old_B2_bandpass(likedata.opt.B2_old_bandpass);
        end
      end
    case {'K95', 'BK14_95'}
      likedata.bandpass{ii} = ...
          read_BK_bandpass('/n/bicepfs2/keck/keck_aux_data/bandpass', ...
                           'K95_frequency_spectrum_20150309.txt');
    case {'K220'}
      % Bandpass file for 220s is based on unreliable fts data,
      % currently
      % Can delete this comment and the disp message when the new FTS
      % data are analysed.
      disp('USING BANDPASS BASED ON UNTRUSTED FTS DATA')
      likedata.bandpass{ii} = ...
          read_BK_bandpass('/n/bicepfs2/keck/keck_aux_data/bandpass', ...
                           'K220_frequency_spectrum_20160120.txt');	  
    case {'B1_100'}
      % Need to find BICEP1 bandpasses. Using center frequency from Bierman.
      likedata.bandpass{ii} = [96.2, 1];
    case {'B1_150'}
      % Need to find BICEP1 bandpasses. Using center frequency from Bierman.
      likedata.bandpass{ii} = [150.6, 1];
    case {'Pl_30', 'Pl030', 'P030'}
      % Planck 30 GHz (LFI)
      likedata.bandpass{ii} = read_LFI_bandpass('LFI_BANDPASS_F030.txt');
    case {'Pl_44', 'Pl044', 'P044'}
      % Planck 44 GHz (LFI)
      likedata.bandpass{ii} = read_LFI_bandpass('LFI_BANDPASS_F044.txt');
    case {'Pl_70', 'Pl070', 'P070'}
      % Planck 70 GHz (LFI)
      likedata.bandpass{ii} = read_LFI_bandpass('LFI_BANDPASS_F070.txt');
    case {'Pl_100', 'Pl100', 'P100'}
      % Planck 100 GHz (HFI)
      likedata.bandpass{ii} = ...
          read_Planck_bandpass('Planck_Bandpass_100GHz_DX11d_FULL.fits');
    case {'Pl_143', 'Pl143', 'P143'}
      % Planck 143 GHz (HFI)
      if likedata.opt.Planck_full_bandpass > 0
        % Include spiderweb bolometers
        likedata.bandpass{ii} = ...
            read_Planck_bandpass('Planck_Bandpass_143GHz_DX11d_FULL.fits');
      else
        % Polarization sensitive bolometers only
        likedata.bandpass{ii} = ...
            read_Planck_bandpass('Planck_Bandpass_143GHz_DX11d_POL.fits', ...
                                 '/n/bicepfs2/b2planck/incoming/2014_10_23/bandpass');
      end
    case {'Pl_217', 'Pl217', 'P217'}
      % Planck 217 GHz (HFI)
      if likedata.opt.Planck_full_bandpass > 0
        % Include spiderweb bolometers
        likedata.bandpass{ii} = ...
            read_Planck_bandpass('Planck_Bandpass_217GHz_DX11d_FULL.fits');
      else
        % Polarization sensitive bolometers only
        likedata.bandpass{ii} = ...
            read_Planck_bandpass('Planck_Bandpass_217GHz_DX11d_POL.fits', ...
                                 '/n/bicepfs2/b2planck/incoming/2014_10_23/bandpass');
      end
    case {'Pl_353', 'Pl353', 'P353'}
      % Planck 353 GHz (HFI)
      if likedata.opt.Planck_full_bandpass > 0
        % Include spiderweb bolometers
        likedata.bandpass{ii} = ...
            read_Planck_bandpass('Planck_Bandpass_353GHz_DX11d_FULL.fits');
      else
        % Polarization sensitive bolometers only
        likedata.bandpass{ii} = ...
            read_Planck_bandpass('Planck_Bandpass_353GHz_DX11d_POL.fits', ...
                                 '/n/bicepfs2/b2planck/incoming/2014_10_23/bandpass');
      end
    case {'WMAP7_K', 'WMAP9_K', 'W023'}
      % One differencing assembly at K-band.
      likedata.bandpass{ii} = read_WMAP_bandpass('K', 1);
    case {'WMAP7_Ka', 'WMAP9_Ka', 'W033'}
      % One differencing assembly at Ka-band.
      likedata.bandpass{ii} = read_WMAP_bandpass('Ka', 1);
    case {'WMAP7_Q', 'WMAP9_Q', 'W041'}
      % Two differencing assemblies at Q-band.
      likedata.bandpass{ii} = read_WMAP_bandpass('Q', 2);
    case {'WMAP7_V', 'WMAP9_V', 'W061'}
      % Two differencing assemblies at V-band.
      likedata.bandpass{ii} = read_WMAP_bandpass('V', 2);
    case {'WMAP7_W', 'WMAP9_W', 'W094'}
      % Four differencing assemblies at W-band.
      likedata.bandpass{ii} = read_WMAP_bandpass('W', 4);
    case {'LT'}
      % Lensing template -- use empty bandpass array.
      likedata.bandpass{ii} = [];
    otherwise
      warning('No bandpass information for experiment %s', ...
              likedata.opt.expt{ii});
      likedata.bandpass{ii} = [];
  end
end


function bp = read_BK_bandpass(dir, fname)
% Default to BICEP2 bandpass if no directory or file is specified.
if nargin < 1
  dir = '/n/bicepfs1/bicep2/bicep2_aux_data/bandpass';
end
if nargin < 2
  fname = 'B2_frequency_spectrum_20141216.txt';
end
fname = fullfile(dir, fname);
% Ignore bandpass for R-J source (column 3).
[bp(:,1), bp(:,2), dummy] = textread(fname, '%f,%f,%f', ...
                                     'commentstyle', 'shell');


function bp = read_old_B2_bandpass(x)
% Read 2014-06-26 version of BICEP2 bandpass.
dirname = '/n/bicepfs1/bicep2/bicep2_aux_data/bandpass';
fname = 'B2_frequency_spectrum_20140626.txt';
fname = fullfile(dirname, fname);
[bp(:,1), bp(:,2)] = textread(fname, '%f,%f', 'commentstyle', 'shell');
% If x is specified, multiply bandpass by nu^x (NOTE: x typically negative).
if nargin > 0
  bp(:,2) = bp(:,2) .* (bp(:,1).^x);
end
bp(:,2) = bp(:,2) / max(bp(:,2));


function bp = read_Planck_bandpass(file, dir)
if nargin < 2
  % Default choice for bandpass directory.
  dir = '/n/bicepfs2/b2planck/incoming/140806/bandpass/';
end
data = fitsread(fullfile(dir, file), 'bintable');
% Planck bandpasses extend to very high frequencies (18 THz!!). If we don't 
% truncate these, we get crazy results because the antenna-thermodynamic 
% conversion factor is so large at high frequencies.
% Use frequency range of 10 to 1000 GHz.
i1 = find(data{1} >= 10, 1, 'first');
i2 = find(data{1} <= 1000, 1, 'last');
bp(:,1) = data{1}(i1:i2);
bp(:,2) = data{2}(i1:i2);


function bp = read_LFI_bandpass(file)
% Directory for LFI bandpasses (downloaded from irsa.ipac.caltech.edu)
dir = '/n/bicepfs2/b2planck/lfi_bandpass';
% Load bandpass from ascii file.
fname = fullfile(dir, file);
[bp(:,1), bp(:,2), d1, d2] = textread(fname, '%f %f %f %c', ...
                                      'commentstyle', 'shell');
                                      

function bp = read_WMAP_bandpass(band, nda)
% Read WMAP bandpasses from this directory where I put them. 
%  dir = '/n/bicepfs2/users/cbischoff/multicomp/bandpass/';
dir = '/n/bicepfs2/keck/external_bandpasses/';

% Loop over differencing assemblies.
for i=1:nda
  % First radiometer in the differencing assembly.
  fname = sprintf('wmap_bandpass_%s%i1_v5.cbp', band, i);
  [nu amp1 amp2] = textread(fullfile(dir, fname), '%f %f %f', ...
                            'commentstyle', 'shell');
  data(:,4*i-3) = amp1;
  data(:,4*i-2) = amp2;

  % Second radiometer in the differencing assembly.
  fname = sprintf('wmap_bandpass_%s%i2_v5.cbp', band, i);
  [nu amp1 amp2] = textread(fullfile(dir, fname), '%f %f %f', ...
                            'commentstyle', 'shell');
  data(:,4*i-1) = amp1;
  data(:,4*i-0) = amp2;
end

% Average across all differencies assemblies, both amplifiers.
bp(:,1) = nu;
bp(:,2) = mean(data, 2);
