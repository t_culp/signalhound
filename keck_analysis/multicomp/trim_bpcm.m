function bpcm = trim_bpcm(bpcm, likedata)
% bpcm = trim_bpcm(bpcm, likedata)

n_ell = numel(likedata.opt.l);
n_expt = numel(likedata.opt.expt);
n_field = numel(likedata.opt.fields);
M = n_expt * n_field;
n_spec = M * (M + 1) / 2; % Number of distinct expt/field combinations.
N = n_ell * n_spec;

% Zero blocks that are further off-diagonal than likeopt.offdiag
for ii=1:n_ell
  for jj=1:n_ell
    if abs(ii - jj) > likedata.opt.offdiag
      % Zero this block, which has size [n_spec, n_spec].
      bpcm((ii-1)*n_spec+1:ii*n_spec, (jj-1)*n_spec+1:jj*n_spec) = 0;
    end
  end
end

return % END of function trim_bpcm
