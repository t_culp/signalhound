function spec_out = ivecp(spec_in)
% spec_out = ivecp(spec_in)
%
% Convert bandpowers from H-L vecp ordering to symmetric matrix form.
%
% [Input]
%   spec_in   Input spectra with vecp ordering along the first dimension. 
%             Size of the first dimension must equal N*(N+1)/2, for 
%             integer N. Second dimension is typically ell bins.
%
% [Output]
%   spec_out  Symmetric matrix form of the same spectra, with size [N,N,:]

% Determine the number of expt * fields.
vecp_size = size(spec_in, 1);
N = round(-0.5 + sqrt(0.25 + 2 * vecp_size));
% If N is non-integer, then this can't be correct vecp.
if (N * (N + 1) / 2 ~= vecp_size)
  disp('ERROR: first dimension of spec_in does not have the right size');
  spec_out = [];
  return;
end

% Get H-L vecp ordering, treating as N different fields (not
% important to differentiate between different experiments and
% different CMB fields for this case.
order = expt_field_order(1, N);

% Loop through the vecp and construct symmetric matrix.
for s=order
  spec_out(s.f1, s.f2, :) = spec_in(s.i, :);
  spec_out(s.f2, s.f1, :) = spec_in(s.i, :);
end
