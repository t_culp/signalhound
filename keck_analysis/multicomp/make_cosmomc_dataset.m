function make_cosmomc_dataset(dataset, fidmod, fields, usesim)
% make_cosmomc_dataset(dataset, fidmod, fields, usesim)
%
% This function generates the dataset files used by CosmoMC.
%
% [Input]
%   dataset    Select which data set to use. Options are the same as listed 
%              for get_likedata.m
%   fidmod     Select fiducial model for H-L likelihood. Defaults to 'tensor' 
%              (r=0.2 plus lensing). Other options are 'dust' (Adust=3.6 uK^2 
%              plus lensing) and 'BKP_bestfit' (r=0.05, Adust=3.3 uK^2 plus 
%              lensing).
%   fields     Cell array listing CMB fields to use. Defaults to {'B'}.
%   usesim     Allows you to export a sim realization in place of the 
%              real data. If argument is empty, then export real data as 
%              usual (default behavior). 
%              If argument is an integer, then export that realization from 
%              the 'simr' field in the same file as the real bandpowers. 
%              If argument is a cell array, then first entry is the integer 
%              realization number, second entry (optional) is the name of the 
%              field for the sim type you want (defaults to 'simr'), and third 
%              entry is the pipeline final file to take the sim realization 
%              from (defaults to the file used for the real bandpowers).
%              Instead of a single integer realization number, you can specify 
%              an array of realization numbers, which will produce a similar 
%              number of .dataset and cl_hat.dat files (rest of the files are 
%              common across all realizations).
%
% [Output]
%   dataset file     Meta file describing dataset.
%   Clhat file       Real bandpowers.
%   Cfl file         Fiducial model bandpowers.
%   Nl file          Noise bias bandpowers.
%   bpcm file        Bandpower covariance matrix.
%   bandpass files   Experiment bandpasses.
%   window files     Bandpower window functions.
%   parameters file  List of likelihood foreground/nuisance parameters.

% Parse fidmod argument.
if nargin < 2
  fidmod = [];
end
if isempty(fidmod)
  fidmod = 'tensor';
end
if ~any(strcmp(fidmod, {'tensor', 'dust', 'BKP_bestfit'}))
  % Argument is some unknown string.
  fidmod = 'tensor';
end
% Default option for fields argument.
if nargin < 3
  fields = {};
end
if isempty(fields)
  fields = {'B'};
end
% Default option for usesim argument.
if nargin < 4
  usesim = [];
end

% Get likedata structure.
likeopt.l = [2:10];
likeopt.fields = fields;
% For BKP, we built covariance matrix components from LCDM+r=0.2 sims.
% For BK14, we switched to just using LCDM-only sims.
switch dataset
  case {'BKP', 'BKP_yr', 'BKP_hr', 'BKP_auto', 'BP', 'BP_yr', 'BP_hr', ...
        'BP_auto', 'KP', 'KP_yr', 'KP_hr', 'KP_auto'}
    likeopt.fidmodel.tensor = 2; % r=0.2
  otherwise
    likeopt.fidmodel.tensor = 0;
end
likeopt.fidmodel.lcdm = 1;
likeopt.fidmodel.dust = 0;
likeopt.fidmodel.unlens = 0;
if strcmp(dataset, 'BKP_yr')
  % Necessary for HL likelihood.
  likeopt.Planck143_EBfull = 1;
end
likedata = get_likedata(dataset, likeopt);

% Generate bandpower covariance matrix for fiducial model.
if strcmp(fidmod, 'tensor')
  % Fiducial model is lensed-LCDM + r=0.2
  pfid = [0.2, 1, 0, 0, -3, -0.6, 1.59, -0.42, 1, 2, 0, 19.6];
elseif strcmp(fidmod, 'dust')
  % Fiducial model has r=0, Alens=1, Async=0, Adust=3.6.
  % Dust EE/BB ratio = 2, beta_dust = 1.59, Tdust = 19.6
  pfid = [0, 1, 0, 3.6, -3, -0.6, 1.59, -0.42, 1, 2, 0, 19.6];
elseif strcmp(fidmod, 'BKP_bestfit')
  % Fiducial model has r=0.05, Alens=1, Async=0, Adust=3.3.
  % Dust EE/BB ratio = 2, beta_dust = 1.59, Tdust = 19.6
  pfid = [0.05, 1, 0, 3.3, -3, -0.6, 1.59, -0.42, 1, 2, 0, 19.6];
end
C_fl = like_getexpvals(pfid, likedata);
[ell, rms] = model_rms(pfid, likedata);
bpcm = scale_bpcm(rms, likedata);

% Replace real bandpowers with a sim, if requested.
if nargin < 4
  usesim = [];
end
if ~isempty(usesim)
  % Default to using sim realization from 'simr' field.
  simfield = 'simr';
  % Default to using sim realization from the final bandpowers file.
  simfile = likedata.opt.finalfile;
  % Can override defaults with cell array usesim argument.
  if iscell(usesim)
    % First element is integer realization number
    simid = usesim{1};
    % Second element specifies which sim field to use.
    if numel(usesim) > 1
      simfield = usesim{2};
    end
    % Third element specifies whith file to use.
    if numel(usesim) > 2
      simfile = usesim{3};
    end
  else
    % usesim is not a cell array, just an integer specifying
    % realization number.
    simid = usesim(1);
  end

  % Get sim realizations.
  if strcmp(dataset, 'BKP')
    expt_select = [1,5:8];
  else
    expt_select = [];
  end
  bpsim = extract_fgsims(simfile, likedata.opt, simfield, ...
                         expt_select, likedata.opt.Planck_datasplit);
end

% Store dataset option in likedata structure.
switch dataset
  case {'BKP', 'BKP_yr', 'BKP_hr', 'BKP_auto', ...
        'BP', 'BP_yr', 'BP_hr', 'BP_auto', ...
        'KP', 'KP_yr', 'KP_hr', 'KP_auto'}
    likedata.dataset = 'BKPlanck';
  otherwise
    likedata.dataset = 'BK14';
end
% Store BICEP2/Keck data option in likedata structure.
switch dataset
  case {'BKP', 'BKP_yr', 'BKP_hr', 'BKP_auto'}
    likedata.bkopt = '_comb';
  case {'BP', 'BP_yr', 'BP_hr', 'BP_auto'}
    likedata.bkopt = '_B';
  case {'KP', 'KP_yr', 'KP_hr', 'KP_auto'}
    likedata.bkopt = '_K';
  otherwise
    likedata.bkopt = '';
end
% Store datasplit option in likedata structure.
% Empty string means no datasplit.
switch dataset
  case {'BKP', 'BP', 'KP'}
    likedata.datasplit = '_detset';
  case {'BKP_yr', 'BP_yr', 'KP_yr'}
    likedata.datasplit = '_year';
  case {'BKP_hr', 'BP_hr', 'KP_hr'}
    likedata.datasplit = '_halfring';
  case {'BKP_auto', 'BP_auto', 'KP_auto'}
    likedata.datasplit = '_auto';
  otherwise
    likedata.datasplit = '';
end
% Store fiducial model option in likedata structure.
likedata.fidmod = fidmod;

% If usesim is set, then write out one .dataset and one _cl_hat.dat
% file for each realization.
if ~isempty(usesim)
  for ii=1:numel(simid)
    write_dataset(likedata, simid(ii));
    write_Clhat(likedata, bpsim(:,:,simid(ii)), simid(ii));
  end
else
  write_dataset(likedata, 0);
  write_Clhat(likedata, likedata.real, 0);
end
write_Cfl(likedata, C_fl);
write_Nl(likedata);
write_bpcm(likedata, bpcm);
write_bandpass(likedata);
write_windows(likedata);
write_params(likedata);

return % END of function make_cosmomc_dataset


function write_dataset(likedata, simid)
% write_dataset(likedata, simid)
%
% Writes CosmoMC dataset meta file.

n_expt = numel(likedata.opt.expt);
n_field = numel(likedata.opt.fields);
% Create file.
if simid > 0
  fname = [likedata.dataset likedata.datasplit likedata.bkopt ...
           '_' likedata.fidmod sprintf('_sim%03i.dataset', simid)];
else
  fname = [likedata.dataset likedata.datasplit likedata.bkopt ...
           '_' likedata.fidmod '.dataset'];
end
fid = open_data_file_and_print_header(fname, date, likedata.dataset, ...
                                      likedata.datasplit);
% Likelihood type.
fprintf(fid, 'like_approx = HL\n');
fprintf(fid, '\n');
% Map names and descriptions.
mapnamestr = 'map_names =';
mapfldstr = 'map_fields =';
for ii=1:n_expt
  for jj=1:n_field
    mapnamestr = [mapnamestr ' ' likedata.opt.expt{ii} '_' ...
                  likedata.opt.fields{jj}];
    mapfldstr = [mapfldstr ' ' likedata.opt.fields{jj}];
  end
end
fprintf(fid, [mapnamestr '\n']);
fprintf(fid, [mapfldstr, '\n']);
fprintf(fid, '\n');
% Ell bins.
fprintf(fid, 'binned = T\n');
fprintf(fid, 'nbins = %i\n', numel(likedata.opt.l));
fprintf(fid, '\n');
% Ell range for theory.
fprintf(fid, 'cl_lmin = 2\n');
fprintf(fid, 'cl_lmax = 600\n');
fprintf(fid, '\n');
% Generate bandpower ordering.
order = expt_field_order(n_expt, n_field);
bpstr = '';
for s=order
  bpstr = [bpstr ' ' likedata.opt.expt{s.e1} '_' likedata.opt.fields{s.f1} ...
           'x' likedata.opt.expt{s.e2} '_' likedata.opt.fields{s.f2}];
end
% Fiducial model bandpower file.
fname = [likedata.dataset likedata.datasplit likedata.bkopt ...
         '_fiducial_' likedata.fidmod '.dat'];
fprintf(fid, 'cl_fiducial_file = %s\n', fname);
fprintf(fid, 'cl_fiducial_order =%s\n', bpstr);
fprintf(fid, 'cl_fiducial_includes_noise = F\n');
fprintf(fid, '\n');
% Read data bandpower file.
if simid > 0
  fname = [likedata.dataset likedata.datasplit likedata.bkopt ...
           sprintf('_cl_hat_sim%03i.dat', simid)];
else
  fname = [likedata.dataset likedata.datasplit likedata.bkopt ...
           '_cl_hat.dat'];
end
fprintf(fid, 'cl_hat_file = %s\n', fname);
fprintf(fid, 'cl_hat_order =%s\n', bpstr);
fprintf(fid, 'cl_hat_includes_noise = F\n');
fprintf(fid, '\n');
% Noise bias bandpower file.
fname = [likedata.dataset likedata.datasplit likedata.bkopt ...
         '_noise.dat'];
fprintf(fid, 'cl_noise_file = %s\n', fname);
fprintf(fid, 'cl_noise_order =%s\n', bpstr);
fprintf(fid, '\n');
% Bandpower covariance matrix.
fname = [likedata.dataset likedata.datasplit likedata.bkopt ...
         '_covmat_' likedata.fidmod '.dat'];
fprintf(fid, 'covmat_fiducial = %s\n', fname);
fprintf(fid, 'covmat_cl =%s\n', bpstr);
fprintf(fid, '\n');
% Bandpower window functions.
fname = ['windows/' likedata.dataset likedata.datasplit likedata.bkopt ...
         '_bpwf_bin%u.txt'];
fprintf(fid, 'bin_window_files = %s\n', fname);
fprintf(fid, 'bin_window_in_order =%s\n', bpstr);
fprintf(fid, '\n');
% Experiment bandpasses.
for ii=1:n_expt
  for jj=1:n_field
    fprintf(fid, ['bandpass[' likedata.opt.expt{ii} '_' likedata.opt.fields{jj} ...
                  '] = bandpass_' likedata.opt.expt{ii} '.txt\n']);
  end
end
fprintf(fid, '\n');
% Foreground parameters.
fname = [likedata.dataset '.paramnames'];
fprintf(fid, 'nuisance_params = %s\n', fname);
% Done.
fclose(fid);

return % END of function write_dataset


function write_Clhat(likedata, Clhat, simid)
% write_Clhat(likedata, Clhat, simid)
%
% Writes CosmoMC real data bandpowers file.

n_expt = numel(likedata.opt.expt);
n_field = numel(likedata.opt.fields);
% Create file.
if simid > 0
  fname = [likedata.dataset likedata.datasplit likedata.bkopt ...
           sprintf('_cl_hat_sim%03i.dat', simid)];
else
  fname = [likedata.dataset likedata.datasplit likedata.bkopt '_cl_hat.dat'];
end
fid = open_data_file_and_print_header(fname, date, likedata.dataset);
% Generate bandpower ordering.
order = expt_field_order(n_expt, n_field);
bpstr = '';
for s=order
  bpstr = [bpstr ' ' likedata.opt.expt{s.e1} '_' likedata.opt.fields{s.f1} ...
           'x' likedata.opt.expt{s.e2} '_' likedata.opt.fields{s.f2}];
end
fprintf(fid, '#%s\n', bpstr);
% Print bandpowers.
fmtstr = ['%i' repmat(' %0.6g', 1, numel(order)) '\n'];
for ii=1:numel(likedata.opt.l)
  fprintf(fid, fmtstr, ii, Clhat(ii,:));
end
fprintf(fid, '\n');
% Done.
fclose(fid);

return % END of function write_Clhat


function write_Cfl(likedata, C_fl)
% write_Cfl(likedata, C_fl)
% 
% Write CosmoMC fiducial model bandpowers file.

n_expt = numel(likedata.opt.expt);
n_field = numel(likedata.opt.fields);
% Create file.
fname = [likedata.dataset likedata.datasplit likedata.bkopt ...
         '_fiducial_' likedata.fidmod '.dat'];
fid = open_data_file_and_print_header(fname, date, likedata.dataset);
% Generate bandpower ordering.
order = expt_field_order(n_expt, n_field);
bpstr = '';
for s=order
  bpstr = [bpstr ' ' likedata.opt.expt{s.e1} '_' likedata.opt.fields{s.f1} ...
           'x' likedata.opt.expt{s.e2} '_' likedata.opt.fields{s.f2}];
end
fprintf(fid, '#%s\n', bpstr);
% Print bandpowers.
fmtstr = ['%i' repmat(' %0.6g', 1, numel(order)) '\n'];
for ii=1:numel(likedata.opt.l)
  fprintf(fid, fmtstr, ii, C_fl(ii,:));
end
fprintf(fid, '\n');
% Done.
fclose(fid);

return % END of function write_Cfl


function write_Nl(likedata)
% write_Nl(likedata)
%
% Writes CosmoMC noise bias bandpowers file.

n_expt = numel(likedata.opt.expt);
n_field = numel(likedata.opt.fields);
% Create file.
fname = [likedata.dataset likedata.datasplit likedata.bkopt ...
         '_noise.dat'];
fid = open_data_file_and_print_header(fname, date, likedata.dataset);
% Generate bandpower ordering.
order = expt_field_order(n_expt, n_field);
bpstr = '';
for s=order
  bpstr = [bpstr ' ' likedata.opt.expt{s.e1} '_' likedata.opt.fields{s.f1} ...
           'x' likedata.opt.expt{s.e2} '_' likedata.opt.fields{s.f2}];
end
fprintf(fid, '#%s\n', bpstr);
% Print bandpowers.
fmtstr = ['%i' repmat(' %0.6g', 1, numel(order)) '\n'];
for ii=1:numel(likedata.opt.l)
  fprintf(fid, fmtstr, ii, likedata.N_l(ii,:));
end
fprintf(fid, '\n');
% Done.
fclose(fid);

return % END of function write_Nl


function write_bpcm(likedata, bpcm)
% write_bpcm(likedata, bpcm)
%
% Writes CosmoMC bandpower covariance matrix file.

n_expt = numel(likedata.opt.expt);
n_field = numel(likedata.opt.fields);
% Create file.
fname = [likedata.dataset likedata.datasplit likedata.bkopt ...
         '_covmat_' likedata.fidmod '.dat'];
fid = open_data_file_and_print_header(fname, date, likedata.dataset);
% Generate bandpower ordering.
order = expt_field_order(n_expt, n_field);
bpstr = '';
for s=order
  bpstr = [bpstr ' ' likedata.opt.expt{s.e1} '_' likedata.opt.fields{s.f1} ...
           'x' likedata.opt.expt{s.e2} '_' likedata.opt.fields{s.f2}];
end
fprintf(fid, '#%s\n', bpstr);
% Print bandpower covariance matrix.
fmtstr = [repmat(' %0.6g', 1, numel(order)*numel(likedata.opt.l)) '\n'];
for ii=1:numel(order)*numel(likedata.opt.l)
  fprintf(fid, fmtstr, bpcm(ii,:));
end
fprintf(fid, '\n');
% Done.
fclose(fid);

return % END of function write_bpcm


function write_bandpass(likedata)
% write_bandpass(likedata)
%
% Writes CosmoMC experiment bandpass files.

% Loop over experiments.
for ii=1:numel(likedata.opt.expt)
  % Create file.
  fname = ['bandpass_' likedata.opt.expt{ii} '.txt'];
  fid = open_data_file_and_print_header(fname, date, likedata.dataset);
  % Print bandpass.
  for jj=1:size(likedata.bandpass{ii}, 1)
    fprintf(fid, '%0.3f %0.6g\n', likedata.bandpass{ii}(jj,:));
  end
  fprintf(fid, '\n');
  % Done.
  fclose(fid);
end

return % END of function write_bandpass


function write_windows(likedata)
% write_windows(likedata)
%
% Write CosmoMC bandpower window function files.

n_expt = numel(likedata.opt.expt);
n_field = numel(likedata.opt.fields);
% Check that windows directory exists.
if ~exist('windows', 'dir')
  mkdir('windows');
end
% Generate bandpower ordering.
order = expt_field_order(n_expt, n_field);
bpstr = '';
for s=order
  bpstr = [bpstr ' ' likedata.opt.expt{s.e1} '_' likedata.opt.fields{s.f1} ...
           'x' likedata.opt.expt{s.e2} '_' likedata.opt.fields{s.f2}];
end
fmtstr = ['%i' repmat(' %0.6g', 1, numel(order)) '\n'];
% Loop over ell bins.
for ii=1:numel(likedata.opt.l)
  % Create file.
  fname = ['windows/' likedata.dataset likedata.datasplit likedata.bkopt ...
           sprintf('_bpwf_bin%i.txt', ii)];
  fid = open_data_file_and_print_header(fname, date, likedata.dataset);
  % Print bandpower ordering.
  fprintf(fid, '#%s\n', bpstr);
  % Print bandpower window functions.
  for jj=1:numel(likedata.bpwf.l)
    fprintf(fid, fmtstr, likedata.bpwf.l(jj), ...
            likedata.bpwf.Cs_l(jj,1+(ii-1)*numel(order):ii*numel(order)));
  end
  fprintf(fid, '\n');
  % Done.
  fclose(fid);
end

return % END of function write_windows


function write_params(likedata)
% write_params(likedata)
%
% Writes file listing foreground/nuisance parameters for likelihood.

% Open file for writing.
fname = [likedata.dataset '.paramnames'];
fid = fopen(fname, 'w');
% Write parameters and latex code.
fprintf(fid, 'BBdust         A_{B,\\mathrm{dust}}\n');
fprintf(fid, 'BBsync         A_{B,\\mathrm{sync}}\n');
fprintf(fid, 'BBalphadust    \\alpha_{B,\\mathrm{dust}}\n');
fprintf(fid, 'BBbetadust     \\beta_{B,\\mathrm{dust}}\n');
fprintf(fid, 'BBTdust        T_{\\mathrm{dust}}\n');
fprintf(fid, 'BBalphasync    \\alpha_{B,\\mathrm{sync}}\n');
fprintf(fid, 'BBbetasync     \\beta_{B,\\mathrm{sync}}\n');
fprintf(fid, 'BBdustsynccorr \\epsilon_{\\mathrm{dust,sync}}\n');
fprintf(fid, 'EEtoBB_dust     EE_{\\mathrm{dust}}/BB_{\\mathrm{dust}}\n');
fprintf(fid, 'EEtoBB_sync     EE_{\\mathrm{sync}}/BB_{\\mathrm{sync}}\n');
% Done.
fclose(fid);

return % END of function write_params


function fid = open_data_file_and_print_header(filename, day, dataset, datasplit)
% open_data_file_and_print_header(filename, day, dataset, datasplit
%
% Open file to write and print standard header.

if nargin < 4
  datasplit = '';
end

% Open file for writing.
fid = fopen(filename, 'w');
% Analysis-specific header comments.
switch dataset
  case 'BKPlanck'
    fprintf(fid, '# BICEP2/Keck Array and Planck Joint Analysis January 2015 Data Products\n');
    fprintf(fid, ['# The BICEP2/Keck and Planck Collaborations, ' ...
                  'A Joint Analysis of BICEP2/Keck Array and Planck Data\n']);
  case 'BK14'
    fprintf(fid, '# BICEP2/Keck Array October 2015 Data Products\n');
    fprintf(fid, ['# BICEP2/Keck Array VI: Improved Constraints on ' ...
                  'Cosmology and Foregrounds When Adding 95 GHz Data ' ...
                  'from Keck Array\n']);
end
% General header comments.
fprintf(fid, '# http://bicepkeck.org/\n#\n');
fprintf(fid, '# File: %s\n', filename);
fprintf(fid, '# Date: %s\n#\n', datestr(day, 'yyyy-mm-dd'));
% Comments for .dataset files.
if ~isempty(strfind(filename, '.dataset'))
  switch dataset
    case 'BKPlanck'
      fprintf(fid, ['# Dataset definition file for the multi-component likelihood ' ...
                    'described in Section III of A Joint Analysis of BICEP2/Keck ' ...
                    'Array and Planck Data.\n']);
      fprintf(fid, ['# Examples of how to use this dataset are available at ' ...
                    'http://bicepkeck.org\n']);
      fprintf(fid, ['# This file is also available in CosmoMC ' ...
                    '(http://cosmologist.info/cosmomc/, Jan 2015 version).\n']);
      if ~isempty(datasplit) && ~strcmp(datasplit, '_auto')
        fprintf(fid, ['# This file uses the %s data splits for Planck ' ...
                      'single-frequency spectra.  See Section II A ' ...
                      'of A Joint Analysis of BICEP2/Keck Array and Planck Data ' ...
                      'for more details.\n'], datasplit(2:end));
      end
    case 'BK14'
      fprintf(fid, ['# Dataset definition file for the multi-component ' ...
                    'likelihood described in BICEP2/Keck Array VI\n']);
      fprintf(fid, ['# Examples of how to use this dataset are available ' ...
                    'at http://bicepkeck.org\n']);
      fprintf(fid, ['# This file is also available in CosmoMC ' ...
                    '(http://cosmologist.info/cosmomc/, version TBD).\n']);
  end
  fprintf(fid, '\n');
end
% Comments for cl_hat.dat files.
if ~isempty(strfind(filename, '_cl_hat.dat'))
  switch dataset
    case 'BKPlanck'
      fprintf(fid, ['# Data bandpowers for the multi-component likelihood ' ...
                    'described in Section III of A Joint Analysis of BICEP2/Keck Array ' ...
                    'and Planck Data.\n']);
    case 'BK14'
      fprintf(fid, ['# Data bandpowers for the multi-component likelihood ' ...
                    'described in BICEP2/Keck Array VI\n']);
  end
  fprintf(fid, '# See the comments in the .dataset file for further information.\n');
  fprintf(fid, '# Units are uK^2 CMB.\n');
  fprintf(fid, '\n');
end
% Comments for _fiducial_ files.
if ~isempty(strfind(filename, '_fiducial_'))
  switch dataset
    case 'BKPlanck'
      fprintf(fid, ['# Fiducial model bandpowers for the multi-component ' ...
                    'likelihood described in Section III of A Joint ' ...
                    'Analysis of BICEP2/Keck Array and Planck Data.\n']);
    case 'BK14'
      fprintf(fid, ['# Fiducial model bandpowers for the multi-component ' ...
                    'likelihood described in BICEP2/Keck Array VI.\n']);
  end
  fprintf(fid, '# See the comments in the .dataset file for further information.\n');
  fprintf(fid, '# Units are uK^2 CMB.\n');
  fprintf(fid, '\n');
end
% Comments for noise.dat files.
if ~isempty(strfind(filename, '_noise.dat'))
  switch dataset
    case 'BKPlanck'
      fprintf(fid, ['# Noise bandpowers for the multi-component ' ...
                    'likelihood described in Section III of A Joint ' ...
                    'Analysis of BICEP2/Keck Array and Planck Data.\n']);
    case 'BK14'
      fprintf(fid, ['# Noise bandpowers for the multi-component ' ...
                    'likelihood described in BICEP2/Keck Array VI.\n']);
  end
  fprintf(fid, '# See the comments in the .dataset file for further information.\n');
  fprintf(fid, '# Units are uK^2 CMB.\n');
  fprintf(fid, '\n');
end
% Comments for _covmat_ files.
if ~isempty(strfind(filename, '_covmat_'))
  switch dataset
    case 'BKPlanck'
      fprintf(fid, ['# Bandpower covariance matrix for the multi-component ' ...
                    'likelihood described in Section III of A Joint ' ...
                    'Analysis of BICEP2/Keck Array and Planck Data.\n']);
    case 'BK14'
      fprintf(fid, ['# Bandpower covariance matrix for the multi-component ' ...
                    'likelihood described in BICEP2/Keck Array VI.\n']);
  end
  fprintf(fid, '# See the comments in the .dataset file for further information.\n');
  fprintf(fid, '# Units are uK^4 CMB.\n');
  fprintf(fid, '\n');
end
% Comments for bandpass files.
if ~isempty(strfind(filename, 'bandpass'))
  expt = filename(10:strfind(filename, '.txt')-1);
  switch dataset
    case 'BKPlanck'
      fprintf(fid, ['# Experiment bandpasses for the multi-component ' ...
                    'likelihood described in Section III of A Joint ' ...
                    'Analysis of BICEP2/Keck Array and Planck Data.\n']);
    case 'BK14'
      fprintf(fid, ['# Experiment bandpasses for the multi-component ' ...
                    'likelihood described in BICEP2/Keck Array VI.\n']);
  end
  fprintf(fid, '# See the comments in the .dataset file for further information.\n');
  fprintf(fid, ['# This file provides the frequency spectrum for: %s.\n'], expt);
  fprintf(fid, '#   Column 1 = frequency in GHz\n');
  fprintf(fid, '#   Column 2 = response to a source with uniform spectral radiance\n');
  fprintf(fid, '# See Planck 2013 results IX and BK-II for further details.\n');
  fprintf(fid, '\n');
end
% Comments for window function files.
if ~isempty(strfind(filename, '_bpwf_bin'))
  bin = str2num(filename(strfind(filename, '.txt')-1));
  lval = 2.5 + 35 * bin;
  switch dataset
    case 'BKPlanck'
      fprintf(fid, ['# Bandpower window functions for the multi-component ' ...
                    'likelihood described in Section III of A Joint ' ...
                    'Analysis of BICEP2/Keck Array and Planck Data.\n']);
    case 'BK14'
      fprintf(fid, ['# Bandpower window functions for the multi-component ' ...
                    'likelihood described in BICEP2/Keck Array VI.\n']);
  end
  fprintf(fid, '# See the comments in the .dataset file for further information.\n');
  fprintf(fid, '# See Section 6.3 of BK-I for details of the window function calculation.\n');
  fprintf(fid, '# Bin: %i (ell~%5.1f)\n', bin, lval);
  fprintf(fid, '\n');
end

return % END of function open_data_file_and_print_header
