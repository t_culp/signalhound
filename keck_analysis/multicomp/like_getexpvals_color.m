function expv = like_getexpvals_color(param, likeopt, spec)
% lik = like_getexpvals_color(param, likeopt, spec)
%
% Version of like_getexpvals to use for color constraint analysis
% (similar to Jeff's color constraint from the BICEP2 results paper).
%
% Take model parameters, 150 GHz bandpower values, and likeopt struct, 
% then evaluate bandpower expectation values of the model for all 
% specified fields, experiments, and ell bins.
%
% [Input]
%   param    Multi-component model parameters:
%     param(1) = beta, effective spectral index
%     param(2) = A_L, lensing amplitude
%     param(3) = E/B ratio (default = 1)
%   likeopt  likeopt data structure (see below)
%   spec     150 GHz bandpowers. Default: Use first set of real data 
%            bandpowers in likeopt (usually BICEP2 BB auto-spectrum).
%
% == Relevant likeopt fields ==
% likeopt.expt    Array containing names and observing frequencies for each
%                 experiment included in the analysis.
% likeopt.fields  Cell array listing the CMB fields used for the
%                 analysis. Options are {'T', 'E', 'B'}, but only 'E' and 
%                 'B' are implemented in this model.
% likeopt.bpwf    Bandpower window functions for all experiments and
%                 experiment cross-spectra. Should be an array with
%                 length x*(x+1)/2, where x = (# of experiments),
%                 following H-L ordering.
% likeopt.mod     likeopt field containing CMB models from CAMB. 
%                 See like_read_theory.m
%
% [Output]
%   expv     Array of model bandpower expectation values with size (N,M),
%            where N is the number of ell bins and M = x*(x+1)/2
%            with x = (# of experiments) * (# of CMB fields).
%            Ordering of the second dimension follows the "H-L order", 
%            which is kind of complicated.
%            If the return_model argument (see above) is set, then the 
%            returned spectra are calculated for delta-ell=1, not bandpowers.

% Default spectrum, if none specified.
if nargin < 3
  spec = likeopt.real(:,1); %BICEP2 BB autospectrum.
end

% Get number of ell bins, CMB fields, and experiments.
n_ell = numel(likeopt.l);
n_field = numel(unique(likeopt.fields));
n_expt = numel(likeopt.expt);

% Theory spectra (for CMB and foregrounds) are not typically calculated
% for the same range of ell as the bandpower window functions. Calculate
% the range of indices to use for calculating expectation values.
if ~isfield(likeopt.mod, 'itheory')
  % Find the largest value of lmin between the theory spectra and
  % all bpwf. Find the smallest value of lmax.
  % Assumes that theory spectra and bpwf are all specified at
  % delta-ell = 1 resolution.
  lmin = lfine(1);
  lmax = lfine(end);
  for i=1:length(likeopt.bpwf)
    lmin = max(lmin, likeopt.bpwf(i).l(1));
    lmax = min(lmax, likeopt.bpwf(i).l(end));
  end
  % itheory gives the range of indices to use in the theory model.
  likeopt.mod.itheory = [find(lfine == lmin):find(lfine == lmax)];
  % ibpwf gives the range of indices to use in the bpwf.
  for i=1:length(likeopt.bpwf)
    likeopt.bpwf(i).ibpwf = [find(likeopt.bpwf(i).l == lmin): ...
                        find(likeopt.bpwf(i).l == lmax)];
  end
end

% Calculate scaling to T_CMB at each frequency.
% This is the scaling to apply to *maps*. For bandpower expectation 
% values, you need two powers of this scaling.
for i=1:numel(likeopt.expt) 
  scale(i) = (likeopt.expt(i).freq / likeopt.mod.fg_freq)^param(1) * ...
      antenna_to_Tcmb(likeopt.mod.fg_freq) / ...
      antenna_to_Tcmb(likeopt.expt(i).freq);
end

% Now loop over all pairwise combinations of (expt, field) using
% H-L vecp ordering.
order = expt_field_order(n_expt, n_field);
for s=order
  % Get bandpower window functions for this particular
  % combination of experiments. Bandpower window functions are
  % stored in likeopt with H-L type ordering.
  bpwfindex = s.e1;
  for k=1:(s.e2-s.e1)
    bpwfindex = bpwfindex + n_expt + 1 - k;
  end
  bpwf = likeopt.bpwf(bpwfindex);

  % Bandpower expectation values depend strongly on what type of
  % spectra this is. Only EE and BB are implemented! We expect EB
  % and TB to be zero (up to weird foregrounds). TT and TE will
  % be non-zero, but I'm not dealing with them.
  if (likeopt.fields{s.f1} == 'E') && (likeopt.fields{s.f2} == 'E')
    %% This is EE.
    % Spectrum includes an extra factor of param(3) 
    % 1. LCDM + Lensing (NOTE: likeopt.mod.lensing is zero for EE)
    model = likeopt.mod.lcdm(:,2) + ...
            param(2) * likeopt.mod.lensing(:,2);
    % 2. Apply window functions to calculate expv.
    mod_expv(:,s.i) = apply_bpwf(model(likeopt.mod.itheory), ...
                                 bpwf.Cs_l(bpwf.ibpwf,likeopt.l,3));	
    % 3. Add frequency-scaled bandpowers.
    expv(:,s.i) = spec * param(3) * scale(s.e1) * scale(s.e2) + ...
        mod_expv(:,s.i);
  elseif (likeopt.fields{s.f1} == 'B') && (likeopt.fields{s.f2} == 'B')
    %% This is BB.
    % 1. LCDM + Lensing
    model = likeopt.mod.lcdm(:,3) + ...
            param(2) * likeopt.mod.lensing(:,3);
    % 2. Apply window functions to calculate expv.
    mod_expv(:,s.i) = apply_bpwf(model(likeopt.mod.itheory), ...
                                 bpwf.Cs_l(bpwf.ibpwf,likeopt.l,4));
    % 3. Add frequency-scaled bandpowers.
    expv(:,s.i) = spec * scale(s.e1) * scale(s.e2) + mod_expv(:,s.i);
  else
    % For anything else, return zero.
    if return_model
      expv(:,s.i) = zeros(size(lfine));
    else
      expv(:,s.i) = zeros(n_ell, 1);
    end
  end   
end


%% SUBFUNCTIONS %%

%% apply_bpwf
%
% expv = apply_bpwf(model, bpwf)
%
% Calculates expectation values for the given model and bandpower
% window functions. 
% * model should be a vector with length [n_ell]. 
% * bpwf is an array with size [n_ell, n_band], where n_band is the
%   number of bandpowers to calculate.
%
% Returns a vector of [n_band] expectation values.
function expv = apply_bpwf(model, bpwf)

nband = size(bpwf, 2);
expv = sum(repmat(model, 1, nband) .* bpwf, 1);
expv = expv';
