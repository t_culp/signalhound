function loglik = like_hl(model, data, noise_bias, prep, real_part)
% loglik = like_hl(model, data, noise_bias, prep)
%
% Calculates log(likelihood) for the specified data and model using
% the Hamimeche-Lewis likelihood approximation.
%
% [Input]
%   model   Array of model bandpowers with shape [n_ell, n_spectra].
%           Calculate this using like_getexpvals.m
%   data    Array of real data bandpowers with shape [n_ell, n_spectra].
%           Obtained from pipeline data products.
%   noise_bias  Array of noise (and E->B leakage) bias values that
%           have been subtracted from the data. These values will
%           be added to both model and data.
%   prep    Precomputed fiducial model bandpowers and inverse
%           bandpower covariance matrix, obtained using
%           hamimeche_lewis_prep.m
%   real_part  Optional argument. Set to one to take the real part
%           of the log-likelihood before returning. Defaults to 0.
%           It is not recommended to do this!
%
% [Output]
%   loglik  Single value for log(likelihood).

if nargin < 5
  real_part = 0;
end

% Add noise bias to model and convert to symmetric matrix form.
C_l = ivecp((model + noise_bias)');
% Do the same thing to the real data.
C_l_hat = ivecp((data + noise_bias)');

% Calculate log(likelihood) using H-L approximation.
loglik = hamimeche_lewis_likelihood(prep, C_l_hat, C_l, 1);
if real_part
  loglik = real(loglik);
end
