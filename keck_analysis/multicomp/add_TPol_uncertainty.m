function likedata = add_TPol_uncertainty(uncer, likedata)
% bpcm_TPol = add_TPol_uncertainty(uncer, likedata)
%
% Calculates an additive contribution to the bandpower covariance matrix due 
% to the uncertainty of residual T->P leakage.
%
% [Input]
%   uncer     Residual uncertainty from imperfect subtraction of T->P leakage, 
%             in units of uK^2 for l*(l+1)C_l/2*pi. This should be an array 
%             with the same size and arrangement as the bandpower arrays 
%             stored in likedata (e.g. likedata.real).
%             If you pass an empty array as this argument, then we
%             will use results provided by Chris from Keck 2014
%             beam map sims, with the assumption that the
%             uncertainty is equal to 100% of the residual bias.
%   likedata  Data structure for multicomponent analysis.
%
% [Output]
%   likedata  Input likedata structure, but with additional contribution to 
%             bandpower covariance components from T->P leakage uncertainty.
%             The uncer argument will be stored in likedata.opt.uncer_TPol.
%           NOTE: If likedata.opt.uncer_TPol is already set, then this 
%                 function will refuse to add additional variance.

% If likedata.opt.uncer_TPol is already set, then exit.
if isfield(likedata.opt, 'uncer_TPol')
  disp(['[add_TPol_uncertainty] ERROR: ' ...
        'T->Pol residual uncertainty was already applied']);
  return
end

% Size of the problem.
nexpt = numel(likedata.opt.expt);
nfield = numel(likedata.opt.fields);
order = expt_field_order(nexpt, nfield);
l = likedata.opt.l;

% If argument uncer is specified, check that it is the same size as 
% likedata.real.
if ~isempty(uncer)
  if all(size(uncer) ~= size(likedata.real))
    disp(['[add_TPol_uncertainty] ERROR: ' ...
          'input array of uncertainty estimates has the wrong size']);
    return
  end
else
  % Initialize uncertainty estimate as an empty array.
  uncer = zeros(size(likedata.real));
  % Load results from Chris.
  filepath = '~csheehy/csl_bmsim_dp1102_matrix_keck2014.mat';
  bmru = load(filepath);
  % Chris' file contains residual uncertainty estimates for BK14_95 and
  % BK14_150 only.
  for s=order
    % BK14_95
    if strcmp(likedata.opt.expt(s.e1), 'BK14_95') && ...
        strcmp(likedata.opt.expt(s.e2), 'BK14_95')
      f1 = likedata.opt.fields(s.f1);
      f2 = likedata.opt.fields(s.f2);
      uncer(:,s.i) = bmru.csl100(l, aps_getbpind(f1, f2, 0));
    end
    % BK14_150
    if strcmp(likedata.opt.expt(s.e1), 'BK14_150') && ...
        strcmp(likedata.opt.expt(s.e2), 'BK14_150')
      f1 = likedata.opt.fields(s.f1);
      f2 = likedata.opt.fields(s.f2);
      uncer(:,s.i) = bmru.csl150(l, aps_getbpind(f1, f2, 0));
    end
  end
end
% Store the uncertainty estimates in likedata.opt.
likedata.opt.uncer_TPol = uncer;

% Bandpower covariance contribution from T->P leakage applies only
% to the diagonal of the noise-only component of the bpcm.
likedata.bpcov.noi = likedata.bpcov.noi + ...
    diag(reshape((uncer.^2)', numel(uncer), 1));

return % END of function add_TPol_uncertainty
