function likedata = likedata_add_expt(likedata, name, auto_Nl, expt_model)
% likedata = likedata_add_expt(likedata, name, auto_Nl, expt_model)
%
% Add a new experiment to the likedata structure with noise levels scaled from 
% an experiment that is already found in that structure.
% 
% The new experiment is based off a "model" experiment that already exists in 
% the likedata structure. It will have the same response to signals as this 
% model experiment, but you can specify alternate N_l to get different noise 
% levels. New experiment will have real bandpower values all set to zero and an 
% empty bandpass array, so you still need to specify these values.
%
% [Input]
%   likedata    Input likedata structure to be expanded.
%   name        Name for the new experiment. Defaults to 'NEW'.
%   auto_Nl     Noise bandpowers for the new experiment. This array should have 
%               size equal to [N_ell, N_fields]. For example, if your only 
%               field is 'B', then size is [N_ell, 1].
%   expt_model  The index of the "model" experiment to copy / modify.
%
% [Output]
%   likedata    New, extended likedata structure.

% If model experiment is not specified, use the first experiment.
if (nargin < 4) || isempty(expt_model)
  expt_model = 1;
end
% If model experiment is specified with the string experiment name,
% figure out which experiment number it corresponds to.
if isstr(expt_model)
  expt_model_str = expt_model;
  expt_model = find(strcmp(expt_model_str, likedata.opt.expt));
  if isempty(expt_model)
    disp(sprintf('[ERROR] %s not found in likedata.opt.expt', expt_model_str));
  end
end

% If new experiment name is not specified, call it 'NEW'
if (nargin < 2) || ~isstr(name)
  name = 'NEW';
end

% Get size and ordering for data structures.
nexpt = numel(likedata.opt.expt);
nfield = numel(likedata.opt.fields);
nell = numel(likedata.opt.l);
ordin = expt_field_order(nexpt, nfield);
nspecin = numel(ordin);
ordout = expt_field_order(nexpt+1, nfield);
nspecout = numel(ordout);

% If new experiment N_l are not specified, then copy the one from
% the model experiment.
model_Nl = likedata.N_l(:,[1:nfield]+nfield*(expt_model-1));
if (nargin < 3) || all(size(auto_Nl) ~= [nell, nfield])
  auto_Nl = model_Nl;
end

% Expand list of experiment names.
new_expt = likedata.opt.expt;
new_expt{nexpt+1} = name;

% Expand list of bandpasses with dummy entry.
new_bandpass = likedata.bandpass;
new_bandpass{nexpt+1} = [];

% Expand real bandpowers, noise bias bandpowers, bandpower window functions.
% * Real bandpower entries are just set to zero.
% * Noise bias bandpowers for auto-spectra are set to auto_Nl values.
% * Noise bias bandpowers for cross-spectra are set to zero.
% * For bandpower window functions, copy the window function that
%   you would get if you replace the new experiment with its model experiment.
% * For bpcm fiducial model (bpcov.C_fl), just copy the model experiment.
new_real = zeros(nell, nspecout);
new_Nl = zeros(nell, nspecout);
new_bpwf = zeros(numel(likedata.bpwf.l), nspecout*nell);
new_rwf = zeros(nell, nspecout);
new_Cfl = zeros(nell, nspecout);
for s=ordout
  if (s.e1 == nexpt+1) || (s.e2 == nexpt+1)
    % For any newly added auto or cross-spectrum, set real data
    % bandpowers to zero. These will presumably be filled in later
    % by user.
    new_real(:,s.i) = 0;

    if (s.e1 == s.e2) && (s.f1 == s.f2)
      % Auto-spectrum, use input noise bias.
      new_Nl(:,s.i) = auto_Nl(:,s.f1);
    else
      % Cross-spectrum, assume zero noise bias.
      new_Nl(:,s.i) = 0;
    end

    % Choose bandpower window function that we would get if we
    % looked up model experiment instead of new experiment.
    if s.e1 == nexpt+1
      s.e1 = expt_model;
    end
    if s.e2 == nexpt+1
      s.e2 = expt_model;
    end
    idx = likeopt_getxind(likedata.opt, s.e1, s.e2, s.f1, s.f2);
  else
    % Just copy existing entry.
    idx = likeopt_getxind(likedata.opt, s.e1, s.e2, s.f1, s.f2);
    new_real(:,s.i) = likedata.real(:,idx);
    new_Nl(:,s.i) = likedata.N_l(:,idx);
  end
  % Copy bandpower window function entries.
  new_bpwf(:,s.i+nspecout*[0:nell-1]) = likedata.bpwf.Cs_l(:,idx+nspecin*[0:nell-1]);
  new_rwf(:,s.i) = likedata.bpwf.rwf(:,idx);
  new_Cfl(:,s.i) = likedata.bpcov.C_fl(:,idx);
end

% Expand bandpower covariance matrix components.
new_sig = zeros(size(likedata.bpcov.sig));
new_noi = zeros(size(likedata.bpcov.noi));
new_sn1 = zeros(size(likedata.bpcov.sn1));
new_sn2 = zeros(size(likedata.bpcov.sn2));
new_sn3 = zeros(size(likedata.bpcov.sn3));
new_sn4 = zeros(size(likedata.bpcov.sn4));
for r=ordout
  if (r.e1 == nexpt+1) && (r.e2 ~= nexpt+1)
    % Use MOD x e2 in place of NEW x e2
    idx1 = likeopt_getxind(likedata.opt, expt_model, r.e2, r.f1, r.f2);
    % Scale factors for entries.
    sig_scale1 = ones(nell, 1);
    noi_scale1 = sqrt(auto_Nl(:,r.f1) ./ model_Nl(:,r.f1));
    sn1_scale1 = ones(nell, 1);
    sn2_scale1 = ones(nell, 1);
    sn3_scale1 = sqrt(auto_Nl(:,r.f1) ./ model_Nl(:,r.f1));
    sn4_scale1 = sqrt(auto_Nl(:,r.f1) ./ model_Nl(:,r.f1));
    % Might need to shuffle the various sn matrices depending on
    % expt ordering.
    swap1 = ((expt_model < r.e2) ~= (nexpt+1 < r.e2));
    % If e2 == expt_model, need to note that this isn't really an
    % auto-spectrum.
    notauto1 = (r.e2 == expt_model);
  elseif (r.e1 ~= nexpt+1) && (r.e2 == nexpt+1)
    % Use e1 x MOD in place of e1 x NEW
    idx1 = likeopt_getxind(likedata.opt, r.e1, expt_model, r.f1, r.f2);
    % Scale factors for entries.
    sig_scale1 = ones(nell, 1);
    noi_scale1 = sqrt(auto_Nl(:,r.f2) ./ model_Nl(:,r.f2));
    sn1_scale1 = sqrt(auto_Nl(:,r.f2) ./ model_Nl(:,r.f2));
    sn2_scale1 = sqrt(auto_Nl(:,r.f2) ./ model_Nl(:,r.f2));
    sn3_scale1 = ones(nell, 1);
    sn4_scale1 = ones(nell, 1);
    % Might need to shuffle the various sn matrices depending on
    % expt ordering.
    swap1 = ((expt_model < r.e1) ~= (nexpt+1 < r.e1));
    % If e1 == expt_model, need to note that this isn't really an
    % auto-spectrum.
    notauto1 = (r.e1 == expt_model);
  elseif (r.e1 == nexpt+1) && (r.e2 == nexpt+1)
    % Use MOD x MOD in place of NEW x NEW
    idx1 = likeopt_getxind(likedata.opt, expt_model, expt_model, r.f1, r.f2);
    % Scale factors for entries.
    sig_scale1 = ones(nell, 1);
    noi_scale1 = sqrt(auto_Nl(:,r.f1) ./ model_Nl(:,r.f1)) .* ...
        sqrt(auto_Nl(:,r.f2) ./ model_Nl(:,r.f2));
    sn1_scale1 = sqrt(auto_Nl(:,r.f2) ./ model_Nl(:,r.f2));
    sn2_scale1 = sqrt(auto_Nl(:,r.f2) ./ model_Nl(:,r.f2));
    sn3_scale1 = sqrt(auto_Nl(:,r.f1) ./ model_Nl(:,r.f1));
    sn4_scale1 = sqrt(auto_Nl(:,r.f1) ./ model_Nl(:,r.f1));
    swap1 = false;
    notauto1 = false;
  else
    % Just copy over the entry.
    idx1 = likeopt_getxind(likedata.opt, r.e1, r.e2, r.f1, r.f2);
    sig_scale1 = ones(nell, 1);
    noi_scale1 = ones(nell, 1);
    sn1_scale1 = ones(nell, 1);
    sn2_scale1 = ones(nell, 1);
    sn3_scale1 = ones(nell, 1);
    sn4_scale1 = ones(nell, 1);
    swap1 = false;
    notauto1 = false;
  end
  % Expand index over ell.
  idx1 = idx1 + nspecin * [0:nell-1];

  for s=ordout
    if (s.e1 == nexpt+1) && (s.e2 ~= nexpt+1)
      % Use MOD x e2 in place of NEW x e2
      idx2 = likeopt_getxind(likedata.opt, expt_model, s.e2, s.f1, s.f2);
      % Scale factors for entries.
      sig_scale2 = ones(nell, 1);
      noi_scale2 = sqrt(auto_Nl(:,s.f1) ./ model_Nl(:,s.f1));
      sn1_scale2 = ones(nell, 1);
      sn2_scale2 = sqrt(auto_Nl(:,s.f1) ./ model_Nl(:,s.f1));
      sn3_scale2 = ones(nell, 1);
      sn4_scale2 = sqrt(auto_Nl(:,s.f1) ./ model_Nl(:,s.f1));
      % Might need to shuffle the various sn matrices depending on
      % expt ordering.
      swap2 = ((expt_model < s.e2) ~= (nexpt+1 < s.e2));
      % If e2 == expt_model, need to note that this isn't really an
      % auto-spectrum.
      notauto2 = (s.e2 == expt_model);
    elseif (s.e1 ~= nexpt+1) && (s.e2 == nexpt+1)
      % Use e1 x MOD in place of e1 x NEW
      idx2 = likeopt_getxind(likedata.opt, s.e1, expt_model, s.f1, s.f2);
      % Scale factors for entries.
      sig_scale2 = ones(nell, 1);
      noi_scale2 = sqrt(auto_Nl(:,s.f2) ./ model_Nl(:,s.f2));
      sn1_scale2 = sqrt(auto_Nl(:,s.f2) ./ model_Nl(:,s.f2));
      sn2_scale2 = ones(nell, 1);
      sn3_scale2 = sqrt(auto_Nl(:,s.f2) ./ model_Nl(:,s.f2));
      sn4_scale2 = ones(nell, 1);
      % Might need to shuffle the various sn matrices depending on
      % expt ordering.
      swap2 = ((expt_model < s.e1) ~= (nexpt+1 < s.e1));
      % If e2 == expt_model, need to note that this isn't really an
      % auto-spectrum.
      notauto2 = (s.e1 == expt_model);
    elseif (s.e1 == nexpt+1) && (s.e1 == nexpt+1)
      % Use MOD x MOD in place of NEW x NEW
      idx2 = likeopt_getxind(likedata.opt, expt_model, expt_model, s.f1, s.f2);
      % Scale factors for entries.
      sig_scale2 = ones(nell, 1);
      noi_scale2 = sqrt(auto_Nl(:,s.f1) ./ model_Nl(:,s.f1)) .* ...
          sqrt(auto_Nl(:,s.f2) ./ model_Nl(:,s.f2));
      sn1_scale2 = sqrt(auto_Nl(:,s.f2) ./ model_Nl(:,s.f2));
      sn2_scale2 = sqrt(auto_Nl(:,s.f1) ./ model_Nl(:,s.f1));
      sn3_scale2 = sqrt(auto_Nl(:,s.f2) ./ model_Nl(:,s.f2));
      sn4_scale2 = sqrt(auto_Nl(:,s.f1) ./ model_Nl(:,s.f1));
      swap2 = false;
      notauto2 = false;
    else
      % Just copy over the entry.
      idx2 = likeopt_getxind(likedata.opt, s.e1, s.e2, s.f1, s.f2);
      sig_scale2 = ones(nell, 1);
      noi_scale2 = ones(nell, 1);
      sn1_scale2 = ones(nell, 1);
      sn2_scale2 = ones(nell, 1);
      sn3_scale2 = ones(nell, 1);
      sn4_scale2 = ones(nell, 1);
      swap2 = false;
      notauto2 = false;
    end
    % Expand index over ell.
    idx2 = idx2 + nspecin * [0:nell-1];
    
    % Copy covariance entries.
    i1 = r.i + nspecout * [0:nell-1];
    i2 = s.i + nspecout * [0:nell-1];
    new_sig(i1,i2) = (sig_scale1 * sig_scale2') .* likedata.bpcov.sig(idx1,idx2);
    new_noi(i1,i2) = (noi_scale1 * noi_scale2') .* likedata.bpcov.noi(idx1,idx2);
    if notauto1 && notauto2
      % Using Cov(a*a, a*a) in place of Cov(a*b, a*b) need an extra
      % factor of 0.5 correction for difference between variance of
      % an auto-spectrum and variance of a cross-spectrum of
      % uncorrelated fields.
      new_noi(i1,i2) = 0.5 * new_noi(i1,i2);
    end
    if swap1 && ~swap2
      % Order swapped for first bandpower entry.
      % sn1 <-> sn3, sn2 <-> sn4
      new_sn1(i1,i2) = (sn1_scale1 * sn1_scale2') .* likedata.bpcov.sn3(idx1, idx2);
      new_sn2(i1,i2) = (sn2_scale1 * sn2_scale2') .* likedata.bpcov.sn4(idx1, idx2);
      new_sn3(i1,i2) = (sn3_scale1 * sn3_scale2') .* likedata.bpcov.sn1(idx1, idx2);
      new_sn4(i1,i2) = (sn4_scale1 * sn4_scale2') .* likedata.bpcov.sn2(idx1, idx2);
    elseif ~swap1 && swap2
      % Order swapped for second bandpower entry.
      % sn1 <-> sn2, sn3 <-> sn4
      new_sn1(i1,i2) = (sn1_scale1 * sn1_scale2') .* likedata.bpcov.sn2(idx1, idx2);
      new_sn2(i1,i2) = (sn2_scale1 * sn2_scale2') .* likedata.bpcov.sn1(idx1, idx2);
      new_sn3(i1,i2) = (sn3_scale1 * sn3_scale2') .* likedata.bpcov.sn4(idx1, idx2);
      new_sn4(i1,i2) = (sn4_scale1 * sn4_scale2') .* likedata.bpcov.sn3(idx1, idx2);
    elseif swap1 && swap2
      % Order swapped for both entries.
      % sn1 <-> sn4, sn2 <-> sn3
      new_sn1(i1,i2) = (sn1_scale1 * sn1_scale2') .* likedata.bpcov.sn4(idx1, idx2);
      new_sn2(i1,i2) = (sn2_scale1 * sn2_scale2') .* likedata.bpcov.sn3(idx1, idx2);
      new_sn3(i1,i2) = (sn3_scale1 * sn3_scale2') .* likedata.bpcov.sn2(idx1, idx2);
      new_sn4(i1,i2) = (sn4_scale1 * sn4_scale2') .* likedata.bpcov.sn1(idx1, idx2);
    else
      % No swaps!
      new_sn1(i1,i2) = (sn1_scale1 * sn1_scale2') .* likedata.bpcov.sn1(idx1, idx2);
      new_sn2(i1,i2) = (sn2_scale1 * sn2_scale2') .* likedata.bpcov.sn2(idx1, idx2);
      new_sn3(i1,i2) = (sn3_scale1 * sn3_scale2') .* likedata.bpcov.sn3(idx1, idx2);
      new_sn4(i1,i2) = (sn4_scale1 * sn4_scale2') .* likedata.bpcov.sn4(idx1, idx2);
    end
  end
end

% Copy new entries into likedata structure.
likedata.opt.expt = new_expt;
likedata.bandpass = new_bandpass;
likedata.real = new_real;
likedata.N_l = new_Nl;
likedata.bpwf.Cs_l = new_bpwf;
likedata.bpwf.rwf = new_rwf;
likedata.bpcov.C_fl = new_Cfl;
likedata.bpcov.sig = new_sig;
likedata.bpcov.noi = new_noi;
likedata.bpcov.sn1 = new_sn1;
likedata.bpcov.sn2 = new_sn2;
likedata.bpcov.sn3 = new_sn3;
likedata.bpcov.sn4 = new_sn4;

% Recalculate bpcm masks.
likedata.bpcov = mask_bpcm(likedata.bpcov, likedata.opt);
