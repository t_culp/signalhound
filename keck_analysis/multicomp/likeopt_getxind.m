function xind = likeopt_getxind(likeopt, expt1, expt2, fld1, fld2)
% xind = likeopt_getxind(likeopt, expt1, expt2, fld1, fld2)
%
% Returns the index of a particular cross-spectrum in a likeopt
% data structure.
%
% [Input]
%   likeopt  likeopt data structure (needs likeopt.expt, likeopt.fields)
%   expt1    Numerical index of first experiment in likeopt.expt.
%   expt2    Numerical index of second experiment in likeopt.expt.
%   fld1     Numerical index of first field in likeopt.fields. Defaults 
%            to 1 if not specified.
%   fld2     Numerical index of second field in likeopt.fields. Defaults 
%            to 1 if not specified.
%
%   The last four arguments can be arrays of indices. If some of the 
%   arguments are shorter, then they will be expanded by repeating the 
%   last entry to match the longest.
%
% [Output]
%   xind     Index of the desired spectrum. 
%            * Real data bandpowers = likeopt.real(:,xind)
%            * Noise bias bandpowers = likeopt.N_l(:,xind)
%            Bandpower window functions and bandpower covariance matrix are 
%            also stored with this ordering, but repeated for each ell bin.

% NOTE: In the likeopt ordering, we never have combinations with 
% expt1 > expt2. If such a combination is requested, this function will
% simultaneously flip expt1<=>expt2 and field1<=>field2. This means that if 
% you ask for B1_E x B2_B, it would return the index to B2_B x B1_E, which 
% is really the same thing.

% Default values for fld1, fld2
if nargin < 4
  fld1 = 1;
end
if nargin < 5
  fld2 = 1;
end

% Expand arguments out to match the length of the longest one.
N = max([numel(expt1), numel(expt2), numel(fld1), numel(fld2)]);
if N > 1
  expt1(end:N) = expt1(end);
  expt2(end:N) = expt2(end);
  fld1(end:N) = fld1(end);
  fld2(end:N) = fld2(end);
end

% Get number of experiments, fields.
n_expt = numel(likeopt.expt);
n_field = numel(likeopt.fields);
% Get vecp ordering in array format.
order = expt_field_order(n_expt, n_field);
order = reshape(struct2array(order), 5, numel(order));

% Loop over input arguments and calculate indices.
for i=1:N
  if expt1(i) > expt2(i)
    % If expt1 > expt2, swap 1 & 2 for both expt and field.
    xind(i) = find((order(1,:) == expt2(i)) & (order(2,:) == fld2(i)) & ...
                   (order(3,:) == expt1(i)) & (order(4,:) == fld1(i)), 1);
  elseif (expt1(i) == expt2(i)) && (fld1(i) > fld2(i))
    % If expt1 == expt2 but fld1 ~= fld2, then you get the same
    % spectra for either ordering of (fld1,fld2). But only the
    % entry with fld1 < fld2 is found in order.
    xind(i) = find((order(1,:) == expt1(i)) & (order(2,:) == fld2(i)) & ...
                   (order(3,:) == expt2(i)) & (order(4,:) == fld1(i)), 1);
  else
    xind(i) = find((order(1,:) == expt1(i)) & (order(2,:) == fld1(i)) & ...
                   (order(3,:) == expt2(i)) & (order(4,:) == fld2(i)), 1);
  end
end
