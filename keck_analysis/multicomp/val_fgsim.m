% val_fgsim.m
% Extracts aps from Gaussian foreground sims in the format needed by
% the multicomponent analysis.
% KDA (adapted from CB), committed 2014-11-18

clear;
% Create likeopt structure.
% B2 files
%apsfile = ['aps/0751x1613/xxx4_a_filtp3_weight3_gs_dp1100_jack0_' ...
%           'xxx5_a_filtp3_weight3_gs_dp1100_jack0_' ...
%           'xxx6_a_filtp3_weight3_gs_dp1100_jack0_' ...
%           'xxx6_a_filtp3_weight3_gs_dp1100_jack0_' ...
%           'xxx6_a_filtp3_weight3_gs_dp1100_jack4_matrix'];
%finalfile = ['final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_' ...
%             'real_a_filtp3_weight3_gs_dp1100_jack0_' ...
%             'real_a_filtp3_weight3_gs_dp1100_jack4_' ...
%             'pureB_matrix_directbpwf'];
% Keck files
%apsfile = ['aps/1353x1614/xxx4_ab_filtp3_weight3_gs_dp1100_jack0_' ...
%           'xxx5_ab_filtp3_weight3_gs_dp1100_jack0_' ...
%           'xxx6_ab_filtp3_weight3_gs_dp1100_jack0_' ...
%           'xxx6_ab_filtp3_weight3_gs_dp1100_jack0_' ...
%           'xxx6_ab_filtp3_weight3_gs_dp1100_jack4_matrix'];
%finalfile = ['final/1353x1614/real_ab_filtp3_weight3_gs_dp11022_jack0_' ...
%             'real_ab_filtp3_weight3_gs_dp1100_jack0_' ...
%             'real_ab_filtp3_weight3_gs_dp1100_jack4_' ...
%             'pureB_matrix_directbpwf'];
%likeopt.l = [2:6];
%likeopt.fields = {'B'};
%likeopt.Planck_datasplit = 0;
%likeopt.offdiag = 1;
%likeopt = like_read_simset(apsfile, finalfile, likeopt);
%likeopt = like_read_theory(likeopt);
%likeopt.mod.dust_freq = 353;
%likeopt = likeopt_select(likeopt, [1:8], [], []);
%likeopt = like_read_bandpass(likeopt);
%likeopt = mask_bpcm(likeopt);

% save likeopt
%save('simfiles/fgsim/keck/likeopt.mat');

% only need to generate the likopt once for a given set of
% experiments...if running additional models, just reload the file
% generated above.
load('simfiles/fgsim/keck/likeopt.mat');

% Load Gaussian foreground sims for desired model.
% B2 sims
%fg = load(['final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_' ...
%           'real_a_filtp3_weight3_gs_dp1100_jack0_' ...
%           'pureB_matrix_directbpwf_fgm0']);
% Keck sims     
fg = load(['final/1353x1614/real_ab_filtp3_weight3_gs_dp11022_jack0_' ...
           'real_ab_filtp3_weight3_gs_dp1100_jack0_' ...
           'pureB_matrix_directbpwf_fgm6']);
fgsim = extract_fgsims(fg.r, likeopt, 'simr');
nsim = size(fgsim, 3);

% Precalculate H-L likelihood terms.
%p = [0.2, 1, 0, 0, -3, -0.6, 1.59, -0.42, 1, 1, 0, 19.6];
p = [0.2, 1, 0, 0, -3, -0.6, 1.6, -0.4, 1, 1, 0, 19.6];
C_fl = like_getexpvals(p, likeopt);
[ell, rms] = model_rms(p, likeopt);
bpcm = scale_bpcm(rms, likeopt);
prep = hamimeche_lewis_prepare(ivecp((C_fl + likeopt.N_l)'), bpcm);

p_sims=[0.2, 1, 0, 0, -3, -0.6, 1.6, -0.4, 1, 1, 0, 19.6]; % m0
%p_sims=[0.05, 1, 0, 2.5, -3, -0.6, 1.6, -0.4, 1, 1, 0, 19.6]; % m1
%p_sims=[0.05, 1, 1e-4, 2.5, -3, -0.6, 1.6, -0.4, 1, 1, 0, 19.6]; % m2
%p_sims=[0.1, 1, 1e-3, 5, -3, -0.6, 1.6, -0.4, 1, 1, 0, 19.6]; % m3
%p_sims=[0, 1, 1e-4, 2.5, -3, -0.6, 1.6, -0.4, 1, 1, 0, 19.6]; % m5b
%p_sims=[0.1, 1, 1e-3, 5, -3, -0.4, 1.6, -0.4, 1, 1, 0.5, 19.6]; % m6

% CHANGE THIS before running a new simset.
%save('simfiles/fgsim/keck/m0_prep.mat')


