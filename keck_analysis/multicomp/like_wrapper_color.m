% KDA 2014-06-24

clear;

likeopt = get_default_likeopt;
likeopt.fields = {'B'};         % BB only
likeopt.l = 2:6;                % Use standard 5 ell bins.
%likeopt.expt(3).name = 'WMAP_K';
%likeopt.expt(3).freq = 23;

% Get data products from final .mat files.
likeopt = like_read_data(likeopt);

% Read theory spectra from CAMB files.
likeopt = like_read_theory(likeopt);

% Grid evaluation over beta and A_L -- eventually some sampler here
beta_range = [-4:0.05:4];
%A_l_range = [-0.5:0.05:3.5];
% Other model parameters -- set E/B mixing to 1.
% We will be varying the first (or first two) element(s) of this vector;
% other elements will remain fixed to these values.
p = [-1, 1, 1];

% For H-L likelihood, precompute fiducial model and inverse bpcm.
% Need to add noise bias to fiducial model bandpowers and convert
% to symmetric matrix before passing to hamimeche_lewis_prep.
prep = hamimeche_lewis_prepare(ivecp((likeopt.C_fl + likeopt.N_l)'), ...
                               likeopt.bpcm);

for i = 1:length(beta_range)
%  for j = 1:length(A_l_range)
    % Calculate model expectation values.
    p(1) = beta_range(i);    % beta value
%    p(2) = A_l_range(j);    % A_L value

    % Profile likelihood calculation. 
    % Search for bandpowers that maximize the likelihood for this p
    % vector, starting with the B2 BB autospectrum likeopt.real(:,1)
    [specmax, logLval] = fminsearch(...
      @(spec) -like_hl(like_getexpvals_color(p,likeopt,spec), ...
                       likeopt.real, likeopt.N_l, prep), ...
      likeopt.real(:,1));
    logl_hl(i) = -logLval;
  %  end
end

% Peak normalize.
logl_hl = logl_hl - max(logl_hl(:));
%[temp,midx_hl]= max(logl_hl(:));
%[xhl,yhl]=ind2sub(size(logl_hl),midx_hl);

% Call Jeff's code to plot comparison
out = color_constraint_b1xb2('sub_lcdm',1);
mlike = out.mlike - max(out.mlike);

figure(1); clf;
plot(beta_range,exp(logl_hl));
hold all;
plot(out.n,exp(mlike));
xlabel('Spectral index \beta_{excess}');
ylabel('Likelihood');
title('\beta likelihood, B2 X B1100 + B1100, excess over lensing');
legend('H-L (Kate)', 'H-L (Jeff)');

%figure(2); clf;
%contourf(beta_range, A_l_range, exp(logl_hl'), exp(-([1, 2]).^2/2));
%set(gca, 'YDir', 'normal')
%xlabel('\beta'); 
%ylabel('Lensing Amplitude A_L');
%title('r vs A_L, 2*log(L), BICEP2 BB');
%colorbar();
%hold on;
%plot([0 0.4], [1.05 1.05], 'k--');
%plot([0 0.4], [0.95 0.95], 'k--');
%scatter(beta_range(xhl),A_l_range(yhl),30,'k+');
%colormap cool
%caxis([0 1.5]);
