% Specify the ini file you want to farm out multiple times:
cd ~/cosmomcb2keck
inifile = 'inifiles/b2p_run6_fiducial.ini'
% strip it apart, we'll make subdirs to organize the whole thing
[p,fn,ext] = fileparts(inifile);
% We need multiple ini files to hand in different numbers as names for the chains.
% Those are going to be created in a subdir that is created from the inifile name:
run_dir = [p,'/',fn];
mkdir(run_dir);

% the number of farm jobs:
njobs = 1;

for ii = 1:njobs

  % create a run number for each job, eg _00001 for the first job:
  run = ['_',num2str(ii,'%.5d')];
  % create a new ini filename that has this number appended to the filename:
  inifile_run = [fn,run,ext];
  % this file is created run_dir directory, make the full name including the path...:
  fn_run = [run_dir,'/',inifile_run];
  % and make a copy:
  system_safe(['cp -f ',inifile,' ',fn_run])
  % the file_root needs to be altered in that file to make sure the chains are saved in separate
  % files. Grep for the relevant line:
  [s,file_root] = system_safe(['grep file_root ',fn_run])
  % strip off the trailing newline character:
  file_root = file_root(1:end-1);
  % append the run number to the file_root:
  file_root_run = [file_root,run];
  % and replace the current line in the file with the new line:
  system_safe(['sed -i ''s/',file_root,'/',file_root_run,'/'' ',fn_run])
  
  % now all that need to be done is to farm it out
  cmd = ['''./cosmomc ',fn_run,''''];
  cmd = ['system_safe(',cmd,')'];
  farmfile_dir = ['farmfiles/',fn];
  system_safe(['mkdir -p ',farmfile_dir]);
  farmit(farmfile_dir,cmd,'queue','serial_requeue,general','mem',10000,'maxtime',24*60,'submit',1)
end

