function bpcm = scale_bpcm(signal_amp, likedata)
% bpcm = scale_bpcm(signal_amp, likedata)
%
% Calculate semi-analytic bandpower covariance matrix, rescaled to a new 
% signal model.
%
% [Input]
%   signal_amp  Signal field amplitudes as a function of ell for all 
%               signal components as seen by all experiments. This should
%               be an array with shape [n_ell, n_expt, n_cpt], where n_ell 
%               is the size of the delta-ell=1 bandpower window functions, 
%               n_expt is the number of separate experiments/observing
%               frequencies, and n_cpt is the number of *independent* 
%               signal components in your model. 
%            ** If you want the standard 12-parameter model, just pass the
%               output of model_rms.m here. **
%               If you pass an empty array [] for this argument, then 
%               scale_bpcm will return an unscaled bandpower covariance 
%               matrix (i.e. the bpcm for the model used by the signal sims).
%   likedata    Data structure for multicomponent likelihood analysis
%
% [Output]
%   bpcm        Scaled bandpower covariance matrix.

% Get bandpower covariance matrix components.
% Apply masks if mask_bpcm option is set.
if likedata.opt.mask_bpcm
  sig = likedata.bpcov.sig .* likedata.bpcov.mask_sig;
  noi = likedata.bpcov.noi .* likedata.bpcov.mask_noi;
  sn1 = likedata.bpcov.sn1 .* likedata.bpcov.mask_sn1;
  sn2 = likedata.bpcov.sn2 .* likedata.bpcov.mask_sn2;
  sn3 = likedata.bpcov.sn3 .* likedata.bpcov.mask_sn3;
  sn4 = likedata.bpcov.sn4 .* likedata.bpcov.mask_sn4;
else
  sig = likedata.bpcov.sig;
  noi = likedata.bpcov.noi;
  sn1 = likedata.bpcov.sn1;
  sn2 = likedata.bpcov.sn2;
  sn3 = likedata.bpcov.sn3;
  sn4 = likedata.bpcov.sn4;
end

% If signal_amp argument is empty, then output model = bpcm fiducial model.
% Just return the sum of the six bpcm components.
if isempty(signal_amp)
  bpcm = sig + noi + sn1 + sn2 + sn3 + sn4;
  % if likeopt.offdiag exists trim the cov mat
  if isfield(likedata.opt, 'offdiag')
    bpcm = trim_bpcm(bpcm, likedata);
  end
  return
end

% Get number of experiments, fields, spectra, ell bins.
n_field = numel(likedata.opt.fields);
n_expt = numel(likedata.opt.expt);
order = expt_field_order(n_expt, n_field);
n_spec = numel(order);
n_ell = numel(likedata.opt.l);
% Get number of signal components.
n_cpt = size(signal_amp, 3);

% Expand array of bandpower window functions to include third dimension 
% with size = n_cpt.
bpwf = repmat(likedata.bpwf.Cs_l, [1, 1, n_cpt]);

% Position along one axis of the bandpower covariance matrix specifies a 
% particular combination of two experiments and two fields, 
% i.e. BICEP2 E-modes x BICEP1 B-modes.
% The order array (returned above by expt_field_order) specifies how these
% combinations are ordered in the bandpower covariance matrix (H-L vecp
% ordering). The output of model_rms gives field rms values for each 
% combination of one experiment and one field, i.e. BICEP2 E-modes.
% The variable findex_1 maps (expt1, field1) from the bpcm ordering onto 
% the corresponding field rms entries; variable findex_2 does the same
% mapping but for (expt2, field2) from the bpcm.

% For convenience, convert order from array of structs into a 2D array with 
% size [5, n_spec].
%   order(1,:) = expt 1
%   order(2,:) = field 1
%   order(3,:) = expt 2
%   order(4,:) = field 2
%   order(5,:) = index (1:n_spec)
order = reshape(struct2array(order), 5, n_spec);
% Array of field rms values has the following indexing along its second
% dimension: (expt field) = (1 1), (1 2), ..., (2 1), (2 2), ...
% i.e. expt increments slowly, field increments fast.
% Calculate the indexing in this array from bpcm order.
findex_1 = order(2,:) + (order(1,:) - 1) * n_field;
findex_2 = order(4,:) + (order(3,:) - 1) * n_field;
% Repeat for all ell bins.
findex_1 = repmat(findex_1, 1, n_ell);
findex_2 = repmat(findex_2, 1, n_ell);

% Now, we can calculate the scaling factors at each position along the
% bpcm axis for each signal component.
% scale_1 and scale_2 are the scale factors corresponding to the first and 
% second (expt, field) combination. Both arrays have shape [n_spec, n_cpt],
% where n_spec is the size of the bpcm axis and n_cpt is the number of 
% signal components.
% Scale factors are a ratio of the field rms in the output model to the 
% field rms in the input model, where rms is given by the sqrt of the 
% bandpower expectation value. Note that there is some potential confusion
% here regarding bandpower window functions -- scaling for B2 E-modes will
% be slightly different if calculated for a B2 x B2 entry vs a B2 x B1 
% entry, because those have different bandpower window functions.

% First, calculate the denominator terms from the bandpower covariance 
% matrix fiducial model.
lbin = reshape(ones(1, n_spec)' * [1:n_ell], n_spec*n_ell, 1);
s1d = sqrt(repmat(diag(likedata.bpcov.C_fl(lbin,findex_1))', [1,1,n_cpt]));
s2d = sqrt(repmat(diag(likedata.bpcov.C_fl(lbin,findex_2))', [1,1,n_cpt]));

% Take ratio of output model amplitude to bpcm fiducial model amplitude.
scale_1 = sqrt(sum(signal_amp(:,findex_1,:).^2 .* bpwf, 1)) ./ s1d;
scale_1 = squeeze(scale_1);
scale_2 = sqrt(sum(signal_amp(:,findex_2,:).^2 .* bpwf, 1)) ./ s2d;
scale_2 = squeeze(scale_2);

% Initialize bandpower covariance matrix.
bpcm = zeros(n_spec * n_ell, n_spec * n_ell);

% Now, create the scaled bpcm by summing the appropriately scaled
% sub-matrices. Outer products are used to convert the vector of scale
% factors into a square array of scale factors that can directly multiply
% the corresponding covariance sub-matrix.

% Signal x signal terms: Cov(S_i x S_j, S_k x S_l)
% First case is where all four fields going into the covariance are the 
% same signal type. This corresponds to Victor's term 1.
% Calculate this contribution for each signal component.
for ii=1:n_cpt
  scale_matrix = (scale_1(:,ii) .* scale_2(:,ii)) * ...
      (scale_1(:,ii) .* scale_2(:,ii))';
  bpcm = bpcm + scale_matrix .* sig;
end
% Next case has two different signal types, but it is only non-zero if 
% the same two signal types show up in both halves of the covariance 
% term. This corresponds to Victor's term 7 if i=k and j=l or Victor's
% term 8 if i=l and j=k. 
% Calculate this contribution for each pairwise combination of signal 
% components where i~=j. Also, need an additional factor of 1/2 in the 
% scaling, to account for the difference between variance of an 
% auto-spectrum vs cross-spectrum.
for ii=1:n_cpt
  for jj=(ii+1):n_cpt
    % Term 7 contributions.
    scale_matrix = 0.5 * (scale_1(:,ii) .* scale_2(:,jj)) * ...
        (scale_1(:,ii) .* scale_2(:,jj))';
    bpcm = bpcm + scale_matrix .* sig;
    scale_matrix = 0.5 * (scale_1(:,jj) .* scale_2(:,ii)) * ...
        (scale_1(:,jj) .* scale_2(:,ii))';
    bpcm = bpcm + scale_matrix .* sig;
    % Term 8 contributions.
    scale_matrix = 0.5 * (scale_1(:,ii) .* scale_2(:,jj)) * ...
        (scale_1(:,jj) .* scale_2(:,ii))';
    bpcm = bpcm + scale_matrix .* sig;
    scale_matrix = 0.5 * (scale_1(:,jj) .* scale_2(:,ii)) * ...
        (scale_1(:,ii) .* scale_2(:,jj))';
    bpcm = bpcm + scale_matrix .* sig;
  end
end

% Noise x noise terms don't scale with signal model.
bpcm = bpcm + noi;

% Signal x noise terms: Cov(S_i x N_j, S_k x N_l) and similar.
% Include the scalings for signal fields only. Noise doesn't scale with
% signal model. Only non-zero if both signal terms correspond to the same 
% component, so we only have to do a single loop over signal components
% (but calculate all four permutations, which correspond to Victor's
% terms 3--6). No factor of 0.5 necessary, because the bpcov.snX matrices
% have already been calculated from cross-spectra of independent fields
% (signal x noise).
for ii=1:n_cpt
  % Term 3 contribution: Cov(S_i x N_j, S_k x N_l)
  scale_matrix = scale_1(:,ii) * scale_1(:,ii)';
  bpcm = bpcm + scale_matrix .* sn1;
  % Term 4 contribution: Cov(S_i x N_j, N_k x S_l)
  scale_matrix = scale_1(:,ii) * scale_2(:,ii)';
  bpcm = bpcm + scale_matrix .* sn2;
  % Term 5 contribution: Cov(N_i x S_j, S_k x N_l)
  scale_matrix = scale_2(:,ii) * scale_1(:,ii)';
  bpcm = bpcm + scale_matrix .* sn3;
  % Term 6 contribution: Cov(N_i x S_j, N_k x S_l)
  scale_matrix = scale_2(:,ii) * scale_2(:,ii)';
  bpcm = bpcm + scale_matrix .* sn4;
end

% if likeopt.offdiag exists trim the cov mat
if isfield(likedata.opt, 'offdiag')
  bpcm = trim_bpcm(bpcm, likedata);
end

return % END of function scale_bpcm
