function likedata=compute_modelcomps(likedata,mod)
% likedata=compute_modelcomps(likedata,mod)
%
% Find bpcm, model values and chi2

% get bpcm for this model
[ell,cmp]=model_rms(mod,likedata); likedata.bpcm=scale_bpcm(cmp,likedata);

% get expv of this model and the components thereof
% note that for EE all include base LCDM signal
% (and remember we do not vary EE when varying A_L)

% total
likedata.expv=like_getexpvals(mod,likedata);

% tensors
likedata.expvr=like_getexpvals([mod(1),0,0,0,mod(5:end)],likedata);

% lensing
likedata.expvl=like_getexpvals([0,mod(2),0,0,mod(5:end)],likedata);

% sync alone
likedata.expvs=like_getexpvals([0,0,mod(3),0,mod(5:end)],likedata);

% dust alone
likedata.expvd=like_getexpvals([0,0,0,mod(4),mod(5:end)],likedata);

% sync+dust with correlation boost
likedata.expvsd=like_getexpvals([0,0,mod(3),mod(4),mod(5:end)],likedata);

% get diag errors in same shape as likedata.real
e=sqrt(diag(likedata.bpcm));
likedata.e=reshape(e,size(likedata.real'))';

% chi2 with full bpcm
d=cvec(likedata.real');
m=cvec(likedata.expv');
chi2=(d-m)'*inv(likedata.bpcm)*(d-m);

% simple chi2 for comparison
chi2s=sum(((d-m)./e).^2);

% record
pte=1-chi2cdf(chi2,likedata.dof);
likedata.chi2=chi2; likedata.pte=pte; likedata.chi2s=chi2s;

% compute global chi
likedata.chi=nansum((d-m)./e);

return
