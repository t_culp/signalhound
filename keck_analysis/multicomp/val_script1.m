% Run bpcm_val and generate a plot of the correlation matrix
% KDA 09-29-2014
clear;

% Use B2, B1_100, WMAP7_K
likeopt = get_default_likeopt;
likeopt.expt(3).name = 'WMAP7_K';
likeopt.expt(3).freq = 23;

% If using direct bpcm:
% Get data products from final .mat files.
likeopt = like_read_data(likeopt);
% Read bpwf from aps
lik = get_default_likeopt;
lik.expt(3).name = 'WMAP7_K';
lik.expt(3).freq = 23;
lik = like_read_aps(lik);
likeopt.bpwf = lik.bpwf;

% If using semi-analytic bpcm:
%likeopt = like_read_aps(likeopt);

% Read theory spectra from CAMB files.
likeopt = like_read_theory(likeopt);
p = [0.2, 1, 0, 0, -3.3, -0.6, 1.6, -0.4, 1, 1, 0, 19.6]; %tensors+lensing

% compute bandpowers, bpcm, correlation matrix
tic;
[bps,bpcm] = bpcm_val(p, likeopt);
toc
corr=zeros(size(bpcm));
for i=1:length(corr(:,1))
  for j=1:length(corr(1,:))
    corr(i,j)=log10(abs(bpcm(i,j)/sqrt(bpcm(i,i)*bpcm(j,j))));
  end
end

% display correlation matrix (log color scale)
figure(14); clf;
pcolor(corr);
shading flat 
colorbar();
caxis([-3 0]);
