% Makes plots containing a distribution of H-L maximum likelihood
% values for a set of Gaussian foreground sims. The ML values are
% assumed to be loaded from an output file from maxL_fgsim. The plot 
% generated shows a 2D scatter plot of the maximum likelihood
% values for r vs A_dust for each sim. The ML values for the real BICEP2 or 
% Keck data are also included. This code was used to make the plots
% in this posting:
% http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20141110_fg_val/,
% which also defines the input parameters for each model.
%
% Committed 2014-11-18 (KDA)

clear;

% Set model and experiment.
model='m0';
%expt='b2'; % BICEP2 + all Planck frequencies (full autos)
expt='keck'; % Keck + all Planck frequencies (full autos)

switch model
  case 'm0'
    rmin=-0.05;
    rmax=0.4;
    Asmax=6e-4;
    Admin=-1.5;
    Admax=4;
  case 'm1'
    rmin=-0.1;
    rmax=0.25;
    Asmax=6e-4;
    Admin=-1;
    Admax=6;
  case 'm2'
    rmin=-0.1;
    rmax=0.25;
   Asmax=8e-4;
    Admin=-1;
    Admax=6;
  case 'm3'
    rmin=-0.1;
    rmax=0.4;
    Asmax=3e-3;
    Admin=0;
    Admax=11;
  case 'm5'
    rmin=-0.15;
    rmax=0.15;
    Asmax=6e-4;
    Admin=0;
    Admax=6;
  case 'm6'
    rmin=-0.1;
    rmax=0.4;
    Asmax=3e-3;
    Admin=0;
    Admax=11;
end

% Load ML values for real data
load(sprintf('/n/bicepfs2/users/kalex/simfiles/%s_real.mat',expt))
real_hl=maxL_hl;

% Load file containing ML values for all 499 sims for model,
% expt. Change this to point to your maxL_fgsim.m output file if
% generating distributions for new sims.
load(sprintf('/n/bicepfs2/users/kalex/simfiles/fgsim/%s/%s/like_tot.mat',expt,model)) 

% Make plot of r vs A_dust ML values for sims and real data
dr=(rmax-rmin)/18;
dAd=(Admax-Admin)/12;

fig1=figure(1);clf;
plot(maxL_hl(:,1),maxL_hl(:,3),'b.','MarkerSize',4)
hold all
h=plot(p_sims(1),p_sims(4),'ro','MarkerSize',5);
g=plot(mean(maxL_hl(:,1)),mean(maxL_hl(:,3)),'k+'); 
f=plot(real_hl(1,1),real_hl(1,3),'gp','MarkerSize',5);
set(h,'LineWidth',1)
set(g,'LineWidth',2)
xlabel('r')
ylabel('A_{dust} (\muK^2)')
title(expt)
xlim([rmin rmax])
ylim([Admin Admax])
text(rmin+dr,Admax-dAd,sprintf('r = %f, A_{dust} = %f, A_{sync} = %f',mean(maxL_hl(:,1)),mean(maxL_hl(:,3)),mean(maxL_hl(:,2))))
text(rmin+dr,Admax-dAd*2,sprintf('Input model: r = %s, A_{dust} = %s, A_{sync} = %s,',num2str(p_sims(1)),num2str(p_sims(4)),num2str(p_sims(3))),'Color','r')
text(rmin+dr*5,Admax-dAd*2.8,'dust/sync corr = 50%','Color','r')
%print(fig1,sprintf('%s_fgsim_%s.png',model,expt),'-dpng');
