function loglik = like_chi2(model, data, bpcm)
% loglik = like_chi2(model, data, bpcm)
%
% Calculates log(likelihood) for the specified data assuming Gaussian
% bandpower errors.
%
% log(likelihood) = -0.5 * chi^2 - 0.5 * log(det(bpcm))
%     where chi^2 = (model - data)' * inv(bpcm) * (model - data)
%
% [Input]
%   model   Array of model bandpowers with shape [n_ell, n_spectra].
%           Calculate this using like_getexpvals.m
%   data    Array of real data bandpowers with shape [n_ell, n_spectra].
%           Obtained from pipeline data produces using like_makedataprod.m
%   bpcm    Bandpower covariance matrix with shape 
%           [n_ell*n_spectra, n_ell*n_spectra]. Obtained from pipeline 
%           data products using like_makedataprod.m
%
% [Output]
%   loglik  Single value for log(likelihood).

n_ell = size(model, 1);
n_spectra = size(model, 2);

% Difference model and data.
% Reshape into 1D vector with n_ell*n_spectra elements.
bpdev = reshape((model - data)', n_ell * n_spectra, 1);

% Calculate determinant and inverse of bandpower covariance matrix.
% dbpcm = det(bpcm);
[R,p] = chol(bpcm);
ln_det = sum(log(abs(diag(R)))) * 2;
ibpcm = inv(bpcm);

% Calculate log likelihood, including normalization by det(bpcm).
% loglik = -0.5 * (bpdev' * ibpcm * bpdev + log(dbpcm));
loglik = -0.5 * (bpdev' * ibpcm * bpdev + ln_det);
