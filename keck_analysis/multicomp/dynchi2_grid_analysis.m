function [likedata,l]=dynchi2_grid_analysis(nspecs,bps,offdiag,keepspec,betaprior,psfsub,usebps,incee,usehl,subsim,A_s,A_L,fidmod)
%
% Using Harvard code to read the data do either dynchi2 or HL grid
% likelihood evaluation 
%
% r
% A_d - the dust power at ell=80 and 353GHz
% beta_d - the nu^beta modifier term on the 19.6K blackbody
%
% There are various things we want to vary:
% nspecs = 2 for B2/P353 and 3 for B2/P217/P353
% bps = 2:6 or 2:10 for instance
% offdiag = Inf for full bpcm or 1 for no bp-bp correlations (which
% are barely detected with 99 sims
% keepspec = use all spectra or ignore some - only works for dynchi2
% betaprior =1 apply PIPXXX prior, =2 triple that
% psfsub = use Planck single freq cross spectrum instead of auto,
% 3=Y1xY2, 4=DS1xDS2, 5=HR1xHR2
% also... if 13,14,15 do K1213
%         if 23,24,25 do B+K combined
% usebps = use full bandpass rather than nominal center freq
% incee = include EE (and EB)
% usehl = use HL rather than dynchi2
% subsim = subsitute sim n in place of real data
%
% e.g.
% BKP baseline
% dynchi2_grid_analysis(3,2:6,1,[],1,34,1,0,1);
%
% Evol to BK14
% BKP with Planck auto
% dynchi2_grid_analysis(3,2:6,1,[],1,30,1,0,1);
% Add fidmodel tweak
% dynchi2_grid_analysis(3,2:6,1,[],1,36,1,0,1);
% Switch to BK14_150
% dynchi2_grid_analysis(23,2:6,1,[],1,40,1,0,1);
% Turn on 95
% dynchi2_grid_analysis(24,2:6,1,[],1,40,1,0,1);
% Turn off 217/353
% dynchi2_grid_analysis(25,2:6,1,[],1,40,1,0,1);
% 95/150/217/353 with no prior on beta_d
% dynchi2_grid_analysis(24,2:6,1,[],0,40,1,0,1);

if(~exist('nspecs','var'))
  nspecs=2;
end
if(~exist('bps','var'))
  bps=2:6;
end
if(~exist('offdiag','var'))
  offdiag=1;
end
if(~exist('keepspec','var'))
  keepspec=[];
end
if(~exist('betaprior'))
  betaprior=0;
end
if(~exist('psfsub'))
  psfsub=0;
end
if(~exist('usebps'))
  usebps=0;
end
if(~exist('incee'))
  incee=0;
end
if(~exist('usehl'))
  usehl=0;
end
if(~exist('subsim'))
  subsim=0;
end
if(~exist('A_s'))
  A_s=false;
end
if(~exist('A_L'))
  A_L=false;
end
if(~exist('fidmod'))
  % default is now lcdm+dust
  fidmod=2;
end

% Options for get_likedata
likeopt.l = bps;
%likeopt.Planck_datasplit = 2; % now default
likeopt.offdiag = offdiag;
if incee
  likeopt.fields = {'E', 'B'};
else
  likeopt.fields = {'B'};
end
if(subsim)
  likeopt.subsim=subsim;
end

% options to try and force it back to BKP paper state
if(psfsub<36)
  likeopt.Planck_noisebias = 2; % only relevant for crosses
  if incee
    % Need to set these two options to ensure real-valued likelihood.
    likeopt.Planck_noisebias = 1;
    likeopt.Planck143_EBfull = 1;
  end
  % For BKP analysis, we used LCDM+r=0.2 sims to build bandpower
  % covariance matrix.
  likeopt.fidmodel.tensor = 2; % r=0.2
  likeopt.fidmodel.lcdm = 1;
  likeopt.fidmodel.dust = 0;
  likeopt.fidmodel.unlens = 0;
end

% which data set to use
switch psfsub
  case 0
    likedata = get_likedata('BP_auto', likeopt);
    psf='';
  case 3 % Y1xY2
    likedata = get_likedata('BP_yr', likeopt);
    psf='_psfy1xy2';
  case 4 % DS1xDS2
    likedata = get_likedata('BP', likeopt);
    psf='_psfds1xds2';
  case 5 % HR1xHR2
    likedata = get_likedata('BP_hr', likeopt);
    psf='_psfhr1xhr2';

  % Keck only - unfixed dp1102
  case 13 % Keck Y1xY2 
    likedata = get_likedata('KP_yr', likeopt);
    psf='_psfy1xy2keck';
  case 14 % Keck DS1xDS2
    likedata = get_likedata('KP', likeopt);
    psf='_psfds1xds2keck';
  case 15 % Keck HR1xHR2
    likedata = get_likedata('KP_hr', likeopt);
    psf='_psfhr1xhr2keck';
  
  % use map combined B2+K
  case {30,36}
    likedata = get_likedata('BKP_auto', likeopt);
    psf='_psffxfcombm';
  case 33
    likedata = get_likedata('BKP_yr', likeopt);
    psf='_psfy1xy2combm';
  case 34
    likedata = get_likedata('BKP', likeopt);
    psf='_psfds1xds2combm';
  case 35
    likedata = get_likedata('BKP_hr', likeopt);
    psf='_psfhr1xhr2combm';
    
  % go to BK14
  case 40
    likedata = get_likedata('BK14', likeopt);
    psf='_bk14';
end

likedata.opt
likedata.opt.fidmodel

% downselect to bands within the given data set
b2k = likedata.opt.expt{1};
switch nspecs
 case 2
  likedata = likeopt_select(likedata, {b2k,'P353'}, [], []);
 case 3
  likedata = likeopt_select(likedata, {b2k,'P217','P353'}, [], []);
 case 4
  likedata = likeopt_select(likedata, {b2k,'P100','P217','P353'}, [], []);
 case 5
  likedata = likeopt_select(likedata, {b2k,'P030','P217','P353'}, [], []);
 case 6
  likedata = likeopt_select(likedata, {b2k,'P030','P100','P217','P353'}, [], []);
 case 7
  % keep all of them
  likedata = likeopt_select(likedata, {b2k,'P030','P044','P070','P100','P143','P217','P353'},[], []);
 case 8
  % see what sync limit is like when drop 30GHz
  likedata = likedata_select(likedata, {b2k,'P044','P070','P100','P143','P217','P353'},[], []);
 case 9
  % keep only P353 - need to set r=0 below
  likedata = likeopt_select(likedata, {'P353'}, [], []);

 % these cases are BxPlanck cross spectra only
 case 12
  likedata = likeopt_select(likedata, {b2k,'P353'}, [], []);
  likedataa = likeopt_select(likedata, {'P353'}, [], []);
 case 13
  likedata = likeopt_select(likedata, {b2k,'P217','P353'}, [], []);
  likedataa = likeopt_select(likedata, {'P217','P353'}, [], []);
  
 % cases for BK14 - this is a total mess but this is just for
 % sanity check of COSMOMC anyway
 case 22 % special case to force opts back to BKP state
  likedata = likeopt_select(likedata, {'BK14_150','P217','P353'}, [], []);
 case 23
  likedata = likeopt_select(likedata, {'BK14_150','P217','P353'}, [], []);
 case 24
  likedata = likeopt_select(likedata, {'BK14_95','BK14_150','P217','P353'}, [], []);
 case 25
  likedata = likeopt_select(likedata, {'BK14_95','BK14_150'}, [], []);
end

% nan out unwanted spectra - this is incompatible with HL and
% superceded by the subtraction of likelihoods method which should
% work for both
if(~isempty(keepspec))
  if(length(keepspec)<size(likedata.real,2))
    keepspec=false(1,size(likedata.real,2));
    keepspec(keepspec)=true;
  else
    keepspec=logical(keepspec);
  end
  likedata.real(repmat(~keepspec,[size(likedata.real,1),1]))=NaN;
  likedata.opt.keepspec=keepspec;
else
  keepspec=true(1,size(likedata.real,2));
end

% define the grid
l.A_d=0:0.2:10;
l.r=0:0.01:0.3;

% special case to see P353 only
if(nspecs==9)
  l.r=[0:3]*1e-9;
  l.A_d=0:0.25:15;
end

betapriorvals=[1.59,0.11]; % values from PIPXXX
switch betaprior
 case 1
  % standard prior - tight grid range
  l.beta=1.2:0.2:2;
 case 2
  % relaxed prior and grid range
  l.beta=0.4:0.2:2.8;
  betapriorvals(2)=betapriorvals(2)*3;
 case 0
  % no prior and wide grid range
  l.beta=0:0.2:3.5;
 case 3
  % shifted down prior and grid range
  betapriorvals=[1.3,0.11];
  l.beta=0.9:0.2:1.7;
 case 4
  % shifted up prior and grid range
  betapriorvals=[1.9,0.11];
  l.beta=1.5:0.2:2.3;
end
if(length(A_s)==1)
  switch A_s
   case 1
     % uncorr
    l.A_s=0:0.00005:0.0005;
    dscorr=0;
   case 2
     % 100% corr
    l.A_s=[0:0.00005:0.0005]/10;
    dscorr=1;
   case 3
     % 50% corr
    l.A_s=[0:0.00005:0.0005]/4;
    dscorr=0.5;
   case 0
     % no sync
    l.A_s=0;
    dscorr=0;
  end
else
  % specify corr and A_s vals
  dscorr=A_s(1);
  l.A_s=A_s(2:end);
  A_s=1;
end
if(A_L)
  l.A_L=0:0.1:1.8;
else
  l.A_L=1;
end

% base model
switch fidmod
 case 1
  mod=[0.2,1,0,0,-3.3,-0.6,1.59,-0.42,1,2,dscorr,19.6];
 case 2
  mod=[0,1,0,3.6,-3.3,-0.6,1.59,-0.42,1,2,dscorr,19.6];
end

if(usehl)
  % Calculate fiducial model bandpowers, bpcm.
  C_fl=like_getexpvals(mod,likedata); % Fiducial model bandpowers.
  [ell,rms]=model_rms(mod,likedata);
  M_fl=scale_bpcm(rms,likedata); % Fiducial model bandpower covariance matrix.
  % H-L precalculation -- stuff that doesn't need to be recomputed at each grid point.
  prep=hamimeche_lewis_prepare(ivecp((C_fl + likedata.N_l)'),M_fl);

  if(exist('likedataa'))
    C_fla=like_getexpvals(mod,likedataa); % Fiducial model bandpowers.
    [ella,rmsa]=model_rms(mod,likedataa);
    M_fla=scale_bpcm(rms,likedataa); % Fiducial model bandpower covariance matrix.
    prepa=hamimeche_lewis_prepare(ivecp((C_fla + likedataa.N_l)'),M_fla);
  end
end

l.l=zeros([length(l.A_d),length(l.r),length(l.beta),length(l.A_s),length(l.A_L)]);
if(exist('likedataa'))
  l.la=l.l;
end  
for i=1:length(l.A_d)
  i
  tic
  for j=1:length(l.r)
    for k=1:length(l.beta)
      for m=1:length(l.A_s)
        for n=1:length(l.A_L)
          p=[l.r(j),l.A_L(n),l.A_s(m),l.A_d(i),mod(5:6),l.beta(k),mod(8:12)];
          switch usehl
           case 0
            l.l(i,j,k,m,n)=calcl(likedata,p);
            if(exist('likedataa'))
              l.la(i,j,k,m,n)=calcl(likedataa,p);
            end
           case 1
            model=like_getexpvals(p,likedata);
            l.l(i,j,k,m,n)=like_hl(model,likedata.real,likedata.N_l,prep);
            if(exist('likedataa'))
              modela=like_getexpvals(p,likedataa);
              l.la(i,j,k,m,n)=like_hl(modela,likedataa.real,likedataa.N_l,prepa);
            end
          end
        end
      end
    end
  end
  toc
end

% subtract the alt likelihood if required
if(exist('likedataa'))
  l.l=l.l-l.la;
end

% convert from logL to L
l.l=exp(l.l);

% apply prior on beta if requested
if(betaprior)
  betap(1,1,:)=l.beta;
  s=size(l.l);
  s(3)=1;
  betag=repmat(betap,s);
  betag=gauss([1,betapriorvals],betag);
  l.l=l.l.*betag;
end

% record dof of the fit
dof=sum(~isnan(cvec(likedata.real)))-A_s-A_L;
if(betaprior)
  dof=dof-2;
else
  dof=dof-3;
end
likedata.dof=dof;


% find max like model
[v,i]=maxmd(l.l);
switch length(i)
 case 3
  i=[i,1,1];
 case 4
  i=[i,1];
end
% assemble the ML model
mod=[l.r(i(2)),l.A_L(i(5)),l.A_s(i(4)),l.A_d(i(1)),mod(5:6),l.beta(i(3)),mod(8:12)];
likedata.mlmod=mod;

% compute the model components
likedata=compute_modelcomps(likedata,mod);

figure; clf;
% try to make it look OK on Kirit's computer
set(gcf,'DefaultAxesFontSize', 8);
set(gcf,'DefaultTextFontSize', 8);

% control which things to plot where
if(A_s|A_L)
  ny=4; nx=8;
  nn=[5,14,23,32,13,21,22,29,30,31];
  setwinsize(gcf,1600,800);
else
  ny=3; nx=6;
  nn=[4,11,18,0,0,10,16,17];
  setwinsize(gcf,1200,600);
end

% 1d
subplot(ny,nx,nn(1));
y=summd(l.l,2); plot(l.r,y./max(y)); xlabel('r'); axis tight
subplot(ny,nx,nn(2));
y=summd(l.l,1); plot(l.A_d,y./max(y)); xlabel('A_d'); axis tight
subplot(ny,nx,nn(3));
y=summd(l.l,3); plot(l.beta,y./max(y)); xlabel('\beta_d'); axis tight
if(betaprior)
  hold on; plot(l.beta,gauss([1,betapriorvals],l.beta),'r'); hold off
end
if(A_s|A_L)
  subplot(ny,nx,nn(4));
  y=summd(l.l,4); plot(l.A_s,y./max(y)); xlabel('A_s'); axis tight
  subplot(ny,nx,nn(5));
  y=summd(l.l,5); plot(l.A_L,y./max(y)); xlabel('A_L'); axis tight
end

% 2d
subplot(ny,nx,nn(6));
imagesc(l.r,l.A_d,summd(l.l,[1,2])); axis xy
xlabel('r'); ylabel('A_d');

subplot(ny,nx,nn(7));
imagesc(l.r,l.beta,summd(l.l,[2,3])'); axis xy
xlabel('r'); ylabel('\beta_d');
subplot(ny,nx,nn(8));
imagesc(l.A_d,l.beta,summd(l.l,[1,3])'); axis xy
xlabel('A_d'); ylabel('\beta_d');

if(A_s)
  subplot(ny,nx,nn(8));
  imagesc(l.r,l.A_s,summd(l.l,[2,4])'); axis xy
  xlabel('r'); ylabel('A_s');
  subplot(ny,nx,nn(9));
  imagesc(l.A_d,l.A_s,summd(l.l,[1,4])'); axis xy
  xlabel('A_d'); ylabel('A_s');
  subplot(ny,nx,nn(10));
  imagesc(l.beta,l.A_s,summd(l.l,[3,4])'); axis xy
  xlabel('\beta_d'); ylabel('A_s');
end
  
if(A_L)
  subplot(ny,nx,nn(8));
  imagesc(l.r,l.A_L,summd(l.l,[2,5])'); axis xy
  xlabel('r'); ylabel('A_L');
  subplot(ny,nx,nn(9));
  imagesc(l.A_d,l.A_L,summd(l.l,[1,5])'); axis xy
  xlabel('A_d'); ylabel('A_L');
  subplot(ny,nx,nn(10));
  imagesc(l.beta,l.A_L,summd(l.l,[3,5])'); axis xy
  xlabel('\beta_d'); ylabel('A_L');
end

% plot bp devs of model
subplot(3,6,[5,6]);
d=cvec(likedata.real);
m=cvec(likedata.expv);
e=cvec(likedata.e);
plot([1:length(d)]-0.5,(d-m)./e,'.');
xlim([0,length(d)]);
set(gca,'Xtick',[0:size(likedata.real,1):length(d)]);
grid
% label the spectra
n=length(likedata.opt.expt);
for i=1:n
  likedata.opt.expt{i}=strrep(likedata.opt.expt{i},'_','\_');
end
for i=1:n
  for j=i:n
    k=likeopt_getxind(likedata.opt,i,j);
    yl=ylim;
    h1=text(((k-1)+0.5)*size(likedata.real,1),yl(1)+diff(yl)*0.1,...
         sprintf('%sx%s',likedata.opt.expt{i},likedata.opt.expt{j}),...
         'Rotation',90);
  end
end
title(sprintf('(Data-Model)/Uncertainty, \\chi^2=%.1f, pte=%.3f for %d dof, \\chi=%.1f',likedata.chi2,likedata.pte,likedata.dof,likedata.chi));

% plot bandpower correl matrix
subplot(3,6,12);
imagesc(corrcov(likedata.bpcm)); axis xy; caxis([0,1]);
title('bandpower correlation')

% write params of ML model on plot
if(~incee)
  subplot(n,2*n,n);
else
  subplot(n+1,2*(n+1),1);
end
title('ML model:')
box on
text(0.1,0.8,sprintf('r=%.2f',mod(1)));
text(0.1,0.65,sprintf('A_d=%.1f',mod(4)));
text(0.1,0.5,sprintf('\\beta_d=%.2f',mod(7)));
if(A_s)
  text(0.1,0.35,sprintf('A_s=%.4f',mod(3)));
end
if(A_L)
  text(0.1,0.35,sprintf('A_L=%.2f',mod(2)));
end

% plot each spectrum
for f=1:length(likedata.opt.fields) 
  for i=1:n
    for j=i:n
      if(~incee)
        subplot(n,2*n,i+(j-1)*2*n);
      else
        switch f
         case 1
          subplot(n+1,2*(n+1),j+1+(i-1)*2*(n+1));
         case 2
          subplot(n+1,2*(n+1),i+(j)*2*(n+1));
        end
      end        
      k=likeopt_getxind(likedata.opt,i,j,f,f);
      %[i j k]
      d=likedata.real(:,k);
      m=likedata.expv(:,k);
      e=likedata.e(:,k);
      ell=likedata.lval;
      hold on; box on
      if(incee&f==1)
        % EE
        if(A_s)
          % want to show contribution due to sync and its correlation
          % for EE we plot total minus dust as both include LCDM
          plot(ell,likedata.expv(:,k)-likedata.expvd(:,k),'c');
        end
        plot(ell,likedata.expv(:,k)-likedata.expvs(:,k),'m');
        plot(ell,likedata.expvl(:,k),'r');
      else
        % BB
        if(A_s)
          % for BB we explicitly calc sync&dust and subtract dust
          % only - although total-lensing-r-dust should be the same
          plot(ell,likedata.expvsd(:,k)-likedata.expvd(:,k),'c');
        end
        plot(ell,likedata.expvd(:,k),'m');
        plot(ell,likedata.expvr(:,k),'g');
        plot(ell,likedata.expvl(:,k),'r');
      end
      plot(ell,m,'b-');
      plot(ell,m+e,'b:'); plot(ell,m-e,'b:');
      plot(ell,d,'k.');
      hold off
      title(sprintf('%sx%s',likedata.opt.expt{i},likedata.opt.expt{j}));
      %axis tight; xlim([ell(1)-20,ell(end)+20]);
      line(xlim,[0,0],'LineStyle',':','color','k');
      ptitle(sprintf('\\chi^2=%.1f, \\chi=%.1f',...
                     sum(((d-m)./e).^2),sum(((d-m)./e))));
    end
  end
end

%mkgif(sprintf('dynchi2_bps%1d%1d_specs%1d_%s_%1d%s_%1d_%1d_%03d_%1d.gif',bps([1,end]),nspecs,strrep(num2str(likeopt.keepspec),' ',''),betaprior,psf,usebps,usehl,subsim,incee));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function l=calcl(likedata,mod)

% get the bpcm for this model
[ell,cmp]=model_rms(mod,likedata);
likedata.bpcm=scale_bpcm(cmp,likedata);

% get the expv for this model
m=like_getexpvals(mod,likedata);

% get the data
d=likedata.real;
  
% get the bpcm
bpcm=likedata.bpcm;

if(isfield(likedata.opt,'keepspec'))
  % delete columns of real and expv to disregard spectra
  d=d(:,likedata.opt.keepspec);
  m=m(:,likedata.opt.keepspec);
  
  % delete the corresponding row/col of the bpcm
  z=repmat(likedata.opt.keepspec,[size(likedata.real,1),1]);
  z=cvec(z');
  bpcm=bpcm(z,z);
end

d=cvec(d');
m=cvec(m');

chi2=(d-m)'*inv(bpcm)*(d-m);
l=(1/sqrt(det(bpcm)))*exp(-chi2/2);

% make compatible with like_hl which delivers logL
l=log(l);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotmod(likedata,mod)

% get the bpcm for this model
[ell, cmp] = model_rms(mod,likedata);
likedata.bpcm=scale_bpcm(cmp,likedata);
eb=sqrt(diag(likedata.bpcm));

% get the expv for this model
expv=like_getexpvals(mod,likedata);

subplot(2,2,1)
errorbar2(likedata.lval,likedata.real(:,1),eb(1:3:end),'b.');
hold on; plot(likedata.lval,expv(:,1),'r'); hold off
subplot(2,2,4)
errorbar2(likedata.lval,likedata.real(:,2),eb(2:3:end),'b.');
hold on; plot(likedata.lval,expv(:,2),'r'); hold off
subplot(2,2,3)
errorbar2(likedata.lval,likedata.real(:,3),eb(3:3:end),'b.');
hold on; plot(likedata.lval,expv(:,3),'r'); hold off

subplot(2,2,2)
d=cvec(likedata.real');
m=cvec(expv');
e=sqrt(diag(likedata.bpcm));
plot((d-m)./e,'.');

return


% we have 4 files from Stefan:

% 1: the full x full
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1100_jack0_pureB_matrix_directbpwf.mat

% 2: the half x half
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack4_real_a_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_nonej_directbpwf.mat

% 3: the full x half
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_nonej_directbpwf.mat

% 4: the full x jack
load final/0751x1613/real_a_filtp3_weight3_gs_dp1102_jack0_real_a_filtp3_weight3_gs_dp1100_jack4_pureB_matrix_directbpwf.mat



% debug
likeopt.l=2:6;
likeopt.fields={'B'};
likedata=get_likedata('BK14',likeopt);

likedataa=likeopt_select(likedata,[2,1,10,11],[],[]);
likedataa.opt.expt

likedatab = likeopt_select(likedata,{'BK14_95','BK14_150','P217','P353'},[],[]);
likedatab.opt.expt

