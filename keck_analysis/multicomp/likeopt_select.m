function likedata = likeopt_select(likedata, expt, field, ell)
% likeopt = likeopt_select(likeopt, expt, field, ell)
%
% Downselect data in a likeopt structure to keep only specified experiments,
% CMB fields, and ell bins.
%
% [Input]
%   likedata  Likelihood analysis data structure
%   expt      Experiments to retain in the data structure. This argument can
%             be either an array of integer indices or a cell array of 
%             experiment names. If this argument is an empty array, then 
%             the function *keeps* all experiments.
%   field     CMB fields to retain in the data structure. This argument can 
%             be either a string or cell array listing the fields to keep. 
%             Examples: 'EB' or {'E', 'B'} will both select E and B only.
%             If this argument is an empty array (or empty string), then 
%             the function *keeps* all fields.
%   ell       Ell bins to retain in the data structure. Should be an 
%             integer array of ell bin indices. If this argument is an 
%             empty array, then the function *keeps* all ell bins.
%
% [Output]
%   likedata  Likelihood data structure with only selected data

% Get some dimensions.
n_expt = numel(likedata.opt.expt);
n_field = numel(likedata.opt.fields);
n_ell = numel(likedata.opt.l);
n_spec = size(likedata.real, 2);

% Determine which experiments to keep.
if ~isempty(expt)
  if iscell(expt)
    % Argument is a cell array containing the names of experiments to keep. 
    for ii=1:numel(expt)
      ind = find(strcmp(likedata.opt.expt, expt{ii}));
      if ~isempty(ind)
        keep_expt(ii) = ind;
      else
        keep_expt(ii) = 0;
      end
    end
    % Experiment names that weren't found in likedata.opt.expt show
    % up as zeros in keep_expt. Drop those.
    keep_expt = keep_expt(keep_expt ~= 0);
  else
    % Argument provides the indices of experiments to keep, but truncate
    % the argument if it specifies more experiments than are actually 
    % contained in likeopt.
    keep_expt = expt((expt > 0) & (expt <= n_expt));
  end
  % Not allowed to change the ordering of experiments, only
  % downselect them. If ordering changes, this really messes up the
  % SxN covariance sub-matrices.
  keep_expt = sort(keep_expt, 'ascend');
else
  % Empty argument means keep all experiments.
  keep_expt = [1:n_expt];
end

% Determine which fields to keep.
if ~isempty(field)
  if ~iscell(field)
    field = {field};
  end
  for ii=1:numel(field)
    ind = find(strcmp(likedata.opt.fields, field{ii}));
    if ~isempty(ind)
      keep_field(ii) = ind;
    else
      keep_field(ii) = 0;
    end
    % Fields that weren't found in likedata.opt.fields show up as
    % zeros in keep_field. Drop those.
    keep_field = keep_field(keep_field ~= 0);
  end
  % Not allowed to change the ordering of fields, only
  % downselect them. If ordering changes, this really messes up the
  % SxN covariance sub-matrices.
  keep_field = sort(keep_field, 'ascend');
else
  % Empty argument means keep all fields.
  keep_field = [1:n_field];
end

% Determine which ell bins to keep.
if ~isempty(ell)
  [dummy, keep_ell] = intersect(likedata.opt.l, ell);
else
  keep_ell = [1:n_ell];
end

% Determine which bandpowers to keep.
new_order = expt_field_order(numel(keep_expt), numel(keep_field));
new_order = reshape(struct2array(new_order), 5, numel(new_order));
expt1 = keep_expt(new_order(1,:));
fld1 = keep_field(new_order(2,:));
expt2 = keep_expt(new_order(3,:));
fld2 = keep_field(new_order(4,:));
keep_bp = likeopt_getxind(likedata.opt, expt1, expt2, fld1, fld2);

% Downsize real bandpowers, noise bias.
if isfield(likedata, 'real')
  likedata.real = likedata.real(keep_ell, keep_bp);
end
if isfield(likedata, 'N_l')
  likedata.N_l = likedata.N_l(keep_ell, keep_bp);
end

% Downsize covariance matrices and bandpower window functions.
for ii=1:numel(keep_ell)
  keep_cov(1+(ii-1)*numel(keep_bp):ii*numel(keep_bp)) = ...
      keep_bp + (keep_ell(ii) - 1) * n_spec;
end
if isfield(likedata, 'bpwf')
  likedata.bpwf.Cs_l = likedata.bpwf.Cs_l(:, keep_cov);
  likedata.bpwf.rwf = likedata.bpwf.rwf(keep_ell, keep_bp);
end
if isfield(likedata, 'bpcov')
  % Most fields in this structure are shaped like covariance
  % matrix, except for C_fl field.
  fld = fields(likedata.bpcov);
  for ii=1:numel(fld)
    if strcmp(fld{ii}, 'C_fl')
      % Downsize C_fl field
      likedata.bpcov.(fld{ii}) = ...
          likedata.bpcov.(fld{ii})(keep_ell, keep_bp);
    else
      % Downsize other field
      likedata.bpcov.(fld{ii}) = ...
          likedata.bpcov.(fld{ii})(keep_cov, keep_cov);
    end
  end
end

% Downsize bandpass cell array.
if isfield(likedata, 'bandpass')
  likedata.bandpass = likedata.bandpass(keep_expt);
end

% Downsize opt.expt
likedata.opt.expt = likedata.opt.expt(keep_expt);
% Downsize opt.fields
likedata.opt.fields = likedata.opt.fields(keep_field);
% Downsize opt.l
likedata.opt.l = likedata.opt.l(keep_ell);
likedata.lval = likedata.lval(keep_ell);

return
