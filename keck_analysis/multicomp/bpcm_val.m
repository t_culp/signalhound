function [bp_expv,bpcm] = bpcm_val(param, likeopt)
% Takes same param vector as like_getexpvals
% Generates fake data for N experiments using a Gaussian random vector
% with length set by the number of degrees of freedom in each ell bin
% Computes all simulated auto and cross spectra for nsims
% Returns bandpowers for each sim, bpcm
%
% KDA 07-28-14
% KDA 09-29-14 Cleaned up code, added ell bin correlations

% Set default param vector
if length(param)==0
  param = [0.2, 1, 0, 0, -3.3, -0.6, 1.6, -0.4, 1, 1, 0, 19.6]; %tensors+lensing only
end

% set constants
nsims = 1e4; % Note: 1e6 sims requires extra memory for MATLAB!

% Get number of ell bins, CMB fields, and experiments.
n_ell = numel(likeopt.l);
n_expt=numel(likeopt.expt); % number of experiments
n_field = numel(unique(likeopt.fields));
M = n_expt * n_field;
n_spec = M * (M + 1) / 2; % Number of distinct expt/field combinations.
order = expt_field_order(n_expt, n_field); % H-L vecp ordering

%degrees of freedom parameter for each ell bin
%from http://bmode.caltech.edu/~spuder/analysis_logbook/analysis/20140205_sigmas/
dofs = [4, 16, 45, 87, 128, 134, 186, 292, 287, 332]; 
% Number of overlapping dof. Try to get ~10% correlation btw ell bins
dofs_corr = [1, 3, 7, 11, 13, 16, 24, 29, 31]; % = to 10% of avg dofs for each pair of bins
%dofs_corr = [1, 1, 4, 8, 14, 16, 24, 29, 31]; % ~10% corr for bins 2-6 (examined bpcm)

% Get bandpower expectation values for each signal component.
expv = like_getexpvals(param, likeopt, 2);
n_cpt = size(expv, 3);
  
% Generate fake noise and signal realizations for each ell bin, expt
% -- assume likeopt will contain N_l, C_fl
% -- correlation between ell bins modeled as overlap of gaussian
% random vectors for signal and noise
% -- Also assume same number of DOF for signal, noise in each expt.

% preallocate bandpower matrices
bps = zeros(n_ell, n_spec, nsims);
noise_bps = zeros(n_ell, n_spec, nsims); 
signal_bps = zeros(n_ell, n_spec, nsims);
sn_bps = zeros(n_ell, n_spec, nsims, 2); 
last_dof=0;
last_field = zeros(dofs(1), nsims, n_cpt);
last_noise = zeros(n_expt, dofs(1), nsims);
for bin=1:n_ell
  dof=dofs(likeopt.l(bin));

  % Signal realizations with average bandpower 1, to be scaled by
  % the expvals computed above. Seen by all experiments.
  field = zeros(dof, nsims, n_cpt);
  for j=1:n_cpt
    field(:,:,j) = generate_gauss_vec(1.0, dof, nsims);
    % ell bin signal correlations
    if bin>1
      corr=dofs_corr(likeopt.l(bin)-1);
      field(1:corr,:,j) = last_field(last_dof-corr+1:end,:,j) * ...
	sqrt(last_dof / dof);
    end
  end
  
  % generate noise realizations for this ell bin for each experiment
  % and combine with scaled signal terms to get total signal+noise
  total_sn=zeros(n_expt,dof,nsims);
  noise=zeros(n_expt,dof,nsims); % noise only
  signal=zeros(n_expt,dof,nsims); % signal only
  for i=1:n_expt
    % Want N_l for autospec for each experiment -- this is correct
    % because HL ordering lists all autospecs first
    this_noise = generate_gauss_vec(likeopt.N_l(bin,i), dof, nsims);
    % ell bin noise correlations
    if bin>1
      % Need to account for different noise amplitude across bins
      this_noise(1:corr,:) = ...
	  sqrt(likeopt.N_l(bin,i)/likeopt.N_l(bin-1,i)) * ...
          sqrt(last_dof / dof) * last_noise(i,last_dof-corr+1:end,:);
    end  
    this_signal = zeros(size(this_noise));
    for j=1:n_cpt
      this_signal = this_signal + sqrt(expv(bin,i,j)) * field(:,:,j);
    end
    total_sn(i,:,:) = this_noise + this_signal;
    noise(i,:,:) = this_noise;
    signal(i,:,:) = this_signal;
  end

  last_dof=dof;
  last_field=field;
  last_noise=noise;
 
  % compute all bandpowers for this ell bin
  for s=order
    bps(bin,s.i,:)=squeeze(dot(total_sn(s.e1,:,:),total_sn(s.e2,:,:),2)); 
    noise_bps(bin,s.i,:)=squeeze(dot(noise(s.e1,:,:),noise(s.e2,:,:),2));
    signal_bps(bin,s.i,:)=squeeze(dot(signal(s.e1,:,:),signal(s.e2,:,:),2));
    sn_bps(bin,s.i,:,1) = squeeze(dot(signal(s.e1,:,:), noise(s.e2,:,:), 2));
    sn_bps(bin,s.i,:,2) = squeeze(dot(noise(s.e1,:,:), signal(s.e2,:,:), 2));
  end
  for j=1:n_expt % remove noise bias for autospectra
    bps(bin,j,:) = bps(bin,j,:)-likeopt.N_l(bin,j);
  end
end

bp_expv = mean(bps,3) % bandpower expectation values, avg of sims

% Reshape bps appropriately and compute bandpower covariance matrix (bpcm)
bps2=zeros(n_spec*n_ell,nsims);
for i=1:n_ell
  for j=1:n_spec
    bps2((i-1)*n_spec+j,:)=bps(i,j,:);
  end
end
bpcm=cov(bps2');

% Zero blocks that are further off-diagonal than likeopt.offdiag
% This is done in the pipeline for the results paper -- do we want to
% eventually do this for the multicomp code, when we are done testing?
% It probably doesn't make a huge difference.
%for i=1:n_ell
%  for j=1:n_ell
%    if abs(i - j) > likeopt.offdiag;
%      % Zero this block, which has size [n_spec, n_spec].
%      bpcm((i-1)*n_spec+1: i*n_spec, (j-1)*n_spec+1: j*n_spec) = 0;
%    end
%  end
%end


% Save direct bpcm.
save('bps_corr_alt.mat','bps','bpcm'); % for me

% Save aps files. Works for BB only!!
for s=order
  if s.e1 == s.e2
    % Experiment auto-spectra.
    % aps(1) = signal-auto
    aps(1).l = [10, 37.5, 72.5, 107.5, 142.5, 177.5, 212.5, 247.5, 282.5, ...
                317.5, 352.5, 387.5, 422.5, 457.5, 492.5, 527.5, 562.5];
    aps(1).Cs_l = zeros(17,6,nsims);
    aps(1).Cs_l(likeopt.l,4,:) = signal_bps(:,s.i,:);
    % aps(2) = noise-auto
    aps(2) = aps(1);
    aps(2).Cs_l(likeopt.l,4,:) = noise_bps(:,s.i,:);
    % aps(3) = signal x noise
    aps(3) = aps(1);
    aps(3).Cs_l(likeopt.l,4,:) = sn_bps(:,s.i,:,1);
    % Save aps
    filename = sprintf('auto_%s.mat', likeopt.expt(s.e1).name);
    save(filename, 'aps', '-v7.3');
  else
    % Experiment cross-spectra.
    % aps(1) = S1 x S1
    aps(1).l = [10, 37.5, 72.5, 107.5, 142.5, 177.5, 212.5, 247.5, 282.5, ...
                317.5, 352.5, 387.5, 422.5, 457.5, 492.5, 527.5, 562.5];
    aps(1).Cs_l = zeros(17,6,nsims);
    aps(1).Cs_l(likeopt.l,4,:) = signal_bps(:,s.e1,:);
    % aps(2) = N1 x N1
    aps(2) = aps(1);
    aps(2).Cs_l(likeopt.l,4,:) = noise_bps(:,s.e1,:);
    % aps(3) = S2 x S2
    aps(3) = aps(1);
    aps(3).Cs_l(likeopt.l,4,:) = signal_bps(:,s.e2,:);
    % aps(4) = N2 x N2
    aps(4) = aps(1);
    aps(4).Cs_l(likeopt.l,4,:) = noise_bps(:,s.e2,:);
    % aps(5) = S1 x N1
    aps(5).l = [10, 37.5, 72.5, 107.5, 142.5, 177.5, 212.5, 247.5, 282.5, ...
                317.5, 352.5, 387.5, 422.5, 457.5, 492.5, 527.5, 562.5];
    aps(5).Cs_l = zeros(17,9,nsims);
    aps(5).Cs_l(likeopt.l,4,:) = sn_bps(:,s.e1,:,1);
    % aps(6) = N1 x S2
    aps(6) = aps(5);
    aps(6).Cs_l(likeopt.l,4,:) = sn_bps(:,s.i,:,2);
    % aps(7) = S2 x N2
    aps(7) = aps(5);
    aps(7).Cs_l(likeopt.l,4,:) = sn_bps(:,s.e2,:,1);
    % aps(8) = S1 x S2
    aps(8) = aps(5);
    aps(8).Cs_l(likeopt.l,4,:) = signal_bps(:,s.i,:);
    % aps(9) = N1 x N2
    aps(9) = aps(5);
    aps(9).Cs_l(likeopt.l,4,:) = noise_bps(:,s.i,:);
    % aps(10) = S1 x N2
    aps(10) = aps(5);
    aps(10).Cs_l(likeopt.l,4,:) = sn_bps(:,s.i,:,1);
    % Save aps
    filename = sprintf('cross_%s_%s.mat', likeopt.expt(s.e1).name, ...
                       likeopt.expt(s.e2).name);
    save(filename, 'aps', '-v7.3');
  end
end

end

%%%%%%%%%%%%%
% Helper function from Immanuel's code
function v = generate_gauss_vec(amplitude, dof, n_realization)
%Normalize so average auto spectrum = amplitude
	%randn gives mean 0 and std 1
	% dof * amplitude / dof = amplitude
	v = randn(dof, n_realization) * sqrt( amplitude / dof);
end
