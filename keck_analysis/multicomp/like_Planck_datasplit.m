function likeopt = like_Planck_datasplit(likeopt, fixNl)
% likeopt = like_Planck_datasplit(likeopt)
% 
% This function will look through all of the experiments found in the 
% likeopt structure. If there is any experiment 'X' where experiments 
% 'XH1' and 'XH2' also exist, it will replace the full map auto-spectrum 
% with the corresponding data split cross-spectrum, but leave the full map 
% cross-spectra with other frequencies/experiments (wrapper function for 
% likeopt_datasplit).
%
% [Input]
%   likeopt  likeopt data structure including full map and data
%            split components.
%   fixNl    Set this argument to 1 to set the N_l (noise bias) field for the 
%            data split crosses to 1/2 the average of the noise bias for the data 
%            split autos. This step allows you to use the likeopt structure with 
%            H-L likelihood.
%
% [Output]
%   likeopt  likeopt data structure with full map auto-spectra replaced by 
%            data split cross-spectra, and dropping unused data split results.

% Optional argument.
if nargin < 2
  fixNl = 0;
end
if isempty(fixNl)
  fixNl = 0;
end

% Loop over experiments.
for i=1:numel(likeopt.expt)
  % We will be downsizing likeopt as we go, so first check to see
  % if we are done.
  if i > numel(likeopt.expt)
    break
  end

  % Compile experiment names into a cell array. These change each
  % time through the outer loop!
  names = {};
  for j=1:numel(likeopt.expt)
    names{j} = likeopt.expt(j).name;
  end

  % Check to see if this experiment has associated data splits.
  ih1 = find(strcmp([names{i} 'H1'], names));
  ih2 = find(strcmp([names{i} 'H2'], names));
  if ~isempty(ih1) && ~isempty(ih2)
    % Call likeopt_datasplit to downsize the likeopt structure.
    likeopt = likeopt_datasplit(likeopt, i, [ih1, ih2], fixNl);
  end
end

% Note that we have performed this operation.
likeopt.Planck_datasplit = 1;
