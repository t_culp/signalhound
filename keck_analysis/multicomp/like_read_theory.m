function likedata = like_read_theory(likedata)
% likedata = like_read_theory(likedata)
%
% Reads CAMB files and stores theory spectra used for the multicomponent 
% model.
%
% [Input]
%   likedata  Data structure for multicomponent likelihood analysis.
%             The following fields of likedata.opt will be assigned default 
%             values in like_read_theory, if they haven't been specified 
%             already:
%     .camb_dir  
%     .camb_unlensed
%     .camb_lensed
%     .camb_tensor
%     .
% 
% Theory components:
%   1. likedata.theory.lcdm = Standard lensed-LCDM, except zero BB.
%   2. likedata.theory.lensing = Lensing BB, zero for all others.
%   3. likedata.theory.tensor = Tensor contribution to TT, EE, BB, TE.
%
% NOTE: Ordering of spectra in theory follows H-L convention -- TT,
% EE, BB, TE, TB, EB -- not standard pipeline ordering.

% Fill in default likedata.opt entries.
if ~isfield(likedata, 'opt')
  likedata.opt = [];
end
if ~isfield(likedata.opt, 'camb_dir')
  % Directory containing theory spectra from CAMB.
  likedata.opt.camb_dir = '/n/bicepfs1/bicep2/bicep2_aux_data/official_cl';
end
if ~isfield(likedata.opt, 'camb_unlensed')
  % File containing unlensed Cs_l.
  likedata.opt.camb_unlensed = 'camb_planck2013_r0.fits';
end
if ~isfield(likedata.opt, 'camb_lensed')
  % File containing lensed Cs_l.
  likedata.opt.camb_lensed = 'camb_planck2013_r0_rev1_lensing.fits';
end
if ~isfield(likedata.opt, 'camb_tensor')
  % File containing tensor Cs_l.
  likedata.opt.camb_tensor = 'camb_planck2013_r0p1.fits';
end
if ~isfield(likedata.opt, 'fg_pivot')
  % Set the ell value used as a pivot scale for foreground
  % power laws...
  likedata.opt.fg_pivot = 80;
end
if ~isfield(likedata.opt, 'sync_freq')
  % Pivot frequency for synchrotron foreground.
  likedata.opt.sync_freq = 150;
end
if ~isfield(likedata.opt, 'dust_freq')
  % Pivot frequency for dust foreground.
  likedata.opt.dust_freq = 353;
end

% Read CAMB files.
camb_unlensed = load_cmbfast(fullfile(likedata.opt.camb_dir, ...
                                      likedata.opt.camb_unlensed));
camb_lensed = load_cmbfast(fullfile(likedata.opt.camb_dir, ...
                                    likedata.opt.camb_lensed));
camb_tensor = load_cmbfast(fullfile(likedata.opt.camb_dir, ...
                                    likedata.opt.camb_tensor));

% Store ell values.
% For now, I am just assuming that the three CAMB files share
% common ell range.
likedata.theory.l = camb_unlensed.l;

% LCDM model with lensing, but zero BB.
likedata.theory.lcdm = camb_lensed.Cs_l(:, [1,3,4,2,5,6]);
likedata.theory.lcdm(:,3) = 0;

% Lensing contribution for BB only.
likedata.theory.lensing = zeros(size(likedata.theory.lcdm));
likedata.theory.lensing(:,3) = camb_lensed.Cs_l(:,4);

% Tensor contribution for TT, EE, BB, etc.
likedata.theory.tensor = camb_tensor.Cs_l(:,[1,3,4,2,5,6]) - ...
    camb_unlensed.Cs_l(:,[1,3,4,2,5,6]);

% Determine r value for tensor theory spectrum.
% Filename is usually something like 'camb_planck2013_r0p1.fits' to
% indicate r=0.1
if ~isempty(regexp(likedata.opt.camb_tensor, 'r(\d*)p(\d*)'))
  tok = regexp(likedata.opt.camb_tensor, 'r(\d*)p(\d*)', 'tokens');
  tok = tok{1};
  likedata.theory.r = str2num(tok{1}) + str2num(tok{2}) / 10;
else
  % Could also look for something like 'camb_planck2013_r0.fits'
  if ~isempty(regexp(likedata.opt.camb_tensor, 'r(\d*)'))
    tok = regexp(likedata.opt.camb_tensor, 'r(\d*)', 'tokens');
    tok = tok{1};
    likedata.theory.r = str2num(tok{1});
  else
    % Couldn't figure out theory r value.
    disp(sprintf(['[like_read_theory] WARNING: could not determine ' ...
                  'r value for CAMB tensor spectrum %s'], ...
                 likedata.opt.camb_tensor));
  end
end

% Check to see if the bandpower window functions have already been
% read using like_read_simset. If they have, then call trim_likeopt to 
% line up theory spectra with bpwf.
if isfield(likedata, 'bpwf')
  likedata = trim_likeopt(likedata);
end
