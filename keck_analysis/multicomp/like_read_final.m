function likedata = like_read_final(finalfile, likeopt)
% likedata = like_read_final(finalfile, likeopt)
%
% Reads bandpowers, noise bias, window functions, and suppression
% factors from a pipeline final file. Prepares data structure for
% multicomponent likelihood analysis.
% 
% This is generally the first function that is called to build up
% the data structure used to calculate multicomponent likelihoods. 
% The likedata structure returned by this function is then passed
% to like_read_simset, along with an aps file that corresponds to
% the same data reduction as this finalfile.
%
% [Input]
%   finalfile  Path to final bandpowers file
%   likeopt    Option structure, with the following fields:
%     .fields    Cell array listing CMB fields to include in the
%                analysis. Defaults to {'T', 'E', 'B'}.
%     .l         Array listing ell bins to include in the
%                analysis. Default to all ell bins in the final
%                bandpowers file (which is usually [1:17]).
%     .subsim    Set this to a positive integer to use that
%                realization from the simr field in place of the 
%                real data bandpowers.
% 
% [Output]
%   likedata   Data structure for multicomponent likelihood
%              analysis. Includes the following fields:
%     .opt       Copy of the input likeopt structure, with some
%                additions made in the course of this function.
%     .real      Real data bandpowers
%     .N_l       Noise bias bandpowers
%     .bpwf      Data structure containing window functions and
%                suppression factors:
%       .l         Ell range for window functions
%       .Cs_l      Bandpower window functions
%       .rwf       "Reciprocal window function", i.e. inverse of
%                  bandpower suppression factors

% Add options to likedata structure.
likedata.opt = likeopt;
if ~isfield(likedata.opt, 'subsim')
  % Use real data by default.
  likedata.opt.subsim = 0;
end

% Load final bandpowers file.
likedata.opt.finalfile = finalfile;
f = load(finalfile);

% Get experiment names.
n_expt = (-1 + sqrt(1 + 8 * numel(f.r))) / 2;
for ii=1:n_expt
  likedata.opt.expt{ii} = f.opt.finalopt.mapname{ii};
end

% If CMB fields are not specified, select all fields.
if ~isfield(likedata.opt, 'fields') || isempty(likedata.opt.fields)
  likedata.opt.fields = {'T', 'E', 'B'};
end
n_field = numel(likedata.opt.fields);

% If ell bins are not specified, select all ell bins in final
% bandpowers file.
if ~isfield(likedata.opt, 'l') || isempty(likedata.opt.l)
  likedata.opt.l = [1:numel(f.r(1).l)];
end
n_ell = numel(likedata.opt.l);
likedata.lval = f.r(1).l(likedata.opt.l);

% Copy data from final bandpowers file.
order = expt_field_order(n_expt, n_field);
n_spec = numel(order);
for s=order
  % Is this a cross between two different experiments?
  iscross = (s.e1 ~= s.e2);
  % Determine indices that correspond to this spectrum.
  xind = aps_getxind(numel(f.r), s.e1, s.e2);
  bpind = aps_getbpind(likedata.opt.fields{s.f1}, ...
                       likedata.opt.fields{s.f2}, iscross);
  wfind = bpwfind(likedata.opt.fields{s.f1}, likedata.opt.fields{s.f2}, ...
                  iscross);
  % Copy data.
  if likedata.opt.subsim
    % Copy data from simr field instead of real.
    likedata.real(:,s.i) = ...
        f.r(xind).simr(likedata.opt.l, bpind, likedata.opt.subsim);
  else
    % Real bandpowers.
    likedata.real(:,s.i) = f.r(xind).real(likedata.opt.l, bpind);
  end
  likedata.N_l(:,s.i) = f.r(xind).db(likedata.opt.l, bpind);
  likedata.bpwf.Cs_l(:,s.i+[0:n_ell-1]*n_spec) = ...
      f.bpwf(xind).Cs_l(:,likedata.opt.l,wfind);
  likedata.bpwf.rwf(:,s.i) = f.r(xind).rwf(likedata.opt.l, bpind);
end
% Ell values for window functions.
likedata.bpwf.l = f.bpwf(1).l;

return % END of function like_read_final


function ind = bpwfind(field1, field2, iscross)
% ind = bpwfind(field1, field2, iscross)
%
% For two fields, return the index of the corresponding bandpower
% window function in a standard pipeline bpwf structure.
%
% [Input]
%   field1   Letter 'T', 'E', or 'B' indicating the first CMB field.
%   field2   Letter 'T', 'E', or 'B' indicating the second CMB field.
%   iscross  0 for experiment auto-spectrum, 1 for cross
%
% [Output]
%   ind      Index for the desired window function
%
% [Examples]
%   >> bpwfind('T', 'T', 0) % returns 1
%   >> bpwfind('E', 'B', 0) % returns 3
%   >> bpwfind('E', 'B', 1) % returns 3
%   >> bpwfind('B', 'E', 0) % returns 3
%   >> bpwfind('B', 'E', 1) % returns 3

% Just in case someone passed cell arrays for field1, field2.
if iscell(field1)
  field1 = field1{1};
end
if iscell(field2)
  field2 = field2{1};
end

order = {'TT', 'TE', 'EE', 'BB', 'TB', 'EB', 'ET', 'BT', 'BE'};
if iscross
  % Use TE bpwf for TB; Use ET bpwf for BT; Use EE bpwf for EB, BE.
  index = [1, 2, 3, 4, 2, 3, 7, 7, 3];
else
  % Use TE bpwf for TB, ET, BT; Use EE bpwf for EB, BE.
  index = [1, 2, 3, 4, 2, 3, 2, 2, 3];
end
ind = index(strcmp(order, [field1 field2]));

return % END of function bpwfind
