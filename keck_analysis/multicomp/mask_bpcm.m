function bpcov = mask_bpcm(bpcov, likeopt)
% likeopt = mask_bpcm(likeopt)
%
% Create masks for bandpower covariance submatrices that remove terms with 
% zero expectation value under the assumption of independent noise for all 
% experiments.
%
% [Input]
%   bpcov    Data structure containing bandpower covariance matrix components
%   likeopt  Options data structure for multicomponent likelihood analysis
%
% [Output]
%   bpcov    Copy of input with additional fields for masks

% If likedata is passed as the second argument, just grab the opt field.
if isfield(likeopt, 'opt')
  likeopt = likeopt.opt;
end

% Determine size of the problem from likeopt.
n_ell = numel(likeopt.l);
n_expt = numel(likeopt.expt);
n_field = numel(likeopt.fields);
order = expt_field_order(n_expt, n_field);
n_spec = numel(order);

% Criteria depends on matrix type:
% 1. Signal-only: Keep all terms (no mask)
% 2. Noise-only: Keep variance terms only, i.e. Cov(N_i x N_j, N_k x N_l)
%    for (i,j) = (k,l).
% 3. Signal-noise: Keep terms where the noise contributions come from the 
%    same expt, i.e. Cov(S_i x N_j, S_k x N_l) for j=l.
% Calculate the masks for a single n_spec x n_spec block, then just 
% repeat them for all n_ell x n_ell blocks.
mask_sig = ones(n_spec, n_spec);
mask_noi = zeros(n_spec, n_spec);
mask_sn1 = zeros(n_spec, n_spec);
mask_sn2 = zeros(n_spec, n_spec);
mask_sn3 = zeros(n_spec, n_spec);
mask_sn4 = zeros(n_spec, n_spec);
for i=1:n_spec
  for j=1:n_spec
    % Get experiment indices to use for selection criteria.
    e1 = order(i).e1;
    e2 = order(i).e2;
    e3 = order(j).e1;
    e4 = order(j).e2;

    % Noise-only
    if ((e1 == e3) && (e2 == e4)) || ((e1 == e4) && (e2 == e3))
      mask_noi(i,j) = 1;
    end
    % Signal-noise
    if (e2 == e4)
      mask_sn1(i,j) = 1;
    end
    if (e2 == e3)
      mask_sn2(i,j) = 1;
    end
    if (e1 == e4)
      mask_sn3(i,j) = 1;
    end
    if (e1 == e3)
      mask_sn4(i,j) = 1;
    end
  end
end

% Replicate over ell bins.
bpcov.mask_sig = repmat(mask_sig, [n_ell, n_ell]);
bpcov.mask_noi = repmat(mask_noi, [n_ell, n_ell]);
bpcov.mask_sn1 = repmat(mask_sn1, [n_ell, n_ell]);
bpcov.mask_sn2 = repmat(mask_sn2, [n_ell, n_ell]);
bpcov.mask_sn3 = repmat(mask_sn3, [n_ell, n_ell]);
bpcov.mask_sn4 = repmat(mask_sn4, [n_ell, n_ell]);

return % END of function mask_bpcm
