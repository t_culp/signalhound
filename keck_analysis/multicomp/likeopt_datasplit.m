function likeopt = likeopt_datasplit(likeopt, ifull, isplit, fixNl)
% likeopt = likeopt_datasplit(likeopt, ifull, isplit)
%
% Merge Planck results from full map and data split maps in a likeopt data 
% structure.
%
% [Input]
%   likeopt  likeopt data structure, including all spectra from both full 
%            and data split maps.
%   ifull    Experiment index for the full map.
%   isplit   Experiment indices for the two corresponding data split maps.
%   fixNl    Set this argument to 1 to set the N_l (noise bias) field for the 
%            data split cross to 1/2 the average of the noise bias for the data 
%            split autos. This step allows you to use the likeopt structure with 
%            H-L likelihood.
%
% [Output]
%   likeopt  likeopt data structure, downsized to use data split cross for 
%            single-frequency and full maps for cross-frequency.

% Determine size of input likeopt.
n_expt = numel(likeopt.expt);
n_field = numel(likeopt.fields);
n_maps = n_expt * n_field;
n_spec = n_maps * (n_maps + 1) / 2;
n_ell = numel(likeopt.l);
n_bp = n_spec * n_ell;

% Replace Planck full map auto-spectra with data split crosses.
% Do this for each field combination.
order = expt_field_order(1, n_field);
for s=order
  % Index of the full map auto-spectrum.
  iauto = likeopt_getxind(likeopt, ifull, ifull, s.f1, s.f2);
  iautol = iauto + n_spec * [0:n_ell-1];

  if s.f1 == s.f2
    % Index of data split cross-spectrum.
    icross = likeopt_getxind(likeopt, isplit(1), isplit(2), s.f1, s.f2);
    icrossl = icross + n_spec * [0:n_ell-1];

    % Replace full map auto with data split cross.
    likeopt.real(:,iauto) = likeopt.real(:,icross);
    if fixNl
      % Following Appendix C of Hamimeche & Lewis (2008), set the noise bias 
      % for the data split cross to something like the noise bias on the 
      % full map auto, except with a penalty for uneven splits.

      % Find location for data split autos.
      iauto_h1 = likeopt_getxind(likeopt, isplit(1), isplit(1), s.f1, s.f2);
      iauto_h2 = likeopt_getxind(likeopt, isplit(2), isplit(2), s.f1, s.f2);
      % Replace N_l with 1/2 of the average N_l from data split autos.
      likeopt.N_l(:,iauto) = ...
          0.25 * (likeopt.N_l(:,iauto_h1) + likeopt.N_l(:,iauto_h2));
    else
      % Just copy N_l from data split cross.
      likeopt.N_l(:,iauto) = likeopt.N_l(:,icross);
    end
    likeopt.bpwf.Cs_l(:,iautol) = likeopt.bpwf.Cs_l(:,icrossl);
    likeopt.aps.sig = matrix_fix1(likeopt.aps.sig, iautol, icrossl);
    likeopt.aps.noi = matrix_fix1(likeopt.aps.noi, iautol, icrossl);

    % Signal x noise is really a cross, even if the CMB field combination 
    % is TT, EE, or BB. So we need to combine both signal x H1noise and
    % signal x H2noise.
    % Since all signal sims are the same, it doesn't really matter which 
    % experiment is corresponds to "signal". However, the order of fields 
    % *is* important because Var(Esig x Bnoi) != Var(Enoi x Bsig). The 
    % function likeopt_getxind will swap field ordering if e1 > e2; to 
    % avoid that, I am always using e1 = e2 here.
    icross1 = likeopt_getxind(likeopt, isplit(1), isplit(1), s.f1, s.f2);
    icross1l = icross1 + n_spec * [0:n_ell-1];
    icross2 = likeopt_getxind(likeopt, isplit(2), isplit(2), s.f1, s.f2);
    icross2l = icross2 + n_spec * [0:n_ell-1];
    likeopt.aps.sn1 = matrix_fix2(likeopt.aps.sn1, iautol, ...
                                  icross1l, icross2l);
    likeopt.aps.sn2 = matrix_fix2(likeopt.aps.sn2, iautol, ...
                                  icross1l, icross2l);
    likeopt.aps.sn3 = matrix_fix2(likeopt.aps.sn3, iautol, ...
                                  icross1l, icross2l);
    likeopt.aps.sn4 = matrix_fix2(likeopt.aps.sn4, iautol, ...
                                  icross1l, icross2l);
  else
    % Two different data split cross-spectra.
    icross1 = likeopt_getxind(likeopt, isplit(1), isplit(2), s.f1, s.f2);
    icross1l = icross1 + n_spec * [0:n_ell-1];
    icross2 = likeopt_getxind(likeopt, isplit(1), isplit(2), s.f2, s.f1);
    icross2l = icross2 + n_spec * [0:n_ell-1];

    % Replace full map auto with combination of data split crosses.
    likeopt.real(:,iauto) = (likeopt.real(:,icross1) + ...
                             likeopt.real(:,icross2)) / 2;
    if fixNl
      % Following Appendix C of Hamimeche & Lewis (2008), set the noise bias 
      % for the data split cross to something like the noise bias on the 
      % full map auto, except with a penalty for uneven splits.

      % Find location for data split autos.
      iauto_h1 = likeopt_getxind(likeopt, isplit(1), isplit(1), s.f1, s.f2);
      iauto_h2 = likeopt_getxind(likeopt, isplit(2), isplit(2), s.f1, s.f2);
      % Replace N_l with 1/2 of the average N_l from data split autos.
      likeopt.N_l(:,iauto) = ...
          0.25 * (likeopt.N_l(:,iauto_h1) + likeopt.N_l(:,iauto_h2));
    else
      % Just copy N_l from data split cross.
      likeopt.N_l(:,iauto) = (likeopt.N_l(:,icross1) + ...
                              likeopt.N_l(:,icross2)) / 2;
    end
    likeopt.bpwf.Cs_l(:,iautol) = (likeopt.bpwf.Cs_l(:,icross1l) + ...
                                   likeopt.bpwf.Cs_l(:,icross2l)) / 2;
    likeopt.aps.sig = matrix_fix2(likeopt.aps.sig, iautol, ...
                                  icross1l, icross2l);
    likeopt.aps.noi = matrix_fix2(likeopt.aps.noi, iautol, ...
                                  icross1l, icross2l);

    % Since all signal sims are the same, it doesn't really matter which 
    % experiment is corresponds to "signal". However, the order of fields 
    % *is* important because Var(Esig x Bnoi) != Var(Enoi x Bsig). The 
    % function likeopt_getxind will swap field ordering if e1 > e2; to 
    % avoid that, I am always using e1 = e2 here.
    icross1 = likeopt_getxind(likeopt, isplit(1), isplit(1), s.f1, s.f2);
    icross1l = icross1 + n_spec * [0:n_ell-1];
    icross2 = likeopt_getxind(likeopt, isplit(2), isplit(2), s.f1, s.f2);
    icross2l = icross2 + n_spec * [0:n_ell-1];
    likeopt.aps.sn1 = matrix_fix2(likeopt.aps.sn1, iautol, ...
                                  icross1l, icross2l);
    likeopt.aps.sn2 = matrix_fix2(likeopt.aps.sn2, iautol, ...
                                  icross1l, icross2l);
    likeopt.aps.sn3 = matrix_fix2(likeopt.aps.sn3, iautol, ...
                                  icross1l, icross2l);
    likeopt.aps.sn4 = matrix_fix2(likeopt.aps.sn4, iautol, ...
                                  icross1l, icross2l);
  end
end

% Now, use likeopt_select to remove the data splits.
expt_select = find(~ismember([1:n_expt], isplit));
likeopt = likeopt_select(likeopt, expt_select, [], []);


% Subfunction: matrix_fix1
% ---
% Substitute covariance matrix elements representing the data split cross 
% for the elements that correspond to full map autos.
% Use this version of the function for the case where both CMB fields are 
% the same (TT, EE, BB). Use matrix_fix2 for TE, TB, EB.
function m_out = matrix_fix1(m_in, iauto, icross)

% Copy input matrix to output.
m_out = m_in;
% Replace rows and columns corresponding to full map auto with those 
% corresponding to data split cross.
m_out(iauto,:) = m_in(icross,:);
m_out(:,iauto) = m_in(:,icross);
% Replace diagonal elements in each block corresponding to variance of 
% full map auto with the variance of the data split cross.
m_out(iauto,iauto) = m_in(icross,icross);


% Subfunction: matrix_fix2
% ---
% Like matrix_fix1, except that it handles the more complicated case where 
% we need to keep track of both a cross and alt-cross.
function m_out = matrix_fix2(m_in, iauto, icross1, icross2)

% Copy input matrix to output.
m_out = m_in;
% Replace rows and columns corresponding to full map auto with the average 
% of the rows/columns for the corresponding cross and alt-cross.
m_out(iauto,:) = (m_in(icross1,:) + m_in(icross2,:)) / 2;
m_out(:,iauto) = (m_in(:,icross1) + m_in(:,icross2)) / 2;
% Replace diagonal elements in each block corresponding to variance of 
% full map auto with variance calculate for data split cross and alt-cross.
m_out(iauto,iauto) = (m_in(icross1,icross1) + m_in(icross1,icross2) + ...
                      m_in(icross2,icross1) + m_in(icross2,icross2)) / 4;

