
% Produces all the plots in the Multi-Frequency Analysis Results posting.
%
% Loops over choices for beta_sync, dust/sync correlation, type of
% likelihood evaluation and choice of experiments.

clear; 

beta_sync = [-3.3:0.3:-3.0];
corr_range = [0:0.25:1];
likoption = {'chi2','hl','chi2_cpts','hl_cpts'}; % options for the likelihood evaluation
expt = {'b2','b2b1','b2wk','b2b1wk'}; % choices for experiments

for i = 1:length(beta_sync)
    for j = 1:length(corr_range)
        for k = 1:length(likoption)
            for m = 1:length(expt)
                multidim_lik_plot(i, j, likoption{k}, expt{m})
            end
        end
    end
end
