function maxL_toysim(savename, simrange, loadfile) 
% Finds the max likelihood values for a subset of toy sim
% realizations in the matrix bps. The parameters r, A_sync, and A_dust
% are allowed to vary while the others are held constant at their
% fiducial model (model 0) values. Three different likelihood
% estimators are used: H-L, dynamic chi2, and simple chi2.
%
% Inputs:
% savename: the path to the output file.
% simrange: the starting and ending sim indices (ex. [1 100])
% loadfile: file containing all necessary inputs. The relevant variables are:
%    likeopt -- likeopt structure built for the simset (contains
%    semi-analytic bpcm)
%    p -- the fiducial model vector (starting guess for H-L)
%    p_sims -- the parameter vector used to generate the simset (not
%    used in the likelihood computations)
%    bps -- a set of sims constructed using the parameters in p_sims
%    prep -- precomputed structure needed for H-L
%
% All variables in the workspace are saved to an output file. The
% relevant variables are:
%    maxL_hl -- the H-L maximum likelihood for each sim
%    maxL1_hl -- the H-L maximum likelihood for each sim if the
%    maximization algorithm's starting point is perturbed (test
%    convergence)
%    maxL_c -- the simple chi2 ML for each sim
%    maxL1_c -- the simple chi2 ML for each sim with perturbed
%    starting point
%    maxL_c2 -- the dynamic chi2 ML for each sim
%    maxL1_c2 -- the dynamic chi2 ML for each sim with perturbed
%    starting point
%    e_hl/c/c2 and e1_hl/c/c2 -- error flags for minimization
%    algorithm (i.e. does the algorithm think it converged to an
%    actual ML solution. 1=yes, 0=no)
%    looptime -- how long the main loop took to run (in
%    seconds). Runtime scales linearly w/number of sims.
%
% KDA 10-07-2014
% Update 11-10-2014 perturb x1 only in the positive direction to get better
% convergence behavior.
% Update 11-18-2014 -- added comments, committed to pipeline.

nsims = simrange(2) - simrange(1) + 1;

maxL_c=zeros(nsims,3);% Note: assumes nsims > n_spec. Should be ok.
Lval_c=zeros(nsims,1);
maxL_c2=zeros(nsims,3);
Lval_c2=zeros(nsims,1);
maxL_hl=zeros(nsims,3);
Lval_hl=zeros(nsims,1);
e_c=zeros(nsims,1); % error flags for fminsearch
e_c2=zeros(nsims,1);
e_hl=zeros(nsims,1);
e1_c=zeros(nsims,1);
e1_c2=zeros(nsims,1);
e1_hl=zeros(nsims,1);

opt = optimset('MaxIter',2500,'MaxFunEvals',2500,'TolX',1e-10,'TolFun',1e-10);

nloop=nsims;
load(loadfile);
likeopt.bpcm=bpcm;
tic;
% In this loop, vary r, A_s, A_d 
for k = 1:nloop 
  likeopt.real=bps(:,:,k+simrange(1)-1);
  
  % simple chi2
  chi_fun = @(x) -like_chi2(like_getexpvals([x(1) p(2) x(2) x(3) p(5:end)], ...
      likeopt), likeopt.real, likeopt.bpcm);
  
  % dynamic chi2
  chi2_fun = @(x) chi2_func(x,p,likeopt);
  
  % HL
  hl_fun = @(x) hl_func(x, p, likeopt, prep);
   
  % randomly vary x0 to test convergence 
  % approx width of randn distrib taken from multicomp results III,
  % confirmed with width of full likelihood distribs from a sim realization
  x0 = [p(1) p(3:4)];
  x1 = [p(1) p(3:4)] + abs(randn(1,3)).*[0.05 5e-5 0.8];% B2+Planck vals
%  x1 = [p(1) p(3:4)] + randn(1,3).*[0.1 5e-5 0.006]; % B2+B1+WMAP7K vals

    while true
    try
      [maxL_hl(k,:),Lval_hl(k),e_hl(k)]=fminsearch(hl_fun,x0,opt);
      break;
    catch exception
      disp(sprintf('Exception caught: sim %i is too negative',k))
      x0 = [p(1) p(3:4)] + abs(randn(1,3)).*[0.05 5e-5 0.8];
    end
  end
  while true
    try
      [maxL1_hl(k,:),Lval1_hl(k),e1_hl(k)]=fminsearch(hl_fun,x1,opt);
      break;
    catch exception
      disp('Exception caught')
      x1 = [p(1) p(3:4)] + abs(randn(1,3)).* [0.05 5e-5 0.8];
    end
  end
  [maxL_c(k,:),Lval_c(k),e_c(k)]=fminsearch(chi_fun,x0,opt);
  [maxL_c2(k,:),Lval_c2(k),e_c2(k)]=fminsearch(chi2_fun,x0,opt);
  [maxL1_c(k,:),Lval1_c(k),e1_c(k)]=fminsearch(chi_fun,x1,opt);
  [maxL1_c2(k,:),Lval1_c2(k),e1_c2(k)]=fminsearch(chi2_fun,x1,opt);  
end
looptime=toc;
save(savename);
end

function c2 = chi2_func(x, p, likeopt)
[ell,rms]=model_rms([x(1) p(2) x(2) x(3) p(5:end)],likeopt);
c2 =  -like_chi2(like_getexpvals([x(1) p(2) x(2) x(3) p(5:end)], ...
      likeopt), likeopt.real, scale_bpcm(rms, likeopt));
end

function hl = hl_func(x, p, likeopt, prep)
hl = -like_hl(like_getexpvals([x(1) p(2) x(2) x(3) p(5:end)], ...
       likeopt), likeopt.real, likeopt.N_l, prep);
if hl == 1e20
  hl = 'STARTOVER'; % force restart if code enters bad region of param space
end
end