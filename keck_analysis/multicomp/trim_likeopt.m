function likedata = trim_likeopt(likedata)
% likedata = trim_likeopt(likedata)
%
% Theory spectra and bandpower window functions are both specified at 
% delta-ell = 1 resolution, but usually over different ranges. This
% function checks likedata.bpwf.l and likedata.theory.l, then adjusts them 
% to use a common ell range.
%
% [Input]
%   likedata  Data structure for multicomponent likelihood.
%
% [Output]
%   likedata  Same data structure, but with bpwf and model spectra
%             trimmed.

% Check to make sure that likedata.bpwf and likedata.theory both exist.
if ~isfield(likedata, 'bpwf') || ~isfield(likedata, 'theory')
  return
end

% Get min and max ell values.
lmin = max(likedata.bpwf.l(1), likedata.theory.l(1));
lmax = min(likedata.bpwf.l(end), likedata.theory.l(end));

% Trim bpwf.
l1 = find(likedata.bpwf.l == lmin);
l2 = find(likedata.bpwf.l == lmax);
likedata.bpwf.l = likedata.bpwf.l(l1:l2);
likedata.bpwf.Cs_l = likedata.bpwf.Cs_l(l1:l2,:);

% Trim theory spectra.
l1 = find(likedata.theory.l == lmin);
l2 = find(likedata.theory.l == lmax);
likedata.theory.l = likedata.theory.l(l1:l2);
likedata.theory.lcdm = likedata.theory.lcdm(l1:l2,:);
likedata.theory.lensing = likedata.theory.lensing(l1:l2,:);
likedata.theory.tensor = likedata.theory.tensor(l1:l2,:);

return % END of trim_likeopt
