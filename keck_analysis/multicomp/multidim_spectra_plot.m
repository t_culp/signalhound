%
% Takes in a likeopt structure and a number of models and produces 6 spectra
% that contrast the real bp to the calculated expv for each of the models.
% It also calculates bpdev in units of sigma, and calculates the chi2 for
% each of the models.
%
% NOTE: you need to feed it a likeopt structure that was calculated by
% including three experiments; this can be done after either after you evaluate 
% the wrapper, or load it from a file.
%
% 06-24-2014 VB

clear;

% Load the right likeopt structure
% load('/n/home01/vbuza/code/work/data/b2b1_100b1_150.mat');
load('/n/home01/vbuza/code/work/data/b2b1_100wk.mat');

n_expt = length(likeopt.expt); % nr of experiments
N = n_expt*(n_expt+1)/2; % number of spectra
n_ell = length(likeopt.l); % number of ell bins

% Forming the right names of the experiments -- relevant just for the
% titles
expt_order = expt_field_order(n_expt, length(likeopt.fields));
band_names = cell(1, length(expt_order(1,:)));
for i = 1:length(expt_order(1,:))
    if expt_order(i).e1 == expt_order(i).e2
        band_names{i} = strcat(likeopt.expt(expt_order(i).e1).name);
    elseif expt_order(i).e1 ~= expt_order(i).e2
        band_names{i} = strcat(likeopt.expt(expt_order(i).e1).name, 'x', likeopt.expt(expt_order(i).e2).name);
    end
end
band_names = strrep(band_names,'_','\_');

% Make the multipole number of ell band
for i=1:length(likeopt.l)
l(i) = sum(likeopt.bpwf(1).l' .* likeopt.bpwf(1).Cs_l(:,i+1,4));
end

% Take the bpcm, invert it, and take the error as the sqrt of the diagonals of bpcm
bpcm = likeopt.bpcm;
ibpcm = inv(bpcm);
err = sqrt(diag(bpcm));

% Specify your default parameters.
p = [0, 1, 0, 0, -3.0, -0.6, 1.5, -0.4, 1, 1, 0];

% Specify your models

% Case 1 -- contrasting B2B1WK with B2B1
% %Model 1, no dust, r=0.2
% p1 = p;
% p1(1) = 0.2;
% p1(3) = 0;
% p1(4) = 0;
% exp1 = like_getexpvals(p1, likeopt, 1);
% exp1_bp = like_getexpvals(p1, likeopt);
% 
% % Model 2, r/dust/sync amplitude taken from best fit of B2B1WK
% p2 = p;
% p2(1) = 0.03;
% p2(3) = 1.1e-4;
% %p2(4) = 6e-3;
% p2(4) = 9e-3;
% exp2 = like_getexpvals(p2, likeopt, 1);
% exp2_bp = like_getexpvals(p2, likeopt);
%     
% % Model 3, r/dust amplitude taken from best fit of B2B1, sync from B2B1WK
% p3 = p;
% % p3(1) = 0.12;
% p3(1) = 0.15;
% p3(3) = 1.1e-4;
% p3(4) = 3e-3;
% exp3 = like_getexpvals(p3, likeopt, 1);
% exp3_bp = like_getexpvals(p3, likeopt);

% Case 2 -- contrasting different corr % for B2B1Wk
%Model 1, r/dust/sync amplitude taken from best fit of B2B1WK with 0%
% correlation
p1 = p;
p1(1) = 0.03;
p1(3) = 1.1e-4;
p1(4) = 8e-3;
p1(11) = 0;
exp1 = like_getexpvals(p1, likeopt, 1);
exp1_bp = like_getexpvals(p1, likeopt);

% Model 2, r/dust/sync amplitude taken from best fit of B2B1WK with 0%
% correlation
p2 = p;
% p2(1) = 0.03;
% p2(3) = 1.1e-4;
% p2(4) = 8e-3;
p2(1) = 0.15;
p2(3) = 1.05e-4;
p2(4) = 1e-4;
p2(11) = 0.25;
exp2 = like_getexpvals(p2, likeopt, 1);
exp2_bp = like_getexpvals(p2, likeopt);
    
% Model 3, r/dust/sync amplitude taken from best fit of B2B1WK with 0%
% correlation
p3 = p;
% p3(1) = 0.03;
% p3(3) = 1.1e-4;
% p3(4) = 8e-3;
p3(1) = 0.2;
p3(3) = 1.1e-4;
p3(4) = 5e-5;
p3(11) = 0.5;
exp3 = like_getexpvals(p3, likeopt, 1);
exp3_bp = like_getexpvals(p3, likeopt);

figure();
%set(gcf, 'PaperPosition',[0 0 10 6]);
setwinsize(gcf,1525,800);

for i=1:N
    % subplot(2,3,i)
    % Plot real values with their respective errors at each ell 
    % errorbar(l, likeopt.real(:,i), err(i:N:30),'.','LineWidth', 1.5, 'MarkerSize', 15);
    
    bpcm_i = bpcm(i:N:30,i:N:30);
    ibpcm_i = inv(bpcm_i);
    real_i = likeopt.real(:,i);
    
    bpdev1 = real_i - exp1_bp(:,i);
    bpdev2 = real_i - exp2_bp(:,i);
    bpdev3 = real_i - exp3_bp(:,i);
    
    sig = err(i:N:30);
    
    bpdev1_sig = bpdev1./sig;
    bpdev2_sig = bpdev2./sig;
    bpdev3_sig = bpdev3./sig;
    
    chi2(1,i) = bpdev1' * ibpcm_i * bpdev1;
    chi2(2,i) = bpdev2' * ibpcm_i * bpdev2;
    chi2(3,i) = bpdev3' * ibpcm_i * bpdev3;

    hold all;
    
    if i == 1 
        subplot(6,3,[1,4])
        % Uncomment next 2 lines if you want to plot smooth exp value lines for both models
        errorbar(l, real_i, sig,'.','LineWidth', 1.5, 'MarkerSize', 15);
        hold all;
        plot(exp1(:,i), 'r','LineWidth', 1.5);
        hold all;
        plot(exp2(:,i), 'Color', [0 0.5 0],'LineWidth', 1.5);
        hold all;
        plot(exp3(:,i), 'Color', [0 0.85 1],'LineWidth', 1.5);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([0 0.04])
        xlim([0 200])
        %xlabel('Multipole'); 
        ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]');
        title(strcat(band_names(i),', BB'));
        master_legend = legend('Real bp','corr 0%','corr 25%','corr 50%');
        set(master_legend,'Position',[0.0138541666666667 0.515888888888889 0.0803125 0.163333333333333]);
        
        subplot(6,3,7)
        plot(l, bpdev1_sig, 'r','LineWidth', 1.5 );
        %plot(l, bpdev1_sig, 'r.','MarkerSize', 10);
        hold all;
        plot(l, bpdev2_sig, 'Color', [0 0.5 0],'LineWidth', 1.5);
        %plot(l, bpdev2_sig,'.', 'Color', [0 0.5 0],'MarkerSize', 10);
        hold all;
        plot(l, bpdev3_sig, 'Color', [0 0.85 1],'LineWidth', 1.5);
        %plot(l, bpdev3_sig, '.', 'Color', [0 0.85 1],'MarkerSize', 10);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([-3 3])
        xlim([0 200])
        set(gca,'XTickLabel',[])
        %xlabel('Multipole'); 
        ylabel('bpdev [\sigma]');
        texstring1 = texlabel(sprintf('chi^2=%.3g', chi2(1,i)));
        text(205,2.5,texstring1,'EdgeColor',[1 0 0], 'LineWidth', 1.5);
        texstring2 = texlabel(sprintf('chi^2=%.3g', chi2(2,i)));
        text(205,0,texstring2,'EdgeColor',[0 0.5 0], 'LineWidth', 1.5);
        texstring3 = texlabel(sprintf('chi^2=%.3g', chi2(3,i)));
        text(205,-2.5,texstring3,'EdgeColor',[0 0.85 1], 'LineWidth', 1.5);
        %legend1 = legend(sprintf('c2=%.3g', chi2(1,i)), sprintf('c2=%.3g', chi2(2,i)),sprintf('c2=%.3g', chi2(3,i)),'Location','South',...
        %    'Orientation','Horizontal');
        %set(legend1,'FontSize',8);
        
    elseif i == 2
        subplot(6,3,[2,5])
        errorbar(l, real_i, sig,'.','LineWidth', 1.5, 'MarkerSize', 15);
        hold all;
        plot(exp1(:,i), 'r','LineWidth', 1.5);
        hold all;
        plot(exp2(:,i), 'Color', [0 0.5 0],'LineWidth', 1.5);
        hold all;
        plot(exp3(:,i), 'Color', [0 0.85 1],'LineWidth', 1.5);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([-0.05 0.05])
        xlim([0 200])
        %xlabel('Multipole'); 
        %ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]');
        title(strcat(band_names(i),', BB'));
        
        subplot(6,3,8)
        plot(l, bpdev1_sig, 'r','LineWidth', 1.5, 'MarkerSize', 10);
        hold all;
        plot(l, bpdev2_sig, 'Color', [0 0.5 0],'LineWidth', 1.5, 'MarkerSize', 10);
        hold all;
        plot(l, bpdev3_sig, 'Color', [0 0.85 1],'LineWidth', 1.5, 'MarkerSize', 10);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([-3 3])
        xlim([0 200])
        set(gca,'XTickLabel',[])
        texstring1 = texlabel(sprintf('chi^2=%.3g', chi2(1,i)));
        text(205,2.5,texstring1,'EdgeColor',[1 0 0], 'LineWidth', 1.5);
        texstring2 = texlabel(sprintf('chi^2=%.3g', chi2(2,i)));
        text(205,0,texstring2,'EdgeColor',[0 0.5 0], 'LineWidth', 1.5);
        texstring3 = texlabel(sprintf('chi^2=%.3g', chi2(3,i)));
        text(205,-2.5,texstring3,'EdgeColor',[0 0.85 1], 'LineWidth', 1.5);
        %xlabel('Multipole'); 
        %ylabel('# sigma');
        %title(strcat(band_names(i),', BB'));

    elseif i == 3 
        subplot(6,3,[3,6])  
        errorbar(l, real_i, sig,'.','LineWidth', 1.5, 'MarkerSize', 15);
        hold all;
        plot(exp1(:,i), 'r','LineWidth', 1.5);
        hold all;
        plot(exp2(:,i), 'Color', [0 0.5 0],'LineWidth', 1.5);
        hold all;
        plot(exp3(:,i), 'Color', [0 0.85 1],'LineWidth', 1.5);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([-10 15])
        xlim([0 200])
        %xlabel('Multipole'); 
        %ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]');
        title(strcat(band_names(i),', BB'));
        
        subplot(6,3,9)
        plot(l, bpdev1_sig, 'r','LineWidth', 1.5);
        hold all;
        plot(l, bpdev2_sig, 'Color', [0 0.5 0],'LineWidth', 1.5);
        hold all;
        plot(l, bpdev3_sig, 'Color', [0 0.85 1],'LineWidth', 1.5);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([-3 3])
        xlim([0 200])
        set(gca,'XTickLabel',[])
        texstring1 = texlabel(sprintf('chi^2=%.3g', chi2(1,i)));
        text(205,2.5,texstring1,'EdgeColor',[1 0 0], 'LineWidth', 1.5);
        texstring2 = texlabel(sprintf('chi^2=%.3g', chi2(2,i)));
        text(205,0,texstring2,'EdgeColor',[0 0.5 0], 'LineWidth', 1.5);
        texstring3 = texlabel(sprintf('chi^2=%.3g', chi2(3,i)));
        text(205,-2.5,texstring3,'EdgeColor',[0 0.85 1], 'LineWidth', 1.5);
        %xlabel('Multipole'); 
        %ylabel('# sigma');
        
    elseif i == 4
        subplot(6,3,[10,13])
        % Uncomment next 2 lines if you want to plot smooth exp value lines for both models
        errorbar(l, real_i, sig,'.','LineWidth', 1.5, 'MarkerSize', 15);
        hold all;
        plot(exp1(:,i), 'r','LineWidth', 1.5);
        hold all;
        plot(exp2(:,i), 'Color', [0 0.5 0],'LineWidth', 1.5);
        hold all;
        plot(exp3(:,i), 'Color', [0 0.85 1],'LineWidth', 1.5);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([-0.02 0.04])
        xlim([0 200])
        %xlabel('Multipole'); 
        ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]');
        title(strcat(band_names(i),', BB'));
        
        subplot(6,3,16)
        plot(l, bpdev1_sig, 'r','LineWidth', 1.5);
        hold all;
        plot(l, bpdev2_sig, 'Color', [0 0.5 0],'LineWidth', 1.5);
        hold all;
        plot(l, bpdev3_sig, 'Color', [0 0.85 1],'LineWidth', 1.5);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([-3 3])
        xlim([0 200])
        set(gca,'XTickLabel',[])
        texstring1 = texlabel(sprintf('chi^2=%.3g', chi2(1,i)));
        text(205,2.5,texstring1,'EdgeColor',[1 0 0], 'LineWidth', 1.5);
        texstring2 = texlabel(sprintf('chi^2=%.3g', chi2(2,i)));
        text(205,0,texstring2,'EdgeColor',[0 0.5 0], 'LineWidth', 1.5);
        texstring3 = texlabel(sprintf('chi^2=%.3g', chi2(3,i)));
        text(205,-2.5,texstring3,'EdgeColor',[0 0.85 1], 'LineWidth', 1.5);
        %xlabel('Multipole'); 
        ylabel('bpdev [\sigma]');

    elseif i == 5
        subplot(6,3,[11,14])
        errorbar(l, real_i, sig,'.','LineWidth', 1.5, 'MarkerSize', 15);
        hold all;
        plot(exp1(:,i), 'r','LineWidth', 1.5);
        hold all;
        plot(exp2(:,i), 'Color', [0 0.5 0],'LineWidth', 1.5);
        hold all;
        plot(exp3(:,i), 'Color', [0 0.85 1],'LineWidth', 1.5);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([-0.2 0.25])
        xlim([0 200])
        %xlabel('Multipole'); 
        %ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]');
        title(strcat(band_names(i),', BB'));
        
        subplot(6,3,17)
        plot(l, bpdev1_sig, 'r','LineWidth', 1.5);
        hold all;
        plot(l, bpdev2_sig, 'Color', [0 0.5 0],'LineWidth', 1.5);
        hold all;
        plot(l, bpdev3_sig, 'Color', [0 0.85 1],'LineWidth', 1.5);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([-3 3])
        xlim([0 200])
        set(gca,'XTickLabel',[])
        texstring1 = texlabel(sprintf('chi^2=%.3g', chi2(1,i)));
        text(205,2.5,texstring1,'EdgeColor',[1 0 0], 'LineWidth', 1.5);
        texstring2 = texlabel(sprintf('chi^2=%.3g', chi2(2,i)));
        text(205,0,texstring2,'EdgeColor',[0 0.5 0], 'LineWidth', 1.5);
        texstring3 = texlabel(sprintf('chi^2=%.3g', chi2(3,i)));
        text(205,-2.5,texstring3,'EdgeColor',[0 0.85 1], 'LineWidth', 1.5);
        %xlabel('Multipole'); 
        %ylabel('# sigma');

    elseif i == 6
        subplot(6,3,[12,15])
        % Uncomment next 2 lines if you want to plot smooth exp value lines for both models
        errorbar(l, real_i, sig,'.','LineWidth', 1.5, 'MarkerSize', 15);
        hold all;
        plot(exp1(:,i), 'r','LineWidth', 1.5);
        hold all;
        plot(exp2(:,i), 'Color', [0 0.5 0],'LineWidth', 1.5);
        hold all;
        plot(exp3(:,i), 'Color', [0 0.85 1],'LineWidth', 1.5);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([-0.1 0.15])
        xlim([0 200])
        %xlabel('Multipole'); 
        %ylabel('l(l+1)C_l^{BB}/2\pi [\muK^2]');
        title(strcat(band_names(i),', BB'));
        
        subplot(6,3,18)
        plot(l, bpdev1_sig, 'r','LineWidth', 1.5);
        hold all;
        plot(l, bpdev2_sig, 'Color', [0 0.5 0],'LineWidth', 1.5);
        hold all;
        plot(l, bpdev3_sig, 'Color', [0 0.85 1],'LineWidth', 1.5);
        hold all;
        plot([0 200], [0 0],'k--')
        ylim([-3 3])
        xlim([0 200])
        set(gca,'XTickLabel',[])
        xlabel('Multipole');
        texstring1 = texlabel(sprintf('chi^2=%.3g', chi2(1,i)));
        text(205,2.5,texstring1,'EdgeColor',[1 0 0], 'LineWidth', 1.5);
        texstring2 = texlabel(sprintf('chi^2=%.3g', chi2(2,i)));
        text(205,0,texstring2,'EdgeColor',[0 0.5 0], 'LineWidth', 1.5);
        texstring3 = texlabel(sprintf('chi^2=%.3g', chi2(3,i)));
        text(205,-2.5,texstring3,'EdgeColor',[0 0.85 1], 'LineWidth', 1.5);
        %ylabel('# sigma');
        
    end  
end

