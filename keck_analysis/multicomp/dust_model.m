function [scale_dust] = dust_model(nu, param, likeopt, use_greybody)
% Function containing the dust model.   
% [Input]
%   nu           The frequency of the desired experiment in GHz
%   param        Multi-component model parameter vector, defined in model_rms
%   likeopt      Likeopt data structure
%   use_greybody If 1, model the dust as a greybody. Otherwise, use a simple
%                power law (old model).
% [Output]
%   scale_dust   The appropriate scale factor to convert any dust model 
%                defined at the frequency likeopt.mod.fg_freq to the 
%                frequency nu.
% KDA 2014-09-25

if nargin < 4
  use_greybody = 1; % use greybody model by default
end

if use_greybody
  % Fundamental constants and Planck's law
  h = 6.63e-34; % J-s (Planck's constant)
  k = 1.38e-23; % J/K (Boltzmann's constant)
  Ipl = @(v, T) v^3/(exp(h*v*10^9/(k*T))-1); % normalization not important here
  
  % Currently, the scale factor is computed using the ratio of the intensities 
  % at the centers of the relevant frequency bands. This will eventually
  % be replaced with proper experiment bandpasses.
  % A single receiver with throughput A-Omega = lambda^2 is assumed.
  scale_dust = (nu / likeopt.mod.fg_freq)^(param(7)-2) * ... 
      Ipl(nu, param(12)) / Ipl(likeopt.mod.fg_freq, param(12)) * ...
      antenna_to_Tcmb(likeopt.mod.fg_freq) / antenna_to_Tcmb(nu);
  
else
  % Use the old power law model
  scale_dust = antenna_to_Tcmb(likeopt.mod.fg_freq) * ...
      (nu / likeopt.mod.fg_freq)^param(7) / ...
      antenna_to_Tcmb(nu);
end

end