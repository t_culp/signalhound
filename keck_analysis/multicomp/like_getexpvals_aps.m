function expv = like_getexpvals_aps(param, likeopt, return_model, spec)
% lik = like_getexpvals_color(likeopt,lik)
%
% Take model parameters and likeopt struct, evaluates bandpower
% expectation values of the model for all specified fields,
% experiments, and ell bins.
%
% 2014-06-24 KDA  Modified to add color constraint option.
%
% [Input]
%   param         Multi-component model parameters:
%     param(1)  = r, tensor-to-scalar ratio
%     param(2)  = A_L, lensing amplitude (1 = standard LCDM lensing)
%     param(3)  = synchrotron power spectrum amplitude, in
%                 uK_{CMB}^2, at nu = 150 GHz and ell = 100
%     param(4)  = dust power spectrum amplitude, in uK_{CMB}^2, at
%                 nu = 150 GHz and ell = 100
%     param(5)  = beta_sync, synchrotron frequency spectral index
%     param(6)  = gamma_sync, synchrotron spatial spectral index
%     param(7)  = beta_dust, dust frequency spectral index
%     param(8)  = gamma_dust, dust spatial spectral index
%     param(9)  = E/B ratio for synchrotron. 1 means that synchrotron 
%                 contributes equal amounts of E and B power; 2 means 
%                 that synchrotron contributes twice as much E as B; etc.
%     param(10) = E/B ratio for dust. 1 means that dust contributes equal 
%                 amounts of E and B power; 2 means that dust
%                 contributes twice as much E as B; etc.
%     param(11) = dust/sync correlation parameter
%     param(12) = T_d, dust temperature (19.6 K for PIP97 model)
%
%                 OR, for color constraint:
%     param(1)  = beta, effective spectral index
%     param(2)  = A_L, lensing amplitude
%     param(3)  = E/B ratio (default = 1)
%
%   likeopt       likeopt data structure
%   return_model  -- If this argument is set to one, then the function will 
%                 return full delta-ell=1 spectra for the model, rather 
%                 than bandpower expectation values. 
%                 -- If this argument is set to two, then the function will
%                 return full delta-ell=1 spectra for each type of signal
%                 that is going into the model: cmb, sync, dust, corr
%                 dust/sync and total. 
%                 Default: return_model=0.
%   spec          150 GHz bandpowers. Default: B2 BB for ell bins 2:6.
%
% == Relevant likeopt fields ==
% likeopt.expt    Array containing names and observing frequencies for each
%                 experiment included in the analysis.
% likeopt.fields  Cell array listing the CMB fields used for the
%                 analysis. Options are {'T', 'E', 'B'}, but only 'E' and 
%                 'B' are implemented in this model.
% likeopt.bpwf    Bandpower window functions for all experiments and
%                 experiment cross-spectra. Should be an array with
%                 length x*(x+1)/2, where x = (# of experiments),
%                 following H-L ordering.
% likeopt.mod     likeopt field containing CMB models from CAMB. 
%                 See like_read_theory.m
%
% [Output]
%   expv     Array of model bandpower expectation values with size (N,M,S),
%            where N is the number of ell bins, M = x*(x+1)/2
%            with x = (# of experiments) * (# of CMB fields), and S is the
%            number of signal components. 
%            Ordering of the second dimension follows the "H-L order", 
%            which is kind of complicated.
%            If the return_model argument (see above) is set, then the 
%            returned spectra are calculated for delta-ell=1, not bandpowers.

% Optional argument defaults.
if nargin < 3
  return_model = 0;
end

if nargin < 4
  spec = likeopt.real(:,1); %BICEP2 BB autospectrum.
end

% Get number of ell bins, CMB fields, and experiments.
n_ell = numel(likeopt.l);
n_field = numel(unique(likeopt.fields));
n_expt = numel(likeopt.expt);

lfine = likeopt.mod.l; % ell values at fine (\Delta\ell=1) resolution
order = expt_field_order(n_expt, n_field); % H-L vecp ordering

% Theory spectra (for CMB and foregrounds) are not typically calculated
% for the same range of ell as the bandpower window functions. Calculate
% the range of indices to use for calculating expectation values.
%
% Work out this indexing once and store the results in likeopt, so
% that we don't have to repeat it on future calls to getexpvals.
if ~isfield(likeopt.mod, 'itheory')
  % Find the largest value of lmin between the theory spectra and
  % all bpwf. Find the smallest value of lmax.
  % Assumes that theory spectra and bpwf are all specified at
  % delta-ell = 1 resolution.
  lmin = lfine(1);
  lmax = lfine(end);
  for i=1:length(likeopt.bpwf)
    lmin = max(lmin, likeopt.bpwf(i).l(1));
    lmax = min(lmax, likeopt.bpwf(i).l(end));
  end
  % itheory gives the range of indices to use in the theory model.
  likeopt.mod.itheory = [find(lfine == lmin):find(lfine == lmax)];
  % ibpwf gives the range of indices to use in the bpwf.
  for i=1:length(likeopt.bpwf)
    likeopt.bpwf(i).ibpwf = [find(likeopt.bpwf(i).l == lmin): ...
                        find(likeopt.bpwf(i).l == lmax)];
  end
end

% Default behavior: multi-component model
if length(param) == 12 
  % Calculate ell template shapes for foregrounds.
  % Calculated at 150 GHz in units of antenna temperature. Does not
  % include scaling factor to other frequencies.
  template_sync = param(3) * (lfine / likeopt.mod.fg_pivot).^param(6);
  template_dust = param(4) * (lfine / likeopt.mod.fg_pivot).^param(8);

  % Calculate scaling to T_CMB for sync and dust at each
  % frequency. This is the scaling to apply to *maps*. For bandpower
  % expectation values, you need two powers of this scaling.
  % Note that the templates are calculated in *thermodynamic temperature*
  % at 150 GHz, so you have to convert to antenna temperature, then
  % apply spectral index scaling, then convert back to thermodynamic.
  for i=1:numel(likeopt.expt)
    scale_sync(i) = (likeopt.expt(i).freq / likeopt.mod.fg_freq)^param(5) * ...
        antenna_to_Tcmb(likeopt.mod.fg_freq) / ...
        antenna_to_Tcmb(likeopt.expt(i).freq);
    scale_dust(i) = (likeopt.expt(i).freq / likeopt.mod.fg_freq)^param(7) * ...
        antenna_to_Tcmb(likeopt.mod.fg_freq) / ...
        antenna_to_Tcmb(likeopt.expt(i).freq);
  end

  % Now loop over all pairwise combinations of (expt, field) using
  % H-L vecp ordering.
  for s=order
    % Get bandpower window functions for this particular
    % combination of experiments. Bandpower window functions are
    % stored in likeopt with H-L type ordering.
    bpwfindex = s.e1;
    for k=1:(s.e2-s.e1)
      bpwfindex = bpwfindex + n_expt + 1 - k;
    end
    bpwf = likeopt.bpwf(bpwfindex);

    % Bandpower expectation values depend strongly on what type of
    % spectra this is. Only EE and BB are implemented! We expect EB
    % and TB to be zero (up to weird foregrounds). TT and TE will
    % be non-zero, but I'm not dealing with them.
    if (likeopt.fields{s.f1} == 'E') && (likeopt.fields{s.f2} == 'E')
      %% This is EE.
      % Foregrounds include an extra factor of param(9) or param(10).
      % 1. CMB signals
      model(:,s.i,1) = likeopt.mod.lcdm(:,2) + ...
              param(2) * likeopt.mod.lensing(:,2) + ...
              param(1) / likeopt.mod.rfid * likeopt.mod.tensor(:,2);
      % 2. Synchrotron
      % Apply one frequency scaling factor per experiment (frequency
      % scaling factors are calculated for units of temperature, not
      % temperature^2).
      
      % model = model + template_sync * param(9) * ...
      %        scale_sync(s.e1) * scale_sync(s.e2);
     
      model(:,s.i,2) = template_sync * param(9) * ...
              scale_sync(s.e1) * scale_sync(s.e2);
      % 3. Dust
      % Apply one frequency scaling factor per experiment (frequency 
      % scaling factors are calculated for units of temperature, not
      % temperature^2).
      model(:,s.i,3) = template_dust * param(10) * ...
              scale_dust(s.e1) * scale_dust(s.e2);
      % 4. Correlated foregrounds (synchrotron x dust).
      model(:,s.i,4) = sqrt(param(9) * param(10)) * param(11) * ...
            (scale_sync(s.e1) * scale_dust(s.e2) + ...
             scale_sync(s.e2) * scale_dust(s.e1)) * ...
            real(sqrt(template_sync .* template_dust));
    
      if return_model == 1 
        % Return the unbinned model.
        expv(:,s.i) = sum(model(:,s.i,:), 3);
      elseif return_model == 2
        % Calculate bandpower expectation values for each signal component
        % separately
        expv(:,s.i,:) = apply_bpwf(model(likeopt.mod.itheory,s.i,:), ...
                               bpwf.Cs_l(bpwf.ibpwf,likeopt.l,3), 1);
      elseif return_model == 3
        % Return the unbinned model for each signal component separately
        expv(:,s.i,:) = model(:,s.i,:);
      else
        % Calculate bandpower expectation values.
        expv(:,s.i) = apply_bpwf(sum(model(likeopt.mod.itheory,s.i,:),3), ...
                               bpwf.Cs_l(bpwf.ibpwf,likeopt.l,3), 0);
      end
    elseif (likeopt.fields{s.f1} == 'B') && (likeopt.fields{s.f2} == 'B')
      %% This is BB.
      % 1. CMB signals
      model(:,s.i,1) = likeopt.mod.lcdm(:,3) + ...
            param(2) * likeopt.mod.lensing(:,3) + ...
            param(1) / likeopt.mod.rfid * likeopt.mod.tensor(:,3);
      % 2. Synchrotron
      % Apply one frequency scaling factor per experiment (frequency
      % scaling factors are calculated for units of temperature, not
      % temperature^2).
      model(:,s.i,2) = template_sync * scale_sync(s.e1) * scale_sync(s.e2);
      % 3. Dust
      % Apply one frequency scaling factor per experiment (frequency 
      % scaling factors are calculated for units of temperature, not
      % temperature^2).
      model(:,s.i,3) = template_dust * scale_dust(s.e1) * scale_dust(s.e2);
      % 4. Correlated foregrounds (synchrotron x dust)
      model(:,s.i,4) = param(11) * (scale_sync(s.e1) * scale_dust(s.e2) + ...
                                 scale_sync(s.e2) * scale_dust(s.e1)) * ...
            real(sqrt(template_sync .* template_dust));

%       if return_model == 1
%         % Return the unbinned model.
%         expv(:,s.i) = model;
%       elseif return_model == 2
%         
%         expv_prep{s.i} = [apply_bpwf(model_cmb(likeopt.mod.itheory), ...
%                                bpwf.Cs_l(bpwf.ibpwf,likeopt.l,4)), ... 
%                           apply_bpwf(model_sync(likeopt.mod.itheory), ...
%                                bpwf.Cs_l(bpwf.ibpwf,likeopt.l,4)), ...
%                           apply_bpwf(model_dust(likeopt.mod.itheory), ...
%                                bpwf.Cs_l(bpwf.ibpwf,likeopt.l,4)), ...
%                           apply_bpwf(model_corr(likeopt.mod.itheory), ...
%                                bpwf.Cs_l(bpwf.ibpwf,likeopt.l,4)), ...
%                           apply_bpwf(model(likeopt.mod.itheory), ...
%                                bpwf.Cs_l(bpwf.ibpwf,likeopt.l,4))];
%         expv = cell2mat(expv_prep);
%       elseif return_model == 3
%           expv_prep{s.i} = [model_cmb, model_sync, model_dust, model_corr, model];
%           expv = cell2mat(expv_prep);
%       else
%         % Calculate bandpower expectation values.
%         expv(:,s.i) = apply_bpwf(model(likeopt.mod.itheory), ...
%                                bpwf.Cs_l(bpwf.ibpwf,likeopt.l,4));
%       end  
    
    if return_model == 1 
        % Return the unbinned model.
        expv(:,s.i) = sum(model(:,s.i,:), 3);
      elseif return_model == 2
        % Calculate bandpower expectation values for each signal component
        % separately
        expv(:,s.i,:) = apply_bpwf(model(likeopt.mod.itheory,s.i,:), ...
                               bpwf.Cs_l(bpwf.ibpwf,likeopt.l,4), 1);
      elseif return_model == 3
        % Return the unbinned model for each signal component separately
        expv(:,s.i,:) = model(:,s.i,:);
      else
        % Calculate bandpower expectation values.
        expv(:,s.i) = apply_bpwf(sum(model(likeopt.mod.itheory,s.i,:),3), ...
                               bpwf.Cs_l(bpwf.ibpwf,likeopt.l,4), 0);
    end
    
    else
      % For anything else, return zero.
      if return_model
        expv(:,s.i) = zeros(size(lfine));
      else
        expv(:,s.i) = zeros(n_ell, 1);
      end
    end   
  end
  
% Do color constraint instead
else    
  % Calculate scaling to T_CMB at each frequency.
  % This is the scaling to apply to *maps*. For bandpower
  % expectation values, you need two powers of this scaling.
  for i=1:numel(likeopt.expt) 
    scale(i) = (likeopt.expt(i).freq / ...
	likeopt.mod.fg_freq)^param(1) * ...
	antenna_to_Tcmb(likeopt.mod.fg_freq) / ...
	antenna_to_Tcmb(likeopt.expt(i).freq);
  end
  
  % Now loop over all pairwise combinations of (expt, field) using
  % H-L vecp ordering.
  for s=order
    % Get bandpower window functions for this particular
    % combination of experiments. Bandpower window functions are
    % stored in likeopt with H-L type ordering.
    bpwfindex = s.e1;
    for k=1:(s.e2-s.e1)
      bpwfindex = bpwfindex + n_expt + 1 - k;
    end
    bpwf = likeopt.bpwf(bpwfindex);

    % Bandpower expectation values depend strongly on what type of
    % spectra this is. Only EE and BB are implemented! We expect EB
    % and TB to be zero (up to weird foregrounds). TT and TE will
    % be non-zero, but I'm not dealing with them.
    if (likeopt.fields{s.f1} == 'E') && (likeopt.fields{s.f2} == 'E')
      %% This is EE.
      % Spectrum includes an extra factor of param(3) 
      % 1. LCDM + Lensing
      model = likeopt.mod.lcdm(:,2) + ...
             param(2) * likeopt.mod.lensing(:,2);
	 
      % Calculate bandpower expectation values.
      mod_expv(:,s.i) = apply_bpwf(model(likeopt.mod.itheory), ...
                              bpwf.Cs_l(bpwf.ibpwf,likeopt.l,3));	
      expv(:,s.i) = spec * param(3) * scale(s.e1) * scale(s.e2) + mod_expv(:,s.i);
      
    elseif (likeopt.fields{s.f1} == 'B') && (likeopt.fields{s.f2} == 'B')
      %% This is BB.
      % 1. LCDM + Lensing
      model = likeopt.mod.lcdm(:,3) + ...
            param(2) * likeopt.mod.lensing(:,3);
      
      % Calculate bandpower expectation values.      
      mod_expv(:,s.i) = apply_bpwf(model(likeopt.mod.itheory), ...
                               bpwf.Cs_l(bpwf.ibpwf,likeopt.l,4));      
      expv(:,s.i) = spec * scale(s.e1) * scale(s.e2) + mod_expv(:,s.i);    
    else
      % For anything else, return zero.
      if return_model
        expv(:,s.i) = zeros(size(lfine));
      else
        expv(:,s.i) = zeros(n_ell, 1);
      end
    end   
  end
end

  
    

%% SUBFUNCTIONS %%

%% antenna_to_Tcmb
%
% factor = antenna_to_Tcmb(nu)
% 
% Calculates conversion from antenna temperature to thermodynamic
% temperature for the specified frequency (in GHz). Returned value
% has units of [antenna / thermodynamic], so if you have a value in
% antenna temperature units, divide by this number to obtain
% thermodynamic units.
function factor = antenna_to_Tcmb(nu)

% Physical constants.
h = 6.626e-34; % J*s
kB = 1.381e-23; % J/K
Tcmb = 2.72548; % K

% Calculate conversion factor.
x = h * nu * 1e9 / (kB * Tcmb);
factor = x^2 * exp(x) / (exp(x) - 1)^2;

%% apply_bpwf
%
% expv = apply_bpwf(model, bpwf, cpts)
%
% Calculates expectation values for the given model and bandpower
% window functions. 
% * model is a matrix of size [n_ell, 1, n_cpts], where n_cpts is equal
%   to 4 if we want to calculate expv for each component independelty, and
%   equal to 1 if we just want to calculate one set of expv for the total
%   signal.
% * bpwf is an array with size [n_ell, n_band], where n_band is the
%   number of bandpowers to calculate.
% * cpts is 1 if we want to do the calculation for all the signal
%   components separately, or 0 if we want to just do the total signal.
%
% Returns a matrix of [n_band, n_cpts] expectation values.
function expv = apply_bpwf(model, bpwf, cpts)

nband = size(bpwf, 2);
if cpts == 0
    expv = sum(repmat(model, 1, nband) .* bpwf, 1);
    expv = expv';
elseif cpts == 1
    n_cpts = size(model, 3);
    for i = 1:n_cpts
        expv(i,:) = sum(repmat(squeeze(model(:,:,i)), 1, nband) .* bpwf, 1); 
    end
    expv = expv';
end

%%%% OLD STUFF BELOW HERE %%%%

%% freq dependence of 4 components (in uK_cmb)
% TOD: Implement correct shape from sims for tensors, lensing
% cmbt_fshape = ones(1,n_bands); % tensors (sqrt of exp bps, depends on r, sims)
% cmbl_fshape = ones(1,n_bands); % lensing (sqrt of exp bps, depends on A_l, sims)
% dust_fshape = (likeopt.bands.freq./150).^likeopt.mod.beta_d;
% sync_fshape = (likeopt.bands.freq./150).^likeopt.mod.beta_s;


% spatial dependence of 4 components. Tensors and
% lensing from sims, dust and sync power law indices set as input
% params p_d, p_s
% TODO: Implement correct spatial dependence
% cmbt_pshape = ones(1,n_ell);
% dust_pshape = ones(1,n_ell); 
% sync_pshape = ones(1,n_ell);
% cmbl_pshape = ones(1,n_ell);


% Compute matrix form of auto and cross spectra for each ell bin.
% ASSUMES ONLY BB FOR NOW.
% TODO: Allow for multiple spectra for each band
% TODO: Implement possibility for sync/dust correlation.
% TODO: Implement spatial dependence
% mod=zeros(n_ell,n_bands,n_bands)
% for i=1:n_ell
%   for j=1:n_bands
%     for k=1:n_bands
%       mod(j,k,i) = cmbt_fshape(j)*cmbt_fshape(k) + ...
% 	  cmbl_fshape(j)*cmbl_fshape(k) + ...
% 	  likeopt.mod.A_d*dust_fshape(j)*dust_fshape(k) + ...
% 	  likeopt.mod.A_s*sync_fshape(j)*sync_fshape(k);
%     end
%   end
% end

% TODO: Make sure order of spectra is consistent across likelihood code!

% For models WITH a CAMB C_l file
% For each band, load up each model and calc expvals
% for i = 1:n_bands
%   load(sprintf('%s%s',likeopt.bands.filebase,likeopt.bands.files{i}));
%   tmp = [];
%   for j = 1:n_mods
%     inpmod = load_cmbfast(likeopt.mod.theory{j});
%     
%     % This uses the helper function here, get_expvals
%     %tmp = setfield(tmp,likeopt.mod.list{j},get_expvals(inpmod,expt.bpwf));
%     
%     % This makes a dummy struct "s" so that calc_expvals can be used
%     % Will fail if we want spectra 7:9... 
%     s = struct; % length(s) = 1
%     s.real = []; % needed to get nspec
%     s = calc_expvals(s,inpmod,expt.bpwf);
%     tmp = setfield(tmp,likeopt.mod.list{j},s.expv);
%     
%   end
%   mod(i).in = tmp;
% end

% Construct a vector for each model
% length = n_bands * n_spec * n_ell

% CHANGE THIS TO WORK FOR ARBITRARY FIELDS LIKE ABOVE
% for a = 1:n_mods
%   for i = 1:n_ell
%     for j = 1:n_spec
%       for k = 1:n_bands
% 	ind = (n_bands*n_spec*(i-1))+(n_bands*(j-1))+(k-1)+1;
% 	switch likeopt.mod.list{a}
% 	  case 'r'
% 	    switch likeopt.spec{j}
% 	      case 'E'
% 		vec.r(ind) = mod(k).in.r(likeopt.l(i),3);
% 	      case 'B'
% 		vec.r(ind) = mod(k).in.r(likeopt.l(i),4);
% 	    end
% 	  case 'A_l'
% 	    switch likeopt.spec{j}
% 	      case 'E'
% 		vec.A_l(ind) = mod(k).in.A_l(likeopt.l(i),3);
% 	      case 'B'
% 		vec.A_l(ind) = mod(k).in.A_l(likeopt.l(i),4);
% 	    end
% 	end
%       end
%     end
%   end
% end
% 
% lik.mod = mod;
% lik.modvec = vec;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% NOT USED ANYMORE 2014-05-30
% Use input theory Cls and bpwf to get expvals...almost identical to
% calc_expvals, but no "s" structure
function expv = get_expvals(inpmod,bpwf)

% Input model: TT, TE, EE, BB, TB, EB, ET, BT, BE
% Bpwf:        TT, TP, EE>EE, BB>BB, EE>BB, BB>EE, (PT)
nbin = size(bpwf.Cs_l,2);

% Don't assume inpmod and bpwf start from same ell, shift as needed
% Inpmod starts at ell = 0, bpwf maybe higher
nl = size(bpwf.Cs_l,1);
indx = 1:nl;
indx = indx + find(bpwf.l(1) == inpmod.l) - 1;

% TT
x = bpwf.Cs_l(:,:,1).*repmat(inpmod.Cs_l(indx,1),[1,nbin]);
expv(:,1) = sum(x)';

% TE
x = bpwf.Cs_l(:,:,2).*repmat(inpmod.Cs_l(indx,2),[1,nbin]);
expv(:,2) = sum(x)';

% EE
x1 = bpwf.Cs_l(:,:,3).*repmat(inpmod.Cs_l(indx,3),[1,nbin]); % EE>EE * EE
x2 = bpwf.Cs_l(:,:,6).*repmat(inpmod.Cs_l(indx,4),[1,nbin]); % BB>EE * BB
expv(:,3) = sum([x1;x2])';

% BB
x1 = bpwf.Cs_l(:,:,4).*repmat(inpmod.Cs_l(indx,4),[1,nbin]); % BB>BB * BB
x2 = bpwf.Cs_l(:,:,5).*repmat(inpmod.Cs_l(indx,3),[1,nbin]); % EE>BB * EE
expv(:,4) = sum([x1;x2])';

% TB bpwf not explicitly calculated - use TE
x = bpwf.Cs_l(:,:,2).*repmat(inpmod.Cs_l(indx,5),[1,nbin]);
expv(:,5) = sum(x)';

% EB bwpf not explicitly calculated - use EE
x = bpwf.Cs_l(:,:,3).*repmat(inpmod.Cs_l(indx,6),[1,nbin]);
expv(:,6) = sum(x)';

% Cross stuff
expv(:,7) = expv(:,2); % TE, ET
expv(:,8) = expv(:,5); % TB, BT
expv(:,9) = expv(:,6); % BE, EB

return


% Load the model bandpowers
% We have n_models and n_bands
% HOLY SHIT SO JANKY
%
if(0) % fuck this i'm going with bpwf
for i = 1:n_bands
  switch likeopt.bands.names{i}
    case 'B1_comb'
      for j = 1:n_mods
	switch likeopt.mod.list{j}
	  case 'r'
	    filein = sprintf('%s%s',likeopt.bands.filebase,likeopt.bands.exp_tens{i});
	    mod.in(i).r = importdata(filein,' ',12);
	  case 'A_l'
	    filein = sprintf('%s%s',likeopt.bands.filebase,likeopt.bands.exp_lens{i});
	    mod.in(i).A_l = importdata(filein,' ',12);
	end
      end
    case 'B2_150'
      for j = 1:n_mods
	switch likeopt.mod.list{j}
	  case 'r'
	    filein = sprintf('%s%s',likeopt.bands.filebase,likeopt.bands.exp_tens{i});
	    mod.in(i).r = importdata(filein);
	  case 'A_l'
	    filein = sprintf('%s%s',likeopt.bands.filebase,likeopt.bands.exp_lens{i});
	    mod.in(i).A_l = importdata(filein);
	end
      end
  end
end

% Construct a vector for each model
% length = n_bands * n_spec * n_ell

for a = 1:n_mods
  for i = 1:n_ell
    for j = 1:n_spec
      for k = 1:n_bands
	ind = (n_bands*n_spec*(i-1))+(n_bands*(j-1))+(k-1)+1;
	switch likeopt.mod.list{a}
	  case 'r'
	    switch likeopt.spec{j}
	      case 'E'
		mod.exp.r(ind) = mod.in(k).r.data(i,6);
	      case 'B'
		mod.exp.r(ind) = mod.in(k).r.data(i,7);
	    end
	  case 'A_l'
	    switch likeopt.spec{j}
	      case 'E'
		mod.exp.A_l(ind) = mod.in(k).A_l.data(i,6);
	      case 'B'
		mod.exp.A_l(ind) = mod.in(k).A_l.data(i,7);
	    end
	end
      end
    end
  end
end

end % if(0)


