function likedata = like_read_simset(aps_file, likedata)
% likedata = like_read_simset(aps_file, likedata)
%
% Read data from simset aps file and calculate bandpower covariance
% matrix components for use in likelihood analysis.
%
% This function should be called after like_read_final, with an aps
% file that corresponds to the same data reduction as this finalfile.
% 
% [Input]
%   aps_file  Path to pipeline aps file containing all auto and
%             cross-spectra for signal and noise sims to be used
%             for calculating bandpower covariance matrix components.
%   likedata  Data structure for multicomponent likelihood analysis. 
%             This structure should be created using like_read_final 
%             before being passed to like_read_simset.
%             The likedata.opt field includes the following entries
%             that control the behavior of this function:
%     opt.l         Which ell bins to use. This should already be
%                   set for like_read_final and could cause problems if 
%                   you change it around.
%     opt.fields    Which CMB fields to use. This should already be
%                   set for like_read_final and could cause problems if 
%                   you change it around.
%     opt.fidmodel  Structure that specifies the particular combination of
%                   signal sims to use for calculating bandpower 
%                   covariances. Fields of this structure correspond to four
%                   signal sim types used in the pipeline: 'unlens' (type 2), 
%                   'dust' (type 3), 'tensor' (type 4), and 'lcdm' (type 5). 
%                   Values for each field correspond to a multiplier of the 
%                   power in that type of signal sim. So for example, if you 
%                   want to use a lensed-LCDM+r=0.2 model for your signal sims,
%                   set fidmodel.tensor=2 (because type 4 sims correspond to 
%                   r=0.1) and fidmodel.lcdm=1. Sim types that are set to zero 
%                   will be ignored.
%                   Default fidmodel is lensed-LCDM only (i.e. all fields
%                   zero except fidmodel.lcdm=1).
%     opt.mapdef    Array of structures, with one element for each experiment
%                   listed in opt.expt. For each experiment, the structure 
%                   should contain the same signal fields as included in the 
%                   fidmodel structure, containing the map index in the aps 
%                   file for the labeled sim type. In addition to labeling the 
%                   signal sims, every element of the mapdef array must have a 
%                   'noise' field, specifying the map index in the aps file for
%                   the noise sims of that experiment.
%                   If mapdef is not specified, the default assumption is that 
%                   this apsfile is a "45666" file of the type used in BKP 
%                   joint analysis, with common tensor and LCDM sims as maps 1 
%                   and 2, followed by all noise sims.
%     opt.dropsim   Set to an integer or array of integers to drop that sim 
%                   realization from the covariance calculation. Values that 
%                   don't correspond to sim realizations (<1 or >nsim) will
%                   be ignored.
%     opt.Planck_datasplit
%                   If this field is set to one, then Planck full map 
%                   auto-spectra will be replaced by the corresponding split 
%                   map cross-spectra and all extraneous terms involving 
%                   split maps will be discarded before calculating 
%                   covariances. This operation is equivalent to calling
%                   like_Planck_datasplit on the likeopt after the fact, but 
%                   it can be much faster to do it inside of like_read_simset.
%                   If Planck_datasplit = 2, then we additionally replace the 
%                   N_l (noise bias) term with some multiple of the
%                   average of the noise bias for data split autos
%                   (see next option). This allows you to use the 
%                   likeopt structure with H-L likelihood.
%     opt.Planck_noisebias
%                   This field controls the treatment of Planck noise bias 
%                   terms when likeopt.Planck_datasplit=2. See 2015-01-30 
%                   posting for details; this field corresponds to the X 
%                   parameter defined in Section 2. Default value is 2, but
%                   we might want to switch default to 1.
%     opt.Planck143_EBfull
%                   If this field is set to one, then we use the full map
%                   bandpowers for Planck 143 GHz EB. This is a blatant 
%                   kludge to get around some badly behaving data in the 
%                   Planck year-split.
%
% [Output]
%   likedata  Input likedata structure extended by the addition of a bpcov 
%             field containing covariance matrix components and updates
%             to likedata.opt.
%     .bpcov    Structure containing covariance matrix components
%       .sig      Covariance matrix for signal x signal bandpowers
%       .noi      Covariance matrix for noise x noise bandpowers
%       .sn1      Covariance matrix for signal x noise bandpowers
%       .sn2      Covariance matrix between signal x noise and noise x 
%                 signal bandpowers
%       .sn3      Covariance matrix between noise x signal and signal x 
%                 noise bandpowers
%       .sn4      Covariance matrix for noise x signal bandpowers
%       .C_fl     Ensemble average of signal sims, which should match up to 
%                 the requested fidmodel.
%     .opt      Two options for bandpower covariance matrix treatment are not 
%               used by this function, but will have default values assigned 
%               if they are not previously specified:
%       .mask_bpcm  Set to 1 (default) to apply masks to the covariance matrix 
%                   components. The masks explicitly zero terms that have zero 
%                   expectation value (but which have non-zero sample variance 
%                   as measured from finite number of sims).
%       .offdiag    Specifies the maximum ell bin separation for non-zero 
%                   bandpower covariance. Covariances between bandpowers 
%                   separated by many ell bins are very difficult to measure 
%                   from modest number of sims. Defaults to 1, i.e. keep 
%                   covariance only between bandpowers that have same or 
%                   adjacent ell bins.

% Default opt parameters.
if ~isfield(likedata, 'opt')
  likedata.opt = [];
end
if ~isfield(likedata.opt, 'fidmodel')
  % Default model is just lensed-LCDM.
  likedata.opt.fidmodel.tensor = 0; % type 4 sim
  likedata.opt.fidmodel.lcdm = 1;   % type 5 sim
  likedata.opt.fidmodel.unlens = 0; % type 2 sim
  likedata.opt.fidmodel.dust = 0;   % type 3 sim
end
if ~isfield(likedata.opt, 'dropsim')
  % By default, calculate covariance matrix components using all
  % sims.
  likedata.opt.dropsim = [];
end
if ~isfield(likedata.opt, 'Planck_datasplit')
  % By default, don't try to replace Planck full map autos with data 
  % split crosses.
  likedata.opt.Planck_datasplit = 0;
end
if likedata.opt.Planck_datasplit > 1
  % This field is only used for Planck_datasplit=2.
  if ~isfield(likedata.opt, 'Planck_noisebias')
    % Default value X = 1.
    likedata.opt.Planck_noisebias = 1;
  end
end
if ~isfield(likedata.opt, 'Planck143_EBfull')
  % Use data split cross by default.
  likedata.opt.Planck143_EBfull = 0;
end
if ~isfield(likedata.opt, 'mask_bpcm')
  % This option is not used in like_read_simset, but assign default
  % value for later.
  % Default is to apply masks to bpcm components.
  likedata.opt.mask_bpcm = 1;
end
if ~isfield(likedata.opt, 'offdiag')
  % This option is not used in like_read_simset, but assign default
  % value for later.
  % Default is to only keep covariance terms separated by at most
  % one ell bin.
  likedata.opt.offdiag = 1;
end

% If mapdef argument is not supplied, then assume that the simset
% file is a 456 (or 45666) file, like we used for BKP analysis. In
% that case, the first map is a tensor-only signal sim, the
% second map is an LCDM signal sim, and all subsequent maps are
% noise sims for the various experiments. All experiments use the
% same signal sims.
if ~isfield(likedata.opt, 'mapdef')
  for ii=1:numel(likedata.opt.expt)
    likedata.opt.mapdef(ii).tensor = 1;
    likedata.opt.mapdef(ii).lcdm = 2;
    likedata.opt.mapdef(ii).unlens = []; % 456 doesn't include unlensed-LCDM
    likedata.opt.mapdef(ii).dust = [];   % 456 doesn't include dust signal
    likedata.opt.mapdef(ii).noise = 2 + ii;
  end
end

% For convenience / shorter lines of code
fidmodel = likedata.opt.fidmodel;
mapdef = likedata.opt.mapdef;

% Load simset file.
likedata.opt.apsfile = aps_file;
a = load(aps_file);
st = check_mapdef(a.aps, mapdef, fidmodel);
if st > 0 % ERROR condition
  return
end

% Get experiment names. These are taken from the final bandpowers
% file by like_read_final.
expt = likedata.opt.expt;
n_expt = numel(expt);
% Get CMB fields to use. Default to TEB.
if ~isfield(likedata.opt, 'fields')
  likedata.opt.fields = {'T', 'E', 'B'};
end
fields = likedata.opt.fields;
n_field = numel(fields);
% Get ell bins to use. Default to all.
if ~isfield(likedata.opt, 'l')
  likedata.opt.l = [1:numel(a.aps(1).l)];
end
n_ell = numel(likedata.opt.l);
l = likedata.opt.l; % Short variable name for convenience.
likedata.lval = a.aps(1).l(l);

% If likeopt.Planck_datasplit is set, figure out the data splits
% corresponding to each full map.
if likedata.opt.Planck_datasplit
  % Array to keep track of data split maps.
  is_split = zeros(n_expt, 1);
  % Check to see if each experiment has corresponding data splits.
  for ii=1:n_expt
    ih1 = find(strcmp([expt{ii} 'H1'], expt));
    ih2 = find(strcmp([expt{ii} 'H2'], expt));
    if ~isempty(ih1) && ~isempty(ih2)
      % Mark data split entries.
      is_split(ih1) = 1;
      is_split(ih2) = 1;
      % Record the indices of the two split maps corresponding to
      % this full map.
      splits{ii} = [ih1, ih2];
    else
      % This is not a full map with corresponding split maps.
      splits{ii} = [];
    end
  end
  % Get the number of non-data split experiments (including non-Planck 
  % experiments).
  n_expt_full = sum(~is_split);
else
  % Will be keeping all experiments.
  is_split = zeros(n_expt, 1);
  splits = cell(n_expt);
  n_expt_full = n_expt;
end

% Next, split up aps into signal x signal, noise x noise, signal x noise, 
% and noise x signal types, while switching to H-L vecp order.
% Apply suppression factor correction in this step.
% Store real bandpowers, noise (and leakage) debias, and bandpower
% window functions in likeopt.
order = expt_field_order(n_expt_full, n_field);
n_spec = numel(order);
for s=order
  % For convenience / shorter lines of code.
  % s.f1, s.f2 are numeric; f1, f2 are letters (i.e. 'E' or 'B')
  f1 = likedata.opt.fields{s.f1};
  f2 = likedata.opt.fields{s.f2};
  % Experiment auto vs experiment cross.
  iscross = (s.e1 ~= s.e2);
  % Check whether we are specifically opting out of the Planck
  % data-split behavior because this is P143 E x P143 B.
  P143EB = likedata.opt.Planck143_EBfull && ~iscross && ...
           strcmp(likedata.opt.expt{s.e1}, 'P143') && ...
           any('E' == [f1, f2]) && any('B' == [f1, f2]);
  % We replace this spectrum with the cross between two data splits
  % if *all* of the following conditions are met:
  %   1. likedata.opt.Planck_datasplit > 0
  %   2. Spectrum is an experiment auto
  %   3. We found data splits corresponding to this experiment
  %   4. This isn't P143 E x P143 B with the Planck143_EBfull option set
  do_Planck_datasplit = (likedata.opt.Planck_datasplit && ~iscross && ...
                         ~isempty(splits{s.e1}) && ~P143EB);
  if do_Planck_datasplit
    % Data split case.
    xind = likeopt_getxind(likedata.opt, splits{s.e1}(1), ...
                           splits{s.e1}(2), s.f1, s.f2);
    rwf = likedata.bpwf.rwf(:,xind);
    e1 = mapdef(splits{s.e1}(1));
    e2 = mapdef(splits{s.e1}(2));
  else
    % Normal case -- no data-split swapping.
    xind = likeopt_getxind(likedata.opt, s.e1, s.e2, s.f1, s.f2);
    rwf = likedata.bpwf.rwf(:,xind);
    e1 = mapdef(s.e1);
    e2 = mapdef(s.e2);
  end
  % Get SxS, NxN, SxN, and NxS spectra from aps file, combine over
  % signal types (if specified), and apply supfac correction.
  sig(s.i+[0:n_ell-1]*n_spec,:) = ...
      get_spec(a.aps, rwf, e1, f1, e2, f2, l, fidmodel, 1);
  noi(s.i+[0:n_ell-1]*n_spec,:) = ...
      get_spec(a.aps, rwf, e1, f1, e2, f2, l, fidmodel, 2);
  sn(s.i+[0:n_ell-1]*n_spec,:) = ...
      get_spec(a.aps, rwf, e1, f1, e2, f2, l, fidmodel, 3);
  ns(s.i+[0:n_ell-1]*n_spec,:) = ...
      get_spec(a.aps, rwf, e1, f1, e2, f2, l, fidmodel, 4);
  % For Planck_datasplit case, average these bandpowers with the
  % results we get by swapping e1 and e2 (this picks up the
  % alt-cross case). Only makes a difference for cross between two
  % different CMB fields (like EB).
  % Note that this is probably not necessary at all for sig,
  % because the two halves of the Planck datasplit use the same
  % signal simulations.
  if do_Planck_datasplit
    sig(s.i+[0:n_ell-1]*n_spec,:) = ...
        (sig(s.i+[0:n_ell-1]*n_spec,:) + ...
         get_spec(a.aps, rwf, e2, f1, e1, f2, l, fidmodel, 1)) / 2;
    noi(s.i+[0:n_ell-1]*n_spec,:) = ...
        (noi(s.i+[0:n_ell-1]*n_spec,:) + ...
         get_spec(a.aps, rwf, e2, f1, e1, f2, l, fidmodel, 2)) / 2;
    sn(s.i+[0:n_ell-1]*n_spec,:) = ...
        (sn(s.i+[0:n_ell-1]*n_spec,:) + ...
         get_spec(a.aps, rwf, e2, f1, e1, f2, l, fidmodel, 3)) / 2;
    ns(s.i+[0:n_ell-1]*n_spec,:) = ...
        (ns(s.i+[0:n_ell-1]*n_spec,:) + ...
         get_spec(a.aps, rwf, e2, f1, e1, f2, l, fidmodel, 4)) / 2;
  end

  % If replacing an experiment auto-spectrum with a data-split
  % cross, we need to also fix up the real bandpowers and noise biases.
  if do_Planck_datasplit
    % Index of the auto-spectrum to be replaced.
    xind1 = likeopt_getxind(likedata.opt, s.e1, s.e1, s.f1, s.f2);
    % Index of the cross-spectra to use for replacement.
    % If f1 == f2, then these two spectra will be the same.
    % If f1 ~= f2, then these are cross and alt-cross.
    xind2(1) = likeopt_getxind(likedata.opt, splits{s.e1}(1), ...
                               splits{s.e1}(2), s.f1, s.f2);
    xind2(2) = likeopt_getxind(likedata.opt, splits{s.e1}(2), ...
                               splits{s.e1}(1), s.f1, s.f2);
    % Replace real bandpowers.
    likedata.real(:,xind1) = mean(likedata.real(:,xind2), 2);
    % Replace window functions and rwf.
    lstep = [0:n_ell-1] * size(likedata.bpwf.rwf, 2);
    likedata.bpwf.Cs_l(:,xind1+lstep) = ...
        (likedata.bpwf.Cs_l(:,xind2(1)+lstep) + ...
         likedata.bpwf.Cs_l(:,xind2(2)+lstep)) / 2;
    likedata.bpwf.rwf(:,xind1) = mean(likedata.bpwf.rwf(:,xind2), 2);
    % If opt.Planck_datasplit=1, then we do the same replacement
    % for the noise bias term. Result is that this spectrum will
    % have very little noise bias (because it is a cross-spectrum)
    % and this will cause trouble for the H-L approximation.
    if likedata.opt.Planck_datasplit == 1
      likedata.N_l(:,xind1) = mean(likedata.N_l(:,xind2), 2);
    else
      % If opt.Planck_datasplit=2, we average the noise biases from
      % the auto-spectra of the data splits and then divide by factor
      % given in opt.Planck_noisebias (defaults to 1).
      xind3 = likeopt_getxind(likedata.opt, splits{s.e1}(1), ...
                              splits{s.e1}(1), s.f1, s.f2);
      xind4 = likeopt_getxind(likedata.opt, splits{s.e1}(2), ...
                              splits{s.e1}(2), s.f1, s.f2);
      likedata.N_l(:,xind1) = ...
          (likedata.N_l(:,xind3) + likedata.N_l(:,xind4)) / 2 / ...
          likedata.opt.Planck_noisebias;
    end
  end
end

% Now that data has been copied over, cut out the data-split
% entries. (Need to do this before adding the bpcov field, because
% those entries are already cut down to the correct length.)
if likedata.opt.Planck_datasplit
  likedata = likeopt_select(likedata, find(is_split == 0), [], []);
end

% Optionally drop particular realizations from covariance calculation.
if ~isempty(likedata.opt.dropsim)
  % Sim indices should be integer.
  likedata.opt.dropsim = round(likedata.opt.dropsim);
  % Drop duplicates.
  likedata.opt.dropsim = unique(likedata.opt.dropsim);
  % Get rid of values that are <1 or >nsim.
  nsim = size(sig, 2);
  in_range = (likedata.opt.dropsim >= 1) & (likedata.opt.dropsim <= nsim);
  likedata.opt.dropsim = likedata.opt.dropsim(in_range);
  % Drop sim realization(s).
  mask = setdiff([1:nsim], likedata.opt.dropsim);
  sig = sig(:,mask);
  noi = noi(:,mask);
  sn = sn(:,mask);
  ns = ns(:,mask);
end

% Now, calculate covariance sub-matrices for semi-analytic bpcm.
likedata.bpcov.sig = cov(sig');
likedata.bpcov.noi = cov(noi');
% Combined signal x noise and noise x signal gives all four signal-noise
% sub-matrices.
N = n_spec * n_ell;
sncov = cov([sn; ns]');
likedata.bpcov.sn1 = sncov([1:N], [1:N]);
likedata.bpcov.sn2 = sncov([1:N], [N+1:2*N]);
likedata.bpcov.sn3 = sncov([N+1:2*N], [1:N]);
likedata.bpcov.sn4 = sncov([N+1:2*N], [N+1:2*N]);
% Also store signal expectation values from the sims.
likedata.bpcov.C_fl = reshape(mean(sig, 2), n_spec, n_ell)';
% Generate masks for covariance matrix.
likedata.bpcov = mask_bpcm(likedata.bpcov, likedata.opt);

return % END of function like_read_simset


function status = check_mapdef(aps, mapdef, fidmodel)
% status = check_mapdef(aps, mapdef, fidmodel)
%
% Checks the mapdef structure to make sure that the aps structure
% has enough entries to account for all of the maps that are needed
% for the bandpower covariance matrix construction.
% 
% [Input]
%   aps       Pipeline aps structure
%   mapdef    Structure that provides the map indices in the aps
%             structure for each sim type for each experiment.
%   fidmodel  Structure specifying signal model to use when
%             calculating bandpower covariance matrix components.
%
% [Output]
%   status    0 if tests all pass, 1 otherwise

% Number of maps that have been crossed in this aps structure.
nmap = (-1 + sqrt(1 + 8 * numel(aps))) / 2;

% Check signal sim types.
simtype = fields(fidmodel);
for ii=1:numel(simtype)
  type = simtype{ii};
  % Only worry about this simtype if it is non-zero.
  if fidmodel.(type) > 0
    for ii=1:numel(mapdef)
      % Error if index of this map isn't defined.
      if ~isfield(mapdef(ii), type) || isempty(mapdef(ii).(type))
        disp(sprintf(['[like_read_simset] ERROR: mapdef(%i) has no ' ...
                      'information for sim type %s'], ii, type));
        status = 1;
        return
      else
        % Error if index of this map doesn't exist in aps structure.
        if (mapdef(ii).(type) <= 0) || (mapdef(ii).(type) > nmap)
          disp(sprintf(['[like_read_simset] ERROR: invalid map index for ' ...
                        'mapdef(%i).%s (=%i)'], ii, type, mapdef(ii).(type)));
          status = 1;
          return
        end
      end
    end
  end
end

% Check noise sims.
for ii=1:numel(mapdef)
  % Error if index of this map isn't defined.
  if ~isfield(mapdef(ii), 'noise') || isempty(mapdef(ii).noise)
    disp(sprintf(['[like_read_simset] ERROR: mapdef(%i) has no ' ...
                  'information for sim type noise'], ii));
    status = 1;
    return
  else
    % Error if index of this map doesn't exist in aps structure.
    if (mapdef(ii).noise <= 0) || (mapdef(ii).noise > nmap)
      disp(sprintf(['[like_read_simset] ERROR: invalid map index for ' ...
                    'mapdef(%i).noise (=%i)'], ii, mapdef(ii).noise));
      status = 1;
      return
    end
  end
end
                    
status = 0; % Zero indicates success.
return % END of function check_mapdef


function spectra = get_spec(aps, rwf, e1, f1, e2, f2, l, fidmodel, type)
% spectra = get_spec(aps, likedata, e1, f1, e2, f2, fidmodel, type)
%
% Extract the requested spectrum from aps data structure, with the
% following complications:
%   * Can be SxS, NxN, SxN, or NxS spectrum (controlled by type argument)
%   * Signal can be an arbitrary combination of tensor-only, LCDM,
%     and dust signal sims (so long as all are present in the aps file) 
%   * Applies suppression factor correction to aps
%
% [Input]
%   aps       Pipeline aps structure
%   rwf       Inverse suppression factor for this spectrum
%   e1        Mapdef structure for experiment 1
%   f1        CMB field 1, either 'T', 'E', or 'B'
%   e2        Mapdef structure for experiment 2
%   f2        CMB field 2, either 'T', 'E', or 'B'
%   l         Ell bins to select
%   fidmodel  Structure specifying multipliers for different signal
%             types. 
%   type      Specifies the type of signal/noise spectra:
%               1 = signal x signal
%               2 = noise x noise
%               3 = signal x noise
%               4 = noise x signal
%
% [Output]
%   spectra    Array of the requested spectra, with size [n_ell,n_sim]

% Signal sim types included in fiducial model.
simtype = fields(fidmodel);

% Determine which maps are crossed for this spectra. There is only
% one type of noise map, but a signal map could be a combination of
% tensor, lcdm, and dust signal sims, as specified by fidmodel.
% First map
if any(type == [1,3])
  % First element in the cross is signal.
  map1 = [];
  mult1 = [];
  % Loop over signal sim types.
  for ii=1:numel(simtype)
    if ~isempty(fidmodel.(simtype{ii})) && fidmodel.(simtype{ii}) > 0
      % Include this signal component.
      map1(end+1) = e1.(simtype{ii});
      mult1(end+1) = sqrt(fidmodel.(simtype{ii}));
    end
  end
else
  % First element in the cross is noise.
  map1 = [e1.noise];
  mult1 = [1];
end
% Second map
if any(type == [1,4])
  % Second element in the cross is signal.
  map2 = [];
  mult2 = [];
  % Loop over signal sim types.
  for ii=1:numel(simtype)
    if ~isempty(fidmodel.(simtype{ii})) && fidmodel.(simtype{ii}) > 0
      % Include this signal component.
      map2(end+1) = e2.(simtype{ii});
      mult2(end+1) = sqrt(fidmodel.(simtype{ii}));
    end
  end
else
  % Second element in the cross is noise.
  map2 = [e2.noise];
  mult2 = [1];
end

% Allocate output array.
n_ell = numel(l);
n_sim = size(aps(1).Cs_l, 3);
spectra = zeros(n_ell, n_sim);

% Now, loop over the maps for this cross and add up the spectra.
for ii=1:numel(map1)
  for jj=1:numel(map2)
    if map1(ii) == map2(jj)
      % Experiment auto-spectrum
      xind = aps_getxind(numel(aps), map1(ii), map2(jj));
      bpind = aps_getbpind(f1, f2, 0);
    else
      % Experiment cross-spectrum
      if map1(ii) < map2(jj)
        % Experiment cross-spectrum with normal ordering
        xind = aps_getxind(numel(aps), map1(ii), map2(jj));
        bpind = aps_getbpind(f1, f2, 1);
      else
        % Experiment cross-spectrum with reverse ordering -- need to
        % swap the role of cross and alt-cross!
        xind = aps_getxind(numel(aps), map2(jj), map1(ii));
        bpind = aps_getbpind(f2, f1, 1);
      end
    end
    spectra = spectra + mult1(ii) * mult2(jj) * ...
              squeeze(aps(xind).Cs_l(l,bpind,:)) .* repmat(rwf, 1, n_sim);
  end
end

return % END of function get_spec
