function likedata = get_likedata(dataset, likeopt)
% likedata = get_likedata(dataset, likeopt)
%
% High level function to build likedata structure used for multicomponent 
% likelihood analysis. Handles complications due to complicated simset 
% files. File paths are hard-coded for odyssey!
%
% These are the supported dataset options:
%   'BKP'      BICEP2+Keck(2012-13) x Planck, with Planck single-frequency 
%              auto-spectra replaced by cross-spectra between detector-set 
%              split. This is the primary dataset used for the 2015 BKP 
%              joint analysis paper.
%   'BKP_yr'   Like 'BKP', except using the Planck year split.
%   'BKP_hr'   Like 'BKP', except using the Planck half-ring split.
%   'BKP_auto' Like 'BKP', except using Planck full map auto-spectra.
% Can also use any of the four options above but with either 'BP' or 'KP' 
% instead of 'BKP' to get results for BICEP2 x Planck or Keck x Planck. 
%   'BK14'     BICEP2 and Keck Array data through 2014 (95 and 150 GHz), 
%              along with WMAP 23 GHz, Planck 30, 44, 70, and 353 GHz. 
%   'BK14_LTa'  BK14 dataset plus perfect zero-noise lensing template
%   'BK14_LTb'  BK14 dataset plus perfect lensing template with noise
%   'BK14_LTc'  BK14 dataset plus lensing template from BK CMB map
%               plus ideal phi map.
%
% [Input]
%   dataset  Select which dataset you want to use from options listed above.
%   likeopt  Option data structure, can specify the following fields:
%     .l       Which ell bins to include. Defaults to [2:6].
%     .fields  Which CMB fields to include. Defaults to {'B'}.
%     .fidmodel  Fiducial signal model to use when calculating bandpower 
%              covariance matrix components from sims. Defaults to lensed-LCDM 
%              (no foregrounds, no tensors). See like_read_simset for more 
%              details. Note that the choice of fiducial model *does not* 
%              prevent you from calculating the bandpower covariance matrix 
%              scaled to a different model using scale_bpcm.
%     .Planck_noisebias  Parameter that affects the artificial noise bias 
%              assigned to Planck data split crosses. Only relevant for datasets 
%              that use Planck splits. Defaults to 1. 
%     .Planck143_EBfull  Selects whether to override data-split cross for Planck 
%              143 EB with the spectrum derived from the full map (because of 
%              problems with that spectrum specific to the year-split cross). 
%              Only relevant for datasets that use Planck splits. Defaults to 0.
%
% [Output]
%   likedata  Data structure for multicomponent likelihood analysis.

% Set likeopt defaults.
if nargin < 2
  likeopt = [];
end
if ~isfield(likeopt, 'l')
  % Select ell bins.
  likeopt.l = [2:6];
end
if ~isfield(likeopt, 'fields')
  % Select CMB fields.
  likeopt.fields = {'B'};
end
if ~isfield(likeopt, 'fidmodel')
  % Fiducial model for bpcm calculation is just lensed-LCDM.
  likeopt.fidmodel.tensor = 0;
  likeopt.fidmodel.lcdm = 1;
  likeopt.fidmodel.dust = 0;
  likeopt.fidmodel.unlens = 0;
end

% Set Planck data-split options depending on dataset.
switch dataset
  case {'BKP', 'BKP_yr', 'BKP_hr', 'BP', 'BP_yr', 'BP_hr', 'KP', ...
        'KP_yr', 'KP_hr'}
    % Apply Planck data split procedure.
    % Option Planck_datasplit=1 is deprecated / not used.
    likeopt.Planck_datasplit = 2;
    % Apply defaults for noise bias treatment and P143 EB.
    if ~isfield(likeopt, 'Planck_noisebias')
      likeopt.Planck_noisebias = 1;
    end
    if ~isfield(likeopt, 'Planck143_EBfull')
      likeopt.Planck143_EBfull = 0;
    end
  otherwise
    % No data splits, make sure option is turned off.
    likeopt.Planck_datasplit = 0;
end

% Select simid based on choice of dataset.
switch dataset
  case {'BKP', 'BKP_yr', 'BKP_hr', 'BKP_auto'}
    simid = '1456x1457/';
    coadd = 'aab_';
  case {'BP', 'BP_yr', 'BP_hr', 'BP_auto'}
    simid = '0751x1613/';
    coadd = 'a_';
  case {'KP', 'KP_yr', 'KP_hr', 'KP_auto'}
    simid = '1351x1614/';
    coadd = 'ab_';
  case {'BK13'}
    simid = '1456x1615/';
    coadd = 'aab_';
  case {'BK14'}
    simid = '1459x1615/';
    coadd = 'aabd_';
  case {'BK14_LTa'}
    simid = '1459x1615x1631/';
    coadd = 'aabd_';
  case {'BK14_LTb'}
    simid = '1459x1615x1632/';
    coadd = 'aabd_';
  case {'BK14_LTc'}
    simid = '1459x1615x3005/';
    coadd = 'aabd_';
end

% Select finalfile based on choice of dataset.
switch dataset
  case {'BKP', 'BP', 'KP'}
    % jack4 = Planck detset split
    finalfile = ['/n/bicepfs2/b2planck/pipeline/final/' simid ...
                 'real_' coadd 'filtp3_weight3_gs_dp1102_jack0_' ...
                 'real_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
                 'real_' coadd 'filtp3_weight3_gs_dp1100_jack4_' ...
                 'pureB_matrix_directbpwf'];
  case {'BKP_yr', 'BP_yr', 'KP_yr'}
    % jack3 = Planck year split
    finalfile = ['/n/bicepfs2/b2planck/pipeline/final/' simid ...
                 'real_' coadd 'filtp3_weight3_gs_dp1102_jack0_' ...
                 'real_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
                 'real_' coadd 'filtp3_weight3_gs_dp1100_jack3_' ...
                 'pureB_matrix_directbpwf'];
  case {'BKP_hr', 'BP_hr', 'KP_hr'}
    % jack5 = Planck half-ring split
    finalfile = ['/n/bicepfs2/b2planck/pipeline/final/' simid ...
                 'real_' coadd 'filtp3_weight3_gs_dp1102_jack0_' ...
                 'real_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
                 'real_' coadd 'filtp3_weight3_gs_dp1100_jack5_' ...
                 'pureB_matrix_directbpwf'];
  case {'BKP_auto', 'BP_auto', 'KP_auto'}
    % BKP final file with no Planck data splits.
    finalfile = ['/n/bicepfs2/b2planck/pipeline/final/' simid ...
                 'real_' coadd 'filtp3_weight3_gs_dp1102_jack0_' ...
                 'real_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
                 'pureB_matrix_directbpwf'];
  case {'BK14'}
    % BK14 final file is stored in bicep2 pipeline directory
    finalfile = ['/n/bicepfs1/bicep2/pipeline/final/' simid ...
                 'real_' coadd 'filtp3_weight3_gs_dp1102_jack0_' ...
                 'real_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
                 'pureB_matrix_cm_directbpwf'];
  case {'BK14_LTa', 'BK14_LTb'}
    % BK14 final files are stored in bicep2 pipeline directory
    finalfile = ['/n/bicepfs1/bicep2/pipeline/final/' simid ...
                 'real_' coadd 'filtp3_weight3_gs_dp1102_jack0_' ...
                 'real_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
                 'real_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
                 'pureB_matrix_cm_directbpwf'];
  case {'BK14_LTc'}
    % BK14 final files are stored in bicep2 pipeline directory
    finalfile = ['/n/bicepfs1/bicep2/pipeline/final/' simid ...
                 'real_' coadd 'filtp3_weight3_gs_dp1102_jack0_' ...
                 'real_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
                 'real_lt_CMBbk_PHIideal_pureB_matrix_cm_directbpwf'];
  case {'BK13'}
    % BK13 final file that has Ext datasets observed with BK14
    finalfile = ['/n/bicepfs1/bicep2/pipeline/final/' simid ...
                 'real_' coadd 'filtp3_weight3_gs_dp1102_jack0_' ...
                 'real_aabd_filtp3_weight3_gs_dp1100_jack0_' ...
                 'pureB_matrix_cm_directbpwf'];   
  otherwise
    % Unsupported dataset.
    disp(sprintf('[get_likedata] ERROR: dataset %s is not defined'), dataset);
    return 
end
% Read bandpowers, etc, from final file.
likedata = like_read_final(finalfile, likeopt);

% Select apsfile based on dataset.
% Don't need to specify mapdef structure for the simple 45666 files
% from original BKP analysis, only for more complicated cases.
switch dataset
  case {'BKP', 'BP', 'KP'}
    % 45666 file including Planck detset split
    apsfile = ['/n/bicepfs2/b2planck/pipeline/aps/' simid ...
               'xxx4_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx5_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack4_matrix'];
  case {'BKP_yr', 'BP_yr', 'KP_yr'}
    % 45666 file including Planck year split
    apsfile = ['/n/bicepfs2/b2planck/pipeline/aps/' simid ...
               'xxx4_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx5_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack3_matrix'];
  case {'BKP_hr', 'BP_hr', 'KP_hr'}
    % 45666 file including Planck half-ring split
    apsfile = ['/n/bicepfs2/b2planck/pipeline/aps/' simid ...
               'xxx4_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx5_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack5_matrix'];
  case {'BKP_auto', 'BP_auto', 'KP_auto'}
    % 45666 file including Planck detset split
    % But mapdef is chosen so that we ignore the additional data
    % split maps.
    apsfile = ['/n/bicepfs2/b2planck/pipeline/aps/' simid ...
               'xxx4_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx5_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack4_matrix'];
  case {'BK14'}
    % Build mapdef structure.
    % Map 1 = lensed-LCDM, observed by Keck 95 GHz
    % Map 2 = lensed-LCDM, observed by Keck 150 GHz (also used for WMAP/Planck)
    % Maps 3--11 = noise sims for BK95, BK150, W023, P030, P044, P070, P100, P217, P353
    for ii=1:numel(likedata.opt.expt)
      if strcmp(likedata.opt.expt{ii}, 'BK14_95')
        % signal sims, observed as 95 GHz
        likedata.opt.mapdef(ii).lcdm = 1;
      else
        % signal sims, observed as 150 GHz
        likedata.opt.mapdef(ii).lcdm = 2;
      end
      likedata.opt.mapdef(ii).tensor = [];
      likedata.opt.mapdef(ii).dust = [];
      likedata.opt.mapdef(ii).unlens = [];
      likedata.opt.mapdef(ii).noise = 2 + ii;
    end
    % 566 file, stored in Keck pipeline aps directory 
    apsfile = ['/n/bicepfs2/keck/pipeline/aps/' simid ...
               'xxx5_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'matrix_cm'];
  case {'BK14_LTa', 'BK14_LTb'}
    % Build mapdef structure.
    % Map 1 = lensed-LCDM, observed by Keck 95 GHz
    % Map 2 = lensed-LCDM, observed by Keck 150 GHz (also used for WMAP/Planck)
    % Maps 3-12 = noise sims for BK95, BK150, W023, P030, P044, P070, P100, P217, P353, LT
    for ii=1:numel(likedata.opt.expt)
      if strcmp(likedata.opt.expt{ii}, 'BK14_95')
        % signal sims, observed as 95 GHz
        likedata.opt.mapdef(ii).lcdm = 1;
      else
        % signal sims, observed as 150 GHz
        likedata.opt.mapdef(ii).lcdm = 2;
      end
      likedata.opt.mapdef(ii).tensor = [];
      likedata.opt.mapdef(ii).dust = [];
      likedata.opt.mapdef(ii).unlens = [];
      likedata.opt.mapdef(ii).noise = 2 + ii;
    end
    % 5666 file, stored in Keck pipeline aps directory
    apsfile = ['/n/bicepfs2/keck/pipeline/aps/' simid ...
               'xxx5_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'matrix_cm'];
  case {'BK14_LTc'}
    % Build mapdef structure.
    % Map 1 = lensed-LCDM, observed by Keck 95 GHz
    % Map 2 = lensed-LCDM, observed by Keck 150 GHz (also used for WMAP/Planck)
    % Maps 3--12 = noise sims for BK95, BK150, W023, P030, P044, P070, P100, P217, P353, LT
    for ii=1:numel(likedata.opt.expt)
      if strcmp(likedata.opt.expt{ii}, 'BK14_95')
        % signal sims, observed as 95 GHz
        likedata.opt.mapdef(ii).lcdm = 1;
      else
        % signal sims, observed as 150 GHz
        likedata.opt.mapdef(ii).lcdm = 2;
      end
      likedata.opt.mapdef(ii).tensor = [];
      likedata.opt.mapdef(ii).dust = [];
      likedata.opt.mapdef(ii).unlens = [];
      likedata.opt.mapdef(ii).noise = 2 + ii;
    end
    % 5666 file, stored in Keck pipeline aps directory
    apsfile = ['/n/bicepfs2/keck/pipeline/aps/' simid ...
               'xxx5_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_lt_CMBbk_PHIideal_matrix_cm'];
  case {'BK13'}
    % Build mapdef structure.
    % Map 1 = lensed-LCDM, observed by Keck 150 GHz (also used for WMAP/Planck)
    % Maps 2--10 = noise sims for BK150, W023, P030, W033, P044, P070, P100,
    %		  		 P143, P217, P353
    for ii=1:numel(likedata.opt.expt)
      % signal sims, observed as 150 GHz
      likedata.opt.mapdef(ii).lcdm = 1;
      likedata.opt.mapdef(ii).tensor = [];
      likedata.opt.mapdef(ii).dust = [];
      likedata.opt.mapdef(ii).unlens = [];
      likedata.opt.mapdef(ii).noise = 1 + ii;
    end
    % 566 file, stored in Keck pipeline aps directory 
    apsfile = ['/n/bicepfs2/keck/pipeline/aps/' simid ...
               'xxx5_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_' coadd 'filtp3_weight3_gs_dp1100_jack0_' ...
               'xxx6_aabd_filtp3_weight3_gs_dp1100_jack0_' ...
               'matrix_cm'];
  otherwise
    % Unsupported dataset.
    disp(sprintf('[get_likedata] ERROR: dataset %s is not defined'), dataset);
    return 
end

% Build covariance matrix components from aps file.
likedata = like_read_simset(apsfile, likedata);

% Planck detset split doesn't contain results for LFI frequencies.
switch dataset
  case {'BKP'}
    likedata = likeopt_select(likedata, {'B2K','P100','P143','P217','P353'}, [], []);
  case {'BP'}
    likedata = likeopt_select(likedata, {'B2','P100','P143','P217','P353'}, [], []);
  case {'KP'}
    likedata = likeopt_select(likedata, {'K','P100','P143','P217','P353'}, [], []);
end

% Hack to choose bpcov.sig pre factor of 2 bug fix
%ldep = load('/n/home05/cbischoff/BKP_yr_deprecated.mat');
%likedata.bpcov.sig = ldep.likeopt.aps.sig;

% Select BK + LFI + 217 + 353
%if strcmp(dataset, 'BKP_yr')
%  likedata = likeopt_select(likedata, [1:4,7,8], [], []);
%end

% Downselect to 95, 150 and 220
%likedata = likeopt_select(likedata, [1,5,7], [], []);

% Select old dataset, i.e. no WMAP, and only P353 from the HFI's
%likedata = likeopt_select(likedata, [3:7,11], [], []);

% Read CAMB theory spectra.
likedata = like_read_theory(likedata);
% Get experiment bandpasses.
likedata = like_read_bandpass(likedata);

return % END of function get_likedata
