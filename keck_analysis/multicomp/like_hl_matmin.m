function neglogl=like_hl_matmin(x,dumfunc,likedata,prep)
% neglogl=like_hl_matmin(x,dumfunc,likedata,prep)
%
% wrap like_hl and like_getexpvals for use by matmin

x

% we can set bounds in minuit so no need for penalty terms
neglogl= -1*like_hl(like_getexpvals([x(1),1,x(2:7),1,2,x(8),19.6],likedata),likedata.real,likedata.N_l,prep)+0.5*(x(6)-1.59)^2/0.11^2+0.5*(x(4)+3.1)^2/0.3^2;

neglogl

return
