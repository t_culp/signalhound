function expv = like_getexpvals(param, likedata, return_type)
% expv = like_getexpvals(param, likedata, return_type)
%
% Calculate bandpower expectation values for 11 parameter multicomponent
% model.
%
% [Input]
%   param        Multi-component model parameters:
%     param(1)  = r, tensor-to-scalar ratio
%     param(2)  = A_L, lensing amplitude (1 = standard LCDM lensing)
%     param(3)  = synchrotron power spectrum amplitude, in
%                 uK_{CMB}^2, at nu = 150 GHz and ell = 100
%     param(4)  = dust power spectrum amplitude, in uK_{CMB}^2, at
%                 nu = 150 GHz and ell = 100
%     param(5)  = beta_sync, synchrotron frequency spectral index
%     param(6)  = gamma_sync, synchrotron spatial spectral index
%     param(7)  = beta_dust, dust frequency spectral index
%     param(8)  = gamma_dust, dust spatial spectral index
%     param(9)  = E/B ratio for synchrotron. 1 means that synchrotron 
%                 contributes equal amounts of E and B power; 2 means 
%                 that synchrotron contributes twice as much E as B; etc.
%     param(10) = E/B ratio for dust. 1 means that dust contributes equal 
%                 amounts of E and B power; 2 means that dust
%                 contributes twice as much E as B; etc.
%     param(11) = dust/sync correlation parameter
%     param(12) = T_d, dust temperature (19.6 K for PIP97 model)
%   likedata     Data structure for multicomponent likelihood analysis
%   return_type  Bitwise parameter controlling output structure:
%                If return_type MOD 2 = 1, then the function will return 
%                full delta-ell=1 spectra for the model, rather than 
%                bandpower expectation values. 
%                If (return_type / 2) MOD 2 = 1, then the function will 
%                return expected power for the four model components
%                separately.
%                Default: return_type = 0 (i.e. return bandpowers for 
%                sum over all components).
%
% [Output]
%   expv  Array of model bandpower expectation values with size (N,M),
%         where N is the number of ell bins and M = x*(x+1)/2
%         with x = (# of experiments) * (# of CMB fields).
%         Ordering of the second dimension follows the "H-L order", 
%         which is kind of complicated.

% Optional argument defaults.
if nargin < 3
  return_type = 0;
end

% Get field rms values for this model.
[lfine, fld] = model_rms(param, likedata);

% Get number of ell bins, CMB fields, experiments.
n_ell = numel(likedata.opt.l);
n_field = numel(unique(likedata.opt.fields));
n_expt = numel(likedata.opt.expt);
order = expt_field_order(n_expt, n_field); % H-L vecp ordering
n_spec = numel(order);

% Now loop over all pairwise combinations of (expt, field) using
% H-L vecp ordering.
for s=order
  if mod(return_type, 2) == 0
    % Get bandpower window functions for this particular combination of 
    % experiments and fields. 
    bpwf = likedata.bpwf.Cs_l(:,[s.i:n_spec:n_spec*n_ell]);
  end

  if s.f1 == s.f2
    % If this is a TT, EE, or BB spectrum, then variance of that
    % field can be calculated from the field rms values.

    % Select the field rms values corresponding to this bandpower.
    fld1 = squeeze(fld(:, (s.e1 - 1) * n_field + s.f1, :));
    fld2 = squeeze(fld(:, (s.e2 - 1) * n_field + s.f2, :));
    bp = fld1 .* fld2;
  else
    if ismember(likedata.opt.fields(s.f1), 'TE') && ...
        ismember(likedata.opt.fields(s.f2), 'TE')
      % For TE, include the CMB theory spectrum but no foreground
      % contribution (because it isn't implemented).
      bp(:,1) = likedata.theory.lcdm(:,4);
      bp(:,[2:4]) = repmat(0 * likedata.theory.lcdm(:,4), 1, 3);
    else
      % Set TB, EB to zero.
      bp = repmat(0 * likedata.theory.lcdm(:,4), 1, 4);
    end
  end

  if mod(floor(return_type / 2), 2) == 0
    % Model signal is divided up into four components, which are 
    % independent so they have no expected cross-power. To get total 
    % power from all components, just sum the power contribution from 
    % each component.
    bp = sum(bp, 2);
  end

  if mod(return_type, 2) == 0
    % Apply window functions to obtain bandpowers.
    for jj=1:size(bp,2)
      expv(:,s.i,jj) = apply_bpwf(bp(:,jj), bpwf);
    end
  else
    % Return the delta-ell = 1 theory spectra.
    expv(:,s.i,:) = bp;
  end
end

return % END of function like_getexpvals


function expv = apply_bpwf(model, bpwf)
% expv = apply_bpwf(model, bpwf)
%
% Calculates expectation values for the given model and bandpower
% window functions. 
%
% [Input]
%   model  Theory spectrum, vector with length [n_ell]
%   bpwf   Window function array of size [n_ell, n_band] where n_band is the
%          number of bandpowers to calculate.
%
% [Output]
%   expv   Vector of [n_band] expectation values.

nband = size(bpwf, 2);
expv = sum(repmat(model, 1, nband) .* bpwf, 1);
expv = expv';

return % END of function apply_bpwf
