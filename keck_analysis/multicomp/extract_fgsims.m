function fgsim = extract_fgsims(fgfile, likeopt, rfield, expt_select, Planck_datasplit)
% fgsim = extract_fgsims(fgfile, likeopt, rfield, expt_select, Planck_datasplit)
%
% Extract bandpower simulations from a pipeline final .mat file and
% reorder them to use as fake data for multicomponent likelihood.
%
% [Input]
%   fgfile   Path to reduc_final output file containing foreground sims.
%   likeopt  Likelihood options structure, usually stored as likedata.opt.
%   rfield   Optional argument. Name of the field in the r data structure
%            where the desired sims are stored. Defaults to 'simr'.
%   expt_select  Optional argument. Specify which experiments to keep from 
%            the reduc_final data structure. The number of experiments left 
%            after doing this selection must match the number of experiments 
%            in the likeopt structure. Defaults to keeping all
%            experiments.
%   Planck_datasplit  If this argument is non-zero, then Planck 
%            single-frequency autos will be replaced by the corresponding
%            data split crosses. Defaults to zero.
%
% [Output]
%   fgsim    Array with size [n_ell, n_spectra, n_sim], where n_ell and 
%            n_spectra are the number of ell bins and expt/field 
%            combinations from the likeopt data structure and n_sim is the
%            number of sim realizations in the final data structure.

% Optional argument.
if nargin < 3
  rfield = 'simr';
end
if isempty(rfield)
  rfield = 'simr';
end

% Load file containing foreground sims.
fg = load(fgfile);
r = fg.r;

% Default behavior is Planck_datasplit = 0.
if nargin < 5
  Planck_datasplit = 0;
end
if Planck_datasplit
  % Determine which maps have associated data splits.
  nmap = 0.5 * (-1 + sqrt(1 + 8 * numel(r)));
  names = fg.opt.finalopt.mapname(1:nmap);
  issplit = zeros(1, numel(names));
  for ii=1:numel(names)
    ih1 = find(strcmp([names{ii} 'H1'], names));
    ih2 = find(strcmp([names{ii} 'H2'], names));
    if ~isempty(ih1) && ~isempty(ih2)
      % Mark these maps as data splits.
      issplit(ih1) = 1;
      issplit(ih2) = 2;
      % Copy data split cross in place of single frequency auto.
      splitind = aps_getxind(r, ih1, ih2);
      r(ii) = combine_cross(r(splitind));
    end
  end

  % Now, drop all of the data split autos and crosses.
  % The ordering used for keepr matches ordering of pipeline aps
  % field, so the downselected r structure can be passed along to
  % the next steps.
  full = find(~issplit);
  keepr = full;
  for ii=1:numel(full)
    for jj=(ii+1):numel(full)
      keepr(end+1) = aps_getxind(r, full(ii), full(jj));
    end
  end
  % Downselect.
  r = r(keepr);
end

% Downselect experiments.
if nargin < 4
  expt_select = [];
end
if ~isempty(expt_select)
  % Determine which elements of r structure to keep.
  % The ordering used for keepr matches ordering of pipeline aps
  % field, so the downselected r structure can be passed along to
  % the next steps.
  keepr = expt_select;
  for ii=1:numel(expt_select)
    for jj=(ii+1):numel(expt_select)
      keepr(end+1) = aps_getxind(r, expt_select(ii), expt_select(jj));
    end
  end
  % Downselect.
  r = r(keepr);
end

% Determine number of ell bins, experiments, CMB fields.
n_ell = numel(likeopt.l);
n_expt = numel(likeopt.expt);
n_field = numel(likeopt.fields);
% Check that r data structure matches.
if numel(r) ~= (n_expt * (n_expt + 1) / 2)
  disp(['ERROR: number of experiments in r, likeopt structures do ' ...
        'not match']);
  fgsim = [];
  return 
end

% Loop over expt, field combinations.
order = expt_field_order(n_expt, n_field);
for s=order
  % Find the right element in the r data structure.
  xi = aps_getxind(r, s.e1, s.e2);
  iscross = (s.e1 ~= s.e2);
  % Get bandpowers.
  bi = aps_getbpind(likeopt.fields(s.f1), likeopt.fields(s.f2), iscross);
  fgsim(:,s.i,:) = squeeze(r(xi).(rfield)(likeopt.l,bi,:));
end

return % END of function extract_fgsims


function r = combine_cross(r)
% r = combine_cross(r)
%
% Takes an element of the r structure from final .mat files and
% averages cross and alt-cross. Works for elements of aps structure too.

fld = fields(r);
for i=1:numel(fld)
  if size(r.(fld{i}), 2) == 9
    r.(fld{i}) = (r.(fld{i})(:,[1,2,3,4,5,6],:) + ...
                  r.(fld{i})(:,[1,7,3,4,8,9],:)) / 2;
  end
end

return % END of function combine_cross
