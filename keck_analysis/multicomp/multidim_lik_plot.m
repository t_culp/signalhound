function multidim_lik_plot(beta_index, corr_index, hl, expt)

% Reads the data from 5D grid runs, fixes the last two dimensions, and
% marginalizes over each of the remaining dimensions separately to produce three 2D-plots 
%
% NOTE: Make sure to select the right output folder (look for 'figpath' in
% one of the last lines).
%
% The 5 dimensions are: r, sync amplitude, dust amplitude, beta sync, dust/sync correlation
% Corresponding to elements 1, 3, 4, 5, and 11 of the following array of
% parameter values. The order of the parameters can be checked in like_getexpvals
% p = [0, 1, 0, 0, -3.3, -0.6, 1.5, -0.4, 1, 1, 0];
% 
% The step sizes and ranges of the grid need to be provided below, as well
% as the path to where the files resulting from the grid evaluations lie.
% The .mat files are expected to store just logl_chi2 and logl_hl, which
% are outputs of like_wrapper.hl
%
% The function can be used to produce a singular plot, for a specific
% choice of beta_sync, correlation, likelihood type and expt choice; or can
% be used as an input to posting_plots.m to produce all the plots in the
% Multi-Frequency Analysis Results posting. 

%Specify step sizes for each paramater
r_step = 0.01;
dust_step = 1e-3;
sync_step = 1e-5;
beta_step = 0.3;
corr_step = 0.25;

%Specify ranges for each parameter
r_range = [0:r_step:0.6];
dust_range = [0:dust_step:0.040];
sync_range = [0:sync_step:5e-4];
beta_sync = [-3.3:beta_step:-3.0];
corr_range = [0:corr_step:1];

% Load the specified file. b2 is a placeholder for B2_150. b1 for B1_100 and
% wk for WMAP_K
switch expt
    case 'b2b1wk'
        load('/n/home01/vbuza/code/work/data/09152014/5D_lik_b2b1wk.mat');
    case 'b2b1'
	load('/n/home01/vbuza/code/work/data/09152014/5D_lik_b2b1.mat');
    case 'b2wk'
	load('/n/home01/vbuza/code/work/data/09152014/5D_lik_b2wk.mat');
    case 'b2'
	load('/n/home01/vbuza/code/work/data/09152014/5D_lik_b2.mat');
end

% Check for the type of likelihood evaluation selected and make apropriate
% choices of arrays
switch hl
    case 'chi2'
        lik_type = {'chi2'};
        logl = logl_chi2;
    case 'hl'
        lik_type = {'hl'};
        logl = logl_hl;
    case 'chi2_cpts'
        lik_type = {'chi2_cpts'};
        logl = logl_chi2_cpts;
    case 'hl_cpts'
        lik_type = {'hl_cpts'};
        logl = logl_hl_cpts;
end

% Order: r, sync amplitude, dust amplitude, beta sync, corr %
logLike = squeeze(logl(:,:,:,beta_index,corr_index));

% Since the output of like_wrapper_hl.m is a log likelihood, in order to
% marginalize, we need to exponentiate first, then marginalize, then
% normalize. This marginalization is made over the dust amplitude.
lik2d = sum(exp(logLike), 3);
lik2d = lik2d / sum(lik2d(:));
logLik2d = log(lik2d);
logLik2d = logLik2d - max(logLik2d(:));
lik2d = exp(logLik2d);

% Finds the maximum of lik2d and its position on the x,y axis
[maxL,ind] = max(lik2d(:));
[m,n] = ind2sub(size(lik2d),ind);
max_y = r_step*(m-1);
max_x = sync_step*(n-1);

% Visibility is set to off, so don't expect the plot in a pop-up window.
figure('visible','off');
% figure();
% setwinsize(gcf,1200,450);

subplot(2,3,1)
% Plot 1, 2 and 5 sigma contours. Apprixomate -- not exactly Bayesian
contourf(sync_range, r_range, lik2d, exp(-([1, 2, 5]).^2/2));
hold on;
plot(max_x, max_y, 'kx');
xlim([0 4e-4]);
colormap cool
caxis([0 1.5]);
set(gca, 'YDir', 'normal')
xlabel('sync amplitude [uK_{CMB}^2]');
ylabel('r');

% The same step above is repeated, now marginalizing over sync.
lik2d = squeeze(sum(exp(logLike), 2));
lik2d = lik2d / sum(lik2d(:));
logLik2d = log(lik2d);
logLik2d = logLik2d - max(logLik2d(:));
lik2d = exp(logLik2d);

[maxL,ind] = max(lik2d(:));
[m,n] = ind2sub(size(lik2d),ind);
max_y = r_step*(m-1);
max_x = dust_step*(n-1);

subplot(2,3,2)
contourf(dust_range, r_range, lik2d, exp(-([1, 2, 5]).^2/2));
hold on;
plot(max_x, max_y, 'kx');
colormap cool
caxis([0 1.5]);
set(gca, 'YDir', 'normal')
xlabel('dust amplitude [uK_{CMB}^2]');
ylabel('r');

% The same step above is repeated, now marginalizing over r.
lik2d = squeeze(sum(exp(logLike), 1));
lik2d = lik2d / sum(lik2d(:));
logLik2d = log(lik2d);
logLik2d = logLik2d - max(logLik2d(:));
lik2d = exp(logLik2d);

[maxL,ind] = max(lik2d(:));
[m,n] = ind2sub(size(lik2d),ind);
max_y = sync_step*(m-1);
max_x = dust_step*(n-1);

subplot(2,3,3)
contourf(dust_range, sync_range, lik2d, exp(-([1, 2, 5]).^2/2));
hold on;
plot(max_x, max_y, 'kx');
colormap cool
ylim([0 4e-4]);
caxis([0 1.5]);
set(gca, 'YDir', 'normal')
xlabel('dust amplitude [uK_{CMB}^2]');
ylabel('sync amplitude [uK_{CMB}^2]');


%%%%%%%% TRIAL TO HAVE 2 MARGINS
lik1d = squeeze(sum(sum(exp(logLike), 3),2));
lik1d = lik1d / sum(lik1d(:));
logLik1d = log(lik1d);
logLik1d = logLik1d - max(logLik1d(:));
lik1d = exp(logLik1d);

[maxL,ind] = max(lik1d(:));
[m,n] = ind2sub(size(lik1d),ind);
max_x = r_step*(m-1);

sig1 = maxL * exp(-1/2);
sig2 = maxL * exp(-1);

[max_68, indx1] = min(abs(lik1d(m:length(lik1d))-sig1));
r_68_max = r_step*(m+indx1-2);
lik1d_68_max = lik1d(m+indx1-1);

[min_68, indx2] = min(abs(lik1d(1:m)-sig1));
r_68_min = r_step*(indx2-1);
lik1d_68_min = lik1d(indx2);

subplot(2,3,4)
plot(r_range, lik1d);
hold on;
plot([max_x max_x], [0 1], 'k')
plot([r_68_min r_68_min], [0 lik1d_68_min], 'k--')
plot([r_68_max r_68_max], [0 lik1d_68_max], 'k--')
plot([],'k:')
ylim([0 1.2])
xlim([0 0.6])
xlabel('r');
ylabel('likelihood');

%%% DUST
lik1d = squeeze(sum(sum(exp(logLike), 2),1));
lik1d = lik1d / sum(lik1d(:));
logLik1d = log(lik1d);
logLik1d = logLik1d - max(logLik1d(:));
lik1d = exp(logLik1d);

[maxL,ind] = max(lik1d(:));
[m,n] = ind2sub(size(lik1d),ind);
max_x = dust_step*(ind-1);

sig1 = maxL * exp(-1/2);
sig2 = maxL * exp(-1);

[max_68, indx1] = min(abs(lik1d(m:length(lik1d))-sig1));
dust_68_max = dust_step*(m+indx1-2);
lik1d_68_max = lik1d(m+indx1-1);
    
[min_68, indx2] = min(abs(lik1d(1:m)-sig1));
dust_68_min = dust_step*(indx2-1);
lik1d_68_min = lik1d(indx2);

subplot(2,3,5)
plot(dust_range, lik1d);
hold on;
plot([max_x max_x], [0 1], 'k')
plot([dust_68_min dust_68_min], [0 lik1d_68_min], 'k--')
plot([dust_68_max dust_68_max], [0 lik1d_68_max], 'k--')
plot([],'k:')
ylim([0 1.2])
xlim([0 0.04])
xlabel('dust amplitude [uK_{CMB}^2]');
ylabel('likelihood');

% SYNC
lik1d = squeeze(sum(sum(exp(logLike), 3),1));
lik1d = lik1d / sum(lik1d(:));
logLik1d = log(lik1d);
logLik1d = logLik1d - max(logLik1d(:));
lik1d = squeeze(exp(logLik1d));

[maxL,ind] = max(lik1d(:));
max_x = sync_step*(ind-1);

sig1 = maxL * exp(-1/2);
sig2 = maxL * exp(-1);

[max_68, indx1] = min(abs(lik1d(ind:length(lik1d))-sig1));
sync_68_max = sync_step*(ind+indx1-2);
lik1d_68_max = lik1d(ind+indx1-1);

[min_68, indx2] = min(abs(lik1d(1:ind)-sig1));
sync_68_min = sync_step*(indx2-1);
lik1d_68_min = lik1d(indx2);

subplot(2,3,6)
plot(sync_range, lik1d);
hold on;
plot([max_x max_x], [0 1], 'k')
plot([sync_68_min sync_68_min], [0 lik1d_68_min], 'k--')
plot([sync_68_max sync_68_max], [0 lik1d_68_max], 'k--')
plot([],'k:')
ylim([0 1.2])
xlim([0 4e-4])
xlabel('sync amplitude [uK_{CMB}^2]');
ylabel('likelihood');


% Makes a consistent choice for names, selects size and font and saves
% figure to disk in a .png format
beta_name = {'beta33','beta30'};
corr_name = {'corr0','corr25','corr50','corr75','corr100'};
figpath = '/n/home01/vbuza/code/work/data/09152014/posting_plots_fixed_WK/';
figname = strcat(beta_name{beta_index},'_',corr_name{corr_index},'_',lik_type,'_',expt);
set(gcf, 'PaperPosition',[0 0 9.5 6]);
set(findall(gcf,'type','text'),'fontSize',8);
saveas(gcf, fullfile(figpath, char(figname)), 'eps');

% Use this if you want to plot a specific conditional likelihood.
% The case here is looking at B2+B1_100 at non-zero fixed sync.
% figure(); clf;
% a = load('C:\Users\Victor\Desktop\Cmb Pol\Lik Files v4\data\5D_lik_b2b1.mat');
% likcond = exp(a.logl_hl(:,12,:,1,1));
% likcond = likcond / max(likcond(:));
% likcond = squeeze(likcond);
% contourf(dust_range, r_range, likcond, exp(-([1,2,5]).^2 / 2));
% hold all;
% caxis([0 1.5]);

end
