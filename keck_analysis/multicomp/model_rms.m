function [ell, cmp] = model_rms(param, likedata)
% [ell, cmp] = model_rms(param, likedata)
%
% Calculates field rms values, in uK_CMB, for components of the model.
% These field rms values can be used to calculate bandpower expectation 
% values for TT/EE/BB spectra or they can be used to scale bandpower
% covariances that have been derived from sims.
%
% NOTE: RMS values correspond to the square root of l*(l+1)C_l/(2*pi), 
% so these should not be compared to fluctuation levels in actual maps.
%
% [Input]
%   param    Multi-component model parameters:
%     param(1)  = r, tensor-to-scalar ratio
%     param(2)  = A_L, lensing amplitude (1 = standard LCDM lensing)
%     param(3)  = synchrotron power spectrum amplitude, in
%                 uK_{CMB}^2, at nu = 150 GHz and ell = 100
%     param(4)  = dust power spectrum amplitude, in uK_{CMB}^2, at
%                 nu = 150 GHz and ell = 100
%     param(5)  = beta_sync, synchrotron frequency spectral index
%     param(6)  = gamma_sync, synchrotron spatial spectral index
%     param(7)  = beta_dust, dust frequency spectral index
%     param(8)  = gamma_dust, dust spatial spectral index
%     param(9)  = E/B ratio for synchrotron. 1 means that synchrotron 
%                 contributes equal amounts of E and B power; 2 means 
%                 that synchrotron contributes twice as much E as B; etc.
%     param(10) = E/B ratio for dust. 1 means that dust contributes equal 
%                 amounts of E and B power; 2 means that dust
%                 contributes twice as much E as B; etc.
%     param(11) = dust/sync correlation parameter
%     param(12) = T_d, dust temperature (19.6 K for PIP97 model)
%   likedata  Data structure for multicomponent likelihood analysis
%
% [Output]
%   ell  Ell values that the model has been evaluated for. Typically 
%        delta-ell = 1 spacing.
%   cmp  Array containing rms values for each ell, expt/field combination, 
%        and model component. Dimension 1 of the array ranges over ell 
%        values (should have the same size as 'ell' output). Dimension 2 
%        of the array ranges over experiment/field combinations. For 
%        example, with two experiments and two fields ('E' and 'B'), this
%        dimension would have size 4 with ordering: 
%          [expt1 E, expt1 B, expt2 E, expt2 B]
%        Dimension 3 has size=4, for four model components:
%          cmp(:,:,1) = CMB component
%          cmp(:,:,2) = uncorrelated synchrotron component
%          cmp(:,:,3) = uncorrelated dust component
%          cmp(:,:,4) = correlated synchrotron + dust

% Ell values over which input models are defined.
ell = likedata.theory.l;

% Get number of experiments and CMB fields.
n_field = numel(unique(likedata.opt.fields));
n_expt = numel(likedata.opt.expt);

% Calculate power for each model component at pivot frequency
% (usually 150 GHz).
% * First dimension is ell.
% * Second dimension is {TT, EE, BB}.
% ==NOTES==
% 1. likeopt.mod.tensor and likeopt.mod.lensing are non-zero for BB only.
% 2. Foregrounds are calculated for EE and BB only.
% 3. Notes 1 and 2 imply that TT will just give standard LCDM 
%    result, regardless of model parameters.
cmb = likedata.theory.lcdm(:,1:3) + ...
      param(1) / likedata.theory.r * likedata.theory.tensor(:,1:3) + ...
      param(2) * likedata.theory.lensing(:,1:3);
cmbl = param(2) * likedata.theory.lensing(:,1:3);
sync = zeros(size(cmb));
sync(:,3) = param(3) * (ell / likedata.opt.fg_pivot).^param(6);
sync(:,2) = sync(:,3) * param(9);
dust = zeros(size(cmb));
dust(:,3) = param(4) * (ell / likedata.opt.fg_pivot).^param(8);
dust(:,2) = dust(:,3) * param(10);

% Loop over experiments and fields.
for ii=1:n_expt
  % Lensing template map is labeled 'LT'. Includes only CMB lensing signal.
  % NOTE: Currently works for B-modes only!!
  if strcmp(likedata.opt.expt{ii}, 'LT')
    scale_sync = 0;
    scale_dust = 0;
    use_cmb = cmbl;
  else
    % Calculate sync and dust scaling to the observing frequency for
    % this experiment.
    scale_sync = freq_scaling(likedata.bandpass{ii}, param(5), [], ...
                              likedata.opt.sync_freq);
    scale_dust = freq_scaling(likedata.bandpass{ii}, param(7), ...
                              param(12), likedata.opt.dust_freq);
    use_cmb = cmb;
  end
    
  for jj=1:n_field
    % Index of expt,field in cmp array.
    k = (ii-1)*n_field + jj;
    % Field type: T=1, E=2, B=3
    ftype = find(strcmp(likedata.opt.fields{jj}, {'T', 'E', 'B'}));

    % RMS of CMB component.
    cmp(:,k,1) = sqrt(use_cmb(:,ftype));

    % RMS of foreground fields.
    % First, scale sync and dust power spectra for this expt, field.
    sy = sync(:,ftype) * scale_sync^2;
    du = dust(:,ftype) * scale_dust^2;
    % Correlated foregrounds are divided into three independent 
    % fields:
    %   2. Uncorrelated sync
    %   3. Uncorrelated dust
    %   4. Correlated parts of sync and dust
    cmp(:,k,2) = sqrt((1 - param(11)) * sy);
    cmp(:,k,3) = sqrt((1 - param(11)) * du);
    cmp(:,k,4) = sqrt(param(11) * (sy + du + 2 * sqrt(sy .* du)));
  end
end
