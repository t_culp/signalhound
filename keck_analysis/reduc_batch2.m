function reduc_batch2(tags,doplots,cpfilestobicepfs1)
% reduc_batch1(tags,doplots)
%
% Run after reduc_batch1 to make reducplots
% if doplots is set to 1, generate hard copy redcuplots
%
%
% % e.g.
% reduc_batch1(get_tags('cmb2011'))

if(~exist('doplots','var'))
  doplots=0;
end

if(~exist('onlyreducplots','var'))
  onlyreducplots=0;
end

if(~exist('cpfilestobicepfs1','var'))
  cpfilestobicepfs1=[];
end

ph=taglist_to_phaselist(tags)

for i=1:length(ph.name)
   
  reduc_makeperphasemaps(ph.tags{i});
  
  if doplots
    reduc_plotperphasemaps(ph.name{i});

    if(~isempty(cpfilestobicepfs1))
      copyfile(['reducplots/perphase_' ph.name{i} '*'],cpfilestobicepfs1)
    end
  end
  
end

return
