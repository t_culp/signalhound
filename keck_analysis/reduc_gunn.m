function reduc_gunn(tag,freq)
% reduc_gunn(tag,freq)
%
% Attempt Kuo style analysis of Gunn data
%
% e.g.
%
% 100GHz
% reduc_gunn('070209a',100)
%
% 150GHz high amp
% reduc_gunn('070210a',150)
%
% 150GHz low amp - went the wrong way - this data useless...
% reduc_gunn('070214a',150)

% get the data
d=read_run(tag);
  
% find the 0.01Hz piece
b=find_blk(d.frame.features==2^12+2^6);
mapind=make_mapind(d,b);

% get the channels lists
[p,ind]=get_array_info(tag);

% plot data
figure(1)
subplot(2,1,1)
%plot(d.tf(mapind),d.lockin.adcData(mapind,ind.rgl100));
plot(d.tf,d.lockin.adcData(:,ind.rgl100));
subplot(2,1,2)
%plot(d.tf(mapind),d.lockin.adcData(mapind,ind.rgl150));
plot(d.tf,d.lockin.adcData(:,ind.rgl150));

samprate=length(d.lockin.azPos)/size(d.tracker.horiz_mount,1);
feat=cvec(repmat(d.frame.features',samprate,1));

% make ideal ref signal
ref=(double(d.lockin.spare(:,2))>0.2);

% strip to 0.01Hz bit
ref=ref&feat==2^12+2^6;

% make +/-0.5
ref=ref-0.5;

% find the transitions
r=diff(ref);

x1=find(r==+1)-4; % this gives 5 samples before the low->high transition

% disguard the first/last which are likely bogus
x1=x1(2:end-1);

% process each transition in turn
for i=1:length(x1)
%for i=1:2
  i

  s=x1(i); e=x1(i)+4999; %nominal half cycle period
  
  % get the data and append extra col of ref
  v=double([d.lockin.adcData(s:e,:),ref(s:e)]);

  % now flip and align to complete square wave period
  % this is Kiwon's idea not mine
  vp=-v;
  
  % move vp down such that mean of first 5 samples is zero
  vp=vp-repmat(mean(vp(1:5,:)),[size(v,1),1]);
  
  % move vp up such that mean of first 5 samples is same as mean of
  % last 5 samples of v
  vp=vp+repmat(mean(v(end-4:end,:)),[size(v,1),1]);

  % double up
  v=[v;vp]; r=[r-r];
  
  % remove mean
  v=v-repmat(mean(v),[size(v,1),1]);
  
  % rescale each channel such that swings +/-0.5
  %v=v./repmat(prctile(v,90)-prctile(v,10),[size(v,1),1]);
  v=v./(2*repmat(mean(v(4900:4999,:)),[size(v,1),1]));
  
  % store
  vs(:,:,i)=v;
  
end
  
% take ft of each channel
fv=fft(vs);
  
% throw away negative freq part
n=size(vs,1);
fv=fv(1:n/2+1,:,:);
    
% calc the freq axis
n=size(fv,1);
f=linspace(0,samprate/2,n)'; % 0 to Nyquist for sample rate
  
% the even harmonics are close to zero - disguard
f=f(2:2:end);
fv=fv(2:2:end,:,:);
  
% ratio is transfer functions
n=size(fv);
tf=fv./repmat(fv(:,end,:),[1,size(fv,2),1]);

% check that all ref ft are identical
if(sum(fv(:,end,:)~=repmat(fv(:,end,1),[1,1,size(fv,3)]))~=0)
  error('all refs fts not the identical')
end

% compare the mean of the ft over all available edges to ft of ref
tfm=mean(fv,3)./repmat(fv(:,end,1),[1,size(fv,2)]);

switch freq
  case 100
    chs=ind.rgl100
  case 150
    chs=ind.rgl150
end

% plot the individual transitions
if(0)
  figure(2); clf; setwinsize(gcf,1400,600);
  for j=1:length(chs)
  
    i=chs(j);
    
    subplot(1,4,1)
    ts=[1:size(vs,1)]*1/samprate;
    plot(ts,squeeze(vs(:,i,:)));
    hold on
    plot(ts,squeeze(vs(:,end,:)),'k');
    hold off
    xlim([-5,105]);
    ylim([-0.6,0.6]);
    title('time domain')
    xlabel('t (sec)')
    
    subplot(1,4,2)
    plot(f,squeeze(real(fv(:,i,:))));
    hold on;
    plot(f,squeeze(real(fv(:,end,:))),'k');
    hold off
    axis tight
    title('real part of ft')
    xlabel('freq (Hz)')
    
    subplot(1,4,3)
    semilogx(f,squeeze(abs(tf(:,i,:))));
    hold on
    %plot(f,abs(mean(tf(:,i,:),3)),'k');
    plot(f,abs(tfm(:,i)),'k');
    hold off
    xlim([0.01,20]);
    ylim([0,1.2]);
    title('transfer func amp')
    xlabel('freq (Hz)')
    
    subplot(1,4,4)
    semilogx(f,squeeze(unwrap(angle(tf(:,i,:))))*180/pi);
    hold on
    %plot(f,abs(mean(tf(:,i,:),3)),'k');
    plot(f,unwrap(angle(tfm(:,i)))*180/pi,'k');
    hold off
    xlim([0.01,20]);
    ylim([-400,20]);
    title('transfer func phase')
    xlabel('freq (Hz)')
    
    gtitle(p.channel_name{i});
    drawnow
    
    mkgif(sprintf('%3d/fig1_%02d.gif',freq,i));  
  
    fh=fopen(sprintf('%3d/fig1_%02d.html',freq,i),'w');
    
    fprintf(fh,'<a href="..">up</a> ------ ');
    fprintf(fh,'edges, ');
    fprintf(fh,'<a href="fig2_%02d.html">transfunc</a> ------ ',i);
    
    if(i~=chs(1))
      last=chs(j-1);
    else
      last=chs(end);
    end
    if(i~=chs(end))
      next=chs(j+1);
    else
      next=chs(1);
    end
    fprintf(fh,'<a href="fig1_%02d.html">last</a>, ',last);
    fprintf(fh,'<a href="fig1_%02d.html">next</a>\n',next);
    
    fprintf(fh,'<p><img src="fig1_%02d.gif">\n',i);
    
    fclose(fh);

  end
end

% plot the mean tf versus previously used models
if(0)
  [tc1,k]=ParameterRead('timeconst/060909_old.txt');
  tc1=tc1.timeconst;

  [tc2,k]=ParameterRead('timeconst/060909.txt');
  tc2=[tc2.tc1;tc2.tc2;tc2.weight];
  
  figure(3); clf; setwinsize(gcf,1000,600);
  for j=1:length(chs)
    
    i=chs(j);
    
    % setup butterworth electronics filter
    % this is bilinear transform of analog filter
    [bb,ab]=butter(6,20/50);
    
    % transform to second order section for combine below
    sosb=tf2sos(bb,ab);
    
    % single exp filter in analog domain
    ba1=[0,1];
    aa1=[tc1(1,i),1];
    
    % xform using bilinear to digital domain
    % - we must use blinear rather than impinvar as otherwise
    % the zero freq gain deviates from one for small tc
    [b1,a1]=bilinear(ba1,aa1,100,1);
    sos1=tf2sos(b1,a1);
    
    % combine filters
    [bc1,ac1]=sos2tf([sos1;sosb]);
    
    % calculate freq response
    [hc1,f2]=freqz(bc1,ac1,size(vs,1)/2+1,100);
    
    % setup dual first part
    ba21=[0,1];
    aa21=[tc2(1,i),1];
    [b21,a21]=bilinear(ba21,aa21,100,1);
    sos21=tf2sos(b21,a21);
    [bc21,ac21]=sos2tf([sos21;sosb]);
    [hc21,f2]=freqz(bc21,ac21,size(vs,1)/2+1,100);
    
    % if 2nd tc has weight greater than zero for this det
    if(tc2(3,i)>0)
      ba22=[0,1];
      aa22=[tc2(2,i),1];
      [b22,a22]=bilinear(ba22,aa22,100,1);
      sos22=tf2sos(b22,a22);
      [bc22,ac22]=sos2tf([sos22;sosb]);
      [hc22,f2]=freqz(bc22,ac22,size(vs,1)/2+1,100);
      
      % add the responses with approriate weight
      hc2=hc21*(1-tc2(3,i))+hc22*tc2(3,i);
    else
      hc2=hc21;
    end
    
    %f2=f2(2:2:end); h=h(2:2:end);
    
    subplot(1,2,1)
    semilogx(f,abs(tfm(:,i)),'k');
    hold on
    plot(f2,abs(hc1),'r')
    plot(f2,abs(hc2),'b')
    hold off
    xlim([0.01,20]);
    ylim([0,1.2]);
    title('transfer func amp')
    xlabel('freq (Hz)')
    legend('data',...
	sprintf('single tc model: %.3f',tc1(1,i)),...
	sprintf('dual tc model: %.3f, %.3f, %.3f',tc2(:,i)));
    
    subplot(1,2,2)
    semilogx(f,unwrap(angle(tfm(:,i)))*180/pi,'k');
    hold on
    plot(f2,phase(hc1)*180/pi,'r')
    plot(f2,phase(hc2)*180/pi,'b')
    hold off
    xlim([0.01,20]);
    ylim([-400,20]);
    title('transfer func phase')
    xlabel('freq (Hz)')
    
    gtitle(p.channel_name{i});  
    drawnow
    
    mkgif(sprintf('%3d/fig2_%02d.gif',freq,i));  
    
    fh=fopen(sprintf('%3d/fig2_%02d.html',freq,i),'w');
    
    fprintf(fh,'<a href="..">up</a> ------ ');
    fprintf(fh,'<a href="fig1_%02d.html">edges</a>, ',i);
    fprintf(fh,'transfunc ------ ');
    
    if(i~=chs(1))
      last=chs(j-1);
    else
      last=chs(end);
    end
    if(i~=chs(end))
      next=chs(j+1);
    else
      next=chs(1);
    end
    fprintf(fh,'<a href="fig2_%02d.html">last</a>, ',last);
    fprintf(fh,'<a href="fig2_%02d.html">next</a>\n',next);
    
    fprintf(fh,'<p><img src="fig2_%02d.gif">\n',i);
    
    fclose(fh);
  end
end

keyboard

% fit the observed trans func to model to get best fit pars
if(1)
  figure(4); clf; setwinsize(gcf,1000,600);
  for j=1:length(chs)
    
    i=chs(j);

    % need to limit which f points are used - certainly don't care
    % much how well we fit tfunc at high freq and in addition want a
    % fit which works well over a wide range of freq - not just at the
    % high freq where all the points are located for linear f scale

    % consider only a log progression sub sample of the points
    u=floor(logspace(0,log10(length(f)),100));
    % further limit to <10Hz
    u=u(f(u)<10);
    
    if(0)
      tc1(i)=matmin('chisq',[0.02],[],'dualexp_tfunc',tfm(u,i),[],f(u));
      free.lb=[0,0.1,0.03];
      free.ub=[0.1,10,1];
      free.free=[0,1,1];
      tc2(:,i)=matmin('chisq',[tc1(i),5,0.03],free,'dualexp_tfunc',tfm(u,i),[],f(u));
      free.free=[1,1,1];
      tc2(:,i)=matmin('chisq',tc2(:,i),free,'dualexp_tfunc',tfm(u,i),[],f(u));
    end
    
    if(0)
      tc1(i)=lsqnonlin(@(x) abs(tfm(:,i)-dualexp_tfunc(x,f)),0.02,0,0.1);
      tc2(1,i)=tc1(i);
      tc2(2:3,i)=lsqnonlin(@(x) abs(tfm(u,i)-dualexp_tfunc([tc1(i),x],f(u))),[1,0.2],[0.1,0.03],[10,1]);
      tc2(:,i)=lsqnonlin(@(x)   abs(tfm(u,i)-dualexp_tfunc(x,f(u))),tc2(:,i),[0,0.1,0.03],[0.1,10,1]);
    end

    % fit to tfunc amp only - seems to be the way to go - phase still
    % looks OK - 100-4-B still fails
    if(1)
      tc1(i)=lsqnonlin(@(x) abs(tfm(:,i)-dualexp_tfunc(x,f)),0.02,0,0.1);
      tc2(1,i)=tc1(i);
      tc2(2:3,i)=lsqnonlin(@(x) abs(tfm(u,i))-abs(dualexp_tfunc([tc1(i),x],f(u))),[1,0.2],[0.1,0.03],[10,1]);
      tc2(:,i)=lsqnonlin(@(x)   abs(tfm(u,i))-abs(dualexp_tfunc(x,f(u))),tc2(:,i),[0,0.1,0.03],[0.1,10,1]);
    end
    
    hc1=dualexp_tfunc(tc1(i),f);
    hc2=dualexp_tfunc(tc2(:,i),f);
    
    subplot(1,2,1)
    semilogx(f(u),abs(tfm(u,i)),'.k');
    hold on
    plot(f,abs(hc1),'r')
    plot(f,abs(hc2),'g')
    hold off
    %xlim([0.01,20]);
    ylim([0,1.2]);
    title('transfer func amp')
    xlabel('freq (Hz)')
    legend('data',...
	sprintf('single tc model: %.3f',tc1(i)),...
	sprintf('dual tc model: %.3f, %.3f, %.3f',tc2(:,i)));
    
    subplot(1,2,2)
    semilogx(f(u),unwrap(angle(tfm(u,i)))*180/pi,'.k');
    hold on
    plot(f,phase(hc1)*180/pi,'r')
    plot(f,phase(hc2)*180/pi,'g')
    hold off
    %xlim([0.01,20]);
    ylim([-400,20]);
    title('transfer func phase')
    xlabel('freq (Hz)')
    
    gtitle(p.channel_name{i});  
    drawnow

    pause
  end
end  

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compare some example filter responses

% butter
[bb,ab]=butter(6,20/50);
sosb=tf2sos(bb,ab);
[hb,f2]=freqz(bb,ab,5000,100);

% exp 20ms
ba21=[0,1];
aa21=[0.02,1];
[b21,a21]=bilinear(ba21,aa21,100,1);

% response of exp alone
[he,f2]=freqz(b21,a21,5000,100);

% exp combined with butter
sos21=tf2sos(b21,a21);
[bc21,ac21]=sos2tf([sos21;sosb]);
[hc21,f2]=freqz(bc21,ac21,5000,100);

% exp 500ms
ba22=[0,1];
aa22=[0.5,1];
[b22,a22]=bilinear(ba22,aa22,100,1);

% exp combined with butter
sos22=tf2sos(b22,a22);
[bc22,ac22]=sos2tf([sos22;sosb]);
[hc22,f2]=freqz(bc22,ac22,5000,100);

% add the responses with approriate weight
hc2=hc21*(1-0.3)+hc22*0.3;

figure(4); clf; setwinsize(gcf,1000,600);
subplot(1,2,1)
semilogx(f2,abs(hb),'b')
hold on
plot(f2,abs(he),'r')
plot(f2,abs(hc21),'g')
plot(f2,abs(hc2),'m')
hold off
xlim([0.01,20]);
ylim([0,1.2]);
title('transfer func amp')
xlabel('freq (Hz)')
legend({'Butterworth alone','20ms exp alone',...
    '20ms exp followed by Butterworth',...
    '20ms exp plus 30% 500ms exp followed by Butterworth'},'Location','South')

subplot(1,2,2)
semilogx(f2,phase(hb)*180/pi)
hold on
plot(f2,phase(he)*180/pi,'r')
plot(f2,phase(hc21)*180/pi,'g')
plot(f2,phase(hc2)*180/pi,'m')
hold off
xlim([0.01,20]);
ylim([-400,20]);
title('transfer func phase')
xlabel('freq (Hz)')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot response of analog and digital butterworth

f=linspace(0,pi,5000);

% analog Butterworth
[bba,aba]=butter(6,pi*20/50,'s');

% analog freq response
hba=freqs(bba,aba,f);

% digital Butterworth - this is bilinear transform of analog filter
[bbd,abd]=butter(6,20/50);

% digital freq response
hbd=freqz(bbd,abd,f);

f=f*50/pi;

% compare
clf; setwinsize(gcf,1000,600);
subplot(1,2,1)
semilogx(f,abs(hbd));
hold on
plot(f,abs(hba),'r');
hold off
xlim([0.01,20]);
ylim([0,1.2]);
legend({'digital Butterworth','analog Butterworth'},'Location','South');

subplot(1,2,2)
semilogx(f,unwrap(angle(hbd))*180/pi);
hold on
plot(f,unwrap(angle(hba))*180/pi,'r');
hold off
xlim([0.01,20]);
ylim([-400,20]);


% exp in analog and digital domains

f=linspace(0,pi,5000);

% analog exp
bea=[0,1];
aea=[0.02,1];

% analog freq response
hea=freqs(bea,aea,f*100); % need the x100...

% xform to digital
[bed,aed]=bilinear(bea,aea,100,1);

% digital freq response
hed=freqz(bed,aed,f);

f=f*50/pi;

% compare
clf; setwinsize(gcf,1000,600);
subplot(1,2,1)
semilogx(f,abs(hea));
hold on
plot(f,abs(hed),'r');
hold off
xlim([0.01,20]);
ylim([0,1.2]);
legend({'analog exp','digital exp'},'Location','South');

subplot(1,2,2)
semilogx(f,unwrap(angle(hea))*180/pi);
hold on
plot(f,unwrap(angle(hed))*180/pi,'r');
hold off
xlim([0.01,20]);
ylim([-400,20]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% we can also wonder if it is OK to combine exp and butterworth
% response as simple product of transfer functions...
%
% ...of course we can

f=linspace(0,pi,5000);

% analog exp
bea=[0,1];
aea=[0.02,1];

% analog freq response
hea=freqs(bea,aea,f*100); % need the x100...
    
% analog Butterworth
[bba,aba]=butter(6,pi*20/50,'s');

% analog freq response
hba=freqs(bba,aba,f);

f=f*50/pi;

% compare
clf; setwinsize(gcf,1000,600);
subplot(1,2,1)
semilogx(f,abs(hba));
hold on
plot(f,abs(hea),'r');
plot(f,abs(hba.*hea),'g');
hold off
xlim([0.01,20]);
ylim([0,1.2]);
legend({'analog butter','analog exp'},'Location','South');

subplot(1,2,2)
semilogx(f,unwrap(angle(hba))*180/pi);
hold on
plot(f,unwrap(angle(hea))*180/pi,'r');
plot(f,unwrap(angle(hba.*hea))*180/pi,'g');
hold off
xlim([0.01,20]);
ylim([-400,20]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compare simple all analog combined filter calc to the complicated
% way I was doing it...
%
% almost exactly the same result

% WAY I WAS DOING IT

% setup butterworth electronics filter
% this is bilinear transform of analog filter
[bb,ab]=butter(6,20/50);
  
% transform to second order section for combine below
sosb=tf2sos(bb,ab);

% single exp filter in analog domain
be=[0,1];
ae=[0.2,1];

% xform using bilinear to digital domain
% - we must use blinear rather than impinvar as otherwise
% the zero freq gain deviates from one for small tc
[b1,a1]=bilinear(be,ae,100,1);
sos1=tf2sos(b1,a1);

% combine filters
[bc1,ac1]=sos2tf([sos1;sosb]);

% calculate freq response
[hc1,f2]=freqz(bc1,ac1,5000,100);

% COMBINE DIGITAL FILTERS AS PRODUCT OF XFER FUNCS
[hbd,f2]=freqz(bb,ab,5000,100);
[hed,f2]=freqz(b1,a1,5000,100);
hc2=hed.*hbd;

% ALL ANALOG VERSION

f=linspace(0,pi,5000);

% analog freq response
hea=freqs(be,ae,f*100); % need the x100...
    
% analog Butterworth
[bba,aba]=butter(6,pi*20/50,'s');

% analog freq response
hba=freqs(bba,aba,f);

% combine (product of xfer functions)
hca=hea.*hba;

f=f*50/pi;

% compare
clf; setwinsize(gcf,1000,600);
subplot(1,2,1)
semilogx(f2,abs(hc1));
hold on
plot(f,abs(hca),'r');
plot(f2,abs(hc2),'g');
hold off
xlim([0.01,20]);
ylim([0,1.2]);
legend({'digital combined sos style','analog combined','digital combined as prod of tfs'},'Location','South');

subplot(1,2,2)
semilogx(f2,unwrap(angle(hc1))*180/pi);
hold on
plot(f,unwrap(angle(hca))*180/pi,'r');
plot(f2,unwrap(angle(hc2))*180/pi,'g');
hold off
xlim([0.01,20]);
ylim([-400,20]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Jamie now tells me there is a second butterworth
% 1 - 6 pole 20Hz
% 2 - 2 pole 30Hz

%f=linspace(0,50,5000);

[bb,ab]=butter(6,20/50);
[hb,f]=freqz(bb,ab,5000,100);

% butter 1
[bb1,ab1]=butter(6,pi*20/50,'s');
hb1=freqs(bb1,ab1,f*pi/50);

% butter 2
[bb2,ab2]=butter(2,pi*30/50,'s');
hb2=freqs(bb2,ab2,f*pi/50);

hc=hb1.*hb2;

figure(4); clf; setwinsize(gcf,1000,600);
subplot(1,2,1)
semilogx(f,abs(hb),'b')
hold on
%plot(f,abs(hb2),'r')
plot(f,abs(hc),'g')
hold off
xlim([0.01,20]);
ylim([0,1.2]);
title('transfer func amp')
xlabel('freq (Hz)')
legend({'digital 6 pole 20Hz','analog 6 pole 20Hz plus 2 pole 30Hz'},'Location','South')

subplot(1,2,2)
semilogx(f,phase(hb)*180/pi)
hold on
%plot(f,phase(hb2)*180/pi,'r')
plot(f,phase(hc)*180/pi,'g')
hold off
xlim([0.01,20]);
ylim([-400,20]);
title('transfer func phase')
xlabel('freq (Hz)')

