function reduc_plotscan_cor(tags,out,rx)
% reduc_plotscan_cor(tag,out,rx)
%
% Make covariance & correlation matrix plots for web post
%
% out = 0, print to screen
%     = 1, write plot to reducplots/ 
%
% rx = vector of requested receivers to plot (default all)
%
% e.g. reduc_plotscan_cor([get_tags('bicepcmb2007',2),get_tags('bicepcmb2008',2)])
%      reduc_plotscan_cor({'20100115d1'},1,[0,1])

% Based on reduc_plotscans.m - RWA 20110607

if(~exist('out','var'))
  out=0;
end

if(~exist('rx','var'))
  rx=[0,1,2,3,4,5];
end

if(out==1)
  set(0,'DefaultFigureVisible','off');
end


for j=1:length(tags)

% get calibrated tod
load(['data/',tags{j},'_tod']);

% Drop pathological half-scans
fs.sf=fs.sf(fs.sf>0); fs.ef=fs.ef(fs.ef>0);
fs.s =fs.s (fs.s>0);  fs.e =fs.e (fs.e>0);

% get nominal array info
[p,ind]=get_array_info(tags{j});

% massage ind to only include the requested rx
ind.rgl150=ind.rgl150(ismember(p.rx(ind.rgl150),rx));
ind.rgl150b=ind.rgl150b(ismember(p.rx(ind.rgl150b),rx));  

% mask the non-scan data
s.sf=[fs.sf;en.sf]; s.ef=[fs.ef;en.ef];
mapind=make_mapind(d,s);
d.mce0.data.fb(~mapind,:)=NaN;

% Cut out any half-scans with glitches
d.mce0.data.fb=cut_incomp_chanblk(d.mce0.data.fb,fs,ind.e);

% P3 filter data for clearer pixel-pixel correlation
d=filter_scans(d,fs,'p3',ind.e);
d=filter_scans(d,en,'p3',ind.e);

% do pair difference
dd=sumdiff_pairs(d,p,fsb,ind.a,ind.b);
dd=sumdiff_pairs(dd,p,en,ind.a,ind.b);

%close all 
if ishandle(1)
  set(0,'CurrentFigure',1);
else
  figure(1);
end

% plot scans
for i=1:length(fsb.s)
  clf

    mapind = [];
    for k=1:length(fs.sf)
        % Exclude half-scans for which more than 10% of channels are
        % nan'd
        fracnans=sum(isnan(sum(df.mce0.data.fb(fs.sf(k):fs.ef(k),:))))/528;
        if fracnans<0.10
            mapind=[mapind fs.sf(k):fs.ef(k)];
        end
    end

    fb=dd.mce0.data.fb(mapind,:);

    % correlation pair sum:
    subplot(2,2,1)
    temp=corrcoef(fb(:,ind.la));
    temp(isnan(temp))=-1.1;
    cm=jet;
    cm(1,:)=[1,1,1];
    imagesc(temp);
    caxis([-1.1 1])
    title('Correlation, pair sum')
    axis square
    colormap(cm)
    colorbar

    % correlation pair diff:
    subplot(2,2,2)
    temp=corrcoef(fb(:,ind.lb));
    temp(isnan(temp))=-1.1;
    imagesc(temp);
    caxis([-.2 .2])
    title('Correlation, pair diff')
    axis square   
    colormap(cm)
    colorbar

    % covariance pair sum:
    subplot(2,2,3)
    temp=cov(fb(:,ind.la));
    temp(isnan(temp))=min(min(temp))-1;
    imagesc(temp)
    caxis([-1 1])
    title('Covariance, pair sum')
    axis square
    colormap(cm)
    colorbar

    % covariance pair diff:
    subplot(2,2,4)
    temp=cov(fb(:,ind.lb));
    temp(isnan(temp))=min(min(temp))-1;
    imagesc(temp)
    caxis([-.2 .2])
    title('Covariance, pair diff')
    axis square
    colormap(cm)
    colorbar

     drawnow
  if(out==1)
% crop output image to get rid of whitespace on left and right
% make the image super wide
    if( numel(unique(p.rx)) > numel(rx) )
      rxext=['_rx',int2str(intersect(rx,p.rx))];
      rxext=rxext(rxext~=' ');
    else
      rxext='';
    end
    setwinsize(gcf,900,750);
    mkpng(sprintf('reducplots/cov_%s_%03d%s.png',tags{j},i,rxext),1);
    setpermissions(sprintf('reducplots/cov_%s_%03d%s.png',tags{j},i,rxext));
  else
    pause
  end
end
end


if(out==1)
  close(gcf)
end
set(0,'DefaultFigureVisible','on');

return
