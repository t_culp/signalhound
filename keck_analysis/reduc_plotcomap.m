function reduc_plotcomap(file,ukpervolt,c,pltype,pl)
% reduc_plotcomap(file,ukpervolt,c,pltype,pl)
%
% Plot coadded maps
%
% e.g. reduc_plotcomap('filtp3_weight2_jack0')
%      reduc_plotcomap('sim130014_filtn_weight2_jack0',[500e3,450e3],[150,15],[0,3,4])
%
% color stretch for temperature data is +/- c(1)
% color stretch for polarization data is +/- c(2)
%
% pltype is a vector specifying which plot types to display
%
% 1 - plot 7 panel T,Q,U and noise maps and corr for 1 freq (default)
% 2 - plot 6 panel T/Q/U 100/150 maps (currently unused)
% 3 - plot unweighted and weighted integration time
% 4 - plot 6 panel T/Q/U 100/150 noise maps 
% 5 - plot single 100 GHz giant map
% 6 - plot single 150 GHz giant map
% 7 - plot T and Tnoise side by side
% 8 - plot 100/150 T maps
% 9 - plot 100/150 FT mask
% 10- plot 100/150 FT quantity 
% 11- plot 100/150 Q maps
% 12- compare expected/obs noise etc
% 13- plot noise histograms
% 17- plot 150 GHz T map with polarization lines
% 18- plot weight and maxval diagnostics
%
% pl=1 plots to png

if(~exist('ukpervolt','var'))
  ukpervolt=[];
end
if(~exist('c','var'))
  c=[];
end
if(~exist('pltype','var'))
  pltype=[];
end
if(~exist('pl','var'))
  pl=0;
end

if(isempty(ukpervolt))
  ukpervolt=get_ukpervolt();
end
if(isempty(pltype))
  pltype=1;
end

if(any(ukpervolt==1))
  units= '(Volts)';
else
  units= '(\muK)';
end

nplots=length(pltype);
pp=1;

load(sprintf('maps/%s',file))
if ~exist('map','var') && exist('ac','var')
    map=make_map(ac,m,coaddopt);
end
%load(sprintf('movie/%s',file))

filename=file;
cut=strfind(file, '.mat');
if(isempty(cut))
  cut=length(file);
else
  cut=cut-1;
end
file=strrep(file,'_','\_');

% jackknife the maps
map=jackknife_map(map);

% cal the maps
map=cal_coadd_maps(map,ukpervolt);

% smooth the  maps
%map=smooth_maps(m,map,0.5);

% Inject gaussian shaped lumps into variance map to mask point
% sources
%map=mask_pntsrc(m,map,get_src([],mapopt));

i=1;

% if colorstretch not specified base on noise level
if(isempty(c))
  c1=80*min(sqrt(map(i).Tvar(:)));
  % coaddtype==3 only has diff map, not Q/U
  if isfield(map(i), 'Dvar')
    c2=80*min(sqrt(map(i).Dvar(:)));
  else
    c2=80*min(sqrt(map(i).Qvar(:)));
  end
else
  c1=c(1);
  c2=c(2);
end

% Plot 7 panel T,Q,U and noise maps and corr for 1 freq 
if(~isempty(intersect(pltype,1)))
  figure(pp); clf
  setwinsize(gcf,700,900)
  colormap jet
  
  subplot(4,2,1)
  plot_map(m,map(i).T,-c1,c1); title('T map');
  subplot(4,2,2)
  plot_map(m,sqrt(map(i).Tvar),0,c1); title('T noise map');
  
  subplot(4,2,3)
  plot_map(m,map(i).Q,-c2,c2); title('Q map');
  subplot(4,2,4)
  plot_map(m,sqrt(map(i).Qvar),0,c2); title('Q noise map');
  
  subplot(4,2,5)
  plot_map(m,map(i).U,-c2,c2); title('U map');
  subplot(4,2,6)
  plot_map(m,sqrt(map(i).Uvar),0,c2); title('U noise map');
  
  subplot(4,2,8)
  c=map(i).QUcovar./sqrt(map(i).Qvar.*map(i).Uvar);
  plot_map(m,c,-0.2,0.2);
  title('QU correlation map');
  
  if(i==1)
    gtitle(sprintf('%s 100GHz',file));
  else
    gtitle(sprintf('%s 150GHz',file));
  end
  
  printname=strcat('maps/',filename(1:cut),'_7panelTQU.png');
  if(pl) mkpng(printname); end

  pp=pp+1;

end

% plot 6 panel T/Q/U 100/150 maps 
if(~isempty(intersect(pltype,2)))
  figure(pp); clf
  setwinsize(gcf,900,700)
  %colormap jet
  subplot(3,2,1)
  plot_map(m,map(1,i).T,-c1,c1);title(sprintf('100GHz T %s', units))
  subplot(3,2,2)
  plot_map(m,map(2,i).T,-c1,c1);title(sprintf('150GHz T %s', units))
  subplot(3,2,3)
  plot_map(m,map(1,i).Q,-c2,c2);title(sprintf('100GHz Q %s', units))
  subplot(3,2,4)
  plot_map(m,map(2,i).Q,-c2,c2); title(sprintf('150GHz Q %s', units))
  subplot(3,2,5)
  plot_map(m,map(1,i).U,-c2,c2); title(sprintf('100GHz U %s', units))
  subplot(3,2,6)
  plot_map(m,map(2,i).U,-c2,c2); title(sprintf('150GHz U %s', units))
  gtitle(file);
  printname=strcat('maps/',filename(1:cut),'_multifig_100_150_sig.png');
  if(pl) mkpng(printname); end
  pp=pp+1;
end

% plot unweighted and weighted integration time
if(~isempty(intersect(pltype,3)))
  figure(pp); clf
   setwinsize(gcf,900,700)
  colormap jet
  subplot(2,2,1)
  plot_map(m,map(1).Titime,0, max(map(1).Titime(:))); title('Titime')
  subplot(2,2,2)
  plot_map(m,map(1).Twitime,0, max(map(1).Twitime(:))); title('Twitime')
  subplot(2,2,3)
  plot_map(m,map(1).Pitime,0, max(map(1).Pitime(:))); title('Pitime')
  subplot(2,2,4)
  plot_map(m,map(1).Pwitime,0, max(map(1).Pwitime(:))); title('Pwitime')
  
  printname=strcat('maps/',filename(1:cut),'_nhits.png');
  if(pl) mkpng(printname); end
  pp=pp+1;

end

% plot 6 panel T/Q/U 100/150 noise maps 
if(~isempty(intersect(pltype,4)))
  figure(pp); clf
  setwinsize(gcf,900,900)
  %colormap jet
  subplot(3,2,1)
  plot_map(m,sqrt(map(1).Tvar),0,c1); title('100GHz T noise map')
  subplot(3,2,2)
  plot_map(m,sqrt(map(2).Tvar),0,c1); title('150GHz T noise map')
  subplot(3,2,3)
  plot_map(m,sqrt(map(1).Qvar),0,c2); title('100GHz Q noise map')
  subplot(3,2,4)
  plot_map(m,sqrt(map(2).Qvar),0,c2); title('150GHz Q noise map')
  subplot(3,2,5)
  plot_map(m,sqrt(map(1).Uvar),0,c2); title('100GHz U noise map')
  subplot(3,2,6)
  plot_map(m,sqrt(map(2).Uvar),0,c2); title('150GHz U noise map')
  printname=strcat('maps/',filename(1:cut),'_multifig_100_150_var.png');
  if(pl) mkpng(printname); end
  pp=pp+1;

end

% plot single giant map
if(~isempty(intersect(pltype,5)))
  figure(pp); 
  %colormap gray
  setwinsize(gcf,1200,700)
  clf
  plot_map(m,map(1).T,-c1,c1);
  if strcmp(get_experiment_name(),'bicep2')
    title(sprintf('150GHz T %s', units));
  else
    title(sprintf('100GHz T %s', units));
  end
  gtitle(file,0.85);
  printname=strcat('maps/',filename(1:cut),'_100GHz.png');
  if(pl) mkpng(printname); end
  pp=pp+1;

end

% plot single 150 GHz giant map
if(~isempty(intersect(pltype,6)))
  figure(pp); 
  %colormap gray
  setwinsize(gcf,1200,700)
  clf
  plot_map(m,map(2).T,-c1,c1); title(sprintf('150GHz T %s', units));
  printname=strcat('maps/',filename(1:cut),'_150GHz.png');
  gtitle(file);
  if(pl) mkpng(printname);end
  pp=pp+1;
end


% plot T and Tnoise side by side
if(~isempty(intersect(pltype,7)))
  figure(pp); 
   i=1;
  colormap jet
  setwinsize(gcf,800,300)
  subplot(1,2,1)
  plot_map(m,map(i).T,-c1,c1); title('100GHz T map');
  subplot(1,2,2)
  plot_map(m,sqrt(map(i).Tvar),0,c1); title('100GHz T noise map');
  pp=pp+1;
   
end

% plot 100/150 T maps
if(~isempty(intersect(pltype,8)))
  figure(pp); clf; setwinsize(gcf,1500,600); 
  colormap gray
  %colormap(flipud(jet))
  subplot(1,2,1)
  plot_map(m,map(1).T,-c1,c1); title('100GHz T map')
  subplot(1,2,2)
  plot_map(m,map(2).T,-c1,c1); title('150GHz T map')
  %gtitle(file)
  pp=pp+1;

end

% plot FT mask
if(~isempty(intersect(pltype,9)))
  figure(pp); clf; setwinsize(gcf,900,700);
  colormap gray
  %colormap(flipud(jet))
  subplot(2,2,1)
  plot_map(m,1./map(1).Tvar,0,max(1./map(1).Tvar(:))); title('T mask')
  subplot(2,2,3)
  plot_map(m,1./map(1).Qvar,0,max(1./map(1).Qvar(:))); title('Q mask')
  subplot(2,2,4)
  plot_map(m,1./map(1).Uvar,0,max(1./map(1).Uvar(:))); title('U mask')
  gtitle(file)
  pp=pp+1;

end

% plot 100/150 FT quantity
if(~isempty(intersect(pltype,10)))
  figure(pp); clf; setwinsize(gcf,1500,600);
  pp=pp+1;
  colormap gray
  %colormap(flipud(jet))
  subplot(1,2,1)
  x=map(1).T./map(1).Tvar./max(1./map(1).Tvar(:)); x(isnan(x))=0;
  plot_map(m,x,-c1,c1); title('100GHz T map/var')
  subplot(1,2,2)
  x=map(2).T./map(2).Tvar./max(1./map(1).Tvar(:)); x(isnan(x))=0;
  plot_map(m,x,-c1,c1); title('150GHz T map/var')
  %gtitle(file)
end

% plot 100 Q maps
if(~isempty(intersect(pltype,11)))
  figure(pp); clf
  setwinsize(gcf,1200,700)
  colormap jet
  plot_map(m,map(1).Q,-c2,c2); title('100GHz Q map')
  gtitle(file);
  printname=strcat('maps/',filename(1:cut),'_100_Q.png');
  if(pl) mkpng(printname); end
  pp=pp+1;
end

%plot giant 150Q map
if(~isempty(intersect(pltype,14)))
  figure(pp); clf
  setwinsize(gcf,1200,700)
  colormap jet
   plot_map(m,map(2).Q,-c2,c2); title('150GHz Q map')
  gtitle(file);
  printname=strcat('maps/',filename(1:end-4),'_150_Q.png');
  if(pl) mkpng(printname); end
  pp=pp+1;
end

% plot 100 U maps
if(~isempty(intersect(pltype,15)))
  figure(pp); clf
  setwinsize(gcf,1200,700)
  colormap jet
  plot_map(m,map(1).U,-c2,c2); title('100GHz U map')
  gtitle(file);
  printname=strcat('maps/',filename(1:cut),'_100_U.png');
  if(pl) mkpng(printname); end
  pp=pp+1;
end

%plot giant 150U map
if(~isempty(intersect(pltype,16)))
  figure(pp); clf
  setwinsize(gcf,1200,700)
  colormap jet
   plot_map(m,map(2).U,-c2,c2); title('150GHz U map')
  gtitle(file);
  printname=strcat('maps/',filename(1:cut),'_150_U.png');
  if(pl) mkpng(printname); end
  pp=pp+1;
end

% compare expected/obs noise etc
if(~isempty(intersect(pltype,12)))
  figure(pp); clf
  pp=pp+1;
  setwinsize(gcf,450,500)
  
  subplot(2,2,1)
  [bincenter,v]=rms_bands(sqrt(map(i).Tvar),map(i).T.^2,20,0,c1);
  plot(bincenter,sqrt(v),'*'); grid
  xlim([0,c1*1.2]); ylim([0,c1*1.2]); axis square
  hold on; plot([0,c1*1.2],[0,c1*1.2],'r'); hold off
  xlabel('Expected map noise'); ylabel('Observed map noise');
  title('T')
  
  subplot(2,2,2)
  [bincenter,v]=rms_bands(sqrt(map(i).Qvar),map(i).Q.^2,20,0,c2);
  plot(bincenter,sqrt(v),'*'); grid
  xlim([0,c2*1.2]); ylim([0,c2*1.2]); axis square
  hold on; plot([0,c2*1.2],[0,c2*1.2],'r'); hold off
  xlabel('Expected map noise'); ylabel('Observed map noise');
  title('Q')
  
  subplot(2,2,3)
  [bincenter,v]=rms_bands(sqrt(map(i).Uvar),map(i).U.^2,20,0,c2);
  plot(bincenter,sqrt(v),'*'); grid
  xlim([0,c2*1.2]); ylim([0,c2*1.2]); axis square
  hold on; plot([0,c2*1.2],[0,c2*1.2],'r'); hold off
  xlabel('Expected map noise'); ylabel('Observed map noise');
  title('U')
  
  subplot(2,2,4)
  [bincenter,v]=rms_bands(map(i).QUcovar,map(i).U.*map(i).Q,10,-c2.^2,c2.^2);
  plot(bincenter,v,'*'); grid
  c=(c2*1.2).^2;
  xlim([-c,c]); ylim([-c,c]);
  axis square
  hold on; plot([-c,c],[-c,c],'r'); hold off
  xlabel('Calc cov'); ylabel('Observed cov');
  title('QU')

  if(i==1)
    gtitle(sprintf('%s',file));
  else
    gtitle(sprintf('%s 150GHz',file))
  end
end

% plot noise histograms
if(~isempty(intersect(pltype,13)))
  % take each pixel val and normalize by expected rms at that map
  % location and histogram
  figure(pp); clf
  pp=pp+1;
  setwinsize(gcf,700,550)
  
  subplot(2,3,1)
  plot_hist(map(1).T./sqrt(map(1).Tvar)); title('T');
  subplot(2,3,2)
  plot_hist(map(1).Q./sqrt(map(1).Qvar)); title('Q');
  subplot(2,3,3)
  plot_hist(map(1).U./sqrt(map(1).Uvar)); title('U');
  
  if(length(map>1))
    subplot(2,3,4)
    plot_hist(map(2).T./sqrt(map(2).Tvar)); title('150GHz T');
    subplot(2,3,5)
    plot_hist(map(2).Q./sqrt(map(2).Qvar)); title('150GHz Q');
    subplot(2,3,6)
    plot_hist(map(2).U./sqrt(map(2).Uvar)); title('150GHz U');
  end
  
  gtitle(file);
end
  
% Plot T map with polarization lines on top
if(~isempty(intersect(pltype,17)))
  figure(pp)
  setwinsize(gcf,1500,1000)
  colormap jet
  map=make_tqu_apmap(m,map,[],[50,300],[]);
  win=1./map(i).Qvar; win=win./prctile(win(:),90);
  map(i).Qap=map(i).Qap./win; map(i).Uap=map(i).Uap./win;
  plot_map(m,map(i).T,-c1,c1); title('T map with polarization');
  hold on
  grid off
  plot_pol(m,map(i).Qap,map(i).Uap,map(i).Qvar,map(i).Uvar,0.18,[],2)
  pp=pp+1;
end

% Plot diagnostic quantities accumulated during map making
if(~isempty(intersect(pltype,18)))
  figure(pp)
  setwinsize(gcf,1500,1000); clf
  colormap jet

  %coaddopt.cut.elnod_mean
  
  % concatenate the scansets
  hsmax=structcat(1,coaddopt.hsmax);
  
  % reduce to MMDD
  tlab=coaddopt.tags';
  for i=1:length(tlab)
    tlab{i}=tlab{i}(:,5:8);
  end
  
  % make x-axis labels
  n=size(tlab,1);
  nl=min([n,20]);
  xt=linspace(1,n,nl); xl=tlab(round(xt));
  
  subplot(3,3,[1,2])
  imagesc(hsmax.d'); colorbar
  set(gca,'XTick',xt); set(gca,'XTickLabel',xl);
  xlabel('scanset number'); title('scanset max deviation (value/sigma-half-scan)');
  ylabel('channel number')
  
  [p,ind]=get_array_info(coaddopt.tags{1}(1:8));
  
  subplot(3,3,[4,5])
  imagesc(hsmax.w(:,ind.la)'); colorbar
  set(gca,'XTick',xt); set(gca,'XTickLabel',xl);
  xlabel('scanset number'); title('scanset max sum weight');
  ylabel('pair number')
  %caxis([0,10]);  
  
  subplot(3,3,[7,8])
  imagesc(hsmax.w(:,ind.lb)'); colorbar
  set(gca,'XTick',xt); set(gca,'XTickLabel',xl);
  xlabel('scanset number'); title('scanset max diff weight');
  ylabel('pair number')
  %caxis([0,40]);  
    
  subplot(3,6,5)
  hplot(coaddopt.devhist.sum.bc,coaddopt.devhist.sum.n,'L');
  yl=ylim; ylim([1e0,yl(2)]);
  title('pair sum deviations');
  hold on
  x=coaddopt.devhist.sum.bc; mv=max(coaddopt.devhist.sum.n);
  plot(x,gauss([mv,0,1],x),'r');
  hold off
  subplot(3,6,6)
  hplot(coaddopt.devhist.dif.bc,coaddopt.devhist.dif.n,'L');
  ylim([1e0,yl(2)]);
  title('pair diff deviations');
  hold on
  x=coaddopt.devhist.dif.bc; mv=max(coaddopt.devhist.dif.n);
  plot(x,gauss([mv,0,1],x),'r');
  hold off
  
  subplot(3,6,11)
  hplot(coaddopt.whist.sum.bc,coaddopt.whist.sum.n);
  xlim([0,10])
  title('pair sum weights (per halfscan)');
  subplot(3,6,12)
  hplot(coaddopt.whist.sum.bc,coaddopt.whist.sum.n,'L');
  title('pair sum weights (per halfscan)');
  
  subplot(3,6,17)
  hplot(coaddopt.whist.dif.bc,coaddopt.whist.dif.n);
  title('pair diff weights (per halfscan)');
  xlim([0,50])
  subplot(3,6,18)
  hplot(coaddopt.whist.dif.bc,coaddopt.whist.dif.n,'L');
  title('pair diff weights (per halfscan)');
  
  gtitle(sprintf('Diagnostic stats on tod as actually binned into map %s',file));
  
  printname=strcat('maps/',filename(1:cut),'_diagnostics.png');
  if(pl) mkpng(printname); end

  pp=pp+1;
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_map(m,map,low,high)
imagescnan(m.x_tic,m.y_tic,map);
grid on;
axis xy; set(gca,'XDir','reverse');
set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
caxis([low,high]);
colorbar
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [bc,z]=rms_bands(x,y,nbin,low,high)
% calc spread in bands
[bc,n]=hfill(x,nbin,low,high);
[bc,sumsq]=hfill(x,nbin,low,high,y);
z=sumsq./n;
return

%%%%%%%%%%%%%%%%%%%%%
function plot_hist(x)
[bc,n]=hfill(x,50,-5,5);
p=hfitgauss(bc,n); xlabel(sprintf('width=%f',p(3)));
axis square
return
