% white figures
colordef white
set(0,'DefaultFigureColor','white');

% Make the figure at top left and square
p=get(0,'DefaultFigurePosition');
set(0,'DefaultFigurePosition',[10,p(2),p(4),p(4)]);
clear p;

% Prefer grayscale colormap
%set(0,'DefaultFigureColormap',gray);
% For some wacked reason the above pops up a figure
close all;

% Prefer WISYWIG style when make eps files
% - set to 'manual' for fit to "paper size" style
set(0,'DefaultFigurePaperPositionMode','auto');

% If error in function drop to keyboard prompt
dbstop if error

% add paths to sub-dirs
if ~isdeployed
  [r,h]=unix('pwd');
  h=h(1:end-1);

  addpath([h,'/beamcen']);
  addpath([h,'/beammap']);
  addpath([h,'/chflags']);
  addpath([h,'/util']);
  addpath([h,'/healpix']);
  addpath([h,'/read_dir']);
  addpath([h,'/external_lib']);
  addpath([h,'/kendrick']);
  addpath([h,'/matrix']);
  addpath([h,'/squid_tuning']);
  addpath([h,'/simruncode']);
  addpath([h,'/multicomp']);
  
  % add paths to libraries
  [r,h]=unix('printenv HOME');
  h=h(1:end-1);
  
  addpath([h,'/tes_analysis']);
  tes_analysis_setup
  
  clear r home
end
