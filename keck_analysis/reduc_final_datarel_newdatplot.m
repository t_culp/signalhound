function sp=reduc_final_datarel_newdatplot(rf)
% sp=reduc_final_datarel_newdatplot(rf)
%
% Read back newdat files to check
%
% e.g.
% reduc_final_datarel_newdatplot('0304real_filtp3_weight3_gs_jack0_debias')

% open the file
fh=fopen(sprintf('newdat/%s.newdat',rf),'r');

% get the window file base name from first line of file
% (normally same as the name of the file)
x=fgetl(fh);
wfbn=sscanf(x,'%s');

% get the number of bins
x=fgetl(fh);
nbins=sscanf(x,'%f',6);

% for the moment we ignore TB,EB
nbins=nbins(nbins~=0);

% read "BAND_SELECTION" line
x=fgetl(fh);
x=sscanf(x,'%s',1);
if(~strcmp(x,'BAND_SELECTION'))
  error('next line should be BAND_SELECTION')
end

% read the band selections
for i=1:6
  x=fgetl(fh);
  x=sscanf(x,'%f',2); binl(i)=x(1); binh(i)=x(2);
end

% read the abs cal line
x=fgetl(fh);
x=sscanf(x,'%f',3);
recalswitch=x(1); recalfac=x(2); abscaluncer=x(3);

% read the beam size/uncer line
x=fgetl(fh);
x=sscanf(x,'%f',3);
beamswitch=x(1); beamsize=x(2); beamuncer=x(3);

% read liketype
x=fgetl(fh);
liketype=sscanf(x,'%f',1);

% for each spectrum
ss={'TT','EE','BB','EB','TE','TB'}

for i=1:6
  
  % check says TT, EE etc
  x=fgetl(fh);
  spectag=sscanf(x,'%s',1);
  if(~strcmp(spectag,ss{i}))
    error('failed to read expected spec tag')
  end
  
  % read lines giving bandpower data
  for j=1:nbins(i)
    x=fgetl(fh);
    x=sscanf(x,'%f');
    sp.bn{i}(j)=x(1);
    sp.Cl{i}(j)=x(2);
    sp.Clel{i}(j)=x(3);
    sp.Cleu{i}(j)=x(4);
    sp.xfac{i}(j)=x(5);
    sp.ll{i}(j)=x(6);
    sp.lu{i}(j)=x(7);
    sp.lm{i}(j)=mean(x(6:7));
    if(length(x)>7)
      sp.ltyp{i}(j)=x(8);
    else
      sp.ltyp{i}(j)=0;
    end
  end
  
  % now scan the corr matrix
  sp.bpcorrm{i}=fscanf(fh,'%f',[nbins(i),nbins(i)]);
  
  % seems necessary to skip to next line
  x=fgetl(fh);
end

% read the all-with-all covmat
a=sum(nbins);
sp.bpcm=fscanf(fh,'%f',[a,a]);

fclose(fh);

% now read the bpwf's
for i=1:sum(nbins)
  x=textread(sprintf('newdat/windows/%s%d',wfbn,i));
  sp.bpwf{i}.l=x(:,1); sp.bpwf{i}.Cl=x(:,2:end);
end

% now read the beam errors as posted by Cynthia for B1 - there was no
% slot for this in newdat file at the time although Michael Brown
% added one for QUaD final release so we should use that for B2 etc.
beamuncer=textread('newdat/BICEP_20090618_beam_err.txt','','commentstyle','shell');

if(nargout==0)
  % plot the spectra
  figure(1); clf; setwinsize(gcf,700,700);
  for i=1:6
    subplot(3,2,i);
    errorbar(sp.lm{i},sp.Cl{i},sp.Clel{i},sp.Cleu{i},'.');
    line([sp.ll{i};sp.lu{i}],[sp.Cl{i};sp.Cl{i}],'color','b');
    
    % plot the x-factors
    hold on
    plot(sp.lm{i},sp.xfac{i},'r');
    
    hold off
    xlim([0 400])
    title(ss{i});
  end

  % plot the corr mat
  figure(2); clf; setwinsize(gcf,700,700);
  for i=1:6
    subplot(3,2,i);
    imagesc(sp.bpcorrm{i}); colorbar
    title(ss{i});
  end
  
  % plot the cov mat
  figure(3); clf; setwinsize(gcf,700,700);
  imagesc(log10(abs(sp.bpcm))); colorbar
  axis square
  
  % check the cov mat diag agrees with stated errors
  figure(4); clf;
  plot([sp.Clel{:}]./sqrt(diag(sp.bpcm))','.-');

  % plot the bpwf
  figure(5); clf; setwinsize(gcf,700,700);
  l=0;
  for i=1:6
    % find first and last bins of this spectrum
    s=l+1; l=s+nbins(i)-1;
    
    [s l]
    
    subplot(3,2,i); hold on; box on
    %set(gca,'YScale','log');
    
    % for each bin in this spectrum
    for j=s:l
      plot(sp.bpwf{j}.l,sp.bpwf{j}.Cl);
      bpwfs(j)=sum(sp.bpwf{j}.Cl(:));
    end
    
    xlim([0 2000])
    title(ss{i})
    legend({'TT','TE','EE','BB'})
  end

  keyboard
  
  % check the total weight 
  figure(6)
  plot(bpwfs);
end

% get a standard model
sp.inpmod=load_cmbfast('aux_data/official_cl/camb_wmap5yr_noB.fits');

% calc the expv using the bpwf
l=0;
for i=1:6
  % find first and last bins of this spectrum
  s=l+1; l=s+nbins(i)-1;
  
  for j=s:l
    n=size(sp.bpwf{j}.Cl,1);
    sp.expv{i}(j-s+1)=sum(sum(sp.bpwf{j}.Cl.*sp.inpmod.Cs_l(1:n,1:4)));
  end
end

% calc the chi2 of each spectra
l=0;
for i=1:6
  % find first and last bins of this spectrum
  s=l+1; l=s+nbins(i)-1;
  % fetch data, model and cov mat
  d=sp.Cl{i}; m=sp.expv{i}; c=sp.bpcm(s:l,s:l); x=sp.xfac{i};
  
  % force cov mat diagonal - low stats sets give crazy chi2 values when
  % include off diags
  %c=diag(diag(c));
    
  % add abs cal uncertainty to cov mat
  switch i
    case 1
      % Chiang et al Sec 9.2 gives different values for TT, TE/TB
      % and EE/BB/EB - but newdat file has slot for only one value so
      % Cynthia fills that with latter value
      c=c+0.040^2*(cvec(m)*rvec(m));
    case {5,6}
      c=c+0.045^2*(cvec(m)*rvec(m));
    otherwise
      c=c+abscaluncer^2*(cvec(m)*rvec(m));
  end
  
  % add beam uncertainty
  s=beamuncer(:,3);
  c=c+(cvec(s)*rvec(s)).*(cvec(m)*rvec(m));
  
  % invert cov mat
  c=inv(c);
  
  % perform offset log-normal trans
  if(ismember(i,[1,2,3]))
    c=c.*(cvec(d+x)*rvec(d+x));
    d=log(d+x);
    m=log(m+x);
  end
  
  % take deviations between data and model
  r=d-m;
  
  % calc chi2
  sp.chi2(i)=r*c*r';
end

return



% calc the TT/EE/BB/TE chi2
d=[Cl{:}]; m=expv; r=d-m;
chi2(5)=r*inv(bpcm)*r'

% calc the EE/BB/TE chi2
i=24:92;
d=[Cl{2:4}]; m=expv(i); r=d-m;
chi2(6)=r*inv(bpcm(i,i))*r'

% calc the EE/BB chi2
i=24:69;
d=[Cl{2:3}]; m=expv(i); r=d-m;
chi2(7)=r*inv(bpcm(i,i))*r'

ndf=[23,23,23,23,92,69,46]

1-chi2cdf(chi2,ndf)
end
