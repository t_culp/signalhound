function reduc_final_comp(r1f,r2f,lmax,dia)
% reduc_final_comp(r1f,r2f,lmax, dia)
%
% compare two sets of final spectra
%
% reduc_final_comp('0750/real_a_filtp3_weight3_gs_dp1102_jack0_pureBold','0750/real_a_filtp3_weight3_gs_dp1102_jack0_pureB')


if(~exist('lmax','var'))
  lmax=500;
end
  
if(~exist('dia','var'))
  dia=0;
end

cut1=strfind(r1f, '_');
cut2=strfind(r2f, '_');
tit=sprintf('%s_%s',r1f(1:cut1(1)-1),r2f(1:cut2(1)-1))

% get first run
x=load(sprintf('final/%s',r1f));
r1=x.r;
% use inpmod from first run
inpmod=x.inpmod;

if  ~isempty(findstr(r2f,'cynthia'))
  r2=read_cynthia_spectra(r2f);
else
  
  % get second run
  x=load(sprintf('final/%s',r2f));
  r2=x.r;

  % strip down so both have same number of columns
  n=min([size(r1),size(r2)])
  if(n==1)
    r1=r1(1); r2=r2(1);
  end
  
  % strip down so both sets have same number of realizations
  n=min([size(r1(1).sim,3),size(r2(1).sim,3)])
  for i=1:size(r1);
    r1(i).sim=r1(i).sim(:,:,1:n);
  end
  for i=1:size(r2);
    r2(i).sim=r2(i).sim(:,:,1:n);
  end
  
  % if r2 is freq jack compare to 100/150/cross
  if(length(r1)==3&length(r2)==1)
    r2(2)=r2(1); r2(3)=r2(1);
  end
  
  % calc cov and derr
  r2=get_bpcov(r2);
  
  % diagonalize the bandpowers if wanted
  if(dia==1)
    r1=diag_bandpow(r1,2);
    r2=diag_bandpow(r2,2);
  end
  
end

% calc cov and derr
r1=get_bpcov(r1);

% assume jacktype of 1st spectra
n=strfind(r1f,'jack');
jacktype=str2num(r1f(n+4));

% process file names for printf
r1f=strrep(r1f,'_','\_');
r2f=strrep(r2f,'_','\_');


% plot the 2 sets of spectra
figure(1); clf
setwinsize(gcf,900,700)
if(length(r1)>1)
  plot_bars(1,r1(1),r2(1),inpmod,'Bicep2',jacktype,lmax);
  plot_bars(2,r1(2),r2(2),inpmod,'Keck',jacktype,lmax);
  plot_bars(3,r1(3),r2(3),inpmod,'cross',jacktype,lmax);
else
  plot_bars(1,r1(1),r2(1),inpmod,'Spectra',jacktype,lmax);
end
%gtitle(sprintf('%s (blue), %s (magenta)',r1f,r2f));
%printfig(1,sprintf('%s_jack%1d_spec_%01d.gif',tit,jacktype,lmax),'pp')

% plot the supression factors
figure(2); clf
setwinsize(gcf,900,700)
if(length(r1)>1)
  plot_supfac(1,r1(1),r2(1),'Bicep2',lmax);
  plot_supfac(2,r1(2),r2(2),'Keck',lmax);
  plot_supfac(3,r1(3),r2(3),'cross',lmax);
else
  plot_supfac(1,r1(1),r2(1),'Supfacs',lmax);
end
%gtitle(sprintf('%s (blue), %s (magenta)',r1f,r2f));

% plot the supression factor ratios
figure(3); clf
setwinsize(gcf,900,700)
if(length(r1)>1)
  plot_supfacr(1,r1(1),r2(1),'Bicep2',lmax);
  plot_supfacr(2,r1(2),r2(2),'Keck',lmax);
  plot_supfacr(3,r1(3),r2(3),'cross',lmax);
else
  plot_supfacr(1,r1(1),r2(1),'Supfac ratio',lmax);
end
%gtitle(sprintf('%s (blue), %s (magenta)',r1f,r2f));


return

% zero out the signal
if(length(r1)>1)
  r1(1).real(:)=0; r1(2).real(:)=0; r1(3).real(:)=0;
  r2(1).real(:)=0; r2(2).real(:)=0; r2(3).real(:)=0;
else
  r1(1).real(:)=0;
  r2(1).real(:)=0;
end
inpmod.Cs_l(:,:)=0;

% compare error bar sizes as error bars
figure(2); clf
setwinsize(gcf,900,700)
if(length(r1)>1)
  plot_bars(1,r1(1),r2(1),inpmod,'100GHz',1,lmax)
  plot_bars(2,r1(2),r2(2),inpmod,'150GHz',1,lmax)
  plot_bars(3,r1(3),r2(3),inpmod,'cross',1,lmax)
else
  plot_bars(1,r1(1),r2(1),inpmod,'freq jack',jacktype,lmax);
end
gtitle(sprintf('%s (blue), %s (magenta)',r1f,r2f));
%printfig(2,sprintf('%s_jack%1d_errorbar_%01d.gif',tit,jacktype,lmax),'pp')


% compare error bar sizes as ratios
figure(3); clf
setwinsize(gcf,900,700)
if(length(r1)>1)
  plot_ratio(1,r1(1),r2(1),inpmod,'100GHz',lmax)
  plot_ratio(2,r1(2),r2(2),inpmod,'150GHz',lmax)
  plot_ratio(3,r1(3),r2(3),inpmod,'cross',lmax)
else
  plot_ratio(1,r1(1),r2(1),inpmod,'freq jack',lmax)
end
gtitle(sprintf('%s diag error divided by %s',r2f,r1f));
%printfig(3,sprintf('%s_jack%1d_errorratio_%01d.gif',tit,jacktype,lmax),'pp')

return
% compare bp cov mats
figure(4); clf
setwinsize(gcf,900,700)
if(length(r1)>1)
  plot_covmat(1,r1(1),r2(1),'100GHz')
  plot_covmat(2,r1(2),r2(2),'150GHz')
  plot_covmat(3,r1(3),r2(3),'cross')
else
  plot_covmat(1,r1(1),r2(1),'freq jack')
end
gtitle(sprintf('%s vs %s',r1f,r2f));
%printfig(4,sprintf('%s_jack%1d_bpcov_%01d.gif',tit,jacktype_lmax),'pp')

return
% compare bp cov mat eigen values
chibins=get_chibins();
figure(5); clf
setwinsize(gcf,900,700)
if(length(r1)>1)
  plot_eig(1,r1(1),r2(1),'100GHz',chibins)
  plot_eig(2,r1(2),r2(2),'150GHz',chibins)
  plot_eig(3,r1(3),r2(3),'cross',chibins)
else
  plot_eig(1,r1(1),r2(1),'freq jack',chibins)
end
gtitle(sprintf('%s (blue), %s (red), y-axis log10 eigen vals',r1f,r2f));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_eig(n,c,e,freq,cb)

%yrange=[-100,100;-100,100;-100,100;-100,100;-100,100;-100,100];

nb=size(c.real,1);

lab={'TT','TE','EE','BB','TB','EB'};
for i=1:6
  subplot(6,3,(i-1)*3+n);
  plot(log10(eig(c.cov{i}(cb{i},cb{i}))),'b.-')
  hold on
  plot(log10(eig(e.cov{i}(cb{i},cb{i}))),'r.-')
  hold off
  grid
  axis tight
  ylabel(lab{i})
  if(i==1)
    title(freq);
  end
  %subplot_grid2(6,3,(i-1)*3+n);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_covmat(n,c,e,freq)

bl=2;
bu=10;

lab={'TT','TE','EE','BB','TB','EB'};
for i=1:6
  
  c1=corrcoef(squeeze(c.sim(:,i,:))');
  c2=corrcoef(squeeze(e.sim(:,i,:))');
  
  subplot(6,6,(i-1)*6+(n-1)*2+1)
  %imagesc(c.cov{i}(bl:bu,bl:bu));
  imagesc(sqrt(abs(c.cov{i}(bl:bu,bl:bu))));
  %imagesc(c1(bl:bu,bl:bu));
  ca=caxis;
  xlabel(sprintf('%.0e to %.0e',ca));
  ylabel(lab{i})
  axis xy; axis image

  if(i==1)
    title(sprintf('case 1 %s',freq));
  end
  
  subplot(6,6,(i-1)*6+(n-1)*2+2)
  imagesc(sqrt(abs(e.cov{i}(bl:bu,bl:bu))));
  %imagesc(c2(bl:bu,bl:bu));
  caxis(ca);
  xlabel(sprintf('%.0e to %.0e',ca));
  ylabel(lab{i})
  axis xy; axis image
  
  if(i==1)
    title(sprintf('case 2 %s',freq));
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_ratio(n,c,e,inpmod,freq,lmax)

%yrange=[-100,100;-100,100;-100,100;-100,100;-100,100;-100,100];

nb=size(c.real,1);

lab={'TT','TE','EE','BB','TB','EB'};
for i=1:6
  subplot_grid(6,3,(i-1)*3+n)
  %semilogy(c.l,sqrt(diag(e.cov{i}))./sqrt(diag(c.cov{i})),'b.-')
  plot(c.l,e.derr(:,i)./c.derr(:,i),'b.-')
  hold on
  %semilogy(c.l,c.derr(:,i)./e.derr(:,i),'r.-')
  %set(gca,'YScale','log');
  %set(gca,'ytick',[0.01,0.1,1])
  %ylim([0.0001,1.2]);
  xlim([0,lmax]);
  ylim([0 2])
  ylabel(lab{i})
  grid
  set(gca,'ytick',[0.5,1,1.5,2])
   if(i==1)
    title(freq);
  end
  subplot_grid2(6,3,(i-1)*3+n)
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_bars(n,c,e,inpmod,freq,jacktype,lmax)

if(jacktype==0)
  % ylim so we can see the signal spectra
  if (lmax == 500)
    yrange=[-500,8000;-150,150;-1,25;-.2,.3;-20,20;-1,1];
  elseif (lmax == 200)
    yrange=[-500,6000;-60,40;-.5,1.5;-.05,.05;-6,6;-.5,.5];
  end
else
  if (lmax == 500)
    yrange=[-5,5;-.2,.2;-.05,.05;-.05,.05;-.2,.2;-.05,.05];
  elseif (lmax == 200)
    yrange=[-5,5;-.1,.1;-.02,.02;-.02,.02;-.1,.1;-.02,.02];
  end
end
  
lab={'TT','TE','EE','BB','TB','EB'};
for i=1:6
  subplot_grid(6,3,(i-1)*3+n);
  errorbar2(c.l-5,c.real(:,i),c.derr(:,i),'b.');
  hold on;
  errorbar2(e.l+5,e.real(:,i),e.derr(:,i),'m.');
  plot(inpmod.l,inpmod.Cs_l(:,i),'r');
  hold off;
  ylim(yrange(i,:));
  xlim([0,lmax]);
  grid
  ylabel(lab{i})
  if(i==1)
    title(freq);
  end
  subplot_grid2(6,3,(i-1)*3+n);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_supfac(n,c,e,freq,lmax)
lab={'TT','TE','EE','BB','TB','EB'};
for i=1:6
  subplot_grid(6,3,(i-1)*3+n);
  plot(c.l,1./c.rwf(:,i));
  hold on
  plot(c.l,1./e.rwf(:,i),'m');
  hold off
  ylim([-0.1,1.1]);
  xlim([0,lmax]);
  grid
  ylabel(lab{i})
  if(i==1)
    title(freq);
  end
  subplot_grid2(6,3,(i-1)*3+n);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_supfacr(n,c,e,freq,lmax)
lab={'TT','TE','EE','BB','TB','EB'};
for i=1:6
  subplot_grid(6,3,(i-1)*3+n);
  plot(c.l,e.rwf(:,i)./c.rwf(:,i));
  ylim([0.5,1.5]);
  xlim([0,lmax]);
  grid
  ylabel(lab{i})
  if(i==1)
    title(freq);
  end
  subplot_grid2(6,3,(i-1)*3+n);
end

return
