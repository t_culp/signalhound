function pairmapcontam_chflag_driver(nbase,daughter)
% pairmapcontam_chflag_driver(nbase,daughter)
%
% Driver to wrap creation and analysis of maps to create the pairmap
% contamination parameter channel flag files.
%
% INPUTS
%   nbase       Serial number to use for the coadd. Corresponding (and
%               compatible) pairmaps must already exist under the 1351
%               serial number.
%
%   daughter    Daughter identifying standard full-season coadd.
%
% EXAMPLE
%   pairmapcontam_chflag_driver(1711,'e')
%

  if ~exist('nbase','var') || isempty(nbase)
    error('nbase is not optional')
  end
  if ischar(nbase)
    nbase = str2double(nbase);
  end

  if ~exist('daughter','var') || isempty(daughter)
    error('daughter is not optional')
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Setup all the inputs

  % Reuse pairmaps from the standard simset, chosen based on the experiment.
  expt = get_experiment_name();
  switch expt
    case 'keck'
      nreal = 1351;
    otherwise
      error('Unspported experiment ''%s''.', expt);
  end

  % Get needed values based on what year/coadd is being used.
  switch daughter
    case 'a'
      tags = get_tags('cmb2012');
    case 'b'
      tags = get_tags('cmb2013');
    case {'c','d'}
      tags = get_tags('cmb2014');
    case 'e'
      tags = get_tags('cmb2015');
    otherwise
      error('Unsupported daughter ''%s''.', daughter);
  end

  % Specify all the coaddopt options to generate what we want.
  clear coaddopt;
  coaddopt.sernum = sprintf('%04ireal', nbase);
  % Override pairmap location with nreal
  coaddopt.realpairmapset = sprintf('%04i', nreal);
  % Specific to pairmap contamination analysis:
  coaddopt.coaddtype = 3;
  coaddopt.jacktype = '02';
  % OK for special coadd, and saves space + save/load times.
  coaddopt.save_cuts = false;
  % Other standard options:
  coaddopt.daughter = daughter;
  coaddopt.filt = 'p3';
  coaddopt.gs = true;
  coaddopt.weight = 3;
  coaddopt.chflags = [];
  coaddopt.deproj = [0,0,0,0];
  coaddopt = get_default_coaddopt(coaddopt);
  % Override default cuts with proper for this year
  coaddopt.cut = get_default_round2_cuts([], tags{1}(1:4));

  % Farming options:
  submit = false;
  coaddmem = 20000;
  coaddcoaddsmem = 20000;
  coaddtime = 4*60;
  coaddcoaddstime = 30;
  queue = 'serial_requeue,itc_cluster';

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Generate per-deck coadds if necessary.

  % Parse all tags to create list of unique deck angles.
  [dates,phases,scans,dks,extra] = parse_tag(tags);
  udks = rvec(unique(dks));
  dkstr = arrayfun(@(dk) sprintf('dk%03i', dk), udks, 'uniformoutput',false);
  
  % Create a mask which chooses only common deck tags
  tagmask = false(numel(udks)+1, numel(tags));
  for ii=1:numel(udks)
    tagmask(ii,:) = dks == udks(ii);
  end

  % Now create coadd jobs if any maps are missing
  expectmaps = {};
  for ii=1:length(dkstr)
    coaddopt.daughter = [daughter '_' dkstr{ii}];
    tagz = tags(tagmask(ii,:));
    numsplits = ceil(length(tagz)/80);

    dkmap = get_map_filename(coaddopt);
    expectmaps = [expectmaps dkmap];
    if ~all(cellfun(@exist_file, dkmap))
      farm_coaddpairmaps(tagz, coaddopt, ...
          'OnlyMissing', true, ...
          'Realizations', [], ...
          'Queue', queue, ...
          'MemRequire', coaddmem, ...
          'maxtime', coaddtime, ...
          'SplitSubmission', numsplits, ...
          'UseCompiled', false, ...
          'submit', submit);
    end
  end
  if ~all(cellfun(@exist_file,expectmaps))
    fprintf(1,'Submit jobs by running babysitjobs(%04i,''wait10'')\n',nbase);
    disp('Rerun this script after coadd jobs have completed.');
    return
  end

  % Create an all-deck coadd by combining the per-deck coadds. This works
  % since deprojection (if turned on) happens over a phase which is at
  % constant deck angle. If that ever changes, though, this becomes
  % invalid.
  
  % Generate names for all the maps we expect to read.
  % First jack0...
  coaddopt.jacktype = '0';
  mapn0 = cellfun(@(dk) get_map_filename( ...
      setfield(coaddopt,'daughter',[daughter dk])), dkstr, ...
      'uniformoutput',false);
  mapa0 = get_map_filename(setfield(coaddopt,'daughter',[daughter '_dkall']));
  % ...then jack2.
  coaddopt.jacktype = '2';
  mapn2 = cellfun(@(dk) get_map_filename( ...
      setfield(coaddopt,'daughter',[daughter dk])), dkstr, ...
      'uniformoutput',false);
  mapa2 = get_map_filename(setfield(coaddopt,'daughter',[daughter '_dkall']));

  havedkall = true;
  sn = coaddopt.sernum;
  dall = [daughter 'dkall'];

  % Coadd dkall jack0 if necessary:
  if ~exist_file(mapa0)
    havedkall = false;
    [dum,tmpf,dim] = fileparts(mapa0);
    [farmfile,jobname] = farmfilejobname(sn,tmpf(6:end));
    farmit(farmfile, 'reduc_coadd_coadds(mapn0,sn,dall,false);', ...
        'jobname',jobname, ...
        'queue', queue, ...
        'overwrite', 'skip', ...
        'mem', coaddcoaddsmem, ...
        'maxtime', coaddcoaddstime, ...
        'submit', submit, ...
        'var',{'mapn0','sn','dall'});
  end

  % Coadd dkall jack2 if necessary:
  if ~exist_file(mapa2)
    havedkall = false;
    [dum,tmpf,dim] = fileparts(mapa2);
    [farmfile,jobname] = farmfilejobname(sn,tmpf(6:end));
    farmit(farmfile, 'reduc_coadd_coadds(mapn2,sn,dall,false);', ...
        'jobname',jobname, ...
        'queue', queue, ...
        'overwrite', 'skip', ...
        'mem', coaddcoaddsmem, ...
        'maxtime', coaddcoaddstime, ...
        'submit', submit, ...
        'var',{'mapn2','sn','dall'});
  end

  if ~havedkall
    fprintf(1,'Submit jobs by running babysitjobs(%04i,''wait10'')\n',nbase);
    disp('Rerun this script after coadd jobs have completed.');
    return
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Analyze coadds and produce the parameter files.
  coaddopt.daughter = daughter;
  reduc_pairmapcontam_chflag(coaddopt, tags);
end

