function stats=chflag_diagplots(usedate,chmap)
% stats=chflag_diagplots(usedate,chmap)
%
% Plots diagnostic histograms for channel flag cuts for the epoch determined
% by usedate. Plots are saved to ['chflags/' usedate].
%
% INPUTS
%   usedate    fp_data epoch in 'YYYYMMDD' format.
%
%   chmap      Optional. If supplied (file path or contents struct), the map
%              must be a per-detector map (coaddtype==4) and will be used to
%              identify the number of cuts simply due to empty maps (i.e.
%              non-responsive detectors still within the rgl list).
%
% OUTPUTS
%   stats      Structure of cut counts for each flag, for each frequency.
%
% EXAMPLE
%   stats = chflag_diagplots('20150101');
%

  % Date for loading current channel flags
  if ~exist('usedate','var') || isempty(usedate)
    usedate = '20150101';
  end
  if isnumeric(usedate)
    usedate = sprintf('%08d', usedate);
  end
  ndate = str2double(usedate);
  stats = struct();

  if ~exist('chmap','var')
    chmap = [];
  end
  if ~isempty(chmap) && ischar(chmap)
    fprintf(1, 'loading per-detector map %s...\n', chmap);
    chmap = load(chmap);
  end
  if ~isempty(chmap)
    % Sanity checks on the given map:
    %
    % 1. Must be per-detector coadd
    if chmap.coaddopt.coaddtype ~= 4
      error('chmap must be a per-detector (coaddtype 4) coadd');
    end
    % 2. Must have channel flags disabled
    if ~isempty(chmap.coaddopt.chflags)
      error('cannot use map that has channel flags enabled.')
    end
  end

  % Get version of p,ind which don't make use of channel flags
  [pp,indp] = get_array_info(usedate);

  % Then get statistics built from the flagged values when channel flags are
  % enabled.
  expt = get_experiment_name();
  chflags = get_default_chflags(expt, usedate(1:4));
  [p,ind] = get_array_info(usedate, [],[],[],[], chflags);
  % Convert p.flag to uint64 for convenience later
  p.flag = uint64(p.flag);

  % Enumerate the frequencies.
  freqs = unique(p.band(ind.la));
  fqstr = arrayfun(@(ff) sprintf('%03d', ff), freqs, 'uniformoutput',false);

  % Total number of light detectors as well as per-frequency counts
  numl    = length(indp.l);
  numlf   = cellfun(@(ff) length(indp.(['l' ff])), fqstr);
  numgl   = length(indp.gl);
  numglf  = cellfun(@(ff) length(indp.(['gl' ff])), fqstr);
  numrgl  = length(indp.rgl);
  numrglf = cellfun(@(ff) length(indp.(['rgl' ff])), fqstr);

  % To load the values manually, we need to know the fpu mappings.
  [fpus,rxs] = get_fp_names(usedate);

  % Store statistics information
  stats.date = usedate;
  stats.freqs = freqs;
  stats.num_l   = cvec(numlf);
  stats.num_gl  = cvec(numglf);
  stats.num_rgl = cvec(numrglf);
  stats.chflags = chflags.par_name;

  stats.cut_l_tot   = NaN * zeros(numel(freqs), numel(stats.chflags));
  stats.cut_l_unq   = stats.cut_l_tot;
  stats.cut_gl_tot  = stats.cut_l_tot;
  stats.cut_gl_unq  = stats.cut_l_tot;
  stats.cut_rgl_tot = stats.cut_l_tot;
  stats.cut_rgl_unq = stats.cut_l_tot;
  if ~isempty(chmap)
    stats.num_live = NaN * cvec(freqs);
    stats.cut_live_l_tot    = cell(numel(freqs), numel(stats.chflags));
    stats.cut_live_l_unq    = stats.cut_live_l_tot;
    stats.cut_live_gl_tot   = stats.cut_live_l_tot;
    stats.cut_live_gl_unq   = stats.cut_live_l_tot;
    stats.cut_live_rgl_tot  = stats.cut_live_l_tot;
    stats.cut_live_rgl_unq  = stats.cut_live_l_tot;
  end

  % For each of the channel flags:
  for ii=1:length(chflags.par_name)
    fprintf(1, 'inspecting %s...\n', chflags.par_name{ii})

    % Load the raw flag values (as is done in get_array_info()).
    rawvals = [];
    for jj=1:length(fpus)
      ffile = find_file_by_date(ndate, ...
          sprintf('aux_data/%s_%s', chflags.filebase{ii}, fpus{jj}));
      rawvals = [rawvals; ffile.(chflags.par_name{ii})];
    end

    % Get the flag value which identifies this cut
    flagv = conv_fpdata_flags(chflags.par_name(ii));
    % Filter the channel flags for those that have been marked by this cut
    filtf = bitand(p.flag, flagv) ~= 0;
    filtu = p.flag == flagv;

    % Calculate basic statistics of how many channels are being cut due to
    % this parameter.
    %
    % Total and unique cut counts for the entire array:
    numcutl = sum(filtf(indp.l));
    numunql = sum(filtu(indp.l));
    numcutgl = sum(filtf(indp.gl));
    numunqgl = sum(filtu(indp.gl));
    numcutrgl = sum(filtf(indp.rgl));
    numunqrgl = sum(filtu(indp.rgl));
    % Total and unique cut counts per frequency:
    numcutlf   = cellfun(@(ff) sum(filtf(indp.(['l' ff]))), fqstr);
    numunqlf   = cellfun(@(ff) sum(filtu(indp.(['l' ff]))), fqstr);
    numcutglf  = cellfun(@(ff) sum(filtf(indp.(['gl' ff]))), fqstr);
    numunqglf  = cellfun(@(ff) sum(filtu(indp.(['gl' ff]))), fqstr);
    numcutrglf = cellfun(@(ff) sum(filtf(indp.(['rgl' ff]))), fqstr);
    numunqrglf = cellfun(@(ff) sum(filtu(indp.(['rgl' ff]))), fqstr);

    % Sanity check!
    if sum(numcutlf) ~= numcutl
      error('sum(numcutfreq) == numcuttot assertion failed!');
    end
    if sum(numunqlf) ~= numunql
      error('sum(numcutfreq) == numcuttot assertion failed!');
    end

    % Store statistics
    stats.cut_l_tot(:,ii)   = cvec(numcutlf);
    stats.cut_l_unq(:,ii)   = cvec(numunqlf);
    stats.cut_gl_tot(:,ii)  = cvec(numcutglf);
    stats.cut_gl_unq(:,ii)  = cvec(numunqglf);
    stats.cut_rgl_tot(:,ii) = cvec(numcutrglf);
    stats.cut_rgl_unq(:,ii) = cvec(numunqrglf);

    % Calculate stats over only the detectors that produce a map
    if ~isempty(chmap)
      indab = [ind.a,ind.b];
      emap = false(numel(ind.e),1);
      % Identify an empty map by filling no entries in wsum
      emap(indab) = arrayfun(@(ac) ~any(rvec(ac.wsum)), chmap.ac);

      for ff=1:length(fqstr)
        % Count the number of actual live detectors.
        stats.num_live(ff) = sum(~emap(indp.(['l' fqstr{ff}])));

        % Combine unique and set flag masks with non-empty mask.
        rfiltf = find(filtf & ~emap);
        rfiltu = find(filtu & ~emap);
        % Shortcut per-frequency light and rgl lists **into the ideal case
        % with no channel cuts applied!!**
        l = indp.(['l' fqstr{ff}]);
        gl = indp.(['gl' fqstr{ff}]);
        rgl = indp.(['rgl' fqstr{ff}]);
        % For these, keep a complete list of detectors which fail the flag
        % since the caller may be interested in plotting these specific maps
        % to investigate.
        stats.cut_live_l_tot{ff,ii}   = intersect(rfiltf, l);
        stats.cut_live_l_unq{ff,ii}   = intersect(rfiltu, l);
        stats.cut_live_gl_tot{ff,ii}  = intersect(rfiltf, gl);
        stats.cut_live_gl_unq{ff,ii}  = intersect(rfiltu, gl);
        stats.cut_live_rgl_tot{ff,ii} = intersect(rfiltf, rgl);
        stats.cut_live_rgl_unq{ff,ii} = intersect(rfiltu, rgl);
      end
    end

    %%%%%%%%%%%%%%%%%%%%
    % PLOTTING SECTION %
    %%%%%%%%%%%%%%%%%%%%

    outdir = fullfile('chflags', usedate);
    if ~exist(outdir, 'dir')
      system(['mkdir -p ' outdir]);
    end
    clf(); setwinsize(gcf, 1000, 500);

    % Estimate a reasonable plot range.
    lo = min(chflags.low(:,ii));
    hi = max(chflags.high(:,ii));
    lb = lo - (hi-lo);
    ub = hi + (hi-lo);
    bins = linspace(lb, ub, 200);

    % Hack any infinity values to be at the upper bound so that they are
    % visually evident.
    rv = rawvals;
    rv(~isfinite(rv)) = ub - 0.0001;

    [xx,h1] = stairhist(rv(ind.l), bins, 'b');
    hold on
    % Make sure the y-limit isn't being scaled too high by the infinities in
    % the last bin.
    xlim([lb,ub])
    ylim([0, max(xx(1:end-2))*1.1])

    % Show the thresholds
    ylims = repmat(ylim(), size(chflags.low,1), 1)';
    xones = repmat([1,1],  size(chflags.low,1), 1)';
    hl = plot(xones.*[chflags.low(:,ii)  chflags.low(:,ii)]',  ylims, 'm--');
    hh = plot(xones.*[chflags.high(:,ii) chflags.high(:,ii)]', ylims, 'r--');

    title(sprintf('%s parameter values for entire array', ...
        chflags.par_name{ii}), 'interpreter','none');
    ylabel('counts')
    % Use xlabel for statistics
    N = numl;
    Nrgl = numrgl;
    xlabel({...
      sprintf(['cuts %0.2f%% (%0.2f%% uniquely) of all lights ' ...
               '- %d of %d lights pass'], ...
        100*numcutl/N, 100*numunql/N, N-numcutl, N), ...
      sprintf(['cuts %0.2f%% (%0.2f%% uniquely) of rgls ', ...
               '- %d of %d rgls pass'], ...
        100*numcutrgl/Nrgl, 100*numunqrgl/Nrgl, ...
        Nrgl-numcutrgl, Nrgl) ...
      });


    mkpng(fullfile(outdir, sprintf('%s_all.png', ...
        chflags.par_name{ii})))

    % Make a version of the histogram that emphasizes the detectors which are
    % being cut uniquely by this flag parameter.
    set(h1, 'color', [0.85, 0.85, 0.85]);
    lunq = intersect(find(filtu), ind.l);
    [dum,h2] = stairhist(rv(lunq), bins, 'b');
    title(sprintf('%s parameter values for entire array, uniquely cut', ...
        chflags.par_name{ii}), 'interpreter','none');

    mkpng(fullfile(outdir, sprintf('%s_all_unq.png', ...
        chflags.par_name{ii})))

    % Plot per-frequency histograms
    for ff=1:length(freqs)
      fs = fqstr{ff};
      delete(h1);
      delete(h2);
      delete(hl);
      delete(hh);

      [dum,h1] = stairhist(rv(ind.(['l' fs])), bins, 'b');
      title(sprintf('%s parameter values for %sGHz detectors', ...
          chflags.par_name{ii}, fs), 'interpreter','none');

      % Show just the threshold for this frequency.
      rx = min(size(chflags.low,1), p.rx(ind.(['l' fs])(1)) + 1);
      hl = plot([1,1].*chflags.low(rx,ii),  ylim(), 'm--');
      hh = plot([1,1].*chflags.high(rx,ii), ylim(), 'r--');

      N = numlf(ff);
      Nrgl = numrglf(ff);
      xlabel({...
        sprintf(['cuts %0.2f%% (%0.2f%% uniquely) of all lights ' ...
                 '- %d of %d lights pass'], ...
          100*numcutlf(ff)/N, 100*numunqlf(ff)/N, N-numcutlf(ff), N), ...
        sprintf(['cuts %0.2f%% (%0.2f%% uniquely) of rgls ', ...
                 '- %d of %d rgls pass'], ...
          100*numcutrglf(ff)/Nrgl, 100*numunqrglf(ff)/Nrgl, ...
          Nrgl-numcutrglf(ff), Nrgl) ...
        });

      mkpng(fullfile(outdir, sprintf('%s_%sGHz.png', ...
          chflags.par_name{ii}, fs)))

      set(h1, 'color', [0.85, 0.85, 0.85]);
      lunq = intersect(find(filtu), ind.(['l' fs]));
      [dum,h2] = stairhist(rv(lunq), bins, 'b');
      title(sprintf('%s parameter values for %sGHz detectors, uniquely cut',...
          chflags.par_name{ii}, fs), 'interpreter','none');

      mkpng(fullfile(outdir, sprintf('%s_%sGHz_unq.png', ...
          chflags.par_name{ii}, fs)))
    end % each in freqs

  end % each in chflags.par_name
end
