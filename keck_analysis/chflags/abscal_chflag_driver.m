function abscal_chflag_driver(nbase,daughter)
% abscal_chflag_driver(nbase,daughter)
%
% Driver to wrap creation and analysis of maps to create the abscal
% channel flag parameters.
%
% INPUTS
%   nbase       Serial number to use for the coadd. Corresponding (and
%               compatible) pairmaps must already exist under the 1351
%               serial number.
%
%   daughter    Daughter identifying standard full-season coadd.
%
% EXAMPLE
%   abscal_chflag_driver(1712,'e')
%

  if ~exist('nbase','var') || isempty(nbase)
    error('nbase is not optional')
  end
  if ischar(nbase)
    nbase = str2double(nbase);
  end

  if ~exist('daughter','var') || isempty(daughter)
    error('daughter is not optional')
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Setup all the inputs

  % Reuse pairmaps from the standard simset, chosen based on the experiment.
  expt = get_experiment_name();
  switch expt
    case 'keck'
      nreal = 1351;
    otherwise
      error('Unspported experiment ''%s''.', expt);
  end

  % Define all input maps. The calibration ('cal') external map should match
  % the BICEP map frequency as closely as possible, while the reference
  % ('ref') external map can be in principle anything that is not also cal.
  % In practice, we reuse Planck 143GHz for all ref except when it's used as
  % our 150GHz's cal.

  Pdir = 'input_maps/planck/planck_derivs_nopix';

  P100_bKuber100 = [Pdir '/HFI_SkyMap_100_2048_R1.10_nominal_nside0512_nlmax1280_bKuber100rev1.fits'];
  P100_bB2bbns   = [Pdir '/HFI_SkyMap_100_2048_R1.10_nominal_nside0512_nlmax1280_bB2bbns.fits'];

  P143_bKuber100 = [Pdir '/HFI_SkyMap_143_2048_R1.10_nominal_nside0512_nlmax1280_bKuber100rev1.fits'];
  P143_bB2bbns   = [Pdir '/HFI_SkyMap_143_2048_R1.10_nominal_nside0512_nlmax1280_bB2bbns.fits'];
  P143_bKuber220 = [Pdir '/HFI_SkyMap_143_2048_R1.10_nominal_nside0512_nlmax1280_bKuber220.fits'];

  P217_bKuber220 = [Pdir '/HFI_SkyMap_217_2048_R1.10_nominal_nside0512_nlmax1280_bKuber220.fits'];

  % Make alias for which sim type number refers which which map to make
  % accidents harder to make later.
  %   type 1 = ref map
  %   type 2 = cal map
  RLZREFSTR = '1';
  RLZCALSTR = '2';

  % Get needed values based on what year/coadd is being used.
  switch daughter
    case 'a'
      tags = get_tags('cmb2012');
      % Define appropriate external maps in frequency ascending order. The
      % calibration ('cal') external map should match the BICEP map frequency
      % as closely as possible, while the reference ('ref') external map can be
      % in principle anything that is not also cal.
      calmaps = {P143_bB2bbns};
      refmaps = {P100_bB2bbns};
      % For better description during plotting, a set of titles can be given
      % to reduc_abscal. titles will a per-frequency list of these titles.
      % Order is real, reference, calibration.
      titles = {{'Keck 2012', 'Planck 100', 'Planck 143'}};
    case 'b'
      tags = get_tags('cmb2013');
      calmaps = {P143_bB2bbns};
      refmaps = {P100_bB2bbns};
      titles = {{'Keck 2013', 'Planck 100', 'Planck 143'}};
    case {'c','d'}
      tags = get_tags('cmb2014');
      % Choose convention that ref is always P143 except for 150GHz which
      % uses P100 instead.
      calmaps = {P100_bKuber100, P143_bB2bbns};
      refmaps = {P143_bKuber100, P100_bB2bbns};
      titles = {...
        {'Keck 2014', 'Planck 143', 'Planck 100'},...
        {'Keck 2014', 'Planck 100', 'Planck 143'},...
      };
    case 'e'
      tags = get_tags('cmb2015');
      calmaps = {P100_bKuber100, P143_bB2bbns, P217_bKuber220};
      refmaps = {P143_bKuber100, P100_bB2bbns, P143_bKuber220};
      titles = {...
        {'Keck 2015', 'Planck 143', 'Planck 100'},...
        {'Keck 2015', 'Planck 100', 'Planck 143'},...
        {'Keck 2015', 'Planck 143', 'Planck 217'},...
      };
    otherwise
      error('Unsupported daughter ''%s''.', daughter);
  end

  [p,ind] = get_array_info(tags{1}(1:8));

  clear mapopt;
  mapopt.sernum = sprintf('%04ireal', nbase);
  mapopt.update = true;
  mapopt.weight = 3;
  % We need to make A/B maps. This doesn't do anything for real since we call
  % reduc_makeabmaps directly, but this is what gets reduc_makesim to pass
  % simulated TODs to reduc_makeabmaps instead of reduc_makepairmaps.
  mapopt.abmap = true;
  % Build abscals using observed beam centers.
  mapopt.beamcen = 'obs';
  % Disable deprojection for extra speed. We'll only care about T
  % cross-spectra, so deprojection has no effect.
  mapopt.deproj = false;
  % Pack ac to keep under v7 file size limit.
  mapopt.acpack = true;
  mapopt = get_default_mapopt(mapopt);

  clear coaddopt;
  coaddopt.sernum = mapopt.sernum;
  coaddopt.daughter = daughter;
  coaddopt.coaddtype = 4;
  coaddopt.weight = 3;
  % Only need jack0 maps for analysis.
  coaddopt.jacktype = '0';
  % Deprojection must match mapopt above.
  coaddopt.deproj = false;
  % Disable channel flags or else suffer a chicken/egg problem.
  coaddopt.chflags = [];
  % Other standard options:
  coaddopt.cut = get_default_round2_cuts([], tags{1}(1:4));
  coaddopt = get_default_coaddopt(coaddopt);

  clear simopt;
  simopt.update = true;
  simopt.mapopt = mapopt;
  % Make sure no noise is applied in external "reobservation".
  simopt.noise  = 'none';
  % Maintain CMB units of external input map.
  simopt.ukpervolt = 1;
  % Use same beam/pointing information as mapopt.
  simopt.beamcen = mapopt.beamcen;
  simopt.chi = mapopt.chi;
  simopt.epsilon = mapopt.epsilon;
  % Also use observed beam width as known by beam map analysis.
  simopt.beamwid = 'obs';
  % Don't inject extra differential pointing - beam centers will already
  % point where the physical detectors actually point.
  simopt.diffpoint = 'ideal';
  % Make sure TODs aren't saved and pairmaps made immediately.
  simopt.maketod = false;
  % The eps, chi, and gain dithers take per-frequency values, so make
  % sure there's the correct number of rows. These values do no dithering,
  % but are still required to be present.
  [p,ind] = get_array_info(tags{1}(1:8));
  nfreq = numel(unique(p.band(ind.la)));
  simopt.rndeps = repmat([0,0], nfreq, 1);
  simopt.rndchi = repmat([0,0], nfreq, 1);
  simopt.abgain = repmat([1,0], nfreq, 1);

  % Duplicate specific versions for cal and ref:
  %
  % Make calibration maps.
  mapoptcal = mapopt;
  mapoptcal.sernum = [mapopt.sernum(1:4) '000' RLZCALSTR];

  simoptcal = simopt;
  simoptcal.sernum = mapoptcal.sernum;
  simoptcal.rlz = 0;
  simoptcal.sigmapfilename = calmaps;
  simoptcal.mapopt = mapoptcal;

  coaddoptcal = coaddopt;
  coaddoptcal.sernum = mapoptcal.sernum;
  coaddoptcal.save_cuts = false;

  % Make reference maps.
  mapoptref = mapopt;
  mapoptref.sernum = [mapopt.sernum(1:4) '000' RLZREFSTR];

  simoptref = simopt;
  simoptref.sernum = mapoptref.sernum;
  simoptref.rlz = 0;
  simoptref.sigmapfilename = refmaps;
  simoptref.mapopt = mapoptref;

  coaddoptref = coaddopt;
  coaddoptref.sernum = mapoptref.sernum;
  coaddoptref.save_cuts = false;

  % Farming options
  maketagsperjob = 5;
  submit = false;
  makemem    = 18000;
  makesimmem = 18000;
  coaddmem       = 20000;
  coaddcoaddsmem = 40000;
  maketime    = 60;
  makesimtime = 1.5*60;
  coaddtime       = 30;
  coaddcoaddstime = 30;
  queue = 'serial_requeue,itc_cluster';

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Do work. First, make necessary pairmaps.

  % List all expected real pairmaps and then filter for non-existing ones.
  realpairmaps = get_pairmap_filename(tags, mapopt);
  calpairmaps  = get_pairmap_filename(tags, mapoptcal);
  refpairmaps  = get_pairmap_filename(tags, mapoptref);

  realexist = cellfun(@exist_file, realpairmaps);
  calexist  = cellfun(@exist_file, calpairmaps);
  refexist  = cellfun(@exist_file, refpairmaps);

  for ii=1:ceil(numel(tags)/maketagsperjob)
    lb = (ii-1)*maketagsperjob + 1;
    ub = min(ii*maketagsperjob, numel(tags));
    tag = tags(lb:ub);

    % Selectively make jobs for missing pairmap groups
    if ~all(rvec(realexist(lb:ub)))
      [dum,fname,dum] = fileparts(realpairmaps{lb});
      [farmfile,jobname] = farmfilejobname(mapopt.sernum, fname);
      farmit(farmfile, 'reduc_makeabmaps(tag,mapopt);', ...
          'jobname',jobname, ...
          'overwrite', 'skip', ...
          'queue', queue, ...
          'mem', makemem, ...
          'maxtime', maketime, ...
          'submit', submit, ...
          'var', {'tag','mapopt'});
    end

    % Not using runsim() here to avoid needing par_make_simrunfiles().

    if ~all(rvec(calexist(lb:ub)))
      [dum,fname,dum] = fileparts(calpairmaps{lb});
      [farmfile,jobname] = farmfilejobname(simoptcal.sernum, fname);
      farmit(farmfile, 'reduc_makesim(tag,simoptcal);', ...
          'jobname',jobname, ...
          'overwrite', 'skip', ...
          'queue', queue, ...
          'mem', makesimmem, ...
          'maxtime', makesimtime, ...
          'submit', submit, ...
          'var', {'tag','simoptcal'});
    end

    if ~all(rvec(refexist(lb:ub)))
      [dum,fname,dum] = fileparts(refpairmaps{lb});
      [farmfile,jobname] = farmfilejobname(simoptref.sernum, fname);
      farmit(farmfile, 'reduc_makesim(tag,simoptref);', ...
          'jobname',jobname, ...
          'overwrite', 'skip', ...
          'queue', queue, ...
          'mem', makesimmem, ...
          'maxtime', makesimtime, ...
          'submit', submit, ...
          'var', {'tag','simoptref'});
    end
  end

  % Fall through to coadd checks and selectively start coadding. This avoids
  % a synchronization barrier and lets the coadds for one map start even if
  % pairmaps for another map have not yet completed.

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Coadd the maps
  numsplits = ceil(length(tags)/80);

  % Only start coadding if all necessary pairmaps exist:
  realmap = get_map_filename(coaddopt);
  if all(rvec(realexist)) && ~exist_file(realmap)
    farm_coaddpairmaps(tags, coaddopt, ...
        'OnlyMissing', true, ...
        'Realizations', [], ...
        'Queue', queue, ...
        'MemRequire', coaddmem, ...
        'maxtime', coaddtime, ...
        'SplitSubmission', numsplits, ...
        'UseCompiled', false, ...
        'submit', submit);
  end

  calmap = get_map_filename(coaddoptcal);
  if all(rvec(calexist)) && ~exist_file(calmap)
    farm_coaddpairmaps(tags, coaddoptcal, ...
        'OnlyMissing', true, ...
        'Realizations', [], ...
        'Queue', queue, ...
        'MemRequire', coaddmem, ...
        'maxtime', coaddtime, ...
        'SplitSubmission', numsplits, ...
        'UseCompiled', false, ...
        'submit', submit);
  end

  refmap = get_map_filename(coaddoptref);
  if all(rvec(refexist)) && ~exist_file(refmap)
    farm_coaddpairmaps(tags, coaddoptref, ...
        'OnlyMissing', true, ...
        'Realizations', [], ...
        'Queue', queue, ...
        'MemRequire', coaddmem, ...
        'maxtime', coaddtime, ...
        'SplitSubmission', numsplits, ...
        'UseCompiled', false, ...
        'submit', submit);
  end

  if ~exist_file(realmap) || ~exist_file(calmap) || ~exist_file(refmap)
    disp('Submit jobs by running babysitjobs().');
    disp('Rerun this script after jobs have completed.')
    return
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Make CSV files for abscals and channel flags

  if ~exist('abscal', 'dir')
    system('mkdir -p abscal');
  end

  % Add useful information to be embedded in the CSV files.
  comments = {...
    ['Parameters are based on analysis of ' num2str(length(tags)) ...
     ' tags from ' sprintf('%04i',nreal) 'real TODs.'], ...
    ['Abscal analysis under sernum ' sprintf('%04i',nbase) '.'],...
  };

  fprintf(1,'loading real map %s...\n', realmap);
  qmap = load(realmap);
  fprintf(1,'loading reference map %s...\n', refmap);
  rmap = load(refmap);
  fprintf(1,'loading calibration map %s...\n', calmap);
  cmap = load(calmap);

  % Make the channel flags
  reduc_abscal_chflag(qmap, rmap, cmap, comments);


  % Then make plots, which will take much longer.
  pngbase = ['abscal/chflags_' datestr(now(),'yyyymmdd')];
  freqs = unique(p.band(ind.la));
  nfreqs = numel(freqs);
  for ii=1:nfreqs
    fprintf(1, 'making plots for all %iGhz detectors...\n', freqs(ii));
    % We need to find the indices for each frequency's detectors within
    % the [ind.a,ind.b] list that per-detector maps are made in.
    indab = [ind.a,ind.b];
    fq = sprintf('%03i', freqs(ii));
    indabf = [ind.(['a' fq]), ind.(['b' fq])];

    [dum,plots] = ismember(indabf,indab);

    % Run reduc_abscal with appropriate titles given to generate plots. This
    % will also unnecessarily recalculate the abscal values, but we'll just
    % ignore it.
    reduc_abscal(qmap, rmap, cmap, pngbase, plots, titles{ii});
  end
end

