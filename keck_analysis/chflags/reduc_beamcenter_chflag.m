function reduc_beamcenter_chflag(beamcen,basedate,fwhm,comments)
% reduc_beamcenter_chflag(beamcen,basedate,fwhm,comments)
%
% Calculates the beam center related channel flag cut parameters based on
% the standard beam center analysis outputs.
%
% Creates the following files in beamfit/NNNN (same folder as input data):
%
% INPUTS
%   beamcen    Path to a beam centers CSV file.
%
%   basedate   Needed to load correct info from get_array_info().
%
%   fwhm       Optional. If specified, should be an array giving the FWHM
%              beam size (in arc minutes) of a nominal beam for each
%              frequency. If not provided, the beam sizes provided by
%              p.fwhm_maj are used.
%
%   comments   Additional comments to include in output CSV files. Standard
%              comments include basic description of column values and
%              username/timestamp of file generation.
%
% EXAMPLE
%   beamfit = 'beamfit/1711/real_e_filtp3_weight3_gs_jack24.mat';
%   cmts = {...
%     'K2015 beam centers calculated in coadd 1711', ...
%     'Pager available at http://bicep.rc.fas.harvard.edu/etc' };
%   reduc_beamcorropt_chflag(beamfit,'20150101', [], cmts);
%

  if ~exist('comments','var') || isempty(comments)
    comments = {};
  end

  % Load the given parameters 
  [fits,dum] = ParameterRead(beamcen);
  % We'll need some basic array info
  [p,ind] = get_array_info(basedate);

  % Fill in FWHM if necessary.
  freqs = rvec(unique(p.band(ind.l)));
  if ~exist('fwhm','var') || isempty(fwhm)
    for ff=freqs
      lf = ind.(sprintf('l%03i',ff));
      ii = get_band_ind(p, ind, p.band(lf(1)));
      fwhm(ii) = p.fwhm_maj(lf(1)) * 60;
    end
  end
  if numel(fwhm) ~= numel(freqs)
    error('Length of fwhm does not match number of frequencies.')
  end

  % Get a few pieces of information common to all files to be written
  datestamp = datestr(now(), 'yyyymmdd');
  [r,username] = system_safe('whoami'); username = strtrim(username);

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % fp_data_radioptflags_*.csv

  % A/B offsets, calculated as in get_array_info(). Note that we only make
  % the calculation ofer the light lists to keep NaNs in open and dark
  % channels (which cannot have CMB beam centers derived).
  [lat lon] = reckon(0, 0, fits.r, fits.theta+90);
  [r,az] = distance(lat(ind.la),lon(ind.la), lat(ind.lb),lon(ind.lb));
  % Fill in distances symmetrically for both A and B
  aboffset = NaN*zeros(size(p.gcp));
  aboffset(ind.la) = r;
  aboffset(ind.lb) = r;

  % Then scale by nominal beam size to get percent error
  aboffset_pct = NaN*zeros(size(p.gcp));
  for ff=freqs
    lf = ind.(sprintf('l%03i',ff));
    ii = get_band_ind(p, ind, p.band(lf(1)));
    % Factor of 2.3548 is to convert from FWHM to sigma, and 60 to convert
    % from arcmin to deg.
    aboffset_pct(lf) = aboffset(lf) ./ fwhm(ii) .* 60*2.3548;
  end

  % Magnitude of the position error
  pos_err = hypot(fits.err_pos_x, fits.err_pos_y);
  % Leave NaNs for open channels, but mark errors and uncertainties for
  % real detectors as infinite.
  mask = rvec(isnan(pos_err)) & rvec(ismember(ind.e, ind.l));
  pos_err(mask) = Inf;

  % Units and comments are constant across the outputs.
  units.gcp = '(null)';
  units.aboffset = 'deg';
  units.aboffset_pct = 'unitless';
  units.pos_err = 'deg';

  beamsizes = arrayfun(@(fq,sz) ...
    sprintf('%0.2f arcmin @ %03iGHz',sz,fq), freqs, fwhm, ...
    'uniformoutput',false);
  beamsizes = sprintf('%s, ', beamsizes{:});
  beamsizes = beamsizes(1:end-2);
  fullcmt = [...
      { 'RADIO POINTING CHANNEL FLAG PARAMETERS', ...
       ['CREATED ' datestamp ' BY ' upper(username)],  ...
       ['A/B offset magnitudes (aboffset) computed from CMB-derived beam ' ...
        'centers as'], ...
       ['    provided in ' beamcen '.'], ...
       ['A/B offset fractions (aboffset_pct) compare offset against the ' ...
        'nominal beam '], ...
       ['    size:  ' beamsizes], ...
      }, ...
      rvec(comments), ...
    ];

  [datbase dum dum] = fileparts(beamcen);
  % Create separate inputs for each focal plane.
  [fps,rxs] = get_fp_names(basedate);
  for ii=1:length(rxs)
    nn = (p.rx == rxs(ii));

    clear pp;
    pp.gcp = p.gcp(nn);
    pp.aboffset = aboffset(nn);
    pp.aboffset_pct = aboffset_pct(nn);
    pp.pos_err = pos_err(nn);

    fpdata = fullfile(datbase, ['fp_data_radioptflags_' fps{ii} '.csv']);
    fprintf(1, 'writing %s...\n', fpdata);
    save_csv(fpdata, pp, 'comment',fullcmt, 'units',units);
  end
end

