function reduc_pairmapcontam_chflag(coaddopt,tags)
% reduc_pairmapcontam_chflag(coaddopt,tags)
%
% Calculates the pairmap contamination parameter for a given set of coadded
% maps.
%
% INPUTS
%   coaddopt  A coaddopt of options which generates the path to coadded
%             maps when the daughter is appended with '_dkNNN' or '_dkall'.
%
%             Note that the following are always set and will override any
%             other value which is set:
%
%               coaddopt.coaddtype = 3;
%               coaddopt.jacktype = '02';
%
%   tags      Either a cell array of tags or the name of a tag set understood
%             by get_tags (i.e. 'cmb2015'). get_tags() will be called with
%             no additional flags.
%
% EXAMPLE
%   coaddopt.sernum = '1711real';
%   coaddopt.chflags = [];
%   coaddopt.deproj = [0,0,0,0];
%   reduc_pairmapcontam_chflag(coaddopt,'cmb2015');
%

  if ~exist('coaddopt','var') || isempty(coaddopt) || ...
     ~isfield(coaddopt,'sernum') || isempty(coaddopt.sernum)
    error('Specifying coaddopt.sernum is not optional!')
  end
  % Always override coaddtype to ensure pairsum/diff maps.
  coaddopt.coaddtype = 3;
  % Fill out remaining unspecified parameters with defaults to make sure
  % get_map_filename() returns correctly later.
  coaddopt = get_default_coaddopt(coaddopt);

  if ~exist('tags','var') || isempty(tags)
    error('tags is not optional')
  end
  if ischar(tags)
    tags = get_tags(tags);
  end

  % Save the original daughter.
  origdaughter = '';
  if isfield(coaddopt,'daughter')
    origdaughter = coaddopt.daughter;
  end

  % Enumerate the list of deck angles
  [dates,phases,scans,decks,extra] = parse_tag(tags);
  dks = unique(decks);
  dkstr = arrayfun(@(dk) sprintf('dk%03i', dk), dks, 'uniformoutput',false);

  % Generate names for all the maps we expect to read.
  % First jack0...
  coaddopt.jacktype = '0';
  mapn0 = cellfun(@(dk) get_map_filename( ...
      setfield(coaddopt,'daughter',[origdaughter '_' dk])), dkstr, ...
      'uniformoutput',false);
  % ...then jack2.
  coaddopt.jacktype = '2';
  mapn2 = cellfun(@(dk) get_map_filename( ...
      setfield(coaddopt,'daughter',[origdaughter '_' dk])), dkstr, ...
      'uniformoutput',false);

  % Make sure these maps all exist.
  mapn = [mapn0 mapn2];
  notexist = ~cellfun(@exist_file, mapn);
  if any(notexist)
    error('Missing maps:\n%s', sprintf('\t%s\n', mapn{:}))
  end

  % Collect contamination parameters across all deck angles.
  [p,ind] = get_array_info(tags{1}(1:8));
  cc = zeros(length(dks), length(ind.a));

  for ii=1:length(dks)
    fprintf(1,'loading %s_%s maps...\n', coaddopt.sernum, ...
        [coaddopt.daughter '_' dkstr{ii}]);
    j0 = load(mapn0{ii});
    j2 = load(mapn2{ii});
    map0 = make_map(j0.ac, j0.m, j0.coaddopt);
    map2 = make_map(j2.ac, j2.m, j2.coaddopt);

    % Sanity checks:
    if size(map0,1) ~= length(ind.a)
      error('Expected %i pairs, but map contains %i channel maps.', ...
          length(ind.a), size(map0,1));
    end
    dktags = tags(decks == dks(ii));
    if ~isempty(setdiff(dktags,j0.coaddopt.tags)) ...
    && ~isempty(setdiff(dktags,j2.coaddopt.tags))
      warning('Coadded map tag list differs from given tag list.')
    end

    % Compute var(map/mapvar) / var(jack2map/jack2mapvar) for each channel
    for jj=1:size(map0,1)
      % Make peak-normalized 1/var maps
      Dv0 = map0(jj).Dvar ./ min(map0(jj).Dvar(:));
      Dv2 = 0.5*(map2(jj,1).Dvar + map2(jj,2).Dvar);
      Dv2 = Dv2 ./ min(Dv2(:));
      % Apodize maps
      dif0 = map0(jj).D ./ Dv0;
      dif2 = 0.5*((map2(jj,1).D - map2(jj,2).D)) ./ Dv2;

      % Pairsum values are unused, but might be useful to plot.
      %Tv0 = map0(jj).Tvar ./ max(map0(jj).Tvar(:));
      %Tv2 = 0.5*(map2(jj,1).Tvar + map2(jj,2).Tvar);
      %Tv2 = Tv2 ./ max(Tv2(:));
      %sum0 = map0(jj).T ./ Tv0;
      %sum2 = 0.5*(map2(jj,1).T - map2(jj,2).T) ./ Tv2;

      % Compute the variance quantity
      cc(ii,jj) = nanvar(dif0(:)) / nanvar(dif2(:));
    end % for jj=1:size(map0,1)
  end % for ii=1:length(dks)

  % Among the per-deck parameters, take the maximum as the contamination
  % parameter for each channel. For the threshold, use the minimum mean over
  % channels.
  sigma = min(nanmean(abs(cc-1), 2));
  param = max(cc, [], 1);

  % Expand to GCP size for writing out to CSV fp_data files.
  contam = NaN * zeros(size(p.gcp));
  contam(ind.a) = abs(param - 1);
  contam(ind.b) = abs(param - 1);

  [fps,rxs] = get_fp_names(tags{1}(1:8));
  for ii=1:length(rxs)
    nn = (p.rx == rxs(ii));
    % fp_data file will contain GCP indices and pairdiff contamination
    % parameters.
    pp.gcp = p.gcp(nn);
    pp.pairdiff_contam = contam(nn);
    % Unitless quantities for each
    units.gcp = '(null)';
    units.pairdiff_contam = 'unitless';
    % Store extra information in the comments:
    yr = tags{1}(1:4);
    sn = coaddopt.sernum;
    [r,un] = system_safe('whoami'); un = strtrim(un);
    dt = datestr(now(), 'yyyymmdd');
    comments = {...
      ['Contaminated pairmaps ' yr ' (calculated from coadd ' sn ')'], ...
      ['parameter = abs(var(jack0diff)/var(jack2diff) - 1)'], ...
      ['recommended threshold = ' num2str(4*sigma)], ...
      ['Created ' dt ' by ' un], ...
    };

    fpdata = ['fp_data_contaminatedpairs_' fps{ii} '_' dt '.csv'];
    fprintf(1, 'writing %s...\n', fpdata);
    save_csv(fpdata, pp, 'comment',comments, 'units',units);
  end
end

