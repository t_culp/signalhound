function reduc_abscal_chflag(chmap,refmap,calmap,comments)
% reduc_abscal_chflag(chmaps,refmaps,calmaps,comments)
%
% Produces channel flags CSV files for the abscal parameters.
%
% INPUTS
%   chmap      Per-detector coadded real map to find abscal factors for.
%
%   calmap     External reference map which has been reobserved through the
%              simulation pipeline.
%
%   calmap     External calibration map which has been reobserved through the
%              simulation pipeline.
%
%   comments   Additional comments to include in output CSV files. Standard
%              comments include basic description of column values and
%              username/timestamp of file generation.
% EXAMPLE
%   chmap  = 'maps/1712/real_e_filtp3_weight3_gs_jack04.mat';
%   refmap = 'maps/1712/0001_e_filtp3_weight3_gs_jack04.mat';
%   calmap = 'maps/1712/0002_e_filtp3_weight3_gs_jack04.mat';
%   cmts = {...
%     'K2015 per-detector abscals coadd 1711', ...
%     'Pager available at http://bicep.rc.fas.harvard.edu/etc' };
%
%   reduc_abscal_chflag(chmap, refmap, calmap, cmts);
%

  if ~exist('comments','var') || isempty(comments)
    comments = {};
  end

  if ischar(chmap)
    fprintf(1,'loading detector maps from %s...\n', chmap);
    chmap  = load(chmap);
  end
  if ischar(refmap)
    fprintf(1,'loading reference maps from %s...\n', refmap);
    refmap = load(refmap);
  end
  if ischar(calmap)
    fprintf(1,'loading calibration maps from %s...\n', calmap);
    calmap = load(calmap);
  end

  p = chmap.coaddopt.p;
  ind = chmap.coaddopt.ind;
  % Per-detector maps are accumulated in order of all A, then all B.
  indab = [ind.a,ind.b];
  nch = numel(indab);
  if size(calmap.ac,1) ~= nch || size(refmap.ac,1) ~= nch ...
  || size(calmap.ac,1) ~= nch
    error('expected to be provided per-detector coadded maps');
  end

  % Get per-detector abscal values.
  ukpv = reduc_abscal(chmap, refmap, calmap, [], false, {'','',''});

  % Basic structure of the CSV files:
  b.gcp = p.gcp;
  b.ukpv = NaN*zeros(size(b.gcp));
  b.ukpv_pct = NaN*zeros(size(b.gcp));
  b.ukpvaoverb = NaN*zeros(size(b.gcp));
  u.gcp = '(null)';
  u.ukpv = 'uK/fbu';
  u.ukpv_pct = 'unitless';
  u.ukpvaoverb = 'unitless';

  % Start forming our eventual CSV output by putting the raw ukpervolt
  % values in GCP order.
  b.gcp = p.gcp;
  b.ukpv(indab) = ukpv;

  % Getting A-over-B ratio is simple since values are all A, then all B in
  % proper pair order.
  b.ukpvaoverb(ind.a) = ukpv(1:nch/2) ./ ukpv(nch/2+(1:nch/2));
  b.ukpvaoverb(ind.b) = b.ukpvaoverb(ind.a);

  % Fractional deviation is done by frequency.
  freqs = unique(p.band(ind.la));
  nfreqs = numel(freqs);
  ukpvmed = zeros(size(freqs));
  for ii=1:nfreqs
    indl = ind.(sprintf('l%03i',freqs(ii)));
    ukpvmed(ii) = nanmedian(b.ukpv(indl));
    b.ukpv_pct(indl) = b.ukpv(indl) ./ ukpvmed(ii);
  end

  % Collect a bit of auxiliarly information to insert into CSV comments:
  [r,username] = system_safe('whoami'); username = strtrim(username);
  datestamp = datestr(now(), 'yyyymmdd');

  % The structure now contains all focal planes, but the data must be output
  % in per-focal plane data files.
  [fps,rxs] = get_fp_names(chmap.coaddopt.tags{1}(1:8));
  for ii=1:length(rxs)
    bb = structcut(b, find(p.rx == rxs(ii)));
    indd = strip_ind(ind, find(p.rx == rxs(ii)));

    thisfreq = unique(p.band(indd.l));
    thismed = round(ukpvmed(thisfreq==freqs));

    fullcmt = [...
      {...
        ['PER-DETECTOR ABSCAL CHANNEL FLAG PARAMETERS'],...
        ['CREATED ' datestamp ' BY ' upper(username)],...
        ['ukpv gives scale factor in microkelvin per fbu.'],...
        ['ukpv_pct values are scaled with respect to the median response:'],...
        ['    median = ' num2str(thismed) ' uK/fbu for ' num2str(thisfreq)...
            'GHz detectors.'],...
      },...
      rvec(comments)
    ];

    fpdata = ['abscal/fp_data_ukpvflags_' fps{ii} '_' datestamp '.csv'];
    fprintf(1, 'writing %s...\n', fpdata);
    save_csv(fpdata, bb, 'comment',fullcmt, 'units',u);
  end

  % Per-detector abscals are also used for subtraction of differential
  % ellipticity during deprojection. Produce that output as well.
  bp.gcp = p.gcp;
  bp.rx = p.rx;
  bp.ukpv = b.ukpv;
  up.gcp = '(null)';
  up.rx = '(null)';
  up.ukpv = 'uK/fbu';
  fullcmt = [...
    {...
      ['PER-DETECTOR ABSCALS'],...
      ['CREATED ' datestamp ' BY ' upper(username)],...
      ['ukpv gives scale factor in microkelvin per fbu.'],...
    },...
    rvec(comments);
  ];

  fpdata = ['abscal/abscal_' datestamp '.csv'];
  fprintf(1, 'writing %s...\n', fpdata);
  save_csv(fpdata, bp, 'comment',fullcmt, 'units',up);
end

