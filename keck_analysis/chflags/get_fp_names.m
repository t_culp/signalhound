function [fps,rx]=get_fp_names(fpdate)
% [fps,rx]=get_fp_names(fpdate)
%
% Enumerates the names of the focal planes within the array during epoch
% specified by fpdate.
%
% INPUTS
%   fpdate    String or numeric date, or tag to specify time epoch.
%
% OUTPUTS
%   fps       Cell array of focal plane names.
%
%   rx        Array of corresponding receiver numbers.
%
% EXAMPLE
%   dat = 20150101;
%   fps = get_fp_names(dat)
%   rx1 = find_file_by_date(dat, ['aux_data/fp_data/fp_data_' fps{2}]);
%   rx1 = ParameterRead(rx1);
%

  if ischar(fpdate)
    if length(fpdate) > 8
      [fpdate,dum,dum,dum] = parse_tag(fpdate);
    else
      fpdate = str2double(fpdate);
    end
  end

  master = find_file_by_date(fpdate, 'aux_data/fp_data/fp_data_master');
  fps = rvec(master.fpu);
  rx = rvec(master.rx);
end
