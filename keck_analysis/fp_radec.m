function fp_radec(ra,dec,dk,chi_grid)
% fp_radec(ra,dec,dk)
% fp_radec(ra,dec,dk,chi_grid)
%
% Draw bicep focal plane for given deck angle on an ra/dec map with
% true bolo positions and orientations
%
% chi_grid will set all A/B polarization angles equal to chi_grid and
% chi_grid+90, which is useful for making sure that the orientations
% behave at high declination
% 
%


[p,ind]=get_array_info;


if(exist('chi_grid','var'))
  p.chi=-p.theta+chi_grid;
  indd=2:2:length(p.chi);
  p.chi(indd)=p.chi(indd)+90;
end

if(exist('dk','var'))
  p=rotarray(p,dk);
end

alpha=chi2alpha(ra,dec,p);
[decb,rab]=reckon(dec,ra,p.r,p.theta+90);

roff=p.fwhm_maj/(2*sqrt(2*log(2)));
xo=roff.*sin(alpha*pi/180)./cos(decb*pi/180);
yo=roff.*cos(alpha*pi/180);

xl=[rab-xo,rab+xo]';
yl=[decb-yo,decb+yo]';

%clf
plot(rab,decb,'o')
for i=1:2:length(rab)
  text(rab(i)+.2,decb(i)+.2,num2str(i))
end


%set(gca,'Xdir','reverse')

line(xl(:,ind.la),yl(:,ind.la),'Color','blue')
%line(xl(:,ind.l100a),yl(:,ind.l100a),'Color','blue')
%line(xl(:,ind.l220a),yl(:,ind.l220a),'Color','blue')

line(xl(:,ind.lb),yl(:,ind.lb),'Color','red')
%line(xl(:,ind.l100b),yl(:,ind.l100b),'Color','red')
%line(xl(:,ind.l220b),yl(:,ind.l220b),'Color','red')

set(gca,'YDir','reverse');

return











