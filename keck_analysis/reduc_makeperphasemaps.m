function reduc_makeperphasemaps(tags)
% reduc_makeperphasemaps(taglist_for_single_phase)
% 
% generates coadded per-phase pairmaps for reducplots, saves to maps/0000/
%
% i.e. tags=get_tags('all','new')
%      ph=taglist_to_phaselist(tags);
%      reduc_makeperphasemaps(ph.tags{1})
%
% CDS 20110420

% get run information
[p,ind]=get_array_info(tags{1});
rx=unique(p.rx);

ph=taglist_to_phaselist(tags);

for i=1:numel(ph.name)
  tagz=ph.tags{i};

  coaddopt.filt='p3';
  coaddopt.gs=1;
  coaddopt.weight=3;
  coaddopt.jacktype='02';  
  coaddopt.sernum='0000real';
  coaddopt.daughter=ph.name{i};
  coaddopt.chflags=[];
  
  if(~exist('maps/0000','dir'))
    mkdir('maps/0000');
  end
  
  % coadd over all pairs
  % only need to do this step if there aren't multiple rx's
  % otherwise will split by rx and later add their ac structures
  if(numel(rx)==1)
    coaddopt.coaddtype=0;
    reduc_coaddpairmaps(tagz,coaddopt);
  end
  
  % split by rx
  if(numel(rx)>1)
    coaddopt.coaddtype=1;
    reduc_coaddpairmaps(tagz,coaddopt);
  end
  
  % per pair sum diff
  coaddopt.coaddtype=3;
  coaddopt.jacktype='0';
  reduc_coaddpairmaps(tagz,coaddopt);
  
end

  
return
