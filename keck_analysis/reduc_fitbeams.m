function reduc_fitbeams(tag)
% reduc_fitbeams(tag)
%
% Plot per channel maps zooming in on a src
%
% e.g.: reduc_fitbeams('050713_maps_filtm')

cflag=0;

load(['data/',tag]);
tag=strrep(tag,'_','\_');

% get index array relevant for this day
[p,ind]=get_array_info(tag);

% re-read p array so that we see src positions relative to nominal
% note that offset of center feed is forced to zero before write
% output
p=get_array_info;

%figure(1)
%maps_plot_allch(mapu{1},p,ind.rgla,tag)

%figure(2)
%plot_ind_abd_zoom(mapu{1},dmapu{1},p,ind.rgla,tag,cflag)

figure(1)
fpa=plot_ind_zoom_array(mapu{1},p,ind.gla,tag,1,0);
%mkgif(sprintf('%s_ibeams_a.gif',tag));
figure(2)
fpb=plot_ind_zoom_array(mapu{1},p,ind.glb,tag,1,0);
%mkgif(sprintf('%s_ibeams_b.gif',tag));

% a/b to sum/diff
[dmapu,pp]=maps_channel_diff(mapu,ind.la,ind.lb,p);
figure(3)
fpc=plot_ind_zoom_array(dmapu{1},pp,ind.rglb,tag,2,fpb);
%mkgif(sprintf('%s_ibeams_a-b.gif',tag));

% fetch tod data and get mean air temp
load(['data/',tag(1:6),'_tod']);
samprate=length(d.tf)/length(d.t);
i=reshape(mapu{1}.ind,[samprate,size(d.weather.air_temp)]);
i=i(1,:);
meanairtemp=mean(d.weather.air_temp(i));

% merge a/b info
fp=fpa+fpb;

if(1)
  % force center channel position offset to be zero
  fp(:,2)=fp(:,2)-mean(fp(1:2,2));
  fp(:,3)=fp(:,3)-mean(fp(1:2,3));
end

% convert angle to deg
fp(:,6)=fp(:,6)*180/pi;

% peak=0 indicates fit failed
fp(fp(:,1)==0,:)=0;

% write parameter file
if(0)
  fh=fopen(sprintf('beams/%s.txt',tag(1:6)),'w');
  fprintf(fh,'DATE,%s\n',tag(1:6));
  fprintf(fh,'DECK,%f\n',mapu{1}.dk);
  fprintf(fh,'SOURCE,%s\n',mapu{1}.src);
  fprintf(fh,'EXT_TEMP,%f\n',meanairtemp);
  fprintf(fh,'BODY\n');
  fprintf(fh,'        channel_name, ra_off_dos, dec_off,    beam_sig_a,beam_sig_b,beam_phi,beam_peak\n');
  fprintf(fh,'Units,  (null),       deg,        deg,        deg,       deg,       deg,     volts\n');
  fprintf(fh,'Format, string,       float,      float,      float,     float,     float,   float\n');
  for i=1:size(fp,1)
    fprintf(fh,'       %8s,    %10.6f, %10.6f, %9.6f, %9.6f, %7.2f, %7.3f\n',...
	p.channel_name{i},fp(i,2:6),fp(i,1));
  end
  fclose(fh);
end

if(0)
figure(3); clf
hold on
x=fp(:,2); y=fp(:,3); r=0.05;
[xc,yc]=circle(x,y,r,[],1);
i=intersect(find(fp(:,2)~=0),ind.gla);
text(x(i),y(i)+r+0.03,p.channel_name(i),'HorizontalAlignment','center','Color','r');
plot(xc(:,i),yc(:,i),'r');
i=intersect(find(fp(:,2)~=0),ind.glb);
text(x(i),y(i)-r-0.03,p.channel_name(i),'HorizontalAlignment','center','Color','b');
plot(xc(:,i),yc(:,i),'b');
hold off
axis square; xlim([-1,1]); ylim([-1,1]); box on
end

% de-rotate x,y feed offset angles according to dk angle
% as maps_coadd will re-rotate
p.ra_off_dos=fp(:,2); p.dec_off=fp(:,3);
p=rotarray(p,-mapu{1}.dk);

figure(4); clf; setwinsize(gcf,700,300);
mapu100=maps_coadd(mapu,ind.rgl100,p);
mapu150=maps_coadd(mapu,ind.rgl150,p);
plot_coadd_maps(mapu100{1},mapu150{1},tag,cflag);
%mkgif(sprintf('%s_cobeams.gif',tag));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_coadd_maps(map1,map2,tag,cflag)

c=cos(map1.elc*pi/180);

subplot(1,2,1)
imagesc(map1.rdoff.x_tic*c,map1.rdoff.y_tic,map1.rdoff.map);
axis xy; axis equal; axis tight
set(gca,'XDir','reverse'); % RA
title(sprintf('100GHz src=%s dk=%.0f',map1.src,map1.dk));
xlabel('RA offset dos (deg)'); ylabel('Dec offset (deg)'); 
colorbar
xlim([-0.2,0.2]); ylim([-0.2,0.2])
grid
%set(gca,'XColor','b'); set(gca,'YColor','b')

subplot(1,2,2)
imagesc(map2.rdoff.x_tic*c,map2.rdoff.y_tic,map2.rdoff.map);
axis xy; axis equal; axis tight
set(gca,'XDir','reverse'); % RA
title(sprintf('150GHz src=%s dk=%.0f',map2.src,map2.dk));
colorbar
xlim([-0.2,0.2]); ylim([-0.2,0.2])
grid
%set(gca,'XColor','b'); set(gca,'YColor','b')

gtitle(tag);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fp=plot_ind_zoom_array(map,p,ind,tag,flag,fpb)

fp=zeros(62,7);

% rotate array according to dk angle for this map
grid_phi=p.grid_phi;
p=rotarray(p,map.dk);

setwinsize(gcf,900,900); clf
for i=ind

  [xg,yg]=meshgrid(map.rdoff.x_tic,map.rdoff.y_tic);
  
  % rdoff map is constructed for center feed. Shift map by nominal
  % feed offset angles - source should now be at center.
  
  % shift map up by nominal feed offset
  yg=yg+p.dec_off(i);
  % go from x is ra offset to x is deg on sky at dec of source
  xg=xg.*cos((map.elc+yg)*pi/180);
  % shift map sideways by nominal feed offset
  xg=xg+p.ra_off_dos(i);
  
  % find pixel closest to 0,0 and pick out nearby region
  [mv,mi]=minmd(abs(xg)+abs(yg));
  ix=abs(xg(mi(1),:))<0.25;
  iy=abs(yg(:,mi(2)))<0.25;
  
  % chop down the x/y axes, map and grids
  x_tic=xg(mi(1),ix); y_tic=yg(iy,mi(2));
  mapc=map.rdoff.map(iy,ix,i);
  xg=xg(iy,ix); yg=yg(iy,ix);

  % remove median from each row of zoomed region
  mapc=bsxfun(@minus,mapc,median(mapc,2));
  
  % plot the map
  % Plot array layout RA/Dec style with RA inc to left
  axes('position',[-p.ra_off_dos(i)/2+0.49,p.dec_off(i)/2+0.44,0.08,0.08]);
  imagesc(x_tic,y_tic,mapc);
  axis xy; axis equal; axis tight; grid
  set(gca,'XDir','reverse'); % RA
 
  if(grid_phi(i+rem(i,2)-1)==-57)
    set(gca,'XColor','r'); set(gca,'YColor','r')
  else
    set(gca,'XColor','b'); set(gca,'YColor','b');
  end
    
  %par=[min(mapc(:)),0,0,0.04];
  %par=lsqnonlin(@(x) mapc-gauss2(x,xg,yg),par);
  %mod=gauss2(par,xg,yg);

  switch flag
    case 1
  
      % fit to elliptical gauss model
      [mv,mi]=maxmd(mapc);
      par=[mv,x_tic(mi(2)),y_tic(mi(1)),0.04,0.04,pi/2];
      %lb=[0,x_tic(1),y_tic(1),0.01,0.01,-inf];
      %ub=[1.1,x_tic(end),y_tic(end),0.1,0.1,+inf];
      %[par,dum,dum,stat]=lsqnonlin(@(x) mapc-egauss2(x,xg,yg),par,lb,ub);
      [par,dum,dum,stat]=lsqnonlin(@(x) mapc-egauss2(x,xg,yg),par);
      %[par,pare,gof,stat]=matmin('chisq',par,[0,1,1,0,0,0],'egauss2',mapc,ones(size(mapc)),xg,yg);
      %[par,pare,gof,stat]=matmin('chisq',par,[0,0,0,1,1,1],'egauss2',mapc,ones(size(mapc)),xg,yg);
      mod=egauss2(par,xg,yg);
      
      if(par(2)<x_tic(1)|par(2)>x_tic(end)|par(3)<y_tic(1)|par(3)>y_tic(end))
	par(1)=0;
      end
      
      % enforce a>b
      if(par(4)<par(5))
	t=par(4);
	par(4)=par(5);
	par(5)=t;
	par(6)=par(6)-pi/2;
      end
      
      % enforce 0<phi<pi
      par(6)=rem(par(6),pi);
      if(par(6)<0)
	par(6)=par(6)+pi;
      end
      
      % plot the ellipse and line
      hold on
      xc=par(2); yc=par(3);
      [x,y]=ellipse(xc,yc,par(4),par(5),par(6),[],1);
      plot(x,y,'r');
      c=cos(par(6)); s=sin(par(6));
      line([-c,c]+xc,[-s,s]+yc,'color','r');
      hold off
      
      title(sprintf('%s %.3f %.3f %4.2f',p.channel_name{i},par(1),par(4),par(4)/par(5)));
      
      % stat from fit does not indicate much
      % instead if a,b or peak are pegged at lower limit zero the result
      %if(par(1)<0.011|par(4)<0.011|par(5)<0.011)
      %	par(1)=0;
      %end
      
      % go from source offset in field to feed offset on sky
      par(2)=-par(2)+p.ra_off_dos(i);
      par(3)=-par(3)+p.dec_off(i);
      
      par=[par,stat];
      par
      
      fp(i,:)=par;
      
    case 2
      % find the peak of the difference map
      [mv,mi]=max(abs(mapc(:)));
      mv=mapc(mi);
      fp(i,1)=mv/(fpb(i,1));
      
      title(sprintf('%s %.3f',p.channel_name{i},fp(i,1)));
  end
  
end

gtitle(sprintf('%s src=%s dk=%.0f',tag,map.src,map.dk));

% draw RA/Dec indicators
axes('position',[0.85,0.06,0.08,0.08],'Visible','off');
line([0,1],[0,0]);
line([1,1],[0,1]);
text(0.5,-0.1,'RA','HorizontalAlignment','center','FontSize',15);
text(1.12,0.5,'Dec','HorizontalAlignment','center','FontSize',15,'Rotation',90);

return
