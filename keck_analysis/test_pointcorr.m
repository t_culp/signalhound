% Examing how well on field pointing offset tracks the crossobs using
% the subset of days when the center pixel scans over either of the 2
% brightest sources in the field

% These are
% 1 - PKS0537-441 at elevation of 44.09 and
% 2 - PKS0518-45 (PICTOR A) at elevation of 45.78

% Daily strips start at 50-n*0.16 and work up 0.64deg
% So source 2 occurs in strips 27:30
% and source 1 occurs in strips 37:40

% Get all such runs and make single day coadd maps from them

mapopt.type='cmb';
mapopt.filt='p3';
mapopt.jacktype=1;
mapopt.weight=2;
mapopt.exname='';
mapopt.fd=0;

% all relevant stripes
tags={'050618','050704','050818','050922',...
    '050619','050702','050709','050820',...
    '050620','050705','050821','050903',...
    '050621','050822','050904',....
    '050628','050914','050919',...
    '050629','050915','050916',...
    '050707','050917',...
    '050708','050918'};

% kick out some days which have non-pointing problems
tags=setdiff(tags,{'050619','050628','050914','050915','050918'});

% For each day make the single day coadd map
for i=1:length(tags)
  tags{i}
  
  reduc_pntsrcmask(tags{i},get_src);
  reduc_applycal(tags{i},0); % at the moment no pointing correction
  mapopt.exmaptag=[tags{i},'_'];
  reduc_makecomap(tags(i),mapopt);
end

% Now read in each single day map, fit source location in each deck
% angle and record

% Now read in crossobs results from calval and overplot 
