function bpwf=reduc_directbpwf(apsname)
% function bpwf = reduc_directbpwf(apsname)
% creates bpwf from stack of delta function aps
% apsname   =  (string) containing apsname pattern with wildcards to
%              create aps of.
%            ie. '0704/???B_allcmb_filtp3_weight3_gs_dp1100_jack0_matrix.mat'
%
% e.g.:
% reduc_directbpwf('0704/???[E,B]_allcmb_filtp3_weight3_gs_dp1100_jack0_matrix.mat')
%

% dir function only knows * wildcard
[s,d]=system_safe(sprintf('ls -1 aps/%s',apsname));
% for some bizarre reason d now contains non-printing nonsense
d=strip_nonsense(d);
apsfiles=strread(d,'%s');
naps=length(apsfiles);

for ii=1:naps

  % load aps files
  load(apsfiles{ii}, 'aps')
  
  % figure out what ell we are out from filename
  slash_index=max(strfind(apsfiles{ii},'/'));
  ell=str2num(apsfiles{ii}(slash_index+1:slash_index+3));
  if ell==0
    continue
  end
  disp(['doing ell = ' num2str(ell)])

  % change into array
  for jj=1:size(aps,1) %allow for mult experiments in the same file
    for r=1:size(aps,2)
      apsarr(jj).Cs_l(r,:,:)=aps(jj,r).Cs_l;  
    end
  end
  
  % take the mean
  for jj=1:size(aps,1)
    apsarr(jj).ave=squeeze(mean(apsarr(jj).Cs_l,1));
  end
  
  % put into stacks of E and B sims
  % initialize apse/b here if they don't exist
  if ~exist('apse','var')
    apse(size(aps,1)).l=[]; apse(size(aps,1)).Cs_l=[];
    apsb(size(aps,1)).l=[]; apsb(size(aps,1)).Cs_l=[];
  end
  for jj=1:size(aps,1)
    if(strfind(apsfiles{ii}, 'E'))
      apse(jj).l(end+1,1)=ell;
      apse(jj).Cs_l(end+1,:,:,:)=apsarr(jj).ave;
    else
      apsb(jj).l(end+1,1)=ell;
      apsb(jj).Cs_l(end+1,:,:,:)=apsarr(jj).ave;
    end
  end
  
end

% the initial delta function was flat in Cl(l+1)l and in K^2
% convert it to Cl(l+1)l/2pi in muK^2:
for jj=1:length(apse)
  apse(jj).Cs_l = apse(jj).Cs_l*2*pi/1e12;
  apsb(jj).Cs_l = apsb(jj).Cs_l*2*pi/1e12;

  % interpolate to a full range of ell:
  l = apse(jj).l(1):1:apse(jj).l(end);
  apse(jj).Cs_l = interp1(apse(jj).l,apse(jj).Cs_l,l,'spline');
  apse(jj).l=l;

  % make a standard bpwf data structure:
  bpwf(jj).l=apse(jj).l;
  % TT
  bpwf(jj).Cs_l(:,:,1)=apse(jj).Cs_l(:,:,1);
  % EE from pure EE sky
  bpwf(jj).Cs_l(:,:,3)=apse(jj).Cs_l(:,:,3);
  % BB from pure EE sky
  bpwf(jj).Cs_l(:,:,5)=apse(jj).Cs_l(:,:,4);
  % TP from pure TT/EE sky
  bpwf(jj).Cs_l(:,:,2)=apse(jj).Cs_l(:,:,2);

  if ~isempty(apsb(jj).Cs_l)
    % interpolate b's to same ell as e's
    apsb(jj).Cs_l = interp1(apsb(jj).l,apsb(jj).Cs_l,l,'spline');
    apsb(jj).l=l;
    % BB from pure BB sky
    bpwf(jj).Cs_l(:,:,4)=apsb(jj).Cs_l(:,:,4);
    % EE from pure BB sky
    bpwf(jj).Cs_l(:,:,6)=apsb(jj).Cs_l(:,:,3);
  end
  
  if size(apse(jj).Cs_l,3)>6
    % PT from pure TT/EE sky (nonzero for the cross spectra cases)
    bpwf(jj).Cs_l(:,:,7)=apse(jj).Cs_l(:,:,7);
  end
end

%normalize to max for each spec
if(0)
  for jj=1:length(bpwf)
    for i=1:size(bpwf(jj).Cs_l,3)
      peak=max(max(bpwf(jj).Cs_l(:,:,i)));
      bpwf(jj).Cs_l(:,:,i)=bpwf(jj).Cs_l(:,:,i)./peak;
    end
  end
end
%normalize the bpwf to a sum of one
if(0)
  bpwf=norm_bpwf(bpwf);
end

%save bpwf
bpwfname=strrep(apsname, '???[E,B]', 'xxxx');
bpwfname=['aps/' strrep(bpwfname, '.mat', '_directbpwf.mat')];
saveandtest(bpwfname, 'bpwf');
setpermissions(bpwfname);

%optional plotting
if(0)
  setwinsize(gcf, 800,800)
  if strfind(apsname, 'B_')
    subplot(3,1,1)
    plot(bpwf(jj).l, bpwf(jj).Cs_l(:,:,1))
    ylabel('l(l+1)C^{TT}_l/2\pi');
    subplot(3,1,2)
    plot(bpwf(jj).l, bpwf(jj).Cs_l(:,:,5))
    ylabel('l(l+1)C^{TB}_l/2\pi');
    subplot(3,1,3)
    plot(bpwf(jj).l, bpwf(jj).Cs_l(:,:,4))
    ylabel('l(l+1)C^{BB}_l/2\pi');
    xlabel('Multipole');
    print_fig('bpwfTTTBBB')
  
    clf
    subplot(3,1,1)
    semilogy(bpwf(jj).l, bpwf(jj).Cs_l(:,:,1))
    ylim([1d-6,1d1])
    ylabel('log[l(l+1)C^{TT}_l/2\pi]');
    subplot(3,1,2)
    semilogy(bpwf(jj).l, bpwf(jj).Cs_l(:,:,5))
    ylabel('log[l(l+1)C^{TB}_l/2\pi]');
    ylim([1d-6,1d1])
    subplot(3,1,3)
    semilogy(bpwf(jj).l, bpwf(jj).Cs_l(:,:,4))
    ylabel('log[l(l+1)C^{BB}_l/2\pi]');
    xlabel('Multipole');
    ylim([1d-6,1d1])
    print_fig('bpwfTTTBBB_log')
  end  
  
  if strfind(apsname, 'E_')
    subplot(3,1,1)
    plot(bpwf(jj).l, bpwf(jj).Cs_l(:,:,1))
    ylabel('l(l+1)C^{TT}_l/2\pi');
    subplot(3,1,2)
    plot(bpwf(jj).l, bpwf(jj).Cs_l(:,:,2))
    ylabel('l(l+1)C^{TE}_l/2\pi');
    subplot(3,1,3)
    plot(bpwf(jj).l, bpwf(jj).Cs_l(:,:,3))
    ylabel('l(l+1)C^{EE}_l/2\pi');
    xlabel('Multipole');
    print('-dpng', 'bpwfTTTEEE')
    
        clf
    subplot(3,1,1)
    semilogy(bpwf(jj).l, bpwf(jj).Cs_l(:,:,1))
    ylim([1d-6,1d1])
    ylabel('log[l(l+1)C^{TT}_l/2\pi]');
    subplot(3,1,2)
    semilogy(bpwf(jj).l, bpwf(jj).Cs_l(:,:,2))
    ylabel('log[l(l+1)C^{TE}_l/2\pi]');
    ylim([1d-6,1d1])
    subplot(3,1,3)
    semilogy(bpwf(jj).l, bpwf(jj).Cs_l(:,:,3))
    ylabel('log[l(l+1)C^{EE}_l/2\pi]');
    xlabel('Multipole');
    ylim([1d-6,1d1])
    print('-dpng', 'bpwfTTTBBB_log')
  end  
end

%  keyboard
return
