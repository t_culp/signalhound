function reduc_sidelobemaps_plot_allrx(file_base, outdir_base, varargin)
  for iRx = 0:4
    file = [file_base num2str(iRx) '.mat'];
    outdir = [outdir_base   num2str(iRx) '/'];
    plot_all_lame(file, outdir, iRx, varargin{:});
    plot_all_small_lame(file, outdir, iRx, varargin{:});
  end
end
