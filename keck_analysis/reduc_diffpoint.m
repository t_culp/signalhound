function reduc_diffpoint(mapname, outdir)
% code to generate different order smoothed maps 
% given a starting unsmoothed initial map.
% the initial map can be WMAP or some other synfast map.
% output array will be saved in outdir.
%
%1 generate T Tx Ty from WMAP for each frequency beam.
%2 generate Te0 Te1 Te2 from WMAP for each frequency beam  
%
% ie. reduc_diffpoint('wmap_band_imap_r9_5yr_W_v3.fits')
  
  
  cd ~/bicep_analysis/skymaps/

  if ~exist('outdir','var')
    outdir='wmap_diffpoint/'
  end
  
 %define offset to use when shifting beams
 off=0.10;
      
%get map coordinates
m=get_map_defn('bicep',[],'radec');

%increase resolution to 0.1 deg pixels
if(1)
  m.pixsize=0.1;
  m.y_tic=m.ly:m.pixsize:m.hy;
  m.x_tic=m.lx:(m.pixsize/cosd(mean([m.ly,m.hy]))):m.hx;
  m.nx=length(m.x_tic);
  m.ny=length(m.y_tic);
end

%reading wmap 5 year T map
hwmap=read_fits_map(mapname);
hwmap.map=hwmap.map(:,1);

%convert to ra dec
lwmap=healpix_to_longlat(hwmap,m.x_tic,m.y_tic,1);

% generate the beam 
xv=m.y_tic-mean(m.y_tic); % center on zero
xv=xv-min(xv(xv>0));

% calculate beam sigma from fwhm
c=sqrt(8*log(2));
beam_sig(1)=55.68/60/c; beam_sig(2)=36.24/60/c;
freq={'100GHz','150GHz'};

%loop over 2 frequencies
for i=1:2
  % gen beam grid to 5 sigma
  x=xv(abs(xv)<beam_sig(i)*5); 
  [bxg,byg]=meshgrid(x);
  
  % gen gaussian beam
  beam=egauss2([1,0,0,beam_sig(i),beam_sig(i),0],bxg,byg);
  beam=beam./sum(beam(:));
  
  % generate offset gaussian beams
  beam1x=egauss2([1,off,0,beam_sig(i),beam_sig(i),0],bxg,byg);
  beam2x=egauss2([1,-off,0,beam_sig(i),beam_sig(i),0],bxg,byg);
  beam1y=egauss2([1,0,off,beam_sig(i),beam_sig(i),0],bxg,byg);
  beam2y=egauss2([1,0,-off,beam_sig(i),beam_sig(i),0],bxg,byg);
  beam1x=beam1x./sum(beam1x(:));
  beam2x=beam2x./sum(beam2x(:));
  beam1y=beam1y./sum(beam1y(:));
  beam2y=beam2y./sum(beam2y(:));
  
  % generate dipole beams
  beam_dx=beam1x-beam2x;
  beam_dy=beam1y-beam2y;
  
  
  %generate elliptical beams
  beam1q=egauss2([1,0,0,(1+off)*beam_sig(i),(1+off)*beam_sig(i),0],bxg,byg);
  beam2q=egauss2([1,0,0,(1+off)*beam_sig(i),beam_sig(i),0],bxg,byg);
  beam3q=egauss2([1,0,0, beam_sig(i),(1+off)*beam_sig(i),0],bxg,byg);
  beam4q=egauss2([1,0,0,(1+off)*beam_sig(i),beam_sig(i),-pi/4],bxg,byg);
  beam5q=egauss2([1,0,0,(1+off)*beam_sig(i),beam_sig(i),pi/4],bxg,byg);
  beam1q=beam1q./sum(beam1q(:));
  beam2q=beam2q./sum(beam2q(:));
  beam3q=beam3q./sum(beam3q(:));
  beam4q=beam4q./sum(beam4q(:));
  beam5q=beam5q./sum(beam5q(:));

  beam_e0=beam-beam1q;
  beam_e1=beam2q-beam3q;
  beam_e2=beam4q-beam5q;
  
  % smooth map with gaussian
  lmap(:,:,i).T=conv2(lwmap,beam,'same');
  
  % smooth map with dipoles
  lmap(:,:,i).Tx=conv2(lwmap,beam_dx,'same');
  lmap(:,:,i).Ty=conv2(lwmap,beam_dy,'same');
  
   % smooth map with quadrupoles
  lmap(:,:,i).Te0=conv2(lwmap,beam_e0,'same');
  lmap(:,:,i).Te1=conv2(lwmap,beam_e1,'same');
  lmap(:,:,i).Te2=conv2(lwmap,beam_e2,'same');

  
  %write out maps
  filename=sprintf('%s%s_smooth%03d.mat',outdir,mapname(1:end-5),off*100);
  save(sprintf('%s',filename),'m','lmap');
    
  
  %plots
  figure(2*i)
  setwinsize(gcf,900,700)
  subplot('Position',[0.03,.69,0.55,0.28])
  imagesc(m.x_tic,m.y_tic,lwmap);colorbar;
  title('WMAP 5yr, 90GHz,(uk) original resolution');caxis([-600 600]);
  grid
  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
  
  subplot('Position',[0.65,0.36,0.3,0.3])
  imagesc(x,x,beam_dx);
  title('Horizontal dipole')
  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1]);
  axis equal
  colorbar
  
  subplot('Position',[0.65,0.03,0.3,0.3])
  imagesc(x,x,beam_dy);
  title('Vertical dipole')
  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1]);
  axis equal
  colorbar
  
  subplot('Position',[0.03,0.36,0.55,0.28])
  imagesc(m.x_tic,m.y_tic,lmap(:,:,i).Tx);
  title(sprintf('WMAP 5yr 90GHz,(uk) smoothed w/%s  horizonatal dipole',freq{i}))
  caxis([-20 20]);
  grid; 
  colorbar;  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
  
  subplot('Position',[0.02,0.03,0.55,0.28])
  imagesc(m.x_tic,m.y_tic,lmap(:,:,i).Ty);
  title(sprintf('WMAP 5yr, 90GHz,(uK) smoothed w/%s vertical dipole',freq{i}))
  caxis([-20 20]);
  grid;
  colorbar;  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1]);
  printfig(2*i,sprintf('wmap_5yr_T_dipole_smooth%s.gif',freq{i}),'pp')

  figure(2*i+1)
  setwinsize(gcf,900,700)
  subplot('Position',[0.03,.69,0.55,0.28])
  imagesc(m.x_tic,m.y_tic,lmap(:,:,i).Te0);colorbar;
  title(sprintf('WMAP 5yr 90GHz,(uk) smoothed w/%s  E0 beam',freq{i}))
  caxis([-2 2]);
  grid
  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
  
  subplot('Position',[0.65,0.69,0.3,0.3])
  imagesc(x,x,beam_e0);
  title('zeroth order elliptical beam')
  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1]);
  axis equal
  colorbar
  
  subplot('Position',[0.65,0.36,0.3,0.3])
  imagesc(x,x,beam_e1);
  title('First order elliptical beam')
  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1]);
  axis equal
  colorbar
  
  subplot('Position',[0.65,0.03,0.3,0.3])
  imagesc(x,x,beam_e2);
  title('Second order Elliptical  beam')
  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1]);
  axis equal
  colorbar
  
  subplot('Position',[0.03,0.36,0.55,0.28])
  imagesc(m.x_tic,m.y_tic,lmap(:,:,i).Te1);
  title(sprintf('WMAP 5yr 90GHz,(uk) smoothed w/%s  E1 beam',freq{i}))
  caxis([-2 2]);grid;
  colorbar;  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
  
  subplot('Position',[0.02,0.03,0.55,0.28])
  imagesc(m.x_tic,m.y_tic,lmap(:,:,i).Te2);
  title(sprintf('WMAP 5yr, 90GHz,(uK) smoothed w/%s E2 beam',freq{i}))
  caxis([-2 2]);grid;
  colorbar;  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1]);
  printfig(2*i+1,sprintf('wmap_5yr_T_quadrupole_smooth%s.gif',freq{i}),'pp')

end  
  
  figure(1) 
  x=xv(abs(xv)<beam_sig(i)*5); 
  [bxg,byg]=meshgrid(x);
  beam1=egauss2([1,0,0,beam_sig(1),beam_sig(1),0],bxg,byg);
  beam1=beam1./sum(beam1(:));
        
 
  setwinsize(gcf,900,700)
  subplot('Position',[0.03,.69,0.55,0.28])
  imagesc(m.x_tic,m.y_tic,lwmap);colorbar;
  title('WMAP 5yr, 90GHz, (uK) original resolution');caxis([-600 600]);
  grid
  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])
  
  subplot('Position',[0.65,0.36,0.3,0.3])
  imagesc(x,x,beam1);
  title('100GHz gaussian')
  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1]);
  axis equal
  colorbar
  
  subplot('Position',[0.65,0.03,0.3,0.3])
  imagesc(x,x,beam);
  title('150GHz gaussian')
  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1]);
  axis equal
  colorbar
  
  subplot('Position',[0.03,0.36,0.55,0.28])
  imagesc(m.x_tic,m.y_tic,lmap(:,:,1).T);
  title('WMAP 5yr, 90GHz,(uK) smoothed with 100GHz gaussian');
  caxis([-200 200]);
  grid; 
  colorbar;  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1])

  subplot('Position',[0.03,0.03,0.55,0.28])
  imagesc(m.x_tic,m.y_tic,lmap(:,:,2).T);
  title('WMAP 5yr, 90GHz,(uK) smoothed with 150GHz gaussian');
  caxis([-200 200]);
  grid;
  colorbar;  set(gca,'PlotBoxAspectRatio',[m.xdos,m.ydos,1]);
  printfig(1,'wmap_T_monopole_smooth100_smooth150.gif','pp')
  

 
if(0) 
 load('scratch/B20060702_cmb10_1A_B_315_tod')
 load('skymaps/wmap_diffpoint/wmap_band_imap_r9_5yr_W_v3_smooth.mat')
 difopt.m=m;
 difopt.map=lmap;
 difopt.ukpervolt=get_ukpervolt();
 difopt.type='T';
 
tag='B20060702_cmb10_1A_B_315';
[p,ind]=get_array_info(tag);


d=filter_scans2(d,fs,'p3',ind.gl,p,difopt)
end
