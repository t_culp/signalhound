function reduc_bpwf_plot(file,linlog)
% reduc_bpwf_plot(file,linlog)
%
% Plot the bpwf and the resulting expectation vals
%
% e.g.
% reduc_bpwf_plot('final/0751x1351/real_a_filtp3_weight3_gs_dp1100_jack0_real_a_filtp3_weight3_gs_dp1100_jack01_matrix_overrx')

load(sprintf('%s',file));
% force to just first one
if(exist('r'))
  r=r(1); bpwf=bpwf(1);
end

% plot the bpwf
figure(1); clf

if(length(bpwf)>1)
  setwinsize(gcf,900,700)
  plot_bpwf(1,bpwf(1),'1',linlog)
  plot_bpwf(2,bpwf(2),'2',linlog)
  plot_bpwf(3,bpwf(3),'cross',linlog)
else
  setwinsize(gcf,900,500)
  plot_bpwf_single(bpwf,linlog)
end
gtitle(sprintf('BPWF from %s',strrep(file,'_','\_')));

figure(2); clf
setwinsize(gcf,900,500)
if(length(bpwf)>1)
  setwinsize(gcf,900,700)
  plot_bpwf_sum(1,bpwf(1),'1',linlog)
  plot_bpwf_sum(2,bpwf(2),'2',linlog)
  plot_bpwf_sum(3,bpwf(3),'cross',linlog)
else
  setwinsize(gcf,900,500)
  plot_bpwf_single_sum(bpwf,linlog)
end
gtitle(sprintf('BPWF integrals %s',strrep(file,'_','\_')));

if(~exist('r'))
  return
end

% get input model from cmbfast
%inpmod=load_cmbfast([]);

% bin edges for "standard bicep bins"
if  size(bpwf(1).Cs_l,2) == 68
  [be,n]=get_bins('bicep_fine');
else
  [be,n]=get_bins('bicep_norm');
end
bc=mean([be(1:end-1);be(2:end)]);
  
for i=1:length(bpwf)
  r(i).l=bc;
end
r=calc_expvals(r,inpmod,bpwf);

% compare with expv computed by simple interp
if(1)
  for j=1:length(r)
    for i=1:size(inpmod.Cs_l,2)
      r(j).expv2(:,i)=interp1(inpmod.l,inpmod.Cs_l(:,i),r(j).l);
    end
  end
end

% plot model and expectation value of bandpowers
figure(3); clf
setwinsize(gcf,900,700)
lmax=500;
plot_expval(1,r(1),inpmod,'1',lmax);
if(length(r)>1)
  plot_expval(2,r(2),inpmod,'2',lmax);
  plot_expval(3,r(3),inpmod,'cross',lmax);
end
gtitle(sprintf('Band power expectation values for %s',strrep(file,'_','\_')));

%compute band correlation
for j=1:length(bpwf)
  for i=1:size(bpwf(1).Cs_l,3)
    for k=1:size(bpwf(1).Cs_l,2)-1
      q1=find(bpwf(j).Cs_l(:,k+1,i) < bpwf(j).Cs_l(:,k,i));
      q2=find(bpwf(j).Cs_l(:,k+1,i) > bpwf(j).Cs_l(:,k,i));
      bpwf(j).corr(k,i)=sum(bpwf(j).Cs_l(q2,k,i))+sum( bpwf(j).Cs_l(q1,k+1,i)); 
    end
  end
end

% plot band correlation
figure(4);clf;
setwinsize(gcf,1000,600)
subplot(1,3,1)
plot(r(1).l(1:end-1),bpwf(1).corr(:,[1,2,3,4]),'*')
title('1'); ylim([0,0.5])
legend('TT','TE','EE->EE','BB->BB')

if(length(r)>1)
  subplot(1,3,2)
  plot(r(2).l(1:end-1),bpwf(2).corr(:,[1,2,3,5]),'*')
  title('2'); ylim([0,0.25])
  legend('TT','TE','EE->EE','EE->BB')  
  subplot(1,3,3)
  plot(r(3).l(1:end-1),bpwf(3).corr(:,[1,2,3,5]),'*')
  title('cross');
  legend('TT','TE','EE->EE','EE->BB')
  ylim([0,0.25])
end

gtitle('Band power window function correlations for l width=35',0.97)

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_bpwf(n,aps,freq,linlog)

lab={'TT','TE','EE->EE','BB->BB','EE->BB','BB->EE'};
for i=1:6
  subplot_grid(6,3,(i-1)*3+n)
  
  plot(aps.l,aps.Cs_l(:,:,i));  
  axis tight
  if(i<=4)
    ylim([0,0.04]);
  else
    ylim([0,0.009]);
  end
  grid
  subplot_grid2(6,3,(i-1)*3+n)

  switch linlog
   case 'log'  
    set(gca,'yscale','log'); ylim([1e-10,1e-1]);
  end
  
  ylabel(lab{i})
  if(i==1)
    title(freq);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_bpwf_single(bpwf,linlog)
lab={'TT','TE','EE->EE','BB->BB','EE->BB','BB->EE'};
for i=1:6
  subplot(3,2,i)
  plot(bpwf.l,bpwf.Cs_l(:,:,i));
  ylabel(lab{i})
  if(sum(bpwf.Cs_l(:,:,1))<0.9)
    ylim([0,0.02]);
  else
    ylim([0,0.04]);
  end
  switch linlog
   case 'log'
    set(gca,'yscale','log'); ylim([1e-10,1e-1]);
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_bpwf_single_sum(bpwf,linlog)
lab={'TT','TE','EE->EE','BB->BB','EE->BB / EE->EE','BB->EE / BB->BB'};
for i=1:6
  subplot(3,2,i)
  if(i<=4)
    plot(sum(bpwf.Cs_l(:,:,i),1));
  else
    plot(sum(bpwf.Cs_l(:,:,i),1)./sum(bpwf.Cs_l(:,:,i-2),1));
  end
  ylabel(lab{i})
  ylim([-0.1,1.1]);
  grid
  switch linlog
   case 'log'
    set(gca,'yscale','log'); ylim([1e-6,1.1]);
  end
end


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_expval(n,r,inpmod,freq,lmax)

lab={'TT','TE','EE','BB'};
for i=1:4
  subplot(4,3,(i-1)*3+n)
  plot(r.l,r.expv(:,i),'.');
  hold on;
  plot(r.l+10,r.expv2(:,i),'.r');
  plot(inpmod.l,inpmod.Cs_l(:,i),'k');
  hold off;
  xlim([0 lmax])
  if(i==4)
    ylim([0,0.02])
  end
  ylabel(lab{i})
  if(i==1)
    title(freq);
  end
end

return

