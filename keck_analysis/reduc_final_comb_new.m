function reduc_final_comb(rf,lmax)
% reduc_final_comb(rf)
%
% Combine 100/150/cross spectra
%
% Hack around trying to consider all bandpowers at once when deriving
% combination weights
%
% e.g.
% reduc_final_comb_new('sim003_filtp3_weight2_jack0')

% load the data set
load(sprintf('final/%s',rf));

bw1=56/60/(sqrt(8*log(2))); bw2=36/60/(sqrt(8*log(2)));
bw=[bw1,bw2,gmean(bw1,bw2)];
bwu=0.1;

% for each of 6 spectra
for i=1:6
  
  clear w
  
  % take the bands*3 cov mat
  nb=size(r(1).sim,1);
  s=[squeeze(r(1).sim(:,i,:))',squeeze(r(2).sim(:,i,:))',squeeze(r(3).sim(:,i,:))'];
  s=cov(s);
  
  % set off-diags to zero
  t=diag(diag(s,nb),nb)+diag(diag(s,2*nb),2*nb);
  t=t+t'+diag(diag(s));
  
  %x=linspace(0.8,0,nb);
  %t=diag([x,x],nb)+diag(x,2*nb);
  %t=t+t'+diag([linspace(1,1,nb),linspace(1,0.2,nb),linspace(1,0.5,nb)]);
  
  if(0)
    % beam uncer percentage
    clear bs
    for j=1:length(r)
      l=r(j).l;
      bu=exp((bw(j)*pi/180)^2*(bwu^2+2*bwu)*l.*(l+1))-1;
      
      % mult this by expectation values (could use obs values or mean of
      % sims...)
      bu=bu.*r(j).expv(:,i);
      
      % record
      bs{j}=bu;
    end
    bs{1}=linspace(0,1,nb)';
    bs{2}=linspace(0,0.2,nb)';
    bs{3}=linspace(0,0.5,nb)';
    
    % take overall outer product - assume beam fluc fully correlated
    bs=vertcat(bs{:});
    bs=bs*bs';
    
    subplot(1,4,1); imagesc(t); colorbar
    subplot(1,4,2); imagesc(bs); colorbar
    
    % add to cov mat
    t=t+bs;
    
    subplot(1,4,3); imagesc(t); colorbar
  end
  
  % take the inv cov of this
  c=inv(t);

  % take the col sum
  w=sum(c);
  
  % reshape to give weights for each of 100/150/cross
  w=reshape(w,[nb,3]);
  
  % normalize to sum of 1
  w=w./repmat(sum(w,2),[1,3]);
  
  if(i==2|i==5|i==6)
    w=[w,zeros(nb,1)];
  end
  
  % store the weights
  ws{i}=w;
end

% combine the fields of r according to the weights
for i=1:6
  rc.real(:,i)=comb_r({r.real},i,ws{i});
  rc.expv(:,i)=comb_r({r.expv},i,ws{i});
  %rc.rwf(:,i)=comb_r({r.rwf},i,ws{i});
  
  rc.sim(:,i,:)=comb_r({r.sim},i,ws{i});
  %rc.sigsim(:,i,:)=comb_r({r.sigsim},i,ws{i});
  %rc.noisim(:,i,:)=comb_r({r.noisim},i,ws{i});
end
rc.l=r(1).l;
rc.ws=ws;

if(0)
% combine the fields of bpwf according to the weights
for i=1:4 % for each of TT/TE/EE/BB
  % combine the bpwf according to the weights
  for j=1:size(bpwf(1).Cs_l,2) % for each bandpower
    
    switch i
      case 1
	% TT simple 3 way collapse over freq
	bpwfc.Cs_l(:,j,i)=sum([bpwf(1).Cs_l(:,j,i).*ws{i}(j,1),bpwf(2).Cs_l(:,j,i).*ws{i}(j,2),bpwf(3).Cs_l(:,j,i).*ws{i}(j,3)],2);
      case 2
	% TE 4 way collapse including ET
	bpwfc.Cs_l(:,j,i)=sum([bpwf(1).Cs_l(:,j,i).*ws{i}(j,1),bpwf(2).Cs_l(:,j,i).*ws{i}(j,2),bpwf(3).Cs_l(:,j,i).*ws{i}(j,3),bpwf(3).Cs_l(:,j,7).*ws{i}(j,4)],2);
      case {3,4}
	% EE/BB 3 way collapse of XX->XX & XX->YY functions
	bpwfc.Cs_l(:,j,i)=sum([bpwf(1).Cs_l(:,j,i).*ws{i}(j,1),bpwf(2).Cs_l(:,j,i).*ws{i}(j,2),bpwf(3).Cs_l(:,j,i).*ws{i}(j,3)],2);
	bpwfc.Cs_l(:,j,i+2)=sum([bpwf(1).Cs_l(:,j,i+2).*ws{i}(j,1),bpwf(2).Cs_l(:,j,i+2).*ws{i}(j,2),bpwf(3).Cs_l(:,j,i+2).*ws{i}(j,3)],2);
	
    end
  end
end
bpwfc.l=bpwf(1).l;
end

% plot the 100/150/cross points and their combination
r=get_bpcov(r);
rc=get_bpcov(rc);

l={'TT','TE','EE','BB','TB','EB'};
for i=1:6
  figure(i); clf
  setwinsize(gcf,1200,900)

  if (lmax == 500)
    yrange=[-2000,10000;-120,150;-5,30;-5,30;-50,50;-50,50];
  elseif (lmax == 200)
    yrange=[-2000,10000;-100,10;-.5,1.5;-.5,.5;-10,10;-1,1];
  end
  
  subplot(3,1,1)
  
  errorbar2(r(1).l-4,r(1).real(:,i),r(1).derr(:,i),'r.');
  hold on
  errorbar2(r(2).l-2,r(2).real(:,i),r(2).derr(:,i),'g.');
  errorbar2(r(3).l,r(3).real(:,i),r(3).derr(:,i),'b.');
  errorbar2(rc.l+2,rc.real(:,i),rc.derr(:,i),'k.')  
  switch i
    case 2
      errorbar2(r(3).l+25,r(3).real(:,7),r(3).derr(:,7),'m.');
    case 5
      errorbar2(r(3).l+25,r(3).real(:,8),r(3).derr(:,8),'m.');
    case 6
      errorbar2(r(3).l+25,r(3).real(:,9),r(3).derr(:,9),'m.');
  end
  plot(rc.l,rc.expv(:,i),'co')
  plot(inpmod.l,inpmod.Cs_l(:,i),'r');
  hold off
  grid
  xlim([0,lmax]);
  ylim(yrange(i,:));
  
  %set(gca,'YScale','log')

  title(sprintf('%s - red=100GHz, green=150GHz, blue=cross, black=combined',l{i}))
  %legend('100GHz','150GHz','cross','comb')
  xlabel('multipole'); ylabel('power')
  
  subplot(3,1,2)
  plot(rc.l,rc.derr(:,i)./r(2).derr(:,i),'.-');
  xlim([0,lmax]);
  ylim([0.5,1.5])
  xlabel('multipole'); ylabel('combined error bar / 150GHz error bar')
  grid
  
  subplot(3,1,3)
  plot(rc.l,ws{i}); ylim([-.2,1]);
  xlim([0,lmax]);
  legend({'100GHz','150GHz','cross'});
  xlabel('multipole'); ylabel('combination weights')
  grid
  
  %mkgif(sprintf('%s.gif',l{i}));
  %pause
end

if(0)
figure(2); clf
plot(r(1).l,r(1).rwf(:,1),'k--')
hold on
plot(r(1).l,r(2).rwf(:,1),'k--')
plot(r(1).l,r(3).rwf(:,1),'k--')
plot(rc.l,rc.rwf)
hold off
ylim([0,5]);
grid
xlabel('multipole'); ylabel('reciprocal of window function');
end

if(0)
  clf
  % check if combined result unbiased
  for i=1:6
    plot(r(1).l,r(1).expv(:,i,:),'or');
    hold on
    plot(r(1).l,r(2).expv(:,i,:),'og');
    plot(r(1).l,r(3).expv(:,i,:),'ob');
    if(i==2)
      plot(r(1).l,r(3).expv(:,7,:),'om');
    end
    
    plot(r(1).l,mean(r(1).sigsim(:,i,:),3),'r');
    plot(r(1).l,mean(r(2).sigsim(:,i,:),3),'g');
    plot(r(1).l,mean(r(3).sigsim(:,i,:),3),'b');
    if(i==2)
      plot(r(1).l,mean(r(3).sigsim(:,7,:),3),'m');
    end
    plot(rc.l,mean(rc.sigsim(:,i,:),3),'k');
    hold off
    grid
    
    pause
  end
end

% save the combined results
r=rc; %bpwf=bpwfc;
rf
%pause
save(sprintf('final/%s_comb',rf),'r');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xc=comb_r(x,i,ws)

% for each realization
for j=1:size(x{1},3)
  switch i
    case {1,3,4} % TT,EE,BB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j)].*ws,2);
    case 2 % TE
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,7,j)].*ws,2);
    case 5 % TB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,8,j)].*ws,2);
    case 6 % EB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,9,j)].*ws,2);
      
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function x=gen_corr_rnd(c,s)

x=randn(s);
c=chol(c);
x=x*c;

return
