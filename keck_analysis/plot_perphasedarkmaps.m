function plot_perphasedarkmaps(phase_tag,out,rx,allfreq)
% plot_perphasedarkmaps(phase_tag,out,rx)
% phase_tag='YYYYMMDDP_dk###'
% out = 0  print to screen
%     = 1  print to file
% rx = 'all' plots coadd over all receivers (default)
%    = n plots only for receiver n
% allfreq = true   Generates an "all-frequency" plot, even if given single rx
%         = false  Normal per-rx or per-freq heuristics apply
% 
% e.g. plot_perphasedarkmaps('20110312B_dk113',1)

[p,ind]=get_array_info(phase_tag);

if ~exist('out','var') || isempty(out)
  out=0; 
end
if ~exist('rx','var') || isempty(rx)
  rx=unique(p.rx);
end

if ~exist('allfreq','var') || isempty(allfreq)
  if numel(rx) > 1
    allfreq = true;
  else
    allfreq = false;
  end
end


% don't plot to screen if writing a png file
if(out==1); 
  set(0,'DefaultFigureVisible','off');
end

% plot dark individual channel maps
if(numel(rx) > 1)
  setwinsize(gcf,1100,1200)
else
  setwinsize(gcf,1100,600)
end
clf

colormap jet;

fname=sprintf('maps/0000/real_%s_*_gs_jack03.mat',phase_tag);

% load map
[map,m,coaddopt]=load_and_cal_map(phase_tag,fname);

% how many dark channel pairs are we plotting?
mapordera=ind.a;
maporderb=ind.b;
if(numel(rx)~=numel(unique(p.rx)))  
  ind=strip_ind(ind,find(ismember(p.rx,rx)));
end

% find A darks
ind.da=intersect(ind.d,find(strcmp(p.pol,'A')));
npairs=numel(ind.da);

% find corresponding B darks
for i=1:numel(ind.da)
  [dum,j]=intersect(ind.a,ind.da(i));
  ind.db(i)=ind.b(j);
end

% what is our color axis?  
ca=500;

% plot the maps
for i=1:npairs
  
  % what map index contains this dark pair?
  [dum,mapind]=intersect(mapordera,ind.da(i));

  % only plot sub-region of map with information
  good=~isnan(map(mapind).T);
  x=(any(good,1));
  y=(any(good,2));  
  x=min(find(x==1)):max(find(x==1));
  y=min(find(y==1)):max(find(y==1));
  
  % rx / channel info for plot labels
  thisrx=p.rx(ind.da(i));
  thisgcp=[p.gcp(ind.da(i)),p.gcp(ind.db(i))];

  % left column
  subplot(npairs,2,i*2-1); 
  imagesc(m.x_tic(x),m.y_tic(y),map(mapind).T(y,x));caxis([-ca,ca]);
  if(i==1)
    h=text(.5,1.2,'Sum','Units','Normalized',...
	'HorizontalAlignment','center','FontSize',12);
  end

  % right column
  subplot(npairs,2,i*2); 
  imagesc(m.x_tic(x),m.y_tic(y),map(mapind).D(y,x));caxis([-ca,ca]);
  if(i==1)
    h=text(.5,1.2,'Diff','Units','Normalized',...
	'HorizontalAlignment','center','FontSize',12);
  end
  h=text(1.05,0.7,['rx ',num2str(thisrx)],'Units','Normalized',...
    'HorizontalAlignment','left');
  h=text(1.05,0.3,['gcp ',num2str(thisgcp(1)),'/',num2str(thisgcp(2))],'Units','Normalized',...
    'HorizontalAlignment','left');

end

% colorbar
cax=caxis();
axes('Position',[.03,.1,.8,.8],'Visible','off');caxis(cax);
colorbar('Location','WestOutside');

% super title
gtitle(sprintf('PHASE %s (uK)',phase_tag),[],'none');

if(out==1)
  % Default to no extension
  rxext = '';

  % Only add an extension if considering subsets of receivers
  if numel(rx) < numel(unique(p.rx))
    if numel(rx) > 1 || allfreq
      freq = unique(p.band(p.band~=0 & ~isnan(p.band) & ismember(p.rx, rx)));
      rxext = sprintf('_all%03d', freq);
    else
      rxext = sprintf('_rx%i', rx);
    end
  end

  outfile = sprintf('reducplots/%s/perphase_%s_002%s.png', phase_tag(1:6), ...
      phase_tag, rxext);

  mkpng(outfile,1);
  setpermissions(outfile);
  set(0,'DefaultFigureVisible','on');
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [map,m,coaddopt]=load_and_cal_map(phase_tag,fname)

% load map
mapn=dir(fname);
load(['maps/0000/',mapn.name]);
coaddopt.coaddtype=3;
if ~exist('map','var') && exist('ac','var')
    map=make_map(ac,m,coaddopt);
end

% aply abs cal to maps
uk=get_ukpervolt(phase_tag);

% For Keck 2014+, a per-receiver abscal is returned.
% *Because coaddtype=3 is loaded*, we know how to expand uk
if(size(map,1)>1 && numel(uk)~=size(map,1))
  % First expand to the entire detector size since it's a size which will
  % work with repmat exactly.
  uk = repmat(uk,numel(coaddopt.ind.e)/numel(uk),1);
  % The map only contains pairsum/pairdiff channels, so then choose only those
  % corresponding elements. 
  uk = uk(coaddopt.ind.b);
end

map=cal_coadd_maps(map,uk);

return
