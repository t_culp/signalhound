#!/usr/bin/env python
# b1_r_wrapper.py
# written by  D Barkats and K. Karkare. Adapted from code written by V Buza.
# change history is in cvs
# This code was adapted from B2 branch matlab code. See hl_like_from_final.m for equivalent
# All variable names are the same as in matlab equivalent
#
# This  wrapper will do the following
#      Calls bicep1_likelihood_init which loads up the bpwf and all the data products from
# http://bicep.rc.fas.harvard.edu/bicep1_3yr/
#      Loads in a theory spectrum from CAMB in this case BB r = 0.1
#      Loops over r values at which to evaluate the likelihood
#          Scales fiducial BB by r, gets expvals
#          Computes the likelihood 
#      Saves the likelihood for that r
#
# Inputs:
#        - (hardwired) B1 data products from B1_3yr_likelihood_20131003.tar.gz: B1_3yr_likelihood_bandpowers_20131003.txt, B1_3yr_bpcm_20131003.txt
#        - (hardwired) B1 window functions from B1_3yr_bpwf_20131003.tar.gz:  B1_3yr_bpwf_bin[1-9]_20131003.txt
#        - (hardwired) theory input spectrum for r = 0.1: camb_wmap5yr_withB_uK.txt
# Outputs:
#        - a text file output of the likelihood
#
# Calls:
#        - bicep1_utils.py: all the utility functions.
#
# Currently limitation:
#   - only tested for field B
#
#$Id #

from optparse import OptionParser

# Execution starts here
if __name__ == '__main__':
    usage =   '''
    This is the top level function to produce a HL likelihood on r using BICEP data products.
    Refer to  http://lanl.arxiv.org/abs/1310.1422 for details.
    This will output the HL likelihood    
    For bug reports and feature requests, contact dbarkats [at] alma.cl or kkarkare [at] cfa.harvard.edu
    '''

    # Initialize parser
    parser = OptionParser()

    parser.add_option("-r", "--rlist", dest="rlist",
                      help="Specify a single value or range of tensor-to-scalar values r to calculate the likelihood for. Default is None and uses the hardwired rlist for the experiment. Format is a single value of r or a comma-separated range: rmin,rmax,rstep (ie. -1,5,0.001)")

    parser.add_option("-f", "--fields", dest="fields",default = 'B',
                      help="Specify one of the following fields: T,E,B,TE,TB,EB,TEB,  B is default.")

    parser.add_option("-e", "--exp", dest="experiment",default = 'bicep1',
                      help="Specify one of the following experiments to work with: only works for bicep1. Default is bicep1")
    
    (options, args) = parser.parse_args()

# Imports
import numpy as np
import sys
from numpy import linalg as LA

# BICEP-specific module
import bicep1_util as b1u  

# define hard-wired defaults
field = options.fields   # field to calculate likelihood on
if field != 'B' and field !='E' and field !='EB':
    print "only field = E, B, or EB are available for now"
    sys.exit()
exp = options.experiment
if  options.rlist == None:
    if exp == 'bicep1':
        rlist = np.arange(-1.2,5.01,0.01) # default r values to evaluate for B1
else:
    orlist = options.rlist.split(',')
    if np.size(orlist) == 1:
        rlist = np.array([np.float(options.rlist)])
    elif np.size(orlist) == 3:
        rlist = np.arange(np.float(orlist[0]),np.float(orlist[1])+np.float(orlist[2]),np.float(orlist[2]))
    else:
        print 'Format is a single value of r or a comma separate range: rmin,rmax,rstep (ie. -1,5,0.001) '
        sys.exit()
        
# load the data products and bandpower window function from disk:
C_l, C_l_hat, N_l, C_fl, M_inv, bpwf_l, bpwf_Cs_l =  b1u.init(exp, field)

# Load the input r = 0.1 BB spectrum
file_in = "camb_wmap5yr_withB_uK.txt"
(ell,BB_Cs_l) = b1u.load_cmbfast(file_in)

# Load the LCDM + lensing spectra
lensing = "camb_wmap5yr_lensed_uK.txt"
(ell1,inpmodLensing_Cs_l) = b1u.load_cmbfast(lensing)
expvLensing = b1u.calc_expvals(ell1,inpmodLensing_Cs_l,bpwf_l,bpwf_Cs_l)

# Load the LCDM input spectra
LCDM = "camb_wmap5yr_noB_uK.txt"
(ell1,inpmodLCDM_Cs_l) = b1u.load_cmbfast(LCDM)
expvLCDM = b1u.calc_expvals(ell1,inpmodLCDM_Cs_l,bpwf_l,bpwf_Cs_l)

# Initiating the logarithmic likelihood arrays
logLike = np.zeros(len(rlist))

# loop over r values
print "### Evaluating the HL likelihood..."
for i in range(0,len(rlist)):

    expv = expvLCDM.copy()
    
    r  =  rlist[i]
    # Scale the BB spectrum from fiducial model (r=0.1) and get its expectation value
    this_mod = BB_Cs_l.copy()
    this_mod[:,3] = np.multiply(this_mod[:,3],r/0.1)
    expvBB = b1u.calc_expvals(ell,this_mod,bpwf_l,bpwf_Cs_l)

    # Add the scaled BB + the fixed lensing BB to the LCDM
    expv[:,3] = expv[:,3]+ expvBB[:,3] + expvLensing[:,3]
    
    # Choose the relevant input spectra expecation value according to field
    if field == "T":
        C_l[:,0,0] = expv[:,0]
    elif field == 'E':
        C_l[:,0,0] = expv [:,2]
    elif field == 'B':
        C_l[:,0,0] = expv[:,3] 
    elif field == "EB":
        C_l[:,0,0] = expv[:,2]
        C_l[:,0,1] = expv[:,5]
        C_l[:,1,0] = expv[:,5]
        C_l[:,1,1] = expv[:,3]
    elif field == "TB":
        C_l[:,0,0] = expv[:,0]
        C_l[:,0,1] = expv[:,4]
        C_l[:,1,0] = expv[:,5]
        C_l[:,1,1] = expv[:,3]
    elif field == "TE":
        C_l[:,0,0] = expv[:,0]
        C_l[:,0,1] = expv[:,1]
        C_l[:,1,0] = expv[:,1]
        C_l[:,1,1] = expv[:,2]
    
    # add Noise bias N_l to expectation values.
    C_l = C_l + N_l

    logL = b1u.evaluateLikelihood(C_l,C_l_hat,C_fl,M_inv)
    logLike[i]=np.real(logL)

    if i == len(rlist)-1:
        sys.stdout.write("Likelihood: %4.3f %8.5f  \n" % (r, logLike[i]) )
    else:
        sys.stdout.write("Likelihood: %4.3f %8.5f  \r" % (r, logLike[i]) )
        sys.stdout.flush()

# Saves the logarithmic likelihood to file
b1u.saveLikelihoodToText(rlist,logLike,field, exp = exp)

