function reduc_plotcovmat(finalspectra, pl) 
% produces some plots of the full and partial bpcm bandpower covariance
% matrix or correlation coefficients.
    
% exemple reduc_plotcovmat('sim004_filtp3_weight2_jack0')
  
if(~exist('pl','var'))
  pl=0;
end

  clf;
load(sprintf('final/%s',finalspectra));
   
% generate full bandpower covariance matrix and correlation matrix
if(0)
  for j=1:length(r)
    x=size(r(j).sim);
    snm(j).Cs_l=reshape(r(j).sim,[x(1)*x(2),x(3)]);
  end
  Cs_l=vertcat(snm(:).Cs_l);
  bpcm=cov(Cs_l');
  bpcc=corrcoef(Cs_l');
end

%number of individual lbins
nb=length(r(1).l);

%restrict to first 4 spectra TT,TE, EE,BB
f=4;
for j=1:length(r)
  x=size(r(j).sim);
  x=[x(1),f,x(3)];
  snm(j).Cs_l=reshape(r(j).sim(:,1:f,:),[x(1)*x(2),x(3)]);
end
Cs_l=vertcat(snm(:).Cs_l);
bpcc.full=corrcoef(Cs_l');


% grab only 100, 150, cross bpcm individually 
x=size(r(1).sim);
x=[x(1),f,x(3)];
Cs_l=reshape(r(1).sim(:,1:f,:),[x(1)*x(2),x(3)]);
bpcc.A=corrcoef(Cs_l');
Cs_l=reshape(r(2).sim(:,1:f,:),[x(1)*x(2),x(3)]);
bpcc.B=corrcoef(Cs_l');
Cs_l=reshape(r(3).sim(:,1:f,:),[x(1)*x(2),x(3)]);
bpcc.C=corrcoef(Cs_l');


%plot full corr matrix
figure(1)
setwinsize(gcf,900,700)
colormap gray;
imagesc(bpcc.full)
covaxis(nb,4,[1,2,3],8,-10)
caxis([-1 1]);
colorbar
axis xy
axis square
set(gca,'gridlinestyle','-')        
%set(gca,'xtick',[0:nb:end])
%set(gca,'ytick',[0:nb:end])  
printname=strcat(finalspectra,'_fullcovmat.gif');
if(pl) printfig(1,printname,'pp'); end


%plot zoom on 100 
figure(2)
clf;
setwinsize(gcf,400,300)
colormap gray;
imagesc(bpcc.A)
covaxis(nb,4,1,7,-7)
caxis([-1 1]);
colorbar
axis xy
axis square
set(gca,'gridlinestyle','-')        
%set(gca,'xtick',[0:nb:end])
%set(gca,'ytick',[0:nb:end])  
gtitle('100x100',0.95)
printname=strcat(finalspectra,'_100covmat.gif');
if(pl) printfig(2,printname,'pp'); end


%plot zoom on 150 
figure(3)
clf;
setwinsize(gcf,400,300)
colormap gray;
imagesc(bpcc.B)
covaxis(nb,4,2,-61,-7)
caxis([-1 1]);
colorbar
axis xy
axis square
set(gca,'gridlinestyle','-')   
gtitle('150x150',0.95)
%set(gca,'xtick',[0:nb:end])
%set(gca,'ytick',[0:nb:end])  
printname=strcat(finalspectra,'_150covmat.gif');
if(pl) printfig(3,printname,'pp'); end


%plot zoom on 100x150 
figure(4)
clf;
setwinsize(gcf,400,300)
colormap gray;
imagesc(bpcc.C)
covaxis(nb,4,3,-128,-7)
caxis([-1 1]);
colorbar
axis xy
axis square
set(gca,'gridlinestyle','-')   
gtitle('100x150',0.95)
%set(gca,'xtick',[0:nb:end])
%set(gca,'ytick',[0:nb:end])  
printname=strcat(finalspectra,'_crosscovmat.gif');
if(pl) printfig(4,printname,'pp'); end


return
end

function covaxis(nbands,nspectra,nfreq,x,y)
xtitle={'TT'  'TE' 'EE' 'BB','TB','EB'};
xcol=['r','b','g'];
nb=nbands;
f=nspectra;
for i=0:f-1
  for k=nfreq-1
    text(x+nb*i+nb*f*k,y,xtitle(i+1),'Color',xcol(k+1),'FontSize',14);
    text(y,x+nb*i+nb*f*k,xtitle(i+1),'Color',xcol(k+1),'FontSize',14);
    end
end



return
end
