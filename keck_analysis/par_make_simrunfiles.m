function sernum = par_make_simrunfiles(tags,simopt,nbase,daughter)
% par_make_simrunfiles(tags,simopt,nbase,daughter)
%
% Make a file for each simulation run containing pre-computed dither parameters. There
% are enough dithers for 1000 realizations.
%
% The number of detectors for the first and last tag must be the same.
%
% The following fields are ignored and/or set internally by the driver code:
%  - simopt.sernum
%  - simopt.rlz
%  - simopt.ditherstate
%  - simopt.state
%  - simopt.sig
%  - simopt.noise
%  - simopt.maketod
%  - simopt.sigmapfilename
%  - simopt.subsim
%  - simopt.update
%

% Check that number of detectors is the same for first and last tag.
[p1,ind1]=get_array_info(tags{1});
[p2,ind2]=get_array_info(tags{end});
if numel(ind1.e)~=numel(ind2.e)
  error(['Sim should be run on set of tags for which the number of detectors does not change.']);
end

% Make sure random number generator state is not set prior to getting dithers 
if isfield(simopt,'ditherstate')
  simopt=rmfield(simopt,'ditherstate');
end

% Get truly random numbers
randn('state',sum(1e6*clock));

% These fields get overwritten later, so we'll delete them now to avoid confusion
if isfield(simopt,'rlz')
  simopt=rmfield(simopt,'rlz');
end
if isfield(simopt,'sernum')
  simopt=rmfield(simopt,'sernum');
end
if isfield(simopt,'sig')
  simopt=rmfield(simopt,'sig');
end
if isfield(simopt,'noise')
  simopt=rmfield(simopt,'noise');
end
if isfield(simopt,'maketod')
  simopt=rmfield(simopt,'maketod');
end
if isfield(simopt,'sigmapfilename')
  simopt=rmfield(simopt,'sigmapfilename');
end
if isfield(simopt,'subsim')
  simopt=rmfield(simopt,'subsim');
end
if isfield(simopt,'update')
  simopt=rmfield(simopt,'update');
end

% Get enough dithers for 1000 realizations.
dither.simopt=simopt;
dither.simopt.rlz=1:1000;

% Get dithers
[dither.simopt, dither.p]=get_dithers(dither.simopt,ind1);

% Copy in ditherstate field
simopt.ditherstate=dither.simopt.ditherstate;

% Strip unnecessary fields because it's confusing
flds=fieldnames(dither.simopt);
for i=1:numel(flds)
  fld=flds{i};
  switch fld
   case {'rndeps','rndcen','rndsig','rndelc','rndelp','rndchi','abgain','rlz','ditherstate'}
    % do nothing, we want these fields
   otherwise
    dither.simopt=rmfield(dither.simopt,fld);
  end
end

% If directory doesn't exist, create it
if ~exist('simrunfiles','dir')
  system('mkdir simrunfiles');
end

% Save dithers, simopt
fname=sprintf(['simrunfiles/%04d_',daughter,'_dithers.mat'],nbase);
save(fname,'dither','simopt');

% Make sure this file doesn't get overwritten
setpermissions(fname,'a-w');

return


