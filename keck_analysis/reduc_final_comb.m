function reduc_final_comb(rf, lmax, pl)
% reduc_final_comb(rf)
%
% Combine 100/150/cross spectra
%
% For each bandpower of TT,TE,EE,BB we have 3 measurements from
% 100/150/cross. These differ in quality and are highly correlated - we
% want to make a weighted combine. We don't need to combine the
% uncertainty estimates as we will combine the signal+noise sims
% results also and measure the final bandpower covariance empirically.
%
% Also combine the BPWF and 1/W correction factor.
% This later is needed so we can measure the systematic change in that
% factor that comes about from changing the beam width by some percentage.
%
% Added some code to include systematic error due to beam uncertainty
% when calculating combination weights.
%
% e.g.
% reduc_final_comb('sim005_filtp3_weight2_jack0')

if ~exist('pl','var')
  pl=0;
end
   

% load the data set
load(sprintf('final/%s',rf));

% find jacktype
n=strfind(rf,'jack');
jacktype=str2num(rf(n+4));

% beam width and fractional uncertainty thereon
bw1=56/60/(sqrt(8*log(2))); bw2=36/60/(sqrt(8*log(2)));
bw=[bw1,bw2];
bwu=0.1;  %in percent ie 0.1 =10%.

% for each of 6 spectra
for i=1:6
  % for each bandpower
  for j=1:length(r(1).l)
    
    clear s d
    
    % collect the 100/150/cross s+n sim results
    s(:,1)=r(1).sim(j,i,:);
    s(:,2)=r(2).sim(j,i,:);
    s(:,3)=r(3).sim(j,i,:);
    
    % for TE, TB and EB include also the alternate freq cross spectrum
    switch i
     case 2
      s(:,4)=r(3).sim(j,7,:);
     case 5
      s(:,4)=r(3).sim(j,8,:);
     case 6
      s(:,4)=r(3).sim(j,9,:);
    end
   
    
    % take the covariance of the s+n sims
    c=cov(s);

    % calc beam uncertainty
    % calc B_l at nominal beam widths    
    l=r(1).l(j);
    bl=calc_bl(l,bw);
    
    % calc B_l at upward fluc beam widths
    blp=calc_bl(l,bw*(1+bwu));
    
    % calc W_l at 100/150 and W_l for cross spectra
    wl=[bl.^2,prod(bl)];
    wlp=[blp.^2,prod(blp)];
    
    % take ratio minus one as frac error due to beam fluc
    wlu=wl./wlp-1;
    
    if(i==2|i==5|i==6)
      wlu=[wlu,wlu(3)];
    end
    
    % store the values for combine
    rwl=1./wl; rwlp=1./wlp;
    for k=1:length(r)
      r(k).rwl(j,i)=rwl(k);
      r(k).rwlp(j,i)=rwlp(k);
    end
    
    switch i
     case 2
      r(3).rwl(j,7)=rwl(3);
      r(3).rwlp(j,7)=rwlp(3);
     case 5
      r(3).rwl(j,8)=rwl(3);
      r(3).rwlp(j,8)=rwlp(3);
     case 6
      r(3).rwl(j,9)=rwl(3);
      r(3).rwlp(j,9)=rwlp(3);
    end
    
    
    % add additional uncertainty due to beam
    if(0)
      
      % mult this by exp val (which is nearly the same for
      % 100/150/cross) to get sys err
      % we could use the obs bandpower but those are just a noisy
      % version of the expvals and will cause the weights to fluc
      % around even more than they do already
      e=wlu.*r(1).expv(j,i);

      % if we assume beam fluc is 100% correlated between 100/150 then
      % we should do:
      e=e'*e;
      % else we should do (although even if beam err uncorr there may
      % still be sys err corr between each of 100/150 and cross...)
      %e=diag(e.^2);
      
      % add to diag of cov mat
      c=c+e;
    end
    
    % form the weight as column sum of inv cov mat
    w=sum(inv(c));

    % normalize the weights to unity
    w=w./sum(w);

    % alternate craziness supression recipe suggested (I think) by
    % John Kovac
    % if any weight negative keep adding a small amount to the
    % diagonal until it's not
    while(any(w<0))
      c=c+diag(diag(c)*1e-2);    
      w=sum(inv(c));
      w=w./sum(w);
    end
       
    % craziness supression recipe I used in log post
    %if(sum(abs(w))>10)
    %  w=zeros(size(w));
    %  w(2)=1;
    %end
   
    % store the weights
    ws{i}(j,:)=w;
  end
    
end

% combine the fields of r according to the weights
for i=1:6
  rc.real(:,i)=comb_r({r.real},i,ws{i});
  rc.expv(:,i)=comb_r({r.expv},i,ws{i});
  rc.rwf(:,i)=comb_r({r.rwf},i,ws{i});
  
  rc.rwl(:,i)=comb_r({r.rwl},i,ws{i});
  rc.rwlp(:,i)=comb_r({r.rwlp},i,ws{i});
  
  rc.sim(:,i,:)=comb_r({r.sim},i,ws{i});
  rc.sig(:,i,:)=comb_r({r.sig},i,ws{i});
  rc.noi(:,i,:)=comb_r({r.noi},i,ws{i});
  
  if isfield(r,'simr');
    rc.simr(:,i,:)=comb_r({r.simr},i,ws{i});
  end
  rc.db(:,i,:)=comb_r({r.db},i,ws{i});
end
rc.l=r(1).l;
rc.ws=ws;

if(1)
% combine the fields of bpwf according to the weights
for i=1:4 % for each of TT/TE/EE/BB
  % combine the bpwf according to the weights
  for j=1:size(bpwf(1).Cs_l,2) % for each bandpower
    
    switch i
      case 1
      % TT simple 3 way collapse over freq
	    bpwfc.Cs_l(:,j,i)=sum([bpwf(1).Cs_l(:,j,i).*ws{i}(j,1),bpwf(2).Cs_l(:,j,i).*ws{i}(j,2),bpwf(3).Cs_l(:,j,i).*ws{i}(j,3)],2);
      case 2
      % TE 4 way collapse including ET
	    bpwfc.Cs_l(:,j,i)=sum([bpwf(1).Cs_l(:,j,i).*ws{i}(j,1),bpwf(2).Cs_l(:,j,i).*ws{i}(j,2),bpwf(3).Cs_l(:,j,i).*ws{i}(j,3),bpwf(3).Cs_l(:,j,7).*ws{i}(j,4)],2);
      %bpwfc.Cs_l(:,j,i)=sum([bpwf(1).Cs_l(:,j,i).*ws{i}(j,1),bpwf(2).Cs_l(:,j,i).*ws{i}(j,2),bpwf(3).Cs_l(:,j,i).*ws{i}(j,3)],2);

      case {3,4}
      % EE/BB 3 way collapse of XX->XX & XX->YY functions
      bpwfc.Cs_l(:,j,i)=sum([bpwf(1).Cs_l(:,j,i).*ws{i}(j,1),bpwf(2).Cs_l(:,j,i).*ws{i}(j,2),bpwf(3).Cs_l(:,j,i).*ws{i}(j,3)],2);
      bpwfc.Cs_l(:,j,i+2)=sum([bpwf(1).Cs_l(:,j,i+2).*ws{i}(j,1),bpwf(2).Cs_l(:,j,i+2).*ws{i}(j,2),bpwf(3).Cs_l(:,j,i+2).*ws{i}(j,3)],2);
	
    end
  end
end
bpwfc.l=bpwf(1).l;
end

% also combine the supression factor
supfacc.l=supfac(1).l;
supfacc.tt=sum([supfac(1).tt.*ws{1}(:,1),supfac(2).tt.*ws{1}(:,2),supfac(3).tt.*ws{3}(:,3)],2);
supfacc.te=sum([supfac(1).te.*ws{2}(:,1),supfac(2).te.*ws{2}(:,2),supfac(3).te.*ws{2}(:,3),supfac(3).et.*ws{2}(:,4)],2);
supfacc.ee=sum([supfac(1).ee.*ws{3}(:,1),supfac(2).ee.*ws{3}(:,2),supfac(3).tt.*ws{3}(:,3)],2);
supfacc.bb=sum([supfac(1).bb.*ws{4}(:,1),supfac(2).tt.*ws{4}(:,2),supfac(3).tt.*ws{3}(:,3)],2);
supfacc.tb=sqrt(supfacc.tt.*supfacc.bb);
supfacc.eb=sqrt(supfacc.ee.*supfacc.bb);
% and the rwf
tt=supfacc.tt; ee=supfacc.ee; bb=supfacc.bb;
te=supfacc.te; tb=supfacc.tb; eb=supfacc.eb;
supfacc.rwf=1./[tt,te,ee,bb,tb,eb];


% get the covariance from sims
r=get_bpcov(r);
rc=get_bpcov(rc);

if(pl)
% plot the 100/150/cross points and their combination
figure(1); clf
setwinsize(gcf,1200,800)
l={'TT','TE','EE','BB','TB','EB'};

if(jacktype==0)
  % ylim so we can see the signal spectra
  if (lmax == 500)
    yrange=[-500,8000;-150,150;-1,25;-.1,.5;-20,20;-1,1];
  elseif (lmax == 200)
    yrange=[-500,6000;-60,40;-.5,1.5;-.02,.05;-6,6;-.5,.5];
  end
else
  if (lmax == 500)
    yrange=[-5,5;-.2,.2;-.05,.05;-.05,.05;-.2,.2;-.05,.05];
  elseif (lmax == 200)
    yrange=[-5,5;-.1,.1;-.02,.02;-.02,.02;-.1,.1;-.02,.02];
  end
end

for i=1:6
  subplot(3,1,1)
  errorbar2(r(1).l-2,r(1).real(:,i),r(1).derr(:,i),'r.');
  hold on
  errorbar2(r(2).l,r(2).real(:,i),r(2).derr(:,i),'b.');
  errorbar2(r(3).l+5,r(3).real(:,i),r(3).derr(:,i),'g.');
  errorbar2(rc.l+2,rc.real(:,i),rc.derr(:,i),'k.')  
  switch i
    case 2
      errorbar2(r(3).l+7,r(3).real(:,7),r(3).derr(:,7),'c.');
    case 5
      errorbar2(r(3).l+7,r(3).real(:,8),r(3).derr(:,8),'c.');
    case 6
      errorbar2(r(3).l+7,r(3).real(:,9),r(3).derr(:,9),'c.');
  end
  plot(rc.l,rc.expv(:,i),'yx')
  plot(inpmod.l,inpmod.Cs_l(:,i),'r');
  hold off
  grid
  xlim([0,lmax]);
  ylim(yrange(i,:));
  title(sprintf('%s - red=Bicep2, blue=Keck, green=cross, black=combined, cyan=alt cross',l{i}))
  xlabel('multipole'); ylabel('power')
  
  subplot(3,1,2)
  plot(rc.l,rc.derr(:,i)./r(1).derr(:,i),'.-');
  ylim([0,1])
  xlim([0,lmax]);
  xlabel('multipole'); ylabel('combined error bar / Bicep2 error bar')
  grid
  
  subplot(3,1,3)
  plot(rc.l,ws{i}(:,1),'r'); hold on;
  plot(rc.l,ws{i}(:,2),'b');
  plot(rc.l,ws{i}(:,3),'g');
 switch i
    case 2
      plot(rc.l,ws{i}(:,4),'c');
    case 5
     plot(rc.l,ws{i}(:,4),'c');
    case 6
     plot(rc.l,ws{i}(:,4),'c');
  end
  ylim([0,1]);
  legend({'Bicep2','Keck','cross','alt cross'});
  xlabel('multipole'); ylabel('combination weights')
  xlim([0,lmax]);
  grid
  hold off;
  fname=sprintf('final/%s_comb_%s_%i.png',rf,l{i},lmax); fname=strrep(fname,'.mat','');
  mkpng(fname); 
end

if(0)
  % hand tuned effective beam width for the combined spectra
  bw=47/60/(sqrt(8*log(2)));


  figure(2); clf
  plot(r(1).l,r(1).rwf(:,1),'r--')
  hold on
  plot(r(1).l,r(2).rwf(:,1),'b--')
  plot(r(1).l,r(3).rwf(:,1),'g--')
  plot(rc.l,rc.rwf(:,1),'k--')
  l=2:lmax; plot(l,1./calc_bl(l,bw).^2,'k');
  %plot(rc.l,rc.rwl);
  %plot(rc.l,rc.rwlp);
  hold off
  ylim([0,100]);
  grid
  xlabel('multipole'); ylabel('reciprocal of window function');


  figure(3); clf
  plot(rc.l,rc.rwlp./rc.rwl-1);
  hold on
  l=2:lmax;
  plot(l,exp((bw*pi/180)^2*(bwu^2+2*bwu)*l.*(l+1))-1,'k')
  hold off
end

if(1)
% re-calc the expvals using the combined BPWF to check they match the
% combined expvals
rcr=calc_expvals(rc,inpmod,bpwfc);

  figure(4); clf
  % check if combined result unbiased
  for i=1:6
    plot(r(1).l,r(1).expv(:,i,:),'or');
    hold on
    plot(r(1).l,r(2).expv(:,i,:),'ob');
    plot(r(1).l,r(3).expv(:,i,:),'og');
    if(i==2)
      plot(r(1).l,r(3).expv(:,7,:),'om');
    end
    plot(rc(1).l,rc.expv(:,i,:),'ok');
    plot(rc(1).l,rcr.expv(:,i,:),'xk');
    
    plot(r(1).l,mean(r(1).sig(:,i,:),3),'r');
    plot(r(1).l,mean(r(2).sig(:,i,:),3),'b');
    plot(r(1).l,mean(r(3).sig(:,i,:),3),'g');
    if(i==2)
      plot(r(1).l,mean(r(3).sig(:,7,:),3),'m');
    end
    plot(rc.l,mean(rc.sig(:,i,:),3),'k');
    hold off
    grid
    
    %pause
  end

end
end % if(pl)

% save the combined results
bpwf=bpwfc;
r=rc;
rf=strrep(rf,'.mat','')
supfac=supfacc;
save(sprintf('final/%s_debias_comb',rf),'r','bpwf','inpmod','supfac','opt');

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xc=comb_r(x,i,ws)

% for each realization
for j=1:size(x{1},3)
  switch i
    case {1,3,4} % TT,EE,BB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j)].*ws,2);
    case 2 % TE
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,7,j)].*ws,2);
    case 5 % TB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,8,j)].*ws,2);
    case 6 % EB
      xc(:,j)=sum([x{1}(:,i,j),x{2}(:,i,j),x{3}(:,i,j),x{3}(:,9,j)].*ws,2);
      
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function x=gen_corr_rnd(c,s)

x=randn(s);
c=chol(c);
x=x*c;

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function bl=calc_bl(l,bw)

for i=1:length(bw)
  b=(pi/180)*bw(i); % deg to radians
  b=1/b; % go to Fourier space
  %bw=bw*2*pi; % conv to ell space <= this is not needed - why not??? see:
  % http://books.google.com/books?id=iBc9TmNLD7kC&pg=PA347&lpg=PA347&dq=gaussian+beam+window+function&source=web&ots=rsVqoBliUW&sig=TlI6fUctMQp0xuQmFaCwExUDhYo
  
  bl(:,i)=gauss([1,0,b],l);
end

return
