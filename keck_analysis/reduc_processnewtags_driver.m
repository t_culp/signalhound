function reduc_processnewtags_driver(uptodate,farmstuff,autoflow,updatetaglist,onlynewtods,allmissing,interactive,missingdate,dotunings,cpfilestobicepfs1,lastmassreduc,savedcut)
% reduc_processnewtags_driver(uptodate,farmstuff,autoflow,updatetaglist,onlynewtods,allmissing,interactive,missingdate,dotunings)
% 
% inputs
%       uptodate:           Process most recent tags starting with uptodate 
%                               (yyyy-mmm-dd:HH:MM:SS or yyyy-mmm-dd ,
%                               defaults to beginning of last day in
%                               tag_list.csv)
%
%       farmstuff:          Set to 1 to farm jobs (default 0)
%                           Set to N to farm N simultaneous jobs on spud
%
%       autoflow:           Set to 1 to automatically proceed if no bjobs are
%                               running (default 0).
%
%       updatetaglist:      Set to 1 to run reduc_make_tag_list (default 0)
%
%       onlynewtods:        Set to 1 to process only new TODs (default 0) and
%                               those data products stemming from the new TODs.
%                               When checking for missing data products, the
%                               presence of an old file is NOT sufficient for
%                               "not missing" status.
%
%       allmissing:         Set to 1 to process all missing data products.
%                               Process new TODs (overrides onlynewtods); 
%                               process all the data products stemming from the
%                               newly processed TODs AND process all missing
%                               data products (pairmaps, reducplots, per-phase
%                               maps) even if they do not stem from newly
%                               processed TODs.  When checking for missing data
%                               products, the presence of an old file IS
%                               sufficient for "not missing" status.  
%                                    
%       interactive:        Set to 0 to turn off interactive mode, i.e. respond
%                               'y' to all (default 1)
%
%       missingdate:        Same format as uptodate; if supplied, data products
%                               created after this date will we treated as "not
%                               missing". When running with onlynewtods=0, use
%                               this date to re-run reduc_processnewtags_driver
%                               to avoid processing TODs that were created
%                               earlier. (By default, the missingdate would have
%                               have been "now.") When running with
%                               allmissing=1, use this date to flag "old" data
%                               products as missing. (By default, the
%                               missingdate would have been some time long ago
%                               in the past.) 
%
%       dotunings:          Default 1. If 0, do not update squid tuning plots.
%
%       cpfilestobicepfs1:  Set to string to copy reducplots to that directory
%                               after generation. This is for Keck, where the
%                               reducplots are generated on panlfs because of 
%                               time issues and copies to bicepfs1 for web
%                               viewing.
%                               Default: empty for B2 and B3, 
%                               Keck: '/n/bicepfs1/keck/pipeline/reducplots/'
%
%       lastmassreduc:      Date of last cutparams mass reduction. This is to
%                               prevent cutparam files that don't have the most
%                               recent cutparams in them from stopping the 
%                               cutplot generation.  Default: '2012-Aug-01'
% 
%       savedcut:           This is an option to save and use saved concatenated
%                               cutparam file for the entire year. This means
%                               that it does not re-evaluate the cuts for all
%                               tags everytime.  
%
% Code to generate reducplots at pole.  based on instructions by RWO.
%
% ex. reduc_processnewtags_driver([],1,1,1,1);
%
% JAB 20100921
% v2 RWA 20110621


% Condition input arguments
if(~exist('uptodate','var') || isempty(uptodate))
  p=ParameterRead('aux_data/tag_list.csv');
  % make sure uptodate starts is the last day in taglist
   uptodate=datenum(p.tstart{end},'dd-mmm-yyyy:HH:MM:SS');
   uptodate=datestr(floor(uptodate),'yyyy-mmm-dd:HH:MM:SS');
end
if(~exist('farmstuff','var') || isempty(farmstuff))
  farmstuff=0;
end
if(~exist('autoflow','var') || isempty(autoflow))
  autoflow=0;
end
if(~exist('updatetaglist','var') || isempty(updatetaglist))
  updatetaglist=0;
end
if(~exist('onlynewtods','var') || isempty(onlynewtods))
  onlynewtods=0;
end
if(~exist('allmissing','var') || isempty(allmissing))
  allmissing=0;
end
if(allmissing)
  onlynewtods=1;
end
if(~exist('interactive','var') || isempty(interactive))
  interactive=1;
end
if(~exist('missingdate','var'))
  missingdate=[];
end
if(~exist('dotunings','var') || isempty(dotunings))
  dotunings=1;
end
if(~exist('cpfilestobicepfs1','var'))
  cpfilestobicepfs1=[];
end
if(~exist('lastmassreduc','var'))
  lastmassreduc=[];
end
if(~exist('savedcut','var') || isempty(savedcut))
  savedcut=1;
end

%What experiment are we?
experiment=get_experiment_name();

% %Set cpfilestobicepfs1 for keck
% if(isempty(cpfilestobicepfs1) & strcmp(experiment,'Keck'))
%   cpfilestobicepfs1='/n/bicepfs2/keck/pipeline/reducplots/';
% end

%last time the cutparams were mass generated
if(isempty(lastmassreduc))
  if strcmp(experiment,'Keck');
    lastmassreduc='2012-Sep-12';
  else
    lastmassreduc='2012-Aug-01';
  end
end

% Run the star pointing analysis
reduc_starpoint([],[],datestr(utc2datenum(uptodate),'yyyymmdd'));

% reduce/plot squid tunings
if dotunings
  disp('generating squid tunings');
  squid_tuning_driver(experiment);
end

% Update the tag list
if updatetaglist
  disp('update taglist...');
  unix(['cp aux_data/tag_list.csv aux_data/tag_list.tmp.csv']);
  reduc_make_tag_list('log','aux_data/tag_list.tmp.csv','aux_data/tag_stats.tmp.csv',uptodate,'');
  unix(['cp aux_data/tag_list.tmp.csv aux_data/tag_list.csv']);
  disp(['Updated tag list.  You might want to check it in to CVS.']);
  unix(['cp aux_data/tag_stats.tmp.csv aux_data/tag_stats.csv']);
  disp(['Updated tag stats file.  You might want to check it in to CVS.']);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate the html in pipeline/reducplots
disp('generating tag browser html...')
%reduc_make_tag_browser([],[],cpfilestobicepfs1,[],farmstuff,uptodate);
reduc_make_tag_browser([],[],cpfilestobicepfs1,[],0,uptodate);

% Get tags from uptodate to present
if onlynewtods
  tags = tags_inrange(uptodate,'new');
else
  tags = tags_inrange(uptodate);
end

% If we are Keck, run with more memory and alloted queue time on odyssey in some
% circumstances. Tradeoff is slower queueing.
if strcmp(experiment,'keck') || strcmp(experiment, 'bicep3')
  serial_requeue=1;
  more_mem=1;
else
  serial_requeue=1;
  more_mem=0;
end

% TODs made before this time will not count when checking if missing
startd=now;
if ~isempty(missingdate)
  startd=datenum(missingdate,'yyyy-mmm-dd');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Process tags in reduc_initial and generate tag plots
if(interactive)
  s = input(['Process ' num2str(length(tags)) ' tags within date range?'],'s');
end
if ~interactive || strcmp(s,'yes') || strcmp(s,'y')
  disp('process tags per scanset...');
  if(~isempty(tags))
    reduc_processnewtags(tags,farmstuff,more_mem,0,0,cpfilestobicepfs1);
    waitnextstep('reduc_batch1',farmstuff,autoflow);
  end
end

% Check for missing TODs, and if requested, reprocess
if ~isempty(missingdate)
  disp(sprintf('TODs created after %s will not be reprocessed',missingdate));
end
if(interactive)
  s = input('Check for missing TODs among those that should have been processed?','s');
end
if ~interactive || strcmp(s,'yes') || strcmp(s,'y')
  missing = missing_tods(startd,tags);
  while ~isempty(missing)
    disp(['Found ' num2str(length(missing)) ' tags within range missing tods.'])
    if(interactive)
      s = input('Resubmit missing?','s');
    end
    if ~interactive || strcmp(s,'yes') || strcmp(s,'y')
      disp('resubmitting missing tags...')
      reduc_processnewtags(missing,farmstuff,more_mem,0,0,cpfilestobicepfs1);
      waitnextstep('reduc_batch1',farmstuff,autoflow);
      missing = missing_tods(startd,tags);
    else
      missing = [];
    end
  end
end

% TOD reduction summary
disp('TOD reduction summary...');
disp([num2str(numel(tags)) ' tags should have just been newly processed.'])
% Grab all tags within date range with TODs and cutparams files, not just newly processed tags
tagswithtods = tags_inrange(uptodate,{'has_tod','has_cuts'});
% Tags that should have been updated and have TODs
newtagstmp=intersect(tags,tagswithtods);
disp([num2str(numel(newtagstmp)) ' of these tags have TODs.'])
% Tags that actually were updated and have TODs
[missing,newtags] = missing_tods(startd,newtagstmp);
disp([num2str(numel(newtags)) ' of these tags have newly processed TODs.'])

if(~allmissing)
  % Look for obsolete data products among TODs that were newly processed
  tags=newtags;
  whichtags='the successfully newly processed';
else
  if ~isempty(missingdate)
    % Look for missing data prodcuts among TODs generated 
    tags=tagswithtods;
    whichtags='all existing';
    startd=datenum(missingdate,'yyyy-mmm-dd');
  else
    % Look for for missing data products among ALL TODs within date range
    tags=tagswithtods;
    whichtags='all existing';  
    startd=0;
  end
end

if ~isempty(missingdate)
  disp(sprintf('from here forward, data products created after %s will not be reprocessed',missingdate));
end

% Check for missing reduc plots, and if requested, reprocess
if(interactive)
  s = input(['Check for missing reduc plots among ' whichtags ' TODs within date range?'],'s');
end
if ~interactive || strcmp(s,'yes') || strcmp(s,'y')
  missing = missing_reducplots(startd,tags);
  while ~isempty(missing)
    disp(['Of ' whichtags ' ' num2str(numel(tags)) ... 
          ' tags with TODs+cutparams, found ',...
          num2str(length(missing)) ' missing reducplots'])
    if(interactive)
      s = input('Resubmit missing?','s');
    end
    if ~interactive || strcmp(s,'yes') || strcmp(s,'y')
      disp('resubmitting missing reduc plots...')
      reduc_processnewtags(missing,farmstuff,more_mem,1,0,cpfilestobicepfs1);
      waitnextstep('reduc_batch1',farmstuff,autoflow);
      missing = missing_reducplots(startd,tags);
    else
      missing = [];
    end
  end
end

% Check for missing pairmaps
if(interactive)
  s = input(['Check for missing pairmaps among ' whichtags ' tags?'],'s');
end
if ~interactive || strcmp(s,'yes') || strcmp(s,'y')
  missing=missing_pairmaps(startd,tags);
  while ~isempty(missing)
    disp(['Of ' whichtags ' ' num2str(numel(tags)) ... 
          ' tags with TODs+cutparams, found ',...
          num2str(length(missing)) ' missing pairmaps'])
    if(interactive)
      s = input('Resubmit missing?','s');
    end
    if ~interactive || strcmp(s,'yes') || strcmp(s,'y')
      disp('resubmitting missing tags...')
      reduc_processnewtags(missing,farmstuff,more_mem,0,1,cpfilestobicepfs1);
      waitnextstep('reduc_batch1',farmstuff,autoflow);
      missing = missing_pairmaps(startd,tags);
    else
      missing = [];
    end
  end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do per-phase processing; generate T/Q/U + dark maps
disp('process tags per phase...');
ph=taglist_to_phaselist(newtags);
disp(['Processing ' num2str(numel(ph.name)) ...
      ' phases containing at leat one newly processed TOD (mandatory)'])
if(~isempty(ph.name))
  reduc_processnewtags_perphase(ph.name,farmstuff,serial_requeue,more_mem,cpfilestobicepfs1);
  waitnextstep('reduc_batch2',farmstuff,autoflow);
end

% Check for missing per-phase reduc plots, and if requested, reprocess
if (interactive)
  s = input(['Check for missing per-phase map reduc plots among ' ...
          whichtags ' pairmaps within date range?'],'s'); 
end
if ~interactive || strcmp(s,'yes') || strcmp(s,'y')
  % these phases have at least one existing pairmap
  [dum,existing_pairmaps]=missing_pairmaps(startd,tags);
  ph=taglist_to_phaselist(tags);
  existing_phases=ph.name;
  % these phases have at least one existing pairmap but no reducplot
  missing = missing_perphase_reducplots(startd,existing_phases);
  while ~isempty(missing)
    disp(['For ' whichtags ' pairmaps, found ',...
          num2str(length(missing)) ' corresponding per-phase reducplots that are missing'])
    if(interactive)
      s = input('Resubmit missing?','s');
    end
    if ~interactive || strcmp(s,'yes') || strcmp(s,'y')
      disp('resubmitting missing coadds/reduc plots...')
      reduc_processnewtags_perphase(missing,farmstuff,serial_requeue,...
	                                more_mem,cpfilestobicepfs1);
      waitnextstep('reduc_batch2',farmstuff,autoflow);
      missing = missing_perphase_reducplots(startd,existing_phases);
    else
      missing = [];
    end
  end
end

if(exist('existing_phases','var'))
missing = missing_perphase_reducplots(startd,existing_phases);
else
missing=[];  
end
disp(['There are ' num2str(numel(missing)) ' missing per-phase map reduc plots among ' ...
     'the phases that have at least one existing pairmaps among' whichtags ...
      ' tags within date range.']);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% => done when update tags via reduc_make_tag_list.m
% Generate stats, including cryo service and per-phase calibration tags
% disp('Update tag stats');

% ri=get_run_info;

% get rid of tags before uptodate
% uptodatenum=utc2datenum(uptodate);
% ridatenum=datenum(ri.tstart,'dd-mmm-yyyy:HH:MM:SS');
% ri=structcut(ri,ridatenum>=uptodatenum);

% reduc_calc_stats(ri.tag);
% disp('reduc_calc_stats finished.  Dont forget to commit changes to tag_stags.csv to CVS')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate cal plots
disp('Generate cal plots for newly processed TODs (mandatory)');

reduc_farm_calplots(newtags,farmstuff,cpfilestobicepfs1);

% Check for missing cal plots
if(interactive)
  s = input(['Check for missing stats+calplots among ' whichtags ' TODs within date range?'],'s');
end
if ~interactive || strcmp(s,'yes') || strcmp(s,'y')
  missing=missing_calplots(startd,tags);
  while ~isempty(missing)
    disp(['There are ' num2str(length(missing)) ...
          ' tags missing either stats or cal plots.'])
    disp(['There are ' num2str(numel(unique(parse_tag(missing)))) ' total missing cal plots.']);
    if(interactive)
      s = input('Resubmit missing?','s');
    end
    if ~interactive || strcmp(s,'yes') || strcmp(s,'y')
      disp('resubmitting missing phases...')
      reduc_farm_calplots(missing,farmstuff,cpfilestobicepfs1);
      waitnextstep('reduc_plotcals',farmstuff,autoflow);
      missing = missing_calplots(startd,tags);
    else
      missing = [];
    end
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate cutplots
disp('Generate cutplots');

% what year are we?
day=parse_tag(tags);
day=num2str(day(1));
year=day(1:4);

% do entire year up to date
cutplottags=get_tags(['all' year],'has_cuts');

%cut out the cutparams made before the last mass reduction 
cutplotstartd=datenum(lastmassreduc,'yyyy-mmm-dd');
[missing cutplottags]=missing_tods(cutplotstartd,cutplottags);

%want to farm this out
if farmstuff
  LICENSE = 'bicepfs1:17'; % 1500 slots -> 1500/17 = 88 jobs
  
  if serial_requeue
    QUEUE='serial_requeue';
    MAXTIME=1440; % specify the runtime limit to avoid error in case the monthly maintenance is nearby.
  else
    QUEUE='general';
    MAXTIME=1440*7; % specify the runtime limit to avoid error in case the monthly maintenance is nearby.
  end

  if more_mem
    % 20130926 GPT - 15000 just isn't enough any more.
    MEM=30000;
  else
    MEM=8000;
  end
end

% if using the previously save structure
if savedcut
  filename=['data/real/' year '_cutparams.mat'];
  if exist(filename,'file')
    load(filename)

    %cut out tags already processed
    cutplottags=setdiff(cutplottags,cut_tags);

    %also make sure they are all after the last already included tag
    ri=get_run_info(cutplottags);
    ri_cut=get_run_info(cut_tags);
    ridatenum=datenum(ri.tstart,'dd-mmm-yyyy:HH:MM:SS');
    lastdatenum=datenum(ri_cut.tstart(end),'dd-mmm-yyyy:HH:MM:SS');
    ri=structcut(ri,ridatenum>=lastdatenum);

    cutplottags=cat(2,year,ri.tag');
  end

  [farmfile,jobname] = farmfilejobname('BICEP', 'reduc_plotcuts', ...
      cutplottags{1});

  %make cutplots
  if farmstuff
    farmit(farmfile,...
           ['reduc_plotcuts(cutplottags,[],[],[],0,1,1,'...
              '''reduc_plotcuts_online/' num2str(year) ''',''' filename ...
              ''',1);'],...
           'queue',QUEUE, 'mem',MEM, 'jobname',jobname, ...
           'license',LICENSE, 'account','bicepdata_group', ...
           'var','cutplottags','maxtime',MAXTIME,...
           'overwrite',false); % specify the runtime limit to avoid error in case the monthly maintenance is nearby.
  else
    reduc_plotcuts(cutplottags,[],[],[],0,1,1,['reduc_plotcuts_online/' num2str(year)],filename,1);
  end

else

  [farmfile,jobname] = farmfilejobname('BICEP', 'reduc_plotcuts', ...
      cutplottags{1});

  % make cutplots (compress halfscans)
  if farmstuff
    farmit(farmfile, ...
           ['reduc_plotcuts(cutplottags,[],[],[],0,1,1,' ...
               '''reduc_plotcuts_online/' num2str(year) ''',[],1);'], ...
           'queue',QUEUE, 'mem',MEM, 'jobname',jobname, ...
           'license',LICENSE, 'account','bicepdata_group', ...
           'var','cutplottags',...
           'overwrite',false);
  else
    reduc_plotcuts(cutplottags,[],[],[],0,1,1,['reduc_plotcuts_online/' num2str(year)],[],1);
  end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate squid tuning html
if dotunings
  disp('generating squid tuning browser html...');
  tuning_browser_html;
end

return


function tags=tags_inrange(uptodate,tod_opt)

if nargin == 1
    tags = get_tags('all');
else
    tags = get_tags('all',tod_opt);
end

ri=get_run_info(tags);

% get rid of tags before uptodate
uptodatenum=utc2datenum(uptodate);
ridatenum=datenum(ri.tstart,'dd-mmm-yyyy:HH:MM:SS');
ri=structcut(ri,ridatenum>=uptodatenum);

tags=ri.tag;

return


function [missing,present]=missing_pairmaps(startd,tags)

missingind=false(size(tags));
for i=1:numel(tags)
  d=dir(sprintf('pairmaps/0000/real/%s_*.mat',tags{i}));
  if isempty(d)
    missingind(i)=true;
  elseif d.datenum<startd
    missingind(i)=true;
  end
end
missing=tags(missingind);
present=tags(~missingind);

return


function [missing,present]=missing_tods(startd,tags)

missingind=false(size(tags));
for i=1:numel(tags)
  d=dir(sprintf('data/real/%s/%s_cutparams.mat',tags{i}(1:6),tags{i}));
  if isempty(d)
    missingind(i)=true;
  elseif d.datenum<startd
    missingind(i)=true;
  end
end
missing=tags(missingind);
present=tags(~missingind);

return


function missing=missing_reducplots(startd,tags)
% The last reducplot to process is *_001_cov_round2.png.
missingind=~has_tod(tags,'reducplots/','_001_cov_round2.png');
for i=1:numel(tags)
  % If the plots are not missing, verify that they have been generated after
  % the given start date (which is probably "now" when the driver was
  % first executed).
  if ~missingind(i)
    subdir = tags{i}(1:6);
    d=dir(sprintf('reducplots/%s/%s_001*.png',subdir,tags{i}));
    if d(1).datenum<startd
      missingind(i)=true;
    end
  end
end

missing=tags(missingind);

return


function missing=missing_perphase_reducplots(startd,tags)
% takes phase tags

missingind=false(size(tags));
for i=1:numel(tags)
  subdir = tags{i}(1:6);
  d=dir(sprintf('reducplots/%s/perphase_%s_001.png',subdir,tags{i}));
  if isempty(d)
    missingind(i)=true;
  elseif d.datenum<startd
    missingind(i)=true;
  end
end
missing=tags(missingind);

return


function missing=missing_calplots(startd,tags)

missingind=false(size(tags));
for i=1:numel(tags)
  day=parse_tag(tags{i});
  subdir = tags{i}(1:6);
  d=dir(sprintf('reducplots/%s/cal_%d.png',subdir,day));
  if isempty(d)
    missingind(i)=true;
  elseif d.datenum<startd
    missingind(i)=true;
  end
end
missing=tags(missingind);

return
