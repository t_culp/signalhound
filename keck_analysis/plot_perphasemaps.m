function plot_perphasemaps(phase_tag, out, rx, allfreq)
% plot_perphasemaps(phase_tag,out,rx)
% phase_tag='YYYYMMDDP_dk###'
% out = 0  print to screen
%     = 1  print to file
% rx = 'all' plots coadd over all receivers (default)
%    = n plots only for receiver n
% allfreq = true   Generates an "all-frequency" plot, even if given single rx
%         = false  Normal per-rx or per-freq heuristics apply
% 
% e.g. plot_perphasemaps('20110312B_dk113',1)

[p,ind]=get_array_info(phase_tag);

if ~exist('out','var') || isempty(out)
  out=0; 
end

if ~exist('rx','var') || isempty(rx)
  rx=unique(p.rx);
end

if ~exist('allfreq','var') || isempty(allfreq)
  if numel(rx) > 1
    allfreq = true;
  else
    allfreq = false;
  end
end

indrx=strip_ind(ind,find(ismember(p.rx,rx)));

% Stripping ind should leave only one frequency behind (assuming each receiver
% is a single frequency).
freq = unique(p.band(indrx.rgl));

% don't plot to screen if writing a png file
if(out==1); 
  set(0,'DefaultFigureVisible','off');
end

% plot T/Q/U maps
setwinsize(gcf,1100,600)
clf

colormap jet;

% If plotting all receivers, may use the fully coadded map, otherwise use
% by-rx coadded maps. Both methods agree to better than 1 part in 1e10.
fname = sprintf('maps/0000/real_%s_filtp3_weight3_gs', phase_tag);
if (numel(rx) == numel(unique(p.rx))) && ~exist([fname '_jack01.mat'])
  fname = [fname '_jack0.mat'];
else
  fname = [fname '_jack01.mat'];
end

if numel(rx) == 1
  i=rx+1;
else
  i=1;
end

% load map
[map,m,coaddopt]=load_and_cal_map(phase_tag,fname,rx);


% plot maps
subplot(2,3,1)
plot_perphase_panel(map(i,1).T,m,sprintf('%i GHz T (\\muK)',freq));

subplot(2,3,4)
plot_perphase_panel(map(i,1).Q,m,sprintf('%i GHz Q (\\muK)',freq));

subplot(2,3,5)
plot_perphase_panel(map(i,1).U,m,sprintf('%i GHz U (\\muK)',freq));

% plot signal histogram
subplot(2,3,3)
plot_perphase_histogram({map(i,1).T,map(i,1).Q,map(i,1).U}, -250:2.5:250, ...
    {'T','Q','U'}, sprintf('%i GHz T/Q/U SIGNAL HISTOGRAMS',freq))

% plot noise histogram
tnoise=nanmedian(sqrt(map(i,1).Tvar(:)));
qnoise=nanmedian(sqrt(map(i,1).Qvar(:)));
unoise=nanmedian(sqrt(map(i,1).Uvar(:)));  

tlegtext=sprintf('T_{med}=%0.1f',tnoise);
qlegtext=sprintf('Q_{med}=%0.1f',qnoise);
ulegtext=sprintf('U_{med}=%0.1f',unoise);  

subplot(2,3,6)
plot_perphase_histogram({sqrt(map(i,1).Tvar),sqrt(map(i,1).Qvar),sqrt(map(i,1).Uvar)},...
    0:1:100,{tlegtext,qlegtext,ulegtext}, ...
    sprintf('%i GHz T/Q/U NOISE HISTOGRAMS',freq));
hold on
yl=ylim();
plot([tnoise,tnoise],[yl(1),yl(2)],'k--')
plot([qnoise,qnoise],[yl(1),yl(2)],'b--')
plot([unoise,unoise],[yl(1),yl(2)],'r--')  
hold off

% plot jackknife map
fname = sprintf('maps/0000/real_%s_filtp3_weight3_gs', phase_tag);
if (numel(rx) == numel(unique(p.rx))) && ~exist([fname '_jack21.mat'])
  fname = [fname '_jack2.mat'];
else
  fname = [fname '_jack21.mat'];
end

% load jackknife map
[map,m,coaddopt]=load_and_cal_map(phase_tag,fname,rx);
map=jackknife_map(map);

% plot jackknife maps
subplot(2,3,2)
plot_perphase_panel(map(i).T,m,sprintf('%i GHz T SCAN JACKKNIFE (\\muK)',freq));
cax=caxis();

% figure out how many scansets were co-added, and how many should have been
nscansets=length(coaddopt.tags);
if isempty(strfind(coaddopt.tags{1}(9),'D')) && isempty(strfind(coaddopt.tags{1}(9),'G'));
  ntotscansets=10;
else
  if numel(unique(p.rx))==1
    ntotscansets=7;
  else
    ntotscansets=3;
  end
end

% super title
if(numel(rx) == numel(unique(p.rx)))
  gtitle(sprintf('PHASE %s (%d of %d scansets); Rx=ALL', phase_tag, ...
    nscansets, ntotscansets), .97, 'none');
else 
  gtitle(sprintf('PHASE %s (%d of %d scansets); Rx=%s', phase_tag, ...
    nscansets, ntotscansets, num2str(rx)), .97, 'none');
end

% colorbar
axes('Position',[.03,.1,.8,.8],'Visible','off');caxis(cax);
colorbar('Location','WestOutside');

if(out==1)
  % Default to no extension
  rxext = '';

  % Only add suffixes if a subset of the receivers were plotted
  if numel(unique(p.rx)) > numel(rx)
    if numel(rx) > 1 || allfreq
      rxext = sprintf('_all%03d', freq);
    else
      rxext = sprintf('_rx%i', rx);
    end
  end

  subdir = phase_tag(1:6);
  outfile = sprintf('reducplots/%s/perphase_%s_001%s.png', subdir, ...
      phase_tag, rxext);

  mkpng(outfile,1);
  setpermissions(outfile);
  set(0,'DefaultFigureVisible','on');
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_perphase_panel(map,m,tit)

imagesc(m.x_tic,m.y_tic,map);
caxis([-250,250]);
xlabel('RA');ylabel('Dec');
title(tit);

return



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plot_perphase_histogram(map,bins,legtext,label)

colorder={'k','b','r'};

box on; hold on
for i=1:numel(map)
  d=rvec(map{i});
  [n,x]=hist(d(abs(d)<bins(end)),bins);
  stairs(x,n,colorder{i})
end

title(label);
xlabel('\muK_{cmb}');
ylabel('pixel count');
axis tight
set(gca,'XGrid','on','YGrid','on');
l1=legend(legtext);
set(l1,'Box','off');
hold off

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [map,m,coaddopt]=load_and_cal_map(phase_tag,fname,rx)

% load map
mapn=dir(fname);
load(['maps/0000/',mapn.name]);
uk=get_ukpervolt(phase_tag);

if ~exist('map','var') && exist('ac','var')
  % for years where there are more than one abscal, we need to apply it to the ac
  % structure before coadding over rx
  ac=cal_coadd_ac(ac,uk,coaddopt);
  if numel(rx) > 1 && coaddopt.coaddtype == 1
    acc=coadd_ac_overrx(ac(rx+1,:));
    map=make_map(acc,m,coaddopt);
  else
    map=make_map(ac,m,coaddopt);
  end
else % for times when reduc_coaddpairmaps saved a map instead of ac structure
  map=cal_coadd_maps(map,uk);
end

return
