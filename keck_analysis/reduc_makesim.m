function reduc_makesim(tags,simopt)
% reduc_makesim(tags,simopt)
%
% Make simulated data
%
% tags=cell array of tags to process
%
% simopt.sernum = 8 character string of form AABBCCCD
%                 AA is user's personal serial number prefix
%                 BB is sim ID number, specific to user
%                 CCC is realization number for specific sim 
%                 D denotes kind of sim, usually 1 for noise only, 2 for signal only
%                 - reduc_makesim ignores CCC and replaces it with value from simopt.rlz
%                 - there is no default, sernum must be user specified
%
% simopt.rlz = the simulation realizations to create in parallel
%              (e.g. 1:100 or 10:50) simopt.sernum and simopt.sigmapfilename is
%              modified accordingly. Default is rlz=0, in which case nothing is
%              modified. 
%
% simopt.noise = 'none'  - no noise
%              = 'data'  - use noise cross spectra measured from data
%                          "on-the-fly" - there are no spectra stored in external file. (default)
%              = 'white' - use fixed level of white noise as specified by
%                          wnlev
%
% simopt.wnlev = white noise in uK/sqrt(s) (assuming ukpervolt=[1,1])
%
% simopt.ntags - scale down noise, so that each input tag represents ntags worth of
%                data (default 1)
%
% simopt.sig = 'none'    - no signal
%            = 'normal'  - full signal (as loaded in) (default)
%            = 'nopol'   - set Q/U maps to zero
%            = 'onlypol' - set T map to zero
%            = 'onlyq'   - set T/U maps to zero
%            = 'test'    - test pattern to check it works
%
% simopt.sigmaptype = 'corrmap' - flat sky 2D map data/corrmap000001 etc
%                   = 'comap'   - ra/dec map as output by reduc_coaddpairmaps
%                   = 'healmap' - healpix map from B03,WMAP,synfast etc (default)
%                   = 'cel'     - fullsky ra/dec map (ie from FDS)
%
% simopt.sigmapfilename = name of file for above
%                         - a single string to use same map for 100/150
%                         - a 1 x 2 string cell array {'map1','map2'}
%
% simopt.separateABmaps = false (default), interpret two input maps as frequency split
%                       = true, interpret two input maps as A/B split (i.e. use map1
%                         for the A's and map2 for the B's.)
%                         (separate maps for A_100,B_100,A_150,B_150 not yet implemented)
%
% simopt.coord -  coordinate system of input healpix signal map
%                 = 'C' for celestial/equitorial coordinates (default)
%                 = 'G' for galactic coordinates
%
% simopt.sigmapbeamfwhms - Enables interpolation from healpix fits maps of different
%                          pre-smoothed beam sizes to an intermediate size.
%                        = array of beam size FWHMs of bracketing maps in arcmin. The
%                          maps must exist, i.e. simopt.sigmapbeamfwhms=[29.5,32.5];
%                        - The files specified in simopt.sigmapfilename will be altered
%                          according to simopt.sigmapbeamfwhms. 
%                          If filename contains string "_sxxx", it is replaced with,
%                          for instance, "_s29p50".
%                          If filename does not contain string "_sxxx", "_s29p50" is
%                          appended as a suffix.
%                          Thus specifying either:
%                           (1) simopt.sigmapfilename='input_maps/camb_wmap5yr_noB/map_cmb_n0512_rxxxx' or
%                           (2) simopt.sigmapfilename='input_maps/camb_wmap5yr_noB/map_cmb_n0512_rxxxx_sxxxx'
%                          together with
%                           simopt.sigmapbeamfwhms=[29.5,32.5];
%                           simopt.rlz=10;
%                          will cause reduc_makesim to use for support points the maps:
%                           input_maps/camb_wmap5yr_noB/map_cmb_n0512_r0010_s29p50.fits and
%                           input_maps/camb_wmap5yr_noB/map_cmb_n0512_r0010_s32p50.fits
%
% simopt.corrmap_apsfile - name of "cmbfast" spectrum to use if
%                          makecorrmap is called
%
% simopt.siginterp - interpolation type used when resamp sig map
%   When using a healpix map one can use:
%     'healpixnearest' - simply use nearest pixel value
%     'taylor'         - (default) interpolates using Taylor expansion in
%                        derivatives of map - so they must be
%                        present
%   'griddata_cubicmx' - use the cubicmx algorithm stolen from
%                        griddata to interpolate between nearby
%                        pixels without using derivatives
%     'randol'         - same as the last but also interpolate the
%                        derivatives of T and use them to apply
%                        beam shape perturbations (including A/B offsets)
%     'doublegauss'    - uses two circular gaussians to approximate an elliptical beam
%                        automatically uses the 'taylor' interpolate the maps
%                        demands input maps with smaller beamwidth smoothing than the nominal
%                        beamwidth: best use with beamwidth interpolation over 'sigmapbeamfwhms'
%                        option.
%     'multigauss'     - uses up to four circular gaussians to approximate an elliptical beam
%                        the number of circular gaussians is adjusted depending on the ellipticity
%                        automatically uses the 'taylor' interpolate the maps
%                        demands input maps with smaller beamwidth smoothing than the nominal
%                        beamwidth: best use with beamwidth interpolation over 'sigmapbeamfwhms'
%                        option.
%     'healconvolve'   - Experimental! - directly convolve by sampling a
%                        "cloud" of points around the tod trajectory and sum
%                        these with weights to effect a map space beam convolution
%                        Note, that the application to pre-smoothed input maps, results in a 
%                        double smoothing.                          
%   When using a flat map one can use:
%     'linear'         - do linear interpolation using interp2.
%                        This will also work with healpix maps but
%                        is not recommended as it defeats the point.
%
% simopt.ukpervolt = cal factor to apply to sig map
%                    (default is output of get_ukpervolt())
%
% Following only relevant when we are using a flat map (and linear interp):
% simopt.type - used to determine map area for intermediate flat map (default 'bicep')
% simopt.interpix = the pixel size of the intermediate map in degrees (default 0.25)
%
% simopt.ptsrc = 'on'   - inject point source location and intensity spec by simopt.src
%              = 'none' - (default)
%
% simopt.src.name = name           - ptsrc name, takes cell array of strings.
%       .src.ra   = ra             - ptsrc RA coord in deg.
%       .src.dec  = dec            - ptsrc DEC coord in deg.
%       .src.int  = [I_100;I_150]  - intensities for 100 & 150 GHz.
%
% These params control get_array_info:
%
% simopt.beamcen - determines which beam centers to get back from get_array_info
%                = 'ideal' - feed offset angles design vals from fp_data (default)
%                = 'obs'   - as observed and recorded in the beams files
%                = 'zero'  - zero p.r and p.theta
% simopt.beamwid = 'zero'  - no map smoothing
%                = 'ideal' - as returned by fp_data files
%                = 'obs'   - read from aux_data file (default)
% simopt.beammapfilename = empty (default)
%                        = mat file containing beammap data cube and ad
%                          structure for explicit convolution of a flat, intermediate
%                          map with a beam. Must be used with simopt.siginterp='linear'. 
%                          If set, simopt.beamwid is ignored. 
%                          In principle, the beammap can be pixelized differently than
%                          the intermediate map (set by simopt.interpix), but care must
%                          be taken if the beammap is being downgraded in resolution.
% simopt.curveskyrotbeam = False or not defined (default), rotate beam maps by negative
%                          of hor.dk
%                        = True, account for projection of focal plane on curved sky
%                          when rotating beam maps prior to convolution
% simopt.curveskyrescale = False or not defined (default), when siginterp = 'linear',
%                          convolve entire flat map with beam
%                        = True, rescale map for each detector such that pixels are
%                          square along the scan trajectory
% simopt.diffpoint = 'ideal' - no differential pointing (default)
%                  = 'obs'  - include differential pointing
% simopt.chi     = 'ideal' - chi=design vals wrt theta. (except CP wrt theta=0) (default)
%                = 'obs'   - chi as returned by get_array_info
% simopt.epsilon = 'ideal' - epsilon=0
%                = 'obs'   - epsilon as returned by get_array_info
% simopt.lb      = false (default) - do nothing
%                = true - add 180 degrees to p.theta to simulate a little buddy. All
%                         dithered parameters are interpreted as occuring after the
%                         offset. This only occurs in reduc_makesim. reduc_makepairmaps
%                         will use the non-offset p.r and p.theta, and so accumulate
%                         the little buddy timestream assuming the main beam
%                         trajectory (unless mapopt.beamcen='assim', etc.). It is
%                         possibly useful to specify simopt.abgain=ones(Ndet,Nreal)*x
%                         where x is the little buddy amplitude. 
%
% simopt.xtalk = false (default) do nothing
%              = level of inductive cross talk (i.e. .05 for 5 percent cross talk)
%
% simopt.tc = false (default) do nothing
%           = scalar or nchan array or time constants in seconds; smooth timestreams
%             according to these values
%
% simopt.xtalk_relgain = false to calculate xtalk in units of input map signal
%                      = true (default) to adjust by ADU/airmass scaling
%
% These params add "dithers" to the results from get_array_info:
% 
% simopt.rndcen = [mra,sra;mdec,sdec] - (2x2) random dither on A/B beamcen, mean and sigma
%               = beamcen dither array (Ndet x Nreal x 2) 2 is for ra_off_dos and dec_off.
%                 Leaves differential pointing unchanged. 
% simopt.rndsig = [pairmean, pairsig; diffmean, diffsig] - dither on the beamwidth pair to pair, 
%                 and differential beamwidth between A and B, Note funny definition of differential
%                 beamwidth (sig_A^2-sig_B^2)/(sig_A^2+sig_B^2)
%               = beamwidth dither array (Ndet x Nreal) applied channel by channel
% simopt.rndelp = [pairmean, pairsig; diffmean, diffsig] - dither on ellipse parameter p pair to pair, 
%                 and differential beamwidth between A and B
%               = p dither array (Ndet x Nreal) applied channel by channel
% simopt.rndelc = [pairmean, pairsig; diffmean, diffsig] - dither on ellipse parameter c pair to pair, 
%                 and differential beamwidth between A and B
%               = c dither array (Ndet x Nreal) applied channel by channel
% simopt.rndeps = [m100,s100;m150,s150] - random epsilon dither, mean and sigma
%               = epsilon dither array (Ndet x Nreal)
% simopt.rndchi = [m100,s100;m150,s150] - random pol angle dither, mean and sigma
%               = chi dither array (Ndet x Nreal)
% simopt.abgain = [m100,s100;m150,s150] - random abgain gen, mean and sigma
%               = abgain dither array (Ndet x Nreal)
%
% simopt.maketod = 0 - don't save an intermediate TOD, go straight to reduc_makepairmaps 
%                = 1 - save intermediate TOD instead of making the pairmap (default)
%
% simopt.pairmeanscp = 0 - do nothing: use the parameters sigma,c,p as delivered from get_array_info (default) 
%                    = 1 - set sigma,c,p of the A and B beam of a pair to the mean sigma.c.p 
%                      of the pair. This removes the differential portion of these parameters but 
%                      keeps pair to pair correlations across the focal plane. Additional dithers
%                      can be used to reintroduce differential parameters.
%  
% simopt.mapopt = mapopts to pass to reduc_makepairmaps if simopt.maketod = 0
%                 Can be cell array of mapopt structures, in which case a pairmap is
%                 made for each mapopt.
%                 If simopt.mapopt.abmap=1, uses reduc_makeabmaps instead of reduc_makepairmaps.
%
% simopt.striptod = 0 - save all fields of simulated TOD (default)
%                 = 1 - only save those fields needed by reduc_makepairmaps and set to
%                       NaN all non good-light channels to save memory
%
% simopt.state = empty or undefined - set randn state based on clock time at beginning
%                of reduc_makesim; record the unique randn state for each
%                tag/realization
%              = scalar or two element numeric array - set randn state to this value at beginning of
%                reduc_makesim; record the unique randn state for each tag/realizaiton
%              = Ntags x Nealizations cell array - set randn state for each tag/realization
%                according to seeds in this array, each element of which is a two element output
%                of randn('state')
%  
% simopt.update = 0 (default) will re-make any pairmaps which already exist.
%               = 1 only missing pairmaps will be made (only works with simopt.maketod=0)
%
% simopt.elofs = N will add N degrees to the elevation offset.  May not be exactly the
%              expected number of degrees since the pointing model is applied in
%              reduc_initial. If this field is not present, the trajectory specified in
%              the tod will be used (this is standard practice). 
%
% - read T,Q,U signal maps
% - read data for each tag
% - sample off map to get signal tod
% - generate covariant noise
% - add together to get sim data
%
% e.g. clear simopt; simopt.noise='none';  simopt.sig='normal';
% simopt.sigmaptype='healmap'; simopt.ukpervolt=[1,1];
% simopt.type='bicepgalf';
% simopt.sigmapfilename='/data/inputmaps/wmap7/wmap_band_iqumap_r9_7yr_W_v4.fits';
% reduc_makesim({'20100115d1','20100115d2'},simopt,1)
%
% or for multiple realizations, e.g.
% simopt.sigmaptype='healmap'; simopt.ukpervolt=[1,1];
% simopt.type='bicep';
% simopt.rlz=1:100;
% simopt.sigmapfilename='input_maps/camb_wmap5yr_noB/map_cmb_n0512_rxxxx_s31p22.fits';
% reduc_makesim({'20100115d1','20100115d2'},simopt,1)

if isdeployed
  % farmit set's matlab mcr cache to scratch. Make sure it is usable to everyone.
  system_safe('chmod -R g+rwX /scratch/.mcrCache4 || true');  
  % save the farmfile for later deletion
  farmfile=tags;
  % if using the compiled version of this code, tags will instead point to the .mat
  % farmfile.  Load it to get simopts and tag list.  If running the normal,
  % non-compiled version of this code, this if block is not executed.
  x=load(tags);
  % if compiled function is called from runsim (farmit) then tags is the farmfile.
  % If running the compiled version directly from the shell prompt, then tags should be
  % the location of a .mat file that contains only the variables tags and simopt
  % also, simopt points to the full path directory where reduc_makesim lies.
  if isfield(x,'val')
    % executed if compiled code is called from runsim
    % find which element stores the tags.
    tagvar=find(strcmp(x.var,'tags'));
    tags=x.val{tagvar};
    % find which element stores the simopts
    simoptvar=find(strcmp(x.var,'simopt'));
    simopt=x.val{simoptvar};
  else
    % executed if compiled code run from shell prompt
    tags=x.tags;
    simopt=x.simopt;
  end
  clear x tagvar simoptvar  
end

if(~exist('tags','var'))
  tags=get_tags;
end
if(~exist('n','var'))
  n=[];
end

simopt = get_default_simopt(simopt)

% Check for the presence of serial number:
if(~isfield(simopt,'sernum')|~isstr(simopt.sernum)|length(simopt.sernum)~=8)
  error('Please specify a valid 8 character serial number string (simopt.sernum).')
end
if(strcmp(simopt.sernum,'real'))
  error('`real` is not a valid serial number (simopt.sernum).')
end

% Generate random sets of *dither* array params.
% Note that these are dithers which will modify whatever gets
% returned from get_array_info inside the loop over tags.
% So it is possible to have them added to ideal vals - in which case
% epsilon for example should have a mean component, or to have them
% added to nominal in which case it probably shouldn't.
% Also note that these dithers are fixed over all days.
% 
% Allow dithers to be Nchan x Nrlz arrays rather than a 2x2 array specifying
% mean/std.
[dum ind]=get_array_info(tags{1});
[simopt pp]=get_dithers(simopt,ind);

% simopt random state on input
randstate=simopt.state;

% when not making tods check if pairmaps exist and
% record in the fexist matrix.
om = simopt.update & ~simopt.maketod;
if(om)
  disp('checking for existing pairmaps...');
  if ~iscell(simopt.mapopt)
    simopt.mapopt={simopt.mapopt};
  end
  fexist=zeros(length(tags),numel(simopt.rlz),numel(simopt.mapopt));
  
  % this is the same loop as coming up below. Here used
  % to go through the pairmap filenames:
  cexist=0;
  cchecked=0;
  for i=1:length(tags)
    tag=tags{i};
    for k=1:numel(simopt.rlz)
      rlz=simopt.rlz(k);
      % set serial number for this realization
      simopt.sernum=sprintf('%s%03d%s',simopt.sernum(1:4),rlz,simopt.sernum(end));
      for l=1:numel(simopt.mapopt)
        simopt.mapopt{l}.sernum=simopt.sernum;
        mapopt=simopt.mapopt{l};
        % this is the beginning of reduc_makepairmaps, to
        % generate the pairmap filename:
        mapopt=get_default_mapopt(mapopt);
        % however if we do a subsim, reduc_makepairmaps will
        % force the weight to zero. Do this here too but don't
        % dare to change the actual mapopts:
        mapopt2 = mapopt;
        if (isfield(simopt,'subsim') & simopt.subsim)
          mapopt2.weight=0;
        end
        filename=get_pairmap_filename(tag,mapopt2);
        clear mapopt2;
        display(['Check for ',filename])
        fexist(i,k,l)=exist(filename,'file');
        cchecked=cchecked+1;
        if(fexist(i,k,l))
          cexist=cexist+1;
        end
      end
    end  
  end
  display([num2str(cexist) ' of ' num2str(cchecked) ' exist.'])
end

% for each tag
for i=1:length(tags)
  % if only missing option, skip this tag if all of the pairmaps
  % for this tag exist (not any are missing)
  if (om & ~any(fexist(i,:)==0))
    continue;
  end
  
  tag=tags{i}

  % read in real tod
  todtag=tag;
  % if requested load tod from a previous year - this is for
  % coverage area sims of upcoming years
  if(isfield(simopt,'todtagyear'))
    todtag(1:4)=simopt.todtagyear;
    disp(sprintf('tod tag now switched to %s',todtag));
  end
  fn=sprintf('scratch/real/%s/%s_tod',todtag(1:6),todtag);
  disp(sprintf('loading tod from %s',fn));
  load(fn);

  % Get array info and index arrays
  if strcmp(simopt.beamcen,'zero')
    beamcen=[];
  else
    beamcen=simopt.beamcen;
  end
  [p0,ind]=get_array_info(tag,beamcen,simopt.chi,simopt.beamwid,simopt.diffpoint,[],simopt.epsilon,[],simopt.polofs);
  if(simopt.pairmeanscp)
    p0=pair_2_meanscp(p0,ind)
  end
  if strcmp(simopt.beamcen,'zero')
    p0.r(:)=0;
    p0.theta(:)=0;
  end
  
  % Add offset to p.theta to simulate little buddies
  if simopt.lb
    p0.theta=p0.theta+180;
    p0.theta(p0.theta>360)=p0.theta(p0.theta>360)-360;
  end
   
  % for simtraj files create lockin.adcData
  if(~isfield(d,'antenna0'))
    d.mce0.data.fb=zeros([length(d.t),length(p0.type)]);
  end
 
  % if elofs is a simopt field, offset the telescope pointing
  if(isfield(simopt,'elofs'))
    % adding elevation offset to entire tag, even though only the idices encompassed in
    % the fs structure are used in gen_pointing
    disp(sprintf('Shifting elevation by %.2f degrees from nominal',simopt.elofs));
    d.pointing.hor.el=d.pointing.hor.el+simopt.elofs;
    % stolen from reduc_initial to calculate new celestial coordinates
    % without adding an elevation offset, this recalculation reproduces previous
    % calculations to machine precision for ra and dec and an rms of 1e-8 for dk
    lat=median(double(d.antenna0.tracker.siteActual(:,2))/3.6e6);
    lon=median(double(d.antenna0.tracker.siteActual(:,1))/3.6e6);
    [d.pointing.cel.ra,d.pointing.cel.dec]=azel2radec(...
        d.pointing.hor.az,d.pointing.hor.el,d.t,lat,lon);
    d.pointing.cel.dk=d.pointing.hor.dk-parallactic(...
        d.pointing.hor.az,d.pointing.hor.el,d.t,lat,lon);
  end
  
  % Modify detector pol angle due to waveplates for this date/time
  p0=do_hwp_rot(d.t(1),p0);

  % needed only when display real/sim scans
  %d=filter_scans(d,fxs,'p0',ind.l);

  % record where real data is NaN (due to glitch removal)
  tod_mask=isnan(d.mce0.data.fb);

  % record the array params that were used
  simopt.ind=ind;

  % generate noise cov matrices if required, common to all realizations
  if strcmp(simopt.noise,'data')
    % "new" noise model which extracts info directly from the data and
    % makes sim noise which can be filtered like the real data (gets
    % rid of pre-filtering)

    % scale down observed data on which noise sim will be based
    d.mce0.data.fb=d.mce0.data.fb./sqrt(simopt.ntags);

    % find contiguous blocks of field scanning
    % ??? another function does this already?
    sb=find_blk(bitand(d.array.frame.features,2^1),length(d.t)/length(d.ts));

    % there is crap at the start of these before data is good - kludge it out
    sb.sf=sb.sf+50;

    % generate low and high freq covariant noise matrices
    covnoi=prep_cov_noise(d,sb,ind.gl);
  end

  % For each realization
  m=0;
  for k=1:numel(simopt.rlz)
    % if only missing option, skip this realization if all of the pairmaps exist
    % (not any are missing)
    if (om & ~any(fexist(i,k,:)==0))
      continue;
    end
    % m counts how many realizations have actually been run,
    % to tell check_pointing when to regenerate the pointing
    m=m+1;
    
    rlz=simopt.rlz(k);
    disp(sprintf('rlz %03d',rlz))
    
    % set serial number for this realization
    simopt.sernum=sprintf('%s%03d%s',simopt.sernum(1:4),rlz,simopt.sernum(end));

    % set random seed for this realization if specified
    if iscell(randstate)
      randn('state',randstate{i,k});
    end
    
    % record random seed for this realization
    simopt.state=randn('state');
    
    % expand p structure and modify with dithers
    p=add_dithers(p0,pp,rlz,simopt.rlz);

    % record p structure
    simopt.p=p;
    
    % create output array for sim, with dim: datasize x nchan
    d.sim=zeros(size(d.mce0.data.fb));

    % generate signal data if requested
    if(~strcmp(simopt.sig,'none')|~strcmp(simopt.ptsrc,'none'))

      % get the signal map every time if there is more than one realization, otherwise
      % only load it for the first tag
      if numel(simopt.rlz)>1 | ~exist('map','var')
        map=get_sig_map(simopt,rlz);
      end
      % generate pointing information only if it will be different than previous
      if(check_pointing(pp,simopt,m))
        point=gen_pointing(d,fs,map,ind.l,p,simopt,ind);
      end
      % generate the sky signal data
      d=gen_sig(d,fs,map,ind,p,simopt,point);
    end

    % generate noise data if requested
    if(~strcmp(simopt.noise,'none'))
      switch simopt.noise
       case 'data'
        disp('generating cov noise...')
        d=gen_cov_noise(d,sb,ind.gl,covnoi);
       case 'white'
        d=gen_white_noise(d,fs,simopt.wnlev,ind);
       otherwise
        % do nothing
      end
    end

    d.mce0.data.fb=d.sim;
    d=rmfield(d,'sim');
    
    % add crosstalk for signal only sims only
    if strcmp(simopt.noise,'none')
      d=add_xtalk(d,fs,en,simopt.xtalk,simopt.p,simopt.ind,simopt.xtalk_relgain);
    end
    
    % smooth by time constants for signal only sims only
    if strcmp(simopt.noise,'none') & any(simopt.tc)
      d=apply_timeconst(d,simopt.tc);
    end
 
    % re-mask the sim tod to match real
    if(~isfield(simopt,'subsim') | ~simopt.subsim)
      % allow to turn this off when making minimal tag list signal
      % only sims for tag subset substitution
      d.mce0.data.fb(tod_mask)=NaN;
    end
   
    % either save TOD or make pairmap directly
    if(simopt.maketod)
      % check if the temporary directory for this realization already exists.
      % if not, create it.
      if(~isdir(sprintf('scratch/%s/',simopt.sernum(1:4))))
        system(sprintf('mkdir scratch/%s/',simopt.sernum(1:4)));
      end
      if(~isdir(sprintf('scratch/%s/%s',simopt.sernum(1:4),simopt.sernum(5:end))))
        system(sprintf('mkdir scratch/%s/%s',simopt.sernum(1:4),simopt.sernum(5:end)));
      end

      % strip down TOD to bare minimum needed for reduc_makecomap, and set to
      % NaN all non good light channels to save memory
      if(simopt.striptod)
        d=strip_tod(d);
        nanind=setxor(simopt.ind.e,simopt.ind.gl);
        d.mce0.data.fb(:,nanind)=NaN;
      end

      % Force saving as v7 as it gives the smallest file sizes. (v7.3 is accursed)
      fname=sprintf('scratch/%s/%s/%s_tod',simopt.sernum(1:4),simopt.sernum(5:end),tag);
      disp(sprintf('saving %s',fname));
      saveandtest(fname,'d','fs','en','fsb','simopt','-v7');
    else  
      % don't save intermediate tod, just make pairmaps
      data.d=d;
      data.fs=fs;
      data.en=en;
      data.fsb=fsb;
      data.simopt=simopt;
      data.tag=tag;

      % set up for multiple mapopt options
      if ~iscell(simopt.mapopt)
        simopt.mapopt={simopt.mapopt};
      end
      
      for l=1:numel(simopt.mapopt)
        % if only missing option, skip this mapoption if the pairmap exists:
        if (om & fexist(i,k,l)~=0)
          continue;
        end
        
        disp('making pairmap without saving TOD');
        simopt.mapopt{l}.sernum=simopt.sernum;
        if isfield(simopt.mapopt{l},'abmap') && simopt.mapopt{l}.abmap
          disp('Making A/B individual channel maps!');
          reduc_makeabmaps(data,simopt.mapopt{l});
        else
          reduc_makepairmaps(data,simopt.mapopt{l});
        end

      end
    end
    
  end % loop over realizations
end % loop over tags

% if using the compiled version of the code, the .mat farmfile will not get deleted by
% farmit.m, so delete it here since the code should have successfully finished if it's
% made it to this point.
if isdeployed
  delete(farmfile);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function p0=pair_2_meanscp(p0,ind)
%  p0=pair_2_meanscp(p0,ind)
%  sets sigma, c and p of each channel to the mean of the respective pair
%  hence all differential contributions of the parameters are removed
  [sigma,c,p] = egauss2_mmt2scp(p0.fwhm_maj,p0.fwhm_min,p0.alpha+p0.theta);
  meanSigma(ind.a) = (sigma(ind.a)+sigma(ind.b))/2; meanSigma(ind.b) = meanSigma(ind.a);
  meanC(ind.a) = (c(ind.a)+c(ind.b))/2; meanC(ind.b) = meanC(ind.a);
  meanP(ind.a) = (p(ind.a)+p(ind.b))/2; meanP(ind.b) = meanP(ind.a);
  [p0.fwhm_maj,p0.fwhm_min,p0.alpha] = egauss2_scp2mmt(meanSigma',meanC',meanP');
  p0.alpha = p0.alpha - p0.theta;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function do_pointing = check_pointing(pp,simopt,k)
% False if no dithers and realization number > 1 since no new pointing calculation is
% needed   

N_beamcen=size(pp.ra_off_dos,2);
N_chi=size(pp.chi,2);
N_c=size(pp.c,2);
N_p=size(pp.p,2);

do_pointing = true;
switch simopt.siginterp
 case{'taylor','healpixnearest'}
  if N_beamcen==1 & N_chi==1 & k>1
    do_pointing = false;
  end      
 
 case{'healconvolve','linear','doublegauss','multigauss'}
  if N_beamcen==1 & N_chi==1 & N_c==1 & N_p==1 & k>1
    do_pointing = false;
  end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function point=gen_pointing(d,fs,map,ch,q,simopt,ind)
% Generate pointing info for gen_sig.
% If there are dithers, then there will be a unique pointing
% for each realization. This only handles one realization at a time
% because you can't generally hold Nrlz worth of pointing info in memory 
% in the presence of dithers. 
disp('gen_pointing...');
tic

% get the mapind
mapind=make_mapind(d,fs);

% for each channel
for k=ch
  % count the channels out
  if(rem(k,50)==0)
    disp(k)
  end

  f = get_band_ind(q,ind,q.band(k));
   
  if(size(map,2)>1)
    j=k; 
  else
    j=1;
  end

  switch simopt.siginterp
      
    case {'taylor','healpixnearest','taylor+','taylor1','griddata_cubicmx','randol'}
      % interpolate directly from healpix map with no option to convolve beam

      % find ra/dec input parameters
      rapt=d.pointing.cel.ra(mapind);
      decpt=d.pointing.cel.dec(mapind);

      % find ra/dec trajectory for this channel using UNROUNDED deck
      r=q.r(k);
      th=q.theta(k)-d.pointing.cel.dk;
      [y,x]=reckon(decpt,rapt,r,th(mapind)-90);

      % calculate bolometer orientation angle at each time sample for the UNROUNDED deck
      alpha=chi2alpha(rapt,decpt,r,th(mapind),q.chi(k),q.chi_thetaref(k)-d.pointing.cel.dk(mapind));

    case {'doublegauss'}
      % double gauss is similar to taylor, but constructs two different pointings for
      % two subbeams which are shifted such that they approximate an ellipse

      % find ra/dec input parameters
      rapt=d.pointing.cel.ra(mapind);
      decpt=d.pointing.cel.dec(mapind);

      % find ra/dec trajectory for this channel using UNROUNDED deck
      r=q.r(k);
      th=q.theta(k)-d.pointing.cel.dk;
      % the common pointing of the two subgaussians
      [y,x]=reckon(decpt,rapt,r,th(mapind)-90);

      % calculate bolometer orientation angle at each time sample for the UNROUNDED deck
      alpha=chi2alpha(rapt,decpt,r,th(mapind),q.chi(k),q.chi_thetaref(k)-d.pointing.cel.dk(mapind));

      % fetch how much the beams are going to be shifted:
      [delta,dum] = get_doublegauss_par(q.fwhm_maj(k),q.fwhm_min(k));
      % this is the shifting direction:
      alphaEll = q.alpha(k);
      
      % find ra/dec trajectory for this channel using UNROUNDED deck
      % first get the position of the first sub-gaussian by shifting
      % from the initial pixel pointing direction a distance delta/2 
      % in direction of the major axis orientation of the ellipses:
      % this is a small change in comparison to q.r q.theta
      [r1,theta1] = shift_pixel_pointing(q.r(k),q.theta(k),delta/2,alphaEll);
      % then proceed as in the other cases:
      theta1=theta1-d.pointing.cel.dk;
      [y1,x1]=reckon(decpt,rapt,r1,theta1(mapind)-90);

      % and do the same for the second sub-gaussian, but shifting
      % in the opposite direction:
      [r2,theta2] = shift_pixel_pointing(q.r(k),q.theta(k),delta/2,alphaEll+180);
      theta2=theta2-d.pointing.cel.dk;
      [y2,x2]=reckon(decpt,rapt,r2,theta2(mapind)-90);

    case {'multigauss'}
      % multi gauss is similar to taylor, but constructs two different pointings for
      % several subbeams which are shifted such that they approximate an ellipse

      % find ra/dec input parameters
      rapt=d.pointing.cel.ra(mapind);
      decpt=d.pointing.cel.dec(mapind);

      % calculate bolometer orientation angle at each time sample for the UNROUNDED deck
      r=q.r(k);
      th=q.theta(k)-d.pointing.cel.dk;
      alpha=chi2alpha(rapt,decpt,r,th(mapind),q.chi(k),q.chi_thetaref(k)-d.pointing.cel.dk(mapind));

      % fetch how much the beams are going to be shifted:
      [dum,shifts,dum] = get_multigauss_par(q.fwhm_maj(k),q.fwhm_min(k));
      % this is the shifting direction:
      alphaEll = q.alpha(k);
      
      % find ra/dec trajectory for this channel using UNROUNDED deck
      % get the position of each sub-Gaussian by shifting
      % from the initial pixel pointing direction in direction of the major axis orientation of the ellipses:
      % these are a small change in comparison to q.r q.theta
      for c = 1:length(shifts)
        shift = shifts(c);
        [r,theta] = shift_pixel_pointing(q.r(k),q.theta(k),shift,alphaEll);
        % then proceed as in the other cases:
        theta=theta-d.pointing.cel.dk;
        [y(:,c),x(:,c)]=reckon(decpt,rapt,r,theta(mapind)-90);
      end

    case 'healconvolve'
      % convolve beam directly from healpix map
      % construct offset point "cloud"
      % want as many points as possible to fully sample beam shape -
      % but time scales as npoints.
      n=5; % number of rings, 5=60, 6=84, 7=112
      % use healpix to construct an equally spaced pixel cloud which has
      % equally spaced rings of pixels
      [theta,phi]=pix2ang_ring(512,0:sum(4:4:n*4)-1);
      % we can scale the ring radius as we wish - go out to 3 sigma
      % making the implicit assumption that beam power beyond is
      % negligible fraction of total
      c=2*sqrt(2*log(2));
      s=mean([q.fwhm_maj(k) q.fwhm_min(k)])/c;
      theta=theta*3*s/max(theta);
      % convert from healpix to mapping toolbox convention
      dec=90-theta; ra=phi*180/pi;
      % find the range and angle of these - will be applied to tod
      % trajectory using reckon below
      [oc.r,oc.th]=distance(90-1e-12,0,dec,ra);
      % convert from polar to cartesian - will be used to lookup beam
      % values below - is there a better way to do this? - do we
      % need to make flat sky approx?
      [oc.x,oc.y]=pol2cart(phi,theta);
    
      % find ra/dec input parameters used below
      rapt=d.pointing.cel.ra(mapind);
      decpt=d.pointing.cel.dec(mapind);

      % find ra/dec trajectory for this channel
      r=q.r(k);								  
      th=q.theta(k)-d.pointing.cel.dk;
      [y,x]=reckon(decpt,rapt,r,th(mapind)-90);

      % calculate beam ellipse angle on the sky as function of time
      % - do this exactly as for pol angle chi - do we need an alpharef?
      % (STF): recheck if the angles are used properly here, compare i.e. taylor+...
      elang=chi2alpha(rapt,decpt,r,q.theta(k),q.alpha(k)-d.pointing.cel.dk,0);

      % find beam weight values for this channel - since points are
      % equally spaced on the sky weight is just beam value
      bv=egauss2beam({q.fwhm_maj(k)/c,q.fwhm_min(k)/c,elang*pi/180},oc.x,oc.y);
      % normalize each set of weights to unity
      bv=bv./repmat(sum(bv,2),[1,length(oc.x)]);

      % calculate bolometer orientation angle at each time sample for the UNROUNDED deck
      alpha=chi2alpha(rapt,decpt,r,th(mapind),q.chi(k),q.chi_thetaref(k)-d.pointing.cel.dk(mapind));

    case 'linear'
      % interpolate from ra/dec flat map

      % Constructing an intermediate map requires a constant dk angle. Because of
      % jitter in the mount, we must take the mean of the dk angles. We used to allow
      % multiple dk angles in a single tag, ad so we rounded the dk angles. Since this
      % seems unlikely to come back, we can just take the mean of the dk angles.
      d.pointing.hor.dkmean=mean(d.pointing.hor.dk(mapind));
      d.pointing.cel.dkmean=mean(d.pointing.cel.dk(mapind));
      d.pointing.cel.decmean=mean(d.pointing.cel.dec(mapind));

      % rotate array to this deck angle
      qp=rotarray(q,d.pointing.hor.dkmean);
      
      % check to make sure pointing dec is within safe limits given size of focal plane
      maxdec=min(90-atan(tan(qp.r(ind.l)*pi/180).*cos(qp.theta(ind.l)*pi/180))*180/pi);
      if(max(abs(d.pointing.cel.dec(mapind)))>maxdec)
        error('pointing dec is too high; coordinates on sky no longer have unique bolometer orientations')
      end

      % avoid the time consuming loop if there are not multiple dithers for r/theta
      rapt=d.pointing.cel.ra(mapind);
      decpt=d.pointing.cel.dec(mapind);
      
      % find ra/dec trajectory for this channel using UNROUNDED deck
      r=q.r(k);
      th=q.theta(k)-d.pointing.cel.dk;
      [y,x]=reckon(decpt,rapt,r,th(mapind)-90);

      if isfield(map(f,j),'x_tic')

        % this hack apparently needed for galactic fields
        if(~any(map(f,j,:).x_tic<0))
          x(x<0,i)=x(x<0,i)+360;
        end

        % construct the map as viewed by bolo orientation w.r.t. north at
        % each coordinate. Only needed if simopt.curveskyrescale = False
        [xgrid,ygrid]=meshgrid(map(f,j).x_tic,map(f,j).y_tic);
        [rapt,decpt]=coord2point(xgrid,ygrid,qp.r(k),qp.theta(k));
        a=chi2alpha(rapt,decpt,qp.r(k),qp.theta(k),qp.chi(k),qp.chi_thetaref(k));
        alpha=reshape(a,size(rapt));
      else
        alpha=[];
      end

      % constructed Guassian beams for this channel & dk angle, if needed
      if(~strcmp(simopt.beamwid,'zero') & isempty(simopt.beammapfilename))
        xv=0:interpix:90; 
        xv=xv-min(xv(xv>0)); % center on zero
        % calculate beam sigma from fwhm
        c=2*sqrt(2*log(2));
        beam_sig_a=qp.fwhm_maj(k)/c; 
        beam_sig_b=qp.fwhm_min(k)/c;
        xx=xv(abs(xv)<beam_sig_a*15); % gen beam grid to 5 sigma
        [bxg,byg]=meshgrid(xx,fliplr(xx));
        % The rotation angle here should be brought into line with what is in the beam
        % map code below! I am leaving it for now. CDS 20150721
        Tbeam=egauss2([1,0,0,beam_sig_a,beam_sig_b,(qp.alpha(k)+qp.theta(k))*pi/180],bxg,byg);
        Tbeam=Tbeam./sum(sum(Tbeam));
        % sim Roger effect
        if isfield(qp,'polofs_x')
          rot=bxg*qp.polofs_x(k)+byg*qp.polofs_y(k);
        else
          rot=0;
        end
        tmpqbeam=Tbeam.*cosd(2*rot)*(1-qp.epsilon(k))/(1+qp.epsilon(k));
        tmpubeam=Tbeam.*sind(2*rot)*(1-qp.epsilon(k))/(1+qp.epsilon(k));
        % enforce convention that mean(U)=0 (all dets); mean(Q)>0 (A dets); mean(Q)<0 (B dets)
        qmean=mean(tmpqbeam(:));
        umean=mean(tmpubeam(:));
        tmpth=atan2(umean,qmean);
        if ismember(k,ind.b)
          tmpth=tmpth+pi;
        end
        Qbeam=cos(tmpth)*tmpqbeam+sin(tmpth)*tmpubeam;
        Ubeam=cos(tmpth)*tmpubeam-sin(tmpth)*tmpqbeam;
      end
  end
  
  % save pointing to output point structure for each channel
  if(exist('x','var'))
    point(k).x=x;
    point(k).y=y;
    point(k).alpha=alpha;
  end
    
  % save pointing in case of the double gaussian approximation
  if(exist('x1','var'))
    point(k).x1=x1;
    point(k).y1=y1;
    point(k).x2=x2;
    point(k).y2=y2;
    point(k).alpha=alpha;
  end

  % special cases for beam convolution
  % in healconvolve and linear
  if(exist('bv','var'))
    point(k).bv=bv;
    point(k).oc.r=oc.r;
    point(k).oc.th=oc.th;
  end
  
  if(exist('Tbeam','var'))
    point(k).Tbeam=Tbeam;
    point(k).Qbeam=Qbeam;
    point(k).Ubeam=Ubeam;
  end

  clear x y x1 y1 x2 y2 alpha beam bv oc

end

% If loading in a beam map, do so for all channels simultaneously
if ~isempty(simopt.beammapfilename)
  beam=load(simopt.beammapfilename);
  for k=ch
    tbeam=beam.map(k).T;
    if isfield(beam.map(k),'Q')
      qbeam=beam.map(k).Q;
      ubeam=beam.map(k).U;
    else
      % sim Roger effect
      if isfield(q,'polofs_x')
        rot=zeros(length(beam.ad.t_val_deg{2}),length(beam.ad.t_val_deg{1}));
        % Use q.polofs_x, _y (unrotated version) instead of qp.polofs_x, _y.
        % This is because the beams get rotated later by the dk angle in the rotbeam commands.
        rot=rot+q.polofs_x(k)*repmat(beam.ad.t_val_deg{1},length(beam.ad.t_val_deg{2}),1);
        rot=rot+q.polofs_y(k)*repmat(-beam.ad.t_val_deg{2}',1,length(beam.ad.t_val_deg{1}));
      else
        rot=0;
      end
      tmpqbeam=tbeam.*cosd(2*rot)*(1-q.epsilon(k))/(1+q.epsilon(k));
      tmpubeam=tbeam.*sind(2*rot)*(1-q.epsilon(k))/(1+q.epsilon(k));
      % Convention: A dets have mean(Q)>0, mean(U)=0
      %             B dets have maen(Q)<0, mean(U)=0
      qmean=nanmean(tmpqbeam(:));
      umean=nanmean(tmpubeam(:));
      tmpth=atan2(umean,qmean);
      if ismember(k,ind.b)
        tmpth=tmpth+pi;
      end
      qbeam=cos(tmpth)*tmpqbeam+sin(tmpth)*tmpubeam;
      ubeam=cos(tmpth)*tmpubeam-sin(tmpth)*tmpqbeam;
    end

    % Rotation angle of the beam map. The calculation is basically the same as in
    % chi2alpha. It does not depend on RA.
    if isfield(simopt,'curveskyrotbeam') && simopt.curveskyrotbeam;
      rotangle = curveskyrotbeam(d.pointing.cel.decmean,d.pointing.cel.dkmean,q.r(k),q.theta(k));
    else
      rotangle= - d.pointing.hor.dkmean;
    end
    
    % Includes resampling beam map to desired grid spacing
    [b,ad]=rotbeam(tbeam,beam.ad,rotangle,simopt.interpix);
    point(k).Tbeam=b;
    [b,ad]=rotbeam(qbeam,beam.ad,rotangle,simopt.interpix);
    point(k).Qbeam=b;
    [b,ad]=rotbeam(ubeam,beam.ad,rotangle,simopt.interpix);
    point(k).Ubeam=b;
    
    point(k).ad=ad;
    
  end
end

toc

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [beam,ad] = rotbeam(beam,ad,dk,interpix)

% First interpolate the stored beam onto a grid that is appropriate for the
% intermediate map pixelization and has (0,0) located at the center
% pixel, which has a specific definition for the purposes of conv2 and imrotate

% Grid to interpolate beam onto
x=0:interpix:ad.Field_size_deg(1);
y=0:interpix:ad.Field_size_deg(2);

% Find the center pixel and put the zero point of the grid there
nx=numel(x);
ny=numel(y);

% Ensure beam has an odd number of pixels. It's just easier this way.
if nx/2==round(nx/2)
  nx=nx-1;
  x=x(1:end-1);
end
if ny/2==round(ny/2)
  ny=ny-1;
  y=y(1:end-1);
end

x0=round((nx+1)/2);
y0=round((ny+1)/2);

x=x-x(x0);
y=y-y(y0);
[xx,yy]=meshgrid(x,y);

% Interpolate the beam, extrapolated values are zero
% beware that even when there is no change in the nominal grid values along
% the edge can get set to zero
beam=interp2(ad.t_val{1}*180/pi,ad.t_val{2}*180/pi,beam,xx,yy,'linear',0);
%beam=interp2(ad.t_val{1}*180/pi,ad.t_val{2}*180/pi,beam,xx,yy,'nearest');

%renormalize by change in area
inputbeampix_x=(ad.t_val{1}(2)-ad.t_val{1}(1))*180/pi;
inputbeampix_y=(ad.t_val{2}(2)-ad.t_val{2}(1))*180/pi;
beam=beam.*(interpix/inputbeampix_x).*(interpix/inputbeampix_y);

% Rotate the beam
beam=imrotate(beam,dk,'bilinear','crop');

% Calculate new ad
% ad.t_val{2} is reverse order in Kirit's beam map files - this leads
% to it getting set back to normal order... don't think this matters
% but lead to confusion when plotting
Npix=[size(beam,2),size(beam,1)];
ad=calc_ad2(Npix*interpix,Npix);

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [r_s, theta_s] = shift_pixel_pointing(r,theta,s,alpha)

% Shift the direction r,theta by angular distance s in direction alpha as used in focal plane
% reckon first to the pixel pointing position:
[y x]=reckon(0,0,r,theta+90);

% then take a bearing towards the offset
[yn, xn]=reckon(y,x,s,theta+90+alpha);

% fetch new r and theta:
[r_s bearing]=distance(0,0,yn,xn);
theta_s=bearing-90;

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function bv=egauss2beam(p,x1,x2)

% p{1}=sigma_major
% p{2}=sigma_minor
% p{3}=major axis angle

% derivative of egauss2 function specialized for use in healconvolve
% above

% expand to full size
p{3}=repmat(p{3},[1,size(x1,2)]);
x1=repmat(x1,[size(p{3},1),1]);
x2=repmat(x2,[size(p{3},1),1]);

% rotate by specified angle
s=sin(p{3}); c=cos(p{3});
x1r=+x1.*c+x2.*s;
x2r=-x1.*s+x2.*c;

% calc elliptical gaussian values
bv=exp(-(x1r.^2./(2*p{1}.^2)+x2r.^2./(2*p{2}.^2)));

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function d=gen_sig(d,fs,map,ind,q,simopt,point)

disp('gen_sig...');
tic

q.gamma=(1-q.epsilon)./(1+q.epsilon);

% get array pointing to relevant field scans
mapind=make_mapind(d,fs);

d.pointing.hor.dkmean=mean(d.pointing.hor.dk(mapind));
d.pointing.cel.dkmean=mean(d.pointing.cel.dk(mapind));
qp=rotarray(q,d.pointing.hor.dkmean);

% prepare the interpolation to different beam widths
% Note that for 'healconvolve' only one beam width is expected
% to be handed in and this if clause will be skipped
if (length(simopt.sigmapbeamfwhms)>1)
  disp('setting up for beam width interpolation')
  for i=1:size(map,1)
    % different frequencies, the first variable here only needs to be a dummy
    [dum,suppPoints(i)] = healpix_interp_beamwidth(map(1).fwhm,map(i,:));
  end
end

% get the Delaunay triangulation of the healpix centers
switch simopt.siginterp
  case {'griddata_cubicmx','randol'}
    % find coords of the healpix pixels
    switch map(1).ordering
      case 'RING'  
        [theta_p,phi_p]=pix2ang_ring(map(1).nside,map(1).pixel);
      case 'NESTED'
        [theta_p,phi_p]=pix2ang_nest(map(1).nside,map(1).pixel);
    end
    % force phi to span zero - will not work with a field which
    % spans phi=pi
    i=phi_p>pi; phi_p(i)=phi_p(i)-2*pi;
    % Do the delaunay triangulation of the healpix pixel centers
    tri=delaunay(phi_p,theta_p);
end

% expand the ukpervolts to per channel basis.
% for simopt.ukpervolt=[rx0,rx1,rx2,...] (or tile, channel)
% this will make ukpervolt=[rx0,rx0,...,rx1,rx1,....]
% only when ukpervolt is coming per pair 
% (numel(ukpervolt)==size(ind.a,2)) this is more complicated:
if ~(numel(simopt.ukpervolt) == size(ind.a,2))
  ukpervolt = repmat(simopt.ukpervolt,size(ind.e,2)/numel(simopt.ukpervolt),1);
  ukpervolt = ukpervolt(:)';
else
  ukpervolt = zeros(size(ind.e));
  % set a and b of a pair to the same value:
  ukpervolt(ind.a) = simopt.ukpervolt;
  ukpervolt(ind.b) = simopt.ukpervolt;
end

% for each channel
for k=ind.l

  % count the channels out
  if(rem(k,50)==0)
    disp(k)
  end
  
  if ~simopt.separateABmaps
    % interpret multiple input maps as per-frequency
    f = get_band_ind(q,ind,q.band(k));
  else
    % interpret dual input maps as one for A and one for B
    if ~isempty(intersect(k,ind.a))
      f=1;
    else
      f=2;
    end
  end
  
  % If more than one maps is given, beamwidth interpolation is good to go:  
  if (length(simopt.sigmapbeamfwhms)>1)
    % first fetch the beamwidth which is useful for the specific channel/simoption....
    switch simopt.siginterp
     case 'doublegauss'
      % fetch the beamwidth needed for the sub-Gaussians in the doublegauss approximation:
      [dum,cbeamwidth] = get_doublegauss_par(q.fwhm_maj(k),q.fwhm_min(k));
     case 'multigauss'
      % fetch the beamwidths, and relative amplitudes of the sub-Gaussians
      [cbeamwidth,dum,rel_amps] = get_multigauss_par(q.fwhm_maj(k),q.fwhm_min(k));
     case {'randol','taylor+'}
      % in the deprojection basis a single beamwidth is assigned to the A/B pair,
      % this single value is the cbeamwidth calculated here:
      % get the index of the A/B pair
      abl=[find(k==ind.a),find(k==ind.b)];
      fwhm_a = sqrt((q.fwhm_maj(ind.a(abl))^2 + q.fwhm_min(ind.a(abl))^2)/2);
      fwhm_b = sqrt((q.fwhm_maj(ind.b(abl))^2 + q.fwhm_min(ind.b(abl))^2)/2);
      cbeamwidth = sqrt((fwhm_a^2 + fwhm_b^2)/2);
     otherwise
      % circular beam approximation:
      cbeamwidth = (q.fwhm_min(k) + q.fwhm_maj(k))/2;
      %cbeamwidth*60
    end
    % and then run the interpolation:
    cmap = healpix_interp_beamwidth(cbeamwidth, suppPoints(f));
  else 
    % otherwise use the single map as it is:
    cmap = map(f,1);
  end
  
  switch simopt.siginterp
   case {'healpixnearest','taylor','taylor1','taylor+'}
    % lookup the nearest healpix pixel. then optionally use 1st
    % or 1st and 2nd derivatives at that location to interpolate
    % to the timestream pointing locations
    
    % extract pointing from point structure
    x=point(k).x;
    y=point(k).y;
    gamma=q.gamma(k);
    alpha=point(k).alpha;
    
    if simopt.force_ab_int_from_common_pix
      % find point in a/b lists
      abl=[find(k==ind.a),find(k==ind.b)];
      % take pointing to extract pix from as mean of a/b
      x(:,2)=mean([point(ind.a(abl)).x,point(ind.b(abl)).x],2);
      y(:,2)=mean([point(ind.a(abl)).y,point(ind.b(abl)).y],2);
    end
    
    % need the indices "loc" of the hit pixels in case that 'taylor+' is used...
    [v,loc] = healpixmap2tod(cmap,x,y,alpha,gamma,simopt.siginterp,simopt.coord);
    
    if(strcmp(simopt.siginterp,'taylor+'))
      % go on to do beam width and ellipticity perturbations on the
      % assumption of constant derivatives within healpix cells
      
      % translate the beam shape parameters from get_array_info
      % into s,r,p form
      % the next few lines will migrate into gen_pointing
      rapt=d.pointing.cel.ra(mapind);
      decpt=d.pointing.cel.dec(mapind);
      r=q.r(k);
      th=q.theta(k)-d.pointing.hor.dk;
      alphaEll=chi2alpha(rapt,decpt,r,th(mapind),q.alpha(k),q.theta(k)-d.pointing.hor.dk(mapind));
      % the ellipse parameterization assumes the angle to be counted counterclockwise from x
      [s,c,p]=egauss2_mmt2scp(q.fwhm_maj(k),q.fwhm_min(k),alphaEll+90);
      % get beam size of input map in sigmas
      sigmamap=cmap.fwhm/sqrt(8*log(2));
      % put s and sigmamap into radians
      s=s*pi/180; sigmamap=sigmamap*pi/180;
      % take the delta of the squares of beam width versus beam of
      % input map
      delsigsq=s.^2-sigmamap.^2;
      
      % add in beam width perturbation assuming constant
      % derivatives in healpix cells
      v=v + (delsigsq/2)*(cmap.map(loc,10)+cmap.map(loc,16));
      
      % add in the ellipticity perturbation
      v=v + sigmamap.^2*(p.*(cmap.map(loc,16)-cmap.map(loc,10))/2+c.*cmap.map(loc,13));
    end
    
   case 'griddata_cubicmx'
    % interpolate between closeby healpix pixel values
    
    % transform from ra/dec in deg to Healpix style theta/phi
    theta=(90-point(k).y)*pi/180;
    phi=point(k).x*pi/180;
      
    % lookup the closest triangles using previously derived tri list
    t=t_search(phi_p,theta_p,tri,phi,theta);
    
    % interpolate T/Q/U at mean a/b pointing
    T=cubicmx(phi_p,theta_p,cmap.map(:,1),phi,theta,tri,t);
    Q=cubicmx(phi_p,theta_p,cmap.map(:,2),phi,theta,tri,t);
    U=cubicmx(phi_p,theta_p,cmap.map(:,3),phi,theta,tri,t);
    
    % construct the tod
    v=T + q.gamma(k)*(Q.*cos(2*point(k).alpha*pi/180) + ...
                      U.*sin(2*point(k).alpha*pi/180));
    
    
   case 'randol'
    % use the same method as above but now also interpolate the
    % derivatives and use these to apply beam shape perturbations
    % as worked out by Randol
    
    % transform pointing trajectory of this channel from ra/dec
    % in deg to Healpix style theta/phi in rad
    theta=(90-point(k).y)*pi/180;
    phi=point(k).x*pi/180;
    
    % find index in a/b lists
    abl=[find(k==ind.a),find(k==ind.b)];
    % find mean pointing of this pair
    x=mean([point(ind.a(abl)).x,point(ind.b(abl)).x],2);
    y=mean([point(ind.a(abl)).y,point(ind.b(abl)).y],2);
    % transform also
    phim=x*pi/180;
    thetam=(90-y)*pi/180;
    
    % take the offset of this channel from a/b mean
    dphi=phi-phim;
    dtheta=theta-thetam;
    % correct for compression in phi direction
    dphi=dphi.*sin(theta(:,1));
    
    % lookup the closest triangles to the mean a/b pointing
    t=t_search(phi_p,theta_p,tri,phim,thetam);
    
    % interpolate T/Q/U at mean a/b pointing
    T=cubicmx(phi_p,theta_p,cmap.map(:,1),phim,thetam,tri,t);
    Q=cubicmx(phi_p,theta_p,cmap.map(:,2),phim,thetam,tri,t);
    U=cubicmx(phi_p,theta_p,cmap.map(:,3),phim,thetam,tri,t);
    % alternative:       
    % T=rbfCloudInterp(cmap,thetam,phim,neighbours,75,0.0055,1);
    % ...
    
    % construct the base tod
    v=T + q.gamma(k)*(Q.*cos(2*point(k).alpha*pi/180) + ...
                      U.*sin(2*point(k).alpha*pi/180));
    
    % if pointing offsets are non zero add perturbation
    if(any(dphi~=0)|any(dtheta~=0))
      disp('doing pointing offset perturbation')
      % interp the 1st derivs
      
      dTdt=cubicmx(phi_p,theta_p,cmap.map(:,4),phim,thetam,tri,t);
      dTdp=cubicmx(phi_p,theta_p,cmap.map(:,7),phim,thetam,tri,t);
      
      % add in centroid shift perturbation
      v=v + dTdt.*dtheta + dTdp.*dphi;
    end
    
    % translate the beam shape parameters from get_array_info
    % into s,r,p form
    % the next few lines will migrate into gen_pointing
    rapt=d.pointing.cel.ra(mapind);
    decpt=d.pointing.cel.dec(mapind);
    r=q.r(k);
    th=q.theta(k)-d.pointing.hor.dk;
    alphaEll=chi2alpha(rapt,decpt,r,th(mapind),q.alpha(k),q.theta(k)-d.pointing.hor.dk(mapind));
    % the ellipse parameterization assumes the angle to be counted from x
    [s,c,p]=egauss2_mmt2scp(q.fwhm_maj(k),q.fwhm_min(k),alphaEll+90);
    
    % use presence of (scalar) sigmapbeamfwhms as indication that
    % we are to apply beam width and ellipticity perturbations
    if(~isempty(simopt.sigmapbeamfwhms))
      disp('doing beam width perturbation')
      % translate the beam width of the input map which is fwhm in
      % armin to sigma in degrees
      % sigmamap was set above to be the common A/B beamwidth
      sigmamap=cmap.fwhm/sqrt(8*log(2));
      % put s and sigmamap into radians
      s=s*pi/180; sigmamap=sigmamap*pi/180;
      % take the delta of the squares of beam width versus beam of
      % input map
      delsigsq=s.^2-sigmamap.^2;
      % calc 2nd derivs
      dTdtdt=cubicmx(phi_p,theta_p,cmap.map(:,10),phim,thetam,tri,t);
      dTdpdp=cubicmx(phi_p,theta_p,cmap.map(:,16),phim,thetam,tri,t);
      % add in beam width perturbation
      v=v + (delsigsq/2)*(dTdtdt+dTdpdp);
      
      % if beam is elliptical continue to add perturbation
      if(c~=0|p~=0)
        disp('doing ellipticity perturbation')
        % calc the final deriv
        dTdtdp=cubicmx(phi_p,theta_p,cmap.map(:,13),phim,thetam,tri,t);
        % add in the ellipticity perturbation
        v=v + sigmamap.^2*(p.*(dTdpdp-dTdtdt)/2 + c.*dTdtdp);
      end
    end
    
    
   case{'doublegauss'}
    % fetch the seperate TODs for the two sub-beams:
    % force common pixel for subbeams:        
    % x1 = [point(k).x1, point(k).x]; y1 = [point(k).y1, point(k).y];
    % x2 = [point(k).x2, point(k).x]; y2 = [point(k).y2, point(k).y];
    % g1 = healpixmap2tod(cmap,x1,y1,point(k).alpha,q.gamma(k),'taylor',simopt.coord);
    % g2 = healpixmap2tod(cmap,x2,y2,point(k).alpha,q.gamma(k),'taylor',simopt.coord);
    
    % regular version        
    g1 = healpixmap2tod(cmap,point(k).x1,point(k).y1,point(k).alpha,q.gamma(k),'taylor',simopt.coord);
    g2 = healpixmap2tod(cmap,point(k).x2,point(k).y2,point(k).alpha,q.gamma(k),'taylor',simopt.coord);
    
    % the average corresponds to the approximated elliptical beam:
    v = (g1+g2)/2;
    
    
   case 'multigauss'
    v = zeros(length(point(k).x(:,1)),1);
    % do a loop over the sub-Gaussians and sum them according to their relative amplitudes
    for c = 1:length(rel_amps)
      v = v + rel_amps(c) * healpixmap2tod(cmap(c),point(k).x(:,c),point(k).y(:,c),point(k).alpha,q.gamma(k),'taylor',simopt.coord);
    end
    
    
   case 'healconvolve'
    % convolve beam directly from healpix map
    
    % extract pointing from point structure
    % and bv and oc for beam convolution
    x=point(k).x;
    y=point(k).y;
    alpha=point(k).alpha;
    oc.r=point(k).oc.r;
    oc.th=point(k).oc.th;
    bv=point(k).bv;
    
    % for each offset location within the beam
    % (this loop is the slow part)
    interpmap=zeros(length(x),3);
    for l=1:numel(oc.r)
      % apply the offset to the pointing position								
      [yp,xp]=reckon(y,x,oc.r(l),oc.th(l));
      % add in the tod interpolated at the offset position with weighting
      interpmap=interpmap+repmat(bv(:,l),[1,3]).*healpix_interp(cmap,xp,yp,'taylor',simopt.coord);
    end
    
    % construct the tod
    v=interpmap(:,1) + q.gamma(k)*(interpmap(:,2).*cos(2*alpha*pi/180) + ...
                                   interpmap(:,3).*sin(2*alpha*pi/180));
    
    
   case 'linear'

    % interpolate from ra/dec flat map
    
    % Constructing an intermediate map requires a constant dk angle. Because of
    % jitter in the mount, we must use the mean of the dk angles.
 
    if isfield(simopt,'curveskyrescale') && simopt.curveskyrescale
      % like linear but construct the flat map on the fly so that the pixels are square
      % along the scan declination
      ad=point(k).ad;
      x=point(k).x;
      y=point(k).y;
      
      % find the beam map pixel spacing and x/y span
      bmps=ad.Field_size_deg/ad.N_pix;
      bmsx=max(ad.t_val_deg{1});
      bmsy=max(ad.t_val_deg{2});
      % find the box on the sky containing the scanned trajectory
      xl=min(x(:)); xu=max(x(:));
      yl=min(y(:)); yu=max(y(:));
      % find the x direction curved sky scale factor
      xsf=1/cosd(mean(y(:)));
      % expand the box by the size of the beammaps
      xl=xl-bmsx*xsf; xu=xu+bmsx*xsf;
      yl=yl-bmsy;     yu=yu+bmsy;
      % make a grid of points
      cmap.x_tic=xl:bmps*xsf:xu;
      cmap.y_tic=yl:bmps:yu;
      [xx,yy]=meshgrid(cmap.x_tic,cmap.y_tic);
      % interpolate from healpix map
      sm=healpix_interp(cmap,xx(:),yy(:),'taylor');
      sm=reshape(sm,[size(xx),3]);
      cmap.T=sm(:,:,1); cmap.Q=sm(:,:,2); cmap.U=sm(:,:,3);
      
      % Get alpha on the fly.
      % Construct the map as viewed by bolo orientation w.r.t. north at the pixel
      [xgrid,ygrid]=meshgrid(cmap.x_tic,cmap.y_tic);
      [rapt,decpt]=coord2point(xgrid,ygrid,qp.r(k),qp.theta(k));
      a=chi2alpha(rapt,decpt,qp.r(k),qp.theta(k),qp.chi(k),qp.chi_thetaref(k));
      alpha=reshape(a,size(rapt));
    else
      % abgain included as well
      x=point(k).x;
      y=point(k).y;
      alpha=point(k).alpha;
    end

    if isfield(point(k),'Tbeam');
      if ismember(k,ind.b)
        absgn=-1;
      else
        absgn=+1;
      end
      
      % Input T sky (cmap.T) is an array with dimension 1 = increasing dec,
      % dimension 2 = increasing RA.  The beammap (point(k).Tbeam) is the
      % instantaneous projection onto the sky of the beam with dimension 1 =
      % decreasing elevation and dimenstion 2 = increasing AZ.  For
      % DK=drumangle=0, this means dimension 1 = -y', dimension 2 = +x'.
      % For drumangle=90, this rotates to dimension 1 = +x', dimension 2 = +y'.      
      sky=conv2(cmap.T,flipud(fliplr(point(k).Tbeam)),'same') + ...
          absgn*conv2((cmap.Q.*cosd(2*alpha)+cmap.U.*sind(2*alpha)),flipud(fliplr(point(k).Qbeam)),'same') + ...
          absgn*conv2((cmap.U.*cosd(2*alpha)-cmap.Q.*sind(2*alpha)),flipud(fliplr(point(k).Ubeam)),'same');
      
    else
      sky=cmap.T + q.gamma(k)*(cmap.Q.*cos(2*alpha*pi/180) + ...
                               cmap.U.*sin(2*alpha*pi/180));
    end
    
    % interp2 fails when x/y are NaN which is silly as it should just
    % return NaN for the interp val
    v=interp2nan(cmap.x_tic,cmap.y_tic,sky,x,y,simopt.siginterp);
    
  end
  
  % translate temperature into detector units
  v = v./ukpervolt(k);
  
  % sim ab gain cal mismatch
  v=v*q.abgain(k);
  
  % replace detector data
  d.sim(mapind,k)=d.sim(mapind,k)+cvec(single(v));
  
  % plot some data to test
  if(0)
    clf
    subplot(4,1,1);
    plot_map(cmap,cmap.T); colorbar; title('Flat unsmoothed map');
    subplot(4,1,2)
    imagesc(point(k).ad.t_val_deg{1},point(k).ad.t_val_deg{2},point(k).Tbeam-point(k-1).Tbeam);
    axis xy; axis square; colorbar; title('Tbeam');
    subplot(4,1,3)
    plot_map(cmap,sky); colorbar; title('convolved sky');
    hold on; plot(x,y,'k'); hold off
    subplot(4,1,4)
    z=find(mapind);
    plot(d.sim(z(1:800),k).*ukpervolt(k)-d.sim(z(1:800),k-1).*ukpervolt(k-1),'r');
    plot(d.sim(z(1:800),k).*ukpervolt(k)-d.sim(z(1:800),k-1).*ukpervolt(k-1)-gt);
    title('sim timestream'); xlabel('time (samples in first 2 halfscans)');
    keyboard
  end
  
end

toc

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [delta,width_dg] = get_doublegauss_par(fwhm_maj, fwhm_min);
% returns the parameter for the double gaussian approximation, currently to the procedure described in:
% http://bicep0.caltech.edu/~spuder/analysis_logbook/analysis/20120703_ElliptBeamApprox/BeamEllipticity.html

% this valid in a certain range of ellipticities, otherwise give warning
e = 2 * (fwhm_maj - fwhm_min)/(fwhm_maj + fwhm_min);
if (e>0.11) 
  warning(['Using double Gaussian approximation to elliptical beams above valid ellipticities of e = 0.11: (' num2str(e) ')']);
end

% the optimal beamwidth used in the double gauss approximation is alsmost the minor axis width of the ellipse  
width_dg = 1.0005 * fwhm_min;

% the offset of the Gaussians, carefull the formula assumes sigma and not FWHM:
delta = 2.8175 * width_dg/sqrt(8*log(2)) * sqrt(e);

% catch the special case of no ellipticity:
if (fwhm_maj==fwhm_min)
  delta=0;
  width_dg=fwhm_maj;
end

return
  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [widths,shifts,amps] = get_multigauss_par(fwhm_maj, fwhm_min);
% [widths,shifts,amps] = get_multigauss_par(fwhm_maj, fwhm_min);
% returns the parameter for the multi gaussian approximation

persistent mgp;
if (size(mgp,1)==0) 
  mgp = get_multigauss_supp();
end

% this valid in a certain range of ellipticities, otherwise give warning
e = 2 * (fwhm_maj - fwhm_min)/(fwhm_maj + fwhm_min);
fwhm_nom = (fwhm_maj+fwhm_min)/2;

if (e<1e-5) %one Gaussian
  widths = interp1(mgp.i1(:,1),mgp.i1(:,2),e,'spline');
  shifts = 0; 
  amps = 1;
elseif (e>=1e-5 & e<0.0025) %2 Gaussians
  widths = interp1(mgp.i2(:,1),mgp.i2(:,2),e,'spline');
  shifts = interp1(mgp.i2(:,1),mgp.i2(:,3),e,'spline');
  amps = [0.5,0.5];
  widths=[widths,widths];
  shifts=[shifts,-shifts];
elseif (e>=0.0025 & e<0.03) %3 Gaussians
  widths = interp1(mgp.i3(:,1),mgp.i3(:,2:3),e,'spline');
  shifts = interp1(mgp.i3(:,1),mgp.i3(:,4),e,'spline');
  amps   = interp1(mgp.i3(:,1),mgp.i3(:,5),e,'spline');
  widths=[widths(2),widths];
  shifts=[shifts,0,-shifts];
  amps  =[(1-amps)/2,amps,(1-amps)/2];
elseif (e>=0.03) %4 Gaussians
  if(e>0.15) warning(['Using multi Gaussian approximation to elliptical beams above valid ellipticities of e = 0.15 (' num2str(e) ')']); end
  widths = interp1(mgp.i4(:,1),mgp.i4(:,2:3),e,'spline');
  shifts = interp1(mgp.i4(:,1),mgp.i4(:,4:5),e,'spline');
  amps   = interp1(mgp.i4(:,1),mgp.i4(:,6),e,'spline');
  widths=[widths(end:-1:1),widths];
  shifts=[shifts(end:-1:1),-shifts];
  amps  =[(1-amps)/2,amps/2,amps/2,(1-amps)/2];
end

% the values support points were simulated for a nominal beam width of 31.22 arcmin,
% finally scale to the current nominal beam width:
widths = widths * fwhm_nom/(31.22/60);
shifts = shifts * fwhm_nom/(31.22/60);

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [v,loc]=healpixmap2tod(cmap,x,y,alpha,gamma,siginterp,coord)
% interpolates the map to the pointing trajectory and constructs the TOD by combining T,Q & U

% interpolate off the healpix map at required coords
[interpmap,loc]=healpix_interp(cmap,x,y,siginterp,coord);

% construct the tod
v=interpmap(:,1) + gamma*(interpmap(:,2).*cos(2*alpha*pi/180) + ...
                          interpmap(:,3).*sin(2*alpha*pi/180));

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function d=gen_white_noise(d,fs,wnlev,ind)

% add white noise at specified level

% assume input wnlev is noise averaging over 1 second
tint=d.tf(2)-d.tf(1);
wnlev=wnlev./sqrt(tint);

mapind=make_mapind(d,fs);

% allow the addition of noise to signal
d.sim(mapind,ind.l100)=d.sim(mapind,ind.l100)+...
  wnlev(1)*randn([sum(mapind),length(ind.l100)]);
d.sim(mapind,ind.l150)=d.sim(mapind,ind.l150)+...
  wnlev(2)*randn([sum(mapind),length(ind.l150)]);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function covnoi=prep_cov_noise(d,fs,ch)
% output covnoi: .cd, .cdf, .nset 
% which will be used for all realiz by gen_cov_noise

disp('preparing cov noise matrices...');

% get samp rate
[sampratio,samprate]=get_sampratio_rate(d);

% the number of channels we are generating for
m=length(ch);

% super crude gap fill which just linear interps across gap - this is
% centainly not OK if we have more than few % of gaps
d=gap_fill(d,fs,ch);

% Filter half scans and keep the poly coefficients
[d,lm_coeff]=filter_scans(d,fs,'c1',ch);

% PART 1 - MEASURE AND RE-GEN LOW FREQ NOISE USING LEGENDRE
% POLYNOMIALS
if(1)
  nset(1)=1;
  
  % for each "spectrum set"
  for i=1:length(fs.s)/nset(1)
    
    % PART 1A - FIND THE JOINT COVARIANCE BEWEEN ALL THE CHAN/MODES
  
    % permute from inst/chan/coeff to chan/coeff/inst
    % - we drop the offset mode as it's huge and useless
    s=(i-1)*nset(1)+1;
    e=i*nset(1);    
    x=permute(lm_coeff(s:e,:,2:end),[2,3,1]);
  
    % concat chan/coeff dims
    x=reshape(x,[size(x,1)*size(x,2),size(x,3)]);
  
    % take the covariance matrix
    lm_covmat=(x*x')/size(x,2);
    
    % perform the chol decomp
    cd(:,:,i)=get_chol(lm_covmat);
  end  
  
  % save cd matrix
  covnoi.cd=cd;

end

% PART 2 - MEASURE AND RE-GEN THE HIGH FREQ NOISE USING BINNED FOURIER MODES
if(1)
  nset(2)=1;
  
  ftd=[];
  
  % for each "spectrum set"
  for i=1:length(fs.s)/nset(2)

    % PART 2A - FIND THE CHAN-CHAN COV MAT OF THE BINNED FOURIER MODES

    tic
    
    clear ftd
    % accumulate over half-scans in this set
    for k=1:nset(2)
    
      % get the half-scan
      l=(i-1)*nset(2)+k;
      s=fs.sf(l); e=fs.ef(l);
      n=e-s+1;
      
      % fetch the real data
      v=double(d.mce0.data.fb(s:e,ch));
	
      % windowing is extremely problematic -
      % after many struggles this was as good as any I found
      w=tukeywin(n,0.2); % just roll off the ends
      
      v=bsxfun(@times,v,w);
      
      n=2^nextpow2(n*1.5);
      
      % do fft with padding
      ft=fft(v,n);
      
      % undo normalization change due to window application (and padding)
      ft=ft*sqrt(n/sum(w.^2));
      
      % throw away neg freq part and zero freq
      ft=ft(2:n/2+1,:);
      
      % on first pass pre-allocate the output array
      if(k==1)
	ftd=zeros([size(ft,1),nset(2),size(ft,2)]);
      end
	
      % store the data for this half scan - note this will fail if all
      % half scans in set are not exact same length
      % (use this funny index order to simplify code below)
      ftd(:,k,:)=ft;
    end
    
    % calc the half freq axis (exc zero freq)
    f=[1:n/2]'*samprate/n;
    
    % first few modes are one per bin - then log spaced bins
    nind=10; nlog=60;
    fm_be(1:nind)=f(1:nind)-1e-9;
    lf=log10(f);
    fm_be(nind+1:nind+nlog+1)=10.^linspace(lf(nind+1)-1e-9,lf(end)+1e-9,nlog+1);
    
    % preallocate for (small) speed benefit
    fm_covmat = zeros(m,m,length(fm_be)-1);
    cdf = zeros(m,m,length(fm_be)-1);
    
    % for each bin
    for j=1:length(fm_be)-1
      % find the modes in this bin
      fp=fm_be(j)<f&f<=fm_be(j+1);
      
      % pull out the modes in this bin
      tb=ftd(fp,:,:);
      
      % concat mode/nset dims together
      tb=reshape(tb,[size(tb,1)*nset(2),m]);
      
      % we can now take matrix product to gen cov mat for this freq bin
      fm_covmat(:,:,j)=(tb'*tb)/size(tb,1);
      
      % and gen the cov mat
      cdf(:,:,j,i)=get_chol(fm_covmat(:,:,j));
    end
  end

  % save output
  covnoi.cdf=cdf;
  covnoi.nset=nset;
  
end

return

%%%%%%%%%%%%%%%%%%%%%%%
function d=gen_cov_noise(d,fs,ch,covnoi)
% use cd and cdf from prep_cov_noise to generate noise

nset=covnoi.nset;
cd=covnoi.cd;
cdf=covnoi.cdf;

% the number of channels we are generating for
m=length(ch);
  
% PART 1B - RE-GEN THE LOW FREQ NOISE 

% for each "spectrum set"
for i=1:length(fs.s)/nset(1)

  % for each half scan
  for k=1:nset(1)
    % get scan pointers
    l=(i-1)*nset(1)+k;
    s=fs.sf(l); e=fs.ef(l);
    n=e-s+1;
  
    % gen uncorrelated random poly coeff
    coeff=randn(1,size(cd,1));
    % scale and mix the coeff using chol decomp
    coeff=coeff*cd;

    % construct the poly mode array
    x=linspace(-1,1,n)';
    clear X
    for j=1:size(coeff,2)/m
      X(:,j)=chebpoly(j,x);
    end

    % (Vectorized algorithm)
    % make a coefficient matrix (Ncoeff x Nbolo)
    c = reshape(coeff,[],m); % make a coefficient matrix (Ncoeff x Nbolo)
    v = X*c;

    % keep the pcoeff for testing (same index order as returned from filter_scans)
    ckeep(l,:,:)=c';

    % add into big array
    d.sim(s:e,ch)=d.sim(s:e,ch)+v;
  end
end

% PART 2B - RE-GEN THE HIGH FREQ NOISE

tic

% for each "spectrum set"
for i=1:length(fs.s)/nset(2)

  % for each half scan in the set
  for k=1:nset(2)
    % get scan pointers
    l=(i-1)*nset(2)+k;
    s=fs.sf(l); e=fs.ef(l);
    n=e-s+1;
    n=2^nextpow2(n*1.5);
  
    % gen uncorrelated rand numbers
    % note that since we use "symmetric" switch in ifft below we do
    % everything "one sided" for simplicity and speed
    x=zeros(n,m);
    x(2:n/2,:)=complex(randn(n/2-1,m),randn(n/2-1,m))/sqrt(2);
    x(n/2+1,:)=randn(1,m); % nyquist real

    % get samp rate
    [sampratio,samprate]=get_sampratio_rate(d);
  
    % calc the half freq axis (exc zero freq)
    f=[1:n/2]'*samprate/n;
  
    % first few modes are one per bin - then log spaced bins
    nind=10; nlog=60;
    fm_be(1:nind)=f(1:nind)-1e-9;
    lf=log10(f);
    fm_be(nind+1:nind+nlog+1)=10.^linspace(lf(nind+1)-1e-9,lf(end)+1e-9,nlog+1);
  
    % for each f mode bin
    for j=1:length(fm_be)-1  
      % find the modes in this bin
      ind=find(fm_be(j)<f&f<=fm_be(j+1));
      % x includes zero freq mode - f doesn't
      ind=ind+1;
      x(ind,:)=x(ind,:)*cdf(:,:,j,i);
    end
      
    % ft to get sim time stream
    v=ifft(x,'symmetric');
      
    % add sim noise into output array cutting down to required length
    v=single(real(v)); 
    d.sim(s:e,ch)=d.sim(s:e,ch)+v(1:e-s+1,:);

    % for test purpose only make auto spectra
    if(0)
      % re-fft
      ft=fft(v);
      % throw away symmetric part
      nup=ceil((n+1)/2);
      ft=ft(2:nup,:);
      % generate auto spectra
      hs=ft.*conj(ft);
      % force result real instead of fold-over-and-add which does
      % the same thing
      hs=real(hs);
      % store for plot
      ps(:,:,k)=hs;
    end
  end

end  
  
toc 
    
% plot example resampled auto spectra
if(0)
  clf
  set(gcf,'DefaultAxesColorOrder',jet(nset(2)));
  loglog(f',squeeze(ps(:,1,:)),'.');
  mu=squeeze(fm_covmat(1,1,:));
  for j=1:length(fm_be)-1
    line(fm_be(j:j+1),[mu(j),mu(j)],'LineWidth',5,'Color','k');
  end
  title(sprintf('channel 1-1 spectra as regenerated for scan set %d',i));
  pause
end  

% when testing plot the input and output
if(0)
  setwinsize(gcf,600,600); clf
  for i=1:length(fs.s)
    % get scan pointers
    s=fs.sf(i); e=fs.ef(i);
    n=e-s+1;

    t=(d.t(s:e)-d.t(fs.sf(i)))*86400;
    
    set(gcf,'DefaultAxesColorOrder',jet(length(ch)));
    subplot(2,1,1)
    x=d.mce0.data.fb(s:e,ch);
    x=bsxfun(@minus,x,mean(x));
    plot(t,x); axis tight; %ylim([-0.1,0.1]);
    ylabel('Real (V)'); xlabel('time (s)')
  
    subplot(2,1,2)
    plot(t,d.sim(s:e,ch)); axis tight; %ylim([-0.1,0.1]);
    ylabel('Sim (V)'); xlabel('time (s)')

    gtitle(sprintf('Real and Sim section %d',i));
    
    pause
    %drawnow; mkgif(sprintf('movie/%03d.gif',i));
  end
end

return


%%%%%%%%%%%%%%%%%%%%%%%
function cd=get_chol(x)

% Not sure if this is needed anymore
if(all(rvec(x)==0))
  cd=zeros(size(x));
  return
end

% take eigen values/vectors
[V,D] = eig(x);

% set small/negative eigenvalues to a small positive value
D=diag(D);
D=max(D,(max(D)/1e12)*ones(size(D)));
    
% regenerate the matrix which should now be positive definite
z=V*diag(D)*V';

% get rid of small imag component on diag that appears to be caused by
% numerical precision limits
z=(z+z')/2;

% take the chol decomposition
cd=chol(z);
    
% look at effect of "pos-def fixup"
if(0)
  subplot(1,2,1)
  imagesc(x); colorbar
  title('input fourier cov matrix')
  subplot(1,2,2)
  imagesc(x-z); colorbar
  title('delta of input and output') 
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function d=gap_fill(d,fs,ind)
% d=gap_fill(d,fs,ind)
%
% fill in the de-glitch gaps
%
% at the moment very crude linear interp
% note that this algorithm is intended to be used only for filling
% gaps before noise estimation or similar - in particular it replaces
% an all NaN half scan with zero - so resulting data must never be
% used for mapping or similar

disp('gap_fill... (linear interp across NaNs)')

% for each period
for i=1:length(fs.sf)
  
  % fetch the data
  s=fs.sf(i); e=fs.ef(i);
  v=d.mce0.data.fb(s:e,:);
  n=size(v,1);
  
  % for each channel
  for j=ind
    
    o = v(:,j);
    scv = find_blk(isnan(o),length(d.t)/length(d.ts));
    
    for kk=1:length(scv.s)
      sn = scv.s(kk); en = scv.e(kk);
      % if the start is the first index and the end is not the last index
      if(sn==1&&en~=n)
	% fill with value after end
	v(sn:en,j)=v(en+1,j);
      end
	  
      % if the end is the last index and the start is not the first
      % index
      if(en==n&&sn~=1)
	% fill with last good value
	v(sn:en,j)=v(sn-1,j);
      end
	  
      % if the start and end are clear of the ends
      if(sn~=1&&en~=n)
	% interpolate across the glitch
	sv=v(sn-1,j); ev=v(en+1,j);
	l=en-sn+3;
	int=linspace(sv,ev,l);
	
	v(sn:en,j)=int(2:end-1);
	
	% plot when testing
	if(0)
	  plot(v(:,j));
	  hold on
	  plot(o,'r')
	  hold off
	  [i j sn en l]
	  pause
	end
	
      end
      
      % note that if a channel is NaN for the entire trace we
      % set it to zero
      if(sn==1&&en==n)
	v(sn:en,j)=0;
      end
      
    end
	
  end

  % put the data back
  d.mce0.data.fb(s:e,:)=v;
end

return
