function add_alms(signal_infile, noise_infile, signalpnoise_outfile)
%  add_alms(signal_infile, noise_infile, signalpnoise_outfile)
%  
%  Adds signal and noise alms

display('adding signal and noise alms...')

% fetch info explicitly to have a template hdr for the output file:
info=fitsinfo(signal_infile);

% signal_alms
% whatever is loaded in with read_fits_alms is converted to uK
s_almT=read_fits_alms(signal_infile,1);
s_almQ=read_fits_alms(signal_infile,2);
s_almU=read_fits_alms(signal_infile,3);

% noise_alms
% whatever is loaded in with read_fits_alms is converted to uK
n_almT=read_fits_alms(noise_infile,1);
n_almQ=read_fits_alms(noise_infile,2);
n_almU=read_fits_alms(noise_infile,3);

% in principle we can write the alms to a fits file in whatever
% units we want. The pipeline code will take care of this.
% Unfortunatly synfast does not: it will use the units of the
% input spectrum and attach them to the alms.
% Hence we want to keep the unit of the signal alm before conversion
% by the pipeline, which presumably matches the unit of the input spectrum
unit = get_unit(info.BinaryTable(1).Keywords);
conv_fac = 1;
if strcmp(unit, 'K') conv_fac = 1e-6; 
elseif strcmp(unit,'mK') conv_fac = 1e-3;
elseif strcmp(unit,'uK') conv_fac = 1;
elseif strcmp(unit,'muK') conv_fac = 1;
elseif strcmp(unit,'microK') conv_fac = 1;
end
 
% add signal and noise, first column is index
sn_alm(:,:,1) = [s_almT.index (s_almT.alms + n_almT.alms)*conv_fac];
sn_alm(:,:,2) = [s_almQ.index (s_almQ.alms + n_almQ.alms)*conv_fac];
sn_alm(:,:,3) = [s_almU.index (s_almU.alms + n_almU.alms)*conv_fac];

% save signal + noise alms:
fitswrite_bintable(sn_alm, info, signalpnoise_outfile);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function hdr = set_units_to(unit,hdr)
for ii =1:size(hdr,1)
  if any(strfind(hdr{ii,1},'TUNIT'))
    % skip ones that are not associated with temperature:
    if any(strfind(hdr{ii,2},'K')) hdr{ii,2} = unit; end
  end
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function unit = get_unit(hdr)
for ii =1:size(hdr,1)
  if any(strfind(hdr{ii,1},'TUNIT2')) 
    unit=hdr{ii,2};
    return
  end
end
return

