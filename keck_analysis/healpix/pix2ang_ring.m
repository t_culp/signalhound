function [theta, phi] = pix2ang_ring(nside, ipix)
% [theta, phi] = pix2ang_ring(nside, ipix)
%
%       renders Theta and Phi coordinates of the nominal pixel center
%       given the RING scheme pixel number Ipix and map resolution parameter Nside
%
% INPUT
%    Nside     : determines the resolution (Npix = 12* Nside^2)
%	SCALAR
%    Ipix  : pixel number in the RING scheme of Healpix pixelisation in [0,Npix-1]
%	can be an ARRAY 
%       pixels are numbered along parallels (ascending phi), 
%       and parallels are numbered from north pole to south pole (ascending theta)
%
% OUTPUT
%    Theta : angle (along meridian = co-latitude), in [0,Pi], theta=0 : north pole,
%	is an ARRAY of same size as Ipix
%    Phi   : angle (along parallel = azimut), in [0,2*Pi]
%	is an ARRAY of same size as Ipix
%
% SUBROUTINE
%    nside2npix
%
% HISTORY
%    June-October 1997,  Eric Hivon & Kris Gorski, TAC
%    Aug  1997 : treats correctly the case nside = 1
%    Feb 1999,           Eric Hivon,               Caltech
%         renamed pix2ang_ring
%    Sept 2000,          EH
%           free memory by discarding unused variables
%    June 2003,  EH, replaced STOPs by MESSAGEs
%    Feb 2006, CLP took this file from Healpix 2.01 distribution and
%              translated to Matlab following example of JMK
%

% -----------------------------------------------------------------------------
%
%  Copyright (C) 1997-2005  Krzysztof M. Gorski, Eric Hivon, Anthony J. Banday
%
%
%
%
%
%  This file is part of HEALPix.
%
%  HEALPix is free software% you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation% either version 2 of the License, or
%  (at your option) any later version.
%
%  HEALPix is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY% without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with HEALPix% if not, write to the Free Software
%  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
%
%  For more information about HEALPix see http://healpix.jpl.nasa.gov
%
% -----------------------------------------------------------------------------

if(length(nside)>1)
  error('nside should be a scalar');
end

npix = nside2npix(nside);

if(~exist('ipix'))
  ipix=[0:npix-1]';
end

% force ipix to be 1-D
array_shape = size(ipix);
ipix = ipix(:);

if(any(round(ipix) ~= ipix))
  error('Pixel numbers must be integer');
end

nside = double(nside);
nl2 = 2*nside;
nl3 = 3*nside;
nl4 = 4*nside;
ncap = nl2*(nside-1);
nsup = nl2*(5*nside+1);
fact1 = 1.5*nside;
fact2 = (3*nside)*nside;
np = length(ipix);
theta = zeros(size(ipix));
phi   = zeros(size(ipix));

min_pix = min(ipix);
max_pix = max(ipix);
if(min_pix<0|max_pix>npix-1)
  error('pixel values out of range');
end

% north polar cap ; ---------------------------------
pix_np = find(ipix<ncap);
if(length(pix_np>0))

  ip = ipix(pix_np) + 1;
  iring = floor(sqrt(ip/2 - sqrt(floor(ip/2)))) + 1; % counted from NORTH pole, starting at 1
  iphi  = ip - 2*iring.*(iring-1);
  
  theta(pix_np) = acos( 1-iring.^2 ./ fact2);
  phi(pix_np)   = (iphi - 0.5) * pi./(2*iring);
  
end % ------------------------------------------------------------------------

% equatorial strip ; ---------------------------------
pix_eq = find((ipix >= ncap) & (ipix < nsup));
if(length(pix_eq>0))

  ip    = ipix(pix_eq) - ncap;
  iring = floor(ip/nl4) + nside; % counted from NORTH pole
  iphi  = mod(ip,nl4) + 1;
  
  fodd  = 0.5 * (1 + mod(iring+nside,2)); % 1 if iring is odd, 1/2 otherwise
  
  theta(pix_eq) = acos((nl2 - iring) / fact1);
  phi(pix_eq)   = (iphi - fodd) * pi/(2*nside);

end % ------------------------------------------------------------------------

% south polar cap ; ---------------------------------
pix_sp = find(ipix >= nsup);
if((length(pix_np) + length(pix_sp) + length(pix_eq))~=np)
  error('npix dont add up')
end
if(length(pix_sp>0))
  
  ip =  npix - ipix(pix_sp);
  iring = floor(sqrt(ip/2 - sqrt(floor(ip/2)))) + 1; % counted from SOUTH pole, starting at 1
  iphi  = 4*iring + 1 - (ip - 2*iring.*(iring-1));
  
  theta(pix_sp) = acos( - 1 + iring.^2 / fact2);
  phi(pix_sp)   = (iphi - 0.5) * pi./(2*iring);
  
end % ------------------------------------------------------------------------

% make output same shape as input
theta = reshape(theta,array_shape);
phi   = reshape(phi  ,array_shape);

return

%=======================================================================
% The permission to use and copy this software and its documentation, 
% without fee or royalty is limited to non-commercial purposes related to 
% Microwave Anisotropy Probe (MAP) and
% PLANCK Surveyor projects and provided that you agree to comply with
% the following copyright notice and statements,
% and that the same appear on ALL copies of the software and documentation.
%
% An appropriate acknowledgement has to be included in any
% publications based on work where the package has been used
% and a reference to the homepage http://www.tac.dk/~healpix
% should be included
%
% Copyright 1997 by Eric Hivon and Kris Gorski.
%  All rights reserved.
%=======================================================================
