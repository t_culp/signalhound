function reduc_synfastdriver()  
% reduc_synfastdriver()  
%  
% just go into this function and run the subfunctions by hand
% they specify the options to gen_map that were used to generate
% the Bicep2/Keck 150 and Keck 100 input maps
% 
% let your input_maps folder point to /n/panlfs2/bicep/keck/pipeline/input_maps
% to run at full speed
%
% then sync back to bicepfs2:
% /n/bicepfs1/bicep2/pipeline/input_maps_synfastv2.20a/
%
% for Odyssey, put in your .bashrc:
% source /n/home03/sfliescher/Programs/exportrc 
% to get the installation of our version of synfast
%
% Otherwise, install Stefan's synfast code, see instructions:
% http://bicep.caltech.edu/~spuder/keck_analysis_logbook/analysis/20120406_PatchSynfast/

if(0)
  make_simset_rlz150()
end

if(0)
  make_simset_rlz100()
end

if(0)
  make_simset_rlz220()
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_simset_rlz150()
  % here some code that directly make the maps needed for a simset with constrained rlz

  % the unlesend sims can run on Odyssey:
  cmd = {}
  for rlz = 0:499
    crlz = num2str(rlz);
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',      0,''B2bbns'','''',     0,''Pl143'',dry_run)'];  % EnoB w/o lensing (type2)
    cmd{end+1} = ['gen_map(' crlz ', 512,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',      0,''B2bbns'',''Pl143'',0,''Pl143'',dry_run)'];  % EnoB w/o lensing (deproj. of types 2,3,4)
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/camb_planck2013_r0p1_noE.fits'',0,''B2bbns'',''''     ,0,''Pl143'',dry_run)'];  % BnoE w/o lensing (type4)
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/dust_150_3p75.fits'',           0,''B2bbns'',''''     ,0,'''',dry_run,1,',crlz,')'];  % dust (type3)
  end

  % make sure you have the farmfiles/synfast dir...
  for ccmd = cmd
    dry_run = 1;
    map_name = eval(ccmd{1});
    if ~exist(map_name,'file')
      dry_run = 0;
      farmit('farmfiles/synfast/',ccmd,'var','dry_run','queue','serial_requeue','maxtime',60,'mem',10000,'submit',1);
    else
      disp([map_name,' exists'])
    end
  end

  % the lensed ones have to run on spud so that simlensspud works. 
  % a working executable is spud: /home/fliescher/Programs/lenspix/simlensspud
  cmd = {}
  for rlz = 0:499
    crlz = num2str(rlz);
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',1,''B2bbns'','''',0,''Pl143'')'];% EnoB w/ lensing (type5)
    cmd{end+1} = ['gen_map(' crlz ',512,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',1,''B2bbns'',''Pl143'',0,''Pl143'')']; % EnoB w/ lensing (deproj. of type5)
  end
  for ccmd = cmd
    ccmd{1}
    eval(ccmd{1})
  end
  
  for ccmd = cmd
    map_name = eval(strrep(ccmd{1},')',',1)'));
    if ~exist(map_name,'file')
      farmit('farmfiles/synfast/',ccmd,'queue','serial_requeue','maxtime',60,'mem',10000,'submit',0);
    else
      disp([map_name,' exists'])
    end
  end
  
  
return

function make_simset_rlz100()
  % here some code that directly make the maps needed for a simset with constrained rlz

  % the unlesend sims can run on Odyssey:
  cmd = {}
  for rlz = 0:499
    crlz = num2str(rlz);
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',0,''Kuber100'','''',0,''Pl100'',dry_run)'];% EnoB w/o lensing (type2)
    cmd{end+1} = ['gen_map(' crlz ', 512,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',0,''Kuber100'','''',0,''Pl100'',dry_run)'];% EnoB w/o lensing (type2), for matrix reobservation
    cmd{end+1} = ['gen_map(' crlz ', 512,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',0,''Kuber100'',''Pl100'',0,''Pl100'',dry_run)']; % EnoB w/o lensing (deproj. of type 2,3,4)
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/camb_planck2013_r0p1_noE.fits'',0,''Kuber100'','''',0,''Pl100'',dry_run)'];   % BnoE w/o lensing (type4)
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/dust_95_3p75.fits'',0,''Kuber100'','''',0,'''',dry_run,1,',crlz,')'];   % dust (type3)
  end

  % the lensed ones have to run on spud so that simlensspud works. 
  % a working executable is spud: /home/fliescher/Programs/lenspix/simlensspud
  for rlz = 0:499
    crlz = num2str(rlz);
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',1,''Kuber100'','''',0,''Pl100'',dry_run)'];% EnoB w/ lensing (type5)
    cmd{end+1} = ['gen_map(' crlz  ',512,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',1,''Kuber100'',''Pl100'',0,''Pl100'',dry_run)']; % EnoB w/ lensing (deproj. of type5)
  end

  % make sure you have the farmfiles/synfast dir...
  for ccmd = cmd
    dry_run = 1;
    map_name = eval(ccmd{1});
    if ~exist(map_name,'file')
      dry_run = 0;
      farmit('farmfiles/synfast/',ccmd,'var','dry_run','queue','serial_requeue,itc_cluster','maxtime',60,'mem',10000,'submit',1);
      % if you are on spud jst do the eval instead:
      % eval(ccmd{:})
    else
      disp([map_name,' exists'])
    end
  end
return

function make_simset_rlz220()
  % here's some code that directly makes the maps needed for a simset with constrained rlz

  % the unlesend sims can run on Odyssey:
  cmd = {}
  for rlz = 0:499
    crlz = num2str(rlz);
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',0,''Kuber220'','''',0,''Pl217'',dry_run)'];% EnoB w/o lensing (type2)
    cmd{end+1} = ['gen_map(' crlz ', 512,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',0,''Kuber220'','''',0,''Pl217'',dry_run)'];% EnoB w/o lensing (type2), for matrix reobservation
    cmd{end+1} = ['gen_map(' crlz ', 512,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',0,''Kuber220'',''Pl217'',0,''Pl217'',dry_run)']; % EnoB w/o lensing (deproj. of type 2,3,4)
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/camb_planck2013_r0p1_noE.fits'',0,''Kuber220'','''',0,''Pl217'',dry_run)'];   % BnoE w/o lensing (type4)
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/dust_220_3p75.fits'',0,''Kuber220'','''',0,'''',dry_run,1,',crlz,')'];   % dust (type3)
  end

  % the lensed ones have to run on spud so that simlensspud works. 
  % a working executable is spud: /home/fliescher/Programs/lenspix/simlensspud
  for rlz = 0:499
    crlz = num2str(rlz);
    cmd{end+1} = ['gen_map(' crlz ',2048,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',1,''Kuber220'','''',0,''Pl217'',dry_run)'];% EnoB w/ lensing (type5)
    cmd{end+1} = ['gen_map(' crlz  ',512,1536,''aux_data/official_cl/camb_planck2013_r0.fits'',1,''Kuber220'',''Pl217'',0,''Pl217'',dry_run)']; % EnoB w/ lensing (deproj. of type5)
  end

  % make sure you have the farmfiles/synfast dir...
  for ccmd = cmd
    dry_run = 1;
    map_name = eval(ccmd{1});
    if ~exist(map_name,'file')
      dry_run = 0;
      farmit('farmfiles/synfast/',ccmd,'var','dry_run','queue','serial_requeue,itc_cluster','maxtime',60,'mem',10000,'submit',1);
      % if you are on spud jst do the eval instead:
      % eval(ccmd{:})
    else
      disp([map_name,' exists'])
    end
  end
return

