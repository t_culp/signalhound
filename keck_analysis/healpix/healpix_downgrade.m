function hmp=healpix_downgrade(hm)
% hmp=healpix_downgrade(hm)
%
% downgrade the resolution of nested healpix map

%if hm is a string, load the filename
if(isstr(hm))
  hm=read_fits_map(hm);
end

% check the input map is nested
if(~strcmp(hm.ordering,'NESTED'))
  error('healpix_downgrade: Input map must be nested');
end

% check the input map is full sky
if(nside2npix(hm.nside)~=size(hm.map,1))
  error('healpix_downgrade: Input map must be fill sky');
end

disp(sprintf('downgrading from nside=%d to %d',hm.nside,hm.nside/2));

% size of input map
s=size(hm.map);

% preassign output map
mapo=zeros(s(1)/4,s(2));

% for each column of input map
for i=1:s(2)
  m=reshape(hm.map(:,i),[4,s(1)/4]);
  switch hm.type{i}
    case 'HITS'
      mapo(:,i)=sum(m);
    otherwise
      mapo(:,i)=mean(m);
  end
end

% copy input map to output
hmp=hm;

% overwrite input map with downgraded
hmp.map=mapo;

% rewrite the pixel and nside fields
hmp.pixel=(0:size(hmp.map,1)-1)';
hmp.nside=hm.nside/2;

return
