function [n,m] = neighbor_ring(nside,i)
% [n,m]=neighbor_ring(nside,ipix)
%
%   Find the pixels that border the pixel ipix. The output is
%   organized into pixels that share an edge, and pixels that
%   meet at a corner.
%
%   n = [NW,NE,SW,SE] pixels sharing and edge.
%   m = [N, W, E, S]  pixels meeting at a corner.
%
%   For some pixels, the m output may contain NaN, as some
%   healpix pixels have fewer than eight neighbors.
%
%   The corner-touching pixels are only calculated if the
%   output m is being used. Calculation of edge-touching
%   pixels only is faster.

% ensure we have a column vector
i = reshape(i,[],1);

% validate nside
if ~isnumeric(nside) || ~isscalar(nside) || nside<1 ...
   || mod(nside,1)~=0 || mod(log(nside)/log(2),1)~=0
  error(['Invalid argument for nside.']);
end

% if nside==1
%  n = neighbor_ring_nside1(i);
%  return
% end

% define useful constants for specified nside
H = def_numerology(nside);

% validate pixel numbers
if ~isnumeric(i) || any(i<0) || any(i>=H.npix) || any(mod(i,1)~=0)
  error(['Invalid pixel number.']);
end

% flip any southerly pixels so we only have to
% worry about the north polar region
do_flip = (i >= H.npix/2);
i(do_flip) = flip(i(do_flip),H);

% calculate ring number, phi-over-pi
[jring,phipi] = i2jp(i,H);

% break pixels into classes by boundary shape
C = def_classes(jring,phipi,H);

% identify pixels that share an edge
[i_nw i_ne i_sw i_se] = find_nw_ne_sw_se(jring,phipi,H,C);
n = [i_nw i_ne i_sw i_se];
% flip back as needed
n(do_flip,:) = flip(n(do_flip,:),H);

% identify pixels that share a point
if nargout>1
  [i_n i_w i_e i_s] = find_n_w_e_s(jring,phipi,H,C);
  m = [i_n i_w i_e i_s];
  % flip back as needed
  m(do_flip,:) = flip(m(do_flip,:),H);
end

return

%%%%

% helper function FLIP:
% flip healpix indices north-south.
% This was to think about only one polar region, instead of two.
% However, the logic here is about as messy as the south pole would have been.
function i = flip(i,H)

  ispol = (i < H.npolpix) | (H.npix-1-i < H.npolpix);
  isequ = ~ispol;
  jringequ = H.npolring + floor((i(isequ)-H.npolpix) / (4*H.nside));
  % account for even/odd rows in equ region
  isofs = ispol;
  isofs(isequ) = mod(jringequ-H.npolring,2)==1;
  % pixels at phi-over-pi=0 are a special case
  ismer = false (size(isequ));
  ismer(isequ) = ~isofs(isequ) & (i(isequ) - H.npolpix - (jringequ-H.npolring)*(4*H.nside))==0;

  i = H.npix - i;
  i(isofs) = i(isofs)-1;
  i(ismer) = i(ismer)-4*H.nside;

  i = i(:,end:-1:1);

  return

%%%%

% helper function I2JP:
% turn healpix "ring" indices into
% ring number and phi-over-pi
function [jring,phipi] = i2jp(i,H)

  ispol = (i < H.npolpix);
  isequ = ~ispol;

  % ring number for equatorial pixels
  jring = H.npolring + floor((i-H.npolpix) / (4*H.nside));
  % ring number for north-polar pixels
  jring(ispol) = round(sqrt((i(ispol)+1)/2) - 1);

  % number of pixel within its ring
  kphi = i - H.npolpix - (jring - H.npolring)*4*H.nside;
  kphi(ispol) = i(ispol) - 2*jring(ispol).*(1 + jring(ispol));

  % is the first pixel in this ring centered at zero, or offset?
  isofs = true (size (ispol));
  isofs(isequ) = mod (jring(isequ)-H.npolring,2)==1;

  % number in each ring
  npixinring = 4 * H.nside * ones(size(isequ));
  npixinring(ispol) = 4 * (1 + jring(ispol));

  % phi/pi of pixel (evenly spaced within ring)
  phipi = 2 * mod ((kphi + 1/2*isofs) ./ npixinring, 1);

  return

%%%%

% helper function JP2I:
% turn ring number and phi-over-pi
% into healpix "ring" indices.
function i = jp2i(jring,phipi,H)

  C = def_classes(jring,phipi,H);

  % is the first pixel in this ring centered at zero, or offset?
  isofs = true (size (C.ispol));
  isofs(C.isequ) = mod (jring(C.isequ)-H.npolring,2)==1;

  % number in each ring
  npixinring = 4 * H.nside * ones(size(C.isequ));
  npixinring(C.ispol) = 4 * (1 + jring(C.ispol));

  % index of pixel within ring
  kphi = (phipi/2) .* npixinring - 1/2*isofs;
  kphi = mod(round(kphi),npixinring);

  % put it all together
  i = zeros(size(jring));
  i(C.ispol) = kphi(C.ispol) + 2*jring(C.ispol).*(1 + jring(C.ispol));
  i(C.isequ) = kphi(C.isequ) + H.npolpix + (jring(C.isequ)-H.npolring)*4*H.nside;

  return

%%%%

% helper function FIND_NW_NE_SW_SE:
% find neighbor pixels sharing an edge.
function [i_nw i_ne i_sw i_se] = find_nw_ne_sw_se(jring,phipi,H,C)

  % number in each ring
  npixinring = 4 * H.nside * ones(size(jring));
  npixinring(C.ispol) = 4 * (1 + jring(C.ispol));

  phipi2 = phipi - 1./npixinring;
  phipi2(C.leftvert) = phipi(C.leftvert) - 2./npixinring(C.leftvert);
  jring2 = jring - 1;
  jring2(C.leftvert) = jring(C.leftvert);

  i_nw = jp2i(jring2,phipi2,H);

  phipi2 = phipi + 1./npixinring;
  phipi2(C.rightvert) = phipi(C.rightvert) + 2./npixinring(C.rightvert);
  jring2 = jring - 1;
  jring2(C.rightvert) = jring(C.rightvert);

  i_ne = jp2i(jring2,phipi2,H);

  jring2 = jring + 1;
  phipi2 = phipi - 1./npixinring;
  phipi2(C.leftvert) = phipi(C.leftvert) - 0.5./npixinring(C.leftvert);

  i_sw = jp2i(jring2,phipi2,H);

  jring2 = jring + 1;
  phipi2 = phipi + 1./npixinring;
  phipi2(C.rightvert) = phipi(C.rightvert) + 0.5./npixinring(C.rightvert);

  i_se = jp2i(jring2,phipi2,H);

  return

%%%%

% helper function FIND_N_W_E_S:
% find neighbor pixels sharing a corner.
function [i_n i_w i_e i_s] = find_n_w_e_s(jring,phipi,H,C)

  % number in each ring
  npixinring = 4 * H.nside * ones(size(jring));
  npixinring(C.ispol) = 4 * (1 + jring(C.ispol));

  phipi2 = phipi;
  % phipi2(C.leftvert) = phipi(C.leftvert) - 1.5./npixinring(C.leftvert);
  % phipi2(C.rightvert) = phipi(C.rightvert) + 1.5./npixinring(C.rightvert);
  phipi2(C.ispol) = phipi(C.ispol) + 10./npixinring(C.ispol).*(mod(phipi(C.ispol),0.5)-0.25);
  jring2 = jring - 2;
  jring2(C.leftvert | C.rightvert) = jring(C.leftvert | C.rightvert) - 1;
  jring2(C.istip) = 0;
  phipi2(C.istip) = mod(phipi(C.istip)+1,2);
  jring2(C.tripoint & C.isequ) = NaN;
  phipi2(C.tripoint & C.isequ) = NaN;

  i_n = jp2i(jring2,phipi2,H);

  phipi2 = mod(phipi - 2./npixinring, 2);
  phipi2(C.leftvert & C.tripoint) = NaN;
  jring2 = jring;
  jring2(C.leftvert & ~C.tripoint) = jring2(C.leftvert & ~C.tripoint)+1;
  phipi2(C.leftvert & ~C.tripoint) = phipi(C.leftvert & ~C.tripoint) - 1.5./npixinring(C.leftvert & ~C.tripoint);
  % jring2(C.istip) = jring2(C.istip)+1;
  % phipi2(C.istip) = phipi(C.istip) - 1.5./npixinring(C.istip);

  i_w = jp2i(jring2,phipi2,H);

  phipi2 = mod(phipi + 2./npixinring, 2);
  phipi2(C.rightvert & C.tripoint) = NaN;
  % phipi2(C.istip) = phipi(C.istip) + 1.5./npixinring(C.istip);
  jring2 = jring;
  jring2(C.rightvert & ~C.tripoint) = jring2(C.rightvert & ~C.tripoint)+1;
  phipi2(C.rightvert & ~C.tripoint) = phipi(C.rightvert & ~C.tripoint) + 1.5./npixinring(C.rightvert & ~C.tripoint);

  i_e = jp2i(jring2,phipi2,H);

  phipi2 = phipi;
  % phipi2(C.leftvert) = phipi2(C.leftvert) + 0.5./npixinring(C.leftvert);
  % phipi2(C.rightvert) = phipi2(C.rightvert) - 0.5./npixinring(C.rightvert);
  phipi2(C.ispol) = phipi(C.ispol) - 4./(npixinring(C.ispol)+4).*(mod(phipi(C.ispol),0.5)-0.25);
  jring2 = jring + 2;

  i_s = jp2i(jring2,phipi2,H);
  i_s(i_s<0 | i_s>=H.npix) = NaN;

  return

%%%%

% helper function DEF_NUMEROLOGY:
% calculate number of pixels, etc.
function H = def_numerology(nside)

  % Numerology
  H.npix = 12 * nside^2;
  H.npolpix = 2 * nside * (nside+1);
  H.npolring = nside;
  H.nequring = 2*nside - 1;
  H.nside = nside;

  return

%%%%

% helper function DEF_CLASSES:
% divide pixels into classes with different
% geometric properties, for which neighbors
% are differently oriented.
function C = def_classes(jring,phipi,H)

  C.ispol = (jring < H.npolring);             % in north polar patch
  C.isequ = ~C.ispol;                         % in equatorial belt
  C.istip = (jring == 0);                     % in ring zero (right at pole)

  % number in each ring
  npixinring = 4 * H.nside * ones(size(C.isequ));
  npixinring(C.ispol) = 4 * (1 + jring(C.ispol));
  tmp = mod(phipi(C.ispol), 0.5);

  C.leftvert = false(size(C.ispol));          % northwest boundary is a line of longitude
  C.rightvert = false(size(C.ispol));         % northeast boundary is a line of longitude
  C.leftvert(C.ispol) = tmp < 2./npixinring(C.ispol);
  C.rightvert(C.ispol) = 0.5-tmp < 2./npixinring(C.ispol);

  tmp = mod(phipi + 0.25, 0.5) - 0.25;
  C.tripoint = (jring==H.npolring | jring==H.npolring-1);  % at a tri-point, with one less neighbor
  C.tripoint = C.tripoint & abs(tmp)<1.5./npixinring;

  return

