function [l,cl,csl]=alm2cl(alm1,alm2)
% funciton [l,cl,csl]=alm2cl(alm)
%  or
%          [l,cl,csl]=alm2cl(alm1,alm2) for cross spectrum
%
% i.e. almT=read_fits_alms('alms.fits',1);
%      almE=read_fits_alms('alms.fits',2);
%      almB=read_fits_alms('alms.fits',3);
%      [l,TT]=alm2cl(alm);
%      plot(l,TT.*l.*(l+1)/(2*pi));

if ~exist('alm2','var')
  alm2=alm1;
end

[ll,m]=index2lm(alm1.index);

l=unique(ll);
cl=NaN(size(l));
alms1=complex(alm1.alms(:,1),alm1.alms(:,2));
alms2=complex(alm2.alms(:,1),alm2.alms(:,2));

for k=1:numel(l)
  x1=alms1(ll==l(k));
  x2=alms2(ll==l(k));
  x=x1.*conj(x2);
  cl(k)=mean(x);
end

csl=cl.*l.*(l+1)/(2*pi);

end
