function make_synfastmaps(filename, n, spectra_file, WMAPband, nside, nlmax, beams, cut, dopntsrc)
error('Don''t use - use gen_map instead')
 %this driver:
 % 1.Makes a signal only synfast map & alms from a input camb spectrum (synfast)
 % 2.Makes a noise map consistent with WMAP in KECK patch. (make_noise_map)
 % 3.Find Noise alms (anafast)
 % 4.Add noise alms to signal alms (add_alms)
 % 5.Correct alms for WMAP beam, convol w/ B2/KECK Beam size (alteralms)
 % 6.Make signal + noise map & noise map from alms (w/derivs) (synfast)
 %
 %OR , if WMAPband=[]
 %
 % 1.Make signal map (w/derivs) (synfast)
 % 
 %
 %
 %INPUTS:
 %
 %  filename    :    name of output file [_signal,_noise,_signalpnoise] (MANDATORY INPUT)
 %  n           :    sim number (string)  (MANDATORY INPUT)
 %  spectra_file:    name for file containing input spectra (camb)
 %
 %  WMAPband    :    Band of WMAP to simulate noise for
 %              :    If [], then no noise or signal + noise maps will
 %                   be made.
 %              :    Default is V band
 %                   Noise levels of WMAP are derived here:
 %                   http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20120801_synfast_wmapnoise/
 %              :    Wmap beam sizes are found here:
 %                   http://lambda.gsfc.nasa.gov/product/map/current/
 %              :    WMAP nhits maps are here:
 %                   http://lambda.gsfc.nasa.gov/product/map/dr4/skymap_info.cfm
 %              :    Planck noise level is calculated here:
 %              :    http://bicep.caltech.edu/~spuder/keck_analysis_logbook/analysis/20130502_noise_planck/
 %
 %
 %  nside       :    healpix nside of output maps (ie '0512') 
 %  nlmax       :    max l to include (string)
 %  beams       :    size of beams to convol output with (string, if >1 entries, should be a cell)
 %  cut         :    coordinates (ra, dec) of cut sky 
 %
 %
 %
 %
 % e.g.: make_synfastmaps('input_maps/planck_sim/test_synfastmaps','1', [], 'planck143');  
 %       ...makes s,n,s+n maps for planck 143 noise level
 %       make_synfastmaps('test_synfastmaps','1',[],1);
 %       ...makes s map only
 %
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  %-----------------%
  %check existence
  %-----------------%
  if ~exist('spectra_file','var')
    spectra_file=[];
  end
  
  if ~exist('nside','var')
    nside=[];
  end
  
  if ~exist('nlmax','var')
    nlmax=[];
  end
  
  if ~exist('beams','var')
    beams=[];
  end
  
  if ~exist('cut','var')
    cut=[];
  end
  
  if ~exist('WMAPband','var')
    WMAPband=[];
  end
  
  if ~exist('dopntsrc','var')
    dopntsrc=[];
  end
  
  %-----------------%
  %Some defaults
  %-----------------% 
  
  if(isempty(spectra_file))
    spectra_file='aux_data/official_cl/camb_wmap5yr_noB.fits'
  end
  
  if(isempty(nside))
    nside='0512'
  end

  if(isempty(WMAPband))
    WMAPband=''
  end
  

  %which WMAP band are we making noise for?
  if(strcmp(WMAPband, 'V'))
    nlevel={[3.137,3.164],'/n/bicepfs2/keck/input_maps/wmap/wmap_band_imap_r9_7yr_V_v4.fits'}
    wmap_beam='/n/bicepfs2/keck/input_maps/wmap/bl_v_wmap_7yr.fits';
    wmap_beam_size='21';
    nside_in=nside;
    donoise=1
  elseif(strcmp(WMAPband, 'W'))
    nlevel={[6.549,6.594],'/n/bicepfs2/keck/input_maps/wmap/wmap_band_imap_r9_7yr_V_v4.fits'}
    wmap_beam='/n/bicepfs2/keck/input_maps/wmap/bl_w_wmap_7yr.fits';
    wmap_beam_size='13.2';
    nside_in=nside;
    donoise=1
  elseif(strcmp(WMAPband, 'K'))
    nlevel={[1.437,1.456],'/n/bicepfs2/keck/input_maps/wmap/wmap_band_imap_r9_7yr_V_v4.fits'}
    wmap_beam='52.8';
    wmap_beam_size='52.8';
    nside_in=nside;
    donoise=1
  elseif(strcmp(WMAPband, 'planck143'))  
    %nlevel={[7.77,7.77], '/n/bicepfs1/bicep2/pipeline/input_maps/planck_jet/HFI_SkyMap_143_0512_R1.10_nominal.fits'}
    nlevel={[.777,.777], 'input_maps/planck/planck_maps/HFI_SkyMap_143_2048_R1.10_nominal.fits'}
    %wmap_beam='input_maps/planck_jet/bl_143_planck.fits';
    wmap_beam='input_maps/planck/planck_beam/b_l_143GHz.fits';
    wmap_beam_size='7.27';
    %from here:
    %http://www.sciops.esa.int/wikiSI/planckpla/index.php?title=Effective_Beams&instance=Planck_Public_PLA
    donoise=1
    nside_in='2048';
    display('setting Nside = 2048 (mandatory for planck noise)')
  else
   display('WMAP band not defined...only making signal maps')
   donoise=0  
   nside_in=nside;
  end
  
  if(isempty(beams))
     %beams={'29.5','31','31.22','32.5'}
     beams={'31.22'}
  end

 for i=1:length(beams)
  if(str2num(wmap_beam_size)>str2num(beams{i}))
   display(['WARNING: WMAP beam (' wmap_beam  ') larger than output beam size (' beams{i} ')'])
   display('Deconv not possible with Alteralms')
   ignore = input('Continue without wmap beam convolution? Y/N [N]', 's');   
   if isempty(ignore)
    ignore = 'N';
   end
   if strcmp(ignore, 'Y')
     wmap_beam='0'
    else
     return
   end
  end
 end

  if(isempty(nlmax))
     clear nlmax;
     nlmax='1499'  %cant be higher than 1500 for WMAP_beam (or planck) beam files
  end
  
   if(isempty(cut))
     cut.theta1='-70.';
     cut.theta2='-45.';
     cut.phi1='-55.';
     cut.phi2='55.'
  end
  
  if(isempty(dopntsrc))
     dopntsrc=0;
  end

  if(isnumeric(beams))
    error('beams must be string or cell of strings')
  end
  
  if(~iscell(beams))
  beams={beams}
  end
  
  %make string of n
  nn=str2num(n)
    
%make sure temp directory exists
direc=strfind(filename, '/');
if(~isempty(direc))
  if ~exist(filename(1:max(direc)),'dir')
    system(['mkdir ' filename(1:max(direc))]);
  end
 tempdir=[filename(1:max(direc)) num2str(nn-1,'%.4d') '_temp/'];
else
   if ~exist(filename,'dir')
    system(['mkdir ' filename]);
   end
 tempdir=[filename '/' num2str(nn-1, '%.4d') '_temp/'];
end

if ~exist(tempdir,'dir')
     system(['mkdir ' tempdir]);
  end

 
  
if donoise  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 %-----------------------------------%
 %1.Make a synfast map and alms from spectrum
 %-----------------------------------%
     tmp_signal_map=[tempdir 'tmp_signal_map'];
     tmp_signal_alms=[tempdir 'tmp_signal_alms'];
     run_synfast(n, spectra_file, nside_in, tmp_signal_map,[],'0', wmap_beam, nlmax, 0, tempdir, '6')

     %make alms in a seperate step so that Q/U alms are made
     run_anafast(tmp_signal_map,tmp_signal_alms, nlmax,tempdir)
         
     
 %-----------------------------------%
 %2.Make a noise map from measured noise (wmap_noise_level.m)
 %-----------------------------------%
     tmp_noise_map=[tempdir '/tmp_noise_map'];
     make_noise_map(n, WMAPband, nlevel, tmp_signal_map, tmp_noise_map, dopntsrc)


 %-----------------------------------%
 %3.make noise alms from noise map
 %-----------------------------------%
     tmp_noise_alms=[tempdir '/tmp_noise_alms']; 
     run_anafast(tmp_noise_map, tmp_noise_alms, nlmax, tempdir)

 %-----------------------------------%
 %3b.convert noise alms from G to C
 %-----------------------------------%
     run_alteralm(tmp_noise_alms,['!' tmp_noise_alms],'0','0',tempdir,1);  
     
     
 %-----------------------------------%
 %4.Add noise alms to signal alms
 %-----------------------------------%
     tmp_signalpnoise_alms=[tempdir 'tmp_signalpnoise_alms'];
     add_alms(tmp_signal_alms, tmp_noise_alms,  tmp_signal_alms, tmp_signalpnoise_alms)
      
 
 %last two steps are different for beamsizes...loop
 for i=1:length(beams)
    beam=beams{i};
    beamname=num2str(str2num(beam), '%4.2f');
    beamname=strrep(beamname,'.','p');
    
 %-----------------------------------%
 %5.correct alms for WMAP beam, convert to B2/Keck beam
 %-----------------------------------%
     
     run_alteralm(tmp_noise_alms, [tmp_noise_alms 'corr'], wmap_beam, beam, tempdir)
     run_alteralm(tmp_signalpnoise_alms, [tmp_signalpnoise_alms 'corr'], wmap_beam, beam, tempdir)


 %-----------------------------------%
 %6.Make s, n, & s+n maps with derivs (using Stefans cut sky synfast)
 %-----------------------------------%
     %signal is different because we can just generate it from spectra (make TQU)
     run_synfast(n,spectra_file, nside, [filename '_n' nside '_s' beamname '_signal'], [],cut, beam, nlmax, 0, tempdir, '6')

     %noise and signalpnoise must be made from alms
     run_synfast(n,[tmp_noise_alms 'corr.fits'], nside, [filename '_n' nside '_s' beamname '_noise_' WMAPband 'band'], [],cut, '0', nlmax, 1, tempdir, '6')
     run_synfast(n,[tmp_signalpnoise_alms 'corr.fits'], nside,[filename '_n' nside '_s' beamname '_signalpnoise_' WMAPband 'band'], [],cut, '0', nlmax, 1, tempdir, '6')
     
  %-----------------------------------%
  
  end %beam loop
  
  %remove temp dir
  system(['rm -r ' tempdir])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
  
else %No donoise  
  

  for i=1:length(beams)
    beam=beams{i};
    beamname=num2str(str2num(beam), '%4.2f');
    beamname=strrep(beamname,'.','p');

   %-----------------------------------%
   %1.Make s maps with derivs (using Stefans cut sky synfast)
   %-----------------------------------%
       run_synfast(n,spectra_file, nside, [filename '_n' nside '_s' beamname '_signal'], [],cut, beam, nlmax, 0, tempdir, '6')
  
  end %beam loop     
       
  %remove temp dir
  system(['rm -rf ' tempdir])
  
  
end %if donoise  

     
return     
     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function run_synfast(n, spectra_file, nside, out_map, out_alms, cut, beam, nlmax, alms, tempdir, simtype)

%    INPUTS: 
%      spectra_file : string containing name of spectra file
%      nside        : nside of healpix map 
%      out_file     : string containing name of output file
%      cut          : define cut.theta1, etc to only save B2/Keck patch, 
%                     '0' to save full sky
%      beam         : size of beam to apply to map
%      alms         : 0 if spectra_file is spectrum (camb output)
%                   : 1 if spectra_file is alm file 
%

 out_map_full=[out_map '.fits'];
 if exist(out_map_full,'file')
  cmd=['rm ' out_map_full];
  system(cmd)
 end

 if(~isempty(out_alms))
 out_alms_full=[out_alms '.fits'];
  if exist(out_alms_full,'file')
   cmd=['rm ' out_alms_full];
 system(cmd);
 end
end
 
 %get seed for this sim
 seeds=get_seedlist();
 nn=str2num(n);
 seed=num2str(seeds(nn));

 
%write synfast .in file 
h=fopen([tempdir 'driver_synfast.in'],'wt');

if(~alms)
  fprintf(h, ['infile = ' spectra_file '\n']);
else
  fprintf(h, ['almsfile = ' spectra_file '\n']);
end

fprintf(h, ['simul_type = ' simtype '\n']);
fprintf(h, ['nsmax = ' nside '\n']);
fprintf(h, ['nlmax = ' nlmax '\n']);
fprintf(h, ['iseed =' seed  '\n']);
if isempty(strfind(beam,'.fits'))
  fprintf(h, ['fwhm_arcmin = ' beam '\n']);
else
  fprintf(h, ['beam_file = ' beam '\n']);
end

if(~strcmp(cut, '0'))  
fprintf(h, ['theta_cut1 = ' cut.theta1 '\n']);
fprintf(h, ['theta_cut2 = ' cut.theta2 '\n']);
fprintf(h, ['phi_cut1 = ' cut.phi1 '\n']);
fprintf(h, ['phi_cut2 = ' cut.phi2 '\n']);
end  

fprintf(h, 'apply_windows = f \n');
fprintf(h, ['outfile = ' out_map_full '\n']);

if(~isempty(out_alms))
fprintf(h, ['outfile_alms = ' out_alms_full '\n']);
end

fclose(h);
system(['synfast ' tempdir '/driver_synfast.in']);
%-----------------------------------%

return
  
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function run_anafast(map_file, out_file, nlmax, tempdir)
 
 out_file_full=[out_file '.fits'];
 if exist(out_file_full,'file')
  cmd=['rm ' out_file_full];
  system(cmd)
 end

 map_file_full=[map_file '.fits'];

h=fopen([tempdir 'driver_anafast.in'],'wt');
fprintf(h, ['simul_type= 2\n'])
fprintf(h, ['infile='  map_file_full '\n']);
fprintf(h, ['outfile_alms=' out_file_full '\n']);
fprintf(h, ['nlmax= ' nlmax '\n']);
fclose(h);
system(['anafast ' tempdir 'driver_anafast.in']);
 
 return
 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function run_alteralm(in_file, out_file, fwhm_arcmin_in, fwhm_arcmin_out, tempdir,  GtoC)

if ~exist('GtoC','var')
  GtoC=false;
end

 out_file_full=[out_file '.fits'];
 if exist(out_file_full,'file')
  cmd=['rm    ' out_file_full];
  system(cmd)
 end
 
 in_file_full=[in_file '.fits'];

  h=fopen([tempdir '/driver_alteralm.in'],'wt');
  fprintf(h,['infile_alms = ' in_file_full '\n']);
  fprintf(h,['outfile_alms = ' out_file_full '\n']);
if any(strfind(fwhm_arcmin_in,'.fits'))
  % beams files
  fprintf(h,['beam_file_in = ' fwhm_arcmin_in '\n']);
else
  fprintf(h,['fwhm_arcmin_in = ' fwhm_arcmin_in '\n']);
end
if GtoC
  fprintf(h, 'coord_in = G \n');
  fprintf(h, 'coord_out = C \n');
end
  fprintf(h,['fwhm_arcmin_out = ' fwhm_arcmin_out '\n']);
  fclose(h)
  system(['alteralm ' tempdir 'driver_alteralm.in']);
  

return

  
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function make_noise_map(n, WMAPband, nlevel, signal_map, out_file, dopntsrc)
  
if ~exist('dopntsrc','var')
  dopntsrc=false;
end

display('making noise map...')

 %get seed for this sim
 seeds=get_seedlist();
 nn=str2num(n);
 seed=seeds(nn);

%read in synfast map for size, hdr etc
filename=[signal_map, '.fits'];
s_map=fitsread(filename, 'BinTable');
info=fitsinfo(filename);
hdr=info.PrimaryData.Keywords;
xhdr=info.BinaryTable.Keywords;

%rand noise in same dimensions as synfast output
len=size(cell2mat(s_map(1)));

%make non uniform noise from N hit maps in 'C'
%hmap7=fitsread(['input_maps/wmap/wmap_band_imap_r9_7yr_' WMAPband '_v4_coordC.fits'], 'BinTable');


% if nlevel is a cell array, noise = nlevel{1}/sqrt(Nhits), where Nhits is contained in
% the fits file specified in nlevel{2} in the second, N_OBS field
if iscell(nlevel)
  disp('scaling noise by WMAP N_hits map');
  nhitsmap=read_fits_map(nlevel{2});

  sigma0=nlevel{1};
  
  % convert nested to ring
  if strcmp(nhitsmap.ordering,'NESTED')
    [th,ph]=pix2ang_nest(nhitsmap.nside,nhitsmap.pixel);
    pix=ang2pix_ring(nhitsmap.nside,th,ph);
    [dum,ind]=sort(pix);
  else
    ind=1:numel(nhitsmap.pixel);
  end
  nhits=nhitsmap.map(ind,2);
  
  nlevel=sigma0(1)./sqrt(nhits);
  nlevelQU=sigma0(2)./sqrt(nhits);
  
  nlevel=reshape(nlevel,len);
  nlevelQU=reshape(nlevelQU,len);
  
end

noise_map=nlevel;
noise_mapQU=nlevelQU;

%mK -> uK
noise_map=noise_map*1d3;
noise_mapQU=noise_mapQU*1d3; 

%gen rand number, multiply by noise_map
randn('seed', seed)
noise=randn(len).*noise_map;
randn('seed', seed+1)
noiseQ=randn(len).*noise_mapQU.*sqrt(2);  %Q and U maps indiviudally are sqrt(2) higher 
randn('seed', seed+2)
noiseU=randn(len).*noise_mapQU.*sqrt(2);



% insert WMAP point sources in galactic coordinates
if dopntsrc
  
  disp('inserting point sources...');
  
  if exist('nhitsmap','var')
    hmap=nhitsmap;
  else
    hmap=read_fits_map(filename);
  end
  
  % zero out fields
  hmap.map=zeros(size(hmap.pixel));
  hmap.type=hmap.type(1); 
  hmap.units=hmap.units(1);
  
  % insert point sources in uK units
  hmap=insert_healpix_pntsrc(hmap,'G');
  
  % massage into correct shape
  if strcmp(hmap.ordering,'NESTED')
    % convert nested to ring
    [th,ph]=pix2ang_nest(hmap.nside,hmap.pixel);
    pix=ang2pix_ring(hmap.nside,th,ph);
    [dum,ind]=sort(pix);
  else
    ind=1:numel(hmap.pixel);
  end
  pntsrc=hmap.map(ind,1);
  pntsrc=transpose(reshape(pntsrc,len));
  
  % add into noise
  noise=noise+pntsrc;
  
end


%cut hdr down to size for just one tqu map
xhdr_temp=xhdr(1:119,:);
xhdr_end=xhdr(end,:);
xhdr_cut=[xhdr_temp;xhdr_end];
%modify tfields to be for one map
xhdr_cut{8,2}=3;
xhdr_cut{4,2}=size(noise,2)*4*3;

%modify unit fields to be uK
for i=1:length(xhdr_cut)
  un=strfind(xhdr_cut{i,1},'TUNIT');
  if~isempty(un)
    xhdr_cut{i,2}='uK';
  end
end



%store as cell for fitswrite
noise_cell=cell(1,3);
noise_cell{1}=noise;
noise_cell{2}=noiseQ;
noise_cell{3}=noiseU;


%save
filename=[out_file '.fits'];
info.BinaryTable.Keywords=xhdr_cut;
fitswrite_bintable(noise_cell, info, filename)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function add_alms(signal_infile, noise_infile, signal_outfile, signalpnoise_outfile)

display('adding signal and noise alms...')

%load synfast alms
filename=[signal_infile '.fits']
s_almT=fitsread(filename,'BinTable',1);
s_almQ=fitsread(filename,'BinTable',2);
s_almU=fitsread(filename,'BinTable',3);
info=fitsinfo(filename);

%correct K->uK and save
s_almT=cell2mat(s_almT);
s_almT(:,2)=s_almT(:,2)*1000000.;
s_almT(:,3)=s_almT(:,3)*1000000.;
s_almQ=cell2mat(s_almQ);
s_almQ(:,2)=s_almQ(:,2)*1000000.;
s_almQ(:,3)=s_almQ(:,3)*1000000.;
s_almU=cell2mat(s_almU);
s_almU(:,2)=s_almU(:,2)*1000000.;
s_almU(:,3)=s_almU(:,3)*1000000.;


%load noise alms
filename=[noise_infile '.fits'];
n_almT=fitsread(filename,'BinTable',1);
n_almQ=fitsread(filename,'BinTable',2);
n_almU=fitsread(filename,'BinTable',3);
n_almT=cell2mat(n_almT);
n_almQ=cell2mat(n_almQ);
n_almU=cell2mat(n_almU);

%addd signal and noise
sn_almT=s_almT;
sn_almT(:,2)=s_almT(:,2)+n_almT(:,2);
sn_almT(:,3)=s_almT(:,3)+n_almT(:,3);
sn_almQ=s_almQ;
sn_almQ(:,2)=s_almQ(:,2)+n_almQ(:,2);
sn_almQ(:,3)=s_almQ(:,3)+n_almQ(:,3);
sn_almU=s_almU;
sn_almU(:,2)=s_almU(:,2)+n_almU(:,2);
sn_almU(:,3)=s_almU(:,3)+n_almU(:,3);



%save signal K->uK alms
s_alm(:,:,1)=s_almT;
s_alm(:,:,2)=s_almQ;
s_alm(:,:,3)=s_almU;

filename=[signal_outfile '.fits'];
fitswrite_bintable(s_alm, info, filename);

%save signal plus noise
sn_alm(:,:,1)=sn_almT;
sn_alm(:,:,2)=sn_almQ;
sn_alm(:,:,3)=sn_almU;

filename = [signalpnoise_outfile '.fits'];
fitswrite_bintable(sn_alm, info, filename);


return
