function map_name = gen_synfast_map_name(input_spec,beam,nside,rlz,lensed,noise_band, constrained)
% map_name = gen_synfast_map_name(input_spec,beam,nside,rlz,lensed,noise_band, multipole)
% creates the name and subdir of a synfast map from the parameters
% multipole is a special to create bandpower window functions - just don't hand in this variable at all when you not doing this
% 
% map_name = gen_synfast_map_name('aux_data/official_cl/camb_planck2013_r0p1.fits','B2bbns',2048,1,0,'', 0);
% map_name = gen_synfast_map_name('delta22.fits','B2bbns',2048,1,0,'',0,22);
% and see gen_map.m 

[dum,type] = fileparts(input_spec);
if isempty(strfind(input_spec,'delta'))
  if lensed
    map_name = 'map_lensed';
  else
    map_name = 'map_unlens';
  end
else
  map_name = ['map_',strrep(type,'delta','')];
  type='delta';
end

subdir = [type '/'];
map_name = [map_name sprintf('_n%04d',nside)];
map_name = [map_name sprintf('_r%04d',rlz)];
if ~iscell(beam)
  map_name = [map_name sprintf('_s%05.2f',beam)];
else
  map_name = [map_name sprintf('_s%s',beam{2})];
end

%add constrained
if ~strcmp(constrained,'')
  map_name = [map_name '_c' constrained];
end  

%take care of noise
if ~strcmp(noise_band,'')
  map_name = [map_name sprintf('_d%s',noise_band)];
else
  map_name = [map_name '_dNoNoi'];
end

%replace points with p 
subdir  = strrep(subdir,'.','p');
map_name = strrep(map_name,'.','p');

%append the file extension
map_name = [map_name '.fits'];
map_name = ['input_maps/' subdir map_name];

return
