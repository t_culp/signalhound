function map=read_fits_map(filename,ext)
% map=read_fits_map(filename,ext)
%
% Read in a healpix map from fits file
%
% Analog of IDL read_fits_map function distributed with Healpix
% package
%
% ext specifies which fits extension to read (default=1)
%
% map=read_fits_map('/data/quad/healmaps/wmap3/wmap_band_iqumap_r9_3yr_W_v2.fits')
% map=read_fits_map('/data/quad/healmaps/b03/b03.x33n.w1x2y1z2.na36.hp10.lp4.all.fits')
% map=read_fits_map('/data/mlb/healpix_maps/cmb_map1.fits')

if ~exist('ext') || isempty(ext)
  ext=1;
end

% get the header information
map=get_hmap_properties(filename);

% 2012/08/24 (STF) I have check on using fitsread2 with the 'BinTableFast' option to replace the next
% call, but it is way slower. I checked with a profiler where time is used up in fitsread and it turns
% out that fread itsself is using most of the time, which we probably cannot speed up.
data=fitsread(filename,'BinTable',ext);

% for each map
for i=1:length(data)  
  % massage to column vector in pixel number order
  m=data{i}'; map.map(:,i)=m(:);  
end

% Some healpix maps are partial sky coverage
% For B2K maps (at least) this is achieved by putting the pixel index
% into the first column and labelling it's units as 'PIXEL'
if(strcmp(map.type{1},'PIXEL'))
  % pull out the pixel index column
  map.pixel=map.map(:,1);
  map.map=map.map(:,2:end);
  map.units=map.units(2:end);
  map.type=map.type(2:end);
else
  map.pixel=(0:size(map.map,1)-1)';
end

% for each column with non uK units convert to uK
for i=1:length(map.units)
  if i==1
      tmax=max(map.map(:,1));
  end
  if(strcmp(map.units{i},'mK')|strcmp(map.units{i},'mK,thermodynamic'))
    map.units{i}=strrep(map.units{i},'mK,thermodynamic','uK');
    map.map(:,i)=map.map(:,i)*1e3;
  end
  if(strcmp(map.units{i},'K')) 
    map.units{i}=strrep(map.units{i},'K','uK');
    map.map(:,i)=map.map(:,i)*1e6;
  end
  if(strcmp(map.units{i},'Kelvin')) 
    map.units{i}=strrep(map.units{i},'Kelvin','uK');
    map.map(:,i)=map.map(:,i)*1e6;
  end 
  if(strmatch(map.units{i},'muK'))
    map.units{i}=strrep(map.units{i},'muK','uK');
  end
  if(strmatch(map.units{i},'microK'))
    map.units{i}=strrep(map.units{i},'microK','uK');
  end
  if(strcmp(map.units{i},'unknown')) && (tmax < 1E-3)
    warning('Unknown unit of the healpix map - assume it is in K')
    map.units{i}=strrep(map.units{i},'unknown','uK');
    map.map(:,i)=map.map(:,i)*1e6;
  end 
  if(strcmp(map.units{i},'unknown')) && (tmax > 1E-3)
    warning('Unknown unit of the healpix map - assume it is in uK')
    map.units{i}=strrep(map.units{i},'unknown','uK');
  end 
  if(strcmp(map.units{i},'K_CMB')) 
    map.units{i}=strrep(map.units{i},'K_CMB','uK');
    map.map(:,i)=map.map(:,i)*1e6;
  end 
  if(strcmp(map.units{i},'KCMB')) 
    map.units{i}=strrep(map.units{i},'KCMB','uK');
    map.map(:,i)=map.map(:,i)*1e6;
  end 
  if(strcmp(map.units{i},'K_CMB^2')) 
    map.units{i}=strrep(map.units{i},'K_CMB^2','uK^2');
    map.map(:,i)=map.map(:,i)*1e12;
  end 
  if(strcmp(map.units{i},'(K_CMB)^2'))
    map.units{i}=strrep(map.units{i},'(K_CMB)^2','uK^2');
    map.map(:,i)=map.map(:,i)*1e12;
  end
end

return
