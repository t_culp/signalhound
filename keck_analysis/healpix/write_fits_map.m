function write_fits_map(fname,hmap)
% write_fits_map(filename,hmap)
%
% Write a MATLAB healpix structure contained in hmap to a fits file.
%
% Works with full or cut sky healpix maps. However, because the pixel field is a
% different data type than the map fields, the poorly written fitswrite_bintable loops
% over every value in the map to write the fits file, and this can slow things way
% down. Stefan fixed fitswrite_bintable to avoid the loop for full sky maps, but not
% for cut sky maps.
%
% i.e. (Add noise to the first three fields of a healipx map and write only those fields)
%      hmap=read_fits_map('map.fits');
%      hmap.map=randn(size(hmap.map(:,1:3)))+hmap.map(:,1:3);
%      hmap.type=hmap.type(1:3);
%      hmap.units=hmap.units(1:3);
%      write_fits_map('map2.fits',hmap);

% Fill out info field
info.PrimaryData.Keywords={
    'SIMPLE'     'T'                               ''
    'BITPIX'     [                 32]             ''
    'NAXIS'      [                  0]             ''
    'EXTEND'     'T'                               ''
    'END'                           ''             ''
};

% Is this a cut sky healpix map? If so, we need an extra field for the pixel values.
iscut=numel(hmap.pixel)~=12*hmap.nside^2;

% Generate header keywords
xhdr=get_xhdr(hmap,iscut);
info.BinaryTable.Keywords=xhdr;

% Assemble the data structure
dat=cell(1,size(hmap.map,2)+iscut);

for k=1:size(hmap.map,2)+iscut
  if k==1 & iscut
    dat{k}=double(hmap.pixel);
  else
    dat{k}=hmap.map(:,k-iscut);
  end
end

% Write fits map
fitswrite_bintable(dat,info,fname);

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xhdr=get_xhdr(hmap,iscut)

nside=hmap.nside;
nfields=size(hmap.map,2)+iscut;
unit='test';
npix=numel(hmap.pixel);

xhdr={
    'XTENSION'    'BINTABLE'          ' binary table extension                         '                        
    'BITPIX'      [             8]    ' 8-bit bytes                                    '                        
    'NAXIS'       [             2]    ' 2-dimensional binary table                     '                        
    'NAXIS1'      [     4*nfields]    ' width of table in bytes                        '                        
    'NAXIS2'      [          npix]    ' number of rows in table                        '                        
    'PCOUNT'      [             0]    ' size of special data area                      '                        
    'GCOUNT'      [             1]    ' one data group (required keyword)              '                        
    'TFIELDS'     [       nfields]    ' number of fields in each row                   '                        
    'TEMPTYPE'    'THERMO'            ' temperature type either THERMO or ANTENNA      '                        
    'PIXTYPE'     'HEALPIX'           ' HEALPIX Pixelisation                           '                        
    'ORDERING'    hmap.ordering       ' Pixel ordering scheme, either RING or NESTED   '                        
    'NSIDE'       [         nside]    ' Resolution parameter for HEALPIX               '                        
    'POLCCONV'    hmap.polcconv       ''
    'FWHM'        [     hmap.fwhm]    ''
    'COORDSYS'    hmap.coordsys       ' Coord system                                   '
     };

for k=1:nfields
  
  if k==1 & iscut
    unit='numbers';
    form='1J';
    type='PIXEL';
  else
    unit=hmap.units{k-iscut};
    form='1E';
    type=hmap.type{k-iscut};
  end
  
  ks=num2str(k);
  
  xhdr_add={
      ['TTYPE' ks]     type                ' map                                            '                        
      ['TFORM' ks]     form                ' data format of field: 4-byte REAL              '                        
      ['TUNIT' ks]     unit                ' map unit                                       '                        
      'COMMENT'        ''                  ''
      };

  xhdr=[xhdr;xhdr_add];
  
end

xhdr_add = {'END'         ''                  ''};
xhdr=[xhdr;xhdr_add];


return
