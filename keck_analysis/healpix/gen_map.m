function map_name = gen_map(rlz,nside,nlmax,input_spec,lensed,beam,noise_band,debug,constrained,dry_run,do_cut,seed)
% map_name = gen_map(0,512,1499,'aux_data/official_cl/camb_planck2013_r0p1.fits',1,28,'WmapV')
% 
% rlz       : Realiztation counted from 0
%  
% nside     : NSide healpix map, i.e. 512   
%  
% nlmax     : lmax of alms, standard 1499 (use 1536 for constrained)
%  
% input_spec: i.e. 'aux_data/official_cl/camb_planck2013_r0p1.fits'
%  
% lensed    : 0 or 1 - lense the map or not
%  
% beam      : beam to smooth the final map with
%             either fwhm in arcmin, i.e. 31.22 (numeric) or 
%             string identifier of a beam file. Known ids in get_bl_filename()
%
% noise_band: Band of WMAP/Planck to simulate noise for
%             if  not exist or '' no noise will be added (default)
%             valid choices are: '', 'WmapV', 'WmapW', 'WmapK', 'Pl143', 'Pl100'
%  
%             Noise levels of WMAP are derived here:
%             http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20120801_synfast_wmapnoise/
%             Wmap beam sizes are found here:
%             http://lambda.gsfc.nasa.gov/product/map/current/
%             WMAP nhits maps are here:
%             http://lambda.gsfc.nasa.gov/product/map/dr4/skymap_info.cfm
%             Planck noise level is calculated here:
%             http://bicep.caltech.edu/~spuder/keck_analysis_logbook/analysis/20130502_noise_planck/
%  
% debug     : 1 or 0 (default), if 1 all temporary data products and steering
%             files to synfast etc. will not be deleted
%  
% constrained: '' (default), if 'Pl100' or 'Pl143' will make constrained Q/U
%              realizations using Planck's COM_CompMap_CMB-nilc_2048_R1.20 map.
%              as final step the T sky is replaced with Pl100 or Pl142 as specified
%
% do_cut     : 1 (default), cut the sky or not
%
% dry_run    : 0 (default), if 1: just return the map_name
%  
% seed       : allow to hand in a seed explicitly - overwrites the seed = seedlist(rlz+1) selection
%              only applies to the synfast generated random fields, but not the lensing
%
% examples  :
%  gen_map(0,512,1499,'aux_data/official_cl/camb_planck2013_r0p1.fits',0,28) % unlensed
%  gen_map(0,512,1499,'aux_data/official_cl/camb_planck2013_r0p1.fits',1,28,'WmapV') % lensed w/ noise
%  gen_map(0,512,1499,'aux_data/official_cl/camb_planck2013_r0p1.fits',1,28,'',1) % lensed, w/o noise, debug mode
%  gen_map(0,512,1499,'aux_data/official_cl/camb_planck2013_r0.fits',0,'B2bbns','Pl143') % unlensed, Beamprofile, Planck noise
%  gen_map(0,512,1499,'aux_data/official_cl/camb_planck2013_r0p1.fits',0,31,'',1) % check
%  gen_map(0,512,1499,'aux_data/official_cl/camb_planck2013_r0p1.fits',0,31,'WmapV',1) % X
%  gen_map(0,512,1499,'aux_data/official_cl/camb_planck2013_r0p1.fits',0,'B2bbns','WmapV',1) %
%  gen_map(0,512,1499,'aux_data/official_cl/camb_planck2013_r0p1.fits',0,'B2bbns','',1) %
%  gen_map(0,512,1499,'aux_data/official_cl/camb_planck2013_r0p1.fits',0,0,'',1)
%  gen_map(0,512,1499,'aux_data/official_cl/camb_planck2013_r0p1.fits',0,31,'',1)
%  gen_map(0,512,1536,'aux_data/official_cl/camb_planck2013_r0.fits',0,31,'',1,1)

if isstr(beam)
  % Pass along both the FITS file path and name to synfast routines.
  beam = {get_bl_filename(beam), beam};
end

%  defaults:
if ~exist('noise_band','var')
  noise_band = '';
end

%  defaults:
if ~exist('constrained','var') || any(constrained==0)
  constrained = '';
end

%  defaults:
if exist('debug','var') & debug
  global synfast_debug;
elseif ~isempty(whos('global','synfast_debug'))
  clearvars -global synfast_debug
end

if ~exist('dry_run','var')
  dry_run = 0;
end

if ~exist('do_cut','var')
  do_cut = 1;
end

if ~exist('seed','var')
  seed = [];
end

% avoid reprocessing if final file is already there.
map_name = gen_synfast_map_name(input_spec,beam,nside,rlz,lensed,noise_band, constrained);
if dry_run
  return
end
if exist(map_name,'file') 
  display(['File ',map_name,' exists, skipping'])
  return
end
display(['Working on ',map_name])

% fetch the signal alm. If it does not exist, it will be generated
alms_name = gen_signal_alms(rlz,nlmax,input_spec,lensed, constrained, 1, seed);

if ~strcmp(noise_band,'')

  % fetch the noise alm. If it does not exist, it will be generated
  alm_noise_name = gen_noise_alms(rlz,nlmax,noise_band);
  
  % add noise and signal alm
  alm_sn_name = ['synfast_tmp/alm_sn_' gen_stamp() '.fits'];
  add_alms(alms_name, alm_noise_name, alm_sn_name);
  alms_name = alm_sn_name;
  
end

% run synfast on the alm handed in from above, apply the beam and skycut:
if do_cut
  cut.theta1=-70; cut.theta2=-45;cut.phi1=-55; cut.phi2=55;
  %cut.theta1=-80; cut.theta2=-20;cut.phi1=-75; cut.phi2=75;
else
  cut = [];
end
display(['Generating final healpix map: ',map_name])
run_synfast(rlz, input_spec, alms_name, nside, map_name, [], cut, beam, nlmax);

% in debug mode also generate the noise only map with beam applied:
if ~isempty(whos('global','synfast_debug')) & exist('alm_noise_name','var')
  display('Generating noise only alms')
  run_synfast(rlz, input_spec, alm_noise_name, nside, ['synfast_tmp/map_noise' gen_stamp() '.fits'], [], [], beam, nlmax);
end

% no debug mode: remove tmp files
if isempty(whos('global','synfast_debug'))
  if exist('alm_sn_name','var')
    system(['rm ' alm_sn_name]);
  end
end

return


