function lmap=healpix_to_longlat(hmap,long,lat,coord,dopolrot,finterp)
% lmap=healpix_to_longlat(hmap,long,lat,coord,dopolrot)
%
% Take a healpix map and return "square grid" long/lat map
% NB: the result will always be in IAU convention
%
% e.g.:
%       long=73:0.02:93; lat=-52:0.02:-43;
%       map=healpix_to_longlat('/data/quad/healmaps/b03/b03.x33n.w1x2y1z2.na36.hp10.lp4.all.fits',long,lat,0);
%       map=healpix_to_longlat('/data/mlb/healpix_maps/cmb_map1.fits',long,lat,0);
%       imagesc(long,lat,map(:,:,1)); axis xy; set(gca,'XDir','reverse');
%
%       res=0.5; long=-180:res:180; lat=-90:res:90;
%       map=healpix_to_longlat('/data/quad/healmaps/wmap3/wmap_band_imap_r9_3yr_W_v2.fits',long,lat,1);
%       imagesc(long,lat,map(:,:,1)); axis xy; set(gca,'XDir','reverse');
% coord: specify coordinate system of healpix map. Valid options are 'C'
%        and 'G' for celestial and galactic, respectively.  Default is 'C'.
% dopolrot: 1 (default) rotate Q and U if converting galactic to celestial and Q and U
%           are present
%           0 don't do that

% Condition input arguments:
if ~exist('coord','var')
  coord='C';
end

if ~exist('finterp','var')
  finterp=0';
end

if ~exist('dopolrot','var') || isempty(dopolrot)
  dopolrot=true;
end

if ~strcmpi(coord,'C') && ~strcmpi(coord,'G')
  error('Invalid coordinate system specification. Choose ''C'' or ''G''')
end

% if healmap is a string read it in from file
% if not it should be pre-read map structure as returned by
% read_fits_maps
if(ischar(hmap))
  hmap=read_fits_map(hmap);
  % overwrite coordsys from fits header if available:
  if ~strcmp(hmap.coordsys,'UNKNOWN')
    disp(['Use coordinate system from map: ',hmap.coordsys])
    coord = hmap.coordsys;
  end
end

llong = long; llat=lat;
% if long/lat are 1D assume shorthand for meshgrid output
if(any(size(long)==1))
  [long,lat]=meshgrid(long,lat);
end

% Rotate interpolation coords if healpix map is galactic
if strcmpi(coord,'G')
  % Transform interpolation coords from cel to gal:
  disp('Input map assumed to be in Galactic coords')
  [long,lat]=euler(long,lat,1,0);
end

% deg to rad
long=long*pi/180;
lat=lat*pi/180;

% convert from -pi<dec<pi SP->NP to healpix 0<theta<2pi NP->SP
lat=-lat+pi/2;

if finterp
  disp('using cubic interpolation')
  % for the interpolation fetch a wider patch than the requested sky:
  ddlong = abs(llong(end) - llong(1));
  ddlat  = abs(llat(end) - llat(1));
  dlong  = llong(2)-llong(1);
  dlat   = llat(2)-llat(1);
  llong  = llong(1)-ddlong*0.05:dlong:llong(end)+ddlong*0.05;
  llat   = llat(1)-ddlat*0.05:dlat:llat(end)+ddlat*0.05;
  [llong,llat]=meshgrid(llong,llat);
  
  % Rotate interpolation coords if healpix map is galactic
  if strcmpi(coord,'G')
    % Transform interpolation coords from cel to gal:
    disp('Input map assumed to be in Galactic coords')
    [llong,llat]=euler(llong,llat,1,0);
  end
  
  llong=llong*pi/180;
  llat=llat*pi/180;
  
  llat=-llat+pi/2;
  
  switch hmap.ordering
    case 'RING'
      hmapind=ang2pix_ring(hmap.nside,llat,llong);
    case 'NESTED'
      hmapind=ang2pix_nest(hmap.nside,llat,llong);
  end 
  hmapind = unique(hmapind);
  switch hmap.ordering
    case 'RING'
      [plat,plong]=pix2ang_ring(hmap.nside,hmapind);
    case 'NESTED'
      [plat,plong]=pix2ang_nest(hmap.nside,hmapind);
  end
  tri=delaunay(plong,plat);
  % lookup the closest triangles using previously derived tri list
  t=t_search(plong,plat,tri,long,lat);
  % interpolate T/Q/U at mean a/b pointing
  % T=cubicmx(plong,plat,hmap.map(hmapind,1),long,lat,tri,t);
  % Q=cubicmx(plong,plat,hmap.map(hmapind,2),long,lat,tri,t);
  for col=1:size(hmap.map,2)
    lmap(:,:,col) = cubicmx(plong,plat,hmap.map(hmapind,col),long,lat,tri,t);
  end
  
else
  % find the hmap pixel indecies which correspond to required lmap pixels
  switch hmap.ordering
    case 'RING'
      hmapind=ang2pix_ring(hmap.nside,lat,long);
    case 'NESTED'
      hmapind=ang2pix_nest(hmap.nside,lat,long);
  end

  % To map partial sky healpix map to longlat map is ticky problem in
  % index de-referencing.
  % There will normally be many lmap pixels mapping to individual hmap
  % pixels (lmap higher res) and some of the desired lmap pixels may not
  % be available in partial sky hmap (and should come out as NaN).

  % find the unique pixel indecies and the mapping which puts them back
  % into the longlat map
  [uind,dum,lmapind]=unique(hmapind(:));
  % lmapind is length of lmap(:), and reconstructs lmap from a vector in
  % uind order

  % Both uind and map.pixel will (should!) be unique.
  % Their intersection will be the length of uind, or shorter in the
  % event that some of the desired hmap pixels are not available.
  % Find the mappings which allow us to move pixel values from hmap to
  % uind vector order.
  [dum,indintomappixel,indintouind]=intersect(hmap.pixel,uind);

  % for each column of healpix map
  for col=1:size(hmap.map,2)

    % create output map
    m(:,:)=zeros(size(hmapind));
    
    % setup pixval array as all NaN
    pixval=NaN*ones(size(uind));
    
    % copy available pixel values from healpix map to uind vector
    % order
    pixval(indintouind)=hmap.map(indintomappixel,col);
    
    % expand out available pixel values to fill lmap
    m=pixval(lmapind);

    % reshape to 2d map
    m=reshape(m,size(hmapind));
    
    % insert into layer of final output map
    lmap(:,:,col)=m;
  end
end

% If healpix map is in galactic, rotate Q/U to celestial coords
% note that to get the sense of the rotation right we must know the
% polarization convention of the healpix map
if strcmp(coord,'G') & any(strfind(rvec(char(hmap.type)),'Q')) & dopolrot
  
  %go back to degrees
  lat=-lat+pi/2;
  gb=lat*180/pi;
  gl=long*180/pi;

  if strcmpi(hmap.polcconv,'IAU')
    iau=true;
  else
    iau=false;
  end
  
  % rotate pol vectors
  disp('rotating pol vectors');
  [ra, dec, lmap(:,:,2), lmap(:,:,3)]=euler_qu(gl, gb, lmap(:,:,2), lmap(:,:,3),2,iau);
  
end

% Try to detect polarization map in need of conversion from Healpix
% to IAU polarization convention 
% - note that this must be done after the rotation above
if(any(strcmp(hmap.polcconv,{'COSMO','UNKNOWN'})) & any(strfind(rvec(char(hmap.type)),'Q')))
  if size(hmap.map,2)>=3
    lmap(:,:,3)=-lmap(:,:,3);
    disp('converting from Healpix polconv to IAU');
  end
end  

return
