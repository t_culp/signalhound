function concatenate_fits_alms(fn1,fn2,fnout,lcut)
% concatenate_fits_alms(alm1,alm2,almout,lcut)
%
% Read in two a_lm fitls files and fill in the low ell a_lms of alm2 with the a_lms of
% alm1. 
%
% alm1, alm2 = filenames of alm fits files to concatenate. 
% almout = output filename
% lcut = multipole below which alm2's a_lms are filled in with alm1's (default max ell of
%        alm1)

% Get header
h = fitsinfo(fn1);
nfields = numel(h.BinaryTable);

for k=1:nfields
  
  alm1 = read_fits_alms(fn1,k,0);
  alm2 = read_fits_alms(fn2,k,0);

  if k==1
    % Create new alm structure
    almout = alm2;
    almout = rmfield(almout,'alms');
    
    % Get indices
    [l1,m1]=index2lm(alm1.index);
    [l2,m2]=index2lm(alm2.index);
        
    % Get lcut
    if ~exist('lcut','var') || isempty(lcut)
      lcut = max(l1);
    end 
  
  end
  
  % Add in alm2
  almout.alms(:,:,k)=[alm2.alms(:,1),alm2.alms(:,2)];
  
  % Replace with alm1
  ind1 = find(l1 <= lcut);
  ind2 = find(l2 <= lcut);
  
  almout.alms(ind2,:,k) = alm1.alms(ind1,:);
    
end

write_fits_alms(fnout,almout);

return

