function subs_Talms(alms_name_in, alms_name_out, cmb_Tmap)
% subs_Talms(rlz,alms_name,nlmax,cmb_Tmap)
%
% We would like to always use Planck 143GhZ T sky as our T sky
% This function allows you to substitute the Talms back into our alms
% file after you've created E & B alms (and lensed the E & B alms).
% 
%  alms_name_in     : string containing name of input alms file
%  alms_name_out    : string containing name of output alms file
%  cmb_Tmap         : Full sky map of T to use as constraint
%  
% example:
% subs_Talms('test.fits')

display(['Force Talms to be ',cmb_Tmap])

%read the input alms T field
alms=read_fits_alms(alms_name_in)  

%get info
m=length(alms.index);
info=get_info(alms.nside, alms.nlmax, m);

%get the nlmax of the input
if isnumeric(alms.nlmax)
  nlmax = num2str(alms.nlmax);
else
  nlmax=alms.nlmax
end
  
%default T map is planck 143 Ghz
if ~exist('cmb_Tmap', 'var') | isempty(cmb_Tmap) | strcmp(cmb_Tmap,'Pl143')
  cmb_Tmap='input_maps/planck/planck_maps/HFI_SkyMap_143_2048_R1.10_nominal.fits';
  planck_beam='input_maps/planck/planck_beam/b_l_143GHz.fits';
  planck_nside='2048'; 
elseif strcmp(cmb_Tmap,'Pl100')
  cmb_Tmap='input_maps/planck/planck_maps/HFI_SkyMap_100_2048_R1.10_nominal.fits';
  planck_beam='input_maps/planck/planck_beam/b_l_100GHz.fits';
  planck_nside='2048';
elseif strcmp(cmb_Tmap,'Pl217')
  cmb_Tmap='input_maps/planck/planck_maps/HFI_SkyMap_217_2048_R1.10_nominal.fits';
  planck_beam='input_maps/planck/planck_beam/b_l_217GHz.fits';
  planck_nside='2048';  
else
  error(['Unknown cmb_Tmap: ',cmb_Tmap])
end

%Make alms from cmb_Tmap
%-----------------------------------%
%make alms in a seperate step so that Q/U alms are made
stamp = gen_stamp();
tmp_almsfile1=['synfast_tmp/' stamp  '1_alms.fits'];
tmp_almsfile2=['synfast_tmp/' stamp  '2_alms.fits'];
run_anafast(cmb_Tmap,tmp_almsfile1, nlmax, [], []);

%need to use alteralms to correct for planck beam & rotate to cel
run_alteralm(tmp_almsfile1,tmp_almsfile2,planck_beam,'0',1,planck_nside);

%Now subs the T alms created above into the input alms
%-----------------------------------%

%WARNING units must be in K for synfast to generate map from the alms
%synfast ignores the units in the header?
%so using read_fits_alms really screws with you...

%load alms created above
almT=fitsread(tmp_almsfile2,'bintable',1);
almE=fitsread(alms_name_in,'bintable',2);
almB=fitsread(alms_name_in,'bintable',3);

alm(:,:,1)=[almT{1},almT{2},almT{3}];
alm(:,:,2)=[almE{1},almE{2},almE{3}];
alm(:,:,3)=[almB{1},almB{2},almB{3}];
 
fitswrite_bintable(alm,info,alms_name_out);    

if isempty(whos('global','synfast_debug'))
  system(['rm -f ' tmp_almsfile1]);
  system(['rm -f ' tmp_almsfile2]);
  system(['rm -f ' alms_name_in]);
end

  return
  
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function info = get_info(nside, nlmax, nalms)
%  function xhdr = get_xhdr(planck_nside)
%  returns a minimum header for a fits file with alms

% Fill out info field
info.PrimaryData.Keywords={
    'SIMPLE'     'T'                               ''
    'BITPIX'     [                 32]             ''
    'NAXIS'      [                  0]             ''
    'EXTEND'     'T'                               ''
    'END'                           ''             ''
};

%make binary header
nfields=3;

xhdr={
    'XTENSION'    'BINTABLE'          ' binary table extension                         '                        
    'BITPIX'      [             8]    ' 8-bit bytes                                    '                        
    'NAXIS'       [             2]    ' 2-dimensional binary table                     '                        
    'NAXIS1'      [     4*nfields]    ' width of table in bytes                        '                        
    'NAXIS2'      [         nalms]    ' number of rows in table                        '                        
    'PCOUNT'      [             0]    ' size of special data area                      '                        
    'GCOUNT'      [             1]    ' one data group (required keyword)              '                        
    'TFIELDS'     [       nfields]    ' number of fields in each row                   '                        
    'NSIDE'       [         nside]    ' Resolution parameter for HEALPIX               '                        
    'FWHM'        [             0]    ''
    'COORDSYS'                 'C'    ' Coord system                                   '
    'MAX-LPOL'    [         nlmax]    ' lmax of alms                                   '  
    };

for k=1:nfields
   if k==1 
    unit='numbers';
    form='1J';
    type='INDEX';
  end
  if k==2
    unit='K';
    form='1E';
    type='REAl';
  end
  if k==3
    unit='K';
    form='1E';
    type='IMAG';
  end

    
  ks=num2str(k);
  
  xhdr_add={
      ['TTYPE' ks]     type                ' map                                            '                        
      ['TFORM' ks]     form                ' data format of field: 4-byte REAL              '                        
      ['TUNIT' ks]     unit                ' map unit                                       '                        
      'COMMENT'        ''                  ''
      };

  xhdr=[xhdr;xhdr_add];
  
end

xhdr_add = {'END'         ''                  ''};
xhdr=[xhdr;xhdr_add];

%put in info
info.BinaryTable(1).Keywords=xhdr;
info.BinaryTable(2).Keywords=xhdr;
info.BinaryTable(3).Keywords=xhdr;

%Contents
info.Contents={'Primary', 'Binary Table', 'Binary Table', 'Binary Table'};

return