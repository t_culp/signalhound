function h=orthview(map,col,c)
% h=orthview(map,col,c)
%
% make orthographic projections from Healpix map

if(~exist('col'))
  col=1;
end

% nside=1 corresponds to 90 deg spacing in the az direction

% for now fixed spacing
gs=0.5; %deg

% generate matrix grid
lon=-180+gs/2:gs:180-gs/2;
lat=-90+gs/2:gs:90-gs/2;

m=healpix_to_longlat(map,lon,lat);

%[lat, lon] = meshgrat(lat, lon, size(m(:,:,1)));

%setwinsize(gcf,1000,500);
clf
subplot(1,2,1)
axesm('MapProjection','globe','Grid','on','GLineStyle','-','GLineWidth',0.1,'MLineLocation',45,'PLineLocation',[0,20,40,60]);
h(1)=pcolorm([-90,90],[-180,180],m(:,:,col));
if(exist('c','var'))
  caxis(c);
end
view(270,90)
axis off
set(gca,'XDir','reverse');
subplot(1,2,2)
axesm('MapProjection','globe','Grid','on','GLineStyle','-','GLineWidth',0.1,'MLineLocation',45,'PLineLocation',[0,-20,-40,-60]);
h(2)=pcolorm([-90,90],[-180,180],m(:,:,col));
if(exist('c','var'))
  caxis(c);
end
view(90,-90)
axis off
set(gca,'XDir','reverse');

return
