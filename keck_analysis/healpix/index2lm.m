function [l,m]=index2lm(index)
% [l,m]=index2lm(index)
%
% Analog of IDL healpix routine index2lm

l=floor(sqrt(index-1));
m = floor(index - l.^2 - l - 1);

return
