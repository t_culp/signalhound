function [alms_name,phi_alm_in,phi_nside,phi_lmax] = gen_signal_alms(rlz,nlmax,input_spec,lensed,constrained,update,seed,lensrlz)
% alms_name = gen_signal_alms(rlz,nlmax,input_spec,lensed,constrained,update,seed,lensrlz)
%  
%  create signal alms given an input spectrum
%  does not rerun, when file already exists
%  alms_name = the filename theses are saved to
%  
%  rlz       : Realiztation counted from 0
%    
%  nlmax     : lmax of alms, use 1499 (1536 for constrained)
%  
%  input_spec: i.e. 'aux_data/official_cl/camb_planck2013_r0p1.fits'
%  
%  lensed    : 0 or 1 - apply lensing or not
%
%  constrained : generate constrained E alms based on T alms
%              : 0,'' - unconstrained
%              : 1 (='Pl143'),'Pl100' will make constrained Q/U
%                realizations using Planck's COM_CompMap_CMB-nilc_2048_R1.20 map.
%                'Pl217' will make constrained Q/U realizations using
%                input_maps/planck/planck_maps/HFI_SkyMap_217_2048_R1.10_nominal.fits
%                as final step the T sky is replaced with Pl100,
%                Pl143, or Pl217 as specified
%  
%  update   : 0 default, if 1 don't remake the alms if they exist
%  
%  seed     : [], usually the seed is generated from the rlz number, this allows to hand in a
%             seed explicitly.
%
%  lensrlz : realization of lensing potential; same as rlz by default, but can be
%            different, e.g. when generating higher ell a_lms with same
%            lensing potential as existing low ell sims. If the lensing realization is
%            not the same as the CMB realization, generate unlensed a_lms using the
%            observed NILC TT spectrum for the constraint to avoid biasing EE high by
%            the NILC noise a_lms. This makes the low ell a_lms slightly different, so
%            substitute the old unlensed a_lms at l<500, then pass these munged
%            together a_lms through lenspix.
%
%  example:
%  gen_signal_alms(0,1499,'aux_data/official_cl/camb_planck2013_r0p1.fits',1)

%  defaults:
if(~exist('update','var'))
  update=0;
end

if isnumeric(constrained)
  if constrained==0
    constrained='';
  else
    constrained='Pl143';
  end
end

if ~exist('seed','var')
  seed = [];
end

if ~exist('lensrlz','var') || isempty(lensrlz)
  lensrlz=rlz;
end

% If the lensing realization is not the CMB realization, we are extending a_lms to
% higher ell (e.g. for SPT delensing)
mungealms = lensrlz~=rlz;

% get the name of the signal alms, 
% if it exists, we are done and just return the name:
alms_name = get_alms_name(rlz,nlmax,input_spec,lensed,'', constrained);

% check if to make/remake the file
if exist(alms_name,'file')
  if update
    display(['File ',alms_name,' exists, skipping'])
    return
  else
    display(['File ',alms_name,' exists, remaking it'])
  end
end

% perpare the directiory to save to:
[dir,dum] = fileparts(alms_name);
mkdir(dir)

if lensed
  display('Generating lensed signal alms')
  % to do lensing the unlensed alms are needed first,
  % hence call recursively without the lensing option:
  [alm_name_unlensed,phi_alm_in,phi_nside,phi_lmax] = gen_signal_alms(rlz,nlmax,input_spec,0, constrained,update,seed,lensrlz);
  
  % fetch few filenames:
  lens_pot_in = strrep(input_spec,'.fits','_lenspotentialcls.dat');
  lens_pot_map_out = get_lens_pot_name(lensrlz,nlmax,input_spec);
  
  % apply lensing:
  if strcmp(constrained,'')
    lens_alms(lensrlz,alm_name_unlensed,lens_pot_in,nlmax,alms_name,lens_pot_map_out,phi_);
  else
    %make a temp file so we don't get overlapping reads and writes!
    %this can be a problem when farming many jobs of more than one NSIDE
    alms_name_temp=strrep(alms_name,'.fits',['_',gen_stamp(),'.fits']);
    lens_alms(lensrlz,alm_name_unlensed,lens_pot_in,nlmax,alms_name_temp,lens_pot_map_out,phi_nside,phi_alm_in,phi_lmax);
    
    %force Talms to be planck 143
    display(['Force Talms to be ',constrained])
    subs_Talms(alms_name_temp, alms_name, constrained)
   end
else
  % create unlensed alms
  display('Generating unlensed signal alms')
  % we need a bogus out map since synfast does not run without
  mkdir('synfast_tmp')
  out_map = ['synfast_tmp/map_' gen_stamp() '.fits'];

  if strcmp(constrained,'')
    % run synfast:
    run_synfast(rlz, input_spec, [], 4, out_map, alms_name, [], 0, nlmax,[],seed);
    % no debug mode: remove tmp files
    if isempty(whos('global','synfast_debug'))
      system(['rm -f ' out_map]);
    end
  else
    %make a temp file so we don't get overlapping reads and writes!
    %this can be a problem when farming many jobs of more than one NSIDE
    alms_name_temp=strrep(alms_name,'.fits',['_',gen_stamp(),'.fits']);
    
    %make constrained alms (T constraint is planck NILC)
    gen_constrained_alms(rlz, input_spec,alms_name_temp, nlmax, [], mungealms)

    %force Talms to be planck 143
    subs_Talms(alms_name_temp, alms_name,constrained)  
    
    if mungealms
      % Hard coded because our existing simset is fixed
      alms_name_simset = strrep(alms_name,'extended_','');
      alms_name_simset = strrep(alms_name_simset,sprintf('l%04d',nlmax),'l1536');
      alms_name_simset = strrep(alms_name_simset,sprintf('r%04d',rlz),sprintf('r%04d',lensrlz));
      alms_name_out = strrep(alms_name,sprintf('r%04d',rlz),sprintf('r%04d',lensrlz));
      if isempty(strfind(alms_name_out,'r0p1'))
        % The scalar CAMB spectra are fine up to l=1500 so the extended alms will have
        % no discontinuity
        lcross=1536;
      else
        % The tensor CAMB spectra are 30% low at l=1500 due to low k_eta_max. Upping
        % the l changes the spectrum here. Pick a lower crossover point to avoid a
        % discontinuity. This is probably fine because the r=0.1 spectra never get
        % lensed so there is no mixing of power among ell, and we never look at
        % l>600.
        lcross=600;
      end
      concatenate_fits_alms(alms_name_simset,alms_name,alms_name_out,lcross);

      if lcross==1536
        % Only compute phi a_lms for the lensed r=0 a_lms
        alms_name = alms_name_out;
        phi_map_in = sprintf('input_maps/lensing_potentials/camb_planck2013_r0/phi_r%04d.fits',lensrlz);
        phi_alm_in = sprintf('input_maps/lensing_potentials/camb_planck2013_r0/phi_alms_r%04d.fits',lensrlz);
        run_anafast(phi_map_in,phi_alm_in,1536,1);
        phi_nside = 2048;
        phi_lmax = nlmax;
      end
    else
      phi_alm_in = [];
      phi_nside = [];
      phi_lmax = [];
    end
      
  end
  

end

 
return

function lens_pot_name = get_lens_pot_name(rlz,nlmax,input_spec)
% create the name for the fits file to save the lensing potential to

alms_name = get_alms_name(rlz,nlmax,input_spec,1,'');
[dir,dum] = fileparts(alms_name);
dir = strrep(dir,'alms','lensing_potentials');
filename = ['phi' sprintf('_r%04d',rlz) '.fits'];
lens_pot_name = [dir '/' filename];

return



