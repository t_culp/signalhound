function rotate_healpixmap(file_in, file_out, select, lmax, nside, cut)
% rotate_healpixmap(file_in, file_out, select, lmax, nside, cut)
%
% Rotates a healpix map
% INPUTS:
%
%      file_in: filename of input healpix alms (or map)
%      file_out: filename of output healpix alms (or map)
%      SELECT   From          To        |   SELECT      From            To
%       1     RA-Dec (2000)  Galactic   |     4       Ecliptic      RA-Dec    
%       2     Galactic       RA-DEC     |     5       Ecliptic      Galactic  
%       3     RA-Dec         Ecliptic   |     6       Galactic      Ecliptic  
%
%       OR 
%       select is a 3 element vector (psi, theta, phi) 
%       in radians ([-2pi,2pi],[-2pi,2pi], [-2pi,2pi])
%       specifiying arbitrary euler angles to rotate by
%       
%       select=0 does no rotation
%
% OPTIONAL INPUTS
%
%     lmax : maximum ell to include (default 2.5*nside)
%     cut: coordinates (ra, dec) of cut sky
%          default: [-45,-70], [-55,55]
%          cut=0 -> Full sky
% e.g.
% file_in='/n/bicepfs2/b2planck/pipeline/input_maps/FFP8_Planck/temp/alms_20141029_014051_dy4e.fits'
% file_out='/n/bicepfs2/b2planck/pipeline/input_maps/FFP8_Planck/temp/alms_20141029_014051_dy4e_rot.fits'
% rotate_healpixmap(file_in, file_out, 2, 2.5*512, 512, 0)
% 
% in_map='/n/bicepfs2/b2planck/pipeline/input_maps/FFP8_Planck/ffp8_fg_bpm_353_full_map.fits';
% out_map='/n/bicepfs2/b2planck/pipeline/input_maps/FFP8_Planck/ffp8_fg_bpm_353_full_map_rot.fits';
% rotate_healpixmap(in_map,out_map,2, 512*2.5, 512,0)
% 
% in_map='/n/bicepfs2/b2planck/pipeline/input_maps/FFP8_Planck/ffp8_fg_bpm_353_full_map.fits';
% out_map='/n/bicepfs2/b2planck/pipeline/input_maps/FFP8_Planck/ffp8_fg_bpm_353_full_map_arbrot.fits';
% rotate_healpixmap(in_map,out_map,[-pi/2, pi, 1.3], 512*2.5, 512,0)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist('select','var') || isempty(select)
  error('must input select')
end

if ~exist('nside','var') || isempty(nside)
  nside=512;
end

if ~exist('lmax','var') || isempty(lmax)
  lmax=2.5*nside;
end

if ~exist('cut', 'var')
  cut=[];
end

if isempty(cut) || cut==1
  clear cut
  cut.theta1='-70.';
  cut.theta2='-45.';
  cut.phi1='-55.';
  cut.phi2='55.';
elseif cut==0
  cut=[];
end

direc=strfind(file_out, '/');
tempdir=[file_out(1:max(direc)) '/temp/'];
if ~exist(tempdir,'dir')
  system_safe(['mkdir ' tempdir]);
end

%load or calculate the alms from the map
%=========================================================
info=fitsinfo(file_in);
input_is_map = check_hmap(info.BinaryTable(1).Keywords);

if input_is_map
  disp('Input file appears to be a map...creating alms...')
  tmp_alms=[tempdir '/alms_',gen_stamp,'.fits']
  run_anafast(file_in, tmp_alms, lmax);
  disp('done making alms')
  disp('reading alms...')
  info=fitsinfo(tmp_alms);
  %alm=read_fits_alms(tmp_alms,1);
  alm(1)=read_fits_alms(tmp_alms,1);
  alm(2)=read_fits_alms(tmp_alms,2);
  alm(3)=read_fits_alms(tmp_alms,3);
  disp('done reading alms')
else
  disp('reading alms...')
  info=fitsinfo(file_in);
  %alm=read_fits_alms(file_in,1);
  alm(1)=read_fits_alms(file_in,1);
  alm(2)=read_fits_alms(file_in,2);
  alm(3)=read_fits_alms(file_in,3);
  disp('done reading alms')
end

%figure out what angles to rotate by
%=========================================================
if (numel(select)==1)
  % Use function coordsys2euler_zyz.m to find euler angles for
  % coordinate transformations
  switch select
    case 0
      psi=0;theta=0;phi=0;
    case 1
      [psi, theta, phi]=coordsys2euler_zyz(2000, 2000, 'Q', 'G');
    case 2
      [psi, theta, phi]=coordsys2euler_zyz(2000, 2000, 'G', 'Q');
    case 3
      [psi, theta, phi]=coordsys2euler_zyz(2000, 2000, 'Q', 'E');
    case 4
      [psi, theta, phi]=coordsys2euler_zyz(2000, 2000, 'E', 'Q');
    case 5
      [psi, theta, phi]=coordsys2euler_zyz(2000, 2000, 'E', 'G');
    case 6
      [psi, theta, phi]=coordsys2euler_zyz(2000, 2000, 'G', 'E'); 
  end
else 
  %if select is a vector of euler anlges, than to do requested rotation
  psi=select(1);theta=select(2);phi=select(3);
end
      

%rotate the alms
%=========================================================

%convert alms from fits form
alm_in=convert_alm(alm);

%rotate alms
alm_out=rotate_alms(lmax, alm_in, psi, theta, phi);

%now convert alms back to fits form
alms=invert_alm(alm_out);

%now write fits alms
alm=alm(1);
alm=rmfield(alm,'alms')
alm.index=alms(1).index;
for kd=1:numel(alms)
  alm.alms(:,:,kd)=[alms(kd).alms(:,1),alms(kd).alms(:,2)];
end

if(input_is_map)
  alms_rot=strrep(tmp_alms, '.fits', '_rot.fits');
else
  alms_rot=file_out;
end
write_fits_alms(alms_rot,alm)


%calculate map from rotated alms
%=========================================================
if(input_is_map)
  disp('since input was a map, making a map as the output')
  run_synfast([],[],alms_rot,nside,file_out,[],cut,'0',lmax);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out_alm=convert_alm(alm)
%function alm=convert_alm(alm)
%
%converts from fits file alms data structure to one with
%(p,l,m)
%
% to go back, use invert_alm.m

%------------------------------
%eg.
%alm=read_fits_alms('input_maps/alms/camb_planck2013_r0/alm_unlens_l1536_r0001.fits');
%alm=convert_alm(alm);
%------------------------------

nd=numel(alm);

for kd=1:nd
  
  index=alm(kd).index;
  in_alms=alm(kd).alms;
  
  %index=l^2+l+m+1  => complete the square
  %l=(index-m+3/4)^1/2-1/2... assume m=0 and take the floor,
  %then make array of l values for these alms
  ell=floor(sqrt(index-.75)-1/2);
  
  m=index-(ell+1/2).^2-3/4;
  
  lmax=max(ell);
  %out_alm=zeros(nd,lmax, lmax);
  
  %convert 2 column alms (real, imag) into complex
  in_alms=in_alms(:,1)+complex(0,1).*in_alms(:,2);
  
  for i=1:length(index)
    out_alm(kd,ell(i)+1, m(i)+1)=in_alms(i);
    out_index(kd,ell(i)+1, m(i)+1)=index(i);
  end
    
  
end

    
return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out_alm=invert_alm(alm)
%function alm=invert_alm(alm)
%
%converts alms in the form (p,l,m) to one in the fits 
%file alms data structure
%
%% to go back, use convert_alm.m
%------------------------------

lmax=size(alm,2)-1;
index=zeros(lmax+1,lmax+1);

%create the fits file alm "index"
for l=0:lmax
  m=0:l;
  ll=l+1;
  index(ll,1:l+1)=l^2+l+m+1;
end

%set zeros to NAN
index(index==0)=NaN;

%dimensions of alm
nd=size(alm,1);

for kd=1:nd
  
  alm_s=squeeze(alm(kd,:,:));
  
  %reshape into vector
  alm_v=reshape(alm_s.', [1,(lmax+1)^2]);
  index_v=reshape(index.', [1,(lmax+1)^2]);

  %remove nan points
  locs=find(~isnan(index_v));
  out_alm(kd).index=index_v(locs)';
  out_alm(kd).alms(:,1)=real(alm_v(locs));
  out_alm(kd).alms(:,2)=imag(alm_v(locs));
end
  
  
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function unit = check_hmap(hdr)
for ii =1:size(hdr,1)
  if any(strfind(hdr{ii,2},'ANAFAST'))
    unit=0;
    break
  elseif (any(strfind(hdr{ii,2},'MAP'))) || (any(strfind(hdr{ii,2},'SYNFAST')))
    unit=1;
    break
  elseif (any(strfind(hdr{ii,2},'NESTED')))
    unit=1
    break
  end
end
if(~exist('unit', 'var'))
  error('Could not determine whether input file was map or alms')
end
  return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function unit = get_unit(hdr)
for ii =1:size(hdr,1)
  if any(strfind(hdr{ii,1},'TUNIT2')) 
    unit=hdr{ii,2};
    return
  end
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%