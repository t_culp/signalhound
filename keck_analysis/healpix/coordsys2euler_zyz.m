function [psi, theta, phi]=coordsys2euler_zyz(iepoch, oepoch, isys, osys)
% [psi, theta, phi]=coordsys2euler_zyz(iepoch, oepoch, isys, osys)
%
% routine producing Euler angles psi, theta, phi 
%  (for rotation around the unrotated Z, Y, Z axes respectively)
% corresponding to changes of coordinates from ISYS to OSYS
% for respective epochs IEPOCH and OEPOCH
% the angles definition match the one required for rotate_alm.
% EH, March 2005
%   April 06 2005, corrected psi calculation
% Converted to Matlab 20141027 J.Tolan
%
%

% generate basis vector
v1 = [1.0, 0.0, 0.0];
v2 = [0.0, 1.0, 0.0];
v3 = [0.0, 0.0, 1.0];

% rotate them 
v1p=xcc_V_CONVERT(v1,iepoch,oepoch,isys,osys);
v2p=xcc_V_CONVERT(v2,iepoch,oepoch,isys,osys);
v3p=xcc_V_CONVERT(v3,iepoch,oepoch,isys,osys);

%normalize vectors
v1p = v1p / sqrt(dot(v1p,v1p));
v2p = v2p / sqrt(dot(v2p,v2p));
v3p = v3p / sqrt(dot(v3p,v3p));

% figure out Euler angles
theta=angdist(v3,v3p);
psi = atan2(v2p(3),-v1p(3));
phi = atan2(v3p(2), v3p(1));

return

function dist=angdist(v1, v2)
%=======================================================================
% call angdist(v1, v2, dist)
% computes the angular distance dist (in rad) between 2 vectors v1 and v2
% dist = atan2 ( |v1 x v2| / (v1 . v2) )
% (more accurate than acos(v1. v2) when dist close to 0 or Pi.
% 2011-08-25: replaced ACOS with ATAN2
%=======================================================================
% scalar product   s = A. cos(theta)
sprod = dot(v1,v2);
%vectorial product  v = A. sin(theta)
v3=cross(v1, v2);
vprod = sqrt(dot(v3,v3));
% theta = atan( |v|/s) in [0,Pi]
dist = atan2(vprod, sprod);

return

function ovector=xcc_V_CONVERT(ivector,iepoch,oepoch,isys,osys)
%
%  Function to convert between standard coordinate systems, including
%  precession.
%  
%
%    Q,C:  Equatorial coordinates
%    E  :  Ecliptic coordinates
%    G  :  Galactic coordinates
%    SG :  Super Galactic coordinates  (TB implemented)
%    U  :  Universal Rest-frame coordinates (TB implemented)
%
%  03/14/88, a.c.raugh, STX.
%  11/15/01, C.Burigana & D.Maino, OAT
%
%
%  Arguments:
%
%   IVECTOR(1:3)   ! Input coordinate vector
%   IEPOCH     ! Input epoch
%   OEPOCH     ! Output epoch    
%   OVECTOR(1:3)   ! Output coordinate vector
%   ISYS       ! Input system
%   OSYS       ! Output system


isysu = strtrim(upper(isys));
osysu = strtrim(upper(osys));

%  If the input coordinate system was not ecliptic, convert to ecliptic
%  as intermediate form:

if (isysu ~= 'E')
  if (isysu == 'Q' || isysu == 'C')
    xvector = xcc_dp_q_to_e(ivector,iepoch);
  elseif (isysu == 'G')
    xvector = xcc_dp_g_to_e(ivector,iepoch);
  else
    error([' unknown input coordinate system: ' isysu])
  end
else
  xvector(1:3)=ivector(1:3);
end
%
%  If the begin and end epoch are different, precess:
%
if (iepoch ~= oepoch) 
  yvector=xcc_dp_precess(xvector,iepoch,oepoch);
else
  yvector(1:3)=xvector(1:3);
end
%
%  If output system is not ecliptic, convert from ecliptic to desired
%  system:
%
if (osysu ~= 'E') 
  if (osysu == 'Q') || (osysu == 'C')
    ovector=xcc_dp_e_to_q(yvector,oepoch);
  elseif (osysu == 'G')
    ovector=xcc_dp_e_to_g(yvector,oepoch);
  else
    error([' unknown output coordinate system: ' osysu])      
  end
else
  ovector(1:3)=yvector(1:3);
end
return

function ovector=xcc_dp_e_to_q(ivector,epoch)
%
%  Routine to convert from ecliptic coordinates to equatorial (celestial)
%  coordinates at the given epoch.  Adapted from the COBLIB routine by Dr.
%  E. Wright.
%
%  02/01/88, a.c.raugh, STX.
%  11/16/01, C.Burigana,D.Maino, OAT
%
%
%  Arguments:
%
%   IVECTOR(3)   ! Input coordinate vector
%    EPOCH      ! Equatorial coordinate epoch
%   OVECTOR(3)   ! Output coordinate vector

%  Set-up:
%
T = (epoch - 1900.d0) / 100.d0 ;
epsilon = 23.452294d0 - 0.0130125d0*T - 1.63889d-6*T^2   + 5.02778d-7*T^3;
%
%  Conversion:
%
dc = cosd(epsilon);
ds = sind(epsilon);
hvector(1) = ivector(1);
hvector(2) = dc*ivector(2) - ds*ivector(3);
hvector(3) = dc*ivector(3) + ds*ivector(2);
%
%  Move to output vector:
%
ovector(1:3) = hvector(1:3);
%
return

function ovector=xcc_dp_e_to_g(ivector,epoch)
%
%  Routine to convert ecliptic (celestial) coordinates at the given
%  epoch to galactic coordinates.  The ecliptic coordinates are first
%  precessed to 2000.0, then converted. 
%
%              --------------
%
%  Galactic Coordinate System Definition (from Zombeck, "Handbook of
%  Space Astronomy and Astrophysics", page 71):
%
%     North Pole:  12:49     hours right ascension (1950.0)
%           +27.4    degrees declination   (1950.0)
%
%  Reference Point:  17:42.6   hours right ascension (1950.0)
%           -28 55'    degrees declination   (1950.0)
%
%              --------------
%
%  03/08/88, a.c.raugh, STX.
%  11/16/01, C.Burigana, D.Maino, OAT
%
%
%  Arguments:
%
%   real(dp) ::  IVECTOR(3)    ! Input coordinate vector
%   real(dp) ::  EPOCH       ! Input coordinate epoch
%   real(dp) ::  OVECTOR(3)    ! Output coordinate vector
%
%  Program variables:
%
%   real(dp) ::  HVECTOR(3)    ! Temporary holding place for coordinates
%   real(dp) ::  T(3,3)      ! Galactic coordinate transformation matrix
%   integer(i4b) ::  I       ! Loop variables
% 
T =[-0.054882486d0, -0.993821033d0, -0.096476249d0; %1st column
  0.494116468d0, -0.110993846d0,  0.862281440d0; %2nd column
  -0.867661702d0, -0.000346354d0,  0.497154957d0]; %3rd column
T=T.';
%
%   Precess coordinates to 2000.0 if necesssary:
%
if (epoch ~= 2000.d0)
  hvector=xcc_dp_precess(ivector,epoch,2000.d0);
else
  hvector(1:3)=ivector(1:3);
end
%
%   Multiply vector by transpose of transformation matrix:
%
for i = 1:3
  ovector(i) = sum(hvector(1:3).*T(1:3,i).');
end

return

function  ovector=xcc_dp_g_to_e(ivector,epoch)
%
%  Routine to convert galactic coordinates to ecliptic (celestial) 
%  coordinates at the given epoch.  First the conversion to ecliptic 
%  2000 is done, then if necessary the results are precessed.
%
%  03/08/88, a.c.raugh, STX.
%  11/16/01, C.Burigana, D.Maino, OAT
%
%
%  Arguments:
%
%  real(dp) ::  IVECTOR(3)    ! Input coordinate vector
%  real(dp) ::  EPOCH       ! Input coordinate epoch
%  real(dp) ::  OVECTOR(3)    ! Output coordinate vector
%
%  Program variables:
%
%  real(dp) ::  HVECTOR(3)    ! Temporary holding place for coordinates
%  real(dp) ::  T(3,3)      ! Galactic coordinate transformation matrix
%  integer(i4b) ::  I,J       ! Loop variables
%
T =[ -0.054882486d0, -0.993821033d0, -0.096476249d0; % 1st column
    0.494116468d0, -0.110993846d0,  0.862281440d0; % 2nd column
   -0.867661702d0, -0.000346354d0,  0.497154957d0]; % 3rd column
T=T.';
%
%   Multiply by transformation matrix:
%
for i = 1:3
  hvector(i) = sum(ivector(1:3).*T(i,1:3));
end
%
%   Precess coordinates to epoch if necesssary:
%
if (epoch ~= 2000.d0)
%     error('epoch converson not supported')
  ovector=xcc_dp_precess(hvector,2000.d0,epoch);
else
  ovector(1:3)=hvector(1:3);
end
%
return


function ovector=xcc_dp_q_to_e(ivector,epoch)
%
%  Routine to convert equatorial (celestial) coordinates at the given
%  epoch to ecliptic coordinates.  Adapted from the COBLIB routine by Dr.
%  E. Wright.
%
%  02/01/88, a.c.raugh, STX.
%  11/16/01, C.Burigana, D.Maino, OAT
%
%
%  Arguments:
%
%  real(dp) ::  IVECTOR(3)   ! Input coordinate vector
%   real(dp) ::  EPOCH      ! Epoch of equatorial coordinates
%  real(dp) ::  OVECTOR(3)   ! Output coordinate vector
%
%  Program variables:
%
%  real(dp) ::  T        ! Centuries since 1900.0
%  real(dp) ::  EPSILON    ! Obliquity of the ecliptic, in degrees
%  real(dp) ::  HVECTOR(3)   ! Temporary holding place for output vector
%  real(dp) ::  dc, ds
%
%  Set-up:
%
T = (epoch - 1900.d0) / 100.d0;
epsilon = 23.452294d0 - 0.0130125d0*T - 1.63889d-6*T^2 + 5.02778d-7*T^3;
%
%  Conversion:
%
dc = cosd(epsilon);
ds = sind(epsilon);
hvector(1) = ivector(1);
hvector(2) = dc*ivector(2) + ds*ivector(3);
hvector(3) = dc*ivector(3) - ds*ivector(2);
%
%  Move to output vector:
%
ovector(1:3) = hvector(1:3);

return

function  ovector=xcc_dp_precess(ivector,iepoch,oepoch)
%
%  Subroutine to precess the input ecliptic rectangluar coordinates from
%  the initial epoch to the final epoch.  First the precession around the
%  Z-axis is done by a simple rotation.  A second rotation about the 
%  X-axis is then performed to account for the changing obliquity of the
%  ecliptic.
%
%  02/25/88, a.c.raugh, STX.
%  03/22/88, acr: modified to include changing epsilon
%  11/16/01, Carlo Burigana & Davide Maino, OAT
%
%
%  Parameters:
%
%   real(dp) ::   IVECTOR(3)  ! Input coordinates
%   real(dp) ::   IEPOCH    ! Initial epoch, years (input)
%   real(dp) ::   OEPOCH    ! Final epoch, years (input)
%   real(dp) ::   OVECTOR(3)  ! Output coordinates
%
%  Routine variables:
%
%   real(dp) ::   TEMP    ! Holding place for rotation results
%   real(dp) ::   GP_LONG   ! General precession in longitude
%   real(dp) ::   dE      ! Change in the obliquity of the ecliptic
%   real(dp) ::   OBL_LONG  ! -Longitude about which the obliquity is changing
%   real(dp) ::   dL      ! Longitude difference
%   real(dp) ::   TM      ! Mid-epoch, in tropical centuries since 1900
%   real(dp) ::   TVECTOR(3)  ! Temporary holding vector
%   real(dp) :: dco, dso, dce, dse, dcl, dsl
%
Tm = ((oepoch+iepoch)/2.d0 - 1900.d0) / 100.d0;
gp_long  = (oepoch-iepoch) * (50.2564d0+0.0222d0*Tm) / 3600.d0;
dE     = (oepoch-iepoch) * (0.4711d0-0.0007d0*Tm) / 3600.d0;
obl_long = 180.d0 - (173.d0 + (57.06d0+54.77d0*Tm) / 60.d0) + gp_long / 2.d0;
dL     = gp_long - obl_long;
%
%   Z-axis rotation by OBL_LONG:
%
dco = cosd(obl_long);
dso = sind(obl_long);
tvector(1) = ivector(1)*dco - ivector(2)*dso;
tvector(2) = ivector(1)*dso + ivector(2)*dco;
tvector(3) = ivector(3);
%
%   X-axis rotation by dE:
%
dce = cosd(dE);
dse = sind(dE);
temp     = tvector(2)*dce - tvector(3)*dse;
tvector(3) = tvector(2)*dse + tvector(3)*dce;
tvector(2) = temp;
%
%   Z-axis rotation by GP_LONG - OBL_LONG:
%
dcl = cosd(dL);
dsl = sind(dL);
temp     = tvector(1)*dcl - tvector(2)*dsl;
tvector(2) = tvector(1)*dsl + tvector(2)*dcl;
tvector(1) = temp;
%
%   Transfer results to output vector:
%
ovector(1:3) = tvector(1:3);

return
