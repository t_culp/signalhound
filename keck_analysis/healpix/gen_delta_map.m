function map_name = gen_delta_map(specs,mpls,rlzs,nside,nlmax,beam)
% map_name = gen_delta_map(spec,nside,nlmax,rlz,beam)
% creates the map "map_name" which has a delta peak at the given multipole mpls.
% specs     = 'E' - delta function in TT,TE and EE
%           = 'B' - delta function in TT and BB
%           = 'EB' - (default) loop through both
% mpls      = 80 (default)
%           = [10,20,30]
% rlzs      = 0 (default); is passed to run_synfast where it will use the random seed list
%           = [0:100]
% nside     = 512 (default)
% nlmax     = 1499 (default)
% beam      = {get_bl_filename('B2bbns'),'B2bbns'} (default), 
%             can also be numeric to specify fwhm
%  
% gen_delta_map('EB',10:20:100,[0:3])
  
if ~exist('specs','var')
  specs = 'E';
end
if ~exist('mpls','var')
  mpls = 80;
end
if ~exist('rlzs','var')
  rlzs = 0;
end
if ~exist('nside','var')
  nside = 512;
end
if ~exist('nlmax','var')
  nlmax = 1499;
end
if ~exist('beam','var')
  beam = {get_bl_filename('B2bbns'),'B2bbns'}; 
end

% pic a regular fits file for spectra as template:
filename='aux_data/official_cl/camb_planck2013_r0p1.fits';
data=fitsread(filename, 'table');
info=fitsinfo(filename);
hdr=info.PrimaryData.Keywords; xhdr=info.AsciiTable.Keywords;

for spec=specs
  for l=mpls
    for rlz=rlzs
      % order in the data coming from CAMB: TT,EE,BB,TE
      % nullify the spectra:
      for ii=1:length(data)
        data{ii}=zeros(size(data{ii}));
      end

      % the fits file has the cl
      % set the delta peak such that l(l+1)Cl is 1
      df = 1/(l*(l+1));
      % always set TT to one:
      data{1}(l+1)=df;
      switch spec
        case 'E'
          % set EE and TE to one
          data{2}(l+1)=df;
          data{4}(l+1)=df;
        case 'B'
          % set BB to one
          data{3}(l+1)=df;
      end
      % proper name for the spectrum will be handled by gen_synfast_map_name
      % to make a proper map name in the delta/ folder
      input_spec=['delta',spec];
      input_spec = [input_spec sprintf('_l%04d',l)];
      map_name = gen_synfast_map_name(input_spec,beam,nside,rlz,0,'','');
      
      % write the spectrum to a temp file:
      if(~exist('input_maps/delta', 'dir'))
        system_safe('mkdir input_maps/delta')
      end
      input_spec = ['input_maps/delta/' input_spec '_' gen_stamp() '.fits'];
      fitswrite_table(data, hdr, xhdr, input_spec)
      
      % and finally run synfast:
      cut.theta1=-70; cut.theta2=-45;cut.phi1=-55; cut.phi2=55;
      run_synfast(rlz, input_spec, [], nside, map_name, [], cut, beam, nlmax);
      
      % do some plotting to check
      if (0)
        inpmod=load_cmbfast(input_spec);
        fig = figure()
        set(fig,'Position',[1925,253,865,567])
        titles = {'TT','TE','EE','BB'}
        for ii=1:4
          subplot(2,2,ii)
          hold
          plot(inpmod.l,inpmod.Cs_l(:,ii),'k')
          xlabel('Multipole l')
          ylabel('l(l+1)C_{ l}/2\pi [\mu K^2]')
          title(titles{ii})
          ylim([0,2e11])
        end
        map=read_fits_map(map_name);
        gs=0.5;
        lon=-180+gs/2:gs:180-gs/2;
        lat=-90+gs/2:gs:90-gs/2;
        map=healpix_to_longlat(map,lon,lat);

        fig = figure()
        set(fig,'Position',[2051,157,568,702])
        titles={'T','Q','U'}
        for ii=1:3
          subplot(3,1,ii)
          imagesc(lon,lat,map(:,:,ii));
          colorbar;
          set(gca,'YDir','normal');
          set(gca,'XDir','reverse');
          xlim([-55,55])
          ylim([-70,-45])
%              xlabel('Longitude [deg]')
%              ylabel('Latitude [deg]')
%              title(50,-66,titles{ii},'Fontsize',14 ,'color','y')
          lstr = sprintf('%03d',l)
          title([titles{ii},' l',lstr,' ',spec]);
        end
        mkpng(['synfast_',lstr,spec,'.png'])
      end
      
      % remove the temporary spectrum:
      system(['rm ',input_spec])
      
    end
  end
end
return

