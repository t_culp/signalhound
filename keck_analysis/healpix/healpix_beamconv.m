function [map,neighb_out]=healpix_beamconv(hmap,ra,dec,tilt,beampar,neighb)
%  [map,neighb_out]=healpix_beamconv(hmap,ra,dec,tilt,beampar,neighb)
% wmap maps are in galactic coords so we need to xform them - the
% coords sys doesn't appear to be stored in any of the header fields
% of the healpix maps - as it surely should be
% Note that we don't xform the Q/U (if present) so gal pol maps will
% be broken!

if(strfind(hmap.filename,'wmap'))
  [ra,dec]=euler(ra,dec,1,0);
end

% deg to rad
theta=cvec((90-dec)*pi/180);
phi=cvec(ra*pi/180);

% Find nearest pixel to each requested position
switch hmap.ordering
 case 'RING'
   pix=ang2pix_ring(hmap.nside,theta,phi);
 case 'NESTED'
   pix=ang2pix_nest(hmap.nside,theta,phi);
 otherwise
   error('healpix map must have RING or NESTED ordering')
end

% default cutoff radius for neighborhoods, in degrees
rcut = 0.5;

% Construct neighbor list if needed
if nargin<5 || isempty(neighb)
  neighb=healpix_neighborhood(hmap,rcut);
end
if nargout>1
  neighb_out = neighb;
end

% Default beam parameters
if nargin<4 || isempty(beampar)
  beampar.fwhm_maj=0.25;
  beampar.fwhm_min=0.25;
  beampar.alpha=0;
end

% Offset flat-sky coordinates to account for difference
% between healpix pixel centers and mount-pointing samples
switch hmap.ordering
  case 'RING'
    [hpixtheta,hpixphi]=pix2ang_ring(hmap.nside,pix);
  case 'NESTED'
    [hpixtheta,hpixphi]=pix2ang_nest(hmap.nside,pix);
end
[dist,azim]=distance(hpixtheta+pi/2,hpixphi,theta+pi/2,phi,'radians');
dxofs=dist.*cos(azim);
dyofs=dist.*sin(azim);

% List pixels to be used for interp
disp(['Making pixel list.']);
usepix=NaN*zeros(length(ra),size(neighb.list,2));
dx=usepix;
dy=usepix;
for j=1:size(neighb.list,2)
  usepix(:,j)=neighb.list(pix+1,j);
  dx(:,j)=neighb.dx(pix+1,j)-dxofs;
  dy(:,j)=neighb.dy(pix+1,j)-dyofs;
end

% Loop over num neighbors, find map values for each
for j=1:min(3,size(hmap.map,2))
  val{j}=NaN*zeros(size(usepix));
end 
disp(['Finding map values and beam kernel offsets.']);
for i=1:size(usepix,2)
  tmpusepix=usepix(:,i);
  cc=~isnan(tmpusepix);
  tmpusepix(~cc)=0;
  for j=1:length(val)
    val{j}(:,i)=hmap.map(tmpusepix+1,j);
  end
end

% evaluate beam kernel at selected neighbor points
weight=eval_beam(dx,dy,tilt,beampar);
% ensure unit normalization - is this a good idea?
weightnorm=nansum(weight,2);

% sum weighted healpix values
for j=1:length(val)
  map(:,j)=nansum(val{j}.*weight,2)./weightnorm;
end

return


%%%%%%%%
function weight=eval_beam(dx,dy,tilt,beampar)

% weight = max(1 ./ ((dist + 0.0).^2) - 1, 0);
% weight = (dist < 0.5);

% calculate beam sigma from fwhm
c=2*sqrt(2*log(2));
p=[1,0,0,beampar.fwhm_maj/c,beampar.fwhm_min/c,beampar.alpha];
p=repmat(p,length(tilt),1);
p(:,end)=p(:,end)-tilt(:)*pi/180;
weight = egauss2(p,dx,dy);

return


%%%%%%%%
function neighb=healpix_neighborhood(hmap,r)

disp(['Finding first round of neighbors']);
tic
switch hmap.ordering
  case 'RING'
    [fpix cpix]=neighbor_ring(hmap.nside,hmap.pixel);
    [th ph]=pix2ang_ring(hmap.nside,hmap.pixel);
  case 'NESTED'
    [fpix cpix]=neighbor_nest(hmap.nside,hmap.pixel);
    [th ph]=pix2ang_nest(hmap.nside,hmap.pixel);
  otherwise
    error('healpix map must have RING or NESTED ordering');
end
neighb0=[hmap.pixel fpix cpix];
clear fpix cpix
pixvec=[sin(th).*cos(ph),sin(th).*sin(ph),cos(th)];
toc

disp(['Running mex code']);
tic
neighb.list=healpix_neighborhood_c(neighb0,pixvec,r);
toc

disp(['Finding flat-sky-approximation position offsets']);
tic
neighb.dx=zeros(size(neighb.list));
neighb.dy=zeros(size(neighb.list));
for i=1:size(neighb.list,2)
  tmpusepix=neighb.list(:,i);
  cc = isfinite(tmpusepix) & (tmpusepix>=0);
  tmpusepix(~cc)=0;
  switch hmap.ordering
    case 'RING'
      [useth,useph]=pix2ang_ring(hmap.nside,tmpusepix);
    case 'NESTED'
      [useth,useph]=pix2ang_nest(hmap.nside,tmpusepix);
  end
  if i==1
    th0=useth;
    ph0=useph;
    continue
  end
  dist=zeros(size(tmpusepix));
  azim=zeros(size(tmpusepix));
  [dist(cc),azim(cc)]=distance(th0(cc)+pi/2,ph0(cc),useth(cc)+pi/2,useph(cc),'radians');
  neighb.dx(:,i) = dist .* cos(azim);
  neighb.dy(:,i) = dist .* sin(azim);
end
toc

return

