function run_alteralm(infile_alms, outfile_alms, beam_in, beam_out, GtoC, nsmax, keepCoord)
%  run_alteralm(infile_alms, outfile_alms, beam_in, beam_out, GtoC, nsmax, keepCoord)
%  
%  runs the alteralm program
%  
%  infile_alms     : filename of alms to manipulate
%  
%  outfile_alms    : filename to save to
%  
%  beam_in         : beam to remove from input alms
%                    can be beamwidth in arcmin, i.e. 28
%                    can be '...fits' file containing Bls
%                    
%  beam_out        : beam to apply to output alms
%                    can be beamwidth in arcmin, i.e. 28
%                    can be '...fits' file containing Bls
%  
%  GtoC            : 1 or 0, do a rotation from G to C
%
%  nsmax           : pixel window nside
%  
%  keepCoord       : (default false), keep the coordinate system specified
%                    in the infile_alms file header alive - only applied when
%                    GtoC=0
%

display(['Running alteralm ',infile_alms,' -> ',outfile_alms])

if ~exist('GtoC','var')
  GtoC=false;
end

if exist(outfile_alms,'file')
  system_safe(['rm -f ' outfile_alms]);
end

if ~exist('nsmax','var')
  nsmax='0';% no window pixel function
end

if ~exist('keepCoord','var')
  keepCoord=false;
end
  
if(isnumeric(nsmax))
  nsmax=num2str(nsmax);
end

if ~exist('synfast_tmp','dir')
  mkdir('synfast_tmp');
end
infile = ['synfast_tmp/alteralm' gen_stamp() '.in'];

h=fopen(infile,'wt');

fprintf(h,['infile_alms = ' infile_alms '\n']);
fprintf(h,['outfile_alms = ' outfile_alms '\n']);


if any(strfind(beam_in,'.fits'))
  fprintf(h,['beam_file_in = ' beam_in '\n']);
else
  fprintf(h,['fwhm_arcmin_in = ' num2str(beam_in) '\n']);
end

if any(strfind(beam_out,'.fits'))
  fprintf(h,['beam_file_out = ' beam_out '\n']);
else
  fprintf(h,['fwhm_arcmin_out = ' num2str(beam_out) '\n']);
end

fprintf(h,['nsmax_out = ' nsmax ' \n']); 

if GtoC
  fprintf(h, 'coord_in = G \n');
  fprintf(h, 'coord_out = C \n');
end

if ~GtoC && keepCoord
  fi = fitsinfo(infile_alms);
  n=find(strcmp(fi.BinaryTable(1).Keywords(:,1),'COORDSYS'));
  coordsys=upper(fi.BinaryTable(1).Keywords{n,2});
  % just the first letter
  coordsys=coordsys(1);
  fprintf(h, ['coord_in = ',coordsys,' \n']);
  fprintf(h, ['coord_out = ',coordsys,' \n']);
end

fclose(h);

system_safe(['alteralm ' infile]);

% no debug mode: remove tmp files
if isempty(whos('global','synfast_debug'))
  system_safe(['rm -f ' infile]);
end

return
