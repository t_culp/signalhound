function run_anafast(map_file, out_file, nlmax, simtype, spec_file, mask_file)
% run_anafast(map_file, out_file, nlmax, simtype, spec_file, maskfile)
%
% map_file = input fits file of map (must be full sky)
%
% out_file = output fits file of alms
%
% nlmax = max ell of alms (to be safe should not exceed 2x nside of input map)
%
% simtype = 1 for input T only map
%         = 2 for input T/Q/U maps (default)          
%
% spec_file = output power spectrum fits file; sets anafast "outfile" parameter; not
%             specified by default.
%
% maskfile = not specified by default

display(['Running anafast ',map_file,' -> ',out_file])

if ~exist('simtype','var') | isempty(simtype)
  simtype=2;
end

if isnumeric(simtype)
  simtype=num2str(simtype);
end

if exist(out_file,'file')
  system_safe(['rm -f ' out_file]);
end

if ~exist('synfast_tmp','dir')
  mkdir('synfast_tmp');
end
infile = ['synfast_tmp/anafast_' gen_stamp() '.in'];

h=fopen(infile,'wt');
fprintf(h, ['simul_type=' simtype '\n']);
fprintf(h, ['infile='  map_file '\n']);
if exist('spec_file','var') && ~isempty(spec_file)
  fprintf(h, ['outfile=' spec_file '\n']);
end
if exist('mask_file','var') && ~isempty(mask_file)
  fprintf(h, ['maskfile=' mask_file  '\n']);
end
fprintf(h, ['outfile_alms=' out_file '\n']);
fprintf(h, ['nlmax= ' num2str(nlmax) '\n']);
fclose(h);
system_safe(['anafast ' infile]);

system_safe(['rm -f ' infile]);

% Remove power spectrum if not requested, which defaults to cl_out.fits
if ~exist('spec_file','var')
  system_safe('rm -f cl_out.fits');
end
 
return
