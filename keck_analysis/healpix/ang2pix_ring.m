function ipring=ang2pix_ring(nside,theta,phi)
% ipring=ang2pix_ring(nside,theta,phi)
%
%        renders the RING scheme pixel number Ipring for a pixel which, given the
%        map resolution parameter Nside, contains the point on the sphere
%        at angular coordinates Theta and Phi
%
% INPUT
%    Nside     : determines the resolution (Npix = 12* Nside^2)
%	SCALAR
%    Theta : angle (along meridian), in [0,Pi], theta=0 : north pole,
%	can be an ARRAY
%    Phi   : angle (along parallel), in [0,2*Pi]
%	can be an ARRAY of same size as theta
%
% OUTPUT
%    Ipring  : pixel number in the RING scheme of HEALPIX pixelisation in [0,Npix-1]
%	can be an ARRAY of same size as Theta and Phi
%    pixels are numbered along parallels (ascending phi), 
%    and parallels are numbered from north pole to south pole (ascending theta)
%
%
% SUBROUTINE
%    nside2npix
%
% HISTORY
%    June-October 1997,  Eric Hivon & Kris Gorski, TAC, 
%            original ang_pix
%    Feb 1999,           Eric Hivon,               Caltech
%            name changed to ang2pix_ring
%    Sept 2000,          EH
%           free memory by discarding unused variables
%    June 2003,  EH, replaced STOPs by MESSAGEs
%    Aug  2004,  EH, use !PI as theta upper-bound instead of !DPI
%    Feb  2006,  CLP, took this file from Healpix 2.01 distribution and
%              translated to Matlab
%
%*********************************************************************************

% -----------------------------------------------------------------------------
%
%  Copyright (C) 1997-2005  Krzysztof M. Gorski, Eric Hivon, Anthony J. Banday
%
%
%
%
%
%  This file is part of HEALPix.
%
%  HEALPix is free software% you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation% either version 2 of the License, or
%  (at your option) any later version.
%
%  HEALPix is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY% without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with HEALPix% if not, write to the Free Software
%  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
%
%  For more information about HEALPix see http://healpix.jpl.nasa.gov
%
% -----------------------------------------------------------------------------

if(nargin~=3)
  error('Must have 3 input arg');
end

if(length(nside)>1)
  error('nside should be a scalar');
end

npix = nside2npix(nside);

if(any(size(theta)~=size(phi)))
  error('inconsistent theta and phi');
end

if(any(theta<0|theta>pi))
  error('theta out of range');
end

% force theta/phi to be 1-D
array_shape = size(theta);
theta=theta(:);
phi=phi(:);

ipring = zeros(size(theta));

%------------------------------------------------------------
pion2 = pi * 0.5;
twopi = pi * 2;
nl2   = 2*nside;
nl4   = 4*nside;
ncap  = nl2*(nside-1);

cth0 = 2/3;

cth_in = cos(theta);
phi_in = mod(phi,twopi);
% phi_in = phi + (phi<=0)*twopi;

% NaN values ----------------------
pix_nan = find(~isfinite(cth_in) | ~isfinite(phi_in));
if(length(pix_nan)>0)
  ipring(pix_nan) = NaN;
  cth_in(pix_nan) = NaN;
end % -------------------------------------------------------

% equatorial strip ----------------
pix_eqt = find(cth_in<=cth0 & cth_in > -cth0); % equatorial strip
if(length(pix_eqt)>0)
  
  tt = phi_in(pix_eqt)/pion2;
  
  jp = floor(nside*(0.5 + tt - cth_in(pix_eqt)*0.75)); % increasing edge line index
  jm = floor(nside*(0.5 + tt + cth_in(pix_eqt)*0.75)); % decreasing edge line index
  
  ir = (nside + 1) + jp - jm; % in {1,2n+1} (ring number counted from z=2/3)
  k =  ( mod(ir,2) == 0);   % k=1 if ir even, and 0 otherwise
  
  ip = floor( ( jp+jm+k + (1-nside) ) / 2 ) + 1; % in {1,4n}
  ip = ip - nl4*(ip > nl4);
  
  ipring(pix_eqt) = ncap + nl4*(ir-1) + ip - 1;
end % -------------------------------------------------------
      
% north polar caps ------------------------
pix_np  = find(cth_in >  cth0);
if(length(pix_np)>0)

      tt = phi_in(pix_np) / pion2;
      tp = mod(tt,1);
      tmp = sqrt( 3*(1 - abs(cth_in(pix_np))) );

      jp = floor( nside * tp       .* tmp ); % increasing edge line index
      jm = floor( nside * (1 - tp) .* tmp ); % decreasing edge line index

      ir = jp + jm + 1;         % ring number counted from the closest pole
      ip = floor( tt .* ir ) + 1; % in {1,4*ir}
      ir4 = 4*ir;
      ip = ip - ir4.*(ip > ir4);

      ipring(pix_np) = 2*ir.*(ir-1) + ip - 1;   
end % -------------------------------------------------------

% south polar caps ------------------------
pix_sp=find(cth_in <= -cth0);
if(length(pix_sp)>0)
  
      tt = phi_in(pix_sp) / pion2;
      tp = mod(tt,1);
      tmp = sqrt( 3*(1 - abs(cth_in(pix_sp))));

      jp = floor( nside * tp       .* tmp ); % increasing edge line index
      jm = floor( nside * (1 - tp) .* tmp ); % decreasing edge line index

      ir = jp + jm + 1;          % ring number counted from the closest pole
      ip = floor( tt .* ir ) + 1; % in {1,4*ir}
      ir4 = 4*ir;
      ip = ip - ir4.*(ip > ir4);

      ipring(pix_sp) = npix - 2*ir.*(ir+1) + ip - 1;
end % -------------------------------------------------------


% make output same shape as input
ipring = reshape(ipring,array_shape);

return

%=======================================================================
% The permission to use and copy this software and its documentation, 
% without fee or royalty is limited to non-commercial purposes related to 
% Microwave Anisotropy Probe (MAP) and
% PLANCK Surveyor projects and provided that you agree to comply with
% the following copyright notice and statements,
% and that the same appear on ALL copies of the software and documentation.
%
% An appropriate acknowledgement has to be included in any
% publications based on work where the package has been used
% and a reference to the homepage http://www.tac.dk/~healpix
% should be included
%
% Copyright 1997 by Eric Hivon and Kris Gorski.
%  All rights reserved.
%=======================================================================
