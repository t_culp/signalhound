function [theta, phi] = pix2ang_nest(nside, ipix)
% [theta, phi] = pix2ang_nest(nside, ipix)
%
%       renders Theta and Phi coordinates of the nominal pixel center
%       given the NESTED scheme pixel number Ipix and map resolution parameter Nside
%
% INPUT
%    Nside     : determines the resolution (Npix = 12* Nside^2),
%       should be a power of 2 (not tested)
%	SCALAR
%    Ipix  : pixel number in the NESTED scheme of Healpix pixelisation in [0,Npix-1]
%	can be an ARRAY 
%
% OUTPUT
%    Theta : angle (along meridian = co-latitude), in [0,Pi], theta=0 : north pole,
%	is an ARRAY of same size as Ipix
%    Phi   : angle (along parallel = azimut), in [0,2*Pi]
%	is an ARRAY of same size as Ipix
%
% SUBROUTINE
%    nside2npix
%
% HISTORY
%     Feb 1999,           Eric Hivon,               Caltech
%    Sept 2000,          EH
%           free memory by discarding unused variables
%    June 2003,  EH, replaced STOPs by MESSAGEs
%    July 2011, CLP took this file from Healpix 2.01 distribution and
%               translated to Matlab
%

% -----------------------------------------------------------------------------
%
%  Copyright (C) 1997-2005  Krzysztof M. Gorski, Eric Hivon, Anthony J. Banday
%
%
%
%
%
%  This file is part of HEALPix.
%
%  HEALPix is free software% you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation% either version 2 of the License, or
%  (at your option) any later version.
%
%  HEALPix is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY% without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with HEALPix% if not, write to the Free Software
%  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
%
%  For more information about HEALPix see http://healpix.jpl.nasa.gov
%
% -----------------------------------------------------------------------------

if(length(nside)>1)
  error('nside should be a scalar');
end

npix = nside2npix(nside);

if(~exist('ipix'))
  ipix=[0:npix-1]';
end

% force ipix to be 1-D
array_shape = size(ipix);
ipix = ipix(:);

if(any(round(ipix) ~= ipix))
  error('Pixel numbers must be integer');
end

% coordinate of the lowest corner of each face
jrll = [2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4]; % in unit of nside
jpll = [1, 3, 5, 7, 0, 2, 4, 6, 1, 3, 5, 7]; % in unit of nside/2

nside = double(nside);
nl2 = 2*nside;
nl3 = 3*nside;
nl4 = 4*nside;
ncap = nl2*(nside-1);
nsup = nl2*(5*nside+1);
npface = nside * nside;
np = length(ipix);
fn = double(nside);
fact1 = 1/(3*fn*fn);
fact2 = 2/(3*fn);
piover2 = pi/2;

min_pix = min(ipix);
max_pix = max(ipix);
if(min_pix<0|max_pix>npix-1)
  error('pixel values out of range');
end

% initiates the array for the pixel number -> (x,y) mapping
[pix2x,pix2y]=init_pix2xy;

% finds the face, and the number in the face
face_num = floor(ipix/npface); % face number in {0,11}
ipf = mod(ipix,npface);        % pixel number in the face {0,npface-1}

%     finds the x,y on the face (starting from the lowest corner)
%     from the pixel number
ip_low = mod(ipf,1024);         % content of the last 10 bits
ip_trunc = floor(ipf./1024);    % truncation of the last 10 bits
ip_med = mod(ip_trunc,1024);    % content of the next 10 bits
ip_hi  = floor(ip_trunc./1024); % content of the high weight 10 bits

%   computes (x,y) coordinates on the face
ix = 1024*pix2x(ip_hi+1) + 32*pix2x(ip_med+1) + pix2x(ip_low+1);
iy = 1024*pix2y(ip_hi+1) + 32*pix2y(ip_med+1) + pix2y(ip_low+1);
  
%     transforms this in (horizontal, vertical) coordinates
jrt = ix + iy;                 % 'vertical' in {0,2*(nside-1)}
jpt = ix - iy;                 % 'horizontal' in {-nside+1,nside-1}

%     computes the z coordinate on the sphere
jr =  jrll(face_num+1)*nside - jrt - 1; % ring number in {1,4*nside-1}

nr     = double(np);
kshift = double(np);
theta  = double(np);

% equatorial region
pix_eqt = find( jr>=nside & jr<=nl3);
if(length(pix_eqt)>0)
  nr(pix_eqt)     = nside;   % equatorial region 
  theta(pix_eqt)  = acos((2*nside-jr(pix_eqt))*fact2);
  kshift(pix_eqt) = mod(jr(pix_eqt) - nside,2);
end

% north pole
pix_npl = find(jr<nside);
if(length(pix_npl)>0)
  nr(pix_npl)     = jr(pix_npl);
  theta(pix_npl)  = acos(+1 - nr(pix_npl).^2 * fact1);
  kshift(pix_npl) = 0;
end

% south pole
pix_spl = find(jr>nl3);
if(length(pix_eqt)+length(pix_npl)+length(pix_spl) ~= np)
  error('pixels nums dont add up');
end
if(length(pix_spl)>0)
  nr(pix_spl)     = nl4 - jr(pix_spl);
  theta(pix_spl)  = acos(-1 + nr(pix_spl).^2 * fact1);
  kshift(pix_spl) = 0;
end

%     computes the phi coordinate on the sphere, in [0,2Pi]
jp = (jpll(face_num+1).*nr + jpt + 1 + kshift)/2; % 'phi' number in the ring in {1,4*nr}
jp = jp - nl4 .* (jp > nl4);
jp = jp + nl4 .* (jp < 1);

phi  = double(np);
phi = (jp - (kshift+1)*0.5) .* (piover2 ./ nr);

% make output same shape as input
theta = reshape(theta,array_shape);
phi   = reshape(phi  ,array_shape);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [pix2x,pix2y]=init_pix2xy()

pix2x = double(1024);
pix2y = double(1024);

for kpix = 0:1023 % pixel number
  jpix = kpix;
  ix = 0;
  iy = 0;
  ip = 1;             % bit position (in x and y)
  while (jpix>0)      % go through all the bits
    id = mod(jpix,2); % bit value (in kpix), goes in ix
    jpix = floor(jpix/2);
    ix = id*ip+ix;
    
    id = mod(jpix,2); % bit value (in kpix), goes in iy
    jpix = floor(jpix/2);
    iy = id*ip+iy;
    
    ip = 2*ip;        % next bit (in x and y)
  end
  pix2x(kpix+1) = ix;         % in 0,31
  pix2y(kpix+1) = iy;         % in 0,31
end

return

%=======================================================================
% The permission to use and copy this software and its documentation, 
% without fee or royalty is limited to non-commercial purposes related to 
% Microwave Anisotropy Probe (MAP) and
% PLANCK Surveyor projects and provided that you agree to comply with
% the following copyright notice and statements,
% and that the same appear on ALL copies of the software and documentation.
%
% An appropriate acknowledgement has to be included in any
% publications based on work where the package has been used
% and a reference to the homepage http://www.tac.dk/~healpix
% should be included
%
% Copyright 1997 by Eric Hivon and Kris Gorski.
%  All rights reserved.
%=======================================================================
