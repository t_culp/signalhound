function alms_name = gen_noise_alms(rlz,nlmax,band,almdir)
% alms_name = gen_noise_alms(rlz,nlmax,band)
%  
%  create noise alms for a given band, 
%  does not rerun, when file already exists
%  alms_name = the filename theses are saved to
%  
%  rlz       : Realiztation counted from 0
%  
%  nlmax     : lmax of alms
%  band      : Band of WMAP/Planck to simulate noise for
%              valid choices are: 'WmapV', 'WmapW', 'WmapK', 'Pl143', 'Pl100'
%  
%              Noise levels of WMAP are derived here:
%              http://bicep.caltech.edu/~spuder/analysis_logbook/analysis/20120801_synfast_wmapnoise/
%              Wmap beam sizes are found here:
%              http://lambda.gsfc.nasa.gov/product/map/current/
%              WMAP nhits maps are here:
%              http://lambda.gsfc.nasa.gov/product/map/dr4/skymap_info.cfm
%              Planck noise level is calculated here:
%              http://bicep.caltech.edu/~spuder/keck_analysis_logbook/analysis/20130502_noise_planck/
%  almdir    : specify directory for output a_lms (leave blank for default)

display('Generating noise alms')

% get the name of the noise alms, 
% if it exists, we are done and just return the name:
alms_name = get_alms_name(rlz,nlmax,'',0,band);
[dir,fn,ext] = fileparts(alms_name);

if exist('almdir','var') && ~isempty(almdir)
  dir=almdir;
  alms_name=fullfile(dir,[fn,ext]);
end

if exist(alms_name,'file')
  disp(sprintf('file %s exists - skipping',alms_name));
  return
end

mkdir(dir)

% which WMAP/Planck band are we making noise for?
% noise level in mK: it is hardcode here and in make_noise_map below, that nlevel is coming in mK.
% if something else needs to be handed in, coding has to be done!
if strcmpi(band,'wmap7v')
  nlevel={[3.137,3.164],'input_maps/wmap7/wmap_band_imap_r9_7yr_V_v4.fits'} %noise level in mK
  beam_in='input_maps/wmap7/bl/bl_v_wmap_7yr.fits';
  beam_out='aux_data/beams/beamfile_20130222_sum.fits';
elseif strcmpi(band,'wmap7w')
  nlevel={[6.549,6.594],'input_maps/wmap7/wmap_band_imap_r9_7yr_W_v4.fits'} %noise level in mK
  beam_in='input_maps/wmap/bl_w_wmap_7yr.fits';
  beam_out=0;
elseif strcmpi(band,'wmap7k')
  nlevel={[1.437,1.456],'input_maps/wmap7/wmap_band_imap_r9_7yr_K_v4.fits'} %noise level in mK
  %nlevel={[1.437*1d3,1.456*1d3],'input_maps/wmap/wmap_band_imap_r9_7yr_K_v4.fits'} %A hack for making noise maps
  beam_in=52.8;
  beam_out=52.8;
elseif strcmpi(band,'pl143')
  %from here:
  %http://www.sciops.esa.int/wikiSI/planckpla/index.php?title=Effective_Beams&instance=Planck_Public_PLA
  nlevel={[.777,.777], 'input_maps/planck/planck_maps/HFI_SkyMap_143_2048_R1.10_nominal.fits'} %noise level in mK
  beam_in='input_maps/planck/planck_beam/b_l_143GHz.fits';
  beam_out=0;
elseif strcmpi(band,'pl100')
  %from here:
  %http://www.sciops.esa.int/wikiSI/planckpla/index.php?title=Effective_Beams&instance=Planck_Public_PLA
  nlevel={[1.567,1.567], 'input_maps/planck/planck_maps/HFI_SkyMap_100_2048_R1.10_nominal.fits'} %noise level in mK
  beam_in='input_maps/planck/planck_beam/b_l_100GHz.fits';
  beam_out=0;
elseif strcmpi(band,'wmap9k')
  nlevel={[1.429,1.435],'input_maps/wmap9/wmap_band_imap_r9_9yr_K_v5.fits'};
  beam_in=0;
  beam_out=0;
elseif strcmpi(band,'wmap9ka')
  nlevel={[1.466,1.472],'input_maps/wmap9/wmap_band_imap_r9_9yr_Ka_v5.fits'};
  beam_in=0;
  beam_out=0;
elseif strcmpi(band,'wmap9kb')
  nlevel={[1.429,1.435],'input_maps/wmap9/wmap_band_iqumap_r9_9yr_K_v5.fits'};
  beam_in='input_maps/wmap9/bl/bl_k_wmap_9yr.fits';
  beam_out='aux_data/beams/beamfile_20130222_sum.fits';
elseif strcmpi(band,'wmap9kab')
  nlevel={[1.466,1.472],'input_maps/wmap9/wmap_band_iqumap_r9_9yr_Ka_v5.fits'};
  beam_in='input_maps/wmap9/bl/bl_k_wmap_9yr.fits';
  beam_out='aux_data/beams/beamfile_20130222_sum.fits';
elseif strcmp(band, 'pl353')
  nlevel={'input_maps/planck_pr2/HFI_SkyMap_353_2048_R2.00_full.fits'};
  beam_in='input_maps/planck/planck_beam/b_l_353GHz.fits';
  beam_out=0;
elseif strcmpi(band, 'pl217')
  % Numbers from Stefan's post:
  % http://bicep.rc.fas.harvard.edu/bkcmb/analysis_logbook/analysis/20140602_PlanckNoise/PlanckNoise.html
  nlevel={[1.109,1.109],'input_maps/planck/planck_maps/HFI_SkyMap_217_2048_R1.10_nominal.fits'} %noise level in mK
  beam_in='input_maps/planck/planck_beam/b_l_217GHz.fits';
  beam_out=0;
else
  error('Unknown band in gen_noise_alms')
end


mkdir('synfast_tmp')
%-----------------------------------%
%1.Make a noise map from measured noise (wmap_noise_level.m)
%-----------------------------------%
map_noise_withbeam=['synfast_tmp/map_noise_withbeam' gen_stamp() '.fits'];
make_noise_map(rlz, nlevel, map_noise_withbeam)

%-----------------------------------%
%2.make noise alms from noise map
%-----------------------------------%
alms_noise_withbeam=['synfast_tmp/alms_noise_withbeam' gen_stamp() '.fits'];
run_anafast(map_noise_withbeam, alms_noise_withbeam, nlmax)

%-----------------------------------%
%3.convert noise alms from G to C and correct for WMAP beam:
%-----------------------------------%
run_alteralm(alms_noise_withbeam,alms_name,beam_in,beam_out,1);  

% no debug mode: remove tmp files
if isempty(whos('global','synfast_debug'))
  system(['rm ' map_noise_withbeam]);
  system(['rm ' alms_noise_withbeam]);
end

return

function make_noise_map(rlz, nlevel, out_file)
% function make_noise_map(rlz, nlevel, out_file)
% makes a noise realization given the noise level and nhits map handed in over nlevel
%  
% rlz   : to fix the random seed, counted from 0

display('making noise map...')

if ~isnumeric(rlz)
  rlz = str2num(rlz);
end
%get seed for this sim
seeds=get_seedlist();
seed = seeds(rlz+1);

%figure out what information the variable nlevel
%and the fits map contain, and
%decide how we will use it to construct noise maps

if  length(nlevel)<2 
  if isnumeric(nlevel{1})
    %in the zeroth case we dont have an input map, just make white noise
    nmap_opt=0;
   else
    info=fitsinfo(nlevel{1});
   end
 else
  %read in synfast map for size, hdr etc
  %  s_map=fitsread(signal_map, 'BinTable');
  info=fitsinfo(nlevel{2});  
end

%figure out if there is polarization
n=find(strcmp(info.BinaryTable(1).Keywords(:,2),'Q_STOKES'));
nn=find(strcmp(info.BinaryTable(1).Keywords(:,2),'Q_POLARISATION'));
nnn=[n,nn];

if isempty(nnn)
  %in the first case we don't have nhits map for Q/U, so we extrapolate
  %from the I nhits map assuming the noise is sqrt(2) higher
  nmap_opt=1;
else 
  %in the second case we have a Q,U map, and we use the n hits map
  %to make the T noise map and the QU cov matrix to make
  %the polarization noise
  if isnumeric(nlevel{1}) && length(nlevel)==2 
  nmap_opt=2;
  if(~is_this_wmap(nlevel{2}))
   error('only wmap maps can be treated this way, try case 3 instead')
  end
  else
    %in the third case we only have an input map, use the cov for T,Q,U
    nmap_opt=3;
  end
end



switch nmap_opt
  
  case 0
    % we need an input map to get the size noise map to generate
  
  case 1
    % if nlevel is a cell array, noise = nlevel{1}/sqrt(Nhits), where Nhits is contained in
    % the fits file specified in nlevel{2} in the second, N_OBS field
    disp('scaling noise by N_hits map');
    nhitsmap=read_fits_map(nlevel{2});
    
    sigma0=nlevel{1};
    nhits=nhitsmap.map(:,2);
    
    nlevelT=sigma0(1)./sqrt(nhits);
    nlevelQU=sigma0(2)./sqrt(nhits);
    
    noise_map=nlevelT;
    noise_mapQU=nlevelQU;
    len=size(noise_map);
    
    % convert here mK -> K
    noise_map=noise_map*1d-3;
    noise_mapQU=noise_mapQU*1d-3; 
    
    % gen rand number, multiply by noise_map
    randn('seed', seed)
    noiseT=randn(len).*noise_map;
    randn('seed', seed+1)
    noiseQ=randn(len).*noise_mapQU.*sqrt(2);  %Q and U maps indiviudally are sqrt(2) higher 
    randn('seed', seed+2)
    noiseU=randn(len).*noise_mapQU.*sqrt(2);
    
  case 2
 
    %Temperature is done the same way as case 1
    nhitsmap=read_fits_map(nlevel{2});
    sigma0=nlevel{1};
    nlevel=sigma0(1)./sqrt(nhitsmap.map(:,2));
    randn('seed', seed)
    noiseT=randn(size(nlevel)).*nlevel;

    
    % But for Polarization use the cov matrix
    nhitsmap=read_fits_map(nlevel{2},2);
    randn('seed', seed+1)
    qq=nhitsmap.map(:,2);
    qu=nhitsmap.map(:,3);
    uu=nhitsmap.map(:,4);
    noiseQ=zeros(size(qq));
    noiseU=zeros(size(qq));
    
    for k=1:numel(qq)
      x=mvnrnd([0,0],inv([qq(k),qu(k);qu(k),uu(k)]));
      noiseQ(k)=x(1);
      noiseU(k)=x(2);
    end
    
    % mK -> K and Q/U maps individuall are sqrt(2) higher
    noiseT=noiseT*1e-3;
    noiseQ=noiseQ*1e-3*sqrt(2);
    noiseU=noiseU*1e-3*sqrt(2);
    
  case 3  
    
    % Temperature and Polarization use the cov matrix
    infor=fitsinfo(nlevel{1});
    if numel(infor.BinaryTable)==1
      nhitsmap=read_fits_map(nlevel{1});
      ii=nhitsmap.map(:,5);
      qq=nhitsmap.map(:,8);
      qu=nhitsmap.map(:,9);
      uu=nhitsmap.map(:,10);
    else
      nhitsmap=read_fits_map(nlevel{1},2);
      ii=nhitsmap.map(:,1);
      qq=nhitsmap.map(:,2);
      qu=nhitsmap.map(:,3);
      uu=nhitsmap.map(:,4);
    end
    
    %determine whether the units are in uK or in counts
    %and then set factor to convert to K
    if(any(strcmp(nhitsmap.units, 'counts')))
      factor=1d-3;
    else
      factor=1d-6;
    end
    
    noiseT=zeros(size(ii));
    noiseQ=zeros(size(qq));
    noiseU=zeros(size(uu));
    
    %generate unit norm random vectors from seeds
    randn('seed', seed)
    noiseT=randn(size(noiseT));
    randn('seed', seed+1)
    noiseQ=randn(size(noiseQ)); 
    randn('seed', seed+2)
    noiseU=randn(size(noiseU));   
   
    
    %if this is a wmap map, then the units are in nhits counts
    % and we need to find the inverse
    if is_this_wmap(nlevel{1})
     %inverse of nhits counts gives cov
     tt=1./ii;
     detc=1./(qq.*uu-qu.^2);
     qqo=qq;
     qq=detc.*uu;
     qu=detc.*(-qu);
     uu=detc.*qqo;
    end

    %cholesky decomposition 
    tt=sqrt(ii);
    a=sqrt(qq);
    b=qu./a;
    c=sqrt(uu-b.^2);
        
    %multiply random vector by cholesky decompostition
    noiseT=tt.*noiseT;
    noiseQ=a.*noiseQ+b.*noiseU;
    noiseU=b.*noiseU+c.*noiseU;

    %convert units to K and Q/U maps
    noiseT=noiseT*factor;
    noiseQ=noiseQ*factor;
    noiseU=noiseU*factor;
    
end

% convert nested to ring
if strcmp(nhitsmap.ordering,'NESTED')
  [th,ph]=pix2ang_nest(nhitsmap.nside,nhitsmap.pixel);
  pix=ang2pix_ring(nhitsmap.nside,th,ph);
  [dum,ind]=sort(pix);
  noiseT=noiseT(ind);
  noiseQ=noiseQ(ind);
  noiseU=noiseU(ind);
end

% save
disp('saving noise map...');

hmap.map(:,1)=noiseT;
hmap.map(:,2)=noiseQ;
hmap.map(:,3)=noiseU;
hmap.units={'K','K','K'};
hmap.type={'TEMPERATURE','Q-POLARIZATION','U-POLARIZATION'};
hmap.coordsys='G';
hmap.nside=nhitsmap.nside;
hmap.ordering='RING';
hmap.polcconv='COSMO';
hmap.pixel=nhitsmap.pixel;
hmap.fwhm='UNKNOWN';

write_fits_map(out_file,hmap);

return

function wmap=is_this_wmap(file_name)

%default
wmap=0;

%get the fits header
i=fitsinfo(file_name);

%does the fits file tell us what experiment it is?
n=find(strcmp(i.PrimaryData.Keywords(:,1),'TELESCOP'));
if ~isempty(n)
  if strcmp(i.PrimaryData.Keywords(n,2),'WMAP');
  wmap=1;
  else 
  wmap=0;
end
end

%does the file tell us it is a planck freq?
n=find(strcmp(i.PrimaryData.Keywords(:,1),'FREQ'));
if ~isempty(n)
  freq=i.PrimaryData.Keywords(n,2);
  if strcmp(freq,'353') || strcmp(freq,'217') || strcmp(freq,'143')
  wmap=0;
  end
end

return
