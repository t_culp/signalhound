function dust_make_cl()
% function dust_make_cl()
% makes input cls for synfast for the dust model (simtype3)
 
% synfast does not accept all zero input spectra. Use a multiplicative
% factor to render unimportant spectra (0-like) from actual spectra
e = 1e-40;

% Construct the dust model using the multicomponent code.
% First the parameters of the model:
% pure Dust with:
%  Ad = 3.75 muK^2 at l=80 and 353 GHz
%  beta_dust 1.6
%  alpha_dust -0.4
%  E/B ratio for dust 2
%  T_greybody 19.6 K
param =  [0, 0, 0, 3.75, 0, 0, 1.6, -0.4, 0, 2, 0, 19.6, 0, 0];
likeopt.opt.expt{1} = 'K95';
likeopt.opt.expt{2} = 'B2_150';
likeopt.opt.expt{3} = 'K220';
likeopt = like_read_theory(likeopt)
likeopt.opt.fields = {'T', 'E', 'B'};
likeopt = like_read_bandpass(likeopt)

% fetch the model from the multicomponent framework
[ell, cmp] = model_rms(param, likeopt);

% run some basic checks:
% the EE/BB ratio: 
nanmean((cmp(:,2,3)./cmp(:,3,3)).^2)
% >> 2

% the ratio between 95 and 150:
nanmean((cmp(:,3,3)./cmp(:,6,3)))
% >> 0.3807

% In the camb files C_l are store instead of Cs_l. Also the units should be K^2 instead of muK^2.
% Model_rms gives the sqrt of Cs_ls. So
%ell = repmat(ell,[1,6,4]);
ell = repmat(ell,[1,9,4]);
cmp = cmp.^2 * 2 * pi ./ ell ./ (ell+1) / 1e6^2;

% the modelrms does not included T, create it at 10% polarization fraction
% one could create T from E+B using a fixed pfrac. However, in the current simulation
% setup, we don't want to include a T dust signal, as the LCDM sims already contain
% the measured T sky from the closest Planck freqs. 
% The lines below are exectuted, but then the spectra are effectively set to zero:
pfrac = 0.1;
cmp(:,1,3) = (cmp(:,2,3)+cmp(:,3,3))/pfrac;
cmp(:,4,3) = (cmp(:,5,3)+cmp(:,6,3))/pfrac;
cmp(:,7,3) = (cmp(:,8,3)+cmp(:,9,3))/pfrac;
if(1)
  cmp(:,1,3) = cmp(:,1,3) * e;
  cmp(:,4,3) = cmp(:,4,3) * e;
  cmp(:,7,3) = cmp(:,7,3) * e;
end
  
% A nanmean is need above since the values for ell=0 are a mixture of Inf, NaN and 0. 
% Usually the CAMB ell=0,1 values are 0. Do this here as well:
cmp([1,2],:,:) = 0;

% now write into a fits file. Get the official cl as jig:
% this was already used in model_rms to fetch the ell range and LCDM
% component in cmp.
fn = [likeopt.opt.camb_dir,'/',likeopt.opt.camb_unlensed]
fi = fitsinfo(fn)
temp=(fitsread(fn,'table'));

% check that the units in the camb file and cmp are really the same:
r  = cmp(:,1,1)./temp{1};
nanmean(r(r>0))
% >> 1.0036

% the headers for the fit files. 
hdr=fi.PrimaryData.Keywords; xhdr=fi.AsciiTable.Keywords;

% have a look at those. We'll just explicitly rewrite them here:

hdr={...
    'SIMPLE',    'T',   ' file does conform to FITS standard             ';
    'BITPIX',    [8],   ' number of bits per data pixel                  ';
    'NAXIS',     [0],   ' number of data axes                            ';
    'EXTEND',    'T',   ' FITS dataset may contain extensions            ';
    'COMMENT',    '',   '  FITS (Flexible Image Transport System) format is defined in ''Astronomy';
    'COMMENT',    '',   '  and Astrophysics'', volume 376, page 359; bibcode: 2001A&A...376..359H ';
    'DATE',     date,   '';
    'END',        '',   '';
};


xhdr0={...
    'XTENSION',   'TABLE',                ' ASCII table extension                          ';
    'BITPIX',     [                 8],   ' 8-bit ASCII characters                         ';
    'NAXIS',      [                 2],   ' 2-dimensional ASCII table                      ';
    'NAXIS1',     [                63],   ' width of table in characters                   ';
    'NAXIS2',     [       size(ell,1)],   ' number of rows in table                        ';
    'PCOUNT',     [                 0],   ' no group parameters (required keyword)         ';
    'GCOUNT',     [                 1],   ' one data group (required keyword)              ';
    'TFIELDS',    [                 4],   ' number of fields in each row                   ';
    'COMMENT',                      '',   '------------------------------------------------';
    'COMMENT',                      '',   'CMB power spectrum C(l) keywords                ';
    'COMMENT',                      '',   '------------------------------------------------';
    'EXTNAME',    'COMPUTED POWER SPECTRUM', '';
    'COMMENT',                      '',   'POWER SPECTRUM : C(l)                           ';
    'COMMENT',                      '',   '';
    'CREATOR',    'FGPOWERLAW',           ' Software creating the FITS file                ';
    'VERSION',    'Mar 2015',             ' Version of the simulation software             ';
    'POLAR',      'T',                    ' Polarisation included (True/False)             ';
    'POLNORM',    'CMBFAST',              ' Uses E-B conventions                           ';
    'COMMENT',                      '',   '';
    'COMMENT',                      '',   '';
    'TBCOL1',     [                 1],   ' beginning column of field   1                  ';
    'TFORM1',     'E15.7',                '';
    'TTYPE1',     'TEMPERATURE',          ' Temperature C(l)                               ';
    'TUNIT1',     'K^2',                  ' physical unit of field                         ';
    'COMMENT',                      '',   '';
    'TBCOL2',     [                17],   ' beginning column of field   2                  ';
    'TFORM2',     'E15.7',                '';
    'TTYPE2',     'E-mode C_l',           ' ELECTRIC polarisation C(l)                     ';
    'TUNIT2',     'K^2',                  ' physical unit of field                         ';
    'COMMENT',                      '',   '';
    'TBCOL3',     [                33],   ' beginning column of field   3                  ';
    'TFORM3',     'E15.7',                '';
    'TTYPE3',     'B-mode C_l',           ' MAGNETIC polarisation C(l)                     ';
    'TUNIT3',     'K^2',                  ' physical unit of field                         ';
    'COMMENT',                      '',   '';
    'TBCOL4',     [                49],   ' beginning column of field   4                  ';
    'TFORM4',     'E15.7',                '';
    'TTYPE4',     'E-T cross corr.',      ' Gradient-Temperature cross terms               ';
    'TUNIT4',     'K^2',                  ' physical unit of field                         ';
    'COMMENT',                      '',   '';
    'COMMENT',                      '',   '------------------------------------------------';
    'COMMENT',                      '',   'Dust Model                                      ';
    'COMMENT',                      '',   '------------------------------------------------';
};

%  Ad = 3.75 muK^2 at l=80 and 353 GHz
%  beta_dust 1.6
%  alpha_dust -0.42
%  E/B ratio for dust 2
%  T_greybody 19.6 K
param =  [0, 0, 0, 3.75, 0, 0, 1.6, -0.4, 0, 2, 0, 19.6, 0, 0];

xhdr1={...
    'A_d',       [param(4)],                 ['muK^2 at l=',num2str(likeopt.opt.fg_pivot),'and ',num2str(likeopt.opt.dust_freq),' GHz                        '];
    'fg_pivot',  [likeopt.opt.fg_pivot],      'see multicomponent likeopt                      ';
    'dust_fre',  [likeopt.opt.dust_freq],     'see multicomponent likeopt                      ';
    'beta_dus',  [param(7)],                  'see multicomponent parameters                   ';
    'alpha_du',  [param(8)],                  'see multicomponent parameters                   ';
    'EE/BB_du',  [param(10)],                 'see multicomponent parameters                   ';
    'T_greybo',  [param(12)],                 'see multicomponent parameters                   ';
    'END',       '',                          '';
};
xhdr=[xhdr0;xhdr1];

outfile{1} = 'aux_data/official_cl/dust_95_3p75.fits';
outfile{2} = 'aux_data/official_cl/dust_150_3p75.fits';
outfile{3} = 'aux_data/official_cl/dust_220_3p75.fits';

% this is TT,EE,BB and TE=0 for 95 GHz:
% the TE cross is in the fourth column, effectively set that to zero...
temp{1} = cmp(:,1,3);
temp{2} = cmp(:,2,3);
temp{3} = cmp(:,3,3);
% temp{4} is TE - set to zero:
temp{4}= sqrt(cmp(:,1,3).*cmp(:,2,3)) * e;
%fitswrite_table(temp, hdr, xhdr, outfile{1})

% this is TT,EE,BB and TE=0 for 150 GHz:
temp{1} = cmp(:,4,3);
temp{2} = cmp(:,5,3);
temp{3} = cmp(:,6,3); 
% temp{4} is TE - set to zero:
temp{4}= sqrt(cmp(:,4,3).*cmp(:,5,3)) * e;
%fitswrite_table(temp, hdr, xhdr, outfile{2})

% this is TT,EE,BB and TE=0 for 220 GHz:
temp{1} = cmp(:,7,3);
temp{2} = cmp(:,8,3);
temp{3} = cmp(:,9,3); 
% temp{4} is TE - set to zero:
temp{4}= sqrt(cmp(:,7,3).*cmp(:,8,3)) * e;
keyboard
fitswrite_table(temp, hdr, xhdr, outfile{3})

return
