function ipix=ang2pix_nest(nside,theta,phi)
% ipix=ang2pix_nest(nside,theta,phi)
%
%        renders the NESTED scheme pixel number Ipix for a pixel which, given the
%        map resolution parameter Nside, contains the point on the sphere
%        at angular coordinates Theta and Phi
%
% INPUT
%    Nside     : determines the resolution (Npix = 12* Nside^2)
%       should be a power of 2 (not tested)
%	SCALAR
%    Theta : angle (along meridian), in [0,Pi], theta=0 : north pole,
%	can be an ARRAY
%    Phi   : angle (along parallel), in [0,2*Pi]
%	can be an ARRAY of same size as Theta
%
% OUTPUT
%    Ipix  : pixel number in the NESTED scheme HEALPIX pixelisation in [0,Npix-1]
%	is an ARRAY of same size as Theta and Phi
%
% SUBROUTINE
%    nside2npix
%
% HISTORY
%    June-October 1997,  Eric Hivon & Kris Gorski, TAC
%                  original ang_pix_nest
%    Feb 1999,           Eric Hivon, Caltech
%                  renamed ang2pix_nest
%    March 1999,  correction of a bug when nside is not LONG
%    Sept 2000,          EH
%           free memory by discarding unused variables
%    June 2003,  EH, replaced STOPs by MESSAGEs
%    Aug  2004,  EH, use !PI as theta upper-bound instead of !DPI
%    Mar  2006,  CLP, took this file from Healpix 2.01 distribution and
%              translated to Matlab
%
%-
%*****************************************************************************

% -----------------------------------------------------------------------------
%
%  Copyright (C) 1997-2005  Krzysztof M. Gorski, Eric Hivon, Anthony J. Banday
%
%
%
%
%
%  This file is part of HEALPix.
%
%  HEALPix is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
%
%  HEALPix is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with HEALPix; if not, write to the Free Software
%  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
%
%  For more information about HEALPix see http://healpix.jpl.nasa.gov
%
% -----------------------------------------------------------------------------

if(nargin~=3)
  error('Must have 3 input arg');
end

if(length(nside)>1)
  error('nside should be a scalar');
end

npix = nside2npix(nside);

if(any(size(theta)~=size(phi)))
  error('inconsistent theta and phi');
end

if(any(theta<0|theta>pi))
  error('theta out of range');
end

% algorithm below malfunctions for phi outside range -2pi to +2pi -
% force it into range
phi=mod(phi,2*pi);

% force theta/phi to be 1-D
array_shape = size(theta);
theta=theta(:);
phi=phi(:);

np=length(theta);

%------------------------------------------------------------
ns_max = 8192;

for i = 0:127        %for converting x,y into
  j  = i;                      %pixel numbers
  k  = 0;
  ip = 1;
  while(1)
    if (j==0)
      x2pix(i+1) = k;
      break;
    else
      id = mod(j,2);
      j  = floor(j/2);
      k  = ip*id+k;
      ip = ip*4;
    end
  end
end
y2pix = 2 * x2pix;

twopi  = 2*pi;
piover2 = pi/2;
z = cos(theta);
z0 = 2/3;
phi_in = mod(phi,twopi);
phi_in = phi + (phi<=0)*twopi;

tt  = phi_in / piover2; % in [0,4[
phi_in = 0;

%face_num = zeros(np,1);
%ix = zeros(np,1);
%iy = ix;

pix_nan = find(~isfinite(theta) | ~isfinite(phi)); % NaN entries
if(length(pix_nan)>0)

  ix(pix_nan) = 0;
  iy(pix_nan) = 0;
  face_num(pix_nan) = NaN;
  z(pix_nan) = NaN;
end

pix_eqt =find(z<=z0 & z>-z0); % equatorial strip
if(length(pix_eqt)>0)

  %     (the index of edge lines increase when the longitude=phi goes up)
  jp = floor(ns_max*(0.5 + tt(pix_eqt) - z(pix_eqt)*0.75)); % ascend edge line index
  jm = floor(ns_max*(0.5 + tt(pix_eqt) + z(pix_eqt)*0.75)); % descend edge line index

  %     finds the face
  ifp = floor(jp / ns_max);  % in {0,4}
  ifm = floor(jm / ns_max);
  p_np = find(ifp == ifm);
  p_eq = find(ifp < ifm);
  p_sp = find(ifp > ifm);

  if (length(p_np) > 0)
    face_n(p_np) = mod(ifp(p_np),4) + 4;
  end
  if (length(p_eq) > 0)
    face_n(p_eq) = mod(ifp(p_eq),4);
  end
  if (length(p_sp) > 0)
    face_n(p_sp) = mod(ifm(p_sp),4) + 8;
  end

  face_num(pix_eqt) = face_n;
  ix(pix_eqt) = mod(jm,ns_max);
  iy(pix_eqt) = ns_max - mod(jp,ns_max) - 1;
end

pix_pol = find(z>z0 |  z<=-z0); % polar caps
if(length(pix_pol)>0)

  zz = z(pix_pol);
  ntt = min(floor(tt(pix_pol)),3);
  tp = tt(pix_pol) - ntt;
  tmp = sqrt( 3*(1 - abs(z(pix_pol))));  % in ]0,1]

  %     (the index of edge lines increase when distance from the closest pole goes up)
  jp = floor( ns_max * tp       .* tmp ); % line going toward the pole as phi increases
  jm = floor( ns_max * (1 - tp) .* tmp ); % that one goes away of the closest pole
  jp = min(jp,(ns_max-1)); % for points too close to the boundary
  jm = min(jm,(ns_max-1));

  %     finds the face and pixel's (x,y)
  p_np = find(zz > 0);
  p_sp = find(zz < 0);
  if (length(p_np) > 0)
    face_num(pix_pol(p_np)) = ntt(p_np);
    ix(pix_pol(p_np)) = ns_max - jm(p_np) - 1;
    iy(pix_pol(p_np)) = ns_max - jp(p_np) - 1;
  end
  if (length(p_sp) > 0)
    face_num(pix_pol(p_sp)) = ntt(p_sp) + 8;
    ix(pix_pol(p_sp)) = jp(p_sp);
    iy(pix_pol(p_sp)) = jm(p_sp);
  end
end

ix_low = mod(ix,128);
ix_hi  = floor(ix/128);
iy_low = mod(iy,128);
iy_hi  = floor(iy/128);

ipix =  (x2pix(ix_hi+1)+y2pix(iy_hi+1)) * 16384 + (x2pix(ix_low+1)+y2pix(iy_low+1));

ipix = floor(ipix ./ (ns_max/nside).^2);  % in {0, nside**2 - 1}

ipix = ipix + face_num.* nside^2;  % in {0, 12*nside**2 - 1}

% make output same shape as input
ipix = reshape(ipix,array_shape);

return

%=======================================================================
% The permission to use and copy this software and its documentation, 
% without fee or royalty is limited to non-commercial purposes related to 
% Microwave Anisotropy Probe (MAP) and
% PLANCK Surveyor projects and provided that you agree to comply with
% the following copyright notice and statements,
% and that the same appear on ALL copies of the software and documentation.
%
% An appropriate acknowledgement has to be included in any
% publications based on work where the package has been used
% and a reference to the homepage http://www.tac.dk/~healpix
% should be included
%
% Copyright 1997 by Eric Hivon and Kris Gorski.
%  All rights reserved.
%=======================================================================
