function fitswrite_bintable(data_in, info, filename)
% fitswrite_bintable(data_in, info, filename)
%
% Saves a Matlab matrix as a FITS image.
%
% INPUTS
%   data_in     A cell row vector containing the column data vectors (same
%               form as returned by fitsread()).
%
%   info        Structure of FITS header information. See fitsinfo() for
%               a detailed description.
%
%   filename    Output FITS file name.
%
% EXAMPLES
%
%   data = fitsread(filename, 'BinTable');
%   info = fitsinfo(filename);
%   fitswrite_bintable(data, info, 'test.fits');
%
% KNOWN DEFICIENCES
%
%   1. Doesn't support arrays of imaginary numbers.
%   2. Only handles simple 2 dimensional arrays of data.
%
% NOTES
%
%   Original implementation by R. G. Abraham, Institute of Astronomy,
%   Cambridge University <abraham@ast.cam.ac.uk> (i.e. fitswrite_table()),
%   and subsequently modified to support writing Binary Tables based on
%   documentation from
%
%       www.cv.nrao.edu/fits/documents/standards/bintable_aa.ps
%

display(['Writing fits bintable to: ',filename])

% Determine the endianness by checking byte conversions of a couple of
% values against expectations.
%
%   single(-1) = 0xbf 80 00 00  in big-endian archs
%              = 0x00 00 80 bf  in little-endian archs
%
%   int32(32767) = 0x00 00 7f ff in big-endian archs
%                = 0xff 7f 00 00 in little-endian archs
%
if all(typecast(single(-1),  'uint8') == [191 128 000 000]) && ...
   all(typecast(int32(32767),'uint8') == [000 000 127 255])

  isbe = true;
  orderbytes = @(v) v;

elseif all(typecast(swapbytes(single(-1)),  'uint8') == [191 128 000 000]) && ...
       all(typecast(swapbytes(int32(32767)),'uint8') == [000 000 127 255])

  isbe = false;
  orderbytes = @swapbytes;

else
  error('Could not determine endianness of this machine.');
end

%get hdr, xhdr
hdr=info.PrimaryData.Keywords;
infobin=info.BinaryTable;

%make hdr
header_cards=[];
for i=1:size(hdr,1)
  card=make_card(hdr{i,1}, hdr{i,2});
  header_cards=[header_cards; card];
end  

header_record = make_header_record(header_cards);
fid=fopen(filename,'w');
fwrite(fid,header_record','char');


for k=1:length(infobin)
  % Make xhdr
  xhdr=infobin(k).Keywords;  
  header_cards_bin=[];
  for i=1:size(xhdr,1)
    card=make_card(xhdr{i,1}, xhdr{i,2});
    header_cards_bin=[header_cards_bin; card];
  end  

  header_record_bin = make_header_record(header_cards_bin);
  fwrite(fid,header_record_bin','char');

  data=data_in(:,:,k);

  % If data is actually a matrix instead of a cell array of column vectors,
  % transform it for consistency.
  if ~iscell(data)
    datac = cell(1,size(data,2));
    for cc=1:size(data,2)
      datac{cc} = data(:,cc);
    end
    data = datac;
    clear datac;
  end

  % The bulk of the remainder of this loop are machinations required to put
  % the data in the correct format in memory so that only a single file
  % write invocation is required. The naive approach taken by writing every
  % data entry individually is very inefficient and causes the time to save
  % to balloon rapidly.
  %
  % The basic idea is to cast the data columns (which are probably double)
  % to the final on-disk data types (typically single and int32 for synfast
  % maps). Since Matlab arrays must be a homogenous type, we type-cast each
  % column to a matrix of bytes so that we can concatenate different data
  % types together. (We handle swapping bytes from little-endian to
  % big-endian at the same time.) With a single, large byte matrix, we can
  % then simply write the matrix to disk in row-major order with a single
  % efficient write.

  % Data conversion helpers:
  %
  % Note that the full specification allows 'M', 'D', 'C', 'P', 'E', 'J',
  % 'I', 'B', 'L', 'A', and 'X'. We only support 'D', 'E', and 'J' here.
  fits_types = {'D', 'E', 'J'};
  ml_types = {'double', 'single', 'int32'};
  % Build a sscanf string that matches all the fits_types.
  scan_types = ['%[' sprintf('%s', fits_types{:}) ']'];

  % Consider each column of data
  tform = cell(1, size(data,2));
  tform_type = cell(1, size(data,2));
  tform_num = zeros(1, size(data,2));
  for jj=1:size(data,2)
    % Find what type of data the column contains
    loc = strmatch(sprintf('TFORM%i', jj), xhdr(:,1), 'exact');
    if isempty(loc)
      error('Data type for column %d unspecified.', jj);
    end
    tform{jj} = xhdr{loc,2};

    % Translate the FITS specification to a number of elements and the
    % type information. The number can be excluded to imply 1, so we first
    % look for a valid data type as a single character.
    tmp = sscanf(tform{jj}, scan_types);
    if ~isempty(tmp)
      tform_num(jj) = 1;
      tform_type{jj} = char(tmp(1));
    else
      tmp = sscanf(tform{jj}, ['%d' scan_types]);
      if numel(tmp) ~= 2
        error('Unsupported type of data to write in column %d', jj);
      end
      tform_num(jj) = tmp(1);
      tform_type{jj} = char(tmp(2));
    end

    % Cast the data to the correct corresponding type, swapping bytes as well
    % (if necessary).
    col_type = ml_types{strmatch(tform_type{jj}, fits_types)};
    datatmp = cellfun(@(v) orderbytes(cast(v, col_type)), ...
        data(:,jj), 'uniformoutput', false);

    % With guaranteed uniform types, concatenate the cell rows together into
    % a single matrix (if size(data,1) > 1).
    datatmp = cell2mat(datatmp);

    % Typecast reads the array in order and outputs bytes. We then mash it
    % back into a matrix-like form where dimension 1 is still the rows in
    % the column, but dimension 2 are the individual bytes of the data.
    %
    % We also group the output accordinate to the TFORM number specification
    % at this point by "widening" the table to include the contiguous
    % entries.
    sz = size(typecast(datatmp(1), 'uint8'), 2);
    tmp = typecast(datatmp, 'uint8');
    datatmp = reshape(tmp, sz*tform_num(jj), size(datatmp,1)/tform_num(jj))';
    clear tmp;

    % Now overwrite the original column in data with the direct byte values.
    data{1,jj} = datatmp;
  end

  % With all the preprocessing done, just turn the cell matrix into a
  % regular matrix. We explicitly ignore any other rows in the original data
  % since those have already been concatenated in.
  data = cell2mat(data(1,:));

  % Now write to the output file, making sure to transpose since matlab will
  % read in column-major order, but we want to output in row-major order.
  % Also note that the 'b' is still necessary since the individual bytes must
  % be output in big-endian mode.
  fwrite(fid, data', 'uint8', 'b');

  % Finally, we must pad the output to a multiple of 2880 bytes.
  npad = 2880 - rem(prod(size(data)), 2880);
  if npad > 0
    fwrite(fid, zeros(npad, 1, 'uint8'), 'ubit8');
  end

end  %multple bintables

fclose(fid);

return

function card=make_card(keyword,value)
%MAKE_CARD turns a set of strings into a valid FITS card

%Make keyword field 8 bytes long
lk=length(keyword);
if (lk > 8) & (nargin>1)
  error('Keyword must be less than or equal to 8 characters!')
elseif (lk < 8 )
  keyword=[keyword,setstr(ones(1,8-lk)*32)];
end;

%Deal with both straight keyword and keyword/value pair
if (nargin==1 | strcmp(keyword,'END     '))
  %Keyword without a value
  card=keyword;  
else
  %Key/value pair has an equal sign and space at bytes 9 and 10
  card=[keyword,'= '];

  % Now output the value. The FITS standard wants things to start 
  % in different columns depending on what type of data the
  % value holds, according to the following rules:
  %
  % Logical: T or F in column 30
  %
  % Character string: A beginning quote in column 11 and an
  % ending quote between columns 20 and 80.
  %
  % Real part of an integer or floating point number: right 
  % justified, ending in column 30.
  %
  % Imaginary part: right justified, ending in
  % column 50, starting after column 30 (NB. I won't bother 
  % to support an imaginary part in this M-file, and will 
  % let some radio astronomer who needs it add it if they want).

  if isstr(value)
    % Test for logical. If logical it goes in column 30 
    if (length(value)==1) & (strmatch(upper(value),'T') | strmatch(upper(value),'F'))
       card=[card,setstr(ones(1,19)*32),value];  
    else  
      %Value must be a character string. Pad if less than 8
      %characters long.
      lv=length(value);
        if (lv > 70)
       error('Value must be less than 70 characters long!')
        elseif (lv < 10 )
         value=[value,setstr(ones(1,8-lv)*32)];
        end;
      card=[card,'''',value,''''];
    end;  
  else
    %Value must be a number. Convert to a string. Maximum
    %precision is set to 10 digits
    value=num2str(value,10);
    lv=length(value);
  
    %Write this out so it will end on column 30
    card=[card,setstr(ones(1,20-lv)*32),value];  
  end;
end;

%Now pad the output to make it exactly 80 bytes long
card=[card,setstr(ones(1,80-length(card))*32)];

return

function hrec=make_header_record(card_matrix)
[nrow,ncol] = size(card_matrix);
n_blanks = 36 - rem(nrow,36);
blank_line = setstr(ones(1,80)*32);
hrec = [card_matrix; repmat(blank_line,n_blanks,1)];
return
