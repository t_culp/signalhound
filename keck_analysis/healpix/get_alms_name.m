function alms_name = get_alms_name(rlz,nlmax,input_spec,lensed,noise_band,constrained)
%  alms_name = get_alms_name(rlz,nlmax,input_spec,lensed,noise_band, constrained)
%  
%  generate the filename of an alms fits file matching the given parameters
%  
%  examples:
%  alms_name = get_alms_name(rlz,nlmax,input_spec,lensed,'') : signal
%  alms_name = get_alms_name(rlz,nlmax,'',0,'WmapW') : noise

if ~exist('constrained', 'var')
  constrained=0;
end

if ~(strcmp(noise_band,''))
  [dum,type] = fileparts(noise_band);
  alms_name= 'alm_noise';
else
  [dum,type] = fileparts(input_spec);
  if lensed
    alms_name= 'alm_lensed';
  else
    alms_name = 'alm_unlens';
  end
  if ~strcmp(constrained,'')
    alms_name = [alms_name '_c' constrained];
  end  
end
subdir = [type '/'];

alms_name = [alms_name sprintf('_l%04d',nlmax)];
alms_name = [alms_name sprintf('_r%04d',rlz)];

%append the file extension
alms_name = [alms_name '.fits'];
if ~(strcmp(noise_band,''))
  alms_name = ['input_maps/alms/noise/' subdir alms_name];
else
      alms_name = ['input_maps/alms/' subdir alms_name];
end

return
