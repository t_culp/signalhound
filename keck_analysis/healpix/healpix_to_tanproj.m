function map=healpix_to_tanproj(hmap,longc,latc,x,y)
% map=healpix_to_tanproj(hmap,rac,decc,x,y)
%
% Take a healpix map and return tan projection map of an area around
% specified long/lat center
%
% e.g.
% ad=calc_ad(10,256); x=ad.t_val_deg; y=x;
% map=healpix_to_tanproj('/data/quad/healmaps/wmap3/wmap_band_iqumap_r9_3yr_K_v2.fits',83,-47,x,y,1);
% imagesc(x,y,map(:,:,1)); skyplot;

% construct map grid
[x,y]=meshgrid(x,y);

% convert to lat/long grid
[long,lat]=tanproj_to_radec(x,y,longc,latc);

% call healpix_to_latlong to do the rest of the work
map=healpix_to_longlat(hmap,long,lat);

return
