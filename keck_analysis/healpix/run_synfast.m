function run_synfast(rlz, input_spec, in_alms, nside, out_map, out_alms, cut, beam, nlmax, simtype, seed)
%  run_synfast(rlz, input_spec, in_alms, nside, out_map, out_alms, cut, beam, nlmax, simtype, seed)
%  
%  rlz          : will pick the rlzth+1 seed from the seedlist, rlz counts from 0
%  input_spec   : string containing name of spectra file
%  in_alms      : string containing name of input alms file
%                 if [] the input_spec will be used to generate
%                 a set of random alms using rlz/seedlist
%  nside        : nside of the final healpix map
%  out_file     : string containing name of output file
%  cut          : define cut.theta1, etc to only save B2/Keck patch, 
%                 i.e.: cut.theta1=-70; cut.theta2=-45;cut.phi1=-55; cut.phi2=55;
%                 [] to save full sky
%  beam         : size of beam to apply to map, fwhm_arcmin
%                 OR if string, beam window function fits filename
%  nlmax        : maximum l to include
%  simtype      : simul_type parameter of synfast, 6 default
%  seed         : [], usually the seed is generated from the rlz number, this allows to hand in a
%                 seed explicitly.
%
%  all numerical values can be handed in as string or as number
%  
% examples:
% run_synfast(1,'aux_data/official_cl/camb_planck2013_r0p1.fits',[],2048,'out_map.fits',cut,31.22,1024,6);

display('Running synfast')

if ~exist('simtype','var')
  simtype=[];
end

if ~exist('seed','var')
  seed = [];
end

if isempty(simtype)
  simtype=6;
end

if ~isnumeric(simtype)
  simtype = str2num(simtype);
end

if ~isnumeric(nlmax)
  nlmax = str2num(nlmax);
end

if ~exist('synfast_tmp','dir')
  mkdir('synfast_tmp');
end
if ~isnumeric(rlz)
  rlz = str2num(rlz);
end

if exist(out_map,'file')
  cmd=['rm -f ' out_map];
  system_safe(cmd);
end
[dir,dum] = fileparts(out_map);
if ~exist(dir, 'dir')
  mkdir(dir);
end

if(~isempty(out_alms))
  if exist(out_alms,'file')
    cmd=['rm -f ' out_alms];
  system_safe(cmd);
  end
  [dir,dum] = fileparts(out_alms);
  if ~exist(dir, 'dir')
    mkdir(dir);
  end
end

if iscell(beam)
  beam=beam{1};
  disp('beam was input as cell, using first entry for beam smoothing')
end

%get seed for this sim if not handen in
if isempty(seed)
  seeds=get_seedlist();
  seed = seeds(rlz+1);
end

%write synfast .in file 
stamp = gen_stamp();
synfastinfile = ['synfast_tmp/synfast_' stamp '.in'];
h=fopen(synfastinfile,'wt');

if ~isempty(input_spec)
fprintf(h, ['infile = ' input_spec '\n']);
end

if ~isempty(in_alms)
  fprintf(h, ['almsfile = ' in_alms '\n']);
end

fprintf(h, ['simul_type = ' num2str(simtype) '\n']);
fprintf(h, ['nsmax = ' num2str(nside) '\n']);
fprintf(h, ['nlmax = ' num2str(nlmax) '\n']);

if isempty(in_alms)
  fprintf(h, ['iseed =' num2str(seed)  '\n']);
end

% oh man, ever more complicated: ofen '0' is handed in to specify
% fwhm=0 to avoid beam smoothing, this, however, needs to be a number
% and not a string at this point:
try
  beam2 = str2num(beam);
  if ~isempty(beam2)
    beam = beam2;
  end
catch
  % just leave it the string or cell it is.
end
if ~isstr(beam)
  fprintf(h, ['fwhm_arcmin = ' num2str(beam) '\n']);
else
  fprintf(h, ['beam_file = ' beam '\n']);
end

if(~isempty(cut)) 
  fprintf(h, ['theta_cut1 = ' num2str(cut.theta1) '\n']);
  fprintf(h, ['theta_cut2 = ' num2str(cut.theta2) '\n']);
  fprintf(h, ['phi_cut1 = ' num2str(cut.phi1) '\n']);
  fprintf(h, ['phi_cut2 = ' num2str(cut.phi2) '\n']);
end  

fprintf(h,'windowfile = none \n');
fprintf(h, 'apply_windows = t \n');
fprintf(h, ['outfile = ' out_map '\n']);

if(~isempty(out_alms))
  fprintf(h, ['outfile_alms = ' out_alms '\n']);
end

fclose(h);

system_safe(['synfast ' synfastinfile]);

% no debug mode: remove tmp files
if isempty(whos('global','synfast_debug'))
  system_safe(['rm -f ' synfastinfile]);
end
 
return
