function plot_healpix_map(hmap,col,sf)
% plot_healpix_map(hmap,col,sf)
%
% Plot the raw contents of healpix map as a set of "fish scales"
%
% e.g. to plot the third (U) column of WMAP K-band cut down map:
% plot_healpix_map('input_maps/wmap/wmap_band_iqumap_r9_9yr_K_v5_nside0512_nlmax1500_bB2bbns.fits',3)

if ~exist('col','var')
  col=[];
end
if ~exist('sf','var')
  sf=[];
end
if(isempty(col))
  col=1;
end
if(isempty(sf))
  sf=10; % works for BICEP field and small plot
end

if(~isstruct(hmap))
  hmap=read_fits_map(hmap);
end

% find the pixel center locations
switch hmap.ordering
 case 'RING'
  [plat,plong]=pix2ang_ring(hmap.nside,hmap.pixel);
 case 'NESTED'
  [plat,plong]=pix2ang_nest(hmap.nside,hmap.pixel);
end

% center on RA=0 as that is convenient for BICEP/Keck field
plong(plong>pi)=plong(plong>pi)-2*pi;
% convert from Healpix 0<theta<2pi NP->SP to -pi/2<theta<pi/2 SP->NP
plat=pi/2-plat;

plong=plong*180/pi;
plat=plat*180/pi;

% plot a colored disk at each location
scatter(plong,plat,sf,hmap.map(:,col),'o','fill');
box on
axis tight
xlabel('RA (deg)'); ylabel('Dec (deg)');

% make x/y scaling approx correct
xl=xlim; yl=ylim;
xdos=diff(xlim)*cosd(mean(ylim)); ydos=diff(ylim);
set(gca,'PlotBoxAspectRatio',[xdos,ydos,1])

% IAU axis directions
set(gca,'XDir','reverse');

%colorbar

return
