function [interpMaps,h] = healpix_interp_beamwidth(targetBeamWidths, hmaps)
%  [interpMap, suppPoints] = healpix_interp_beamwidth(targetBeamWidth, input_maps)
% 
%  Given an array of input maps of different beamwidths, linearly interpolate between
%  maps to produce a smoothed map of intermediate beamsize.
%
%  targetBeamWidth = FWHM of desired output map in degrees, can be a list
%  hmaps = array of healpix maps as returned by read_fits_map. All maps must be same
%          size. 
%        = suppPoints output of a previous call to healpix_interp_beamwidth (faster)
%
% ex. hmap(1)=read_fits_map('map_fwhm30p0.fits');
%     hmap(2)=read_fits_map('map_fwhm31p0.fits');
%     [map,supp]=healpix_interp_beamwidth(0.5,hmap);
%     map2=healpix_interp_beamwidth(0.52,supp);


if isfield(hmaps,'pixel')
  % data must be massaged into correct form
  h.suppPoints=[];
  h.fwhms = [hmaps(1,:).fwhm];
  h.mapSize = size(hmaps(1).map);
  h.mapTempl=hmaps(:,1);
  h.mapTempl=rmfield(h.mapTempl,'map');
  h.mapTempl.map = [];
  h.mapTempl.filename='none';
  
  for ii = 1:length(hmaps) % loop over the beamwidths
    h.suppPoints(ii,:,:) = hmaps(ii).map;
  end
  sz=size(h.suppPoints);
  % e.g. 2x1000x18 -> 2x18000
  h.suppPoints=reshape(h.suppPoints,[sz(1),sz(2)*sz(3)]);
  clear hmaps;  
else
  % input data already in correct form
  h=hmaps;
end

%allow for a list of targetBeamWidths
for j=1:length(targetBeamWidths)
  targetBeamWidth=targetBeamWidths(j);
  % most simple case of only two input maps calculated here right away:
  if (length(h.fwhms)==2)
    intMap = h.suppPoints(1,:) + (h.suppPoints(2,:)-h.suppPoints(1,:)) / (h.fwhms(2)-h.fwhms(1)) * (targetBeamWidth-h.fwhms(1));
  else % the more involved cases handle with interp1,interp1q:
    if (targetBeamWidth >= h.fwhms(1) && targetBeamWidth <= h.fwhms(end))
      % interpolation works fine with interp1q
      intMap = interp1q(h.fwhms,h.suppPoints,targetBeamWidth);
    else
      % interp1 which manages extrapolation, do not use 2012a version
      intMap = interp1(h.fwhms,h.suppPoints,targetBeamWidth,'linear','extrap');
    end
  end

  % reshape to original map size e.g. 18000 -> 1000x18
  intMap = reshape(intMap,h.mapSize);

  % create a map exactly like the input map
  interpMaps(j) = h.mapTempl;
  interpMaps(j).map = intMap;

  % assign the new beamwidth
  interpMaps(j).fwhm = targetBeamWidth;
end

return
