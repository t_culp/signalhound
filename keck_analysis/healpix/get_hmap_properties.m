function map=get_hmap_properties(filename,ext)
% map=get_hmap_properties(filename,ext)
%
% Read map fits header and extract properties like nside, ordering, etc. Quicker than
% calling read_fits_map.
%
% ext = fits extension (default, 1)

if ~exist('ext','var')
  ext=1;
end

% get the headers
h=fitsinfo(filename);
map.filename=filename;
map.hdr=h.PrimaryData;
map.xhdr=h.BinaryTable(ext);


nfields = numel(find(strncmp(map.xhdr.Keywords(:,1),'TTYPE',5)));

% for each map
for i=1:nfields
  
  % pull out type and unit of each map
  n=find(strcmp(map.xhdr.Keywords(:,1),sprintf('TTYPE%1d',i)));
  if isempty(n)
    map.type{i}='';
  else
    map.type{i}=map.xhdr.Keywords{n,2};
  end
  n=find(strcmp(map.xhdr.Keywords(:,1),sprintf('TUNIT%1d',i)));
  if(n>0)
    map.units{i}=map.xhdr.Keywords{n,2};
  else
    map.units{i}='';
  end
end

%find the coord sys, if it exists
n=find(strcmp(map.xhdr.Keywords(:,1),'COORDSYS'));

if(~isempty(n))
  map.coordsys=upper(map.xhdr.Keywords{n,2});
  if ~strcmp(map.coordsys,'UNKNOWN')
    map.coordsys=map.coordsys(1);
  end
else
  map.coordsys='UNKNOWN';
end

% the wmap 9yr map don't specify the coordinate system in the fits file,
% but is is galactic. Put it in
if ~isempty(strfind(filename,'wmap')) && ~isempty(strfind(filename,'9yr')) && strcmp(map.coordsys,'UNKNOWN')
  map.coordsys='G';
end  

% all healpix maps have to tell us what the nside and ordering is
n=find(strcmp(map.xhdr.Keywords(:,1),'NSIDE'));
map.nside=map.xhdr.Keywords{n,2};
n=find(strcmp(map.xhdr.Keywords(:,1),'ORDERING'));
map.ordering=map.xhdr.Keywords{n,2};
n=find(strcmp(map.xhdr.Keywords(:,1),'FWHM'));
if(~isempty(n))
  map.fwhm=map.xhdr.Keywords{n,2};
else
  map.fwhm='UNKNOWN';
end
n=find(strcmp(map.xhdr.Keywords(:,1),'POLCCONV'));
if(~isempty(n))
  map.polcconv=map.xhdr.Keywords{n,2};
else
  map.polcconv='UNKNOWN';
end

return
