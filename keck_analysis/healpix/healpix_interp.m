function [map,loc]=healpix_interp(hmap,ra,dec,interptype,coord)
% map=healpix_interp(hmap,ra,dec,interptype,coord)
%
% hmap = healpix map containing T Q and U with first and second derivatives
%        May be in celestial or galactic coords
% ra,dec = interpolation locations in degrees - must be in celestial coords
% interptype = 'taylor' for a Taylor expansion using derivatives
%            = 'healpixnearest' for a nearest pixel interpolation (does not require that hmap
%            have derivatives)
% coord = coordinate system. Valid choices are 'C' and 'G' for galactic and
%         celestial coordinates, respectively
%
% map = N x 3 array of interpolated map where N is size of theta/phi array 

% For reference: Derivative values:
% T_p=hmap.map(loc,1);
% dTdth=hmap.map(loc,4);
% dTdph=hmap.map(loc,7);
% dTdth2=hmap.map(loc,10);
% dTdthdph=hmap.map(loc,13);
% dTdph2=hmap.map(loc,16);
% 
% Q_p=hmap.map(loc,2);
% dQdth=hmap.map(loc,5);
% dQdph=hmap.map(loc,8);
% dQdth2=hmap.map(loc,11);
% dQdthdph=hmap.map(loc,14);
% dQdph2=hmap.map(loc,17);
% 
% U_p=hmap.map(loc,3);
% dUdth=hmap.map(loc,6);
% dUdph=hmap.map(loc,9);
% dUdth2=hmap.map(loc,12);
% dUdthdph=hmap.map(loc,15);
% dUdph2=hmap.map(loc,18);

% Condition input arguments:
if ~exist('coord','var')
  coord='C';
end

if ~strcmpi(coord,'C') && ~strcmpi(coord,'G')
  error('Invalid coordinate system specification. Choose ''C'' or ''G''')
end

if(~exist('interptype','var'))
  interptype='taylor';
end

% Rotate interpolation coords if healpix map is galactic
if strcmpi(coord,'G')
  % Transform interpolation coords from cel to gal:
  [ra, dec]=euler(ra, dec, 1, 0);
end

% transform from ra/dec (in deg) to healpix theta/phi (in rad)
theta=(90-dec)*pi/180;
phi=ra*pi/180;

% NaN values make healpix code fragile -- replace with a sensible value
badpix=~isfinite(theta)|~isfinite(phi);
theta(badpix)=0;
phi(badpix)=0;

% Find nearest pixel to each requested position
% Note that in case of common a/b a second column in theta and phi holds the common pointing positions
switch hmap.ordering
 case 'RING'
   % note use second column to select pix to interp from if it exists
   pix=ang2pix_ring(hmap.nside,theta(:,end),phi(:,end));
 case 'NESTED'
   pix=ang2pix_nest(hmap.nside,theta(:,end),phi(:,end));
 otherwise
   error('healpix map must have RING or NESTED ordering')
end
% find pixel indices in hmap
[dum,loc]=ismember(pix,hmap.pixel);
clear dum

% check if any needed pixels are not in the provided (cutsky)
% healpix map
if(any(loc==0))
  warning('partial healpix map does not cover all requested interpolation locations; parts of TOD will be set to NaN')
  % point pixels which are not present to dummy map entry which is
  % all NaN
  loc(loc==0)=max(loc)+1;
  hmap.map(max(loc)+1,:)=NaN;
end

% if doing taylor compute needed stuff
switch interptype
 case {'taylor','taylor1','taylorT','taylor+','taylor1T'}
  % find the pixel theta/phi
  switch hmap.ordering
   case 'RING'  
    [theta_p,phi_p]=pix2ang_ring(hmap.nside,pix);
   case 'NESTED'
    [theta_p,phi_p]=pix2ang_nest(hmap.nside,pix);
  end
  % distance in theta and phi of desired interpolation locations from nearest pixel center
  dtheta=theta(:,1)-theta_p;
  clear theta_p
  dphi=phi(:,1)-phi_p;
  clear phi_p
  % Is this needed? Our field is centered on zero...
  dphi(dphi<-1)=dphi(dphi<-1)+2*pi;
  % correct for compression around rings
  dphi=dphi.*sin(theta(:,1));
end

% preallocate to speed up
map=zeros(length(pix),3);

% do the actual interpolation
switch interptype
 case {'taylor','taylor+'}
  
  % do the taylor expansion
  map(:,1) = hmap.map(loc,1) + cvec(hmap.map(loc,4).*dtheta + hmap.map(loc,7).*dphi + ...
                                    0.5*(hmap.map(loc,10).*dtheta.^2 + hmap.map(loc,16).*dphi.^2 + 2*dtheta.*dphi.*hmap.map(loc,13)));
  
  %    map(:,1) = T_p + cvec(dTdth.*dtheta + dTdph.*dphi + ...
  %                          0.5*(dTdth2.*dtheta.^2 + dTdph2.*dphi.^2 + 2*dtheta.*dphi.*dTdthdph) );
  
  map(:,2) = hmap.map(loc,2) + cvec(hmap.map(loc,5).*dtheta + hmap.map(loc,8).*dphi + ...
                                    0.5*(hmap.map(loc,11).*dtheta.^2 + hmap.map(loc,17).*dphi.^2 + 2*dtheta.*dphi.*hmap.map(loc,14)) );
  
  %    map(:,2) = Q_p + cvec(dQdth.*dtheta + dQdph.*dphi + ...
  %                          0.5*(dQdth2.*dtheta.^2 + dQdph2.*dphi.^2 + 2*dtheta.*dphi.*dQdthdph) );
  
  map(:,3) = hmap.map(loc,3) + cvec(hmap.map(loc,6).*dtheta + hmap.map(loc,9).*dphi + ...
                                    0.5*(hmap.map(loc,12).*dtheta.^2 + hmap.map(loc,18).*dphi.^2 + 2*dtheta.*dphi.*hmap.map(loc,15)) );
  
  %    map(:,3) = U_p + cvec(dUdth.*dtheta + dUdph.*dphi + ...
  %                          0.5*(dUdth2.*dtheta.^2 + dUdph2.*dphi.^2 +
  %                          2*dtheta.*dphi.*dUdthdph) );
  
 case 'taylorT' % Return only T and no Q/U:
  map = hmap.map(loc,1) + cvec(hmap.map(loc,4).*dtheta + hmap.map(loc,7).*dphi + ...
                               0.5*(hmap.map(loc,10).*dtheta.^2 + hmap.map(loc,16).*dphi.^2 + 2*dtheta.*dphi.*hmap.map(loc,13)));
  
  
 case 'taylor1'
  map(:,1) = hmap.map(loc,1) + cvec(hmap.map(loc,4).*dtheta + hmap.map(loc,7).*dphi);
  map(:,2) = hmap.map(loc,2) + cvec(hmap.map(loc,5).*dtheta + hmap.map(loc,8).*dphi);
  map(:,3) = hmap.map(loc,3) + cvec(hmap.map(loc,6).*dtheta + hmap.map(loc,9).*dphi);

 case 'taylor1T'
  map = hmap.map(loc,1) + cvec(hmap.map(loc,4).*dtheta + hmap.map(loc,7).*dphi);

 case 'healpixnearest'
  for i=1:min(3,size(hmap.map,2))
    map(:,i) = hmap.map(loc,i);
  end
  if size(hmap.map,2)<3
    map=map(:,1:size(hmap.map,2));
  end
  
 otherwise
  error('not a valid interptype')
  
end

% Put NaN back in for bad input coordinates
map(badpix,:)=NaN;

% If healpix map is in galactic, rotate Q/U back to celestial coords.
% ra/dec should be in gal coords as gal2cel_qu does the rotation to cel
if strcmpi(coord,'G') && size(map,2)>=3
  [ra, dec, map(:,2), map(:,3)]=gal2cel_qu(ra',dec', map(:,2), map(:,3));
end

return


