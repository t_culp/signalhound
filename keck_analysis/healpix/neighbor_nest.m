function [n,m] = neighbor_nest(nside,i)
% [n,m]=neighbor_nest(nside,ipix)
%
%   Find the pixels that border the pixel ipix. The output is
%   organized into pixels that share an edge, and pixels that
%   meet at a corner.
%
%   n = [NW,NE,SW,SE] pixels sharing and edge.
%   m = [N, W, E, S]  pixels meeting at a corner.
%
%   For some pixels, the m output may contain NaN, as some
%   healpix pixels have fewer than eight neighbors.
%
%   The corner-touching pixels are only calculated if the
%   output m is being used. Calculation of edge-touching
%   pixels only is faster.

% ensure we have a column vector
i = reshape(i,[],1);

% validate nside
if ~isnumeric(nside) || ~isscalar(nside) || nside<1 ...
   || mod(nside,1)~=0 || mod(log(nside)/log(2),1)~=0
  error(['Invalid argument for nside.']);
end

% validate pixel numbers
npix = 12 * nside * nside;
if ~isnumeric(i) || any(i<0) || any(i>=npix) || any(mod(i,1)~=0)
  error(['Invalid pixel number.']);
end

lev = log(nside)/log(2);

n = zeros(length(i),4);
for dir=0:3
  n(:,dir+1) = find_neighbor(i,dir,lev);
end

% Stupid methods for finding diagonal neighbors.
if nargout>1

  % find N
  tmp1 = find_neighbor(n(:,1),1,lev);  % NE of NW
  tmp2 = find_neighbor(n(:,2),0,lev);  % NW of NE
  m(:,1) = tmp1;
  m(tmp1~=tmp2,1) = NaN;               % if they're not the same, drop...
  m(tmp1==i,1) = tmp2(tmp1==i);        % ... unless one came back to SELF, in which case take the other...
  m(tmp2==i,1) = tmp1(tmp2==i);
  % ... unless both came back to SELF, in which case we're at the top.
  m(tmp1==i & tmp2==i,1) = bitxor(i(tmp1==i & tmp2==i),2 * 4^lev);

  % find W
  tmp1 = find_neighbor(n(:,3),0,lev);  % NW of SW
  tmp1(tmp1==n(:,1)) = NaN;            % if NW of SW is same as NW
  tmp2 = find_neighbor(n(:,1),2,lev);  % SW of NW
  tmp2(tmp2==n(:,3)) = NaN;            % if SW of NW is same as SW
  m(:,2) = tmp1;
  m(i>=npix/2,2) = tmp2(i>=npix/2);      % NW of SW is reliable except in S. polar cap.

  % find E
  tmp1 = find_neighbor(n(:,4),1,lev);  % NE of SE
  tmp1(tmp1==n(:,2)) = NaN;            % if NE of SE is same as NE
  tmp2 = find_neighbor(n(:,2),3,lev);  % SE of NE
  tmp2(tmp2==n(:,4)) = NaN;            % if SE of NE is same as SE
  m(:,3) = tmp1;
  m(i>=npix/2,3) = tmp2(i>=npix/2);      % NE of SE is reliable except in S. polar cap.

  % find S
  tmp1 = find_neighbor(n(:,3),3,lev);  % SE of SW
  tmp2 = find_neighbor(n(:,4),2,lev);  % SW of SE
  m(:,4) = tmp1;
  m(tmp1~=tmp2,4) = NaN;               % if they're not the same, drop...
  m(tmp1==i,4) = tmp2(tmp1==i);        % ... unless one came back to SELF, in which case take the other...
  m(tmp2==i,4) = tmp1(tmp2==i);
  % ... unless both came back to SELF, in which case we're at the bottom.
  m(tmp1==i & tmp2==i,4) = bitxor(i(tmp1==i & tmp2==i),2 * 4^lev);

end

return

%%%%

% helper function FIND_NEIGHBOR;
% find the nearest neighbor index in the
% specified direction, where dir is:
%   0 = NW
%   1 = NE
%   2 = SW
%   3 = SE
% lev = log2(nside).
function j=find_neighbor(i,dir,lev)

up = i;                % digits remaining to be processed.
j = zeros(size(i));    % build neighbor addresses here.
c = false(size(i));    % true when neighbor has been found already.
dig = zeros(size(i));  % next digit to be added to neighbor address.
lev0 = lev;

% Magic numbers
[n,m,p,q,d] = def_numerology(dir);

% Start at finest level, work upward.
while sum(~c)>0

  % treat level 0 (nside=1) as a special case.
  if lev==0
    on = up;
    up = 0;
    dig(c) = on(c);
    dig(~c) = m(1+on(~c));
    % for top/bottom row, some swapping is needed, as the
    % faces mate differently than for others.
    j(~c & q(1+on)) = top_swap(j(~c & q(1+on)),p,lev0-1);
    j = j + 4^lev0 * dig;
    return
  end

  % ordinary levels: digit permutation finds neighbors.
  on = mod(up,4);
  up = floor(up/4);
  dig(c) = on(c);
  dig(~c) = n(1+on(~c));
  c(on==d(1) | on==d(2)) = true;
  j = j + 4^(lev0-lev)*dig;
  lev = lev-1;
end
j = j + 4^(lev0-lev) * up;

return

%%%%

% helper function TOP_SWAP:
% permute digits to account for odd mating of
% top / bottom row of nside=1 pixels.
function j = top_swap(j,p,lev)

  j0 = j;
  up = j;
  j = zeros(size(j));
  lev0 = lev;
  for lev=0:lev0
    on = mod(up,4);
    up = floor(up/4);
    j = j + p(1+on) * 4^lev;
  end

return

%%%%

% helper function DEF_NUMEROLOGY:
% calculate magic numbers.
%   n = how the pixels usually line up at any level
%   m = how the pixels line up at nside=1, level 0
%   p = how to do the "top_swap"
%   q = which pixels at nside=1 need the "top swap"
%   d = which indices are done at any level > 0
function [n,m,p,q,d] = def_numerology(dir)

  % Magic numbers
  switch(dir)

    % northwest
    case 0, n = [2 3 0 1];
            m = [3 0 1 2 3 0 1 2 4 5 6 7];
            p = [1 3 0 2];
            q = [1 1 1 1 0 0 0 0 0 0 0 0];
            d = [0 1];
  
    % northeast
    case 1, n = [1 0 3 2];
            m = [1 2 3 0 0 1 2 3 5 6 7 4];
            p = [2 0 3 1];
            q = [1 1 1 1 0 0 0 0 0 0 0 0];
            d = [0 2];
    
    % southwest
    case 2, n = [1 0 3 2];
            m = [4 5 6 7 11 8 9 10 11 8 9 10];
            p = [2 0 3 1];
            q = [0 0 0 0 0 0 0 0 1 1 1 1];
            d = [1 3];

    % southeast
    case 3, n = [2 3 0 1];
            m = [5 6 7 4 8 9 10 11 9 10 11 8];
            p = [1 3 0 2];
            q = [0 0 0 0 0 0 0 0 1 1 1 1];
            d = [2 3];

  end
  q = logical(q)';
  p = p';

return
