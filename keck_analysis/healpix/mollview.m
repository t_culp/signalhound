function [m,lat,lon]=mollview(map,col,axlim,gs,coord)
% [m,lat,lon]=mollview(map,col,axlim,gs,coord)
%
% map is a healpix map file name
% col (defaut 1) is the map from this file which is to be displayed, 1 is temperature
% axlim (default no limit) is a part of the sky to zoom into
% gs (default 0.5 deg) is the width of the grid to do the plotting
% coord is passed forward to healpix_to_longlat 
%
% Plot healpix map in Mollweide projection
%
% Similar to Healpix mollview command
%
% e.g.: mollview('data/synfast_1024_map.fits');
%       mollview('/home/pryke/dataproc/wmap1/maps/map_w_imap_yr1_v1.fits');
%       mollview('/home/pryke/dataproc/wmap1/maps/teg_wie.fits');
%       mollview('/data/quad/healmaps/wmap3/wmap_band_imap_r9_3yr_W_v2.fits');
%
% usefull for maps that have been cut
%       mollview('input_maps/camb_wmap5yr_noB/map_cmb_n0512_r0000_s32p50.fits',1,[-70 -45 -55 55])
%

if(~exist('col'))
  col=1;
end
if(~exist('gs'))
  gs=0.5; %deg
end
if(~exist('axlim'))
  %axlim=[-70 -45 -55 55];
  axlim=[];
end
if(~exist('coord'))
  coord='C';
end

% generate matrix grid
lon=-180+gs/2:gs:180-gs/2;
lat=-90+gs/2:gs:90-gs/2;

try
  m=healpix_to_longlat(map,lon,lat,coord);
  latlim = [-90 90];
  lonlim = [-180 180];

catch
  % might be a fits file saved by us. That has a different format:
  m = fitsread(map);
  z=fitsinfo(map);
  pdk=z.PrimaryData.Keywords;
  nx=pdk{find(strcmp(pdk(:,1),'NAXIS1')),2};
  xl=pdk{find(strcmp(pdk(:,1),'CRVAL1')),2};
  xd=pdk{find(strcmp(pdk(:,1),'CDELT1')),2};
  xn=pdk{find(strcmp(pdk(:,1),'NAXIS1')),2};
  x_tic=xl:xd:(xl+xd*(xn-0.5));
  ny=pdk{find(strcmp(pdk(:,1),'NAXIS2')),2};
  yl=pdk{find(strcmp(pdk(:,1),'CRVAL2')),2};
  yd=pdk{find(strcmp(pdk(:,1),'CDELT2')),2};
  yn=pdk{find(strcmp(pdk(:,1),'NAXIS2')),2};
  y_tic=yl:yd:(yl+yd*(yn-0.5));
  latlim = [min(y_tic) max(y_tic)];
  lonlim = [min(x_tic) max(x_tic)];
end

[lat, lon] = meshgrat(latlim, lonlim, size(m(:,:,1)));

if(~exist('axlim', 'var')) || isempty(axlim)
  axesm('MapProjection','mollweid','Grid','on',...
    'GLineStyle','-','GLineWidth',0.1);
  axis off
  
  %find color axis limits
  hc=max(max(m(:,:,col))); 
  lc=min(min(m(:,:,col)));
else
  axesm('MapProjection','mollweid','Grid','on',...
    'GLineStyle','-','GLineWidth',0.1,'MapLatLimit',[axlim(1),axlim(2)],'MapLonLimit',[axlim(3),axlim(4)]);
  axis off

  %find color axis limits
  f1=intersect(find(lat>axlim(1)),find(lat<axlim(2)));
  f2=intersect(find(lon>axlim(3)),find(lon<axlim(4)));
  f=intersect(f1,f2);
  a=m(:,:,col);a=a(:);
  hc=max(a(f));
  lc=min(a(f));
end

pcolorm(lat, lon, m(:,:,col))

set(gca,'XDir','reverse');
caxis([lc,hc])

return
