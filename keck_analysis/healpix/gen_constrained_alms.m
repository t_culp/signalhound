function gen_constrained_alms(rlz,input_spec,alms_name,nlmax,cmb_Tmap,use_meas_clT)
% gen_constrained_alms(rlz, input_spec,alms_name,nlmax,cmb_Tmap)
%
%  rlz          : will pick the rlzth+1 seed from the seedlist, rlz counts from 0
%  input_spec   : string containing name of spectra file
%  alms_name    : string containing name of output alms file
%  nlmax        : maxim l to contain (must be even)
%  cmb_Tmap     : Full sky map of T to use as constraint
%  use_meas_clT : default false. Constraining sims assume map a_lms follow theory
%                 spectrum, which is a bad assumption at high ell for Planck NILC because
%                 of noise bias. Use measured T C_ls instead of theory T C_ls for
%                 constraining but still with theory TE.
%  
%  WARNING: 
%    You get different realizations for the same 'rlz' if 
%    nlmax is not the same. Always use nlmax=1536
%
% examples:
%  gen_constrained_alms(1, [],'test.fits',[],[])
%  gen_constrained_alms(1,'aux_data/official_cl/camb_planck2013_r0p1_noE.fits', 'testB.fits')

display('Generating constrained alms that respect TE correlations')

if ~exist('use_meas_clT') || isempty(use_meas_clT)
  use_meas_clT=false;
end

%rlz should be a number
if ~isnumeric(rlz)
  rlz = str2num(rlz);
end

%default nlmax
if ~exist('nlmax', 'var') || isempty(nlmax)
  nlmax='1536';
end

%there is a problem if nlmax is odd
if ~isnumeric(nlmax)
  nlmaxn=str2num(nlmax);
else
  nlmaxn=nlmax;
end
if rem(nlmaxn,2)
  error('choose an even nlmax for constrained realizations')
end   

if isnumeric(nlmax)
  nlmax = num2str(nlmax);
end

%default T constraint is Planck NILC 
if ~exist('cmb_Tmap', 'var') || isempty(cmb_Tmap)
  cmb_Tmap='input_maps/planck_foreground/COM_CompMap_CMB-nilc_2048_R1.20.fits';
  planck_beam='input_maps/planck_foreground/COM_CompMap_CMB-nilc_2048_R1.20_beam.fits';
  planck_mask='input_maps/planck_foreground/COM_CompMap_CMB-nilc_2048_R1.20_mask.fits';
  planck_nside='2048';
  %this is lensed TT spectra to go along with the unavoidably lensed NILC.
  input_specTT='aux_data/official_cl/camb_planck2013_extended_r0_lensing.fits';
end

if ~exist('input_spec', 'var') || isempty(input_spec)
  input_spec='aux_data/official_cl/camb_planck2013_r0.fits';
end


%Make alms from real sky CMB map
%-----------------------------------%
%make alms in a seperate step so that Q/U alms are made
stamp = gen_stamp();
tmp_almsfile1=['synfast_tmp/' stamp  '1_alms.fits'];
tmp_almsfile2=['synfast_tmp/' stamp  '2_alms.fits'];
run_anafast(cmb_Tmap,tmp_almsfile1, nlmax, [], [], planck_mask);

     
%need to use alteralms to correct for planck beam
run_alteralm(tmp_almsfile1,tmp_almsfile2,planck_beam,'0',0,planck_nside);


%Now take T alms and constuct E alms
%-----------------------------------%

%load alms created above
tmp_alms=fitsread(tmp_almsfile2, 'bintable', 1);

if use_meas_clT
  % Get power spectrum of alms
  xx = read_fits_alms(tmp_almsfile2);
  [l,cl,csl]=alm2cl(xx);
end

%index=l^2+l+m+1  => complete the square
%l=(index-m+3/4)^1/2-1/2... assume m=0 and take the floor,
%then make array of l values for these alms
ell=floor(sqrt(tmp_alms{1}+.75)-1/2);

%load input spectrums
modTT=load_cmbfast(input_specTT); %used lensed spectra for TT
mod=load_cmbfast(input_spec);

%generate a list of random complex, norm=1 coefficients
%figure out how many alms we have
m=length(tmp_alms{1}); %nalms=(nlmax*1)*(nlmax+2)/2;
%use our seeds
seeds=get_seedlist();
seed=seeds(rlz+1);
randn('seed', seed);
%generate the random vector for EE
r2=randn(m*2,1);
re=complex(r2(1:m), r2(m+1:m*2))/sqrt(2);
%generate the random vector for BB
seed=seeds(rlz+2);
randn('seed', seed);
r2=randn(m*2,1);
rb=complex(r2(1:m), r2(m+1:m*2))/sqrt(2);


%index is same for all
Talm(:,1)=tmp_alms{1};
Ealm(:,1)=tmp_alms{1};
Balm(:,1)=tmp_alms{1};


%for each ell
for l=1:str2num(nlmax)

  if use_meas_clT
    CTT=cl(l); % measured
  else
    CTT=modTT.C_l(l,1); % model
  end
  CTE=mod.C_l(l,2);
  CEE=mod.C_l(l,3);
  CBB=mod.C_l(l,4);
  
  %find alm index for this l
  ind=find(ell==l);
  
  %T_alms have been found above
  Talm(ind,2)=tmp_alms{2}(ind);
  Talm(ind,3)=tmp_alms{3}(ind);
  
  %E_alms
  %eqn 27, http://arxiv.org/abs/0711.2321v2
  Ealm(ind,2)=CTE/CTT*tmp_alms{2}(ind) + sqrt(CEE-CTE^2/CTT)*real(re(ind)); 
  Ealm(ind,3)=CTE/CTT*tmp_alms{3}(ind) + sqrt(CEE-CTE^2/CTT)*imag(re(ind));
    
  %so far B_alms all zero -> Now add B modes if they are in input_spec
  Balm(ind,2)=sqrt(CBB)*real(rb(ind));
  Balm(ind,3)=sqrt(CBB)*imag(rb(ind));
  
end

%write an alms file  

%get info
info=get_info(planck_nside, nlmax, m);

%scale alms from uK -> K
Talm(:,2)=Talm(:,2)*1d-6;
Talm(:,3)=Talm(:,3)*1d-6;
Ealm(:,2)=Ealm(:,2)*1d-6;
Ealm(:,3)=Ealm(:,3)*1d-6;
Balm(:,2)=Balm(:,2)*1d-6;
Balm(:,3)=Balm(:,3)*1d-6;

%make sure there are no NANs in the alms
bads=find(isnan(Ealm));
Ealm(bads)=0;
bads=find(isnan(Balm));
Balm(bads)=0;

%store all alms as one variable
alm(:,:,1)=Talm;
alm(:,:,2)=Ealm;
alm(:,:,3)=Balm;

%write alms
out_alms= ['synfast_tmp/alm_constrained_out_' gen_stamp() '.fits'];
fitswrite_bintable(real(alm),info,out_alms);

%need to use alteralms to rotate to ra,dec from gal
run_alteralm(out_alms,alms_name,'0','0',1,planck_nside);

if isempty(whos('global','synfast_debug'))
  system(['rm -f ' out_alms]);
  system(['rm -f ' tmp_almsfile1]);
  system(['rm -f ' tmp_almsfile2]);
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function info = get_info(nside, nlmax, nalms)
%  function xhdr = get_xhdr(planck_nside)
%  returns a minimum header for a fits file with alms

% Fill out info field
info.PrimaryData.Keywords={
    'SIMPLE'     'T'                               ''
    'BITPIX'     [                 32]             ''
    'NAXIS'      [                  0]             ''
    'EXTEND'     'T'                               ''
    'END'                           ''             ''
};

%make binary header
nfields=3;

xhdr={
    'XTENSION'    'BINTABLE'          ' binary table extension                         '                        
    'BITPIX'      [             8]    ' 8-bit bytes                                    '                        
    'NAXIS'       [             2]    ' 2-dimensional binary table                     '                        
    'NAXIS1'      [     4*nfields]    ' width of table in bytes                        '                        
    'NAXIS2'      [         nalms]    ' number of rows in table                        '                        
    'PCOUNT'      [             0]    ' size of special data area                      '                        
    'GCOUNT'      [             1]    ' one data group (required keyword)              '                        
    'TFIELDS'     [       nfields]    ' number of fields in each row                   '                        
    'NSIDE'       [         nside]    ' Resolution parameter for HEALPIX               '                        
    'FWHM'        [             0]    ''
    'COORDSYS'                 'C'    ' Coord system                                   '
    'MAX-LPOL'    [         nlmax]    ' lmax of alms                                   ' 
    };

for k=1:nfields
   if k==1 
    unit='numbers';
    form='1J';
    type='INDEX';
  end
  if k==2
    unit='K';
    form='1E';
    type='REAl';
  end
  if k==3
    unit='K';
    form='1E';
    type='IMAG';
  end

    
  ks=num2str(k);
  
  xhdr_add={
      ['TTYPE' ks]     type                ' map                                            '                        
      ['TFORM' ks]     form                ' data format of field: 4-byte REAL              '                        
      ['TUNIT' ks]     unit                ' map unit                                       '                        
      'COMMENT'        ''                  ''
      };

  xhdr=[xhdr;xhdr_add];
  
end

xhdr_add = {'END'         ''                  ''};
xhdr=[xhdr;xhdr_add];

%put in info
info.BinaryTable(1).Keywords=xhdr;
info.BinaryTable(2).Keywords=xhdr;
info.BinaryTable(3).Keywords=xhdr;

%Contents
info.Contents={'Primary', 'Binary Table', 'Binary Table', 'Binary Table'};

return