function alms=read_fits_alms(filename,table_num,convert_units)
% map=read_fits_map(filename,table_num,convert_units)
%
% Read in a healpix alms from fits file
%
% Analog of IDL read_fits_map function distributed with Healpix
% package
%
% table_num = 1,2,3 -> T,E,B (default 1)
%
% convert_units = K->uK. True by default, but this causes massive headaches
%                 all throughout the pipeline because the basic functions for reading
%                 and writing fits files do not show what is actually in the file, and
%                 healpix tools are incredibly dumb when it comes to units.
%
% map=read_fits_map('/data/quad/healmaps/wmap3/wmap_band_iqumap_r9_3yr_W_v2.fits')
% map=read_fits_map('/data/quad/healmaps/b03/b03.x33n.w1x2y1z2.na36.hp10.lp4.all.fits')
% map=read_fits_map('/data/mlb/healpix_maps/cmb_map1.fits')

if ~exist('table_num','var')
  table_num=1;
end

if ~exist('convert_units') || isempty(convert_units)
  convert_units = true;
end

% get the headers
h=fitsinfo(filename);
alms.filename=filename;
alms.hdr=h.PrimaryData;
alms.xhdr=h.BinaryTable;
data=fitsread(filename,'BinTable',table_num);

% for each map
for i=1:length(data)
  
  % massage to column vector in pixel number order
  m=data{i}'; alms.alms(:,i)=m(:);
  
  % pull out type and unit of each map
  n=find(strcmp(alms.xhdr(1).Keywords(:,1),sprintf('TTYPE%1d',i)));
  alms.type{i}=alms.xhdr(1).Keywords{n,2};
  n=find(strcmp(alms.xhdr(1).Keywords(:,1),sprintf('TUNIT%1d',i)));
  if(n>0)
    alms.units{i}=alms.xhdr(1).Keywords{n,2};
  else
    alms.units{i}='';
  end
end

% all healpix maps have to tell us what the nside and ordering is
n=find(strcmp(alms.xhdr(1).Keywords(:,1),'NSIDE'));
% but not all fits files with alms:
if any(n)
  alms.nside=alms.xhdr(1).Keywords{n,2};
else
  alms.nside='UNKNOWN';
end

%find the coord sys, if it exists
n=find(strcmp(alms.xhdr(1).Keywords(:,1),'COORDSYS'));
if(~isempty(n))
  alms.coordsys=alms.xhdr(1).Keywords{n,2};
else
  alms.coordsys='UNKNOWN';
end

%find the fwhm, if it exists
n=find(strcmp(alms.xhdr(1).Keywords(:,1),'FWHM'));
if(~isempty(n))
  alms.fwhm=alms.xhdr(1).Keywords{n,2};
else
  alms.fwhm='UNKNOWN';
end

%find the nlmax, if it exists
n=find(strcmp(alms.xhdr(1).Keywords(:,1),'MAX-LPOL'));
if(~isempty(n))
  alms.nlmax=alms.xhdr(1).Keywords{n,2};
else
  alms.nlmax='UNKNOWN';
end

% Some healpix maps are partial sky coverage
% For B2K maps (at least) this is achieved by putting the pixel index
% into the first column and labelling it's units as 'PIXEL'

if(strcmp(alms.type{1},'INDEX'))    
  % pull out the pixel index column
  alms.index=alms.alms(:,1);
  alms.alms=alms.alms(:,2:end);
  alms.units=alms.units(2:end);
  alms.type=alms.type(2:end);
else
  map.pixel=(0:size(map.map,1)-1)';
end

if convert_units
  % for each column with non uK units convert to uK
  for i=1:length(alms.units)
    if(strfind(alms.units{i},'mK'))
      alms.units{i}=strrep(alms.units{i},'mK','uK');
      alms.alms(:,i)=alms.alms(:,i)*1e3;
    end
    if(strmatch(alms.units{i},'K'))
      alms.units{i}=strrep(alms.units{i},'K','uK');
      alms.alms(:,i)=alms.alms(:,i)*1e6;
    end
    %sometime alm unit comes as 'K_CMB'
    if(strmatch(alms.units{i},'K_CMB'))
      alms.units{i}=strrep(alms.units{i},'K_CMB','uK');
      alms.alms(:,i)=alms.alms(:,i)*1e6;
    end 
    % sometime alm unit come as muK/microK instead of uK, fix here:
    if(strmatch(alms.units{i},'muK'))
      alms.units{i}=strrep(alms.units{i},'muK','uK');
    end
    if(strmatch(alms.units{i},'microK'))
      alms.units{i}=strrep(alms.units{i},'microK','uK');
    end
  end
end

return
