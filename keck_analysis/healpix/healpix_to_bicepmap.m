function map=healpix_to_bicepmap(hmap,m,interptype,coord)
% map=healpix_to_bicepmap(hmap,m,interptype,coord);
%
% hmap = input healpix map
% map = bicep map structure
% interptype = 'taylor' - use second derivatives contained in hmap to interpolate to
%               pixel centers (default)
%            = 'healpixnearest' - use nearest neighbor interpolation
% coord = coord sys of input hmap 'C' (default) or 'G'
%
% Input map is assumed to be in healpix pol convention, so sign of U map is flipped  to
% make it IAU.
%
% i.e. 
% m=get_map_defn('bicep');
% hmap = read_fits_map('input_maps/lensing_convergences/camb_planck2013_r0/kappa_r0001.fits');
% map = healpix_to_bicepmap(hmap,m);

if ~exist('interptype','var') || isempty(interptype)
  interptype='taylor';
end

if ~exist('coord') || isempty(coord)
  coord='C';
end

map.x_tic=m.x_tic;
map.y_tic=m.y_tic;

% interpolate
[x,y]=meshgrid(m.x_tic,m.y_tic);

% projection
if ~strcmp(m.proj,'radec')
  switch m.proj
   case 'arc'
    [x,y]=arcproj_to_radec(x(:),y(:),m.racen,m.deccen);
   case 'ortho'
    [x,y]=sinproj_to_radec(x(:),y(:),m.racen,m.deccen);
  end
end
y=healpix_interp(hmap,x(:),y(:),interptype,coord);

% Stick into map
sz=[m.ny,m.nx];
map.T=reshape(y(:,1),sz);
if size(y,2)>1
  map.Q=reshape(y(:,2),sz);
  map.U=reshape(y(:,3),sz);

  % Healpix -> IAU pol convention
  map.U=-map.U;
end

return

