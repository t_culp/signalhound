function mod = healpix_alm2power(fitsfile,fitsfile2)
% mod = healpix_alm2power(fitsfile)
%  of
% mod = healpix_alm2power(fitsfile1,fitsfile2)
%  
%  calculates the C_l and Cs_l from the alms in the fitsfile
%  compare to load_cmbfast, or cross spectrum of fitsfile1 and fitsfile2. 

finfo = fitsinfo(fitsfile);
lmax = fits_gethdrval(finfo,'MAX-LPOL',1);
for ii = 1:length(finfo.BinaryTable)
  alm1  = read_fits_alms(fitsfile,ii);
  if exist('fitsfile2','var')
    alm2 = read_fits_alms(fitsfile2,ii);
  else
    alm2=alm1;
  end
  [l,Cs_l(:,ii),C_l(:,ii)] = alm2power(alm1,alm2,lmax);
end
mod.l=l;
mod.Cs_l=Cs_l;
mod.C_l=C_l;

return

%%
function val = fits_gethdrval(hdr,key,col)

if isfield(hdr,'BinaryTable')
  kwds = hdr.BinaryTable(col).Keywords;
elseif isfield(hdr,'xhdr')
  kwds = hdr.xhdr(col).Keywords;
else
  error('Did not find fits header.')
end
n=find(strcmp(kwds(:,1),key));
val = kwds(n,2);
val = val{1};

return

%%
function [l,Cs_l,C_l] = alm2power(alm1,alm2,lmax)
C_l = zeros(lmax,1);
Cs_l = zeros(lmax,1);

il = 1;
for l=0:lmax
  nl = l+1;
  C_l(nl) = (alm1.alms(il,1)*alm2.alms(il,1) + ...
             2 * sum(alm1.alms(il+1:il+l,1).*alm2.alms(il+1:il+l,1) + alm1.alms(il+1:il+l,2).*alm2.alms(il+1:il+l,2)))/ (2*l + 1.);
  Cs_l(nl)= C_l(nl) * l*(l+1) /pi/2;
  il = il + nl;
end

l=0:lmax;

return

