function npix=nside2npix(nside)
% npix=nside2npix(nside)
%
% returns npix = 12*nside*nside
% number of pixels on a Healpix map of resolution nside
%
% if nside is not a power of 2 <= 8192 error
%
% MODIFICATION HISTORY:
%
%     v1.0, EH, Caltech, 2000-02-11
%     v1.1, EH, Caltech, 2002-08-16 : uses !Healpix structure
%     Feb 2006, CLP, translated to Matlab

% -----------------------------------------------------------------------------
%
%  Copyright (C) 1997-2005  Krzysztof M. Gorski, Eric Hivon, Anthony J. Banday
%
%
%
%
%
%  This file is part of HEALPix.
%
%  HEALPix is free software% you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation% either version 2 of the License, or
%  (at your option) any later version.
%
%  HEALPix is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY% without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with HEALPix% if not, write to the Free Software
%  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
%
%  For more information about HEALPix see http://healpix.jpl.nasa.gov
%
% -----------------------------------------------------------------------------

% is nside a power of 2 ?
if(~any(nside==[1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192]))
  error('nside must be power of 2');
end

npix = 12* nside^2;

return

