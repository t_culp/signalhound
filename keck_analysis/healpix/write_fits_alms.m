function write_fits_alms(fname,alms)
% write_fits_alms(filename,hmap)
%
% Write a MATLAB healpix structure contained in alms to a fits file.
%
% 
%      e.g.
%
%      For T alms:
%
%      alms=read_fits_alms('test.fits');
%      write_fits_alms('alms.fits',alms);
%
%      For T,E,B alms, it is more complicated
%
%      alms=read_fits_alms('test.fits');
%      almT=read_fits_alms('test.fits', 1);
%      almE=read_fits_alms('test.fits', 2);
%      almB=read_fits_alms('test.fits', 3);
%      alms=rmfield(alms, 'alms');
%      alms.alms(:,:,1)=[almT.alms(:,1),almT.alms(:,2)];
%      alms.alms(:,:,2)=[almE.alms(:,1),almE.alms(:,2)];
%      alms.alms(:,:,3)=[almB.alms(:,1),almB.alms(:,2)];
%      write_fits_alms('alms.fits',alms);

% Fill out info field
info.PrimaryData.Keywords={
    'SIMPLE'     'T'                               ''
    'BITPIX'     [                 32]             ''
    'NAXIS'      [                  0]             ''
    'EXTEND'     'T'                               ''
    'END'                           ''             ''
};



% Generate header keywords
xhdr=get_xhdr(alms);

for i=1:size(alms.alms,3)
  info.BinaryTable(i).Keywords=xhdr;
end


%add the index to the first colum
for i=1:length(info.BinaryTable)
  dat(:,:,i)=[alms.index,alms.alms(:,:,i)];
end


% Write fits map
fitswrite_bintable(dat,info,fname);

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xhdr=get_xhdr(alms)

nside=alms.nside;
nfields=size(alms.alms,2)+1;
unit='test';
npix=numel(alms.index);

xhdr={
    'XTENSION'    'BINTABLE'          ' binary table extension                         '                        
    'BITPIX'      [             8]    ' 8-bit bytes                                    '                        
    'NAXIS'       [             2]    ' 2-dimensional binary table                     '                        
    'NAXIS1'      [     4*nfields]    ' width of table in bytes                        '                        
    'NAXIS2'      [          npix]    ' number of rows in table                        '                        
    'PCOUNT'      [             0]    ' size of special data area                      '                        
    'GCOUNT'      [             1]    ' one data group (required keyword)              '                        
    'TFIELDS'     [       nfields]    ' number of fields in each row                   '                        
    'TEMPTYPE'    'THERMO'            ' temperature type either THERMO or ANTENNA      '                        
    'PIXTYPE'     'HEALPIX'           ' HEALPIX Pixelisation                           '                        
    'NSIDE'       [         nside]    ' Resolution parameter for HEALPIX               '                        
    'FWHM'        [     alms.fwhm]    ''
    'COORDSYS'    alms.coordsys       ' Coord system                                   '
    'MAX-LPOL'    [    alms.nlmax]    ' lmax of alms                                   '
     };

for k=1:nfields
   if k==1 
    unit='numbers';
    form='1J';
    type='INDEX';
  else
    unit=alms.units{k-1};
    form='1E';
    type=alms.type{k-1};
  end
    
  ks=num2str(k);
  
  xhdr_add={
      ['TTYPE' ks]     type                ' map                                            '                        
      ['TFORM' ks]     form                ' data format of field: 4-byte REAL              '                        
      ['TUNIT' ks]     unit                ' map unit                                       '                        
      'COMMENT'        ''                  ''
      };

  xhdr=[xhdr;xhdr_add];
  
end

xhdr_add = {'END'         ''                  ''};
xhdr=[xhdr;xhdr_add];


return
