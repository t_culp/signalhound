function [pchi,rot]=beams2chi(basedate,beamcen)
% [pchi,rot]=beams2chi(basedate,beamcen)
%
% Calculates corrected chi angles for a given set of observed beam centers.
%
% INPUTS
%   basedate    Epoch date string to pass to get_array_info in order to load
%               the ideal beam centers.
%
%   beamcen     Path to observed beam centers CSV file for which updated chi
%               angles will be computed.
%
% OUTPUTS
%   pchi        Structure containing rotated chi angles compatible with the
%               observed beam centers.
%
%   rot         Rotation angles for each receiver which were subtracted from
%               the ideal chi angles.
%
% EXAMPLE
%   pc = beams2chi('20150101', 'aux_data/beams/beamcen_20150101.csv');
%

  % Get ideal pointing information.
  [p,ind] = get_array_info(basedate);

  % Load in the observed beam centers from the specified file.
  [po,ko] = ParameterRead(beamcen);

  % Compute the x/y coordinates for both ideal and observed pointings.
  % IMPORTANT! get_array_info applies a drum angle rotation, so undo that
  %            to have comparable values as come raw from the beam
  %            center file.
  xi = p.r  .* cosd(p.theta + p.drumangle);
  yi = p.r  .* sind(p.theta + p.drumangle);
  xo = po.r .* cosd(po.theta);
  yo = po.r .* sind(po.theta);

  % Prepare the output data
  pchi.gcp   = p.gcp;
  pchi.theta = p.theta + p.drumangle;
  pchi.chi   = NaN*zeros(size(p.gcp));

  % Make lsqnonlin quieter
  optim = optimset('Display','off');

  rglmask = cvec(ismember(ind.e, ind.rgl));
  rxs = rvec(unique(p.rx(ind.l)));
  rot = zeros(size(rxs));
  for ii=1:length(rxs)
    % Restrict to only this receiver.
    rr = (p.rx == rxs(ii));
    nn = (rr & rglmask);
    % Then given both ideal and observed pointings, further mask to only
    % those with valid pointing values.
    cc = nn & (isfinite(xi) & isfinite(yi) & isfinite(xo) & isfinite(yo));
    % Use only selected channels in fit by wrapping g.o.f. function in
    % anonymous closure.
    params = lsqnonlin(...
        @(a) goffn(xi(cc),yi(cc),xo(cc),yo(cc),a), [0 0 0 0], ...
        [], [], optim);

    % Rotate ideal chi values by best-fit rotation angle.
    rot(ii) = rad2deg(params(4));
    pchi.chi(rr) = p.chi(rr) - rot(ii);
    if false
      fprintf(1,[...
          'rx%i optimal parameters:\n' ...
          '  x/y shift  = (%0.3f, %0.3f)\n' ...
          '  mag factor = %0.3f\n'...
          '  rot angle  = %0.3f\n'], ...
          rxs(ii), params(1), params(2), params(3), rot(ii));
    end
  end
end

function cost=goffn(x1,y1,x2,y2,a)
% The goodness-of-fit function for a 4-parameter model identifying
%
% x1,y1,x2,y2 should be fixed during operation by enclosing in an anonymous
% closure.
%
% a are free parameters being fed back through the fit routine.
%

  % Idea is to find a rotation of the ideal beam centers (x1,y1) which best
  % agree with the observed beam centers (x2,y2). Do this by allowing the
  % ideal beams to rotate and translate on the sky. The fit parameter is
  % then the distance between the observed and [rotated] ideal centers.

  % Allow the entire focal plane to shift laterally in case there is a
  % systematic offset seen in the beam centers.
  dx = a(1);
  dy = a(2);

  % A magnification scaling parameter (AKA plate scale)
  sc = a(3);

  % The overall rotation angle of interest
  ang = a(4);

  cost = sqrt( ( x2+dx - (1+sc)*( x1*cos(ang) + y1*sin(ang)) ).^2 ...
             + ( y2+dy - (1+sc)*(-x1*sin(ang) + y1*cos(ang)) ).^2 );
end
