function fitfile=get_beamfit_filename(beamcorropt)
% fitfile=get_beamfit_filename(beamcorropt)
%
% Generates the output file name for a beam correlation fit file based on the
% correlation options.
%
% INPUTS
%   beamcorropt    Beam correlation options. Output filename is derived from
%                  mandatory 'sernum' and 'corrname' fields, and futher
%                  modifications from optional 'tile' and 'coaddopt' fields.
%
% OUTPUT
%   fitfile        Path to corresponding beam fit file.
%
% EXAMPLE
%   beamcorropt.sernum = coaddopt.sernum;
%   beamcorropt.corrname = '20150101';
%   beamcorropt.coaddopt = coaddopt;
%
%   fitfile = get_beamfit_filename(beamcorropt);
%

  if ~isfield(beamcorropt,'sernum') || isempty(beamcorropt.sernum)
    error('sernum cannot be empty');
  end
  if ~isfield(beamcorropt,'corrname') || isempty(beamcorropt.corrname)
    error('corrname cannot be empty');
  end

  outdir = fullfile('beamfit', beamcorropt.sernum(1:4));

  corrname = beamcorropt.corrname;

  mapbase = '';
  if isfield(beamcorropt,'coaddopt')
    [mapdir mapbase dum] = fileparts(get_map_filename(...
        get_default_coaddopt(beamcorropt.coaddopt)));
    mapbase = ['_' mapbase];
  end

  tilepart = '';
  if isfield(beamcorropt,'tile') && ~isempty(beamcorropt.tile)
    tilepart = ['_tile' num2str(beamcorropt.tile)];
  end

  fitfile = fullfile(outdir, [corrname mapbase tilepart '.mat']);
end

