function plot_beamcenters(p,ind,cmpbc,newbc,newcut,varargin)
% plot_beamcenters(p,ind,cmpbc,newbc,newcut,varargin)
%
% Creates quiver plot showing beam center shifts between a given comparison
% reference and new beam centers. Additionally indicates any beams which
% have NaN error estimates (most likely due to empty coadded maps) with
% filled green circles, and detectors that make use of the 4-parameter
% fallback fit are indicated by open blue circles.
%
% INPUTS
%   p         Standard p structure.
%
%   ind       Standard ind structure, possibly stripped to single rx values
%             in the case of Keck.
%
%   cmpbc     Structure of r and theta positions to compare against.
%
%   newbc     Structure of r and theta positions for the new beam centers.
%
%   newcut    Structure containing beam fit cut information as output from
%             reduc_calc_beamcenters(). Optional. If not provided, cut
%             indicators will not be plotted.
%
%   varargin  Other options:
%
%             'noannot'     Does not plot circles to indicate no correlation
%                           and cuts.
%
%             'notilelbl'   Do not add text labels to annotate the tiles.
%
%             'nolegend'    Do not add the legend.
%
% EXAMPLE
%
%   % Plot new beam centers against ideal. Need to correct for drum angle.
%   [p,ind] = get_array_info('20150101');
%   idealbc.r = p.r;
%   idealbc.theta = p.theta + p.drumangle;
%
%   % Read new beam centers from CSV
%   newbc  = ParameterRead('beamfit/1711/beamcen.csv');
%   newcut = ParameterRead('beamfit/1711/beamcen.csv');
%
%   % For Keck, need to strip to only single rx
%   ind3 = strip_ind(ind, find(p.rx == 3));
%
%   plot_bcquivers(p, ind3, idealbc, newbc, newcut);
%

  % Setup default options
  noannot = false;
  notilelbl = false;
  nolegend = false;

  % Then process any options.
  %
  % Scalar options (i.e. not key-value pair)
  optmask   = true(size(varargin));
  [noannot,  optmask] = scalaropt(varargin, 'noannot',   optmask);
  [notilelbl,optmask] = scalaropt(varargin, 'notilelbl', optmask);
  [nolegend, optmask] = scalaropt(varargin, 'nolegend',  optmask);
  % Remove the scalar options from the varargin list
  varargin = varargin(optmask);
  % Then process the key-value pairs
  %
  % Currently none, so throw error if any leftover.
  if length(varargin) > 0
    error('bkpipeline:plot_beamcenters:opts', ...
        'unrecognized options:\n\t%s', strtrim(evalc('disp(varargin)')));
  end

  [reflat reflon] = reckon(0, 0, p.r, p.theta+p.drumangle+90);
  [cmplat cmplon] = reckon(0, 0, cmpbc.r, cmpbc.theta+90);
  [newlat newlon] = reckon(0, 0, newbc.r, newbc.theta+90);

  % Differences between the beam centers
  dx = newlat - cmplat;
  dy = newlon - cmplon;

  % Find detectors that have a NaN position error, mostly likely due to not
  % producing maps.
  if isfield(newbc, 'err_pos_x')
    nanerr = find(isnan(newbc.err_pos_x) | isnan(newbc.err_pos_y));
    nanerr = intersect(ind.rgl, nanerr);
  else
    nanerr = [];
  end

  % Also enumerate the list of detectors that were cut by the fitting code.
  if exist('newcut','var') && ~isempty(newcut)
    cutdet = intersect(ind.rgl, find(~newcut.overall));
  end

  % Multiplication factor to grow the quiver sizes:
  L = 5;

  cla();
  % Then overdraw indicators for detectors that presumably didn't produce a
  % map at all to correlate.
  if ~noannot && ~isempty(nanerr)
    scatter(reflat(nanerr), reflon(nanerr), 'g','filled');
  end
  hold on
  % Optionally show the 4-param fallback detectors
  if ~noannot && exist('newcut','var') && ~isempty(newcut)
    scatter(reflat(cutdet), reflon(cutdet), 'bo');
  end
  % Finally, put quivers on the plot indicating the beam center motion to
  % go from the reference to the new beam centers.
  ii = ind.rgl;
  quiver(reflat(ii), reflon(ii), L*dx(ii), L*dy(ii), 0, 'r');
  hold off

  axis square
  box on

  % Estimate the minimum and maximum axis ranges required to plot the
  % quivers.
  xl(1) = floor(min(reflat)/5)*5;
  xl(2) = ceil( max(reflat)/5)*5;
  yl(1) = floor(min(reflon)/5)*5;
  yl(2) = ceil( max(reflon)/5)*5;
  xlim(xl);
  ylim(yl);

  if ~notilelbl
    % Annotate and identify each tile
    tiles = rvec(unique(p.tile(ind.rgl)));
    for tt=tiles
      indt = strip_ind(ind, find(p.tile == tt));

      if mean(reflon(indt.l)) > 0
        yy = max(reflon(indt.l)) + 0.5;
        va = 'bottom';
      else
        yy = min(reflon(indt.l)) - 0.5;
        va = 'top';
      end
      xx = mean(reflat(indt.l));
      text(xx, yy, sprintf('tile %i', tt), ...
          'horizontalalignment','center', ...
          'verticalalignment',va);
    end
  end

  if ~nolegend
    % Add a legend entries
    key = {};
    if ~noannot && any(nanerr)
      key = {'No correlation'};
    end
    if ~noannot && exist('newcut','var') && ~isempty(newcut) && ~isempty(cutdet)
      key = [key {'4-param fallback'}];
    end
    key = [key {['BC shift (\times{}' num2str(L) ')']}];
    legend(key)
  end
end

function [tf,mask]=scalaropt(args,opt,mask)
% [tf,mask]=scalaropt(args,opt,mask)
%
% Helper for processing scalar varargin options.
%
% INPUTS
%   args  Cell array (i.e. varargin)
%   opt   Option (as string) to search for
%   mask  Mask from previous calls to scalaropt (or true(size(args)) on
%         first call).
%
% OUTPUTS
%   tf    True if option found, otherwise false.
%   mask  Mask that marks processed argument entries as false.
%

  % ismember() only works on purely string cell arrays, so mask out anything
  % which is not a string when searching and building the mask to return.
  strmask = cellfun(@ischar, args);
  tf = ismember(args(strmask), opt);
  mask(strmask) = mask(strmask) & ~tf;
  tf = any(tf);
end
