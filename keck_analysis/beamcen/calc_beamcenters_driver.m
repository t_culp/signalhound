function calc_beamceters_driver(nbase,daughter)
% calc_beamcenters_driver(nbase,daughter)
%
% Driver to wrap creation and analysis of maps which provide new beam centers
% for a full-season analysis.
%
% INPUTS
%   nbase       Serial number to use for new pairmaps and coadd.
%
%   daughter    Daughter identifying standard full-season coadd.
%
% EXAMPLE
%   calc_beamcenters_driver(1711,'e')
%

  if ~exist('nbase','var') || isempty(nbase)
    error('nbase is not optional')
  end
  if ischar(nbase)
    nbase = str2double(nbase);
  end

  if ~exist('daughter','var') || isempty(daughter)
    error('daughter is not optional')
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Setup all the inputs

  % Reuse pairmaps from the standard simset, chosen based on the experiment.
  expt = get_experiment_name();
  switch expt
    case 'keck'
      nreal = 1351;
    case 'bicep3';
      nreal = 0000;
    otherwise
      error('Unspported experiment ''%s''.', expt);
  end

  % Define some common answers:
  Pdir = 'input_maps/planck/planck_derivs_nopix';
  P100_bKuber100 = [Pdir '/HFI_SkyMap_100_2048_R1.10_nominal_nside0512_nlmax1280_bKuber100rev1.fits'];
  P143_bB2bbns   = [Pdir '/HFI_SkyMap_143_2048_R1.10_nominal_nside0512_nlmax1280_bB2bbns.fits'];
  P217_bKuber220 = [Pdir '/HFI_SkyMap_217_2048_R1.10_nominal_nside0512_nlmax1280_bKuber220.fits'];

  Pextdir = 'input_maps/planck/planck_derivs_bicepext';
  P100_b24p16 = [Pextdir '/HFI_SkyMap_100_2048_R1.10_nominal_nside0512_nlmax1280_b24.16.fits'];

  % Default to using bicepfine map type, but allow a year/coadd to override that option.
  mapdefn = 'bicepfine';

  % Get needed values based on what year/coadd is being used.
  switch expt
    case 'keck'
      switch daughter
        case 'a'
          tags = get_tags('cmb2012');
          % Appropriate input maps in frequency ascending order.
          extmaps = {P143_bB2bbns};
          % Correlation name
          corrname = '20120101';
          % Nominal beam size at FWHM in arcmins. If empty, defaults taken from
          % the focal plane ideal fp_data files are used to calculate for
          % channel flags, otherwise a frequency-ascending order of overrides may
          % be provided.
          fwhm = [30.1];
        case 'b'
          tags = get_tags('cmb2013');
          extmaps = {P143_bB2bbns};
          corrname = '20130101';
          fwhm = [30.1];
        case {'c','d'}
          tags = get_tags('cmb2014');
          extmaps = {P100_bKuber100, P143_bB2bbns};
          corrname = '20140101';
          fwhm = [43.1, 30.1];
        case 'e'
          tags = get_tags('cmb2015');
          extmaps = {P100_bKuber100, P143_bB2bbns, P217_bKuber220};
          corrname = '20150101';
          fwhm = [43.1, 30.1, 20.6];
        otherwise
          error('Unsupported daughter ''%s''.', daughter);
      end

    case 'bicep3'
      % Need a bigger map area for B2016.
      mapdefn = 'bicepext';

      switch daughter
        case '2016prelim'
          tags = get_tags('cmb2016');
          % The first 766 tags were present when this analysis was started.
          tags = tags(1:766);
          extmaps = {P100_b24p16};
          corrname = '2016prelim';
          fwhm = [24.16];
        otherwise
          error('Unsupported daughter ''%s''.', daughter);
      end
  end

  clear mapopt;
  mapopt.sernum = sprintf('%04ireal', nbase);
  % Make no starting assumption on pointing except design specs.
  mapopt.beamcen = 'ideal';
  % Also set chi and epsilon to ideal, which don't matter since they only
  % effect polarization, and beam centers come from temperature only. This
  % avoid issues in early-season data which may not have these specified yet.
  mapopt.epsilon = 'ideal';
  mapopt.chi = 'ideal';
  % Make finer resolution maps.
  mapopt.type = mapdefn;
  % Disable deprojection for extra speed. We're only correlating the T maps,
  % so deprojection has no effect.
  mapopt.deproj = false;
  % Pack ac to keep under v7 file size limit.
  mapopt.acpack = true;
  mapopt = get_default_mapopt(mapopt);

  clear coaddopt;
  coaddopt.sernum = mapopt.sernum;
  % Per-detector (un-summed/diffed) maps
  coaddopt.coaddtype = 4;
  % Only need jack2 maps for analysis.
  coaddopt.jacktype = '2';
  % Deprojection must match mapopt above.
  coaddopt.deproj = false;
  % Disable channel flags or else suffer a chicken/egg problem.
  coaddopt.chflags = [];
  % Other standard options:
  coaddopt.cut = get_default_round2_cuts([], tags{1}(1:4));
  coaddopt = get_default_coaddopt(coaddopt);

  % BICEP3 early 2016 has problem with elnod_gof triggering and cutting large
  % chunk of data. To do early-season analysis, need as much data as
  % possible, and elnod_gof quite degenerate with other cuts already, so
  % advantage to not cutting unnecessarily is bigger than any possible
  % contamination caused by disabling this cut.
  if strcmp(expt,'bicep3') && strcmp(daughter,'2016prelim')
    warning('Disabling elnod_gof in 2016 prelim coadd.')
    coaddopt.cut.elnod_gof = Inf;
  end

  clear beamcorropt;
  beamcorropt.sernum = mapopt.sernum;
  beamcorropt.coord = 'C';
  beamcorropt.corrname = corrname;
  beamcorropt.coaddopt = coaddopt;

  % Farming options
  submit = false;
  makemem = 30000;
  coaddmem = 20000;
  coaddcoaddsmem = 45000;
  corrmem = 55000;
  maketime = 2*60;
  coaddtime = 4*60;
  coaddcoaddstime = 30;
  corrtime = 23*60;
  queue = 'serial_requeue,itc_cluster';

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Do work. First, make pairmaps if necessary.

  [farmfile,jobname] = farmfilejobname(mapopt.sernum, 'placeholder');
  [farmdir,dum,dum] = fileparts(farmfile);
  if ~exist(farmdir,'dir')
    system_safe(['mkdir -p ' farmdir]);
  end

  % List all expected pairmaps and then filter for non-existing ones.
  pairmaps = get_pairmap_filename(tags, mapopt);
  existing = cellfun(@exist_file, pairmaps);
  for tag=rvec(tags(~existing))
    [dum,fname,dum] = fileparts(get_pairmap_filename(tag{1}, mapopt));
    [farmfile,jobname] = farmfilejobname(mapopt.sernum, fname);
    farmit(farmfile, 'reduc_makeabmaps(tag,mapopt);', ...
        'jobname',jobname, ...
        'overwrite', 'skip', ...
        'queue', queue, ...
        'mem', makemem, ...
        'maxtime', maketime, ...
        'submit', submit, ...
        'var', {'tag','mapopt'});
  end
  if ~all(existing)
    fprintf(1,'Submit jobs by running babysitjobs(%04i,''wait10'')\n',nbase);
    disp('Rerun this script after pairmaps have all been generated.')
    return
  end


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Coadd maps.

  % Parse all tags to create list of unique deck angles.
  [dates,phases,scans,dks,extra] = parse_tag(tags);
  udks = rvec(unique(dks));
  dkstr = arrayfun(@(dk) sprintf('dk%03i', dk), udks, 'uniformoutput',false);

  % Create a mask which chooses only common deck tags
  tagmask = false(numel(udks)+1, numel(tags));
  for ii=1:numel(udks)
    tagmask(ii,:) = dks == udks(ii);
  end

  % Now create coadd jobs if any maps are missing
  expectmaps = {};
  for ii=1:length(dkstr)
    coaddopt.daughter = [daughter '_' dkstr{ii}];
    tagz = tags(tagmask(ii,:));
    numsplits = ceil(length(tagz)/80);

    dkmap = get_map_filename(coaddopt);
    expectmaps = [expectmaps dkmap];
    if ~exist_file(dkmap)
      farm_coaddpairmaps(tagz, coaddopt, ...
          'OnlyMissing', true, ...
          'Realizations', [], ...
          'Queue', queue, ...
          'MemRequire', coaddmem, ...
          'maxtime', coaddtime, ...
          'SplitSubmission', numsplits, ...
          'UseCompiled', false, ...
          'submit', submit);
    end
  end
  if ~all(cellfun(@exist_file,expectmaps))
    fprintf(1,'Submit jobs by running babysitjobs(%04i,''wait10'')\n',nbase);
    disp('Rerun this script after coadd jobs have completed.');
    return
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Run correlations on maps.

  [p,ind] = get_array_info(tags{1}(1:8));

  % Use changes in p.tile to identify number of tiles. Some pixels may be
  % marked by NaN, though, so need to mask array appropriately.
  ptmask = rvec(isfinite(p.tile));
  % Leading pad to make original length again.
  boundary(ptmask) = [0 diff(rvec(p.tile(ptmask)))];
  % Get locations of first non-NaN pixel for use in identifying frequency
  % of each tile.
  tilestart = [find(ptmask,1,'first') find(boundary)];
  ntiles = length(tilestart);

  allexist = true;
  for tt=1:ntiles
    % Select the correct input reference map to use:
    freqidx = get_band_ind(p,ind, p.band(tilestart(tt)));

    % Choose specific tile and select appropriate input map
    beamcorropt.hmapfile = extmaps{freqidx};
    beamcorropt.tile = tt;

    % Then for each deck angle, do a correlation
    for ii=1:length(dkstr)
      % Choose specific deck coadd
      coaddopt.daughter = [daughter '_' dkstr{ii}];
      mapf = get_map_filename(coaddopt);
      [farmfile,jobname] = farmfilejobname(coaddopt.sernum, 'beamcorr', ...
          dkstr{ii}, sprintf('tile%02i', tt));

      % Update embedded coaddopt for file naming
      beamcorropt.coaddopt = coaddopt;
      % Skip if the fit already exists.
      if exist_file(get_beamfit_filename(beamcorropt))
        continue;
      end

      % If we get this far, then indicate that not all beam files are done.
      allexist = false;
      farmit(farmfile, 'reduc_beamfit_correlate(mapf,beamcorropt,1);', ...
          'jobname', jobname, ...
          'overwrite', 'skip', ...
          'queue', queue, ...
          'mem', corrmem, ...
          'maxtime', corrtime, ...
          'submit', submit, ...
          'var', {'mapf', 'beamcorropt'});
    end % ii=1:length(dkstr)
  end % tt=1:ntiles
  if ~allexist
    disp('Re-run this driver once all correlation files are processed');
    return
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Combine separate tile correlations into single deck file.

  combbmfile = cell(size(dkstr));
  for ii=1:length(dkstr)
    coaddopt.daughter = [daughter '_' dkstr{ii}];

    % Skip regenerating the file if it already exists.
    beamcorropt.coaddopt = coaddopt;
    try; beamcorropt = rmfield(beamcorropt, 'tile'); end
    combbmfile{ii} = get_beamfit_filename(beamcorropt);
    if exist_file(combbmfile{ii})
      continue;
    end

    % First correlation can simply be loaded.
    beamcorropt.tile = 1;
    bmfile = get_beamfit_filename(beamcorropt);
    fprintf(1,'loading %s...\n', bmfile);
    corr = load(bmfile);

    % Remove tile specification from the beamcorropt for the combined file
    try; corr.beamcorropt = rmfield(corr.beamcorropt,'tile'); end

    for tt=2:ntiles
      beamcorropt.tile = tt;
      bmfile = get_beamfit_filename(beamcorropt);
      fprintf(1,'loading %s...\n', bmfile);
      corr2 = load(bmfile);

      % Each of xc, xca, xcb, and diffcorr are initialized as zero and
      % skipped for detectors not in the chosen tile, so combining is as
      % simple as summing as they load.
      corr.xc  = corr.xc  + corr2.xc;
      corr.xca = corr.xca + corr2.xca;
      corr.xcb = corr.xcb + corr2.xcb;
      corr.diffcorr = corr.diffcorr + corr2.diffcorr;
      % The logical mask combines with binary-or.
      corr.done_lost = corr.done_list | corr2.done_list;
    end % tt=1:ntiles

    % With everything collated, save back out to disk
    fprintf(1, 'saving combined data to %s...\n', combbmfile{ii});
    save(combbmfile{ii}, '-struct', 'corr');
  end % ii=1:length(dkstr)

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Run fits on the correlations

  % For combined fit information, use only the correlation name
  try; beamcorropt = rmfield(beamcorropt, 'coaddopt'); end
  try; beamcorropt = rmfield(beamcorropt, 'tile'); end
  fitfile = get_beamfit_filename(beamcorropt);
  if ~exist_file(fitfile)
    fprintf(1,'Calculating fits to correlations...\n');
    reduc_beamfit_fit(combbmfile, fitfile);
    fprintf(1,'Fits are stored in %s.\n', fitfile);
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Make CSV files for beam centers and channel flags

  % Add useful information to be embedded in the CSV files.
  comments = {...
      ['Parameters are based on analysis of ' num2str(length(tags)) ...
       ' tags from ' sprintf('%04i',nreal) 'real TODs.'], ...
      ['Beam center analysis under sernum ' sprintf('%04i',nbase) '.'],...
    };

  % Make the "final" beam centers using the per-tile fallback method.
  reduc_calc_beamcenters(fitfile, tags{1}(1:8), '4paramtile', comments);
  % Use the per-tile fits to build the channel flags:
  [fitdir dum dum] = fileparts(fitfile);
  beamcencsv = fullfile(fitdir, 'beamcen.csv');
  reduc_beamcenter_chflag(beamcencsv, tags{1}(1:8), fwhm, comments);
end

