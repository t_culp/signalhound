function [cc,stdc,aboffc,ampc]=calc_beamfit_cut(r,th,amp,p,ind)
% [cc,stdc,aboffc,ampc]=calc_beamfit_cut(r,th,amp,p,ind)
%
% Performs consistency cuts on a set of beam fit data.
%
% INPUTS
%
%   r,th     Beam positions from beam fit. See reduc_beamfit_fit().
%   amp      Correlation amplitude from fit. See reduc_beamfit_fit().
%
%   p,ind    Standard p,ind structures from get_array_info().
%
% OUTPUTS
%   cc       Boolean mask for all detectors indicating a pass or
%            failure. Output is the logical-and of all further
%            individual cut masks.
%
%   stdc     Cut mask for std < 1deg over all scan dirs and decks.
%
%   aboffc   Cut mask for average A/B pos offset < 0.5deg.
%
%   ampc     Cut mask for amplitude consistency with respect to
%            per-frequency median.
%

  % Detectors of different frequencies tend to differ in their
  % characteristics, so compute the median on a per-frequency basis.
  ampmed = zeros(size(amp,1),1);
  freqs = unique(p.band(ind.la));
  for ii=1:length(freqs)
    thisfreq = (p.band == freqs(ii));
    ampmed(thisfreq) = nanmedian(rvec(amp(thisfreq,:,:)));
  end

  rcos=r.*cos(th*pi/180);
  rsin=r.*sin(th*pi/180);

  % don't use dk- and sdir-averaged versions
  maxsdir=max(1,size(rcos,2)-1);
  maxdk=max(1,size(rcos,3)-1);
  nch=size(rcos,1);

  % Require that std over scan dir, dk angles be < 1 degree
  tmprcos=reshape(rcos(:,1:maxsdir,1:maxdk),nch,[],1);
  tmprsin=reshape(rsin(:,1:maxsdir,1:maxdk),nch,[],1);
  tmpcc=sqrt(nanstd(tmprcos,[],2).^2 + nanstd(tmprsin,[],2).^2) < 1;

  stdc = tmpcc;
  cc = tmpcc;

  % Require that A/B offset be < 0.5 degree
  tmprcos=rcos(:,end,end);
  tmprsin=rsin(:,end,end);
  tmpcc=sqrt((tmprcos(ind.a)-tmprcos(ind.b)).^2+(tmprsin(ind.a)-tmprsin(ind.b)).^2)<0.5;

  aboffc = false(size(ind.e));
  aboffc(ind.a) = tmpcc;
  aboffc(ind.b) = tmpcc;
  cc(ind.a) = cc(ind.a) & tmpcc;
  cc(ind.b) = cc(ind.b) & tmpcc;

  % Require that amplitude be OK
  tmpamp=reshape(amp,nch,[],1);
  tmpcc=all(bsxfun(@gt,tmpamp,ampmed/3.5) & bsxfun(@lt,tmpamp,3*ampmed), 2);

  ampc = tmpcc;
  cc = cc & tmpcc;
end
