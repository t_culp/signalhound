function reduc_calc_beamcenters(beamfitfile,basedate,fallbackfit,comments)
% reduc_calc_beamcenters(beamfitfile,basedate,fallbackfit,comments)
%
% Uses the output from reduc_beamfit_fit() to generate the CSV files that
% are committed to aux_data and specify the beam centers.
%
% INPUTS
%   beamfitfile    File path to data saved by previous call to
%                  reduc_beamfit_fit().
%
%   basedate       Epoch date string to pass to get_array_info.
%
%   fallbackfit    Choose which fit to fallback to if the per-detector fits
%                  fail to pass the cuts in calc_beamfit_cut(). Valid options
%                  are:
%
%                  '4param'        Uses values from a per-rx 4-parameter fit
%                  '4paramtile'    Uses values from a per-tile 4-parameter fit
%
%   comments       Additional comments to include in output CSV files.
%                  Standard comments include basic description of column
%                  values and username/timestamp of file generation.
%
% EXAMPLE
%   fitname = 'beamfit/1711/real_e_filtp3_weight3_gs_jack24.mat';
%   [r,s] = system('whoami');
%   comments = {...
%       ['Keck Array 2015 beam centers']; ...
%       ['Created ' datestr(now(), 'yyyymmdd') ' by ' strtrim(s)]; ...
%     };
%   reduc_calc_beamcenters(fitname, '20150101', [], comments);
%

  if ~exist('fallbackfit','var') || isempty(fallbackfit)
    fallbackfit = '4paramtile';
  end

  xx = load(beamfitfile);
  [p,ind] = get_array_info(basedate);

  % Initialize the output structure with all necessary fields.
  %
  % Copy or default to values that already exist in aux_data that don't get
  % updated in this analysis.
  b.gcp = p.gcp;
  b.fwhm_maj = p.fwhm_maj;
  b.fwhm_min = p.fwhm_min;
  % For everything else, allocate empty values.
  b.r              = NaN*zeros(size(b.gcp));
  b.theta          = NaN*zeros(size(b.gcp));
  b.err_pos_x      = NaN*zeros(size(b.gcp));
  b.err_pos_y      = NaN*zeros(size(b.gcp));
  b.aboffset_r     = NaN*zeros(size(b.gcp));
  b.aboffset_th    = NaN*zeros(size(b.gcp));
  b.aboffset_err_x = NaN*zeros(size(b.gcp));
  b.aboffset_err_y = NaN*zeros(size(b.gcp));
  % Specify the units in the same order as fields were added to b.
  units.gcp            = '(null)';
  units.fwhm_maj       = 'deg';
  units.fwhm_min       = 'deg';
  units.r              = 'deg';
  units.theta          = 'deg';
  units.err_pos_x      = 'deg';
  units.err_pos_y      = 'deg';
  units.aboffset_r     = 'deg';
  units.aboffset_th    = 'deg';
  units.aboffset_err_x = 'deg';
  units.aboffset_err_y = 'deg';

  % Determine number of scan directions which may include an average over
  % both if two scan directions were provided.
  nscd = max(size(xx.r2,2)-1, 1);
  % The number of deck angles and detectors
  ndks = numel(xx.dklist);
  ndet = numel(ind.e);

  % r3,th3 are the r,theta positions for A and B maps processed independently
  [lat3 lon3] = reckon(0, 0, xx.r3, xx.th3+90);
  x3 = xx.r3 .* cos(xx.th3*pi/180);
  y3 = xx.r3 .* sin(xx.th3*pi/180);
  % Make weighted mean and std for all.
  %
  % Reshape and combine the scan dir and deck dimensions to permit reduction
  % over a single dimension in function calls.
  w = reshape(xx.amp3, ndet, [], 1);
  w(w<0) = 0;
  x = reshape(x3(:,1:nscd,1:ndks), ndet, [], 1);
  y = reshape(y3(:,1:nscd,1:ndks), ndet, [], 1);
  % Weighted means
  xw = nansum(x.*w,2) ./ nansum(w,2);
  yw = nansum(y.*w,2) ./ nansum(w,2);
  % Standard deviations. nanvar can't act on whole matrices over a particular
  % dimension, so we have to drop down to a loop here :-(
  xstd = zeros(size(x,1),1);
  ystd = zeros(size(y,1),1);
  for ii=1:size(x,1)
    xstd(ii) = sqrt(nanvar(x(ii,:),w(ii,:),2));
    ystd(ii) = sqrt(nanvar(y(ii,:),w(ii,:),2));
  end
  % Transforms into polar coordinates
  r    = hypot(xw, yw);
  rstd = hypot(xstd, ystd);
  th = 180/pi * atan2(yw, xw);

  % Now do the same for the combined A-B beams.
  % Pre-allocate, then fill the A detectors with A-B fits.
  abw = NaN*zeros(size(w));
  abx = NaN*zeros(size(x));
  aby = NaN*zeros(size(y));
  abxw = NaN*zeros(size(x,1),1);
  abyw = NaN*zeros(size(y,1),1);

  abw(ind.a,:) = w(ind.a,:) + w(ind.b,:);
  abx(ind.a,:) = x(ind.a,:) - x(ind.b,:);
  aby(ind.a,:) = y(ind.a,:) - y(ind.b,:);
  % Weighted means
  abxw(ind.a,:) = nansum(abx(ind.a,:).*abw(ind.a,:),2) ./ nansum(abw(ind.a,:),2);
  abyw(ind.a,:) = nansum(aby(ind.a,:).*abw(ind.a,:),2) ./ nansum(abw(ind.a,:),2);
  % Standard deviations
  abxstd = NaN*zeros(size(abw,1),1);
  abystd = NaN*zeros(size(abw,1),1);
  for ii=rvec(ind.a)
    abxstd(ii) = sqrt(nanvar(abx(ii,:),abw(ii,:),2));
    abystd(ii) = sqrt(nanvar(aby(ii,:),abw(ii,:),2));
  end
  % Copy from A indices to B indices as well
  abw(ind.b) = abw(ind.a);
  abxw(ind.b) = abxw(ind.a);
  abyw(ind.b) = abyw(ind.a);
  abxstd(ind.b) = abxstd(ind.a);
  abystd(ind.b) = abystd(ind.a);
  % Transform to polar coordinates
  abr    = hypot(abxw, abyw);
  abrstd = hypot(abxstd, abystd);
  abth = 180/pi * atan2(abyw, abxw);

  % Apply standard fit cuts to determine how values are chosen.
  [cc,stdcc,abcc,ampcc] = calc_beamfit_cut(xx.r3, xx.th3, xx.amp3, p, ind);

  % Use per-detector fits for pairs that pass the cut...
  b.r(cc) = r(cc);
  b.theta(cc) = th(cc);
  % ...and use an appropriate 4-param fit when detectors fail the cut:
  switch fallbackfit
    case '4param'
      b.r(~cc) = xx.r6(~cc);
      b.theta(~cc) = xx.th6(~cc);
      % For use in comments later
      didpt = 'per-rx';
    case '4paramtile'
      b.r(~cc) = xx.r8(~cc);
      b.theta(~cc) = xx.th8(~cc);
      didpt = 'per-tile';
    otherwise
      error('Unrecognized fallback fit type.')
  end
  % Use per-detector error no matter what source r,th come from.
  b.err_pos_x = xstd;
  b.err_pos_y = ystd;

  % Include A/B offsets
  b.aboffset_r = abr;
  b.aboffset_th = abth;
  b.aboffset_err_x = abxstd;
  b.aboffset_err_y = abystd;

  % Make sure open channels are NaN'd.
  b.r(ind.o) = NaN;
  b.theta(ind.o) = NaN;
  b.err_pos_x(ind.o) = NaN;
  b.err_pos_y(ind.o) = NaN;
  b.aboffset_r(ind.o) = NaN;
  b.aboffset_th(ind.o) = NaN;
  b.aboffset_err_x(ind.o) = NaN;
  b.aboffset_err_y(ind.o) = NaN;

  % Get a few pieces of information common to all files to be written
  datestamp = datestr(now(), 'yyyymmdd');
  [r,username] = system_safe('whoami'); username = strtrim(username);
  fullcmts = [...
      {['CMB-DERIVED BEAM CENTERS'],...
       ['CREATED ' datestamp ' BY ' upper(username)], ...
       ['Channels which fail consistency checks in map correlation '...
        'fallback to using'], ...
       ['    beam centers derived from a ' didpt ' 4-parameter model '...
        'fit to remaining']...
       ['    channels.'],...
      },...
      rvec(comments),...
    ];

  % Save the beam center CSV file
  [fitdir fitbase ext] = fileparts(beamfitfile);
  cenpath = fullfile(fitdir, 'beamcen.csv');
  fprintf(1,'Saving beam centers to %s...\n', cenpath);
  save_csv(cenpath, b, 'units',units, 'comment',fullcmts);

  % For diagnostics, produce a companion CSV which provides the cut masks.
  z.gcp = b.gcp;
  z.overall = uint8(cc); % Convert logical to integer explicitly
  z.std_r_cut = uint8(stdcc);
  z.aboff_cut = uint8(abcc);
  z.amp_cut = uint8(ampcc);

  zunits.gcp = '(null)';
  zunits.overall = '(null)';
  zunits.std_r_cut = '(null)';
  zunits.aboff_cut = '(null)';
  zunits.amp_cut = '(null)';
  zcmts = fullcmts;
  zcmts{1} = 'CONSISTENCY CHECK CUT MASKS ON CMB-DERIVED BEAM CENTERS';

  cutpath = [cenpath(1:end-4) '_cuts.csv'];
  fprintf(1,'Saving cut mask diagnostics to %s...\n', cutpath);
  save_csv(cutpath, z, 'units',zunits, 'comment',zcmts)


  % The differential pointing file is actually the same as beam centers, so
  % dump another copy of the given data to disk to make it clear the file is
  % needed.
  difpath = fullfile(fitdir, 'diffpoint.csv');
  fprintf(1,'Saving diff point parameters to %s...\n', difpath);
  save_csv(difpath, b, 'units',units, 'comment',fullcmts);


  % Generate corrected chi angles based on the new beam centers.
  [pchi,rot] = beams2chi(basedate, cenpath);
  rxs = rvec(unique(p.rx(ind.l)));
  rotstr = arrayfun(@(rx,rr) ...
    sprintf('rx%01i = %0.3f',rx,rr), rxs, rot, ...
    'uniformoutput',false);
  rotstr = sprintf('%s, ', rotstr{:});
  rotstr = rotstr(1:end-2);

  cunits.gcp = '(null)';
  cunits.theta = 'deg';
  cunits.chi = 'deg';
  ccmts = [...
      {['ADJUSTED CHI ANGLES FOR OBSERVED BEAM CENTERS'],...
       ['CREATED ' datestamp ' BY ' upper(username)],...
       ['Ideal centers are fit to observed centers with 4-parameter '...
        'model, and the'],...
       ['    best-fit overall rotation is used to modify the ideal '...
        'chi angles.'],...
       ['Adjustment angles (in degrees): '],...
       ['    ' rotstr],...
      },...
      rvec(comments),...
    ];

  chipath = fullfile(fitdir, 'chi.csv');
  fprintf(1,'Saving adjusted chi angles to %s...\n', chipath);
  save_csv(chipath, pchi, 'units',cunits, 'comment',ccmts);

  %%
  % Now output CSV files that contain pointing information for the other
  % types of fits stored in the fit file.

  disp('Creating diagnostic CSV files...')

  clear b units;
  % Initialize the output structure with all necessary fields.
  %
  % Provide GCP as a common reference point
  b.gcp = p.gcp;
  % For everything else, allocate empty values.
  b.r              = NaN*zeros(size(b.gcp));
  b.theta          = NaN*zeros(size(b.gcp));
  % Specify the units in the same order as fields were added to b.
  units.gcp            = '(null)';
  units.r              = 'deg';
  units.theta          = 'deg';

  x = xx.r3 .* cos(xx.th3*pi/180);
  y = xx.r3 .* sin(xx.th3*pi/180);
  % Use weighted mean of positions.
  %
  % Reshape and combine the scan dir and deck dimensions to permit reduction
  % over a single dimension in function calls.
  w = reshape(xx.amp3, ndet, [], 1);
  w(w<0) = 0;
  x = reshape(x3(:,1:nscd,1:ndks), ndet, [], 1);
  y = reshape(y3(:,1:nscd,1:ndks), ndet, [], 1);
  % Weighted means
  xw = nansum(x.*w,2) ./ nansum(w,2);
  yw = nansum(y.*w,2) ./ nansum(w,2);
  % Transforms into polar coordinates and store
  b.r     = hypot(xw, yw);
  b.theta = 180/pi * atan2(yw, xw);
  fullcmts = [...
      {['CREATED ' datestamp ' BY ' upper(username)], ...
       ['Weighted mean of per-detector fits. No fallback applied. For ' ...
        'diagnostic'], ...
       ['    use only.']
      },...
      rvec(comments),...
    ];
  outfile = fullfile(fitdir, 'beamcen_perdet.csv');
  fprintf(1,'Saving per-pair fits to %s..\n', outfile);
  save_csv(outfile, b, 'units',units, 'comment',fullcmts);

  b.r     = xx.r6;
  b.theta = xx.th6;
  fullcmts = [...
      {['CREATED ' datestamp ' BY ' upper(username)], ...
       ['Per-rx 4 parameter model fits. No fallback applied. For ' ...
        'diagnostic'], ...
       ['    use only.']
      },...
      rvec(comments),...
    ];
  outfile = fullfile(fitdir, 'beamcen_4paramrx.csv');
  fprintf(1,'Saving per-rx 4param fits to %s..\n', outfile);
  save_csv(outfile, b, 'units',units, 'comment',fullcmts);

  b.r     = xx.r8;
  b.theta = xx.th8;
  fullcmts = [...
      {['CREATED ' datestamp ' BY ' upper(username)], ...
       ['Per-rx 4 parameter model fits. No fallback applied. For ' ...
        'diagnostic'], ...
       ['    use only.']
      },...
      rvec(comments),...
    ];
  outfile = fullfile(fitdir, 'beamcen_4paramtile.csv');
  fprintf(1,'Saving per-tile 4param fits to %s..\n', outfile);
  save_csv(outfile, b, 'units',units, 'comment',fullcmts);
end

