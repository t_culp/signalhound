function reduc_makedeltaaps(mapname, apsopt, do_farm)
% function reduc_makedeltaaps(mapname, apsopt, do_farm)
%
% creates aps for maps of any ell following pattern in mapname
% mapname   =  (string) containing mapname pattern with wildcards to
%              create aps of.
%            ie. '0704/???E_allcmb_filtp3_weight3_gs_dp1100_jack0.mat'
% apsopt    =  the usual apsopts 
% do_farm   =  0 (default) - run locally
%           =  n>0 - start up n jobs that work through the maps, 
%              use for instance 50.
%  
% eg:
% B2
% apsopt.pure_b='normal'
% apsopt.purifmatname={'/n/panlfs2/bicep/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat'};
% mapname='0704/???E_allcmb_filtp3_weight3_gs_dp1100_jack0.mat'
% reduc_makedeltaaps(mapname,apsopt)
% mapname='0704/???B_allcmb_filtp3_weight3_gs_dp1100_jack0.mat'
% reduc_makedeltaaps(mapname,apsopt)
%
% keck 2012
%  apsopt.pure_b='normal'
%  apsopt.purifmatname={'/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_rxa_proj.mat'};
%  mapname='1453/???E_allcmb_filtp3_weight3_gs_dp1100_jack0.mat'
%  reduc_makedeltaaps(mapname,apsopt,50)
%  mapname='1453/???B_allcmb_filtp3_weight3_gs_dp1100_jack0.mat'
%  reduc_makedeltaaps(mapname,apsopt,50)
%
% keck 2013
%  apsopt.pure_b='normal'
%  apsopt.purifmatname={'/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0705/healpix_red_spectrum_lmax700_beamB2bbns_reob0705_rxa_proj.mat'};
%  mapname='1454/???E_allcmb_filtp3_weight3_gs_dp1100_jack0.mat'
%  reduc_makedeltaaps(mapname,apsopt,50)
%  mapname='1454/???B_allcmb_filtp3_weight3_gs_dp1100_jack0.mat'
%  reduc_makedeltaaps(mapname,apsopt,50)
%
% keck 2012 + 2013
% apsopt.pure_b='normal'
% apsopt.purifmatname={'/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat'};
% mapname='1455/???E_allcmb_filtp3_weight3_gs_dp1100_jack0.mat'
% reduc_makedeltaaps(mapname,apsopt,50)
% mapname='1455/???B_allcmb_filtp3_weight3_gs_dp1100_jack0.mat'
% reduc_makedeltaaps(mapname,apsopt,50)
%  
% B2 for pure_e
% apsopt.pure_b='normal'
% apsopt.purifmatname={'/n/bicepfs2/bicep2/pipeline/matrixdata/c_t/0704/healpix_red_spectrum_lmax700_beamB2bbns_reob0704_proj.mat'};
% apsopt.pure_e=true;
% mapname='0706/???E_allcmb_filtp3_weight3_gs_dp1100_jack0.mat'
% reduc_makedeltaaps(mapname,apsopt,50)
%  
% for BK14_95=K2014_95 x BK14_150:
%  
% apsopt.pure_b='normal'
% apsopt.purifmatname={'/n/panlfs2/bicep/keck/pipeline/matrixdata/c_t/0706/healpix_red_spectrum_lmax700_beamB2bbns_reob0706_rxa_proj.mat'};
% apsopt.commonmask='gmean';
% mapname={'1407/???B_d_100GHz_filtp3_weight3_gs_dp1100_jack0.mat','d_100','aabd_150'}
% reduc_makedeltaaps(mapname,apsopt,50)
% mapname={'1407/???E_d_100GHz_filtp3_weight3_gs_dp1100_jack0.mat','d_100','aabd_150'}
% reduc_makedeltaaps(mapname,apsopt,50) 
%  
%  in the end run:
%  babysitjobs('deltaaps/*.mat', 'wait5')

if(~exist('do_farm','var'))
  do_farm=0;
end

% get defaul apsopt
apsopt = get_default_apsopt(apsopt);

% we always want to keep all the maps for bpwf
apsopt.howtojack='none';

% don't apply ukpervolt
apsopt.ukpervolt='none';

% get rid of coaddopts
apsopt.save_coaddopt=0;
 
% tell reduc_makeaps to work through the list of maps randomly
% and not to overwrite existing aps:
apsopt.random_order=1;
apsopt.update=1

QUEUE='serial_requeue,general,itc_cluster';

%get username
maxtime=120; %minutes
if(isfield(apsopt, 'purifmatname'))
  if length(mapname)>1
    MEM=30000;
  end
  MEM=25000;
else
  MEM=5000;
end

cmd='reduc_makeaps(mapname, apsopt)';
if do_farm
  if ~exist('farmfiles/deltaaps', 'dir')
    system_safe('mkdir farmfiles/deltaaps')
  end
  for ii=1:do_farm
    farmit('farmfiles/deltaaps',cmd,'var',{'mapname', 'apsopt'},'queue',QUEUE,'mem',MEM,'maxtime',maxtime,'submit',0);
  end
else
  eval(cmd)
end
 
return
