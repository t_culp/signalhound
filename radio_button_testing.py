import matplotlib.pyplot as plt
from matplotlib.widgets import RadioButtons
import Tkinter
import os

plt.ion()
fig, ax = plt.subplots()
medianPlot = None
root = None
startBox = None
endBox = None
okBtn = None
cancelBtn = None
startBoxInput = '' 
endBoxInput = ''
plt.subplots_adjust(bottom = 0.2)
# [0.7, 0.05, 0.1, 0.075]
medianText = fig.text(0.85, 0.15, 'median', ha='center', va='center',
    transform=ax.transAxes)
radioBtns = RadioButtons(plt.axes([0.8, 0.025, 0.15, 0.1]), ('On', 'Off'), 1)

# base path
base = 'C:\\Users\\keck\\Documents\\signalhound'

def retrieveInputFields():
  global startBoxInput
  global endBoxInput
  global root
  startBoxInput = startBox.get()
  endBoxInput = endBox.get()
  root.destroy()

def medianButtons(label):
  global medianPlot
  if label == 'On':
    mediansPath = os.path.join(base, 'medians')
    if not os.path.isdir(mediansPath):
      os.mkdir(mediansPath)
    # date selector...
    global root
    global startBox
    global endBox
    global startBoxInput
    global endBoxInput
    global okBtn
    global cancelBtn
    root = Tkinter.Tk()
    root.title('Enter start and end dates')
    Tkinter.Label(root, text='Start date').grid(row=0)
    Tkinter.Label(root, text='End date').grid(row=1)
    startBox = Tkinter.Entry(root)
    endBox = Tkinter.Entry(root)
    startBox.grid(row=0, column=1)
    endBox.grid(row=1, column=1)
    okBtn = Tkinter.Button(root, text='OK', command=retrieveInputFields).grid(row=3,
        column=0, sticky='W', pady=4)
    cancelBtn = Tkinter.Button(root, text='Cancel', command=root.destroy).grid(row=3, column=1,
        sticky='W', pady=4)
    root.mainloop()
    del root
    # check the dates here and make a plot (medianPlot in rsweepview)
  else:
    # remove the plot (medianPlot.hide() or something, perhaps)
    pass

plt.draw()
radioBtns.on_clicked(medianButtons)

while True:
  plt.pause(0.05)
