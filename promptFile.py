import Tkinter
import tkFileDialog

def promptFile():
  root = Tkinter.Tk()
  root.withdraw()
  return tkFileDialog.askopenfilename(parent=root, 
    title='Select a file', 
    filetypes = ((".csv files", "*.csv"), ("All files", "*.*")))

# if cancel is selected, an empty string is returned
print promptFile()
