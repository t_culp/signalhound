# organize_traces.py
#
# turns a directory full of traces with names like 20160611_111111_trace.csv
# into a directory full of subdirectories with names like 20160611/ which
# furthermore contain the traces with names like 20160611_111111_trace.csv
#
# takes an optional -p flag which specifies the path you want to organize.
# otherwise, defaults to the current working directory to organize.

import os
import sys
import re
import sets
import shutil

path = os.getcwd() 
i = 1
while i < len(sys.argv):
  if sys.argv[i] == '-p':
    sys.argv.pop(i)
    path = sys.argv.pop(i)
  else:
    i += 1

if not os.path.isdir(path):
  print 'Invalid path supplied.'
  exit()

dates = sets.Set()

traces = filter(lambda elt : re.match('^\d{8}_\d{6}_trace.csv$', elt) is not\
  None, os.listdir(path))

for elt in traces:
  dates.add(re.match('^(\d{8})_\d{6}_trace.csv$', elt).group(1))

while len(dates) != 0:
  date = dates.pop()
  subdir = os.path.join(path, date)
  if not os.path.isdir(subdir):
    os.mkdir(subdir)
  i = 0
  length = len(traces)
  while i < length:
    if re.match('^' + date + '_\d{6}_trace.csv$', traces[i]) is not None:
      shutil.move(os.path.join(path, traces[i]), subdir)
      del traces[i]
      length -= 1
    else:
      i += 1
