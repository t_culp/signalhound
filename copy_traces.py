# copy_traces.py
#
# takes an origin  directory full of traces with names like
# 20160611_111111_trace.csv and copies the traces into a destination directory
# full of subdirectories with names like 20160611/ which furthermore contain the
# traces with names like 20160611_111111_trace.csv
#
# takes optional -f and -t flags which specify the paths you want to copy from
# and to, respectively. Otherwise, defaults to the current working directory to
# organize.

import os
import sys
import re
import shutil

orgin = os.getcwd() 
destination = os.getcwd()
i = 1
while i < len(sys.argv):
  if sys.argv[i] == '-f':
    sys.argv.pop(i)
    origin = sys.argv.pop(i)
  elif sys.argv[i] == '-t':
    sys.argv.pop(i)
    destination = sys.argv.pop(i)
  else:
    i += 1

if not os.path.isdir(origin) or not os.path.isdir(destination):
  print 'Invalid path(s) supplied.'
  exit()

dates = set()

traces = filter(lambda elt : re.match('^\d{8}_\d{6}_trace.csv$', elt) is not\
  None, os.listdir(origin))

for elt in traces:
  dates.add(re.match('^(\d{8})_\d{6}_trace.csv$', elt).group(1))

while len(dates) != 0:
  date = dates.pop()
  subdir = os.path.join(destination, date)
  if not os.path.isdir(subdir):
    os.mkdir(subdir)
  i = 0
  length = len(traces)
  while i < length:
    if re.match('^' + date + '_\d{6}_trace.csv$', traces[i]) is not None:
      if not os.path.exists(os.path.join(subdir, traces[i])):
        shutil.copy(os.path.join(origin, traces[i]), subdir)
      del traces[i]
      length -= 1
    else:
      i += 1
