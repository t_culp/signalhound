import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy
import time
import datetime
import os
import threading 
import math
import re
from matplotlib.widgets import RadioButtons
import Tkinter
import tkMessageBox
import tkFileDialog
import sys

# a lot of initialization and getting objects ready to be drawn
plt.ion()
fig, ax = plt.subplots()
fig.canvas.set_window_title('12.4 GHz Spectrum Analysis')
plt.xlabel("Frequency (MHz)")
plt.ylabel("Amplitude (dBm)")
plt.grid()
plt.subplots_adjust(bottom=0.2)
coords = plt.text(0.2, 0.9, '', ha='center', va='center',
  transform=ax.transAxes)
elapsedTime = plt.text(0.8, 0.9, 'time elapsed:', ha='center', va='center',
  transform=ax.transAxes)
points, = ax.plot([0], [0], 'b', zorder=10)
medianPlot = None
plot1 = None
plot2 = None
plot3 = None
root = None
startBox = None
endBox = None
startBoxInput = '' 
endBoxInput = ''
title = fig.text(0.5, 0.95, '', ha='center', va='center', transform=ax.transAxes)
medianText = fig.text(0.175, 0.135, 'median', ha='center', va='center',
  transform=ax.transAxes, size=10, family='monospace', name='monospace')
# the 1 in the last argument specifies that 'Off' is selected by default.
medianRadioBtns = RadioButtons(plt.axes([0.125, 0.01, 0.1, 0.1]), ('On', 'Off'), 1)
plot1Text = fig.text(0.35, 0.135, 'plot 1', ha='center', va='center',
  transform=ax.transAxes, size=10, family='monospace', name='monospace')
plot1Btns = RadioButtons(plt.axes([0.3, 0.01, 0.1, 0.1]), ('On', 'Off'), 1)
plot2Text = fig.text(0.525, 0.135, 'plot 2', ha='center', va='center',
  transform=ax.transAxes, size=10, family='monospace', name='monospace')
plot2Btns = RadioButtons(plt.axes([0.475, 0.01, 0.1, 0.1]), ('On', 'Off'), 1)
plot3Text = fig.text(0.7, 0.135, 'plot 3', ha='center', va='center',
  transform=ax.transAxes, size=10, family='monospace', name='monospace')
plot3Btns = RadioButtons(plt.axes([0.65, 0.01, 0.1, 0.1]), ('On', 'Off'), 1)
tMonitor = None

# currently loaded frequencies (MHz) and amplitudes (dBm) 
freqs = []
amps = []
medianAmps = []
medianFreqs = []
plot1Amps = []
plot1Freqs = []
plot2Amps = []
plot2Freqs = []
plot3Freqs = []
plot3Amps = []

# time at which the current plot was loaded
startTime = datetime.datetime.now().replace(microsecond=0)

# base is where this script will go looking. It will try to find
# directories like '20160608' and get the latest and greatest .csv file from

# there.
base = 'C:\\Users\\keck\\Documents\\signalhound'
i = 1
while i < len(sys.argv):
  if sys.argv[i] == '-p':
    sys.argv.pop(i)
    base = sys.argv.pop(i)
  else:
    i += 1
del i
if not os.path.isdir(base):
  print 'Invalid path supplied.'
  exit()

# boolean keeping track of the state of the figure
closed = threading.Event()

def promptFile():
  global root
  root = Tkinter.Tk()
  root.withdraw()
  ret = tkFileDialog.askopenfilename(parent=root,
    title='Select a file',
    filetypes = (('.csv files', '*.csv'), ('All files', '*.*')))
  destroyRoot()
  return ret

# extracts the frequencies from the .csv files.
def extractFreqs(filename):
  f = open(filename, 'r')
  # discard first line of file
  f.readline()
  freqs = []
  for line in f:
    freqs.append(float(line.split(',')[0]))
  f.close()
  return freqs

# extracts the amplitudes from the .csv files. The convert boolean indicates
# that the function will convert from mW to dBm
def extractAmps(filename, convert):
  f = open(filename, 'r') 
  # discard first line of file
  f.readline()
  amps = [] 
  # converting mW to dBm on the fly inside the loop
  if convert:
    for line in f: 
      amps.append(10 * math.log10(float(line.split(',')[1])))
  else:
    for line in f: 
      amps.append(float(line.split(',')[1]))
  f.close()
  return amps 

# contains the functionality of both extractAmps and extractFreqs. More
# efficient if both frequencies and amplitudes are needed at the same time to
# prevent having to open and scan a file twice with a call to each function.
def extractFreqsAndAmps(filename, convert):
  f = open(filename, 'r')
  f.readline()
  freqs = []
  amps = []
  if convert:
    for line in f:
      freqs.append(float(line.split(',')[0]))
      amps.append(10 * math.log10(float(line.split(',')[1])))
  else:
    for line in f:
      freqs.append(float(line.split(',')[0]))
      amps.append(float(line.split(',')[1]))
  f.close()
  return freqs, amps

# calculates a median trace over the course of the dates (datetime objects)
# given in start and end (invariant: start <= end)
def calculateMedian(start, end):
  day = datetime.timedelta(days=1)
  # this will be a list of lists... data[i][:] gives an entire trace, 
  data = []
  # all of the files we are looking at should share the same frequencies, so
  # getting an arbitrary set of frequencies will work.
  freqs = []
  subdir = os.path.join(base, start.strftime('%Y%m%d'))
  if not os.path.isdir(subdir):
    return None, None
  for trace in os.listdir(subdir):
    if re.match('^\d{8}_\d{6}_trace.csv$', trace) is not None:
      freqs = extractFreqs(os.path.join(subdir, trace))
      break
  # loop through the days and build our list of lists of amplitudes
  while start <= end:
    subdir = os.path.join(base, start.strftime('%Y%m%d'))
    if not os.path.isdir(subdir):
      return None, None
    for trace in os.listdir(subdir):
      if re.match('^\d{8}_\d{6}_trace.csv$', trace) is not None:
        data.append(extractAmps(os.path.join(subdir, trace), False))
    start = start + day
  medians = [None] * len(freqs)
  for i in xrange(len(freqs)):
    fixedFrequency = [None] * len(data)
    for j in xrange(len(data)):
      fixedFrequency[j] = data[j][i]
    medians[i] = numpy.median(fixedFrequency)
  return freqs, medians

# destroys the root Tkinter window
def destroyRoot():
  global root
  root.quit()
  root.destroy()
  root = None

# retrieves the date strings from the median dates input box (and then destroys
# the root Tkinter window).
def retrieveInputFields():
  global startBoxInput, endBoxInput
  startBoxInput = startBox.get()
  endBoxInput = endBox.get()
  destroyRoot()

# defines action for the median radio buttons. When the median is turned on, the
# function checks for the existence of a medians folder, prompts for dates from 
# which to calculate a median, checks if that median has already been calculated
# and if the user would like to plot or recalculate that median, and acts
# accordingly, saving any medians calculated to the medians subdirectory in the
# base path. When the median is turned off, the line is hidden.
def medianButtons(label):
  global medianPlot, medianFreqs, medianAmps, root
  if label == 'On' and root is None:
    mediansPath = os.path.join(base, 'medians')
    if not os.path.isdir(mediansPath):
      os.mkdir(mediansPath)
    # prompt for dates
    global startBox, endBox, startBoxInput, endBoxInput
    root = Tkinter.Tk()
    root.title('Enter start and end dates')
    Tkinter.Label(root, text='Start date').grid(row=0)
    Tkinter.Label(root, text='End date').grid(row=1)
    startBox = Tkinter.Entry(root)
    endBox = Tkinter.Entry(root)
    startBox.grid(row=0, column=1)
    endBox.grid(row=1, column=1)
    okBtn = Tkinter.Button(root, text='OK', command=retrieveInputFields)\
      .grid(row=3, column=0, sticky='W', pady=4)
    cancelBtn = Tkinter.Button(root, text='Cancel', command=destroyRoot)\
      .grid(row=3, column=1, sticky='W', pady=4)
    root.mainloop()
    # check the dates (startBoxInput and endBoxInput) here and make a plot 
    # if both strings are empty, a cancel likely occurred.
    if startBoxInput == '' and endBoxInput == '':
      medianRadioBtns.set_active(1)
      return
    startDate = None
    endDate = None
    try:
      startDate = datetime.datetime.strptime(startBoxInput, '%Y-%b-%d')
      endDate = datetime.datetime.strptime(endBoxInput, '%Y-%b-%d')
    except ValueError: 
      temp = Tkinter.Tk()
      temp.withdraw()
      tkMessageBox.showerror('Error', 'Invalid date format (must be yyyy-mmm-dd)')
      temp.destroy()
      medianRadioBtns.set_active(1)
      return
    finally:
      startBoxInput = ''
      endBoxInput = ''
    if startDate > endDate:
      temp = Tkinter.Tk()
      temp.withdraw()
      tkMessageBox.showerror('Error', 'End date is earlier than start date')
      temp.destroy()
      medianRadioBtns.set_active(1)
      return
    # checking to see if this median has already been calculated
    possibleMedian = os.path.join(base, 'medians', '%s_%s_median.csv' %
      (startDate.strftime('%Y%m%d'), endDate.strftime('%Y%m%d')))
    temp = Tkinter.Tk()
    temp.withdraw()
    if os.path.isfile(possibleMedian) and tkMessageBox.askyesno('Median exists',
        ('The requested median already exists.\n%s\nPlot the existing median? ' +
        '(Median will be recalculated if no.)') % possibleMedian):
      medianFreqs, medianAmps = extractFreqsAndAmps(possibleMedian, True)
    else:
      medianFreqs, medianAmps = calculateMedian(startDate, endDate)
      if medianFreqs is None:
        tkMessageBox.showerror('Error', 
          'Missing directory between %s and %s, inclusive.' % 
          (startDate.strftime('%Y-%b-%d'), endDate.strftime('%Y-%b-%d')))
        temp.destroy()
        medianRadioBtns.set_active(1)
        medianFreqs = []
        return
      ret = 'Frequency (MHz),Amplitude Min(mW)\n'
      for i in xrange(len(medianFreqs)):
        ret = ret + str(medianFreqs[i]) + ',' + str(medianAmps[i]) + '\n'
      out = open(possibleMedian, 'w')
      out.write(ret)
      out.close()
      medianAmps = map(lambda mw : 10 * math.log10(mw), medianAmps)
    temp.destroy()
    if medianPlot is None:
      medianPlot, = ax.plot(medianFreqs, medianAmps, 'g')
    else:
      medianPlot.set_data(medianFreqs, medianAmps)
      medianPlot.set_visible(True)
    #ax.relim()
    #ax.autoscale_view(True, True, True)
    medianText.set_text('median:\n' + os.path.basename(possibleMedian))
  else:
    # remove the plot 
    medianFreqs = []
    medianAmps = []
    medianText.set_text('median')
    if medianPlot is not None:
      medianPlot.set_visible(False)

# handler for the plot 1 radio buttons.
def plot1Buttons(label):
  global plot1Freqs, plot1Amps, plot1, root
  if label == 'On' and root is None:
    filename = promptFile()
    if filename == '':
      plot1Btns.set_active(1)
      return
    plot1Freqs, plot1Amps = extractFreqsAndAmps(filename, True)
    if plot1 is None:
      plot1, = ax.plot(plot1Freqs, plot1Amps, 'r')
    else:
      plot1.set_data(plot1Freqs, plot1Amps)
      plot1.set_visible(True)
    plot1Text.set_text('plot 1:\n' + os.path.basename(filename))
  else:
    plot1Freqs = []
    plot1Amps = []
    plot1Text.set_text('plot 1')
    if plot1 is not None:
      plot1.set_visible(False) 

# handler for the plot 2 radio buttons
def plot2Buttons(label):
  global plot2Freqs, plot2Amps, plot2, root
  if label == 'On' and root is None:
    filename = promptFile()
    if filename == '':
      plot2Btns.set_active(1)
      return
    plot2Freqs, plot2Amps = extractFreqsAndAmps(filename, True)
    if plot2 is None:
      plot2, = ax.plot(plot2Freqs, plot2Amps, 'm')
    else:
      plot2.set_data(plot2Freqs, plot2Amps)
      plot2.set_visible(True)
    plot2Text.set_text('plot 2:\n' + os.path.basename(filename))
  else:
    plot2Freqs = []
    plot2Amps = []
    plot2Text.set_text('plot 2')
    if plot2 is not None:
      plot2.set_visible(False)

# handler for the plot 3 radio buttons
def plot3Buttons(label):
  global plot3Freqs, plot3Amps, plot3, root
  if label == 'On' and root is None:
    filename = promptFile()
    if filename == '':
      plot3Btns.set_active(1)
      return
    plot3Freqs, plot3Amps = extractFreqsAndAmps(filename, True)
    if plot3 is None:
      plot3, = ax.plot(plot3Freqs, plot3Amps, 'k')
    else:
      plot3.set_data(plot3Freqs, plot3Amps)
      plot3.set_visible(True)
    plot3Text.set_text('plot 3:\n' + os.path.basename(filename))
  else: 
    plot3Freqs = []
    plot3Amps = []
    plot3Text.set_text('plot 3')
    if plot3 is not None:
      plot3.set_visible(False) 

# takes a sorted list (most to least recent) of directories and returns the
# most recent matching .csv file according to the established file structure.
def checkDirectories(dirs):
  for d in dirs:
    d = os.path.join(base, d)
    csvs = []
    for i in os.listdir(d):
      if re.match('^\d{8}_\d{6}_trace.csv$', i) is not None:
        csvs.append(os.path.join(d, i))
    if len(csvs) == 0:
      continue
    return max(csvs, key=os.path.getctime)
  return None

# attempts to convert dateString to a datetime object using formatString.
# Returns None if this is impossible (designed to be used in a higher-order
# function).
def dateStringToDatetime(dateString, formatString):
  try:
    return datetime.datetime.strptime(dateString, formatString)
  except ValueError:
    return None

# returns the most recent file. Waits until a file is available. Returns None
# only when the GUI is closed.
def getRecent():
  recent = None
  while recent is None and not closed.isSet():
    # creates a list of strings representing subdirectories in the base
    # directory in the correct date format. checkDirectories then returns the
    # most recent .csv file (or None if none). Waits if none.
    dirs = map(lambda elt : elt.strftime('%Y%m%d'),
      filter(lambda elt : elt is not None, 
      map(lambda elt : dateStringToDatetime(elt, '%Y%m%d'),
      filter(lambda elt : re.match('^\d{8}$', elt) is not None, 
      os.listdir(base)))))
    dirs.reverse()
    recent = checkDirectories(dirs)
    if recent is None:
      time.sleep(1.0)
  return recent

# given a filename, extracts the freqencies and amplitudes into a tuple,
# respectively. Converts the amplitudes to dBm from mW for plotting purposes.
# This function is also considerate of the situation at hand: it makes sure that
# the file has been completely written first (i.e. number of lines in the file
# is not changing). The arrays are stored in the global variables, so this
# function should ONLY be used for standard GUI proceedings. The likes of
# extractAmps simply take a file and extract the amplitudes without any other
# considerations.
def currentTraceExtractFreqsAndAmps(filename):
  f = open(filename) 
  # this procedure ensures that the number of lines in the file is not changing.
  # This allows there to be any number of lines in the file instead of depending
  # on a fixed number like 62000. However, this does not allow for 0 lines in a
  # file. 
  i = 0
  j = -1
  while i == 0:
    for line in f:
      i += 1
    f.seek(0)
    j = 0
    time.sleep(0.25)
    for line in f:
      j += 1
    f.seek(0)
    if i != j:
      i = 0
      time.sleep(0.01)
  # discard first line of file
  f.readline()
  global freqs, amps
  freqs[:] = []
  amps[:] = []
  for line in f:
    # s[0] is the frequency, s[1] is the min amp, s[2] is the max amp
    s = line.split(',')
    freqs.append(float(s[0]))
    #converting mW to dBm on the fly
    amps.append(10 * math.log10(float(s[1])))
  f.close()
  # converting the amplitudes from file in mW to dBm
  # amps = map(lambda mw : 10 * math.log10(mw), map(float, amps))

# find the index of the element of lst which most closely matches match.
# invariant: len(lst) != 0
def findClosestIndex(lst, match):
  smallestDiff = abs(match - lst[0])
  smallestDiffIndex = 0
  for i in xrange(1, len(lst)):
    diff = abs(match - lst[i])
    if diff < smallestDiff:
      smallestDiff = diff
      smallestDiffIndex = i
    else:
      break
  return smallestDiffIndex

# GUI event handler called on mouse motion
def handleMotion(evt):
  global freqs, amps, medianAmps, medianFreqs, plot1Freqs, plot1Amps, \
    plot2Freqs, plot2Amps, plot3Freqs, plot3Amps
  if evt.xdata is None or evt.ydata is None or\
  (len(freqs) == 0 and len(medianFreqs) == 0 and len(plot1Freqs) == 0 and \
    len(plot2Freqs) == 0 and len(plot3Freqs) == 0):
    return
  # generate a guess index for where I should start looking for a frequency
  # which most closely matches evt.xdata. The 2 is conditionally subtracted at
  # the end to give some room to look around.
  displayFreqs = []
  if len(freqs) == 0:
    if len(medianFreqs) == 0:
      if len(plot1Freqs) == 0:
        if len(plot2Freqs) == 0:
          displayFreqs = plot2Freqs
        else:
          displayFreqs = plot3Freqs
      else:
        displayFreqs = plot1Freqs
    else:
      displayFreqs = medianFreqs
  else:
    displayFreqs = freqs
  guess = int(round((evt.xdata - displayFreqs[0]) / 
    (displayFreqs[1] - displayFreqs[0])))
  if guess > len(displayFreqs):
    guess = len(displayFreqs) - 3
  elif guess >= 2:
    guess -= 2
  else: 
    guess = 0
  smallestDiffIndex = guess
  smallestDiff = abs(evt.xdata - displayFreqs[guess])
  for i in xrange(guess + 1, guess + 4):
    if i > len(displayFreqs) - 1:
      break
    diff = abs(evt.xdata - displayFreqs[i])
    if diff < smallestDiff:
      smallestDiff = diff
      smallestDiffIndex = i
    else:
      break
  # sometimes weird things can happen with the zoom so this is just a catch all
  # in case who knows what happens
  if smallestDiffIndex not in xrange(len(displayFreqs)):
    return
  nearbyFreq = displayFreqs[smallestDiffIndex]
  coordsText = 'frequency: %f MHz\n' % nearbyFreq 
  if len(freqs) != 0:
    coordsText = coordsText + ('current amplitude: %f dBm\n' %
      amps[findClosestIndex(freqs, nearbyFreq)])
  if len(medianFreqs) != 0:
    coordsText = coordsText + ('median amplitude: %f dBm\n' %
      medianAmps[findClosestIndex(medianFreqs, nearbyFreq)])
  if len(plot1Freqs) != 0:
    coordsText = coordsText + ('plot 1 amplitude: %f dBm\n' %
      plot1Amps[findClosestIndex(plot1Freqs, nearbyFreq)])
  if len(plot2Freqs) != 0:
    coordsText = coordsText + ('plot 2 amplitude: %f dBm\n' %
      plot2Amps[findClosestIndex(plot2Freqs, nearbyFreq)])
  if len(plot3Freqs) != 0:
    coordsText = coordsText + ('plot 3 amplitude: %f dBm\n' %
      plot3Amps[findClosestIndex(plot3Freqs, nearbyFreq)])
  # remove the last newline character
  coordsText = coordsText[:-1]
  coords.set_text(coordsText)

# if the figure is closed, an event that is checked in updatePlot,
# directoryMonitor.run, timeMonitor.run, and the main thread's execution loop 
# is switched to facilitate the clean closing of the application
def handleClose(evt):
  closed.set()

# updates the plot with the data passed in. 
def updatePlot(f):
  if closed.isSet():
    return
  global points, freqs, amps, tMonitor
  points.set_data(freqs, amps)
  ax.relim()
  ax.autoscale_view(True, True, True)
  title.set_text(f)
  plt.draw()
  if tMonitor is None:
    tMonitor = timeMonitor('time monitor')
    tMonitor.start()
  print 'update applied.'

# defines a thread which waits for directory updates. Upon directory update, the
# thread updates the plot.
class directoryMonitor(threading.Thread):
  def __init__(self, name):
    threading.Thread.__init__(self)
    self.name = name

  def run(self):
    current = None
    while not closed.isSet():
      update = getRecent()
      if current != update and update != None:
        print update
        current = update
        currentTraceExtractFreqsAndAmps(current)
        global startTime
        startTime = datetime.datetime.\
          fromtimestamp(os.path.getctime(current)).replace(microsecond=0)
        updatePlot(current)
      time.sleep(0.1)

# defines a thread which updates text in the GUI with the time elapsed since a
# new plot has been loaded.
class timeMonitor(threading.Thread):
  def __init__(self, name):
    threading.Thread.__init__(self)
    self.name = name

  def run(self):
    while not closed.isSet():
      elapsedTime.set_text('time elasped: %s' % 
        str(datetime.datetime.now().replace(microsecond=0) -
        startTime))
      time.sleep(0.01)

# connecting buttons and events to their handlers, drawing the plot, and
# starting the directory monitor. The loop at the bottom keeps the GUI open and
# accepting input from the user.
plot1Btns.on_clicked(plot1Buttons)
plot2Btns.on_clicked(plot2Buttons)
plot3Btns.on_clicked(plot3Buttons)
medianRadioBtns.on_clicked(medianButtons)
fig.canvas.mpl_connect('close_event', handleClose)
fig.canvas.mpl_connect('motion_notify_event', handleMotion)
plt.draw()

directoryMonitor('directory monitor').start()

while not closed.isSet():
  try:
    plt.pause(0.01)
  except:
    pass
  time.sleep(0.01)

