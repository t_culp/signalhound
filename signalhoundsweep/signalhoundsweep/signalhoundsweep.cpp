// signalhoundsweep.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "sa_api.h"
#include <thread>
#include <iostream>
#include <fstream>
#include <time.h>
#include <signal.h>
#include <chrono>
#include <Windows.h>
#include <cmath>
#include <iomanip>
#include "Shlwapi.h"

#pragma comment(lib, "sa_api.lib")
#pragma comment(lib, "Shlwapi.lib")

int handle = -1;
wchar_t path[MAX_PATH];

void startTimerAndSweep();
void openDevice();
void closeDevice();
void CALLBACK sweep(HWND, UINT, UINT_PTR, DWORD);
void interrupt_handler(int);

void openDevice() {
	std::cout << "API Version: " << saGetAPIVersion() << std::endl;

	saStatus openStatus = saOpenDevice(&handle);
	if (openStatus != saNoError) {
		std::cout << saGetErrorString(openStatus) << std::endl;
		return;
	}

	std::cout << "Device Found!\n";

	saDeviceType deviceType;
	saGetDeviceType(handle, &deviceType);

	std::cout << "Device Type: ";
	if (deviceType == saDeviceTypeSA44) {
		std::cout << "SA44\n";
	}
	else if (deviceType == saDeviceTypeSA44B) {
		std::cout << "SA44B\n";
	}
	else if (deviceType == saDeviceTypeSA124A) {
		std::cout << "SA124A\n";
	}
	else if (deviceType == saDeviceTypeSA124B) {
		std::cout << "SA124B\n";
	}

	int serialNumber = 0;
	saGetSerialNumber(handle, &serialNumber);
	std::cout << "Serial Number: " << serialNumber << std::endl;

	float temperature = 0.0;
	saQueryTemperature(handle, &temperature);
	std::cout << "Internal Temp: " << temperature << " C" << std::endl;

	float voltage = 0.0;
	saQueryDiagnostics(handle, &voltage);
	std::cout << "Voltage: " << voltage << " V" << std::endl;
}

void closeDevice() {
	saAbort(handle);
	saCloseDevice(handle);
}

void CALLBACK sweep(HWND hwnd, UINT msg, UINT_PTR timerId, DWORD dwTime) {
	struct tm newtime;
	__int64 ltime;
	errno_t err;

	_time64(&ltime);

	err = _gmtime64_s(&newtime, &ltime);
	if (err) {
		printf("Invalid argument to _gmtime64_s.");
		return;
	}

	wchar_t directory[MAX_PATH], subdirectory[MAX_PATH], directoryAndName[MAX_PATH], name[MAX_PATH], day[8], month[8], hour[8], minute[8], second[8];

	if (newtime.tm_mday < 10) {
		swprintf_s(day, _countof(day), L"0%d", newtime.tm_mday);
	}
	else {
		swprintf_s(day, _countof(day), L"%d", newtime.tm_mday);
	}

	if (newtime.tm_mon < 9) {
		swprintf_s(month, _countof(month), L"0%d", newtime.tm_mon + 1);
	}
	else {
		swprintf_s(month, _countof(month), L"%d", newtime.tm_mon + 1);
	}

	if (newtime.tm_hour < 10) {
		swprintf_s(hour, _countof(hour), L"0%d", newtime.tm_hour);
	}
	else {
		swprintf_s(hour, _countof(hour), L"%d", newtime.tm_hour);
	}

	if (newtime.tm_min < 10) {
		swprintf_s(minute, _countof(minute), L"0%d", newtime.tm_min);
	}
	else {
		swprintf_s(minute, _countof(minute), L"%d", newtime.tm_min);
	}

	if (newtime.tm_sec < 10) {
		swprintf_s(second, _countof(second), L"0%d", newtime.tm_sec);
	}
	else {
		swprintf_s(second, _countof(second), L"%d", newtime.tm_sec);
	}

	// Configure the device to sweep a 12.4GHz span centered on 6.2GHz
	// Average detector, with RBW/VBW equal to 250kHz
	// SA_LIN_SCALE and SA_LOG_SCALE
	saConfigCenterSpan(handle, 6.200050e9, 12.399900e9);
	saConfigAcquisition(handle, SA_MIN_MAX, SA_LOG_SCALE);
	saConfigLevel(handle, -20.0);
	saConfigSweepCoupling(handle, 250.0e3, 250.0e3, true);
	saConfigGainAtten(handle, 0, SA_AUTO_GAIN, true);

	// Initialize the device with the configuration just set
	saStatus initiateStatus = saInitiate(handle, SA_SWEEPING, 0);
	if (initiateStatus != saNoError) {
		// Handle unable to initialize
		std::cout << saGetErrorString(initiateStatus) << std::endl;
		return;
	}

	// Get sweep characteristics
	int sweepLen;
	double startFreq, binSize;
	saQuerySweepInfo(handle, &sweepLen, &startFreq, &binSize);

	std::cout << "start freq: " << startFreq << "\nbin size: " << binSize 
		<< "\nsweep length: " << sweepLen << std::endl;

	// Allocate memory for the sweep

	float *min = new float[sweepLen];
	float *max = new float[sweepLen];

	// create an array of the swept frequencies (converting Hz to MHz along the way)
	double *freqs = new double[sweepLen];
	freqs[0] = 0; // startFreq / 1.0e6;
	binSize = binSize / 1.0e6;
	for (int i = 1; i < sweepLen; ++i) {
		freqs[i] = freqs[i - 1] + binSize;
	}

  swprintf_s(subdirectory, _countof(subdirectory), L"%d%s%s",
    newtime.tm_year + 1900, month, day);
  PathCombine(directory, path, subdirectory); //L"C:\\Users\\keck\\Documents\\signalhound"
  swprintf_s(name, _countof(name), L"%d%s%s_%s%s%s_trace.csv",
    newtime.tm_year + 1900, month, day, hour, minute, second);
  PathCombine(directoryAndName, directory, name);
  
  auto start_time = std::chrono::high_resolution_clock::now();
  saStatus sweepStatus = saGetSweep_32f(handle, min, max);
  auto end_time = std::chrono::high_resolution_clock::now();
  auto time = end_time - start_time;
  std::wcout << directoryAndName << L" sweep took " <<
    std::chrono::duration_cast<std::chrono::milliseconds>(time).count() << L" ms to run.\n";

  if (sweepStatus != saNoError) {
    // Handle unable to sweep
    std::cout << saGetErrorString(sweepStatus) << ' ' << sweepStatus << '\n';
    return;
  }

  std::ofstream results;
  if (CreateDirectory(directory, NULL) || ERROR_ALREADY_EXISTS == GetLastError()) {
    results.open(directoryAndName);
  } else {
    std::cout << "Failed to create directory.\n";
    return;
  }

  // convert dBm to mW
  for (int i = 0; i < sweepLen; ++i) {
    min[i] = (float) pow(10.0, 0.1 * min[i]);
    max[i] = (float) pow(10.0, 0.1 * max[i]);
  }

  // results << std::fixed << std::setprecision(20); does not allow for scientific notation
  // results << std::setprecision(20); fixes a number of decimal places, but allows scientfic notation
  
  results << "Frequency (MHz),Amplitude Min(mW),Amplitude Max(mW)\n";

  start_time = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < sweepLen; ++i) {
    results << freqs[i] << ',' << min[i] << ',' << max[i] << '\n';
  }
  end_time = std::chrono::high_resolution_clock::now();
  time = end_time - start_time;
  std::wcout << directoryAndName << L" transfer took " <<
    std::chrono::duration_cast<std::chrono::milliseconds>(time).count() << L" ms to run.\n";
  
  results.close();

  free(min);
  free(max);
  free(freqs);
	
}

void startTimerAndSweep() {
	openDevice();
	SetTimer(NULL, 0, 120000, (TIMERPROC)&sweep);
	sweep((HWND)NULL, (UINT)NULL, (UINT_PTR)NULL, (DWORD)NULL);
}

static void interrupt_handler(int s) {
	closeDevice();
	std::cout << "Closed cleanly.\nInterrupt code: " << s << '\n';
	exit(0);
}

int main(int argc, char *argv[], char *envp[])
{
	if (argc == 1) {
		GetCurrentDirectory(_countof(path), path);
	}
	else if (argc == 2) {
		size_t origsize = strlen(argv[1]) + 1;
		size_t convertedChars = 0;
		mbstowcs_s(&convertedChars, path, origsize, argv[1], _TRUNCATE);
		if (GetFileAttributes(path) == INVALID_FILE_ATTRIBUTES) {
			std::cout << "Directory does not exist." << std::endl;
			return 1;
		}
	}
	else {
		std::cout << "Invalid number of command line parameters" << std::endl;
		return 1;
	}
	std::wcout << L"Using " << path << L" as root directory.\n";
	signal(SIGINT, interrupt_handler);
	startTimerAndSweep();
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	
	return 0;
}

